const { execSync } = require('child_process');
const fs = require('fs');
const path = require('path');

const { isEqual } = require('lodash');
const rimraf = require('rimraf');

const { dependencies } = require('../package.json');

const NODE_ENV = process.env.NODE_ENV || 'development';

const versions = {
  NODE_ENV,
};

Object.keys(dependencies).forEach(pkg => {
  // eslint-disable-next-line global-require
  const { version } = require(pkg + '/package.json');
  versions[pkg] = version;
});

const cwd = process.cwd();

const dllCacheDir = path.resolve(cwd, '.cache');
const dllCacheFile = path.resolve(dllCacheDir, 'dll-cache.json');

if (
  fs.existsSync(dllCacheFile) &&
  fs.existsSync(path.resolve('static/assets/vendor.manifest.json'))
) {
  // eslint-disable-next-line global-require
  const cache = require(dllCacheFile);
  if (isEqual(cache, versions)) {
    // eslint-disable-next-line no-console
    console.log('No changes of dependencies, skip');
    return;
  }
  // eslint-disable-next-line no-console
  console.log('changes detected, regenerate dll');
} else {
  // eslint-disable-next-line no-console
  console.log('no cache or manifest found, generate dll');
}

rimraf.sync(path.resolve(cwd, '.cache'));
rimraf.sync(path.resolve(cwd, 'static/assets'));
rimraf.sync(path.resolve(cwd, 'static/vendor.dll.*.js'));

execSync('npm run webpack:dll', {
  stdio: 'inherit',
});

if (!fs.existsSync(dllCacheDir)) {
  fs.mkdirSync(dllCacheDir);
}

fs.writeFileSync(dllCacheFile, JSON.stringify(versions, null, 2));
