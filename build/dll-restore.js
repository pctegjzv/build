const { execSync } = require('child_process');

try {
  execSync(`
mkdir -p static/assets

cp .cache/vendor.dll.*.js static
cp .cache/vendor.dll.*.js static/assets/js
cp .cache/vendor.manifest.json static/assets
`);
} catch (e) {
  // eslint-disable-next-line no-console
  console.log('no cache found');
}
