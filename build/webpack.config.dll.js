const path = require('path');

const webpack = require('webpack');

const resolve = (...args) => path.resolve('static', ...args);

const NODE_ENV = process.env.NODE_ENV || 'development';

const isProd = NODE_ENV === 'production';

module.exports = {
  mode: NODE_ENV,
  entry: {
    vendor: [resolve('app2/vendor.entry.ts')],
  },
  output: {
    filename: `[name].dll.[hash].js`,
    path: resolve('assets/js'),
    library: '[name]_[hash]',
  },
  devtool: !isProd && 'cheap-module-eval-source-map',
  module: {
    noParse: [/semantic-ui-.+\.min\.js$/],
    rules: [
      {
        test: /\.js$/,
        include: [resolve(), resolve('../node_modules/angular-ui-codemirror')],
        use: [
          'thread-loader',
          {
            loader: 'babel-loader',
            options: {
              cacheDirectory: true,
              plugins: ['angularjs-annotate'],
            },
          },
        ],
      },
      {
        test: /\.ts$/,
        loader: 'awesome-typescript-loader',
        options: {
          useBabel: true,
          useCache: true,
          transpileOnly: true,
          babelCore: '@babel/core',
        },
      },
    ],
  },
  plugins: [
    new webpack.DllPlugin({
      path: resolve('assets/[name].manifest.json'),
      name: '[name]_[hash]',
    }),
  ],
};
