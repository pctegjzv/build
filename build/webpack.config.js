const webpack = require('webpack');
const path = require('path');

const AddAssetHtmlPlugin = require('add-asset-html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const RubickI18nWebpackPlugin = require('rubick-i18n-webpack-plugin');

const vendorManifest = require('../static/assets/vendor.manifest.json');

const resolve = (...args) => path.resolve('static', ...args);

module.exports = isProd => {
  const sourceMap = !isProd;

  const exportsLoader = 'exports-loader?module.exports.toString()';
  const styleLoader = isProd ? MiniCssExtractPlugin.loader : 'style-loader';

  const cssLoader = {
    loader: 'css-loader',
    options: {
      sourceMap,
    },
  };

  const postcssLoader = [
    cssLoader,
    {
      loader: 'postcss-loader',
      options: {
        sourceMap,
      },
    },
  ];

  const sassLoader = [
    ...postcssLoader,
    {
      loader: 'sass-loader',
      options: {
        sourceMap,
        includePaths: [
          resolve('app2/shared/styles'),
          resolve('app2/core/styles'),
        ],
      },
    },
  ];

  // all the common webpack config
  const ConfigEntry = {
    landing: [
      '@babel/polyfill',
      resolve('landing/vendor.entry.ts'),
      resolve('landing/landing.entry.ts'),
    ],
    app2: [
      '@babel/polyfill',
      resolve('app2/vendor.entry.ts'),
      resolve('app2/app.entry.ts'),
    ],
    app_user: [
      '@babel/polyfill',
      resolve('app_user/vendor.entry.ts'),
      resolve('app_user/app.entry.ts'),
    ],
    terminal: [
      '@babel/polyfill',
      resolve('terminal/vendor.entry.ts'),
      resolve('terminal/terminal.entry.ts'),
    ],
  };
  const ConfigOutput = {
    path: resolve(),
  };
  const ConfigResolve = {
    alias: {
      requestAnimationFrame: 'requestanimationframe',
    },
    extensions: ['.ts', '.js'],
    modules: [resolve(), 'node_modules'],
  };

  const moduleRulesCommon = [
    /***************** Common static resources related bellow *****************/
    {
      test: /\.(eot|svg|ttf|woff2?)(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'url-loader',
      options: {
        limit: 10000,
        name: 'fonts/[name].[ext]',
      },
    },
    {
      test: /\.(jpe?g|png|gif)$/i,
      use: [
        {
          loader: 'image-loader',
          options: {
            bypassOnDebug: true,
            progressive: true,
            optimizationLevel: 3,
            pngquant: { quality: '65-80', speed: 4 },
          },
        },
        {
          loader: 'url-loader',
          options: {
            limit: 10,
            name: 'img/[name].[ext]',
          },
        },
      ],
    },
    {
      test: /\.html$/,
      loader: 'html-loader',
      options: {
        minimize: isProd,
        caseSensitive: true,
        removeAttributeQuotes: false,
      },
    },
  ];
  const moduleRulesAngularJS = [
    /***************** AngularJS related bellow *****************/
    {
      test: /\.css$/,
      include: [/node_modules/, resolve('css')],
      use: [styleLoader, ...postcssLoader],
    },
    {
      test: /\.scss$/,
      exclude: [
        /node_modules/,
        /static(\\|\/)landing(\\|\/)app/,
        /static(\\|\/)app2/,
        /static(\\|\/)app_user/,
        /static(\\|\/)terminal(\\|\/)app/,
      ],
      use: [styleLoader, ...sassLoader],
    },
    {
      test: /\.js$/,
      include: [resolve(), resolve('../node_modules/angular-ui-codemirror')],
      use: [
        'thread-loader',
        {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
            plugins: ['angularjs-annotate'],
          },
        },
      ],
    },
  ];
  const moduleRulesAngular = [
    /***************** Angular related bellow *****************/
    // 载入全局样式 (Angular) (几乎都在/static/landing/app/, /static/app2/ 中定义)
    {
      test: /\.global\.scss$/,
      use: [styleLoader, ...sassLoader],
    },
    // Angular 2 loaders:
    // We are still relying on Babel to translating ES6 to ES5
    {
      test: /\.ts$/,
      exclude: [/\.spec\.ts$/],
      use: [
        {
          loader: 'awesome-typescript-loader',
          options: {
            useBabel: true,
            useCache: true,
            babelCore: '@babel/core',
          },
        },
        'angular2-template-loader',
        {
          loader: 'angular-router-loader',
          options: {
            loader: 'import',
          },
        },
      ],
    },
    {
      test: /\.scss$/,
      exclude: [/\.global\.scss$/],
      include: [
        /static(\\|\/)landing(\\|\/)app/,
        /static(\\|\/)app2/,
        /static(\\|\/)app_user/,
        /static(\\|\/)terminal(\\|\/)app/,
      ],
      use: [exportsLoader, ...sassLoader],
    },
    {
      test: /\.css$/,
      include: [
        /static(\\|\/)landing(\\|\/)app/,
        /static(\\|\/)app2/,
        /static(\\|\/)app_user/,
        /static(\\|\/)terminal(\\|\/)app/,
      ],
      use: [exportsLoader, ...postcssLoader],
    },
  ];
  const ConfigModule = {
    noParse: [/highcharts/, /semantic-ui-.+\.min\.js$/],
    rules: [
      ...moduleRulesCommon,
      ...moduleRulesAngularJS,
      ...moduleRulesAngular,
    ],
  };

  const ConfigPlugin = [
    // https://github.com/AngularClass/angular-starter/issues/993
    new webpack.ContextReplacementPlugin(
      /(.+)?angular(\\|\/)core(.+)?/,
      resolve(),
    ),
    new webpack.DllReferencePlugin({
      manifest: vendorManifest,
    }),
    new webpack.ProvidePlugin({
      _: 'lodash',
    }),
    ...['app2', 'app_user', 'landing', 'terminal'].map(
      page =>
        new HtmlWebpackPlugin({
          template: resolve(`${page}/index.html`),
          filename: page + '.html',
          inject: 'body',
          cache: false,
          chunks: ['manifest', 'vendors', 'commons', page],
          alwaysWriteToDisk: true,
          showErrors: true,
        }),
    ),
    new AddAssetHtmlPlugin({
      filepath: resolve('assets/js/vendor.dll.*.js'),
      includeSourcemap: false,
    }),
    new CopyWebpackPlugin([
      {
        context: 'node_modules/monaco-editor/min/',
        from: '**/*',
        to: resolve('assets/monaco_lib'),
      },
    ]),
    new RubickI18nWebpackPlugin({
      entry: [
        resolve('app/**/*.i18n.json'),
        resolve('app_user/**/*.i18n.json'),
        resolve('app2/**/*.i18n.json'),
      ],
      outputDir: resolve('assets/i18n'),
      manifestBase: '/static/assets/i18n',
    }),
  ];

  return {
    entry: ConfigEntry,
    output: ConfigOutput,
    resolve: ConfigResolve,
    module: ConfigModule,
    plugins: ConfigPlugin,
    optimization: {
      runtimeChunk: {
        name: 'manifest',
      },
      splitChunks: {
        name: true,
        cacheGroups: {
          vendors: {
            name: 'vendors',
            chunks: 'initial',
            test: /node_modules/,
          },
          commons: {
            name: 'commons',
            chunks: 'all',
            minChunks: 3,
            priority: -10,
          },
        },
      },
    },
  };
};
