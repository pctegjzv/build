const path = require('path');

const webpack = require('webpack');

const resolve = (...args) => path.resolve('static', ...args);

const babelOptions = {
  babelrc: false,
  presets: ['@babel/env'],
  plugins: ['angularjs-annotate'],
};

const sassLoader = {
  loader: 'sass-loader',
  options: {
    includePaths: [resolve('app2/shared/styles')],
  },
};

module.exports = {
  mode: 'development',
  resolve: {
    extensions: ['.ts', '.js'],
    modules: [resolve(), 'node_modules'],
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: [/static\//],
        loader: 'babel-loader',
        options: {
          ...babelOptions,
          cacheDirectory: true,
        },
      },
      {
        test: /\.ts$/,
        use: [
          {
            loader: 'awesome-typescript-loader',
            options: {
              useBabel: true,
              babelOptions,
              useCache: true,
              babelCore: '@babel/core',
            },
          },
          'angular2-template-loader',
        ],
        include: [/static\//],
      },
      {
        test: /\.html$/,
        include: [/static\//],
        loader: 'html-loader',
        options: {
          caseSensitive: true,
          removeAttributeQuotes: false,
        },
      },
      {
        test: /\.scss$/,
        exclude: [/\.global\.scss$/],
        include: [/static\/landing\/app/, /static\/app2/, /static\/app_user/],
        use: [
          'exports-loader?module.exports.toString()',
          'css-loader',
          'postcss-loader',
          sassLoader,
        ],
      },
      {
        test: /\.global\.scss$/,
        use: ['style-loader', 'css-loader', 'postcss-loader', sassLoader],
      },
      {
        test: /\.css$/,
        include: [/node_modules/],
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(eot|svg|ttf|woff2?)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: 'fonts/[name].[ext]',
            },
          },
        ],
      },
      {
        test: /\.(jpe?g|png|gif)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10,
              name: 'img/[name].[ext]',
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new webpack.ContextReplacementPlugin(
      /(.+)?angular(\\|\/)core(.+)?/,
      resolve(),
    ),
  ],
  devtool: 'cheap-module-eval-source-map',
};
