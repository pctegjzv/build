const path = require('path');

const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');
const merge = require('webpack-merge');

const baseConfig = require('./webpack.config')();

module.exports = (env = {}) =>
  merge(baseConfig, {
    mode: 'development',
    devServer: {
      compress: true,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers':
          'X-Requested-With, content-type, Authorization',
      },
      index: '',
      port: 3000,
      watchOptions: {
        poll: 1000,
        ignored: [path.resolve('static/*.html'), 'node_modules'],
      },
      proxy: {
        '*': {
          target: env.TARGET || 'http://localhost:2333',
          secure: false,
        },
      },
    },
    output: {
      filename: 'assets/js/[name].[hash].js',
      chunkFilename: 'assets/js/[name].[hash].js',
      publicPath: 'http://localhost:3000/static/',
    },
    devtool: 'cheap-module-eval-source-map',
    plugins: [
      new HtmlWebpackHarddiskPlugin({
        outputPath: path.resolve('static'),
      }),
    ],
  });
