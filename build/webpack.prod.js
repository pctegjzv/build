const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const merge = require('webpack-merge');

const baseConfig = require('./webpack.config')(true);

module.exports = merge(baseConfig, {
  mode: 'production',
  output: {
    filename: 'assets/js/[name].[contenthash].js',
    chunkFilename: 'assets/js/[name].[contenthash].js',
    publicPath: '/static/',
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'assets/css/[name].[contenthash].css',
      chunkFilename: 'assets/css/[name].[contenthash].css',
    }),
  ],
});
