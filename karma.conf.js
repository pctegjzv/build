/* eslint-disable global-require */
module.exports = config => {
  const testBundlePath = 'static/app2/spec.bundle.js';
  const webpackConfig = require('./build/webpack.config.test');
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    files: ['node_modules/regenerator-runtime/runtime.js', testBundlePath],
    exclude: [],
    preprocessors: {
      [testBundlePath]: ['webpack', 'sourcemap'],
    },
    plugins: [
      require('karma-webpack'),
      require('karma-sourcemap-loader'),
      require('karma-chrome-launcher'),
      require('karma-remap-istanbul'),
      require('karma-jasmine'),
      require('karma-spec-reporter'),
      require('karma-jasmine-html-reporter'),
    ],
    webpack: webpackConfig,
    webpackMiddleware: {
      // webpack-dev-middleware configuration
      // i. e.
      stats: 'minimal',
      colors: true,
    },
    reporters: ['karma-remap-istanbul', 'spec', 'kjhtml'],
    remapIstanbulReporter: {
      reports: {
        html: 'coverage',
        lcovonly: './coverage/coverage.lcov',
      },
    },
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['Chrome'],
    singleRun: true,
    concurrency: Infinity,
  });
};
