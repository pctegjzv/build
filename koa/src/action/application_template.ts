/**
 * 特殊接口，上传和更新yaml文件时使用multipart/form-data
 */
import { Context } from 'koa';
import { get } from 'lodash';

import { JakiroRequest, Request } from '../common/request';
import { Dictionary } from '../types';

class ApplicationTemplate {
  constructor() {}

  handleTemplatePost = async (ctx: Context): Promise<any> => {
    let templateValue: any, templateFormData: any;
    if (ctx.href.indexOf('application-templates') >= 0) {
      templateValue = get(ctx, 'request.body.files.template');
      templateFormData = {
        description: get(ctx, 'request.body.description', ''),
        name: get(ctx, 'request.body.name', ''),
        space_name: get(ctx, 'request.body.space_name', ''),
        template: {
          value: templateValue[1],
          options: {
            filename: templateValue[0],
          },
        },
      };
    } else {
      templateValue = get(ctx, 'request.body.files.services');
      templateFormData = {
        region: get(ctx, 'request.body.region', ''),
        space_name: get(ctx, 'request.body.space_name', ''),
        app_name: get(ctx, 'request.body.app_name', ''),
        services: {
          value: templateValue[1],
          options: {
            filename: templateValue[0],
          },
        },
      };
    }
    const options = {
      ctx,
      method: ctx.method,
      path: ctx.path.replace('/fileview', ''),
      headers: {
        'content-type': 'multipart/form-data',
      },
      args: {
        query: ctx.query,
      },
      formData: templateFormData,
      proxy: true,
    };
    const rep = await JakiroRequest(options);
    ctx.body = rep.result;
    ctx.status = rep.code;
  };

  handleTemplatePut = async (ctx: Context): Promise<any> => {
    let templateValue: any, templateFormData: any;
    const body: Dictionary = ctx.request.body;
    if (ctx.href.indexOf('application-templates') >= 0) {
      templateValue = body.files.template[1];
      templateFormData = {
        description: body.description,
        template: {
          value: templateValue,
          options: {
            filename: 'compose.yaml',
          },
        },
      };
    } else {
      templateValue = body.files.services;
      templateFormData = {
        services: {
          value: templateValue,
          options: {
            filename: 'compose.yaml',
          },
        },
      };
    }
    const options = {
      ctx,
      method: ctx.method,
      path: ctx.path.replace('/fileview', ''),
      headers: {
        'content-type': 'multipart/form-data',
      },
      formData: templateFormData,
      args: {
        query: ctx.query,
      },
      proxy: true,
    };
    const rep = await JakiroRequest(options);
    ctx.body = rep.result;
    ctx.status = rep.code;
  };

  handleTemplateGet = async (ctx: Context): Promise<any> => {
    const options = {
      ctx,
      method: ctx.method,
      path: ctx.path.replace('/fileview', ''),
      args: {
        query: ctx.query,
      },
      proxy: true,
    };
    const res = await JakiroRequest(options);
    if (res.code >= 400) {
      ctx.body = res.result;
      ctx.status = res.code;
    } else {
      const options_file = {
        uri: res.result[ctx.header['read-files']],
        headers: {
          'User-Agent': 'rubick/v1.0',
        },
      };
      const file = await Request(options_file);
      if (file.statusCode >= 400) {
        ctx.body = file.body;
        ctx.status = file.statusCode;
      } else {
        res.result[ctx.header['files-type']] = file.body;
        ctx.body = res.result;
        ctx.status = res.code;
      }
    }
  };
}

const applicationTemplateService = new ApplicationTemplate();
export default applicationTemplateService;
