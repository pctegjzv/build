import * as fs from 'fs';

import { Context } from 'koa';

import { JakiroRequest } from '../common/request';
import rbLogger from '../logger/logger';

const logger = rbLogger.logger;
const DEFAULT_AVATARS_PATH = '/static/images/app-catalog/';
let LOCAL_AVATARS = [];

(() => {
  fs.readdir(process.cwd() + DEFAULT_AVATARS_PATH, (err, files) => {
    if (err) {
      return logger.error('[Koa error]: get avatars files err' + err.message);
    }
    LOCAL_AVATARS = files;
  });
})();

export async function catalogList(ctx: Context, next: Function): Promise<any> {
  const app_list = await JakiroRequest({
    ctx,
    method: 'GET',
    path: ctx.path,
    args: {
      query: ctx.query,
    },
  });
  if (app_list.result) {
    app_list.result.forEach(app => {
      if (LOCAL_AVATARS.indexOf(`${app.app_name}.png`) !== -1) {
        app['avatar'] = `${DEFAULT_AVATARS_PATH}${app.app_name}.png`;
      }
    });
  }
  ctx.body = app_list;
  ctx.status = 200;
}

export async function catalogForward(
  ctx: Context,
  next: Function,
): Promise<any> {
  const option = {
    ctx: ctx,
    method: ctx.method,
    path: ctx.path,
    args: {
      data: ctx.request.body,
      query: ctx.query,
    },
    proxy: true,
  };
  const res = await JakiroRequest(option);
  ctx.body = Array.isArray(res.result) ? { result: res.result } : res.result;
  ctx.status = res.code || 200;
  next();
}
