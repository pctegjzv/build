import { Context } from 'koa';
import * as _ from 'lodash';

import { JakiroRequest } from '../common/request';
import { extract_favicon_path_from_envvar } from '../common/util';
import weblabClient from '../common/weblab_client';
import config from '../config/config';

import environmentsService from './environments';

interface ConsoleType {
  featureView(ctx: any): Promise<any>;
}

class Console implements ConsoleType {
  constructor() {}

  async featureView(ctx: Context) {
    const feature_flags = await weblabClient.prefetch(
      _.get(ctx.query, 'namespace'),
    );
    ctx.body = {
      feature_flags: feature_flags,
    };
  }

  async profileView(ctx: Context) {
    const profile = await JakiroRequest({
      ctx,
      method: 'GET',
      path: `/auth/${_.get(ctx.query, 'namespace')}/profile/`,
    });
    ctx.body = profile;
  }

  async OrgMemberView(ctx: Context) {
    const result = await JakiroRequest({
      ctx,
      method: 'GET',
      path: `/orgs/${_.get(ctx.query, 'org_name') ||
        _.get(ctx.query, 'namespace')}/accounts/${_.get(
        ctx.query,
        'username',
      )}`,
    });
    ctx.body = result;
  }

  async weblabs(ctx: Context) {
    const feature_flags = await weblabClient.prefetch(
      _.get(ctx.query, 'namespace'),
    );
    ctx.body = {
      feature_flags: feature_flags,
    };
  }

  async token(ctx: Context) {
    ctx.body = {
      token: ctx.user.token,
    };
  }

  async consoleView(ctx: Context) {
    const favicon_path = extract_favicon_path_from_envvar();
    const namespace = _.get(ctx, 'user.namespace', '');
    const username = _.get(ctx, 'user.username');
    const data = namespace
      ? {
          is_org_namespace: 1,
          namespace,
          username,
          view_type: 'admin',
        }
      : {
          is_org_namespace: 0,
          username: '',
          namespace: username,
          view_type: 'admin',
        };
    data['favicon_path'] = favicon_path;
    // only for sso entry
    if (
      ctx.session.user &&
      ctx.session.user.ssoSource === config.sso().ssoSource
    ) {
      data['sso_source'] = config.sso().ssoSource;
      data['sso_configs'] = await environmentsService.getSsoConfigContent(ctx);
    } else {
      data['sso_source'] = '';
      data['sso_configs'] = '';
    }

    await ctx.render('app2.html', data);
  }

  async userView(ctx: Context) {
    const favicon_path = extract_favicon_path_from_envvar();
    const namespace = _.get(ctx, 'user.namespace', '');
    const username = _.get(ctx, 'user.username');
    const data = namespace
      ? {
          is_org_namespace: 1,
          namespace,
          username,
          view_type: 'user',
        }
      : {
          is_org_namespace: 0,
          username: '',
          namespace: username,
          view_type: 'user',
        };
    data['favicon_path'] = favicon_path;
    data['sso_source'] = config.sso().ssoSource;
    (data['sso_configs'] = await environmentsService.getSsoConfigContent(ctx)),
      await ctx.render('app_user.html', data);
  }

  async terminalView(ctx: Context) {
    const favicon_path = extract_favicon_path_from_envvar();
    await ctx.render('terminal.html', { favicon_path: favicon_path });
  }
}

const consoleService = new Console();
export default consoleService;
