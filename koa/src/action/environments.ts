import { Context } from 'koa';
import { mapValues } from 'lodash';

import { JakiroRequest } from '../common/request';
import { getEnv, getNameSpace, processStringType } from '../common/util';
import config from '../config/config';

import { OidcConfig } from './sso';

class Environments {
  constructor() {}

  private _handleEnv(options: any) {
    return mapValues(options, val => {
      return processStringType(val);
    });
  }

  getEnvironments = async (ctx: Context): Promise<any> => {
    const { code } = await JakiroRequest({
      ctx,
      path: `/license-auth/${getNameSpace(ctx)}/license-code`,
      method: 'GET',
      proxy: true,
    });
    const env = {
      debug: getEnv('DEBUG'),
      alauda_image_index: getEnv('DOCKER_INDEX'),
      lycan_url: getEnv('LYCAN_URL'),
      is_private_deploy_enabled: getEnv('PRIVATE_DEPLOY_ENABLED'),
      private_deploy_password_pattern_type: getEnv(
        'PRIVATE_DEPLOY_PASSWORD_TYPE',
      ),
      cluster_create_cloud_type: getEnv('CLUSTER_CREATE_CLOUD_TYPE'),
      third_party_login_type: getEnv('THIRD_PARTY_LOGIN_TYPE'),
      user_docs_url: getEnv('USER_DOCS_URL'),
      oss_server_url: getEnv('OSS_SERVER_URL'),
      remove_default_image_registry: getEnv('REMOVE_DEFAULT_IMAGE_REGISTRY'),
      predefined_login_organization: getEnv('PREDEFINED_LOGIN_ORGANIZATION'),
      excluded_nav_states: getEnv('EXCLUDED_NAV_STATES'),
      nav_states_ordering: getEnv('NAV_STATES_ORDERING'),
      overridden_logo_sources: getEnv('OVERRIDDEN_LOGO_SOURCES'),
      service_log_polling_maximum: getEnv('SERVICE_LOG_POLLING_MAXIMUM', 500),
      alarm_severity_level_enabled: getEnv('ALARM_SEVERITY_LEVEL_ENABLED'),
      license_required: /^2\.*/.test(code),
      vendor_customer: getEnv('VENDOR_CUSTOMER', 'PLATFORM'),
      sso_source: getEnv('SSO_SOURCE'),
      sso_guide_page_on: getEnv('SSO_GUIDE_PAGE_ON'),
      projects_v2_enabled: getEnv('PROJECTS_V2_ENABLED'),
      login_pattern: getEnv('LOGIN_PATTERN'),
      default_quota: getEnv('DEFAULT_QUOTA'),
      outsourcing_image_index: getEnv('OUTSOURCING_IMAGE_INDEX'),
      sock_server_url: getEnv('SOCK_SERVER_URL'),
      cmb_nodeport_domain: getEnv('CMB_NODEPORT_DOMAIN'),
      cmb_image_upload_url: getEnv('CMB_IMAGE_UPLOAD_URL'),
      notification_duration: getEnv('NOTIFICATION_DURATION', 6000),
      notification_max_stack: getEnv('NOTIFICATION_MAX_STACK', 8),
      label_base_domain: getEnv('LABEL_BASE_DOMAIN', 'alauda.io'),
      auto_logout_latency: getEnv('AUTO_LOGOUT_LATENCY', 720),
    };
    //sso source config
    if (env.sso_source) {
      env['sso_configuration'] = await this.getSsoConfiguration(ctx);
    }
    const options = this._handleEnv(env);
    ctx.body = options;
  };

  getSsoConfiguration = async (
    ctx: Context,
  ): Promise<{ configs: OidcConfig[]; default_org_name: string }> => {
    const source = getEnv('SSO_SOURCE');
    const tpName = config.oidcSources.includes(source) ? 'oidc' : source;
    return JakiroRequest({
      ctx,
      method: 'GET',
      path: `/v1/tp_sso/${tpName}/configuration`,
    });
  };

  getSsoConfigContent = async (ctx: Context): Promise<string> => {
    const configuration = await this.getSsoConfiguration(ctx);
    return JSON.stringify(configuration.configs);
  };
}

const environmentsService = new Environments();
export default environmentsService;
