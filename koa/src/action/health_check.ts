import * as fs from 'fs';
import { Context } from 'koa';
import * as rp from 'request-promise';

import { getEnv } from '../common/util';
import rbLogger from '../logger/logger';

class HealthCheck {
  GIT_ROOT = '.git/';

  private async checkJakiro() {
    const info = {
      name: 'Jakiro',
      endpoint:
        (getEnv('API_SERVER_URL') || getEnv('JAKIRO_PORT_80_HTTP')) + '/',
    };
    const start = Date.now();
    const options = {
      uri: info.endpoint + '_ping',
      timeout: 2000,
      json: true,
    };
    await rp(options)
      .then(rep => {
        info['status'] = 'OK';
        info['message'] = rep;
        info['latency'] = Date.now() - start + 'ms';
      })
      .catch(err => {
        info['status'] = 'ERROR';
        info['status_code'] = err.statusCode ? err.statusCode : null;
        info['message'] = 'Jakiro down. All API requests should fail.';
        info['suggestion'] = 'Check envfile for API endpoint and its status.';
      });
    return info;
  }

  private async checkWeblabs() {
    const info = {
      name: 'Lightkeeper (Weblabs)',
      endpoint: getEnv('LIGHTKEEPER_ENDPOINT'),
    };
    const start = Date.now();
    const options = {
      uri: info.endpoint.replace(/v1/, '_ping'),
      timeout: 2000,
      json: true,
    };
    await rp(options)
      .then(rep => {
        info['status'] = 'OK';
        info['message'] = rep;
        info['latency'] = Date.now() - start + 'ms';
      })
      .catch(err => {
        info['status'] = 'ERROR';
        info['status_code'] = err.statusCode ? err.statusCode : null;
        info['message'] =
          'Connection to Lightkeeper failed. Weblab related features will fail.';
        info['suggestion'] = 'Check status of lightkeeper server / endpoint';
      });
    return info;
  }

  private async checkOSS() {
    const info = {
      name: 'Wukong (OSS)',
      endpoint: getEnv('OSS_SERVER_URL'),
    };
    const start = Date.now();
    const options = {
      uri: info.endpoint + '/ping',
      timeout: 2000,
      json: true,
    };
    await rp(options)
      .then(rep => {
        info['status'] = 'OK';
        info['message'] = rep;
        info['latency'] = Date.now() - start + 'ms';
      })
      .catch(err => {
        info['status'] = 'ERROR';
        info['status_code'] = err.statusCode ? err.statusCode : null;
        info['message'] =
          'Connection to OSS failed. Artifact related features will not be available.';
        info['suggestion'] = 'Check status of OSS server / endpoint';
      });
    return info;
  }

  /**
   * Get the current HEAD commit ID if any.
   */
  private async getGitCommitId() {
    const fileExists = await new Promise(res => {
      fs.exists(this.GIT_ROOT, data => res(data));
    });
    if (fileExists) {
      const readfile = (filepath: string) =>
        new Promise<string>((resolve, reject) => {
          fs.readFile(filepath, 'utf8', (err, data) => {
            if (err) {
              reject(err);
            } else {
              resolve(data);
            }
          });
        });
      try {
        // .git/HEAD contains the current HEAD branch
        // .git/ref contains files named by the local branches and each of them contains the commit ID.
        const head = await readfile(this.GIT_ROOT + 'HEAD');
        const commitId = await readfile(
          this.GIT_ROOT + head.replace('ref: ', '').trim(),
        );
        if (commitId) {
          return commitId.trim();
        } else {
          return 'N/A';
        }
      } catch (err) {
        rbLogger.logger.warn(
          '[KOA Diagnose] Failed to read git commit ID: ' + err.message,
        );
      }
    }
    return 'N/A';
  }

  async ping(ctx: Context, next: Function): Promise<any> {
    ctx.body = 'Rubick: Your magic is mine!';
    ctx.status = 200;
    next();
  }

  diagnose = async (ctx: Context, next: Function): Promise<any> => {
    const check_list = await Promise.all([
      this.checkJakiro(),
      this.checkWeblabs(),
      this.checkOSS(),
    ]);
    const failed_checks = [];
    for (const check of check_list) {
      if (check['status'] === 'ERROR') {
        failed_checks.push(check['name']);
      }
    }

    const info = {
      version: await this.getGitCommitId(),
      status: failed_checks.length === 0 ? 'OK' : 'ERROR',
      failed_services: failed_checks,
      details: check_list,
    };

    ctx.body = info;
    ctx.status = 200;
    next();
  };
}

const healthCheckAction = new HealthCheck();
export default healthCheckAction;
