import { Context } from 'koa';
import * as _ from 'lodash';

import { JakiroRequest, Request } from '../common/request';
import { getEnv } from '../common/util';
import { GALLERY_IMAGES, IO_GALLERY_IMAGES } from '../config/gallery_images';
import rbLogger from '../logger/logger';

const logger = rbLogger.logger;

const STATUS_OK = 200,
  STATUS_UNAUTHORIZED = 401,
  DOCKER_API_VERSION_ATTR = 'docker-distribution-api-version';

class PublicRepository {
  constructor() {}

  private async apiV2Check(index) {
    const protocols = ['https', 'http'];
    let index_resp = null,
      protocol = null,
      auth = null;
    for (const p of protocols) {
      const url = `${p}://${index}/v2/`;
      try {
        index_resp = await Request({
          method: 'GET',
          url: url,
        });
        protocol = p;
      } catch (e) {
        logger.error(`api version check meet error ${e.message}`);
      }
    }
    if (index_resp && index_resp.statusCode === STATUS_UNAUTHORIZED) {
      auth = index_resp.headers['www-authenticate'];
    }
    return { auth, protocol };
  }

  private parseAuth(auth_info) {
    const regex = new RegExp(
      's*Bearer realm="(.*?)"s*,s*service="(.*?)",s*scope="(.*?)"',
    );
    const valueArr = auth_info.match(regex);
    valueArr.shift();
    return valueArr;
  }

  private async getRepoTagList(
    index,
    repo_path,
    protocol = 'http',
  ): Promise<any> {
    try {
      let tags;
      const tags_url = `${protocol}://${index}/v2/${repo_path}/tags/list`;
      const index_resp = await Request({
        url: tags_url,
        method: 'GET',
        json: true,
      });
      const docker_api_version = index_resp.headers[DOCKER_API_VERSION_ATTR];
      if ('registry/2.0' !== docker_api_version) {
        logger.error(
          `${repo_path} fetch v2 tags meet error: 'Docker-Distribution-API-Version' - ${docker_api_version}`,
        );
        return {};
      } else if (index_resp.statusCode === STATUS_OK) {
        tags = index_resp.body.tags || [];
      } else if (index_resp.statusCode === STATUS_UNAUTHORIZED) {
        const auth_info = index_resp.headers['www-authenticate'];
        const [realm, service, scope] = this.parseAuth(auth_info);
        const tokenUrl = `${realm}?service=${service}&scope=${scope}`;
        const tokenResp = await Request({
          url: tokenUrl,
          method: 'GET',
          json: true,
        });
        const token = tokenResp.body['token'];
        const headers = { Authorization: `Bearer ${token}` };
        const resp = await Request({
          url: tags_url,
          headers,
          method: 'GET',
          json: true,
        });
        if (resp.statusCode === 200) {
          tags = resp.body['tags'] || [];
        }
      }
      return tags;
    } catch (e) {
      throw {};
    }
  }

  private async getImageExist(index, repo_path): Promise<boolean> {
    const checkRes = await this.apiV2Check(index);
    try {
      const tags = await this.getRepoTagList(
        index,
        repo_path,
        checkRes.protocol,
      );
      return !!tags.length;
    } catch (e) {
      throw {};
    }
  }

  PublicRepositoryExist = async (
    ctx: Context,
    next: Function,
  ): Promise<any> => {
    const { repo_path, index } = ctx.query;
    const resultSet = {};
    try {
      if (await this.getImageExist(index, repo_path)) {
        ctx.body = 'Image repo exist';
        ctx.status = 200;
      } else {
        resultSet['errors'] = [
          {
            code: 'repo_not_exist',
            source: '1019',
            message: 'Image repo not exist',
          },
        ];
        ctx.body = resultSet;
        ctx.status = 404;
      }
      next();
    } catch (e) {
      resultSet['errors'] = [
        {
          code: 'repo_not_exist',
          source: '1019',
          message: 'Image repo not exist',
        },
      ];
      ctx.body = resultSet;
      ctx.status = 500;
      next();
    }
  };

  publicRepositoryTagList = async (
    ctx: Context,
    next: Function,
  ): Promise<any> => {
    const { repo_path, index } = ctx.query;
    const resultSet = {};
    let errors = [];
    if (repo_path) {
      const checkRes = await this.apiV2Check(index);
      try {
        const tags = await this.getRepoTagList(
          index,
          repo_path,
          checkRes.protocol,
        );
        resultSet['tags'] = tags.map(t => ({
          tag: t,
          image_id: t,
        }));
      } catch (e) {
        errors = [
          {
            code: 'tag_list_third_party_api_exception',
            source: '1019',
            message: '',
          },
        ];
      }
      if (errors.length) {
        ctx.body = { errors };
        ctx.status = 500;
        next();
      }
    } else {
      ctx.body = { error: 'repo repo_path not given' };
      ctx.body = 500;
      next();
    }
    resultSet['result'] = true;
    ctx.body = resultSet;
    ctx.status = 200;
    next();
  };

  private async getImageLayer(index, repo_path, tag_name, auth, p2) {
    const THIRD_PARTY_EXCEPTION = {
      errors: [
        {
          code: 'third_party_api_exception',
          source: '1019',
          message: 'Third party api connection error',
        },
      ],
    };
    const manifestUrl = `${p2}://${index}/v2/${repo_path}/manifests/${tag_name}`;
    let res = await Request({ url: manifestUrl, method: 'GET' });
    if (auth) {
      try {
        const auth_info = res.headers['www-authenticate'];
        const [realm, service, scope] = this.parseAuth(auth_info);
        const tokenUrl = `${realm}?service=${service}&scope=${scope}`;
        const tokenResp = await Request({
          url: tokenUrl,
          method: 'GET',
          json: true,
        });
        const token = tokenResp.body['token'];
        const headers = { Authorization: `Bearer ${token}` };
        res = await Request({
          url: manifestUrl,
          headers,
          method: 'GET',
          json: true,
        });
        logger.info('[get_tag_details] Finish get v2 manifest');
      } catch (e) {
        throw THIRD_PARTY_EXCEPTION;
      }
    }
    if (res && res.statusCode !== STATUS_OK) {
      throw THIRD_PARTY_EXCEPTION;
    }
    let manifest;
    if (typeof res.body === 'string') {
      manifest = JSON.parse(res.body);
    } else {
      manifest = res.body;
    }
    if (!manifest['history']) {
      throw THIRD_PARTY_EXCEPTION;
    } else {
      return JSON.parse(manifest['history'][0]['v1Compatibility']);
    }
  }

  private async getThirdPartyTagDetail(index, repo_path, tag_name) {
    const checkResult = await this.apiV2Check(index);
    try {
      return await this.getImageLayer(
        index,
        repo_path,
        tag_name,
        checkResult.auth,
        checkResult.protocol,
      );
    } catch (e) {
      throw e;
    }
  }

  private async getAlaudaTagDetail() {}

  publicRepositoryTag = async (ctx: Context, next: Function): Promise<any> => {
    const { index, repo_path, namespace, tag } = ctx.query;
    const arr = repo_path.split('/');
    let errors, result_tag;
    if ((index && index !== getEnv('DOCKER_INDEX')) || arr[0] !== namespace) {
      try {
        result_tag = await this.getThirdPartyTagDetail(index, repo_path, tag);
      } catch (e) {
        errors = [
          {
            code: 'image_layer_third_party_api_exception',
            source: '1019',
            message: '',
          },
        ];
        ctx.body = { errors };
        ctx.status = 500;
        next();
      }
    } else {
      try {
        result_tag = await this.getAlaudaTagDetail();
      } catch (e) {
        errors = [
          {
            code: 'image_layer_alauda_api_exception',
            source: '1019',
            message: '',
          },
        ];
        ctx.body = { errors };
        ctx.status = 500;
        next();
      }
    }
    ctx.body = { tag: result_tag };
    ctx.status = STATUS_OK;
    next();
  };

  PublicRepositoryGallery = async (
    ctx: Context,
    next: Function,
  ): Promise<any> => {
    const { gallery_type } = ctx.query;
    let gallery;
    if (gallery_type) {
      if (getEnv('LOCALE') === 'cn') {
        const repo_paths = _.get(GALLERY_IMAGES, gallery_type, []);
        gallery = await JakiroRequest({
          ctx,
          method: 'POST',
          path: `/gallery-repositories`,
          args: {
            data: { repo_list: repo_paths },
          },
        });
      } else {
        gallery = _.get(IO_GALLERY_IMAGES, gallery_type, []);
      }
    }
    ctx.body = {
      [gallery_type]: gallery,
    };
    ctx.status = 200;
    next();
  };
}

const PublicRepositoryService = new PublicRepository();
export default PublicRepositoryService;
