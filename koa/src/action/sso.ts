import { Context } from 'koa';

import { getDefaultAppViewUrl } from '../common/loginUtil';
import { JakiroRequest } from '../common/request';
import {
  extract_favicon_path_from_envvar,
  getCommonHeaders,
} from '../common/util';
import config from '../config/config';

import environmentsService from './environments';

export interface OidcConfig {
  issuer: string;
  client_id: string;
  client_secret: string;
  display_name: string;
  redirect_uri: string;
  authorization_endpoint: string;
  token_endpoint: string;
  userinfo_endpoint: string;
  scopes_supported: string[];
  response_types_supported: string[];
  claims_supported: string[];
}

class Sso {
  async ssoLandingView(ctx: Context, next: Function) {
    //oidc alias
    const { ticket, source, code } = ctx.query;
    const voucher = ticket || code;
    //tsf use tsf alias, cmb use oidc alias
    const tpName = config.oidcSources.includes(source) ? 'oidc' : source;
    const data = config.oidcSources.includes(source)
      ? {
          code: voucher,
          grant_type: 'authorization_code',
        }
      : { ticket: voucher };

    const result = await JakiroRequest({
      ctx,
      method: 'POST',
      path: `/v1/tp_sso/${tpName}/login`,
      args: {
        data,
      },
      headers: getCommonHeaders(ctx),
    });

    const favicon_path = extract_favicon_path_from_envvar();

    const { is_valid, alauda_user_info: user, tp_logout_url } = result;
    if (is_valid && user.token) {
      ctx.session.user = user;

      ctx.user = user;
      ctx.session.user.ssoSource = source;
      //for oidc logout request
      if (tp_logout_url) {
        ctx.session.tp_logout_url = tp_logout_url;
      }
      const defaultRedirectUrl = await getDefaultAppViewUrl(user.namespace);
      ctx.redirect(defaultRedirectUrl);
      next();
    } else {
      const data = {
        favicon_path: favicon_path,
        sso_source: source,
        sso_configs: await environmentsService.getSsoConfigContent(ctx),
      };
      await ctx.render('sso.html', data);
    }
    next();
  }
}
const ssoService = new Sso();

export default ssoService;
