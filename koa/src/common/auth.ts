import { Context } from 'koa';

import { getEnv, getNameSpace } from '../common/util';
import config from '../config/config';

import { JakiroRequest } from './request';

export interface User {
  username: string;
  namespace: string;
  token: string;
}

export const DJANGO_SESSION_ID = '7940c0775f';

const LOGIN_URL = '/landing';
const LICENSE_ACTIVATION_URL = '/landing/license-activation';

function _redirectToLogin(ctx) {
  let loginUrl = LOGIN_URL;
  if (!_isLoginUrl(ctx.request.href)) {
    loginUrl += `?next=${ctx.path}`;
  }
  ctx.redirect(loginUrl);
}

function _redirectToLicenseActivation(ctx) {
  const licenseAcUrl = LICENSE_ACTIVATION_URL;
  ctx.redirect(licenseAcUrl);
}

function _isLoginUrl(url = '') {
  return !!url.includes(LOGIN_URL);
}

function login_required(ctx, next) {
  const is_ajax = ctx.request.get('X-Requested-With') === 'XMLHttpRequest';

  if (ctx.skipLogin) {
    return next();
  }
  if (is_ajax) {
    if (!ctx.user || !ctx.user.token) {
      ctx.throw(401);
    } else {
      return next();
    }
  } else {
    if (!ctx.user || !ctx.user.token) {
      _redirectToLogin(ctx);
    }
  }
  return next();
}

async function license_required(ctx, next) {
  const is_ajax = ctx.request.get('X-Requested-With') === 'XMLHttpRequest';

  if (ctx.user && ctx.user.token && getEnv('PRIVATE_DEPLOY_ENABLED')) {
    const { result, code } = await JakiroRequest({
      ctx,
      path: `/license-auth/${getNameSpace(ctx)}/license-code`,
      method: 'GET',
      proxy: true,
    });
    //code !== 2xx ,the plateform do not need license
    if (!/^2\.*/.test(code)) {
      return next();
    }

    ctx.session.licenseActived = result.days_until_expiration > 0;
    if (
      !ctx.session.licenseActived &&
      !/\/landing\/license\-activation/.test(ctx.path) &&
      !is_ajax
    ) {
      _redirectToLicenseActivation(ctx);
    }
  }
  return next();
}

//only for sso login, tsf is tiket query,cmb is code query
function sso_filter(ctx: Context, next: Function) {
  const { source, ticket, code } = ctx.query;
  const voucher = ticket || code;
  if (source === config.sso().ssoSource && voucher) {
    const sso_landing_url = `/sso?source=${source}&ticket=${voucher}`;
    ctx.redirect(sso_landing_url);
    return;
  } else if (
    ctx.session.user &&
    ctx.session.user.token &&
    ctx.session.user.ssoSource === 'tsf'
  ) {
    //tsf need skip login page
    ctx.skipLogin = true;
  }
  return next();
}

async function authentication(ctx, next) {
  ctx.user = ctx.session.user;
  return next();
}

export { authentication, login_required, license_required, sso_filter };
