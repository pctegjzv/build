import rbLogger from '../logger/logger';

import weblabClient from './weblab_client';
const logger = rbLogger.logger;

export async function getDefaultAppViewUrl(
  userNamespace,
  nextUrl = '/console',
) {
  logger.debug('login weblabs: [namespace]', JSON.stringify(userNamespace));
  if (userNamespace) {
    const weblabs = await weblabClient.prefetch(userNamespace);
    logger.debug('login weblabs:', JSON.stringify(weblabs));
    if (weblabs && weblabs['USER_VIEW_ENABLED']) {
      nextUrl = '/user';
    }
  }

  return nextUrl;
}
