import { Context } from 'koa';
import * as _ from 'lodash';
import * as moment from 'moment';
import * as request from 'request-promise';

import { getEnv } from '../common/util';
import rbLogger from '../logger/logger';

const logger = rbLogger.logger;

interface JakiroPayload {
  data?: object;
  query?: object;
}

export interface JakiroRequestOption {
  ctx: Context;
  method: string;
  path: string;
  args?: JakiroPayload;
  proxy?: boolean;
  headers?: object;
  formData?: object;
}

function getCurrentUTCTimeString() {
  return (
    moment(new Date())
      .utc()
      .format('YYYY-MM-DD HH:mm:ss SSS') + '(UTC)'
  );
}

export function Request(option) {
  option.simple = false;
  option.resolveWithFullResponse = true;
  const startTime = getCurrentUTCTimeString();
  return request(option)
    .then(function(rep) {
      const endTime = getCurrentUTCTimeString();
      logger.debug(
        `[Koa Request: Response]: <RequestTime> ${startTime} <URL> ${
          option.method
        } ${option.url} ` +
          `<Headers> ${JSON.stringify(option.headers)} <Body> ${JSON.stringify(
            option.body,
          )}  <Query> ${JSON.stringify(option.qs)} ` +
          `<ResponseTime> ${endTime} <ResponseHeader> ${JSON.stringify(
            rep.headers,
          )} <ResponseBody> ${JSON.stringify(rep.body)}`,
      );
      if (
        rep.headers['content-type'] === 'application/json' &&
        rep.body &&
        typeof rep.body === 'string'
      ) {
        try {
          JSON.parse(rep.body);
        } catch (e) {
          throw e;
        }
      }
      return rep;
    })
    .catch(function(err) {
      const endTime = getCurrentUTCTimeString();
      logger.error(
        `[Koa Request Error]: <RequestTime> ${startTime} ${option.method} ${
          option.url
        } <Headers>: ${JSON.stringify(option.headers)}  ` +
          `<Body> ${JSON.stringify(option.body)} <Query> ${JSON.stringify(
            option.qs,
          )}  <ResponseTime> ${endTime} <Error>: ${err.message}`,
      );
      throw err;
    });
}

export async function JakiroRequest(
  Joption: JakiroRequestOption,
): Promise<any> {
  const path = Joption.path.replace('/ajax-sp', '').replace('/ajax', '');
  const requestUrl =
    getEnv('API_SERVER_URL') +
    (path.substring(0, 3).indexOf('v1') !== -1 ||
    path.substring(0, 5).indexOf('v2') !== -1
      ? path
      : '/v1' + path);
  const noJson = _.get(Joption.ctx, 'headers.no-json') === 'true';
  const option = {
    url: requestUrl,
    json: !noJson,
    encoding: noJson ? null : undefined,
    headers: {
      'User-Agent': 'rubick/v1.0',
    },
    method: Joption.method,
    qs: _.get(Joption.args, 'query'),
    useQuerystring: true,
  };
  logger.debug('Jakiro request url: ', option.url, option.qs);
  if (_.get(Joption.ctx, 'user.token')) {
    option.headers['Authorization'] = `Token ${_.get(
      Joption.ctx,
      'user.token',
    )}`;
  }
  if (_.get(Joption.args, 'data') && Joption.method !== 'GET') {
    option['body'] = Joption.args.data;
  }
  if (_.get(Joption, 'formData')) {
    option['formData'] = Joption.formData;
  }
  if (Joption.headers) {
    option.headers = Object.assign({}, option.headers, Joption.headers);
  }
  if (_.get(Joption.ctx, 'headers.alauda-request-id')) {
    option.headers['Alauda-Request-ID'] = _.get(
      Joption.ctx,
      'headers.alauda-request-id',
    );
  }
  let rep: any;
  try {
    rep = await Request(option);
  } catch (err) {
    rep = {
      statusCode: 502,
      error: err.message,
    };
  }

  const { body } = rep;

  let result: object = body || {};

  if (!Joption.proxy) {
    return result;
  }

  const code: number = rep.statusCode;

  if (code >= 400) {
    const errors = body && body.errors;
    if (errors) {
      logger.debug('[Koa request] Json parse error: ' + JSON.stringify(errors));
      result = { errors };
    } else {
      result = {
        code: 'malformed_jakiro_response',
        source: '1019',
        message: rep.error || rep.text,
      };
    }
  }

  return { result, code };
}
