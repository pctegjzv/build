import * as crypto from 'crypto';
import * as moment from 'moment';

import config from '../config/config';

import { Request } from './request';

const ccp_config = config.ccp;

const BASE_URL = 'https://app.cloopen.com:8883';
export function send(to: string, code: string) {
  const data = {
    to: to,
    appId: ccp_config.app_id,
    templateId: ccp_config.template_id,
    datas: [code, '60'],
  };
  const option = {
    url: getUrl(),
    json: true,
    body: data,
    method: 'POST',
    headers: getHeader(),
  };
  return Request(option);
}

function getHeader() {
  const authString = `${ccp_config.account_sid}:${moment().format(
    'YYYYMMDDHHmmss',
  )}`;
  return {
    Accept: 'application/json',
    'Content-Type': 'application/json;charset=utf-8',
    Authorization: new Buffer(authString).toString('base64'),
  };
}

function getSignature() {
  const md5 = crypto.createHash('md5');
  return md5
    .update(
      `${ccp_config.account_sid}${ccp_config.account_token}${moment().format(
        'YYYYMMDDHHmmss',
      )}`,
    )
    .digest('hex');
}

function getUrl() {
  return `${BASE_URL}/2013-12-26/Accounts/${
    ccp_config.account_sid
  }/SMS/TemplateSMS?sig=${getSignature().toUpperCase()}`;
}
