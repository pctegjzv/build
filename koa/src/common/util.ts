import * as crypto from 'crypto';
import { Context } from 'koa';
import { get, trim } from 'lodash';

import { Dictionary } from '../types';

export function getEncAse192(str, secret = 'rubick') {
  const cipher = crypto.createCipher('aes192', secret); // 设置加密类型 和 要使用的加密密钥
  let enc = cipher.update(str, 'utf8', 'hex'); // 编码方式从utf-8转为hex;
  enc += cipher.final('hex'); // 编码方式从转为hex;
  return enc; // 返回加密后的字符串
}

export function getDecAse192(str, secret = 'rubick') {
  const decipher = crypto.createDecipher('aes192', secret);
  let dec = decipher.update(str, 'hex', 'utf8'); // 编码方式从hex转为utf-8;
  dec += decipher.final('utf8'); // 编码方式从utf-8;
  return dec;
}

export function processStringType(s: any) {
  let result = s;
  if (typeof result === 'string') {
    if (result === '') {
      result = null;
    } else if (result.toLowerCase() === 'true') {
      result = true;
    } else if (result.toLowerCase() === 'false') {
      result = false;
    } else if (parseInt(result, 10) + '' === result) {
      result = parseInt(result, 10);
    }
  }
  return result;
}

export function getEnv(name: string, defaultValue: any = '') {
  return processStringType(
    trim(get(process.env, name.toUpperCase(), defaultValue)),
  );
}

export function extract_favicon_path_from_envvar() {
  // default
  let favicon_path = '/static/images/logo/site-ico.png';
  const path = process.env.OVERRIDDEN_LOGO_SOURCES;
  if (path) {
    const temp = path.split(',');
    temp.forEach(r => {
      if (r.startsWith('favicon')) {
        favicon_path = r.split(':')[1];
      }
    });
  }
  return favicon_path;
}

export function extract_background_custom_envvar() {
  return process.env.LANDING_BG_URL || '';
}

export function getNameSpace(ctx: Context) {
  return get(ctx, 'user.namespace') || get(ctx, 'user.username');
}

export function getCommonHeaders(ctx: Context) {
  const headers: Dictionary = {
    'User-Agent': 'rubick/v1.0',
  };

  const userAgent = ctx.get('User-Agent');

  if (userAgent) {
    const originIp =
      ctx.get('X-Forwarded-For') || ctx.get('X-Real-IP') || ctx.ip;
    Object.assign(headers, {
      'rubick-client-agent': userAgent,
      'remote-address': originIp.split(',')[0],
    });
  }

  return headers;
}
