/* tslint:disable:max-line-length */
const IO_GALLERY_IMAGES = {
  'SSH Servers': [
    {
      logo_file: '/static/images/user/tutum.png',
      repo_path: 'tutum/ubuntu',
      description:
        'Ubuntu image with SSH access. For the root password, either set the ROOT_PASS environment variable or check the logs for a randomly generated one',
      link: 'https://registry.hub.docker.com/u/tutum/ubuntu/',
    },
    {
      logo_file: '/static/images/user/tutum.png',
      repo_path: 'tutum/centos',
      description:
        'Centos image with SSH access. For the root password, either set the ROOT_PASS environment variable or check the logs for a randomly generated one',
      link: 'https://registry.hub.docker.com/u/tutum/centos/',
    },
    {
      logo_file: '/static/images/user/tutum.png',
      repo_path: 'tutum/debian',
      description:
        'Debian image with SSH access. For the root password, either set the ROOT_PASS environment variable or check the logs for a randomly generated one',
      link: 'https://registry.hub.docker.com/u/tutum/debian/',
    },
    {
      logo_file: '/static/images/user/tutum.png',
      repo_path: 'tutum/fedora',
      description:
        'Fedora image with SSH access. For the root password, either set the ROOT_PASS environment variable or check the logs for a randomly generated one',
      link: 'https://registry.hub.docker.com/u/tutum/fedora/',
    },
  ],
  'Web Servers': [
    {
      logo_file: '/static/images/user/library.png',
      repo_path: 'nginx',
      description: 'Official build of Nginx',
      link: 'https://registry.hub.docker.com/_/nginx/',
    },
    {
      logo_file: '/static/images/user/library.png',
      repo_path: 'httpd',
      description: 'The Apache HTTP Server Project',
      link: 'https://registry.hub.docker.com/_/httpd/',
    },
    {
      logo_file: '/static/images/user/tutum.png',
      repo_path: 'tutum/tomcat',
      description:
        'Tomcat image - listens in port 8080. For the admin account password, either set TOMCAT_PASS environment variable, or check the logs for a randomly generated one',
      link: 'https://registry.hub.docker.com/u/tutum/tomcat/',
    },
    {
      logo_file: '/static/images/user/tutum.png',
      repo_path: 'tutum/lamp',
      description:
        'LAMP image - Apache listens in port 80, and MySQL in port 3306. For the MySQL admin account password, either set MYSQL_PASS env var or read the logs for a random generated one',
      link: 'https://registry.hub.docker.com/u/tutum/lamp/',
    },
    {
      logo_file: '/static/images/user/tutum.png',
      repo_path: 'tutum/glassfish',
      description:
        'Glassfish image - listens in port 4848, 8080. For the admin account password, either set GLASSFISH_PASS environment variable, or check the logs for a randomly generated one',
      link: 'https://registry.hub.docker.com/u/tutum/glassfish/',
    },
  ],
  Databases: [
    {
      logo_file: '/static/images/user/tutum.png',
      repo_path: 'tutum/mysql',
      description:
        'MySQL Server image - listens in port 3306. For the admin account password, either set MYSQL_PASS environment variable, or check the logs for a randomly generated one',
      link: 'https://registry.hub.docker.com/u/tutum/mysql/',
    },
    {
      logo_file: '/static/images/user/tutum.png',
      repo_path: 'tutum/mongodb',
      description:
        'MongoDB Docker image - listens in port 27017. For the admin password, either set MONGODB_PASS environment variable or check the logs for a randomly generated one',
      link: 'https://registry.hub.docker.com/u/tutum/mongodb/',
    },
    {
      logo_file: '/static/images/user/tutum.png',
      repo_path: 'tutum/postgresql',
      description:
        'PostgreSQL Docker Image - listens on port 5432. For the admin (postgres) password, either set POSTGRES_PASS environment variable or read the logs for a randomly generated one',
      link: 'https://registry.hub.docker.com/u/tutum/postgresql/',
    },
  ],
  'Cache Servers': [
    {
      logo_file: '/static/images/user/tutum.png',
      repo_path: 'tutum/memcached',
      description:
        'Memcached Docker image image - listens in port 11211. For the admin password, either set MEMCACHED_PASS environment variable or read the logs for a randomly generated one',
      link: 'https://registry.hub.docker.com/u/tutum/memcached/',
    },
    {
      logo_file: '/static/images/user/tutum.png',
      repo_path: 'tutum/redis',
      description:
        'Redis Docker image image - listens in port 6379. For the server password, either set REDIS_PASS environment variable or read the logs for a randomly generated one',
      link: 'https://registry.hub.docker.com/u/tutum/redis/',
    },
  ],
  Others: [
    {
      logo_file: '/static/images/user/default-icon.jpg',
      repo_path: 'alauda/hello-world',
      description: 'This is a hello world image from index.alauda.io',
      link: 'https://registry.hub.docker.com/u/alauda/hello-world/',
    },
    {
      logo_file: '/static/images/user/tutum.png',
      repo_path: 'tutum/wordpress',
      description:
        'Wordpress Docker image - listens in port 80. Includes bundled MySQL server',
      link: '',
    },
    {
      logo_file: '/static/images/user/tutum.png',
      repo_path: 'tutum/rabbitmq',
      description:
        'RabbitMQ Docker image - listens in ports 5672/15672 (admin). For the admin password, either set RABBITMQ_PASS environment variable or read the logs for a randomly generated one',
      link: 'https://registry.hub.docker.com/u/tutum/rabbitmq/',
    },
  ],
};
const GALLERY_IMAGES = {
  'SSH Servers': [
    'tutum/ubuntu',
    'tutum/centos',
    'tutum/debian',
    'tutum/fedora',
  ],
  'Web Servers': [
    'tutum/tomcat',
    'tutum/lamp',
    'library/nginx',
    'library/httpd',
    'tutum/glassfish',
  ],
  Databases: ['tutum/mysql', 'tutum/mongodb', 'tutum/postgresql'],
  'Cache Servers': ['tutum/memcached', 'tutum/redis'],
  Others: [
    'alauda/hello-world',
    'alauda/sftp',
    'tutum/wordpress',
    'tutum/rabbitmq',
    'ibosyang/ibos',
  ],
};
export { IO_GALLERY_IMAGES, GALLERY_IMAGES };
