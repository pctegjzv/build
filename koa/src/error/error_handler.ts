import * as _ from 'lodash';

import { RubickErrorResponse } from './exceptions';

const env = process.env.NODE_ENV || 'development';
const isDev = env === 'development';

// 自定义koa-onerror捕捉到的错误处理过程
// use for koa-onerror
function jsonErrorHandler(err) {
  const status = this.status || 404;
  if (err instanceof RubickErrorResponse) {
    this.body = err.data;
    return;
  }

  if (err.error) {
    this.body = new RubickErrorResponse(
      _.get(err, 'error.errors'),
      err.statusCode,
    );
  } else {
    const message =
      (isDev || err.expose) && err.message
        ? err.message
        : this.STATUS_CODES[status];

    this.body = new RubickErrorResponse(
      [
        {
          code: message,
          message: message,
        },
      ],
      status,
    );
  }
}

// tslint:disable-next-line no-unused-variable
function htmlErrorHandler(err) {
  // Todo：
  // 替换html模板，生产环境无错误调用栈，开发环境包含错误调用栈
  // 取出RubickErrorResponse中相应字段 status，err_code
}

// 统一处理404
async function commonErrorHandlerMiddleware(ctx, next) {
  // try {
  await next();
  // if (!ctx.body && !ctx._matchedRoutes && ctx.status !== 204) {
  //   ctx.status = 404;
  //   throw new RubickErrorResponse({
  //     message: `Endpoint [${ctx.request.url}] not found.`,
  //     source: 1019,
  //     code: 'resource_not_exist'
  //   }, 404);
  // 这里不需要catch错误，但需要抛出错误！！
  // 因为抛出的RubickErrorResponse已经是Node.js内置类Error错误的实例，
  // 故会被koa-onerror捕捉到。从而进入jsonErrorHandler方法中。
  // } catch (e) {
  // }
  // }
}

export { jsonErrorHandler, commonErrorHandlerMiddleware };
