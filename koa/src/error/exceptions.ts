import * as _ from 'lodash';

// Abstract class of javascript style
// 继承自Node.js内置类，用于on error中间件捕捉到
class MathildeBaseException extends Error {
  constructor() {
    super();
    if (this.constructor === MathildeBaseException) {
      throw new TypeError(
        'Abstract class "MathildeBaseException" cannot be instantiated directly.',
      );
    }
  }
}

/*
 *  Common error response class for Alauda micro services
 *  Error messages are now translated in UI instead of rendering in python
 *  See spec:
 *  https://bitbucket.org/mathildetech/atropos/wiki/projects/common/error-response-specification
 */

class RubickErrorResponse extends MathildeBaseException {
  errors: Array<any>;
  status: number;
  message: string;
  constructor(errors, status_code) {
    super();
    this.errors = [];
    const self = this;
    _.forEach(errors, error => {
      self.errors.push(_.pick(error, ['code', 'source', 'message', 'fields']));
      // self.message = error.message ;
    });
    this.status = status_code;
  }

  data() {
    return this.errors;
  }
}

export { RubickErrorResponse };
