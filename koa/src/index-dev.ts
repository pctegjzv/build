import * as dotenv from 'dotenv';
import * as fs from 'fs';
import { exec } from 'shelljs';

import config from './config/config';
import { startup } from './index';

let envs = '# int.env\n' + fs.readFileSync('int.env').toString();

if (fs.existsSync('custom.env')) {
  envs += '\n# custom.env\n' + fs.readFileSync('custom.env');
}

fs.writeFileSync('.env', envs);

Object.entries(dotenv.parse(envs)).forEach(([key, value]) => {
  process.env[key] = value;
});

exec(`kill -9 $(lsof -i:${config.default_port} -t) 2> /dev/null`);

startup(true);
