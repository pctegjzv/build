import * as Koa from 'koa';
import * as koaBodyParser from 'koa-bodyparser';
import * as compose from 'koa-compose';
import * as koaMount from 'koa-mount';
import * as koaOnError from 'koa-onerror';
import * as koaQs from 'koa-qs';
import * as koaSession from 'koa-session';
import * as koaStatic from 'koa-static';
import * as koaViews from 'koa-views';

import redirect from './action/forward';
import { authentication } from './common/auth';
import { getEnv } from './common/util';
import config from './config/config';
import { commonErrorHandlerMiddleware } from './error/error_handler';
import rbLogger from './logger/logger';
import { router } from './routes/routes';

export function startup(isDev?: boolean) {
  // init Koa
  const app = new Koa();

  // config Koa
  koaOnError(app, config.errorConfig); // error
  koaQs(app); // query string
  app.keys = config.secretKeys;

  const httpLoggerMiddleware = rbLogger.getHttpLoggerMiddleware();

  const maxAge = parseInt(getEnv('SESSION_MAX_AGE', 43200000), 10);
  const session = koaSession(config.sessionConf(maxAge), app);
  const views = koaViews(config.viewsConf.root, config.viewsConf.opts);
  const parser = koaBodyParser();

  const middlewares = [
    httpLoggerMiddleware,
    commonErrorHandlerMiddleware,
    ...['assets', 'images'].map(folder => {
      folder = `/static/${folder}`;
      return koaMount(folder, koaStatic(config.staticConf + folder));
    }),
    views,
    session,
    authentication,
    parser,
    router.routes(),
    router.allowedMethods(),
    redirect,
  ];

  if (isDev) {
    const { mockMiddleware } = require('./mock');
    middlewares.push(mockMiddleware);
  }

  app.use(compose(middlewares));

  // run Koa
  app.listen(config.default_port, () => {
    rbLogger.logger.info(
      'Rubick Sever is running, Listening on port ' + config.default_port,
    );
  });
}
