import config from '../../config/config';
const LOG_ROOT = config.loggerConfig.logDir;
const dev_config = {
  appenders: {
    out: { type: 'stdout' },
    debug: {
      type: 'file',
      filename: LOG_ROOT + 'rubick.debug.log',
      maxLogSize: 1024 * 1024 * 50,
      backups: 2,
    },
    debug_filter: {
      type: 'logLevelFilter',
      level: 'debug',
      maxLevel: 'debug',
      category: ['log'],
      appender: 'debug',
    },
    error: {
      type: 'file',
      filename: LOG_ROOT + 'rubick.error.log',
      maxLogSize: 1024 * 1024 * 10,
      backups: 2,
    },
    error_filter: {
      type: 'logLevelFilter',
      level: 'error',
      maxLevel: 'error',
      category: ['log'],
      appender: 'error',
    },
    access: {
      type: 'file',
      filename: LOG_ROOT + 'rubick.access.log',
      maxLogSize: 1024 * 1024 * 10,
      backups: 2,
    },
    access_filter: {
      type: 'logLevelFilter',
      level: 'info',
      maxLevel: 'info',
      category: ['log'],
      appender: 'access',
    },
  },
  categories: {
    default: {
      appenders: ['out', 'access_filter', 'debug_filter', 'error_filter'],
      level: 'debug',
    },
  },
  pm2: true,
};
export default dev_config;
