import { Context } from 'koa';
import * as compose from 'koa-compose';
import * as KoaRouter from 'koa-router';

import { injectAllRoutes } from '../decorators';

const router = new KoaRouter().prefix('/ajax');

// import './cluster.controller';
import './mirror.controller';
import './mock-demo.controller';
// import './load-balancer.controller';
import './org.controller';
// import './resource-quota.controller';
import './project.controller';
// import './license-activation.controller';
// import './namespace.controller';
import './region.controller';
import './roles.controller';

injectAllRoutes(router);

export const mockMiddleware = compose([
  async (ctx: Context, next) => {
    await next();
    if (ctx.matched.length) {
      ctx.set('RUBICK_MOCK', 'true');
    }
  },
  router.routes(),
  router.allowedMethods(),
]);
