import { Context } from 'koa';
import { mock } from 'mockjs';

import {
  Controller,
  LoginRequired,
  Method,
  RequestMapping,
} from '../decorators';

const licenseInfo = {
  product_name: '@title',
  enterprise_name: '@ctitle',
  'kind|1': ['release', 'beta'],
  license_code: "@string( 'aeiou', 200, 300 )",
  expire_at: '@time',
  days_until_expiration: '@natural(1,5)',
  'emails|0-10': ['@email'],
};

@Controller
@RequestMapping('/license-auth')
export class MockDemoController {
  // @LoginRequired
  @RequestMapping('/license-code', [Method.PUT, Method.GET])
  getLicense(ctx: Context) {
    ctx.body = mock(licenseInfo);
  }

  @LoginRequired
  @RequestMapping('/notify/emails', [Method.PUT])
  updataEmail(ctx: Context) {}

  @RequestMapping('/registry-code', [Method.GET])
  getRegistryCode(ctx: Context) {
    ctx.body = mock({
      registration_code: "@string( 'aeiou', 15, 20 )",
      product_name: '@ctitle',
    });
  }
}
