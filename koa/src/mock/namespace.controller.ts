import { mock } from 'mockjs';

import {
  Controller,
  LoginRequired,
  Method,
  RequestMapping,
} from '../decorators';

const mockNamespace = {
  cluster: {
    uuid: '@id',
    name: '@word',
    'display_name|1': ['北京一区', '深证一区', '深证二区'],
  },
  kubernetes: {
    apiVersion: 'v1',
    kind: 'Namespace',
    metadata: {
      // 创建时间
      creationTimestamp: '@date()T@time()Z',
      // 名字
      name: '@word',
      resourceVersion: '106086',
      selfLink: '/api/v1/namespaces/config',
      // uuid
      uid: '@id',
    },
    spec: {
      finalizers: ['kubernetes'],
    },
    status: {
      // Active or Terminating
      phase: '@pick(Active,Terminating)',
    },
  },
  resource_actions: ['view', 'update', 'delete'].map(
    action => 'namespace:' + action,
  ),
};

@Controller
@RequestMapping('/v2/kubernetes/clusters/cluster01/namespaces/namesacpe01')
export class KubsNamespaceController {
  @RequestMapping({
    method: Method.GET,
  })
  getNameSpaceDetail(ctx) {
    ctx.body = mock(mockNamespace);
  }
}

//for cmb
@Controller
@RequestMapping('/v2/custom/cmb/clusters/:clusterName/')
export class CmbNamespaceController {
  @LoginRequired
  @RequestMapping('namespaces')
  createNamespace(ctx) {}
}

@Controller
@RequestMapping('/v2/namespaces/')
export class NamespaceController {
  @RequestMapping()
  getNamespaces(ctx) {
    ctx.body = mock({
      count: '@integer(10,200)',
      page_size: 20,
      num_pages: '@integer(1,10)',
      'results|1-20': [mockNamespace],
    });
  }

  @RequestMapping({
    method: Method.DELETE,
    path: ':uuid',
  })
  deleteNamespace(ctx) {
    ctx.body = null;
  }

  @RequestMapping(':uuid')
  getNamespace(ctx) {
    ctx.body = mock(mockNamespace);
  }

  @RequestMapping(':uuid/resources')
  getResources(ctx) {
    ctx.body = mock({
      'apps|0-10': [
        {
          uuid: '@id',
          name: '@word',
          created_at: '@date()T@time()Z',
        },
      ],
      'alaudaservices|0-10': [
        {
          uuid: '@id',
          name: '@word',
          created_at: '@date()T@time()Z',
        },
      ],
      'configmaps|0-10': [
        {
          uuid: '@id',
          name: '@word',
          created_at: '@date()T@time()Z',
        },
      ],
      'persistentvolumeclaims|0-10': [
        {
          uuid: '@id',
          name: '@word',
          created_at: '@date()T@time()Z',
        },
      ],
    });
  }
}
