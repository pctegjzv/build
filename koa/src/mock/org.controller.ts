import { mock } from 'mockjs';

import { Controller, Method, RequestMapping } from '../decorators';

@Controller
@RequestMapping('/orgs')
export class OrgController {
  @RequestMapping({
    path: '/accounts/password',
    method: Method.POST,
  })
  verifyUsername(ctx) {
    ctx.body = mock({
      id: '@id',
    });
  }

  @RequestMapping({
    path: '/accounts/password',
    method: Method.PUT,
  })
  resetUaaPasswordByEmailCode(ctx) {
    ctx.body = null;
  }

  @RequestMapping({
    path: '/accounts/password/:username',
    method: Method.POST,
  })
  sendEmail(ctx) {
    ctx.body = mock({
      emailId: '@id',
      email: '@email',
    });
  }

  @RequestMapping('/:namespace/config/ldap/info')
  ldapInfo(ctx) {
    ctx.body = mock({
      result: {
        invalid_accounts: '@integer(0,10)',
      },
    });
  }

  @RequestMapping('/:namespace/accounts')
  getAccounts(ctx) {
    ctx.body = mock({
      count: 60,
      next: '?page=2',
      previous: null,
      page_size: 20,
      num_pages: 3,
      'results|60': [
        {
          username: '@word',
          type: 'organizations.LDAPAccount',
          created_at: '2017-11-29T10:23:59.421Z',
          updated_at: '2017-11-29T10:23:59.421Z',
          resource_actions: null,
          is_valid: true,
          'roles|20': [
            {
              role_uuid: '@id',
              role_name: '@id',
              template_display_name: '@word',
            },
          ],
        },
      ],
    });
  }

  // @RequestMapping({
  //   path: '/:namespace/accounts/:username',
  //   method: Method.PUT,
  // })
  // updateAccount(ctx) {}
}
