import { mock } from 'mockjs';

import { Controller, Method, RequestMapping } from '../decorators';

const mockProject = {
  namespace: '@word',
  name: '@word',
  uuid: '@id',
  display_name: '@word',
  status: '@word',
  description: '@word',
  template_uuid: '@id',
  template: '@word',
  created_at: '@date()T@time()Z',
  updated_at: '@date()T@time()Z',
  created_by: '@word',
  service_type: ['Kubernetes'],
  'clusters|3-10': [
    {
      name: '@word',
      display_name: '@word',
      uuid: '@id',
    },
  ],
  'admin_list|0-10': ['@name'],
};

const mockQuota = {
  name: '@word',
  uuid: '@id',
  'clusters|1-3': [
    {
      name: '@word',
      display_name: '@word',
      uuid: '@id',
      service_type: '@word',
      quota: {
        cpu: {
          max: '10',
          used: '2',
        },
        memory: {
          max: '1',
          used: '0.2',
        },
        storage: {
          max: '500',
          used: '100',
        },
        pvc_num: {
          max: '10',
          used: '5',
        },
        pods: {
          max: '100',
          used: '50',
        },
      },
    },
  ],
};

@Controller
@RequestMapping('/v2/batch/')
export class ProjectBatchController {
  @RequestMapping({
    method: Method.POST,
  })
  getProjectInfo(ctx) {
    ctx.body = {
      responses: {
        request1: {
          code: 200,
          header: {
            header: 'value',
          },
        },
        request2: {
          code: 204,
          header: {
            'Content-Type': 'application/json',
          },
          body: '成功',
        },
      },
    };
  }
}

@Controller
@RequestMapping('/v1/projects/')
export class ProjectController {
  @RequestMapping(':namespace/:uuid')
  getProjectInfo(ctx) {
    ctx.body = mock(mockProject);
  }

  @RequestMapping(':namespace')
  getProjects(ctx) {
    ctx.body = mock({ 'result|2-10': [mockProject] });
  }

  @RequestMapping(':namespace/:name/quota')
  getProjectQuota(ctx) {
    ctx.body = mock({
      // 项目名称
      name: '@word',
      // 项目UUID
      uuid: '@id',
      // 租户下的集群信息列表
      'clusters|1-10': [
        {
          // 集群名称
          name: '@id',
          // 集群名称
          display_name: '@word',
          // 集群UUID
          uuid: '@id',
          // 集群服务类型
          service_type: 'kubernetes',
          // 集群配额信息
          quota: {
            cpu: {
              max: 100,
              'used|0-100': 0,
            },
            memory: {
              max: 100,
              'used|0-100': 0,
            },
            storage: {
              max: 100,
              'used|0-100': 0,
            },
            pvc_num: {
              max: 100,
              'used|0-100': 0,
            },
            pods: {
              max: 100,
              'used|0-100': 0,
            },
          },
        },
      ],
    });
  }
  @RequestMapping(':namespace/:uuid/quota')
  getQuota(ctx) {
    ctx.body = mock(mockQuota);
  }
}
