import * as jsyaml from 'js-yaml';
import { Random, mock } from 'mockjs';

import { Controller, Method, RequestMapping } from '../decorators';

import { mockMirror } from './mirror.controller';

const mockRegion = {
  display_name: '@word',
  name: '@word',
  namespace: '@word',
  created_at: '@date()T@time()Z',
  container_manager: 'KUBERNETES',
  updated_at: '@date()T@time()Z',
  platform_version: 'v4',
  state: 'RUNNING',
  env_uuid: '@id',
  type: 'CLAAS',
  id: '@id',
  attr: {
    cluser: {
      nic: 'eth0',
    },
    docker: {
      path: '/var/lib/docker',
      version: '1.12.6',
    },
    cloud: {
      name: 'PRIVATE',
    },
    kubernetes: {
      type: 'original',
      endpoint: '@url',
      token: '@id',
      version: '1.9.1',
      cni: {
        type: 'ovs',
      },
    },
  },
  features: {
    logs: {
      type: 'official',
      storage: {
        read_log_source: 'default',
        write_log_source: 'default',
      },
    },
    'service-catalog': {
      type: 'official',
    },
    node: {
      features: ['auto-scaling'],
    },
  },
  resource_actions: ['view', 'update', 'delete', 'clear'].map(
    action => 'cluster:' + action,
  ),
  mirror: Random.pick([undefined, mockMirror]),
};

const regionData = {
  display_name: '@word',
  name: '@word',
  namespace: '@word',
  created_at: '@date()T@time()Z',
  container_manager: 'KUBERNETES',
  updated_at: '@date()T@time()Z',
  'platform_version|1': ['v3', 'v4'],
  state: 'RUNNING',
  env_uuid: '@id',
  type: 'CLAAS',
  id: '@id',
  attr: {
    cluser: {
      nic: 'eth0',
    },
    docker: {
      path: '/var/lib/docker',
      version: '1.12.6',
    },
    cloud: {
      name: 'PRIVATE',
    },
    kubernetes: {
      type: 'original',
      endpoint: '@url',
      token: '@id',
      version: '1.9.1',
      cni: {
        type: 'ovs',
      },
    },
  },
  features: {
    logs: {
      type: 'official',
      storage: {
        read_log_source: 'default',
        write_log_source: 'default',
      },
    },
    'service-catalog': {
      type: 'official',
    },
    node: {
      features: ['auto-scaling'],
    },
  },
  resource_actions: ['view', 'update', 'delete', 'clear'].map(
    action => 'cluster:' + action,
  ),
  'mirror|1': [
    {},
    {
      name: '@word',
      flag: '@word',
      'regions|2-5': [
        {
          name: '@word',
          display_name: '@word',
          uuid: '@id',
        },
      ],
    },
  ],
};

const randomFeature = (official = Random.boolean()) => {
  const enabled = Random.boolean();

  return {
    config: enabled && {
      type: official && 'official',
      application_uuid: official && Random.id(),
      integration_uuid: !official && Random.id(),
    },
    new_version_avaliable: Random.boolean(),
    application_info: enabled &&
      official && {
        uuid: Random.id(),
        name: Random.name(),
        status: Random.pick([
          'Running',
          'Warning',
          'Error',
          'Stopped',
          'Deploying',
        ]),
      },
    integration_info: enabled &&
      !official && {
        uuid: Random.id(),
        name: Random.name(),
        enabled: true,
      },
    template: {
      uuid: Random.id(),
      is_active: Random.boolean(),
      name: Random.name(),
      display_name: Random.name(),
      description: Random.title(),
      versions: [
        {
          version_uuid: Random.id(),
          values_yaml_content: jsyaml.safeDump(
            Array.from({
              length: Random.natural(5, 20),
            }).reduce(result => {
              result[Random.name()] = Random.title();
              return result;
            }, {}),
          ),
        },
      ],
    },
  };
};

@Controller
@RequestMapping('/v2/regions/:namespace/')
export class RegionV2Controller {
  @RequestMapping(':name')
  getRegion(ctx) {
    ctx.body = mock(mockRegion);
  }

  @RequestMapping(':name', Method.POST)
  accessRegion(ctx) {
    ctx.body = mock(mockRegion);
  }

  @RequestMapping()
  getRegions(ctx) {
    ctx.body = mock({ 'result|0-5': [regionData] });
  }

  @RequestMapping(':name/nodes')
  getRegionNodes(ctx) {
    ctx.body = mock({
      'items|400-500': [
        {
          status: {
            volumes_attached: null,
            daemon_endpoints: {
              kubelet_endpoint: {
                port: 10250,
              },
            },
            capacity: {
              'alpha.kubernetes.io/nvidia-gpu': '0',
              pods: '200',
              cpu: '4',
              memory: '14352452Ki',
            },
            addresses: [
              {
                type: 'InternalIP',
                address: '10.7.0.4',
              },
              {
                type: 'Hostname',
                address: '10.7.0.4',
              },
            ],
            allocatable: {
              'alpha.kubernetes.io/nvidia-gpu': '0',
              pods: '200',
              cpu: '4',
              memory: '14250052Ki',
            },
            'images|0-20': [
              {
                size_bytes: 994396426,
                'names|1-2': [
                  'index.alauda.cn/claas/harbinger@sha256:3b2e525399378a2455fcdd3738875e7eba163731d3be09bd74ac01bebc4cda35',
                ],
              },
            ],
            node_info: {
              kube_proxy_version: 'v1.9.1',
              operating_system: 'linux',
              kernel_version: '3.10.0-514.26.2.el7.x86_64',
              system_uuid: 'AACEC22A-E5B5-AD4B-A0AC-137A6DC03788',
              container_runtime_version: 'docker://1.12.6',
              os_image: 'CentOS Linux 7 (Core)',
              architecture: 'amd64',
              boot_id: '668d8714-636e-4012-bce3-138ac861918b',
              machine_id: 'e8abe05b1d42472d882942fe2bdfc47e',
              kubelet_version: 'v1.9.1',
            },
            volumes_in_use: null,
            phase: null,
            conditions: [
              {
                last_heartbeat_time: '2018-04-17 12:18:33+00:00',
                status: 'False',
                last_transition_time: '2018-03-08 07:08:36+00:00',
                reason: 'KubeletHasSufficientDisk',
                message: 'kubelet has sufficient disk space available',
                type: 'OutOfDisk',
              },
              {
                last_heartbeat_time: '2018-04-17 12:18:33+00:00',
                status: 'False',
                last_transition_time: '2018-03-08 07:08:36+00:00',
                reason: 'KubeletHasSufficientMemory',
                message: 'kubelet has sufficient memory available',
                type: 'MemoryPressure',
              },
              {
                last_heartbeat_time: '2018-04-17 12:18:33+00:00',
                status: 'False',
                last_transition_time: '2018-03-08 07:08:36+00:00',
                reason: 'KubeletHasNoDiskPressure',
                message: 'kubelet has no disk pressure',
                type: 'DiskPressure',
              },
              {
                last_heartbeat_time: '2018-04-17 12:18:33+00:00',
                status: 'True',
                last_transition_time: '2018-03-08 07:09:41+00:00',
                reason: 'KubeletReady',
                message: 'kubelet is posting ready status',
                type: 'Ready',
              },
            ],
          },
          kind: null,
          spec: {
            provider_id: null,
            taints: [
              {
                key: 'node-role.kubernetes.io/master',
                time_added: null,
                effect: 'NoSchedule',
                value: null,
              },
            ],
            config_source: null,
            unschedulable: null,
            pod_cidr: '10.99.0.0/24',
            external_id: '10.7.0.4',
          },
          api_version: null,
          metadata: {
            name: '10.7.0.4',
            owner_references: null,
            generation: null,
            namespace: null,
            labels: {
              'node-role.kubernetes.io/master': '',
              'kubernetes.io/hostname': '10.7.0.4',
              'beta.kubernetes.io/os': 'linux',
              'beta.kubernetes.io/arch': 'amd64',
            },
            generate_name: null,
            deletion_timestamp: null,
            cluster_name: null,
            finalizers: null,
            deletion_grace_period_seconds: null,
            initializers: null,
            self_link: '/api/v1/nodes/10.7.0.4',
            resource_version: '14249935',
            creation_timestamp: '2018-03-08 07:08:44+00:00',
            annotations: {
              'node.alpha.kubernetes.io/ttl': '0',
              'flannel.alpha.coreos.com/public-ip': '10.7.0.4',
              'flannel.alpha.coreos.com/backend-data':
                '{"VtepMAC":"b2:74:ab:83:bd:b9"}',
              'flannel.alpha.coreos.com/kube-subnet-manager': 'true',
              'flannel.alpha.coreos.com/backend-type': 'vxlan',
              'volumes.kubernetes.io/controller-managed-attach-detach': 'true',
            },
            uid: '89f8205b-229f-11e8-bd4e-000d3a820982',
          },
        },
      ],
      kind: 'NodeList',
      api_version: 'v1',
      metadata: {
        self_link: '/api/v1/nodes',
        resource_version: '14249938',
        _continue: null,
      },
    });
  }

  @RequestMapping(':regionName/features')
  getRegionFeatures(ctx) {
    ctx.body = {
      log: randomFeature(true),
      volume: randomFeature(),
    };
  }

  @RequestMapping(':regionName/features/:featureName', [
    Method.POST,
    Method.PUT,
  ])
  editRegionFeature(ctx) {
    ctx.body = randomFeature();
  }

  @RequestMapping(':regionName/features/:featureName', Method.DELETE)
  deleteRegionFeature(ctx) {
    ctx.body = null;
  }
}
