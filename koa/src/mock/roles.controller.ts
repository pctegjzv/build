import { mock } from 'mockjs';

import { Controller, RequestMapping } from '../decorators';

@Controller
@RequestMapping('/v1/roles/')
export class ProjectController {
  @RequestMapping(':namespace/types')
  getProjects(ctx) {
    ctx.body = mock([
      {
        role_uuid: '@id',
        role_name: 'role1',
        template_display_name: '',
      },
      {
        role_uuid: '@id',
        role_name: 'role2',
        template_display_name: '@word',
      },
      {
        role_uuid: '@id',
        role_name: '@word',
        template_display_name: '@word',
      },
      {
        role_uuid: '@id',
        role_name: 'role4',
        template_display_name: '',
      },
      {
        role_uuid: '@id',
        role_name: '@word',
        template_display_name: '',
      },
    ]);
  }
}
