#!/bin/sh
unset $(env | grep -i alauda_ | awk -F '=' '{print $1}')
pm2 start ./koa/pm2_conf/cn.conf.json --env production

nginxConf='/etc/nginx/sites-enabled/rubick_nginx.conf'
mkdir -p /etc/nginx/sites-enabled/
mkdir -p /etc/nginx/sites-enabled/logs

# For custom files
mkdir -p /rubick/static/assets/custom

if [ -f "$nginxConf" ]; then
    rm $nginxConf
fi

echo 'Running in prod stage'
ln -s /rubick/conf/rubick_koa_nginx.conf $nginxConf

cp /rubick/conf/mime.types /etc/nginx/sites-enabled/mime.types

# Need to set a valid value for OSS_SERVER_URL to make sure nginx start up correctly
if [ -z "$OSS_SERVER_URL" ]
then
  OSS_SERVER_URL="http://0.0.0.0"
fi

if [ -z "$LOCAL_BACKEND_ENDPOINT" ]
then
  LOCAL_BACKEND_ENDPOINT="http://127.0.0.1:2333"
fi

sed -i "s|{{OSS_SERVER_URL}}|$OSS_SERVER_URL|g" $nginxConf
sed -i "s|{{LOCAL_BACKEND_ENDPOINT}}|$LOCAL_BACKEND_ENDPOINT|g" $nginxConf
sed -i "s|{{SOCK_SERVER_URL}}|$SOCK_SERVER_URL|g" $nginxConf
nginx -p /etc/nginx/sites-enabled/ -c /etc/nginx/sites-enabled/rubick_nginx.conf
