#!/bin/sh
if [ $1 == "frontend" ]
  then
  export TARGET_SERVER=http://backend:2333

  echo "removing old node_modules"
  rm -rf node_modules

  echo "get node_modules from image"
  cp -r /tmp/node_modules .

  echo "current target server"$TARGET_SERVER
  echo "frontend starting"
  npm run webpack:dev:compose

  elif [ $1 == "backend" ]
  then
  echo "get node_modules from image"
  mv -f /tmp/node_modules node_modules
  cp -r /tmp/koa/node_modules koa/node_modules

  echo "backend starting"
  npm run koa
fi