import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-markdown/css/bootstrap-markdown.min.css';
import 'eonasdan-bootstrap-datetimepicker-npm/build/css/bootstrap-datetimepicker.min.css';
import 'codemirror/lib/codemirror.css';
import 'codemirror/addon/scroll/simplescrollbars.css';
import 'codemirror/theme/material.css';
import 'font-awesome/css/font-awesome.min.css';
import 'highlight.js/styles/default.css';
import 'angularjs-slider/dist/rzslider.css';
import 'github-markdown-css/github-markdown.css';
import 'blueimp-file-upload/css/jquery.fileupload.css';
import 'angular-material/angular-material.css';

import 'semantic-ui-accordion/accordion.min.css';
import 'semantic-ui-transition/transition.min.css';
import 'semantic-ui-dropdown/dropdown.min.css';
import 'semantic-ui-button/button.min.css';
import 'semantic-ui-input/input.min.css';
import 'semantic-ui-search/search.min.css';
import 'semantic-ui-checkbox/checkbox.min.css';
import 'semantic-ui-label/label.min.css';
import 'semantic-ui-icon/icon.min.css';
import 'semantic-ui-loader/loader.min.css';
import 'semantic-ui-popup/popup.min.css';
import 'semantic-ui-dimmer/dimmer.min.css';
import 'semantic-ui-modal/modal.min.css';
import 'semantic-ui-message/message.min.css';

// dragula
import 'dragula/dist/dragula.css';

/********************************* require JS *********************************/
require('./core/top-message-bubble.js');

// Require angular app files:
function requireAngularJs() {
  // IMPORTANT NOTE:
  // Unused files must be excluded before climbing the dependencies
  // Otherwise some weird issues will occur
  const req = require.context('./', true, /^((?!\.entry).)+\.js$/);
  const ngFiles = req.keys();

  // By convention, these files suffices identify what they are.
  // Also, we need to make sure they are required in order, otherwise there will be dependency issues.
  // i.e., modules must be required before other types
  _filterAndRequire('module');
  _filterAndRequire('constant');
  _filterAndRequire('config');

  // Require all other files
  _filterAndRequire();

  function _filterAndRequire(type = '') {
    const ngFilesToRequire = ngFiles.filter(filename =>
      filename.endsWith(`${type}.js`),
    );
    ngFilesToRequire.forEach(req);
    _.pullAll(ngFiles, ngFilesToRequire); // No longer need the filenames which are already required
  }
}

requireAngularJs();

const requireScss = require.context('./', true, /\.scss$/);
requireScss.keys().forEach(requireScss);
