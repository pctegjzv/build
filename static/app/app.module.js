import { ANGULAR_DOWNGRADE_MODULE_NAME } from './app.constant';

// Container for downgraded Angular dependencies
angular.module(ANGULAR_DOWNGRADE_MODULE_NAME, []);

angular.module('app', [
  'app.components.layout',
  'app.components.common',
  'app.components.pages',
]);
