/**
 * Created by liudong on 16/10/31.
 */
function requireTemplates() {
  importAll(require.context('../views/', true, /\.html/));
  importAll(require.context('./components/', true, /\.html/));
}
function importAll(r) {
  r.keys().map(r);
}

requireTemplates();
