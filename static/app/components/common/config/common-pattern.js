/**
 * Created by jiaoguangxin on 2017/05/23.
 */

export const RESOURCE_NAME_BASE = {
  pattern: /^[a-zA-Z]([-a-zA-Z0-9]*[a-zA-Z0-9])?$/,
  tip: 'regexp_tip_resource_name_base',
};

export const K8S_RESOURCE_NAME_BASE = {
  pattern: /^[a-z0-9]([-a-z0-9]*[a-z0-9])?$/,
  tip: 'regexp_tip_k8s_resource_name_base',
};

export const K8S_LOG_FILE_PATTERN = {
  pattern: '^(/[A-Za-z0-9\\s_@\\-^!#$%&+={}\\[\\]*\\.]+)+$',
  tip: 'regexp_tip_k8s_log_file',
};

export const INT_PATTERN = {
  pattern: /^-?\d+$/,
  tip: 'integer_pattern',
};

export const K8S_RESOURCE_KEY = {
  pattern: /^[-._a-zA-Z0-9]+$/,
  tip: 'regexp_tip_k8s_resource_key',
};

export const CODE_PATH_PATTERN = {
  GIT: {
    pattern: /^(http(s?):\/\/.+)|(git@.+:.+)|(ssh:\/\/.+@.+)|(git:\/\/.+)/,
    tip: '',
  },
  SVN: {
    pattern: /^(http(s?):\/\/.+)|(svn:\/\/.+)/,
    tip: '',
  },
};

export const K8S_SERVICE_NAME = {
  pattern: /^[a-z]([-a-z0-9]*[a-z0-9])?$/,
  tip: 'regexp_tip_k8s_service_name',
};

export const SUB_PATH = {
  pattern: /^([a-zA-Z0-9_.][a-zA-Z0-9._-]*\/?)*$/,
  tip: 'regexp_tip_sub_path',
};

export const DOMAIN_NAME = {
  pattern: /^[a-z0-9]([-a-z0-9]*[a-z0-9])?(\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*$/,
  tip: 'regexp_tip_domain_name',
};
