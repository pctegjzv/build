import * as _ from 'lodash';

/**
 * Renders artifacts table.
 * An example of artifacts array:
 *
 *  const array = [
 *  {
 *    key: 'dir1/dir2/test.txt',
 *    size: 123,
 *    last_modified: '2017-03-22T09:08:27.836Z'
 *  },
 *  {
 *    key: 'dir1/test.txt',
 *    size: 1231232,
 *    last_modified: '2017-03-22T09:08:27.836Z'
 *  },
 *  {
 *    key: 'dir1/dir2/dir3/test1.txt',
 *    size: 12322,
 *    last_modified: '2017-03-22T09:08:27.836Z'
 *  },
 *  {
 *    key: 'dir1/dir2/dir3/test2.txt',
 *    size: 1232255,
 *    last_modified: '2017-03-22T09:08:27.836Z'
 *  },
 *  {
 *    key: 'dir1/dir2/dir3/test2.txt',
 *    size: 121223,
 *    last_modified: '2017-03-22T09:08:27.836Z'
 *  },
 *  {
 *    key: 'somedir0/adsfd/dadf/test.txt',
 *    size: 121223,
 *    last_modified: '2017-03-22T09:08:27.836Z'
 *  }
 *  ];
 *
 * In the template, we will merge the folders into a better presentation.
 */
const templateStr = require('app/components/common/rb-artifacts-table/rb-artifacts-table.component.html');
angular.module('app.components.common').component('rbArtifactsTable', {
  controller: rbArtifactsTableController,
  controllerAs: 'vm',
  template: templateStr,
  bindings: {
    artifacts: '<',
    prefix: '<', // can be plural, separated by comma
  },
});

function rbArtifactsTableController($log, rbOssClient, rbSafeApply) {
  const vm = this;

  vm.refreshArtifactList = refreshArtifactList;
  vm.$onInit = onInit;
  vm.$onChanges = onChanges;

  //////////
  function onInit() {
    vm.loading = true;
  }

  async function onChanges() {
    try {
      await refreshArtifactList();
    } catch (e) {
      // ???
      $log.error('Failed to load artifacts for ' + vm.prefix);
    }
    vm.loading = false;
    rbSafeApply();
  }

  async function postProcessArtifacts(artifacts) {
    artifacts = await Promise.all(
      artifacts.map(async artifact => {
        return {
          ...artifact,
          downloadUrl: await rbOssClient.generateLink({
            skipMiddleLayer: true,
            path: artifact.path,
          }),
        };
      }),
    );
    // TODO: the user may expect to see the artifacts in a structural table
    // TODO: but right now we only do a simple ordering
    return _.sortBy(artifacts, 'key');
  }

  async function refreshArtifactList() {
    if (!vm.artifacts && vm.prefix) {
      const artifactLists = await Promise.all(
        vm.prefix.split(',').map(prefix => fetchAndProcessArtifactList(prefix)),
      );
      vm.artifactList = artifactLists.reduce(
        (accum, artifacts) => [...accum, ...artifacts],
        [],
      );
    } else {
      vm.artifactList = await postProcessArtifacts(vm.artifacts);
    }
  }

  async function fetchAndProcessArtifactList(prefix) {
    const artifacts = await rbOssClient.listBucketFiles({ prefix });
    artifacts.forEach(artifact => {
      artifact.path = artifact.key;
      artifact.key = _.trimStart(artifact.path.substr(prefix.length), '/');
    });

    return postProcessArtifacts(artifacts);
  }
}
