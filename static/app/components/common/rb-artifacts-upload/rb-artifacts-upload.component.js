import { fromEvent, of } from 'rxjs';
import { concat, filter, map, mapTo, takeUntil } from 'rxjs/operators';

const templateStr = require('app/components/common/rb-artifacts-upload/rb-artifacts-upload.component.html');
angular
  .module('app.components.common')
  .constant('ARTIFACT_UPLOAD_STATE', {
    UPLOADING: 'UPLOADING',
    DELETING: 'DELETING',
    ERROR: 'ERROR',
    READY: 'READY',
  })
  .component('rbArtifactsUpload', {
    controller: rbArtifactsUploadController,
    controllerAs: 'vm',
    template: templateStr,
    bindings: {
      bucket: '<',
      prefix: '<',
      stateChanged: '&',
    },
  });

function rbArtifactsUploadController(
  $element,
  $log,
  rbToast,
  ARTIFACT_UPLOAD_STATE,
  translateService,
  rbCreateObservableFunction,
  rbOssClient,
  rbSafeApply,
) {
  const vm = this;

  vm.removeFile = removeFile;

  vm.$onInit = onInit;
  vm.$postLink = postLink;
  vm.$onDestroy = onDestroy;
  async function onInit() {
    ////////
    // list of file containers
    //  -> { name, file, progress$ }
    const initFiles = (await rbOssClient
      .listBucketFiles({ bucket: vm.bucket, prefix: vm.prefix })
      .catch(err => {
        $log.error('Reloading uploaded file failed!');
        $log.error(err);
        return [];
      })).map(file => {
      return {
        name: file.key.substr((vm.prefix || '').length + 1),
        size: file.size,
      };
    });

    vm.fileContainers = initFiles.map(file => {
      return {
        file,
        name: file.name,
        progress$: of(100),
        finished: true,
      };
    });
    emitUploadState();
    vm.initialized = true;
    rbSafeApply();
  }

  function postLink() {
    vm.$fileInput = $('#artifact_upload_input', $element)[0];
    vm.fileAdded$ = fromEvent(vm.$fileInput, 'change').pipe(
      map(() => vm.$fileInput.files[0]),
      filter(file => !!file),
    );

    vm.fileAddedSubscriber = vm.fileAdded$.subscribe(file =>
      addAndUpload(file),
    );
  }

  function onDestroy() {
    vm.fileAddedSubscriber.unsubscribe();
  }

  async function addAndUpload(file) {
    vm.$fileInput.value = '';
    // In case a file with the same name already exists
    await removeFile(file.name, true);

    const uploadEventHandlers = {};

    const uploadProgress$ = rbCreateObservableFunction(
      uploadEventHandlers,
      'progress',
    ).pipe(
      map(e => {
        if (e.lengthComputable) {
          return Math.round((e.loaded * 100) / e.total);
        } else {
          return -1; // ??? shall be skipped?
        }
      }),
      filter(progress => progress >= 0),
    );
    const uploadLoad$ = rbCreateObservableFunction(
      uploadEventHandlers,
      'load',
    ).pipe(mapTo(100));
    // Progress should emit 0 ~ 100, and complete when load event fires
    const progress$ = uploadProgress$.pipe(
      takeUntil(uploadLoad$),
      concat(uploadLoad$),
    );

    let cancelHandler = undefined;
    const timeout = new Promise(res => (cancelHandler = res));

    const fileContainer = {
      file,
      // File should have unique names subject to a single upload.
      name: file.name,
      size: file.size,
      progress$,
      cancelHandler,
    };

    rbOssClient
      .uploadFile({
        uploadEventHandlers,
        bucket: vm.bucket,
        path: vm.prefix,
        file: file,
        timeout,
      })
      .catch(error => {
        fileContainer.uploadError = error;
        rbToast.error(translateService.get('artifacts_upload_failed'));
      })
      .then(() => {
        fileContainer.finished = true;
        emitUploadState();
      });
    vm.fileContainers.push(fileContainer);
    emitUploadState();
  }

  async function removeFile(fileName, skipDeleting = false) {
    const fileContainerToRemove = vm.fileContainers.find(
      fileContainer => fileContainer.name === fileName,
    );
    if (!fileContainerToRemove) {
      return;
    }
    if (!skipDeleting && fileContainerToRemove.finished) {
      // Call remove API
      fileContainerToRemove.deleting = true;
      emitUploadState();
      rbSafeApply();
      await rbOssClient.deleteFile({
        path: (vm.prefix ? vm.prefix + '/' : '') + fileName,
      });
      fileContainerToRemove.deleting = false;
    }

    // Cancel upload
    if (
      fileContainerToRemove.cancelHandler &&
      fileContainerToRemove.cancelHandler instanceof Function
    ) {
      fileContainerToRemove.cancelHandler();
    }

    vm.fileContainers = vm.fileContainers.filter(
      fileContainer => fileContainer.name !== fileName,
    );
    emitUploadState();
    rbSafeApply();
  }

  function emitUploadState() {
    if (vm.fileContainers.some(fileContainer => fileContainer.uploadError)) {
      vm.stateChanged({ state: ARTIFACT_UPLOAD_STATE.ERROR });
    } else if (
      vm.fileContainers.some(fileContainer => !fileContainer.finished)
    ) {
      vm.stateChanged({ state: ARTIFACT_UPLOAD_STATE.UPLOADING });
    } else if (
      vm.fileContainers.some(fileContainer => fileContainer.deleting)
    ) {
      vm.stateChanged({ state: ARTIFACT_UPLOAD_STATE.DELETING });
    } else {
      vm.stateChanged({ state: ARTIFACT_UPLOAD_STATE.READY });
    }
  }
}
