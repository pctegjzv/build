const templateStr = require('app/components/common/rb-autocomplete/rb-autocomplete.html');
angular.module('app.components.common').component('rbAutocomplete', {
  bindings: {
    content: '<',
    value: '<',
    name: '<',
    pattern: '<',
    onUpdate: '&',
    required: '<',
    setting: '<',
  },
  controller: rbAutoCompleteController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbAutoCompleteController($scope, $element, translateService) {
  const vm = this;
  vm.options = [];
  vm.handleKeywordChange = handleKeywordChange;
  vm.$onChanges = _onChange;
  /////
  function _onChange(changesObj) {
    if (changesObj.content) {
      _updateSource(changesObj.content.currentValue);
    }
  }

  function _updateSource(curValue) {
    if (!curValue) {
      return;
    }
    vm.options = [];
    curValue.forEach(itm => {
      vm.options.push({ title: itm });
    });
    $element.find('.ui.search').search({
      ...(vm.setting || {}),
      searchFields: 'title',
      source: vm.options,
      cache: false,
      error: {
        noResults: translateService.get('search_no_result'),
      },
      onSelect: result => {
        vm.value = result.title;
        vm.handleKeywordChange();
      },
    });
  }

  function handleKeywordChange() {
    if (vm.preValue !== vm.value) {
      vm.onUpdate({ value: vm.value });
      vm.preValue = vm.value;
    }
    return false;
  }
}
