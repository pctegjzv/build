/**
 * Created by liudong on 2017/5/23.
 */
const templateStr = require('app/components/common/rb-basic-info-panel/rb-basic-info-panel.html');

angular.module('app.components.common').component('rbBasicInfoPanel', {
  controller: rbBasicInfoPanelController,
  controllerAs: 'vm',
  template: templateStr,
  bindings: {
    initialized: '<',
    threeColumn: '<',
    iconUrl: '<',
    fields: '<', // fields: [{name, value, state, stateParams, disabled, row}]
  },
});

function rbBasicInfoPanelController($state, rb2RouterUtil) {
  const vm = this;
  vm.$onInit = _init;
  vm.routeToUrl = field => rb2RouterUtil.go(field.state, field.stateParams);

  //////////
  function _init() {
    vm.isBasicInfoCollapse = false;
  }
}
