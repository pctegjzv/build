angular
  .module('app.components.common')
  .directive('rbBlankPageContainer', rbBlankPageContainer);

function rbBlankPageContainer() {
  return {
    scope: {
      template: '<', // Static template cover the default setting
      className: '@', // Custom class name
      img: '@', // Default image
      mainTitle: '@', // Default title
      subTitle: '@', // Default subtitle
      triggerRedirect: '<',
    },
    bindToController: true,
    controller: rbBlankPageContainerController,
    controllerAs: 'vm',
  };
}
function rbBlankPageContainerController(
  $element,
  $scope,
  $compile,
  translateService,
) {
  const vm = this;
  vm.$onInit = _onInit;
  vm.$onChanges = _onChanges;

  function _onInit() {
    replaceContent();
  }
  function _onChanges(changeObj) {
    if (_.get(changeObj, 'triggerRedirect')) {
      replaceContent();
    }
  }

  function replaceContent() {
    if (vm.triggerRedirect === true) {
      const template =
        vm.template ||
        `<div class=${'rb-blank-page-content ' + (vm.className || '')}>
          <img ng-src=${vm.img ||
            '/static/images/blank/no_service.svg'} alt='tips'>
          <h1>${translateService.get(
            vm.mainTitle || 'private_region_not_support',
          )}</h1>
        </div>`;
      $element.html($compile(template)($scope));
    }
  }
}
