import templateString from './rb-code-quality-panel-sonarqube.component.html';

angular
  .module('app.components.common')
  .component('rbCodeQualityPanelSonarqube', {
    template: templateString,
    controller: rbCodeQualityPanelSonarqubeController,
    controllerAs: 'vm',
    bindings: {
      executionData: '<',
      measures: '<',
      analysedDate: '<',
      link: '<',
    },
  });

function rbCodeQualityPanelSonarqubeController() {
  const vm = this;

  vm.$onInit = _onInit;

  function _onInit() {
    vm.metricGroup = {};
    if (!vm.executionData || !vm.executionData.analysis_result) {
      return;
    }
    const measures = vm.executionData.analysis_result.measures;
    measures.forEach(item => {
      switch (item.metric) {
        case 'alert_status':
          vm.metricGroup[item.metric] = {
            value: item.value,
          };
          break;
        case 'security_rating':
        case 'sqale_rating':
        case 'reliability_rating':
          vm.metricGroup[item.metric] = {
            value: String.fromCharCode(64 + parseInt(item.value)),
          };
          break;
        case 'coverage':
        case 'duplicated_lines_density':
          vm.metricGroup[item.metric] = getPercentMetric(
            item.value,
            item.metric,
          );
          break;
      }
    });
  }

  function getPercentMetric(valueStr, type) {
    const value = parseFloat(valueStr) / 100;
    const result = {
      value: valueStr + '%',
    };
    result.status =
      type === 'coverage'
        ? getCoverageStatus(value)
        : getRepetitionStatus(value);
    return result;
  }

  function getCoverageStatus(value) {
    if (value >= 0.8) {
      return 'A';
    } else if (value >= 0.7) {
      return 'B';
    } else if (value >= 0.5) {
      return 'C';
    } else if (value >= 0.3) {
      return 'D';
    } else {
      return 'E';
    }
  }

  function getRepetitionStatus(value) {
    if (value < 0.03) {
      return 'A';
    } else if (value < 0.05) {
      return 'B';
    } else if (value < 0.1) {
      return 'C';
    } else if (value < 0.2) {
      return 'D';
    } else {
      return 'E';
    }
  }
}
