/*
 * usage:
 *
 * <rb-copy-span
 *  bind-text="vm.textShown"
 *  text="vm.textCopy"></rb-copy-span>
 *
 * bind-text: innerHTML in span
 * text: text you wanna copy
 *
 * if text is not exist, both recognized bind-text's value
 *
 */

import clipboard from 'clipboard-polyfill';
import templateStr from 'app/components/common/rb-copy-span/rb-copy-span.html';

angular.module('app.components.common').component('rbCopySpan', {
  bindings: {
    bindText: '<',
    text: '<',
    tooltip: '<',
  },
  controller: rbCopySpanController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbCopySpanController(translateService, $log, rbSafeApply) {
  const vm = this;

  vm.copy = copyText;
  vm.msOver = msOver;
  vm.$onInit = _init;
  vm.$onChanges = _onChanges;

  let text;

  function _init() {
    vm.tooltipDisplay = getTooltipDisplay();
  }

  function _onChanges(changed) {
    text =
      (changed.text && changed.text.currentValue) ||
      changed.bindText.currentValue;
  }

  function copyText($event) {
    $event.stopPropagation();
    try {
      clipboard.writeText(text);
      vm.tooltipDisplay = translateService.get('copy_clipboard_success');
    } catch (err) {
      $log.error(err);
      vm.tooltipDisplay = translateService.get('copy_clipboard_fail');
    }
    rbSafeApply();
  }

  function msOver() {
    vm.tooltipDisplay = getTooltipDisplay();
  }

  function getTooltipDisplay() {
    return vm.tooltip || translateService.get('click_to_copy');
  }
}
