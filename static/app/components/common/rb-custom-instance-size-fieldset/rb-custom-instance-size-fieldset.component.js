import { get } from 'lodash';
import { INT_PATTERN } from 'app/components/common/config/common-pattern';

const templateStr = require('app/components/common/rb-custom-instance-size-fieldset/rb-custom-instance-size-fieldset.component.html');
angular
  .module('app.components.common')
  .component('rbCustomInstanceSizeFieldset', {
    bindings: {
      onChange: '&',
      /**
       * Initial values for mem/cpu
       *
       * { memory: number, cpu: number }
       */
      init: '<',
      initMin: '<',
    },
    controller: rbCustomInstanceSizeFieldsetController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbCustomInstanceSizeFieldsetController(
  CUSTOM_INSTANCE_SIZE_CONFIG,
  $scope,
) {
  const vm = this;
  vm.$onInit = _init;
  vm.onCustomSizeChange = onCustomSizeChange;
  vm.baseUnit = 1;
  vm.unitsOptions = [
    {
      key: 'MB',
      value: 1,
    },
    {
      key: 'GB',
      value: 1024,
    },
  ];
  vm.onFilterByChange = onFilterByChange;
  //////////

  $scope.$watch('vm.baseUnit', () => {
    vm.min = getMin();
  });

  function _init() {
    vm.min = getMin();
    vm.resourceNameInt = INT_PATTERN;
    vm.customSize = Object.assign({}, vm.init || vm.min);
    if (vm.customSize.memory && vm.customSize.memory % 1024 === 0) {
      vm.baseUnit = 1024;
    }
    vm.memDisplay = vm.customSize.memory / vm.baseUnit;

    vm.onChange(Object.assign({}, vm.customSize));
  }

  function getMin() {
    const cpu =
      (vm.initMin && vm.initMin['cpu']) ||
      get(CUSTOM_INSTANCE_SIZE_CONFIG, 'custom.cpu.min', 0.125);
    const memory = get(CUSTOM_INSTANCE_SIZE_CONFIG, 'custom.mem.min', 64);
    const min = { cpu, memory };
    return vm.baseUnit === 1 ? min : Object.assign({}, min, { memory: 1 });
  }

  function onFilterByChange({ value }) {
    if (vm.baseUnit !== value) {
      vm.baseUnit = value;
      onCustomSizeChange();
    }
  }

  function onCustomSizeChange() {
    vm.memDisplay && (vm.customSize.memory = vm.memDisplay * vm.baseUnit);

    vm.onChange(Object.assign({}, vm.customSize));
  }
}
