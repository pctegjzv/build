import * as _ from 'lodash';
const templateStr = require('app/components/common/rb-custom-instance-size-sliders/rb-custom-instance-size-sliders.component.html');
angular
  .module('app.components.common')
  .component('rbCustomInstanceSizeSliders', {
    bindings: {
      /**
       * Additional options to override the default options.
       *
       * See DEFAULT_SLIDER_OPTIONS.
       */
      options: '<',

      /**
       * Emit changes when values changed:
       *
       * { memory: number, cpu: number }
       */
      onChange: '&',

      /**
       * Initial values for mem/cpu
       *
       * { memory: number, cpu: number }
       */
      init: '<',
    },
    controller: rbCustomInstanceSizeSlidersController,
    controllerAs: 'vm',
    template: templateStr,
  });

export const DEFAULT_SLIDER_OPTIONS = {
  cpu: {
    floor: 0.25,
    ceil: 1,
    step: 0.25,
    precision: 2,
    hidePointerLabels: true,
    hideLimitLabels: true,
    showSelectionBar: true,
  },
  memory: {
    floor: 256,
    ceil: 2048,
    step: 256,
    precision: 0,
    hidePointerLabels: true,
    hideLimitLabels: true,
    showSelectionBar: true,
  },
};

const DEFAULT_INIT = {
  cpu: 0.5,
  memory: 512,
};

function rbCustomInstanceSizeSlidersController($timeout, $scope) {
  const vm = this;

  vm.$onInit = $onInit;
  //////////
  function $onInit() {
    vm.model = _.merge({}, DEFAULT_INIT, vm.init);
    const options = _.merge(
      {
        cpu: {
          onChange: _onChange,
        },
        memory: {
          onChange: _onChange,
        },
      },
      DEFAULT_SLIDER_OPTIONS,
      vm.options,
    );
    vm.cpuOptions = options.cpu;
    vm.memoryOptions = options.memory;

    _onChange();
  }

  function _onChange() {
    // Clone the object
    vm.onChange({ ...vm.model });

    // See:
    // https://github.com/angular-slider/angularjs-slider#my-slider-is-not-rendered-correctly-on-load
    $timeout(() => {
      $scope.$broadcast('rzSliderForceRender');
    });
  }
}
