/*
 * Dropdown Menu used for detail page button group
 * Usage:
 * <rb-dropdown-menu rb-title="{{'action' | translate}}" class="pull-right">
      <rb-dropdown-menu-item ng-click="vm.update(vm.alarmDetailObj.name)">
        {{'edit' | translate}}
      </rb-dropdown-menu-item>
      <rb-dropdown-menu-item >
        {{'delete' | translate}}
      </rb-dropdown-menu-item>
      <rb-dropdown-menu-item ng-click="vm.create()">
        {{'permission_manage' | translate}}
      </rb-dropdown-menu-item>
    </rb-dropdown-menu>
 */
angular
  .module('app.components.common')
  .directive('rbDropdownMenuItem', rbDropdownMenuItem);

/**
 * Directive used as an menu item in rbDropdownMenu
 */
function rbDropdownMenuItem() {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    template: '<div class="item" ng-transclude></div>',
    scope: false,
    link,
  };

  function link(scope, element, attrs) {
    // Find parent element dropdown-menu
    const dropdownMenu = element.parents('rb-dropdown-menu');
    dropdownMenu.removeClass('hidden');

    // hide dropdownMenu when no sibling items
    scope.$on('$init', () => {
      setTimeout(() => {
        if (dropdownMenu.children('rb-dropdown-menu-item').length === 0) {
          dropdownMenu.addClass('hidden');
        }
      });
    });
    scope.$watch(
      () => {
        return scope.$eval(attrs['rbDisabled']);
      },
      newValue => {
        if (newValue === true) {
          element.addClass('disabled');
        } else {
          element.removeClass('disabled');
        }
      },
    );
  }
}

angular
  .module('app.components.common')
  .directive('rbDropdownMenu', rbDropdownMenu);

/**
 * A directive used to init the dropdown button group using semantic-dropdown
 * rb-title : used as the placeholder for the menu
 */
function rbDropdownMenu() {
  return {
    restrict: 'E',
    link,
    transclude: true,
    template: `
      <div class="ui dropdown button right">
        <span>Toggle</span>
        <i class="dropdown icon" style="margin-left:2px;margin-right:0;"></i>
        <div class="menu" ng-transclude>
        </div>
      </div>
    `,
    scope: false, // Uses the parent scope
  };

  //////////
  function link(scope, element, attrs) {
    element.find('span:first').text(attrs['rbTitle']);
    element.find('.ui.dropdown').dropdown({
      on: 'click',
      action: 'hide',
    });
  }
}
