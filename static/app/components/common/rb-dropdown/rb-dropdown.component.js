/**
 *  1. single selection dropdown: (options, key, value, init, onChange, search)
 *  dropdown component based on semantic-dropdown
 *  placeholder: used as placeholder when no option selected
 *  options: an array of items to init the dropdown list, elements can be primitive value or object
 *           (element of primitive values will be converted to object with 'key' and 'value' properties)
 *  key: property name to be display for dropdown option
 *  value: property name used as the unique index for each drodown option
 *  init: if given, will be used to determine the initial option of dropdown menu list by matching with the 'value' of each option
 *  onChange: callback funciton for dropdown option change, will be called with the entire option object
 *            object with 'key' and 'value' properties for 'options' of primitive values
 *  search: if set to 'true', will display an input for dropdown to filter options (not compatible with 'floating' mode)
 *  eg: <rb-dropdown
 *        size="lg"
 *        options="vm.regions"
 *        key="display_name" value="name"
 *        init="region_name"
 *        on-change="onRegionChange(option)"></rb-dropdown>
 *
 *  2. floating dropdown (floating="true", options, key, value, onChange)
 *  floating: if set to 'true', will only display an button with array icon
 *  eg:
 *  <div class="ui buttons">
 <div class="ui input">
 <input type="number" name="containerPort" ng-model="rule.containerPort" max="65535" min="1" required>
 </div>
 <rb-dropdown options="vm.requiredPorts" floating="true" on-change="vm.onContainerPortChange(option, rule)"></rb-dropdown>
 </div>
 *
 *  3. multiple selection dropdown (multiple="true", options, key, value, placeholder, onAdd, onRemove)
 *  multiple: if set to 'true', enable multiple selection for dropdown
 *  placeholder: used as placeholder when no option selected
 *  onAdd: callback function when adding a option, will be called with the entire option object
 *  onRemove: callback function when removing a option, will be called with the entire option object
 *  eg:
 *  <rb-dropdown
 *      multiple="true"
 *      foldable="true"
 *      placeholder="{{'select_notification' | translate}}"
 *      options="vm.notificationList"
 *      key="name" value="name"
 *      on-add="vm.onNotificationAdd(option)"
 *      on-remove="vm.onNotificationRemove(option)"></rb-dropdown>
 *
 *  4. common optoins:
 *  size: the width of the dropdown, available optoins are: xs, sm, md, lg (if not given, the dropdown will be 100% width)
 *
 *  5. Display the filter when option length greater than filterThreshold
 *  If show filter by default, then set filter-threshold="1"
 *  If hide filter by default, then set filter-threshold="0"
 */
const _ = require('lodash');
const templateStr = require('app/components/common/rb-dropdown/rb-dropdown.html');
const rbDropdown = {
  bindings: {
    size: '@', // supported size options: xs sm md lg
    // common options
    options: '<',
    key: '@',
    value: '@',
    htmlKey: '@',
    init: '<', // init value for dropdown (when options is ready or changed)
    onChange: '&',
    filterThreshold: '<',
    // floating add-on
    floating: '@',
    // multiple selection options
    multiple: '@',
    foldable: '<',
    placeholder: '@',
    onAdd: '&',
    onRemove: '&',
    isDisabled: '<',
    hasError: '<', // Error state,
    ignoreLastSelection: '@',
    allowEmptyDefault: '<',
  },
  controller: rbDropdownController,
  controllerAs: 'vm',
  template: templateStr,
};

angular.module('app.components.common').component('rbDropdown', rbDropdown);

function rbDropdownController(
  $window,
  $scope,
  $element,
  $timeout,
  translateService,
) {
  'ngInject';
  const vm = this;
  let initialized = false;
  let isPrimitiveOption = false;
  vm.currentValue = '';
  vm.getPlaceholderSetting = () => (vm.placeholder ? 'value' : false);
  const $dropdown = $element.find('.ui.dropdown');
  const settings = {
    placeholder: vm.getPlaceholderSetting(),
    allowReselection: vm.floating === 'true',
    on: vm.floating === 'true' ? 'hover' : 'click',
    forceSelection: false,
    fullTextSearch: 'exact',
    match: 'text',
    onShow: () => {
      if (!vm.multiple && !vm.floating && vm.selectedOption) {
        $dropdown.dropdown('set selected', vm.selectedOption[vm.value]);
      }
      $element.addClass('active');
      if (vm.foldable) {
        $element.find('.ui.label').addClass('visible');
      }
    },
    onHide: () => {
      // 这个下拉框隐藏时会做判断，尝试选中search输入的值
      const $dropdown = $element.find('.ui.dropdown');
      const searchText = $dropdown.find('input.search');
      // 必须判断是否有过滤值，不然用户有可能无法正确选择下拉列表
      if (searchText.val()) {
        $dropdown.dropdown('set selected', searchText.val()); // 如果输入的值能够匹配option则选中，没有匹配项也不存在副作用
        searchText.val(''); // 需要清空输入的值，否则multiple模式下search text会一直保留
        $dropdown.dropdown('set selected', $dropdown.dropdown('get value')); // 此操作是为了，在search text没有匹配值时，input里显示的值revert回当前选中的合法值
      }
      $element.removeClass('active');
      _overHidden();
    },
    onChange: value => {
      _.each(vm._options, option => {
        if (option[vm.value].toString() === value.toString()) {
          $scope.$evalAsync(() => {
            // avoid repeated onChange call, after _setDefaultSelection is called
            if (vm.selectedOption === option && !vm.floating) {
              return;
            }
            vm.selectedOption = option; // remember last selected option
            vm.onChange({ option: option });
          });
          return false;
        }
      });
    },
    message: {
      noResults: translateService.get('dropdown_no_results_found'),
    },
  };
  vm._options = [];
  vm.selectedOption = null;
  vm.isBasicStyle = !vm.floating && !vm.multiple; // basic style is the simple dropdown with a search style
  if (!vm.key || !vm.value) {
    vm.key = 'key';
    vm.value = 'value';
  }
  vm.filterThreshold = 1;
  // if (typeof vm.filterThreshold === 'undefined') {
  // vm.filterThreshold = vm.multiple ? 1 : DEFAULT_FILTER_THRESHOLD;  // multiple dropdown在options发生变化时，需要重新初始化
  // }
  vm.isEmptyOptions = () => _.isEmpty(vm.options);

  vm.$onChanges = changes => {
    // for ng-repeat with rb-dropdown, $onChanges will be invoked when remove an item (with different selected value) from ng-repeat items
    // set the default seleted option and display text when vm.init has value
    if (changes.init && changes.init.currentValue && initialized) {
      _setDefaultValueWithInit();
      if (vm.selectedOption && vm.selectedOption[vm.value] === vm.init) {
        $timeout(() =>
          $dropdown.dropdown('set selected', vm.selectedOption[vm.value]),
        );
      }
    }

    if (changes.isDisabled) {
      if (changes.isDisabled.currentValue) {
        $dropdown.addClass('disabled');
      } else {
        $dropdown.removeClass('disabled');
      }
    }

    if (changes.hasError) {
      if (changes.hasError.currentValue) {
        $dropdown.addClass('error');
      } else {
        $dropdown.removeClass('error');
      }
    }

    if (changes.placeholder) {
      vm._defaultNoOptionsPlaceholderText =
        vm.placeholder || translateService.get('currently_no_options');
      // refer to https://github.com/Semantic-Org/Semantic-UI/blob/master/dist/components/dropdown.js#L1955
      if (vm.placeholder) {
        // placeholder changes from one non-empty value to another non-empty value, update it
        $dropdown.dropdown(
          'set placeholderText',
          vm._defaultNoOptionsPlaceholderText,
        );
      } else {
        // disable placeholder
        settings.placeholder = vm.getPlaceholderSetting();
        $dropdown.dropdown(settings);
      }
    }
  };

  $scope.$watchCollection(
    () => {
      return vm.options;
    },
    (newValue, oldValue) => {
      // Should clear multiple selection whenever new dropdown options (even null)
      if (initialized && oldValue !== undefined && vm.multiple === 'true') {
        // Remove elements which are in the previous options list but not in the new list
        let differentValues;
        if (isPrimitiveOption) {
          differentValues = _.difference(oldValue, newValue);
        } else {
          differentValues = _.differenceBy(oldValue, newValue, vm.value).map(
            option => option[vm.value],
          );
        }
        const valuesToRemove = $dropdown
          .dropdown('get value')
          .split(',')
          .filter(val => differentValues.includes(val));
        $dropdown.dropdown('remove selected', valuesToRemove);
      }
      // if options is set empty (undefined/null/[]) manually, call onChange with option value = null
      if (!_.isEmpty(oldValue) && _.isEmpty(newValue)) {
        const option = {};
        option[vm.key] = null;
        option[vm.value] = null;
        vm.onChange({ option: option });
      }

      if (newValue) {
        const options = newValue;
        if (options.length === 0) {
          vm._options = [];
        } else {
          if (!_.isPlainObject(options[0])) {
            isPrimitiveOption = true;
            vm._options = _optionsPreProcess(options);
          } else {
            isPrimitiveOption = false;
            vm._options = options;
          }
        }
        if (!initialized) {
          $dropdown.addClass('search');
          $dropdown.dropdown(settings);
        }
        // Wait for dom compilation complete
        $timeout(() => {
          $dropdown.dropdown('refresh');
          initialized = true;
        });

        if (!vm.floating) {
          _setDefaultSelection();
        }
      }
    },
  );

  function _overHidden() {
    if (!vm.foldable || !vm.multiple) {
      return;
    }
    const $dropdown = $element.find('.ui.dropdown');
    const labels = [...$dropdown.find('.ui.label')];
    // 74: 因为在行尾没有隐藏的话会和显示折叠的数字重合一点，所以干脆把判定搞松一些
    const inputWidth =
      parseInt($window.getComputedStyle($dropdown[0], null).width) - 74;
    const count = { w: 0, n: -1 };

    while (count.w <= inputWidth && count.n < labels.length - 1) {
      count.n++;
      count.w += parseInt(
        $window.getComputedStyle(labels[count.n], null).width,
      );
    }

    if (count.w >= inputWidth) {
      labels.forEach((label, i) => {
        if (i >= count.n) {
          label.classList.remove('visible');
          label.classList.add('hidden');
        }
      });
      vm.foldCount = labels.length ? labels.length : null;
      vm.foldCount > 9 && (vm.foldCount = '9+');
    } else {
      vm.foldCount = null;
    }
  }

  function _setDefaultSelection() {
    if (!vm._options || !vm._options.length) {
      $dropdown.dropdown('restore placeholder text');
      // Does not provide calling the onChange back;
      return;
    }
    if (vm.multiple === 'true') {
      // set multiple selection init values
      if (!vm.init || !vm.init.length) {
        return;
      }
      $timeout(() => $dropdown.dropdown('set selected', vm.init));
      return;
    }

    // If options has empty value, we should also select it.
    if (vm.init || vm._options.some(option => !option[vm.value])) {
      // init the selected option according to init value
      _setDefaultValueWithInit();
    } else {
      // find the last selected option
      if (vm.ignoreLastSelection) {
        vm.selectedOption = vm._options[0];
      } else {
        _setDefaultValueWithLastSelected();
      }
    }
    vm.onChange({ option: vm.selectedOption });

    // if html supported, invoke 'set text' action with html str, set dropdown text to display html
    if (vm.htmlKey) {
      const selectedText = _.get(vm.selectedOption, vm.htmlKey, '');
      $dropdown.dropdown('set text', selectedText);
    } else {
      const selectedText = _.get(
        vm.selectedOption,
        vm.key,
        vm.placeholder || '',
      );
      $dropdown.dropdown('set text', `<span>${selectedText}</span>`); // 使dropdown.css中对文字阶段显示的样式生效
    }
    // set dropdown selected value
    $dropdown.dropdown('set selected', _.get(vm.selectedOption, vm.value, ''));
  }

  function _setDefaultValueWithInit() {
    let _exist = false;
    _.each(vm._options, option => {
      // 空值 == undefined
      if (
        (!vm.init && !option[vm.value]) ||
        (vm.init && option[vm.value].toString() === vm.init.toString())
      ) {
        vm.selectedOption = option;
        _exist = true;
        return false;
      }
    });
    if (!_exist) {
      vm.selectedOption = vm.allowEmptyDefault ? null : vm._options[0];
    }
  }

  function _setDefaultValueWithLastSelected() {
    let _exist = false;
    if (vm.selectedOption) {
      _.forEach(vm._options, item => {
        if (item[vm.value] === vm.selectedOption[vm.value]) {
          _exist = true;
          return false;
        }
      });
    }
    if (!_exist) {
      vm.selectedOption = _.find(vm._options, option => {
        return !option.op_disabled;
      });
    }
  }

  //convert value-only options (array that only contains primitive values) to object format
  //result array option will have a 'value' property
  //Note: onChange callback will be called with this converted object format
  function _optionsPreProcess(options) {
    const newOptions = [];
    _.each(options, option => {
      newOptions.push({
        key: option,
        value: option,
      });
    });
    return newOptions;
  }

  vm.$postLink = () => {
    // add customized dropdown size
    if (vm.size !== undefined) {
      $dropdown.removeClass('fluid');
      $element.css('width', 'auto'); // rb-dropdown element should have auto width when size is specified
    }
    // check if floating drodown (dropdown with only icon button)
    if (vm.floating === 'true') {
      $dropdown.addClass('floating icon button').removeClass('selection');
      $dropdown.find('> input').remove();
      $dropdown.find('> .text').remove();
    }
    // check if multiple dropdown
    if (vm.multiple === 'true') {
      $dropdown.addClass('multiple');
      settings.onAdd = value => {
        _.each(vm._options, option => {
          if (option[vm.value].toString() === value.toString()) {
            $scope.$evalAsync(() => {
              vm.onAdd({ option: option });
            });
            return false;
          }
        });
      };
      settings.onRemove = value => {
        _.each(vm._options, option => {
          if (option[vm.value].toString() === value.toString()) {
            $scope.$evalAsync(() => {
              vm.onRemove({ option: option });
            });
            return false;
          }
        });
      };
    }
    // init dropdown
    $dropdown.dropdown(settings);
  };
}

export { rbDropdown };
