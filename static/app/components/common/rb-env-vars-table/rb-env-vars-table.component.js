/**
 * Created by liudong on 16/9/26.
 */
const templateStr = require('app/components/common/rb-env-vars-table/rb-env-vars-table.component.html');
angular.module('app.components.common').component('rbEnvVarsTable', {
  bindings: {
    /**
     * Array of env files.
     * envFile:
     *   { name: string, uuid: string, content: Array<[string, string]> }
     */
    envFiles: '<',

    /**
     * Array of env vars, or key/value map of env vars
     * envVar:
     *   { name: string, value: string } | any
     */
    envVars: '<',
  },
  controller: rbEnvVarsTableController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbEnvVarsTableController($element) {
  const vm = this;

  // File name -> filter name
  vm.envFileFilters = {};
  vm.envVarsFilter = '';

  // Input may be a simple mapping, should be converted to an array
  vm.envVarList = [];

  vm.isEmpty = isEmpty;
  vm.envVarFilterFunction = envVarFilterFunction;
  vm.getEnvFileFilterFunction = getEnvFileFilterFunction;
  vm.$postLink = _postLink;
  vm.$onChanges = _onChanges;
  //////////
  function isEmpty() {
    return (
      (vm.envFiles ? vm.envFiles.length : 0) +
        (vm.envVars ? vm.envVars.length : 0) ===
      0
    );
  }

  function _onChanges({ envVars }) {
    if (envVars && envVars.currentValue) {
      if (vm.envVars instanceof Array) {
        vm.envVarList = vm.envVars.map(({ name, value }) => [name, value]);
      } else {
        vm.envVarList = Object.entries(vm.envVars);
      }

      // A special rule to filter out auto-generated env var:
      vm.envVarList = vm.envVarList.filter(
        ([key]) => key !== '__CUSTOM_DOMAIN_NAME__',
      );
    }
  }

  function envVarFilterFunction(row) {
    return !vm.envVarsFilter || row[0].includes(vm.envVarsFilter);
  }

  function getEnvFileFilterFunction(envfileName) {
    return row =>
      !vm.envFileFilters[envfileName] ||
      row[0].includes(vm.envFileFilters[envfileName]);
  }

  function _postLink() {
    $element.on('click', '.rb-detail-table-caption .fa', e => {
      $(e.target)
        .parent()
        .parent()
        .find('.rb-detail-table-wrap')
        .toggleClass('rb-collapse');
      $(e.target)
        .parent()
        .toggleClass('rb-collapse');
    });
  }
}
