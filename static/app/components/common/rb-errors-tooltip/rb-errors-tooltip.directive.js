import { debounce } from 'lodash';

import { BaseTooltipDirective } from 'app2/shared/directives/tooltip/base-tooltip.directive.ts';

angular
  .module('app.components.common')
  .directive('rbErrorsTooltip', rbErrorsTooltip);

let watchCounter = 0;

/**
 * This is the AngularJS versions of ErrorsTooltipDirective
 */
function rbErrorsTooltip(rb2DefaultErrorMapperService) {
  return {
    restrict: 'A',
    link,
    require: ['?ngModel', '?^form'],
  };

  ////////
  function getWatchReference(name) {
    return `$$_rubick_tooltip_watcher_${watchCounter}_${name}`;
  }

  function link(scope, element, attrs, [ngModel, form]) {
    watchCounter++;
    ngModel = scope.$eval(attrs.rbErrorsTooltip) || ngModel;

    // If form is provided, we will watch form errors instead.
    scope[getWatchReference('ngModel')] = ngModel;
    if (form) {
      scope[getWatchReference('form')] = form;
    }

    class ErrorsTooltip extends BaseTooltipDirective {
      constructor(nativeElement) {
        super(nativeElement);
        this.mode = 'error';
      }

      updateTooltipContent(tooltip) {
        const shouldShowTooltip =
          tooltip && (ngModel.$dirty || this.isFormSubmitted);
        // Init only on first showing
        if (shouldShowTooltip && !this.initialized) {
          const position = attrs.rbErrorsTooltipPosition || 'top right';
          this.init({
            inline: true,
            on: 'manual',
            position,
            // We use class rc-error-tooltip as a hint to do custom styling
            className: {
              popup: 'ui popup rc-tooltip error',
            },
            lastResort: position,
          });
        }
        if (shouldShowTooltip) {
          super.updateTooltipContent(tooltip);
          this.showTooltip();
        } else {
          this.hideTooltip();
        }
      }

      getErrorTooltip() {
        const mapper = scope.$eval(attrs.rbErrorsTooltipMapper);
        const errors = Object.entries(ngModel.$error || {});

        const mapFunction = (key, error) => {
          function getAttr(attr) {
            return $(ngModel.$$element).attr(attr);
          }

          // Since AngularJS does not provide the required length value,
          // we need to set it by ourself.
          switch (key) {
            case 'minlength':
              error = {
                requiredLength: getAttr('minlength') || getAttr('ng-minlength'),
              };
              break;
            case 'maxlength':
              error = {
                requiredLength: getAttr('maxlength') || getAttr('ng-maxlength'),
              };
              break;
            case 'min':
              error = { min: getAttr('min') || getAttr('ng-min') };
              break;
            case 'max':
              error = { max: getAttr('max') || getAttr('ng-max') };
              break;
          }

          // Mapper can be an object or a function
          if (!mapper) {
            return rb2DefaultErrorMapperService.map(key, error);
          } else if (typeof mapper.map === 'function') {
            return mapper.map(key, error, ngModel);
          } else if (mapper.hasOwnProperty(key)) {
            return mapper[key];
          } else {
            return rb2DefaultErrorMapperService.map(key, error);
          }
        };

        return errors
          .map(([key, error]) => mapFunction(key, error))
          .filter(tooltip => !!tooltip)
          .map(tooltip => `<span>${tooltip}</span>`)
          .join('<br>');
      }

      get isFormSubmitted() {
        return form && form.$submitted;
      }
    }

    const errorsTooltip = new ErrorsTooltip(element);

    function onErrorChange() {
      const tooltip = errorsTooltip.getErrorTooltip();
      errorsTooltip.updateTooltipContent(tooltip);
    }

    const debounceOnErrorChange = debounce(onErrorChange, 50);

    // Should toggle error tooltip whenever ngModel changes or form is submitted.
    const ngModelDeregistration = scope.$watch(
      getWatchReference('ngModel') + '.$error',
      () => {
        debounceOnErrorChange();
      },
      true,
    );
    const formDeregistration = scope.$watch(
      getWatchReference('form') + '.$submitted',
      () => {
        debounceOnErrorChange();
      },
    );

    scope.$on('$destroy', () => {
      delete scope[getWatchReference()];
      ngModelDeregistration();
      formDeregistration();
      errorsTooltip.destroy();
    });
  }
}
