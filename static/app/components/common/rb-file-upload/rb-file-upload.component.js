/**
 * Created by wsk on 12/5/16.
 */
const templateStr = require('app/components/common/rb-file-upload/rb-file-upload.html');
angular.module('app.components.common').component('rbFileUpload', {
  bindings: {
    acceptFileTypes: '<', // Regexp, such as /(\.|\/)(gif|jpe?g|png)$/i
    fileMaxNum: '<',
    onChange: '&', // file list change event handler
    onAdd: '&',
    onRemove: '&',
    invalidMessage: '@',
    form: '<', // the parent form
    required: '<', // is required field, default is false
    url: '@', // Must give the file upload url here to send a request which content type is multipart not application/json
    onSubmit: '&', // Must return the formData
    onSendProgress: '&',
    onSendEnd: '&',
    onSendSuccess: '&',
    onSendError: '&',
    // Todo: change the control mode without the flag
    submitFlag: '<', // used to set file upload submit
  },
  controller: rbFileUploadController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbFileUploadController($scope, $element, rbToast, translateService) {
  const vm = this;
  vm.fileList = [];
  vm.files = [];
  vm._curFileNums = 0;
  vm.deleteFile = deleteFile;
  /////
  vm.$onChanges = changeObj => {
    if (_.get(changeObj, 'submitFlag.currentValue')) {
      _submitFile();
    }
  };
  vm.$onInit = () => {
    vm.fileMaxNum = vm.fileMaxNum || 1;
  };
  vm.$postLink = () => {
    // Angular
    $element
      .find('.fileinput-button')
      .append(
        `<input id="fileupload" type="file" name="files" ${
          vm.multiple ? 'multiple' : ''
        }>`,
      );
    // The reason for didn't cache the input element is when 'fileuploadadd', the input dom will update by plugin
    $element
      .find('#fileupload')
      .fileupload({
        dataType: 'json',
        url: vm.url,
        autoUpload: false,
        // replaceFileInput: false,
        acceptFileTypes: vm.acceptFileTypes,
        //maxFileSize: 999000,
        formData: () => {
          if (!_.isFunction(vm.onSubmit)) {
            return [];
          } else {
            const formData = vm.onSubmit();
            if (_.isArray(formData) && formData[0].name && formData[0].value) {
              return formData;
            } else {
              return _.map(formData, (value, key) => ({
                name: key,
                value: value,
              }));
            }
          }
        },
      })
      .on('fileuploadprocessalways', (e, data) => {
        const file = data.files[0];
        vm.fileName = file.name;
        if (file.error) {
          if (file.error === 'File type not allowed') {
            vm.error = vm.invalidMessage || 'file_type_not_allowed';
          } else {
            vm.error = file.error;
          }
          return;
        } else {
          vm.error = '';
          addFile(file);
        }
      });
  };

  function addFile(file) {
    vm._curFileNums++;
    vm.files.push(file);
    vm.onAdd({ file });
    _updateFileList();
    if (vm._curFileNums >= vm.fileMaxNum) {
      _disableFileUpload();
    }
  }

  function deleteFile(file, index) {
    vm.files[index] = null;
    vm._curFileNums--;
    _updateFileList();
    vm.onRemove({ file });
    if (vm._curFileNums < vm.fileMaxNum) {
      _enableFileUpload();
    }
  }

  function _submitFile() {
    if (_.isFunction(vm.onSendProgress)) {
      vm.onSendProgress();
    }
    $element
      .find('#fileupload')
      .fileupload('send', {
        files: vm.fileList,
      })
      .success(jqXHR => {
        rbToast.success(
          translateService.get(_.get(jqXHR, 'responseJSON.status')),
        );
        if (_.isFunction(vm.onSendSuccess)) {
          vm.onSendSuccess();
        }
      })
      .error(jqXHR => {
        const result = jqXHR.responseJSON;
        rbToast.error(translateService.get(_.get(result, 'errors[0].message')));
        if (_.isFunction(vm.onSendError)) {
          vm.onSendError();
        }
      })
      .complete((/*result, textStatus, jqXHR*/) => {
        if (_.isFunction(vm.onSendEnd)) {
          vm.onSendEnd();
        }
      });
  }

  function _disableFileUpload() {
    vm.disabled = true;
    $element.find('#fileupload').prop('disabled', true);
  }

  function _enableFileUpload() {
    vm.disabled = false;
    $element.find('#fileupload').prop('disabled', false);
  }

  function _updateFileList() {
    vm.fileList = _.compact(vm.files);
    vm.fileName = _.get(vm.fileList[vm.fileList.length - 1], 'name');
    if (_.isFunction(vm.onChange)) {
      vm.onChange({ files: vm.fileList });
    }
  }
}
