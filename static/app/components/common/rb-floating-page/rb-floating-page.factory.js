angular
  .module('app.components.common')
  .factory('rbFloatingPage', rbFloatingPageFactory);

/**
 * Like modal, but has a fixed size that will cover the main routing
 * view (ui-view), to show a page-sized component in an isolated
 * modal without routing away.
 *
 * A typical usage of this factory is to edit an associated
 * resource of another resource.
 */
function rbFloatingPageFactory($rootScope, $compile) {
  const $floatingPageContainer = $('.floating-page-container');
  const $floatingPageContainerMask = $('.floating-page-container-mask');
  const $floatingPageCloseIcon = $floatingPageContainer.find('.fa-remove');
  const $titleAnchor = $('.floating-page-title', $floatingPageContainer);
  const $contentAnchor = $('.floating-page-content', $floatingPageContainer);
  const ANIMATION_TIMEOUT = 500;

  let _componentElement = null;
  let _isolateScope = null;

  // Defer functions
  let _resolver = null;
  let _reject = null;

  return {
    // Provides settings of the component to show, and returns a
    // Promise on complete
    show,
    hide, // Resolve the global floating page
    cancel, // Reject the global floating page
  };
  //////////
  function show({
    title, // title to be shown on the header
    locals = {}, // values to be passed to the components
    component, // component string, can either be `rbSomeComponent` or `rb-some-component`
  }) {
    const waitForRelease = _isShowing();
    $titleAnchor.text(title);

    // Release previous component
    if (waitForRelease) {
      _release(false);
    }

    // Prepare component and append to DOM
    _isolateScope = $rootScope.$new(true);
    _.assign(_isolateScope, locals);
    _componentElement = $compile(_generateComponentTemplate(component, locals))(
      _isolateScope,
    );

    $floatingPageContainer.css('display', 'block');
    $floatingPageContainerMask.css('display', 'block');

    setTimeout(() => {
      $floatingPageContainer.addClass('active');
      $floatingPageCloseIcon.bind('click', this.cancel);
      $contentAnchor.append(_componentElement);
    }, waitForRelease ? ANIMATION_TIMEOUT : 300);

    return new Promise((res, rej) => {
      _resolver = res;
      _reject = rej;
    }).catch(() => {});
  }

  function cancel() {
    _reject && _reject();
    _release();
  }

  function hide(res) {
    _resolver && _resolver(res);
    _release();
  }

  function _release(displayNone = true) {
    $floatingPageContainer.removeClass('active');
    $floatingPageCloseIcon.unbind('click', this.cancel);
    _componentElement && _componentElement.remove();
    _isolateScope && _isolateScope.$destroy();
    _reject = null;
    _resolver = null;

    displayNone &&
      setTimeout(() => {
        $floatingPageContainer.css('display', 'none');
        $floatingPageContainerMask.css('display', 'none');
      }, ANIMATION_TIMEOUT);
  }

  // e.g.:
  // _generateComponentTemplate('asdAsdA', {a: 123, b: 123, cAsdS: 456})
  //   => "<asd-asd-a a="a" b="b" c-asd-s="cAsdS"></asd-asd-a>"
  function _generateComponentTemplate(component, locals) {
    const componentTag = _.kebabCase(component);
    const attrs = Object.keys(locals)
      .map(attrName => `${_.kebabCase(attrName)}="${attrName}"`)
      .join(' ');
    return `<${componentTag} ${attrs}></${componentTag}>`;
  }

  function _isShowing() {
    return $floatingPageContainer.hasClass('active');
  }
}
