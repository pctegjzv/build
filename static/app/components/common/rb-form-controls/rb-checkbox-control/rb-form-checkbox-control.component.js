const templateStr = require('app/components/common/rb-form-controls/rb-checkbox-control/rb-form-checkbox-control.html');
angular.module('app.components.pages').component('rbFormCheckboxControl', {
  controller: rbFormCheckboxControlController,
  bindings: {
    label: '<',
    name: '<',
    value: '<',
    description: '<',
  },
  controllerAs: 'vm',
  template: templateStr,
});

function rbFormCheckboxControlController(rbFormDataService) {
  const vm = this;
  vm.$onInit = _init;
  vm.handleInputChange = handleInputChange;
  ///
  function _init() {
    if (vm.value === 'true' || vm.value === true) {
      vm.value = true;
    } else {
      vm.value = false;
    }
    rbFormDataService.setFormField(vm.name, vm.value);
  }
  function handleInputChange() {
    rbFormDataService.setFormField(vm.name, vm.value);
  }
}
