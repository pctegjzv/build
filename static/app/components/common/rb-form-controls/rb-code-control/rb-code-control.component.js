const templateStr = require('app/components/common/rb-form-controls/rb-code-control/rb-code-control.component.html');
angular.module('app.components.pages').component('rbCodeControl', {
  controller: rbCodeControlController,
  bindings: {
    form: '<',
    label: '<',
    name: '<',
    pattern: '<',
    required: '<',
    patternHint: '<',
    description: '<',
    codeType: '<',
  },
  controllerAs: 'vm',
  template: templateStr,
});

function rbCodeControlController(rbFormDataService, rbGlobalSetting) {
  const vm = this;
  vm.$onInit = _init;

  function _init() {
    vm.editorOptions = rbGlobalSetting.getEditorOptions('yaml');
    rbFormDataService.setFormField(vm.name, () => vm.code);
  }
}
