/**
 * Created by wsk on 12/26/16.
 */

// import FormStore from './rb-form-store';

// const store = new FormStore(); // Storing
export const COMPONENT_MAP = [
  {
    name: 'div',
    types: ['interval'],
  },
  {
    name: 'div',
    types: ['header'],
  },
  {
    name: 'rb-form-input-control',
    types: ['int', 'string', 'number', 'text'],
  },
  {
    name: 'rb-form-instance-size-control',
    types: ['cluster_size'],
  },
  {
    name: 'rb-form-checkbox-control',
    types: ['bool'],
  },
  {
    name: 'rb-form-select-control',
    types: ['option', 'multi_option', 'single_ip_tag', 'multi_ip_tag'],
  },
  {
    name: 'rb-form-radio-control',
    types: ['radio'],
  },
  {
    name: 'rb-form-radio-group-tab-control',
    types: ['radio_group_tab'],
  },
  {
    name: 'rb-table-input-control',
    types: ['table_input'],
  },
  {
    name: 'rb-tags-input-control',
    types: ['tags_input'],
  },
  {
    name: 'rb-code-control',
    types: ['code'],
  },
  {
    name: 'rb-mount-points-control',
    types: ['mount_points'],
  },
];

export const EQUIVALENT_TYPE = {
  int: 'number',
  string: 'text',
  single_ip_tag: 'option',
  multi_ip_tag: 'multi_option',
  multi_option: 'multi_option',
};
export const TAB_TYPES_ARR = ['radio_group_tab'];
