import {
  COMPONENT_MAP,
  EQUIVALENT_TYPE,
  TAB_TYPES_ARR,
} from '../rb-form-constant';

angular.module('app.components.pages').component('rbFormControl', {
  controller: rbFormControlController,
  bindings: {
    prefix: '@',
    config: '<',
    form: '<',
    configMap: '<',
  },
  controllerAs: 'vm',
  template: '<div bind-html-compile="vm.componentTmpl"></div>',
});

function rbFormControlController(
  $injector,
  translateService,
  rbFormDataService,
  rbFormDisplayService,
  rbFormUtilities,
) {
  const vm = this;
  let CONFIG_SEQUENCE_ARR = [];
  const _inStackKey = [''];
  const _inStackParentIndex = [0];
  let _stackDepth = 0;
  vm.componentAttrData = [];
  vm.tabComponentsInfo = {};
  vm.components = [];
  vm.componentsParentKey = [];

  vm.$onChanges = changeObj => {
    if (changeObj.config) {
      CONFIG_SEQUENCE_ARR = _.get(vm.configMap, 'CONFIG_SEQUENCE_ARR', []);
      const config = changeObj.config.currentValue;
      if (!config) {
        return;
      }
      rbFormDataService.resetForm();
      _resetDisplayStatus();
      _registerDisplayStatus();
      vm.componentsInfo = _generateComponentsInfo(config);
      vm.componentTmpl = _generateTmpls();
    }
  };

  /**
   * transform config to componentInfp
   * @param {Object} config
   * e.g
   * {
   *    'a_config': {
   *      'attribute_name': 'node_num',
   *      'display_name': 'Node number',
   *      'type': 'int',
   *      'max_value': '123',
   *      'min_value': '234',
   *      ...
   *    },
   *    'b_config': {
   *    ...
   *    }
   * }
   * @return {Object} componentInfo
   * e.g
   * {
   *    {
   *      type: 'header',
   *      child: 'Title1',
   *      class: 'style1-class style2-class'
   *    },
   *    {
   *      type: 'int',
   *      attr: {
   *        name: 'node_num',
   *        label: 'Node number',
   *        max: 'name1',
   *        type: 'int'
   *        description: undefined,
   *        key: undefined,
   *        options: undefined,
   *        ....
   *      }
   *    },{
   *      type: 'text',
   *      attr: {
   *        name: 'app_name',
   *        label: 'App name',
   *        description: 'xxxxx',
   *        ....
   *      }
   *    },
   *    {
   *      type: 'interval',
   *      class: 'the-interval-style-class'
   *    }
   * }
   */
  function _generateComponentsInfo(config) {
    const componentsInfo = {};
    _.forEach(config, (content, type) => {
      const componentInfo = [];

      _.forEach(content, item => {
        componentInfo.push({
          type: item.type,
          attrs: {
            form: vm.form,
            name: item.attribute_name,
            description: item.description,
            pattern: item.pattern,
            // API returns pattern hint as pattern_hint
            'pattern-hint': item['pattern-hint'] || item['pattern_hint'],
            type: item.type,
            label: item.display_name,
            multiple: EQUIVALENT_TYPE[item.type] === 'multi_option',
            value: item.default_value || item.value,
            key: item.key,
            unit: item.unit,
            required: false === item.required ? false : true,
            'field-keys': item['field-keys'],
            'null-placeholder': item['null-placeholder'],
            'select-placeholder': item['select-placeholder'],
            options: _.isFunction(item.option) ? [] : item.option,
            'options-control': _.isFunction(item.option)
              ? _.isArray(item.option())
                ? $injector.invoke(item.option())
                : item.option
              : undefined,
            'on-change': _.isFunction(item.onChange)
              ? item.onChange
              : _.isArray(item.onChange)
                ? $injector.invoke(item.onChange)
                : undefined,
            'on-add': _.isFunction(item.onAdd)
              ? item.onAdd
              : _.isArray(item.onAdd)
                ? $injector.invoke(item.onAdd)
                : undefined,
            'on-remove': _.isFunction(item.onRemove)
              ? item.onRemove
              : _.isArray(item.onRemove)
                ? $injector.invoke(item.onRemove)
                : undefined,
            min: item.min_value,
            max: item.max_value,
            minlength: item.minlength,
            maxlength: item.maxlength,
            prefix: vm.prefix,
            status: true,
          },
        });
      });
      if (_hasHeaderOrFooter(type)) {
        componentInfo.unshift({
          type: 'header',
          child: type,
          class: 'rb-header-alt',
        });
        if (_hasFooter(type)) {
          componentInfo.push({
            type: 'interval',
            class: 'rb-form-fieldset-interval',
          });
        }
      }
      componentsInfo[type] = componentInfo;
    });
    return componentsInfo;
  }
  function _generateTmpls() {
    if (!CONFIG_SEQUENCE_ARR.length) {
      CONFIG_SEQUENCE_ARR = Object.keys(vm.componentsInfo);
    }
    const entryConfig = CONFIG_SEQUENCE_ARR;
    traverseConfigs(entryConfig);
    return vm.components.join('');
  }

  function _createTmpl(component, index, isTab) {
    let componentTmpl = '';
    for (let i = 0; i < COMPONENT_MAP.length; i++) {
      const elem = COMPONENT_MAP[i];
      const name = elem.name.match(/([-\w]+)(@*)/)[0];
      const status = isTab ? `ng-show="vm.isShown[${index}]"` : '';
      let directives = elem.name.match(/(@+)([\W\w]+)/);
      directives = _.isArray(directives) ? directives[0].replace('@', ' ') : '';

      if (elem.types.includes(_.get(component, 'type'))) {
        vm.componentAttrData[index] = component.attrs;
        _.each(component.attrs, (value, attr) => {
          if (!value) {
            return;
          }
          componentTmpl += ` ${attr}="vm.componentAttrData[${index}]['${attr}']"`;
        });
        if (component.class) {
          componentTmpl += ` class="${component.class}"`;
        }
        componentTmpl = `<${name} ${status} ${componentTmpl} ${directives}>${translateService.get(
          component.child,
        ) || ''}</${name}>`;
        break;
      }
    }
    return componentTmpl;
  }

  function traverseConfigs(entryKeys, isSubConf) {
    _stackDepth++;
    _inStackKey.push('');
    _inStackParentIndex.push(0);
    entryKeys.forEach(key => {
      if (isSubConf && !vm.componentsInfo[key]) {
        return;
      }
      _inStackKey[_inStackKey.length - 1] = key;
      _inStackParentIndex[_inStackParentIndex.length - 1] =
        vm.components.length - 1;
      traverseComponentInfo(vm.componentsInfo[key], isSubConf);
    });
    _inStackKey.pop();
    _inStackParentIndex.pop();
    _stackDepth--;
  }
  function traverseComponentInfo(confArr, isSubConf) {
    if (!confArr) {
      return;
    }
    confArr.forEach(elem => {
      doPush(
        elem,
        isSubConf,
        _inStackKey[_stackDepth - 1],
        _inStackParentIndex[_stackDepth - 1],
      );
      if (isTabComponent(elem)) {
        const subConfigKeys = getSubConf(elem);
        traverseConfigs(subConfigKeys, true);
      }
    });
  }

  function doPush(elem, isSubConf, parentKey, parentIndex) {
    const curIndex = vm.components.length - 1;
    if (isSubConf) {
      rbFormDisplayService.initComponentDisplay(
        _inStackKey[_stackDepth],
        curIndex,
        CONFIG_SEQUENCE_ARR.includes(parentKey) ? '' : parentKey,
        parentIndex,
      );
    }
    vm.components.push(_createTmpl(elem, curIndex, isSubConf));
  }

  /////////// utilities
  function _registerDisplayStatus() {
    rbFormDisplayService.registerDisplayStatusHandler((index, newStatus) => {
      vm.isShown[index] = newStatus;
      // make the required field optional, prevent hidden form fields being validated
      if (vm.componentAttrData[index]) {
        vm.componentAttrData[index]['required'] = newStatus;
        vm.componentAttrData[index]['status'] = newStatus;
      }
    });
  }

  function _resetDisplayStatus() {
    vm.isShown = {};
    rbFormDisplayService.resetComponentDisplayStatus();
  }

  function isTabComponent(item) {
    return TAB_TYPES_ARR.includes(item.type);
  }

  function getSubConf(item) {
    const keys = _.get(item, 'attrs.options');
    return _.map(keys, key => rbFormUtilities.getConfigKey(key));
  }

  function _hasHeaderOrFooter(type) {
    if (CONFIG_SEQUENCE_ARR.length) {
      return CONFIG_SEQUENCE_ARR.includes(type);
    } else {
      return true;
    }
  }

  function _hasFooter(type) {
    return CONFIG_SEQUENCE_ARR.indexOf(type) !== CONFIG_SEQUENCE_ARR.length - 1;
  }
}
