import _ from 'lodash';
import FormStore from './rb-form-store';

import { TAB_TYPES_ARR } from './rb-form-constant';

angular
  .module('app.components.pages')
  .factory('rbFormDataService', rbFormDataServiceFactory);

function rbFormDataServiceFactory(translateService, rbFormUtilities) {
  let _confirmTemplate = {}; // generated during _normalizeConfig phase
  let _formStore = new FormStore();
  let _curConfigName = '';
  return {
    // config name
    getCurConfName,
    // form functions
    initForm,
    resetForm,
    clearForm,
    getFormField,
    setFormField,
    // config functions
    getConfigFormData,
    normalizeConfig,
    // modify confirmTemplate function
    amendConfirmFieldTemplateByAttrName: _amendConfirmFieldTemplateByAttrName,
  };

  // config name
  function getCurConfName() {
    return _curConfigName;
  }

  //////////////////// form functions
  function initForm(curConfigName) {
    _curConfigName = curConfigName;
    resetForm();
  }

  function resetForm() {
    _formStore = new FormStore();
  }

  function clearForm() {
    _formStore.clearStore();
  }

  function setFormField(key, value) {
    _formStore.setFieldStore(key, value);
  }

  function getFormField(key, func) {
    const value = _formStore.getFieldStore(key);
    if (!func) {
      if (value || value === false) {
        if (_.isFunction(value)) {
          return value();
        } else {
          return value;
        }
      } else {
        return '';
      }
    } else {
      _formStore.onFieldStoreChange(key, func);
    }
  }

  function _groupingData(data, GROUP_MAP) {
    const configs = {};
    if (_.isArray(data) && GROUP_MAP) {
      _.forEach(data, item => {
        _.forEach(GROUP_MAP, (config, configName) => {
          if (config.includes(item.attribute_name)) {
            const configKey = `${configName}_config`;
            configs[configKey] = configs[configKey] || [];
            configs[configKey].push(item);
          }
        });
      });
      return configs;
    }
  }

  ////////// config functions
  function normalizeConfig(
    data,
    {
      _curConfName = '', //
      FILLING_NAME_LIST = null,
      FILLING_CONFIGS = null,
      NAME_TO_TYPE_LIST = null,
      GROUP_MAP = null,
    },
  ) {
    const detail = data;
    initForm(_curConfName);
    data = _groupingData(data, GROUP_MAP) || data;
    let config = _.cloneDeep(data);
    config = _.each(config, (item, key) => {
      if (key.includes('config') && _.get(item, 'length')) {
        _.each(item, field => {
          if (
            NAME_TO_TYPE_LIST &&
            NAME_TO_TYPE_LIST.includes(field['attribute_name'])
          ) {
            field.type = field['attribute_name'];
          }
        });
      } else {
        delete config[key];
      }
    });
    _confirmTemplate = _.cloneDeep(config);
    _.each(FILLING_NAME_LIST, (item, key) => {
      // Get trans config key
      let fillKey = '';
      if (item.defaultInclude) {
        if (!(_.get(item, 'list.excludes') || []).includes(_curConfigName)) {
          fillKey = key;
        }
      } else {
        if ((_.get(item, 'list.includes') || []).includes(_curConfigName)) {
          fillKey = key;
        }
      }

      if (fillKey) {
        // Fill correspond config, inner's loop according to the structure of FILLING_CONFIGSS
        _.each(FILLING_CONFIGS[fillKey], (content, configType) => {
          if (!config[configType] && configType !== '*') {
            // For example region_config, fill the whole config
            config[configType] = content;
            _.each(content, extraField => {
              _confirmTemplate[extraField['attribute_name']] =
                extraField.required === false ? 'optional' : 'required';
            });
          } else {
            // For a specific filling_config: for example advance_config: [{},...]. For filling specify field. It's better no such case
            _.each(content, field => {
              // For a specific field of filling config
              if (_.isEmpty(field)) {
                return;
              }
              if (field['before_filling'] && !field['before_filling'](data)) {
                return;
              }
              // Find which field need to fill
              _.forEach(config, (conf, type) => {
                if ('*' !== configType && type !== configType) {
                  return;
                }
                for (let i = 0; i < config[type].length; i++) {
                  // The field fill by attribute_name, Find the field has same attribute_name
                  // Don't break the for loop because it may has multiple config[type] (eg. 'single_ip_tag') items match the field
                  if (
                    field['attribute_name'] ===
                    config[type][i]['attribute_name']
                  ) {
                    // Merge the lack attribute
                    config[type][i] = _.merge(config[type][i], field);
                  }
                  if (
                    _.isRegExp(field['attribute_name']) &&
                    field['attribute_name'].test(
                      config[type][i]['attribute_name'],
                    )
                  ) {
                    // Merge the lack attribute
                    const matchField = _.cloneDeep(field);
                    delete matchField['attribute_name'];
                    config[type][i] = _.merge(config[type][i], matchField);
                  }
                  // The field fill by type, Find the field has same type
                  if (
                    (_.isArray(field['type']) &&
                      field['type'].includes(config[type][i]['type'])) ||
                    (_.isArray(field['attribute_name']) &&
                      field['attribute_name'].includes(
                        config[type][i]['attribute_name'],
                      ))
                  ) {
                    const _fieldCopy = _.cloneDeep(field);
                    delete _fieldCopy.type;
                    delete _fieldCopy['attribute_name'];
                    config[type][i] = _.merge(config[type][i], _fieldCopy);
                  }
                }
                if (field['isExtra']) {
                  config[type].unshift(field);
                  _confirmTemplate[field['attribute_name']] = 'required';
                }
              });
            });
          }
        });
      }
    });
    return { detail, config };
  }

  function getConfigFormData(CONFIG_SEQUENCE_ARR) {
    const realFormDatas = {};
    const curConfirmTemplate = _.cloneDeep(_confirmTemplate);
    const tabConfigMap = {};
    _.forIn(curConfirmTemplate, (config, key) => {
      // For the field in the xx_config pass by backend
      if (_.isArray(config)) {
        realFormDatas[key] = {};
        _.forEach(config, field => {
          if (TAB_TYPES_ARR.includes(field.type)) {
            tabConfigMap[
              rbFormUtilities.getConfigKey(
                getFormField(field['attribute_name']),
              )
            ] = key;
          }
          if (getFormField(field['attribute_name'])) {
            // For example: realFormDatas['advance_data']['node_num'] = ngFormDatas['node_num']
            realFormDatas[key][field['attribute_name']] = getFormField(
              field['attribute_name'],
            );
          }
        });
      } else if (typeof config === 'string') {
        if (getFormField(key)) {
          // For the field add by frontend
          realFormDatas[key] = getFormField(key);
        }
      }
    });

    let depth = _getSubConfMaxDepth(tabConfigMap, CONFIG_SEQUENCE_ARR);
    // Recursive Merge fields in others xxx_config to its parent config
    while (depth--) {
      _.forEach(realFormDatas, (configData, configKey) => {
        if (configKey.includes('config')) {
          _.merge(realFormDatas[tabConfigMap[configKey]], configData);
        }
      });
    }
    _.forEach(realFormDatas, (configData, configKey) => {
      if (
        configKey.includes('config') &&
        !CONFIG_SEQUENCE_ARR.includes(configKey)
      ) {
        delete realFormDatas[configKey];
      }
    });
    return {
      res: realFormDatas,
    };
  }

  function _amendConfirmFieldTemplateByAttrName(amendCb, attrName) {
    Object.keys(_confirmTemplate).forEach(config => {
      if (config.includes('config')) {
        const configArr = _confirmTemplate[config];
        configArr.forEach(field => {
          if (field.attribute_name === attrName) {
            amendCb(field);
          }
        });
      }
    });
  }

  function _getSubConfMaxDepth(parentMap, firstSeq) {
    let nextSeq = [];
    let curSeq = firstSeq;
    let depth = 0;
    while (curSeq.length) {
      depth++;
      for (const key in parentMap) {
        if (curSeq.includes(parentMap[key])) {
          nextSeq.push(key);
        }
      }
      curSeq = nextSeq;
      nextSeq = [];
    }
    if (depth) {
      depth++;
    }
    return depth;
  }
}
