/**
 * Created by wsk on 12/1/16.
 */
import _ from 'lodash';

angular
  .module('app.components.pages')
  .factory('rbFormDisplayService', rbFormDisplayServiceFactory);

function rbFormDisplayServiceFactory() {
  let _configDisplayStatus = {};
  let _onChangeStatus = null;
  let _configParentKeyIndex = {};
  let _childrenList = [];
  let _restoreEdgeCache = [];
  return {
    resetComponentDisplayStatus,
    initComponentDisplay,
    registerDisplayStatusHandler,
    updateComponentDisplayStatus,
    getMap,
  };

  function resetComponentDisplayStatus() {
    _configDisplayStatus = {};
    _configParentKeyIndex = {};
    _childrenList = [];
    _restoreEdgeCache = [];
  }

  function registerDisplayStatusHandler(cb) {
    _onChangeStatus = cb;
  }

  function initComponentDisplay(configKey, index, parentKey, parentIndex) {
    _configDisplayStatus[configKey] = _configDisplayStatus[configKey] || {};
    _configParentKeyIndex[configKey] = _configParentKeyIndex[configKey] || {};
    _configDisplayStatus[configKey][index] = false;
    _configParentKeyIndex[configKey][index] = { parentKey, parentIndex };

    const parentHashStr = parentKey + ' ' + parentIndex;
    const curHashStr = configKey + ' ' + index;
    const edge = [curHashStr, parentHashStr];
    _childrenList.push(edge);
    const _newEdges = [];
    _childrenList.forEach(preEdge => {
      // is connected: pre edge tail is cur head
      if (preEdge[0] === parentHashStr) {
        const newEdge = _.clone(preEdge);
        newEdge.unshift(curHashStr);
        _newEdges.push(newEdge);
      }
    });
    _childrenList = _childrenList.concat(_newEdges);
    _onChangeStatus(index, _configDisplayStatus[configKey][index]);
  }

  function updateComponentDisplayStatus(configKey, newStatus) {
    configKey = configKey.trim();

    _.forEach(_configDisplayStatus[configKey], (oldStatus, index) => {
      const { parentKey, parentIndex } = _configParentKeyIndex[configKey][
        index
      ];
      // if parent hide then it hide, and store the edge in cache.
      const curHashStr = configKey + ' ' + index;

      if (parentKey && parentIndex) {
        const parentStatus = _configDisplayStatus[parentKey][parentIndex];
        // if its parent hide, it(it is the parent default option) should hide and will be show when parent show
        if (parentStatus === false && newStatus === true) {
          const parentHashStr = parentKey + ' ' + parentIndex;
          const edge = [curHashStr, parentHashStr];
          // add to cache for recover
          _restoreEdgeCache.push(edge);
          newStatus = false;
        }
      }
      if (newStatus === false) {
        // if it should hide, then hide all its children
        _childrenList.forEach(edge => {
          // it has a child
          if (edge[edge.length - 1] === curHashStr) {
            const [childKey, childIndex] = edge[0].split(' ');
            if (_configDisplayStatus[childKey][childIndex] === true) {
              _restoreEdgeCache.push([edge[0], edge[edge.length - 1]]);
              _changeKeyIndexStatus(childKey, childIndex, false);
            }
          }
        });
      } else if (newStatus === true) {
        // if it should show, then show all the cache children edge (the default tab config fields)
        _recoverChildren(curHashStr);
      }
      _changeKeyIndexStatus(configKey, index, newStatus);
    });
  }

  function _changeKeyIndexStatus(configKey, index, newStatus) {
    _configDisplayStatus[configKey][index] = newStatus;
    _onChangeStatus(index, _configDisplayStatus[configKey][index]);
  }

  function _recoverChildren(curHashStr) {
    _.forEach(_restoreEdgeCache, (edge, index) => {
      if (edge && edge[1] === curHashStr) {
        const [childKey, childIndex] = edge[0].split(' ');
        const childHashStr = edge[0];
        if (_.map(_restoreEdgeCache, item => item[0]).includes(childHashStr)) {
          _recoverChildren(childHashStr);
        }
        _changeKeyIndexStatus(childKey, childIndex, true);
        _restoreEdgeCache.splice(index, 1);
      }
    });
  }
  function getMap() {
    // just for debug
    return (
      JSON.stringify(_configParentKeyIndex) + JSON.stringify(_childrenList)
    );
  }
}
