/**
 * Created by wsk on 12/26/16.
 */
import _ from 'lodash';

export default class FormStore {
  constructor(store = {}, cbList = {}) {
    this.callbackList = cbList;
    this.store = store;
  }

  clearStore() {
    _.forEach(this.getStore(), (store, key) => {
      this.setFieldStore(key, undefined);
    });
  }

  setFieldStore(key, value) {
    this.store[key] = value;
    _.forEach(this.callbackList[key], callback => {
      callback(value);
    });
  }

  clearFieldStore(key) {
    if (_.isArray(this.store[key])) {
      this.setFieldStore(key, []);
    } else if (this.store[key]) {
      this.setFieldStore(key, null);
    }
  }

  getFieldStore(key, defaultVal) {
    return this.store[key] || defaultVal;
  }

  onFieldStoreChange(_key, callback) {
    const keys = _.isArray(_key) ? _key : [_key];
    keys.forEach(key => {
      this.callbackList[key] = this.callbackList[key] || [];
      this.callbackList[key].push(callback);
    });
  }

  removeFieldStoreHandler(key) {
    this.callbackList[key] = null;
  }

  createSingleHandler(key, callback) {
    this.callbackList[key] = [callback];
  }

  getStore() {
    return this.store;
  }

  getCallbackList() {
    return this.callbackList;
  }
}
