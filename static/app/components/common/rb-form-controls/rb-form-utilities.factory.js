/**
 * Created by wsk on 12/1/16.
 */
angular
  .module('app.components.utility')
  .factory('rbFormUtilities', rbFormUtilitiesFactory);

function rbFormUtilitiesFactory(translateService) {
  const curlangIdx = +translateService.getLanguage().includes('zh');
  const LANG_SWITCH = ['en', 'zh'];
  const curlang = LANG_SWITCH[curlangIdx];
  const oplang = LANG_SWITCH[(curlangIdx + 1) % 2];

  function getConfigKey(key) {
    return key
      .split(' ')
      .concat('config')
      .join('_');
  }

  function getLanguage(data) {
    _.forEach(data, (value, key) => {
      if (_.isObject(value)) {
        if (_.isString(value['en']) || _.isString(value['zh'])) {
          data[key] = value[curlang] || value[oplang];
        } else {
          data[key] = getLanguage(value);
        }
      }
    });
    return data;
  }
  return {
    getConfigKey,
    getLanguage,
  };
}
