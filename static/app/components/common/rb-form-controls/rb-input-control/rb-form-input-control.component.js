import { EQUIVALENT_TYPE } from '../rb-form-constant';
import { VALIDATE_RULES } from './rb-form-input-pattern.constant';
const templateStr = require('app/components/common/rb-form-controls/rb-input-control/rb-form-input-control.html');
angular.module('app.components.pages').component('rbFormInputControl', {
  controller: rbInputControlController,
  bindings: {
    form: '<',
    label: '<',
    name: '<',
    value: '<',
    unit: '<',
    type: '<',
    max: '<',
    min: '<',
    maxlength: '<',
    minlength: '<',
    pattern: '<',
    required: '<',
    patternHint: '<',
    description: '<',
    status: '<',
  },
  controllerAs: 'vm',
  template: templateStr,
});

function rbInputControlController(
  $scope,
  rbFormDataService,
  rb2DefaultErrorMapperService,
  translateService,
) {
  const vm = this;
  vm.ngType = '';
  vm.patternHints = [];
  vm.patterns = [];
  vm.$onInit = _init;
  vm.$onChanges = _onChanges;
  vm.handleInputChange = handleInputChange;

  function _init() {
    vm.errorMapper = {
      map: (key, error, control) => {
        if (key === 'pattern') {
          return vm.currentHint;
        } else {
          return rb2DefaultErrorMapperService.map(key, error, control);
        }
      },
    };
    vm.ngType = EQUIVALENT_TYPE[vm.type] || vm.type;
    _fillPatterns();
    vm.validationPipe = validationPipe();
    if (vm.value) {
      _normalizeValue();
      vm._model = vm.value;
      rbFormDataService.setFormField(vm.name, vm._model);
    }

    // String indicates vm.max is a expression string instead of a scalar value
    if (vm.max && typeof vm.max === 'string') {
      $scope.$watch(vm.max, newValue => {
        vm._max = newValue;
      });
    } else {
      vm._max = vm.max;
    }

    // String indicates vm.max is a expression string instead of a scalar value
    if (vm.min && typeof vm.min === 'string') {
      $scope.$watch(vm.min, newValue => {
        vm._min = newValue;
      });
    } else {
      vm._min = vm.min;
    }
  }

  function _syncValue() {
    const savedValue = rbFormDataService.getFormField(vm.name);
    if (savedValue) {
      vm._model = savedValue;
    }
  }

  function _onChanges(changeObj) {
    if (changeObj.status && changeObj.status.currentValue) {
      // refresh the field
      _syncValue();
    }
  }

  function _normalizeValue() {
    switch (vm.ngType) {
      case 'text':
        vm.value = String(vm.value);
        break;
      case 'number':
        vm.value = Number(vm.value);
        break;
    }
  }

  function _fillPatterns() {
    if (_.isRegExp(vm.pattern)) {
      vm.patterns.push(vm.pattern);
      vm.patternHints.push(vm.patternHint);
    }
    if (_.isString(vm.pattern)) {
      if (VALIDATE_RULES[vm.pattern]) {
        vm.patterns.push(VALIDATE_RULES[vm.pattern].pattern);
        vm.patternHints.push(VALIDATE_RULES[vm.pattern].hint);
      } else {
        const match = vm.pattern.match(/\/(.*)\/([g]*)/);
        if (match && match[1]) {
          vm.patterns.push(new RegExp(match[1], match[2]));
          vm.patternHints.push(vm.patternHint || vm.pattern);
        }
      }
    }
    if (_.isString(vm.type) && VALIDATE_RULES[vm.type]) {
      vm.patterns.push(VALIDATE_RULES[vm.type].pattern);
      vm.patternHints.push(VALIDATE_RULES[vm.type].hint);
    }
  }

  function validationPipe() {
    return {
      test: value => {
        let result = true;
        for (let i = 0; i < vm.patterns.length; i++) {
          const regexp = vm.patterns[i];
          if (!_.isRegExp(regexp)) {
            continue;
          }
          result = regexp.test(value);
          if (!result) {
            vm.currentHint = translateService.get(vm.patternHints[i]);
            break;
          }
        }
        return result;
      },
    };
  }

  function handleInputChange() {
    rbFormDataService.setFormField(vm.name, vm._model);
  }
}
