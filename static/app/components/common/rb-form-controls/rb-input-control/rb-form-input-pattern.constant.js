/**
 * Created by wsk on 17/5/9.
 */
export const VALIDATE_RULES = {
  odd: {
    pattern: /^\d*[13579]$/,
    hint: 'odd_pattern',
  },
  even: {
    pattern: /^\d*[02468]$/,
    hint: 'even_pattern',
  },
  int: {
    pattern: /^-?[0-9][^.]*$/,
    hint: 'integer_pattern',
  },
};
