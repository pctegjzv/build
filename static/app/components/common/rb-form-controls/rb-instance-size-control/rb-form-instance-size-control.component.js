const templateStr = require('app/components/common/rb-form-controls/rb-instance-size-control/rb-form-instance-size-control.html');
angular.module('app.components.pages').component('rbFormInstanceSizeControl', {
  controller: rbInstanceSizeControlController,
  bindings: {
    label: '<',
    name: '<',
    options: '<',
    min: '<',
  },
  controllerAs: 'vm',
  template: templateStr,
});
function rbInstanceSizeControlController(rbFormDataService) {
  const vm = this;
  vm.disableCustomize = true;
  vm.onNewInstanceSizeChange = onNewInstanceSizeChange;
  vm.onOldInstanceSizeChange = onOldInstanceSizeChange;
  vm.$onInit = _init;
  function _init() {
    if (_.isArray(vm.options) && vm.options.includes('CUSTOMIZED')) {
      vm.options.pop();
      vm.disableCustomize = false;
    }
    if (vm.min) {
      if (vm.min.mem === undefined) {
        vm.min.mem = 64;
      }
      if (vm.min.cpu === undefined) {
        vm.min.cpu = 0.125;
      }
      vm.disableCustomize = false;
    }
  }
  function onOldInstanceSizeChange(instance_size, config) {
    if (instance_size === 'CUSTOMIZED') {
      rbFormDataService.setFormField(
        vm.name,
        _.extend({ size: instance_size }, config),
      );
    } else {
      rbFormDataService.setFormField(vm.name, instance_size);
    }
  }
  function onNewInstanceSizeChange(instance_size, config) {
    rbFormDataService.setFormField(
      vm.name,
      _.extend({ size: instance_size }, config),
    );
  }
}
