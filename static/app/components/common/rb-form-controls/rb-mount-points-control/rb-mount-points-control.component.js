const templateStr = require('app/components/common/rb-form-controls/rb-mount-points-control/rb-mount-points-control.html');
angular.module('app.components.pages').component('rbMountPointsControl', {
  controller: rbMountPointsController,
  bindings: {
    label: '<',
    name: '<',
  },
  controllerAs: 'vm',
  template: templateStr,
});

function rbMountPointsController(rbFormDataService, rbServiceUtilities) {
  const vm = this;
  vm.mount_points = [];
  vm.onChange = config => {
    const mount_points = _.map(config, rbServiceUtilities.formatConfigFileData);
    rbFormDataService.setFormField(vm.name, mount_points);
  };
}
