const templateStr = require('app/components/common/rb-form-controls/rb-radio-control/rb-form-radio-control.html');
angular.module('app.components.pages').component('rbFormRadioControl', {
  controller: rbFormRadioControlController,
  bindings: {
    label: '<',
    name: '<',
    description: '<',
    options: '<',
  },
  controllerAs: 'vm',
  template: templateStr,
});

function rbFormRadioControlController(rbFormDataService) {
  const vm = this;
  vm.value = vm.options[0];
  vm.onSelect = onSelect;
  vm.$onInit = onSelect;
  ///
  function onSelect() {
    rbFormDataService.setFormField(vm.name, vm.value);
  }
}
