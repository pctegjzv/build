const templateStr = require('app/components/common/rb-form-controls/rb-radio-group-tab-control/rb-form-radio-group-tab-control.html');
angular.module('app.components.pages').component('rbFormRadioGroupTabControl', {
  controller: rbFormRadioGroupTabControlController,
  bindings: {
    label: '<',
    name: '<',
    description: '<',
    options: '<',
    prefix: '<',
    onChange: '<',
  },
  controllerAs: 'vm',
  template: templateStr,
});

function rbFormRadioGroupTabControlController(
  rbFormDataService,
  rbFormUtilities,
  translateService,
  rbFormDisplayService,
) {
  const vm = this;
  const toggleWords = ['disable', 'enable'];
  vm.onChangeRadio = onChangeRadio;
  vm.$onChanges = _onChanges;
  //////
  function _onChanges({ options }) {
    if (options && options.currentValue) {
      vm.options = _.map(vm.options, option => {
        let displayKey = '';
        toggleWords.forEach(word => {
          if (option.endsWith('_' + word) || option.startsWith(word + '_')) {
            displayKey = translateService.get(vm.prefix + '_' + word, {
              key: option.replace(word, '').replace('_', ''),
            });
          }
        });
        if (!displayKey) {
          displayKey = translateService.get(vm.prefix + '_' + option);
        }
        return {
          key: displayKey,
          value: option,
        };
      });
    }
  }

  function onChangeRadio(selected) {
    rbFormDataService.setFormField(vm.name, selected.value);
    if (_.isFunction(vm.onChange)) {
      vm.onChange(selected.value);
    }
    _.forEach(vm.options, option => {
      let newStatus = false;
      if (selected.value === option.value) {
        newStatus = true;
      }
      _changeConfig(rbFormUtilities.getConfigKey(option.value), newStatus);
    });
  }

  function _changeConfig(configKey, newStatus) {
    rbFormDisplayService.updateComponentDisplayStatus(configKey, newStatus);
  }
}
