const templateStr = require('app/components/common/rb-form-controls/rb-select-control/rb-form-select-control.html');
angular.module('app.components.pages').component('rbFormSelectControl', {
  controller: rbSelectControlController,
  bindings: {
    form: '<',
    label: '<',
    name: '<',
    key: '<',
    fieldKeys: '<',
    value: '<',
    required: '<',
    options: '<',
    optionsControl: '<',
    onChange: '<',
    description: '<',
    multiple: '<',
    onAdd: '<',
    onRemove: '<',
    selectPlaceholder: '<',
    nullPlaceholder: '<',
    minlength: '<',
    maxlength: '<',
  },
  controllerAs: 'vm',
  template: templateStr,
});

function rbSelectControlController($log, $scope, rbFormDataService) {
  const vm = this;
  vm.onRealAdd = onRealAdd;
  vm.onRealRemove = onRealRemove;
  vm.onRealChange = onRealChange;
  vm.isOptionsLoading = false;
  vm.showCurField = true;
  vm.$onInit = _init;
  async function _init() {
    vm.options = vm.options || [];
    vm.values = [];
    vm.model = null;
    vm.modelCtrl = null;
    vm.key = vm.key || 'key';
    vm.value = vm.value || 'value';
    if (vm.optionsControl) {
      let value;
      try {
        vm.isOptionsLoading = true;
        value = await vm.optionsControl;
      } catch (e) {
        $log.error('Cant load options', e);
      }
      vm.isOptionsLoading = false;
      if (typeof value !== 'function') {
        vm.options = value;
      } else {
        const registerService = value;
        registerService(_updateOptions, vm);
      }
    }

    if (vm.selectPlaceholder) {
      $scope.$watch(vm.selectPlaceholder, newValue => {
        vm._selectPlaceholder = newValue;
      });
    }

    // String indicates vm.max is a expression string instead of a scalar value
    if (vm.minlength && typeof vm.minlength === 'string') {
      $scope.$watch(vm.minlength, newValue => {
        vm._minlength = newValue;
      });
    } else {
      vm._minlength = vm.minlength;
    }

    if (vm.maxlength && typeof vm.maxlength === 'string') {
      $scope.$watch(vm.maxlength, newValue => {
        vm._maxlength = newValue;
      });
    } else {
      vm._maxlength = vm.maxlength;
    }
  }

  function _updateOptions(data, context) {
    // context is vm from _init()
    context.options = data;
    // Refresh data
    if (context.multiple) {
      context.values = [];
      rbFormDataService.setFormField(context.name, context.values);
    } else {
      // Resume last time selected value if updated options **still** has the preValue
      let isExist = false;
      const preValue = rbFormDataService.getFormField(context.name);
      _.forEach(data, option => {
        if (option[context.value] === preValue) {
          isExist = true;
        }
      });
      if (isExist) {
        context.init = preValue;
      }
    }
  }

  function onRealChange(option) {
    if (vm.multiple) {
      return;
    }
    if (vm.onChange) {
      vm.onChange(option);
    }
    _setFormField(option[vm.value]);
  }

  function onRealAdd(option) {
    let changeValue;
    if (_.isArray(vm.fieldKeys)) {
      changeValue = {};
      vm.fieldKeys.forEach(key => {
        changeValue[key] = option[key];
      });
    } else {
      changeValue = option[vm.value];
    }
    if (vm.onAdd) {
      vm.onAdd(option);
    }
    vm.values.push(changeValue);
    _setFormField(vm.values);
  }

  function onRealRemove(option) {
    let changeValue;
    if (_.isArray(vm.fieldKeys)) {
      changeValue = {};
      vm.fieldKeys.forEach(key => {
        changeValue[key] = option[key];
      });
    } else {
      changeValue = option[vm.value];
    }
    if (vm.onRemove) {
      vm.onRemove(option);
    }
    vm.values = _.filter(vm.values, item => {
      if (_.isEqual(item, changeValue)) {
        return false;
      } else {
        return true;
      }
    });
    _setFormField(vm.values);
  }
  function _setFormField(value) {
    rbFormDataService.setFormField(vm.name, value);
  }
}
