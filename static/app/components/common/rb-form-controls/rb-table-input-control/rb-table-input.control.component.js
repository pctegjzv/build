const templateStr = require('app/components/common/rb-form-controls/rb-table-input-control/rb-table-input.control.component.html');
angular.module('app.components.pages').component('rbTableInputControl', {
  controller: rbTableInputControlController,
  bindings: {
    form: '<',
    label: '<',
    name: '<',
    required: '<',
    description: '<',
    pattern: '<',
    fieldKeys: '<',
    options: '<',
    optionsControl: '<',
  },
  controllerAs: 'vm',
  template: templateStr,
});

function rbTableInputControlController(
  rbFormDataService,
  translateService,
  rb2DefaultErrorMapperService,
  rbLabelsDataService,
  rbDelay,
  $element,
  $window,
  $log,
) {
  const vm = this;
  vm.addTag = addTag;
  vm.deleteTagFromTags = deleteTagFromTags;
  vm.checkDuplicateKey = checkDuplicateKey;
  vm.overLength = overLength;
  vm.columns = vm.fieldKeys || ['key', 'value'];
  vm.rows = [];
  _initCurRow();
  vm.$onInit = _init;
  /////////
  async function _init() {
    vm.errorMapper = {
      map: (key, error, control) => {
        if (key === 'overlength') {
          return translateService.get('rows_value_overlength');
        } else {
          return rb2DefaultErrorMapperService.map(key, error, control);
        }
      },
    };
    if (vm.optionsControl) {
      let value;
      try {
        vm.isOptionsLoading = true;
        value = await vm.optionsControl;
      } catch (e) {
        $log.error('Cant load options', e);
      }
      vm.isOptionsLoading = false;
      vm.options = value;
      vm.columns.forEach(key => {
        _autoComplete(
          `.ui.search.tag-${key}`,
          _.uniqBy(vm.options[key], 'title'),
          key,
        );
      });
    }
  }

  function checkDuplicateKey() {
    const dup = vm.rows.filter(r => r.key === vm.curRow.key);
    vm.isDuplicateKey = !!dup.length || !!(dup[0] && !dup[0].editable);
    return vm.isDuplicateKey;
  }

  function overLength(str) {
    if (str && str.replace(/[\u0391-\uFFE5]/g, 'aa').length >= 100) {
      return false;
    }
    return true;
  }
  async function addTag() {
    if (!_formValid()) {
      return;
    }
    const row = _.extend({ editable: true }, vm.curRow);
    if (vm.isDuplicateKey) {
      vm.rows[_.findIndex(vm.rows, { key: row.key, editable: true })] = row;
      _resetModel();
    } else {
      vm.rows.push(_.extend({ editable: true }, row));
      const rows = _.map(vm.rows, label => _.pick(label, vm.columns));
      rbFormDataService.setFormField(vm.name, rows);
      _resetModel();
    }
    await rbDelay();
    // twinkle
    const el = $element.find('tbody tr.ng-scope')[
      _.findIndex(vm.rows, { key: row.key, editable: true })
    ];
    if (el) {
      el.classList.remove('latest-edit');
      $window.getComputedStyle(el, null).background;
      el.classList.add('latest-edit');
    }
  }

  function deleteTagFromTags(index) {
    vm.rows = vm.rows.filter((r, idx) => idx !== index);
    const rows = _.map(vm.rows, label => _.pick(label, 'key', 'value'));
    rbFormDataService.setFormField(vm.name, rows);
  }

  function _autoComplete(selector, source, type) {
    const $input = $element.find(selector);
    $input.search({
      searchFields: 'title',
      source,
      cache: false,
      error: {
        noResults: translateService.get('search_no_result'),
      },
      onSelect: result => {
        vm.curRow[type] = result.title;
      },
    });
  }
  function _rowIncalid(row) {
    let ret = false;
    vm.columns.forEach(key => {
      if (!row[key] || !overLength(row[key])) {
        ret = true;
      }
    });
    return ret;
  }
  function _formValid() {
    const row = vm.curRow;
    if (vm.curRow.$invalid || _rowIncalid(row)) {
      return false;
    }
    return true;
  }
  function _resetModel() {
    vm.tableForm.$setPristine();
    _initCurRow();
  }
  function _initCurRow() {
    vm.curRow = {};
    vm.columns.forEach(key => {
      vm.curRow[key] = '';
    });
    vm.isDuplicateKey = false;
  }
}
