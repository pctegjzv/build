const templateStr = require('app/components/common/rb-form-controls/rb-tags-input-control/rb-tags-input-control.component.html');
angular.module('app.components.pages').component('rbTagsInputControl', {
  controller: rbTagsInputControlController,
  bindings: {
    form: '<',
    label: '<',
    name: '<',
    key: '<',
    fieldKeys: '<',
    value: '<',
    required: '<',
    options: '<',
    optionsControl: '<',
    description: '<',
    selectPlaceholder: '<',
    nullPlaceholder: '<',
    pattern: '<',
  },
  controllerAs: 'vm',
  template: templateStr,
});

function rbTagsInputControlController(
  rbFormDataService,
  translateService,
  $log,
) {
  const vm = this;
  vm.$onInit = _init;
  vm.tagValidateFn = tagValidateFn;
  vm.onTagChange = onTagChange;
  async function _init() {
    vm.placeholder = translateService.get(
      vm.selectPlaceholder || 'rb_form_tags_select',
    );
    if (vm.optionsControl) {
      let value;
      try {
        vm.isOptionsLoading = true;
        value = await vm.optionsControl;
      } catch (e) {
        $log.error('Cant load options', e);
      }
      vm.isOptionsLoading = false;
      if (typeof value !== 'function') {
        vm.options = value;
      } else if (value instanceof Promise) {
        const registerService = await value;
        registerService(_updateSuggestions, vm);
      } else {
        const registerService = value;
        registerService(_updateSuggestions, vm);
      }
    }
  }
  /////////
  function tagValidateFn(tag) {
    if (vm.tags) {
      return !vm.tags.includes(tag);
    }
    if (vm.pattern) {
      return tag.match(vm.pattern);
    }
    return true;
  }

  function onTagChange(tags) {
    vm.tags = tags;
    let datas = tags;
    if (vm.fieldKeys) {
      datas = tags.map(tag => {
        const match = tag.match(vm.pattern);
        tag = {};
        _.forIn(vm.fieldKeys, (val, key) => {
          tag[key] = match[val];
        });
        return tag;
      });
    }
    rbFormDataService.setFormField(vm.name, datas);
  }

  function _updateSuggestions(data, context) {
    context.options = data;
  }
}
