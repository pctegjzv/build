angular
  .module('app.components.common')
  .directive('rbFormTooltip', rbFormTooltip);

function rbFormTooltip() {
  return {
    scope: {
      position: '<', // string 'top right' 'top bottom'
      tooltipShow: '<', // boolean, if true show the tooltip, or will hide it
    },
    controllerAs: 'vm',
    bindToController: true,
    controller: rbFormTooltipController,
  };
}

function rbFormTooltipController($element) {
  const vm = this;
  let _created = false;
  vm.settings = {
    inline: true,
    exclusive: true,
    addTouchEvents: false,
    position: this.position || 'top right',
    on: 'manual',
  };
  vm.$postLink = () => {
    $element.popup(vm.settings);
    $element.parent().css({ position: 'relative' });
    _created = true;
  };
  vm.$onChanges = ({ tooltipShow }) => {
    if (!_created) {
      return;
    }
    if (tooltipShow) {
      if (tooltipShow.currentValue === true) {
        $element.popup(vm.settings);
        $element.popup('show');
      } else if (tooltipShow.currentValue === false) {
        $element.popup('hide');
      }
    }
  };
}
