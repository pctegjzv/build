/**
 * Created by liudong on 16/9/19.
 */
const templateStr = require('app/components/common/rb-image-select/image-select-card/rb-image-select-card.html');
angular.module('app.components.common').component('rbImageSelectCard', {
  controller: rbImageSelectCardController,
  bindings: {
    repo: '<',
  },
  controllerAs: 'vm',
  template: templateStr,
});

function rbImageSelectCardController() {
  const vm = this;
  vm.initialized = false;

  vm.$onInit = _init;

  //////////

  function _init() {}
}
