/**
 * Created by liudong on 2016/12/22.
 */
const templateStr = require('app/components/common/rb-image-select/project-list/rb-image-select-project-list.html');
angular.module('app.components.common').component('rbImageSelectProjectList', {
  controller: rbImageSelectProjectListController,
  bindings: {
    registry: '<',
  },
  require: {
    imageSelectCardCtrl: '^^rbImageSelect',
  },
  controllerAs: 'vm',
  template: templateStr,
});

function rbImageSelectProjectListController(
  rbRegistryDataService,
  rbRepositoryDataService,
  translateService,
  rbSafeApply,
  WEBLABS,
) {
  const vm = this;

  vm.selectProject = selectProject;
  vm.selectImage = selectImage;
  vm.$onInit = _init;

  //////////

  async function _init() {
    const defaultProject = {
      project_name: translateService.get('default_project_name'),
      no_project: true,
    };
    vm.weblabs = WEBLABS;
    vm.initialized = false;
    vm.loading = false;
    vm.currentProject = defaultProject;
    vm.repositoriesFilter = { name: '' };
    if (!vm.registry.is_public) {
      const projects = await getProjects();
      vm.projects = [defaultProject].concat(projects);
    } else {
      vm.projects = [defaultProject];
    }
    vm.initialized = true;
    selectProject(defaultProject);
    rbSafeApply();
  }

  async function getProjects() {
    return rbRegistryDataService
      .getProjects({
        registry_name: vm.registry.name,
      })
      .then(projects => projects);
  }

  function selectProject(project) {
    vm.currentProject = project;
    // get repositories under this project
    vm.loading = true;
    getRepositories(project).then(repositories => {
      vm.repositories = repositories;
      vm.loading = false;
    });
  }

  function getRepositories(project) {
    return rbRepositoryDataService
      .getRepositories({
        registry_name: vm.registry.name,
        project_name: project.no_project ? undefined : project.project_name,
      })
      .then(result => {
        _.forEach(result, item => {
          item.name = item.name ? item.name : item.repo_path || item.repo_name;
          if (!item.logo_file) {
            item.logo_file = '/static/images/user/default-logo.png';
          }
        });
        return result;
      });
  }

  function selectImage(item) {
    vm.imageSelectCardCtrl.selectImage(item);
  }
}
