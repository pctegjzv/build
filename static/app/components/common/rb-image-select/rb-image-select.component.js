/**
 * Created by liudong on 16/9/19.
 *
 * A page size Docker image selector component.
 */
const templateStr = require('app/components/common/rb-image-select/rb-image-select.component.html');
angular.module('app.components.common').component('rbImageSelect', {
  bindings: {
    /**
     * Will be called when image selected
     * {
     *   image_name: string, // will be namespace/repo_name for thirdparty
     *   registry_index: string,
     *   full_image_name: string // {registry_index}/{project_name}/{image_name}
     *
     *   // Only given if Alauda managed registry selected:
     *   registry_name: string,
     *   project_name: string,
     *   is_public_registry: boolean
     * }
     */
    onImageSelected: '&', // { image_name, registry, is_public_registry }
  },
  controller: rbImageSelectController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbImageSelectController(
  rbSafeApply,
  translateService,
  rbAccountService,
  rbRegistryDataService,
  rbRepositoryDataService,
  rb2DefaultErrorMapperService,
  ENVIRONMENTS,
  WEBLABS,
) {
  const vm = this;
  const namespace = rbAccountService.getCurrentNamespace();
  const dockerImageIndex = 'index.docker.io';

  vm.getJumpstartRepoList = getJumpstartRepoList;
  vm.getAlaudaLibraryRepoList = getAlaudaLibraryRepoList;
  vm.selectPrivateRegistry = selectPrivateRegistry;
  vm.validateRepositoryExist = validateRepositoryExist;
  vm.selectImage = selectImage;
  vm.confirmDockerImage = confirmDockerImage;
  vm.confirmCustomImage = confirmCustomImage;

  vm.$onInit = _init;
  //////////

  function _init() {
    vm.weblabs = WEBLABS;
    vm.jumpstartRepoTypes = [
      'SSH Servers',
      'Web Servers',
      'Databases',
      'Cache Servers',
      'Others',
    ];
    vm.jumpstartRepoType = '';
    vm.jumpstartRepos = {};

    vm.privateRegistryList = [];
    vm.jumpstartRepoList = [];
    vm.accountRepoList = [];
    vm.libraryRepoList = [];

    vm.libraryRepoFilter = { repo_path: '' };
    vm.accountRepoFilter = { repo_path: '' };
    vm.jumpstartRepoFilter = { name: '' };

    vm.jumpstartLoading = false;
    vm.accountRepoLoading = false;
    vm.libraryRepoLoading = false;

    vm.removeDefaultImageRegistry =
      ENVIRONMENTS['remove_default_image_registry'];

    vm.errorMapper = {
      map: (key, error, control) => {
        if (key === 'exist') {
          return translateService.get('image_not_exist');
        } else {
          return rb2DefaultErrorMapperService.map(key, error, control);
        }
      },
    };

    getPrivateRegistryList().then(() => {
      vm.initialized = true;
      rbSafeApply();
    });
    getJumpstartRepoList(vm.jumpstartRepoTypes[0]);
  }

  // get private registry list
  function getPrivateRegistryList() {
    return rbRegistryDataService.getRegistries().then(result => {
      // sort by name, move public registry to the end
      const list = _.sortBy(result, item => {
        return (item.display_name || item.name).toLowerCase();
      });
      vm.privateRegistryList = _.sortBy(list, item => {
        return item.is_public;
      });
    });
  }

  // get jump start repo
  function getJumpstartRepoList(type) {
    vm.jumpstartRepoType = type;
    if (vm.jumpstartRepos[type]) {
      return;
    }
    vm.jumpstartLoading = true;
    return rbRepositoryDataService
      .getPublicRepositoryGallery(type)
      .then(data => {
        _addRepoProperties(data[type]);
        vm.jumpstartRepos[type] = data[type];
      })
      .catch(() => {
        vm.jumpstartRepos[type] = null;
      })
      .then(() => {
        vm.jumpstartLoading = false;
      });
  }

  // alauda library repo list
  function getAlaudaLibraryRepoList() {
    if (vm.libraryRepoList.length > 0) {
      return;
    }
    vm.libraryRepoLoading = true;
    rbRepositoryDataService
      .getPublicRepositoryList('library')
      .then(({ results }) => {
        _addRepoProperties(results);
        vm.libraryRepoList = results;
      })
      .catch(() => {})
      .then(() => {
        vm.libraryRepoLoading = false;
      });
  }

  // add name property for each repo
  function _addRepoProperties(list) {
    _.forEach(list, item => {
      if (item.repo_path) {
        item.name = item.repo_path;
      }
      if (item.repo_name) {
        item.name = item.repo_name;
      }
      if (!item.logo_file) {
        item.logo_file = '/static/images/user/default-logo.png';
      }
    });
  }

  // validate docker image name exist
  function validateRepositoryExist(value) {
    if (!value) {
      return true;
    }
    const repo_path =
      value.indexOf('/') > 0 ? value : ['library', value].join('/');
    const params = {
      repo_path,
      index: dockerImageIndex,
    };

    return new Promise((resolve, reject) => {
      rbRepositoryDataService
        .checkPublicRepositoryExist(params)
        .then(() => {
          resolve();
        })
        .catch(() => {
          reject();
        })
        .then();
    });
  }

  // select private registry -> get project list
  function selectPrivateRegistry(registry) {
    vm.currentRegistryName = registry.name;
  }

  // select image card
  async function selectImage(item) {
    const params = {};
    let full_image_name;
    if (item.registry) {
      params.image_name = item.name;
      params.registry_name = item.registry.name;
      params.registry_index = item.registry.endpoint;
      params.is_public_registry = item.registry.is_public;
      if (item.registry.is_public) {
        params.image_name = namespace + '/' + params.image_name;
        full_image_name = `${item.registry.endpoint}/${namespace}/${item.name}`;
      } else {
        if (item.project) {
          params.project_name = item.project.project_name;
          full_image_name = `${item.registry.endpoint}/${
            item.project.project_name
          }/${item.name}`;
        } else {
          full_image_name = `${item.registry.endpoint}/${item.name}`;
        }
      }
    } else {
      full_image_name = [
        ENVIRONMENTS['alauda_image_index'],
        item.repo_path,
      ].join('/');
      params.registry_index = ENVIRONMENTS['alauda_image_index'];
      params.image_name = item.repo_path;
    }
    params.full_image_name = full_image_name;

    vm.onImageSelected(params);
  }

  // create service using docker image
  function confirmDockerImage() {
    vm.dockerImageForm.$setSubmitted();
    if (
      vm.dockerImageForm.$invalid ||
      vm.dockerImageForm['docker_image_name']['$pending']
    ) {
      return;
    }
    if (vm.dockerImageName) {
      const repo_path =
        vm.dockerImageName.indexOf('/') > 0
          ? vm.dockerImageName
          : ['library', vm.dockerImageName].join('/');
      const imageName = [dockerImageIndex, repo_path].join('/');
      vm.onImageSelected({
        full_image_name: imageName,
        image_name: repo_path,
        registry_index: dockerImageIndex,
      });
    }
  }

  //create service using custom image
  function confirmCustomImage() {
    vm.customImageForm.$setSubmitted();
    if (vm.customImageForm.$invalid) {
      return;
    }
    if (vm.customImageIndex && vm.customImageName) {
      const imageName = [vm.customImageIndex, vm.customImageName].join('/');
      vm.onImageSelected({
        full_image_name: imageName,
        image_name: vm.customImageName,
        registry_index: vm.customImageIndex,
      });
    }
  }
}
