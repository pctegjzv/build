angular
  .module('app.components.pages')
  .directive('rbIsolateForm', rbIsolateForm);

function rbIsolateForm() {
  return {
    restrict: 'A',
    require: '?form',
    link,
  };
}

function link(scope, element, attrs, formController) {
  if (!formController) {
    return;
  }

  formController.$$parentForm.$removeControl(formController);
  if (!formController.$$parentForm) {
    return;
  }

  const setValidity = formController.$setValidity;

  formController.$setValidity = (validationErrorKey, isValid, control) => {
    setValidity.call(formController, validationErrorKey, isValid, control);
    formController.$$parentForm.$setValidity(validationErrorKey, true, this);
  };
}
