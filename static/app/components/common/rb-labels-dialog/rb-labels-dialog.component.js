import _ from 'lodash';

const templateStr = require('app/components/common/rb-labels-dialog/rb-labels-dialog.html');
angular.module('app.components.common').component('rbLabelsDialog', {
  bindings: {
    namespace: '<',
    sourceName: '<',
    sourceType: '<',
  },
  controller: rbLabelsDialogController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbLabelsDialogController(
  $element,
  $window,
  rbModal,
  rbToast,
  rbDelay,
  translateService,
  rb2DefaultErrorMapperService,
  rbLabelsDataService,
) {
  const vm = this;
  vm.labels = [];
  vm.tag = { key: '', value: '' };
  vm.isDuplicateKey = false;
  vm.isLoading = false;
  vm.overLength = overLength;

  vm.addTag = addTag;
  vm.cancel = cancel;
  vm.confirm = confirm;
  vm.deleteTagFromTags = deleteTagFromTags;
  vm.checkDuplicateKey = checkDuplicateKey;

  vm.$onInit = _init;

  /////////
  function _init() {
    vm.errorMapper = {
      map: (key, error, control) => {
        if (key === 'overlength') {
          return translateService.get('labels_value_overlength');
        } else {
          return rb2DefaultErrorMapperService.map(key, error, control);
        }
      },
    };

    _refetchSourceLabels();
  }

  function checkDuplicateKey() {
    const dup = vm.labels.filter(r => r.key === vm.tag.key);
    vm.isDuplicateKey = !!dup.length || !!(dup[0] && !dup[0].editable);
    return vm.isDuplicateKey;
  }

  function overLength(str) {
    if (str) {
      return (
        (str => str.replace(/[\u0391-\uFFE5]/g, 'aa').length)(str) < 100 && true
      );
    }
    return false;
  }

  function formValid() {
    const tg = vm.tag;
    if (vm.tag.$invalid || !tg.key || !tg.value || !overLength(vm.tag.value)) {
      return false;
    }
    return true;
  }

  function _resetModel() {
    vm.tag.$setPristine();
    vm.tag.key = '';
    vm.tag.value = '';
    vm.isDuplicateKey = false;
  }

  async function addTag() {
    if (!formValid()) {
      return;
    }
    const tag = _.extend({ editable: true }, vm.tag);
    if (vm.isDuplicateKey) {
      vm.labels[_.findIndex(vm.labels, { key: tag.key, editable: true })] = tag;
      _resetModel();
    } else {
      vm.labels.push(_.extend({ editable: true }, tag));
      vm.suggestions.keys.push({ title: tag.key });
      vm.suggestions.values.push({ title: tag.value });
      autoComplete(
        '.ui.search.tag-key',
        _.uniqBy(vm.suggestions.keys, 'title'),
        'key',
      );
      autoComplete(
        '.ui.search.tag-val',
        _.uniqBy(vm.suggestions.values, 'title'),
        'value',
      );
      _resetModel();
    }
    await rbDelay();
    // twinkle
    const el = $element.find('tbody tr.ng-scope')[
      _.findIndex(vm.labels, { key: tag.key, editable: true })
    ];
    el.classList.remove('latest-edit');
    $window.getComputedStyle(el, null).background;
    el.classList.add('latest-edit');
  }

  function deleteTagFromTags(index) {
    vm.labels = vm.labels.filter((r, idx) => idx !== index);
  }

  function _refetchSourceLabels() {
    vm.isLoading = true;
    rbLabelsDataService
      .getSourceLabels({
        namespace: vm.namespace,
        name: vm.sourceName,
        source_type: vm.sourceType,
      })
      .then(res => {
        vm.labels = _.sortBy(res.result, 'editable');
      })
      .catch(() => {
        /**/
      })
      .then(() => {
        vm.isLoading = false;
      });

    _getSuggestions();
  }

  function _getSuggestions() {
    rbLabelsDataService
      .getAllLebels()
      .then(res => {
        vm.suggestions = {
          keys: [],
          values: [],
        };
        res.results.forEach(label => {
          vm.suggestions.keys.push({ title: label.key });
          vm.suggestions.values.push({ title: label.value });
        });
        autoComplete(
          '.ui.search.tag-key',
          _.uniqBy(vm.suggestions.keys, 'title'),
          'key',
        );
        autoComplete(
          '.ui.search.tag-val',
          _.uniqBy(vm.suggestions.values, 'title'),
          'value',
        );
      })
      .catch(() => {
        /**/
      });
  }

  function cancel() {
    rbModal.cancel();
  }

  function getAllEditabledLabelsDict() {
    const aLabels = [];
    vm.labels.filter(r => !!r.editable).forEach(r => {
      aLabels.push({
        key: r.key,
        value: r.value,
        editable: true,
      });
    });
    return aLabels;
  }

  function confirm() {
    // vm.tag.$setSubmitted();
    const editableLabels = getAllEditabledLabelsDict();
    const allLabels = [...vm.labels];
    vm.submitting = true;
    rbLabelsDataService
      .updateSourceLabels({
        namespace: vm.namespace,
        name: vm.sourceName,
        source_type: vm.sourceType,
        labels: editableLabels,
      })
      .then(() => {
        rbToast.success(translateService.get('labels_updated'));
        rbModal.hide(allLabels);
      })
      .catch(() => {
        /**/
      })
      .then(() => {
        vm.submitting = false;
      });
  }

  function autoComplete(selector, source, type) {
    const $input = $element.find(selector);
    $input.search({
      searchFields: 'title',
      source,
      cache: false,
      error: {
        noResults: translateService.get('search_no_result'),
      },
      onSelect: result => {
        vm.tag[type] = result.title;
      },
    });
  }
}
