import Highcharts from 'highcharts';
import _ from 'lodash';
import ResizeSensor from 'css-element-queries/src/ResizeSensor';

/**
 * Line chart component.
 * Encapsulates options for a line chart with Highcharts library:
 *   - Provides default option values
 *   - Easier for styling
 *   - Define a common domain for our use cases
 */
const templateStr = require('app/components/common/rb-line-chart/rb-line-chart.html');
angular.module('app.components.common').component('rbLineChart', {
  controller: rbLineChartController,
  controllerAs: 'vm',
  template: templateStr,
  bindings: {
    // Series of line data. Chart will be updated only when the reference is changed
    // e.g: [{
    //   name: 'series name',
    //   data: [7.0, 6.9, 9.5, 14.5, 18.2]
    // }]
    series: '<',

    // Raw Highcharts x/y axis options
    // {x: {}, y: {}}
    axisOptions: '<',

    // Custom highcharts options
    highchartsOptions: '<',

    // Set to false when you want to hide the legend
    hideLegend: '<',

    // A function to decorate the tooltip for a point
    pointTooltipFn: '<',
  },
});

function rbLineChartController($element) {
  const vm = this;
  let initialized;
  let chartsOptions;
  let resizeSensor;
  let $chartContainer;

  // Constant Highcharts options:
  // http://api.highcharts.com/highcharts/
  // Also, test here: http://jsfiddle.net/5a572w8x/
  const HIGHCHARTS_OPTIONS = {
    credits: false,

    chart: {
      backgroundColor: 'transparent',
      height: 240,
      type: 'line',
      // Make sure the chart takes all space
      spacing: [20, 0, 0, 0],
    },

    title: {
      text: '',
    },

    tooltip: {
      headerFormat: '',
    },

    legend: {
      layout: 'horizontal',
      align: 'left',
      verticalAlign: 'bottom',
      borderWidth: 0,
      maxHeight: 200,
    },

    yAxis: {
      title: {
        text: '',
      },
    },

    plotOptions: {
      line: {
        lineWidth: 1,
        marker: {
          lineWidth: 0,
          radius: 3,
        },
      },
    },

    // Hmm, hopefully there are no more than 5 of series ...
    colors: [
      '#7cb5ec',
      '#61D44A',
      '#434348',
      '#f7a35c',
      '#8085e9',
      '#f15c80',
      '#e4d354',
      '#2b908f',
      '#f45b5b',
      '#08DCCB',
    ],

    // Placeholder
    series: [],
  };

  vm.noData = true;
  vm.$onChanges = onChanges;
  vm.$postLink = postLink;
  vm.$onDestroy = _destroy;

  //////////
  function _destroy() {
    resizeSensor.detach($chartContainer, _reflow);
  }

  function postLink() {
    chartsOptions = HIGHCHARTS_OPTIONS;

    vm.pointTooltipFn &&
      _.merge(chartsOptions, {
        tooltip: {
          pointFormatter: function _pointFormatter() {
            // The this keyword refers to the Point object.
            return vm.pointTooltipFn(this);
          },
        },
      });

    const { xAxis, yAxis } = vm.axisOptions || {};
    xAxis && _.merge(chartsOptions, { xAxis });
    yAxis && _.merge(chartsOptions, { yAxis });

    // merge other highchart options
    _.merge(chartsOptions, vm.highchartsOptions || {});
    // Attach highcharts to the dom:
    vm.chart = new Highcharts.Chart(_getChartElement()[0], chartsOptions);
    _redraw();
    setUpResizeSensor();
    initialized = true;
  }

  function onChanges({ series, axisOptions, highchartsOptions }) {
    if (
      (series && series.currentValue) ||
      (axisOptions && axisOptions.currentValue)
    ) {
      vm.noData =
        !vm.series ||
        vm.series.every(seriesItem => seriesItem.data.length === 0);
      _redraw();
    } else if (series && !series.currentValue) {
      vm.noData = true;
    }

    if (initialized && highchartsOptions && highchartsOptions.currentValue) {
      chartsOptions = _.merge(chartsOptions, vm.highchartsOptions);
      vm.chart.update(chartsOptions, true);
      _redraw();
    }
  }

  function setUpResizeSensor() {
    $chartContainer = _getChartElement();
    resizeSensor = new ResizeSensor($chartContainer, _reflow);
  }

  // Redraw the chart
  function _redraw() {
    // Only redraw when vm.series is not null
    if (!vm.series || !vm.chart) {
      return;
    }

    // First, remove series that are not in the input
    // Iterate based on the chart ordering
    const removeItems = [];
    vm.chart.series.forEach(chartSeriesItem => {
      if (
        !vm.series.find(seriesItem => chartSeriesItem.name === seriesItem.name)
      ) {
        removeItems.push(chartSeriesItem);
      }
    });
    removeItems.forEach(item => item.remove(false));

    // Second, add or set data based on the input data ordering
    vm.series.forEach(seriesItem => {
      const chartSeriesItem = vm.chart.series.find(_chartSeriesItem => {
        return _chartSeriesItem.name === seriesItem.name;
      });
      if (chartSeriesItem) {
        chartSeriesItem.setData(seriesItem.data, false);
      } else {
        vm.chart.addSeries(seriesItem, false);
      }
    });

    // Then, update axis
    const { xAxis, yAxis } = vm.axisOptions || {};
    vm.chart.xAxis[0].update(xAxis, false);
    vm.chart.yAxis[0].update(yAxis, false);

    vm.chart.legend.update({ enabled: !vm.hideLegend }, false);

    // Lastly, redraw
    vm.chart.redraw(false);
    vm.chart.reflow();
  }

  function _reflow() {
    return vm.chart.reflow();
  }

  function _getChartElement() {
    return $('.line-highcharts-container', $element);
  }
}
