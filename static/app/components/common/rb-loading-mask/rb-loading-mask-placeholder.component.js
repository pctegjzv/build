/**
 * Created by liudong on 16/11/3.
 */
angular.module('app.components.common').component('rbLoadingMaskPlaceholder', {
  controller: rbLoadingMaskPlaceholderController,
  controllerAs: 'vm',
  template:
    '<div class="rb-loading-mask-placeholder" rb-loading-mask="true"></div>',
});

function rbLoadingMaskPlaceholderController() {}
