angular
  .module('app.components.common')
  .directive('rbLoadingMask', rbLoadingMask);

/**
 * A directive which to decorate the container with loading mask
 */
function rbLoadingMask($compile) {
  return {
    restrict: 'A',
    link,
    scope: false, // Uses the parent scope
  };

  //////////
  function link(scope, element, attrs) {
    const POSITIONED_VALUES = ['relative', 'absolute', 'fixed'];

    let size;
    if (attrs.rbLoaderSize) {
      size = attrs.rbLoaderSize;
    } else {
      const height = element.innerHeight();
      if (height <= 32) {
        size = 'small';
      } else if (height <= 96) {
        size = 'medium';
      } else {
        size = 'large';
      }
    }

    const loadingMask = attrs.rbLoadingMask; // the expression to watch on the parent scope

    scope.$watch(loadingMask, newValue => {
      if (newValue && !POSITIONED_VALUES.includes(element.css('position'))) {
        element.css('position', 'relative');
      }
    });

    const template = `
    <div class="rb-loading-mask" ng-show="${loadingMask}">
      <div class="ui loader ${size} active">
    </div>`;
    element.append($compile(template)(scope));
  }
}
