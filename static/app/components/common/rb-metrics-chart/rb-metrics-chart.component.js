import _ from 'lodash';
import moment from 'moment';

/**
 * Common metrics chart for metrics,
 * eg., CPU utilization, memory usage, instances
 */
const templateStr = require('app/components/common/rb-metrics-chart/rb-metrics-chart.html');
angular.module('app.components.common').component('rbMetricsChart', {
  controller: rbMetricsChartController,
  controllerAs: 'vm',
  template: templateStr,
  bindings: {
    // Can be an array, or a mapping from keys to metrics
    metrics: '<',

    // Possible values:
    //  - Percent
    //  - Count
    //  - All other values will be treated as Count as well.
    unit: '@',
  },
});

function rbMetricsChartController() {
  const vm = this;

  vm.pointTooltipFn = getPointFormat;
  vm.$onChanges = onChanges;
  /////////
  function onChanges({ metrics }) {
    if (metrics && metrics.currentValue) {
      const { axisOptions, series } = _parseMetricsToSeries(
        metrics.currentValue,
      );
      vm.series = series;
      vm.axisOptions = axisOptions;

      // TODO legend auto hide does not work properly at the moment
      // Hide legend when there is only one series
      vm.hideLegend = !series || series.length < 2;
    }
  }

  function _parseMetricsToSeries(metrics) {
    if (_.isEmpty(metrics)) {
      return {};
    }

    // Migrate array to key -> array
    if (Array.isArray(metrics)) {
      metrics = {
        instance: metrics,
      };
    }

    // Calculate the time categories (x axis)
    const categories = calculateCategories(metrics);

    const series = Object.keys(metrics)
      .filter(instance => Array.isArray(metrics[instance]))
      .map(instance => {
        const instanceMetrics = metrics[instance];

        // Fill missing metrics data points with null
        const data = categories.map(x => {
          const point = instanceMetrics.find(
            _point => _point.timestamp === x,
          ) || { value: null };

          // Also, multiply the Percentage values by 100
          return vm.unit === 'Percent' && point.value
            ? +(point.value * 100).toPrecision(6)
            : point.value;
        });

        return {
          name: instance,
          data,
        };
      });

    const axisOptions = _computeAxisOptions(series, categories);

    return { axisOptions, series };
  }

  function _computeAxisOptions(series, categories) {
    const xAxis = {
      categories,
      labels: {
        step: Math.floor(categories.length / 4),
        formatter: function _formatter() {
          return moment(this.value).format('MM-DD HH:mm');
        },
      },
    };

    const yAxis = {
      max: getYAxisMaxValue(series),
      minRange: getYAxisMinRange(),
      title: {
        text: '',
      },
      labels: {
        formatter: function _formatter() {
          if (vm.unit === 'Percent') {
            return this.value + '%';
          } else {
            return this.value;
          }
        },
      },

      // Hide non integer values
      allowDecimals: vm.unit === 'Percent',
    };

    return { xAxis, yAxis };
  }

  function calculateCategories(metrics) {
    const instances = Object.keys(metrics).filter(metricKey =>
      Array.isArray(metrics[metricKey]),
    );
    const maxPointsInstance = _.maxBy(
      instances,
      instance => metrics[instance].length,
    );

    // The categories need to be in ascending order
    return metrics[maxPointsInstance].map(({ timestamp }) => timestamp).sort();
  }

  function getYAxisMinRange() {
    if (vm.unit === 'Percent') {
      return 0.1;
    } else {
      return 1;
    }
  }

  function getYAxisMaxValue(series) {
    let data = [];
    _.forEach(series, s => {
      data = data.concat(s.data);
    });
    const max = _.max(data);
    return max === 0 ? 100 : max * 1.2;
  }

  function getPointFormat(point) {
    const str = moment(point.category).format('YYYY-MM-DD HH:mm:ss');
    if (vm.unit === 'Percent') {
      return (
        '<span style="font-size: 10px">' +
        str +
        '</span><br>' +
        point.series.name +
        ': ' +
        point.y +
        '%'
      );
    } else {
      return (
        '<span style="font-size: 10px">' +
        str +
        '</span><br>' +
        point.series.name +
        ': ' +
        point.y
      );
    }
  }
}
