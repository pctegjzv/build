/**
 * Created by liudong on 2016/12/19.
 */
/**
 * rbModal.show({
 *  title: 'create',
 *  width: 800, // default is 600px;
 *  locals: {
 *   attrFoo : 'bar'
 *  },
 *  template: '<some-test-component attr-foo="attrFoo"></some-test-component>'
 *  // or with syntax sugar:
 *  component: 'someTestComponent' // in this mode, we will fill the locals with the same attribute names for you
 * }).then((data) => {
 *
 * });
 *
 * rbModal.hide(obj) : return a resolved promise with obj
 * rbModal.cancel(obj) : return a rejected promise with obj
 */
const _ = require('lodash');
angular.module('app.components.common').factory('rbModal', rbModalFactory);

function rbModalFactory($rootScope, $compile, $timeout) {
  let modalInstance;
  let _resolve, _reject;
  let _resolveResult, _rejectResult;
  return {
    show,
    hide,
    cancel,
    getModalInstance,
  };

  //////////
  function show({
    title = '',
    width = 600,
    locals = {},
    template = '',
    component = '',
    closable = false,
    autofocus = false,
  }) {
    modalInstance = $(
      `<div class="ui modal rb-modal" style="width: ${width}px;"></div>`,
    );
    _resolve = null;
    _resolveResult = _rejectResult = null;
    let modalTempalte = title
      ? `<div class="header">${title}<i ng-click="close()" class="fa fa-remove"></i></div>`
      : '';
    const _scope = $rootScope.$new(true);
    _scope.close = () => {
      cancel();
    };
    _.assign(_scope, locals);

    if (template && component) {
      throw new Error(
        'Template can not be used with component at the same time for rbModal',
      );
    }

    if (component) {
      template = _generateComponentTemplate(component, locals);
    }

    modalTempalte += `<div class="content">${template}</div>`;
    $timeout(() => {
      $('body').append(modalInstance);
      modalInstance.addClass('rb-modal-loading');
      const width = modalInstance.width();
      modalInstance.removeClass('rb-modal-loading');
      modalInstance.css('margin-left', -width / 2);
      modalInstance
        .modal({
          closable,
          duration: 200,
          autofocus,
          onHidden: () => {
            modalInstance.remove();
            _scope.$destroy();
            modalInstance = null;
            _resolveResult !== null && _resolve && _resolve(_resolveResult);
            _rejectResult !== null && _reject && _reject(_rejectResult);
          },
          onShow: () => {
            modalInstance.append($compile(modalTempalte)(_scope));
            // 之前利用ResizeSensor监测modal大小变化并调用modal('refresh')的方法会引起DEV-2692的问题
            // 使modal默认为scrolling的可以解决当modal超出页面高度时，超出的区域不可可见问题
            $('body.dimmable').addClass('scrolling');
            modalInstance.addClass('scrolling');
          },
          onVisible: () => {
            _scope.$broadcast('rbModalShow');
          },
        })
        .modal('show');
    }, 0);
    return new Promise((resolve, reject) => {
      _resolve = resolve;
      _reject = reject;
    });
  }

  function hide(result = {}) {
    modalInstance && modalInstance.modal('hide');
    _resolveResult = result;
  }

  function cancel(result = {}) {
    modalInstance && modalInstance.modal('hide');
    _rejectResult = result;
  }

  function getModalInstance() {
    return modalInstance;
  }

  // e.g.:
  // _generateComponentTemplate('asdAsdA', {a: 123, b: 123, cAsdS: 456})
  //   => "<asd-asd-a a="a" b="b" c-asd-s="cAsdS"></asd-asd-a>"
  function _generateComponentTemplate(component, locals) {
    const componentTag = _.kebabCase(component);
    const attrs = Object.keys(locals)
      .map(attrName => `${_.kebabCase(attrName)}="${attrName}"`)
      .join(' ');
    return `<${componentTag} ${attrs}></${componentTag}>`;
  }
}
