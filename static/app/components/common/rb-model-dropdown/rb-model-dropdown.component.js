import { rbDropdown } from 'app/components/common/rb-dropdown/rb-dropdown.component';
angular.module('app.components.common').component(
  'rbModelDropdown',
  _.merge({}, rbDropdown, {
    require: {
      ngModel: 'ngModel',
    },
    bindings: {
      required: '<',
      maxlength: '@',
      minlength: '@',
    },
    controller: rbModelDropdownController,
  }),
);
function rbModelDropdownController(
  $window,
  $scope,
  $element,
  $timeout,
  translateService,
) {
  'ngInject';
  const vm = this;
  // Can't use 'apply' because of syntax linting
  rbDropdown.controller.call(
    vm,
    $window,
    $scope,
    $element,
    $timeout,
    translateService,
  );
  const postLink = vm.$postLink;
  const onChange = vm.onChange;
  const onAdd = vm.onAdd;
  const onRemove = vm.onRemove;
  let _multiSelected = [];
  let isFirstChange = true;
  vm.model = 0;

  /////
  vm.$postLink = () => {
    rbModelDropdownModel($scope, vm, vm.ngModel, $timeout);
    postLink();
  };
  vm.onChange = option => {
    vm.model = +!!option;
    if (isFirstChange) {
      isFirstChange = false;
    } else {
      vm.dirty = true;
    }
    onChange(option);
  };
  vm.onAdd = option => {
    vm.dirty = true;
    _multiSelected.push(option);
    vm.model = _multiSelected.length;
    onAdd(option);
  };
  vm.onRemove = option => {
    _multiSelected = _.filter(_multiSelected, itm => {
      if (_.isEqual(itm, option)) {
        return false;
      }
      return true;
    });
    vm.model = _multiSelected.length;
    onRemove(option);
  };
  $scope.$watch('vm.options', newValue => {
    _multiSelected = [];
    if (_.isArray(newValue) && newValue.length === 0) {
      vm.model = 0;
    }
  });
}

function rbModelDropdownModel(scope, vm, ngModelCtrl, $timeout) {
  if (!ngModelCtrl) {
    return;
  }
  scope.$watchGroup(['vm.required', 'vm.minlength', 'vm.maxlength'], () => {
    if (Object.keys(ngModelCtrl.$error).length) {
      ngModelCtrl.$setValidity(_.keysIn(ngModelCtrl.$error)[0], true);
      $timeout(() => {
        ngModelCtrl.$validate();
      }, 50);
    } else {
      ngModelCtrl.$validate();
    }
  });

  scope.$watch('vm.model', newValue => {
    ngModelCtrl.$setViewValue(newValue);
  });

  scope.$watch('vm.dirty', newValue => {
    if (newValue) {
      ngModelCtrl.$setDirty();
    } else {
      ngModelCtrl.$setPristine();
    }
  });

  // Just available in link phase
  ngModelCtrl.$validators.required = (modelValue, viewValue) => {
    const value = viewValue;
    if (vm.required && !value) {
      vm.hasError = true;
      return false;
    }
    vm.hasError = false;
    return true;
  };

  ngModelCtrl.$validators.maxlength = (modelValue, viewValue) => {
    const value = viewValue;
    if (vm.maxlength && value > vm.maxlength) {
      vm.hasError = true;
      return false;
    }
    vm.hasError = false;
    return true;
  };

  ngModelCtrl.$validators.minlength = (modelValue, viewValue) => {
    const value = viewValue;
    if (vm.minlength && value < vm.minlength) {
      vm.hasError = true;
      return false;
    }
    vm.hasError = false;
    return true;
  };
}
