/**
 * Created by liudong on 2016/12/12.
 */
/**
 * onChange: callback with (page, size) params
 * totalItems
 * size: custom page size, use 20 by default
 * rbDisabled
 */
const templateStr = require('app/components/common/rb-pagination/rb-pagination.html');
angular.module('app.components.common').component('rbPagination', {
  bindings: {
    onChange: '&',
    totalItems: '<',
    size: '<',
    disabled: '<',
  },
  controller: rbPaginationController,
  controllerAs: 'vm',
  template: templateStr,
});

const DEFAULT_SIZE = 20; // some api do not support size

function rbPaginationController($scope, $element) {
  const vm = this;
  vm.onPageChange = onPageChange;

  vm.$onInit = _init;
  vm.$onChanges = _onChanges;
  vm.$postLink = _postLink;
  //////////
  function _init() {
    if (typeof vm.currentPage === 'undefined') {
      vm.currentPage = 1;
    }
  }

  function _onChanges({ totalItems, size }) {
    if (size) {
      vm._size = typeof vm.size === 'undefined' ? DEFAULT_SIZE : vm.size;
    }

    if (totalItems && totalItems.currentValue) {
      vm.maxPage = Math.ceil(totalItems.currentValue / vm._size);
      vm.pagePlaceholder = `1~${vm.maxPage}`;
    }
  }

  function _postLink() {
    $element.find('input').on('keyup', event => {
      const keyCode = event.which || event.keyCode;
      if (keyCode === 13) {
        if (vm.disabled || !vm.pageInput || vm.pageInput > vm.maxPage) {
          return;
        }
        $scope.$evalAsync(() => {
          vm.currentPage = vm.pageInput;
          onPageChange();
        });
      }
    });
  }

  function onPageChange() {
    vm.onChange({ page: vm.currentPage, size: vm.size });
  }
}
