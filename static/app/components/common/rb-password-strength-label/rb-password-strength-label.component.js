const templateStr = require('app/components/common/rb-password-strength-label/rb-password-strength-label.html');
angular.module('app.components.common').component('rbPasswordStrengthLabel', {
  bindings: {
    password: '<',
  },
  controller: rbPasswordStrengthLabelController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbPasswordStrengthLabelController(rbPatternHelper) {
  const vm = this;
  let level = 0;
  vm.$onChanges = _onChanges;
  vm.$onInit = _init;
  vm.isLevelActive = isLevelActive;

  //////////
  function _init() {
    vm.levels = new Array(3);
  }

  function _onChanges({ password }) {
    if (password && password.currentValue) {
      getStrengthColors();
    } else {
      level = 0;
    }
  }

  function getStrengthColors() {
    const strength = rbPatternHelper.getPasswordStrength(vm.password);
    level = 1;
    if (strength > 60) {
      level = 3;
    } else if (strength >= 45) {
      level = 2;
    }
  }

  function isLevelActive(index) {
    const levelIndex = 3 - index;
    return level >= levelIndex;
  }
}
