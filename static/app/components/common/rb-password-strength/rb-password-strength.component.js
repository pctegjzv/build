const templateStr = require('app/components/common/rb-password-strength/rb-password-strength.html');
angular.module('app.components.common').component('rbPasswordStrength', {
  bindings: {
    password: '<',
  },
  controller: rbPasswordStrengthController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbPasswordStrengthController(rbPatternHelper) {
  const vm = this;

  vm.getStrengthLabelAndColor = getStrengthLabelAndColor;
  //////////

  function getStrengthLabelAndColor() {
    const strength = rbPatternHelper.getPasswordStrength(vm.password);

    let label = 'weak';
    let color = '#f00';
    if (strength > 60) {
      label = 'strong';
      color = '#158900';
    } else if (strength >= 45) {
      label = 'medium';
      color = '#e8cb00';
    }
    return { color, label };
  }
}
