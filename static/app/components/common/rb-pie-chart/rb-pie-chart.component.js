/**
 * Created by liudong on 2017/6/20.
 */
import Highcharts from 'highcharts';

const templateStr = require('app/components/common/rb-pie-chart/rb-pie-chart.html');
angular.module('app.components.common').component('rbPieChart', {
  controller: rbPieChartController,
  controllerAs: 'vm',
  template: templateStr,
  bindings: {
    // Series of chart data. Chart will be updated only when the reference is changed
    // e.g: [{
    //   name: 'series name',
    //   y: 200,
    //   color: #123123
    // }]
    series: '<',
    colors: '<',
  },
});

function rbPieChartController($element) {
  const vm = this;
  // let initialized = false;
  // let $chartContainer;
  vm.$onInit = _init;
  vm.$onDestroy = _destroy;
  vm.$onChanges = _onChanges;
  vm.$postLink = _postLink;

  const HIGHCHARTS_OPTIONS = {
    credits: false,
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie',
    },
    title: {
      text: null,
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %',
          style: {
            color:
              (Highcharts.theme && Highcharts.theme.contrastTextColor) ||
              'black',
          },
        },
      },
    },
    // Placeholder
    series: [],
  };

  //////////
  function _init() {}

  function _destroy() {}

  function _onChanges({ series }) {
    if (series && series.currentValue) {
      vm.noData =
        !vm.series ||
        vm.series.every(seriesItem => seriesItem.data.length === 0);
      _redraw();
    }
  }

  function _postLink() {
    const chartsOptions = _.merge({}, HIGHCHARTS_OPTIONS);
    if (vm.colors) {
      Object.assign(chartsOptions, { colors: vm.colors });
    }
    vm.chart = new Highcharts.Chart(_getChartElement()[0], chartsOptions);
    _redraw();
  }

  function _redraw() {
    if (!vm.series || !vm.chart) {
      return;
    }
    // First, remove series that are not in the input
    // Iterate based on the chart ordering
    const itemsToRemove = [];
    vm.chart.series.forEach(chartSeriesItem => {
      if (
        !vm.series.find(seriesItem => chartSeriesItem.name === seriesItem.name)
      ) {
        itemsToRemove.push(chartSeriesItem);
      }
    });
    itemsToRemove.forEach(item => item.remove(false));

    // Second, add or set data based on the input data ordering
    vm.series.forEach(seriesItem => {
      const chartSeriesItem = vm.chart.series.find(_chartSeriesItem => {
        return _chartSeriesItem.name === seriesItem.name;
      });
      if (chartSeriesItem) {
        chartSeriesItem.setData(seriesItem.data, false);
      } else {
        vm.chart.addSeries(seriesItem, false);
      }
    });
    vm.chart.redraw(); // enable redraw animation by default
  }

  function _getChartElement() {
    return $('.pie-highcharts-container', $element);
  }
}
