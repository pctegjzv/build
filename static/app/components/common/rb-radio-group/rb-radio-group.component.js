/**
 * Created by liudong on 16/9/4.
 */
import { each, find, isPlainObject, isEqual } from 'lodash';

import template from 'app/components/common/rb-radio-group/rb-radio-group.html';
/**
 * disabledValues: array containing option values that cannot be selected
 */
angular.module('app.components.common').component('rbRadioGroup', {
  bindings: {
    options: '<',
    htmlKey: '@',
    key: '@',
    value: '@',
    init: '<', // option value to init
    disabledValues: '<', // array of option values
    readonly: '<', // make the component readonly
    onChange: '&',
  },
  controller: rbRadioGroupController,
  controllerAs: 'vm',
  template,
});

function rbRadioGroupController($scope) {
  const vm = this;
  let _disabledValues = '';
  vm.selectedOption = {};

  vm.$onInit = _init;
  vm.$onChanges = _onChanges;
  vm.$doCheck = _doCheck;
  vm.select = select;
  vm._onChange = _onChange;

  ////////

  function _onChange(selection) {
    if (isEqual(selection, vm._lastSelection)) {
      return;
    }
    vm._lastSelection = selection;
    vm.onChange(selection);
  }

  function select(option) {
    if (vm.disabledValues && vm.disabledValues.includes(option[vm.value])) {
      return;
    }
    vm.selectedOption = option;
    vm._onChange({ option: option });
  }

  function _init() {
    if (!vm.key || !vm.value) {
      vm.key = 'key';
      vm.value = 'value';
    }

    $scope.$watchCollection(
      () => {
        return vm.options;
      },
      newValue => {
        if (newValue !== undefined) {
          const options = newValue;
          if (options.length === 0) {
            vm._options = [];
          } else {
            if (!isPlainObject(options[0])) {
              vm._options = _optionsPreProcess(options);
            } else {
              vm._options = options;
            }
          }
          _setDefaultSelection();
        }
      },
    );
  }

  function _onChanges(changes) {
    if (changes.init && changes.init.currentValue) {
      _setDefaultSelection();
    }
  }

  function _doCheck() {
    if (vm.disabledValues && vm.disabledValues.join('') !== _disabledValues) {
      _disabledValues = vm.disabledValues.join('');
      _setDefaultSelection();
    }
  }

  function _setDefaultSelection() {
    if (!vm._options || vm._options.length === 0) {
      return;
    }
    if (vm.init) {
      let _exist = false;
      each(vm._options, option => {
        if (option[vm.value].toString() === vm.init.toString()) {
          vm.selectedOption = option;
          _exist = true;
          return false;
        }
        if (!_exist) {
          vm.selectedOption = find(vm._options, item => {
            return !(
              vm.disabledValues && vm.disabledValues.includes(item[vm.value])
            );
          });
        }
      });
    } else {
      // find first item with _disabled=false or _disabled undefined
      vm.selectedOption = find(vm._options, item => {
        return !(
          vm.disabledValues && vm.disabledValues.includes(item[vm.value])
        );
      });
    }
    vm._onChange({ option: vm.selectedOption });
  }

  function _optionsPreProcess(options) {
    const newOptions = [];
    each(options, option => {
      newOptions.push({
        key: option,
        value: option,
      });
    });
    return newOptions;
  }
}
