import { combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  switchMap,
  throttleTime,
} from 'rxjs/operators';

import { resource_types as RESOURCE_TYPES } from '../../pages/rbac_role/rb-rbac-helper.constant';
/**
 * A group of repository related dropdowns.
 *
 * The purpose of this component is to provide
 * a utility component for repository selection
 * to be used commonly throughout the frontend for re-usability.
 *
 * onRegistryChanged/onProjectChanged/onRepositoryChanged:
 *   Whenever the selection is changed, the three callbacks will be invoked
 *
 * init:
 *   The object to be used to initialize the default selections:
 *   { registry, project, repository } // Registry/project/repository names
 *
 * readonly:
 *   readonly or not
 */
const templateStr = require('app/components/common/rb-repository-dropdown-group/rb-repository-dropdown-group.component.html');
angular.module('app.components.pages').component('rbRepositoryDropdownGroup', {
  controller: rbRepositoryDropdownGroupController,
  controllerAs: 'vm',
  bindings: {
    onRegistryChanged: '&',
    onProjectChanged: '&',
    onRepositoryChanged: '&',
    onLoadingChanged: '&',
    init: '<',
    readonly: '<',
    registryParams: '<',
    repositoryActions: '<',
    requiredRepositoryField: '<',
  },
  template: templateStr,
});

function rbRepositoryDropdownGroupController(
  rbCreateObservableFunction,
  rbSafeApply,
  rbRoleUtilities,
  rbRegistryDataStore,
  rbRegistryCustomDataStore,
  rbRegistryProjectDataStore,
  rbRepositoryDataStore,
) {
  const vm = this;
  const disposers = [];
  let registryDataStore;

  vm.$onInit = onInit;
  vm.$postLink = postLink;
  vm.$onDestroy = onDestroy;
  //////////
  function onInit() {
    vm.onLoadingChanged({ loading: true });
    if (vm.registryParams) {
      registryDataStore = rbRegistryCustomDataStore.getInstance(
        vm.registryParams,
      );
    } else {
      registryDataStore = rbRegistryDataStore;
    }
    if (!vm.repositoryActions) {
      vm.repositoryActions = [];
    }
    _initRegistrySignals();
    _initProjectSignals();
    _initRepositorySignals();

    disposers.push(
      combineLatest(vm.registries$, vm.projects$, vm.repositories$)
        .pipe(map(values => values.every(_value => !!_value)))
        .subscribe(finishedLoading => {
          vm.onLoadingChanged({ loading: !finishedLoading });
        }),
    );
  }

  function postLink() {
    vm.form.selectedRepositoryName.$setDirty();
  }

  function onDestroy() {
    disposers.forEach(disposer => disposer.unsubscribe());
  }

  function _initRegistrySignals() {
    vm.registries$ = registryDataStore.data$;
    vm.registry$ = rbCreateObservableFunction(vm, 'registryChanged');

    disposers.push(
      rbCreateObservableFunction(vm, 'registryDropdownClicked')
        .pipe(throttleTime(10000))
        .subscribe(() => {
          registryDataStore.refetch();
        }),
    );

    disposers.push(
      vm.registry$.subscribe(registry => {
        vm.onRegistryChanged({ value: registry });
        vm.selectedRegistry = registry;
        rbSafeApply();
      }),
    );
  }

  function _initProjectSignals() {
    vm.project$ = rbCreateObservableFunction(vm, 'projectChanged');

    vm.projects$ = vm.registry$.pipe(
      switchMap(registry => {
        return (
          registry &&
          rbRegistryProjectDataStore.getInstance({
            registry_name: registry.name,
          }).data$
        );
      }),
    );

    disposers.push(
      rbCreateObservableFunction(vm, 'projectDropdownClicked')
        .pipe(throttleTime(10000))
        .subscribe(() => {
          rbRegistryProjectDataStore
            .getInstance({ registry_name: vm.selectedRegistry.name })
            .refetch();
        }),
    );

    disposers.push(
      vm.project$.subscribe(project => {
        vm.onProjectChanged({ value: project });
        vm.onRepositoryChanged(undefined);
        vm.selectedProject = project;
        rbSafeApply();
      }),
    );
  }

  function _initRepositorySignals() {
    vm.repository$ = rbCreateObservableFunction(vm, 'repositoryChanged');

    vm.repositories$ = vm.project$.pipe(
      switchMap(
        project =>
          project &&
          rbRepositoryDataStore
            .getInstance({
              registry_name: vm.selectedRegistry.name,
              project_name: project.project_name,
            })
            .data$.pipe(
              filter(repositories => !!repositories),
              map(repositories =>
                repositories.filter(item =>
                  _.every(vm.repositoryActions, action =>
                    rbRoleUtilities.resourceHasPermission(
                      item,
                      RESOURCE_TYPES.REPOSITORY,
                      action,
                    ),
                  ),
                ),
              ),
            ),
      ),
    );

    disposers.push(
      rbCreateObservableFunction(vm, 'repositoryDropdownClicked')
        .pipe(throttleTime(30000))
        .subscribe(() => {
          rbRepositoryDataStore
            .getInstance({
              registry_name: vm.selectedRegistry.name,
              project_name:
                vm.selectedProject && vm.selectedProject.project_name,
            })
            .refetch();
        }),
    );

    disposers.push(
      vm.repository$.pipe(distinctUntilChanged()).subscribe(repository => {
        vm.onRepositoryChanged({ value: repository });
        vm.selectedRepository = repository;
      }),
    );
  }
}
