const templateStr = require('app/components/common/rb-search-dropdown/rb-search-dropdown.html');

angular.module('app.components.common').component('rbSearchDropdown', {
  controller: rbSearchDropdownController,
  controllerAs: 'vm',
  template: templateStr,
  bindings: {
    placeholder: '<', // string
    backendUrl: '<', // string
    fieldsSetting: '<', // object
    responseItemFilter: '<', // function used to filter backend results item
    objectOptionEnabled: '<', // boolean
    multiple: '<', // boolean
    onChange: '&', // funciton ref
    onAdd: '&', // function ref
    onRemove: '&', // function ref
  },
});

function rbSearchDropdownController(
  $element,
  $timeout,
  $log,
  translateService,
) {
  const vm = this;
  let $dropdown;
  let backendUrl;
  let optionResults = [];

  vm.$onInit = _init;
  vm.$postLink = _postLink;
  vm.$onDestroy = _destroy;

  ///////////
  function _init() {
    vm.placeholder =
      vm.placeholder || translateService.get('currently_no_options');
    // TODO: enable custom API search field (name, displayName, ...)
    if (vm.backendUrl.includes('?')) {
      backendUrl = `${vm.backendUrl}&search={query}`;
    } else {
      backendUrl = `${vm.backendUrl}?search={query}`;
    }
  }

  function _postLink() {
    $dropdown = $element.find('.ui.dropdown');
    const settings = {
      placeholder: 'string',
      message: {
        noResults: translateService.get('dropdown_no_results_found'),
      },
      apiSettings: {
        headers: {
          'RUBICK-AJAX-REQUEST': true,
        },
        cache: false,
        throttle: 800,
        url: backendUrl,
        onResponse: response => {
          const remoteValuesField = _.get(
            vm.fieldsSetting,
            'remoteValues',
            'results',
          );
          const results = response[remoteValuesField];
          if (vm.responseItemFilter) {
            response[remoteValuesField] = results.filter(vm.responseItemFilter);
          }
          _saveOptionResults(results);
          return response;
        },
      },
      saveRemoteData: false,
      onShow: () => {
        $element.addClass('active');
      },
      onHide: () => {
        $element.removeClass('active');
      },
    };
    if (vm.fieldsSetting) {
      settings.fields = vm.fieldsSetting;
    }
    if (vm.multiple) {
      settings.onAdd = value => {
        _onAdd(value);
      };
      settings.onRemove = value => {
        _onRemove(value);
      };
    } else {
      settings.onChange = value => {
        _onChange(value);
      };
    }
    $timeout(() => {
      $dropdown.dropdown(settings);
    }, 200);
  }

  function _onChange(value) {
    if (vm.objectOptionEnabled) {
      const option = _getOption(value);
      if (!option) {
        $log.error('could not find changed option');
        return;
      }
      vm.onChange({ option });
    } else {
      vm.onChange({ value });
    }
  }

  function _onAdd(value) {
    if (vm.objectOptionEnabled) {
      const option = _getOption(value);
      if (!option) {
        $log.error('could not find added option');
        return;
      }
      vm.onAdd({ option });
    } else {
      vm.onAdd({ value });
    }
  }

  function _onRemove(value) {
    if (vm.objectOptionEnabled) {
      const option = _getOption(value);
      if (!option) {
        $log.error('could not find removed option');
        return;
      }
      vm.onRemove({ option });
    } else {
      vm.onRemove({ value });
    }
  }

  function _saveOptionResults(results) {
    if (!vm.objectOptionEnabled) {
      return;
    }
    optionResults = results;
  }

  function _getOptionValueField() {
    return _.get(vm.fieldsSetting, 'value', 'value');
  }

  function _getOption(value) {
    const valueField = _getOptionValueField();
    return optionResults.find(option => {
      return option[valueField] === value;
    });
  }

  function _destroy() {}
}
