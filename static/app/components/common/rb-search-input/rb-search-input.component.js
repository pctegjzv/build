/**
 * - Simplest usage example:
 *
 * When you click the search button or press enter key, it will pass the input value to your search callback.
 * <rb-search-input
 * on-search="vm.searchByMyQeueryString(value)"
 * ></rb-search-input>
 *
 * - If you pass the loading param at right time, the searh button will become a loading style and prevent search again
 * when the pre server response doesn't return.
 * So you'd better binding the loading as:
 *
 * - SUGGESTED simplest usage:
 * <rb-search-input
 * on-search="vm.searchByMyQeueryString(value)"
 * loading="vm.isSearchingNow"
 * ></rb-search-input>
 */
const templateStr = require('app/components/common/rb-search-input/rb-search-input.html');
angular.module('app.components.common').component('rbSearchInput', {
  bindings: {
    keycode: '<', // The same usage as rb-key-code, default is 13
    searchOnChange: '<', // Bool, optional, true will auto search on keyword change, false won't
    delay: '<', // auto search time delay, default is 1000 ms
    onSearch: '&', // **REQUIRED**  your searching function
    placeholder: '@', // default is 'keyword'
    loading: '<', // passing true on loading, passing false on server response
  },
  controller: rbSearchInputController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbSearchInputController() {
  const vm = this;
  vm.$onInit = _onInit;
  vm.search = search;
  /////
  function _onInit() {
    vm.keycode = vm.keycode || 13;
    if (vm.searchOnChange) {
      vm.delay = vm.delay || 1000;
      vm.onInputChange = _.debounce(vm.search, vm.delay);
    }
  }

  function search(isActive) {
    vm.rbsearchInput.$setSubmitted();
    if (vm.rbsearchInput.$invalid) {
      return;
    }
    if (vm.searchOnChange) {
      if (
        vm.preIsActive &&
        !isActive &&
        Date.now() - vm.preCallTime <= vm.delay
      ) {
        // Just call once before the key auto changing within delay time
        return;
      }
      vm.preIsActive = isActive;
      vm.preCallTime = Date.now();
    }
    if (!vm.loading) {
      vm.onSearch({ value: vm.value });
    }
  }
}
