import LinksLayouter from './rb-service-links-layouter';
import LinksPainter from './rb-service-links-painter';
import {
  DEPLOYING_STATUS_NAMES,
  RUNNING_STATUS_NAMES,
  ERROR_STATUS_NAMES,
} from 'app/components/utility/rb-status-names.constant';
import { INSTANCE_SIZE_INFO } from 'app/components/pages/app_service/rb-service.constant';

const templateStr = require('app/components/common/rb-service-links-graph/rb-service-links-graph.html');
angular.module('app.components.common').component('rbServiceLinksGraph', {
  bindings: {
    services: '<',
    appName: '<',
  },
  controller: rbServiceLinksGraphController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbServiceLinksGraphController(
  $element,
  $state,
  translateService,
  rbBytesFormatterFilter,
) {
  const vm = this;

  let linksLayouter, linksPainter, instanceSizeInfo, cpuUnit;

  vm.$onChanges = _changes;
  vm.$postLink = _postLink;
  vm.$onDestroy = _destroy;
  vm.zoomIn = zoomIn;
  vm.zoomOut = zoomOut;
  vm.resetGraph = resetGraph;

  /////

  function _changes({ services }) {
    if (services && linksLayouter && linksPainter) {
      paintLinksGraph(services.currentValue);
    }
  }

  function _postLink() {
    vm.graphScale = 1;
    cpuUnit = translateService.get('unit_cpu');
    instanceSizeInfo = JSON.parse(
      JSON.stringify(INSTANCE_SIZE_INFO).replace(/cpuUnit/g, cpuUnit),
    );
    linksLayouter = new LinksLayouter();
    const ele = $element.find('.canvas-container')[0];
    linksPainter = new LinksPainter(ele);
    const containerWidth = parseInt(getComputedStyle(ele).width);
    linksPainter.setCanvasWidth(Math.min(containerWidth, 1920));
    linksPainter.addEventListener('nodeclick', node => {
      const clickedService = vm.services.find(
        service => service.service_name === node.id,
      );
      goToService(clickedService);
    });
    paintLinksGraph(vm.services);
  }

  function _destroy() {
    linksPainter.destroy();
  }

  function zoomIn() {
    vm.graphScale += 0.1;
    linksPainter.setScale(vm.graphScale);
  }

  function zoomOut() {
    vm.graphScale -= 0.1;
    linksPainter.setScale(vm.graphScale);
  }

  function resetGraph() {
    vm.graphScale = 1;
    linksPainter.resetTranslation();
  }

  function paintLinksGraph(services) {
    if (!(services && services.length)) {
      return;
    }

    requestAnimationFrame(() => {
      const nodesData = genNodesData(services);
      const edgesData = genEdgesData(services);

      linksLayouter.setData(nodesData, edgesData);
      linksLayouter.layout();
      const graph = linksLayouter.getGraph();
      const nodes = linksLayouter.getNodes();
      const edges = linksLayouter.getEdges();
      linksLayouter.clear();

      linksPainter.setRealWidth(graph.width);
      linksPainter.setRealHeight(graph.height);
      linksPainter.setCanvasHeight(Math.min(Math.max(graph.height, 300), 780));
      linksPainter.render(nodes, edges);
    });
  }

  function genNodesData(services) {
    return services.map(service => {
      let instanceSize;
      if (instanceSizeInfo[service.instance_size]) {
        instanceSize = instanceSizeInfo[service.instance_size];
      } else {
        const size = service.custom_instance_size;
        instanceSize = {
          cpu: `${size.cpu} ${cpuUnit}`,
          mem: rbBytesFormatterFilter(size.mem * 1024 * 1024),
        };
      }
      const healthyNumInstances = service.healthy_num_instances || 0;
      return {
        name: service.service_name,
        status: getServiceStatus(service.current_status),
        insNum: `${healthyNumInstances}/${service.target_num_instances}`,
        insSize: `${instanceSize.cpu} ${instanceSize.mem}`,
        insStatus:
          healthyNumInstances < service.target_num_instances
            ? 'error'
            : 'running',
      };
    });
  }

  function genEdgesData(services) {
    const links = services.map(service => service.linked_to_services);
    return []
      .concat(...links)
      .filter(link =>
        services.find(service => service.service_name === link.dest),
      )
      .map(link => ({
        from: link.dest,
        to: link.src,
        alias: link.dest_alias,
        status: getServiceStatus(
          services.find(service => service.service_name === link.src)
            .current_status,
        ),
      }));
  }

  function getServiceStatus(status) {
    const statusName = status.toLowerCase();
    if (DEPLOYING_STATUS_NAMES.includes(statusName)) {
      return 'deploying';
    } else if (RUNNING_STATUS_NAMES.includes(statusName)) {
      return 'running';
    } else if (ERROR_STATUS_NAMES.includes(statusName)) {
      return 'error';
    } else {
      return 'stopped';
    }
  }

  function goToService(service) {
    $state.go('app_service.service.service_detail', {
      service_name: service.uuid,
    });
  }
}
