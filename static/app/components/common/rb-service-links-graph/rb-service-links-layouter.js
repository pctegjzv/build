import dagre from 'dagre';

const style = {
  node: {
    width: 200,
    height: 130,
    lineWidth: 3,
    radius: 8,
    divide: 2,
    opacity: 0.03,
  },
  // 服务名
  title: {
    fontSize: 20,
    color: '#262626',
    marginTop: 30,
  },
  // 实例数量
  tag: {
    width: 60,
    height: 24,
    fontSize: 14,
    color: '#FFFFFF',
  },
  // 实例大小
  insSize: {
    lineHeight: 36,
    fontSize: 18,
    color: '#BDBDBD',
  },
  edge: {
    fontSize: 18,
    textColor: '#D3D3D3',
  },
  primaryColor: {
    stopped: '#B2B2B2',
    running: '#72C63D',
    deploying: '#0BADEB',
    error: '#FC5D68',
  },
};

/**
 * 使用时: 设置数据(set*Data) -> 计算布局(layout) -> 获取布局结果(get*),根据需要清除旧数据(clear)
 */
class LinksLayouter {
  constructor() {
    this.g = new dagre.graphlib.Graph();
    // 配置布局参数(https://github.com/cpettitt/dagre/wiki#configuring-the-layout)
    this.g.setGraph({
      rankdir: 'BT',
      nodesep: 100,
      ranksep: 150,
      edgesep: 50,
      marginx: 40,
      marginy: 40,
      width: style.node.width,
      height: style.node.height,
    });
  }

  setNodesData(nodesData) {
    nodesData.forEach(node => {
      this.g.setNode(node.name, {
        id: node.name,
        data: node,
        width: style.node.width,
        height: style.node.height,
      });
    });
  }

  setEdgesData(edgesData) {
    edgesData.forEach(edge => {
      this.g.setEdge(edge.from, edge.to, {
        width: style.node.lineWidth,
        height: style.node.lineWidth,
        alias: edge.alias,
        status: edge.status,
      });
    });
  }

  setData(nodesData, edgesData) {
    this.setNodesData(nodesData);
    this.setEdgesData(edgesData);
  }

  layout() {
    if (this.g.edges().length > 0) {
      dagre.layout(this.g);
    } else {
      const disX = 60;
      const disY = 60;
      const marginX = 40;
      const marginY = 40;
      const num = 4;
      const nodeWidth = style.node.width;
      const nodeHeight = style.node.height;
      const nodes = this.g.nodes();
      nodes.forEach((nodeId, index) => {
        const node = this.g.node(nodeId);
        const i = (index % num) + 1;
        const j = Math.floor(index / num) + 1;
        node.x = marginX + nodeWidth * i + disX * (i - 1) - nodeWidth / 2;
        node.y = marginY + nodeHeight * j + disY * (j - 1) - nodeHeight / 2;
      });
      const numX = nodes.length < num ? nodes.length : num;
      const numY = Math.ceil(nodes.length / num);
      this.g.graph().width = marginX * 2 + nodeWidth * numX + disX * (numX - 1);
      this.g.graph().height =
        marginY * 2 + nodeHeight * numY + disY * (numY - 1);
    }
  }

  clear() {
    this.g.nodes().forEach(node => {
      this.g.removeNode(node);
    });
    this.g.edges().forEach(edge => {
      this.g.removeEdge(edge);
    });
  }

  // 返回画布宽高对象
  getGraph() {
    return this.g.graph();
  }

  getNodes() {
    return this.g
      .nodes()
      .map(nodeId => this.g.node(nodeId))
      .map(node => {
        const layout = {
          stroke: style.node.lineWidth,
          radius: style.node.radius,
          opacity: style.node.opacity,
          primaryColor: style.primaryColor[node.data.status],
          title: this._calcTitleLayout(node.x, node.y, node.width, node.height),
          tag: this._calcTagLayout(node.x, node.y, node.data.insStatus),
          divide: this._calcDivideLayout(
            node.x,
            node.y,
            node.width,
            node.height,
          ),
          insSize: this._calcInsSizeLayout(
            node.x,
            node.y,
            node.width,
            node.height,
          ),
        };
        return Object.assign(node, layout);
      });
  }

  getEdges() {
    return this.g
      .edges()
      .map(edgeId => this.g.edge(edgeId))
      .map(edge => {
        const layout = {
          color: style.primaryColor[edge.status],
          arrow: this._calcEdgeArrow(edge.points),
          label: this._calcEdgeLabel(edge.points[1].x, edge.points[1].y),
        };
        return Object.assign(edge, layout);
      });
  }

  _calcTagLayout(nodeX, nodeY, status) {
    return {
      x: nodeX,
      y: nodeY,
      width: style.tag.width,
      height: style.tag.height,
      radius: style.tag.height / 2,
      backgroundColor: style.primaryColor[status],
      fontSize: style.tag.fontSize,
      color: style.tag.color,
    };
  }

  _calcDivideLayout(nodeX, nodeY, nodeWidth, nodeHeight) {
    const x1 = nodeX - nodeWidth / 2;
    const x2 = nodeX + nodeWidth / 2;
    const y = nodeY + nodeHeight / 2 - style.insSize.lineHeight;
    return { x1, y1: y, x2, y2: y, linewidth: style.node.divide };
  }

  _calcTitleLayout(nodeX, nodeY, nodeWidth, nodeHeight) {
    return {
      x: nodeX,
      y: nodeY - nodeHeight / 2 + style.title.marginTop,
      fontSize: style.title.fontSize,
      color: style.title.color,
    };
  }

  _calcInsSizeLayout(nodeX, nodeY, nodeWidth, nodeHeight) {
    return {
      x: nodeX,
      y: nodeY + nodeHeight / 2 - style.insSize.fontSize,
      fontSize: style.insSize.fontSize,
      color: style.insSize.color,
    };
  }

  _calcEdgeLabel(x, y) {
    return {
      x: x + 10,
      y: y,
      color: style.edge.textColor,
      fontSize: style.edge.fontSize,
    };
  }

  _calcEdgeArrow(points) {
    const width = 10;
    const height = 16;
    const sunken = 5;
    const a1 = [0, 0];
    const a2 = [width, -height];
    const a3 = [0, sunken - height];
    const a4 = [-width, -height];

    return {
      // 向下的箭头的坐标，
      points: [...a1, ...a2, ...a3, ...a4],
      // 箭头的旋转角度
      rotation: this._calcRotation(points[0], points[1]),
    };
  }

  _calcRotation(point1, point2) {
    const x = Math.abs(point1.x - point2.x);
    const y = Math.abs(point1.y - point2.y);
    const z = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    const cos = y / z;
    const rotation = Math.acos(cos);
    return point1.x <= point2.x ? rotation : -rotation;
  }
}

export default LinksLayouter;
