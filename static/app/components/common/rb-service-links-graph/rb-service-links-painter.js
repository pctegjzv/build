import { fromEvent, merge } from 'rxjs';
import {
  filter,
  map,
  switchMap,
  takeUntil,
  throttleTime,
} from 'rxjs/operators';

import buildTwo from './two-factory';

class LinksPainter {
  constructor(container = 'body') {
    if (typeof container === 'string') {
      container = document.querySelector(container);
    }
    this.two = buildTwo().appendTo(container);
    this.canvas = container.querySelector('canvas');

    this.subscriptions = [];

    this.events = {
      nodeclick: new Map(),
      nodehover: new Map(),
    };
    const clickSub = fromEvent(this.canvas, 'click').subscribe(
      this.clickedHandler.bind(this),
    );
    const mouseMoveSub = fromEvent(this.canvas, 'mousemove')
      .pipe(throttleTime(1000 / 60))
      .subscribe(this.hoverHandler.bind(this));
    this.subscriptions.push(clickSub, mouseMoveSub);

    this.shapes = [];
    this._addDragListener();
  }

  clickedHandler(event) {
    const clickedNode = this.calcPosition(event);
    if (clickedNode) {
      this.events.nodeclick.forEach(handler => {
        handler(clickedNode);
      });
    }
  }

  hoverHandler(event) {
    const hoverNode = this.calcPosition(event);
    if (hoverNode) {
      this.canvas.style.cursor = 'pointer';
      this.canvas.title = hoverNode.id;
      this.events.nodehover.forEach(handler => {
        handler(hoverNode);
      });
    } else {
      this.canvas.style.cursor = 'move';
      this.canvas.title = '';
    }
  }

  destroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  _addDragListener() {
    const mouseDown$ = fromEvent(this.canvas, 'mousedown');
    const mouseMove$ = fromEvent(this.canvas, 'mousemove').pipe(
      throttleTime(1000 / 60),
    );
    const mouseUp$ = fromEvent(this.canvas, 'mouseup');
    const mouseLeave$ = fromEvent(this.canvas, 'mouseleave');
    const dragSub = mouseDown$
      .pipe(
        filter(event => !this.calcPosition(event)),
        map(event => ({
          initTranslation: {
            x: this.canvasGroup.translation.x,
            y: this.canvasGroup.translation.y,
          },
          initEvent: event,
        })),
        switchMap(({ initTranslation, initEvent }) =>
          mouseMove$.pipe(
            takeUntil(merge(mouseUp$, mouseLeave$)),
            map(event => ({
              x: event.clientX - initEvent.clientX + initTranslation.x,
              y: event.clientY - initEvent.clientY + initTranslation.y,
            })),
          ),
        ),
      )
      .subscribe(({ x, y }) => {
        this.canvasGroup.translation.set(x, y);
        this.two.update();
      });
    this.subscriptions.push(dragSub);
  }

  drawNode(node) {
    const backgroundRect = this.two.makeRoundedRectangle(
      node.x,
      node.y,
      node.width,
      node.height,
      node.radius,
    );
    const borderRect = backgroundRect.clone();
    backgroundRect.fill = node.primaryColor;
    backgroundRect.opacity = node.opacity;
    borderRect.fill = 'none';
    borderRect.stroke = node.primaryColor;
    borderRect.linewidth = node.stroke;

    const title = node.title;
    const nodeName =
      node.data.name.length > 15
        ? node.data.name.slice(0, 15) + '...'
        : node.data.name;
    const titleText = this.two.makeText(nodeName, title.x, title.y);
    titleText.size = title.fontSize;
    titleText.fill = title.color;

    const tag = node.tag;
    const tagRect = this.two.makeRoundedRectangle(
      tag.x,
      tag.y,
      tag.width,
      tag.height,
      tag.radius,
    );
    tagRect.fill = tag.backgroundColor;
    tagRect.stroke = 'none';

    const tagText = this.two.makeText(node.data.insNum, tag.x, tag.y);
    tagText.size = tag.fontSize;
    tagText.fill = tag.color;

    const divide = node.divide;
    const divideLine = this.two.makeLine(
      divide.x1,
      divide.y1,
      divide.x2,
      divide.y2,
    );
    divideLine.linewidth = divide.linewidth;
    divideLine.stroke = node.primaryColor;

    const insSize = node.insSize;
    const insSizeText = this.two.makeText(
      node.data.insSize,
      insSize.x,
      insSize.y,
    );
    insSizeText.size = insSize.fontSize;
    insSizeText.fill = insSize.color;

    this.shapes.push(
      backgroundRect,
      borderRect,
      titleText,
      tagRect,
      tagText,
      divideLine,
      insSizeText,
    );
  }

  drawEdge(edge) {
    const points = [].concat(...edge.points.map(point => [point.x, point.y]));
    const edgeCurve = this.two.makeCurve(...points, true);
    edgeCurve.stroke = edge.color;
    edgeCurve.fill = 'none';
    edgeCurve.linewidth = edge.width;

    const arrow = edge.arrow;
    const arrowPath = this.two.makePath(...arrow.points, false);
    arrowPath.fill = edge.color;
    arrowPath.stroke = 'none';
    const arrowGroup = this.two.makeGroup(arrowPath);
    arrowGroup.translation.set(points[0], points[1]);
    arrowGroup.rotation = arrow.rotation;

    const label = edge.label;
    const labelText = this.two.makeText(edge.alias, label.x, label.y);
    labelText.size = label.fontSize;
    labelText.fill = label.color;
    labelText.alignment = 'left';

    this.shapes.push(edgeCurve, arrowGroup, labelText);
  }

  render(nodes, edges) {
    this.nodes = nodes;
    this.edges = edges;

    this.two.clear();
    this.shapes = [];
    const preGroup = this.canvasGroup;

    this.edges.forEach(edge => this.drawEdge(edge));
    this.nodes.forEach(node => this.drawNode(node));
    const shapesGroup = this.two.makeGroup(...this.shapes);
    shapesGroup.translation.set(-this.realWidth / 2, 0);
    this.canvasGroup = this.two.makeGroup(shapesGroup);
    if (preGroup) {
      this.canvasGroup.scale = preGroup.scale;
      this.canvasGroup.translation.set(
        preGroup.translation.x,
        preGroup.translation.y,
      );
    } else {
      this.canvasGroup.scale = 1;
      this.canvasGroup.translation.set(this.canvasWidth / 2, 0);
    }

    this.two.update();
  }

  // 设置画布宽高
  setCanvasWidth(width) {
    this.two.width = width;
    this.canvasWidth = width;
  }

  setCanvasHeight(height) {
    this.two.height = height;
    this.canvasHeight = height;
  }

  setRealWidth(width) {
    this.realWidth = width;
  }

  setRealHeight(height) {
    this.realHeight = height;
  }

  setScale(scale) {
    this.canvasGroup.scale = scale;
    this.two.update();
  }

  resetTranslation() {
    this.canvasGroup.translation.set(this.canvasWidth / 2, 0);
    this.canvasGroup.scale = 1;
    this.two.update();
  }

  addEventListener(event, handler) {
    this.events[event].set(handler, handler);
    return handler;
  }

  removeEventListener(event, handler) {
    if (handler) {
      this.events[event].delete(handler);
    } else {
      this.events[event].clear();
    }
  }

  calcPosition(event) {
    if (!this.canvasGroup) {
      return false;
    }

    const x = event.offsetX;
    const y = event.offsetY;
    const scale = this.canvasGroup.scale;
    const transformX =
      this.canvasGroup.translation.x - (this.realWidth / 2) * scale;
    const transformY = this.canvasGroup.translation.y;
    return this.nodes.find(node => {
      const w = (node.width / 2) * scale;
      const h = (node.height / 2) * scale;
      const x1 = node.x * scale - w + transformX;
      const x2 = node.x * scale + w + transformX;
      const y1 = node.y * scale - h + transformY;
      const y2 = node.y * scale + h + transformY;
      return x1 < x && x < x2 && (y1 < y && y < y2);
    });
  }
}

export default LinksPainter;
