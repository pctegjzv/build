/**
 * https://two.js.org/#documentation
 * this.two.js 内部this必须指向全局作用域，同时禁用AMD
 * two.js 会导致内存泄漏，采用单例模式减少内存占用
 */
import Two from 'imports-loader?define=>false,this=>window!two.js';

function twoFactory() {
  let two;
  return () => {
    if (two) {
      return two;
    } else {
      // 设置渲染方式，svg渲染文字会向上偏移，所以使用canvas
      const twoType = Two.Types['canvas'];
      return (two = new Two({ type: twoType }));
    }
  };
}

export default twoFactory();
