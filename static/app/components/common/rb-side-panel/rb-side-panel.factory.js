const _ = require('lodash');
const ResizeSensor = require('css-element-queries/src/ResizeSensor');

angular
  .module('app.components.common')
  .factory('rbSidePanel', rbSidePanelFactory);

/**
 * Side panel factory to show a side panel either on the bottom or on the right.
 *
 * The basic use is to feed in a component name by invoking 'open' method.
 * This factory will construct and manage the lifecycle of the compiled component for you.
 *
 * e.g.:
 *   rbSidePanel.show({
 *     component: 'someRandomComponent',
 *     locals: {
 *       someAttributes: ...
 *     }
 *   })
 */
function rbSidePanelFactory($rootScope, $timeout, $compile) {
  let sidePanelContainer = null;
  let sidePanelAnchor = null;
  let pageScorllContainer = null;

  const SUPPORTED_POSITIONS = ['bottom', 'right'];
  const DEFAULT_SIZE = { bottom: 200, right: 500 };
  let _componentElement = null;
  let _resizeSensor = null;
  let _isolateScope = null;

  const _debouncedResizePageComponentContainer = _.throttle(
    _resizePageComponentContainer,
    100,
  );

  _init();

  return {
    open,
    close,
    get atBottom() {
      return _isSidePanelBottom();
    },
    get isShowing() {
      return _isShowing();
    },
    reposition,
  };

  ////////
  function open({
    position = 'bottom', // bottom / right
    component, // component string, can either be `rbSomeComponent` or `rb-some-component`
    size, // refers to height when position is bottom, width when position is right
    locals = {}, // values to be passed to the components
    resizable = true, // whether or not to should the resize bar
  }) {
    if (!SUPPORTED_POSITIONS.includes(position)) {
      throw new Error('Only supports bottom/right side panel at the moment!');
    }

    // Release previous component
    if (_isShowing()) {
      _release();
    }

    // Prepare component and append to DOM
    _isolateScope = $rootScope.$new(true);
    _.assign(_isolateScope, locals);
    _componentElement = $compile(_generateComponentTemplate(component, locals))(
      _isolateScope,
    );
    sidePanelAnchor.append(_componentElement);

    reposition({ position, size, resizable });

    // Decorate the container with component name for custom styling
    sidePanelContainer.attr('component', component);
  }

  function close() {
    _release();
    sidePanelContainer.css('display', 'none');
    _debouncedResizePageComponentContainer();
  }

  function _release() {
    _componentElement && _componentElement.remove();
    _isolateScope && _isolateScope.$destroy();
    _resizeSensor && _resizeSensor.detach();
  }

  function reposition({ position, size, resizable }) {
    // Set panel position globally
    const positionChanged =
      sidePanelContainer.attr('panel-position') !== position;
    sidePanelContainer.attr('panel-position', position);
    if (!size) {
      size = Math.max(
        !positionChanged ? _getCurrentContainerSize() : 0,
        DEFAULT_SIZE[position],
      );
    }

    const widthOrHeight = _isSidePanelBottom() ? 'width' : 'height';
    pageScorllContainer.css(widthOrHeight, '100%');

    // Apply default size for container and make it visible
    sidePanelContainer.css({
      display: 'block',
      [_isSidePanelBottom() ? 'height' : 'width']: size + 'px',
      [_isSidePanelBottom() ? 'width' : 'height']: '100%',
    });

    if (resizable) {
      sidePanelContainer.addClass('resizable');
    } else {
      sidePanelContainer.removeClass('resizable');
    }

    // Attach listeners to adjust the main page container size
    _resizeSensor && _resizeSensor.detach();
    _resizeSensor = new ResizeSensor(sidePanelContainer, () => {
      _debouncedResizePageComponentContainer();
    });
    _debouncedResizePageComponentContainer();
  }

  // e.g.:
  // _generateComponentTemplate('asdAsdA', {a: 123, b: 123, cAsdS: 456})
  //   => "<asd-asd-a a="a" b="b" c-asd-s="cAsdS"></asd-asd-a>"
  function _generateComponentTemplate(component, locals) {
    const componentTag = _.kebabCase(component);
    const attrs = Object.keys(locals)
      .map(attrName => `${_.kebabCase(attrName)}="${attrName}"`)
      .join(' ');
    return `<${componentTag} ${attrs}></${componentTag}>`;
  }

  function _resizePageComponentContainer() {
    const widthOrHeight = _isSidePanelBottom() ? 'height' : 'width';
    pageScorllContainer.css(
      widthOrHeight,
      `calc(100% - ${_getCurrentContainerSize()}px)`,
    );

    // For right side panel, we should have some tricks around the container size.
    if (!_isSidePanelBottom()) {
      $('#rb-page-component-container').css(
        'width',
        `calc(100% + ${_getCurrentContainerSize()}px)`,
      );
    }
  }

  function _isSidePanelBottom() {
    return sidePanelContainer.attr('panel-position') !== 'right';
  }

  function _getCurrentContainerSize() {
    if (!_isShowing()) {
      return 0;
    } else {
      return _isSidePanelBottom()
        ? sidePanelContainer.outerHeight()
        : sidePanelContainer.outerWidth();
    }
  }

  function _isShowing() {
    return sidePanelContainer && sidePanelContainer.css('display') !== 'none';
  }

  function _init() {
    $timeout(() => {
      sidePanelContainer = $('.page-side-panel-container');
      sidePanelAnchor = $('.page-side-panel-container-anchor');
      pageScorllContainer = $('.page-scroll-container');
    });
  }
}
