const templateStr = require('app/components/common/rb-status-badge/rb-status-badge.html');
angular.module('app.components.common').component('rbStatusBadge', {
  bindings: {
    status: '<',
  },
  controller: rbStatusBadgeController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbStatusBadgeController(translateService) {
  const vm = this;

  vm.$onInit = _init;

  function _init() {
    vm.statusText = getStatusText(vm.status);
  }

  function getStatusText(status) {
    return translateService.get(`status_${status.toLowerCase()}`);
  }
}
