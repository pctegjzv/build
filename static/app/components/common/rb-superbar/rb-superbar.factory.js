/**
 * Created by liudong on 2017/3/3.
 * <div class="rb-page-heading">
 *   <div class="rb-superbar-content">
 *     ...
 *   </div>
 * </div>
 *
 * 1. add `rb-superbar-content` class for tool bar content area and apply your custom style:
 * 2. use `register` to initialize superbar:  rbSuperbar.register($('.rb-page-heading'));
 * 3. rbSuperbar will apply fixed position and custom background for $target element $('.rb-page-heading')
 * 4. use `detroy` in component $destroy
 *
 */
import _ from 'lodash';
const ResizeSensor = require('css-element-queries/src/ResizeSensor');

angular.module('app.components.common').factory('rbSuperbar', rbSuperbar);

function rbSuperbar() {
  let $target, $container;
  let height,
    containerPaddingTop,
    targetMarginBottom,
    targetOffsetTop,
    scrollbarWidth,
    pageContainerWidth;
  let initialized = false;
  let scrollListener, resizeSensor;
  const globalOptions = {
    targetPadding: 0,
  };

  const $pageContainer = $('.page-scroll-container');

  return {
    register,
    destroy,
  };

  /**
   * https://github.com/mleibman/SlickGrid/blob/master/slick.grid.js#L378
   * @returns {number|*}
   */
  function getScrollbarWidth() {
    if (scrollbarWidth) {
      return scrollbarWidth;
    }
    const $c = $(
      '<div style="position:absolute; top:-10000px; left:-10000px; width:100px; height:100px; overflow:scroll;"></div>',
    ).appendTo('body');
    scrollbarWidth = $c.width() - $c[0].clientWidth;
    $c.remove();
  }

  /**
   *
   * @param target
   * @param options: targetPadding(equals to target's padding-left and padding-right)
   */
  function register(target, options) {
    if (initialized) {
      return;
    }
    Object.assign(globalOptions, options);
    $target = target;
    $container = $target.parent();

    containerPaddingTop = parseInt($container.css('padding-top'));
    targetOffsetTop = $target.css('top');
    pageContainerWidth = $pageContainer.width();
    scrollListener = _.throttle(() => {
      const scrollTop = $pageContainer.scrollTop();
      if (scrollTop > 8) {
        _showSuperbar();
      } else {
        _hideSuperbar();
      }
    }, 50);
    _init();
  }

  function destroy() {
    $target = $container = null;
    $pageContainer.off('scroll', scrollListener);
    resizeSensor.detach();
  }

  function _init() {
    getScrollbarWidth();
    resizeSensor = new ResizeSensor($pageContainer, () => {
      pageContainerWidth = $pageContainer.width();
      initialized &&
        $target.width(
          pageContainerWidth -
            36 -
            scrollbarWidth -
            2 * globalOptions.targetPadding,
        );
    });
    $pageContainer.on('scroll', scrollListener);
  }

  function _showSuperbar() {
    if (initialized) {
      return;
    }
    if (!height) {
      height = $target.height();
      targetMarginBottom = parseInt($target.css('margin-bottom'));
    }
    $target.attr('id', 'rb-superbar');
    $target.addClass('rb-superbar');
    $target.width(
      pageContainerWidth -
        36 -
        scrollbarWidth -
        2 * globalOptions.targetPadding,
    );
    $target.css(
      'top',
      $('.page-container-header').outerHeight() +
        $('rc-header-toolbar').outerHeight(),
    );
    $container.addClass('rb-superbar-container');
    !initialized &&
      $container.css(
        'padding-top',
        height + targetMarginBottom + containerPaddingTop + 'px',
      );
    initialized = true;
  }

  function _hideSuperbar() {
    if (!initialized) {
      return;
    }
    $target.attr('id', '');
    $target.removeClass('rb-superbar');
    $target.css({
      width: '100%',
      top: targetOffsetTop,
    });
    $container.css('padding-top', containerPaddingTop + 'px');
    $container.removeClass('rb-superbar-container');
    initialized = false;
  }
}
