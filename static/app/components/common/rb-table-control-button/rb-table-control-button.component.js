/**
 * Created by zhouwenbo on 2017/7/11.
 */
const {
  TABLE_CONTROL_BUTTON_ICON,
} = require('./rb-table-control-button.constant.js');
const templateStr = require('app/components/common/rb-table-control-button/rb-table-control-button.html');

angular.module('app.components.common').component('rbTableControlButton', {
  bindings: {
    tooltip: '<',
    onClick: '&',
    disabled: '<',
    status: '@',
  },
  controller: rbTableControlButtonController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbTableControlButtonController() {
  const vm = this;
  vm.statusControl = status => {
    return TABLE_CONTROL_BUTTON_ICON[status];
  };
}
