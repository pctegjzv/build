/**
 * Created by zhouwenbo on 2017/7/13.
 */
const TABLE_CONTROL_BUTTON_ICON = {
  update: 'fa fa-pencil',
  delete: 'fa fa-trash-o',
  start: 'fa fa-play',
  stop: 'fa fa-stop',
  alarm_ack: 'fa fa-bell-slash',
  alarm_create: 'fa fa-bell',
};

const TABLE_CONTROL_BUTTON_CONSTANTS = {
  TABLE_CONTROL_BUTTON_ICON,
};

module.exports = TABLE_CONTROL_BUTTON_CONSTANTS;

angular
  .module('app.components.pages')
  .constant('TABLE_CONTROL_BUTTON_CONSTANTS', TABLE_CONTROL_BUTTON_CONSTANTS);
