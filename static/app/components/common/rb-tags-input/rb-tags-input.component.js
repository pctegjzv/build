/**
 *
 *  Usage:
 *  vm.myTags = ['1', '2', '3'];
 *  <rb-tags-input on-change="vm.onTagsChange(tags)" init-tags="vm.myTags" tag-pattern="'^[0-9]+$'"></rb-tags-input> (allow numbers only)
 *
 *  initTags: should be array of strings (optional)
 *  onChange: on change callback, param tags will be array of strings
 *  tagPattern: reg pattern to validate tag before added to tags array (optional)
 *  validateFn: function to validate items, return ture of false
 *  tagClass: Classname for the tags, or a function returning a classname
 *  suggestions: Suggestion data sources, eg: ['hello', 'world!']
 *  beforeItemAdd: Triggered just before an item gets added, return new value. example: using to reformat the item getting added
 *  $scope.addNewTag(text): Used to add new tags. As soon as additionalTags changes, tagsInput will try to add additionalTags as new tags DOM.
 *
 *  keyupCallback: a function execute when key up
 *    e.g.
 *      function(keyCode, txt, selected) {
 *        // ....
 *        return true;
 *        // if true, hide the autocomplete when execute
 *      }
 *
 *  只有在组件定宽或有最大宽度时，内部标签才能在过长时显示省略号。当父元素使用弹性布局时组件有可能被内部标签撑大。
 *  当组件在未显示时初始化，自定义placeholder可能显示不全，可在当前作用域内使用"rb-tags-input .tags>input.input"选择器手动设置placeholder宽度。
 *
 */

const templateStr = require('app/components/common/rb-tags-input/rb-tags-input.html');
angular.module('app.components.common').component('rbTagsInput', {
  bindings: {
    useObj: '<',
    initTags: '<',
    addOnSpace: '<',
    onChange: '&',
    validateFn: '<',
    placeholder: '<',
    isDisabled: '<',
    tagClass: '<',
    suggestions: '<',
    beforeItemAdd: '<',
    keyupCallback: '<',
    isRequired: '<',
    foldable: '<',
  },
  controller: rbTagsInputController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbTagsInputController($element, $window, $document, translateService) {
  const vm = this;

  let tagsInputScope;

  vm.addNewTag = addNewTag;
  vm.show = _show;

  // keyupCallback
  let inputTxt = '';
  const keyupCallbackHandler = {
    // get keyCode
    // 因为ngTagsInput组件中，某些默认行为被取消了，所以这些keyCode获取需要在keydown时获取
    // node_modules/ng-tags-input/build/ng-tags-input.js - line: 491, line: 817
    keydown($event) {
      if (!vm.keyupCallback) {
        return;
      }
      if ($event) {
        vm.inputKeyCode = $event.keyCode;
      }
    },
    // get txt
    // 每当有新标签被添加时，会先清空vm.text, 导致keyup时获取不到真实的text, 所以在keypress时获取
    keypress($event) {
      if (!vm.keyupCallback) {
        return;
      }
      if ($event) {
        if (!vm.text) {
          inputTxt = '' + $event.key;
        } else {
          inputTxt = vm.text + $event.key;
        }
      }
    },
    keyup() {
      if (!vm.keyupCallback) {
        return;
      }
      // 获取autocomplete是否被选中的状态, 因为这里没有合适的api, 所以直接匹配class为selected的
      const list = $element.find('.autocomplete .suggestion-list')[0];
      if (list) {
        vm.selected = !!list.innerHTML.match('ng-scope selected');
      }

      const flag = vm.keyupCallback(vm.inputKeyCode, inputTxt, vm.selected);

      // 如果keyupCallback返回true, 则隐藏autocomplete
      if (flag === true) {
        list && list.classList.add('hidden');
      } else {
        list && list.classList.remove('hidden');
      }

      // 在回调函数执行后，再清空记录text信息的inputTxt
      if (vm.inputKeyCode === 13 || vm.selected) {
        inputTxt = '';
      }
    },
    // 点击添加标签时，也清空inputTxt
    clickItem($event) {
      if (!vm.keyupCallback) {
        return;
      }
      if ($event.target.classList.contains('suggestion-item')) {
        inputTxt = '';
      }
    },
  };
  vm.keydown = keyupCallbackHandler.keydown;
  vm.keypress = keyupCallbackHandler.keypress;
  vm.keyup = keyupCallbackHandler.keyup;
  vm.clickItem = keyupCallbackHandler.clickItem;
  vm.setTitle = setTitle;

  vm.tags = [];
  vm.onAddedOrRemoved = onAddedOrRemoved;
  vm.getSource = getSource;
  vm.$onInit = _init;
  vm.$onChanges = _changes;
  vm.$onDestroy = _destroy;

  /////////
  function _init() {
    vm.placeholder =
      vm.placeholder || translateService.get('add_by_enter_or_space');
    vm.addOnSpace = vm.addOnSpace !== false;
    $document.on('click', _hide);
  }

  function _changes({ initTags }) {
    if (initTags && initTags.currentValue) {
      vm.tags = JSON.parse(JSON.stringify(vm.initTags));
    }
  }

  function _show($event) {
    $event && $event.stopPropagation();
    if (!vm.foldable || !vm.$tags) {
      return;
    }
    const labels = [...vm.$tags.find('.tag-item')];

    if (labels.length) {
      labels.forEach(label => {
        label.classList.remove('hidden');
        label.classList.add('visible');
      });
    }

    vm.$tags.children('input.input')[0].classList.remove('hidden');
    vm.$tags.children('input.input')[0].classList.add('visible');

    vm.foldCount = labels.length || null;
  }

  function _hide() {
    if (!vm.foldable) {
      return;
    }
    vm.$tags || (vm.$tags = $element.find('tags-input .tags'));
    const labels = [...vm.$tags.find('.tag-item')];
    if (!labels.length) {
      return;
    }
    // 35是首尾padding什么的
    const tagInput = vm.$tags.children('input')[0];
    const tagInputWidth = parseInt(
      $window.getComputedStyle(tagInput, null).width,
    );
    const inputWidth =
      parseInt($window.getComputedStyle(vm.$tags[0], null).width) - 35;
    const count = { w: 0, n: -1 };

    while (
      count.w + tagInputWidth <= inputWidth &&
      count.n < labels.length - 1
    ) {
      count.n++;
      count.w +=
        parseInt($window.getComputedStyle(labels[count.n], null).width) + 8;
    }

    if (count.w + tagInputWidth >= inputWidth) {
      labels.forEach((label, i) => {
        if (i > count.n) {
          label.classList.remove('visible');
          label.classList.add('hidden');
        }
      });
      tagInput.classList.remove('visible');
      tagInput.classList.add('hidden');
    }
    // count display
    vm.foldCount = labels.length || null;
  }

  function _destroy() {
    $document.off('click', _hide);
  }

  function addNewTag(text) {
    if (!tagsInputScope) {
      tagsInputScope = angular.element('tags-input', $element).isolateScope();
    }
    tagsInputScope.newTag.text(text);
    tagsInputScope.tagList.addText(text);
  }

  function getSource(query) {
    const trimedQuery = query.replace(/\s+/g, '');
    if (!query.match(/^(\*+)/)) {
      return vm.suggestions.filter(
        item =>
          (vm.useObj
            ? item.text.replace(/\s+/g, '').search(trimedQuery)
            : item.replace(/\s+/g, '').search(trimedQuery)) >= 0,
      );
    } else {
      return vm.suggestions.filter(item => item);
    }
  }

  function onAddedOrRemoved(tags, action, $tag) {
    // seems like ng-tags-input has a bug
    if (!tags) {
      tags = [$tag];
    }

    if (vm.beforeItemAdd && action === 'add') {
      const lastTag = tags[tags.length - 1];
      const result = vm.beforeItemAdd(vm.useObj ? lastTag : lastTag.text);
      if (vm.useObj) {
        lastTag.text = result.text;
        lastTag.value = result.value;
      } else {
        lastTag.text = result;
      }
    }

    vm.onChange({ tags: getTags(tags) });
    if (vm.foldable) {
      vm.foldCount = tags.length || null;
    }
  }

  function setTitle($event) {
    const target = $event.target;
    if (target.tagName === 'LI') {
      target.title = $(target)
        .find('span.ng-binding')
        .html();
    }
  }

  function getTags(tags) {
    return vm.useObj
      ? JSON.parse(JSON.stringify(tags))
      : tags.map(tag => tag.text);
  }
}
