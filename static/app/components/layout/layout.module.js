angular
  .module('app.components.layout', ['app.core', 'app.components.pages'])
  .config(registerNavStates)
  .config(customizeMdTheme);

let ngRoutes;

/**
 * Config Routing related states for navigation list for legacy states.
 */
function registerNavStates(
  $locationProvider,
  $urlRouterProvider,
  $urlMatcherFactoryProvider,
) {
  $locationProvider.html5Mode(true);

  // Make trailing slash optional for all routes
  $urlMatcherFactoryProvider.strictMode(false);

  $urlRouterProvider.otherwise(($injector, $location) => {
    const ngRouter = $injector.get('ngRouter');

    if (!ngRoutes) {
      ngRoutes = ngRouter.config.map(({ path }) => path.split('/')[0]);
    }

    const url = $location.url();

    ngRouter.navigateByUrl(
      ngRoutes.includes(url.split('/')[1]) ? url : '/home',
    );
  });
}

function customizeMdTheme($mdThemingProvider) {
  'ngInject';
  $mdThemingProvider
    .theme('default')
    // Related to md-ripple colors (e.g., md-tabs)
    .accentPalette('grey', { default: '600' });
}
