/**
 * Created by liudong on 2017/7/19.
 */
angular
  .module('app.components.pages')
  .factory('rbAlarmDataService', rbAlarmDataServiceFactory);

function rbAlarmDataServiceFactory(rbHttp, rbAccountService) {
  const namespace = rbAccountService.getCurrentNamespace();
  const ALARMS_URL_V2 = `/ajax/v2/alarms/${namespace}/`;
  const LOG_ALARMS_URL_V2 = `/ajax/v2/log_alarms/${namespace}/`;

  return {
    getAlarms,
    getServiceAlarms,
    getAlarm,
    createAlarm,
    updateAlarm,
    deleteAlarm,
    deleteSubAlarm,
    ackAlarm,
    getLogAlarms,
    getLogAlarm,
    createLogAlarm,
    updateLogAlarm,
    deleteLogAlarm,
  };

  function getAlarms() {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: ALARMS_URL_V2,
      })
      .then(({ result }) => {
        return result || [];
      });
  }

  function getServiceAlarms(service_uuid) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: ALARMS_URL_V2,
        params: {
          service_uuid,
        },
      })
      .then(res => {
        return res.alarms || [];
      });
  }

  function getAlarm(uuid) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: ALARMS_URL_V2 + uuid,
    });
  }

  function createAlarm(data) {
    return rbHttp.sendRequest({
      method: 'POST',
      url: ALARMS_URL_V2,
      data,
    });
  }

  function updateAlarm(uuid, data) {
    return rbHttp.sendRequest({
      method: 'PUT',
      url: ALARMS_URL_V2 + uuid,
      data,
    });
  }

  function deleteAlarm(uuid) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url: ALARMS_URL_V2 + uuid,
    });
  }

  /**
   * send acknowledge to alarm
   * @param uuid  alarm uuid
   * @param alertKeys array of alert_key (alarm child name)
   */
  function ackAlarm(uuid, alertKeys) {
    const actions = alertKeys.map(key => {
      return {
        type: 'ack',
        alert_key: key,
      };
    });
    return rbHttp.sendRequest({
      method: 'POST',
      url: ALARMS_URL_V2 + uuid + '/action',
      data: actions,
      addNamespace: false,
    });
  }
  function deleteSubAlarm(uuid, alertKeys) {
    const actions = {
      type: 'delete', // todo: api params
      alert_key: alertKeys,
    };
    return rbHttp.sendRequest({
      method: 'POST',
      url: ALARMS_URL_V2 + uuid + '/action',
      data: actions,
      addNamespace: false,
    });
  }

  function getLogAlarms({ page = 1, page_size = 20, name = '' } = {}) {
    return rbHttp.sendRequest({
      method: 'GET',
      params: {
        page,
        page_size,
        name,
      },
      url: LOG_ALARMS_URL_V2,
    });
  }

  function getLogAlarm(uuid) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: LOG_ALARMS_URL_V2 + uuid,
    });
  }

  function createLogAlarm(data) {
    return rbHttp.sendRequest({
      method: 'POST',
      url: LOG_ALARMS_URL_V2,
      data,
    });
  }

  function updateLogAlarm(uuid, data) {
    return rbHttp.sendRequest({
      method: 'PUT',
      url: LOG_ALARMS_URL_V2 + uuid,
      data,
    });
  }

  function deleteLogAlarm(uuid) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url: LOG_ALARMS_URL_V2 + uuid,
    });
  }
}
