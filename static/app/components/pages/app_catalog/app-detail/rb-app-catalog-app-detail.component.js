import { CONFIG_SEQUENCE_ARR } from '../rb-app-catalog-constant';

const templateStr = require('app/components/pages/app_catalog/app-detail/rb-app-catalog-app-detail.html');
angular.module('app.components.pages').component('rbPageAppCatalogDetail', {
  controller: rbPageAppCatalogDetailController,
  bindings: {
    appName: '<',
    appType: '<',
  },
  controllerAs: 'vm',
  template: templateStr,
});

function rbPageAppCatalogDetailController(
  $state,
  $timeout,
  rbAppCatalogDataService,
  rbFormDataService,
  translateService,
  rbConfirmBox,
  rbSafeApply,
  $log,
) {
  const vm = this;
  vm.configMap = { CONFIG_SEQUENCE_ARR };
  vm.cancel = cancel;
  vm.launch = launch;
  vm.onAppChange = onAppChange;
  vm.onImageError = onImageError;
  vm.defaultImg = defaultImg;
  vm.getDetail = _getDetail;
  vm.$onInit = _onInit;

  ///////
  async function _onInit() {
    rbFormDataService.resetForm();
    const namesObj = await rbAppCatalogDataService.getAppCatalogNameList(
      vm.appType,
    );
    if (!namesObj[vm.appName]) {
      namesObj[vm.appName] = vm.appName;
    }
    vm.appNames = _.map(namesObj, (value, key) => ({ value, key }));
    _getDetail();
  }

  async function _getDetail() {
    try {
      vm.loading = true;
      vm.loadingError = false;
      const {
        config,
        detail,
      } = await rbAppCatalogDataService.getAppCatalogDetail(vm.appName);
      vm.formConfig = config;
      vm.appDetail = detail;
      vm.initialized = true;
    } catch (e) {
      $log.error('Failed to load APP_CATALOG_DETAIL_URL', e);
      vm.loadingError = true;
    }
    vm.loading = false;
    rbSafeApply();
  }

  function cancel() {
    $state.go('^');
  }

  async function launch() {
    vm.configForm.$setSubmitted();
    if (vm.configForm.$invalid) {
      return;
    }
    const submit = rbFormDataService.getConfigFormData(CONFIG_SEQUENCE_ARR);
    try {
      await rbConfirmBox.show({
        title: translateService.get('create'),
        textContent: translateService.get('catalog_confirm_create'),
      });
      vm.creating = true;
      const res = await rbAppCatalogDataService.createAppCatalog(
        _.cloneDeep(submit.res),
      );
      $state.go('app_service.app.app_detail', {
        app_name: res.uuid,
      });
    } catch (e) {
      e && $log.error('Failed to load APP_CATALOG_DETAIL_URL', e);
    }
    vm.creating = false;
  }

  function onAppChange(option) {
    $state.go('^.detail', { appName: option.key });
  }

  function onImageError() {
    vm.appDetail.avatar = defaultImg();
  }

  function defaultImg() {
    return '/static/images/app-catalog/' + vm.appName + '.png';
  }
}
