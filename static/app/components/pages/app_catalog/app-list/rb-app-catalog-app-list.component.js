import { STATUS_COLOR_MAP } from '../rb-app-catalog-constant';
import { resource_types as RESOURCE_TYPES } from '../../rbac_role/rb-rbac-helper.constant';

const templateStr = require('app/components/pages/app_catalog/app-list/rb-app-catalog-app-list.html');
angular.module('app.components.pages').component('rbPageAppCatalogList', {
  controller: rbPageAppCatalogListController,
  controllerAs: 'vm',
  template: templateStr,
  bindings: {
    appType: '<',
  },
});

function rbPageAppCatalogListController(
  rbAppCatalogDataService,
  rbAccountService,
  rbRoleUtilities,
  translateService,
  $timeout,
  $state,
  rbSafeApply,
  rbModal,
  $log,
) {
  const vm = this;
  vm.statusMap = {};
  vm.loadingStatus = [];
  vm.showAppInfo = showAppInfo;
  vm.showStatus = showStatus;
  vm.createApp = createApp;
  vm.scaleUpDown = scaleUpDown;
  vm.loadList = _getList;
  vm.$onInit = _onInit;
  //////////////
  async function _onInit() {
    vm.initialized = false;
    vm.loadingFail = [];
    _getList()
      .catch(() => [])
      .then(() => {
        vm.initialized = true;
      });
    vm.applicationCreateEnabled = await rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.APPLICATION,
    );
    _CreateStatusMap();
    rbSafeApply();
  }

  async function _getList() {
    try {
      vm.appList = await rbAppCatalogDataService.getAppCatalogList({
        app_type: vm.appType,
      });
      vm.listLoadingError = false;
    } catch (e) {
      vm.listLoadingError = true;
      $log.error('Failed to load APP_CATALOG_LIST_URL', e);
    }
    rbSafeApply();
    try {
      const {
        result: appNum,
      } = await rbAppCatalogDataService.getAppCatalogServiceNum();
      _.forEach(vm.appList, item => {
        item.app_num = _.get(
          _.find(appNum, itm => itm.app_name === item.app_name),
          'app_num',
        );
      });
    } catch (e) {
      $log.error('Failed to load APP_CATALOG_APP_NUM_URL', e);
    }
    rbSafeApply();
  }

  async function _getStatus(index) {
    try {
      vm.loadingStatus[index] = true;
      const appName = vm.appList[index]['app_name'];
      const result = await rbAppCatalogDataService.getAppCatalogServiceList(
        appName,
      );
      if (result) {
        const statusList = [];
        _.forEach(result, (apps, region) => {
          statusList.push({
            region,
            apps,
            selected: true,
          });
        });
        vm.appList[index].statusList = statusList;
      }
      vm.loadingFail[index] = false;
    } catch (e) {
      vm.loadingFail[index] = true;
      $log.error('Failed to load APP_CATALOG_LIST_URL', e);
    }
    vm.loadingStatus[index] = false;
    rbSafeApply();
  }

  function showStatus(index) {
    if (vm.appList[index].disabled) {
      return;
    }
    vm.appList[index].showServiceStatus = true;
    _getStatus(index);
  }

  function showAppInfo(index) {
    vm.appList[index].showServiceStatus = false;
  }

  function createApp(appName) {
    $state.go('^.detail', { appName });
  }

  function scaleUpDown(item, index) {
    const appInfo = vm.appList[index];
    rbModal.show({
      width: 850,
      title: translateService.get('catalog_elastic'),
      locals: {
        appData: appInfo.statusList,
        appTypeInfo: {
          name: appInfo.app_name,
          display_name: appInfo.display_name,
          avatar: appInfo.avatar,
        },
      },
      component: 'rbCatalogAutoScalingDialog',
    });
  }

  function _CreateStatusMap() {
    _.each(STATUS_COLOR_MAP, (statusList, type) => {
      _.each(statusList.split(' '), status => {
        vm.statusMap[status] = type;
      });
    });
  }
}
