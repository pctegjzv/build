const templateStr = require('app/components/pages/app_catalog/catalog-scaling/rb-catalog-auto-scaling-dialog.component.html');
angular.module('app.components.pages').component('rbCatalogAutoScalingDialog', {
  controller: rbCatalogAutoScalingDialogController,
  controllerAs: 'vm',
  bindings: {
    appData: '<',
    appTypeInfo: '<',
  },
  template: templateStr,
});

function rbCatalogAutoScalingDialogController(
  rbModal,
  rbAppDataService,
  rbAppCatalogDataService,
  rbRegionNodeDataService,
  translateService,
  rbSafeApply,
  rbToast,
) {
  const vm = this;
  const configNames = ['basic_config', 'advanced_config'];
  vm.save = save;
  vm.cancel = cancel;
  vm.removeNode = removeNode;
  vm.undoRemoveNode = undoRemoveNode;
  vm.onAddNode = onAddNode;
  vm.onRemoveNode = onRemoveNode;
  vm.handleRegionChange = handleRegionChange;
  vm.handleAppChange = handleAppChange;
  vm.newNodes = [];

  function handleRegionChange(option) {
    vm.curRegion = option.region;
    vm.apps = option.apps;
  }

  function handleAppChange(option) {
    vm.curApp = option;
    catalogAppDetail(option.uuid, option.name);
  }

  function catalogAppDetail(uuid, appName) {
    vm.loading = true;
    return rbAppCatalogDataService
      .showAppInfo(vm.appTypeInfo.name, uuid)
      .then(result => {
        const res = result;
        vm.appInfo = res;
        vm.services = res.service;
        _.forEach(configNames, configName => {
          vm.isFixed = _.get(res, `${configName}.is_fixed`);
          if (vm.isFixed) {
            return;
          }
        });
        vm.isFixed = vm.isFixed === 'false' ? false : vm.isFixed;
        const scalableConfig = _.get(res, 'advanced_config', {});
        const numOfKeys = Object.keys(scalableConfig).filter(field =>
          field.includes('num_of_'),
        );
        vm.numOfConfigs = numOfKeys.map(key => ({
          realKey: key,
          displayName: translateService.get('catalog_num_of_key', {
            key: key.replace('num_of_', ''),
          }),
          value: scalableConfig[key],
        }));
        if (vm.isFixed) {
          // get current available nodes
          rbRegionNodeDataService.nodeTags(vm.curRegion).then(data => {
            const ipTags = _.chain(data)
              .filter(x => x.state === 'RUNNING')
              .map(item => {
                const ip = _.get(_.find(item.labels, ['key', 'ip']), 'value');
                return ip;
              })
              .value();
            const usedIps = _.chain(res.service)
              .map('ip')
              .uniq()
              .value();
            vm.ipTags = _.filter(ipTags, ip => !usedIps.includes(ip));
          });
          appDetail(appName);
        }
      })
      .catch(() => {
        rbToast.error(
          translateService.get('catalog_elastic_info_get_error', { appName }),
        );
      })
      .then(() => {
        vm.loading = false;
      });
  }

  function appDetail(appName) {
    // get services status info of an app
    return rbAppDataService.getAppDetail(appName).then(({ app }) => {
      vm.serviceStatsMap = {};
      app.services.forEach(service => {
        vm.serviceStatsMap[service.service_name] = service.current_status;
      });
      vm.services.forEach(service => {
        service.current_status = vm.serviceStatsMap[service.name];
      });
    });
  }

  function save() {
    vm.scalingForm.$setSubmitted();
    if (vm.scalingForm.$invalid) {
      return;
    }
    const payload = _.cloneDeep(vm.appInfo);

    vm.numOfConfigs.forEach(config => {
      if (config.value) {
        payload['advanced_config'][config.realKey] = config.value;
      }
    });

    if (vm.isFixed) {
      let configName = configNames[0];
      _.forEach(configNames, name => {
        if (_.get(vm.appInfo, `${name}.ip_tag`)) {
          configName = name;
        }
      });
      const reservedNodes = _.chain(vm.services)
        .map(service => {
          if (!service.removed) {
            return service.ip;
          }
        })
        .compact()
        .value();
      payload[configName]['ip_tag'] = _.uniq(vm.newNodes.concat(reservedNodes));
    }
    delete payload.service;
    vm.loading = true;
    rbAppCatalogDataService
      .scaling(vm.appTypeInfo.name, vm.curApp.uuid, payload)
      .then(() => {
        vm.loading = false;
        cancel();
      });
  }

  function cancel() {
    rbModal.hide();
  }

  function removeNode($index) {
    vm.services[$index].removed = true;
  }

  function undoRemoveNode($index) {
    vm.services[$index].removed = false;
  }

  function onAddNode(option) {
    vm.newNodes.push(option.value);
  }

  function onRemoveNode(option) {
    _.remove(vm.newNodes, item => item === option.value);
  }
}
