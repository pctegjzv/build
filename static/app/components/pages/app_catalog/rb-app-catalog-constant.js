import CatalogStore from 'app/components/common/rb-form-controls/rb-form-store';
import { RESOURCE_NAME_BASE } from '../../common/config/common-pattern';

let store = new CatalogStore(); // Storing
export const NAME_TO_TYPE_LIST = ['cluster_size'];
/**
 * {
 *  basic: {
 *    'defaultInclude': true,
 *    list: {
 *      excludes: ['gitlab'],
 *      includes: []
 *    },
 *  },
 *  the_redis: {
 *    list: {
 *      include: [a,b,c,d]
 *    }
 *  }
 * }
 */

export const FILLING_NAME_LIST = {
  basic: {
    defaultInclude: true,
  },
  ip_tag: {},
  zookeeper: {
    list: {
      includes: ['zookeeper'],
    },
  },
  ip_tags_subset: {
    list: {
      includes: [
        'hadoop',
        'redis',
        'mysql',
        'elk',
        'spark',
        'batch_processing',
      ],
    },
    defaultInclude: true,
  },
  redis: {
    list: {
      includes: ['redis'],
    },
  },
  mysql: {
    list: {
      includes: ['mysql'],
    },
  },
};

export const FILLING_CONFIGS = {
  basic: {
    '*': [
      {
        attribute_name: /^ip_tag/,
        key: 'node_tag_key',
        value: 'node_tag',
        'null-placeholder': 'no_node_tag_change_region',
        option: () => {
          // This is a correlated field **JUST** dependent on upper field's **value**, (in this case is region)
          // It depends on upper level field **value** to get its own data from **its own independent Data Source**
          return (rbRegionNodeDataService, rbFormDataService, $log) => {
            'ngInject';
            return (callbackForComponent, context) => {
              rbFormDataService.getFormField('region', currentRegion => {
                // Watch the parent field change and update its own options
                context.isOptionsLoading = true;
                rbRegionNodeDataService
                  .nodeTags(currentRegion)
                  .catch(e => {
                    $log.error('Cannot load tag nodes:', e);
                    return [];
                  })
                  .then(data => {
                    data = _.chain(data)
                      .filter(x => x.state === 'RUNNING')
                      .each(item => {
                        item.node_tag = item.node_tag.replace('ip:', '');
                        item.node_tag_key = item.node_tag_key.replace(
                          'ip:',
                          '',
                        );
                      })
                      .value();
                    context.isOptionsLoading = false;
                    if (!data.length) {
                      context.nullPlaceholder = 'no_node_tag_change_region';
                    }
                    callbackForComponent(data, context);
                  });
              });
            };
          };
        },
      },
      {
        attribute_name: /^node_tag/,
        key: 'display',
        value: 'display',
        'field-keys': ['key', 'value'],
        'null-placeholder': 'no_node_tag_change_region',
        option: () => {
          return (rbRegionDataService, rbFormDataService, $log) => {
            'ngInject';
            return (callbackForComponent, context) => {
              rbFormDataService.getFormField('region', currentRegion => {
                context.isOptionsLoading = true;
                rbRegionDataService
                  .getRegionLabels(currentRegion)
                  .catch(e => {
                    $log.error('Cannot load region labels:', e);
                    return [];
                  })
                  .then(({ labels = [] }) => {
                    labels = labels.map(r => {
                      return _.extend({ display: `${r.key}: ${r.value}` }, r);
                    });
                    context.isOptionsLoading = false;
                    if (!labels.length) {
                      context.nullPlaceholder = 'no_node_tag_change_region';
                    }
                    callbackForComponent(labels, context);
                  });
              });
            };
          };
        },
      },
      {
        attribute_name: /^alauda_lb_name/,
        key: 'display',
        value: 'name',
        'null-placeholder': 'app_catalog_no_alb_placeholder',
        option: () => {
          return (
            rbLoadBalancerDataService,
            translateService,
            rbLoadBalancerUtilities,
            rbFormDataService,
            $log,
          ) => {
            'ngInject';
            return (callbackForComponent, context) => {
              rbFormDataService.getFormField('region', currentRegionName => {
                const regionNameInfoMap = store.getFieldStore(
                  'region_name_info_map',
                );
                const region = regionNameInfoMap[currentRegionName];
                const hasAlb = _.get(
                  region,
                  'features.service.features',
                  [],
                ).includes('alb');
                if (hasAlb) {
                  context.showCurField = true;
                  rbFormDataService.amendConfirmFieldTemplateByAttrName(
                    fieldTemplate => {
                      fieldTemplate.required = true;
                    },
                    'alauda_lb_name',
                  );
                } else {
                  context.showCurField = false;
                  rbFormDataService.amendConfirmFieldTemplateByAttrName(
                    fieldTemplate => {
                      fieldTemplate.required = false;
                      rbFormDataService.setFormField('alauda_lb', null);
                    },
                    'alauda_lb_name',
                  );
                }
                context.isOptionsLoading = true;
                rbLoadBalancerDataService
                  .getLoadBalancers({
                    region_name: currentRegionName,
                    region_id: region.id,
                  })
                  .catch(e => {
                    $log.error('Cannot load load balancer lists:', e);
                    return [];
                  })
                  .then(({ result }) => {
                    context.isOptionsLoading = false;
                    let avaliableAlb = _.filter(result, lb =>
                      ['haproxy', 'nginx'].includes(lb.type),
                    );
                    avaliableAlb = _.map(avaliableAlb, lb =>
                      _.merge({ display: lb.name }, lb),
                    );
                    if (!context.required) {
                      avaliableAlb.unshift({
                        name: '',
                        display: translateService.get(
                          'app_catalog_not_choose_alb',
                        ),
                      });
                    }
                    if (!avaliableAlb.length) {
                      context.nullPlaceholder =
                        'app_catalog_no_alb_placeholder';
                    }
                    callbackForComponent(avaliableAlb, context);
                  });
              });
            };
          };
        },
      },
      {
        attribute_name: 'pod_anti_affinity',
        pattern: /(.+):\s*(.+)/,
        'field-keys': { key: 1, value: 2 },
        'select-placeholder': 'catalog_pod_anti_affinity_select',
        option: () => {
          return (
            rbFormDataService,
            rbLabelsDataService,
            rbAppDataService,
            rbAppUtilities,
          ) => {
            'ngInject';
            return (async () => {
              // workaround of the babel bug
              let labels = await rbLabelsDataService.getAllLebels();
              labels = labels['results'];
              return (callbackForComponent, context) => {
                rbFormDataService.getFormField('region', currentRegionName => {
                  context.isOptionsLoading = true;
                  rbAppDataService
                    .getApps({
                      regionName: currentRegionName,
                      basic: false,
                    })
                    .then(apps => {
                      const pod_labels = rbAppUtilities.getPodLablesInApps(
                        apps,
                      );
                      const all_labels = labels.concat(pod_labels);
                      const suggestions = all_labels.map(label => {
                        return `${label.key}: ${label.value}`;
                      });
                      context.isOptionsLoading = false;
                      callbackForComponent(suggestions, context);
                    });
                });
              };
            })();
          };
        },
      },
      {
        attribute_name: /^alauda_config_server/,
        key: 'app_name',
        value: 'uuid',
        option: () => {
          return (rbAppDataService, rbFormDataService, $log) => {
            'ngInject';
            return (callbackForComponent, context) => {
              rbFormDataService.getFormField('region', currentRegion => {
                context.isOptionsLoading = true;
                rbAppDataService
                  .getApps({
                    regionName: currentRegion,
                    type: 'spring_config_server',
                  })
                  .catch(e => {
                    $log.error('Cannot load app spring_config_server:', e);
                    return [];
                  })
                  .then(data => {
                    context.isOptionsLoading = false;
                    if (!data.length) {
                      context.nullPlaceholder =
                        'app_catalog_no_spring_config_server';
                    }
                    callbackForComponent(data, context);
                  });
              });
            };
          };
        },
      },
      {
        attribute_name: 'app_tag',
        'field-keys': ['key', 'value'],
        option: () => {
          return rbLabelsDataService => {
            'ngInject';
            return rbLabelsDataService.getAllLebels().then(res => {
              const suggestions = { key: [], value: [] };
              res.results.forEach(label => {
                suggestions.key.push({ title: label.key });
                suggestions.value.push({ title: label.value });
              });
              return suggestions;
            });
          };
        },
      },
      {
        attribute_name: 'node_port',
        // The min/max value of node_port should depend on region features:
        max_value: () =>
          store.getFieldStore('region_host_port_range', [0, 65535])[1],
        min_value: () =>
          store.getFieldStore('region_host_port_range', [0, 65535])[0],
      },
      {
        attribute_name: 'zookeeper_application',
        key: 'app_name',
        value: 'uuid',
        option: () => {
          return (rbAppDataService, rbFormDataService, $log) => {
            'ngInject';
            return (callbackForComponent, context) => {
              rbFormDataService.getFormField('region', currentRegion => {
                context.isOptionsLoading = true;
                rbAppDataService
                  .getApps({
                    regionName: currentRegion,
                    type: 'zookeeper',
                  })
                  .catch(e => {
                    $log.error('Cannot load app zookeeper:', e);
                    return [];
                  })
                  .then(data => {
                    context.isOptionsLoading = false;
                    if (!data.length) {
                      context.nullPlaceholder =
                        'app_catalog_no_zookeeper_application';
                    }
                    callbackForComponent(data, context);
                  });
              });
            };
          };
        },
      },
    ],
    basic_config: [
      {
        isExtra: true,
        attribute_name: 'space_name',
        'null-placeholder': 'quota_space_required',
        type: 'option',
        key: 'name',
        value: 'name',
        option: () => {
          return rbQuotaSpaceDataService => {
            'ngInject';
            return rbQuotaSpaceDataService
              .getConsumableSpaces()
              .then(spaces => {
                return spaces;
              });
          };
        },
        display_name: 'quota_space',
      },
      {
        isExtra: true,
        attribute_name: 'name',
        type: 'string',
        display_name: 'application_name',
        maxlength: 64,
        pattern: RESOURCE_NAME_BASE.pattern,
        'pattern-hint': RESOURCE_NAME_BASE.tip,
      },
    ],
    region_config: [
      {
        attribute_name: 'region',
        type: 'option',
        key: 'display_name',
        value: 'name',
        onChange: rbFormDataService => {
          'ngInject';
          return currentRegion => {
            const features = _.get(
              currentRegion,
              'features.service.features',
              [],
            );
            const hasAlb = features.includes('alb');
            rbFormDataService.setFormField(
              'network_mode',
              _.intersection(features, ['bridge', 'macvlan', 'flannel']),
            );
            if (hasAlb) {
              rbFormDataService.setFormField('region_alb_version', 'v2');
            } else {
              rbFormDataService.setFormField('region_alb_version', 'v1');
            }
            store.setFieldStore(
              'region_host_port_range',
              _.get(currentRegion, 'features.service.manager.host_port_range'),
            );
          };
        },
        option: () => {
          // If you want option create dynamically, setting the option into ()=>(injects)=>{'ngInject'} form is crucial.
          // This is a dynamic independent field does not rely on other field
          return rbRegionDataService => {
            'ngInject';
            return rbRegionDataService.getRegions(true).then(data => {
              const regionNameInfoMap = {};
              let regions = _.sortBy(data.regions, region => {
                return region.display_name.toLowerCase();
              });
              _.remove(regions, region => {
                return region.privilege === 'R';
              });
              regions = regions.filter(
                region => !(+region.platform_version.substr(1) > 2),
              );
              regions.forEach(region => {
                regionNameInfoMap[region.name] = region;
              });
              store.setFieldStore('region_name_info_map', regionNameInfoMap);
              return regions;
            });
          };
        },
        display_name: 'region',
      },
      {
        attribute_name: 'region_alb_version',
        type: 'hidden',
        required: false,
      },
      {
        attribute_name: 'network_mode',
        type: 'hidden',
        required: false,
      },
    ],
  },
  zookeeper: {
    basic_config: [{}], // A config is **just** used for illustrate the FILLING_CONFIG's effect
  },
  ip_tags_subset: {
    '*': [
      {
        attribute_name: 'ip_tag',
        // A Feeder field give data to Beggar field
        onInit: () => {
          store = new CatalogStore();
        },
        onAdd: option => {
          // Find a specific Beggar's money box
          let _store = store.getFieldStore('avaliable_ip_tag_lists');
          if (!_store) {
            _store = [option];
          } else {
            _store.push(option);
          }
          // Put money to box
          store.setFieldStore('avaliable_ip_tag_lists', _store);
        },
        onRemove: option => {
          const _store = store.getFieldStore('avaliable_ip_tag_lists');
          store.setFieldStore(
            'avaliable_ip_tag_lists',
            _.filter(_store, itm => {
              if (itm.node_tag === option.node_tag) {
                return false;
              }
              return true;
            }),
          );
        },
      },
      {
        type: ['single_ip_tag', 'multi_ip_tag'],
        key: 'node_tag_key',
        value: 'node_tag',
        'null-placeholder': 'null_node_tag_field',
        // A Begger field **NOT ONLY** dependent on upper level field's **value**,
        // BUT ALSO need upper level field to give Data Source to it.
        // that is, the field is NOT capable to get itself data from **its own independent** Data Source
        // (Beggar just can clear itself and waiting for feeders give it data.)
        option: () => rbFormDataService => {
          'ngInject';
          // Do some init works
          store.clearFieldStore('avaliable_ip_tag_lists');
          return (callbackForComponent, context) => {
            const ipTagLists = store.getFieldStore('avaliable_ip_tag_lists');
            if (ipTagLists) {
              // Using pre-existing data
              callbackForComponent(ipTagLists, context);
            }
            // Field store didn't active update when ip_tag's options changed by its upper level field (in this case is region),
            // Clear store actively when upper level field's (ip_tag) value change
            rbFormDataService.getFormField('ip_tag', ipTagValue => {
              // Watch upper level field value, if the value become null then clean itself
              // (The beggar clear its bag when Feeders are all leave.)
              if (!_.get(ipTagValue, 'length')) {
                store.clearFieldStore('avaliable_ip_tag_lists');
              }
            });
            // Create only one handler to specific store id
            // (A beggar waiting for its data come)
            store.onFieldStoreChange('avaliable_ip_tag_lists', ipTagLists => {
              callbackForComponent(ipTagLists, context);
            });
          };
        },
      },
    ],
  },
  redis: {
    '*': [
      {
        attribute_name: 'mode',
        onChange: (translateService, rbFormDataService) => {
          'ngInject';

          function limitNumRule(
            x = rbFormDataService.getFormField('replicas_per_shard') || 0,
            y = rbFormDataService.getFormField('num_of_shards') || 1,
          ) {
            const mode = rbFormDataService.getFormField('mode');
            if ('standalone' === mode) {
              return 1;
            } else if ('replication' === mode) {
              y = 1;
            }
            return y * (x + 1);
          }

          const cb = () => {
            store.setFieldStore('redis_host_length', limitNumRule());
            store.setFieldStore(
              'redis_placeholder',
              translateService.get('catalog_ip_tag_num_placeholder', {
                value: limitNumRule(),
              }),
            );
          };
          rbFormDataService.getFormField(
            ['replicas_per_shard', 'num_of_shards'],
            cb,
          );
          return cb;
        },
      },
      {
        before_filling: config => config.standalone_config,
        attribute_name: 'ip_tag',
        'select-placeholder': () => store.getFieldStore('redis_placeholder'),
        maxlength: () => store.getFieldStore('redis_host_length'),
        minlength: () => store.getFieldStore('redis_host_length'),
      },
    ],
  },
  mysql: {
    '*': [
      {
        // image_tag for mysql indicates MySql version field
        attribute_name: 'image_tag',
        // eslint-disable-next-line
        onChange: rbFormDataService => {
          'ngInject';
          return value => {
            const shouldDisableDbProxy = value && value.version === '5.7';

            // Find the enable DBProxy field
            const enableDbProxyControl$ = $(
              'rb-page-app-catalog-detail rb-form-radio-group-tab-control',
            ).filter((i, item) => {
              const groupTabVm = angular.element(item).isolateScope().vm;
              return groupTabVm && groupTabVm.name === 'enable_dbproxy';
            });

            // Hide DB Proxy element
            enableDbProxyControl$.css(
              'display',
              shouldDisableDbProxy ? 'none' : 'inline',
            );
            if (shouldDisableDbProxy) {
              const radioGroupVm = angular
                .element('rb-radio-group', enableDbProxyControl$)
                .isolateScope().vm;

              radioGroupVm.select(
                radioGroupVm.options.find(
                  option => option.value === 'disable_dbproxy',
                ),
              );
            }
          };
        },
      },
      {
        attribute_name: 'mode',
        onChange: (translateService, rbFormDataService) => {
          'ngInject';

          function limitNumRule(
            x = rbFormDataService.getFormField('replicas_per_shard') || 0,
            y = rbFormDataService.getFormField('num_of_nodes') || 3,
          ) {
            const mode = rbFormDataService.getFormField('mode');
            if ('standalone' === mode) {
              return 1;
            } else if ('replication' === mode) {
              return x + 1;
            } else if ('cluster' === mode) {
              return y;
            }
          }

          const cb = () => {
            store.setFieldStore('mysql_host_length', limitNumRule());
            store.setFieldStore(
              'mysql_placeholder',
              translateService.get('catalog_ip_tag_num_placeholder', {
                value: limitNumRule(),
              }),
            );
          };
          rbFormDataService.getFormField(
            ['replicas_per_shard', 'num_of_nodes'],
            cb,
          );
          return cb;
        },
      },
      {
        before_filling: config => config.standalone_config,
        attribute_name: 'ip_tag',
        'select-placeholder': () => store.getFieldStore('mysql_placeholder'),
        maxlength: () => store.getFieldStore('mysql_host_length'),
        minlength: () => store.getFieldStore('mysql_host_length'),
      },
    ],
  },
};

export const CONFIG_SEQUENCE_ARR = [
  'region_config',
  'basic_config',
  'advanced_config',
];

export const STATUS_COLOR_MAP = {
  error:
    'unhealthy alarm error updateerror multierror systemerror starterror bluegreenerror stoperror scaleerror' +
    ' deleteerror failed',
  success:
    'running partialrunning bluegreen healthy ok job_schedule_active used completed',
  pending:
    'free deploying preparing pending starting updating deleting disorderdeploying creating stopping ' +
    'shutting_down scaling insufficient_data in-progress free ongoing',
};
