import _ from 'lodash';
import {
  FILLING_NAME_LIST,
  FILLING_CONFIGS,
  CONFIG_SEQUENCE_ARR,
  NAME_TO_TYPE_LIST,
} from './rb-app-catalog-constant';

angular
  .module('app.components.pages')
  .factory('rbAppCatalogDataService', rbAppCatalogDataServiceFactory);

function rbAppCatalogDataServiceFactory(
  rbHttp,
  rbToast,
  translateService,
  rbFormDataService,
  rbFormUtilities,
  WEBLABS,
  rbAccountService,
) {
  const namespace = rbAccountService.getCurrentNamespace();
  const CATALOG_CONFIG_ENDPOINT = `/ajax/catalog/${namespace}/`;
  let appNames = {}; // used for getAppCatalogNameList, created by getAppCatalogNameList
  let _curAppName = '';
  if (!WEBLABS['quota_enabled']) {
    FILLING_CONFIGS.basic.basic_config.shift();
  }
  return {
    // fetch data functions
    getAppCatalogList,
    getAppCatalogNameList,
    getAppCatalogServiceNum,
    getAppCatalogServiceList,
    getAppCatalogDetail,
    createAppCatalog,
    showAppInfo,
    scaling,
  };

  ////////// Fetch data function
  function getAppCatalogList(
    params = {
      app_type: '',
    },
  ) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: CATALOG_CONFIG_ENDPOINT,
        params,
      })
      .then(rbFormUtilities.getLanguage)
      .then(({ result }) => {
        appNames = {};
        let appList = {};
        if (result) {
          appList = result;
          appList = _.chain(appList)
            .forEach(app => {
              app.disabled = app.status !== 'ready';
            })
            .sortBy(['disabled'])
            .value();

          _.chain(appList)
            .filter(app => !app.disabled)
            .forEach(app => (appNames[app['app_name']] = app['display_name']))
            .value();
        }
        return appList;
      });
  }

  async function getAppCatalogNameList(app_type) {
    if (!Object.keys(appNames).length) {
      await getAppCatalogList({ app_type });
    }
    return appNames;
  }

  function getAppCatalogServiceNum() {
    return rbHttp.sendRequest({
      method: 'GET',
      url: CATALOG_CONFIG_ENDPOINT + 'app_num',
    });
  }

  function getAppCatalogServiceList(app_name) {
    _curAppName = app_name;
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: CATALOG_CONFIG_ENDPOINT + _curAppName + '/apps',
        params: { app_name },
      })
      .then(data => {
        _.each(data, region => {
          _.each(region, (service, idx) => {
            region[idx].status = _.get(
              region,
              `[${idx}].status`,
              '',
            ).toLowerCase();
          });
        });
        return data;
      });
  }

  function getAppCatalogDetail(app_name) {
    _curAppName = app_name;
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: CATALOG_CONFIG_ENDPOINT + app_name,
      })
      .then(rbFormUtilities.getLanguage)
      .then(result => {
        if (result) {
          return rbFormDataService.normalizeConfig(result, {
            _curConfName: app_name,
            FILLING_NAME_LIST,
            FILLING_CONFIGS,
            CONFIG_SEQUENCE_ARR,
            NAME_TO_TYPE_LIST,
          });
        }
      });
  }

  function createAppCatalog(config) {
    const terms = ['MySQL', 'Redis', 'Kafka', 'ZooKeeper'];
    let curAppName = _curAppName;
    terms.forEach(word => {
      const regexp = new RegExp(word, 'ig');
      curAppName = curAppName.replace(regexp, word);
    });
    return rbHttp
      .sendRequest({
        method: 'POST',
        url: CATALOG_CONFIG_ENDPOINT + _curAppName + '/apps',
        data: config,
      })
      .then(res => {
        rbToast.success(
          translateService.get('app_catalog_config_create_success', {
            appName: curAppName,
          }),
        );
        return res;
      });
  }

  function scaling(appName, uuid, config) {
    return rbHttp
      .sendRequest({
        method: 'PUT',
        url: CATALOG_CONFIG_ENDPOINT + appName + `/apps/${uuid}/`,
        data: config,
      })
      .then(res => {
        rbToast.success(
          translateService.get('app_catalog_detail_update_success', {
            appName: _curAppName,
          }),
        );
        return res;
      });
  }

  function showAppInfo(appName, uuid) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: CATALOG_CONFIG_ENDPOINT + appName + `/elastic/${uuid}`,
    });
  }
}
