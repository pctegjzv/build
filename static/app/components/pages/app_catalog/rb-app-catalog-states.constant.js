const detailState = appTypeConstant => ({
  name: 'detail',
  url: '/detail?appName',
  component: 'rbPageAppCatalogDetail',
  resolve: {
    appName: $stateParams => {
      'ngInject';
      return $stateParams['appName'];
    },
    appType: () => appTypeConstant,
  },
});
module.exports = [
  {
    baseStateName: 'app_catalog.middleware',
    states: [
      {
        name: 'middleware',
        url: '',
        component: 'rbPageAppCatalogList',
        resolve: {
          appType: () => 'MIDDLEWARE',
        },
      },
      detailState('MIDDLEWARE'),
    ],
  },
  {
    baseStateName: 'app_catalog.big_data',
    states: [
      {
        name: 'big_data',
        url: '',
        component: 'rbPageAppCatalogList',
        resolve: {
          appType: () => 'BIG_DATA',
        },
      },
      detailState('BIG_DATA'),
    ],
  },
  {
    baseStateName: 'app_catalog.micro_service',
    states: [
      {
        name: 'micro_service',
        url: '',
        component: 'rbPageAppCatalogList',
        resolve: {
          appType: () => 'MICR_SERVICE',
        },
      },
      detailState('MICR_SERVICE'),
    ],
  },
];
