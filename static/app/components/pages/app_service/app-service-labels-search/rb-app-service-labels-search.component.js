import { BehaviorSubject } from 'rxjs';

const templateStr = require('app/components/pages/app_service/app-service-labels-search/rb-app-service-labels-search.html');

angular.module('app.components.pages').component('rbAppServiceLabelsSearch', {
  controller: rbAppServiceLabelsSearchController,
  controllerAs: 'vm',
  bindings: {
    searchSubject: '<',
    filterFnSubject: '<',
    resources: '<',
  },
  template: templateStr,
});

function rbAppServiceLabelsSearchController(
  translateService,
  rbLabelsDataService,
) {
  const vm = this;
  vm.$onInit = _init;
  vm.$onChanges = _onChanges;
  vm.$onDestroy = _destroy;

  vm.tagValidateFn = tagValidateFn;
  vm.onTagChange = onTagChange;
  vm.beforeTagItemAdd = beforeTagItemAdd;
  vm.enterToSearch = enterToSearch;
  vm.tagClass = tagClass;
  vm.onFilterByChange = onFilterByChange;
  vm.onfiltersAdd = onfiltersAdd;
  vm.onfiltersRemove = onfiltersRemove;
  vm.search = search;

  //////////
  function _init() {
    vm.tagsInputPlaceholder = translateService.get('search_by_name_or_label');
    vm.filterByOptions = [
      {
        name: translateService.get('key'),
        value: 'Keys',
      },
      {
        name: translateService.get('value'),
        value: 'Values',
      },
    ];
    vm.labelsFilters = [];
    vm.lastSearchedKeys = [];
    vm.lastSearchedValues = [];
    vm.filterItems = [];
    vm.filter$ = new BehaviorSubject(null);
    _getLabelsSuggestions();
  }

  function _onChanges({ resources }) {
    if (resources && resources.currentValue) {
      vm.loading = false;
      _updateFilterItemOptions(resources.currentValue);
    }
  }

  function _destroy() {}

  function onTagChange(tags) {
    vm.tags = tags;
  }

  function tagValidateFn(tag) {
    if (vm.tags) {
      return !(vm.tags.includes(tag) || vm.tags.includes('search: ' + tag));
    }
    return true;
  }

  function beforeTagItemAdd(tag) {
    const match = tag.match(/^(.*):(.*)/);
    !!match && ([match[1], match[2]] = [match[1].trim(), match[2].trim()]);

    if (!match) {
      return 'search: ' + tag;
    } else if (match[1] === '' && match[2] === '') {
      return '*: *';
    } else if (match[1] === '') {
      return '*: ' + match[2];
    } else if (match[2] === '') {
      return match[1] + ': *';
    } else {
      return match[1] + ': ' + match[2];
    }
  }

  function enterToSearch(keyCode, text, selected) {
    if (keyCode === 13 && !text && !selected) {
      search();
      return true;
    }
  }

  function tagClass(item) {
    const match = item.match(/(.+):/);
    if (!match) {
      return '';
    } else if (match[1] !== 'search') {
      return 'label';
    }
  }

  function onFilterByChange(option) {
    vm.filterByKey = option.value;
    vm.filterItems = [...vm[`lastSearched${vm.filterByKey}`]];
    vm.filterFnSubject.next(_generateFilterFn());
  }

  function onfiltersAdd(option) {
    vm.labelsFilters.push(option);
    vm.filterFnSubject.next(_generateFilterFn());
  }

  function onfiltersRemove(option) {
    switch (vm.filterByKey) {
      case 'Labels':
        _.remove(vm.labelsFilters, item => {
          return !(item.label !== option.label);
        });
        break;
      case 'Values':
        _.remove(vm.labelsFilters, item => {
          return !(item.value !== option.value);
        });
        break;
      case 'Keys':
        _.remove(vm.labelsFilters, item => {
          return !(item.key !== option.key);
        });
        break;

      default:
        _.remove(vm.labelsFilters, item => {
          return !(item.label !== option.label);
        });
        break;
    }
    vm.filterFnSubject.next(_generateFilterFn());
  }

  function search() {
    vm.loading = true;
    let params = { search: '', label: '' };
    vm.labelsFilters = [];
    vm.filterItems = [];

    if (vm.tags && vm.tags.length > 0) {
      params = _searchFormatter();
    }
    vm.searchSubject.next(params);
  }

  function _generateFilterFn() {
    return item => {
      return _filterLogic(item);
    };
  }

  function _searchFormatter() {
    vm.lastSearchedKeys = [];
    vm.lastSearchedValues = [];
    vm.labelsFilters = [];
    vm.filterItems = [];
    const labelsParam = [];
    const searchParam =
      _.chain(vm.tags)
        .map(tag => {
          // from['a: 1', 'a: 2', 'b: 1', 'c: 1', 'd: 1'] to [{a:1},{a:2},{b:1},{b:2},{c:1},{d:2}]
          const param = {};
          const match = tag.match(/^(.+):\s(.+)$/);
          if (match && match[0]) {
            let value = (match[2] || '').trim();
            if (value === '*' || !value) {
              value = '';
            }
            if (match[1] === 'search') {
              vm.hasQueryString = true;
              param['search'] = value;
            } else {
              labelsParam.push(match[1] + ':' + value);
            }
          }
          return param;
        })
        .reduce((result, param) =>
          _.mergeWith(result, param, (pre, cur) => {
            // from [{a:1},{a:2},{b:1},{b:2},{c:1},{d:2}] to {a:'1,2',b:'1,2',c:'1',d:'2'}
            if (pre) {
              return [pre, cur].join(',');
            } else {
              return cur;
            }
          }),
        )
        .value() || {};
    return {
      search: searchParam.search, // ''
      label: labelsParam.join(','), // ''
    };
  }

  function _updateFilterItemOptions(items = []) {
    items.forEach(item => {
      item.labels.forEach(label => {
        vm.lastSearchedKeys.push({
          key: label.key,
          label: label.key,
        });
        vm.lastSearchedValues.push({
          value: label.value,
          label: label.value,
        });
      });
      vm.filterItems = vm.filterItems.concat(
        vm[`lastSearched${vm.filterByKey}`],
      );
      _filterItemsFormat();
    });
  }

  function _filterItemsFormat() {
    // TO DO: 这里不二次去重赋值，会有bug, 已明确不是解构的问题，待探索
    [vm.lastSearchedKeys, vm.lastSearchedValues] = [
      _.uniqBy(vm.lastSearchedKeys, 'key'),
      _.uniqBy(vm.lastSearchedValues, 'value'),
    ];
    vm.filterItems = _.uniq(vm[`lastSearched${vm.filterByKey}`]);
  }

  function _filterLogic(item) {
    let flag = false;
    if (vm.labelsFilters.length < 1) {
      return true;
    }

    switch (vm.filterByKey) {
      case 'Keys': {
        // AND
        const count = item.labels.reduce(
          (accum, label) =>
            accum +
            vm.labelsFilters.filter(filter => filter.key === label.key).length,
          0,
        );
        flag = count === vm.labelsFilters.length;
        break;
      }
      case 'Values':
        flag = item.labels.some(label =>
          vm.labelsFilters.some(filter => filter.value === label.value),
        );
        break;

      default:
        flag = true;
        break;
    }

    return flag;
  }

  function _getLabelsSuggestions() {
    rbLabelsDataService
      .getAllLebels()
      .then(res => {
        vm.suggestions = res.results.map(label => {
          return `${label.key}: ${label.value}`;
        });
      })
      .catch(() => {});
  }
}
