/**
 * Created by liudong on 16/10/10.
 */
import { RESOURCE_NAME_BASE } from 'app/components/common/config/common-pattern';
import _ from 'lodash';

const jsyaml = require('js-yaml');
const templateStr = require('app/components/pages/app_service/app_create/rb-app-create.html');
angular.module('app.components.pages').component('rbAppCreate', {
  controller: rbAppCreateController,
  bindings: {
    regionName: '<',
  },
  controllerAs: 'vm',
  template: templateStr,
});

function rbAppCreateController(
  $log,
  $state,
  $stateParams,
  rbRegionDataService,
  templateService,
  ENVIRONMENTS,
  rbAccountService,
  rbHttp,
  rbConfirmBox,
  rbToast,
  translateService,
  rbSafeApply,
  rbGlobalSetting,
  WEBLABS,
  rbAppDataService,
  rbQuotaSpaceDataService,
  rbRouterStateHelper,
  CUSTOM_EXAMPLE_YAML,
) {
  const vm = this;
  const imageIndex = ENVIRONMENTS['alauda_image_index'];
  const example_yaml = `redis:
  image: ${imageIndex}/library/redis:latest
  ports:
  - '6379'
web:
  command: python app.py
  image: ${imageIndex}/alauda/flask-redis:latest
  links:
  - redis:redis
  ports:
  - '80/http'
`;

  const templateCache = {};

  vm.$onInit = _init;

  vm.onRegionChange = onRegionChange;
  vm.onTemplateNameChange = onTemplateNameChange;
  vm.validateAppNameDuplicate = validateAppNameDuplicate;
  vm.useDemoTemplate = useDemoTemplate;
  vm.onSpaceChanged = onSpaceChanged;
  vm.cancel = cancel;
  vm.create = create;

  //////////

  async function _init() {
    vm.submitting = false;
    vm.templateLoading = false;
    vm.quotaEnabled = WEBLABS['quota_enabled'];
    vm.resourceNameBaseRegExp = RESOURCE_NAME_BASE;
    vm.appData = {
      namespace: rbAccountService.getCurrentNamespace(),
    };
    vm.selectTemplateEnabled = false;
    vm.regionList = [];
    vm.templateList = [];
    vm.editorOptions = rbGlobalSetting.getEditorOptions('yaml');
    vm.templateName = '';

    await _initRegionData();
    await _initQuotaSpaces();
    vm.initialized = true;
    _initApplicationTemplates();
  }

  async function _initRegionData() {
    if (vm.regionName) {
      vm.appData.region = vm.regionName;
      try {
        const res = await rbRegionDataService.getRegionDetail(vm.regionName);
        vm.region = res.result;
        vm.regionFeatures = _.get(vm.region, 'features.service.features', []);
      } catch (err) {
        $log.error('failed to load region');
        rbRouterStateHelper.back();
      }
    } else {
      const data = await rbRegionDataService.getRegions();
      const regions = _.sortBy(data.regions, region => {
        return region.display_name.toLowerCase();
      });
      _.remove(regions, region => {
        return region.privilege === 'R';
      });
      vm.regions = regions;
    }
  }

  async function _initApplicationTemplates() {
    try {
      const data = await templateService.templateList();
      vm.templateList = data.template_list;
      if ($stateParams['template_name']) {
        vm.selectTemplateEnabled = true;
        vm.templateName = $stateParams['template_name'];
      }
      vm.templateList.map(item => {
        item.display_name = item.space_name
          ? `${item.name}(${item.space_name})`
          : item.name;
        return item;
      });
    } catch (err) {
      $log.error('load templates error');
      vm.templateList = [];
    }
  }

  async function _initQuotaSpaces() {
    if (vm.quotaEnabled) {
      try {
        vm.spaces = await rbQuotaSpaceDataService.getConsumableSpaces();
      } catch (err) {
        $log.error('load spaces error!');
        vm.spaces = [];
      }
    }
  }

  function onRegionChange(option) {
    vm.appData.region = option.name;
  }

  function onSpaceChanged(space) {
    vm.appData.space_name = space.name;
    vm.form.app_name.$validate();
  }

  async function onTemplateNameChange(option) {
    vm.templateName = option.uuid;
    !templateCache[vm.templateName] &&
      (await _getTemplateDetail(vm.templateName));
    vm.appData.services = templateCache[vm.templateName];
    rbSafeApply();
  }

  function _getTemplateDetail(name) {
    vm.templateLoading = true;
    return templateService
      .templateDetail(name)
      .then(data => {
        templateCache[name] = data['template']['yaml'];
      })
      .catch(() => {})
      .then(() => {
        vm.templateLoading = false;
      });
  }

  function useDemoTemplate() {
    vm.appData.services = CUSTOM_EXAMPLE_YAML || example_yaml;
  }

  function validateAppNameDuplicate(name) {
    const space_name = vm.appData.space_name || '';
    return rbAppDataService
      .checkApplicationName({
        application_name: name,
        space_name,
      })
      .catch(({ data }) => {
        vm.errorCode = _.get(data, 'errors[0].code', '');
        if (!vm.errorCode || vm.errorCode === 'malformed_jakiro_response') {
          // 兼容之前的逻辑，如果是重名，api不会返回标准的错误结构，会在rubick转成malformed_jakiro_response，此时前端转成重名错误
          vm.errorCode = 'name_existed';
        }
        return Promise.reject(data);
      })
      .then(() => {
        vm.errorCode = '';
      });
  }

  function create() {
    vm.form.$setSubmitted();
    if (!vm.form.$valid) {
      return;
    }
    // check if yaml syntax is valid
    try {
      jsyaml.safeLoad(vm.appData.services, 'utf8');
    } catch (e) {
      rbToast.warning(translateService.get('yaml_file_invalid'));
      return;
    }
    rbConfirmBox
      .show({
        title: translateService.get('create'),
        textContent: translateService.get('app_service_create_app_confirm', {
          app_name: vm.appData.app_name,
        }),
      })
      .then(() => {
        _create();
      });
  }

  function _create() {
    vm.submitting = true;
    const namespace = vm.appData.namespace;
    const data = _.cloneDeep(vm.appData);
    delete data.namespace;
    if (data.services) {
      data.files = { services: ['compose.yaml', data.services] };
      delete data.services;
    }
    rbHttp
      .sendRequest({
        method: 'POST',
        url: '/ajax-sp/v1/fileview/applications/' + namespace,
        data,
        addNamespace: false,
      })
      .then(response => {
        if (response) {
          $state.go('app_service.app.app_detail', {
            app_name: response.uuid,
          });
        }
      })
      .catch(e => {
        const code = _.get(e, 'data.errors[0].code');
        if (code) {
          const errMsg =
            translateService.get('yaml_error') +
              '<br>' +
              _.get(e, 'data.errors[0].message') || '';
          rbToast.error(errMsg);
        }
      })
      .then(() => {
        vm.submitting = false;
      });
  }

  function cancel() {
    rbRouterStateHelper.back();
  }
}
