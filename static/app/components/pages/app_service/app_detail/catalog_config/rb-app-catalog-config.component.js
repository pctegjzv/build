const templateStr = require('app/components/pages/app_service/app_detail/catalog_config/rb-app-catalog-config.html');

angular.module('app.components.pages').component('rbAppCatalogConfig', {
  bindings: {
    displayInfo: '<', // display_info from app.catalog_info
  },
  controller: rbAppCatalogConfigController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbAppCatalogConfigController(translateService, rbGlobalSetting) {
  const vm = this;
  vm.$onInit = _init;
  vm.getFieldLabel = getFieldLabel;

  //////////
  function _init() {
    vm.editorOptions = rbGlobalSetting.getEditorOptions('yaml', true);
  }

  function getFieldLabel(field) {
    // zh_cn, en
    return translateService.getLanguage() === 'zh_cn'
      ? field.display_name.zh
      : field.display_name.en;
  }
}
