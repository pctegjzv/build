const templateStr = require('app/components/pages/app_service/app_detail/config_update/rb-app-config-update.html');

angular.module('app.components.pages').component('rbAppConfigUpdate', {
  bindings: {
    type: '<', // application type
    uuid: '<', // application uuid
    editableInfo: '<', // display_info from app.catalog_info
  },
  controller: rbAppConfigUpdateController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbAppConfigUpdateController(
  $log,
  rbGlobalSetting,
  translateService,
  rbModal,
  $timeout,
  rbAppDataService,
) {
  const vm = this;
  vm.$onInit = _init;
  vm.submit = submit;
  vm.cancel = cancel;
  vm.getFieldLabel = getFieldLabel;

  //////////
  function _init() {
    vm.editorOptions = rbGlobalSetting.getEditorOptions('yaml');
    vm.editableInfo = _.clone(vm.editableInfo);
    $timeout(() => {
      vm.editorInitialized = true;
    }, 200);
  }

  function submit() {
    vm.form.$setSubmitted();
    if (vm.form.$invalid) {
      return;
    }
    const payload = {};
    vm.editableInfo.forEach(item => {
      payload[item.attribute_name] = item.value;
    });
    try {
      rbAppDataService.updateCatalogApplication({
        type: vm.type,
        uuid: vm.uuid,
        data: payload,
      });
    } catch (error) {
      $log.error('update catalog app failed', error);
    }
    rbModal.hide(vm.editableInfo);
  }

  function cancel() {
    rbModal.cancel();
  }

  function getFieldLabel(field) {
    // zh_cn, en
    return translateService.getLanguage() === 'zh_cn'
      ? field.display_name.zh
      : field.display_name.en;
  }
}
