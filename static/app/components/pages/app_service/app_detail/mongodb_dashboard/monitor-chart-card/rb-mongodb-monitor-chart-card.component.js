const moment = require('moment');
const _ = require('lodash');
const templateStr = require('app/components/pages/app_service/app_detail/mongodb_dashboard/monitor-chart-card/rb-mongodb-monitor-chart-card.html');
angular.module('app.components.pages').component('rbMongodbMonitorChartCard', {
  controller: rbMongodbMonitorChartCardController,
  controllerAs: 'vm',
  bindings: {
    initTimeRange: '<',
    metric: '<', // {aggregator, group_by, metric, over, type}
    period: '<',
  },
  require: {
    rbMongodbAppDashboard: '?^^',
  },
  template: templateStr,
});

function rbMongodbMonitorChartCardController(
  $element,
  $log,
  $state,
  rbMonitorUtilities,
  translateService,
  rbModal,
) {
  const vm = this;
  let destroyed = false;
  let lastUpdateTime = 0,
    lastUpdateTimeRange;
  const $pageContainer = $('.page-scroll-container');

  vm.$onInit = _init;
  vm.$onDestroy = _destroy;
  vm.actionAlarmCreate = actionAlarmCreate;
  vm.actionChartRefresh = actionChartRefresh;
  vm.actionChartDetail = actionChartDetail;
  vm.getChartData = getChartData;
  vm.getTooltipText = getTooltipText;

  //////////

  async function _init() {
    vm.pointTooltipFn = rbMonitorUtilities.getPointFormat;
    vm.highChartsOptions = {
      chart: {
        type: _.get(vm.chart, 'metrics[0].type', 'line'),
        spacing: [20, 20, 20, 10],
      },
    };

    try {
      await getChartData({
        timeRange: vm.initTimeRange,
      });
    } catch (err) {
      $log.error(err);
    }

    vm.rbMongodbAppDashboard.registerChartController({
      metric_name: vm.metric.metric,
      controller: vm,
    });
    vm.initialized = true;
  }

  function _destroy() {
    destroyed = true;
    vm.rbMongodbAppDashboard.unRegisterChartController(vm.metric.metric);
  }

  function getChartData({ timeRange, refresh = false, metricOption = null }) {
    if (destroyed || vm.loading) {
      return Promise.resolve();
    }
    if (metricOption) {
      vm.metric.over = metricOption.over;
      vm.metric.group_by = metricOption.group_by;
    }
    if (
      !metricOption &&
      lastUpdateTimeRange &&
      lastUpdateTimeRange.start === timeRange.start &&
      lastUpdateTimeRange.end === timeRange.end
    ) {
      return;
    }
    if ((refresh || _chartDataOutofDate()) && _chartCardInViewport()) {
      return _getChartData({
        timeRange,
      });
    } else {
      return Promise.resolve();
    }
  }

  async function _getChartData({ timeRange }) {
    vm.loading = true;
    lastUpdateTimeRange = _.clone(timeRange);
    lastUpdateTime = moment().valueOf();
    try {
      const result = await rbMonitorUtilities.getChartData({
        metrics: [vm.metric],
        timeRange,
      });
      _initChart(result);
    } catch (err) {
      $log.error(err);
    }
    vm.loading = false;
  }

  function _initChart(result) {
    if (!result) {
      vm.series = null;
      return;
    }
    const { axisOptions, series } = result;
    vm.series = series;
    vm.axisOptions = axisOptions;
  }

  async function actionChartRefresh() {
    vm.refreshing = true;
    let timeRange;
    // for custom time range, period is -1
    if (vm.period > 0) {
      const end = moment().valueOf();
      timeRange = {
        start: end - vm.period,
        end,
      };
    } else {
      timeRange = lastUpdateTimeRange;
    }
    try {
      await _getChartData({
        timeRange,
      });
    } catch (err) {
      $log.error(err);
    }
    vm.refreshing = false;
  }

  function actionChartDetail() {
    rbModal.show({
      width: 900,
      component: 'rbMonitorChartDetail',
      locals: {
        timeRange: lastUpdateTimeRange,
        chart: {
          display_name: vm.metric.metric,
          metrics: [_.clone(vm.metric)],
        },
        initPeriod: vm.period,
      },
    });
  }

  function _chartDataOutofDate() {
    const now = moment().valueOf();
    return now - lastUpdateTime >= 60 * 1000;
  }

  function _chartCardInViewport() {
    const pageContainerHeight = $pageContainer.height(),
      elementTop = $element.offset().top,
      elementHeight = $element.height();

    const topConstraint = elementTop > 0,
      bottomConstraint = pageContainerHeight > elementTop + elementHeight / 2,
      heightConstraint = pageContainerHeight > elementHeight / 2;

    return topConstraint && bottomConstraint && heightConstraint;
  }

  function actionAlarmCreate() {
    $state.go('monitor.alarmv2.alarm_create', {
      metric: {
        metric_name: vm.metric.metric,
        aggregator: vm.metric.aggregator,
        tags: vm.metric.over.split(','),
      },
    });
  }

  function getTooltipText() {
    return translateService.getLanguage() === 'zh_cn'
      ? _.get(vm.metric, 'tips.zh', '')
      : _.get(vm.metric, 'tips.en', '');
  }
}
