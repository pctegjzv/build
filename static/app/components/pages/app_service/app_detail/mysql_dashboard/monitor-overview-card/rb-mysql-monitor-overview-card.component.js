const moment = require('moment');
const _ = require('lodash');
const templateStr = require('app/components/pages/app_service/app_detail/mysql_dashboard/monitor-overview-card/rb-mysql-monitor-overview-card.html');
angular.module('app.components.pages').component('rbMysqlMonitorOverviewCard', {
  controller: rbMysqlMonitorOverviewCardController,
  controllerAs: 'vm',
  bindings: {
    initTimeRange: '<',
    metric: '<', // {aggregator, group_by, metric, over, type}
  },
  require: {
    rbMysqlAppDashboard: '?^^',
  },
  template: templateStr,
});

function rbMysqlMonitorOverviewCardController(
  $state,
  $log,
  rbMonitorUtilities,
  translateService,
) {
  const vm = this;
  let destroyed;

  vm.$onInit = _init;
  vm.$onDestroy = _destroy;
  vm.actionAlarmCreate = actionAlarmCreate;
  vm.actionChartRefresh = actionChartRefresh;
  vm.getChartData = getChartData;
  vm.getTooltipText = getTooltipText;

  //////////

  async function _init() {
    vm.chartStatus = 'ok';
    try {
      await getChartData({
        timeRange: vm.initTimeRange,
      });
    } catch (err) {
      $log.error(err);
    }
    vm.rbMysqlAppDashboard.registerOverviewController({
      metric_name: vm.metric.metric,
      controller: vm,
    });
    vm.initialized = true;
  }

  function _destroy() {
    destroyed = true;
    vm.rbMysqlAppDashboard.unRegisterChartController(vm.metric.metric);
  }

  async function getChartData({ timeRange }) {
    if (destroyed || vm.loading) {
      return Promise.resolve();
    }
    vm.loading = true;
    try {
      const result = await rbMonitorUtilities.getChartData({
        metrics: [vm.metric],
        timeRange,
      });
      _initChart(result);
    } catch (err) {
      $log.error(err);
    }
    vm.loading = false;
  }

  function _initChart(result) {
    if (!result) {
      return;
    }
    const { series, axisOptions } = result;
    if (!series || !series.length || !axisOptions) {
      return;
    }
    const unit = series[0].unit;
    const categories = _.reverse(axisOptions.xAxis.categories);

    // 对于多组数据，按时间轴分别累加各组数据对应的值: [1,2,3],[2,3,4] => [3,5,7]
    // 当series只有一组数据时(agg为sum,max,min时，查询数据没有group_by)，等价于 results = series[0].data;
    const results = series[0].data.map((value, index) => {
      for (let i = 1; i < series.length; i++) {
        const s = series[i];
        const point = s.data[index];
        if (point === undefined || point === null) {
          return null;
        } else {
          value += point;
        }
      }
      return value;
    });
    // 从后往前取第一个非空值
    _.reverse(results);

    //console.log(results);

    const index = _.findIndex(results, item => {
      return item !== null;
    });
    if (index < 0) {
      return;
    }
    vm.hasData = true;
    const timestamp = categories[index] * 1000;
    const pointData = results[index];

    const warningThreshold = +vm.metric.threshold.Warning;
    const alertThreshold = +vm.metric.threshold.alert;

    if (!Number.isNaN(warningThreshold) && pointData > warningThreshold) {
      vm.chartStatus = 'warning';
    }
    if (!Number.isNaN(alertThreshold) && pointData > alertThreshold) {
      vm.chartStatus = 'alert';
    }
    //console.log('result', index, pointData, vm.metric.threshold);

    vm.timeDisplay = moment(timestamp).format('YYYY-MM-DD HH:mm:ss');
    if (unit === 'Percent') {
      vm.unitDisplay = '%';
      vm.dataDisplay = pointData === 0 ? 0 : (pointData * 100).toFixed(2) + '%';
    } else if (unit === 'Percent__real') {
      vm.unitDisplay = '%';
      vm.dataDisplay = pointData === 0 ? 0 : pointData.toFixed(2) + '%';
    } else if (unit.toLowerCase() === 'unknown') {
      vm.dataDisplay = _.isInteger(pointData)
        ? pointData.toLocaleString()
        : pointData.toFixed(2);
    } else {
      vm.unitDisplay = unit;
      vm.dataDisplay = _.isInteger(pointData)
        ? pointData.toLocaleString()
        : pointData.toFixed(2);
    }
  }

  async function actionChartRefresh() {
    vm.refreshing = true;
    const end = moment().valueOf();
    try {
      await getChartData({
        timeRange: {
          start: end - 30 * 60 * 1000,
          end,
        },
      });
    } catch (err) {
      $log.error(err);
    }
    vm.refreshing = false;
  }

  function actionAlarmCreate() {
    // TODO: tags 包含该应用下所有服务的名称
    $state.go('monitor.alarmv2.alarm_create', {
      metric: {
        metric_name: vm.metric.metric,
        aggregator: vm.metric.aggregator,
        tags: vm.metric.over.split(','),
      },
    });
  }

  function getTooltipText() {
    return translateService.getLanguage() === 'zh_cn'
      ? _.get(vm.metric, 'tips.zh', '')
      : _.get(vm.metric, 'tips.en', '');
  }
}
