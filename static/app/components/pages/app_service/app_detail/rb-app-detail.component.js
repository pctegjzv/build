import { resource_types as RESOURCE_TYPES } from '../../rbac_role/rb-rbac-helper.constant';

/**
 * Created by liudong on 16/10/23.
 */
const templateStr = require('app/components/pages/app_service/app_detail/rb-app-detail.html');
import { SERVICE_INTERMEDIATE_STATUS } from '../rb-service.constant';

angular.module('app.components.pages').component('rbAppDetail', {
  bindings: {
    appName: '<',
  },
  controller: rbAppDetailController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbAppDetailController(
  $filter,
  $state,
  rbAppDataService,
  rbServiceUtilities,
  rbAppUtilities,
  rbRoleUtilities,
  rbRouterStateHelper,
  templateService,
  rbGlobalSetting,
  rbConfirmBox,
  translateService,
  rbLabelsDataService,
  rbToast,
  rbModal,
  rbRegionService,
  WEBLABS,
  $timeout,
) {
  const vm = this;
  let yamlPromise,
    shortTimer,
    longTimer,
    appDeleted = false,
    componentDestroyed = false;

  vm.onTabSelected = onTabSelected;
  vm.actionStart = actionStart;
  vm.actionStop = actionStop;
  vm.actionUpdate = actionUpdate;
  vm.actionDelete = actionDelete;
  vm.actionRetry = actionRetry;
  vm.actionUpdateConfig = actionUpdateConfig;
  vm.exportTemplate = exportTempalte;
  vm.isStatefulService = isStatefulService;
  vm.buttonDisplayExpr = buttonDisplayExpr;
  vm.updateButtonToolTip = updateButtonToolTip;
  vm.getServiceUrl = getServiceUrl;
  vm.refreshOnAction = refreshOnAction;
  vm.shouldShowAppUpdateBtn = shouldShowAppUpdateBtn;
  vm.shouldShowConfigUpdateBtn = shouldShowConfigUpdateBtn;
  vm.shouldShowSpringDashboard = shouldShowSpringDashboard;
  vm.shouldShowRedisDashboard = shouldShowRedisDashboard;
  vm.shouldShowMysqlDashboard = shouldShowMysqlDashboard;
  vm.shouldShowMongodbDashboard = shouldShowMongodbDashboard;
  vm.shouldShowAppCatalogConfig = shouldShowAppCatalogConfig;

  vm.$onInit = _init;
  vm.$onDestroy = _destroy;

  //////////

  async function _init() {
    vm.basicInfoFields = [];
    vm.app = null;
    vm.editorOptions = rbGlobalSetting.getEditorOptions('yaml', true);
    vm.appUtilities = rbAppUtilities;
    vm.showLabelsDialog = rbLabelsDataService.showLabelsDialog;
    vm.appTemplateCreateEnabled = await rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.APPLICATION_TEMPLATE,
    );
    getAppDetail();
  }

  function _destroy() {
    _cancelTimer();
    componentDestroyed = true;
  }

  function _initShortTimer() {
    _cancelTimer();
    shortTimer = $timeout(() => {
      if (!vm.appInAction) {
        return;
      }
      getAppDetail();
    }, 3000);
  }

  function _initLongTimer() {
    _cancelTimer();
    longTimer = $timeout(() => {
      if (vm.appInAction) {
        return;
      }
      getAppDetail();
    }, 30000);
  }

  function _cancelTimer() {
    $timeout.cancel(shortTimer);
    $timeout.cancel(longTimer);
  }

  function _updatePollingTimer() {
    // check if component destroyed
    if (componentDestroyed) {
      _cancelTimer();
      return;
    }
    // check if service is in action
    vm.appInAction =
      vm.app.current_status.endsWith('ing') &&
      vm.app.current_status !== 'Running';
    if (vm.appInAction) {
      _initShortTimer();
    } else {
      _initLongTimer();
    }
  }

  function getAppDetail() {
    rbAppDataService
      .getAppDetail(vm.appName, true)
      .then(data => {
        if (!componentDestroyed) {
          vm.app = data.app;
          vm.app_display_name = vm.app.app_name;
          if (
            vm.app.type === 'mysql' &&
            vm.app.catalog_info.management.admin_url
          ) {
            vm.admin_url = data.app.catalog_info.management.admin_url;
          }
          rbRegionService.setRegionByName(vm.app.region_name);
          updateBasicInfoFields();
        }
      })
      .catch(e => {
        // requesting application does not exist
        if (!vm.initialized && e.status !== 403) {
          rbToast.warning(translateService.get('application_not_exist'));
        }
        if ((!componentDestroyed && appDeleted) || !vm.app) {
          // if deleted when just created, go to list
          rbRouterStateHelper.stateHistory.length > 0 &&
          rbRouterStateHelper.stateHistory[
            rbRouterStateHelper.stateHistory.length - 1
          ].state.name === 'app_service.app.create_app'
            ? $state.go('app_service.app.list')
            : rbRouterStateHelper.back();
        }
      })
      .then(() => {
        vm.initialized = true;
        vm.app && _updatePollingTimer();
      });
  }

  function updateBasicInfoFields() {
    if (!vm.app.type) {
      vm.basicInfoIconUrl = '/static/images/app/default.png';
    } else {
      vm.basicInfoIconUrl = `/static/images/app-catalog/${vm.app.type.toLowerCase()}.png`;
    }
    vm.basicInfoFields = [
      {
        name: 'type',
        value: rbAppUtilities.getAppTypeDisplay(vm.app.type, vm.app.source),
      },
      {
        name: 'cluster',
        html: `<a href="/console/cluster/detail/${vm.app.region_name}">${
          vm.app.region_name
        }</a>`,
      },
      {
        name: 'quota_space',
        value: vm.app.space_name,
        disabled: !WEBLABS.QUOTA_ENABLED,
        state: `quota.detail.${vm.app.space_name}`,
      },
      {
        name: 'created_by',
        value: vm.app.created_by,
      },
      {
        name: 'created_time',
        value: $filter('formatUtcStr')(vm.app.created_at),
      },
      {
        name: 'updated_time',
        value: $filter('formatUtcStr')(vm.app.updated_at),
      },
      {
        name: 'labels',
        row: true,
        html: rbServiceUtilities.getLabelsHtml(vm.app.labels),
      },
    ];
  }

  function onTabSelected(type = '') {
    vm.selectedTab = type;
    switch (type) {
      case 'yaml':
        _getYaml();
        break;
      case 'compose-yaml':
        _getComposeYaml();
        break;
      default:
        break;
    }
  }

  function actionStart() {
    rbConfirmBox
      .show({
        title: translateService.get('start'),
        textContent: translateService.get('app_service_start_app_confirm'),
      })
      .then(() => {
        _appAction('start');
      });
  }

  function actionUpdate() {
    $state.go('app_service.app.update_app', { app_name: vm.appName });
  }

  function actionUpdateConfig() {
    rbModal
      .show({
        width: 750,
        title: translateService.get('app_update_config'),
        component: 'rbAppConfigUpdate',
        locals: {
          type: vm.app.type,
          uuid: vm.app.uuid,
          editableInfo: vm.app.catalog_info.display_info.editable_info,
        },
      })
      .then(() => {
        rbToast.success(
          translateService.get('app_catalog_config_update_success'),
        );
        vm.app.current_status = SERVICE_INTERMEDIATE_STATUS.UPDATING;
        $timeout(() => {
          getAppDetail();
        }, 3000);
      })
      .catch(() => {});
  }

  function actionStop() {
    rbConfirmBox
      .show({
        title: translateService.get('stop'),
        textContent: translateService.get('app_service_stop_app_confirm'),
      })
      .then(() => {
        _appAction('stop');
      });
  }

  function actionDelete() {
    rbConfirmBox
      .show({
        title: translateService.get('delete'),
        textContent: translateService.get('app_service_delete_app_confirm', {
          app_name: vm.app_display_name,
        }),
      })
      .then(() => {
        _appAction('delete');
        $state.go('app_service.app.list');
      });
  }

  function actionRetry() {
    _appAction('retry');
  }

  function exportTempalte() {
    vm.appActionLoading = true;
    _getYaml().then(() => {
      templateService.setExport(vm.yaml);
      vm.appActionLoading = false;
      $state.go('app_service.template.create', { export: true });
    });
  }

  function _appAction(action) {
    vm.appActionLoading = true;
    const params = {
      action: action,
      namespace: vm.app.namespace,
      app_name: vm.appName,
    };
    vm.appInAction = true;
    vm.appActionLoading = true;
    rbAppDataService
      .appAction(params)
      .then(() => {})
      .catch(() => {})
      .then(() => {
        vm.appActionLoading = false;
        // update current_status immediately, wait for 3s, then get app detail
        switch (action) {
          case 'start':
            vm.app.current_status = SERVICE_INTERMEDIATE_STATUS.STARTING;
            break;
          case 'stop':
            vm.app.current_status = SERVICE_INTERMEDIATE_STATUS.STOPPING;
            break;
          case 'delete':
            vm.app.current_status = SERVICE_INTERMEDIATE_STATUS.DELETING;
            break;
          case 'retry':
            vm.app.current_status = SERVICE_INTERMEDIATE_STATUS.STARTING;
            break;
          case 'update':
            vm.app.current_status = SERVICE_INTERMEDIATE_STATUS.UPDATING;
            break;
          default:
            break;
        }
        if (action === 'delete') {
          appDeleted = true;
        }
        refreshOnAction();
      });
  }

  function refreshOnAction() {
    $timeout(() => {
      getAppDetail();
    }, 3000);
  }

  function _getYaml() {
    if (!vm.yaml && !vm.yamlLoading) {
      vm.yamlLoading = true;
      yamlPromise = rbAppDataService
        .getAppYaml(vm.appName)
        .then(data => {
          vm.yaml = data.yaml;
        })
        .catch(() => {})
        .then(() => {
          vm.yamlLoading = false;
        });
    }
    return yamlPromise;
  }

  function _getComposeYaml() {
    if (vm.composeYaml || vm.composeLoading) {
      return;
    }
    vm.composeLoading = true;
    rbAppDataService
      .getAppComposeYaml(vm.appName)
      .then(data => {
        vm.composeYaml = data.yaml;
      })
      .catch(() => {})
      .then(() => {
        vm.composeLoading = false;
      });
  }

  function isStatefulService(service) {
    return service.volumes && service.volumes.length;
  }

  function buttonDisplayExpr(action) {
    return rbRoleUtilities.resourceHasPermission(
      vm.app,
      RESOURCE_TYPES.APPLICATION,
      action,
    );
  }

  function updateButtonToolTip() {
    const disabled = !vm.appUtilities.updateButton(vm.app.current_status);
    return translateService.get(
      disabled ? 'app_service_update_app_invalid' : 'update',
    );
  }

  function getServiceUrl(service) {
    return $state.href('app_service.service.service_detail', {
      service_name: service.uuid,
    });
  }

  function shouldShowAppUpdateBtn() {
    return !vm.app.type; // type存在表示应用市场创建的服务，不允许更新
  }

  function shouldShowConfigUpdateBtn() {
    const editableInfo = _.get(
      vm.app,
      'catalog_info.display_info.editable_info',
      [],
    );
    return editableInfo.length;
  }

  function shouldShowSpringDashboard() {
    return (
      _.get(vm.app, 'catalog_info.has_dashboard', false) &&
      vm.app.type.includes('spring_')
    );
  }

  function shouldShowRedisDashboard() {
    return vm.app.type === 'redis';
  }

  function shouldShowMysqlDashboard() {
    return vm.app.type === 'mysql';
  }

  function shouldShowMongodbDashboard() {
    return vm.app.type === 'mongodb';
  }

  function shouldShowAppCatalogConfig() {
    return _.get(vm.app, 'catalog_info.display_info', null);
  }
}
