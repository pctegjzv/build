const moment = require('moment');
const ElementQueries = require('css-element-queries/src/ElementQueries');

const templateStr = require('app/components/pages/app_service/app_detail/redis_dashboard/rb-redis-app-dashboard.html');

angular.module('app.components.pages').component('rbRedisAppDashboard', {
  bindings: {
    /**
       *  metric_name list used in current redis app
       [{
         # 指标名称
         "metric_name": "redis.net.clients",
         # 指标类型
         "metric_type": "gauge",
         # 指标值的算法
         "agg": "avg",
         # 图表类型，分为以下类型：折线图(line)、柱状图(column)、面积图(area)、总览图(overview)
         "chart_type": "line",
         # 对该指标的补充说明
         "tips": {
           "zh": "当前客户端连接数",
           "en": "The number of connected clients (excluding slaves)",
         },
         # 如果是单一值指标，可以设置警告阈值和危险阈值，到达阈值时界面背景色分别显示黄色和红色，默认显示绿色。
         "threshold": {
           "warning": 1000,
           "alert": 3000
         }
       }]
       */
    //metrics: '<',
    //services: '<' // service list in current reids app [{service_name, uuid}]
    app: '<',
  },
  controller: rbRedisAppDashboardController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbRedisAppDashboardController(
  $state,
  $element,
  $interval,
  rbMonitorDataService,
  rbMonitorUtilities,
  rbAccountService,
  rbPrivilegeService,
  rbModal,
  rbToast,
  rbSafeApply,
  $timeout,
  rbRoleUtilities,
  rbSuperbar,
  WEBLABS,
  translateService,
) {
  const vm = this;

  let timer = null;
  let destroyed = false;
  let pageContainerScrollListener;
  const chartControllers = [],
    overviewControllers = [];
  const $pageContainer = $('.page-scroll-container');

  vm.onServiceChange = onServiceChange;
  vm.onTimeRangeChange = onTimeRangeChange;
  vm.onCustomTimeRangeChange = onCustomTimeRangeChange;
  vm.isCustomTimeRange = isCustomTimeRange;
  vm.registerChartController = registerChartController;
  vm.registerOverviewController = registerOverviewController;
  vm.unRegisterChartController = unRegisterChartController;
  vm.unRegisterOverviewController = unRegisterOverviewController;

  vm.$onInit = _init;
  vm.$postLink = _postLink;
  vm.$onDestroy = _destroy;

  //////////
  function _init() {
    const metrics = _.get(vm.app, 'catalog_info.metrics', []);
    const services = _.get(vm.app, 'services', []);
    const end = moment().valueOf();
    vm.initTimeRange = {
      start: end - 30 * 60 * 1000,
      end,
    };
    vm.dateTimeOptions = rbMonitorUtilities.getDateTimeOptions();
    vm.periodOptions = rbMonitorUtilities.getPeriodOptions();

    vm.serviceOptions = [
      {
        service_name: translateService.get('all'),
        uuid: 'all',
      },
    ].concat(services);

    vm.chartMetrics = metrics
      .filter(item => {
        return item.chart_type !== 'overview';
      })
      .map(item => {
        return {
          metric: item.metric_name,
          aggregator: item.agg,
          over: getServiceNameQueryString(), // TODO: over: app_id/app_name
          group_by: 'service_name',
          type: item.chart_type,
          tips: item.tips,
        };
      });
    vm.overviewMetrics = metrics
      .filter(item => {
        return item.chart_type === 'overview';
      })
      .map(item => {
        return {
          metric: item.metric_name,
          aggregator: item.agg,
          type: item.chart_type,
          over: getServiceNameQueryString(), // TODO: over: app_id/app_name
          group_by: item.agg === 'avg' ? 'service_name' : '', // 对于 'sum'/'max'/'min', 数据统计由API来做
          tips: item.tips,
          threshold: item.threshold,
        };
      });

    _initTimer();
  }

  function _postLink() {
    ElementQueries.init();
    _initPageContainerEventListener();
  }

  function _destroy() {
    destroyed = true;
    $pageContainer.off('scroll', pageContainerScrollListener);
  }

  function _initPageContainerEventListener() {
    pageContainerScrollListener = _.throttle(() => {
      if (vm.currentPeriod > 0) {
        _notifyChartCard({}); // use time period
      } else {
        _notifyChartCard({
          timeRange: vm.timeRange, // use custom time range
        });
      }
    }, 500);
    $pageContainer.on('scroll', pageContainerScrollListener);
  }

  function _initTimer() {
    $interval.cancel(timer);
    timer = $interval(() => {
      _notifyChartCard({});
      _notifyOverviewCard();
    }, 60 * 1000);
  }

  function _notifyOverviewCard() {
    const end = moment().valueOf();
    overviewControllers.forEach(item => {
      item.controller.getChartData({
        timeRange: {
          start: end - 30 * 60 * 1000,
          end,
        },
      });
    });
  }

  function _notifyChartCard({
    timeRange = null,
    refresh = false,
    metricOption = null, // {over: '', group_by: ''}
  }) {
    if (destroyed) {
      return;
    }
    if (!timeRange) {
      const end = moment().valueOf();
      timeRange = {
        start: end - vm.currentPeriod,
        end,
      };
    }
    chartControllers.forEach(item => {
      item.controller.getChartData({
        timeRange,
        metricOption,
        refresh,
      });
    });
  }

  function registerChartController({ metric_name, controller }) {
    chartControllers.push({
      metric_name,
      controller,
    });
  }

  function registerOverviewController({ metric_name, controller }) {
    overviewControllers.push({
      metric_name,
      controller,
    });
  }

  function unRegisterChartController(metric_name) {
    _.remove(chartControllers, item => {
      return metric_name === item.metric_name;
    });
  }

  function unRegisterOverviewController(metric_name) {
    _.remove(overviewControllers, item => {
      return metric_name === item.metric_name;
    });
  }

  function onServiceChange(option) {
    if (option.uuid === 'all') {
      _notifyChartCard({
        refresh: true,
        metricOption: {
          over: getServiceNameQueryString(), // TODO: over: app_id
          group_by: 'service_name',
        },
      });
    } else {
      // over: service_id 使用service_id的话，在创建告警是无法初始化筛选条件中的服务名称
      let over;
      const app = vm.app;
      if (vm.app.space_name) {
        over = `service_name=${app.space_name}.${app.app_name}.${
          option.service_name
        }`;
      } else {
        over = `service_name=${app.app_name}.${option.service_name}`;
      }
      _notifyChartCard({
        refresh: true,
        metricOption: {
          over,
          group_by: '',
        },
      });
    }
  }

  function onCustomTimeRangeChange() {
    if (vm.timeRange.end - vm.timeRange.start < 30 * 60 * 1000) {
      rbToast.warning(translateService.get('metric_timerange_should_less_30'));
      return;
    }
    _notifyChartCard({
      timeRange: vm.timeRange,
      refresh: true,
    });
  }

  function onTimeRangeChange(option) {
    vm.currentPeriod = option.period;
    if (!isCustomTimeRange()) {
      const end = moment().valueOf();
      // update time range values for custom time range input field
      vm.timeRange = {
        start: end - vm.currentPeriod,
        end,
      };
      _notifyChartCard({
        refresh: true,
      });
    }
  }

  function getServiceNameQueryString() {
    // service_name=space_name.app_name.service_name
    const app = vm.app;
    return app.services
      .map(service => {
        //return `service_id=${service.uuid}`;
        if (app.space_name) {
          return `service_name=${app.space_name}.${app.app_name}.${
            service.service_name
          }`;
        } else {
          return `service_name=${app.app_name}.${service.service_name}`;
        }
      })
      .join(',');
  }

  function isCustomTimeRange() {
    return vm.currentPeriod === -1;
  }
}
