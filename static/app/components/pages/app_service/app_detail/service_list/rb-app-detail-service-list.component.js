import { Subject, BehaviorSubject, combineLatest, merge } from 'rxjs';
import {
  debounceTime,
  map,
  publishReplay,
  refCount,
  scan,
  share,
} from 'rxjs/operators';

import { resource_types as RESOURCE_TYPES } from '../../../rbac_role/rb-rbac-helper.constant';

import templateStr from 'app/components/pages/app_service/app_detail/service_list/rb-app-detail-service-list.html';

angular.module('app.components.pages').component('rbAppDetailServiceList', {
  controller: rbAppDetailServiceListController,
  controllerAs: 'vm',
  template: templateStr,
  bindings: {
    app: '<',
  },
  require: {
    rbAppDetail: '?^^',
  },
});

function rbAppDetailServiceListController(
  $log,
  $state,
  translateService,
  rbConfirmBox,
  rbRoleUtilities,
  rbServiceUtilities,
  rbServiceDataService,
) {
  const vm = this;

  vm.$onInit = _init;
  vm.$onDestroy = _destroy;
  vm.$onChanges = _onChanges;

  vm.startService = startService;
  vm.stopService = stopService;
  vm.deleteService = deleteService;
  vm.updateService = updateService;
  vm.promoteService = promoteService;
  vm.onPageChanged = onPageChanged;
  vm.onFilterChanged = onFilterChanged;
  vm.getServiceLinkDisplay = getServiceLinkDisplay;
  vm.getServiceUrl = getServiceUrl;
  vm.buttonIfExpr = buttonIfExpr;
  vm.buttonDisabledExpr = buttonDisabledExpr;
  vm.pagedServices = [];
  vm.pagination = {
    pageSize: 20,
    page: 1,
    count: 0,
  };

  const inActionServices = {};
  const services$ = new BehaviorSubject([]);
  const servicesWrapper$ = services$.pipe(map(list => () => list));
  const filterFnSubject$ = new BehaviorSubject(list => list).pipe(
    debounceTime(200),
  );
  const serviceActionSubject$ = new Subject().pipe(
    map(service => list =>
      // `linked_to_services` does not support service name in independent service API response
      list.map(
        item =>
          item.uuid === service.uuid
            ? Object.assign(item, {
                current_status: service.current_status,
              })
            : item,
      ),
    ),
    share(),
  );

  const combinedServices$ = merge(servicesWrapper$, serviceActionSubject$).pipe(
    scan((list, fn) => fn(list), []),
  );

  const servicePageChanged$ = new BehaviorSubject(1);

  const filteredServices$ = combineLatest(
    combinedServices$,
    filterFnSubject$,
  ).pipe(
    map(([services, filterFn]) => services.filter(filterFn)),
    publishReplay(1),
    refCount(),
  );

  vm.services$ = filteredServices$;

  vm.pagedServices$ = combineLatest(
    filteredServices$,
    servicePageChanged$,
  ).pipe(
    map(([services, page]) =>
      services.slice(
        (page - 1) * vm.pagination.pageSize,
        page * vm.pagination.pageSize,
      ),
    ),
  );

  const subscription = filteredServices$.subscribe(services => {
    vm.pagination.count = services.length;
    const maxPage =
      Math.ceil(vm.pagination.count / vm.pagination.pageSize) || 1;
    vm.pagination.page =
      maxPage > vm.pagination.page ? vm.pagination.page : maxPage;
    onPageChanged(vm.pagination.page);
  });

  //////////
  function _init() {
    vm.getServiceImageFullName = rbServiceUtilities.getServiceImageFullName;
    vm.getServiceImageTooltip = rbServiceUtilities.getServiceImageTooltip;
  }

  function _destroy() {
    subscription.unsubscribe();
  }

  function _onChanges({ app }) {
    if (app && app.currentValue) {
      parseServices(app.currentValue);
    }
  }

  function parseServices(app) {
    const services = app.services || [];
    services$.next(services);
  }

  function onPageChanged(page) {
    vm.pagination.page = page;
    servicePageChanged$.next(page);
  }

  function onFilterChanged() {
    filterFnSubject$.next(service => {
      return service.service_name.includes(vm.filterKey);
    });
  }

  async function startService(service) {
    await rbConfirmBox.show({
      title: translateService.get('start'),
      textContent: translateService.get('app_service_start_service_confirm', {
        service_name: service.service_name,
      }),
    });
    _doServiceAction(service, 'start');
  }

  async function stopService(service) {
    await rbConfirmBox.show({
      title: translateService.get('stop'),
      textContent: translateService.get('app_service_stop_service_confirm', {
        service_name: service.service_name,
      }),
    });
    _doServiceAction(service, 'stop');
  }
  async function promoteService(service) {
    await rbConfirmBox.show({
      title: translateService.get('promote'),
      textContent: translateService.get('app_service_promote_service_confirm', {
        app_name: service.service_name,
      }),
    });
    _doServiceAction(service, 'promote');
  }

  async function deleteService(service) {
    await rbConfirmBox.show({
      title: translateService.get('delete'),
      textContent: translateService.get('delete_app_content', {
        app_name: service.service_name,
      }),
    });
    try {
      inActionServices[service.uuid] = 1;
      await rbServiceDataService.deleteService({
        application: vm.app.app_name,
        service_name: service.uuid,
      });
      serviceActionSubject$.next({
        uuid: service.uuid,
        current_status: 'Deleting',
      });
      delete inActionServices[service.uuid];
      vm.rbAppDetail.refreshOnAction();
    } catch (error) {
      $log.error('delete service failed', error);
    }
  }

  function updateService(service) {
    $state.go('app_service.service.update_service', {
      service_name: service.uuid,
    });
  }

  function buttonIfExpr(service, action) {
    return rbRoleUtilities.resourceHasPermission(
      service,
      RESOURCE_TYPES.SERVICE,
      action,
    );
  }

  function getServiceUrl(service) {
    return $state.href('app_service.service.service_detail', {
      service_name: service.uuid,
    });
  }

  function getServiceLinkDisplay(service) {
    return service.linked_to_services.map(link => link.dest).join(', ');
  }

  function buttonDisabledExpr(service, action) {
    if (inActionServices[service.uuid]) {
      return true;
    }
    switch (action) {
      case 'start':
        return !rbServiceUtilities.startButton(service.current_status);
      case 'stop':
        return !rbServiceUtilities.stopButton(service.current_status);
      case 'delete':
        return !rbServiceUtilities.deleteButton(service.current_status);
      case 'update':
        return !rbServiceUtilities.updateButton(service.current_status);
      // case 'promote':
      //   return !rbServiceUtilities.promoteButton(service.current_status); // todo: promote to master
      default:
        return false;
    }
  }

  async function _doServiceAction(service, action) {
    try {
      inActionServices[service.uuid] = 1;
      const result = await rbServiceDataService.serviceAction({
        application: vm.app.app_name,
        service_name: service.uuid,
        action,
      });
      serviceActionSubject$.next(result);
      delete inActionServices[service.uuid];
      vm.rbAppDetail.refreshOnAction();
    } catch (error) {
      $log.error('start service failed', error);
    }
  }
}
