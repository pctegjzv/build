const templateStr = require('app/components/pages/app_service/app_detail/spring_dashboard/rb-app-spring-dashboard.html');

angular.module('app.components.pages').component('rbAppSpringDashboard', {
  bindings: {
    app: '<',
  },
  controller: rbAppSpringDashboardController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbAppSpringDashboardController($log, rbAppDataService) {
  const vm = this;
  vm.$onInit = _init;
  vm.shouldShowDashboardHeader = shouldShowDashboardHeader;
  vm.onTurbineAppChange = onTurbineAppChange;

  //////////

  async function _init() {
    // if type is spring_hystrix_dashboard, allow user to choose trubine_uuid
    if (vm.app.type === 'spring_hystrix_dashboard') {
      await getTrubineApplicationList();
    } else {
      vm.dashboardUrl = vm.app.catalog_info.dashboard_url;
    }
    vm.initialized = true;
  }

  async function getTrubineApplicationList() {
    try {
      vm.turbineAppList = await rbAppDataService.getApps({
        regionName: vm.app.region_name,
        type: 'spring_turbine',
      });
    } catch (error) {
      $log.error('get trubine app list failed', error);
    }
    vm.initialized = true;
  }

  function shouldShowDashboardHeader() {
    return vm.app.type === 'spring_hystrix_dashboard';
  }

  function onTurbineAppChange(option) {
    _getDashboardUrl(option.uuid);
  }

  async function _getDashboardUrl(uuid) {
    try {
      const dashboard_service_id = _.get(vm.app, 'services[0].uuid', '');
      const { dashboard_url } = await rbAppDataService.getHystrixDashboardUrl({
        uuid: vm.app.uuid,
        trubine_uuid: uuid,
        dashboard_service_id,
      });
      vm.dashboardUrl = dashboard_url;
    } catch (error) {
      $log.error('get dashboard url failed', error);
    }
  }
}
