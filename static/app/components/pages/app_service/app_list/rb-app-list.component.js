import { resource_types as RESOURCE_TYPES } from '../../rbac_role/rb-rbac-helper.constant';

import template from './rb-app-list.html';

const appServicesTemplate = require('ngtemplate-loader!app/components/pages/app_service/app_list/rb-app-services-popover.html');
const resourceLabelsPopoverTemplate = require('ngtemplate-loader!app/components/pages/app_service/list/rb-app-service-list-labels-popover.html');

angular.module('app.components.pages').component('rbAppList', {
  controller: rbAppListController,
  controllerAs: 'vm',
  template,
});

function rbAppListController(
  $log,
  $state,
  WEBLABS,
  translateService,
  rbConfirmBox,
  rbRoleUtilities,
  rbServiceUtilities,
  rbAppUtilities,
  rbRegionService,
  rbAppDataService,
  rbLabelsDataService,
  rbRoleDataService,
) {
  const vm = this;
  const inActionApps = {};

  vm.onRegionSelected = onRegionSelected;
  vm.onPageChanged = onPageChanged;
  vm.refresh = refresh;

  vm.buttonIfExpr = buttonIfExpr;
  vm.buttonDisabledExpr = buttonDisabledExpr;
  vm.startApp = startApp;
  vm.stopApp = stopApp;
  vm.updateApp = updateApp;
  vm.updateAppLabels = updateAppLabels;
  vm.deleteApp = deleteApp;
  vm.getServiceUrl = getServiceUrl;
  vm.getAppUrl = getAppUrl;

  vm.$onInit = _init;
  vm.$onDestroy = _destroy;

  //////////

  function _init() {
    vm.pagination = {
      pageSize: 20,
      page: 1,
      count: 0,
    };

    vm.quotaEnabled = WEBLABS.QUOTA_ENABLED;
    vm.resourceLabelsPopoverTemplate = resourceLabelsPopoverTemplate;
    vm.appServicesTemplate = appServicesTemplate;
    vm.getAppTypeDisplay = rbAppUtilities.getAppTypeDisplay;

    const fetchRequest = ([regionName, params]) => {
      return rbAppDataService.getApps({
        basic: true,
        regionName: regionName,
        labels: 1,
        label: params.label,
        name: params.search,
        page: vm.pagination.page,
        page_size: vm.pagination.pageSize,
        service_info: false,
        with_status: true,
      });
    };
    rbServiceUtilities.initObservables({
      scope: vm,
      rbRegionService,
      onRegionSelected,
      fetchRequest,
    });
  }

  function _destroy() {
    rbServiceUtilities.destroySubscriptions(vm);
  }

  function refresh() {
    vm.refreshSubject$.next(null);
  }

  async function onRegionSelected(region) {
    if (
      region &&
      (!vm.currentRegion || vm.currentRegion.name !== region.name)
    ) {
      vm.currentRegion = region;
      vm.pagination.page = 1;
      const appPermissions = await rbRoleDataService.getContextPermissions(
        RESOURCE_TYPES.APPLICATION,
        {
          cluster_name: region.name,
        },
      );
      vm.appCreateEnabled = appPermissions.includes(
        `${RESOURCE_TYPES.APPLICATION}:create`,
      );
    } else {
      vm.appCreateEnabled = false;
    }
  }

  /**
   * pagination component callback
   * @param page
   */
  function onPageChanged(page) {
    if (page !== vm.pagination.page) {
      vm.pagination.page = page;
      vm.pageSubject$.next(page);
    }
  }

  function buttonIfExpr(app, type) {
    return rbRoleUtilities.resourceHasPermission(
      app,
      RESOURCE_TYPES.APPLICATION,
      type,
    );
  }

  function buttonDisabledExpr(app, type) {
    if (app && inActionApps[app.uuid]) {
      return true;
    }
    switch (type) {
      case 'start':
        return !rbAppUtilities.startButton(app.current_status);
      case 'stop':
        return !rbAppUtilities.stopButton(app.current_status);
      case 'delete':
        return !rbAppUtilities.deleteButton(app.current_status);
      case 'update':
        return !rbAppUtilities.updateButton(app.current_status);
      case 'updatelabel':
        return ['Deleting', 'Deleted', 'Removed'].includes(app.current_status);
      case 'add':
        return false;
    }
    return false;
  }

  async function _doAppAction(app, action) {
    try {
      inActionApps[app.uuid] = 1;
      // TODO: application PUT API does not return current app instance, only return code 200
      await rbAppDataService.appAction({
        app_name: app.uuid,
        action,
      });
      let current_status;
      switch (action) {
        case 'delete':
          current_status = 'Deleting';
          break;
        case 'start':
          current_status = 'Starting';
          break;
        case 'stop':
          current_status = 'Stopping';
          break;
      }
      vm.actionSubject$.next({
        uuid: app.uuid,
        current_status,
      });
      delete inActionApps[app.uuid];
    } catch (error) {
      $log.error('app action failed', action, error);
    }
  }

  async function startApp(app) {
    await rbConfirmBox.show({
      title: translateService.get('start'),
      textContent: translateService.get('app_service_start_app_confirm'),
    });
    _doAppAction(app, 'start');
  }

  async function stopApp(app) {
    await rbConfirmBox.show({
      title: translateService.get('stop'),
      textContent: translateService.get('app_service_stop_app_confirm'),
    });
    _doAppAction(app, 'stop');
  }

  async function deleteApp(app) {
    await rbConfirmBox.show({
      title: translateService.get('delete'),
      textContent: translateService.get('app_service_delete_app_confirm', {
        app_name: app.app_name,
      }),
    });
    _doAppAction(app, 'delete');
  }

  function updateApp(app) {
    $state.go('app_service.app.update_app', {
      app_name: app.uuid,
    });
  }

  function updateAppLabels(app) {
    const app_name = app.uuid;
    rbLabelsDataService
      .showLabelsDialog(app.namespace, 'app', app_name)
      .then(labels => {
        if (labels) {
          vm.actionSubject$.next({
            uuid: app.uuid,
            labels: [...labels],
          });
        }
      });
  }

  function getServiceUrl(app, service) {
    return $state.href('app_service.service.service_detail', {
      service_name: service.uuid,
    });
  }

  function getAppUrl(app) {
    return $state.href('app_service.app.app_detail', {
      app_name: app.uuid,
    });
  }
}
