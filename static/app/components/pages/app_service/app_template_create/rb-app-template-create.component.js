/**
 * Created by liudong on 16/10/11.
 */
const jsyaml = require('js-yaml');
const templateStr = require('app/components/pages/app_service/app_template_create/rb-app-template-create.html');
angular.module('app.components.pages').component('rbAppTemplateCreate', {
  controller: rbAppTemplateCreateController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbAppTemplateCreateController(
  $state,
  $location,
  templateService,
  rbGlobalSetting,
  rbToast,
  translateService,
  WEBLABS,
  rbQuotaSpaceDataService,
  rbRouterStateHelper,
) {
  const vm = this;
  vm.submitting = false;
  vm.templateList = [];
  vm.selectedTempalteName = '';
  vm.selectTemplateEnabled = false;
  vm.editorOptions = rbGlobalSetting.getEditorOptions('yaml');

  vm.$onInit = _init;

  vm.create = create;
  vm.cancel = cancel;
  vm.onTemplateNameChange = onTemplateNameChange;
  vm.onSpaceChanged = onSpaceChanged;

  //////////
  function _init() {
    vm.initialized = false;
    vm.quotaEnabled = WEBLABS['quota_enabled'];

    if ('export' in $location.search() && templateService.getExport()) {
      // export template from application
      vm.yaml = templateService.getExport();
    } else if (
      'from' in $location.search() &&
      $location.search()['from'] !== ''
    ) {
      // create template from existing template name
      _loadExistTemplate($location.search()['from']);
    } else {
      // create template from scrath
      vm.fromScrath = true;
    }
    templateService
      .templateList()
      .then(data => {
        vm.templateList = data['template_list'] || [];
      })
      .catch(() => {})
      .then(() => {
        vm.initialized = true;
      });
    if (vm.quotaEnabled) {
      rbQuotaSpaceDataService.getConsumableSpaces().then(spaces => {
        vm.spaces = spaces;
      });
    }
  }

  function create() {
    vm.form.$submitted = true;
    if (vm.form.$invalid) {
      return;
    }
    if (!vm.yaml) {
      rbToast.warning(
        `${translateService.get('app_template')}${translateService.get(
          'cannot_be_empty',
        )}`,
      );
      return;
    }
    // check if yaml syntax is valid
    try {
      jsyaml.safeLoad(vm.yaml, 'utf8');
    } catch (e) {
      rbToast.warning(translateService.get('yaml_file_invalid'));
      return;
    }
    const data = {
      name: vm.templateName,
      description: vm.templateDesc || '',
      template: vm.yaml,
    };
    if (vm.quotaEnabled && vm.space_name) {
      data.space_name = vm.space_name;
    }
    vm.submitting = true;
    templateService
      .templateCreate(data)
      .then(data => {
        const app_template = data.app_template;
        $state.go('app_service.template.detail', {
          templateName: app_template.uuid,
        });
      })
      .catch(() => {})
      .then(() => {
        vm.form.$setPristine();
        vm.submitting = false;
      });
  }

  function cancel() {
    rbRouterStateHelper.back();
  }

  function _loadExistTemplate(name) {
    if (vm.initialized) {
      vm.templateLoading = true;
    }
    templateService
      .templateDetail(name)
      .then(data => {
        const template = data['template'] || {};
        if (template.yaml) {
          vm.yaml = template.yaml;
        }
      })
      .catch(() => {})
      .then(() => {
        vm.templateLoading = false;
      });
  }

  function onTemplateNameChange(option) {
    vm.selectedTempalteName = option.uuid;
    _loadExistTemplate(vm.selectedTempalteName);
  }

  function onSpaceChanged(space) {
    vm.space_name = space.name;
  }
}
