/**
 * Created by liudong on 16/10/11.
 */
import { resource_types as RESOURCE_TYPES } from '../../rbac_role/rb-rbac-helper.constant';

import template from './rb-app-template-detail.html';

angular.module('app.components.pages').component('rbAppTemplateDetail', {
  controller: rbAppTemplateDetailController,
  controllerAs: 'vm',
  template,
});

function rbAppTemplateDetailController(
  $state,
  $stateParams,
  templateService,
  rbGlobalSetting,
  translateService,
  rbConfirmBox,
  rbRoleUtilities,
  rbToast,
) {
  const vm = this;
  vm.buttonDisplayExpr = buttonDisplayExpr;
  vm.basicInfoCollapse = basicInfoCollapse;
  vm.$onInit = _init;
  //////////
  async function _init() {
    vm.templateName = $stateParams.templateName;
    if (!vm.templateName) {
      $state.go('app_service.template.list');
    }
    vm.isBasicInfoCollapse = false;
    vm.editorOptions = rbGlobalSetting.getEditorOptions('yaml', true);
    vm.initialized = false;
    vm.appCreateEnabled = await rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.APPLICATION,
    );
    vm.appTemplateCreateEnabled = await rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.APPLICATION_TEMPLATE,
    );
    templateService
      .templateDetail(vm.templateName)
      .then(data => {
        vm.template = data['template'] || {};
        vm.template_display_name = vm.template.name;
      })
      .catch(e => {
        if (e.status !== 403) {
          rbToast.error(translateService.get('application_template_not_exist'));
        }
        $state.go('app_service.template.list');
      })
      .then(() => {
        vm.initialized = true;
      });
  }

  vm.appCreate = () => {
    $state.go('app_service.app.create_app', { template_name: vm.templateName });
  };

  vm.saveAs = () => {
    $state.go('app_service.template.create', { from: vm.templateName });
  };

  vm.templateDelete = () => {
    rbConfirmBox
      .show({
        title: translateService.get('delete'),
        textContent: `${translateService.get('delete')}${translateService.get(
          'template',
        )} ${vm.template_display_name}?`,
      })
      .then(() => {
        vm.submitting = true;
        templateService
          .templateDelete(vm.templateName)
          .then(() => {
            $state.go('app_service.template.list');
          })
          .catch(() => {})
          .then(() => {
            vm.submitting = false;
          });
      });
  };

  vm.templateUpdate = () => {
    $state.go('app_service.template.update', { templateName: vm.templateName });
  };

  function basicInfoCollapse() {
    vm.isBasicInfoCollapse = !vm.isBasicInfoCollapse;
  }

  function buttonDisplayExpr(action) {
    return rbRoleUtilities.resourceHasPermission(
      vm.template,
      RESOURCE_TYPES.APPLICATION_TEMPLATE,
      action,
    );
  }
}
