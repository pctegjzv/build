import { sortBy } from 'lodash';

import { resource_types as RESOURCE_TYPES } from '../../rbac_role/rb-rbac-helper.constant';

import template from './rb-app-template-list.html';

angular.module('app.components.pages').component('rbAppTemplateList', {
  controller: templateListController,
  controllerAs: 'vm',
  template,
});

function templateListController(
  templateService,
  WEBLABS,
  rbConfirmBox,
  rbToast,
  rbRoleUtilities,
  rbSafeApply,
  translateService,
) {
  const vm = this;
  vm.templateDelete = templateDelete;
  vm.buttonDisplayExpr = buttonDisplayExpr;
  vm.$onInit = _init;

  ////////
  async function _init() {
    vm.appTemplateCreateEnabled = false;
    vm.deletingMap = {};
    vm.quotaEnabled = WEBLABS.QUOTA_ENABLED;
    vm.initialized = false;
    templateService
      .templateList()
      .then(data => {
        const templateList = data['template_list'] || [];
        vm.templateList = getReorderList(templateList);
      })
      .catch(() => {
        rbToast.warning(translateService.get('template_list_load_fail'));
      })
      .then(() => {
        vm.initialized = true;
      });
    vm.appTemplateCreateEnabled = await rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.APPLICATION_TEMPLATE,
    );
    rbSafeApply();
  }

  function templateDelete(template) {
    const template_name = template.uuid;
    const template_display_name = template.name;
    rbConfirmBox
      .show({
        title: translateService.get('delete'),
        textContent:
          translateService.get('delete') +
          translateService.get('template') +
          '“' +
          template_display_name +
          '”' +
          '?',
      })
      .then(() => {
        vm.deletingMap[template_name] = true;
        templateService.templateDelete(template_name).then(() => {
          templateService
            .templateList()
            .then(data => {
              vm.templateList = data['template_list'] || [];
            })
            .catch(() => {
              rbToast.warning(translateService.get('template_list_load_fail'));
            })
            .then(() => {
              vm.deletingMap[template_name] = true;
            });
        });
      });
  }

  function buttonDisplayExpr(tpl, action) {
    return rbRoleUtilities.resourceHasPermission(
      tpl,
      RESOURCE_TYPES.APPLICATION_TEMPLATE,
      action,
    );
  }

  function getReorderList(list) {
    if (vm.quotaEnabled) {
      const itemsWithoutSpace = sortBy(
        list.filter(item => {
          return !item.space_name;
        }),
        'name',
      );
      const itemsWithSpace = sortBy(
        list.filter(item => {
          return item.space_name;
        }),
        ['space_name', 'name'],
      );
      return itemsWithoutSpace.concat(itemsWithSpace);
    } else {
      return list;
    }
  }
}
