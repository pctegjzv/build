/**
 * Created by liudong on 16/10/11.
 */
const jsyaml = require('js-yaml');
const templateStr = require('app/components/pages/app_service/app_template_update/rb-app-template-update.html');
angular.module('app.components.pages').component('rbAppTemplateUpdate', {
  controller: rbAppTemplateUpdateController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbAppTemplateUpdateController(
  $state,
  $stateParams,
  rbSafeApply,
  templateService,
  rbGlobalSetting,
  rbToast,
  translateService,
  rbRouterStateHelper,
) {
  const vm = this;

  vm.templateName = $stateParams.templateName;
  vm.template = {};

  vm.initialized = false;
  vm.submitting = false;
  vm.editorOptions = rbGlobalSetting.getEditorOptions('yaml');

  vm.$onInit = _init;

  vm.update = update;
  vm.cancel = cancel;

  //////////
  function _init() {
    vm.initialized = false;

    templateService
      .templateDetail(vm.templateName)
      .then(data => {
        if (data['template']) {
          vm.template = data['template'];
          vm.template_display_name = vm.template.name;
        }
      })
      .catch(() => {
        $state.go('app_service.template.list');
      })
      .then(() => {
        vm.initialized = true;
        rbSafeApply();
      });
  }

  function update() {
    vm.form.$submitted = true;
    if (vm.form.$invalid || !vm.template.yaml) {
      return;
    }
    // check if yaml syntax is valid
    try {
      jsyaml.safeLoad(vm.template.yaml, 'utf8');
    } catch (e) {
      rbToast.warning(translateService.get('yaml_file_invalid'));
      return;
    }

    const data = {
      origin_name: vm.templateName,
      name: vm.template.name,
      description: vm.template.description,
      template: vm.template.yaml,
    };
    vm.submitting = true;
    templateService
      .templateUpdate(data)
      .then(data => {
        $state.go('app_service.template.detail', {
          templateName: data.app_template.uuid,
        });
      })
      .catch(() => {})
      .then(() => {
        vm.form.$setPristine();
        vm.submitting = false;
        rbSafeApply();
      });
  }

  function cancel() {
    rbRouterStateHelper.back();
  }
}
