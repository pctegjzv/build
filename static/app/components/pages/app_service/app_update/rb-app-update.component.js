import * as jsyaml from 'js-yaml';
import CN_HELPER from '../rb-app-update-helper-text.zh_cn';
import EN_HELPER from '../rb-app-update-helper-text.en';
import _ from 'lodash';

const templateStr = require('app/components/pages/app_service/app_update/rb-app-update.html');
angular.module('app.components.pages').component('rbAppUpdate', {
  controller: rbAppUpdateController,
  controllerAs: 'vm',
  bindings: {
    appName: '<',
  },
  template: templateStr,
});

function rbAppUpdateController(
  $state,
  rbRouterStateHelper,
  rbAppDataService,
  rbConfirmBox,
  translateService,
  rbGlobalSetting,
  rbRegionService,
  rbRegionDataService,
  rbToast,
  rbSafeApply,
) {
  const vm = this;
  vm.app = {};
  vm.regionName = '';
  vm.regions = [];
  vm.submitting = false;
  vm.yamlData = '';
  vm.initialized = false;

  vm.$onInit = _onInit;
  vm.update = update;
  vm.cancel = cancel;

  function _onInit() {
    vm.editorOptions = rbGlobalSetting.getEditorOptions('yaml');
    vm.updateTips =
      translateService.getLanguage() === 'zh_cn' ? CN_HELPER : EN_HELPER;
    Promise.all([getAppDetail(), getAppCompose(), getRegions()]).then(() => {
      const region = vm.regions.filter(r => r.id === vm.app.region);
      vm.regionName = region[0] && region[0].name ? region[0].name : '';
      rbRegionService.setRegionByName(vm.regionName);
      vm.initialized = true;
      rbSafeApply();
    });
  }

  function getAppDetail() {
    return rbAppDataService
      .getAppDetail(vm.appName)
      .then(data => {
        vm.app = data.app;
      })
      .catch(() => {
        $state.go('app_service.app.list');
        rbToast.error(translateService.get('application_not_exist'));
      });
  }

  function getAppCompose() {
    return rbAppDataService
      .getAppYaml(vm.appName)
      .then(data => {
        vm.yamlData = data.yaml;
      })
      .catch(() => {});
  }

  function getRegions() {
    return rbRegionDataService
      .getRegions()
      .then(data => {
        vm.regions = data.regions;
      })
      .catch(() => {});
  }

  function update() {
    rbConfirmBox
      .show({
        title: translateService.get('update'),
        textContent: translateService.get('app_service_update_app_confirm'),
      })
      .then(() => {
        _update();
      });
  }

  function cancel() {
    rbRouterStateHelper.back();
  }

  function _update() {
    try {
      jsyaml.safeLoad(vm.yamlData, 'utf8');
    } catch (e) {
      rbToast.warning(translateService.get('yaml_file_invalid'));
      return;
    }
    const data = {
      app_name: vm.appName,
      services: vm.yamlData,
    };
    vm.submitting = true;
    rbAppDataService
      .updateApp(data)
      .then(() => {
        vm.cancel();
      })
      .catch(e => {
        const code = _.get(e, 'data.errors[0].code');
        if (code) {
          const errMsg =
            translateService.get('yaml_error') +
              '<br>' +
              _.get(e, 'data.errors[0].message') || '';
          rbToast.error(errMsg);
        }
      })
      .then(() => {
        vm.submitting = false;
      });
  }
}
