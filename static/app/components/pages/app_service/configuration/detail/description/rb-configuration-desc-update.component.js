const templateStr = require('app/components/pages/app_service/configuration/detail/description/rb-configuration-desc-update.html');
angular.module('app.components.pages').component('rbConfigurationDescUpdate', {
  controller: rbConfigurationDescUpdateController,
  bindings: {
    configuration: '<',
    updateConfig: '&',
  },
  controllerAs: 'vm',
  template: templateStr,
});

function rbConfigurationDescUpdateController(rbModal, rbConfigurationService) {
  const vm = this;

  vm.cancel = () => {
    rbModal.cancel();
  };

  vm.confirm = async () => {
    vm.form.$setSubmitted();
    if (vm.form.$invalid) {
      return;
    }
    vm.submitting = true;
    await rbConfigurationService.updateConfiguration(vm.configuration);
    vm.submitting = false;
    rbModal.hide();
  };
}
