const templateStr = require('app/components/pages/app_service/configuration/detail/edit/rb-configuration-item-edit.html');
angular.module('app.components.pages').component('rbConfigurationItemEdit', {
  controller: rbConfigurationItemEditController,
  bindings: {
    configuration: '<',
    index: '<',
  },
  controllerAs: 'vm',
  template: templateStr,
});

function rbConfigurationItemEditController(
  rbModal,
  rbGlobalSetting,
  rbConfigurationService,
  $timeout,
) {
  const vm = this;
  vm.editorInitialized = false;
  vm.keyPattern = {
    test: value => {
      if (!/^[A-Za-z0-9\-_]+$/.test(value)) {
        vm.patternInvalid = true;
        return false;
      }
      vm.patternInvalid = false;
      if (!vm.isExisting && _.find(vm.configuration.content, { key: value })) {
        return false;
      } else {
        return true;
      }
    },
  };
  vm.$onInit = () => {
    const editorOptions = rbGlobalSetting.getEditorOptions('yaml');
    editorOptions.scrollbarStyle = 'null';
    vm.editorOptions = editorOptions;
    vm.curItem = {};
    if (_.isNumber(vm.index)) {
      vm.isExisting = true;
      $timeout(() => {
        vm.editorInitialized = true;
        vm.curItem = vm.configuration.content[vm.index];
      }, 500);
    } else {
      $timeout(() => {
        vm.editorInitialized = true;
      }, 500);
    }
  };
  async function _createConfigurationItem() {
    if (!_.isNumber(vm.index)) {
      vm.configuration.content = [vm.curItem].concat(vm.configuration.content);
    }
    try {
      vm.submiting = true;
      await rbConfigurationService.updateConfiguration(vm.configuration);
      rbModal.hide(vm.configuration);
    } catch (e) {
      throw e;
    }
    vm.submiting = false;
  }

  vm.cancel = () => {
    rbModal.cancel();
  };

  vm.confirm = async () => {
    vm.form.$setSubmitted();
    if (vm.form.$invalid) {
      return;
    }
    // create account
    await _createConfigurationItem();
  };
}
