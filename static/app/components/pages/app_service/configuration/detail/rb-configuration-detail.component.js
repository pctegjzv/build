import { resource_types as RESOURCE_TYPES } from '../../../rbac_role/rb-rbac-helper.constant';

import template from './rb-configuration-detail.html';

angular.module('app.components.pages').component('rbConfigurationDetail', {
  controller: rbConfigurationDetailController,
  bindings: {
    name: '<',
    description: '<',
    isNew: '<',
  },
  bindToController: true,
  controllerAs: 'vm',
  template,
});

function rbConfigurationDetailController(
  $state,
  rbConfigurationService,
  rbGlobalSetting,
  translateService,
  rbConfirmBox,
  rbSafeApply,
  $log,
  rbToast,
  rbRoleUtilities,
  rbModal,
) {
  const vm = this;
  vm.$onInit = _init;
  vm.updateDescription = updateDescription;
  vm.updateConfig = updateConfig;
  vm.configDelete = configDelete;
  vm.itemDelete = itemDelete;
  vm.itemEdit = itemEdit;
  vm.buttonDisplayExpr = buttonDisplayExpr;
  vm.toggleItemValue = $index => {
    vm.toggleItem[$index] = !vm.toggleItem[$index];
  };
  //////////
  async function _init() {
    vm.editorOptions = rbGlobalSetting.getEditorOptions('env', true);
    vm.toggleItem = [];
    if (!vm.name) {
      $state.go('app_service.configuration.list');
    }
    if (vm.isNew) {
      vm.configuration = {
        name: vm.name,
        description: vm.description,
      };
      vm.initialized = true;
    } else {
      await _reloadDetail();
    }
  }

  async function configDelete() {
    if (vm.deleting) {
      return Promise.resolve();
    }
    const title = translateService.get('delete');
    const textContent = `${translateService.get(
      'delete',
    )}${translateService.get('configuration')}"${vm.configDisplayName}"?`;
    try {
      await rbConfirmBox.show({ title, textContent });
      vm.deleting = true;
      await rbConfigurationService.deleteConfiguration(vm.name);
      vm.deleting = false;
      $state.go('app_service.configuration.list');
    } catch (e) {
      e && $log.error('Failed to load configuration_Delete_url', e);
    }
  }

  async function _reloadDetail() {
    try {
      vm.initialized = false;
      vm.configuration = await rbConfigurationService.configurationDetail(
        vm.name,
      );
      vm.configDisplayName = vm.configuration.name;
    } catch (e) {
      vm.initialized = true;
      if (e.status !== 403) {
        rbToast.error(translateService.get('configuration_not_exist'));
      }
      $state.go('app_service.configuration.list');
      e && $log.error('Failed to load configuration_detail_url', e);
    }
    vm.configuration.content = vm.configuration.content || [];
    vm.initialized = true;
    rbSafeApply();
  }

  async function itemDelete($event, $index) {
    $event.stopPropagation();
    const configuration = _.cloneDeep(vm.configuration);
    try {
      await rbConfirmBox.show({
        title: translateService.get('delete'),
        textContent: `${translateService.get('delete_configuration_item')} ${
          configuration.content[$index].key
        }?`,
      });
      configuration.content.splice($index, 1);
      await updateConfig(configuration);
    } catch (e) {
      // No ops when cancel
    }
  }

  async function updateConfig(config) {
    vm.submitting = true;
    await rbConfigurationService.updateConfiguration(config);
    await _reloadDetail();
    vm.submitting = false;
    rbSafeApply();
  }

  async function itemEdit($event, $index) {
    if (_.isNumber($index)) {
      $event.stopPropagation();
    }
    try {
      const configuration = await rbModal.show({
        width: 750,
        title: _.isNumber($index)
          ? translateService.get('update_configuration_item')
          : translateService.get('add_configuration_item'),
        locals: {
          configuration: _.cloneDeep(vm.configuration),
          index: $index,
        },
        template:
          '<rb-configuration-item-edit configuration="configuration" index="index"></rb-configuration-item-edit>',
      });
      vm.configuration.content = _.cloneDeep(configuration.content);
      rbSafeApply();
    } catch (e) {
      // No ops when cancel
    }
  }

  async function updateDescription() {
    try {
      await rbModal.show({
        title: translateService.get('configuration_desc_update'),
        locals: {
          configuration: _.cloneDeep(vm.configuration),
        },
        template:
          '<rb-configuration-desc-update configuration="configuration"></rb-configuration-desc-update>',
      });
      await _reloadDetail();
    } catch (e) {
      // No ops when cancel
    }
  }

  function buttonDisplayExpr(action) {
    return rbRoleUtilities.resourceHasPermission(
      vm.configuration,
      RESOURCE_TYPES.CONFIGURATION_FILE,
      action,
    );
  }
}
