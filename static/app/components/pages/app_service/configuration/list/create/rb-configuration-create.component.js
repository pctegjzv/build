const templateStr = require('app/components/pages/app_service/configuration/list/create/rb-configuration-create.html');
angular.module('app.components.pages').component('rbConfigurationCreate', {
  controller: rbConfigurationCreateController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbConfigurationCreateController(
  rbModal,
  $state,
  WEBLABS,
  rbQuotaSpaceDataService,
  rbConfigurationService,
) {
  const vm = this;
  vm.$onInit = () => {
    vm.quotaEnabled = WEBLABS['quota_enabled'];
    if (vm.quotaEnabled) {
      rbQuotaSpaceDataService.getConsumableSpaces().then(spaces => {
        vm.spaces = spaces;
      });
    }
  };

  async function _createItem() {
    const data = {
      name: vm.name,
      description: vm.description,
      content: [],
    };
    if (vm.quotaEnabled && vm.space_name) {
      data.space_name = vm.space_name;
    }
    vm.submitting = true;
    let _data;
    try {
      _data = await rbConfigurationService.createConfiguration(data);
    } catch (e) {
      vm.submitting = false;
      throw e;
    }
    const configName = rbConfigurationService.getConfigName(_data);
    vm.submitting = false;
    rbModal.hide();
    $state.go('app_service.configuration.detail', { name: configName });
  }

  vm.cancel = () => {
    rbModal.cancel();
  };

  vm.onSpaceChanged = space => {
    vm.space_name = space.name;
  };

  vm.confirm = async () => {
    vm.form.$setSubmitted();
    if (vm.form.$invalid) {
      return;
    }
    // create account
    await _createItem();
  };
}
