import { resource_types as RESOURCE_TYPES } from '../../../rbac_role/rb-rbac-helper.constant';

import template from './rb-configuration-list.html';

angular.module('app.components.pages').component('rbConfigurationList', {
  controller: configurationListController,
  controllerAs: 'vm',
  template,
});

function configurationListController(
  $state,
  $log,
  WEBLABS,
  rbConfigurationService,
  rbConfirmBox,
  rbAccountService,
  rbModal,
  rbSafeApply,
  rbRoleUtilities,
  translateService,
) {
  const vm = this;

  vm.configurationCreate = configurationCreate;
  vm.configurationDelete = configurationDelete;
  vm.getConfigurationList = getConfigurationList;
  vm.onSearchChanged = onSearchChanged;
  vm.buttonDisplayExpr = buttonDisplayExpr;
  vm.getConfigName = getConfigName;
  vm.$onInit = _init;

  //////////
  async function _init() {
    vm.deleting = [];
    vm.initialized = false;
    vm.quotaEnabled = WEBLABS.QUOTA_ENABLED;
    await _updateList();
    vm.configFileCreateEnabled = await rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.CONFIGURATION_FILE,
    );
    vm.searching = '';
    rbSafeApply();
  }

  let _lastSearchCounter = 0;

  async function onSearchChanged(search) {
    const searchCounter = ++_lastSearchCounter;
    vm.search = search;
    vm.searching = true;
    vm.page = 1;
    await _updateList();
    if (searchCounter === _lastSearchCounter) {
      vm.searching = false;
    }
    rbSafeApply();
  }

  async function _updateList() {
    let data = {};
    try {
      vm.loading = true;
      vm.loadError = false;
      data = await rbConfigurationService.configurationList({
        page: vm.page,
        search: vm.search,
      });
    } catch (e) {
      vm.loadError = true;
    }
    vm.initialized = true;
    vm.loading = false;
    vm.configurationList = data.results;
    vm.pagination = data;
    rbSafeApply();
  }

  async function configurationCreate() {
    try {
      const data = await rbModal.show({
        title: translateService.get('configuration_create'),
        template: '<rb-configuration-create></rb-configuration-create>',
      });
      if (data) {
        $state.go('app_service.configuration.detail', data);
      }
    } catch (e) {
      // No-ops when cancel
    }
  }

  async function configurationDelete($index, config) {
    const config_name = vm.getConfigName(config);
    const config_display_name = config.name;
    const title = translateService.get('delete');
    const textContent = `${translateService.get(
      'delete',
    )} ${translateService.get('configuration')}"${config_display_name}"?`;
    try {
      await rbConfirmBox.show({ title, textContent });
      vm.deleting[$index] = true;
      await rbConfigurationService.deleteConfiguration(config_name);
      vm.deleting[$index] = false;
      vm.reloading = true;
      _updateList();
      vm.reloading = false;
    } catch (e) {
      e && $log.error('Failed to load configuration_Delete_url', e);
    }
  }

  function getConfigurationList(page, refresh) {
    vm.page = page || vm.page;
    refresh && (vm.initialized = false);
    _updateList();
  }

  function buttonDisplayExpr(env_file, action) {
    return rbRoleUtilities.resourceHasPermission(
      env_file,
      RESOURCE_TYPES.CONFIGURATION_FILE,
      action,
    );
  }

  function getConfigName(config) {
    return rbConfigurationService.getConfigName(config);
  }
}
