angular
  .module('app.components.pages')
  .factory('rbConfigurationService', configurationServiceFactory);

function configurationServiceFactory(rbHttp, rbAccountService) {
  const namespace = rbAccountService.getCurrentNamespace();
  const CONFIGURATION_ENDPOINT = '/ajax/v1/configs/' + namespace;
  return {
    configurationList,
    createConfiguration,
    configurationDetail,
    updateConfiguration,
    deleteConfiguration,
    getConfigName,
  };

  //////////
  function configurationList({
    cache = false,
    page = 1,
    page_size = 20,
    search,
  }) {
    const _cacheList = rbHttp
      .sendRequest({
        url: CONFIGURATION_ENDPOINT,
        params: { search, page, page_size },
        cache,
        addNamespace: false,
      })
      .then(data => {
        configurationList._cacheList = Promise.resolve(data);
        return data;
      });
    if (
      !configurationList._preListCache &&
      cache &&
      configurationList._cacheList
    ) {
      configurationList._preListCache = cache;
      return configurationList._cacheList;
    } else {
      return _cacheList;
    }
  }

  function createConfiguration(data) {
    return rbHttp.sendRequest({
      method: 'POST',
      url: CONFIGURATION_ENDPOINT,
      data,
      addNamespace: false,
    });
  }

  function configurationDetail(name) {
    return rbHttp.sendRequest({
      url: CONFIGURATION_ENDPOINT + '/' + name,
      addNamespace: false,
    });
  }

  function updateConfiguration(configuration) {
    const name = getConfigName(configuration);
    delete configuration.namespace;
    delete configuration.name;
    return rbHttp.sendRequest({
      method: 'PUT',
      url: CONFIGURATION_ENDPOINT + '/' + name,
      data: configuration,
      addNamespace: false,
    });
  }

  function deleteConfiguration(name) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url: CONFIGURATION_ENDPOINT + '/' + name,
      addNamespace: false,
    });
  }
  function getConfigName(config) {
    return config.uuid ? config.uuid : config.id;
  }
}
