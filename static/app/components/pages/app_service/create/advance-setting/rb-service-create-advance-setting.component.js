/**
 * Created by liudong on 16/9/2.
 */
import _ from 'lodash';
import { INT_PATTERN } from 'app/components/common/config/common-pattern';
const {
  SERVICE_TYPE,
  SERVICE_RUNCOMMAND_TYPE,
  SERVICE_NETWORK_MODES,
  SERVICE_DEPLOY_MODEL,
} = require('../../rb-service.constant');
const { CONTAINER_MANAGERS } = require('../../../region/rb-region.constant');
const templateStr = require('app/components/pages/app_service/create/advance-setting/rb-service-create-advance-setting.html');
angular
  .module('app.components.pages')
  .component('rbServiceCreateAdvanceSetting', {
    bindings: {
      model: '<',
      form: '=',
      imageData: '<',
      region: '<',
      regionFeatures: '<',
    },
    controller: rbServiceCreateAdvanceSettingController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbServiceCreateAdvanceSettingController(
  applicationService,
  rbRegionNodeDataService,
  rbAccountService,
  rbStorageDataService,
  translateService,
  WEBLABS,
  rbRegionDataService,
  rbServiceUtilities,
  rbServiceDataService,
  rbLabelsDataService,
) {
  const vm = this;
  let _runcommand;

  const SCALING_MODE_MANUAL = {
    name: translateService.get('manual'),
    value: 'MANUAL',
  };

  const SCALING_MODE_AUTO = {
    name: translateService.get('auto_scale'),
    value: 'AUTO',
  };

  const SCALING_MODE_MAP = {
    [2]: [SCALING_MODE_MANUAL, SCALING_MODE_AUTO],
    [1]: [SCALING_MODE_MANUAL],
  };

  const SHARED_DRIVER_NAME = ['glusterfs', 'loongstore'];

  let oldNetworkValue = '';
  let oldDeployModel = '';

  vm.onServiceTypeChange = onServiceTypeChange;
  vm.onScalingModeChange = onScalingModeChange;
  vm.onScalingMetricNameChange = onScalingMetricNameChange;
  vm.onscalingMetricStatChange = onscalingMetricStatChange;
  vm.onRunCommandTypeChange = onRunCommandTypeChange;
  vm.selectNodeTag = selectNodeTag;
  vm.isStatefulService = isStatefulService;
  vm.getScalingModeOptions = getScalingModeOptions;

  vm.logFileFieldsEnabled = logFileFieldsEnabled;
  vm.configManageEnabled = configManageEnabled;
  vm.addNodeLabels = addNodeLabels;
  vm.removeNodeLabels = removeNodeLabels;
  vm.checkAvailableNodes = checkAvailableNodes;
  vm.checkUpdateStrategy = checkUpdateStrategy;
  vm.isScalingModeEnabled = isScalingModeEnabled;
  vm.updateStrategyEnabled = updateStrategyEnabled;
  vm.isSupportKubernetes = isSupportKubernetes;
  vm.affinityEnabled = affinityEnabled;

  vm.validateUpdateStrategy = validateUpdateStrategy;
  vm.validateUnavailableValue = validateUnavailableValue;
  vm.zeroCheckInDaemonSet = zeroCheckInDaemonSet;

  vm.$postLink = _link;
  vm.$onInit = _init;
  vm.$doCheck = _doCheck;

  ////////

  function _init() {
    vm.targetNumInstancesRegExp = INT_PATTERN;
    vm.availableVolumes = [];
    vm.availableNodesChecked = false;
    vm.isK8sUpdateNodeTagsEnabled = false;
    vm.scalingMetricNames = [
      {
        key: translateService.get('CPU_UTILIZATION'),
        value: 'CPU_UTILIZATION',
      },
      {
        key: translateService.get('MEMORY_UTILIZATION'),
        value: 'MEMORY_UTILIZATION',
      },
    ];
    vm.scalingMetricStats = [
      {
        key: translateService.get('MEAN'),
        value: 'MEAN',
      },
      {
        key: translateService.get('MAXIMUM'),
        value: 'MAXIMUM',
      },
      {
        key: translateService.get('MINIMUM'),
        value: 'MINIMUM',
      },
    ];
    vm.runCommandTypes = [
      {
        name: translateService.get('use_default'),
        value: SERVICE_RUNCOMMAND_TYPE.DEFAULT,
      },
      {
        name: translateService.get('use_custom'),
        value: SERVICE_RUNCOMMAND_TYPE.CUSTOM,
      },
    ];
    vm.serviceList = []; // avaliable service in the region, from applicationService
    vm.availableNodeTags = [];
    vm.originAvailableNodeLables = [];

    vm.model.node_selector = [];
    vm.model.useDefaultCmd = false;
    vm.model.target_num_instances = 1;
    vm.model.autoscaling_config = {
      metric_name: 'CPU_UTILIZATION',
      metric_stat: 'MEAN',
      upper_threshold: 100,
      lower_threshold: 0,
      decrease_delta: 1,
      increase_delta: 1,
      minimum_num_instances: 1,
      maximum_num_instances: 20,
      wait_period: 30,
    };
    vm.model.instance_envvars = {};
    vm.model.includeLogFiles = [];
    vm.model.excludeLogFiles = [];
    vm.model.linkServiceList = [];
    vm.model.envVars = []; // name, values
    vm.model.envFiles = [];
    vm.model.customDomains = [];
    vm.model.configFiles = [];
    vm.model.update_strategy = {
      max_surge: '1',
      max_surge_disabled: false,
      max_unavailable: '1',
      max_unavailable_disabled: false,
    };
    _initRegionAdvancedFeatures();
    _initServiceAffinity();
  }

  function isSupportKubernetes() {
    return (
      _.get(vm.region, 'container_manager') === CONTAINER_MANAGERS.KUBERNETES
    );
  }

  function _link() {
    vm.form = vm.advancedForm;
  }

  function _initRegionAdvancedFeatures() {
    const containerManager = _.get(vm.region, 'container_manager');
    const regionFeatures = vm.regionFeatures;
    if (containerManager === CONTAINER_MANAGERS.KUBERNETES) {
      vm.isK8sUpdateNodeTagsEnabled = rbServiceUtilities.isK8sUpdateNodeTagsEnabled(
        vm.region,
      );
      rbRegionDataService
        .getRegionLabels(vm.region.name)
        .then(data => {
          vm.availableNodeLables =
            data.labels.length &&
            data.labels.map(r => {
              return _.extend({ display: `${r.key}: ${r.value}` }, r);
            });
          vm.originAvailableNodeLables = vm.availableNodeLables;
        })
        .catch(() => {});
    }
    const regionVolumeEnabled = regionFeatures.includes('volume');
    _updateServiceTypeList(regionVolumeEnabled);
    // get region volumes
    if (regionVolumeEnabled) {
      const driverTypeStr = translateService.get('storage_volume_driver_type');
      rbStorageDataService
        .getVolumes({
          regionId: vm.region.id,
          listAll: true,
          action: 'mount',
        })
        .then(({ volumes }) => {
          vm.availableVolumes = volumes
            .filter(volume => {
              return (
                volume.driver_name === 'glusterfs' ||
                volume.state === 'available'
              );
            })
            .map(volume => {
              const displayText = `${
                volume.name
              }（${driverTypeStr}：${translateService.get(
                'storage_volume_driver_type_' + volume.driver_name,
              )}）`;
              return Object.assign({}, volume, { display_text: displayText });
            });

          // Temporary host path mode
          vm.availableVolumes.unshift({
            id: 'host_path',
            display_text:
              '< ' + translateService.get('volume_mount_host_path') + ' >',
          });
        });
    }
    // get region service list
    // region with container_manager = 'SWARM' does not support link
    if (containerManager !== CONTAINER_MANAGERS.SWARM) {
      const params = {
        namespace: rbAccountService.getCurrentNamespace(),
        region_name: vm.region.name,
      };
      applicationService.applicationList(params).then(data => {
        if (data.list) {
          vm.serviceList = [];
          _.forEach(data.list, service => {
            vm.serviceList.push({
              name: service.service_name,
              uuid: service.uuid,
              space_name: service.space_name,
            });
          });
        }
      });
    }

    _getAvailableNodeTags(vm.region);
  }

  async function _initServiceAffinity() {
    if (!vm.region || vm.region.container_manager !== 'KUBERNETES') {
      return;
    }

    vm.kube_affinity = null;
    vm.loadingServiceAffinity = true;
    try {
      const [services, serviceLabels, nodeLabels] = await Promise.all([
        rbServiceDataService
          .getServices({ region_name: vm.region.name })
          .then(({ results }) => {
            return results;
          })
          .catch(() => null),
        rbLabelsDataService.getAllLebels().catch(() => null),
        rbRegionDataService.getRegionLabels(vm.region.name).catch(() => null),
      ]);

      if (services) {
        vm.services = services;
      }

      if (serviceLabels) {
        vm.serviceLabels = serviceLabels.results || [];
      }

      if (nodeLabels) {
        vm.nodeLabels = nodeLabels.labels || [];
      }
    } finally {
      vm.loadingServiceAffinity = false;
    }
  }

  function onServiceTypeChange(option) {
    if (option.value === SERVICE_TYPE.STATELESS) {
      vm.model.update_strategy.max_surge_disabled = false;
      vm.model.update_strategy.max_unavailable_disabled = false;
      if (vm.model.networkMode === SERVICE_NETWORK_MODES.MACVLAN) {
        vm.model.update_strategy.max_surge_disabled = true;
        vm.model.update_strategy.max_surge = '0%';
      }
    }
    vm.model.service_type = option.value;
  }

  function onScalingModeChange(option) {
    vm.model.scaling_mode = option.value;
  }

  function onScalingMetricNameChange(option) {
    vm.model.autoscaling_config.metric_name = option.value;
  }

  function onscalingMetricStatChange(option) {
    vm.model.autoscaling_config.metric_stat = option.value;
  }

  /**
   * Calculate the scaling mode options. This function will be used as a getter.
   * https://trello.com/c/yag7jujX/526-glusterfs
   */
  function getScalingModeOptions() {
    let showAutoOption = false;
    let isSelectedEbs = false;
    const macvlanAddressListExist =
      vm.model.subnet_assigned_enabled === true &&
      _.get(vm.model, 'address_list', []).length > 0;
    if (
      macvlanAddressListExist ||
      vm.model.pod_controller === SERVICE_DEPLOY_MODEL.STATEFULSET
    ) {
      showAutoOption = false;
    } else if (vm.model.service_type === SERVICE_TYPE.STATELESS) {
      showAutoOption = true;
    } else {
      const selectedVolumes = _.get(vm, 'model.volumes', []).map(
        ({ volume_id }) => {
          return vm.availableVolumes.find(volume => volume.id === volume_id);
        },
      );
      isSelectedEbs = selectedVolumes.some(
        volume => volume && volume.driver_name === 'ebs',
      );
      showAutoOption = !isSelectedEbs;
    }

    // hmm, side-effect for getter function, bad design ...
    // KUBERNETES does not support auto instances but can have multiple instances
    if (
      showAutoOption ||
      (_.get(vm.region, 'container_manager') ===
        CONTAINER_MANAGERS.KUBERNETES &&
        !macvlanAddressListExist &&
        !isSelectedEbs)
    ) {
      vm.disableInstanceNumber = false;
    } else {
      vm.model.target_num_instances = macvlanAddressListExist
        ? _.get(vm.model, 'address_list', []).length
        : 1;
      vm.disableInstanceNumber = true;
    }

    return SCALING_MODE_MAP[showAutoOption ? 2 : 1];
  }

  function onRunCommandTypeChange(option) {
    vm.model.useDefaultCmd = option.value === SERVICE_RUNCOMMAND_TYPE.DEFAULT;
    if (vm.model.useDefaultCmd) {
      vm.model.run_command = vm.imageData.run_command;
      vm.model.entrypoint = vm.imageData.entrypoint;
    } else {
      vm.model.run_command = '';
      if (vm.region.container_manager !== CONTAINER_MANAGERS.SWARM) {
        vm.model.entrypoint = '';
      }
    }
  }

  function selectNodeTag(option) {
    vm.model.node_tag = option.node_tag;
  }

  function isStatefulService() {
    return vm.model.service_type === SERVICE_TYPE.STATEFUL;
  }

  function _updateServiceTypeList(regionVolumeEnabled) {
    const STATELESS = {
      name: translateService.get('stateless_app'),
      value: SERVICE_TYPE.STATELESS,
    };
    const STATEFUL = {
      name: translateService.get('stateful_app'),
      value: SERVICE_TYPE.STATEFUL,
    };
    if (regionVolumeEnabled) {
      vm.serviceTypes = [STATELESS, STATEFUL];
    } else {
      vm.serviceTypes = [STATELESS];
    }
  }

  function _getAvailableNodeTags(region) {
    const currentRegionName = region.name;

    return rbRegionNodeDataService.nodeTags(currentRegionName).then(nodes => {
      // available hosts need to be in running status and belong to compute nodes(jira:dev-657)
      nodes = nodes.filter(n => n.state === 'RUNNING' && n.type === 'SLAVE');
      vm.availableNodeTags = [
        {
          node_tag: '',
          node_tag_key: translateService.get('node_tag_none'),
        },
      ].concat(nodes);
    });
  }

  function _doCheck() {
    if (vm.model.networkMode && oldNetworkValue !== vm.model.networkMode) {
      oldNetworkValue = vm.model.networkMode;
      if (
        vm.model.networkMode &&
        vm.model.networkMode === SERVICE_NETWORK_MODES.MACVLAN
      ) {
        vm.model.update_strategy.max_surge_disabled = true;
        vm.model.update_strategy.max_surge = '0%';
      } else if (
        vm.model.networkMode &&
        vm.model.networkMode !== SERVICE_NETWORK_MODES.MACVLAN
      ) {
        vm.model.update_strategy.max_surge_disabled = false;
      }
    }
    if (vm.model.pod_controller && oldDeployModel !== vm.model.pod_controller) {
      oldDeployModel = vm.model.pod_controller;
      if (vm.model.pod_controller === SERVICE_DEPLOY_MODEL.STATEFULSET) {
        _updateServiceTypeList(false);
      } else {
        _updateServiceTypeList(vm.regionFeatures.includes('volume'));
      }
    }
    if (vm.imageData.run_command && _runcommand !== vm.imageData.run_command) {
      _runcommand = vm.imageData.run_command;
      vm.model.run_command = vm.imageData.run_command;
      vm.model.entrypoint = vm.imageData.entrypoint;
    }
  }

  function logFileFieldsEnabled() {
    return vm.regionFeatures.includes('file-log');
  }

  function configManageEnabled() {
    return vm.regionFeatures.includes('mount-points');
  }

  function addNodeLabels(option) {
    vm.model.node_selector.push(option);
    vm.availableNodeLables.map(r => {
      if (r.key === option.key && r.value !== option.value) {
        r.op_disabled = true;
      }
    });
    vm.availableNodesChecked = false;
  }

  function removeNodeLabels(option) {
    vm.model.node_selector = vm.model.node_selector.filter(r => {
      return r.key !== option.key;
    });
    vm.availableNodeLables.map(r => {
      if (r.key === option.key) {
        r.op_disabled = false;
      }
    });
    vm.availableNodesChecked = false;
  }

  function checkAvailableNodes() {
    let kvStr = '';
    vm.model.node_selector.map(r => {
      kvStr += `${r.key}:${r.value},`;
    });
    kvStr = kvStr.substring(0, kvStr.length - 1);
    rbRegionDataService
      .checkAvailableNodes(kvStr, vm.region.name)
      .then(data => {
        vm.availableNodesChecked = true;
        vm.availableNodes = data.nodes.length;
      })
      .catch(() => {
        vm.availableNodesChecked = false;
      });
  }

  function validateUpdateStrategy(value, type) {
    return type
      ? parseInt(vm.model.update_strategy.max_surge) || parseInt(value)
      : parseInt(value) || parseInt(vm.model.update_strategy.max_unavailable);
  }

  function validateUnavailableValue(value) {
    return !(value.endsWith('%') && parseInt(value) > 100);
  }

  function checkUpdateStrategy(volumes) {
    let sharedVolumeCount = volumes.length;
    if (volumes.length) {
      volumes.forEach(v => {
        if (!SHARED_DRIVER_NAME.includes(v.driver_name)) {
          sharedVolumeCount--;
          vm.model.update_strategy = {
            max_surge: '0%',
            max_surge_disabled: true,
            max_unavailable: '100%',
            max_unavailable_disabled: true,
          };
        } else if (sharedVolumeCount === volumes.length) {
          vm.model.update_strategy.max_surge_disabled = false;
          vm.model.update_strategy.max_unavailable_disabled = false;
        }
      });
    } else {
      vm.model.update_strategy.max_surge_disabled = false;
      vm.model.update_strategy.max_unavailable_disabled = false;
    }
  }

  function isScalingModeEnabled() {
    return vm.model.pod_controller !== SERVICE_DEPLOY_MODEL.DAEMONSET;
  }

  function zeroCheckInDaemonSet(value) {
    return !(
      vm.model.pod_controller === SERVICE_DEPLOY_MODEL.DAEMONSET &&
      _.get(vm.region, 'features.kubernetes.version', '') >= '1.6' &&
      value === '0'
    );
  }

  function updateStrategyEnabled() {
    return (
      _.get(vm.region, 'container_manager') === 'KUBERNETES' &&
      ((vm.model.pod_controller === SERVICE_DEPLOY_MODEL.DAEMONSET &&
        _.get(vm.region, 'features.kubernetes.version', '') >= '1.6') ||
        vm.model.pod_controller === SERVICE_DEPLOY_MODEL.DEPLOYMENT)
    );
  }

  function affinityEnabled() {
    return (
      vm.region &&
      vm.region.container_manager === 'KUBERNETES' &&
      vm.model.pod_controller !== 'DaemonSet'
    );
  }
}
