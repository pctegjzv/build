/**
 * Created by liudong on 16/9/2.
 */
import * as _ from 'lodash';
import { RESOURCE_NAME_BASE } from 'app/components/common/config/common-pattern';

const templateStr = require('app/components/pages/app_service/create/basic-info/rb-service-create-basic-info.html');
angular.module('app.components.pages').component('rbServiceCreateBasicInfo', {
  bindings: {
    regionName: '<',
    registryName: '<',
    region: '<',
    regionFeatures: '<',
    model: '<', // model: serviceData in parent component
    form: '=',
  },
  require: {
    serviceCreateCtrl: '^rbServiceCreate',
  },
  controller: rbServiceCreateBasicInfoController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbServiceCreateBasicInfoController(
  $location,
  $scope,
  $log,
  rbRegionUtilities,
  WEBLABS,
  rbRepositoryDataService,
  applicationService,
  rbQuotaSpaceDataService,
  rbLabelsDataService,
  rbToast,
  translateService,
) {
  const vm = this;
  let repoName, projectName;

  vm.$onInit = _init;
  vm.$postLink = _link;

  //vm.onRegionChange = onRegionChange;
  vm.onImageTagChange = onImageTagChange;
  vm.onInstanceSizeChange = onInstanceSizeChange;
  vm.onSpaceChanged = onSpaceChanged;
  vm.validateServiceNameAsync = validateServiceNameAsync;
  vm.onServiceDeployModelChange = onServiceDeployModelChange;

  vm.deployModelEnabled = deployModelEnabled;
  vm.affinityEnabled = affinityEnabled;
  ////////
  async function _init() {
    vm.quotaEnabled = WEBLABS['quota_enabled'];
    vm.resourceNameBaseRegExp = RESOURCE_NAME_BASE;
    vm.model.autoStart = true;
    vm.serviceDeployModels = [
      { key: 'Deployment', value: 'Deployment' },
      { key: 'DaemonSet', value: 'DaemonSet' },
      { key: 'StatefulSet', value: 'StatefulSet' },
    ];
    _initImageTags();
    _initQuotaSpaces();
    _initServiceLabels();

    // watch image_tag changes and notifies parent component
    $scope.$watch(
      () => {
        return vm.model.image_tag;
      },
      newValue => {
        if (newValue !== undefined) {
          vm.serviceCreateCtrl.onImageTagChange({
            registry_name: vm.registryName,
            project_name: projectName,
            repo_name: repoName,
            tag_name: newValue,
          });
        }
      },
    );
  }

  function onImageTagChange(option) {
    vm.model.image_tag = option.tag_name;
  }

  // model.instance_size
  // model.custom_instance_size {cpu, mem}
  function onInstanceSizeChange(instance_size, config) {
    vm.model.instance_size = instance_size;
    vm.model.custom_instance_size = {
      cpu: config.cpu,
      mem: config.mem,
    };
  }

  function onSpaceChanged(space) {
    vm.model.space_name = space.name;
    vm.form.service_name.$validate();
  }

  // validate service duplicate
  function validateServiceNameAsync(value) {
    const space_name = vm.model.space_name || '';
    return applicationService
      .checkServiceName(value, space_name)
      .catch(({ data }) => {
        vm.serviceNameErrorCode = _.get(data, 'errors[0].code', '');
        if (
          !vm.serviceNameErrorCode ||
          vm.serviceNameErrorCode === 'malformed_jakiro_response'
        ) {
          // 兼容之前的逻辑，如果是重名，api不会返回标准的错误结构，会在rubick转成malformed_jakiro_response，此时前端转成重名错误
          vm.serviceNameErrorCode = 'name_existed';
        }
        return Promise.reject(data);
      })
      .then(() => {
        vm.serviceNameErrorCode = '';
      });
  }

  async function _initQuotaSpaces() {
    if (WEBLABS['quota_enabled']) {
      vm.loadingSpaces = true;
      vm.spaces = await rbQuotaSpaceDataService.getConsumableSpaces();
      vm.loadingSpaces = false;
    }
  }

  async function _initImageTags() {
    if (vm.registryName) {
      const isPublicRegistry = $location.search()['is_public_registry'];
      if (isPublicRegistry === 'true') {
        const [, , _repoName] = vm.model.image_name.split('/');
        repoName = _repoName;
      } else {
        const arr = vm.model.image_name.split('/');
        if (arr.length === 2) {
          // endpoint/repo
          const [, _repoName] = arr;
          repoName = _repoName;
        } else if (arr.length === 3) {
          // endpoint/project/repo
          const [, _projectName, _repoName] = arr;
          projectName = _projectName;
          repoName = _repoName;
        }
      }
    }
    vm.imageTagsLoading = true;
    try {
      vm.imageTags = await _getImageTags();
    } catch (err) {
      $log.error('failed to load image tags');
      rbToast.warning(translateService.get('get_image_tags_failed'));
    }
    vm.imageTagsLoading = false;
  }

  async function _initServiceLabels() {
    if (!vm.region || vm.region.container_manager !== 'KUBERNETES') {
      return;
    }

    const { results } = await rbLabelsDataService
      .getAllLebels()
      .catch(() => ({ results: [] }));

    vm.labelKeys = _.chain(results)
      .map(label => label.key)
      .uniq()
      .value();
    vm.labelValues = _.chain(results)
      .map(label => label.value)
      .uniq()
      .value();
  }

  function _getImageTags() {
    if (vm.registryName) {
      return rbRepositoryDataService
        .getRepositoryTags({
          registry_name: vm.registryName,
          project_name: projectName,
          repo_name: repoName,
          view_type: 'security',
          page_size: 0,
        })
        .then(({ results = [] }) => {
          return results.map(tag => {
            const level = tag.level || 'unscanned';
            const className = 'rb-tag-level-' + level.toLowerCase();
            const levelName = translateService.get(
              'image_repository_scan_status_' + level.toLowerCase(),
            );
            return {
              tag_name: tag.tag_name,
              tag_display: `${
                tag.tag_name
              }<span class="${className} margin-left-1">(${levelName})</span>`,
            };
          });
        });
    } else {
      return rbRepositoryDataService
        .getPublicRepositoryTags({
          image_name: vm.model.image_name,
        })
        .then(tags => {
          return tags.map(tag => {
            return {
              tag_name: tag,
              tag_display: `<span>${tag}</span>`,
            };
          });
        });
    }
  }

  function onServiceDeployModelChange(option) {
    vm.model.pod_controller = option.value;
  }

  function deployModelEnabled() {
    return rbRegionUtilities.isKubernetes(vm.region);
  }

  function affinityEnabled() {
    return (
      vm.region &&
      vm.region.container_manager === 'KUBERNETES' &&
      vm.model.pod_controller !== 'DaemonSet'
    );
  }

  function _link() {
    vm.form = vm.basicForm;
  }
}
