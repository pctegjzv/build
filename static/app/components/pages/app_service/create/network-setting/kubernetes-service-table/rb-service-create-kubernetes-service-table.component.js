import { get } from 'lodash';

const templateStr = require('app/components/pages/app_service/create/network-setting/kubernetes-service-table/rb-service-create-kubernetes-service-table.html');
angular
  .module('app.components.pages')
  .component('rbServiceCreateKubernetesServiceTable', {
    bindings: {
      services: '<',
      serviceName: '<',
      ports: '<',
      mode: '@',
    },
    controller: rbServiceCreateKubernetesServiceTableController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbServiceCreateKubernetesServiceTableController(rbRegionService) {
  const vm = this;
  const KUBERNETES_SERVICE_TYPES = ['Headless', 'NodePort'];

  vm.$onInit = () => {
    vm.kubernetesServiceTypes = KUBERNETES_SERVICE_TYPES.map(t => ({
      key: t,
      value: t,
    }));
    if (vm.mode === 'update') {
      vm.services.forEach(r => {
        r.suffixName = r.name;
        r.server_data = true;
      });
    }

    vm.regionSub = rbRegionService.region$.subscribe(region => {
      vm.hostPortLimits = get(
        region,
        'features.service.manager.host_port_range',
        [30000, 32000],
      );
    });
  };

  vm.$onDestroy = () => {
    vm.regionSub && vm.regionSub.unsubscribe();
  };

  vm.deleteKubernetesService = index => {
    vm.services.splice(index, 1);
  };

  vm.addKubernetesService = () => {
    vm.services.push({
      type: 'Headless',
      name: '',
      node_port: '',
      container_port: '',
      suffixName: '',
      editable: true,
    });
  };

  vm.servcieChangedDisabled = service => {
    return service.server_data && vm.mode === 'update';
  };

  vm.nameChange = service => {
    if (!service.editable || service.server_data) {
      return;
    }
    service.name = `${vm.serviceName}-${
      service.type === 'Headless' ? 'hl' : 'np'
    }-${service.suffixName}`;
  };

  vm.kubernentsServiceChange = (service, option) => {
    if (!service.editable || service.server_data) {
      return;
    }
    service.type = option.value;
    service.name = `${vm.serviceName}-${
      option.value === 'Headless' ? 'hl' : 'np'
    }-${service.suffixName}`;
  };

  vm.containerPortChange = (service, option) => {
    if (option.value) {
      service.container_port = option.value;
    }
  };
  vm.validateKubeServiceName = ($value, service) => {
    if (!service.editable || service.server_data || !$value) {
      return true;
    }
    const pattern = /^[a-zA-Z]([-a-zA-Z0-9]*[a-zA-Z0-9])?$/;
    const name = `${vm.serviceName}-${
      service.type === 'Headless' ? 'hl' : 'np'
    }-${$value}`;
    return name.length < 64 && pattern.test(name);
  };
}
