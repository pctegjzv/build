/**
 * Created by liudong on 16/9/2.
 */
const _ = require('lodash');
const {
  SERVICE_NETWORK_MODES,
  SERVICE_LOAD_BALANCERS,
  SERVICE_DEPLOY_MODEL,
} = require('../../rb-service.constant');
const { CONTAINER_MANAGERS } = require('../../../region/rb-region.constant');
const templateStr = require('app/components/pages/app_service/create/network-setting/rb-service-create-network-setting.html');
angular
  .module('app.components.pages')
  .component('rbServiceCreateNetworkSetting', {
    bindings: {
      model: '<',
      form: '=',
      region: '<',
      regionFeatures: '<',
      imageData: '<',
    },
    controller: rbServiceCreateNetworkSettingController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbServiceCreateNetworkSettingController(
  $log,
  rbLoadBalancerDataService,
  rbNetworkSubnetService,
  translateService,
  rbServiceUtilities,
  rbPrivilegeService,
  rbLoadBalancerUtilities,
  LOAD_BALANCER_TYPES,
  LOAB_BALANCER_SERVICES_TYPE,
) {
  const vm = this;
  let _imagePorts = '';
  let _defaultLoadBalancerTypes = [];
  let _albList = [];
  let _pod_controller = 'Deployment';

  vm.$onInit = _init;
  vm.$postLink = _link;
  vm.$doCheck = _doCheck;

  vm.onNetworkModeChange = onNetworkModeChange;
  vm.onMipnOptionsChange = onMipnOptionsChange;
  vm.subnetOptionsChange = subnetOptionsChange;
  vm.onLoadBalancerTypeChange = onLoadBalancerTypeChange;
  vm.onRequiredPortsChange = onRequiredPortsChange;
  vm.requiredPortValidateFn = requiredPortValidateFn;

  vm.mipnOptionsEnabled = mipnOptionsEnabled;
  vm.iaasLoadBalancersOptionsEnabled = iaasLoadBalancersOptionsEnabled;
  vm.iaasLoadBalancersFieldsEnabled = iaasLoadBalancersFieldsEnabled;
  vm.containerRequiredPortsEnabled = containerRequiredPortsEnabled;
  vm.instancePortsEnabled = instancePortsEnabled;
  vm.subnetOptionEnabled = subnetOptionEnabled;
  vm.subnetChange = subnetChange;
  vm.subnetIpAdd = subnetIpAdd;
  vm.subnetIpRemove = subnetIpRemove;
  vm.ifSubnetOptionTrue = ifSubnetOptionTrue;
  vm.ifSubnetIpsNotEmpty = ifSubnetIpsNotEmpty;
  vm.isDaemonsetOrStatefulset = isDaemonsetOrStatefulset;
  vm.isSupportKubernetesService = isSupportKubernetesService;

  ////////

  function _init() {
    vm.networkModes = [];
    vm.loadBalancerTypes = [];
    vm.ipList = []; // region ip list
    vm.albList = []; // avaliable alb list
    vm.model.address_list = [];
    vm.model.kubernetes_services = [];
    vm.mipnOptions = [
      {
        name: translateService.get('off'),
        value: false,
      },
      {
        name: translateService.get('on'),
        value: true,
      },
    ];
    vm.subnetOptions = [
      {
        name: translateService.get('off'),
        value: false,
      },
      {
        name: translateService.get('on'),
        value: true,
      },
    ];
    vm.disabledMipnValues = [];

    // output data (serviceData)
    vm.model.networkMode = ''; // network mode: bridge, host ...
    vm.model.loadBalancerType = ''; // load balancer name: elb, slb, ulb...
    vm.model.subnets = []; //subnet list
    vm.model.subnetIps = []; //subnet ip list
    vm.model.subnet = '';
    vm.model.container_ports = [];
    vm.model.instance_ports = [];
    vm.model.load_balancers = [];
    vm.model.health_checks = [];
    vm.isServiceHealthcheckEnabled =
      rbServiceUtilities.isServiceHealthcheckEnabled;
    _initRegionNetworkFeatures();
  }

  function onNetworkModeChange(option) {
    vm.model.networkMode = option.value;
    vm.loadBalancerTypes = _defaultLoadBalancerTypes;
    if (option.value === SERVICE_NETWORK_MODES.BRIDGE) {
      vm.disabledMipnValues = [];
    } else if (option.value === SERVICE_NETWORK_MODES.HOST) {
      vm.disabledMipnValues = [true];
    }
    if (option.value !== SERVICE_NETWORK_MODES.MACVLAN) {
      vm.model.address_list = [];
    }
  }

  function onMipnOptionsChange(option) {
    vm.model.mipn_enabled = option.value;
    _setLoadBalancerOptions();
  }

  function subnetOptionsChange(option) {
    vm.model.subnet_assigned_enabled = option.value;
    vm.model.address_list = [];
  }

  function onLoadBalancerTypeChange(option) {
    vm.model.loadBalancerType = option.value;
  }

  function isCustomLoadBalancer() {
    return _filterLoadBalancerStr(vm.model.loadBalancerType);
  }

  function _filterLoadBalancerStr(str) {
    return /^\w{1}lb$/.test(str.toLowerCase());
  }

  function _initRegionNetworkFeatures() {
    const regionFeatures = vm.regionFeatures;
    vm.albEnabled = regionFeatures.includes('alb');
    vm.subnetEnabled = regionFeatures.includes('subnet');
    vm.networkModes = rbServiceUtilities.getNetworksFromRegionFeature(
      vm.region,
    );
    // set order : Bridge、Host、Flannel、Macvlan
    vm.networkModes = [...vm.networkModes].sort(
      (item0, item1) => _getOrder(item0.name) > _getOrder(item1.name),
    );
    _defaultLoadBalancerTypes = [];
    if (regionFeatures.includes('raw-container')) {
      _defaultLoadBalancerTypes.push(SERVICE_LOAD_BALANCERS.RAW);
    }
    if (regionFeatures.includes(LOAD_BALANCER_TYPES.HAPROXY)) {
      _defaultLoadBalancerTypes.push(SERVICE_LOAD_BALANCERS.HAPROXY);
    }
    if (regionFeatures.includes(LOAD_BALANCER_TYPES.NGINX)) {
      _defaultLoadBalancerTypes.push(SERVICE_LOAD_BALANCERS.NGINX);
    }
    const loadBalancer = _.find(regionFeatures, _filterLoadBalancerStr);
    if (loadBalancer) {
      _defaultLoadBalancerTypes.push(loadBalancer.toUpperCase());
    }
    _defaultLoadBalancerTypes.sort();
    vm.loadBalancerTypes = _defaultLoadBalancerTypes;
    vm.mipnEnabled = regionFeatures.includes('mipn');
    vm.internalEndpointEnabled = regionFeatures.includes('internal-haproxy');
    if (vm.albEnabled) {
      _getLoadBlancers();
    }
    if (vm.subnetEnabled) {
      _getSubnetList();
    }
  }

  function _doCheck() {
    // imagePorts change
    if (
      !_.isEmpty(vm.imageData.portList) &&
      _imagePorts !== vm.imageData.portList.join(',')
    ) {
      vm.model.container_ports = _.clone(vm.imageData.portList);
      _imagePorts = vm.imageData.portList.join(',');
    }

    if (
      vm.model.pod_controller &&
      vm.model.pod_controller !== _pod_controller
    ) {
      _pod_controller = vm.model.pod_controller;
      vm.model.load_balancers = [];
    }
  }

  function _link() {
    vm.form = vm.networkForm;
  }

  function _getOrder(name) {
    return { Bridge: 1, Flannel: 2, Host: 3 }[name] || 100;
  }

  function _getLoadBlancers() {
    // if alb_enabled, retrive load balancer list
    rbLoadBalancerDataService
      .getLoadBalancers({
        region_name: vm.region.name,
        region_id: vm.region.id,
        action: 'bind',
      })
      .then(data => {
        if (data.result && data.result.length) {
          // filter loadbalancers: haproxy or elb with no listeners
          const list = data.result.filter(item => {
            return (
              item.type === !item.listeners ||
              !item.listeners.length ||
              rbLoadBalancerUtilities.isSupportCheck(
                item,
                LOAB_BALANCER_SERVICES_TYPE.MULITPLE_SERVICE,
              )
            );
          });
          list.forEach(item => {
            item.display_name = getLoadbalancerDisplay(item);
          });
          _albList = list;
          _setLoadBalancerOptions();
        }
      })
      .catch(() => {});
  }

  function _getSubnetList() {
    vm.loadingSubnets = true;
    return rbNetworkSubnetService
      .getSubnetList({
        region_name: vm.region.name,
      })
      .then(data => {
        if (data.result) {
          vm.model.subnets = data.result;
        }
      })
      .catch(() => {
        //no catch this
      })
      .then(() => {
        vm.loadingSubnets = false;
      });
  }

  function _getSubnetIps(subnetName) {
    vm.loadingSubnetIps = true;
    const params = {
      subnet_name: subnetName,
      used: false,
    };
    return rbNetworkSubnetService
      .getSubnetIps(params)
      .then(data => {
        vm.model.subnetIps = data.result;
      })
      .catch(() => {
        //no catch this
      })
      .then(() => {
        vm.loadingSubnetIps = false;
      });
  }

  function _setLoadBalancerOptions() {
    let tempAlbList = [];
    if (vm.model.mipn_enabled) {
      tempAlbList = _.filter(_albList, item => {
        return rbLoadBalancerUtilities.isSupportCheck(
          item,
          LOAB_BALANCER_SERVICES_TYPE.HTTPS,
        );
      });
    } else {
      // support all loadbalancers
      tempAlbList = _albList;
    }
    // 排序 优先外网，然后优先haproxy
    vm.albList = _.orderBy(
      tempAlbList,
      [
        item => {
          return item.address_type !== 'external';
        },
        item => {
          return item.type !== LOAD_BALANCER_TYPES.HAPROXY;
        },
        item => {
          return item.type !== LOAD_BALANCER_TYPES.NGINX;
        },
      ],
      ['asc', 'asc', 'asc'],
    );
  }

  function onRequiredPortsChange(tags) {
    vm.model.container_ports = tags.map(item => {
      return parseInt(item);
    });
  }

  function requiredPortValidateFn(item) {
    const reg = /^[0-9]+$/;
    const number = parseInt(item);
    return reg.test(item) && number > 0 && number <= 65535;
  }

  function getLoadbalancerDisplay(item) {
    const address_type = translateService.get(
      'lb_address_' + item.address_type,
    );
    return `${item.name} ( ${item.type.toUpperCase()} / ${address_type} / ${
      item.address
    } )`;
  }

  function isDaemonsetOrStatefulset() {
    return (
      vm.model.pod_controller === SERVICE_DEPLOY_MODEL.DAEMONSET ||
      vm.model.pod_controller === SERVICE_DEPLOY_MODEL.STATEFULSET
    );
  }

  function mipnOptionsEnabled() {
    return (
      rbServiceUtilities.isMipnOptionsEnabled(
        vm.region,
        vm.model.networkMode,
      ) && !isDaemonsetOrStatefulset()
    );
  }

  function iaasLoadBalancersOptionsEnabled() {
    return !vm.albEnabled && !isDaemonsetOrStatefulset();
  }

  function iaasLoadBalancersFieldsEnabled() {
    return (
      !vm.albEnabled && isCustomLoadBalancer() && !isDaemonsetOrStatefulset()
    );
  }

  function containerRequiredPortsEnabled() {
    return (
      vm.albEnabled ||
      vm.model.networkMode === SERVICE_NETWORK_MODES.HOST ||
      vm.model.loadBalancerType === SERVICE_LOAD_BALANCERS.RAW
    );
  }

  function instancePortsEnabled() {
    return (
      !vm.albEnabled &&
      [SERVICE_LOAD_BALANCERS.HAPROXY, SERVICE_LOAD_BALANCERS.NGINX].includes(
        vm.model.loadBalancerType,
      ) &&
      (vm.model.networkMode === SERVICE_NETWORK_MODES.HOST ||
        vm.model.networkMode === SERVICE_NETWORK_MODES.BRIDGE)
    );
  }

  function subnetOptionEnabled() {
    return (
      vm.model.networkMode === SERVICE_NETWORK_MODES.MACVLAN && vm.subnetEnabled
    );
  }

  function subnetChange(option) {
    vm.model.subnet_name = option.subnet_name;
    vm.model.subnet_id = option.subnet_id;
    _getSubnetIps(option.subnet_name);
  }

  function subnetIpAdd(option) {
    vm.model.address_list.push(option.address);
  }

  function subnetIpRemove(option) {
    vm.model.address_list.forEach((item, index) => {
      if (item === option.address) {
        vm.model.address_list.splice(index, 1);
      }
    });
  }

  function ifSubnetOptionTrue() {
    return (
      !vm.loadingSubnets &&
      vm.model.subnets.length > 0 &&
      vm.model.subnet_assigned_enabled === true
    );
  }

  function ifSubnetIpsNotEmpty() {
    return (
      vm.model.subnet_assigned_enabled === true &&
      vm.model.subnets.length !== 0 &&
      vm.model.subnetIps.length === 0 &&
      !vm.loadingSubnetIps
    );
  }

  function isSupportKubernetesService() {
    return (
      _.get(vm.region, 'container_manager') === CONTAINER_MANAGERS.KUBERNETES &&
      vm.model.container_ports.length
    );
  }
}
