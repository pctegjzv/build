const templateStr = require('app/components/pages/app_service/create/preview-dialog/instance-port-table/rb-instance-port-table.html');
angular.module('app.components.pages').component('rbInstancePortTable', {
  controller: rbInstancePortTableController,
  controllerAs: 'vm',
  bindings: {
    instancePorts: '<',
  },
  template: templateStr,
});

function rbInstancePortTableController(translateService) {
  const vm = this;

  vm.$onInit = _init;

  function _init() {
    vm.ports = vm.instancePorts.map(item => {
      const endpointType = item.endpoint_type
        .substring(0, item.endpoint_type.indexOf('-'))
        .toUpperCase();
      const servicePort = item.service_port || translateService.get('random');
      return {
        endpointType,
        servicePort,
        containerPort: item.container_port,
        ipaddress: item.ipaddress,
      };
    });
  }
}
