const templateStr = require('app/components/pages/app_service/create/preview-dialog/kubernetes' +
  '-services-table/rb-kubernetes-services-preview-dialog.html');
angular.module('app.components.pages').component('rbKubernetesServicesTable', {
  controller: rbKubernetesServicesTableController,
  controllerAs: 'vm',
  bindings: {
    services: '<',
  },
  template: templateStr,
});

function rbKubernetesServicesTableController() {
  const vm = this;
  vm.$onInit = () => {};
}
