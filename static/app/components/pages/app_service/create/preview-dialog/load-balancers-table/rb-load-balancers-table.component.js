const templateStr = require('app/components/pages/app_service/create/preview-dialog/load-balancers-table/rb-load-balancers-table.html');
angular.module('app.components.pages').component('rbLoadBalancersTable', {
  controller: rbLoadBalancersTableController,
  controllerAs: 'vm',
  bindings: {
    loadBalancers: '<',
  },
  template: templateStr,
});

function rbLoadBalancersTableController(rbServiceUtilities) {
  const vm = this;

  vm.$onInit = _init;

  function _init() {
    const balancers = vm.loadBalancers.map(balancer => {
      const listeners = JSON.parse(JSON.stringify(balancer.listeners));
      if (
        rbServiceUtilities.isIaasLoadBalancer(balancer.type) &&
        balancer.version !== 2
      ) {
        listeners[0].networkType = balancer.is_internal
          ? 'network_internal'
          : 'network_external';
      } else {
        listeners[0].lbName = balancer.name;
        listeners[0].lbType = balancer.type;
      }
      listeners[0].num = balancer.listeners.length;
      listeners[0].showBorder = true;
      return listeners;
    });
    vm.listeners = [].concat(...balancers);
  }
}
