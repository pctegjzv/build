import {
  SERVICE_NETWORK_MODES,
  SERVICE_LOAD_BALANCERS,
} from '../../rb-service.constant';
import _ from 'lodash';
const templateStr = require('app/components/pages/app_service/create/preview-dialog/rb-service-preview-dialog.html');
angular.module('app.components.pages').component('rbServicePreviewDialog', {
  controller: rbServicePreviewDialogController,
  controllerAs: 'vm',
  bindings: {
    serviceData: '<',
    regionDisplayName: '<',
    regionName: '<',
    autoStart: '<',
    instanceSize: '<',
    serviceType: '<',
    lbType: '<',
    mipnOptionsEnabled: '<',
    linkServiceList: '<',
    regionManager: '<',
  },
  template: templateStr,
});

function rbServicePreviewDialogController(
  rbModal,
  translateService,
  rbServiceUtilities,
  rbRegionDataService,
) {
  const vm = this;

  vm.isBasicConfigCollapse = false;
  vm.isNetworkConfigCollapse = true;
  vm.isAdvancedConfigCollapse = true;
  vm.isNoneTargetWithNodeSelector = true;

  vm.$onInit = _init;

  function _init() {
    vm.serviceSettings = JSON.parse(JSON.stringify(vm.serviceData));
    initNetworkSettings();
    initAdvancedSettings();
  }

  function initNetworkSettings() {
    vm.exposedPorts =
      vm.serviceSettings.ports ||
      vm.serviceSettings.raw_container_ports ||
      vm.serviceSettings.required_ports;

    if (
      vm.serviceSettings.network_mode === SERVICE_NETWORK_MODES.BRIDGE &&
      vm.lbType === SERVICE_LOAD_BALANCERS.RAW
    ) {
      vm.exposedPorts = undefined;
    }

    if (vm.serviceSettings.node_selector) {
      vm.serviceSettings.node_selector_string = [];
      const keys = Object.keys(vm.serviceSettings.node_selector);
      keys.forEach(n => {
        vm.serviceSettings.node_selector_string.push(
          `${n}: ${vm.serviceSettings.node_selector[n]}`,
        );
      });
      vm.serviceSettings.node_selector_string = vm.serviceSettings.node_selector_string.join(
        ', ',
      );
      checkAvailableNodes(vm.serviceSettings.node_selector);
    }

    const loadBalancer =
      (vm.serviceSettings.load_balancers &&
        vm.serviceSettings.load_balancers[0]) ||
      {};
    let isIaas;
    if (loadBalancer.type) {
      isIaas =
        rbServiceUtilities.isIaasLoadBalancer(loadBalancer.type) &&
        loadBalancer.version !== 2;
    }
    vm.portMapMode = vm.serviceSettings.instance_ports || isIaas;

    if (
      vm.serviceSettings.load_balancers &&
      vm.serviceSettings.load_balancers[0] &&
      vm.serviceSettings.load_balancers[0].type !== 'SLB'
    ) {
      vm.lbType = undefined;
    }
  }

  function initAdvancedSettings() {
    vm.scalingMode =
      vm.serviceSettings.scaling_mode === 'AUTO'
        ? translateService.get('auto')
        : translateService.get('manual');
    if (vm.serviceSettings.autoscaling_config) {
      vm.autoScalingConfig = JSON.parse(vm.serviceSettings.autoscaling_config);
    }

    if (vm.serviceSettings.instance_envvars['__ALAUDA_FILE_LOG_PATH__']) {
      vm.includeLogFiles = vm.serviceSettings.instance_envvars[
        '__ALAUDA_FILE_LOG_PATH__'
      ].split(',');
    }
    if (vm.serviceSettings.instance_envvars['__ALAUDA_EXCLUDE_LOG_PATH__']) {
      vm.excludeLogFiles = vm.serviceSettings.instance_envvars[
        '__ALAUDA_EXCLUDE_LOG_PATH__'
      ].split(',');
    }
    vm.envVars = vm.serviceSettings.instance_envvars;
    delete vm.envVars['__ALAUDA_FILE_LOG_PATH__'];
    delete vm.envVars['__ALAUDA_EXCLUDE_LOG_PATH__'];
    vm.showEnvVars = Object.keys(vm.envVars).length > 0;

    if (vm.serviceSettings.volumes) {
      vm.volumes = vm.serviceSettings.volumes.map(item => ({
        imagePath: item.app_volume_dir,
        volume:
          item.volume_id === 'host_path'
            ? `< ${translateService.get('volume_mount_host_path')} >`
            : item.volume_name,
        hostPath: item.volume_id === 'host_path' ? item.volume_name : '',
      }));
    }
  }

  function checkAvailableNodes(selector) {
    if (_.isEmpty(selector)) {
      vm.isNoneTargetWithNodeSelector = true;
      return;
    }
    const kvStr = Object.keys(selector)
      .map(r => `${r}:${selector[r]}`)
      .join(',');
    rbRegionDataService
      .checkAvailableNodes(kvStr, vm.regionName)
      .then(data => {
        vm.isNoneTargetWithNodeSelector = data.nodes.length;
      })
      .catch(() => {});
  }

  vm.confirm = () => {
    rbModal.hide();
  };

  vm.cancel = () => {
    rbModal.cancel();
  };
}
