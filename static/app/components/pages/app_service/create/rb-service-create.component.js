/**
 * Created by liudong on 16/9/2.
 */
import * as _ from 'lodash';
const {
  SERVICE_TARGET_STATES,
  SERVICE_NETWORK_MODES,
  SERVICE_LOAD_BALANCERS,
  SERVICE_TYPE,
  SERVICE_DEPLOY_MODEL,
} = require('../rb-service.constant');
const templateStr = require('app/components/pages/app_service/create/rb-service-create.html');
angular.module('app.components.pages').component('rbServiceCreate', {
  bindings: {
    regionName: '<',
    registryName: '<',
    imageName: '<',
  },
  controller: rbServiceCreateController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbServiceCreateController(
  $state,
  $log,
  rbModal,
  rbAccountService,
  rbToast,
  translateService,
  rbServiceUtilities,
  rbRepositoryDataService,
  rbHttp,
  LOAB_BALANCER_SERVICES_TYPE,
  rbLoadBalancerUtilities,
  rbRegionDataService,
  rbRegionService,
  rbRouterStateHelper,
  rbServiceInstanceSize,
  WEBLABS,
) {
  const vm = this;

  vm.prevStep = () => (vm.configStep = Math.max(0, vm.configStep - 1));
  vm.nextStep = nextStep;
  vm.create = create;
  vm.cancel = cancel;
  vm.cancelCreate = cancelCreate;
  vm.onImageTagChange = onImageTagChange;

  vm.$onInit = _init;

  //////////
  async function _init() {
    vm.weblabs = WEBLABS;
    vm.configStep = 0;
    vm.steps = ['basicInfo', 'networkSetting', 'advanceSetting'];
    vm.maxStep = vm.steps.length - 1;
    vm.forms = {}; // ng FormControllers. This will be initialized in templates.
    vm.region = null;
    vm.imageData = {
      run_command: '',
      entrypoint: '',
      portList: [],
      volumes: [],
    };
    if (!vm.regionName) {
      vm.regionName = rbRegionService.getRegionNameFromCookie();
    }
    rbRegionService.setRegionByName(vm.regionName);
    vm.serviceData = {
      region_name: vm.regionName,
      image_name: vm.imageName,
      volumes: [], // if not initialized with [], will be undefined when image tag detail not found
    };
    await _initRegionData();
    vm.initialized = true;
  }

  async function _initRegionData() {
    try {
      const res = await rbRegionDataService.getRegionDetail(vm.regionName);
      vm.region = res.result;
      vm.regionFeatures = _.get(vm.region, 'features.service.features', []);
    } catch (err) {
      $log.error('failed to load region');
      rbRouterStateHelper.back();
    }
  }

  function _getImageDetail(params) {
    if (vm.registryName) {
      return rbRepositoryDataService.getRepositoryTagDetail(params);
    } else {
      return rbRepositoryDataService.getPublicRepositoryTagDetail({
        image_name: vm.imageName,
        tag_name: params.tag_name,
      });
    }
  }

  function _initImageData(data) {
    vm.imageData.run_command = data.run_command || '';
    vm.imageData.entrypoint = data.entrypoint || '';
    vm.imageData.portList.length = 0;
    if (data.instance_ports) {
      const ports = data.instance_ports.split(',');
      _.forEach(ports, port => {
        vm.imageData.portList.push(parseInt(port));
      });
    }
    data.volumes.forEach(volume => {
      volume.isPreset = true;
      volume.display_text =
        '< ' + translateService.get('volume_mount_host_path') + ' >';
      volume.volume_name = volume.app_volume_dir;
      volume.volume_id = 'host_path';
    });
    vm.imageData.volumes = data.volumes;
    vm.serviceData.volumes = data.volumes;
  }

  async function onImageTagChange(params) {
    const result = await _getImageDetail(params);
    if (!result) {
      rbToast.warning(translateService.get('get_image_tag_detail_failed'));
    } else {
      _initImageData(result);
    }
  }

  function nextStep() {
    if (_getValidState()) {
      // check if host has at least one container port
      if (
        vm.configStep === vm.maxStep - 1 &&
        (!_validateHostNetworkSetting() ||
          !_validateMacvlanNetworkSetting() ||
          !_validateLbSettings())
      ) {
        return;
      }
      vm.configStep = Math.min(vm.maxStep, vm.configStep + 1);
    }
  }

  function _validateLbSettings() {
    return !vm.serviceData.load_balancers.some(lb =>
      lb.listeners.some(i => i.protocol === 'https' && !i.domains.length),
    );
  }

  function _validateHostNetworkSetting() {
    if (
      vm.serviceData.networkMode === SERVICE_NETWORK_MODES.HOST &&
      !vm.serviceData.container_ports.length
    ) {
      rbToast.warning(translateService.get('host_mode_container_port_need'));
      return false;
    }
    return true;
  }

  function _validateMacvlanNetworkSetting() {
    if (!vm.regionFeatures.includes('subnet')) {
      return true;
    } else if (
      vm.serviceData.networkMode === SERVICE_NETWORK_MODES.MACVLAN &&
      (vm.serviceData.subnets.length === 0 ||
        (vm.serviceData.subnet_assigned_enabled &&
          vm.serviceData.subnetIps.length === 0))
    ) {
      return false;
    }
    return true;
  }

  function create() {
    if (!_getValidState()) {
      return;
    }
    if (!_validateHostNetworkSetting() || !_validateMacvlanNetworkSetting()) {
      return;
    }

    const parsedData = generateCreateData();
    if (!parsedData) {
      return;
    }
    preview(parsedData)
      .then(() => {
        _create(parsedData);
      })
      .catch(() => {});
  }

  function preview(parsedData) {
    const instanceSizeText = rbServiceInstanceSize.getInstanceSizeText(
      vm.serviceData.custom_instance_size,
    );
    const serviceType =
      vm.serviceData.service_type === 'STATELESS'
        ? translateService.get('stateless_app')
        : translateService.get('stateful_app');
    const mipnOptionsEnabled = rbServiceUtilities.isMipnOptionsEnabled(
      vm.region,
      vm.serviceData.networkMode,
    );
    const linkServiceList = _.clone(vm.serviceData.linkServiceList);
    return rbModal.show({
      title: translateService.get('create_service'),
      width: 800,
      locals: {
        serviceData: parsedData,
        regionDisplayName: vm.region.display_name,
        autoStart: vm.serviceData.autoStart,
        instanceSize: instanceSizeText,
        regionName: vm.serviceData.region_name,
        regionManager: vm.region.container_manager,
        serviceType,
        lbType: vm.serviceData.loadBalancerType,
        mipnOptionsEnabled,
        linkServiceList,
      },
      component: 'rb-service-preview-dialog',
    });
  }

  function generateCreateData() {
    const data = {
      service_name: vm.serviceData.service_name,
      namespace: rbAccountService.getCurrentNamespace(),
      region_name: vm.serviceData.region_name,
      pod_controller: vm.serviceData.pod_controller,
      target_state: vm.serviceData.autoStart
        ? SERVICE_TARGET_STATES.START
        : SERVICE_TARGET_STATES.STOP,
      network_mode: vm.serviceData.networkMode,
      image_name: vm.serviceData.image_name,
      image_tag: vm.serviceData.image_tag,
      instance_size: vm.serviceData.instance_size,
      scaling_mode: vm.serviceData.scaling_mode,
      target_num_instances: vm.serviceData.target_num_instances,
      update_strategy: vm.serviceData.update_strategy,
      run_command: '',
      entrypoint: '',
      space_name: vm.weblabs['quota_enabled']
        ? vm.serviceData.space_name
        : undefined,
      labels: vm.serviceData.labels || undefined,
    };

    // service type
    if (vm.serviceData.scaling_mode === 'AUTO') {
      const autoscaling_config = _.clone(vm.serviceData.autoscaling_config);
      delete autoscaling_config.upper_threshold;
      delete autoscaling_config.lower_threshold;
      delete autoscaling_config.metric_name;
      delete autoscaling_config.metric_stat;
      data.autoscaling_config = JSON.stringify(autoscaling_config);
      data.target_num_instances =
        vm.serviceData.autoscaling_config.minimum_num_instances;
    }

    // custom_instance_size
    if (data.instance_size === 'CUSTOMIZED') {
      data.custom_instance_size = vm.serviceData.custom_instance_size;
    }
    // run_command, entrypoint
    if (!vm.serviceData.useDefaultCmd) {
      data.run_command = vm.serviceData.run_command;
      // region using docker swarm does not support custom entrypoint
      if (vm.region.container_manager !== 'SWARM') {
        data.entrypoint = vm.serviceData.entrypoint;
      }
    }
    // linked_to_apps
    const linked_to_apps = {};
    let linkServiceList = _.filter(vm.serviceData.linkServiceList, link => {
      return !!link.alias;
    });
    linkServiceList = _.uniqWith(linkServiceList, (a, b) => {
      return a.uuid === b.uuid;
    });
    if (linkServiceList.length > 0) {
      _.forEach(linkServiceList, link => {
        linked_to_apps[link['uuid']] = link['alias'];
      });
      data.linked_to_apps = JSON.stringify(linked_to_apps);
    }
    // instance_envvars
    data.instance_envvars = {};
    const envVars = _.filter(vm.serviceData.envVars, envVar => {
      return !!envVar.name;
    });
    if (envVars.length > 0) {
      _.forEach(envVars, item => {
        data.instance_envvars[item.name] = item.value;
      });
    }
    // log files
    const excludeLogFiles = _.filter(vm.serviceData.excludeLogFiles, item => {
      return !!item;
    });
    const includeLogFiles = _.filter(vm.serviceData.includeLogFiles, item => {
      return !!item;
    });
    if (excludeLogFiles.length > 0) {
      data.instance_envvars[
        '__ALAUDA_EXCLUDE_LOG_PATH__'
      ] = excludeLogFiles.join(',');
    }
    if (includeLogFiles.length > 0) {
      data.instance_envvars['__ALAUDA_FILE_LOG_PATH__'] = includeLogFiles.join(
        ',',
      );
    }

    // volumes
    if (vm.serviceData.service_type === SERVICE_TYPE.STATEFUL) {
      data.volumes = vm.serviceData.volumes
        ? _.cloneDeep(vm.serviceData.volumes)
        : [];
      data.volumes = data.volumes
        .filter(vol => !!vol.app_volume_dir)
        .map(volume => ({
          app_volume_dir: volume.app_volume_dir,
          volume_id: volume.volume_id,
          volume_name: volume.volume_name,
        }));
      const cp = data.volumes.map(r => r.app_volume_dir);
      if (cp.length !== _.uniq(cp).length) {
        rbToast.warning(translateService.get('create_app_volume_dir_repeat'));
        return;
      }
    }

    // envfiles
    if (vm.serviceData.envFiles.length > 0) {
      data.envfiles = [];
      const files = _.uniqWith(vm.serviceData.envFiles, (a, b) => {
        return a.uuid === b.uuid;
      });
      _.forEach(files, _file => {
        const envfile = {
          name: _file.name,
          uuid: _file.uuid,
        };
        data.envfiles.push(envfile);
      });
    }
    //subnet
    if (
      vm.regionFeatures.includes('subnet') &&
      vm.serviceData.networkMode === SERVICE_NETWORK_MODES.MACVLAN
    ) {
      if (vm.serviceData.subnet_assigned_enabled === true) {
        data.subnet = {
          subnet_id: vm.serviceData.subnet_id,
          count: vm.serviceData.address_list.length,
          address_list: vm.serviceData.address_list,
        };
      } else {
        data.subnet = {
          subnet_id: vm.serviceData.subnet_id,
        };
      }
    }
    // raw_container_ports: lb 为 raw_container 模式必选
    let container_ports = _.clone(vm.serviceData.container_ports);
    container_ports = _.filter(container_ports, item => {
      return !!item;
    });

    if (vm.regionFeatures.includes('alb')) {
      data.version = 'v2'; // needed for sven v2 api
      data.ports = container_ports;
      data.load_balancers = [];
      // load_balancers -> alb
      let loadBalancerTestPass = true;
      const loadBalancers = _.cloneDeep(vm.serviceData.load_balancers);
      let listenersInvalid = false;
      _.forEach(loadBalancers, loadBalancer => {
        loadBalancer = _.cloneDeep(loadBalancer);
        const listeners = (loadBalancer.listeners = loadBalancer.listeners.filter(
          listener => {
            return !!listener.container_port;
          },
        ));
        const _listeners = _.uniqWith(listeners, (a, b) => {
          return (
            a.listener_port &&
            b.listener_port &&
            a.listener_port === b.listener_port
          );
        });
        if (listeners.length > _listeners.length) {
          rbToast.warning(translateService.get('lb_ports_repeated'));
          loadBalancerTestPass = false;
          return false;
        }
        loadBalancer.listeners.forEach(listener => {
          // only slb v2 need rules to config listener
          if (
            listener.certificate_name.length &&
            !listener.domains.length &&
            listener.protocol === 'https'
          ) {
            listenersInvalid = true;
          }
          if (
            rbLoadBalancerUtilities.isSupportCheck(
              loadBalancer,
              LOAB_BALANCER_SERVICES_TYPE.HTTPS,
            )
          ) {
            const rules = listener.domains.map(r => {
              const splitChar = r.indexOf('^') !== -1 ? '^' : '/';
              return {
                domain: r.split(splitChar)[0],
                url: r.replace(r.split(splitChar)[0], ''),
              };
            });
            listener.rules = rules;
            delete listener.domains;
          }
          if (!listener.listener_port && !_.isNumber(listener.listener_port)) {
            listener.listener_port = 0;
          }
          if (listener.protocol !== 'https') {
            delete listener.certificate_name;
            delete listener.certificate_id;
          }
        });
        if (loadBalancer.listeners.length !== 0) {
          data.load_balancers.push(loadBalancer);
        }
      });
      if (listenersInvalid) {
        return;
      }
      if (!loadBalancerTestPass) {
        return;
      }
      vm.serviceData.mipn_enabled &&
        (data.mipn_enabled = vm.serviceData.mipn_enabled);
    } else {
      if (vm.serviceData.loadBalancerType === SERVICE_LOAD_BALANCERS.RAW) {
        data.raw_container_ports = container_ports;
      } else {
        // required_ports: HOST 模式下必选
        if (vm.serviceData.networkMode === SERVICE_NETWORK_MODES.HOST) {
          data.required_ports = container_ports;
        }
      }
      // load_balancers: lb 模式为 IAAS lb 时必选
      if (
        rbServiceUtilities.isIaasLoadBalancer(vm.serviceData.loadBalancerType)
      ) {
        data.load_balancers = [];
        let loadBalancerTestPass = true;
        const loadBalancers = _.clone(vm.serviceData.load_balancers);
        _.forEach(loadBalancers, loadBalancer => {
          const listeners = loadBalancer.listeners;
          const _listeners = _.uniqWith(listeners, (a, b) => {
            return a.lb_port === b.lb_port;
          });
          if (listeners.length > _listeners.length) {
            rbToast.warning(translateService.get('lb_ports_repeated'));
            loadBalancerTestPass = false;
            return false;
          }
          loadBalancer.listeners = _.filter(listeners, listener => {
            return !!listener.container_port && !!listener.lb_port;
          });
          if (loadBalancer.listeners.length !== 0) {
            data.load_balancers.push(loadBalancer);
          }
        });
        if (!loadBalancerTestPass) {
          return;
        }
      }
      // instance_ports: lb 类型为 haproxy 时必选
      if (
        [SERVICE_LOAD_BALANCERS.HAPROXY, SERVICE_LOAD_BALANCERS.NGINX].includes(
          vm.serviceData.loadBalancerType,
        )
      ) {
        let instance_ports = _.cloneDeep(vm.serviceData.instance_ports);
        instance_ports = _.filter(instance_ports, item => {
          return !!item.container_port;
        });
        data.instance_ports = instance_ports;
        // custom_domain_name
        const allowCustomDomain = _.some(data.instance_ports, item => {
          return item.endpoint_type === 'http-endpoint';
        });
        if (allowCustomDomain) {
          let domains = vm.serviceData.customDomains;
          domains = _.filter(domains, item => {
            return !!item;
          });
          data.custom_domain_name = domains.join(';');
        }
      }
      //multiple instances per node
      if (
        vm.regionFeatures.includes('mipn') &&
        vm.serviceData.networkMode === SERVICE_NETWORK_MODES.BRIDGE &&
        [
          SERVICE_LOAD_BALANCERS.RAW,
          SERVICE_LOAD_BALANCERS.HAPROXY,
          SERVICE_LOAD_BALANCERS.NGINX,
        ].includes(vm.serviceData.loadBalancerType)
      ) {
        data.mipn_enabled = true;
      }
    }

    if (
      rbServiceUtilities.isServiceHealthcheckEnabled(
        vm.serviceData,
        vm.regionFeatures,
      )
    ) {
      data.health_checks = vm.serviceData.health_checks;
    }

    // node_tag
    if (vm.serviceData.node_tag) {
      data.node_tag = vm.serviceData.node_tag;
    }
    // node_labels for k8s
    if (vm.serviceData.node_selector && vm.serviceData.node_selector.length) {
      const target = {};
      vm.serviceData.node_selector.map(r => {
        target[r.key] = r.value;
      });
      data.node_selector = target;
    }

    // update strategy
    if (
      vm.region.container_manager === 'KUBERNETES' &&
      updateStrategyEnabled()
    ) {
      if (
        data.update_strategy.max_surge &&
        data.update_strategy.max_unavailable
      ) {
        Object.keys(data.update_strategy).forEach(k => {
          if (k === 'max_surge' || k === 'max_unavailable') {
            data.update_strategy[k] = data.update_strategy[k].toString();
            if (!data.update_strategy[k].endsWith('%')) {
              data.update_strategy[k] = parseInt(data.update_strategy[k]);
            }
          }
        });
        delete data.update_strategy.max_surge_disabled;
        delete data.update_strategy.max_unavailable_disabled;
      }
    } else {
      delete data.update_strategy;
    }

    // kubernetes services
    if (vm.region.container_manager === 'KUBERNETES') {
      const kube_services = _.cloneDeep(vm.serviceData.kubernetes_services);
      kube_services.forEach(r => {
        delete r.suffixName;
        if (r.type === 'Headless') {
          delete r.container_port;
          delete r.node_port;
        }
        if (!r.node_port) {
          delete r.node_port;
        }
        return r;
      });
      data.kube_config = {
        services: kube_services,
      };
    }

    // kubernetes affinity
    if (affinityEnabled()) {
      const kube_affinity = _.cloneDeep(vm.serviceData.kube_affinity);
      data.kube_config = {
        ...(data.kube_config || {}),
        pod: kube_affinity.pod,
      };
    }

    const configManageEnabled = vm.regionFeatures.includes('mount-points');
    if (configManageEnabled && vm.serviceData.configFiles.length > 0) {
      data.mount_points = _.map(
        vm.serviceData.configFiles,
        rbServiceUtilities.formatConfigFileData,
      );
    } else {
      delete data.mount_points;
    }
    if (data.pod_controller === SERVICE_DEPLOY_MODEL.DAEMONSET) {
      delete data.target_num_instances;
      delete data.scaling_mode;
    }
    if (
      ['DaemonSet', 'StatefulSet'].includes(data.pod_controller) &&
      _.get(vm.region, 'features.kubernetes.version') < '1.6'
    ) {
      delete data.update_strategy;
    }
    return data;
  }

  function _create(data) {
    vm.submitting = true;
    rbHttp
      .sendRequest({
        method: 'POST',
        url: `/ajax/v1/services/${rbAccountService.getCurrentNamespace()}`,
        data: data,
      })
      .then(response => {
        if (response) {
          rbToast.success(translateService.get('service_create_success'));
          const params = {};
          params['service_name'] = response.uuid;
          $state.go('app_service.service.service_detail', params);
        } else {
          if (response.formerrors) {
            for (const key in response.formerrors) {
              rbToast.error(`${key} ${response.formerrors[key].join('')}`);
            }
          }
        }
      })
      .catch(() => {
        // placeholder
      })
      .then(() => {
        vm.submitting = false;
      });
  }

  function _getValidState() {
    let valid;
    const currentForm = vm.forms[vm.steps[vm.configStep]];
    if (currentForm) {
      currentForm.$setSubmitted();
      valid = currentForm.$valid;

      if (vm.weblabs['quota_enabled'] && !vm.serviceData.space_name) {
        valid = false;
      }
    }
    return valid;
  }

  function updateStrategyEnabled() {
    return (
      _.get(vm.region, 'container_manager') === 'KUBERNETES' &&
      ((vm.serviceData.pod_controller === SERVICE_DEPLOY_MODEL.DAEMONSET &&
        _.get(vm.region, 'features.kubernetes.version', '') >= '1.6') ||
        vm.serviceData.pod_controller === SERVICE_DEPLOY_MODEL.DEPLOYMENT)
    );
  }

  function affinityEnabled() {
    return (
      _.get(vm.region, 'container_manager') === 'KUBERNETES' &&
      _.get(vm.serviceData, 'pod_controller') !== 'DaemonSet'
    );
  }

  function cancel() {
    $state.go('app_service.service.image_select', {
      region_name: vm.regionName,
    });
  }

  function cancelCreate() {
    $state.go('app_service.service');
  }
}
