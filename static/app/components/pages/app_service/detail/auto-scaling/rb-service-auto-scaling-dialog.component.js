/**
 * Created by liudong on 2017/7/27.
 */
import { CONTAINER_MANAGERS } from '../../../region/rb-region.constant';
import { SERVICE_DEPLOY_MODEL } from '../../rb-service.constant';
import { INT_PATTERN } from 'app/components/common/config/common-pattern';

const templateStr = require('app/components/pages/app_service/detail/auto-scaling/rb-service-auto-scaling-dialog.html');
angular.module('app.components.pages').component('rbServiceAutoScalingDialog', {
  controller: rbServiceAutoScalingDialogController,
  controllerAs: 'vm',
  bindings: {
    serviceData: '<',
    region: '<',
  },
  template: templateStr,
});

function rbServiceAutoScalingDialogController(
  ngRouter,
  $log,
  $state,
  translateService,
  rbToast,
  rbModal,
  rbServiceDataService,
  rbAlarmDataService,
) {
  const vm = this;

  const SCALING_MODE_MANUAL = {
    name: translateService.get('manual'),
    value: 'MANUAL',
  };

  const SCALING_MODE_AUTO = {
    name: translateService.get('auto_scale'),
    value: 'AUTO',
  };

  const SCALING_MODE_MAP = {
    [2]: [SCALING_MODE_MANUAL, SCALING_MODE_AUTO],
    [1]: [SCALING_MODE_MANUAL],
  };

  vm.$onInit = _init;

  vm.getScalingModeOptions = getScalingModeOptions;
  vm.onScalingModeChange = onScalingModeChange;
  vm.onScalingInAlarmChange = onScalingInAlarmChange;
  vm.onScalingOutAlarmChange = onScalingOutAlarmChange;
  vm.addScalingInAlarm = addScalingInAlarm;
  vm.addScalingOutAlarm = addScalingOutAlarm;
  vm.createAlarm = createAlarm;
  vm.getAlarmItemStatusClass = getAlarmItemStatusClass;
  vm.viewAlarmDetail = viewAlarmDetail;

  vm.confirm = confirm;
  vm.cancel = cancel;

  async function _init() {
    vm.scalingMode = vm.serviceData.scaling_mode;
    vm.targetNumInstancesRegExp = INT_PATTERN;

    if (vm.serviceData.autoscaling_config === '{}') {
      vm.autoscaling_config = {
        decrease_delta: 1,
        increase_delta: 1,
        minimum_num_instances: 1,
        maximum_num_instances: 20,
        wait_period: 30,
      };
    } else {
      vm.autoscaling_config = JSON.parse(vm.serviceData.autoscaling_config);
    }
    vm.target_num_instances = vm.serviceData.target_num_instances || 1;

    await _getAlarms();
    await _getServiceAlarms();

    vm.initialized = true;
  }

  /**
   * get alarms releated to service uuid
   */
  async function _getServiceAlarms() {
    const serviceAlarms = await rbAlarmDataService.getServiceAlarms(
      vm.serviceData.uuid,
    );
    _initServiceAlarms(serviceAlarms);
  }

  async function _getAlarms() {
    vm.alarms = await rbAlarmDataService.getAlarms();
  }

  function _initServiceAlarms(serviceAlarms = []) {
    const scalingInAlarms = [];
    const scalingOutAlarms = [];
    // const types = ['alarm_actions.services', 'insufficient_actions.services', 'ok_actions.services'];
    serviceAlarms.forEach(alarm => {
      if (alarm.action_type === 'scaling_in') {
        scalingInAlarms.push(alarm);
      } else if (alarm.action_type === 'scaling_out') {
        scalingOutAlarms.push(alarm);
      }
    });
    vm.scalingInAlarms = scalingInAlarms;
    vm.scalingOutAlarms = scalingOutAlarms;

    vm.availableAlarms = (vm.alarms || []).filter(item => {
      const res = _.find(serviceAlarms, _item => {
        return item.uuid === _item.uuid;
      });
      return !res;
    });
  }

  /**
   * Calculate the scaling mode options. This function will be used as a getter.
   * https://trello.com/c/yag7jujX/526-glusterfs
   */
  function getScalingModeOptions() {
    let showAutoOption = false;
    if (vm.serviceData.pod_controller === SERVICE_DEPLOY_MODEL.STATEFULSET) {
      showAutoOption = false;
    } else if (!vm.serviceData.is_stateful) {
      showAutoOption = true;
    } else {
      const volumes = _.get(vm, 'serviceData.volumes', []);
      showAutoOption = !volumes.some(volume => volume.driver_name === 'ebs');
    }
    // hmm, side-effect for getter function, bad design ...
    // KUBERNETES does not support auto instances but can have multiple instances
    if (
      showAutoOption ||
      vm.region.container_manager === CONTAINER_MANAGERS.KUBERNETES
    ) {
      vm.disableInstanceNumber = false;
    } else {
      vm.target_num_instances = 1;
      vm.disableInstanceNumber = true;
    }
    return SCALING_MODE_MAP[showAutoOption ? 2 : 1];
  }

  function onScalingModeChange(option) {
    vm.scalingMode = option.value;
  }

  function onScalingInAlarmChange(option) {
    vm.scalingInAlarm = option;
  }

  function onScalingOutAlarmChange(option) {
    vm.scalingOutAlarm = option;
  }

  /**
   * update alarm to bind service uuid
   * @param option
   */
  async function addScalingInAlarm() {
    vm.scalingInAlarmsSubmitting = true;
    await _updateAlarmService(vm.scalingInAlarm, 'scaling_in');
    vm.scalingInAlarmsSubmitting = false;
  }

  async function addScalingOutAlarm() {
    vm.scalingOutAlarmsSubmitting = true;
    await _updateAlarmService(vm.scalingOutAlarm, 'scaling_out');
    vm.scalingOutAlarmsSubmitting = false;
  }

  async function _updateAlarmService(option, scaling_type) {
    if (!option) {
      return;
    }
    const alarm = _.cloneDeep(option);
    const resource = {
      type: scaling_type,
      uuid: vm.serviceData.uuid,
    };
    if (!alarm.alarm_actions) {
      alarm.alarm_actions = {
        services: [],
      };
    }
    if (!alarm.alarm_actions.services) {
      alarm.alarm_actions.services = [];
    }
    alarm.alarm_actions.services.push(resource);
    delete alarm.children;
    try {
      //$log.log('update alarm data', JSON.stringify(alarm));
      await rbAlarmDataService.updateAlarm(alarm.uuid, alarm);
      _getServiceAlarms();
    } catch (err) {
      $log.error('update alarm service error');
    }
  }

  function createAlarm(scaling_type) {
    // save autoscaling config, then redirect to alarm create page
    vm.scalingForm.$setSubmitted();
    if (vm.scalingForm.$invalid) {
      return;
    }

    vm.submitting = true;
    _saveAutoscalingConfig()
      .then(() => {
        rbToast.success(translateService.get('service_scaling_config_saved'));
        _redirectToAlarmCreate(scaling_type);
      })
      .catch(() => {
        // if autoscaling config save failed, no redirect
      })
      .then(() => {
        vm.submitting = false;
      });
  }

  function getAlarmItemStatusClass(status) {
    return `alarm-item-staus-${status.toLowerCase()}`;
  }

  function viewAlarmDetail(alarm) {
    rbModal.cancel();
    $state.go('monitor.alarmv2.alarm_detail', {
      uuid: alarm.uuid,
    });
  }

  function _saveAutoscalingConfig() {
    const params = {
      namespace: vm.namespace,
      service_name: vm.serviceData.uuid,
    };
    const data = {
      scaling_mode: vm.scalingMode,
      autoscaling_config: JSON.stringify(vm.autoscaling_config),
      target_num_instances: vm.autoscaling_config.minimum_num_instances,
    };
    return rbServiceDataService.scaling(params, data);
  }

  function _redirectToAlarmCreate(scaling_type) {
    const application = vm.serviceData.application_name
      ? `${vm.serviceData.application_name}.`
      : '';
    const displayName = `${application}${vm.serviceData.service_name} (${
      vm.serviceData.space_name
    })`;
    const actionService = {
      uuid: vm.serviceData.uuid,
      action_type: scaling_type,
      display_name: displayName,
    };
    rbModal.cancel();
    ngRouter.navigateByUrl(
      `alarm/alarm_create?metric=${JSON.stringify(actionService)}`,
    );
  }

  function confirm() {
    vm.scalingForm.$setSubmitted();
    if (vm.scalingForm.$invalid) {
      return;
    }
    const data = {
      scaling_mode: vm.scalingMode,
    };
    if (vm.scalingMode === 'AUTO') {
      data.autoscaling_config = vm.autoscaling_config;
    } else {
      data.target_num_instances = vm.target_num_instances;
    }
    //$log.log('autoscaling config', data);
    rbModal.hide(data);
  }

  function cancel() {
    rbModal.cancel();
  }
}
