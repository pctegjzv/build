/**
 * Created by liudong on 2016/12/8.
 */
const templateStr = require('app/components/pages/app_service/detail/automation/rb-service-automation.html');
angular.module('app.components.pages').component('rbServiceAutomation', {
  bindings: {
    uuid: '<',
    automation: '<', // service automation data (is_active, is_activatable)
  },
  controller: rbServiceAutomationController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbServiceAutomationController(
  applicationService,
  rbModal,
  rbToast,
  translateService,
) {
  const vm = this;
  vm.submitting = false;

  vm.save = save;
  vm.cancel = cancel;

  vm.$onInit = onInit();

  //////////
  function onInit() {
    vm.is_active = !!vm.automation.is_active;
  }

  function cancel() {
    rbModal.cancel();
  }

  function save() {
    if (vm.is_active === vm.automation.is_active) {
      rbModal.cancel();
      return;
    }
    vm.submitting = true;
    if (vm.is_active) {
      //  create
      applicationService
        .setAutomation({
          action: 'create',
          subject: vm.uuid,
          subject_type: 'service',
          usage_type: 'auto_deploy',
        })
        .then(() => {
          rbModal.hide(true);
          rbToast.success(translateService.get('automation_create_success'));
        })
        .catch(() => {})
        .then(() => {
          vm.submitting = true;
        });
    } else {
      // delete
      applicationService
        .setAutomation({
          action: 'delete',
          id: vm.automation.id,
        })
        .then(() => {
          rbModal.hide();
          rbToast.success(translateService.get('automation_delete_success'));
        })
        .catch(() => {})
        .then(() => {
          vm.submitting = true;
        });
    }
  }
}
