/**
 * Created by wsk on 1/12/17.
 */
import * as _ from 'lodash';
const templateStr = require('app/components/pages/app_service/detail/config-files/rb-service-detail-config-files.html');
angular.module('app.components.pages').component('rbServiceDetailConfigFiles', {
  controller: rbServiceDetailConfigFilesController,
  bindings: {
    configFiles: '<',
  },
  controllerAs: 'vm',
  template: templateStr,
});

function rbServiceDetailConfigFilesController(
  $state,
  rbConfigurationService,
  rbGlobalSetting,
  $timeout,
  translateService,
) {
  const vm = this;
  vm.$onInit = _init;
  vm.$onChanges = _onChanges;
  vm.toggleItemValue = $index => {
    vm.toggleItem[$index] = !vm.toggleItem[$index];
    if (_.get(vm.configFiles[$index], 'value.name')) {
      _updateDetail($index);
    }
  };
  vm.getConfigFileTooltip = getConfigFileTooltip;
  //////////
  async function _init() {
    vm.editorOptions = rbGlobalSetting.getEditorOptions('env', true);
    vm.toggleItem = [];
    vm.initialized = [];
    vm.cacheContents = {};
  }

  async function _updateDetail($index) {
    vm.initialized[$index] = false;
    const item = vm.configFiles[$index].value;
    if (!item) {
      return;
    }
    const { content } = await rbConfigurationService.configurationDetail(
      item.name,
    );
    vm.cacheContents[`${item.name}/${item.key}`] = _.get(
      _.find(content, itm => itm.key === item.key),
      'value',
    );
    vm.initialized[$index] = true;
    $timeout();
  }

  function _removeExpiredCaches(configFiles) {
    const cacheKeys = Object.keys(vm.cacheContents || {});
    const newKeys = configFiles
      .filter(item => item.type !== 'raw')
      .map(item => `${item.value.name}/${item.value.key}`);
    _.difference(cacheKeys, newKeys).forEach(key => {
      delete vm.cacheContents[key];
    });
  }

  function _onChanges(changeObj) {
    if (
      changeObj.configFiles.currentValue &&
      changeObj.configFiles.currentValue !== changeObj.configFiles.previousValue
    ) {
      _removeExpiredCaches(changeObj.configFiles.currentValue);
      vm.toggleItem &&
        vm.toggleItem.forEach((isOpen, idx) => {
          if (isOpen && 'raw' !== vm.configFiles[idx].type) {
            _updateDetail(idx);
          }
        });
    }
  }
  function getConfigFileTooltip(file) {
    return `${translateService.get('click_to_copy')}: ${file.path}`;
  }
}
