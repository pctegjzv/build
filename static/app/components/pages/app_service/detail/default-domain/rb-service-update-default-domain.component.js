const templateStr = require('app/components/pages/app_service/detail/default-domain/rb-service-update-default-domain.component.html');
import { INT_PATTERN } from '../../../../common/config/common-pattern';
angular
  .module('app.components.pages')
  .component('rbServiceUpdateDefaultDomain', {
    bindings: {
      serviceId: '<',
    },
    controller: rbServiceUpdateDefaultDomainController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbServiceUpdateDefaultDomainController(
  $log,
  rbToast,
  rbRegionService,
  rbLoadBalancerDataService,
  translateService,
) {
  const vm = this;
  vm.initialized = false;
  vm.submitting = false;
  vm.unbindSubmitting = false;
  vm.loadbalancerOptions = [];
  vm.listeners = [];
  vm.newListener = {};

  let regionName;
  let selectedLoadbalancerName;

  vm.$onInit = _init;
  vm.submitting = false;
  vm.onLoadbalancerSelect = onLoadbalancerSelect;
  vm.onProtocolChange = onProtocolChange;
  vm.unbind = unbind;
  vm.bind = bind;

  async function _init() {
    vm.portNumberReg = INT_PATTERN;
    vm.protocolOptions = [
      {
        name: 'HTTP',
        value: 'http',
      },
      {
        name: 'TCP',
        value: 'tcp',
      },
    ];
    vm.newListener = {
      service_id: vm.serviceId,
      protocol: 'http',
      container_port: null,
      listener_port: null,
      loadbalancer_id: '',
    };
    const region = await rbRegionService.getCurrentRegion();
    regionName = region.name;
    await Promise.all([getListeners(), getLoadbalancerOptions()]);
    vm.initialized = true;
  }

  async function unbind(listener) {
    if (vm.unbindSubmitting) {
      return;
    }
    const data = {
      action: 'unbind',
      listeners: [
        {
          service_id: vm.serviceId,
          protocol: listener.protocol,
          listener_port: parseInt(listener.listener_port, 10),
          container_port: parseInt(listener.container_port, 10),
        },
      ],
    };
    vm.unbindSubmitting = true;
    try {
      await rbLoadBalancerDataService.bindService(
        listener.loadbalancer_id,
        data,
      );
      rbToast.messageSuccess({
        content: translateService.get('service_unbind_listener_success'),
      });
      vm.listeners = vm.listeners.filter(item => {
        return !(
          item.loadbalancer_id === listener.loadbalancer_id &&
          item.service_id === listener.service_id &&
          item.container_port === listener.container_port &&
          item.listener_port === listener.listener_port
        );
      });
    } catch (err) {
      // placeholder
      $log.log(err);
    }
    vm.unbindSubmitting = false;
  }

  async function bind() {
    vm.form.$setSubmitted();
    if (vm.form.$invalid) {
      return;
    }
    await bindListener();
  }

  function onLoadbalancerSelect(option) {
    if (option) {
      selectedLoadbalancerName = option.name;
      vm.newListener.loadbalancer_id = option.uuid;
    }
  }

  function onProtocolChange(option) {
    if (option) {
      vm.newListener.protocol = option.value;
    }
  }

  function resetNewListener() {
    vm.newListener.container_port = '';
    vm.newListener.listener_port = '';
    vm.form.$setPristine();
  }

  async function getListeners() {
    try {
      const res = await rbLoadBalancerDataService
        .getLoadBalancers({
          region_name: regionName,
          service_id: vm.serviceId,
          frontend: true,
        })
        .then(({ result }) => result);
      res.forEach(lb => {
        if (lb.listeners && lb.listeners.length) {
          lb.listeners.forEach(listener => {
            // 虽然请求里指定了service_id，但默认服务的listener仍然会返回，API暂时没有处理这种情况
            if (listener.service_id === vm.serviceId) {
              vm.listeners.push({
                ...listener,
                loadbalancer_name: lb.name,
                loadbalancer_id: lb.load_balancer_id,
              });
            }
          });
        }
      });
    } catch (err) {
      vm.listeners = [];
    }
  }

  async function getLoadbalancerOptions() {
    try {
      const res = await rbLoadBalancerDataService
        .getLoadBalancers({
          region_name: regionName,
        })
        .then(({ result }) => result);
      vm.loadbalancerOptions = res.map(item => {
        return {
          name: item.name,
          uuid: item.load_balancer_id,
        };
      });
    } catch (err) {
      vm.loadbalancerOptions = [];
    }
  }

  async function bindListener() {
    const data = {
      action: 'bind',
      listeners: [
        {
          service_id: vm.serviceId,
          protocol: vm.newListener.protocol,
          listener_port: parseInt(vm.newListener.listener_port, 10),
          container_port: parseInt(vm.newListener.container_port, 10),
        },
      ],
    };
    vm.submitting = true;
    try {
      await rbLoadBalancerDataService.bindService(
        vm.newListener.loadbalancer_id,
        data,
      );
      rbToast.messageSuccess({
        content: translateService.get('service_bind_listener_success'),
      });
      vm.listeners.push({
        loadbalancer_name: selectedLoadbalancerName,
        loadbalancer_id: vm.newListener.loadbalancer_id,
        service_id: vm.serviceId,
        protocol: vm.newListener.protocol,
        listener_port: vm.newListener.listener_port,
        container_port: vm.newListener.container_port,
      });
      resetNewListener();
    } catch (err) {
      // placeholder
      $log.log(err);
    }
    vm.submitting = false;
  }
}
