const templateStr = require('app/components/pages/app_service/detail/dialog/rb-service-update-strategy-dialog.html');
const {
  SHARED_DRIVER_NAME,
  SERVICE_NETWORK_MODES,
  SERVICE_DEPLOY_MODEL,
} = require('../../rb-service.constant');
angular
  .module('app.components.pages')
  .component('rbServiceUpdateStrategyDialog', {
    controller: rbServiceUpdateStrategyDialogController,
    controllerAs: 'vm',
    bindings: {
      serviceData: '<',
      region: '<',
    },
    template: templateStr,
  });

function rbServiceUpdateStrategyDialogController(
  rbServiceDataService,
  rbModal,
) {
  const vm = this;
  vm.update_strategy = {
    max_surge: '',
    max_unavailable: '',
  };

  vm.$onInit = _init;
  vm.save = save;
  vm.cancel = cancel;

  vm.validateUpdateStrategy = validateUpdateStrategy;
  vm.validateUnavailableValue = validateUnavailableValue;
  vm.onlySupportMaxUnavaible = onlySupportMaxUnavaible;
  vm.zeroCheckInDaemonSet = zeroCheckInDaemonSet;

  function _init() {
    vm.update_strategy = {
      max_surge: '',
      max_surge_disabled: false,
      max_unavailable: '',
      max_unavailable_disabled: false,
    };
    if (vm.serviceData) {
      vm.update_strategy.max_surge = _.get(
        vm.serviceData,
        'update_strategy.max_surge',
        '',
      ).toString();
      vm.update_strategy.max_unavailable = _.get(
        vm.serviceData,
        'update_strategy.max_unavailable',
        '',
      ).toString();
    }
    if (vm.serviceData.network_mode === SERVICE_NETWORK_MODES.MACVLAN) {
      vm.update_strategy.max_surge = '0%';
      vm.update_strategy.max_surge_disabled = true;
    }
    if (vm.serviceData.volumes && vm.serviceData.volumes.length) {
      const hasUnsharedVolume = vm.serviceData.volumes.some(vo => {
        return !SHARED_DRIVER_NAME.includes(vo.driver_name);
      });
      if (hasUnsharedVolume) {
        vm.update_strategy.max_surge = '0%';
        vm.update_strategy.max_unavailable = '100%';
        vm.update_strategy.max_surge_disabled = true;
        vm.update_strategy.max_unavailable_disabled = true;
      }
    }
  }

  function save() {
    vm.strategyForm.$setSubmitted();
    if (vm.strategyForm.$invalid) {
      return;
    }
    if (vm.update_strategy.max_surge && vm.update_strategy.max_unavailable) {
      Object.keys(vm.update_strategy).forEach(k => {
        if (k === 'max_surge' || k === 'max_unavailable') {
          vm.update_strategy[k] = vm.update_strategy[k].toString();
          if (!vm.update_strategy[k].endsWith('%')) {
            vm.update_strategy[k] = parseInt(vm.update_strategy[k]);
          }
        }
      });
    }
    const data = {
      max_surge: vm.update_strategy.max_surge,
      max_unavailable: vm.update_strategy.max_unavailable,
    };
    vm.loading = true;
    const params = {
      application: _.get(vm.serviceData, 'application_name'),
      service_name: _.get(vm.serviceData, 'service_name'),
    };
    rbServiceDataService
      .scaling(params, { update_strategy: data })
      .catch(() => {})
      .then(() => {
        vm.loading = false;
        cancel();
      });
  }

  function cancel() {
    rbModal.hide();
  }

  function validateUpdateStrategy(value, type) {
    return type
      ? parseInt(vm.update_strategy.max_surge) || parseInt(value)
      : parseInt(value) || parseInt(vm.update_strategy.max_unavailable);
  }

  function validateUnavailableValue(value) {
    return !(value.endsWith('%') && parseInt(value) > 100);
  }

  function onlySupportMaxUnavaible() {
    return (
      vm.serviceData.pod_controller === SERVICE_DEPLOY_MODEL.DAEMONSET &&
      _.get(vm.region, 'features.kubernetes.version', '') >= '1.6'
    );
  }

  function zeroCheckInDaemonSet(value) {
    return !(
      vm.serviceData.pod_controller === SERVICE_DEPLOY_MODEL.DAEMONSET &&
      _.get(vm.region, 'features.kubernetes.version', '') >= '1.6' &&
      value === '0'
    );
  }
}
