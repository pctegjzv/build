/**
 * Created by liudong on 16/9/23.
 */
const { SERVICE_NETWORK_MODES } = require('../../rb-service.constant');
const templateStr = require('app/components/pages/app_service/detail/endpoints/rb-service-detail-endpoints.html');
angular.module('app.components.pages').component('rbServiceDetailEndpoints', {
  bindings: {
    serviceData: '<',
    regionFeatures: '<',
  },
  controller: rbServiceDetailEndpointsController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbServiceDetailEndpointsController(
  applicationService,
  $state,
  rbLoadBalancerDataService,
) {
  const vm = this;
  vm.albEnabled = vm.regionFeatures.includes('alb');
  vm.loadbalancers = []; // for alb
  vm.healthcheckStates = {};

  vm.$onInit = _init;
  vm.$onChanges = _onChanges;
  vm.getEndpointDisplay = getEndpointDisplay;
  vm.getLoadbalancerHref = getLoadbalancerHref;
  //////////

  function _init() {
    vm.lbInitialized = false;
  }

  function _onChanges(changesObj) {
    // when service data is ready , clone service data to local variable `vm.service`
    if (changesObj.serviceData && changesObj.serviceData.currentValue) {
      if (!vm.albEnabled) {
        vm.service = vm.serviceData;
        if (
          _.isEmpty(vm.service.instance_ports) &&
          _.isEmpty(vm.service.raw_container_ports) &&
          _.isEmpty(vm.service.load_balancers)
        ) {
          vm.endpointsEmpty = true;
        }
        getHealthcheckState();
      } else {
        getServiceLoadbalancers();
      }
    }
  }

  function getHealthcheckState() {
    // host mode or raw container has no healthcheck
    if (
      vm.service.raw_container_ports.length > 0 ||
      vm.service.network_mode === SERVICE_NETWORK_MODES.HOST
    ) {
      return;
    }
    const params = {
      namespace: vm.service.namespace,
    };
    if (vm.service.application) {
      params.application = vm.service.application;
      params.service_name = vm.service.service_name;
    } else {
      params.service_name = vm.service.uuid;
    }
    applicationService
      .loadBalancerHealthInfo(params)
      .then(data => {
        _updateHealthcheckDisplay(data.result);
      })
      .catch(() => {
        _updateHealthcheckDisplay([]);
      });
  }

  function _updateHealthcheckDisplay(data) {
    // convert lb healthcheck info to key-value map
    const healthcheckStates = {};
    _.forEach(data, item => {
      const listeners = {};
      _.forEach(item.listeners, listener => {
        let count = 0;
        _.forEach(listener.instances, instance => {
          if (instance.is_healthy) {
            count++;
          }
        });
        listeners[listener.port] = {
          text: count + '/' + listener.instances.length,
          health_status: count === listener.instances.length,
        };
      });
      healthcheckStates[item.id] = listeners;
    });
    // not every listener has health info
    _.forEach(vm.service.load_balancers, lb => {
      if (!(lb.lb_id in healthcheckStates)) {
        healthcheckStates[lb.lb_id] = {};
      }
      _.forEach(lb.listeners, listener => {
        if (!(listener.lb_port in healthcheckStates[lb.lb_id])) {
          healthcheckStates[lb.lb_id][listener.lb_port] = null;
        }
      });
    });
    // then read healthcheck state for every lb listener, listener with unique lb_port
    vm.healthcheckStates = healthcheckStates;
  }

  function getServiceLoadbalancers() {
    const params = {
      region_name: vm.serviceData.region.name,
      region_id: vm.serviceData.region_id,
      service_id: vm.serviceData.uuid,
      detail: true,
    };

    rbLoadBalancerDataService
      .getLoadBalancers(params)
      .then(data => {
        if (data.result && data.result.length) {
          vm.loadbalancers = data.result;
        }
        vm.loadbalancers.sort((a, b) => {
          return a.name > b.name;
        });
        vm.loadbalancers.forEach(item => {
          if (item.listeners) {
            item.listeners.forEach(listener => {
              listener.count_total = 0;
              // status: normal abnormal unknown
              if (listener.instances) {
                listener.count_total = listener.instances.length;
                listener.count_normal = listener.instances.filter(instance => {
                  return instance.status === 'normal';
                }).length;
                if (listener.count_normal === listener.count_total) {
                  listener.healthy = true;
                } else {
                  listener.healthy = false;
                }
              } else {
                listener.count_normal = 0;
              }
            });
          }
        });
        // if has no loadbalancer/ports, service has no endpoints
        if (!vm.loadbalancers.length && !vm.serviceData.ports.length) {
          vm.endpointsEmpty = true;
        }
        if (!vm.lbInitialized) {
          vm.lbInitialized = true;
        }
      })
      .catch(() => {
        if (!vm.lbInitialized) {
          vm.endpointsEmpty = true;
        }
      });
  }

  function getEndpointDisplay(listener, rule, lb) {
    let endpoint = '';
    if (rule.domain) {
      if (
        (listener.protocol === 'http' && listener.listener_port === 80) ||
        (listener.protocol === 'https' && listener.listener_port === 443)
      ) {
        endpoint = `${listener.protocol}://${rule.domain + rule.url || ''}`;
      } else {
        endpoint = `${listener.protocol}://${
          rule.domain
        }:${listener.listener_port + rule.url || ''}`;
      }
    } else {
      if (
        (listener.protocol === 'http' && listener.listener_port === 80) ||
        (listener.protocol === 'https' && listener.listener_port === 443)
      ) {
        endpoint = `${listener.protocol}://${lb.address + rule.url}`;
      } else {
        endpoint = `${listener.protocol}://${lb.address +
          ':' +
          listener.listener_port +
          rule.url}`;
      }
    }
    return endpoint;
  }

  function getLoadbalancerHref(lb) {
    return `/console/load_balancer/detail/${lb.name}`;
  }
}
