/**
 * Created by liudong on 2017/1/3.
 */
const templateStr = require('app/components/pages/app_service/detail/exec/rb-service-exec-dialog.html');
angular.module('app.components.pages').component('rbServiceExecDialog', {
  controller: rbServiceExecDialogController,
  controllerAs: 'vm',
  bindings: {
    serviceData: '<',
  },
  template: templateStr,
});

function rbServiceExecDialogController(
  $scope,
  rbModal,
  rbAccountService,
  rbProjectService,
  rbRegionService,
  rbServiceDataService,
  WEBLABS,
  ENVIRONMENTS,
) {
  const vm = this;
  vm.$onInit = _init;

  //////////
  function _init() {
    vm.projectEnabled = WEBLABS['projects_enabled'];

    const projectStr =
      vm.projectEnabled && rbProjectService.get()
        ? ` project/${rbProjectService.get()}`
        : '';
    vm.username = rbAccountService.getCurrentUsername();
    vm.namespace = rbAccountService.getCurrentNamespace();
    vm.lycanUrl = ENVIRONMENTS['lycan_url'];

    vm.projectName = rbProjectService.get();
    vm.instanceName = '';
    vm.command = '/bin/sh';
    vm.user_name = !vm.username
      ? vm.namespace
      : `${vm.namespace}/${vm.username}`;
    vm.copySuccess = false;

    // new k8s service
    if (!vm.serviceData.uuid) {
      Promise.all([
        rbRegionService.getRegionByName(vm.serviceData.cluster.name),
        rbServiceDataService.getNewK8sServiceInstances(
          vm.serviceData.resource.uuid,
        ),
      ]).then(([region, instances]) => {
        vm.serviceExecEndpoint =
          region.features.service && region.features.service.exec.endpoint;
        vm.serviceInstances = instances.map(
          ({ metadata: { name, namespace, uid }, spec: { containers } }) => ({
            uuid: uid,
            name,
            namespace,
            containers,
          }),
        );
      });
    } else {
      vm.serviceExecEndpoint = vm.serviceData.exec_endpoint;
    }

    $scope.$watchGroup(
      [() => vm.instanceName, () => vm.command],
      (newValues, oldValues) => {
        if (
          newValues.join('') !== oldValues.join('') &&
          newValues.join('') !== ''
        ) {
          if (vm.copySuccess) {
            vm.copySuccess = false;
          }

          if (!vm.serviceData.uuid) {
            vm.execCommand = `ssh -p 4022 -t ${vm.user_name}@${
              vm.serviceExecEndpoint
            } ${vm.namespace}/${vm.instanceName} ${vm.command}`;
          } else if (vm.serviceData.application) {
            vm.execCommand = `ssh -p 4022 -t ${vm.user_name}@${
              vm.serviceExecEndpoint
            } ${vm.namespace}/${vm.serviceData.application}/${
              vm.instanceName
            }${projectStr} ${vm.command}`;
          } else {
            vm.execCommand = `ssh -p 4022 -t ${vm.user_name}@${
              vm.serviceExecEndpoint
            } ${vm.namespace}/${vm.instanceName}${projectStr} ${vm.command}`;
          }
        }
      },
    );
  }

  vm.onInstanceChange = option => {
    const [, instance] = option.instance_name.split('.');
    vm.instanceName = `${vm.serviceData.uuid}.${instance}`;
  };

  vm.onInstanceGroupChange = group => {
    vm.serviceContainerGroup = group.name;
    vm.serviceContainers = group.containers;
  };

  vm.onContainerChange = container => {
    vm.instanceName = `${vm.serviceData.resource.uuid}/${
      vm.serviceContainerGroup
    }/${container.name}`;
  };

  vm.confirm = () => {
    vm.execForm.$setSubmitted();
    if (vm.execForm.$invalid) {
      return;
    }
    angular
      .element('.rb-exec-form')
      .attr('action', vm.lycanUrl)
      .submit();
    rbModal.hide();
  };

  vm.cancel = () => {
    rbModal.cancel();
  };

  vm.copyText = () => {
    angular.element('#rb-service-exec-full').select();
    document.execCommand('Copy');
    $scope.$evalAsync(() => {
      vm.copySuccess = true;
    });
  };
}
