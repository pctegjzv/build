import _ from 'lodash';
const templateStr = require('app/components/pages/app_service/detail/kubernetes-services/rb-service-k8s-service-update-dialog.html');
angular
  .module('app.components.pages')
  .component('rbServiceKubernetesServiceUpdateDialog', {
    bindings: {
      services: '<',
      serviceName: '<',
      containerPorts: '<',
      applicationName: '<',
    },
    controller: rbServiceKubernetesServiceUpdateDialogController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbServiceKubernetesServiceUpdateDialogController(
  rbModal,
  rbServiceDataService,
) {
  const vm = this;
  vm.$onInit = _init;
  vm.submitting = false;

  function _init() {
    vm.services.forEach(service => {
      service.suffixName = service.name.replace(
        `${vm.serviceName}-${service.type === 'Headless' ? 'hl' : 'np'}-`,
        '',
      );
    });
  }

  vm.cancel = () => {
    rbModal.hide();
  };
  vm.save = () => {
    vm.form.$setSubmitted();
    if (!vm.form.$valid) {
      return;
    }
    vm.submitting = true;
    const services = _.cloneDeep(vm.services);
    const data = services.map(r => {
      delete r.server_data;
      delete r.suffixName;
      if (r.type === 'Headless') {
        delete r.container_port;
        delete r.node_port;
      }
      if (!r.node_port) {
        delete r.node_port;
      }
      return r;
    });
    rbServiceDataService
      .scaling(
        {
          application: vm.applicationName,
          service_name: vm.serviceName,
        },
        { kube_config: { services: data } },
      )
      .catch(() => {})
      .then(() => {
        vm.submitting = false;
        vm.cancel();
      });
  };
}
