/**
 * Created by liudong on 2016/11/8.
 */
const moment = require('moment');
const templateStr = require('app/components/pages/app_service/detail/log/rb-service-log-polling.html');
angular.module('app.components.pages').component('rbServiceLogPolling', {
  controller: rbServiceLogPollingController,
  controllerAs: 'vm',
  bindings: {
    service: '<', // service data
    source: '<', // log source
  },
  template: templateStr,
});

function rbServiceLogPollingController(
  $element,
  rbHttp,
  $timeout,
  $filter,
  ENVIRONMENTS,
  rbServiceUtilities,
) {
  const vm = this;
  const interval = 10000;
  let timer;
  let _source;
  let componentDestroyed = false;
  vm.loading = false;

  vm.$onChanges = _onChanges;
  vm.$onInit = _init;
  vm.$onDestroy = _onDestroy;
  vm.clearLogs = clearLogs;
  ///////////

  function _init() {}

  function _onChanges({ source }) {
    if (source && source.currentValue && _source !== source.currentValue) {
      _source = source.currentValue;
      $timeout.cancel(timer);
      vm.lastLogTime = 0;
      startPolling();
    }
  }

  function _onDestroy() {
    componentDestroyed = true;
    $timeout.cancel(timer);
  }

  function startPolling() {
    // clear log list
    $element.find('.rb-log-list').html('');
    _polling();
  }

  function _nextTick() {
    if (componentDestroyed) {
      return;
    }
    timer = $timeout(_polling, interval);
  }

  function _renderLogs(logs = []) {
    if (!logs.length) {
      return;
    }
    let scroll = false;
    const $ul = $element.find('.rb-log-list');
    if (!vm.lastLogTime) {
      // this is the first time render, should scroll to bottom automaticlly
      scroll = true;
    } else {
      // else if the scrollbar is not at the bottom, we should keep it
      scroll = $ul.innerHeight() + $ul.scrollTop() + 5 >= $ul[0].scrollHeight;
    }
    const fragment = _createLogsFragment(logs);
    $ul.append(fragment);
    $timeout(() => {
      if (scroll) {
        $ul.scrollTop($ul[0].scrollHeight);
      }
    }, 100);
  }

  function _createLogsFragment(logs) {
    // instance_id, instance_id_full, message, time
    const fragment = document.createDocumentFragment();
    logs.forEach(log => {
      const li = document.createElement('li');
      li.className = 'rb-log-item';
      li.innerHTML = `<span>${$filter('formatDate')(
        log.time * 1000,
        true,
      )}</span>
        <span>${log.instance_id}</span>
        <span class="log-message">${_.escape(log.message)}</span>`;
      fragment.appendChild(li);
    });
    return fragment;
  }

  function _getParams() {
    const now = moment().valueOf();
    let params = {
      log_source: vm.source,
      end_time: (now / 1000).toFixed(3),
    };
    const serviceParams = rbServiceUtilities.getServiceLogParams(vm.service);
    params = _.merge(params, serviceParams);
    // make sure retrived logs number be a constant number, dispite of current instance number
    const instanceNumber = vm.service.current_num_instances || 1;
    if (vm.lastLogTime) {
      params['start_time'] = ((vm.lastLogTime * 1000000 + 1) / 1000000).toFixed(
        6,
      ); // convert to ms
      params['limit'] = Math.ceil(1000 / instanceNumber); // real time log limit to 1000
    } else {
      params['start_time'] = ((now - 3600 * 24 * 7 * 1000) / 1000).toFixed(3);
      params['limit'] = Math.ceil(
        ENVIRONMENTS['service_log_polling_maximum'] / instanceNumber,
      ); // history log controlled by environment variable
    }
    return params;
  }

  function _polling() {
    const params = _getParams();
    vm.loading = true;
    rbHttp
      .sendRequest({
        url: `/ajax/services/${params.namespace}/${params.service_name}/logs`,
        params: params,
      })
      .then(data => {
        const logs = data.result;
        if (logs && logs.length) {
          vm.lastLogTime = logs[logs.length - 1].time;
          _renderLogs(logs);
        }
      })
      .catch(() => {})
      .then(() => {
        vm.loading = false;
        _nextTick();
      });
  }

  function clearLogs() {
    $element.find('.rb-log-list').html('');
  }
}
