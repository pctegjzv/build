/**
 * Created by liudong on 2016/11/8.
 */
const _ = require('lodash');
const moment = require('moment');
const highcharts = require('highcharts');
const templateStr = require('app/components/pages/app_service/detail/log/rb-service-log-search.html');
angular.module('app.components.pages').component('rbServiceLogSearch', {
  controller: rbServiceLogSearchController,
  controllerAs: 'vm',
  bindings: {
    service: '<',
    source: '<',
  },
  template: templateStr,
});

function rbServiceLogSearchController(
  $element,
  applicationService,
  translateService,
  rbToast,
  rbSafeApply,
  rbServiceUtilities,
) {
  const vm = this;
  let bucketsInterval = 0; // interval of two buckets
  let logQueryParams = {};
  let logChartConfig = null; // chart config for log events

  vm.onTimestampChanged = onTimestampChanged;
  vm.getLogs = getLogs;
  vm.search = search;
  vm.logInsight = logInsight;
  vm.timestampOption = 'last_day';

  vm.$onInit = _init;
  //////////
  function _init() {
    const current_time = moment();
    const start_time = current_time
      .clone()
      .startOf('day')
      .subtract(6, 'days');
    vm.loading = false;
    vm.loadingLogs = false;
    vm.logs = [];
    vm.logCache = {};
    vm.logCollapseMap = {};
    vm.queryString = ''; // query string for search
    vm.hasQueryString = false; // log message with highlighted query string should be treated with ng-bind-html
    vm.allowCustomTimeRange = false; // flag to indicate if custom time range is selected
    vm.timestampOptions = [
      {
        type: 'custom_time_range',
        offset: 7 * 24 * 3600 * 1000,
      },
      {
        type: 'last_10_minites',
        offset: 10 * 60 * 1000,
      },
      {
        type: 'last_30_minites',
        offset: 30 * 60 * 1000,
      },
      {
        type: 'last_hour',
        offset: 60 * 60 * 1000,
      },
      {
        type: 'last_3_hour',
        offset: 3 * 3600 * 1000,
      },
      {
        type: 'last_6_hour',
        offset: 6 * 3600 * 1000,
      },
      {
        type: 'last_12_hour',
        offset: 12 * 3600 * 1000,
      },
      {
        type: 'last_day',
        offset: 24 * 3600 * 1000,
      },
      {
        type: 'last_2_days',
        offset: 2 * 24 * 3600 * 1000,
      },
      {
        type: 'last_3_days',
        offset: 3 * 24 * 3600 * 1000,
      },
      {
        type: 'last_5_days',
        offset: 5 * 24 * 3600 * 1000,
      },
      {
        type: 'last_7_days',
        offset: 7 * 24 * 3600 * 1000,
      },
    ];
    vm.timestampOptions.map(option => {
      option['name'] = translateService.get(option.type);
    });
    vm.queryDates = {
      from: start_time.valueOf(),
      to: current_time.valueOf(),
    };
    vm.dateTimeOptions = {
      maxDate: current_time.clone().endOf('day'),
      minDate: start_time,
    };
    vm.pagination = {
      total_items: 0,
      size: 50,
    };
  }

  function onTimestampChanged(option) {
    vm.timestampOption = option.type;
    if (vm.timestampOption !== 'custom_time_range') {
      vm.allowCustomTimeRange = false;
      _resetTimeRange(vm.timestampOption);
    } else {
      vm.allowCustomTimeRange = true;
    }
  }

  function logInsight(log, mode = '') {
    const params = {
      log_time: log.time,
      log_source: vm.source,
      service_name: vm.service.service_name,
      instance_id: log.instance_id_full || log.instance_id,
    };
    if (vm.service.application) {
      params['application'] = vm.service.application;
    }
    if (mode === '') {
      vm.logCollapseMap[log.time] = !vm.logCollapseMap[log.time];
      if (vm.logCache[log.time]) {
        return;
      }
      vm.loadingLogs = true;
      applicationService
        .applicationLogContext(params)
        .then(data => {
          if (data.logs && data.logs.length) {
            const logs = data.logs;
            const index = _.findIndex(logs, item => {
              return item.time === log.time;
            });
            vm.logCache[log.time] = {
              before: _.slice(logs, 0, index),
              after: _.slice(logs, index + 1),
            };
          }
        })
        .catch(() => {})
        .then(() => {
          vm.loadingLogs = false;
        });
    } else if (mode === 'before') {
      params['direction'] = 'before';
      // start time is time of first log
      params['log_time'] = vm.logCache[log.time]['before'][0]['time']; // eg: 1508219609.975000
      vm.loadingLogs = true;
      applicationService
        .applicationLogContext(params)
        .then(data => {
          if (data.logs && data.logs.length) {
            // insert before
            vm.logCache[log.time]['before'] = data.logs.concat(
              vm.logCache[log.time]['before'],
              data.logs,
            );
          } else {
            rbToast.warning(translateService.get('no_more_logs'));
          }
        })
        .catch(() => {})
        .then(() => {
          vm.loadingLogs = false;
        });
    } else if (mode === 'after') {
      params['direction'] = 'later';
      // start time is time of last log
      const len = vm.logCache[log.time]['after'].length;
      params['log_time'] = vm.logCache[log.time]['after'][len - 1]['time']; // eg: 1508219609.975000
      vm.loadingLogs = true;
      applicationService
        .applicationLogContext(params)
        .then(data => {
          if (!_.isEmpty(data.logs)) {
            // insert after
            vm.logCache[log.time]['after'] = vm.logCache[log.time][
              'after'
            ].concat(data.logs);
          } else {
            rbToast.warning(translateService.get('no_more_logs'));
          }
        })
        .catch(() => {})
        .then(() => {
          vm.loadingLogs = false;
        });
    }
  }

  function search(fromTime = 0) {
    if (vm.timestampOption !== 'custom_time_range') {
      _resetTimeRange(vm.timestampOption);
    }
    if (vm.queryDates.from >= vm.queryDates.to) {
      rbToast.warning(translateService.get('log_query_timerange_warning'));
      return false;
    }
    const queryString = _.trim(vm.queryString) || '';
    // query log from graph
    if (fromTime) {
      vm.queryDates.from = fromTime;
      vm.queryDates.to = fromTime + bucketsInterval;
    }
    vm.pagination.total_items = 0;
    const params = rbServiceUtilities.getServiceLogParams(vm.service);
    logQueryParams = _.merge(params, {
      query_string: queryString,
      start_time: (vm.queryDates.from / 1000).toFixed(3),
      end_time: (vm.queryDates.to / 1000).toFixed(3),
      log_source: vm.source,
    });
    vm.loading = true;
    Promise.all([getLogs(), getLogsEvents()])
      .catch(() => {})
      .then(() => {
        vm.loading = false;
        rbSafeApply();
      });
  }

  function getLogsEvents() {
    return applicationService
      .applicationLogsAggregations(logQueryParams)
      .then(data => {
        if (!data.buckets || data.buckets.length === 0) {
          return;
        }
        const buckets = data.buckets;
        let totalCount = 0;
        _.forEach(data.buckets, bucket => {
          totalCount += bucket.count;
          const _date = new Date(bucket.time * 1000);
          bucket.time = _date.getTime();
        });
        if (buckets.length > 2) {
          bucketsInterval = buckets[1].time - buckets[0].time;
        }
        const timestamps = data.buckets.map(bucket => {
          return bucket.time;
        });
        const events = data.buckets.map(bucket => {
          return bucket.count;
        });
        _renderLogEventsChart({ totalCount, timestamps, events });
      });
  }

  function getLogs(page = 1, size = 50) {
    vm.loadingLogs = true;
    logQueryParams.pageno = page;
    logQueryParams.size = size;
    return applicationService
      .applicationLogsQuery(logQueryParams)
      .then(data => {
        if (!data.logs) {
          return;
        }
        vm.pagination.total_items = data.total_items;
        vm.logCache = {};
        vm.logs = data.logs;
      })
      .catch(() => {})
      .then(() => {
        vm.loadingLogs = false;
        const queryString = _.trim(vm.queryString) || '';
        if (!_.isEmpty(queryString)) {
          vm.hasQueryString = true;
        } else {
          vm.hasQueryString = false;
        }
      });
  }

  function _resetTimeRange(type) {
    let offset = vm.timestampOptions[0]['offset'];
    vm.queryDates.to = new Date().getTime();
    _.forEach(vm.timestampOptions, option => {
      if (option.type === type) {
        offset = option.offset;
        return false;
      }
    });
    if (offset > 5 * 24 * 3600 * 1000) {
      const _end = new Date(vm.queryDates.to);
      vm.queryDates.from =
        new Date(
          _end.getFullYear(),
          _end.getMonth(),
          _end.getDate(),
        ).getTime() -
        offset +
        24 * 3600 * 1000;
    } else {
      vm.queryDates.from = vm.queryDates.to - offset;
    }
  }

  function _renderLogEventsChart({
    totalCount = 0,
    timestamps = [],
    events = [],
  }) {
    logChartConfig = {
      credits: false,
      chart: {
        type: 'column',
      },
      title: {
        text: totalCount + ' Logs',
        align: 'left',
      },
      legend: {
        enabled: false,
      },
      xAxis: {
        categories: timestamps,
        labels: {
          step: 4,
          formatter: function thisXAxisFormatter() {
            return moment(this.value).format('MM-DD HH:mm:ss');
          },
        },
      },
      yAxis: {
        allowDecimals: false,
        title: {
          text: '',
        },
      },
      plotOptions: {
        series: {
          cursor: 'pointer',
          point: {
            events: {
              click: e => {
                vm.timestampOption = 'custom_time_range';
                vm.allowCustomTimeRange = true;
                vm.search(e.point.category);
              },
            },
          },
          tooltip: {
            headerFormat:
              '<span style="font-size: 12px">{point.y} Logs</span><br/>',
            pointFormatter: function tooltipFormatter() {
              const str = moment(this.category).format('YYYY-MM-DD HH:mm:ss');
              return '<span style="font-size: 10px">' + str + '</span>';
            },
          },
        },
      },
      series: [
        {
          name: 'Logs',
          data: events,
        },
      ],
    };
    highcharts.chart(
      $element.find('.rb-log-search-events > section')[0],
      logChartConfig,
    );
  }
}
