const _ = require('lodash');
const { SERVICE_LOG_MODE } = require('../../rb-service.constant');
const templateStr = require('app/components/pages/app_service/detail/log/rb-service-log.html');
angular.module('app.components.pages').component('rbServiceLog', {
  controller: rbServiceLogController,
  controllerAs: 'vm',
  bindings: {
    service: '<',
  },
  template: templateStr,
});

function rbServiceLogController(
  $scope,
  applicationService,
  rbServiceUtilities,
  translateService,
) {
  const vm = this;

  vm.onLogSourceChanged = onLogSourceChanged;
  vm.onLogModeChange = onLogModeChange;
  vm.$onInit = _init;
  //////////
  function _init() {
    vm.SERVICE_LOG_MODE = SERVICE_LOG_MODE;
    vm.logMode = SERVICE_LOG_MODE.POLLING;
    vm.logModes = [
      {
        key: translateService.get('auto_polling'),
        value: SERVICE_LOG_MODE.POLLING,
      },
      {
        key: translateService.get('search'),
        value: SERVICE_LOG_MODE.SEARCH,
      },
    ];
    vm.logSource = 'alauda_stdout';
    vm.logSourceList = [];
    vm.logSourceLoading = false;
  }

  function onLogSourceChanged(option) {
    vm.logSource = option.value;
  }

  function onLogModeChange(option) {
    vm.logMode = option.value;
    loadQuerySource();
  }

  function loadQuerySource() {
    vm.logSourceList = [];
    const params = rbServiceUtilities.getServiceLogParams(vm.service);
    vm.logSourceLoading = true;
    applicationService
      .applicationLogSource(params)
      .then(data => {
        vm.logSourceList = [];
        const sources = data.sources.filter(source => {
          return source !== 'stdout';
        });
        _.each(sources, source => {
          if (source === 'alauda_stdout') {
            vm.logSourceList.push({
              name: translateService.get('alauda_stdout'),
              value: source,
            });
          } else if (source === 'alauda_stderr') {
            vm.logSourceList.push({
              name: translateService.get('alauda_stderr'),
              value: source,
            });
          } else {
            vm.logSourceList.push({
              name: source,
              value: source,
            });
          }
        });
      })
      .catch(() => {})
      .then(() => {
        vm.logSourceLoading = false;
      });
  }
}
