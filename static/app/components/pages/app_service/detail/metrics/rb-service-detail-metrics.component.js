/**
 * Created by liudong on 16/9/24.
 */
const moment = require('moment');
const templateStr = require('app/components/pages/app_service/detail/metrics/rb-service-detail-metrics.html');
angular.module('app.components.pages').component('rbServiceDetailMetrics', {
  bindings: {
    serviceData: '<',
  },
  controller: rbServiceDetailMetricsController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbServiceDetailMetricsController(
  translateService,
  rbMonitorDataService,
  rbMonitorUtilities,
  rbToast,
  rbSafeApply,
) {
  const vm = this;
  let serviceParams = {};
  let currentPeriod;
  vm.$onInit = _init;

  vm.selectTimeRange = selectTimeRange;
  vm.selectMetricType = selectMetricType;
  vm.selectMetricAggregator = selectMetricAggregator;
  vm.loadMetrics = loadMetrics;
  vm.isCustomTimeRange = isCustomTimeRange;

  //////////
  function _init() {
    vm.highChartsOptions = {
      legend: {
        align: 'right',
        layout: 'vertical',
      },
    };
    vm.loading = false;
    vm.timeRange = {};
    vm.periodOptions = rbMonitorUtilities.getPeriodOptions();
    vm.metricTypes = [
      { name: translateService.get('instance'), value: 'INSTANCE' },
      { name: translateService.get('service'), value: 'SERVICE' },
    ];
    // 固定的几个service监控指标
    vm.queryTypes = [
      {
        name: translateService.get('cpu_utilization'),
        value: 'service.cpu.utilization',
      },
      {
        name: translateService.get('memory_utilization'),
        value: 'service.mem.utilization',
      },
      {
        name: translateService.get('sent_bytes'),
        value: 'service.net.bytes_sent',
      },
      {
        name: translateService.get('received_bytes'),
        value: 'service.net.bytes_rcvd',
      },
    ];
    vm.hightchartsOptions = {
      chart: {
        height: 480,
        type: 'line',
        spacing: [20, 20, 10, 10],
        ignoreHiddenSeries: false,
      },
      // legend line is 16 height
      legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'bottom',
        y: 10,
        floating: true,
      },
    };
    vm.dateTimeOptions = rbMonitorUtilities.getDateTimeOptions();
    vm.pointTooltipFn = rbMonitorUtilities.getPointFormat;
    vm.metricAggregatorOptions = rbMonitorUtilities.getMonitorAggregatorOptions();

    initMetricParams();
    loadMetrics();
  }

  function loadMetrics() {
    if (vm.loading) {
      return;
    }
    if (!isCustomTimeRange()) {
      const end = moment().valueOf();
      vm.timeRange = {
        start: end - currentPeriod,
        end,
      };
    } else {
      if (vm.timeRange.end - vm.timeRange.start < 30 * 60 * 1000) {
        rbToast.warning(
          translateService.get('metric_timerange_should_less_30'),
        );
        return;
      }
    }
    vm.loading = true;
    const group_by = vm.metricType === 'INSTANCE' ? 'instance_id' : '';
    const requests = vm.queryTypes.map(type => {
      type.loadError = false;
      const query_list = [
        `${vm.aggregator}:${type.value}{service_id=${serviceParams.service_id}}\
by{${group_by}}`,
      ];
      const queryParams = {
        query_list,
        timeRange: vm.timeRange,
        seriesName: serviceParams.service_name,
      };
      if (type.value.split('.').includes('total')) {
        queryParams.accumulatedData = true;
      }
      return rbMonitorUtilities
        .getChartData(queryParams)
        .then(result => {
          if (result) {
            const { axisOptions, series } = result;
            type.series = series;
            type.axisOptions = axisOptions;
          } else {
            type.series = null;
          }
        })
        .catch(() => {
          type.loadError = true;
        })
        .then(() => {});
    });
    Promise.all(requests).then(() => {
      vm.loading = false;
      rbSafeApply();
    });
  }

  function initMetricParams() {
    // service params
    serviceParams = {
      namespace: vm.serviceData.namespace,
      service_name: vm.serviceData.service_name,
      service_id: vm.serviceData.uuid,
      application: vm.serviceData.application,
    };
    // init period
    currentPeriod = vm.periodOptions[0].period;
    // metric type
    vm.metricType = vm.metricTypes[0].value;
    // aggregator
    vm.aggregator = vm.metricAggregatorOptions[0].value;
  }

  function selectTimeRange(option) {
    currentPeriod = option.period;
    !isCustomTimeRange() && loadMetrics();
  }

  function selectMetricType(option) {
    vm.metricType = option.value;
    loadMetrics();
  }

  function selectMetricAggregator(option) {
    vm.aggregator = option.value;
    loadMetrics();
  }

  function isCustomTimeRange() {
    return currentPeriod === -1;
  }
}
