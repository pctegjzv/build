import _ from 'lodash';
import { resource_types as RESOURCE_TYPES } from '../../rbac_role/rb-rbac-helper.constant';
/**
 * Created by liudong on 16/9/22.
 */
const {
  SERVICE_INTERMEDIATE_STATUS,
  SHARED_DRIVER_NAME,
  SERVICE_DEPLOY_MODEL,
} = require('../rb-service.constant');
const { CONTAINER_MANAGERS } = require('../../region/rb-region.constant');
const templateStr = require('app/components/pages/app_service/detail/rb-service-detail.html');
angular.module('app.components.pages').component('rbServiceDetail', {
  controller: rbServiceDetailController,
  controllerAs: 'vm',
  bindings: {
    serviceName: '<',
    application: '<',
  },
  template: templateStr,
});

function rbServiceDetailController(
  $state,
  $log,
  $timeout,
  $filter,
  rbHttp,
  rbServiceDataService,
  rbRouterStateHelper,
  rbAccountService,
  rbRegionDataService,
  rbAppDataService,
  rbRegionService,
  applicationService,
  rbServiceUtilities,
  rbPrivilegeService,
  rbLabelsDataService,
  rbServiceAffinityService,
  rbServiceInstanceSize,
  translateService,
  rbConfirmBox,
  rbToast,
  WEBLABS,
  ENVIRONMENTS,
  rbModal,
  rbRoleUtilities,
) {
  const vm = this;

  let shortTimer, longTimer;
  let componentDestroyed = false;
  let serviceDeleted = false;

  vm.checkActionPermission = checkActionPermission;
  vm.actionStop = actionStop;
  vm.actionStart = actionStart;
  vm.actionDelete = actionDelete;
  vm.actionRetryUpdate = actionRetryUpdate;
  vm.actionRollback = actionRollback;
  vm.actionEdit = actionEdit;
  vm.actionAutomation = actionAutomation;
  vm.actionExec = actionExec;
  vm.actionScalingConfig = actionScalingConfig;
  vm.showLabelsDialog = showLabelsDialog;
  vm.labelsUpdated = labelsUpdated;
  vm.isSupportPodDNS = isSupportPodDNS;

  vm.onTabSelected = onTabSelected;
  vm.getLinkServiceUrl = getLinkServiceUrl;
  vm.isKubernetesContainer = isKubernetesContainer;
  vm.restartInstance = restartInstance;
  vm.updateStrategy = updateStrategy;
  vm.ScalingModeEnabled = ScalingModeEnabled;
  vm.affinityEnabled = affinityEnabled;
  vm.UpdateStrategyWithDeployEnabled = UpdateStrategyWithDeployEnabled;
  vm.updateK8sService = updateK8sService;
  vm.isSupportKubeServices = isSupportKubeServices;
  vm.updateDefaultDomain = updateDefaultDomain;

  vm.$onInit = _init;
  vm.$onDestroy = _destroy;
  //////////

  function _init() {
    vm.initialized = false;
    vm.serviceInAction = false;
    vm.serviceActionLoading = false;
    vm.update_strategy_enabled = true;
    vm.namespace = rbAccountService.getCurrentNamespace();
    vm.weblabs = WEBLABS;

    vm.serviceData = null;
    vm.regionFeatures = [];
    vm.isPrivateDeployEnabled = ENVIRONMENTS['is_private_deploy_enabled'];
    vm.isServiceHealthcheckEnabled =
      rbServiceUtilities.isServiceHealthcheckEnabled;

    // service display
    vm.basicInfoFields = [];
    vm.serviceUtilities = rbServiceUtilities;
    vm.privilegeService = rbPrivilegeService;
    vm.kubernetesServices = [];

    vm.inAction = false;
    vm.isStateful = false;

    getServiceDetail().then(() => {
      getRegionFeatures();
      vm.links = vm.serviceData.linked_to_services || [];
      if (vm.serviceData.volumes && vm.serviceData.volumes.length) {
        vm.serviceData.volumes.forEach(v => {
          if (v.driver_name && !SHARED_DRIVER_NAME.includes(v.driver_name)) {
            vm.update_strategy_enabled = false;
          }
        });
      }
      if (affinityEnabled()) {
        vm.kubeAffinitys = rbServiceAffinityService.valueToSettings(
          vm.serviceData.kube_config,
        );
      }
    });
  }

  function _destroy() {
    _cancelTimer();
    componentDestroyed = true;
  }

  function _initShortTimer() {
    _cancelTimer();
    shortTimer = $timeout(() => {
      if (!vm.serviceInAction) {
        return;
      }
      getServiceDetail();
    }, 3000);
  }

  function _initLongTimer() {
    _cancelTimer();
    longTimer = $timeout(() => {
      if (vm.serviceInAction) {
        return;
      }
      getServiceDetail();
    }, 15000);
  }

  function _cancelTimer() {
    $timeout.cancel(shortTimer);
    $timeout.cancel(longTimer);
  }

  function getRegionFeatures() {
    return rbRegionDataService
      .getRegions()
      .then(data => {
        const regions = data['regions'];
        _.forEach(regions, region => {
          if (region.name === vm.serviceData.region.name) {
            vm.region = region;
            vm.regionFeatures = _.get(region, 'features.service.features', []);
            vm.regionFeatures = vm.regionFeatures.concat(
              _.get(region, 'features.service.metrics.features', []),
            );
            return false;
          }
        });
      })
      .catch(() => {})
      .then(() => {
        _updateServiceDisplay();
        vm.initialized = true;
      });
  }

  function getServiceDetail() {
    return applicationService
      .applicationDetail({
        service_name: vm.serviceName,
        application: vm.application,
      })
      .then(data => {
        if (!componentDestroyed) {
          if (!data.detail) {
            $state.go('app_service.service.list');
          }
          vm.serviceData = data['detail'];
          vm.initialized && _updateServiceDisplay();
          rbRegionService.setRegionByName(vm.serviceData.region.name);
        }
      })
      .catch(e => {
        // requesting service does not exist
        if (!vm.initialized && e.status !== 403) {
          rbToast.warning(translateService.get('service_not_exist'));
        }
        // after deleting service or if service does not exist when init, goback
        if ((!componentDestroyed && serviceDeleted) || !vm.serviceData) {
          if (rbRouterStateHelper.stateHistory.length > 0) {
            const lastStateName =
              rbRouterStateHelper.stateHistory[
                rbRouterStateHelper.stateHistory.length - 1
              ].state.name;
            if (
              [
                'app_service.service.create_service',
                'app_service.service.service_detail',
              ].includes(lastStateName)
            ) {
              $state.go('app_service.service.list');
            } else {
              rbRouterStateHelper.back();
            }
          } else {
            $state.go('app_service.service.list');
          }
        }
      })
      .then(() => {
        // update polling timer, if request failed use the old serviceData
        vm.serviceData && _updatePollingTimer();
        // service links info
      });
  }

  function _updatePollingTimer() {
    // check if component destroyed
    if (componentDestroyed) {
      _cancelTimer();
      return;
    }
    // check if service is in action
    vm.serviceInAction =
      vm.serviceData.current_status.endsWith('ing') &&
      vm.serviceData.current_status !== 'Running';
    if (vm.serviceInAction) {
      _initShortTimer();
    } else {
      _initLongTimer();
    }
  }

  function _updateServiceDisplay() {
    // use default command
    const useDefaultCmd =
      !vm.serviceData.run_command && !vm.serviceData.entrypoint;
    const displayInstanceSize = rbServiceInstanceSize.getInstanceSizeText(
      vm.serviceData.custom_instance_size,
    );
    const displayCmd = useDefaultCmd
      ? translateService.get('use_default')
      : vm.serviceData.run_command;
    const displayEntrypoint = useDefaultCmd
      ? translateService.get('use_default')
      : vm.serviceData.entrypoint;
    const displayImage = `${vm.serviceData.image_name}:${
      vm.serviceData.image_tag
    }`;
    const displayScalingMode =
      vm.serviceData.scaling_mode === 'AUTO'
        ? translateService.get('auto_scaling')
        : translateService.get('manual_scaling');
    vm.kubernetesServices = _.get(vm.serviceData, 'kube_config.services', []);
    if (!vm.serviceData.node_tag && !vm.serviceData.node_selector) {
      vm.serviceData.node_tag = translateService.get('node_tag_none');
    }
    vm.serviceData.node_selector_string = [];
    if (vm.serviceData.node_tag) {
      vm.serviceData.node_selector_string.push(
        vm.serviceData.node_tag.split(':').join(': '),
      );
    }
    if (vm.serviceData.node_selector) {
      const keys = Object.keys(vm.serviceData.node_selector);
      keys.forEach(n => {
        vm.serviceData.node_selector_string.push(
          `${n}: ${vm.serviceData.node_selector[n]}`,
        );
      });
    }
    vm.serviceData.node_selector_string = vm.serviceData.node_selector_string.join(
      ', ',
    );
    // init basic info options
    vm.basicInfoFields = [
      {
        name: 'service_area',
        html: `<a href="/console/cluster/detail/${
          vm.serviceData.region.name
        }">${vm.serviceData.region.name}</a>`,
      },
      {
        name: 'instance_size',
        value: displayInstanceSize,
      },
      {
        name: 'service_create_deploy_model',
        value: vm.serviceData.pod_controller,
        disabled: !vm.isKubernetesContainer(),
      },
      {
        name: 'created_time',
        value: $filter('formatUtcStr')(vm.serviceData.created_at),
      },
      {
        name: 'updated_time',
        value: $filter('formatUtcStr')(vm.serviceData.updated_at),
      },
      {
        name: 'entrypoint',
        value: displayEntrypoint,
      },
      {
        name: 'cmd',
        value: displayCmd,
      },
      {
        name: 'network_mode',
        value: vm.serviceData.network_mode,
      },
      {
        name: 'mipn',
        value: vm.serviceData.mipn_enabled
          ? translateService.get('on')
          : translateService.get('off'),
        disabled: !vm.regionFeatures.includes('mipn'),
      },
      {
        name: 'scaling_mode',
        value: displayScalingMode,
        disabled: vm.serviceData.pod_controller !== 'Deployment',
      },
      {
        name: 'node_tag',
        value: vm.serviceData.node_selector_string,
      },
      {
        name: 'application',
        value: vm.serviceData.application_name,
        disabled: !vm.serviceData.application_name,
        state: 'app_service.app.app_detail',
        stateParams: {
          app_name: vm.serviceData.application_uuid,
        },
      },
      {
        name: 'update_strategy',
        value: _getUpdateStrategyStr(),
        disabled: !_isSupportUpdateStrategy(),
      },
      {
        name: 'quota_space',
        value: vm.serviceData.space_name,
        disabled: !vm.weblabs['quota_enabled'] || !vm.serviceData.space_name,
        state: `quota.detail.${vm.serviceData.space_name}`,
      },
      {
        name: 'image',
        row: true,
        value: displayImage,
      },
      {
        name: 'labels',
        row: true,
        html: rbServiceUtilities.getLabelsHtml(vm.serviceData.labels),
      },
    ];

    // service health_status
    rbServiceUtilities.setServiceHealthStatus(vm.serviceData);
    // service is stateful
    vm.isStateful = vm.serviceData.volumes.length > 0;

    // sort instances
    vm.instances = _.sortBy(vm.instances, instance => {
      const index = instance.instance_name.split('.');
      return _.isNumber(index) ? index : 0;
    });
  }

  function _isSupportUpdateStrategy() {
    return (
      vm.isKubernetesContainer() &&
      ((_.get(vm.serviceData, 'pod_controller') ===
        SERVICE_DEPLOY_MODEL.DAEMONSET &&
        _.get(vm.region, 'features.kubernetes.version') >= '1.6') ||
        _.get(vm.serviceData, 'pod_controller') ===
          SERVICE_DEPLOY_MODEL.DEPLOYMENT)
    );
  }

  function _getUpdateStrategyStr() {
    if (
      _.get(vm.serviceData, 'pod_controller') ===
        SERVICE_DEPLOY_MODEL.DAEMONSET &&
      _.get(vm.region, 'features.kubernetes.version') >= '1.6'
    ) {
      return `${translateService.get('service_max_unavailable')}: ${
        vm.serviceData.update_strategy.max_unavailable
      }`;
    }
    return `${translateService.get('service_max_surge')}: ${
      vm.serviceData.update_strategy.max_surge
    },
        ${translateService.get('service_max_unavailable')}: ${
      vm.serviceData.update_strategy.max_unavailable
    }`;
  }

  function onTabSelected(type) {
    vm.selectedTab = type;
  }

  // service actions
  function _serviceAction(action) {
    const params = {
      action: action,
      pk: {
        namespace: vm.namespace,
        service_name: vm.serviceName,
        application: vm.application,
      },
    };
    vm.serviceInAction = true;
    vm.serviceActionLoading = true;
    applicationService
      .applicationAction(params)
      .then(() => {})
      .catch(() => {})
      .then(() => {
        vm.serviceActionLoading = false;
        // update current_status immediately, wait for 3s, then get service detail
        switch (action) {
          case 'start':
            vm.serviceData.current_status =
              SERVICE_INTERMEDIATE_STATUS.STARTING;
            break;
          case 'stop':
            vm.serviceData.current_status =
              SERVICE_INTERMEDIATE_STATUS.STOPPING;
            break;
          case 'delete':
            vm.serviceData.current_status =
              SERVICE_INTERMEDIATE_STATUS.DELETING;
            break;
          case 'retryupdate':
            vm.serviceData.current_status =
              SERVICE_INTERMEDIATE_STATUS.STARTING;
            break;
          case 'rollback':
            vm.serviceData.current_status =
              SERVICE_INTERMEDIATE_STATUS.UPDATING;
            break;
          default:
            break;
        }
        if (action === 'delete') {
          serviceDeleted = true;
        }
        $timeout(() => {
          getServiceDetail();
        }, 3000);
      });
  }

  function actionStop() {
    const title = translateService.get('stop');
    const textContent = translateService.get(
      'app_service_stop_service_confirm',
      {
        service_name: vm.serviceData.service_name,
      },
    );
    rbConfirmBox
      .show({
        title,
        textContent,
      })
      .then(() => {
        _serviceAction('stop');
      });
  }

  function actionStart() {
    const title = translateService.get('start');
    const textContent = translateService.get(
      'app_service_start_service_confirm',
      {
        service_name: vm.serviceData.service_name,
      },
    );
    rbConfirmBox
      .show({
        title,
        textContent,
      })
      .then(() => {
        _serviceAction('start');
      });
  }

  function actionDelete() {
    const title = translateService.get('delete');
    const textContent = translateService.get(
      'app_service_delete_service_confirm',
      {
        service_name: vm.serviceData.service_name,
      },
    );
    rbConfirmBox
      .show({
        title,
        textContent,
      })
      .then(() => {
        _serviceAction('delete');
        $state.go('app_service.service.list');
      });
  }

  function actionRetryUpdate() {
    _serviceAction('retryupdate');
  }

  function actionRollback() {
    const title = translateService.get('rollback');
    const textContent = translateService.get(
      'app_service_rollback_service_confirm',
      {
        service_name: vm.serviceData.service_name,
      },
    );
    rbConfirmBox
      .show({
        title,
        textContent,
      })
      .then(() => {
        _serviceAction('rollback');
      });
  }

  function actionEdit() {
    $state.go('app_service.service.update_service', {
      service_name: vm.serviceName,
      application: vm.application,
    });
  }

  function actionAutomation() {
    rbModal
      .show({
        title: translateService.get('automation'),
        template:
          '<rb-service-automation uuid="uuid" automation="automation"></rb-service-automation>',
        locals: {
          uuid: vm.serviceData.uuid,
          automation: vm.automation,
        },
      })
      .then(is_active => {
        vm.automation.is_active = !!is_active;
      })
      .catch(() => {});
  }

  async function actionExec() {
    rbModal
      .show({
        title: 'EXEC',
        component: 'rbServiceExecDialog',
        locals: {
          serviceData: vm.serviceData,
        },
      })
      .catch(() => {});
  }

  function actionScalingConfig() {
    const params = {
      namespace: vm.namespace,
      service_name: vm.serviceData.uuid,
    };
    rbModal
      .show({
        width: 750,
        title: translateService.get('auto_scaling'),
        locals: {
          serviceData: vm.serviceData,
          region: vm.region,
        },
        component: 'rbServiceAutoScalingDialog',
      })
      .then(data => {
        if (data.scaling_mode === 'AUTO') {
          data.autoscaling_config = JSON.stringify(data.autoscaling_config);
          data.target_num_instances =
            data.autoscaling_config.minimum_num_instances;
        }
        rbServiceDataService
          .scaling(params, data)
          .then(() => {
            rbToast.success(
              translateService.get('service_scaling_config_saved'),
            );
          })
          .catch(() => {})
          .then(() => {
            if (vm.serviceData.current_status === 'Running') {
              vm.serviceData.current_status =
                SERVICE_INTERMEDIATE_STATUS.SCALING;
            } else {
              vm.serviceData.current_status =
                SERVICE_INTERMEDIATE_STATUS.UPDATING;
            }
            getServiceDetail();
          });
      })
      .catch(() => {});
  }

  function getLinkServiceUrl(linkService) {
    return $state.href('app_service.service.service_detail', {
      service_name: linkService.dest_uuid,
    });
  }

  function isKubernetesContainer() {
    return (
      _.get(vm.region, 'container_manager', '') ===
      CONTAINER_MANAGERS.KUBERNETES
    );
  }

  function isSupportKubeServices() {
    return (
      _.get(vm.region, 'container_manager', '') ===
      CONTAINER_MANAGERS.KUBERNETES
    );
  }

  function checkActionPermission(action) {
    return rbRoleUtilities.resourceHasPermission(
      vm.serviceData,
      RESOURCE_TYPES.SERVICE,
      action,
    );
  }

  function restartInstance(instance) {
    rbConfirmBox.show({
      title: translateService.get('service_instance_restart_confirm_title'),
      textContent: translateService.get('service_instance_restart_confirm'),
      callback: _deleteInstance(instance),
    });
  }

  function _deleteInstance(instance) {
    return () => {
      const data = {
        pod_name: instance.pod_name,
        service_name: vm.serviceData.uuid,
      };
      return rbServiceDataService
        .instanceDelete(data)
        .then(() => {
          vm.serviceData.instances = vm.serviceData.instances.filter(
            i => i.uuid !== instance.uuid,
          );
        })
        .catch(() => {
          /**/
        });
    };
  }

  function updateStrategy() {
    rbModal
      .show({
        title: translateService.get('update_strategy'),
        component: 'rbServiceUpdateStrategyDialog',
        locals: {
          serviceData: _.cloneDeep(vm.serviceData),
          region: _.cloneDeep(vm.region),
        },
      })
      .then(() => {
        getServiceDetail();
      })
      .catch(() => {});
  }

  function labelsUpdated() {
    getServiceDetail();
  }

  function ScalingModeEnabled() {
    return (
      _.get(vm.serviceData, 'pod_controller', '') !==
      SERVICE_DEPLOY_MODEL.DAEMONSET
    );
  }

  function UpdateStrategyWithDeployEnabled() {
    return _isSupportUpdateStrategy();
  }

  function isSupportPodDNS() {
    return (
      _.get(vm.serviceData, 'pod_controller', '') ===
      SERVICE_DEPLOY_MODEL.STATEFULSET
    );
  }

  function updateK8sService() {
    rbModal
      .show({
        width: 750,
        title: translateService.get('kubernetes_service'),
        component: 'rbServiceKubernetesServiceUpdateDialog',
        locals: {
          services: vm.kubernetesServices,
          serviceName: vm.serviceData.service_name,
          containerPorts: vm.serviceData.ports,
          applicationName: vm.serviceData.application_name,
        },
      })
      .then(() => {
        getServiceDetail();
      })
      .catch(() => {});
  }

  function showLabelsDialog(namespace, type, serviceName, onSubmit) {
    if (affinityEnabled()) {
      rbModal
        .show({
          title: translateService.get('labels_update'),
          component: 'rbServiceKubeLabelsDialog',
          locals: {
            service: vm.serviceData,
          },
        })
        .then(onSubmit)
        .catch(() => {});
      return;
    }

    rbLabelsDataService.showLabelsDialog(
      namespace,
      type,
      serviceName,
      onSubmit,
    );
  }

  function affinityEnabled() {
    return (
      vm.serviceData &&
      vm.serviceData.container_manager === CONTAINER_MANAGERS.KUBERNETES &&
      vm.serviceData.pod_controller !== 'DaemonSet'
    );
  }

  function updateDefaultDomain() {
    rbModal
      .show({
        width: 750,
        title: translateService.get('update_default_domain'),
        component: 'rbServiceUpdateDefaultDomain',
        locals: {
          serviceId: vm.serviceData.uuid,
        },
      })
      .catch(() => {
        // placeholder
      });
  }
}
