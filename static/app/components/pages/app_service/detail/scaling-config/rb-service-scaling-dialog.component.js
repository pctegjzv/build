/**
 * Created by liudong on 2017/1/3.
 */
import { CONTAINER_MANAGERS } from '../../../region/rb-region.constant';
import { SERVICE_DEPLOY_MODEL } from '../../rb-service.constant';
const templateStr = require('app/components/pages/app_service/detail/scaling-config/rb-service-scaling-dialog.html');
angular.module('app.components.pages').component('rbServiceScalingDialog', {
  controller: rbServiceScalingDialogController,
  controllerAs: 'vm',
  bindings: {
    serviceData: '<',
    region: '<',
  },
  template: templateStr,
});

function rbServiceScalingDialogController(rbModal, translateService) {
  const vm = this;

  const SCALING_MODE_MANUAL = {
    name: translateService.get('manual'),
    value: 'MANUAL',
  };

  const SCALING_MODE_AUTO = {
    name: translateService.get('auto_scale'),
    value: 'AUTO',
  };

  const SCALING_MODE_MAP = {
    [2]: [SCALING_MODE_MANUAL, SCALING_MODE_AUTO],
    [1]: [SCALING_MODE_MANUAL],
  };
  vm.scalingMode = vm.serviceData.scaling_mode;

  if (vm.serviceData.autoscaling_config === '{}') {
    vm.autoscaling_config = {
      metric_name: 'CPU_UTILIZATION',
      metric_stat: 'MEAN',
      upper_threshold: 100,
      lower_threshold: 0,
      decrease_delta: 1,
      increase_delta: 1,
      minimum_num_instances: 1,
      maximum_num_instances: 20,
      wait_period: 30,
    };
  } else {
    vm.autoscaling_config = angular.fromJson(vm.serviceData.autoscaling_config);
    vm.autoscaling_config.upper_threshold = parseInt(
      vm.autoscaling_config.upper_threshold * 100,
    );
    vm.autoscaling_config.lower_threshold = parseInt(
      vm.autoscaling_config.lower_threshold * 100,
    );
  }
  vm.target_num_instances = vm.serviceData.target_num_instances || 1;
  vm.metricNames = [
    { name: translateService.get('cpu_utilization'), value: 'CPU_UTILIZATION' },
    {
      name: translateService.get('memory_utilization'),
      value: 'MEMORY_UTILIZATION',
    },
  ];
  vm.metricTypes = [
    {
      name: translateService.get('mean'),
      value: 'MEAN',
    },
    { name: translateService.get('maximum'), value: 'MAXIMUM' },
    {
      name: translateService.get('minimum'),
      value: 'MINIMUM',
    },
  ];

  /**
   * Calculate the scaling mode options. This function will be used as a getter.
   * https://trello.com/c/yag7jujX/526-glusterfs
   */
  vm.getScalingModeOptions = () => {
    let showAutoOption = false;
    if (vm.serviceData.pod_controller === SERVICE_DEPLOY_MODEL.STATEFULSET) {
      showAutoOption = false;
    } else if (!vm.serviceData.is_stateful) {
      showAutoOption = true;
    } else {
      const volumes = _.get(vm, 'serviceData.volumes', []);
      showAutoOption = !volumes.some(volume => volume.driver_name === 'ebs');
    }
    // hmm, side-effect for getter function, bad design ...
    // KUBERNETES does not support auto instances but can have multiple instances
    if (
      showAutoOption ||
      vm.region.container_manager === CONTAINER_MANAGERS.KUBERNETES
    ) {
      vm.disableInstanceNumber = false;
    } else {
      vm.target_num_instances = 1;
      vm.disableInstanceNumber = true;
    }

    return SCALING_MODE_MAP[showAutoOption ? 2 : 1];
  };

  vm.onScalingModeChange = option => {
    vm.scalingMode = option.value;
  };
  vm.onMetricNameChange = option => {
    vm.autoscaling_config.metric_name = option.value;
  };
  vm.onMetricTypeChange = option => {
    vm.autoscaling_config.metric_stat = option.value;
  };
  vm.confirm = () => {
    vm.scalingForm.$setSubmitted();
    if (vm.scalingForm.$invalid) {
      return;
    }
    const data = {
      scaling_mode: vm.scalingMode,
    };
    if (vm.scalingMode === 'AUTO') {
      data.autoscaling_config = vm.autoscaling_config;
    } else {
      data.target_num_instances = vm.target_num_instances;
    }
    rbModal.hide(data);
  };

  vm.cancel = () => {
    rbModal.cancel();
  };
}
