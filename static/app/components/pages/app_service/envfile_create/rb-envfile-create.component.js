/**
 * Created by liudong on 16/10/11.
 */
const templateStr = require('app/components/pages/app_service/envfile_create/rb-envfile-create.html');
angular.module('app.components.pages').component('rbEnvfileCreate', {
  controller: rbEnvfileCreateController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbEnvfileCreateController(
  $state,
  $location,
  envfileService,
  parserService,
  rbGlobalSetting,
  rbToast,
  translateService,
  WEBLABS,
  rbQuotaSpaceDataService,
  rbRouterStateHelper,
) {
  const vm = this;
  vm.submitting = false;
  vm.envfileList = [];
  vm.selectedName = '';
  vm.selectEnabled = false;
  vm.editorOptions = rbGlobalSetting.getEditorOptions('env');

  vm.$onInit = _init;

  vm.create = create;
  vm.cancel = cancel;
  vm.onEnvfileNameChange = onEnvfileNameChange;
  vm.onSpaceChanged = onSpaceChanged;

  //////////
  function _init() {
    vm.initialized = false;
    vm.quotaEnabled = WEBLABS['quota_enabled'];
    if ('export' in $location.search() && envfileService.getExport()) {
      // export envfile
      vm.yaml = envfileService.getExport();
    } else if (
      'from' in $location.search() &&
      $location.search()['from'] !== ''
    ) {
      // create envfile from existing envfile name
      _loadExistEnvfile($location.search()['from']);
    } else {
      // create envfile from scrath
      vm.fromScrath = true;
    }
    envfileService
      .envfileList()
      .then(data => {
        vm.envfileList = data['envfiles'] || [];
      })
      .catch(() => {})
      .then(() => {
        vm.initialized = true;
      });
    if (vm.quotaEnabled) {
      rbQuotaSpaceDataService.getConsumableSpaces().then(spaces => {
        vm.spaces = spaces;
      });
    }
  }

  function create() {
    vm.form.$submitted = true;
    if (vm.form.$invalid) {
      return;
    }
    if (!vm.content) {
      rbToast.warning(
        `${translateService.get('envfile')}${translateService.get(
          'cannot_be_empty',
        )}`,
      );
      return;
    }
    // check if envfile syntax is valid
    try {
      parserService.loadEnvfile(vm.content);
    } catch (e) {
      rbToast.warning(
        translateService.get('envfile_invalid', { line: e.line }),
      );
      return;
    }
    const data = {
      name: vm.envfileName,
      description: vm.envfileDesc || '',
      content: parserService.loadEnvfile(vm.content, 'list'),
    };
    if (vm.quotaEnabled && vm.space_name) {
      data.space_name = vm.space_name;
    }
    vm.submitting = true;
    envfileService
      .envfileCreate(data)
      .then(data => {
        const fileName = data.env_file.uuid;
        $state.go('app_service.envfile.detail', { envfileName: fileName });
      })
      .catch(() => {})
      .then(() => {
        vm.form.$setPristine();
        vm.submitting = false;
      });
  }

  function cancel() {
    rbRouterStateHelper.back();
  }

  function onSpaceChanged(space) {
    vm.space_name = space.name;
  }

  function _loadExistEnvfile(name) {
    if (vm.initialized) {
      vm.loading = true;
    }
    envfileService
      .envfileDetail(name)
      .then(data => {
        const envfile = data['envfile'] || {};
        if (envfile.content) {
          vm.content = envfile.content;
        }
      })
      .catch(() => {})
      .then(() => {
        vm.loading = false;
      });
  }

  function onEnvfileNameChange(option) {
    vm.selectedName = option.uuid;
    _loadExistEnvfile(vm.selectedName);
  }
}
