/**
 * Created by liudong on 16/10/11.
 */
import { resource_types as RESOURCE_TYPES } from '../../rbac_role/rb-rbac-helper.constant';

import template from './rb-envfile-detail.html';

angular.module('app.components.pages').component('rbEnvfileDetail', {
  controller: rbEnvfileDetailController,
  controllerAs: 'vm',
  template,
});

function rbEnvfileDetailController(
  $stateParams,
  $state,
  envfileService,
  rbGlobalSetting,
  translateService,
  rbConfirmBox,
  rbRoleUtilities,
  rbToast,
) {
  const vm = this;
  vm.basicInfoCollapse = basicInfoCollapse;
  vm.buttonDisplayExpr = buttonDisplayExpr;
  vm.$onInit = _init;

  //////////
  async function _init() {
    vm.envfileName = $stateParams.envfileName;
    if (!vm.envfileName) {
      $state.go('app_service.envfile.list');
    }
    vm.isBasicInfoCollapse = false;
    vm.editorOptions = rbGlobalSetting.getEditorOptions('env', true);
    vm.initialized = false;
    vm.envFileCreateEnabled = await rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.ENV_FILE,
    );
    envfileService
      .envfileDetail(vm.envfileName)
      .then(data => {
        vm.envfile = data['envfile'] || {};
        vm.envfileDisplayName = vm.envfile.name;
      })
      .catch(e => {
        if (e.status !== 403) {
          rbToast.error(translateService.get('env_file_not_exist'));
        }
        $state.go('app_service.envfile.list');
      })
      .then(() => {
        vm.initialized = true;
      });
  }

  vm.saveAs = () => {
    $state.go('app_service.envfile.create', { from: vm.envfileName });
  };

  vm.envfileDelete = () => {
    rbConfirmBox
      .show({
        title: translateService.get('delete'),
        textContent: `${translateService.get('delete')}${translateService.get(
          'env_file',
        )} ${vm.envfileDisplayName}?`,
      })
      .then(() => {
        vm.submitting = true;
        envfileService
          .envfileDelete(vm.envfileName)
          .then(() => {
            $state.go('app_service.envfile.list');
          })
          .catch(() => {})
          .then(() => {
            vm.submitting = false;
          });
      });
  };

  vm.envfileUpdate = () => {
    $state.go('app_service.envfile.update', { envfileName: vm.envfileName });
  };

  function basicInfoCollapse() {
    vm.isBasicInfoCollapse = !vm.isBasicInfoCollapse;
  }

  function buttonDisplayExpr(action) {
    return rbRoleUtilities.resourceHasPermission(
      vm.envfile,
      RESOURCE_TYPES.ENV_FILE,
      action,
    );
  }
}
