import { resource_types as RESOURCE_TYPES } from '../../rbac_role/rb-rbac-helper.constant';

import template from './rb-envfile-list.html';

angular.module('app.components.pages').component('rbEnvfileList', {
  controller: envfileListController,
  controllerAs: 'vm',
  template,
});

function envfileListController(
  WEBLABS,
  envfileService,
  rbConfirmBox,
  rbAccountService,
  orgService,
  rbRoleDataService,
  rbRoleUtilities,
  rbSafeApply,
  translateService,
) {
  const vm = this;

  vm.envfileDelete = envfileDelete;
  vm.buttonDisplayExpr = buttonDisplayExpr;
  vm.deletingMap = {};
  vm.$onInit = _init;

  //////////
  async function _init() {
    vm.initialized = false;
    vm.quotaEnabled = WEBLABS.QUOTA_ENABLED;
    vm.envFileCreateEnabled = false;
    vm.tableLoading = false;
    envfileService
      .envfileList()
      .then(data => {
        vm.envfileList = data['envfiles'] || [];
      })
      .catch(() => {})
      .then(() => {
        vm.initialized = true;
      });
    vm.envFileCreateEnabled = await rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.ENV_FILE,
    );
    rbSafeApply();
  }

  function envfileDelete(envfile) {
    const envfileName = envfile.uuid;
    const envfileDisplayName = envfile.name;
    rbConfirmBox
      .show({
        title: translateService.get('delete'),
        textContent:
          translateService.get('delete') +
          translateService.get('env_file') +
          '"' +
          envfileDisplayName +
          '"' +
          '?',
      })
      .then(() => {
        vm.deletingMap[envfileName] = true;
        envfileService.envfileDelete(envfileName).then(() => {
          envfileService
            .envfileList()
            .then(data => {
              vm.envfileList = data['envfiles'] || [];
            })
            .catch(() => {})
            .then(() => {
              vm.deletingMap[envfileName] = false;
            });
        });
      });
  }

  function buttonDisplayExpr(env_file, action) {
    return rbRoleUtilities.resourceHasPermission(
      env_file,
      RESOURCE_TYPES.ENV_FILE,
      action,
    );
  }
}
