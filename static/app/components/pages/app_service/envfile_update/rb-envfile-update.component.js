/**
 * Created by liudong on 16/10/11.
 */
const templateStr = require('app/components/pages/app_service/envfile_update/rb-envfile-update.html');
angular.module('app.components.pages').component('rbEnvfileUpdate', {
  controller: rbEnvfileUpdateController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbEnvfileUpdateController(
  $state,
  $stateParams,
  envfileService,
  parserService,
  rbGlobalSetting,
  rbToast,
  rbSafeApply,
  translateService,
  rbRouterStateHelper,
) {
  const vm = this;

  vm.envfileName = $stateParams.envfileName;
  vm.envfile = {};
  vm.envfileList = [];

  vm.initialized = false;
  vm.submitting = false;
  vm.editorOptions = rbGlobalSetting.getEditorOptions('yaml');

  vm.$onInit = _init;

  vm.update = update;
  vm.cancel = cancel;
  vm.validateNameExist = validateNameExist;

  //////////
  function _init() {
    vm.initialized = false;

    envfileService
      .envfileDetail(vm.envfileName)
      .then(data => {
        if (data['envfile']) {
          vm.envfile = data['envfile'];
          vm.envfileDisplayName = vm.envfile.name;
        }
      })
      .catch(() => {
        $state.go('app_service.envfile.list');
      })
      .then(() => {
        vm.initialized = true;
        rbSafeApply();
      });
  }

  function update() {
    vm.form.$submitted = true;
    if (vm.form.$invalid) {
      return;
    }
    if (!vm.envfile.content) {
      rbToast.warning(
        `${translateService.get('envfile')}${translateService.get(
          'cannot_be_empty',
        )}`,
      );
      return;
    }
    // check if envfile syntax is valid
    try {
      parserService.loadEnvfile(vm.envfile.content);
    } catch (e) {
      rbToast.warning(
        translateService.get('envfile_invalid', { line: e.line }),
      );
      return;
    }

    const data = {
      origin_name: vm.envfileName,
      name: vm.envfile.name,
      description: vm.envfile.description,
      content: parserService.loadEnvfile(vm.envfile.content, 'list'),
    };
    vm.submitting = true;
    envfileService
      .envfileUpdate(data)
      .then(data => {
        const fileName = data.env_file.name;
        $state.go('app_service.envfile.detail', { envfileName: fileName });
      })
      .catch(() => {})
      .then(() => {
        vm.form.$setPristine();
        vm.submitting = false;
      });
  }

  function cancel() {
    rbRouterStateHelper.back();
  }

  function validateNameExist(value) {
    return !_.find(vm.envfileList, item => {
      return item.name === value && value !== vm.envfileName;
    });
  }
}
