import * as _ from 'lodash';
const templateStr = require('app/components/pages/app_service/fieldset/affinity-control/edit-affinity-dialog/rb-service-edit-affinity-dialog.component.html');

angular
  .module('app.components.common')
  .component('rbServiceEditAffinityDialog', {
    bindings: {
      serviceLabels: '<',
      nodeLabels: '<',
      affinity: '<',
      weightCounts: '<',
    },
    controller: rbServiceEditAffinityDialogController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbServiceEditAffinityDialogController(
  rbModal,
  rbSafeApply,
  rbServiceAffinityService,
) {
  const vm = this;
  const { types, classifications, operators } = rbServiceAffinityService;
  vm.$onInit = _init;
  vm.$onChanges = _change;
  vm.onTypeChange = onTypeChange;
  vm.onClassificationChange = onClassificationChange;
  vm.onOperatorChange = onOperatorChange;
  vm.onServiceLabelChange = onServiceLabelChange;
  vm.onServiceKeyChange = onServiceKeyChange;
  vm.onNodeLabelChange = onNodeLabelChange;
  vm.weightPercent = weightPercent;
  vm.isPreferred = isPreferred;
  vm.isExisting = isExisting;
  vm.serviceLabelValidateFn = serviceLabelValidateFn;
  vm.serviceKeyValidateFn = serviceKeyValidateFn;
  vm.submit = () => {
    vm.form.$setSubmitted();
    if (vm.form.$valid) {
      rbModal.hide({ updated: vm.model });
    }
  };
  vm.cancel = () => {
    rbModal.cancel();
  };
  vm.nodeLabelSetting = {
    minCharacters: 0,
    transition: 'slide',
  };
  /////////
  function _init() {
    vm.types = toKeyValuePairs(types);
    vm.classifications = toKeyValuePairs(classifications);
    vm.operators = toKeyValuePairs(operators);
    vm.model = angular.copy(vm.affinity || {});
    vm.editType = vm.affinity ? 'update' : 'create';
    vm.serviceLabelLength =
      (vm.model.serviceLabel && vm.model.serviceLabel.length) || 0;
    vm.serviceKeyLength =
      (vm.model.serviceKey && vm.model.serviceKey.length) || 0;
  }

  function _change(changes) {
    if (changes.serviceLabels && changes.serviceLabels.currentValue) {
      const labels = changes.serviceLabels.currentValue;

      vm.serviceLabels = labels.map(label => `${label.key}:${label.value}`);
      vm.serviceKeys = _.chain(labels)
        .map(label => label.key)
        .uniq()
        .value();
    }
  }

  function onTypeChange(option) {
    vm.model.type = option.value;
  }

  function onClassificationChange(option) {
    vm.model.classification = option.value;
  }

  function onOperatorChange(option) {
    vm.model.operator = option.value;
  }

  function onServiceLabelChange(value) {
    vm.model.serviceLabel = value;
    vm.form.serviceLabelLength.$dirty = true;
    vm.serviceLabelLength = vm.model.serviceLabel.length;
  }

  function onServiceKeyChange(value) {
    vm.model.serviceKey = value;
    vm.form.serviceKeyLength.$dirty = true;
    vm.serviceKeyLength = vm.model.serviceKey.length;
  }

  function onNodeLabelChange(value) {
    vm.model.nodeLabel = value;
  }

  function isPreferred() {
    return vm.model.classification === classifications.Preferred;
  }

  function isExisting() {
    return [operators.Exists, operators.DoesNotExist].includes(
      vm.model.operator,
    );
  }

  function weightPercent() {
    const weight = vm.model.weight || 0;
    const weightCount = vm.weightCounts[vm.model.type || types.Affinity] || 0;

    return vm.model.classification === classifications.Preferred &&
      (weightCount || weight)
      ? Math.round((weight * 100) / (weightCount + weight))
      : null;
  }

  function serviceLabelValidateFn(item) {
    return item.indexOf(':') > 0 && item.split(':').length === 2;
  }

  function serviceKeyValidateFn(item) {
    return item.indexOf(':') < 0;
  }
}

const toKeyValuePairs = obj =>
  Object.keys(obj).map(key => ({ key, value: obj[key] }));
