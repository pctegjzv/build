import * as _ from 'lodash';
const templateStr = require('app/components/pages/app_service/fieldset/affinity-control/rb-service-affinity-control.component.html');
angular.module('app.components.pages').component('rbServiceAffinityControl', {
  controller: rbServiceAffinityControlController,
  require: {
    model: 'ngModel',
  },
  bindings: {
    self: '<',
    services: '<',
    serviceLabels: '<',
    nodeLabels: '<',
  },
  controllerAs: 'vm',
  template: templateStr,
});

function rbServiceAffinityControlController(
  translateService,
  rbModal,
  rbServiceAffinityService,
) {
  const vm = this;
  vm.$onInit = _init;
  vm.$onChanges = _change;
  vm.onModeChange = onModeChange;
  vm.openEditDialog = openEditDialog;
  vm.addAffinityService = addAffinityService;
  vm.removeAffinityService = removeAffinityService;
  vm.addAntiAffinityService = addAntiAffinityService;
  vm.removeAntiAffinityService = removeAntiAffinityService;
  vm.removeAdvancedSetting = removeAdvancedSetting;

  ///////////
  function _init() {
    vm.modes = [
      { key: translateService.get('affinity_standard'), value: 'standard' },
      { key: translateService.get('affinity_advanced'), value: 'advanced' },
    ];

    vm.model.$render = () => {
      _.extend(vm, rbServiceAffinityService.toState(vm.model.$viewValue));
    };
  }

  function _change(changes) {
    if (changes.services && changes.services.currentValue) {
      const serviceOptions = (vm.services || []).map(service => {
        if (vm.self && vm.self.uuid === service.uuid) {
          return {
            uuid: service.uuid,
            name: translateService.get('affinity_service_self'),
            display_name: translateService.get('affinity_service_self'),
          };
        }

        return {
          uuid: service.uuid,
          name: service.service_name,
          display_name: service.space_name
            ? `${service.service_name}(${service.space_name})`
            : service.service,
        };
      });

      if (!vm.self || !vm.self.uuid) {
        serviceOptions.unshift({
          uuid: 'self',
          name: translateService.get('affinity_service_self'),
          display_name: translateService.get('affinity_service_self'),
        });
      }

      vm.affinityServices = _.intersection(
        vm.affinityServices,
        serviceOptions.map(service => service.uuid),
      );

      vm.antiAffinityServices = _.intersection(
        vm.antiAffinityServices,
        serviceOptions.map(service => service.uuid),
      );

      vm.affinityServiceOptions = [
        ...serviceOptions.filter(
          service => service.uuid !== 'self' && service.uuid !== vm.self.uuid,
        ),
      ];
      vm.antiAffinityServiceOptions = [...serviceOptions];
    }
  }

  function addAffinityService(service) {
    vm.affinityServices = _.chain(vm.affinityServices)
      .concat([service.uuid])
      .uniq()
      .value();

    vm.antiAffinityServiceOptions = vm.antiAffinityServiceOptions.map(
      option =>
        option.uuid === service.uuid
          ? { ...option, op_disabled: true }
          : option,
    );
    setViewValue();
  }

  function removeAffinityService(service) {
    vm.affinityServices.splice(vm.affinityServices.indexOf(service.uuid), 1);
    vm.antiAffinityServiceOptions = vm.antiAffinityServiceOptions.map(
      option =>
        option.uuid === service.uuid
          ? { ...option, op_disabled: false }
          : option,
    );
    setViewValue();
  }

  function addAntiAffinityService(service) {
    vm.antiAffinityServices = _.chain(vm.antiAffinityServices)
      .concat([service.uuid])
      .uniq()
      .value();

    vm.affinityServiceOptions = vm.affinityServiceOptions.map(
      option =>
        option.uuid === service.uuid
          ? { ...option, op_disabled: true }
          : option,
    );
    setViewValue();
  }

  function removeAntiAffinityService(service) {
    vm.antiAffinityServices.splice(
      vm.antiAffinityServices.indexOf(service.uuid),
      1,
    );
    vm.affinityServiceOptions = vm.affinityServiceOptions.map(
      option =>
        option.uuid === service.uuid
          ? { ...option, op_disabled: false }
          : option,
    );
    setViewValue();
  }

  function setViewValue() {
    vm.model.$setViewValue(
      rbServiceAffinityService.toValue(
        vm.mode,
        vm.affinityServices,
        vm.antiAffinityServices,
        vm.advancedSettings,
      ),
    );
  }

  function onModeChange(option) {
    vm.mode = option.value;
    setViewValue();
  }

  function removeAdvancedSetting(affinity) {
    vm.advancedSettings.splice(vm.advancedSettings.indexOf(affinity), 1);
    setViewValue();
  }

  function advancedSettingsWeightCount(except) {
    const { classifications } = rbServiceAffinityService;
    return _.chain(vm.advancedSettings)
      .filter(
        setting =>
          setting.classification === classifications.Preferred &&
          setting !== except,
      )
      .groupBy(setting => setting.type)
      .mapValues(items =>
        _.reduce(items, (count, item) => count + item.weight, 0),
      )
      .value();
  }

  async function openEditDialog(affinity = null) {
    const { updated } = await rbModal
      .show({
        title: translateService.get(
          affinity ? 'affinity_setting_edit' : 'affinity_setting_add',
        ),
        component: 'rbServiceEditAffinityDialog',
        locals: {
          serviceLabels: vm.serviceLabels || [],
          nodeLabels: _.chain(vm.nodeLabels || [])
            .map(label => label.key)
            .uniq()
            .concat(['kubernetes.io/hostname'])
            .value(),
          affinity,
          weightCounts: advancedSettingsWeightCount(affinity),
        },
      })
      .catch(error => ({ error }));

    if (updated) {
      if (!affinity) {
        vm.advancedSettings.push(updated);
      } else {
        vm.advancedSettings.splice(
          vm.advancedSettings.indexOf(affinity),
          1,
          updated,
        );
      }
      setViewValue();
    }
  }
}
