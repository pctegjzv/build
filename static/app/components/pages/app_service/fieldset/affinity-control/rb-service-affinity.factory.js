angular
  .module('app.components.pages')
  .factory('rbServiceAffinityService', rbServiceAffinityService);

const types = {
  Affinity: 'Affinity',
  AntiAffinity: 'AntiAffinity',
};

const classifications = {
  Required: 'Required',
  Preferred: 'Preferred',
};

const operators = {
  In: 'In',
  NotIn: 'NotIn',
  Exists: 'Exists',
  DoesNotExist: 'DoesNotExist',
};

function rbServiceAffinityService() {
  return {
    toState,
    toValue,
    valueToSettings,
    types,
    classifications,
    operators,
  };

  function toState(value) {
    const defaultState = {
      mode: 'standard',
      affinityServices: [],
      antiAffinityServices: [],
      advancedSettings: [],
    };

    const standard = valueToStandard(value);
    if (standard) {
      return {
        ...defaultState,
        ...standard,
      };
    } else {
      return {
        ...defaultState,
        mode: 'advanced',
        advancedSettings: valueToSettings(value),
      };
    }
  }

  function valueToStandard(value) {
    const standardDefault = {
      affinityServices: [],
      antiAffinityServices: [],
    };

    if (!value) {
      return standardDefault;
    }

    const { pod } = value;

    if (!pod) {
      return standardDefault;
    }

    const { podAffinity, podAntiAffinity } = pod;

    if (!podAffinity || !podAntiAffinity) {
      return standardDefault;
    }

    if (
      podAffinity.preferredDuringSchedulingIgnoredDuringExecution &&
      podAffinity.preferredDuringSchedulingIgnoredDuringExecution.length
    ) {
      return null;
    }

    if (
      podAntiAffinity.preferredDuringSchedulingIgnoredDuringExecution &&
      podAntiAffinity.preferredDuringSchedulingIgnoredDuringExecution.length
    ) {
      return null;
    }

    const affinityServices = configsToServices(
      podAffinity.requiredDuringSchedulingIgnoredDuringExecution,
    );

    const antiAffinityServices = configsToServices(
      podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution,
    );

    if (!affinityServices || !antiAffinityServices) {
      return null;
    }

    return {
      affinityServices,
      antiAffinityServices,
    };
  }

  function configsToServices(configs) {
    if (!configs) {
      return [];
    }

    if (configs.length > 1) {
      return null;
    }

    const matchExpressions = _.get(
      _.head(configs),
      'labelSelector.matchExpressions',
      [],
    );

    if (
      configs.length === 1 &&
      _.get(_.head(configs), 'topologyKey') !== 'kubernetes.io/hostname'
    ) {
      return null;
    }

    if (
      matchExpressions.some(
        expression => expression.key !== 'alauda_service_id',
      )
    ) {
      return null;
    }

    return _.flatMap(matchExpressions, expression => expression.values);
  }

  function valueToSettings(value) {
    return _.chain(value.pod)
      .pick(['podAffinity', 'podAntiAffinity'])
      .mapKeys(
        (_, key) =>
          key === 'podAffinity' ? types.Affinity : types.AntiAffinity,
      )
      .toPairs()
      .flatMap(([type, obj]) => {
        return _.chain(obj)
          .mapKeys(
            (_, key) =>
              key === 'preferredDuringSchedulingIgnoredDuringExecution'
                ? classifications.Preferred
                : classifications.Required,
          )
          .toPairs()
          .flatMap(([classification, configs]) => {
            return _.chain(configs)
              .map(config => configToSetting(config, type, classification))
              .value();
          })
          .value();
      })
      .value();
  }

  function configToSetting(config, type, classification) {
    const wrap =
      classification === classifications.Preferred ? 'podAffinityTerm.' : '';

    const matchExpressions = _.get(
      config,
      `${wrap}labelSelector.matchExpressions`,
    );
    const nodeLabel = _.get(config, `${wrap}topologyKey`);
    const weight = _.get(config, 'weight');
    const operator = _.head(matchExpressions).operator;
    if ([operators.Exists, operators.DoesNotExist].includes(operator)) {
      return {
        type,
        classification,
        operator,
        nodeLabel,
        weight,
        serviceKey: matchExpressions.map(expression => expression.key),
        serviceLabel: [],
      };
    } else {
      return {
        type,
        classification,
        operator,
        nodeLabel,
        weight,
        serviceKey: [],
        serviceLabel: _.flatMap(matchExpressions, expression =>
          expression.values.map(value => `${expression.key}:${value}`),
        ),
      };
    }
  }

  function toValue(
    mode,
    affinityServices,
    antiAffinityServices,
    advancedSettings,
  ) {
    if (mode === 'standard') {
      return standardToValue(affinityServices, antiAffinityServices);
    } else {
      return advancedToValue(advancedSettings);
    }
  }

  function servicesToPodAffinity(services) {
    const matchExpressions = _.chain(services)
      .partition(service => service === 'self')
      .filter(partition => !!partition.length)
      .map(partition => (_.head(partition) === 'self' ? [] : partition))
      .map(values => matchExpression('alauda_service_id', operators.In, values))
      .value();

    return matchExpressions.length
      ? podAffinity(
          [requiredSetting(matchExpressions, 'kubernetes.io/hostname')],
          [],
        )
      : {};
  }

  function standardToValue(affinityServices, antiAffinityServices) {
    return kubeConfig(
      servicesToPodAffinity(affinityServices),
      servicesToPodAffinity(antiAffinityServices),
    );
  }

  function advancedToValue(advancedSettings) {
    const pod = _.chain(advancedSettings)
      .groupBy(setting => setting.type)
      .mapKeys(
        (_, key) =>
          key === types.Affinity ? 'podAffinity' : 'podAntiAffinity',
      )
      .mapValues(settings => {
        const groups = _.chain(settings)
          .groupBy(setting => setting.classification)
          .mapValues(values => values.map(transformAdvancedSetting))
          .value();
        return podAffinity(
          groups[classifications.Required] || [],
          groups[classifications.Preferred] || [],
        );
      })
      .value();

    return {
      pod,
    };
  }

  function transformAdvancedSetting(setting) {
    const matchExpressions = _.chain(takeServiceLabels(setting))
      .map(label => {
        const [key, value] = label.split(':');
        return { key: key.trim(), value: value.trim() };
      })
      .groupBy(keyValue => keyValue.key)
      .mapValues(group => group.map(keyValue => keyValue.value))
      .toPairs()
      .map(([key, values]) => matchExpression(key, setting.operator, values))
      .value();

    return setting.classification === classifications.Preferred
      ? preferredSetting(matchExpressions, setting.nodeLabel, setting.weight)
      : requiredSetting(matchExpressions, setting.nodeLabel);
  }

  function takeServiceLabels(setting) {
    return [operators.Exists, operators.DoesNotExist].includes(setting.operator)
      ? setting.serviceKey.map(key => `${key}:none`)
      : setting.serviceLabel;
  }
}

const matchExpression = (key, operator, values) => {
  if ([operators.Exists, operators.DoesNotExist].includes(operator)) {
    return {
      key,
      operator,
    };
  }

  return {
    key,
    operator,
    values,
  };
};

const requiredSetting = (matchExpressions, topologyKey) => ({
  labelSelector: {
    matchExpressions,
  },
  topologyKey,
});

const preferredSetting = (matchExpressions, topologyKey, weight) => ({
  podAffinityTerm: {
    labelSelector: {
      matchExpressions,
    },
    topologyKey,
  },
  weight,
});

const podAffinity = (
  requiredDuringSchedulingIgnoredDuringExecution,
  preferredDuringSchedulingIgnoredDuringExecution,
) => ({
  requiredDuringSchedulingIgnoredDuringExecution,
  preferredDuringSchedulingIgnoredDuringExecution,
});

const kubeConfig = (podAffinity, podAntiAffinity) => ({
  pod: {
    podAffinity,
    podAntiAffinity,
  },
});
