/**
 * Created by liudong on 2016/11/23.
 */
const templateStr = require('app/components/pages/app_service/fieldset/alauda-load-balancers/rb-service-fieldset-alb.html');
import _ from 'lodash';
angular.module('app.components.pages').component('rbServiceFieldsetAlb', {
  bindings: {
    loadBalancers: '<', // output load_balancers to service model
    albList: '<',
    containerPorts: '<',
  },
  controller: rbServiceFieldsetAlbController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbServiceFieldsetAlbController(
  ENVIRONMENTS,
  LOAD_BALANCER_PROTOCOLS,
  rbLoadBalancerUtilities,
) {
  const vm = this;
  vm.loadBalancerProtocols = LOAD_BALANCER_PROTOCOLS;
  vm.$onInit = _init;
  vm.$onChanges = _onChanges;

  vm.onLoadbalancerChange = onLoadbalancerChange;
  vm.onContainerPortChange = onContainerPortChange;
  vm.onProtocolChange = onProtocolChange;
  vm.addLoadbalancer = addLoadbalancer;
  vm.deleteLoadbalancer = deleteLoadbalancer;
  vm.addListener = addListener;
  vm.deleteListener = deleteListener;

  //////////
  function _init() {
    if (
      !vm.loadBalancers.length &&
      !ENVIRONMENTS['is_private_deploy_enabled']
    ) {
      addLoadbalancer();
    }
  }

  function _onChanges() {}

  function onLoadbalancerChange(item, option) {
    vm.loadBalancerDetails || (vm.loadBalancerDetails = {});
    vm.loadBalancerDetails[item.uniqueId] = _.cloneDeep(option);
    item.load_balancer_id = option.load_balancer_id;
    item.name = option.name;
    item.type = option.type;
    item.version = option.version;
    vm.loadBalancerProtocols = rbLoadBalancerUtilities.supportProtocols(item);
  }

  function onContainerPortChange(listener, option) {
    listener.container_port = option.value;
    // 如果是80，则默认改协议为http，否则默认改协议为tcp
    listener.protocol = listener.container_port === 80 ? 'http' : 'tcp';
  }

  function onProtocolChange(listener, option) {
    listener.protocol = option.value;
  }

  function deleteLoadbalancer($index) {
    vm.loadBalancers.splice($index, 1);
  }

  function addLoadbalancer() {
    vm.loadBalancers.push({
      load_balancer_id: '',
      uniqueId: _.uniqueId('load_balancer_id'),
      type: '',
      listeners: [],
    });
  }

  function addListener(item) {
    const containerPort = vm.containerPorts.length ? vm.containerPorts[0] : '';
    const protocol = containerPort === 80 ? 'http' : 'tcp';
    item.listeners.push({
      listener_port: '',
      container_port: containerPort,
      protocol,
    });
  }

  function deleteListener(item, index) {
    item.listeners.splice(index, 1);
  }
}
