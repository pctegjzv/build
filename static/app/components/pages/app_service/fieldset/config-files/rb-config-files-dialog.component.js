const templateStr = require('app/components/pages/app_service/fieldset/config-files/rb-config-files-dialog.html');
angular.module('app.components.pages').component('rbConfigFilesDialog', {
  controller: rbConfigFilesDialogController,
  controllerAs: 'vm',
  bindings: {
    configFile: '<',
    paths: '<',
    cacheConfigs: '<', // cache data,
    disableRaw: '<',
  },
  template: templateStr,
});

function rbConfigFilesDialogController(
  rbModal,
  rbGlobalSetting,
  $timeout,
  translateService,
  rbConfigurationService,
) {
  const vm = this;
  vm.selectMountFileType = selectMountFileType;
  vm.onConfigChange = onConfigChange;
  vm.onKeyChange = onKeyChange;
  vm.previewCode = previewCode;
  vm.$onInit = _init;
  vm.confirm = () => {
    vm.form.$setSubmitted();
    if (vm.form.$invalid) {
      return;
    }
    if (vm.curConfig.type === 'raw' && !vm.curConfig.rawValue) {
      return;
    }
    if (vm.disableRaw) {
      vm.curConfig.type = 'config';
    }
    rbModal.hide(vm.curConfig);
  };
  vm.cancel = () => {
    rbModal.cancel();
  };
  vm.handlePathPattern = {
    test: value => {
      if (vm.paths.includes(value)) {
        vm.pathPatternHint = 'file_path_repeated';
        return false;
      } else if (!/^\/.*[^/]$/.test(value)) {
        vm.pathPatternHint = 'file_path_invalid';
        return false;
      }
      return true;
    },
  };
  //////////

  async function _init() {
    vm.mountTypes = [
      translateService.get('manual'),
      translateService.get('select'),
    ];
    vm.selectedMountType = vm.disableRaw ? vm.mountTypes[1] : vm.mountTypes[0];
    vm.editorOptions = rbGlobalSetting.getEditorOptions('yaml');
    vm.configs = vm.cacheConfigs
      ? vm.cacheConfigs.configs.map(item => {
          item.display_name = item.space_name
            ? `${item.name}(${item.space_name})`
            : item.name;
          return item;
        })
      : [];
    vm.keys = vm.cacheConfigs ? vm.cacheConfigs.keys : [];
    vm.curConfig = _.cloneDeep(vm.configFile) || {};
    if (!vm.configFile) {
      vm.isNew = true;
    }
    vm.initType =
      vm.curConfig.type === 'raw' ? vm.mountTypes[0] : vm.mountTypes[1];
    $timeout(() => {
      vm.codeMirrorRefresh = true;
    }, 200);
    if (!vm.cacheConfigs) {
      vm.isConfigsLoading = true;
      const { results } = await rbConfigurationService.configurationList({
        cache: vm.cache,
        page_size: 9999,
      });
      vm.isConfigsLoading = false;
      vm.configs = results.map(item => {
        item.display_name = item.space_name
          ? `${item.name}(${item.space_name})`
          : item.name;
        return item;
      });
    }
  }

  function selectMountFileType(option) {
    vm.selectedMountType = option.value;
    if (option.value === vm.mountTypes[0]) {
      vm.curConfig.type = 'raw';
    } else {
      vm.curConfig.type = 'config';
    }
  }

  async function onConfigChange(option) {
    if (!option.id) {
      return;
    }
    if (_.get(vm.cacheConfigs, 'keys')) {
      vm.cacheConfigs.keys = null;
      return;
    }
    vm.curConfig.config = option.name;
    vm.curConfig.uuid = option.id;
    vm.isKeysLoading = true;
    const { content } = await rbConfigurationService.configurationDetail(
      option.id,
    );
    vm.keys = content;
    if (!content.length) {
      vm.keys = [];
      vm.curConfig.key = null;
    }
    vm.isKeysLoading = false;
    $timeout();
  }

  function onKeyChange(option) {
    if (!option.value) {
      return;
    }
    vm.curConfig.value = option.value;
    vm.curConfig.key = option.key;
  }

  function previewCode() {
    const content = vm.curConfig.value;

    function _previewCode() {
      return rbModal.show({
        locals: { content },
        template: '<rb-config-preview content="content"></rb-config-preview>',
        closable: true,
        autofocus: true,
      });
    }

    rbModal.hide({
      show: _previewCode,
      item: vm.curConfig,
      cacheConfigs: {
        configs: vm.configs,
        keys: vm.keys,
      },
    });
  }
}
