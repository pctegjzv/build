const templateStr = require('app/components/pages/app_service/fieldset/config-files/rb-config-preview/rb-config-preview.html');
angular.module('app.components.pages').component('rbConfigPreview', {
  controller: rbConfigPreviewController,
  controllerAs: 'vm',
  bindings: {
    content: '<',
  },
  template: templateStr,
});

function rbConfigPreviewController($timeout, rbModal, rbGlobalSetting) {
  const vm = this;
  vm.$onInit = () => {
    vm.editorOptions = _.merge(
      {},
      rbGlobalSetting.getEditorOptions('yaml', 'nocursor'),
    );
    $timeout(() => {
      vm.displayContent = vm.content;
    }, 200);
  };
  vm.$onDestroy = () => {
    rbModal.hide({ closed: true });
  };
}
