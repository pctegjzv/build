const templateStr = require('app/components/pages/app_service/fieldset/config-files/rb-service-fieldset-config-files.html');
angular
  .module('app.components.pages')
  .component('rbServiceFieldsetConfigFiles', {
    controller: rbServiceFieldsetConfigFilesController,
    controllerAs: 'vm',
    bindings: {
      configFiles: '<',
      isDisabled: '<',
      onChange: '&',
      disableRaw: '<',
    },
    template: templateStr,
  });

function rbServiceFieldsetConfigFilesController(
  translateService,
  rbModal,
  $timeout,
  rbConfigurationService,
) {
  const vm = this;
  vm.previewCode = previewCode;
  vm.editConfigFilesItem = editConfigFilesItem;
  vm.refreshConfig = true;
  vm.deleteConfigFilesItem = $index => {
    vm.configFiles.splice($index, 1);
    _onChangeCb();
  };

  async function previewCode(item) {
    let code = item.type === 'raw' ? item.rawValue : item.value;
    if (!code) {
      if (!item.config) {
        return;
      } else {
        const { content } = await rbConfigurationService.configurationDetail(
          item.uuid,
        );
        code = _.find(content, itm => itm.key === item.key).value;
      }
    }
    rbModal.show({
      width: 750,
      locals: { code },
      template: '<rb-config-preview content="code"></rb-config-preview>',
      closable: true,
      autofocus: true,
    });
  }

  async function editConfigFilesItem($index, item, cacheConfigs) {
    try {
      const data = await rbModal.show({
        width: 750,
        title: translateService.get('add_config_files_item'),
        locals: {
          configFile: item,
          paths: _.filter(
            _.map(vm.configFiles, 'path'),
            (itm, idx) => idx !== $index,
          ),
          disableRaw: vm.disableRaw,
          cacheConfigs,
        },
        template:
          '<rb-config-files-dialog ' +
          'paths="paths" config-file="configFile" cache-configs="cacheConfigs" disable-raw="disableRaw">' +
          '</rb-config-files-dialog>',
      });
      if (data.show && data.item) {
        try {
          await data.show();
          editConfigFilesItem($index, data.item, data.cacheConfigs);
        } catch (e) {
          // NO ops when cancel
        }
      } else {
        if (_.isNumber($index)) {
          vm.configFiles[$index] = data;
        } else {
          // create new file path config
          if (!_.find(vm.configFiles, { path: data.path })) {
            vm.configFiles.push(data);
          }
        }
        _onChangeCb();
        $timeout();
      }
    } catch (e) {
      // NO ops when cancel
    }
  }

  function _onChangeCb() {
    if (vm.onChange) {
      vm.onChange({ config: vm.configFiles });
    }
  }
}
