/**
 * Created by liudong on 16/9/4.
 */
const { SERVICE_NETWORK_MODES } = require('../../rb-service.constant');
const templateStr = require('app/components/pages/app_service/fieldset/container-ports/rb-service-fieldset-container-ports.html');
angular
  .module('app.components.pages')
  .component('rbServiceFieldsetContainerPorts', {
    controller: rbServiceFieldsetContainerPortsController,
    controllerAs: 'vm',
    bindings: {
      networkMode: '<',
      ports: '<',
    },
    template: templateStr,
  });

function rbServiceFieldsetContainerPortsController(rbToast, translateService) {
  const vm = this;

  vm.$onInit = _init;
  vm.addPort = addPort;
  vm.deletePort = deletePort;

  //////////

  function _init() {
    if (vm.ports.length === 0) {
      addPort();
    }
  }

  function addPort() {
    vm.ports.push('');
  }

  function deletePort(index) {
    const remainingPorts = _.compact(vm.ports);
    if (
      vm.networkMode === SERVICE_NETWORK_MODES.HOST &&
      remainingPorts.length === 1 &&
      vm.ports[index]
    ) {
      rbToast.warning(translateService.get('host_mode_container_port_need'));
      return;
    }
    vm.ports.splice(index, 1);
  }
}
