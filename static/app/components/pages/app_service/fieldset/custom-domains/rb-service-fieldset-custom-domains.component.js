/**
 * Created by liudong on 16/9/5.
 */
const templateStr = require('app/components/pages/app_service/fieldset/custom-domains/rb-service-fieldset-custom-domains.html');
angular
  .module('app.components.pages')
  .component('rbServiceFieldsetCustomDomains', {
    controller: rbServiceFieldsetCustomDomainsController,
    controllerAs: 'vm',
    bindings: {
      customDomains: '<',
    },
    template: templateStr,
  });

function rbServiceFieldsetCustomDomainsController() {
  const vm = this;

  vm.$onInit = _init;
  vm.addDomain = addDomain;
  vm.deleteDomain = deleteDomain;

  //////////

  function _init() {
    if (vm.customDomains.length === 0) {
      addDomain();
    }
  }

  function addDomain() {
    vm.customDomains.push('');
  }

  function deleteDomain(index) {
    vm.customDomains.splice(index, 1);
  }
}
