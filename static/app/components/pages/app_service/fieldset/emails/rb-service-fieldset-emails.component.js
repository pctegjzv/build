const templateStr = require('app/components/pages/app_service/fieldset/emails/rb-service-fieldset-emails.html');
angular.module('app.components.pages').component('rbServiceFieldsetEmails', {
  controller: rbServiceFieldsetEmailsController,
  controllerAs: 'vm',
  bindings: {
    emails: '<',
    isDisabled: '<',
  },
  template: templateStr,
});

function rbServiceFieldsetEmailsController() {
  const vm = this;

  vm.$onInit = _init;
  vm.addEmail = addEmail;
  vm.deleteEmail = deleteEmail;

  //////////

  function _init() {
    if (vm.emails && vm.emails.length === 0) {
      addEmail();
    }
  }

  function addEmail() {
    vm.emails.push({
      email: '',
    });
  }

  function deleteEmail(index) {
    vm.emails.splice(index, 1);
  }
}
