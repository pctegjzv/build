/**
 * Created by liudong on 2017/1/3.
 */
const templateStr = require('app/components/pages/app_service/fieldset/env-files/rb-envfile-preview.html');
angular.module('app.components.pages').component('rbEnvfilePreview', {
  controller: rbEnvfilePreviewController,
  controllerAs: 'vm',
  bindings: {
    content: '<',
  },
  template: templateStr,
});

function rbEnvfilePreviewController(rbGlobalSetting, $timeout) {
  const vm = this;
  vm.$onInit = () => {
    vm.editorOptions = rbGlobalSetting.getEditorOptions('yaml', true);
    vm.editorOptions.lineNumbers = false;
    vm.initialized = false;
    const lines = [];
    vm.content.forEach(row => {
      lines.push(row.join('='));
    });
    const content = lines.join('\n');
    $timeout(() => {
      vm.displayContent = content;
      vm.initialized = true;
    }, 300);
  };
}
