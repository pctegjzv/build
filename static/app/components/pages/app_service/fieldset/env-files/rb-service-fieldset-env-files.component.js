/**
 * Created by liudong on 16/9/5.
 */
const templateStr = require('app/components/pages/app_service/fieldset/env-files/rb-service-fieldset-env-files.html');
angular.module('app.components.pages').component('rbServiceFieldsetEnvFiles', {
  controller: rbServiceFieldsetEnvFilesController,
  controllerAs: 'vm',
  bindings: {
    update: '@',
    envFiles: '<',
  },
  template: templateStr,
});

function rbServiceFieldsetEnvFilesController(
  envfileService,
  $uibModal,
  rbModal,
) {
  const vm = this;
  vm.fileList = [];
  vm.fileLoading = false;

  vm.$onInit = _init;
  vm.onFileChange = onFileChange;
  vm.addFile = addFile;
  vm.viewFile = viewFile;
  vm.deleteFile = deleteFile;
  vm.getColspan = getColspan;

  //////////

  function _init() {
    // vm.envFileList: avaliable envfiles
    vm.fileLoading = true;
    envfileService
      .envfileList()
      .then(data => {
        vm.fileList =
          data['envfiles'].map(item => {
            item.display_name = item.space_name
              ? `${item.name}(${item.space_name})`
              : item.name;
            return item;
          }) || [];
      })
      .catch(() => {})
      .then(() => {
        vm.fileLoading = false;
      });
  }

  function onFileChange(option, item) {
    item.name = option.name;
    item.uuid = option.uuid;
    item.file = option;
  }

  function addFile() {
    const file = {
      name: '',
    };
    if (vm.update) {
      file.update = false;
    }
    vm.envFiles.push(file);
  }

  function deleteFile(index) {
    vm.envFiles.splice(index, 1);
  }

  function viewFile(item) {
    rbModal.show({
      locals: {
        content: item.file.content,
      },
      template: `<rb-envfile-preview content="content">
</rb-envfile-preview>`,
      closable: true,
      autofocus: true,
    });
  }

  function getColspan() {
    return vm.update ? 2 : 1;
  }
}
