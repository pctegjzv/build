/**
 * Created by liudong on 16/9/5.
 */
const templateStr = require('app/components/pages/app_service/fieldset/env-vars/rb-service-fieldset-env-vars.html');
angular.module('app.components.pages').component('rbServiceFieldsetEnvVars', {
  controller: rbServiceFieldsetEnvVarsController,
  controllerAs: 'vm',
  bindings: {
    envVars: '<',
    isDisabled: '<',
  },
  template: templateStr,
});

function rbServiceFieldsetEnvVarsController() {
  const vm = this;

  vm.$onInit = _init;
  vm.addEnvVar = addEnvVar;
  vm.deleteEnvVar = deleteEnvVar;

  //////////

  function _init() {
    if (vm.envVars && vm.envVars.length === 0) {
      addEnvVar();
    }
  }

  function addEnvVar() {
    vm.envVars.push({
      name: '',
      value: '',
    });
  }

  function deleteEnvVar(index) {
    vm.envVars.splice(index, 1);
  }
}
