/**
 * Created by liudong on 2017/1/3.
 */
const templateStr = require('app/components/pages/app_service/fieldset/healthcheck/rb-healthcheck-dialog.html');
angular.module('app.components.pages').component('rbHealthcheckDialog', {
  controller: rbHealthcheckDialogController,
  controllerAs: 'vm',
  bindings: {
    item: '<',
    index: '<',
    list: '<',
  },
  template: templateStr,
});

function rbHealthcheckDialogController($scope, rbModal, translateService) {
  const vm = this;
  let list;

  vm.$onInit = _init;
  //////////
  function _init() {
    list = vm.list; // used to check healthcheck item duplication(port or command)
    vm.healthcheckPortInvalid = false;
    vm.healthcheckCommandInvalid = false;

    vm.healthCheckTypes = ['HTTP', 'TCP'];

    // field help text
    vm.hint = {
      // port: 'xxx',
      command: translateService.get('health_check_hint_command'),
      // path: '',
      grace_period_seconds: translateService.get(
        'health_check_hint_grace_period_seconds',
      ),
      interval_seconds: translateService.get(
        'health_check_hint_interval_seconds',
      ),
      max_consecutive_failures: translateService.get(
        'health_check_hint_max_consecutive_failures',
      ),
      timeout_seconds: translateService.get('health_check_hint_timeout'),
      ignore_http1xx: translateService.get('health_check_hint_ignore_http1xx'),
    };

    if (vm.item) {
      vm.healthcheck = _.cloneDeep(vm.item);
    } else {
      vm.healthcheck = {
        protocol: 'HTTP',
        port: 80,
        command: '',
        path: '/',
        grace_period_seconds: 100,
        interval_seconds: 60,
        max_consecutive_failures: 0,
        timeout_seconds: 20,
        ignore_http1xx: false,
      };
    }
  }

  vm.selectHealthcheckType = option => {
    vm.healthcheck.protocol = option.value;
    if (option.value === 'HTTP' && !vm.healthcheck.path) {
      vm.healthcheck.path = '/';
    }
  };

  vm.confirm = () => {
    vm.healthcheckPortInvalid = false;
    vm.healthcheckCommandInvalid = false;

    vm.healthcheckForm.$setSubmitted();
    if (vm.healthcheckForm.$invalid) {
      return;
    }
    if (!healthcheckValidate()) {
      return;
    }
    switch (vm.healthcheck.protocol) {
      case 'HTTP':
        delete vm.healthcheck.command;
        break;
      case 'TCP':
        delete vm.healthcheck.command;
        delete vm.healthcheck.path;
        break;
      case 'COMMAND':
        delete vm.healthcheck.port;
        delete vm.healthcheck.path;
        break;
    }
    rbModal.hide(vm.healthcheck);
  };

  vm.cancel = () => {
    rbModal.cancel();
  };

  $scope.$watch(
    () => {
      return vm.healthcheck.port;
    },
    _.debounce(() => {
      $scope.$evalAsync(() => {
        vm.healthcheckPortInvalid = false;
      });
    }, 500),
  );

  $scope.$watch(
    () => {
      return vm.healthcheck.command;
    },
    _.debounce(() => {
      $scope.$evalAsync(() => {
        vm.healthcheckCommandInvalid = false;
      });
    }, 500),
  );

  function healthcheckValidate() {
    let valid = true;
    _.forEach(list, (item, index) => {
      if (item.protocol === vm.healthcheck.protocol) {
        switch (vm.healthcheck.protocol) {
          case 'HTTP':
          case 'TCP':
            if (vm.healthcheck.port === item.port) {
              if (vm.index === index) {
                break;
              }
              vm.healthcheckPortInvalid = true;
              valid = false;
            }
            break;
          case 'COMMAND':
            if (vm.healthcheck.command === item.command) {
              if (vm.index === index) {
                break;
              }
              vm.healthcheckCommandInvalid = true;
              valid = false;
            }
            break;
        }
      }
    });
    return valid;
  }
}
