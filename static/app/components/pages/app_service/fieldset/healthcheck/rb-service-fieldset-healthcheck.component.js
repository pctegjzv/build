const templateStr = require('app/components/pages/app_service/fieldset/healthcheck/rb-service-fieldset-healthcheck.html');
angular
  .module('app.components.pages')
  .component('rbServiceFieldsetHealthcheck', {
    controller: rbServiceFieldsetHealthcheckController,
    controllerAs: 'vm',
    bindings: {
      healthChecks: '<',
      detail: '@',
    },
    template: templateStr,
  });

function rbServiceFieldsetHealthcheckController(translateService, rbModal) {
  const vm = this;

  vm.addHealthcheckItem = () => {
    rbModal
      .show({
        title: translateService.get('add_healthcheck_item'),
        locals: {
          item: null,
          list: vm.healthChecks,
        },
        template:
          '<rb-healthcheck-dialog item="item" list="list"></rb-healthcheck-dialog>',
      })
      .then(item => {
        vm.healthChecks.push(item);
      })
      .catch(() => {});
  };

  vm.editHealthcheckItem = (index, item) => {
    rbModal
      .show({
        title: translateService.get('edit_healthcheck_item'),
        locals: {
          item: item,
          index: index,
          list: vm.healthChecks,
        },
        template:
          '<rb-healthcheck-dialog item="item" list="list" index="index"></rb-healthcheck-dialog>',
      })
      .then(item => {
        vm.healthChecks[index] = item;
      })
      .catch(() => {});
  };

  vm.deleteHealthcheckItem = index => {
    vm.healthChecks.splice(index, 1);
  };
}
