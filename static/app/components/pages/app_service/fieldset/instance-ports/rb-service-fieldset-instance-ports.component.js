/**
 * Created by liudong on 16/9/4.
 */
const { SERVICE_NETWORK_MODES } = require('../../rb-service.constant');
const templateStr = require('app/components/pages/app_service/fieldset/instance-ports/rb-service-fieldset-instance-ports.html');
angular
  .module('app.components.pages')
  .component('rbServiceFieldsetInstancePorts', {
    controller: rbServiceFieldsetInstancePortsController,
    controllerAs: 'vm',
    bindings: {
      internalEndpointEnabled: '<', // if internal endpoint-type enabled, from region features
      instancePorts: '<', // model.instance_ports
      imagePorts: '<', // image exposed ports, from image detail
      ipList: '<', // avaliable region ip list, from region service
      networkMode: '<',
      containerPorts: '<', // container exposed ports
    },
    template: templateStr,
  });

function rbServiceFieldsetInstancePortsController() {
  const vm = this;
  let _imagePorts = [],
    _ipList = [],
    _internalEndpointEnabled;
  vm.clonedContainerPorts = [];

  vm.endpointTypes = [];
  vm.internalIpList = [];
  vm.externalIpList = [];

  vm.$onInit = _init;
  vm.$doCheck = _doCheck;
  vm.addItem = addItem;
  vm.deleteItem = deleteItem;
  vm.onContainerPortChange = onContainerPortChange;
  vm.onEndpointTypeChange = onEndpointTypeChange;
  vm.onIpAddressChange = onIpAddressChange;
  vm.isHostNetwork = isHostNetwork;

  //////////

  function _init() {
    if (!vm.ipList) {
      vm.ipList = [];
    }
    if (vm.instancePorts.length === 0) {
      addItem();
    }
  }

  function _doCheck() {
    // image port list

    if (!_.isEmpty(vm.imagePorts) && _imagePorts !== vm.imagePorts.join(',')) {
      vm.instancePorts.length = 0;
      _.forEach(vm.imagePorts, port => {
        const endpoint_type = port === 80 ? 'http-endpoint' : 'tcp-endpoint';
        const service_port = endpoint_type === 'http-endpoint' ? 80 : '';
        const instancePort = {
          container_port: port,
          endpoint_type,
          protocol: 'tcp',
          ipaddress: 'sharedip',
          service_port,
        };
        // check if ip list already exist, if exist init the ipaddress with iplist
        if (vm.externalIpList.length > 0) {
          instancePort.ipaddress = vm.externalIpList[0];
        }
        vm.instancePorts.push(instancePort);
      });
      _imagePorts = vm.imagePorts.join(',');
    }
    // ip list
    if (!_.isEmpty(vm.ipList) && vm.ipList !== _ipList) {
      _ipList = vm.ipList;
      vm.internalIpList = [];
      vm.externalIpList = [];
      _.forEach(vm.ipList, item => {
        if (item['endpoint_type'] === 'internal') {
          vm.internalIpList.push(item.ipaddress);
        } else if (item['endpoint_type'] === 'external') {
          vm.externalIpList.push(item.ipaddress);
        }
      });
    }
    // internalEndpointEnabled
    if (_internalEndpointEnabled !== vm.internalEndpointEnabled) {
      _internalEndpointEnabled = vm.internalEndpointEnabled;
      if (!vm.internalEndpointEnabled) {
        vm.endpointTypes = [
          {
            name: 'TCP',
            value: 'tcp-endpoint',
          },
          {
            name: 'HTTP',
            value: 'http-endpoint',
          },
        ];
      } else {
        vm.endpointTypes = [
          {
            name: 'TCP',
            value: 'tcp-endpoint',
          },
          {
            name: 'HTTP',
            value: 'http-endpoint',
          },
          {
            name: 'Internal TCP',
            value: 'internal-endpoint',
          },
          {
            name: 'Internal HTTP',
            value: 'internal-http-endpoint',
          },
        ];
      }
    }
    // host mode, only allow container exposed ports for container_port
    if (vm.containerPorts.join('') !== vm.clonedContainerPorts.join('')) {
      vm.clonedContainerPorts = _.compact(vm.containerPorts);
    }
  }

  function isHostNetwork() {
    return vm.networkMode === SERVICE_NETWORK_MODES.HOST;
  }

  function onContainerPortChange(option, item) {
    item.container_port = option.value;
  }

  function onEndpointTypeChange(option, item) {
    item.endpoint_type = option.value;
    if (
      (option.value === 'internal-endpoint' &&
        vm.internalIpList.length === 0) ||
      (option.value !== 'internal-endpoint' && vm.externalIpList.length === 0)
    ) {
      item.ipaddress = 'sharedip';
    }
    if (
      option.value === 'http-endpoint' ||
      option.value === 'internal-http-endpoint'
    ) {
      item.service_port = 80;
    } else {
      item.service_port = item.service_port || '';
    }
  }

  function onIpAddressChange(option, item) {
    item.ipaddress = option.value;
    if (vm.networkMode === SERVICE_NETWORK_MODES.BRIDGE) {
      vm.instancePorts.forEach(_item => {
        _item.ipaddress = option.value;
      });
    }
  }

  function addItem() {
    vm.instancePorts.push({
      container_port: '',
      endpoint_type: 'tcp-endpoint',
      protocol: 'tcp',
      ipaddress: 'sharedip',
      service_port: '',
    });
  }

  function deleteItem(index) {
    vm.instancePorts.splice(index, 1);
  }
}
