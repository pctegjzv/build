/**
 * Created by liudong on 16/9/3.
 */
import { head } from 'lodash';
import { INT_PATTERN } from 'app/components/common/config/common-pattern';

import templateStr from './rb-service-fieldset-instance-size-new.html';

angular
  .module('app.components.pages')
  .component('rbServiceFieldsetInstanceSizeNew', {
    controller: rbServiceFieldsetInstanceSizeController,
    controllerAs: 'vm',
    bindings: {
      initInstanceSize: '<',
      initCustomInstanceSize: '<',
      region: '<',
      onChange: '&',
      disableCustomize: '<',
      minInstanceSize: '<',
    },
    template: templateStr,
  });

function rbServiceFieldsetInstanceSizeController(
  rbServiceInstanceSize,
  $scope,
) {
  const vm = this;
  vm.$onInit = _init;
  vm.$onChanges = _changes;
  vm.onSelect = onSelect;
  vm.onCustomSizeChange = onCustomSizeChange;
  vm.baseUnit = 1;
  vm.unitsOptions = [
    {
      key: 'MB',
      value: 1,
    },
    {
      key: 'GB',
      value: 1024,
    },
  ];
  vm.memDisplay = null;
  vm.onBaseUnitChange = onBaseUnitChange;
  //////////

  $scope.$watch('vm.baseUnit', () => {
    vm.min = getMin();
  });

  function _init() {
    vm.min = getMin();
    vm.resourceNameInt = INT_PATTERN;
    vm.instanceSizes = rbServiceInstanceSize
      .getInstanceSizes()
      .filter(size => size.cpu >= vm.min.cpu && size.mem >= vm.min.mem);

    setStateByInit(vm.initCustomInstanceSize);
    vm.onChange({
      instance_size: 'CUSTOMIZED',
      config: {
        cpu: vm.selected.cpu,
        mem: vm.selected.mem,
      },
    });
  }

  function getMin() {
    const min =
      vm.minInstanceSize || rbServiceInstanceSize.getMinInstanceSize();
    return vm.baseUnit === 1 ? min : Object.assign({}, min, { mem: 1 });
  }

  function onBaseUnitChange({ value }) {
    if (vm.baseUnit !== value) {
      vm.baseUnit = value;
      onCustomSizeChange();
    }
  }

  function setStateByInit(init) {
    vm.selected = init
      ? (vm.instanceSizes || []).find(
          size => size.cpu === init.cpu && size.mem === init.mem,
        )
      : head(vm.instanceSizes);

    vm.customSize = Object.assign({}, vm.selected ? vm.min : init);

    if (vm.customSize.mem && vm.customSize.mem % 1024 === 0) {
      vm.baseUnit = 1024;
    }
    vm.memDisplay = vm.customSize.mem / vm.baseUnit;
  }

  function _changes({ initCustomInstanceSize }) {
    if (initCustomInstanceSize && initCustomInstanceSize.currentValue) {
      setStateByInit(initCustomInstanceSize.currentValue);
    }
  }

  function onSelect(option = null) {
    vm.selected = option;

    vm.onChange({
      instance_size: 'CUSTOMIZED',
      config: {
        cpu: vm[vm.selected ? 'selected' : 'customSize'].cpu,
        mem: vm[vm.selected ? 'selected' : 'customSize'].mem,
      },
    });
  }

  function onCustomSizeChange() {
    vm.memDisplay && (vm.customSize.mem = vm.memDisplay * vm.baseUnit);

    vm.onChange({
      instance_size: 'CUSTOMIZED',
      config: {
        cpu: vm.customSize.cpu,
        mem: vm.customSize.mem,
      },
    });
  }
}
