/**
 * Created by liudong on 16/9/3.
 */
import _ from 'lodash';
import { INSTANCE_SIZE_INFO } from '../../rb-service.constant';

import templateStr from './rb-service-fieldset-instance-size.html';

angular
  .module('app.components.pages')
  .component('rbServiceFieldsetInstanceSize', {
    controller: rbServiceFieldsetInstanceSizeController,
    controllerAs: 'vm',
    bindings: {
      initInstanceSize: '<',
      initCustomInstanceSize: '<',
      region: '<',
      onChange: '&',
      disableCustomize: '<',
      instanceSizeList: '<',
    },
    template: templateStr,
  });

function rbServiceFieldsetInstanceSizeController(
  $scope,
  translateService,
  $timeout,
) {
  let _initInstanceSize;
  let cpuStepsArray, memStepsArray;
  let _disableCustomize;
  const vm = this;

  const cpuUnit = translateService.get('unit_cpu');
  // form control
  vm.instance_size = 'XXS';
  // form control
  vm.custom_instance_size = {
    mem: '',
    cpu: '',
  };
  vm.$onInit = _init;
  vm.$doCheck = _doCheck;
  vm.$onChanges = _onChanges;

  vm.selectInstanceSize = selectInstanceSize;

  //////////

  function _init() {
    _disableCustomize = !!vm.disableCustomize;
    vm.instanceSizeInfo = JSON.parse(
      JSON.stringify(INSTANCE_SIZE_INFO).replace(/cpuUnit/g, cpuUnit),
    );
    // in case `region` input is empty
    initAvailableInstanceSize({});
  }

  function _doCheck() {
    if (vm.initInstanceSize && _initInstanceSize !== vm.initInstanceSize) {
      vm.instance_size = _initInstanceSize = vm.initInstanceSize;
      if (vm.instance_size === 'CUSTOMIZED') {
        vm.custom_instance_size.cpu = vm.initCustomInstanceSize.cpu;
        vm.custom_instance_size.mem = vm.initCustomInstanceSize.mem;
        vm.customizedMem = _.filter(memStepsArray, o => {
          return o.value === vm.initCustomInstanceSize.mem;
        })[0].legend;
        vm.customizedCpu = _.filter(cpuStepsArray, o => {
          return o.value === vm.initCustomInstanceSize.cpu;
        })[0].legend;
        $timeout(() => {
          $scope.$broadcast('rzSliderForceRender');
        });
      } else {
        vm.customizedMem = memStepsArray[0]['legend'];
        vm.customizedCpu = cpuStepsArray[0]['legend'];
      }
    }
  }

  function _serializeSilderMemConfig() {
    const config = [];
    for (let i = 1; i < 129; i++) {
      config.push({
        value: i * 1024,
        legend: i + ' GB',
      });
    }
    return config;
  }

  function _serializeSilderCpuConfig() {
    const config = [];
    for (let i = 1; i < 37; i++) {
      config.push({
        value: i,
        legend: i + cpuUnit,
      });
    }
    return config;
  }

  function selectInstanceSize(instanceSize) {
    vm.instance_size = instanceSize;
    if (instanceSize !== 'CUSTOMIZED') {
      vm.onChange({
        instance_size: instanceSize,
        config: {
          mem: vm.instanceSizeInfo[instanceSize].mem,
          cpu: vm.instanceSizeInfo[instanceSize].cpu,
        },
      });
    } else {
      vm.onChange({
        instance_size: instanceSize,
        config: {
          mem: vm.custom_instance_size.mem,
          cpu: vm.custom_instance_size.cpu,
        },
      });
    }
  }

  function _onChanges({ region }) {
    if (region && region.currentValue) {
      initAvailableInstanceSize(region.currentValue);
    }
  }

  // init instance size by region: https://bitbucket.org/mathildetech/atropos/wiki/design/claas/region-model#markdown-header-size
  function initAvailableInstanceSize(region) {
    if (region) {
      vm.instanceSizeList = vm.instanceSizeList || [
        'XXS',
        'XS',
        'S',
        'M',
        'L',
        'XL',
      ];
      cpuStepsArray = _.union(
        [
          { value: 0.125, legend: `0.125 ${cpuUnit}` },
          { value: 0.25, legend: `0.25 ${cpuUnit}` },
          { value: 0.5, legend: `0.5 ${cpuUnit}` },
        ],
        _serializeSilderCpuConfig(),
      );
      memStepsArray = _.union(
        [
          { value: 64, legend: '64 MB' },
          { value: 128, legend: '128 MB' },
          { value: 256, legend: '256 MB' },
          { value: 512, legend: '512 MB' },
        ],
        _serializeSilderMemConfig(),
      );
      const isAvailableInstanceFromRegion = _.get(
        region,
        'features.service.size',
        false,
      );
      if (isAvailableInstanceFromRegion) {
        const availableFixedInstanceSize = _.get(
          region,
          'features.service.size.fixed',
          null,
        );
        const availableCustomInstanceSize = _.get(
          region,
          'features.service.size.custom',
          null,
        );

        if (availableFixedInstanceSize) {
          const filterFixedSize = Object.keys(availableFixedInstanceSize);
          vm.instanceSizeList = vm.instanceSizeList.filter(item =>
            filterFixedSize.includes(item),
          );
        } else {
          vm.instanceSizeList = [];
        }

        if (availableCustomInstanceSize) {
          const maxCpu = _.get(availableCustomInstanceSize, 'cpu.max', 36);
          const minCpu = _.get(availableCustomInstanceSize, 'cpu.min', 0.125);
          const maxMem = _.get(
            availableCustomInstanceSize,
            'mem.max',
            128 * 1024,
          );
          const minMem = _.get(availableCustomInstanceSize, 'mem.min', 64);

          cpuStepsArray = cpuStepsArray.filter(item => {
            return item.value >= minCpu && item.value <= maxCpu;
          });
          memStepsArray = memStepsArray.filter(item => {
            return item.value >= minMem && item.value <= maxMem;
          });
          vm.disableCustomize = _disableCustomize;
        } else {
          vm.disableCustomize = true;
        }
      } else {
        vm.disableCustomize = _disableCustomize;
      }
      vm.instance_slider_config = {
        cpu: {
          ceil: cpuStepsArray[cpuStepsArray.length - 1].value,
          floor: cpuStepsArray[0].value,
          hideLimitLabels: true,
          translate: value => {
            const filter = _.filter(cpuStepsArray, o => {
              return o.value === value;
            })[0];
            return filter ? filter.legend : '';
          },
          hidePointerLabels: true,
          stepsArray: cpuStepsArray,
          onChange: () => {
            $scope.$evalAsync(() => {
              vm.onChange({
                instance_size: 'CUSTOMIZED',
                config: {
                  cpu: vm.custom_instance_size.cpu,
                  mem: vm.custom_instance_size.mem,
                },
              });
            });
            vm.customizedCpu = _.filter(cpuStepsArray, o => {
              return o.value === vm.custom_instance_size.cpu;
            })[0].legend;
          },
          showSelectionBar: true,
        },
        mem: {
          ceil: memStepsArray[memStepsArray.length - 1].value,
          floor: memStepsArray[0].value,
          hideLimitLabels: true,
          translate: value => {
            const filter = _.filter(memStepsArray, o => {
              return o.value === value;
            })[0];
            return filter ? filter.legend : '';
          },
          hidePointerLabels: true,
          stepsArray: memStepsArray,
          onChange: () => {
            $scope.$evalAsync(() => {
              vm.onChange({
                instance_size: 'CUSTOMIZED',
                config: {
                  cpu: vm.custom_instance_size.cpu,
                  mem: vm.custom_instance_size.mem,
                },
              });
            });
            vm.customizedMem = _.filter(memStepsArray, o => {
              return o.value === vm.custom_instance_size.mem;
            })[0].legend;
          },
          showSelectionBar: true,
        },
      };
      if (!_initInstanceSize) {
        vm.instance_size = vm.instanceSizeList.length
          ? vm.instanceSizeList[0]
          : vm.disableCustomize
            ? ''
            : 'CUSTOMIZED';
        vm.custom_instance_size.cpu = cpuStepsArray[0].value;
        vm.custom_instance_size.mem = memStepsArray[0].value;
        vm.customizedMem = memStepsArray[0]['legend'];
        vm.customizedCpu = cpuStepsArray[0]['legend'];
      }
      $timeout(() => {
        $scope.$broadcast('rzSliderForceRender');
      });
      selectInstanceSize(vm.instance_size);
    }
  }
}
