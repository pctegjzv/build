import templateStr from './rb-service-labels-control.component.html';

angular.module('app.components.pages').component('rbServiceLabelsControl', {
  controller: rbServiceLabelsControlController,
  require: {
    model: 'ngModel',
  },
  bindings: {
    keys: '<',
    values: '<',
    propagateOnly: '<',
  },
  controllerAs: 'vm',
  template: templateStr,
});

function rbServiceLabelsControlController() {
  const vm = this;
  vm.$onInit = _init;
  vm.addLabel = addLabel;
  vm.removeLabel = removeLabel;
  vm.updateNewLabelKey = updateNewLabelKey;
  vm.updateNewLabelValue = updateNewLabelValue;
  vm.keyPattern = keyPattern();
  vm.valuePattern = valuePattern();
  ///////////
  function _init() {
    resetNew();
    vm.model.$render = () => {
      vm.labels = (vm.model.$viewValue || []).map(label =>
        Object.assign({}, label),
      );
    };
  }

  function resetNew() {
    vm.newLabel = {
      key: '',
      value: '',
      propagate: vm.propagateOnly,
      editable: true,
    };
    vm.newLabelPropagatable = true;
    if (vm.addLabelForm) {
      vm.addLabelForm.$setPristine();
    }
  }

  function setViewValue() {
    vm.model.$setViewValue(vm.labels.map(label => Object.assign({}, label)));
  }

  function addLabel() {
    vm.labels.push(vm.newLabel);
    resetNew();
    setViewValue();
  }

  function removeLabel(index) {
    vm.labels.splice(index, 1);
    setViewValue();
  }

  function updateNewLabelKey(key) {
    vm.newLabel.key = key;
  }

  function updateNewLabelValue(value) {
    vm.newLabel.value = value;
    if (!vm.propagateOnly) {
      vm.newLabelPropagatable = /^([A-Za-z0-9][-A-Za-z0-9_.]*)?[A-Za-z0-9]$/.test(
        value || '',
      );
      if (!vm.newLabelPropagatable) {
        vm.newLabel.propagate = false;
      }
    }
  }

  function keyPattern() {
    const pattern = /^[a-zA-Z0-9][a-zA-Z0-9\-._]*[a-zA-Z0-9]$/;
    return {
      test: value => {
        vm.keyError = '';
        // test for pattern
        if (!pattern.test(value) || value.length > 60) {
          vm.keyError = 'labels_key_hint';
          return false;
        }
        // test for keep words
        if (
          _.startsWith(value.toLowerCase(), 'alauda_') ||
          ['space', 'service_name', 'pod-template-hash'].includes(value)
        ) {
          vm.keyError = 'affinity_labels_key_reserved';
          return false;
        }

        // test uniq
        if (vm.labels.some(label => label.key === value)) {
          vm.keyError = 'affinity_labels_key_unique';
          return false;
        }

        return true;
      },
    };
  }

  function valuePattern() {
    const pattern = /^([A-Za-z0-9][-A-Za-z0-9_.]*)?[A-Za-z0-9]$/;
    return {
      test: value => {
        vm.valueError = '';
        if (!/^[^:,]+$/.test(value)) {
          vm.valueError = 'invalid_pattern';
          return false;
        }

        // test pattern
        if (vm.propagateOnly && !pattern.test(value)) {
          vm.valueError = 'affinity_labels_value_hint';
          return false;
        }

        // test length
        const length = Array.prototype.reduce.call(
          value || '',
          (prev, item) => prev + (/[\u0391-\uFFE5]/g.test(item) ? 2 : 1),
          0,
        );

        if (length > 100) {
          vm.valueError = 'labels_value_overlength';
          return false;
        }

        return true;
      },
    };
  }
}
