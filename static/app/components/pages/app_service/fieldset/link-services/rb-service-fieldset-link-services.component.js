/**
 * Created by liudong on 16/9/5.
 */
const templateStr = require('app/components/pages/app_service/fieldset/link-services/rb-service-fieldset-link-services.html');
angular
  .module('app.components.pages')
  .component('rbServiceFieldsetLinkServices', {
    controller: rbServiceFieldsetLinkServicesController,
    controllerAs: 'vm',
    bindings: {
      serviceList: '<',
      linkServiceList: '<',
    },
    template: templateStr,
  });

function rbServiceFieldsetLinkServicesController() {
  const vm = this;

  vm.onServiceChange = onServiceChange;
  vm.addService = addService;
  vm.deleteService = deleteService;

  vm.$onInit = _init;
  //////////

  function _init() {
    vm.serviceList.forEach(item => {
      item.display_name = item.space_name
        ? `${item.name} (${item.space_name})`
        : item.name;
    });
  }

  function onServiceChange(option, item) {
    item.uuid = option.uuid;
    item.name = option.name;
    item.display_name = option.display_name;
  }

  function addService() {
    vm.linkServiceList.push({
      alias: '',
      name: '',
      uuid: '',
    });
  }

  function deleteService(index) {
    vm.linkServiceList.splice(index, 1);
  }
}
