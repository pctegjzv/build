/**
 * Created by liudong on 16/9/4.
 */
const {
  SERVICE_NETWORK_MODES,
  SERVICE_LOAD_BALANCER_NETWORK,
} = require('../../rb-service.constant');
const templateStr = require('app/components/pages/app_service/fieldset/load-balancers/rb-service-fieldset-load-balancers.html');
angular
  .module('app.components.pages')
  .component('rbServiceFieldsetLoadBalancers', {
    controller: rbServiceFieldsetLoadBalancersController,
    controllerAs: 'vm',
    bindings: {
      networkMode: '<',
      loadBalancerType: '<',
      loadBalancers: '<',
      containerPorts: '<', // container exposed ports
    },
    template: templateStr,
  });

function rbServiceFieldsetLoadBalancersController(translateService) {
  const vm = this;
  let _loadBalancerType;
  vm.clonedContainerPorts = [];

  vm.portMappingTypes = [
    {
      name: translateService.get('network_internal'),
      value: SERVICE_LOAD_BALANCER_NETWORK.NETWORK_INTERNAL,
    },
    {
      name: translateService.get('network_external'),
      value: SERVICE_LOAD_BALANCER_NETWORK.NETWORK_EXTERNAL,
    },
  ];

  vm.$onInit = _init;
  vm.$doCheck = _doCheck;
  vm.onPortMappingTypeChange = onPortMappingTypeChange;
  vm.onListenerContainerPortChange = onListenerContainerPortChange;
  vm.onListenerProtocolChange = onListenerProtocolChange;

  vm.isHostNetwork = isHostNetwork;
  vm.addLoadBalancer = addLoadBalancer;
  vm.addListener = addListener;
  vm.deleteListener = deleteListener;

  vm.loadBalancerProtocols = ['HTTP', 'TCP'];

  //////////

  function _init() {
    if (vm.loadBalancers.length === 0) {
      vm.loadBalancers.push({
        type: SERVICE_LOAD_BALANCER_NETWORK.NETWORK_INTERNAL,
        port_mapping_type: SERVICE_LOAD_BALANCER_NETWORK.NETWORK_INTERNAL, // rb-dropdown ng-repeat issue
        is_internal: true,
        listeners: [
          {
            lb_port: '',
            container_port: '',
            protocol: '',
          },
        ],
      });
    }
  }

  function _doCheck() {
    if (vm.loadBalancerType && _loadBalancerType !== vm.loadBalancerType) {
      _loadBalancerType = vm.loadBalancerType;
      _.forEach(vm.loadBalancers, item => {
        item.type = vm.loadBalancerType;
      });
    }
    if (vm.containerPorts.join('') !== vm.clonedContainerPorts.join('')) {
      vm.clonedContainerPorts = _.compact(vm.containerPorts);
    }
  }

  function isHostNetwork() {
    return vm.networkMode === SERVICE_NETWORK_MODES.HOST;
  }

  function onPortMappingTypeChange(option, loadBalancer) {
    loadBalancer.port_mapping_type = option.value;
    if (option.value === SERVICE_LOAD_BALANCER_NETWORK.NETWORK_INTERNAL) {
      loadBalancer.is_internal = true;
    } else if (
      option.value === SERVICE_LOAD_BALANCER_NETWORK.NETWORK_EXTERNAL
    ) {
      loadBalancer.is_internal = false;
    }
  }

  function onListenerContainerPortChange(option, listener) {
    listener.container_port = option.value;
  }

  function onListenerProtocolChange(option, item) {
    item.protocol = option.value;
  }

  function addLoadBalancer() {
    vm.loadBalancers.push({
      type: _loadBalancerType,
      is_internal: true,
      listeners: [
        {
          lb_port: '',
          container_port: '',
          protocol: '',
        },
      ],
    });
  }

  function addListener(loadBalancer) {
    loadBalancer.listeners.push({
      lb_port: '',
      container_port: '',
      protocol: '',
    });
  }

  function deleteListener(index, loadBalancer, parentIndex) {
    loadBalancer.listeners.splice(index, 1);
    if (loadBalancer.listeners.length === 0) {
      vm.loadBalancers.splice(parentIndex, 1);
    }
  }
}
