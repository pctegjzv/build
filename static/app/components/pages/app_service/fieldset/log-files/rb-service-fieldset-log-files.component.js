/**
 * Created by liudong on 16/9/5.
 */
const templateStr = require('app/components/pages/app_service/fieldset/log-files/rb-service-fieldset-log-files.html');
angular.module('app.components.pages').component('rbServiceFieldsetLogFiles', {
  controller: rbServiceFieldsetLogFilesController,
  controllerAs: 'vm',
  bindings: {
    files: '<',
  },
  template: templateStr,
});

function rbServiceFieldsetLogFilesController() {
  const vm = this;

  vm.$onInit = _init;
  vm.addFile = addFile;
  vm.deleteFile = deleteFile;
  vm.logFilePattern = '^(/[A-Za-z0-9\\s_@\\-^!#$%&+={}\\[\\]*\\.]+)+$';

  //////////

  function _init() {
    if (vm.files.length === 0) {
      addFile();
    }
  }

  function addFile() {
    vm.files.push('');
  }

  function deleteFile(index) {
    vm.files.splice(index, 1);
  }
}
