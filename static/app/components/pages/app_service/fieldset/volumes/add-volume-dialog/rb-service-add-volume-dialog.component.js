import templateString from './rb-service-add-volume-dialog.component.html';

angular.module('app.components.pages').component('rbServiceAddVolumeDialog', {
  bindings: {
    volumes: '<',
    init: '<',
    dirs: '<',
  },
  controller: rbServiceAddVolumeDialogController,
  controllerAs: 'vm',
  template: templateString,
});

function rbServiceAddVolumeDialogController(rbModal) {
  const vm = this;

  vm.$onInit = _onInit;
  vm.cancel = cancel;
  vm.confirm = confirm;
  vm.handleVolumeChange = handleVolumeChange;

  function _onInit() {
    vm.model = Object.assign({}, vm.init);
  }

  function handleVolumeChange(option) {
    vm.model.volume_name =
      option.id === 'host_path' && vm.model.volume_id !== 'host_path'
        ? ''
        : option.name || vm.model.volume_name;
    vm.model.volume_id = option.id;
    vm.model.display_text = option.display_text;
    vm.model.driver_name = option.driver_name;
  }

  function cancel() {
    rbModal.cancel();
  }

  function confirm() {
    rbModal.hide(vm.model);
  }
}
