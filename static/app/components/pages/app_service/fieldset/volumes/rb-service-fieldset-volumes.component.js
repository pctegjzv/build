import templateString from './rb-service-fieldset-volumes.component.html';

angular.module('app.components.pages').component('rbServiceFieldsetVolumes', {
  controller: rbServiceFieldsetVolumesController,
  controllerAs: 'vm',
  bindings: {
    availableVolumes: '<',
    serviceVolumes: '<',
    volumeChanged: '&',
  },
  template: templateString,
});

function rbServiceFieldsetVolumesController(
  rbModal,
  rbSafeApply,
  translateService,
) {
  const vm = this;

  vm.$onInit = _init;
  vm.updateVolume = upadateVolume;
  vm.deleteVolume = deleteVolume;

  //////////

  function _init() {
    if (vm.serviceVolumes && vm.serviceVolumes.length) {
      vm.volumeChanged({ volumes: vm.serviceVolumes });
    }
  }

  function upadateVolume(action, index, data) {
    const ebsVolumes = vm.serviceVolumes
      .filter(volume => volume.driver_name === 'ebs')
      .map(volume => volume.volume_name);

    const volumes = vm.availableVolumes.filter(volume => {
      if (volume.driver_name === 'ebs') {
        return action !== 'update'
          ? !ebsVolumes.includes(volume.name)
          : data.volume_name === volume.name ||
              !ebsVolumes.includes(volume.name);
      } else {
        return true;
      }
    });

    const dirs = vm.serviceVolumes
      .map(volume => volume.app_volume_dir)
      .filter(dir => action !== 'update' || dir !== data.app_volume_dir);

    rbModal
      .show({
        title: translateService.get('add') + translateService.get('volume'),
        component: 'rbServiceAddVolumeDialog',
        width: '640',
        locals: {
          volumes,
          init: data,
          dirs,
        },
      })
      .then(volume => {
        if (action === 'update') {
          vm.serviceVolumes.splice(index, 1, volume);
        } else {
          vm.serviceVolumes.push(volume);
        }
        vm.volumeChanged({ volumes: vm.serviceVolumes });
      })
      .catch(() => {});
  }

  function deleteVolume(index) {
    vm.serviceVolumes.splice(index, 1);
    vm.volumeChanged({ volumes: vm.serviceVolumes });
    rbSafeApply();
  }
}
