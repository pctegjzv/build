const templateStr = require('app/components/pages/app_service/image-select/rb-service-image-select.html');
angular.module('app.components.pages').component('rbServiceImageSelect', {
  controller: rbServiceImageSelectController,
  bindings: {
    regionName: '<',
  },
  controllerAs: 'vm',
  template: templateStr,
});

function rbServiceImageSelectController($state) {
  const vm = this;

  vm.onImageSelected = ({
    full_image_name,
    registry_name,
    is_public_registry,
  }) => {
    $state.go('app_service.service.create_service', {
      region_name: vm.regionName,
      image_name: full_image_name,
      registry: registry_name,
      is_public_registry: is_public_registry,
    });
  };
}
