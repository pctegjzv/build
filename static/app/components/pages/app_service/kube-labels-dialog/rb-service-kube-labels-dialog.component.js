import * as _ from 'lodash';
const templateStr = require('app/components/pages/app_service/kube-labels-dialog/rb-service-kube-labels-dialog.component.html');

angular.module('app.components.common').component('rbServiceKubeLabelsDialog', {
  bindings: {
    service: '<',
  },
  controller: rbServiceKubeLabelsDialogController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbServiceKubeLabelsDialogController(
  applicationService,
  rbLabelsDataService,
  rbModal,
  rbToast,
  translateService,
) {
  const vm = this;
  vm.$onInit = _init;
  vm.propagateChanged = propagateChanged;
  vm.submit = submit;
  vm.cancel = () => {
    rbModal.cancel();
  };
  //////////
  async function _init() {
    vm.loading = true;

    const [service, allLabels] = await Promise.all([
      applicationService
        .applicationDetail({
          service_name: vm.service.uuid,
          application: vm.service.application,
        })
        .catch(() => null),
      rbLabelsDataService.getAllLebels().catch(() => null),
    ]);

    vm.loading = false;

    if (!service) {
      rbToast.warning(translateService.get('service_not_exist'));
      rbModal.cancel();
    }

    const labels = (allLabels && allLabels.results) || [];
    vm.originalLabels = angular.copy(service.detail.labels || []);
    vm.model = service.detail;
    vm.labelKeys = _.chain(labels)
      .map(label => label.key)
      .uniq()
      .value();
    vm.labelValues = _.chain(labels)
      .map(label => label.value)
      .uniq()
      .value();
  }

  function propagateChanged() {
    if (!vm.model) {
      return false;
    }

    const propagateAdded = _.chain(vm.model.labels)
      .differenceBy(vm.originalLabels, label => label.key)
      .some(label => label.propagate)
      .value();

    const propagateRemoved = _.chain(vm.originalLabels)
      .differenceBy(vm.model.labels, label => label.key)
      .some(label => label.propagate)
      .value();

    return propagateAdded || propagateRemoved;
  }

  function submit() {
    vm.submiting = true;
    applicationService
      .applicationUpdate({
        image_data: {
          service_name: vm.model.uuid,
          labels: vm.model.labels.filter(label => label.editable),
        },
        volume_data: vm.model.volumes,
        application: vm.model.application,
      })
      .then(() => {
        rbModal.hide(vm.model.labels);
      })
      .catch(() => {
        rbToast.error(translateService.get('service_update_failed'));
      });

    vm.submiting = false;
  }
}
