/**
 * Created by liudong on 16/10/24.
 */
angular
  .module('app.components.pages')
  .factory('rbAppDataService', rbAppDataServiceFactory);

function rbAppDataServiceFactory(rbHttp, WEBLABS, rbAccountService) {
  const namespace = rbAccountService.getCurrentNamespace();
  const APP_ENDPOINT = '/ajax/v1/applications/' + namespace;
  const APP_FILEVIEW_ENDPOINT =
    '/ajax-sp/v1/fileview/applications/' + namespace;
  const CATALOG_APP_UPDATE_ENDPOINT = `/ajax/v1/catalog/${namespace}/`;

  return {
    getApps,
    appAction,
    getAppDetail,
    getAppYaml,
    getAppComposeYaml,
    updateApp,
    checkApplicationName,
    updateCatalogApplication,
    getHystrixDashboardUrl,
  };

  //////////
  function getApps({
    regionName = '',
    page,
    page_size,
    allowMeta = false,
    instance_ip = '',
    label = '',
    name = '',
    type = '',
    basic = true,
    service_info,
    with_status,
  }) {
    const params = {
      region: regionName,
      meta: allowMeta,
      instance_ip: instance_ip,
      labels: 1,
      label,
      name,
      type,
      basic,
      service_info,
      with_status,
    };
    if (page && page_size) {
      params.page = page;
      params.page_size = page_size;
    }
    return rbHttp
      .sendRequest({
        url: APP_ENDPOINT,
        params,
        addNamespace: false,
      })
      .then(res => {
        // TODO: app api 在分页搜索没有数据时返回结构和有数据时不一致，待api改后删掉。
        if (page_size) {
          return res.result
            ? {
                count: res.result.length,
                results: res.result,
              }
            : res;
        } else {
          return res.result;
        }
      });
  }

  function appAction({ action, app_name }) {
    let tail;
    const method = action === 'delete' ? 'DELETE' : 'PUT';
    if (action === 'delete') {
      tail = app_name;
    } else {
      tail = app_name + '/' + action;
    }

    return rbHttp.sendRequest({
      method,
      url: APP_ENDPOINT + '/' + tail,
      addNamespace: false,
    });
  }

  function getAppDetail(app_name, allowMeta = false) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: APP_ENDPOINT + '/' + app_name,
        params: { meta: allowMeta },
        addNamespace: false,
      })
      .then(res => ({ app: res }));
  }

  function getAppYaml(app_name) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: APP_ENDPOINT + `/${app_name}/yaml`,
        addNamespace: false,
      })
      .then(res => ({ yaml: res }));
  }

  function getAppComposeYaml(app_name) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: APP_ENDPOINT + `/${app_name}/compose-yaml`,
        addNamespace: false,
      })
      .then(res => ({ yaml: res }));
  }

  function updateApp(data) {
    return rbHttp.sendRequest({
      method: 'PUT',
      url: APP_FILEVIEW_ENDPOINT + '/' + data.app_name,
      data: { files: { services: data.services } },
      addNamespace: false,
    });
  }

  function checkApplicationName({ application_name = '', space_name = '' }) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: APP_ENDPOINT + `/${application_name}/check`,
      params: { space_name },
      addNamespace: false,
    });
  }

  function updateCatalogApplication({ type, uuid, data }) {
    const url = CATALOG_APP_UPDATE_ENDPOINT + `${type}/apps/${uuid}`;
    return rbHttp.sendRequest({
      method: 'PUT',
      url,
      data,
      addNamespace: false,
    });
  }

  function getHystrixDashboardUrl({
    uuid = '',
    trubine_uuid = '', // uuid of the selected turbine service
    dashboard_service_id = '', // uuid of the first service in hystrix app
  }) {
    const url = `/ajax/v1/catalog/${namespace}/spring_hystrix_dashboard/apps/${uuid}/dashboard/url`;
    return rbHttp.sendRequest({
      method: 'GET',
      url,
      params: {
        trubine_uuid,
        dashboard_service_id,
      },
      addNamespace: false,
    });
  }
}
