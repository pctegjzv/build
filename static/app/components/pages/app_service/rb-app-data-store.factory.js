import { filter } from 'rxjs/operators';

angular
  .module('app.components.pages')
  .factory('rbAppDataStore', rbAppDataStoreFactory);

function rbAppDataStoreFactory(RbFetchDataStore, rbAppDataService) {
  const dataStoreMap = {}; // region name -> app data store

  class AppDataStore extends RbFetchDataStore {
    constructor(region_name, isK8sRegion) {
      super(genFetchRequest(region_name, isK8sRegion));
    }

    get allWithoutMeta$() {
      // Allow meta will have UnCreated apps
      return this.data$.pipe(
        filter(app => app && app.app.current_status !== 'UnCreated'),
      );
    }
  }

  return {
    getInstance,
  };
  //////////
  function getInstance({ regionName: region_name, isK8sRegion = false }) {
    if (!dataStoreMap[region_name]) {
      dataStoreMap[region_name] = new AppDataStore(region_name, isK8sRegion);
    }
    return dataStoreMap[region_name];
  }

  function genFetchRequest(regionName, isK8sRegion = false) {
    return () =>
      isK8sRegion
        ? Promise.resolve([])
        : rbAppDataService
            .getApps({
              regionName,
              allowMeta: true,
            })
            .catch(() => []);
  }
}
