const EN_HELPER =
  'The following fields will be ignored when updating: `network_mode(V2 format)` `image` `subnet(V2 format)` `alauda_lb` `ports` ' +
  '`expose` `net(V1 format)` `domain` `labels` `volume` `mipn`';
export default EN_HELPER;
