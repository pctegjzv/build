const CN_HELPER =
  '以下属性的修改会被忽略: `network_mode(V2 format)` `image` `subnet(V2 format)` `alauda_lb` `ports` ' +
  '`expose` `net(V1 format)` `domain` `labels` `volume` `mipn`';
export default CN_HELPER;
