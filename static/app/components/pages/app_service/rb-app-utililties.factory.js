/**
 * Created by liudong on 16/10/24.
 */
angular
  .module('app.components.pages')
  .factory('rbAppUtilities', rbAppUtilities);

function rbAppUtilities($filter, translateService) {
  /*应用新状态
   -   App State 已经重新定义
   -
   -   新状态：
   -   - 中间态
   -   Starting
   -   Stopping
   -   Scaling
   -   Updating
   -   Deleting
   -   BlueGreen
   -   DisorderDeploying
   -   Creating
   -   - 最终态
   -   Running
   -   PartialRunning
   -   Stopped
   -   Deleted
   -   StartError
   -   StopError
   -   UpdateError
   -   ScaleError
   -   DeleteError
   -   MultiError
   -   SystemError
   -   CreateError (允许重试)
   -   - 兼容状态
   -   Error, Deploying | 只能 delete
   -   Removed | 无操作
   */
  return {
    startButton,
    stopButton,
    deleteButton,
    retryButton,
    updateButton,
    getPodLablesInApps,
    getAppTypeDisplay,
  };

  function startButton(current_status) {
    return ['Stopped', 'PartialRunning'].includes(current_status);
  }

  function stopButton(current_status) {
    return ![
      'Stopping',
      'Deleting',
      'Removed',
      'Stopped',
      'Deleted',
      'CreateError',
    ].includes(current_status);
  }

  function deleteButton(current_status) {
    return !['Deleting', 'Removed', 'Deleted'].includes(current_status);
  }

  function retryButton(current_status) {
    return [
      'CreateError',
      'DeleteError',
      'StartError',
      'StopError',
      'UpdateError',
    ].includes(current_status);
  }

  function updateButton(current_status = '') {
    // From wiki: http://confluence.alaudatech.com/pages/viewpage.action?pageId=7012377
    const disabledStatus = [
      'CreateError',
      'UpdateError',
      'DeleteError',
      'Deleting',
      'Starting',
      'Stopping',
      'Updating',
      'Scaling',
      'BlueGreen',
      'Deleting',
      'DisorderDeploying',
      'Creating',
      'Deploying',
    ];
    return !disabledStatus.includes(current_status);
  }

  /**
   *
   * @param {[]} apps
   * for example:
   * from:
   * [{
   *  services: [
   *  {
   *    kube_config: {
   *      pod: {
   *        labes: {
   *          'l-k1': 'l-v1',
   *          'l-k1': 'l-v2',
   *        }
   *    }
   *  },
   *  {
   *    kube_config: {
   *      pod: {
   *        labes: {
   *          'l-k1': 'l-v1',
   *          'l-k2': 'l-v1',
   *       }
   *    }
   *  }]
   * },services: [
   *  {
   *    kube_config: {
   *      pod: {
   *        labes: {
   *          'l-k3': 'l-v3'
   *        }
   *    }
   *  }]
   * }]
   * @return {[]} labels
   * return:
   * [
   * {key: l-k1, value: l-v1},
   * {key: l-k1, value: l-v2},
   * {key: l-k2, value: l-v1},
   * {key: l-k3, value: l-v3},
   * ]
   */
  function getPodLablesInApps(apps) {
    return (
      _.chain(apps)
        // [{services:[s1,s2]}, {services:[s3]}] to [[s1,s2],[s3]]
        .map(app => app.services)
        // [s1,s2,s3]
        .flatten()
        // [{k1:v1,k1:v2},{k1:v1,k2:v1},{k1:v1,k1:v2}]
        .map(service => _.get(service, 'kube_config.pod.labels'))
        .compact()
        .map(labelsObj => {
          // [{k1:v1}] to [[{'key':'k1', 'value':v1}]]
          const labelArr = [];
          _.each(labelsObj, (v, k) => {
            labelArr.push({
              key: k,
              value: v,
            });
          });
          return labelArr;
        })
        .flatten()
        .uniqWith(_.isEqual)
        .value()
    );
  }

  /**
   * TODO: 需要API返回分类后的type
   * 目前是人工定义分类，相关定义在chen的环境变量里
   *  MICR_SERVICE  spring_config_server,spring_turbine,spring_zipkin,spring_eureka,spring_zuul,spring_hystrix_dashboard
   MIDDLEWARE  kafka,zookeeper,redis,elk,elasticsearch,mysql
   BIG_DATA  hadoop,hbase,spark,batch_processing,stream_processing,kafka
   "nav_micro_service": "微服务",
   "nav_middleware": "中间件",
   "nav_big_data": "大数据",
   "plugin": "插件"
   * @param type
   */
  function getAppTypeDisplay(type, source) {
    if (source && 'PLUGIN' === source) {
      return `${translateService.get('plugin')} - ${type}`;
    }
    if (type) {
      const t = $filter('rbCapitalizeAll')(type.split('_').join(' '));
      if (
        [
          'spring_config_server',
          'spring_turbine',
          'spring_zipkin',
          'spring_eureka',
          'spring_zuul',
          'spring_hystrix_dashboard',
        ].includes(type)
      ) {
        type = $filter('rbCapitalizeAll')(
          type
            .split('_')
            .slice(1)
            .join(' '),
        );
        return `${translateService.get('nav_micro_service')} - ${type}`;
      } else if (
        [
          'kafka',
          'zookeeper',
          'redis',
          'elk',
          'elasticsearch',
          'mysql',
          'mongodb',
        ].includes(type)
      ) {
        return `${translateService.get('nav_middleware')} - ${t}`;
      } else if (
        [
          'hadoop',
          'hbase',
          'spark',
          'batch_processing',
          'stream_processing',
          'kafka',
        ].includes(type)
      ) {
        return `${translateService.get('nav_big_data')} - ${t}`;
      } else {
        return '';
      }
    }
  }
}
