angular
  .module('app.components.pages')
  .factory('rbEnvfileDataStore', rbEnvfileDataStoreFactory);

/**
 * Observable data store for registry
 */
function rbEnvfileDataStoreFactory(RbFetchDataStore, envfileService) {
  const fetchRequest = () =>
    envfileService.envfileList().then(({ envfiles }) => {
      return envfiles.map(item => {
        item.display_name = item.space_name
          ? `${item.name}(${item.space_name})`
          : item.name;
        return item;
      });
    });
  return new RbFetchDataStore(fetchRequest);
}
