module.exports = [
  {
    baseStateName: 'app_service.service',
    states: [
      {
        name: 'list',
        url: '',
        component: 'rbServiceList',
      },
      {
        name: 'image_select',
        url: '/image_select?region_name',
        component: 'rbServiceImageSelect',
        resolve: {
          regionName: $stateParams => {
            'ngInject';
            return $stateParams['region_name'];
          },
        },
      },
      {
        name: 'create_service',
        url:
          '/create_service?region_name&registry&image_name&is_public_registry',
        component: 'rbServiceCreate',
        resolve: {
          regionName: $stateParams => {
            'ngInject';
            return $stateParams['region_name'];
          },
          registryName: $stateParams => {
            'ngInject';
            return $stateParams['registry'];
          },
          imageName: $stateParams => {
            'ngInject';
            return $stateParams['image_name'];
          },
        },
      },
      {
        name: 'update_service',
        url: '/update_service/:service_name?application',
        component: 'rbServiceUpdate',
        resolve: {
          serviceName: $stateParams => {
            'ngInject';
            return $stateParams['service_name'];
          },
          application: $stateParams => {
            'ngInject';
            return $stateParams['application'];
          },
        },
      },
      {
        name: 'service_detail',
        url: '/service_detail/:service_name?application',
        component: 'rbServiceDetail',
        resolve: {
          serviceName: $stateParams => {
            'ngInject';
            return $stateParams['service_name'];
          },
          application: $stateParams => {
            'ngInject';
            return $stateParams['application'];
          },
        },
      },
    ],
  },
  {
    baseStateName: 'app_service.app',
    states: [
      {
        name: 'list',
        url: '',
        component: 'rbAppList',
      },
      {
        name: 'create_app',
        url: '/create_app?region_name&template_name',
        component: 'rbAppCreate',
        resolve: {
          regionName: $stateParams => {
            'ngInject';
            return $stateParams['region_name'];
          },
        },
      },
      {
        name: 'app_detail',
        url: '/app_detail/:app_name',
        component: 'rbAppDetail',
        resolve: {
          appName: $stateParams => {
            'ngInject';
            return $stateParams['app_name'];
          },
        },
      },
      {
        name: 'update_app',
        url: '/update_app/:app_name',
        component: 'rbAppUpdate',
        resolve: {
          appName: $stateParams => {
            'ngInject';
            return $stateParams['app_name'];
          },
        },
      },
    ],
  },
  {
    baseStateName: 'app_service.template',
    states: [
      {
        name: 'list',
        url: '',
        component: 'rbAppTemplateList',
      },
      {
        name: 'detail',
        url: '/detail/:templateName',
        component: 'rbAppTemplateDetail',
      },
      {
        name: 'create',
        url: '/create?from&export',
        component: 'rbAppTemplateCreate',
      },
      {
        name: 'update',
        url: '/update/:templateName',
        component: 'rbAppTemplateUpdate',
      },
    ],
  },
  {
    baseStateName: 'app_service.envfile',
    states: [
      {
        name: 'list',
        url: '',
        component: 'rbEnvfileList',
      },
      {
        name: 'detail',
        url: '/detail/:envfileName',
        component: 'rbEnvfileDetail',
      },
      {
        name: 'create',
        url: '/create?from',
        component: 'rbEnvfileCreate',
      },
      {
        name: 'update',
        url: '/update/:envfileName',
        component: 'rbEnvfileUpdate',
      },
    ],
  },
  {
    baseStateName: 'app_service.configuration',
    states: [
      {
        name: 'list',
        url: '',
        component: 'rbConfigurationList',
      },
      {
        name: 'detail',
        url: '/detail?name?description',
        component: 'rbConfigurationDetail',
        resolve: {
          name: $stateParams => {
            'ngInject';
            return $stateParams['name'];
          },
          description: $stateParams => {
            'ngInject';
            return $stateParams['description'];
          },
        },
      },
    ],
  },
];

angular
  .module('app.components.pages')
  .constant('rbPageAppServiceStates', module.exports);
