angular
  .module('app.components.pages')
  .factory('rbSearchAppServiceDataStore', rbSearchAppServiceDataStoreFactory);

function rbSearchAppServiceDataStoreFactory(
  RbFetchDataStore,
  rbLabelsDataService,
) {
  const dataStoreMap = {}; // region name -> app data store

  class SearchAppServiceDataStore extends RbFetchDataStore {
    constructor(
      regionName,
      resourceType = 'service',
      labelsParam = '',
      searchParam = '',
    ) {
      super(
        genFetchRequest(regionName, resourceType, labelsParam, searchParam),
      );
    }
  }

  return {
    getInstance,
  };

  //////////
  function getInstance({ regionName, resourceType, labelsParam, searchParam }) {
    const key = `${regionName}_${resourceType}_${labelsParam}_${searchParam}`;
    if (!dataStoreMap[key]) {
      dataStoreMap[key] = new SearchAppServiceDataStore(
        regionName,
        resourceType,
        labelsParam,
        searchParam,
      );
    }
    return dataStoreMap[key];
  }

  function genFetchRequest(
    regionName,
    resourceType,
    labelsParam = '',
    searchParam = '',
  ) {
    return () =>
      rbLabelsDataService.searchLabels({
        region_name: regionName,
        source_type: resourceType,
        label: labelsParam,
        name: searchParam,
      });
  }
}
