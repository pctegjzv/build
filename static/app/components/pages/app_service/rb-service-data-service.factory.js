/**
 * Created by liudong on 2017/7/27.
 */
angular
  .module('app.components.pages')
  .factory('rbServiceDataService', rbServiceDataServiceFactory);

function rbServiceDataServiceFactory(rbHttp, rbAccountService) {
  const namespace = rbAccountService.getCurrentNamespace();
  const SERVICE_ENDPOINT = `/ajax/v1/services/${namespace}/`;

  return {
    scaling,
    instanceDelete,
    getServices,
    getNewK8sServices,
    serviceAction,
    deleteService,
    getNewK8sService,
    getNewK8sServiceInstances,
  };

  function scaling(params, data) {
    return rbHttp.sendRequest({
      method: 'PUT',
      url: SERVICE_ENDPOINT + params.service_name,
      params: { application: params.application },
      data,
    });
  }

  function instanceDelete(data) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url: SERVICE_ENDPOINT + data.service_name + '/instances/' + data.pod_name,
    });
  }

  function getServices({
    basic = false,
    region_name = '',
    page = 1,
    page_size = 20,
    name = '',
    label = '',
    labels = 0,
    order_by = '-created_at',
    instance_ip = '',
  }) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: SERVICE_ENDPOINT,
        params: {
          basic,
          region_name,
          page,
          page_size,
          name,
          label,
          labels,
          order_by,
          instance_ip,
        },
      })
      .catch(() => {
        return new Error('get services');
      });
  }

  function getNewK8sServices({
    cluster = '',
    page = 1,
    page_size = 20,
    name = '',
  }) {
    return rbHttp
      .sendRequest({
        url: '/ajax/v2/services',
        params: {
          cluster,
          page,
          page_size,
          name,
        },
        addNamespace: false,
      })
      .then(({ results }) => results);
  }

  /**
   * service action includes: start stop retryupdate rollback
   * @param service_name
   * @param application
   * @param action
   * @returns {*}
   */
  function serviceAction({ service_name, application, action }) {
    return rbHttp.sendRequest({
      method: 'PUT',
      url: `${SERVICE_ENDPOINT}${service_name}/${action}`,
      params: { application },
      addNamespace: false,
    });
  }

  function deleteService({ service_name, application }) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url: `${SERVICE_ENDPOINT}${service_name}`,
      params: { application },
      addNamespace: false,
    });
  }

  function getNewK8sService(uuid) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: `/ajax/v2/services/${uuid}`,
      addNamespace: false,
    });
  }

  function getNewK8sServiceInstances(uuid) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: `/ajax/v2/services/${uuid}/instances`,
        addNamespace: false,
      })
      .then(({ result }) => result);
  }
}
