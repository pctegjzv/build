angular
  .module('app.components.pages')
  .factory('rbServiceDataStore', rbServiceDataStoreFactory);

function rbServiceDataStoreFactory(RbFetchDataStore, rbServiceDataService) {
  const dataStoreMap = {}; // region name -> app data store

  class ServiceDataStore extends RbFetchDataStore {
    constructor(region_name, isK8sRegion) {
      super(genFetchRequest(region_name, isK8sRegion));
      this.regionName = region_name;
    }
  }

  return {
    getInstance,
  };

  //////////
  function getInstance({ regionName: region_name, isK8sRegion = false }) {
    if (!dataStoreMap[region_name]) {
      dataStoreMap[region_name] = new ServiceDataStore(
        region_name,
        isK8sRegion,
      );
    }
    return dataStoreMap[region_name];
  }

  function genFetchRequest(regionName, isK8sRegion = false) {
    if (!isK8sRegion) {
      return () =>
        rbServiceDataService
          .getServices({
            basic: true,
            region_name: regionName,
            page_size: 100,
          })
          .then(({ results }) => results)
          .catch(() => []);
    } else {
      return () =>
        rbServiceDataService
          .getNewK8sServices({
            cluster: regionName,
            page_size: 100,
          })
          .catch(() => []);
    }
  }
}
