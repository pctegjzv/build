import { INSTANCE_SIZE } from './rb-service.constant';
angular
  .module('app.components.pages')
  .factory('rbServiceInstanceSize', rbServiceInstanceSize);

function rbServiceInstanceSize(
  translateService,
  rbBytesFormatterFilter,
  CUSTOM_INSTANCE_SIZE_CONFIG,
) {
  const fixedConfig =
    (CUSTOM_INSTANCE_SIZE_CONFIG && CUSTOM_INSTANCE_SIZE_CONFIG.fixed) ||
    INSTANCE_SIZE;

  const cpu = _.get(CUSTOM_INSTANCE_SIZE_CONFIG, 'custom.cpu.min', 0.125);
  const mem = _.get(CUSTOM_INSTANCE_SIZE_CONFIG, 'custom.mem.min', 64);

  const instanceSizes = Object.keys(fixedConfig)
    .map(key => ({ key, ...fixedConfig[key] }))
    .sort((a, b) => a.cpu - b.cpu);

  function getInstanceSizes() {
    return instanceSizes;
  }

  function getMinInstanceSize() {
    return {
      cpu,
      mem,
    };
  }

  function getInstanceSizeText(config) {
    const matched = instanceSizes.find(
      size => size.cpu === config.cpu && size.mem === config.mem,
    );

    const type = (matched && matched.key) || translateService.get('CUSTOMIZED');
    const unitCPU = translateService.get('unit_cpu');
    return `${type} ( CPU: ${
      config.cpu
    } ${unitCPU}, MEM: ${rbBytesFormatterFilter(config.mem, 0, 1024 * 1024)} )`;
  }

  return {
    getInstanceSizes,
    getMinInstanceSize,
    getInstanceSizeText,
  };
}
