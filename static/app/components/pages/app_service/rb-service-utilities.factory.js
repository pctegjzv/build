import _ from 'lodash';
import { BehaviorSubject, Subject, combineLatest, interval, merge } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  mapTo,
  publishReplay,
  refCount,
  scan,
  share,
  startWith,
  switchMap,
  tap,
  withLatestFrom,
} from 'rxjs/operators';

const {
  SERVICE_NETWORK_MODES,
  SERVICE_LOAD_BALANCERS,
} = require('./rb-service.constant');
const { CONTAINER_MANAGERS } = require('../region/rb-region.constant');

angular
  .module('app.components.pages')
  .factory('rbServiceUtilities', rbServiceUtilities);

function rbServiceUtilities(translateService) {
  return {
    isIaasLoadBalancer,
    startButton,
    stopButton,
    // promoteButton,
    deleteButton,
    updateButton,
    execButton,
    retryUpdateButton,
    rollbackButton,
    applyButton,
    saveButton,
    labelButton,
    setServiceHealthStatus,
    formatConfigFileData,
    getNetworksFromRegionFeature,
    isServiceHealthcheckEnabled,
    getServiceLogParams,
    isMipnOptionsEnabled,
    isDeploymentMode,
    getLabelsHtml,
    getServiceImageFullName,
    getServiceImageTooltip,
    isK8sUpdateNodeTagsEnabled,
    initObservables,
    destroySubscriptions,
  };

  /*  服务新状态：
   Starting, StartError, Running
   Stopping, StopError, Stopped
   Updating, UpdateError
   Scaling, ScaleError
   BlueGreen
   Deleting, DeleteError, Deleted

   废弃状态：
   Error, Removed, Deploying, BlueGreenError*/

  /////////
  function isIaasLoadBalancer(str) {
    const name = str.toLowerCase();
    return name !== 'alb' && /^\w{1}lb$/.test(name);
  }

  //show button logic
  function startButton(current_status) {
    return 'Stopped' === current_status;
  }

  function stopButton(current_status) {
    return ![
      'Stopping',
      'Deleting',
      'Deleted',
      'Unknown',
      'Stopped',
      'UnCreated',
    ].includes(current_status);
  }
  // function promoteButton(current_status) { // todo: promote to master
  //   return !['Stopping', 'Deleting', 'Deleted', 'Unknown', 'Stopped', 'UnCreated'].includes(current_status);
  // }

  function deleteButton(current_status) {
    return !['Deleting', 'Deleted', 'Unknown', 'UnCreated'].includes(
      current_status,
    );
  }

  function updateButton(current_status) {
    const returnFlag =
      ['Running', 'Stopped'].includes(current_status) ||
      (current_status.endsWith('Error') && current_status !== 'ScaleError');
    return returnFlag;
  }

  function execButton(current_status) {
    return 'Running' === current_status;
  }

  function retryUpdateButton(current_status) {
    return (
      ['BlueGreen'].includes(current_status) || current_status.endsWith('Error')
    );
  }

  function rollbackButton(current_status) {
    return ['UpdateError', 'BlueGreen', 'BlueGreenError'].includes(
      current_status,
    );
  }

  function applyButton(current_status) {
    return 'Running' === current_status || current_status.endsWith('Error');
  }

  function saveButton(current_status) {
    return 'Stopped' === current_status;
  }

  function labelButton(current_status) {
    return 'Updating' !== current_status;
  }

  function setServiceHealthStatus(service) {
    if (!service.health_status) {
      if (service.healthy_num_instances < service.target_num_instances) {
        service.health_status = 'unhealthy';
      } else {
        service.health_status = 'healthy';
      }
    }
  }

  function formatConfigFileData(item) {
    const config = {
      path: item.path,
      type: item.type,
    };
    if (item.type === 'raw') {
      config.value = item.rawValue;
    } else {
      config.value = {
        name: item.config,
        key: item.key,
        uuid: item.uuid,
      };
    }
    return config;
  }

  /**
   * The new region model api doc: https://bitbucket.org/mathildetech/atropos/wiki/design/claas/region-model.md
   * The old region model will not be removed but the new one is suggested
   * @param region
   * @returns {Array}
   */
  function getNetworksFromRegionFeature(region) {
    if (!region || !region.features) {
      return [];
    }
    let _networkModes = [];
    // In the new api, network_modes related region feature is an Array at regionObj.features.network_modes.features
    const newNetworkFeatures = _.get(
      region,
      'features.network_modes.features',
      [],
    );
    if (newNetworkFeatures.length > 0) {
      _networkModes = _getNetworkModesWithNewStructure(newNetworkFeatures);
    } else {
      _networkModes = _getNetworkModesWithTraditionalStructure(region);
    }
    return _networkModes;
  }

  function _getNetworkModesWithNewStructure(newNetworkFeatures) {
    const _networkModes = newNetworkFeatures.map(networkItem => {
      let mode;
      switch (networkItem) {
        case 'host-network':
          mode = {
            name: 'Host',
            value: SERVICE_NETWORK_MODES.HOST,
          };
          break;
        case 'bridge':
          mode = {
            name: 'Bridge',
            value: SERVICE_NETWORK_MODES.BRIDGE,
          };
          break;
        case 'macvlan':
          mode = {
            name: 'Macvlan',
            value: SERVICE_NETWORK_MODES.MACVLAN,
          };
          break;
        case 'flannel':
          mode = {
            name: 'Flannel',
            value: SERVICE_NETWORK_MODES.FLANNEL,
          };
          break;
        default:
          mode = {
            name: networkItem.toUpperCase(),
            value: networkItem.toUpperCase(),
          };
          break;
      }
      return mode;
    });
    return _networkModes;
  }

  function _getNetworkModesWithTraditionalStructure(region) {
    let _networkModes;
    const regionFeatures = region.features.service.features;
    switch (region.container_manager) {
      case CONTAINER_MANAGERS.SWARM:
        _networkModes = [
          {
            name: 'Bridge',
            value: SERVICE_NETWORK_MODES.BRIDGE,
          },
        ];
        break;
      case CONTAINER_MANAGERS.KUBERNETES:
        _networkModes = [
          {
            name: 'Host',
            value: SERVICE_NETWORK_MODES.HOST,
          },
        ];
        break;
      case CONTAINER_MANAGERS.MESOS:
      default:
        _networkModes = [
          {
            name: 'Bridge',
            value: SERVICE_NETWORK_MODES.BRIDGE,
          },
          {
            name: 'Host',
            value: SERVICE_NETWORK_MODES.HOST,
          },
        ];
        break;
    }

    if (regionFeatures.includes('macvlan')) {
      _networkModes = _networkModes.concat([
        {
          name: 'Macvlan',
          value: SERVICE_NETWORK_MODES.MACVLAN,
        },
      ]);
    }

    if (regionFeatures.includes('flannel')) {
      _networkModes = _networkModes.concat([
        {
          name: 'Flannel',
          value: SERVICE_NETWORK_MODES.FLANNEL,
        },
      ]);
    }
    return _networkModes;
  }

  function isServiceHealthcheckEnabled(serviceData, regionFeatures) {
    if (!serviceData || !regionFeatures) {
      return false;
    }
    if (serviceData.networkMode) {
      return (
        [
          SERVICE_NETWORK_MODES.HOST,
          SERVICE_NETWORK_MODES.BRIDGE,
          SERVICE_NETWORK_MODES.FLANNEL,
        ].includes(serviceData.networkMode) &&
        serviceData.loadBalancerType !== SERVICE_LOAD_BALANCERS.RAW
      );
    } else if (serviceData.network_mode) {
      if (regionFeatures.includes('alb')) {
        return [
          SERVICE_NETWORK_MODES.HOST,
          SERVICE_NETWORK_MODES.BRIDGE,
          SERVICE_NETWORK_MODES.FLANNEL,
        ].includes(serviceData.network_mode);
      } else {
        return (
          [
            SERVICE_NETWORK_MODES.HOST,
            SERVICE_NETWORK_MODES.BRIDGE,
            SERVICE_NETWORK_MODES.FLANNEL,
          ].includes(serviceData.network_mode) &&
          _.get(serviceData, 'raw_container_ports', []).length === 0
        );
      }
    }
    return false;
  }

  // get log releated reqeust params for service_name & application
  function getServiceLogParams(service) {
    const params = {
      namespace: service.namespace,
    };
    if (service.application) {
      params['application'] = service.application;
      params['service_name'] = service.service_name;
    } else {
      params['service_name'] = service.uuid;
    }
    return params;
  }

  function isMipnOptionsEnabled(region) {
    if (region) {
      const albEnabled = _.get(
        region,
        'features.service.features',
        [],
      ).includes('alb');
      return (
        albEnabled &&
        ![CONTAINER_MANAGERS.KUBERNETES].includes(region.container_manager)
      );
    } else {
      return false;
    }
  }

  function isDeploymentMode(service) {
    return !['StatefulSet', 'DaemonSet'].includes(service.pod_controller);
  }

  function getLabelsHtml(labels) {
    if (!labels || !labels.length) {
      return 'N/A';
    } else {
      return labels
        .map(label => {
          let className = 'labels-item';
          if (label.propagate) {
            className += ' propagate';
          }
          return `<span class="${className}">${label.key}: ${
            label.value
          }</span>`;
        })
        .join('');
    }
  }

  function getServiceImageFullName(service) {
    return `${service.image_name}:${service.image_tag}`;
  }

  function getServiceImageTooltip(service) {
    return `${translateService.get('click_to_copy')} ${service.image_name}:${
      service.image_tag
    }`;
  }
  function isK8sUpdateNodeTagsEnabled(region) {
    return region.container_manager === 'KUBERNETES';
  }

  function initObservables({
    scope,
    rbRegionService,
    onRegionSelected,
    fetchRequest,
  }) {
    const vm = scope;

    vm.refreshSubject$ = new BehaviorSubject(null);
    vm.pageSubject$ = new BehaviorSubject(1).pipe(distinctUntilChanged());
    vm.paramsSubject$ = new BehaviorSubject({
      search: '',
      label: '',
    }).pipe(
      distinctUntilChanged(),
      publishReplay(1),
      refCount(),
    );
    vm.actionSubject$ = new Subject().pipe(
      map(_item => list =>
        list.map(
          item =>
            item.uuid === _item.uuid ? Object.assign(item, _item) : item,
        ),
      ),
    );

    const polling$ = interval(10000).pipe(startWith(0));
    const loadingProxySubject$ = new Subject();
    const labelsFilterFnSubject$ = new BehaviorSubject(list => list).pipe(
      debounceTime(100),
      share(),
    );
    const resourceRequestTrigger$ = combineLatest(
      rbRegionService.regionName$.pipe(
        filter(regionName => !!regionName),
        distinctUntilChanged(),
      ),
      vm.paramsSubject$,
      vm.pageSubject$,
      vm.refreshSubject$,
      polling$.pipe(
        withLatestFrom(loadingProxySubject$),
        filter(([, loading]) => !loading),
        startWith(null),
      ),
    );
    const resourceRequest$ = resourceRequestTrigger$.pipe(
      switchMap(fetchRequest),
      share(),
    );

    const resourcesWrapper$ = resourceRequest$.pipe(
      filter(response => !(response instanceof Error)),
      tap(response => {
        vm.pagination.count = response.count;
      }),
      map(response => () => response.results),
    );

    const resources$ = merge(resourcesWrapper$, vm.actionSubject$).pipe(
      scan((list, fn) => fn(list), []),
    );

    // Scope variables
    vm.labelsFilterFnSubject$ = labelsFilterFnSubject$;
    vm.resources$ = combineLatest(resources$, vm.labelsFilterFnSubject$).pipe(
      map(([resources, filterFn]) => resources.filter(filterFn)),
    );
    vm.loading$ = merge(
      vm.paramsSubject$.pipe(mapTo(true)),
      vm.pageSubject$.pipe(mapTo(true)),
      rbRegionService.regionName$.pipe(map(regionName => !!regionName)),
      resourceRequest$.pipe(mapTo(false)),
    ).pipe(
      publishReplay(1),
      refCount(),
    );
    vm.empty$ = merge(
      resources$.pipe(
        map(resources => !resources.length),
        startWith(true),
        publishReplay(1),
        refCount(),
      ),
    );
    vm.error$ = resourceRequest$.pipe(
      map(response => response instanceof Error),
      publishReplay(1),
      refCount(),
    );
    vm.initialized$ = merge(
      resources$.pipe(mapTo(true)),
      rbRegionService.regionName$.pipe(mapTo(regionName => !regionName)),
    ).pipe(
      publishReplay(1),
      refCount(),
    );

    // Subscriptions
    vm.searchSubscription = vm.paramsSubject$.subscribe(() => {
      vm.pagination.page = 1;
    });
    vm.regionSubscription = rbRegionService.region$.subscribe(onRegionSelected);
    vm.loadingSubscription = vm.loading$.subscribe(loading => {
      loadingProxySubject$.next(loading);
    });
  }

  function destroySubscriptions(scope) {
    const vm = scope;
    vm.loadingSubscription && vm.loadingSubscription.unsubscribe();
    vm.searchSubscription && vm.searchSubscription.unsubscribe();
    vm.regionSubscription && vm.regionSubscription.unsubscribe();
  }
}
