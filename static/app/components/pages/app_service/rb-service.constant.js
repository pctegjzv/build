/**
 * Created by liudong on 16/9/4.
 */
// Build step number to its value
const SERVICE_NETWORK_MODES = {
  HOST: 'HOST',
  BRIDGE: 'BRIDGE',
  MACVLAN: 'MACVLAN',
  FLANNEL: 'FLANNEL',
};

const SERVICE_LOAD_BALANCERS = {
  HAPROXY: 'HAProxy',
  NGINX: 'Nginx',
  RAW: 'Raw Container',
};

const SERVICE_LOAD_BALANCER_NETWORK = {
  NETWORK_INTERNAL: 'internal',
  NETWORK_EXTERNAL: 'external',
};

const SERVICE_TYPE = {
  STATEFUL: 'STATEFUL',
  STATELESS: 'STATELESS',
};

const SERVICE_RUNCOMMAND_TYPE = {
  DEFAULT: 'DEFAULT',
  CUSTOM: 'CUSTOM',
};

const SERVICE_TARGET_STATES = {
  START: 'STARTED',
  STOP: 'STOPPED',
};

const SERVICE_INTERMEDIATE_STATUS = {
  UPDATING: 'Updating',
  STOPPING: 'Stopping',
  STARTING: 'Starting',
  SCALING: 'Scaling',
  DELETING: 'Deleting',
};

const SERVICE_LOG_MODE = {
  SEARCH: 'search',
  POLLING: 'polling',
};

const INSTANCE_SIZE = {
  XXS: {
    mem: 256,
    cpu: 0.125,
  },
  XS: {
    mem: 512,
    cpu: 0.25,
  },
  S: {
    mem: 1024,
    cpu: 0.5,
  },
  M: {
    mem: 2048,
    cpu: 1,
  },
  L: {
    mem: 4096,
    cpu: 2,
  },
  XL: {
    mem: 8192,
    cpu: 4,
  },
};

const INSTANCE_SIZE_INFO = {
  XXS: {
    mem: '256 MB',
    cpu: '0.125 cpuUnit',
  },
  XS: {
    mem: '512 MB',
    cpu: '0.25 cpuUnit',
  },
  S: {
    mem: '1 GB',
    cpu: '0.5 cpuUnit',
  },
  M: {
    mem: '2 GB',
    cpu: '1 cpuUnit',
  },
  L: {
    mem: '4 GB',
    cpu: '2 cpuUnit',
  },
  XL: {
    mem: '8 GB',
    cpu: '4 cpuUnit',
  },
};

const SHARED_DRIVER_NAME = ['glusterfs', 'loongstore'];

const SERVICE_DEPLOY_MODEL = {
  DEPLOYMENT: 'Deployment',
  DAEMONSET: 'DaemonSet',
  STATEFULSET: 'StatefulSet',
};

const SERVICE_CONSTANTS = {
  SERVICE_NETWORK_MODES,
  SERVICE_LOAD_BALANCERS,
  SERVICE_LOAD_BALANCER_NETWORK,
  SERVICE_TYPE,
  SERVICE_RUNCOMMAND_TYPE,
  SERVICE_TARGET_STATES,
  SERVICE_INTERMEDIATE_STATUS,
  SERVICE_LOG_MODE,
  INSTANCE_SIZE,
  INSTANCE_SIZE_INFO,
  SHARED_DRIVER_NAME,
  SERVICE_DEPLOY_MODEL,
};

module.exports = SERVICE_CONSTANTS;

angular
  .module('app.components.pages')
  .constant('SERVICE_CONSTANTS', SERVICE_CONSTANTS);
