angular
  .module('app.components.pages')
  .factory('rbTemplateDataStore', rbTemplateDataStoreFactory);

/**
 * Observable data store for registry
 */
function rbTemplateDataStoreFactory(RbFetchDataStore, templateService) {
  const fetchRequest = () =>
    templateService.templateList().then(({ template_list }) => {
      return template_list.map(item => {
        item.display_name = item.space_name
          ? `${item.name}(${item.space_name})`
          : item.name;
        return item;
      });
    });
  const fetchDataStore = new RbFetchDataStore(fetchRequest);
  return {
    // The value will be initialized upon first subscribe.
    all: fetchDataStore.data$,
    refetch: () => fetchDataStore.refetch(),
  };
}
