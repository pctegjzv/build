angular
  .module('app.components.pages')
  .factory('rbTemplateServiceDataStore', rbTemplateServiceDataStoreFactory);

/**
 * Observable data store for registry
 */
function rbTemplateServiceDataStoreFactory(RbFetchDataStore, templateService) {
  const dataStoreMap = {};

  return {
    // The value will be initialized upon first subscribe.
    list,
    refetch: template_name => dataStoreMap[template_name].refetch(),
  };
  //////////

  function list(template_name) {
    if (!dataStoreMap[template_name]) {
      refetch(template_name);
    }
    return dataStoreMap[template_name].data$;
  }

  function refetch(template_name) {
    if (!dataStoreMap[template_name]) {
      dataStoreMap[template_name] = new RbFetchDataStore(() =>
        templateService.templateServices(template_name),
      );
    }
    return dataStoreMap[template_name].refetch();
  }
}
