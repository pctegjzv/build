import { resource_types as RESOURCE_TYPES } from '../../rbac_role/rb-rbac-helper.constant';

const templateStr = require('app/components/pages/app_service/service_list/rb-service-list.html');
const resourceLabelsPopoverTemplate = require('ngtemplate-loader!app/components/pages/app_service/list/rb-app-service-list-labels-popover.html');
angular.module('app.components.pages').component('rbServiceList', {
  controller: rbServiceListController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbServiceListController(
  $log,
  $state,
  WEBLABS,
  translateService,
  rbConfirmBox,
  rbRegionService,
  rbRoleUtilities,
  rbServiceUtilities,
  rbServiceDataService,
  rbLabelsDataService,
  rbRoleDataService,
  rbModal,
) {
  const vm = this;
  const inActionServices = {};

  vm.onRegionSelected = onRegionSelected;
  vm.onPageChanged = onPageChanged;
  vm.refresh = refresh;

  vm.buttonIfExpr = buttonIfExpr;
  vm.buttonDisabledExpr = buttonDisabledExpr;
  vm.startService = startService;
  vm.stopService = stopService;
  vm.deleteService = deleteService;
  vm.updateService = updateService;
  vm.updateServiceLabels = updateServiceLabels;
  vm.getServiceUrl = getServiceUrl;

  vm.$onInit = _init;
  vm.$onDestroy = _destroy;

  //////////

  function _init() {
    vm.getServiceImageFullName = rbServiceUtilities.getServiceImageFullName;
    vm.getServiceImageTooltip = rbServiceUtilities.getServiceImageTooltip;
    vm.pagination = {
      pageSize: 20,
      page: 1,
      count: 0,
    };
    vm.quotaEnabled = WEBLABS.QUOTA_ENABLED;
    vm.resourceLabelsPopoverTemplate = resourceLabelsPopoverTemplate;

    const fetchRequest = ([regionName, params]) => {
      return rbServiceDataService.getServices({
        basic: false,
        region_name: regionName,
        name: params.search,
        labels: 1,
        label: params.label,
        page: vm.pagination.page,
        page_size: vm.pagination.pageSize,
      });
    };
    rbServiceUtilities.initObservables({
      scope: vm,
      rbRegionService,
      onRegionSelected,
      fetchRequest,
    });
  }

  function _destroy() {
    rbServiceUtilities.destroySubscriptions(vm);
  }

  function refresh() {
    vm.refreshSubject$.next(null);
  }

  async function onRegionSelected(region) {
    if (
      region &&
      (!vm.currentRegion || vm.currentRegion.name !== region.name)
    ) {
      vm.currentRegion = region;
      vm.pagination.page = 1;
      const servicePermissions = await rbRoleDataService.getContextPermissions(
        RESOURCE_TYPES.SERVICE,
        {
          cluster_name: region.name,
        },
      );
      vm.serviceCreateEnabled = servicePermissions.includes(
        `${RESOURCE_TYPES.SERVICE}:create`,
      );
    } else {
      vm.serviceCreateEnabled = false;
    }
  }

  /**
   * pagination component callback
   * @param page
   */
  function onPageChanged(page) {
    if (page !== vm.pagination.page) {
      vm.pagination.page = page;
      vm.pageSubject$.next(page);
    }
  }

  function buttonIfExpr(service, type) {
    return rbRoleUtilities.resourceHasPermission(
      service,
      RESOURCE_TYPES.SERVICE,
      type,
    );
  }

  function buttonDisabledExpr(service, type) {
    if (service && inActionServices[service.uuid]) {
      return true;
    }
    switch (type) {
      case 'start':
        return !rbServiceUtilities.startButton(service.current_status);
      case 'stop':
        return !rbServiceUtilities.stopButton(service.current_status);
      case 'delete':
        return !rbServiceUtilities.deleteButton(service.current_status);
      case 'update':
        return !rbServiceUtilities.updateButton(service.current_status);
      case 'updatelabel':
        return !rbServiceUtilities.updateButton(service.current_status);
      case 'add':
        return false;
    }
    return false;
  }

  async function _doServiceAction(service, action) {
    try {
      inActionServices[service.uuid] = 1;
      const result = await rbServiceDataService.serviceAction({
        service_name: service.uuid,
        action,
      });
      vm.actionSubject$.next(result);
      delete inActionServices[service.uuid];
    } catch (error) {
      $log.error('start service failed', error);
    }
  }

  async function startService(service) {
    await rbConfirmBox.show({
      title: translateService.get('start'),
      textContent: translateService.get('app_service_start_service_confirm', {
        service_name: service.service_name,
      }),
    });
    _doServiceAction(service, 'start');
  }

  async function stopService(service) {
    await rbConfirmBox.show({
      title: translateService.get('stop'),
      textContent: translateService.get('app_service_stop_service_confirm', {
        service_name: service.service_name,
      }),
    });
    _doServiceAction(service, 'stop');
  }

  async function deleteService(service) {
    await rbConfirmBox.show({
      title: translateService.get('delete'),
      textContent: translateService.get('delete_app_content', {
        app_name: service.service_name,
      }),
    });
    try {
      inActionServices[service.uuid] = 1;
      await rbServiceDataService.deleteService({
        service_name: service.uuid,
      });
      vm.actionSubject$.next({
        uuid: service.uuid,
        current_status: 'Deleting',
      });
      delete inActionServices[service.uuid];
    } catch (error) {
      $log.error('delete service failed', error);
    }
  }

  function updateService(service) {
    $state.go('app_service.service.update_service', {
      service_name: service.uuid,
    });
  }

  async function updateServiceLabels(service) {
    const service_name = service.uuid;
    const showLabelsDialog =
      service && affinityEnabled()
        ? () =>
            rbModal.show({
              title: translateService.get('labels_update'),
              component: 'rbServiceKubeLabelsDialog',
              locals: {
                service,
              },
            })
        : () =>
            rbLabelsDataService.showLabelsDialog(
              service.namespace,
              'service',
              service_name,
            );

    const labels = await showLabelsDialog().catch(() => null);

    if (labels) {
      vm.actionSubject$.next({
        uuid: service.uuid,
        labels: [...labels],
      });
    }
  }

  function affinityEnabled() {
    return (
      vm.currentRegion && vm.currentRegion.container_manager === 'KUBERNETES'
    );
  }

  function getServiceUrl(service) {
    return $state.href('app_service.service.service_detail', {
      service_name: service.uuid,
    });
  }
}
