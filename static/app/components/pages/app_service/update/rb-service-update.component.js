/**
 * Created by liudong on 16/9/12.
 */
const {
  SERVICE_RUNCOMMAND_TYPE,
  SERVICE_NETWORK_MODES,
  SERVICE_DEPLOY_MODEL,
} = require('../rb-service.constant');
const { CONTAINER_MANAGERS } = require('../../region/rb-region.constant');
const _ = require('lodash');
const templateStr = require('app/components/pages/app_service/update/rb-service-update.html');
angular.module('app.components.pages').component('rbServiceUpdate', {
  controller: rbServiceUpdateController,
  controllerAs: 'vm',
  bindings: {
    serviceName: '<',
    application: '<',
  },
  template: templateStr,
});

function rbServiceUpdateController(
  $scope,
  $timeout,
  $state,
  rbRouterStateHelper,
  rbRegionDataService,
  rbImageRepositoryUtilities,
  rbServiceUtilities,
  applicationService,
  rbLabelsDataService,
  rbServiceDataService,
  rbRegionService,
  rbToast,
  rbConfirmBox,
  translateService,
) {
  const vm = this;
  let transformedRepository;
  const SHARED_DRIVER_NAME = ['glusterfs', 'loongstore'];

  vm.onImageTagChange = onImageTagChange;
  vm.onInstanceSizeChange = onInstanceSizeChange;
  vm.onRunCommandTypeChange = onRunCommandTypeChange;
  vm.onBlueGreenTypeChange = onBlueGreenTypeChange;
  vm.cancel = cancel;
  vm.save = save;

  vm.addNodeLabels = addNodeLabels;
  vm.removeNodeLabels = removeNodeLabels;
  vm.checkAvailableNodes = checkAvailableNodes;

  vm.logFileFieldsEnabled = logFileFieldsEnabled;
  vm.validateUpdateStrategy = validateUpdateStrategy;
  vm.validateUnavailableValue = validateUnavailableValue;
  vm.updateStrategyEnabled = updateStrategyEnabled;
  vm.zeroCheckInDaemonSet = zeroCheckInDaemonSet;
  vm.isServiceHealthcheckEnabled =
    rbServiceUtilities.isServiceHealthcheckEnabled;
  vm.affinityEnabled = affinityEnabled;
  vm.$onInit = _init;

  //////////
  function onImageTagChange(option) {
    vm.serviceData.image_tag = option.value;
  }

  function onInstanceSizeChange(instance_size, config) {
    vm.serviceData.instance_size = instance_size;
    if (instance_size === 'CUSTOMIZED') {
      vm.serviceData.custom_instance_size = {
        cpu: config.cpu,
        mem: config.mem,
      };
    }
  }

  function onRunCommandTypeChange(option) {
    vm.useDefaultCmdType = option.value;
    if (vm.useDefaultCmdType === SERVICE_RUNCOMMAND_TYPE.DEFAULT) {
      vm.run_command = vm.imageData.run_command;
      vm.entrypoint = vm.imageData.entrypoint;
    } else {
      vm.run_command = vm.serviceData.run_command || '';
      if (vm.region.container_manager !== CONTAINER_MANAGERS.SWARM) {
        vm.entrypoint = vm.serviceData.entrypoint || '';
      }
    }
  }

  function onBlueGreenTypeChange(option) {
    vm.blueGreenType = option.value;
  }

  async function _init() {
    vm.initialized = false;
    vm.submitting = false;
    vm.isK8sUpdateNodeTagsEnabled = false;
    vm.availableNodesChecked = false;
    vm.init_node_selector = [];
    vm.node_selector = [];
    vm.SERVICE_RUNCOMMAND_TYPE = SERVICE_RUNCOMMAND_TYPE;
    vm.region = null;
    vm.regionFeatures = [];
    vm.imageTags = [];
    vm.imageData = {
      run_command: '',
      entrypoint: '',
      portList: [],
      volumes: [],
    };
    vm.useDefaultCmdType = SERVICE_RUNCOMMAND_TYPE.DEFAULT;
    vm.allowCustomDomain = false;
    vm.healthCheckEnabled = false;
    vm.runCommandTypes = [
      {
        name: translateService.get('use_default'),
        value: SERVICE_RUNCOMMAND_TYPE.DEFAULT,
      },
      {
        name: translateService.get('use_custom'),
        value: SERVICE_RUNCOMMAND_TYPE.CUSTOM,
      },
    ];
    vm.blueGreenTypes = [
      {
        name: translateService.get('switch_off'),
        value: false,
      },
      {
        name: translateService.get('switch_on'),
        value: true,
      },
    ];
    /////////
    vm.serviceData = {};
    vm.serviceList = [];
    vm.envFileList = [];
    // linkServices
    vm.customDomains = [];
    vm.includeLogFiles = [];
    vm.excludeLogFiles = [];
    // envVars
    vm.envFiles = [];
    vm.healthChecks = [];
    vm.run_command = '';
    vm.entrypoint = '';
    // blue green
    vm.blueGreenType = false;
    vm.blueGreenCount = 0;
    vm.update_strategy = {
      max_surge: '',
      max_surge_disabled: false,
      max_unavailable: '',
      max_unavailable_disabled: false,
    };
    // update service node selectors
    vm.originAvailableNodeLables = [];
    // get service detail
    applicationService
      .applicationDetail({
        service_name: vm.serviceName,
        application: vm.application,
      })
      .then(data => {
        vm.serviceData = data['detail'];
        vm.init_custom_instance_size = vm.serviceData.custom_instance_size;
        rbRegionService.setRegionByName(vm.serviceData.region.name);
        if (vm.serviceData.node_tag || vm.serviceData.node_selector) {
          _initNodeLables(
            vm.serviceData.node_tag,
            vm.serviceData.node_selector,
          );
        }
        vm.pod_controller = _.get(
          vm.serviceData,
          'pod_controller',
          'Deployment',
        );
        if (!_.isEmpty(vm.serviceData.update_strategy)) {
          vm.update_strategy.max_surge = _.get(
            vm.serviceData,
            'update_strategy.max_surge',
            '',
          ).toString();
          vm.update_strategy.max_unavailable = _.get(
            vm.serviceData,
            'update_strategy.max_unavailable',
            '',
          ).toString();
        }
        // volume with ebs driver and macvlan network mode can not support multiple instance
        // so max surge is zero
        if (vm.serviceData.network_mode === SERVICE_NETWORK_MODES.MACVLAN) {
          vm.update_strategy.max_surge = '0%';
          vm.update_strategy.max_surge_disabled = true;
        }
        // unshared volumes exist, value of surge and unavailable is assigned
        if (vm.serviceData.volumes && vm.serviceData.volumes.length) {
          const hasUnsharedVolume = vm.serviceData.volumes.some(vo => {
            return !SHARED_DRIVER_NAME.includes(vo.driver_name);
          });
          if (hasUnsharedVolume) {
            vm.update_strategy.max_surge = '0%';
            vm.update_strategy.max_unavailable = '100%';
            vm.update_strategy.max_surge_disabled = true;
            vm.update_strategy.max_unavailable_disabled = true;
          }
        }
        // custom command must have value
        if (vm.serviceData.run_command || vm.serviceData.entrypoint) {
          vm.useDefaultCmdType = SERVICE_RUNCOMMAND_TYPE.CUSTOM;
          vm.run_command = vm.serviceData.run_command;
          vm.entrypoint = vm.serviceData.entrypoint;
        } else {
          vm.useDefaultCmdType = SERVICE_RUNCOMMAND_TYPE.DEFAULT;
        }
        vm.defaultInstanceSize = vm.serviceData.instance_size;
        // env files
        // 默认更新环境变量文件
        vm.envFiles = vm.serviceData.envfiles.map(item => {
          item.update = true;
          return item;
        });
        // env vars
        vm.envVars = [];
        if (vm.serviceData.instance_envvars) {
          _.forIn(vm.serviceData.instance_envvars, (value, key) => {
            if (
              ![
                '__ALAUDA_FILE_LOG_PATH__',
                '__ALAUDA_EXCLUDE_LOG_PATH__',
              ].includes(key)
            ) {
              vm.envVars.push({
                name: key,
                value: value,
              });
            }
          });
        }
        // file log path
        if (vm.serviceData.instance_envvars['__ALAUDA_FILE_LOG_PATH__']) {
          vm.includeLogFiles = vm.serviceData.instance_envvars[
            '__ALAUDA_FILE_LOG_PATH__'
          ].split(',');
        }
        if (vm.serviceData.instance_envvars['__ALAUDA_EXCLUDE_LOG_PATH__']) {
          vm.excludeLogFiles = vm.serviceData.instance_envvars[
            '__ALAUDA_EXCLUDE_LOG_PATH__'
          ].split(',');
        }
        // custom domain
        vm.allowCustomDomain = _.some(vm.serviceData.instance_ports, item => {
          if (item.endpoint_type === 'http-endpoint') {
            return true;
          }
        });
        if (vm.serviceData.custom_domain_name) {
          vm.customDomains = vm.serviceData.custom_domain_name.split(';');
        }
        // health checks enabled
        // vm.healthCheckEnabled = SERVICE_NETWORK_MODES.BRIDGE === vm.serviceData.network_mode && (vm.serviceData.raw_container_ports.length === 0) ||
        // [SERVICE_NETWORK_MODES.MACVLAN, SERVICE_NETWORK_MODES.FLANNEL].includes(vm.serviceData.network_mode);
        // linked_to_services
        vm.linkServices = [];
        if (!vm.application) {
          if (
            vm.serviceData.linked_to_services &&
            vm.serviceData.linked_to_services.length > 0
          ) {
            _.forEach(vm.serviceData.linked_to_services, item => {
              vm.linkServices.push({
                //dest: item.dest,  // TODO: remove legacy field 'dest'
                alias: item.dest_alias,
                dest_uuid: item.dest_uuid,
                dest_name: item.dest_name,
              });
            });
          }
          applicationService
            .applicationList({
              region_name: vm.serviceData.region.name,
            })
            .then(data => {
              if (data.list) {
                const serviceList = angular.fromJson(data.list);
                angular.forEach(serviceList, item => {
                  if (item.service_name !== vm.serviceData.service_name) {
                    vm.serviceList.push({
                      name: item.service_name,
                      uuid: item.uuid,
                      space_name: item.space_name,
                    });
                  }
                });
              }
            });
        }
        // config files
        if (vm.serviceData.mount_points) {
          vm.serviceData.configFiles = _.map(
            vm.serviceData.mount_points,
            item => {
              if (item.type === 'raw') {
                item.rawValue = item.value;
              } else {
                item.config = _.get(item.value, 'name');
                item.uuid = _.get(item.value, 'uuid');
                item.key = _.get(item.value, 'key');
                delete item.value;
              }
              return item;
            },
          );
        } else {
          vm.serviceData.configFiles = [];
        }
        // region features
        const regionListPromise = rbRegionDataService
          .getRegions()
          .then(data => {
            const regions = data['regions'];
            _.forEach(regions, region => {
              if (region.name === vm.serviceData.region.name) {
                vm.region = region;
                vm.isK8sUpdateNodeTagsEnabled = rbServiceUtilities.isK8sUpdateNodeTagsEnabled(
                  vm.region,
                );
                vm.regionFeatures = _.get(
                  region,
                  'features.service.features',
                  [],
                );
                vm.healthCheckEnabled =
                  vm.region.container_manager !== CONTAINER_MANAGERS.SWARM &&
                  vm.healthCheckEnabled;
                return false;
              }
            });
            vm.regionFileLogEnabled = vm.regionFeatures.includes('file-log');
            vm.regionBlueGreenEnabled = vm.regionFeatures.includes(
              'blue-green',
            );
            vm.regionConfigManageEnabled = vm.regionFeatures.includes(
              'mount-points',
            );
          });
        const registryPromise = rbImageRepositoryUtilities.getTransformedRepository(
          vm.serviceData.image_name,
        );
        Promise.all([regionListPromise, registryPromise])
          .then(async ([, obj]) => {
            transformedRepository = obj;
            _getImageTags();
            _getImageDetail();
          })
          .catch(() => {})
          .then(() => {
            vm.initialized = true;
            $timeout(() => {
              $scope.$broadcast('rzSliderForceRender');
            });
          });

        //service affinity
        _initServiceAffinity();

        // watch image tag changes
        $scope.$watch(
          () => {
            return vm.serviceData.image_tag;
          },
          (oldValue, newValue) => {
            if (oldValue !== undefined && newValue !== oldValue) {
              _getImageDetail();
            }
          },
        );
      })
      .catch(() => {
        $state.go('app_service.service.list');
        rbToast.error(translateService.get('service_not_exist'));
      });
  }

  function cancel() {
    rbRouterStateHelper.back();
  }

  function save() {
    vm.form.$setSubmitted();
    checkAvailableNodes();
    if (vm.form.$invalid) {
      return;
    }

    rbConfirmBox
      .show({
        title: translateService.get('update_service'),
        textContent: translateService.get('service_update_confirm', {
          service_name: vm.serviceData.service_name,
        }),
      })
      .then(() => {
        _save();
      });
  }

  function _save() {
    const udpate_strategy = {
      max_surge: vm.update_strategy.max_surge,
      max_unavailable: vm.update_strategy.max_unavailable,
    };
    const serviceData = {
      namespace: vm.serviceData.namespace,
      service_name: vm.serviceName,
      application: vm.application,
      image_tag: vm.serviceData.image_tag,
      update_strategy: udpate_strategy,
      instance_size: vm.serviceData.instance_size,
      custom_instance_size: vm.serviceData.custom_instance_size,
      run_command: '',
      pod_controller: vm.pod_controller,
      entrypoint: '',
      labels: vm.serviceData.labels
        ? vm.serviceData.labels.filter(label => label.editable)
        : undefined,
    };
    // k8s update node selector
    if (vm.isK8sUpdateNodeTagsEnabled) {
      const target = {};
      vm.node_selector.map(r => {
        target[r.key] = r.value;
      });
      serviceData.node_selector = target;
      serviceData.node_tag = '';
    }
    if (vm.useDefaultCmdType === SERVICE_RUNCOMMAND_TYPE.CUSTOM) {
      serviceData.run_command = vm.run_command;
      if (vm.region.container_manage !== CONTAINER_MANAGERS.SWARM) {
        serviceData.entrypoint = vm.entrypoint;
      }
    }
    // linked_to_apps
    if (vm.application) {
      // do not update links for service in application
    } else {
      const linked_to_apps = {};
      let linkServices = _.filter(vm.linkServices, link => {
        return !!link.alias;
      });
      linkServices = _.uniqWith(linkServices, (a, b) => {
        return a.uuid === b.uuid;
      });
      _.forEach(linkServices, link => {
        linked_to_apps[link['uuid']] = link['alias'];
      });
      serviceData.linked_to_apps = JSON.stringify(linked_to_apps);
    }
    // env vars
    serviceData.instance_envvars = {};
    const envVars = _.filter(vm.envVars, envVar => {
      return !!envVar.name;
    });
    _.forEach(envVars, item => {
      serviceData.instance_envvars[item.name] = item.value;
    });
    // log files
    const excludeLogFiles = _.filter(vm.excludeLogFiles, item => {
      return !!item;
    });
    const includeLogFiles = _.filter(vm.includeLogFiles, item => {
      return !!item;
    });
    if (excludeLogFiles.length > 0) {
      serviceData.instance_envvars[
        '__ALAUDA_EXCLUDE_LOG_PATH__'
      ] = excludeLogFiles.join(',');
    }
    if (includeLogFiles.length > 0) {
      serviceData.instance_envvars[
        '__ALAUDA_FILE_LOG_PATH__'
      ] = includeLogFiles.join(',');
    }
    // env files
    if (vm.envFiles.length > 0) {
      let envFiles = _.cloneDeep(vm.envFiles);
      envFiles = _.uniqWith(envFiles, (a, b) => {
        return a.uuid === b.uuid;
      });
      _.forEach(envFiles, _file => {
        if ('file' in _file) {
          delete _file.file;
        }
        if (_file.update && _file.content) {
          delete _file.content;
        }
        delete _file.update;
        delete _file.file;
      });
      serviceData.envfiles = envFiles;
    } else {
      serviceData.envfiles = [];
    }
    // custom domains
    if (vm.allowCustomDomain) {
      vm.customDomains = _.filter(vm.customDomains, item => {
        return !!item;
      });
      serviceData.custom_domain_name = vm.customDomains.join(';');
    }

    // health checks
    if (vm.serviceData.health_checks) {
      serviceData.health_checks = vm.serviceData.health_checks;
    }

    // config manage
    if (vm.regionConfigManageEnabled && vm.serviceData.configFiles) {
      serviceData.mount_points = _.map(
        vm.serviceData.configFiles,
        rbServiceUtilities.formatConfigFileData,
      );
    } else {
      delete serviceData.mount_points;
    }
    // blue green
    if (vm.regionBlueGreenEnabled) {
      serviceData['blue_green'] = !!vm.blueGreenType;
      serviceData['green_num'] = vm.blueGreenCount;
    }
    vm.submitting = true;
    const params = {
      image_data: serviceData,
      volume_data: vm.serviceData.volumes,
    };
    // update strategy
    if (
      serviceData.update_strategy.max_surge &&
      serviceData.update_strategy.max_unavailable
    ) {
      Object.keys(serviceData.update_strategy).forEach(k => {
        if (k === 'max_surge' || k === 'max_unavailable') {
          serviceData.update_strategy[k] = serviceData.update_strategy[
            k
          ].toString();
          if (!serviceData.update_strategy[k].endsWith('%')) {
            serviceData.update_strategy[k] = parseInt(
              serviceData.update_strategy[k],
            );
          }
        }
      });
    }
    if (
      !updateStrategyEnabled() ||
      vm.region.container_manager !== 'KUBERNETES'
    ) {
      delete serviceData.update_strategy;
    }

    // kubernetes affinity
    if (affinityEnabled()) {
      const kube_affinity = _.cloneDeep(vm.kubeAffinity);
      serviceData.kube_config = {
        ...(serviceData.kube_config || {}),
        pod: kube_affinity.pod,
      };
    }
    applicationService
      .applicationUpdate(params)
      .then(() => {
        rbToast.success(translateService.get('service_update_success'));
        $state.go('app_service.service.service_detail', {
          service_name: vm.serviceName,
          application: vm.application,
        });
      })
      .catch(() => {
        rbToast.error(translateService.get('service_update_failed'));
      })
      .then(() => {
        vm.submitting = false;
      });
  }

  function _getImageTags() {
    vm.imageTagsLoading = true;
    rbImageRepositoryUtilities
      .getTransformedRepositoryTags(transformedRepository)
      .then(tags => {
        vm.imageTagsLoading = false;
        vm.imageTags = tags;
        if (!tags.length) {
          rbToast.warning(translateService.get('get_image_tags_failed'));
        }
      });
  }

  function _getImageDetail() {
    vm.imageTagDetailLoading = true;
    transformedRepository.tag_name = vm.serviceData.image_tag;
    rbImageRepositoryUtilities
      .getTransformedRepositoryTagDetail(transformedRepository)
      .then(result => {
        vm.imageTagDetailLoading = false;
        if (!result) {
          rbToast.warning(translateService.get('get_image_tags_failed'));
        } else {
          _initImageData(result);
        }
      });
  }

  function _initImageData(data) {
    vm.imageData.run_command = data.run_command || '';
    vm.imageData.entrypoint = data.entrypoint || '';
    if (vm.useDefaultCmdType === SERVICE_RUNCOMMAND_TYPE.DEFAULT) {
      vm.run_command = vm.imageData.run_command;
      vm.entrypoint = vm.imageData.entrypoint;
    }
    vm.imageData.portList.length = 0;
    if (data.instance_ports) {
      const ports = data.instance_ports.split(',');
      _.forEach(ports, port => {
        vm.imageData.portList.push(parseInt(port));
      });
    }
    vm.imageData.volumes = data.volumes;
  }

  async function _initServiceAffinity() {
    if (!affinityEnabled()) {
      return;
    }

    vm.kubeAffinity = {
      pod: vm.serviceData.kube_config && vm.serviceData.kube_config.pod,
    };
    vm.loadingServiceAffinity = true;

    vm.propagateOnly = !!vm.serviceData.application;

    try {
      const [services, serviceLabels, nodeLabels] = await Promise.all([
        rbServiceDataService
          .getServices({
            region_name: vm.serviceData.region_name,
            page_size: 100,
          })
          .then(({ results }) => {
            return results;
          })
          .catch(() => null),
        rbLabelsDataService.getAllLebels().catch(() => null),
        rbRegionDataService
          .getRegionLabels(vm.serviceData.region_name)
          .then(data => {
            vm.availableNodeLables =
              data.labels.length &&
              data.labels.map(r => {
                return _.extend({ display: `${r.key}: ${r.value}` }, r);
              });
            vm.originAvailableNodeLables = vm.availableNodeLables;
          })
          .catch(() => null),
      ]);

      if (services) {
        vm.services = services;
      }

      if (serviceLabels) {
        vm.serviceLabels = serviceLabels.results || [];
      }

      vm.labelKeys = _.chain(vm.serviceLabels)
        .map(label => label.key)
        .uniq()
        .value();
      vm.labelValues = _.chain(vm.serviceLabels)
        .map(label => label.value)
        .uniq()
        .value();

      if (nodeLabels) {
        vm.nodeLabels = nodeLabels.labels || [];
      }
    } finally {
      vm.loadingServiceAffinity = false;
    }
  }

  function affinityEnabled() {
    return (
      _.get(vm.serviceData, 'container_manager') === 'KUBERNETES' &&
      _.get(vm.serviceData, 'pod_controller') !== 'DaemonSet'
    );
  }

  function logFileFieldsEnabled() {
    return vm.regionFileLogEnabled;
  }

  function validateUpdateStrategy(value, type) {
    return type
      ? parseInt(vm.update_strategy.max_surge) || parseInt(value)
      : parseInt(value) || parseInt(vm.update_strategy.max_unavailable);
  }

  function validateUnavailableValue(value) {
    return !(value.endsWith('%') && parseInt(value) > 100);
  }

  function zeroCheckInDaemonSet(value) {
    return !(
      vm.pod_controller === SERVICE_DEPLOY_MODEL.DAEMONSET &&
      _.get(vm.region, 'features.kubernetes.version', '') >= '1.6' &&
      value === '0'
    );
  }

  function updateStrategyEnabled() {
    return (
      _.get(vm.region, 'container_manager') === 'KUBERNETES' &&
      ((vm.pod_controller === SERVICE_DEPLOY_MODEL.DAEMONSET &&
        _.get(vm.region, 'features.kubernetes.version', '') >= '1.6') ||
        vm.pod_controller === SERVICE_DEPLOY_MODEL.DEPLOYMENT)
    );
  }

  // update service node selectors
  function addNodeLabels(option) {
    vm.node_selector.push(option);
    vm.availableNodeLables.map(r => {
      if (r.key === option.key && r.value !== option.value) {
        r.op_disabled = true;
      }
    });
    vm.availableNodesChecked = false;
  }

  function removeNodeLabels(option) {
    vm.node_selector = vm.node_selector.filter(r => {
      return r.key !== option.key;
    });
    vm.availableNodeLables.map(r => {
      if (r.key === option.key) {
        r.op_disabled = false;
      }
    });
    vm.availableNodesChecked = false;
  }

  function checkAvailableNodes() {
    let kvStr = '';
    vm.node_selector.map(r => {
      kvStr += `${r.key}:${r.value},`;
    });
    kvStr = kvStr.substring(0, kvStr.length - 1);
    rbRegionDataService
      .checkAvailableNodes(kvStr, vm.region.name)
      .then(data => {
        vm.availableNodesChecked = true;
        vm.availableNodes = data.nodes.length;
      })
      .catch(() => {
        vm.availableNodesChecked = false;
      });
  }

  function _initNodeLables(tag, nodes) {
    if (tag) {
      const _tag = tag.split(':').join(': ');
      vm.init_node_selector.push(_tag);
    }
    if (nodes) {
      Object.keys(nodes).forEach(k => {
        vm.init_node_selector.push(`${k}: ${vm.serviceData.node_selector[k]}`);
      });
    }
  }
}
