const templateStr = require('app/components/pages/auth-redirect/rb-auth-redirect.html');
angular
  .module('app.components.pages')

  /**
   * Component which displays a simple info for the user about the current auth redirecting progress
   * For now, there is only link request for private build code repos
   */
  .component('rbAuthRedirect', {
    bindings: {
      code: '<', // Oauth Authorization code
      state: '<', // Oauth State variable
      redirectParams: '<', // Decoded redirect params
      codeClientName: '<', // e.g., GITHUB
    },
    controller: rbAuthRedirectController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbAuthRedirectController(
  rb2RouterUtil,
  BUILD_CONSTANTS,
  translateService,
  rbBuildDataService,
) {
  const vm = this;

  vm.$onInit = init;

  vm.message = translateService.get('auth_redirect_link_code_client', {
    name: BUILD_CONSTANTS.BUILD_OAUTH_CODE_REPOS[vm.codeClientName],
  });

  /////////
  async function init() {
    try {
      // Hmm... maybe the link function should not reside in build data service?
      await rbBuildDataService.linkPrivateBuildCodeClient({
        code: vm.code,
        state: vm.state,
        codeClientName: vm.codeClientName,
        redirectParams: vm.redirectParams,
      });

      const stateParams = {
        oauthCodeRepoClientName: vm.codeClientName,
      };

      const { nextStateName, nextStateParams } = vm.redirectParams;

      if (nextStateParams) {
        Object.assign(stateParams, nextStateParams);
      }

      Object.keys(stateParams).forEach(key => {
        const value = stateParams[key];
        if (value === null || value === undefined) {
          delete stateParams[key];
        }
      });

      // Link success, go to next state name
      rb2RouterUtil.go(nextStateName, stateParams);
    } catch ({ data }) {
      vm.hasError = true;
      if (data.errors.find(error => error.code === 'token_expired')) {
        vm.message = translateService.get('build_code_repo_token_expired');
      } else {
        vm.message = translateService.get('auth_failed_error_hint');
      }
    }
  }
}
