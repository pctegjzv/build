module.exports = {
  states: [
    {
      // clientName: the Oauth code repo client
      // code: Auth code replied from third party website
      // state: Required field for validating by the backend
      url: '/auth-redirect/:clientName?code&state&redirect_params',
      name: 'auth-redirect',
      component: 'rbAuthRedirect',

      // TODO: is there a better way to resolve the bindings defined in the state URL?
      resolve: {
        codeClientName: $stateParams => {
          'ngInject';
          return $stateParams['clientName'];
        },
        code: $stateParams => {
          'ngInject';
          return $stateParams['code'];
        },
        state: $stateParams => {
          'ngInject';
          return $stateParams['state'];
        },
        redirectParams: $stateParams => {
          'ngInject';
          return JSON.parse($stateParams['redirect_params']);
        },
      },
    },
  ],
};
