const templateStr = require('app/components/pages/blank/rb-blank-page.html');
angular.module('app.components.pages').component('rbBlankPage', {
  controller: rbBlankPageController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbBlankPageController() {}
