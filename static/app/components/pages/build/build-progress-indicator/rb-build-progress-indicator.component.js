const templateStr = require('app/components/pages/build/build-progress-indicator/rb-build-progress-indicator.html');
angular.module('app.components.pages').component('rbBuildProgressIndicator', {
  controller: rbBuildProcessIndicatorController,
  controllerAs: 'vm',
  template: templateStr,
  bindings: {
    stepAt: '<',
  },
});

function rbBuildProcessIndicatorController() {}
