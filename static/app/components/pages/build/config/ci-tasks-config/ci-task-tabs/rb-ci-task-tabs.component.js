import templateString from './rb-ci-task-tabs.component.html';
import { BUILD_CI_TASK_TYPES as TASK_TYPES } from 'app/components/pages/build/rb-build.constant';

angular.module('app.components.pages').component('rbCiTaskTabs', {
  template: templateString,
  controller: rbCiTaskTabsController,
  controllerAs: 'vm',
  bindings: {
    onTasksChanged: '&',
    onTaskSelected: '&',
    onTaskDeleted: '&',
    formGroup: '<',
    init: '<',
  },
  require: {
    configCtrl: '^rbCiTasksConfig',
  },
});

function rbCiTaskTabsController(rbModal, translateService) {
  const vm = this;

  vm.$onInit = _onInit;
  vm.$onChanges = _onChanges;
  vm.addTask = addTask;
  vm.deleteTask = deleteTask;
  vm.selectTask = selectTask;
  vm.checkTabStatus = checkTabStatus;
  vm.checkButtonStatus = checkButtonStatus;

  function _onChanges({ init }) {
    if (init) {
      vm.taskList = init.currentValue || [];
      taskListChanged();
      selectTask(vm.taskList[0]);
    }
  }

  function _onInit() {
    vm.availableTypes = TASK_TYPES;
    vm.configCtrl._removeAllTask = removeAllTask;
  }

  function addTask() {
    rbModal
      .show({
        title: translateService.get('build_config_add_ci_task'),
        component: 'rbSelectCiTaskType',
        locals: {
          types: vm.availableTypes,
        },
      })
      .then(type => {
        vm.taskList.push(type);
        taskListChanged();
        selectTask(type);
      })
      .catch(() => {});
  }

  function deleteTask(index) {
    const [deletedTask] = vm.taskList.splice(index, 1);
    taskListChanged();
    vm.onTaskDeleted({ $task: deletedTask });
    selectTask(vm.taskList[0]);
  }

  function taskListChanged() {
    vm.availableTypes = TASK_TYPES.filter(type => !vm.taskList.includes(type));
    vm.onTasksChanged({ $tasks: vm.taskList });
  }

  function selectTask(task) {
    vm.selectedTask = task;
    vm.onTaskSelected({ $task: task });
  }

  function removeAllTask() {
    vm.taskList = [];
    taskListChanged();
  }

  function checkTabStatus(task) {
    const isSelected = vm.selectedTask === task;
    const isInvalid =
      vm.formGroup[task] &&
      vm.formGroup[task].$invalid &&
      vm.formGroup[task].$dirty;
    return {
      'task-tab--normal': !isSelected && !isInvalid,
      'task-tab--selected': isSelected,
      'task-tab--invalid': isInvalid,
    };
  }

  function checkButtonStatus() {
    const isInvalid = vm.formGroup.$self.$dirty && !vm.formGroup.$self.$valid;
    return {
      'task-tab--normal': !isInvalid,
      'task-tab--invalid': isInvalid,
    };
  }
}
