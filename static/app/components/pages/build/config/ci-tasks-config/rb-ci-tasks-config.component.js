import templateString from './rb-ci-tasks-config.component.html';

angular.module('app.components.pages').component('rbCiTasksConfig', {
  template: templateString,
  controller: rbCiTasksConfigController,
  controllerAs: 'vm',
  bindings: {
    model: '<',
    source: '<',
    disabled: '<',
  },
});

function rbCiTasksConfigController() {
  const vm = this;

  vm.$onInit = _onInit;
  vm.$onChanges = _onChanges;
  vm.handleTaskListChanged = handleTaskListChanged;
  vm.handleTaskDeleted = handleTaskDeleted;
  vm.getFormComponent = getFormComponent;
  vm.checkFormValid = checkFormValid;
  vm.setDirty = setDirty;
  vm.removeAllTask = removeAllTask;

  function _onChanges({ model }) {
    if (model && model.currentValue) {
      vm.taskList = Object.keys(model.currentValue);
      vm.currentTask = vm.taskList[0];
    }
  }

  function _onInit() {
    vm.formGroup = { $self: {} };
  }

  function handleTaskListChanged(tasks) {
    vm.taskList = tasks;
    vm.formGroup.$self.$valid = tasks.length > 0;
  }

  function handleTaskDeleted(task) {
    delete vm.model[task];
    delete vm.formGroup[task];
    vm.formGroup.$self.$dirty = true;
  }

  function getFormComponent(taskType) {
    const componentName = `rb-ci-task-form-${taskType}`;
    return (vm.formComponent = `<${componentName} model="vm.model[task]" form="vm.formGroup[task]" source="vm.source"></${componentName}>`);
  }

  function removeAllTask() {
    vm._removeAllTask();
    const copyData = JSON.parse(JSON.stringify(vm.model));
    Object.keys(vm.model).forEach(key => {
      delete vm.model[key];
    });
    vm.formGroup = { $self: {} };
    return copyData;
  }

  function setDirty(dirty = true) {
    if (vm.disabled) {
      dirty = false;
    }
    Object.entries(vm.formGroup).forEach(([, form]) => {
      form.$dirty = dirty;
      if (dirty) {
        form.$submitted = form.$submitted ? form.$submitted++ : 1;
      } else {
        form.$submitted = false;
      }
    });
  }

  function checkFormValid() {
    if (vm.disabled) {
      return true;
    }
    const formValid = Object.entries(vm.formGroup).every(
      ([, form]) => form.$valid,
    );
    return vm.taskList && vm.taskList.length > 0 && formValid;
  }
}
