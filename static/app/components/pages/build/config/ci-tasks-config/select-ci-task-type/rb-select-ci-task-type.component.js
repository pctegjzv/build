import templateString from './rb-select-ci-task-type.component.html';

angular.module('app.components.pages').component('rbSelectCiTaskType', {
  template: templateString,
  controller: rbSelectCiTaskTypeController,
  controllerAs: 'vm',
  bindings: {
    types: '<',
  },
});

function rbSelectCiTaskTypeController(rbModal, WEBLABS) {
  const vm = this;

  vm.$onInit = _onInit;

  function _onInit() {
    vm.sonarqubeEnabled = WEBLABS['CI_INTEGRATION_SONARQUBE'];
  }

  vm.handleSelected = handleSelected;

  function handleSelected(type) {
    if (type === 'sonarqube' && !vm.sonarqubeEnabled) {
      return;
    }
    rbModal.hide(type);
  }
}
