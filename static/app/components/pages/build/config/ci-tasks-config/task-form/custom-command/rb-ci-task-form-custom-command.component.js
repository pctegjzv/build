import templateString from './rb-ci-task-form-custom-command.component.html';

angular.module('app.components.pages').component('rbCiTaskFormCustomCommand', {
  template: templateString,
  controller: rbCiTaskFormCustomCommandController,
  controllerAs: 'vm',
  bindings: {
    model: '=',
    form: '=',
    source: '<',
  },
});

function rbCiTaskFormCustomCommandController($scope, rbGlobalSetting) {
  const vm = this;

  vm.$onInit = _onInit;
  vm.$postLink = _postLink;
  vm.showCommandExample = showCommandExample;

  function _onInit() {
    vm.editorOptions = rbGlobalSetting.getEditorOptions('shell');
  }

  function _postLink() {
    vm.form = $scope.form;

    // A dirty fix to make sure code mirror will be correctly displayed within pipeline
    setTimeout(() => (vm.refreshCodeMirror = true), 200);
  }

  function showCommandExample() {
    if (!vm.model) {
      vm.model = {};
    }
    vm.model.ci_steps = vm.source.langCatalog[vm.source.ciLanguage].find(
      item => item.runtime === vm.source.ciRuntime,
    ).default_ci_steps;
  }
}
