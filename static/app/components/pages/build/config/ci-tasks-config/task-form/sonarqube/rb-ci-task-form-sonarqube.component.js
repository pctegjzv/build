import templateString from './rb-ci-task-form-sonarqube.component.html';

const SUPPORTED_CODED_FORMATS = ['UTF-8', 'US-ASCII', 'Big5', 'GB2312', 'GBK'];

angular.module('app.components.pages').component('rbCiTaskFormSonarqube', {
  template: templateString,
  controller: rbCiTaskFormSonarqubeController,
  controllerAs: 'vm',
  bindings: {
    model: '=',
    form: '=',
    source: '<',
  },
});

function rbCiTaskFormSonarqubeController(
  $scope,
  rbIntegrationDataService,
  rbBuildDataService,
  translateService,
  rbToast,
) {
  const vm = this;

  vm.$onInit = _onInit;
  vm.$postLink = _postLink;
  vm.handleIntegrationChanged = handleIntegrationChanged;

  function _onInit() {
    rbIntegrationDataService
      .getIntegrationList({ types: 'SonarQube', page: 1, page_size: 100 })
      .then(integrations =>
        integrations
          .filter(item => item.enabled)
          .map(({ id, name }) => ({ id, name })),
      )
      .then(integrations => {
        vm.integrations = integrations || [];
        if (vm.integrations.length === 0) {
          vm.languages = [
            {
              name: translateService.get('pipeline_select_none'),
              key: '',
            },
          ];
          vm.qualityGates = [];
          rbToast.error(
            translateService.get('build_sonarqube_integrations_empty'),
          );
        }
      })
      .catch(() => {
        rbToast.error(
          translateService.get('build_sonarqube_get_integrations_failed'),
        );
      });

    if (!vm.model) {
      vm.model = {};
      vm.model.build_integration_config = {};
    }
    vm.codedFormats = SUPPORTED_CODED_FORMATS;
  }

  function _postLink() {
    vm.form = $scope.form;
  }

  function handleIntegrationChanged(integration) {
    vm.model.integration_instance_id = integration.id;
    rbBuildDataService
      .getSonarqubeLanguages(integration.id)
      .then(res => {
        vm.languages = res.languages;
        vm.languages.unshift({
          name: translateService.get('pipeline_select_none'),
          key: '',
        });
      })
      .catch(() => {
        vm.languages = [
          {
            name: translateService.get('pipeline_select_none'),
            key: '',
          },
        ];
        rbToast.error(
          translateService.get('build_sonarqube_get_languages_failed'),
        );
      });
    rbBuildDataService
      .getSonarqubeQualityGates(integration.id)
      .then(res => {
        vm.qualityGates = res.qualitygates;
        vm.defaultQuality = res.qualitygates.find(
          gate => gate.id === res.default,
        ).name;
      })
      .catch(() => {
        vm.qualityGates = [];
        rbToast.error(
          translateService.get('build_sonarqube_get_qualityGates_failed'),
        );
      });
  }
}
