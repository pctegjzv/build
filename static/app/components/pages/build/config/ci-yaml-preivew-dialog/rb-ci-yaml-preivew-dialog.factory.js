angular
  .module('app.components.pages')
  .factory('rbCiYamlPreviewDialog', rbCiYamlPreviewDialog);

function rbCiYamlPreviewDialog(rbModal) {
  return {
    show: data => {
      return rbModal
        .show({
          component: 'rbCiYamlPreview',
          locals: {
            data,
          },
          closable: true,
          autofocus: true,
        })
        .catch(() => {});
    },
  };
}
