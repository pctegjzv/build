import templateStr from './rb-ci-yaml-preivew.component.html';

angular.module('app.components.pages').component('rbCiYamlPreview', {
  template: templateStr,
  controller: rbCiYamlPreviewController,
  controllerAs: 'vm',
  bindings: {
    data: '<',
  },
});

function rbCiYamlPreviewController(rbGlobalSetting, rbBuildDataService) {
  const vm = this;

  vm.$onInit = _onInit;

  function _onInit() {
    vm.editorOptions = rbGlobalSetting.getEditorOptions('yaml');
    vm.editorOptions.readOnly = 'view';

    rbBuildDataService
      .getCiYaml(vm.data)
      .then(({ yml }) => {
        vm.yaml = yml;
      })
      .catch(() => {});
  }
}
