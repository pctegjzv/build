const templateStr = require('app/components/pages/build/config/create/basic-info/rb-build-config-create-basic-info.html');
angular
  .module('app.components.pages')
  .component('rbBuildConfigCreateBasicInfo', {
    bindings: {
      model: '<',
      form: '=',
      notifications: '<',
      oauthCodeRepoClient: '<',
      codeBranch: '<',
      buildEndpoints: '<',
    },
    controller: rbBuildConfigCreateBasicInfoController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbBuildConfigCreateBasicInfoController(
  $scope,
  rbQuotaSpaceDataService,
  rbSafeApply,
  BUILD_CONSTANTS,
  WEBLABS,
) {
  const vm = this;
  vm.$onInit = _init;
  vm.$onChanges = _onChanges;
  vm.$postLink = _link;
  vm.onSpaceChanged = onSpaceChanged;
  vm.onNotificationChange = onNotificationChange;
  vm.onInstanceSizeChange = onInstanceSizeChange;

  ////////

  async function _init() {
    vm.weblabs = WEBLABS;

    vm.quotaEnabled = WEBLABS['quota_enabled'];
    if (vm.quotaEnabled) {
      vm.spaces = await rbQuotaSpaceDataService.getConsumableSpaces();
      rbSafeApply();
    }
  }

  function onSpaceChanged(space) {
    vm.model.space_name = space.name;
  }

  vm.endpointSelected = option => {
    vm.model.endpoint_id = option.endpoint_id;
  };

  function _onChanges({ codeBranch }) {
    if (codeBranch && codeBranch.currentValue) {
      vm.cannotEnableTimingBuild = codeBranch.currentValue.includes('*');
      if (vm.cannotEnableTimingBuild) {
        vm.model.timingBuildEnabled = false;
      }
    }
  }

  function _link() {
    vm.form = $scope.form;

    vm.model.imageCacheEnabled = true;
    vm.model.timingRule = '0 8 * * *';

    vm.instanceSizeInit = {
      cpu: BUILD_CONSTANTS.BUILD_CONFIG_DEFAULT_CONTAINER_SIZE['CPU'],
      memory: BUILD_CONSTANTS.BUILD_CONFIG_DEFAULT_CONTAINER_SIZE['MEMORY'],
    };
  }
  function onNotificationChange(option) {
    vm.model.notification = option.name;
    vm.model.notification_uuid = option.uuid;
  }

  function onInstanceSizeChange({ cpu, memory }) {
    vm.model.memory = memory;
    vm.model.cpu = cpu;
  }
}
