const templateStr = require('app/components/pages/build/config/create/ci/rb-build-config-create-ci.html');
angular.module('app.components.pages').component('rbBuildConfigCreateCi', {
  bindings: {
    model: '<',
    form: '=',
    ciEnvCatalogs: '<',
    configStep: '<',
  },
  controller: rbBuildConfigCreateCiController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbBuildConfigCreateCiController(
  $scope,
  $element,
  rbCiYamlPreviewDialog,
  WEBLABS,
) {
  const vm = this;

  let ciTasksCopy = {};

  vm.$postLink = _link;
  vm.artifactUploadChange = artifactUploadChange;
  vm.ciOptionChange = ciOptionChange;
  vm.previewCiYaml = previewCiYaml;

  ////////

  function _link() {
    vm.sonarqubeEnabled = WEBLABS['CI_INTEGRATION_SONARQUBE'];

    vm.catalogsByLang = _.groupBy(vm.ciEnvCatalogs, 'language');
    vm.languages = Object.keys(vm.catalogsByLang);
    // Apply some default values
    vm.model.ci_config_file_location = '/';
    vm.model.useYamlBuild = false;

    vm.form = $scope.form;

    vm.model.ciTasks = {};
    const tasksConfigVM = angular
      .element('rb-ci-tasks-config', $element)
      .isolateScope().vm;
    vm.removeAllTask = tasksConfigVM.removeAllTask;
    vm.setTaskFormDirty = tasksConfigVM.setDirty;
  }

  function artifactUploadChange() {
    if (vm.model.artifactUploadEnabled) {
      vm.model.allowArtifactUploadFail = true;
    }
  }

  function ciOptionChange() {
    if (!vm.model.ciEnabled || vm.model.useYamlBuild) {
      ciTasksCopy = vm.removeAllTask();
      vm.setTaskFormDirty(false);
    } else {
      vm.model.ciTasks = ciTasksCopy;
    }
  }

  function previewCiYaml() {
    const data = {};
    data.ci_envs = {
      language: vm.model.language,
      runtimes: [
        {
          name: vm.model.runtime,
        },
      ],
    };
    const tasks = vm.model.ciTasks;
    if (tasks['custom-command']) {
      data.ci_steps = tasks['custom-command'].ci_steps.split('\n');
    }
    if (tasks['sonarqube']) {
      data.sonarqube = tasks['sonarqube'];
    }
    rbCiYamlPreviewDialog.show(data);
  }
}
