const templateStr = require('app/components/pages/build/config/create/code-repo/client-select/rb-build-code-repo-client-select-component.html');
angular
  .module('app.components.pages')
  .component('rbBuildCodeRepoClientSelect', {
    controller: rbCodeRepoClientSelectController,
    controllerAs: 'vm',
    template: templateStr,
    bindings: {
      onSelect: '<',
      linkDisabled: '<',
    },
  });

function rbCodeRepoClientSelectController(
  rbSafeApply,
  rbModal,
  translateService,
  rbOschinaLinkDialog,
  rbAccountService,
  rbBuildDataService,
  BUILD_CONSTANTS,
) {
  const vm = this;

  const clientAttributes = {
    QUICK_BUILD: {
      displayName: 'build_quick_build',
    },
    GITHUB: {
      displayName: 'GitHub',
    },
    BITBUCKET: {
      displayName: 'Bitbucket',
    },
    OSCHINA: {
      displayName: 'OSChina',
    },
  };

  vm.$onInit = onInit;

  vm.clientClicked = clientClicked;
  vm.confirmClicked = confirmClicked;
  vm.canSelectClient = canSelectClient;
  vm.getClientTooltip = getClientTooltip;
  //////////

  async function confirmClicked(client) {
    function _OschinaConfirmClicked() {
      return rbOschinaLinkDialog.show({
        oauthCodeClient: client,
        beforeLink: () => (client.is_authed = 'linking'),
      });
    }
    if (client.isAuthed) {
      const params =
        client.name === 'QUICK_BUILD'
          ? undefined
          : {
              oauthCodeRepoClientName: client.name,
            };
      vm.onSelect(params);
    } else if (!vm.linkDisabled) {
      if (client.name === 'OSCHINA') {
        rbModal.hide({
          show: _OschinaConfirmClicked,
        });
      } else {
        vm.fetchingAuthUrl = true;

        const authUrl = await rbBuildDataService.getPrivateBuildCodeClientAuthUrl(
          {
            codeClientName: client.name,
            redirectParams: {
              nextStateName: 'build.config.create',
            },
          },
        );

        location.href = authUrl;
      }
    }
  }

  async function onInit() {
    vm.fetchingAuthUrl = false;
    vm.initialized = false;

    Object.entries(clientAttributes).forEach(kv => {
      kv[1].logo = BUILD_CONSTANTS.BUILD_CODE_CLIENT_LOGOS[kv[0]];
    });

    vm.isSubAccount = rbAccountService.isSubAccount();
    vm.clients = await rbBuildDataService
      .getPrivateBuildCodeClients()
      .catch(() => []);
    // Add QUICK_BUILD by default
    vm.clients.unshift({
      name: 'QUICK_BUILD',
      is_authed: true,
    });

    vm.clients = vm.clients.map(client => ({
      name: client.name,
      logo: clientAttributes[client.name].logo,
      displayName: clientAttributes[client.name].displayName,
      isAuthed: client.is_authed,
    }));

    vm.initialized = true;
    rbSafeApply();
  }

  function clientClicked(client) {
    canSelectClient(client) && confirmClicked(client);
  }

  function canSelectClient(client) {
    // There is only one case that the user cannot select a client repo:
    // - When the client is not authed AND its a sub account
    return client.isAuthed || !vm.isSubAccount || client.name === 'QUICK_BUILD';
  }

  function getClientTooltip(client) {
    return translateService.get(
      canSelectClient(client) ? '' : 'build_cannot_link_hint',
      {
        name: clientAttributes[client.name].displayName,
      },
    );
  }
}
