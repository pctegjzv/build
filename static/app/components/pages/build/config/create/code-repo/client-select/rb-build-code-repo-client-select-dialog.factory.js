angular
  .module('app.components.pages')
  .factory(
    'rbBuildCodeRepoClientSelectDialog',
    rbBuildCodeRepoClientSelectDialog,
  );

function rbBuildCodeRepoClientSelectDialog(rbModal, $state, translateService) {
  return {
    show: ({ onSelect, linkDisabled } = {}) => {
      onSelect =
        onSelect ||
        (params => {
          rbModal.hide();
          $state.go('build.config.create', params);
        });
      return rbModal
        .show({
          title: translateService.get('build_config_create'),
          component: 'rbBuildCodeRepoClientSelect',
          locals: {
            onSelect,
            linkDisabled,
          },
        })
        .catch(() => {});
    },
  };
}
