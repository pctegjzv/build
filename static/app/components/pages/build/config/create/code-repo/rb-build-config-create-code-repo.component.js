import {
  BUILD_CODE_REPO_CLIENT_TYPES,
  BUILD_CODE_REPO_TYPES_MAP,
  BUILD_OAUTH_CODE_REPOS,
  BUILD_CODE_REPO_TYPE_VALUE_DEFAULTS_MAP,
  BUILD_CODE_PATH_PLACEHOLDER,
} from '../../../rb-build.constant';
import { CODE_PATH_PATTERN } from 'app/components/common/config/common-pattern';
const templateStr = require('app/components/pages/build/config/create/code-repo/rb-build-config-create-code-repo.html');

angular
  .module('app.components.pages')
  .component('rbBuildConfigCreateCodeRepo', {
    bindings: {
      model: '<',
      form: '=',
      oauthCodeClientDetail: '<', // {clientName: 'GITHUB', codeRepoOrgs: [{ 'name': '...' }]}
    },
    controller: rbBuildConfigCreateCodeRepoController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbBuildConfigCreateCodeRepoController(
  $scope,
  rbSafeApply,
  translateService,
  rbBuildDataService,
  rbBuildCodeRepoSelectDialog,
  rbDockerfilePresetOverviewDialog,
  WEBLABS,
) {
  const vm = this;
  vm.loading = true;
  vm.$onInit = onInit;
  vm.codeRepoClients = BUILD_CODE_REPO_CLIENT_TYPES.map(type => ({
    key: translateService.get('build_code_repo_client_' + type),
    value: type,
  }));
  vm.codeRepoTypesMap = BUILD_CODE_REPO_TYPES_MAP;
  vm.gitPlaceholder = BUILD_CODE_PATH_PLACEHOLDER.GIT;
  vm.svnPlaceholder = BUILD_CODE_PATH_PLACEHOLDER.SVN;
  vm.gitReg = CODE_PATH_PATTERN.GIT.pattern;
  vm.svnReg = CODE_PATH_PATTERN.SVN.pattern;

  vm.codeRepoOrgChanged = codeRepoOrgChanged;
  vm.codeRepoClientChanged = codeRepoClientChanged;
  vm.selectCodeRepo = selectCodeRepo;

  vm.onAccountChange = onAccountChange;
  vm.accountValidateFn = accountValidateFn;
  vm.shoudEnableAccountFilter = shoudEnableAccountFilter;

  vm.$postLink = _link;

  vm.defaultDockerfileDisplayName = translateService.get(
    'build_code_repo_dockerfile_read_from_repo',
  );
  vm.defaultDockerfileName = 'read_from_repo';
  vm.dockerfileDefaultSelected = true;
  vm.onDockerfileChange = onDockerfileChange;
  vm.openPresetDockerfileDialog = openPresetDockerfileDialog;

  ////////

  function onInit() {
    getDockerfilePresets();
  }

  function addDefaultOption(dockerfilePreset) {
    dockerfilePreset.unshift({
      display_name: vm.defaultDockerfileDisplayName,
      name: vm.defaultDockerfileName,
    });
    return dockerfilePreset;
  }

  function formatDockerfilePreset() {
    if (!vm.dockerfilePreset) {
      return;
    }
    vm.dockerfilePreset.forEach(item => {
      if (!_.trim(item.display_name)) {
        item.display_name = item.name;
      }
      if (!_.trim(item.description)) {
        item.description = '-';
      }
    });
  }

  async function getDockerfilePresets() {
    try {
      vm.loading = true;
      vm.dockerfilePreset = await rbBuildDataService.getDockerfilePreset();
    } catch (e) {
      // ..
    }
    formatDockerfilePreset();
    vm.dockerfileOptions = addDefaultOption(vm.dockerfilePreset || []);
    vm.loading = false;
  }

  function _link() {
    vm.weblabs = WEBLABS;

    vm.model.codeRepoClient = vm.oauthCodeClientDetail
      ? vm.oauthCodeClientDetail.clientName
      : BUILD_CODE_REPO_CLIENT_TYPES[0]; // Radio button need a default value
    vm.model.codeRepoOrgName = vm.oauthCodeClientDetail
      ? vm.oauthCodeClientDetail.clientOrgs.length > 0 &&
        vm.oauthCodeClientDetail.clientOrgs[0].name
      : undefined;
    vm.form = $scope.form;

    // Apply some default values
    vm.model.buildContextPath = '/';
    vm.model.dockerfileLocation = '';

    vm.oauthCodeRepoClientDisplayName = vm.oauthCodeClientDetail
      ? BUILD_OAUTH_CODE_REPOS[vm.oauthCodeClientDetail.clientName]
      : '';

    // For OAuth repos, we assume they are git clients at the moment ...
    codeRepoClientChanged(vm.model.codeRepoClient);
  }

  function onAccountChange(filterAccounts) {
    vm.model.filterAccounts = filterAccounts;
  }

  function accountValidateFn(filterAccount) {
    if (vm.model.filterAccounts) {
      return !vm.model.filterAccounts.includes(filterAccount);
    }
    return true;
  }

  function shoudEnableAccountFilter() {
    return (
      WEBLABS['BUILD_FILTER_BY_TENANT'] &&
      ['SIMPLE_GIT', 'SIMPLE_SVN'].includes(vm.model.codeRepoClient)
    );
  }

  function codeRepoOrgChanged(codeRepoOrg) {
    if (vm.model.codeRepoOrgName !== codeRepoOrg.name) {
      vm.model.codeRepoOrgName = codeRepoOrg.name;
      vm.model.oauthCodeRepoName = undefined;
    }
  }

  function codeRepoClientChanged(codeRepoClient) {
    vm.model.codeRepoClient = codeRepoClient;
    vm.model.codeRepoTypeValue =
      BUILD_CODE_REPO_TYPE_VALUE_DEFAULTS_MAP[codeRepoClient];
  }

  async function selectCodeRepo($event) {
    try {
      const { repoName, repoId } = await rbBuildCodeRepoSelectDialog.show({
        $event,
        codeClientOrgName: vm.model.codeRepoOrgName,
        codeClientName: vm.oauthCodeClientDetail.clientName,
      });

      vm.model.oauthCodeRepoName = repoName || vm.model.oauthCodeRepoName;
      vm.model.oauthCodeRepoId = repoId || vm.model.oauthCodeRepoId;
      rbSafeApply();
    } catch (e) {
      // Do nothing
    }
  }

  function onDockerfileChange(option) {
    vm.dockerfileDefaultSelected = option.name === vm.defaultDockerfileName;
    vm.model.dockerfileInfo = vm.dockerfileDefaultSelected ? null : option;
  }

  function openPresetDockerfileDialog() {
    rbDockerfilePresetOverviewDialog.show();
  }
}
