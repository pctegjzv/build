angular
  .module('app.components.pages')
  .factory('rbBuildCodeRepoSelectDialog', rbBuildCodeRepoSelectDialog);

function rbBuildCodeRepoSelectDialog(rbModal, translateService) {
  return {
    show: ({ codeClientOrgName, codeClientName }) => {
      return rbModal
        .show({
          title: translateService.get('build_config_code_repo'),
          locals: {
            codeClientOrgName,
            codeClientName,
          },
          template: `<rb-build-code-repo-select 
                     code-client-org-name="codeClientOrgName" 
                     code-client-name="codeClientName">
                   </rb-build-code-repo-select>`,
          bindToController: true,
        })
        .catch(() => {});
    },
  };
}
