const templateStr = require('app/components/pages/build/config/create/code-repo/repo-select/rb-build-code-repo-select.html');
angular.module('app.components.pages').component('rbBuildCodeRepoSelect', {
  bindings: {
    codeClientOrgName: '<',
    codeClientName: '<',
  },
  template: templateStr,
  controller: rbBuildCodeRepoSelectController,
  controllerAs: 'vm',
});

function rbBuildCodeRepoSelectController(
  rbModal,
  rbBuildDataService,
  rbSafeApply,
) {
  const ITEMS_PER_PAGE = 8;

  const vm = this;

  const codeRepoCache = [];

  vm.getOauthCodeRepoName = getOauthCodeRepoName;

  vm.cancel = () => rbModal.cancel();

  vm.confirmClicked = () => {
    rbModal.hide({
      repoName: getOauthCodeRepoName(),
      repoId: vm.selectedCodeRepo && vm.selectedCodeRepo.repo_id,
    });
  };

  vm.pageNoChange = pageNoChange;

  vm.$onInit = init;
  ////////

  async function init() {
    vm.selectionMode = 'manual';

    // We should disallow the user manually input a repo name
    vm.hideManual = vm.codeClientName === 'OSCHINA';

    vm.oauthCodeRepoName = undefined;
    vm.selectedCodeRepo = undefined;

    vm.pagination = {
      currentPage: 1,
      maxSize: 5,
      itemsPerPage: ITEMS_PER_PAGE,
    };

    await pageNoChange();
  }

  async function pageNoChange(page = vm.pagination.currentPage) {
    vm.pagination.currentPage = page;
    vm.codeRepos = codeRepoCache[vm.pagination.currentPage];
    rbSafeApply();

    const newPage = vm.pagination.currentPage;

    const { size, values } = await rbBuildDataService.getPrivateBuildCodeRepos({
      codeClientOrgName: vm.codeClientOrgName,
      codeClientName: vm.codeClientName,
      page: vm.pagination.currentPage,
      pagelen: ITEMS_PER_PAGE,
    });

    codeRepoCache[newPage] = values;

    if (newPage === vm.pagination.currentPage) {
      vm.codeRepos = values;

      vm.pagination.totalItems = size;
      vm.pagination.numPages = Math.ceil(size / ITEMS_PER_PAGE);

      rbSafeApply();
    }
  }

  function getOauthCodeRepoName() {
    return vm.selectionMode === 'manual' || !vm.selectedCodeRepo
      ? vm.oauthCodeRepoName
      : vm.selectedCodeRepo.code_repo_path;
  }
}
