import { BUILD_AUTO_TAG_TYPES } from '../../../rb-build.constant';
import { resource_types as RESOURCE_TYPES } from '../../../../rbac_role/rb-rbac-helper.constant';

const templateStr = require('app/components/pages/build/config/create/image-repo/rb-build-config-create-image-repo.html');
angular
  .module('app.components.pages')
  .component('rbBuildConfigCreateImageRepo', {
    bindings: {
      model: '<',
      form: '=',
      imageRepoRegistries: '<',
      codeRepoClient: '<',
    },
    controller: rbBuildConfigCreateImageRepoController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbBuildConfigCreateImageRepoController(
  $scope,
  rbSafeApply,
  rbRegistryDataService,
  rbRepositoryDataService,
  translateService,
  rbRoleUtilities,
  BUILD_CONSTANTS,
  WEBLABS,
) {
  const vm = this;
  const repoTypes = [
    {
      key: translateService.get('build_create_new_image_repo'),
      value: 'create',
    },
    {
      key: translateService.get('build_use_existing_image_repo'),
      value: 'existing',
    },
  ];

  let gitTagsMap, svnTagsMap;

  vm.$onInit = _init;
  vm.$postLink = _link;
  vm.$onChanges = _changes;
  vm.validateCustomImageTag = validateCustomImageTag;
  vm.onCustomizeTagsChange = onCustomizeTagsChange;

  function validateCustomImageTag(tag) {
    return (
      (tag.value || /^[a-zA-Z0-9]+$/.test(tag.text)) &&
      vm.model.customizeTagRule.length < 5
    );
  }

  function _init() {
    vm.weblabs = WEBLABS;
    vm.projects = [];
    vm.repoTypeDisabledValues = [];
    vm.imageRepoTypes = repoTypes;
    vm.model.buildImageEnabled = true;
    vm.repoNamePattern = (() => {
      const regexp = /^[a-z0-9][a-z0-9\-_]*[a-z0-9]$/;
      return {
        test: value => {
          if (!vm.model.buildImageEnabled) {
            return true;
          }
          return regexp.test(value);
        },
      };
    })();
  }

  function _changes(binding) {
    if (
      binding.codeRepoClient &&
      typeof binding.codeRepoClient.previousValue === 'string'
    ) {
      vm.initTagRule = [];
      vm.model.customizeTagRule = [];
      vm.tagRuleSuggestions =
        vm.codeRepoClient === 'SIMPLE_SVN' ? svnTagsMap : gitTagsMap;
      vm.parsedCustomizeTags = '';
    }
  }

  vm.registrySelected = async imageRepoRegistry => {
    vm.model.imageRepoRegistry = imageRepoRegistry;
    vm.repositories = [];

    if (imageRepoRegistry.is_public) {
      vm.projects = [
        {
          project_name: translateService.get('default_project_name'),
          is_default: true,
        },
      ];
      vm.model.imageRepoProject = null;
    } else {
      vm.projectsLoading = true;
      const projects = await rbRegistryDataService.getProjects({
        registry_name: imageRepoRegistry.name,
      });
      vm.projects = projects.concat([
        {
          project_name: translateService.get('default_project_name'),
          is_default: true,
        },
      ]);
      vm.projectsLoading = false;
    }
    const repoCreateEnabled = await rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.REPOSITORY,
      {
        registry_name: imageRepoRegistry.name,
      },
    );
    if (repoCreateEnabled) {
      vm.repoTypeDisabledValues = [];
    } else {
      vm.repoTypeDisabledValues = ['create'];
    }
  };

  vm.onProjectChange = async project => {
    if (!project.is_default) {
      vm.model.imageRepoProject = {
        name: project.project_name,
      };
    } else {
      vm.model.imageRepoProject = null;
    }
    // get repo in this project
    vm.repositories = await rbRepositoryDataService.getRepositories({
      registry_name: vm.model.imageRepoRegistry.name,
      project_name: project.is_default ? '' : project.project_name,
      action: 'push',
    });
    rbSafeApply();
  };

  /////////
  function _link() {
    // Apply some default values
    vm.model.customizeTag = 'latest';
    vm.model.autoTagType = 'TIME';

    const buildAutoTagTypes = JSON.parse(JSON.stringify(BUILD_AUTO_TAG_TYPES));

    vm.autoTagOptions = buildAutoTagTypes.map(autoTagType => ({
      value: autoTagType,
      key: translateService.get('build_image_repo_auto_tag_' + autoTagType),
    }));

    const gitTags = BUILD_CONSTANTS['BUILD_CUSTOM_IMAGE_TAGS']['GIT'];
    const svnTags = BUILD_CONSTANTS['BUILD_CUSTOM_IMAGE_TAGS']['SVN'];
    gitTagsMap = gitTags.map(value => ({
      value: `{${value}}`,
      text: translateService.get('build_image_repo_custom_tag_git_' + value),
    }));
    svnTagsMap = svnTags.map(value => ({
      value: `{${value}}`,
      text: translateService.get('build_image_repo_custom_tag_svn_' + value),
    }));
    vm.tagRuleSuggestions =
      vm.codeRepoClient === 'SIMPLE_SVN' ? svnTagsMap : gitTagsMap;
    vm.model.customizeTagRule = [];

    vm.form = $scope.form;
  }

  function onCustomizeTagsChange(tags) {
    vm.model.customizeTagRule = tags;
    vm.parsedCustomizeTags = tags
      .map(item => item.value || item.text)
      .join('-');
  }
}
