const { BUILD_CODE_REPO_TYPES_MAP } = require('../../rb-build.constant');
const path = require('path');
const templateStr = require('app/components/pages/build/config/create/rb-build-config-create.html');
angular.module('app.components.pages').component('rbBuildConfigCreate', {
  bindings: {
    oauthCodeRepoClientName: '<',
    onCancel: '<',
    onCreate: '<',
  },
  controller: rbBuildConfigCreateController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbBuildConfigCreateController(
  $scope,
  $log,
  $state,
  $element,
  rbBuildDataService,
  rbRegistryDataService,
  notificationService,
  WEBLABS,
) {
  const vm = this;

  let ciTaskConfigVM;

  vm.prevStep = prevStep;
  vm.nextStep = nextStep;
  vm.create = createBuildConfig;
  vm.cancel = cancel;
  vm.shouldShowLoadingMask = shouldShowLoadingMask;
  vm.getConfigName = getConfigName;
  vm.$onInit = _init;

  //////////

  async function _init() {
    vm.configStep = 0;
    vm.steps = ['codeRepo', 'ci', 'imageRepo', 'basicInfo'];
    vm.maxStep = vm.steps.length - 1;
    vm.steps.forEach(step => (vm[step] = {})); // Init model objects
    vm.forms = {}; // ng FormControllers. This will be initialized in templates.
    vm.endpoints = [];
    // Some build config creation steps depend on the following data:
    notificationService
      .notificationList()
      .catch(() => []) // Make it fail safe
      .then(notifications => {
        vm.basicInfoFormReady = true;
        vm.notifications = notifications;
      });

    const registryPromise = rbRegistryDataService
      .getRegistries({
        params: {
          filter: 'repository:push',
        },
      })
      .then(registries => {
        vm.privateRegistries = registries;
      });
    const initPromises = [registryPromise];
    const endpointsPromise = rbBuildDataService
      .getBuildEndpoints()
      .then(data => {
        const list = data.result;
        list.forEach(item => {
          if (!item.endpoint_name) {
            item.endpoint_name = item.endpoint;
          }
        });
        vm.buildEndpoints = list;
      })
      .catch(() => {
        vm.buildEndpoints = [];
      })
      .then(() => {});
    initPromises.push(endpointsPromise);

    Promise.all(initPromises).then(() => {
      vm.imageRepoFormReady = true;
    });

    rbBuildDataService
      .getCiEnvCatalogs()
      .catch(() => [])
      .then(catalogs => {
        vm.ciFormReady = true;
        vm.ciEnvCatalogs = catalogs;
      });

    $scope.$watchGroup(
      ['vm.buildEndpoints', 'vm.imageRepo.imageRepoRegistry.is_public'],
      ([buildEndpoints, isPublic]) => {
        if (!buildEndpoints) {
          return;
        }

        vm.endpoints = isPublic
          ? buildEndpoints
          : buildEndpoints.filter(endpoint => !endpoint.is_public);
      },
    );

    if (vm.oauthCodeRepoClientName) {
      try {
        const clientOrgs = await rbBuildDataService.getPrivateBuildCodeClientOrgs(
          {
            codeClientName: vm.oauthCodeRepoClientName,
          },
        );
        vm.oauthCodeClientDetail = {
          clientName: vm.oauthCodeRepoClientName,
          clientOrgs: clientOrgs,
        };
      } catch ({ data }) {
        if (data.errors.find(error => error.code === 'token_expired')) {
          vm.errorMessage = 'build_code_repo_token_expired';
        } else {
          vm.onCancel ? vm.onCancel() : $state.go('^');
        }
      }
    }
    vm.initialized = true;
  }

  function shouldShowLoadingMask() {
    return (
      !vm.initialized ||
      vm.submitting ||
      (vm.configStep === 1 && !vm.ciFormReady) ||
      (vm.configStep === 2 && !vm.imageRepoFormReady) ||
      (vm.configStep === 3 && !vm.basicInfoFormReady)
    );
  }

  function prevStep() {
    vm.configStep = Math.max(0, vm.configStep - 1);
  }

  function nextStep() {
    let valid = !vm.errorMessage;
    const currentForm = vm.forms[vm.steps[vm.configStep]];
    if (currentForm) {
      currentForm.$setSubmitted();
      valid = currentForm.$valid;
    }

    if (vm.configStep === 1 && !vm.ci.useYamlBuild) {
      if (!ciTaskConfigVM) {
        ciTaskConfigVM = angular
          .element('rb-ci-tasks-config', $element)
          .isolateScope().vm;
      }
      ciTaskConfigVM.setDirty();
      valid = ciTaskConfigVM.checkFormValid();
    }

    switch (vm.configStep) {
      case 1: // from CI to image config
        vm.imageRepo['newImageRepoName'] =
          vm.imageRepo['newImageRepoName'] === undefined
            ? _getCodeRepoName()
            : vm.imageRepo['newImageRepoName'];
        break;
      case 2:
        // from image config to basic config
        if (vm.imageRepo['buildImageEnabled']) {
          if (vm.imageRepo['useExistingImageRepo']) {
            vm.basicInfo['name'] =
              vm.basicInfo['name'] === undefined
                ? vm.imageRepo['imageRepoName']
                : vm.basicInfo['name'];
          } else {
            vm.basicInfo['name'] =
              vm.basicInfo['name'] === undefined
                ? vm.imageRepo['newImageRepoName']
                : vm.basicInfo['name'];
          }
        } else {
          vm.basicInfo['name'] =
            vm.basicInfo['name'] === undefined
              ? _getCodeRepoName()
              : vm.basicInfo['name'];
        }
        break;
    }

    if (valid) {
      vm.configStep = Math.min(vm.maxStep, vm.configStep + 1);
    }
  }

  function _getCodeRepoName() {
    // git@bitbucket.org:mathildetech/aggron.git
    // -> aggron
    // oauth:
    // -> {codeRepoName}
    if (vm.oauthCodeClientDetail) {
      return vm.codeRepo['oauthCodeRepoName'];
    } else {
      return _urlInterception(vm.codeRepo['codeRepoPath']);
    }
  }

  function _urlInterception(urlCompile) {
    const index = urlCompile.lastIndexOf('/');
    let urlComponent = urlCompile.substring(index + 1, urlCompile.length);
    if (urlComponent.indexOf('.git') > 0) {
      urlComponent = urlComponent.replace('.git', '');
    }
    // return urlComponent;
    return urlComponent;
  }

  async function createBuildConfig() {
    const forms = Object.values(vm.forms);
    forms.forEach(form => form.$setSubmitted()); // Toggle submitted state

    if (!forms.every(form => form.$valid)) {
      // Only creating when all forms are valid
      return;
    }

    const newBuildConfig = {
      // Basic Info
      name: vm.basicInfo.name,
      image_cache_enabled: !!vm.basicInfo.imageCacheEnabled,
      auto_build_enabled: !!vm.basicInfo.autoBuildEnabled,

      // Code Repo
      code_repo: {
        code_repo_client: vm.oauthCodeClientDetail
          ? vm.oauthCodeClientDetail.clientName
          : vm.codeRepo.codeRepoClient,
        code_repo_path: vm.oauthCodeClientDetail
          ? vm.codeRepo.codeRepoOrgName + '/' + vm.codeRepo.oauthCodeRepoName
          : encodeURI(vm.codeRepo.codeRepoPath),
        code_repo_type: BUILD_CODE_REPO_TYPES_MAP[vm.codeRepo.codeRepoClient],
        code_repo_type_value: vm.codeRepo.codeRepoTypeValue,
        build_context_path: vm.codeRepo.buildContextPath,
        dockerfile_location: path.resolve(
          vm.codeRepo.buildContextPath,
          './' + vm.codeRepo.dockerfileLocation,
        ),
        code_repo_username: vm.codeRepo.codeRepoUsername,
        code_repo_password: vm.codeRepo.codeRepoPassword,
      },

      // Dockerfile Info
      dockerfile_info: {
        uuid:
          (vm.codeRepo.dockerfileInfo && vm.codeRepo.dockerfileInfo.uuid) || '',
        name:
          (vm.codeRepo.dockerfileInfo && vm.codeRepo.dockerfileInfo.name) || '',
      },

      build_image_enabled: !!vm.imageRepo.buildImageEnabled,
    };

    if (WEBLABS['BUILD_FILTER_BY_TENANT']) {
      newBuildConfig['build_filters'] = {
        committers: vm.codeRepo.filterAccounts,
      };
    }

    newBuildConfig['endpoint_id'] = vm.basicInfo.endpoint_id;

    // Image Repo
    if (!newBuildConfig.build_image_enabled) {
      Object.assign(newBuildConfig, {
        auto_tag_type: null,
        customize_tag: null,
        image_repo: {
          name: null,
          registry: {
            name: null,
          },
          project: null,
        },
      });
    } else {
      Object.assign(newBuildConfig, {
        auto_tag_type: vm.imageRepo.autoTagType,
        customize_tag: vm.imageRepo.customizeTag,
        image_repo: {
          name: vm.imageRepo.useExistingImageRepo
            ? vm.imageRepo.imageRepoName
            : vm.imageRepo.newImageRepoName,
          registry: {
            name: vm.imageRepo.imageRepoRegistry.name,
          },
          project: vm.imageRepo.imageRepoProject,
        },
      });

      if (vm.imageRepo.autoTagType === 'CUSTOM_RULE') {
        newBuildConfig[
          'customize_tag_rule'
        ] = vm.imageRepo.customizeTagRule
          .map(item => item.value || item.text)
          .join('-');
      }
    }

    if (WEBLABS['quota_enabled'] && vm.basicInfo.space_name) {
      newBuildConfig['space_name'] = vm.basicInfo.space_name;
    }

    if (vm.ci.ciEnabled) {
      newBuildConfig.ci_enabled = true;
      if (vm.ci.useYamlBuild) {
        newBuildConfig.ci_config_file_location = vm.ci.ci_config_file_location;
      } else {
        newBuildConfig.ci_envs = {
          language: vm.ci.language,
          runtimes: [
            {
              name: vm.ci.runtime,
            },
          ],
        };
        const tasks = vm.ci.ciTasks;
        if (tasks['custom-command']) {
          newBuildConfig.ci_steps = tasks['custom-command'].ci_steps.split(
            '\n',
          );
        }
        if (tasks['sonarqube']) {
          newBuildConfig.sonarqube = tasks['sonarqube'];
        }
      }
      newBuildConfig.artifact_upload_enabled = vm.ci.artifactUploadEnabled;
      if (newBuildConfig.artifact_upload_enabled) {
        newBuildConfig.allow_artifact_upload_fail =
          vm.ci.allowArtifactUploadFail;
      } else {
        newBuildConfig.allow_artifact_upload_fail = false;
      }
    }

    if (vm.basicInfo.notificationEnabled) {
      newBuildConfig['notification'] = {
        name: vm.basicInfo.notification,
        uuid: vm.basicInfo.notification_uuid,
      };
    }

    if (vm.basicInfo.timingBuildEnabled) {
      newBuildConfig['schedule_rule'] = vm.basicInfo.timingRule;
    }

    if (vm.basicInfo.containerSizeCustomized) {
      newBuildConfig['cpu'] = vm.basicInfo.cpu;
      newBuildConfig['memory'] = vm.basicInfo.memory;
    }

    // When selecting OSChina repos, we need to give repo_id to them for better performance.
    if (vm.oauthCodeRepoClientName === 'OSCHINA') {
      newBuildConfig.code_repo.repo_id = vm.codeRepo.oauthCodeRepoId;
    }

    vm.submitting = true;
    try {
      const { result } = await rbBuildDataService.createPrivateBuildConfig(
        newBuildConfig,
      );
      if (vm.onCreate) {
        await vm.onCreate(result.name);
      } else {
        $state.go('^.detail', {
          name: vm.getConfigName(result),
        });
      }
    } catch (e) {
      $log.error('Failed to create build config: ' + e.message);
    }
    vm.submitting = false;
  }

  function cancel() {
    if (vm.onCancel) {
      vm.onCancel();
    } else {
      $state.go('^.list');
    }
  }
  function getConfigName(config) {
    return rbBuildDataService.getConfigName(config);
  }
}
