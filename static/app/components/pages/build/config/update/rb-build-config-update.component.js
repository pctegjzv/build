import path from 'path';
import _ from 'lodash';
import {
  BUILD_AUTO_TAG_TYPES,
  BUILD_CODE_REPO_TYPES_MAP,
  BUILD_CODE_PATH_PLACEHOLDER,
} from '../../rb-build.constant';
import { CODE_PATH_PATTERN } from 'app/components/common/config/common-pattern';

const templateStr = require('app/components/pages/build/config/update/rb-build-config-update.html');
angular.module('app.components.pages').component('rbBuildConfigUpdate', {
  controller: rbBuildConfigUpdateController,
  controllerAs: 'vm',
  template: templateStr,
  bindings: {
    buildConfigName: '<',
    buildConfig: '=',
    onCancel: '<',
    onUpdate: '<',
  },
});

function rbBuildConfigUpdateController(
  rbSafeApply,
  $scope,
  $log,
  $state,
  $element,
  rbToast,
  rbRouterStateHelper,
  rbGlobalSetting,
  translateService,
  rbBuildDataService,
  rbNotificationDataStore,
  rbAccountService,
  BUILD_CONSTANTS,
  WEBLABS,
  rbCiYamlPreviewDialog,
  rbDockerfilePresetOverviewDialog,
) {
  const vm = this;

  let ciTasksCopy = {};

  vm.weblabs = WEBLABS;

  vm.gitReg = CODE_PATH_PATTERN.GIT.pattern;
  vm.svnReg = CODE_PATH_PATTERN.SVN.pattern;
  vm.gitPlaceholder = BUILD_CODE_PATH_PLACEHOLDER.GIT;
  vm.svnPlaceholder = BUILD_CODE_PATH_PLACEHOLDER.SVN;

  vm.cancel = cancel;
  vm.update = update;

  vm.ciRuntimeChanged = ciRuntimeChanged;
  vm.ciLanguageChanged = ciLanguageChanged;
  vm.endpointSelected = endpointSelected;
  vm.onNotificationChange = onNotificationChange;

  vm.repositoryDropdownGroupInitObject = null;

  vm.onRegistryChanged = onRegistryChanged;
  vm.onProjectChanged = onProjectChanged;
  vm.onRepositoryChanged = onRepositoryChanged;
  vm.onInstanceSizeChange = onInstanceSizeChange;

  vm.validateCustomImageTag = validateCustomImageTag;
  vm.onCustomizeTagsChange = onCustomizeTagsChange;

  vm.onCodeBranchChanged = onCodeBranchChanged;

  vm.onBuildImageEnabledChange = onBuildImageEnabledChange;

  vm.getCiTaskSource = getCiTaskSource;
  vm.ciOptionsChange = ciOptionsChange;

  vm.previewCiYaml = previewCiYaml;

  vm.onAccountChange = onAccountChange;
  vm.accountValidateFn = accountValidateFn;

  vm.$onInit = _init;
  vm.$postLink = _postLink;

  vm.defaultDockerfileDisplayName = translateService.get(
    'build_code_repo_dockerfile_read_from_repo',
  );
  vm.defaultDockerfileName = 'read_from_repo';
  vm.dockerfileDefaultSelected = true;
  vm.onDockerfileChange = onDockerfileChange;
  vm.openPresetDockerfileDialog = openPresetDockerfileDialog;

  //////////

  function validateCustomImageTag(tag) {
    return (
      (tag.value || /^[a-zA-Z0-9]+$/.test(tag.text)) &&
      vm.customizeTags.length < 5
    );
  }

  function onRegistryChanged(value) {
    _.set(vm, 'dirtyBuildConfig.image_repo.registry', { name: value.name });
  }

  function onProjectChanged(value) {
    _.set(vm, 'dirtyBuildConfig.image_repo.project', {
      project_name: value.project_name,
      project_id: value.project_id,

      // Note:
      // backend returns project's name as 'project_name', but accepts 'name' when updating / creating
      name: value.project_name,
    });
  }

  function onRepositoryChanged(value) {
    if (value) {
      vm.dirtyBuildConfig.image_repo.name = value.name;
    }
  }

  async function _init() {
    try {
      await rbBuildDataService
        .getPrivateBuildConfigDetail(vm.buildConfigName)
        .then(res => {
          vm.buildConfig = res;
        });
    } catch (e) {
      rbToast.error(translateService.get('build_not_exist'));
      $state.go('^.list');
      return;
    }

    $('rb-build-config-update').css('overflow', 'visible');

    vm.loading = true;
    vm.initialized = false;
    vm.dockerfileLoaded = false;
    vm.newImageRepoName = '';
    vm.registryParams = {
      filter: 'repository:push',
    };
    vm.repositoryActions = ['push'];
    vm.namespace = rbAccountService.getCurrentNamespace();
    vm.customizeTags = [];

    const buildAutoTagTypes = JSON.parse(JSON.stringify(BUILD_AUTO_TAG_TYPES));

    vm.autoTagOptions = buildAutoTagTypes.map(autoTagType => ({
      value: autoTagType,
      key: translateService.get('build_image_repo_auto_tag_' + autoTagType),
    }));

    vm.codeRepoTypesMap = BUILD_CODE_REPO_TYPES_MAP;

    vm.notifications$ = rbNotificationDataStore.data$;

    const ciConfigPromise = rbBuildDataService
      .getCiEnvCatalogs()
      .catch(() => [])
      .then(catalogs => {
        vm.ciCatalogsByLang = _.groupBy(catalogs, 'language');
        vm.ciLanguages = Object.keys(vm.ciCatalogsByLang);
      });

    if (vm.buildConfig.image_repo.project) {
      vm.init_project_name = vm.buildConfig.image_repo.project.project_name;
    }

    if (vm.buildConfig.schedule_rule) {
      vm.timingBuildEnabled = true;
    }

    const defaultContainerSize =
      BUILD_CONSTANTS.BUILD_CONFIG_DEFAULT_CONTAINER_SIZE;

    if (
      vm.buildConfig.cpu !== defaultContainerSize['CPU'] ||
      vm.buildConfig.memory !== defaultContainerSize['MEMORY']
    ) {
      vm.containerSizeCustomized = true;

      vm.instanceSizeInit = {
        cpu: vm.buildConfig.cpu,
        memory: vm.buildConfig.memory,
      };
    } else {
      vm.instanceSizeInit = {
        cpu: defaultContainerSize['CPU'],
        memory: defaultContainerSize['MEMORY'],
      };
    }

    vm.dirtyBuildConfig = _.cloneDeep(vm.buildConfig);

    getDockerfilePresets();

    const dockerfileInfo = vm.dirtyBuildConfig.dockerfile_info;
    if (dockerfileInfo && dockerfileInfo.uuid) {
      vm.dockerfileSelected = dockerfileInfo.name;
    } else {
      vm.dockerfileSelected = 'read_from_repo';
    }

    vm.ciTasks = {};
    if (
      vm.dirtyBuildConfig.ci_steps &&
      vm.dirtyBuildConfig.ci_steps.length > 0
    ) {
      vm.ciTasks['custom-command'] = {
        ci_steps: vm.dirtyBuildConfig.ci_steps.join('\n'),
      };
    }
    if (vm.dirtyBuildConfig.sonarqube) {
      vm.ciTasks['sonarqube'] = vm.dirtyBuildConfig.sonarqube;
    }

    vm.notificationEnabled = !!vm.buildConfig.notification;

    vm.useYamlBuild = !!vm.buildConfig.ci_config_file_location;

    vm.relativeDockerFileLocation = path.relative(
      vm.dirtyBuildConfig.code_repo.build_context_path,
      vm.dirtyBuildConfig.code_repo.dockerfile_location,
    );

    vm.onCodeBranchChanged(vm.dirtyBuildConfig.code_repo.code_repo_type_value);

    const promises = [ciConfigPromise];

    const endpointsPromise = rbBuildDataService
      .getBuildEndpoints()
      .then(data => {
        const list = data.result;
        list.forEach(item => {
          if (!item.endpoint_name) {
            item.endpoint_name = item.endpoint;
          }
        });
        vm.buildEndpoints = list;
      })
      .catch(() => {
        vm.buildEndpoints = [];
      });
    promises.push(endpointsPromise);

    await Promise.all(promises);

    // apply default value
    if (!vm.dirtyBuildConfig.ci_config_file_location) {
      vm.dirtyBuildConfig.ci_config_file_location = '/';
    }

    vm.repositoryDropdownGroupInitObject = {
      get registry() {
        return _.get(vm, 'dirtyBuildConfig.image_repo.registry.name');
      },
      get project() {
        return _.get(vm, 'dirtyBuildConfig.image_repo.project.project_name');
      },
      get repository() {
        return _.get(vm, 'dirtyBuildConfig.image_repo.name');
      },
    };

    // Initialize artifacts options:
    vm.editorOptions = rbGlobalSetting.getEditorOptions('shell');

    const repoType = vm.dirtyBuildConfig.code_repo.code_repo_client;
    const repoTags =
      repoType === 'SIMPLE_SVN'
        ? BUILD_CONSTANTS['BUILD_CUSTOM_IMAGE_TAGS']['SVN']
        : BUILD_CONSTANTS['BUILD_CUSTOM_IMAGE_TAGS']['GIT'];
    const translatePrefix =
      'build_image_repo_custom_tag' +
      (repoType === 'SIMPLE_SVN' ? '_svn_' : '_git_');
    vm.repoTagsSuggestions = repoTags.map(value => ({
      value: `{${value}}`,
      text: translateService.get(translatePrefix + value),
    }));
    if (vm.dirtyBuildConfig.customize_tag_rule) {
      vm.initRepoTags = vm.dirtyBuildConfig.customize_tag_rule
        .split('-')
        .map(value => {
          const tag = vm.repoTagsSuggestions.find(item => item.value === value);
          return tag || { text: value };
        });
      vm.parsedCustomizeTags = vm.dirtyBuildConfig.customize_tag_rule;
      vm.customizeTags = vm.initRepoTags;
    }

    vm.codeRepoPathEditable =
      repoType === 'SIMPLE_SVN' || repoType === 'SIMPLE_GIT';

    vm.accountFilterEnabled =
      WEBLABS['BUILD_FILTER_BY_TENANT'] && vm.codeRepoPathEditable;
    vm.initFilteredAccounts =
      (vm.dirtyBuildConfig.build_filters &&
        vm.dirtyBuildConfig.build_filters.committers) ||
      [];

    vm.initialized = true;
    vm.loading = false;

    rbSafeApply();
  }

  function formatDockerfilePreset() {
    if (!vm.dockerfilePreset) {
      return;
    }
    vm.dockerfilePreset.forEach(item => {
      if (!_.trim(item.display_name)) {
        item.display_name = item.name;
      }
      if (!_.trim(item.description)) {
        item.description = '-';
      }
    });
  }

  async function getDockerfilePresets() {
    try {
      vm.dockerfilePreset = await rbBuildDataService.getDockerfilePreset();
    } catch (e) {
      // ..
    }
    formatDockerfilePreset();
    vm.dockerfileOptions = addDefaultOption(vm.dockerfilePreset || []);
    vm.dockerfileLoaded = true;
  }

  function addDefaultOption(dockerfilePreset) {
    dockerfilePreset.unshift({
      display_name: vm.defaultDockerfileDisplayName,
      name: vm.defaultDockerfileName,
    });
    return dockerfilePreset;
  }

  function onDockerfileChange(option) {
    vm.dockerfileDefaultSelected = option.name === vm.defaultDockerfileName;
    _.set(vm, 'dirtyBuildConfig.dockerfile_info', {
      uuid: vm.dockerfileDefaultSelected ? '' : option.uuid,
      name: vm.dockerfileDefaultSelected ? '' : option.name,
    });
  }

  function openPresetDockerfileDialog() {
    rbDockerfilePresetOverviewDialog.show();
  }

  function _postLink() {
    const tasksConfigVM = angular
      .element('rb-ci-tasks-config', $element)
      .isolateScope().vm;
    vm.removeAllCiTasks = tasksConfigVM.removeAllTask;
    vm.checkCiTasksValid = () => {
      return (
        !vm.dirtyBuildConfig.ci_enabled ||
        vm.useYamlBuild ||
        tasksConfigVM.checkFormValid()
      );
    };
  }

  function endpointSelected(option) {
    vm.dirtyBuildConfig.endpoint_id = option.endpoint_id;
    vm.dirtyBuildConfig.endpoint_name = option.endpoint_name;
  }

  function cancel() {
    vm.dirtyBuildConfig = _.cloneDeep(vm.buildConfig);

    if (vm.onCancel) {
      vm.onCancel();
    } else {
      rbRouterStateHelper.back();
    }
  }

  function onAccountChange(filterAccounts) {
    vm.filterAccounts = filterAccounts;
  }

  function accountValidateFn(filterAccount) {
    if (vm.filterAccounts) {
      return !vm.filterAccounts.includes(filterAccount);
    }
    return true;
  }

  async function update() {
    const form = $scope.buildConfigUpdateForm;

    if (!form) {
      return;
    }

    form.$setSubmitted();

    if (form.$invalid) {
      return;
    }

    vm.loading = true;
    try {
      if (!vm.dirtyBuildConfig.artifact_upload_enabled) {
        vm.dirtyBuildConfig.allow_artifact_upload_fail = false;
      }

      const updatedBuildConfig = _.cloneDeep(vm.dirtyBuildConfig);

      if (!vm.notificationEnabled) {
        delete updatedBuildConfig.notification;
      }

      if (!vm.timingBuildEnabled) {
        delete updatedBuildConfig.schedule_rule;
      }

      if (!vm.containerSizeCustomized) {
        const defaultContainerSize =
          BUILD_CONSTANTS.BUILD_CONFIG_DEFAULT_CONTAINER_SIZE;
        updatedBuildConfig.cpu = defaultContainerSize['CPU'];
        updatedBuildConfig.memory = defaultContainerSize['MEMORY'];
      }

      if (updatedBuildConfig.auto_tag_type === 'CUSTOM_RULE') {
        updatedBuildConfig.customize_tag_rule = vm.customizeTags
          .map(item => item.value || item.text)
          .join('-');
      } else {
        updatedBuildConfig.customize_tag_rule = '';
      }

      if (!updatedBuildConfig.ci_enabled || vm.useYamlBuild) {
        updatedBuildConfig.ci_envs = {};
      }

      if (!updatedBuildConfig.ci_enabled || !vm.useYamlBuild) {
        updatedBuildConfig.ci_config_file_location = '';
      }

      // Prepare CI steps value:
      if (vm.dirtyBuildConfig.ci_enabled && !vm.useYamlBuild) {
        if (vm.ciTasks['custom-command']) {
          updatedBuildConfig.ci_steps = vm.ciTasks[
            'custom-command'
          ].ci_steps.split('\n');
        } else {
          updatedBuildConfig.ci_steps = [];
        }
        if (vm.ciTasks['sonarqube']) {
          updatedBuildConfig.sonarqube = vm.ciTasks['sonarqube'];
        } else {
          delete updatedBuildConfig.sonarqube;
        }
      } else {
        // This has to be an empty array instead of undefined
        updatedBuildConfig.ci_steps = [];
        delete updatedBuildConfig.sonarqube;
      }

      if (!updatedBuildConfig.build_image_enabled) {
        Object.assign(updatedBuildConfig, {
          auto_tag_type: null,
          customize_tag: null,
          image_repo: {
            name: null,
            registry: {
              name: null,
            },
            project: null,
          },
        });
      }

      // Extract to a standalone service?
      updatedBuildConfig.code_repo.dockerfile_location = path.resolve(
        updatedBuildConfig.code_repo.build_context_path,
        './' + vm.relativeDockerFileLocation,
      );

      if (vm.filterAccounts) {
        updatedBuildConfig.build_filters = {
          committers: vm.filterAccounts,
        };
      }
      await rbBuildDataService.updatePrivateBuildConfig(updatedBuildConfig);

      if (vm.onUpdate) {
        vm.onUpdate(updatedBuildConfig);
      } else {
        $state.go('^.detail', {
          name: vm.buildConfigName,
          buildConfig: updatedBuildConfig,
        });
      }
    } catch (e) {
      $log.error('Failed to update build config');
    }

    vm.loading = false;
    rbSafeApply();
  }
  function onNotificationChange(option) {
    vm.dirtyBuildConfig.notification = {
      name: option.name,
      uuid: option.uuid,
    };
  }

  function ciLanguageChanged(language) {
    if (!vm.initialized) {
      return;
    }
    vm.dirtyBuildConfig.ci_envs = vm.dirtyBuildConfig.ci_envs || {};
    vm.dirtyBuildConfig.ci_envs.language = language;
  }

  function ciRuntimeChanged(option) {
    if (!vm.initialized) {
      return;
    }
    const runtime = option ? option.runtime : '';
    vm.dirtyBuildConfig.ci_envs.runtimes = vm.dirtyBuildConfig.ci_envs
      .runtimes || [{}];
    vm.dirtyBuildConfig.ci_envs.runtimes[0].name = runtime;
  }

  function onInstanceSizeChange({ cpu, memory }) {
    vm.dirtyBuildConfig.memory = memory;
    vm.dirtyBuildConfig.cpu = cpu;
  }

  function onCustomizeTagsChange(tags) {
    vm.customizeTags = tags;
    vm.parsedCustomizeTags = tags
      .map(item => item.value || item.text)
      .join('-');
  }

  function onCodeBranchChanged(codeBranch) {
    if (codeBranch) {
      vm.cannotEnableTimingBuild = codeBranch.includes('*');
      if (vm.cannotEnableTimingBuild) {
        vm.timingBuildEnabled = false;
      }
    }
  }

  function onBuildImageEnabledChange() {
    if (
      vm.dirtyBuildConfig.build_image_enabled &&
      !vm.dirtyBuildConfig.customize_tag
    ) {
      vm.dirtyBuildConfig.customize_tag = 'latest';
    }
  }

  function getCiTaskSource() {
    return {
      model: {
        language: vm.dirtyBuildConfig && vm.dirtyBuildConfig.ci_envs.language,
        runtime:
          vm.dirtyBuildConfig && vm.dirtyBuildConfig.ci_envs.runtimes[0].name,
      },
      langCatalog: vm.ciCatalogsByLang,
    };
  }

  function ciOptionsChange() {
    if (!vm.dirtyBuildConfig.ci_enabled || vm.useYamlBuild) {
      ciTasksCopy = vm.removeAllCiTasks();
    } else {
      vm.ciTasks = ciTasksCopy;
    }
  }

  function previewCiYaml() {
    const data = {};
    data.ci_envs = vm.dirtyBuildConfig.ci_envs;
    const tasks = vm.ciTasks;
    if (tasks['custom-command']) {
      data.ci_steps = tasks['custom-command'].ci_steps.split('\n');
    }
    if (tasks['sonarqube']) {
      data.sonarqube = tasks['sonarqube'];
    }
    rbCiYamlPreviewDialog.show(data);
  }
}
