angular
  .module('app.components.pages')
  .factory(
    'rbDockerfilePresetOverviewDialog',
    rbDockerfilePresetOverviewDialog,
  );

function rbDockerfilePresetOverviewDialog(rbModal, translateService) {
  return {
    show: options => {
      return rbModal
        .show({
          title: translateService.get('build_code_repo_dockerfile_preset'),
          component: 'rbDockerfilePresetOverview',
          locals: options,
        })
        .catch(() => {});
    },
  };
}
