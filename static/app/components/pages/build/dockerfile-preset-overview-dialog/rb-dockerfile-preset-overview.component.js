const templateStr = require('app/components/pages/build/dockerfile-preset-overview-dialog/rb-dockerfile-preset-overview.html');
angular.module('app.components.pages').component('rbDockerfilePresetOverview', {
  controller: dockerfilePresetOverviewController,
  controllerAs: 'vm',
  template: templateStr,
});

function dockerfilePresetOverviewController(rbBuildDataService) {
  const vm = this;
  vm.$onInit = onInit;
  vm.loading = true;
  vm.dockerfilePreset = [];

  function onInit() {
    getDockerfilePresets();
  }

  function formatDockerfilePreset() {
    if (!vm.dockerfilePreset) {
      return;
    }
    vm.dockerfilePreset.forEach(item => {
      if (!_.trim(item.display_name)) {
        item.display_name = item.name;
      }
      if (!_.trim(item.description)) {
        item.description = '-';
      }
    });
  }

  async function getDockerfilePresets() {
    try {
      vm.loading = true;
      vm.dockerfilePreset = await rbBuildDataService.getDockerfilePreset();
    } catch (e) {
      // ..
    }
    formatDockerfilePreset();
    vm.loading = false;
  }
}
