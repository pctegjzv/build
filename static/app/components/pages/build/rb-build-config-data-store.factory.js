import { map } from 'rxjs/operators';

import { resource_types as RESOURCE_TYPES } from '../rbac_role/rb-rbac-helper.constant';

angular
  .module('app.components.pages')
  .factory('rbBuildConfigDataStore', rbBuildConfigDataStoreFactory);

function rbBuildConfigDataStoreFactory(RbFetchDataStore, rbBuildDataService) {
  const fetchRequest = () => {
    return rbBuildDataService.getPrivateBuildConfigs().then(builds => {
      return builds.map(item => {
        item.display_name = item.space_name
          ? `${item.name}(${item.space_name})`
          : item.name;
        return item;
      });
    });
  };

  class RegionDataStore extends RbFetchDataStore {
    constructor() {
      super(fetchRequest);
    }

    getByName$(name) {
      return this.data$.pipe(
        map(configs => configs && configs.find(config => config.name === name)),
      );
    }

    getByResourceAction$(action) {
      return this.data$.pipe(
        map(
          configs =>
            configs &&
            configs.filter(config =>
              _.get(config, 'resource_actions', []).includes(
                `${RESOURCE_TYPES.BUILD_CONFIG}:${action}`,
              ),
            ),
        ),
      );
    }
  }

  return new RegionDataStore();
}
