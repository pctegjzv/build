angular
  .module('app.components.pages')
  .factory('rbBuildDataService', rbBuildDataServiceFactory);

function rbBuildDataServiceFactory(
  rbHttp,
  rbToast,
  translateService,
  rbAccountService,
) {
  const namespace = rbAccountService.getCurrentNamespace();
  const BUILDS_URL = `/ajax/private-builds/${namespace}/`;
  const CODE_CLIENTS_URL = `/ajax/private-build-code-clients/${namespace}/`;
  const CONFIGS_URL = `/ajax/private-build-configs/${namespace}/`;
  const ENDPOINTS_URL = `/ajax/private-build-endpoints/${namespace}/`;
  const CI_URL = '/ajax/ci-envs-catalog/';
  const SONARQUBE_URL = `/ajax/private-build-integrations/${namespace}/sonarqube/`;
  const DOCKERFILE_URL = `/ajax/dockerfile/${namespace}/`;

  return {
    getPrivateBuildValid,
    getPrivateBuilds,
    createPrivateBuild,
    deletePrivateBuild,
    getPrivateBuildDetail,
    createPrivateBuildConfig,
    updatePrivateBuildConfig,
    deletePrivateBuildConfig,
    getPrivateBuildConfigs,
    getPrivateBuildConfigDetail,
    getPrivateBuildCodeClientOrgs,
    getPrivateBuildCodeClients,
    getPrivateBuildCodeClientAuthUrl,
    linkPrivateBuildCodeClient,
    unlinkPrivateBuildCodeClient,
    getPrivateBuildCodeRepos,
    getCiEnvCatalogs,
    getBuildEndpoints,
    getSonarqubeLanguages,
    getSonarqubeQualityGates,
    getCiYaml,
    getConfigName,
    getDockerfilePreset,
  };

  ////////////
  function getPrivateBuildValid() {
    return rbHttp.sendRequest({
      method: 'GET',
      cache: true,
      url: BUILDS_URL + 'is_enabled',
    });
  }

  function getPrivateBuilds({
    build_config_name,
    page,
    image_repo_name,
    registry_name,
    repository_project_name = '',
  }) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: BUILDS_URL,
      params: {
        build_config_name,
        page,
        image_repo_name,
        registry_name,
        repository_project_name,
      },
    });
  }

  function createPrivateBuild(build) {
    if (build.build_display_name) {
      delete build.build_display_name;
    }
    return rbHttp
      .sendRequest({
        method: 'POST',
        url: BUILDS_URL,
        data: build,
      })
      .then(res => {
        rbToast.success(translateService.get('build_create_success'));
        return res;
      });
  }

  function deletePrivateBuild(buildId) {
    return rbHttp
      .sendRequest({
        method: 'DELETE',
        url: BUILDS_URL + buildId,
      })
      .then(res => {
        rbToast.success(translateService.get('build_delete_success'));
        return res;
      });
  }

  function getPrivateBuildDetail(buildId) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: BUILDS_URL + buildId,
    });
  }

  function getPrivateBuildConfigs(page, page_size, search) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: CONFIGS_URL,
        params: { page, page_size, search },
      })
      .then(res => {
        if (res.results) {
          // build pagination
          return res;
        } else if (res.result) {
          // no pagination params
          return res.result;
        }
      });
  }

  function createPrivateBuildConfig(config) {
    return rbHttp
      .sendRequest({
        method: 'POST',
        url: CONFIGS_URL,
        data: config,
      })
      .then(result => ({ result }))
      .then(res => {
        rbToast.success(translateService.get('build_config_create_success'));
        return res;
      });
  }

  function updatePrivateBuildConfig(config) {
    return rbHttp
      .sendRequest({
        method: 'PUT',
        url: CONFIGS_URL + getConfigName(config),
        data: config,
      })
      .then(result => ({ result }))
      .then(res => {
        rbToast.success(translateService.get('build_config_update_success'));
        return res;
      });
  }

  function deletePrivateBuildConfig(name) {
    return rbHttp
      .sendRequest({
        method: 'DELETE',
        url: CONFIGS_URL + name,
      })
      .then(res => {
        rbToast.success(translateService.get('build_config_delete_success'));
        return res;
      });
  }

  function getPrivateBuildConfigDetail(name) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: CONFIGS_URL + name,
    });
  }

  function getPrivateBuildCodeClients() {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: CODE_CLIENTS_URL,
      })
      .then(res => res.result);
  }

  function getPrivateBuildCodeClientOrgs({ codeClientName }) {
    async function _getPrivateBuildCodeClientOrgs(page = 1) {
      const res = await rbHttp.sendRequest({
        method: 'GET',
        url: CODE_CLIENTS_URL + codeClientName,
        params: {
          page,
        },
        addNamespace: false,
      });
      const code_repo_orgs = res.result || res;

      // Bitbucket returns code repo orgs with pagination
      if ('page' in code_repo_orgs) {
        // If this is not the last page, recur the request
        if (
          code_repo_orgs.size / code_repo_orgs.pagelen >
          code_repo_orgs.page
        ) {
          return code_repo_orgs.values.concat(
            await _getPrivateBuildCodeClientOrgs(page + 1),
          );
        } else {
          return code_repo_orgs.values;
        }
      } else {
        // GitHub returns array of orgs directly
        return code_repo_orgs;
      }
    }

    return _getPrivateBuildCodeClientOrgs();
  }

  function _generateOauthRedirectUri({ codeClientName, redirectParams }) {
    const origin = location.origin;
    const encodedRedirectParams = JSON.stringify(redirectParams);

    // We need to configure /auth-redirect in github application management page
    return `${origin}/console/auth-redirect/${codeClientName}?redirect_params=${encodedRedirectParams}`;
  }

  function getPrivateBuildCodeClientAuthUrl({
    codeClientName,
    redirectParams,
  }) {
    return rbHttp
      .sendRequest({
        method: 'POST',
        url: CODE_CLIENTS_URL + codeClientName + '/auth-url',
        addNamespace: false,
      })
      .then(res => {
        // The backend returns a partial URL. We need to append redirect_uri here
        let authUrl = res.auth_url;

        const redirectUri = _generateOauthRedirectUri({
          codeClientName,
          redirectParams,
        });

        // Both GitHub / BitBucket follow Oauth2 RFC. We may need to think about for other cases in the future.
        authUrl += '&redirect_uri=' + encodeURIComponent(redirectUri);

        // Need to confirm with backend about this parameter
        if (codeClientName === 'BITBUCKET') {
          authUrl += '&response_type=code';
        }

        return authUrl;
      });
  }

  function linkPrivateBuildCodeClient({
    codeClientName,
    code,
    state,
    redirectParams,
    oschinaUserEmail,
    oschinaUserPwd,
  }) {
    const redirectUri = _generateOauthRedirectUri({
      codeClientName,
      redirectParams,
    });
    return rbHttp.sendRequest({
      method: 'POST',
      url: CODE_CLIENTS_URL,
      data: {
        code_client_name: codeClientName,
        oschina_user_email: oschinaUserEmail,
        oschina_user_pwd: oschinaUserPwd,
        code,
        state,
        redirect_uri: redirectUri,
      },
      addNamespace: false,
    });
  }

  function unlinkPrivateBuildCodeClient({ codeClientName }) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url: CODE_CLIENTS_URL + codeClientName,
      addNamespace: false,
    });
  }

  async function getPrivateBuildCodeRepos({
    codeClientOrgName,
    codeClientName,
    page,
    pagelen,
  }) {
    // Bitbucket returns code repos with pagination
    const pagingEnabled = ['BITBUCKET'].includes(codeClientName);
    const params = {};

    if (pagingEnabled) {
      Object.assign(params, {
        page,
        pagelen,
      });
    }

    const res = await rbHttp.sendRequest({
      method: 'GET',
      url:
        CODE_CLIENTS_URL +
        codeClientName +
        '/orgs/' +
        codeClientOrgName +
        '/repos',
      params,
      cache: true,
      addNamespace: false,
    });

    const repos = res.result || res;

    if ('page' in repos && pagingEnabled) {
      return repos;
    } else {
      // GitHub returns array of repos directly
      return {
        page,
        size: repos.length,
        values: repos.slice((page - 1) * pagelen, page * pagelen),
      };
    }
  }

  function getCiEnvCatalogs() {
    return rbHttp
      .sendRequest({
        cache: true,
        method: 'GET',
        url: CI_URL,
      })
      .then(res => res.result);
  }

  function getBuildEndpoints() {
    return rbHttp.sendRequest({
      method: 'GET',
      url: ENDPOINTS_URL,
    });
  }

  function getSonarqubeLanguages(integration_id) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: SONARQUBE_URL + integration_id + '/languages',
    });
  }

  function getSonarqubeQualityGates(integration_id) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: SONARQUBE_URL + integration_id + '/qualitygates',
    });
  }

  function getCiYaml(data) {
    return rbHttp.sendRequest({
      method: 'POST',
      url: CONFIGS_URL + 'alaudaci-yaml',
      data,
    });
  }

  function getConfigName(config) {
    return config.uuid || config.config_id;
  }

  function getDockerfilePreset() {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: DOCKERFILE_URL,
      })
      .then(res => res.result);
  }
}
