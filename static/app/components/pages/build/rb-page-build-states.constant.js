// Maps URL params to bindings
const buildConfigParamResolver = {
  buildConfigName: $stateParams => {
    'ngInject';
    return $stateParams['name'];
  },
  buildConfig: $stateParams => {
    'ngInject';
    return $stateParams['buildConfig'];
  },
};

const buildRedirect = trans => {
  const rbBuildDataService = trans.injector().get('rbBuildDataService');
  return rbBuildDataService
    .getPrivateBuildValid()
    .then(({ is_enabled }) => {
      if (!is_enabled) {
        return 'blank_page';
      }
      return {}; // Trans to default state
    })
    .catch(() => 'blank_page');
};

module.exports = [
  {
    baseStateName: 'build.config',
    states: [
      {
        url: '/list',
        name: 'list',
        component: 'rbBuildConfigList',
        redirectTo: buildRedirect,
      },
      {
        url: '/create?oauthCodeRepoClientName',
        name: 'create',
        component: 'rbBuildConfigCreate',
        resolve: {
          oauthCodeRepoClientName: $stateParams => {
            'ngInject';
            return $stateParams['oauthCodeRepoClientName'];
          },
        },
      },
      {
        url: '/detail?name',
        name: 'detail',
        component: 'rbBuildConfigDetail',
        params: {
          buildConfig: null, // build config should be able to be preloaded
        },
        resolve: buildConfigParamResolver,
      },
      {
        url: '/update/:name',
        name: 'update',
        component: 'rbBuildConfigUpdate',
        params: {
          buildConfig: null, // build config should be able to be preloaded
        },
        resolve: buildConfigParamResolver,
      },
    ],
  },
  {
    baseStateName: 'build.history',
    states: [
      {
        url: '/list',
        name: 'list',
        component: 'rbBuildHistoryList',
        redirectTo: buildRedirect,
      },
      {
        url: '/detail?id',
        name: 'detail',
        component: 'rbBuildHistoryDetail',
        params: {
          buildDetail: null, // build detail should be able to be preloaded
        },
        resolve: {
          buildId: $stateParams => {
            'ngInject';
            return $stateParams['id'];
          },
          buildDetail: $stateParams => {
            'ngInject';
            return $stateParams['buildDetail'];
          },
        },
      },
    ],
  },
];
