/**
 * Created by liudong on 2016/11/8.
 */
import moment from 'moment';
import { isEmpty } from 'lodash';
import templateStr from 'app/components/pages/event/list/rb-event-list.html';

angular.module('app.components.pages').component('rbEventList', {
  controller: rbEventListController,
  controllerAs: 'vm',
  bindings: {
    type: '<', // resource_type: service, multiple resource_types: service,region,node
    primaryKey: '<', // resource_id
  },
  template: templateStr,
});

function rbEventListController(
  $element,
  $scope,
  $interval,
  $filter,
  $state,
  eventService,
  orgService,
  rbAccountService,
  translateService,
) {
  const vm = this;
  vm.queryString = '';
  vm.events = [];
  vm.searchMode = false;
  let eventPolling;

  vm.$onInit = _init;
  vm.$onDestroy = _destroy;
  vm.$postLink = _postLink;

  vm.loadEvents = loadEvents;
  vm.queryEvents = queryEvents;

  //////////
  function _init() {
    vm.pagination = {
      pageno: 1,
      total_items: 0,
    };
    loadEvents();
  }

  function _destroy() {
    $interval.cancel(eventPolling);
  }

  // event list 在首页时可以自动轮询
  $scope.$watch(
    () => {
      return vm.pagination.pageno;
    },
    newValue => {
      if (newValue === 1) {
        eventPolling = $interval(() => {
          loadEvents();
        }, 30 * 1000);
      } else {
        $interval.cancel(eventPolling);
      }
    },
  );

  function _postLink() {
    $element.popover({
      content: function _contentFn() {
        return decodeURIComponent($(this).data('message'));
      },
      delay: 150,
      placement: 'top',
      trigger: 'hover',
      selector: '.item-message-icon',
      container: 'body',

      // TODO: the following should not have any sideeffects !
      viewport: function _viewportFn() {
        if (this.$element && this.$element.data('class')) {
          this.options.template = this.options.template.replace(
            'class="popover"',
            'class="popover ' + this.$element.data('class') + '"',
          );
        }
        return 'body';
      },
    });
  }

  function queryEvents(value) {
    vm.queryString = value;
    vm.searchMode = true;
    vm.pagination.total_items = 0; // reset rb-pagination
    loadEvents();
  }

  function loadEvents(page = 1, size = 20) {
    vm.pagination.pageno = page; // watch pageno to support event polling
    const _now = moment();
    const _start = _now
      .clone()
      .startOf('day')
      .subtract(29, 'days');
    const params = {
      namespace: rbAccountService.getCurrentNamespace(),
      end_time: _now.valueOf(),
      start_time: _start.valueOf(),
      pageno: page,
      size: size,
    };
    if (vm.primaryKey) {
      params['event_pk'] = vm.primaryKey;
    }
    if (vm.type) {
      const types = vm.type.split(',');
      if (types.length > 1) {
        params['event_types'] = vm.type;
      } else {
        params['event_type'] = vm.type;
      }
    }
    if (!isEmpty(vm.queryString)) {
      params['query_string'] = vm.queryString;
    }
    vm.eventsLoading = true;

    // 缓存上一次展开的event，下一次轮询扔需要展开
    const showDetailItems = vm.events.filter(event => event.showDetail);

    eventService
      .eventList(params)
      .then(data => {
        // 在刷新events list前把已经弹出的提示给隐藏
        $(
          '.rb-popover-error.popover, .rb-popover-warnning.popover, .rb-popover-success.popover',
        ).popover('destroy');
        vm.events = data.results;
        vm.pagination.total_items = data.total_items;
        if (vm.events.length === 0) {
          return;
        }
        vm.events.forEach(item => {
          item.json = _stringifyDetail(item);
          item.showDetail = showDetailItems.find(
            showDetailItem =>
              showDetailItem.resource_id === item.resource_id &&
              showDetailItem.time === item.time,
          );
          item.message = generateEventMessage(item);
        });
      })
      .finally(() => {
        vm.eventsLoading = false;
      });
  }

  function _resourceDetailUrl(item) {
    let state = 'app_service.app.list';
    let queryParams = '';
    let routeLink;
    switch (item.resource_type.toLowerCase()) {
      case 'service':
        state = `app_service.service.service_detail({service_name: '${item.resource_id ||
          ''}'})`;
        break;
      case 'application':
        state = `app_service.app.app_detail({
          app_name: '${item.resource_id || ''}'
        })`;
        break;
      case 'node':
        state = `region.node_detail({regionName: '${item.detail.region_name ||
          ''}', privateIp: '${item.resource_name || ''}'})`;
        break;
      case 'notification':
        state = `monitor.notification.detail({notificationName: '${item.resource_name ||
          ''}'})`;
        break;
      case 'alarm':
        state = `monitor.alarmv2.alarm_detail({uuid: '${item.resource_id ||
          ''}'})`;
        break;
      case 'log_alarm':
        state = `monitor.alarmv2.log_alarm_detail({uuid: '${item.resource_id ||
          ''}'})`;
        break;
      case 'repository':
      case 'repo':
        routeLink = `/image/repository/detail`;
        queryParams = `{repositoryName: '${item.resource_name || ''}'}`;
        break;
      case 'priv_registry':
        routeLink = `/image/repository`;
        queryParams = `{registryName: '${item.resource_name || ''}'}`;
        break;
      case 'sync_regis_conf':
        routeLink = `/image/sync-center/detail/${item.resource_name}`;
        break;
      case 'sync_regis_history':
        routeLink = `/image/sync-history/detail/${item.resource_id})`;
        break;
      case 'build':
        state = `image.build.detail({build_id: '${item.resource_id || ''}'})`;
        break;
      case 'env_file':
      case 'envfile':
        state = `app_service.envfile.detail({envfileName: '${item.resource_name ||
          ''}'})`;
        break;
      case 'template':
      case 'application_temp':
        state = `app_service.template.detail({templateName: '${item.resource_name ||
          ''}'})`;
        break;
      case 'config':
        state = `app_service.configuration.detail({name: '${item.resource_name ||
          ''}'})`;
        break;
      case 'team':
        state = `org.team({orgName: '${item.namespace ||
          ''}', teamName: '${item.resource_name || ''}'})`;
        break;
      case 'organization':
      case 'ldap_config':
      case 'ldap_sync':
        state = `org.detail({orgName: '${item.namespace || ''}'})`;
        break;
      case 'pipeline':
      case 'pipeline_config':
        state = `pipeline.config.detail({name: '${item.resource_name || ''}'})`;
        break;
      case 'pipeline_history':
        state = `pipeline.history.detail({pipelineName: '${item.resource_name ||
          ''}', historyId: '${item.resource_id || ''}'})`;
        break;
      case 'builds_config':
        state = `build.config.detail({name: '${item.resource_name || ''}'})`;
        break;
      case 'cloud_account':
      case 'cmp':
        state = 'cloud.account.list';
        break;
      case 'volume':
        state = `storage.volume.detail({uuid: '${item.resource_id || ''}'})`;
        break;
      case 'snapshot':
        state = 'storage.snapshot.list';
        break;
      case 'region':
        state = `cluster.detail({name: '${item.resource_name || ''}'})`;
        break;
      case 'feature':
        state = `cluster.detail({name: '${item.detail.region_name || ''}'})`;
        break;
      case 'dashboard':
        state = `monitor.dashboard.state_dashboard_detail({uuid: '${item.resource_id ||
          ''}'})`;
        break;
      case 'space':
      case 'quota':
        state = `resource.quota_space.detail({name: '${item.resource_name}'})`;
        break;
      case 'role':
        state = `org.role_detail({roleName: '${item.resource_name}'})`;
        break;
      case 'lb':
      case 'load_balancer':
        state = `network.load_balancer.detail({name: '${item.resource_name}'})`;
        break;
      case 'job_config':
        state = `job.config.detail({name: '${item.resource_name}'})`;
        break;
      case 'job_history':
        state = `job.history.detail({uuid: '${item.resource_id}'})`;
        break;
      case 'integration':
        state = `integration_center.detail({uuid: '${item.resource_id}'})`;
        break;
    }
    return {
      state,
      routeLink,
      queryParams,
    };
  }

  function _generateMessageTooltip(item) {
    if (!item.detail.message) {
      return '';
    } else {
      let popoverClass = '';
      switch (item.log_level) {
        case 0:
          popoverClass = 'rb-popover-success';
          break;
        case 1:
          popoverClass = 'rb-popover-warnning';
          break;
        case 2:
          popoverClass = 'rb-popover-error';
          break;
        default:
          popoverClass = '';
          break;
      }

      return `<i class="fa fa-commenting item-message-icon"
                 data-message="${encodeURIComponent(item.detail.message)}"
                 data-class="${popoverClass}"/>`;
    }
  }

  function _subLinkUrl(item) {
    switch (item.sub_resource_type) {
      case 'build':
        return `image.build.detail({build_id: ${item.sub_resource_id})`;
    }
  }

  // using 'sub_accounts' template
  function _getParentResourceLink(parent) {
    if (!parent) {
      return '';
    }
    let state;
    switch (parent.resource_type) {
      case 'builds_config':
        state = `build.config.detail({name: '${parent.resource_name}'})`;
        return `<a ui-sref="${state}">${parent.resource_name}</a>`;
      case 'pipeline_config':
        state = `pipeline.config.detail({name: '${parent.resource_name}'})`;
        return `<a ui-sref="${state}">${parent.resource_name}</a>`;
      case 'sync_regis_conf':
        return `<a router-link="'/image/sync-center/detail/${
          parent.resource_name
        }'">${parent.resource_name}</a>`;
      case 'job_config':
        state = `job.config.detail({name: '${parent.resource_name}'})`;
        return `<a ui-sref="${state}">${parent.resource_name}</a>`;
      case 'cloud_account':
        state = 'cloud.account.list';
        return `<a ui-sref="${state}">${parent.resource_name}</a>`;
      case 'role':
        return parent.resource_name;
      default:
        return `<a>${parent.resource_name}</a>`;
    }
  }

  // using 'sub_accounts' template
  function _getResourceLink(event) {
    let state = '';
    switch (event.resource_type) {
      case 'priv_build':
        state = `build.history.detail({id: '${event.resource_id}'})`;
        return `<a ui-sref="${state}">${event.resource_id}</a>`;
      case 'pipeline_history':
        state = `pipeline.history.detail({pipelineName: '${
          event.detail.parent.resource_name
        }', historyId: '${event.resource_id}'})`;
        return `<a ui-sref="${state}">${event.resource_id}</a>`;
      case 'sync_regis_conf':
        state = `image.sync-center.detail({name: '${event.resource_name}'})`;
        return `<a ui-sref="${state}">${event.resource_name}</a>`;
      case 'sync_regis_history':
        return `<a router-link="'/image/sync-history/detail/${
          event.resource_id
        }'">${event.resource_id}</a>`;
      case 'job_history':
        state = `job.history.detail({uuid: '${event.resource_id}'})`;
        return `<a ui-sref="${state}">${event.resource_id}</a>`;
      case 'node':
        state = 'cloud.instance.list';
        return `<a ui-sref="${state}">${event.resource_name}</a>`;
      case 'permission':
        return '';
      default:
        return `<a>${event.resource_id}</a>`;
    }
  }

  function _getPermissionAttrDesc(value) {
    const mapping = orgService
      .getTeamPermissions()
      .concat(orgService.getDefaultResourcePrivileges());
    mapping.forEach(item => {
      if (item.value === value) {
        value = item.name;
        return false;
      }
    });
    return value;
  }

  function generateEventMessage(item) {
    let link = '';
    let sublink = '';
    const messageTooltip = _generateMessageTooltip(item);
    const { state, routeLink, queryParams } = _resourceDetailUrl(item);
    let resource = '',
      parentResource = '';
    let message =
      '<span class="margin-right-2">' +
      $filter('formatDate')(item.time * 1000) +
      '</span>';
    const detail = item.detail;
    switch (item.template_id) {
      case 'generic':
        if (
          ['project', 'project_template', 'role', 'log_filter'].includes(
            item.resource_type,
          )
        ) {
          link = item.resource_name;
        } else if (routeLink) {
          link =
            `<a router-link="'${routeLink}'" query-params="${queryParams}">` +
            item.resource_name +
            '</a>';
        } else {
          link = `<a ui-sref="${state}">` + item.resource_name + '</a>';
        }
        message += _generateSubMessage(
          item.detail.operator,
          'event_' + item.detail.operation,
          item.resource_type,
          link,
        );
        break;
      case 'sub_resource':
        if (item.detail.parent) {
          resource = _getResourceLink(item);
          parentResource = _getParentResourceLink(item.detail.parent);
          message += _generateSubMessage(
            item.detail.operator,
            `event_${item.detail.operation}`,
            item.detail.parent.resource_type,
            parentResource,
            translateService.get('event_of'),
            item.resource_type,
            resource,
          );
        }
        break;
      case 'build':
        link = `<a ui-sref="${state}">` + item.resource_id + '</a>';
        message += _generateSubMessage(
          item.detail.operator,
          'event_' + item.detail.operation,
          item.resource_type,
          link + translateService.get('event_of'),
          translateService.get('build'),
          item.resource_name,
        );
        break;
      case 'build_result':
        link = `<a ui-sref="${state}">` + item.resource_name + '</a>';
        sublink = `<a ui-sref="${_subLinkUrl(item.detail)}">${
          item.detail.sub_resource_name
        }</a>`;
        message += _generateSubMessage(
          item.resource_type,
          link + translateService.get('event_of'),
          'build',
          sublink,
          'event_' + item.detail.operation,
          item.detail.status,
        );
        break;
      case 'build_v2':
        link = `<a ui-sref="${state}">` + item.resource_name + '</a>';
        sublink = `<a ui-sref="${_subLinkUrl(item.detail)}">${
          item.detail.sub_resource_name
        }</a>`;
        message += _generateSubMessage(
          item.detail.operator,
          'event_' + item.detail.operation,
          item.resource_type,
          link + translateService.get('event_of'),
          'build',
          sublink,
        );
        break;
      case 'repo_tag':
        link = `<a ui-sref="${state}">` + item.resource_name + '</a>';
        message += _generateSubMessage(
          item.detail.operator,
          'event_' + item.detail.operation,
          item.resource_type,
          link + translateService.get('event_of'),
          item.detail.sub_resource_type,
          `<span>${item.detail.sub_resource_name}</span>`,
        );
        break;
      case 'node':
        link = `<a ui-sref="${state}">` + item.resource_name + '</a>';
        message += _generateSubMessage(
          item.detail.operator,
          'event_' + item.detail.operation,
          item.resource_type,
          item.namespace +
            '/' +
            item.detail.region_display_name +
            translateService.get('event_of'),
          link,
        );
        break;
      case 'result':
        link = `<a ui-sref="${state}">` + item.resource_name + '</a>';
        message += _generateSubMessage(
          item.resource_type,
          link,
          item.detail.operation,
          item.detail.status,
        );
        break;
      case 'service_status':
      case 'region_status':
        link = `<a ui-sref="${state}">` + item.resource_name + '</a>';
        message += _generateSubMessage(
          item.resource_type,
          link,
          'entered',
          `status_${item.detail.status}`,
          'status',
        );
        break;
      case 'app_task_result':
        link = `<a ui-sref="${state}">` + item.resource_name + '</a>';
        item.links = [];
        item.detail['sub_resource_id'].split(',').forEach((e, i) => {
          // if there's not any service
          item.detail.operation === 'create' && item.detail.status === 'failure'
            ? item.links.push(e)
            : item.links.push(`
                <a ng-href="${$state.href(
                  'app_service.service.service_detail',
                  {
                    service_name: e,
                  },
                )}">${item.detail.sub_resource_name.split(',')[i]}</a>
              `);
        });
        item.links = item.links.join(',&nbsp;');
        message += _generateSubMessage(
          `event_${item.resource_type}`,
          link,
          'event_of',
          item.detail.sub_resource_type,
          item.links,
          `event_result_${item.detail.operation}`,
          `status_${item.detail.status}`,
        );
        break;
      case 'permission-change':
        link = `<a ui-sref="${state}">` + item.resource_name + '</a>';
        message += _generateSubMessage(
          detail.operator,
          'event_' + detail.operation,
          detail.object_type,
          detail['object'],
          translateService.get('event_for'),
          item.resource_type,
          link + translateService.get('event_of'),
          detail.sub_attr_type,
          _getPermissionAttrDesc(detail.sub_attr_name),
        );
        break;
      case 'team-update':
        link = `<a ui-sref="${state}">` + item.resource_name + '</a>';
        message += _generateSubMessage(
          detail.operator,
          'event_' + detail.operation,
          item.resource_type,
          link + translateService.get('event_of'),
          detail.sub_attr_type,
          translateService.get('event_to'),
          _getPermissionAttrDesc(detail.sub_attr_value),
        );
        break;
      case 'generic-svoa':
        if (item.resource_type === 'role') {
          link = item.resource_name;
        } else {
          link = `<a ui-sref="${state}">` + item.resource_name + '</a>';
        }
        message += _generateSubMessage(
          detail.operator,
          'event_' + detail.operation,
          item.resource_type,
          link + translateService.get('event_of'),
          detail.attribute_type,
          detail.attribute,
        );
        break;
      case 'registry-gc':
        message += translateService.get('event_registry_gc', {
          state,
          resource_name: item.resource_name,
        });
        break;
      case 'registry-gc-completed':
        message += translateService.get('event_registry_gc_completed', {
          state,
          resource_name: item.resource_name,
        });
        break;
      default:
        break;
    }
    if (messageTooltip !== '') {
      message += messageTooltip;
    }
    return message;
  }

  function _stringifyDetail(detail) {
    return JSON.stringify(detail, null, 4);
  }

  function _generateSubMessage() {
    return [...arguments]
      .map(term => {
        if (!term.match(/<a/)) {
          return translateService.get(term.toLowerCase());
        } else {
          return term;
        }
      })
      .join(translateService.get('event_term_delimiter'));
  }
}
