angular.module('app.components.pages').component('rbPageExternalContent', {
  controller: rbPageExternalContentController,
  controllerAs: 'vm',
  template:
    '<iframe ng-src="{{ vm.url }}" style="height: 100%; width: 100%; border: none;"></iframe>',
});

function rbPageExternalContentController($sce, rbHttp) {
  const vm = this;

  rbHttp
    .sendRequest({
      method: 'GET',
      url: '/ajax/external/content',
    })
    .then(externalContent => {
      const params = _.pick(externalContent, [
        'username',
        'namespace',
        'permission',
        'timestamp',
        'md5hash',
      ]);
      const completeUrl = externalContent.url + '?' + $.param(params);
      vm.url = $sce.trustAsResourceUrl(completeUrl);
    });
}
