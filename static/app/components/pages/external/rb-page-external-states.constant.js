module.exports = {
  baseStateName: 'external',
  states: [
    {
      name: 'content',
      url: '',
      template: '<rb-page-external-content></rb-page-external-content>',
    },
  ],
};

angular
  .module('app.components.pages')
  .constant('rbPageExternalStates', module.exports);
