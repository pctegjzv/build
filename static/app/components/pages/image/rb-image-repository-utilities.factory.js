const _ = require('lodash');
angular
  .module('app.components.pages')
  .factory('rbImageRepositoryUtilities', rbImageRepositoryUtilities);

function rbImageRepositoryUtilities(
  rbGlobalSetting,
  rbAccountService,
  $injector,
  rbRegistryDataService,
) {
  return {
    formatRepositoryTagDetail,
    getTransformedRepository,
    getTransformedRepositoryTags,
    getTransformedRepositoryTagDetail,
    getImageAddress,
  };

  /////////

  function formatRepositoryTagDetail(tag_config) {
    const config = {};
    if (tag_config.Cmd) {
      config.run_command = [];
      angular.forEach(tag_config.Cmd, item => {
        let cmd = item;
        if (cmd && cmd.split(' ').length > 1) {
          cmd = "'" + cmd + "'";
        }
        config.run_command.push(cmd);
      });
      config.run_command = config.run_command.join(' ');
    }
    if (tag_config.Entrypoint) {
      config.entrypoint = tag_config.Entrypoint.join(' ');
    }
    if (tag_config.ExposedPorts) {
      const ports = Object.keys(tag_config.ExposedPorts);
      config.instance_ports = ports.join(',');
    }
    config.volumes = [];
    if (tag_config.Volumes) {
      const app_volume_dir = Object.keys(tag_config.Volumes);
      for (let i = 0; i < app_volume_dir.length; i++) {
        config.volumes.push({
          app_volume_dir: app_volume_dir[i],
        });
      }
    }
    config.image_envvars = {};
    if (tag_config.Env) {
      angular.forEach(tag_config.Env, e => {
        const arr = e.split('=');
        if (2 === arr.length) {
          config.image_envvars[arr[0]] = arr[1];
        }
      });
    }
    return config;
  }

  /**
   * get detail information from image_name: {endpoint, namespace, registry_name, project_name, repo_name, is_public_registry}
   * @param image_name
   */
  async function getTransformedRepository(image_name) {
    const result = {
      endpoint: '',
      namespace: '',
      registry_name: '',
      project_name: '',
      repo_name: '',
      is_public_registry: true,
    };
    const registries = await rbRegistryDataService.getRegistries();
    const fields = image_name.split('/');
    const [endpoint, ...parts] = fields;
    result.endpoint = endpoint;
    const registry = _.find(registries, item => {
      return item.endpoint === endpoint;
    });
    if (registry) {
      if (registry.is_public) {
        const [, namespace, repo_name] = fields;
        // repository is in public registry but not user namespace (eg: tutum, library), should get repository from thrid party API
        if (namespace !== rbAccountService.getCurrentNamespace()) {
          result.repo_name = parts.join('/');
          return result;
        }
        result.namespace = namespace;
        result.repo_name = repo_name;
      } else {
        if (fields.length === 2) {
          const [, repo_name] = fields;
          result.repo_name = repo_name;
        } else if (fields.length === 3) {
          const [, project_name, repo_name] = fields;
          result.project_name = project_name;
          result.repo_name = repo_name;
        }
      }
      result.is_public_registry = registry.is_public;
      result.registry_name = registry.name;
    } else {
      result.repo_name = parts.join('/');
    }
    return result;
  }

  async function getTransformedRepositoryTags({
    endpoint = '',
    // namespace = '',
    registry_name = '',
    project_name = '',
    repo_name = '',
    // is_public_registry = true
  }) {
    const rbRepositoryDataService = $injector.get('rbRepositoryDataService');
    if (registry_name) {
      return rbRepositoryDataService.getRepositoryTags({
        registry_name,
        project_name,
        repo_name,
      });
    } else {
      return rbRepositoryDataService.getPublicRepositoryTags({
        image_name: [endpoint, repo_name].join('/'),
      });
    }
  }

  async function getTransformedRepositoryTagDetail({
    endpoint = '',
    // namespace = '',
    registry_name = '',
    project_name = '',
    repo_name = '',
    // is_public_registry = true,
    tag_name = '',
  }) {
    const rbRepositoryDataService = $injector.get('rbRepositoryDataService');
    if (registry_name) {
      return rbRepositoryDataService.getRepositoryTagDetail({
        registry_name,
        project_name,
        repo_name,
        tag_name,
      });
    } else {
      return rbRepositoryDataService.getPublicRepositoryTagDetail({
        image_name: [endpoint, repo_name].join('/'),
        tag_name: tag_name,
      });
    }
  }

  function getImageAddress(repo, alaudaImageIndex) {
    if (repo.registry) {
      const registry = repo.registry;
      const project = repo.project;
      if (registry.is_public) {
        return `${repo.registry.endpoint}/${repo.namespace}/${repo.name}`;
      } else {
        if (project) {
          return `${repo.registry.endpoint}/${project.project_name}/${
            repo.name
          }`;
        } else {
          return `${repo.registry.endpoint}/${repo.name}`;
        }
      }
    } else {
      return `${alaudaImageIndex}/${repo.repo_path}`;
    }
  }
}
