angular
  .module('app.components.pages')
  .factory('rbImageSyncDataService', rbImageSyncDataService);

function rbImageSyncDataService(
  rbHttp,
  rbToast,
  translateService,
  rbAccountService,
) {
  const namespace = rbAccountService.getCurrentNamespace();

  const HISTORY_URL = `/ajax/sync-registry/${namespace}/histories/`;
  const CONFIG_URL = `/ajax/sync-registry/${namespace}/configs/`;

  return {
    getConfigs,
    getConfig,
    createConfig,
    deleteConfig,
    updateConfig,

    startSync,
    getHistories,
    getHistory,
    deleteHistory,

    getHistoryLogs,

    getConfigName,
  };

  async function getConfigs({ action = 'view' } = {}) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: CONFIG_URL,
        params: {
          action,
        },
      })
      .then(({ result }) => result);
  }

  async function createConfig(payload) {
    return rbHttp
      .sendRequest({
        method: 'POST',
        url: CONFIG_URL,
        data: payload,
      })
      .then(res => {
        rbToast.success(
          translateService.get('image_sync_center_create_success'),
        );
        return res;
      });
  }

  async function getConfig(configName) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: CONFIG_URL + configName,
    });
  }

  async function updateConfig(payload) {
    const config_name = getConfigName(payload);
    return rbHttp
      .sendRequest({
        method: 'PUT',
        url: CONFIG_URL + config_name,
        data: payload,
      })
      .then(res => {
        rbToast.success(
          translateService.get('image_sync_center_update_success'),
        );
        return res;
      });
  }

  async function deleteConfig(config) {
    const config_name = getConfigName(config);
    return rbHttp
      .sendRequest({
        url: CONFIG_URL + config_name,
        method: 'DELETE',
      })
      .then(res => {
        rbToast.success(
          translateService.get('image_sync_center_delete_success'),
        );
        return res;
      });
  }

  async function startSync({ config_name, tag, dest_id_list }) {
    return rbHttp
      .sendRequest({
        method: 'POST',
        url: HISTORY_URL,
        data: {
          config_name,
          tag,
          dest_id_list,
        },
      })
      .then(res => {
        rbToast.success(
          translateService.get('image_sync_center_start_success'),
        );
        return res.result;
      });
  }

  async function getHistories({
    config_name,
    repo_name,
    registry_name,
    repository_project_name,
    page,
    num_per_page = 20,
  }) {
    return rbHttp.sendRequest({
      url: HISTORY_URL,
      method: 'GET',
      params: {
        config_name,
        repo_name,
        registry_name,
        repository_project_name,
        page,
        num_per_page,
      },
    });
  }

  async function getHistory(history_id) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: HISTORY_URL + history_id,
    });
  }

  async function deleteHistory(history_id) {
    return rbHttp
      .sendRequest({
        method: 'DELETE',
        url: HISTORY_URL + history_id,
      })
      .then(res => {
        rbToast.success(
          translateService.get('image_sync_center_history_delete_success'),
        );
        return res;
      });
  }

  async function getHistoryLogs({
    history_id,
    start_time,
    end_time,
    limit = 2000,
  }) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: HISTORY_URL + history_id + '/logs',
        params: {
          start_time,
          end_time,
          limit,
        },
      })
      .then(({ result }) => ({ logs: result }));
  }
  function getConfigName(config) {
    return config.uuid ? config.uuid : config.config_id;
  }
}
