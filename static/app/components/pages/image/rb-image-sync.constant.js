export const IMAGE_SYNC_HISTORY_STATUS_MAP = {
  S: 'success',
  F: 'error',
  B: 'blocked',
  W: 'waiting',
  I: 'in-progress',
  D: 'deleted',
};

export const IMAGE_SYNC_HISTORY_STATUS_CSS_MAP = {
  S: 'success',
  F: 'error',
  B: 'pending',
  W: 'pending',
  I: 'in-progress',
  D: 'deleted',
};

export const IMAGE_SYNC_DEST_TYPE = {
  INTERNAL: 'INTERNAL_REGISTRY',
  EXTERNAL: 'EXTERNAL_REGISTRY',
};

export const IMAGE_SYNC_CONTEINER_SIZE_OPTIONS = {
  CPU: {
    floor: 0.25,
    ceil: 1,
    step: 0.25,
    precision: 2,
    hidePointerLabels: true,
    hideLimitLabels: true,
    showSelectionBar: true,
  },
  MEMORY: {
    floor: 256,
    ceil: 2048,
    step: 256,
    precision: 0,
    hidePointerLabels: true,
    hideLimitLabels: true,
    showSelectionBar: true,
  },
};

export const IMAGE_SYNC_DEFAULT_CONTAINER_SIZE = {
  CPU: 0.5,
  MEMORY: 512,
};
