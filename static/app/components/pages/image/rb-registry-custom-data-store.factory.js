/**
 * Created by liudong on 2017/7/27.
 */
/**
 * Observable data store for registry with custom request params
 */
angular
  .module('app.components.pages')
  .factory('rbRegistryCustomDataStore', rbRegistryCustomDataStoreFactory);

function rbRegistryCustomDataStoreFactory(
  RbFetchDataStore,
  rbRegistryDataService,
) {
  class RegistryCustomDataStore extends RbFetchDataStore {
    constructor(fetchRequest) {
      super(fetchRequest);
    }
  }

  function getInstance(params) {
    const fetchRequest = () =>
      rbRegistryDataService.getRegistries({
        cache: false,
        params,
      });
    return new RegistryCustomDataStore(fetchRequest);
  }

  return {
    getInstance,
  };
}
