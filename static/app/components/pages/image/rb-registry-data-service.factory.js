/**
 * Created by liudong on 2016/12/22.
 */
import { pick } from 'lodash';

angular
  .module('app.components.pages')
  .factory('rbRegistryDataService', rbRegistryDataServiceFactory);

function rbRegistryDataServiceFactory(rbHttp, rbAccountService) {
  const namespace = rbAccountService.getCurrentNamespace();

  const REGISTRIES_URL = `/ajax/registries/${namespace}/`;

  return {
    getRegistries,
    getRegistry,
    getProjects,
    addProject,
    deleteProject,
    updateRegistry,
  };

  //////////
  async function getRegistries({ cache = false, params = {} } = {}) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: REGISTRIES_URL,
        params,
        cache,
      })
      .then(({ result }) => {
        result.forEach(item => {
          if (!item.display_name) {
            item.display_name = item.name;
          }
        });
        return result;
      })
      .catch(() => []);
  }

  async function getRegistry({ registry_name, cache = false }) {
    const registries = await getRegistries({ cache });
    return registries.find(item => item.name === registry_name);
  }

  function getProjects({ registry_name, cache = false }) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: REGISTRIES_URL + registry_name + '/projects/',
        cache: cache,
      })
      .then(({ result }) => result)
      .catch(() => []);
  }

  function addProject(data) {
    return rbHttp.sendRequest({
      method: 'POST',
      url: REGISTRIES_URL + data.registry_name + '/projects/',
      data: data,
    });
  }

  function deleteProject({ registry_name, project_name }) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url: REGISTRIES_URL + registry_name + '/projects/' + project_name,
    });
  }

  function updateRegistry(data) {
    return rbHttp.sendRequest({
      method: 'PUT',
      url: REGISTRIES_URL + data.uuid,
      data: pick(
        data,
        'integration_id',
        'tag_max_number',
        'auto_garbage_collect',
        'garbage_collect_schedule_rule',
      ),
    });
  }
}
