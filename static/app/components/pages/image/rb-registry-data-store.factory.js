import { filter, first, map } from 'rxjs/operators';

angular
  .module('app.components.pages')
  .factory('rbRegistryDataStore', rbRegistryDataStoreFactory);

/**
 * Observable data store for registry
 */
function rbRegistryDataStoreFactory(RbFetchDataStore, rbRegistryDataService) {
  const fetchRequest = () =>
    rbRegistryDataService.getRegistries({ cache: false });
  class RegistryDataStore extends RbFetchDataStore {
    constructor() {
      super(fetchRequest);
    }

    getRegistry$(registryName) {
      return this.data$.pipe(
        map(
          registries =>
            registries &&
            registries.find(registry => registry.name === registryName),
        ),
      );
    }

    getRegistryDisplayName$(registryName) {
      return this.getRegistry$(registryName)
        .pipe(filter(data => !!data))
        .pipe(first())
        .pipe(map(registry => registry.display_name));
    }
  }
  return new RegistryDataStore();
}
