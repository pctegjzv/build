import { filter, first } from 'rxjs/operators';

angular
  .module('app.components.pages')
  .factory('rbRegistryProjectDataStore', rbRegistryProjectDataStoreFactory);

/**
 * Observable data store for registry's project
 */
function rbRegistryProjectDataStoreFactory(
  translateService,
  RbFetchDataStore,
  rbRegistryDataStore,
  rbRegistryDataService,
) {
  const dataStoreMap = {}; // registry name -> project data store

  return {
    getInstance,
  };

  ////////
  function getInstance({ registry_name }) {
    if (!dataStoreMap[registry_name]) {
      const request = genFetchRequest(registry_name);
      dataStoreMap[registry_name] = new RbFetchDataStore(request);
    }
    return dataStoreMap[registry_name];
  }

  function genFetchRequest(registry_name) {
    return async () => {
      const registry = await rbRegistryDataStore
        .getRegistry$(registry_name)
        .pipe(
          filter(registry => !!registry),
          first(),
        )
        .toPromise();

      let projects = [
        {
          display_name: translateService.get('default_project_name'),
          project_name: '',
          is_default: true,
          registry,
        },
      ];

      if (!registry.is_public) {
        const remoteProjects = await rbRegistryDataService.getProjects({
          registry_name: registry.name,
        });
        remoteProjects.forEach(project => {
          project.display_name = project.project_name;
        });
        projects = remoteProjects.concat(projects);
      }
      return projects;
    };
  }
}
