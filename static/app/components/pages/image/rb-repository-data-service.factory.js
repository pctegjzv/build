/**
 * Created by liudong on 2016/12/22.
 */
// const _ = require('lodash');
angular
  .module('app.components.pages')
  .factory('rbRepositoryDataService', rbRepositoryDataServiceFactory);

/**
 * repository data service under registry
 */
function rbRepositoryDataServiceFactory(
  rbHttp,
  rbAccountService,
  rbImageRepositoryUtilities,
) {
  const namespace = rbAccountService.getCurrentNamespace();

  const REGISTRIES_URL = `/ajax/registries/${namespace}/`;

  return {
    getRepositories,
    getRepository,
    createRepository,
    updateRepository,
    deleteRepository,
    getRepositoryTags,
    updateRepositoryTag,
    getRepositoryTagArtifacts,
    startRepositoryTagScan,
    getRepositoryTagDetail,
    deleteRepositoryTag,
    getRepositoryTagSummaryReport,
    getRepositoryTagDetailReport,
    // ******* public repo *******
    getPublicRepositoryTags,
    getPublicRepositoryTagDetail,
    getPublicRepositoryGallery,
    getPublicRepositoryList,
    checkPublicRepositoryExist,
  };

  function _getBaseUrl(registry_name, project_name) {
    return project_name
      ? `${REGISTRIES_URL}${registry_name}/projects/${project_name}/repositories/`
      : `${REGISTRIES_URL}${registry_name}/repositories/`;
  }

  function getRepositories({
    registry_name,
    project_name,
    action = 'view',
    cache = false,
  }) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: _getBaseUrl(registry_name, project_name),
        params: { action },
        cache,
      })
      .then(({ result }) => result)
      .catch(() => []);
  }

  function getRepository({ registry_name, project_name, repo_name }) {
    return rbHttp
      .sendRequest({
        url: _getBaseUrl(registry_name, project_name) + repo_name,
      })
      .catch(() => null);
  }

  function createRepository(data) {
    return rbHttp.sendRequest({
      method: 'POST',
      url: _getBaseUrl(data.registry_name, data.repository_project_name),
      data,
    });
  }

  function updateRepository(data) {
    return rbHttp.sendRequest({
      method: 'PUT',
      url:
        _getBaseUrl(data.registry_name, data.repository_project_name) +
        data.name,
      data,
    });
  }

  function deleteRepository({ registry_name, project_name, repo_name }) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url: _getBaseUrl(registry_name, project_name) + repo_name,
    });
  }

  async function getRepositoryTags({
    registry_name,
    project_name,
    repo_name,
    view_type = '', // security, detail
    scan_results = false,
    artifacts = false,
    page_size = 20,
    page = 1,
    cache = false,
  }) {
    const params = {
      view_type,
      scan_results,
      artifacts,
      page_size,
      page,
    };

    if (view_type === 'artifacts') {
      delete params['artifacts'];
      delete params['scan_results'];
    }

    try {
      const res = await rbHttp.sendRequest({
        url: _getBaseUrl(registry_name, project_name) + repo_name + '/tags',
        params,
        cache,
      });

      return view_type
        ? {
            count: res.count,
            cur_page: res.cur_page,
            page_size: res.page_size,
            results: res.results,
          }
        : res.result;
    } catch (e) {
      return view_type ? {} : [];
    }
  }

  function getRepositoryTagArtifacts({
    registry_name,
    project_name,
    repo_name,
    tag_name,
    cache = false,
  }) {
    return rbHttp.sendRequest({
      url:
        _getBaseUrl(registry_name, project_name) +
        repo_name +
        '/tags/' +
        tag_name +
        '/artifacts',
      cache,
    });
  }

  function updateRepositoryTag(data) {
    return rbHttp.sendRequest({
      method: 'PUT',
      url:
        _getBaseUrl(data.registry_name, data.project_name) +
        data.repo_name +
        '/tags/' +
        data.tag_name,
      data: { protected: data.protected },
    });
  }

  function startRepositoryTagScan({
    registry_name,
    repository_project_name,
    repo_name,
    tag_name,
  }) {
    return rbHttp.sendRequest({
      method: 'POST',
      url:
        _getBaseUrl(registry_name, repository_project_name) +
        repo_name +
        '/tags/' +
        tag_name,
    });
  }

  function getRepositoryTagDetail({
    registry_name,
    project_name,
    repo_name,
    tag_name,
    cache = false,
  }) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url:
          _getBaseUrl(registry_name, project_name) +
          repo_name +
          '/tags/' +
          tag_name,
        cache,
      })
      .then(({ config }) =>
        rbImageRepositoryUtilities.formatRepositoryTagDetail(config),
      )
      .catch(() => null);
  }

  /**
   * @param registry_name
   * @param project_name
   * @param repo_name
   * @param tag
   * @returns {*}
   */
  function deleteRepositoryTag({
    registry_name,
    project_name,
    repo_name,
    tag,
  }) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url:
        _getBaseUrl(registry_name, project_name) + repo_name + '/tags/' + tag,
    });
  }

  function getRepositoryTagSummaryReport({
    registry_name,
    project_name,
    repo_name,
    tag_name,
    view_type, // cve, package
  }) {
    return rbHttp.sendRequest({
      method: 'GET',
      url:
        _getBaseUrl(registry_name, project_name) +
        repo_name +
        '/tags/' +
        tag_name +
        '/summary',
      params: {
        view_type,
      },
    });
  }

  function getRepositoryTagDetailReport({
    registry_name,
    project_name,
    repo_name,
    tag_name,
    view_type,
    page_size = 20,
    page = 1,
  }) {
    return rbHttp.sendRequest({
      method: 'GET',
      url:
        _getBaseUrl(registry_name, project_name) +
        repo_name +
        '/tags/' +
        tag_name +
        '/detail',
      params: {
        view_type,
        page_size,
        page,
      },
    });
  }

  // ******* public repo *******
  function getPublicRepositoryTags({ image_name, cache = false }) {
    const [index, ...repo_names] = image_name.split('/');
    const repo_path = repo_names.join('/');
    if (!index || !repo_path) {
      return Promise.resolve([]);
    } else {
      return rbHttp
        .sendRequest({
          url: '/ajax/public_repository/tag/list',
          params: { index, repo_path },
          cache,
        })
        .then(({ tags }) => {
          return tags.map(item => {
            return item.tag;
          });
        })
        .catch(() => []);
    }
  }

  function getPublicRepositoryTagDetail({
    image_name,
    tag_name,
    cache = false,
  }) {
    const [index, ...repo_names] = image_name.split('/');
    const repo_path = repo_names.join('/');
    if (!index || !repo_path) {
      return Promise.resolve(null);
    } else {
      return rbHttp
        .sendRequest({
          url: '/ajax/public_repository/tag',
          params: { index, repo_path, tag: tag_name },
          cache,
        })
        .then(({ tag: { config } }) =>
          rbImageRepositoryUtilities.formatRepositoryTagDetail(config),
        )
        .catch(() => null);
    }
  }

  function getPublicRepositoryGallery(type) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: '/ajax/public_repository/gallery',
      params: { gallery_type: type },
    });
  }

  function getPublicRepositoryList(namespace) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: `/ajax/repositories/${namespace}`,
    });
  }

  function checkPublicRepositoryExist(params) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: '/ajax/public_repository/exist',
      params,
    });
  }
}
