import { map } from 'rxjs/operators';

angular
  .module('app.components.pages')
  .factory('rbRepositoryDataStore', rbRepositoryDataStoreFactory);

/**
 * Observable data store for repository
 */
function rbRepositoryDataStoreFactory(
  RbFetchDataStore,
  rbRepositoryDataService,
) {
  const dataStoreMap = {}; // internal  name -> project data store

  class RepositoryDataStore extends RbFetchDataStore {
    constructor({ registry_name, project_name }) {
      super(genFetchRequest({ registry_name, project_name }));
    }
  }

  return {
    getInstance,
    getRepositoryByName$,
  };

  ////////
  function getInstance({ registry_name, project_name }) {
    const internalName = _internalRegistryName({ registry_name, project_name });

    if (!dataStoreMap[internalName]) {
      dataStoreMap[internalName] = new RepositoryDataStore({
        registry_name,
        project_name,
      });
    }
    return dataStoreMap[internalName];
  }

  function getRepositoryByName$({
    registry_name,
    project_name,
    repository_name,
  }) {
    return getInstance({ registry_name, project_name }).data$.pipe(
      map(
        repositories =>
          repositories &&
          repositories.find(repository => repository.name === repository_name),
      ),
    );
  }

  function _internalRegistryName({ registry_name, project_name }) {
    return (project_name ? project_name + '/' : '') + registry_name;
  }

  function genFetchRequest({ registry_name, project_name }) {
    return () =>
      registry_name
        ? rbRepositoryDataService.getRepositories({
            registry_name,
            project_name,
            cache: false,
          })
        : Promise.resolve(null);
  }
}
