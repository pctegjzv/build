angular
  .module('app.components.pages')
  .factory('rbRepositoryTagDataStore', rbRepositoryTagDataStoreFactory);

/**
 * Observable data store for repository
 */
function rbRepositoryTagDataStoreFactory(
  RbFetchDataStore,
  rbRepositoryDataService,
) {
  const dataStoreMap = {}; // internal  name -> project data store

  return {
    getInstance,
  };

  ////////
  function getInstance({ registry_name, project_name, repository_name }) {
    const internalName = _internalRegistryName({
      registry_name,
      project_name,
      repository_name,
    });

    if (!dataStoreMap[internalName]) {
      const request = genFetchRequest({
        registry_name,
        project_name,
        repository_name,
      });
      dataStoreMap[internalName] = new RbFetchDataStore(request);
    }
    return dataStoreMap[internalName];
  }

  function _internalRegistryName({
    registry_name,
    project_name,
    repository_name,
  }) {
    return [registry_name, project_name, repository_name].join('/');
  }

  function genFetchRequest({ registry_name, project_name, repository_name }) {
    return () =>
      rbRepositoryDataService.getRepositoryTags({
        registry_name,
        project_name,
        repo_name: repository_name,
        cache: false,
      });
  }
}
