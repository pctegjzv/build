angular
  .module('app.components.pages')
  .factory('rbSyncRegistryDataStore', rbSyncRegistryDataStoreFactory);

/**
 * Observable data store for registry
 */
function rbSyncRegistryDataStoreFactory(
  RbFetchDataStore,
  rbImageSyncDataService,
) {
  const fetchRequest = () => {
    return rbImageSyncDataService.getConfigs().then(configs => {
      return configs.map(item => {
        item.display_name = item.space_name
          ? `${item.config_name}(${item.space_name})`
          : item.config_name;
        return item;
      });
    });
  };
  class SyncRegistryConfigDataStore extends RbFetchDataStore {
    constructor() {
      super(fetchRequest);
    }
  }
  return new SyncRegistryConfigDataStore();
}
