import { IMAGE_SYNC_DEST_TYPE } from '../../../rb-image-sync.constant';
const templateStr = require('app/components/pages/image/sync-center/create/fieldset-dest-repo/rb-fieldset-image-sync-dest.html');
angular.module('app.components.pages').component('rbFieldsetImageSyncDest', {
  controller: rbFieldsetImageSyncDestController,
  controllerAs: 'vm',
  bindings: {
    model: '<',
  },
  template: templateStr,
});

function rbFieldsetImageSyncDestController() {
  const vm = this;

  vm.addressModes = ['http', 'https'];
  vm.addRow = addRow;
  vm.deleteRow = deleteRow;
  vm.$onInit = onInit;
  let idCounter = 0;
  //////////
  function addRow() {
    vm.model.push({
      _id: idCounter++,
      dest_type: IMAGE_SYNC_DEST_TYPE.EXTERNAL,
    });
  }

  function onInit() {
    vm.model.forEach(row => {
      row._id = idCounter++;
      row.dest_type = IMAGE_SYNC_DEST_TYPE.EXTERNAL;
    });
  }

  function deleteRow(index) {
    vm.model.splice(index, 1);
  }
}
