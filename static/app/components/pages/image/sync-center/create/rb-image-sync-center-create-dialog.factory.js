angular
  .module('app.components.pages')
  .factory('rbImageSyncCenterCreateDialog', rbImageSyncCenterCreateDialog);

function rbImageSyncCenterCreateDialog(rbModal, translateService) {
  return {
    // Show a create snapshot dialog and returns snapshot id when success
    show: ({
      config,
      registry,
      project,
      repository,
      disableRepoSelection,
    } = {}) => {
      return rbModal
        .show({
          width: 900,
          title: config
            ? translateService.get('update')
            : translateService.get('image_sync_center_create'),
          locals: {
            config,
            registry,
            project,
            repository,
            disableRepoSelection,
          },
          component: 'rbImageSyncCenterCreate',
        })
        .then(res => res)
        .catch(() => {});
    },
  };
}
