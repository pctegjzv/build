import * as _ from 'lodash';
import {
  IMAGE_SYNC_DEST_TYPE,
  IMAGE_SYNC_DEFAULT_CONTAINER_SIZE,
} from '../../rb-image-sync.constant';

const templateStr = require('app/components/pages/image/sync-center/create/rb-image-sync-center-create.component.html');
angular.module('app.components.pages').component('rbImageSyncCenterCreate', {
  controller: rbImageSyncCenterCreateController,
  controllerAs: 'vm',
  template: templateStr,
  bindings: {
    config: '<',
    registry: '<',
    project: '<',
    repository: '<',
    disableRepoSelection: '<',
  },
});

function rbImageSyncCenterCreateController(
  $scope,
  rbModal,
  $log,
  rbImageSyncDataService,
  rbQuotaSpaceDataService,
  WEBLABS,
  rbSafeApply,
  rbRegistryDataService,
) {
  const vm = this;

  vm.externalRegistries = [];
  vm.onInternalRegistryAdd = onInternalRegistryAdd;
  vm.onInternalRegistryRemove = onInternalRegistryRemove;
  vm.showExternalRegistry = showExternalRegistry;
  vm.cancel = () => rbModal.cancel();
  vm.hide = res => rbModal.hide(res);
  vm.confirm = confirm;
  vm.onRepositoryChanged = onRepositoryChanged;
  vm.onSpaceChanged = onSpaceChanged;
  vm.onInstanceSizeChange = onInstanceSizeChange;
  vm.formValid = formValid;
  vm.$onInit = onInit;

  $scope.$watchCollection('vm.externalRegistries', newValue => {
    vm.model.dest = vm.model.dest
      .filter(item => item.dest_type === IMAGE_SYNC_DEST_TYPE.INTERNAL)
      .concat(newValue);
  });

  ////////
  function formValid() {
    return (
      _.get(vm, 'model.source.info.repository_name') &&
      vm.model.dest.length > 0 &&
      vm.form.$valid
    );
  }

  async function onInit() {
    if (vm.config) {
      vm.model = _.cloneDeep(vm.config);
      vm.externalRegistries = vm.model.dest.filter(
        item => item.dest_type === IMAGE_SYNC_DEST_TYPE.EXTERNAL,
      );
      vm.initInternalRegistries = vm.model.dest
        .filter(item => item.dest_type === IMAGE_SYNC_DEST_TYPE.INTERNAL)
        .map(item => item.internal_id);
      vm.isUpdate = true;
    } else {
      vm.model = {
        source: {
          info: {
            registry_name: vm.registry,
            project_name: vm.project,
            repository_name: vm.repository,
          },
        },
        dest: [],
      };
    }

    const { CPU, MEMORY } = IMAGE_SYNC_DEFAULT_CONTAINER_SIZE;
    if (
      (vm.model.cpu && vm.model.cpu !== CPU) ||
      (vm.model.memory && vm.model.memory !== MEMORY)
    ) {
      vm.containerSizeCustomized = true;
      vm.instanceSizeInit = {
        cpu: vm.model.cpu,
        memory: vm.model.memory,
      };
    } else {
      vm.instanceSizeInit = {
        cpu: CPU,
        memory: MEMORY,
      };
    }

    rbRegistryDataService.getRegistries().then(registries => {
      vm.ownRegistries = registries.map(item => ({
        display_name: item.display_name,
        internal_id: item.uuid,
      }));
      vm.registriesLoaded = true;

      vm.model.dest = vm.model.dest.filter(
        destRegistry =>
          destRegistry.dest_type !== 'INTERNAL_REGISTRY' ||
          vm.ownRegistries.some(
            ownRegistry => ownRegistry.uuid === destRegistry.internal_id,
          ),
      );
    });

    vm.repositoryInitObj = {
      get registry() {
        return _.get(vm, 'model.source.info.registry_name');
      },
      get project() {
        return _.get(vm, 'model.source.info.project_name');
      },
      get repository() {
        return _.get(vm, 'model.source.info.repository_name');
      },
    };
    vm.quotaEnabled = WEBLABS['quota_enabled'];
    if (vm.quotaEnabled) {
      vm.spaces = await rbQuotaSpaceDataService
        .getConsumableSpaces()
        .catch(() => []);
    }
    vm.initialized = true;
    rbSafeApply();
  }

  function onSpaceChanged(space) {
    vm.model.space_name = space.name;
  }

  async function confirm() {
    vm.form.$setSubmitted();
    if (!vm.formValid()) {
      return;
    }

    vm.submitting = true;
    try {
      const { CPU, MEMORY } = IMAGE_SYNC_DEFAULT_CONTAINER_SIZE;
      if (!vm.containerSizeCustomized) {
        vm.model.cpu = CPU;
        vm.model.memory = MEMORY;
      }

      if (vm.isUpdate) {
        await rbImageSyncDataService.updateConfig(vm.model);
        vm.hide(vm.model);
      } else {
        const config = await rbImageSyncDataService.createConfig(vm.model);
        vm.hide(Object.assign(vm.model, config));
      }
    } catch (err) {
      $log.error(
        'Failed to create sync center config: ' + JSON.stringify(err.data),
      );
    }
    vm.submitting = false;
    rbSafeApply();
  }

  function onRepositoryChanged(value) {
    if (value && value.name) {
      vm.model.source = {
        type: 'repository', // hard-coded for now
        info: {
          registry_name: value.registry.name,
          project_name: value.project && value.project.project_name,
          repository_name: value.name,
        },
      };
    }
  }

  function onInternalRegistryAdd(option) {
    if (!vm.model.dest.some(item => item.internal_id === option.internal_id)) {
      vm.model.dest.push({
        dest_type: IMAGE_SYNC_DEST_TYPE.INTERNAL,
        internal_id: option.internal_id,
      });
    }
  }

  function onInternalRegistryRemove(option) {
    const index = vm.model.dest.findIndex(
      item => item.internal_id === option.internal_id,
    );
    vm.model.dest.splice(index, 1);
  }

  function onInstanceSizeChange({ cpu, memory }) {
    vm.model.memory = memory;
    vm.model.cpu = cpu;
  }

  function showExternalRegistry() {
    vm.externalRegistries.push({});
  }
}
