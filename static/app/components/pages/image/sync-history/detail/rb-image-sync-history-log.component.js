const templateStr = require('app/components/pages/image/sync-history/detail/rb-image-sync-history-log.html');
angular.module('app.components.pages').component('rbImageSyncHistoryLog', {
  controller: rbImageSyncHistoryLogController,
  controllerAs: 'vm',
  bindings: {
    history: '<',
  },
  template: templateStr,
});

function rbImageSyncHistoryLogController(
  $element,
  $filter,
  $timeout,
  rbImageSyncDataService,
) {
  const vm = this;
  const interval = 10000;
  let timer;
  let componentDestroyed = false;
  let $ul;
  vm.loading = false;

  vm.$onInit = _init;
  vm.$onDestroy = _onDestroy;
  vm.$postLink = _postLink;
  vm.clearLogs = clearLogs;
  ///////////

  async function _init() {
    vm.waiting = false;
    await startPolling();
  }

  function _onDestroy() {
    componentDestroyed = true;
    $timeout.cancel(timer);
  }

  function _postLink() {
    $ul = $element.find('.rb-log-list');
  }

  async function startPolling() {
    await _polling();
  }

  function _nextTick() {
    if (componentDestroyed) {
      return;
    }
    timer = $timeout(_polling, interval);
  }

  function _renderLogs(logs = []) {
    let scroll = false;
    if (!vm.lastLogTime) {
      // this is the first time render, should scroll to bottom automaticlly
      scroll = true;
    } else {
      // else if the scrollbar is not at the bottom, we should keep it
      scroll = $ul.innerHeight() + $ul.scrollTop() + 5 >= $ul[0].scrollHeight;
    }
    const fragment = _createLogsFragment(logs);
    $ul.append(fragment);
    $timeout(() => {
      if (scroll) {
        $ul.scrollTop($ul[0].scrollHeight);
      }
    }, 100);
  }

  function _createLogsFragment(logs) {
    // instance_id, instance_id_full, message, time
    const fragment = document.createDocumentFragment();
    logs.forEach(log => {
      const li = document.createElement('li');
      li.className = 'rb-log-item rb-real-log-item';
      li.innerHTML = `<span>${$filter('formatDate')(
        log.time * 1000,
        true,
      )}</span>
        <span class="log-message">${_.escape(log.message)}</span>`;
      fragment.appendChild(li);
    });
    return fragment;
  }

  function _getParams() {
    const params = {
      history_id: vm.history.history_id,
    };
    if (vm.lastLogTime) {
      params['start_time'] = vm.lastLogTime + 1;
    } else {
      params['start_time'] = convertToTimestamp(vm.history.created_at) - 1800;
    }
    if (vm.history.finished_at) {
      params['end_time'] = convertToTimestamp(vm.history.finished_at) + 3600;
    } else {
      params['end_time'] = params['start_time'] + 3600 * 10;
    }
    return params;
  }

  async function _polling() {
    const status = vm.history.status;
    if (['W', 'B'].includes(status)) {
      // waiting, wait for next tick
      vm.waiting = true;
      _nextTick();
    } else if (status === 'I') {
      // inprogress, continue
      vm.waiting = false;
      await _pollLogs();
      _nextTick();
    } else {
      // finished, poll once
      vm.waiting = false;
      await _pollLogs();
    }
  }

  async function _pollLogs() {
    const params = _getParams();
    vm.loading = true;
    const limit = 2000;
    let logsFraction = [];
    let logs = [];
    let retry = false; // 如果为false, 则允许一次retry机会， 不然就直接调用error.
    do {
      try {
        const res = await rbImageSyncDataService.getHistoryLogs(params);
        retry = false;
        logsFraction = res.logs;
        // Newly fetched logs should be at the front
        logs = _.concat(logsFraction, logs);
        // Calculate the params for the next round
        if (logsFraction.length >= limit) {
          const previousEndTime = params.end_time;
          // There may be duplicates since the 'time' for logs is not with enough precision
          // we may need to fix it (remove the duplicates)
          params.end_time = logsFraction[0].time + 1;
          if (
            previousEndTime === params.end_time ||
            params.start_time >= params.end_time
          ) {
            break;
          }
        }
      } catch (e) {
        if (retry) {
          break;
        } else {
          retry = true; // retry one more time
        }
      }
    } while ((logsFraction.length >= limit || retry) && !componentDestroyed);
    vm.loading = false;
    if (logs.length) {
      vm.lastLogTime = logs[logs.length - 1].time;
      _renderLogs(logs);
    }
  }

  function convertToTimestamp(dateStr) {
    const DATE_REG = /^(\d{1,4})-(\d{1,2})-(\d{1,2})T(\d{1,2}):(\d{1,2}):(\d{1,2})\.(.+)$/;
    const result = dateStr.match(DATE_REG);
    const dt = new Date(
      Date.UTC(
        result[1],
        result[2] - 1,
        result[3],
        result[4],
        result[5],
        result[6],
      ),
    );
    return dt.getTime() / 1000;
  }

  function clearLogs() {
    $ul.find('.rb-real-log-item').remove();
  }
}
