const templateStr = require('app/components/pages/job/config/create/rb-job-config-create.component.html');
angular.module('app.components.pages').component('rbJobConfigCreate', {
  bindings: {},
  controller: rbJobConfigCreateController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbJobConfigCreateController(
  $element,
  $state,
  translateService,
  rbConfirmBox,
  rbSafeApply,
  rbRouterStateHelper,
  rbJobDataService,
) {
  const vm = this;

  // Set default values:
  vm.maxStep = 3;
  vm.configStep = 1; // Config step, range from 1 -> 3
  vm.basicModel = {}; // Model for Basic setting
  vm.advancedModel = {}; // Model for Advanced Setting

  vm.imageInfo = undefined; // The image info captured in step 1.

  vm.onImageSelected = onImageSelected;
  vm.reselectImage = reselectImage;
  vm.prevStep = prevStep;
  vm.nextStep = nextStep;
  vm.create = create;
  vm.cancel = cancel;

  vm.canChangeStep = canChangeStep;

  vm.$onInit = onInit;
  //////////
  function onInit() {}

  function onImageSelected({
    registry_index,
    image_name,
    registry_name,
    project_name,
  }) {
    vm.imageInfo = {
      registry_index,
      image_name,
      registry_name,
      project_name,
      registry_project: project_name,
    };

    // Navigate to step 2 after selection
    vm.configStep = 2;
  }

  function reselectImage() {
    vm.configStep = 1;
  }

  function prevStep() {
    _toStep(Math.min(vm.maxStep, vm.configStep - 1));
  }

  function nextStep() {
    _toStep(Math.min(vm.maxStep, vm.configStep + 1));
  }

  function _toStep(step = vm.configStep) {
    // Submit the current form and checks the validity
    const childVm = _getCurrentChildViewModel();
    const childModel = childVm.submitForm();

    if (vm.configStep === 2) {
      vm.basicModel = childModel;
    } else if (vm.configStep === 3) {
      vm.advancedModel = childModel;
    }

    if (childVm.isModelValid()) {
      vm.configStep = step;
    }
  }

  async function create() {
    // Submit the current form to
    // - toggle form validation checks
    // - make sure models are transferred from the embedded forms.
    _toStep();
    if (canChangeStep()) {
      try {
        await rbConfirmBox.show({
          title: translateService.get('create'),
          textContent: translateService.get('job_create_config_hint', {
            name: vm.basicModel.config_name,
          }),
        });

        const model = {
          registry_index: vm.imageInfo.registry_index,
          registry_name: vm.imageInfo.registry_name,
          image_name: vm.imageInfo.image_name,
          registry_project: vm.imageInfo.project_name,
          ...vm.basicModel,
          ...vm.advancedModel,
        };

        vm.submitting = true;
        const res = await rbJobDataService.createConfig(model);
        $state.go('job.config.detail', { name: res.config_uuid });
      } catch (e) {
        // ???
      }
      vm.submitting = false;
    }
    rbSafeApply();
  }

  function cancel() {
    rbRouterStateHelper.back();
  }

  // Cache the vm for efficiency
  const childViewModelMap = {
    // Undefined will be used by the first step (image selection)
    undefined: {
      isModelValid: () => true,
      submitForm: () => ({}),
      isSubmitted: () => true,
    },
  };

  function _getChildViewModel(component) {
    if (!childViewModelMap[component]) {
      try {
        childViewModelMap[component] = $element
          .find(component)
          .isolateScope().vm;
      } catch (e) {
        // means the child vm is still unavailable
      }
    }
    return childViewModelMap[component];
  }

  function _getCurrentChildViewModel() {
    let component;
    if (vm.configStep === 2) {
      component = 'rb-job-config-form-basic';
    } else if (vm.configStep === 3) {
      component = 'rb-job-config-form-advanced';
    }

    return _getChildViewModel(component);
  }

  function canChangeStep() {
    const childVm = _getCurrentChildViewModel();
    return childVm && (!childVm.isSubmitted() || childVm.isModelValid());
  }
}
