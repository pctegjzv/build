import { filter, first, map } from 'rxjs/operators';

import { resource_types as RESOURCE_TYPES } from '../../../rbac_role/rb-rbac-helper.constant';
import { RUN_COMMAND_TYPES } from '../../rb-job.constants';

import templateStr from './rb-job-config-detail.component.html';

angular.module('app.components.pages').component('rbJobConfigDetail', {
  bindings: {
    name: '<',
  },
  controller: rbJobConfigDetailController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbJobConfigDetailController(
  WEBLABS,
  $state,
  $log,
  rbToast,
  formatUtcStrFilter,
  translateService,
  rbRegionDataStore,
  rbCrontabNextFilter,
  rbConfirmBox,
  rbJobUtility,
  rbRouterStateHelper,
  rbEnvfileDataStore,
  rbJobDataService,
  rbRoleUtilities,
  rbSafeApply,
) {
  const vm = this;
  vm.buttonDisplayExpr = buttonDisplayExpr;

  vm.startClicked = startClicked;
  vm.deleteClicked = deleteClicked;

  vm.$onInit = onInit;
  vm.$onDestroy = onDestroy;
  //////////
  async function onInit() {
    vm.weblabs = WEBLABS;
    vm.basicInfoFields = await constructBasicInfoFields({});

    try {
      await refetchConfig();
    } catch (e) {
      $log.log('Failed to load job config ' + vm.name);
      $log.log(e);
      rbToast.error(translateService.get('job_config_not_exist'));
      // Fetch error, go back to previous page.
      !vm.destroyed && rbRouterStateHelper.back();
    }
    vm.initialized = true;
  }

  function onDestroy() {
    vm.destroyed = true;
  }

  function buttonDisplayExpr(action) {
    return rbRoleUtilities.resourceHasPermission(
      vm.config,
      RESOURCE_TYPES.JOB_CONFIG,
      action,
    );
  }

  async function refetchConfig() {
    vm.config = await rbJobDataService.getConfigByName(vm.name);
    vm.basicInfoFields = await constructBasicInfoFields(vm.config);

    const envfileUUIDs = vm.config.envfiles.map(envfile => envfile.uuid);
    vm.envfiles$ = rbEnvfileDataStore.data$.pipe(
      filter(envfiles => !!envfiles),
      map(envfiles =>
        envfiles.filter(envfile => envfileUUIDs.includes(envfile.uuid)),
      ),
    );

    rbSafeApply();
  }

  async function startClicked() {
    vm.starting = true;
    try {
      const configName = vm.config.config_uuid;
      const displayName = vm.config.config_name;
      const { job_uuid } = await rbJobDataService.start(
        configName,
        displayName,
      );
      $state.go('job.history.detail', { uuid: job_uuid });
    } catch (e) {
      // ...?
    }
    vm.starting = false;
    rbSafeApply();
  }

  async function deleteClicked() {
    try {
      await rbConfirmBox.show({
        title: translateService.get('delete'),
        textContent: translateService.get('job_delete_config_hint', {
          name: vm.config.config_name,
        }),
      });
      vm.deleting = true;
      rbSafeApply();
      await rbJobDataService.deleteConfig({
        name: vm.config.config_name,
        uuid: vm.config.config_uuid,
      });
      !vm.destroyed && $state.go('job.config.list');
    } catch (e) {
      // ????
    }
    vm.deleting = false;
    rbSafeApply();
  }

  async function constructBasicInfoFields(config = {}) {
    function getScheduleRuleFieldValue(scheduleRule) {
      const nextTriggerTimeString = translateService.get(
        'build_config_next_trigger_time',
      );
      const cronNextString = rbCrontabNextFilter(scheduleRule);
      return `${scheduleRule} (${nextTriggerTimeString}: ${cronNextString})`;
    }

    function getContainerSizeFieldString(cpu, memory) {
      if (cpu && memory) {
        return `${cpu}${translateService.get(
          'build_config_cores',
        )} / ${memory}MB `;
      } else {
        return '';
      }
    }

    const region = config.region_id
      ? await rbRegionDataStore.data$
          .pipe(
            filter(data => !!data),
            map(regions =>
              regions.find(region => region.id === config.region_id),
            ),
            first(),
          )
          .toPromise()
      : undefined;

    const fullImageName = rbJobUtility.getFullImageName(config);

    return [
      {
        name: 'cluster',
        value: region ? region.display_name : '-',
      },
      {
        name: 'build_config_container_size',
        value: getContainerSizeFieldString(config.cpu, config.memory),
      },
      {
        name: 'created_by',
        value: config.created_by,
      },
      {
        name: 'created_at',
        value: formatUtcStrFilter(config.created_at),
      },
      {
        name: 'updated_at',
        value: formatUtcStrFilter(config.updated_at),
      },
      {
        name: 'timeout',
        value: config.timeout
          ? config.timeout + ' ' + translateService.get('minutes')
          : '',
      },
      {
        name: 'schedule_rule',
        value: config.schedule_rule
          ? getScheduleRuleFieldValue(config.schedule_rule)
          : '',
      },
      {
        name: 'image',
        value: fullImageName ? `${fullImageName}:${config.image_tag}` : '',
      },
      {
        name: 'cmd',
        value:
          config.command_type === RUN_COMMAND_TYPES.CUSTOM
            ? config.command
            : translateService.get('use_default'),
      },
      {
        name: 'quota_space',
        value: config.space_name,
        state: `quota.detail.${config.space_name}`,
      },
    ];
  }
}
