import { resource_types as RESOURCE_TYPES } from '../../../rbac_role/rb-rbac-helper.constant';

import template from './rb-job-config-list.component.html';

angular.module('app.components.pages').component('rbJobConfigList', {
  bindings: {},
  controller: rbJobConfigListController,
  controllerAs: 'vm',
  template,
});

function rbJobConfigListController(
  rbRoleUtilities,
  translateService,
  rbJobDataService,
  rbJobUtility,
  rbConfirmBox,
  rbSafeApply,
  rbDelay,
  WEBLABS,
  $state,
) {
  const vm = this;
  const POLL_INTERVAL = 10 * 1000; // 10 seconds

  vm.searching = '';
  vm.pagination = {
    pageSize: 20,
    pageno: 1,
    totalItems: 0,
  };

  vm.deletingMap = {};
  vm.startingMap = {};

  vm.onSearchChanged = onSearchChanged;
  vm.pageNoChange = pageNoChange;

  vm.buttonDisplayExpr = buttonDisplayExpr;
  vm.startClicked = startClicked;
  vm.deleteClicked = deleteClicked;
  vm.getImageAddress = getImageAddress;

  vm.$onInit = onInit;
  vm.$onDestroy = onDestroy;

  //////////
  async function onInit() {
    vm.quotaEnabled = WEBLABS.QUOTA_ENABLED;
    vm.jobConfigCreateEnabled = await rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.JOB_CONFIG,
    );

    _startPollTimer();
  }

  function onDestroy() {
    vm.destroyed = true;
  }

  // Simply poll data every POLL_INTERVAL seconds
  async function _startPollTimer() {
    while (!vm.destroyed) {
      vm.pageNoChange();
      await rbDelay(POLL_INTERVAL);
    }
  }

  let _lastSearchCounter = 0;

  async function onSearchChanged(search) {
    const searchCounter = ++_lastSearchCounter;
    vm.search = search;
    vm.searching = true;
    await pageNoChange(1);
    if (searchCounter === _lastSearchCounter) {
      vm.searching = false;
    }
    rbSafeApply();
  }

  async function pageNoChange(
    page = vm.pagination.pageno,
    size = vm.pagination.pageSize,
  ) {
    const pageNoChanged = vm.pagination.pageno !== page;
    vm.pagination.pageno = page;
    vm.pagination.pageSize = size;
    vm.loading = true;
    try {
      const { count, results } = await _refetchData({
        pageno: vm.pagination.pageno,
        page_size: vm.pagination.pageSize,
        search: vm.search,
      });

      vm.pagination.totalItems = count;
      vm.list = results;
      vm.loadError = false;
    } catch (e) {
      if (pageNoChanged) {
        vm.list = [];
      }
      vm.loadError = true;
    }
    vm.loading = false;
    rbSafeApply();
  }

  function _refetchData({ pageno, page_size, search }) {
    return rbJobDataService.getConfigs({ pageno, page_size, search });
  }

  function buttonDisplayExpr(item, action) {
    return rbRoleUtilities.resourceHasPermission(
      item,
      RESOURCE_TYPES.JOB_CONFIG,
      action,
    );
  }

  async function startClicked(item) {
    vm.startingMap[item.config_uuid] = true;
    try {
      const configName = item.config_uuid;
      const displayName = item.config_name;
      const { job_uuid } = await rbJobDataService.start(
        configName,
        displayName,
      );
      $state.go('job.history.detail', { uuid: job_uuid });
    } catch (e) {
      //
    }
    vm.startingMap[item.config_uuid] = false;
    rbSafeApply();
  }

  async function deleteClicked(item) {
    try {
      await rbConfirmBox.show({
        title: translateService.get('delete'),
        textContent: translateService.get('job_delete_config_hint', {
          name: item.config_name,
        }),
      });
      vm.deletingMap[item.config_uuid] = true;
      rbSafeApply();
      await rbJobDataService.deleteConfig({
        name: item.config_name,
        uuid: item.config_uuid,
      });
      pageNoChange();
    } catch (e) {
      // ????
    }

    vm.deletingMap[item.config_uuid] = false;
  }

  function getImageAddress(item) {
    return rbJobUtility.getFullImageName(item) + ':' + item.image_tag;
  }
}
