const templateStr = require('app/components/pages/job/config/mutate-forms/advanced/rb-job-config-form-advanced.component.html');
angular.module('app.components.pages').component('rbJobConfigFormAdvanced', {
  bindings: {
    /**
     * Initial model.
     */
    init: '<',

    /**
     * type = 'UPDATE' | 'CREATE'
     */
    mode: '<',
  },
  controller: rbJobConfigFormAdvancedController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbJobConfigFormAdvancedController($scope, rbEnvfileDataStore) {
  const vm = this;

  vm.scheduleRuleEnabled = false;

  // The following will be called from parent
  vm.isModelValid = isModelValid;
  vm.submitForm = submitForm;
  vm.isSubmitted = isSubmitted;

  ////
  vm.envfileAdded = envfileAdded;
  vm.envfileRemoved = envfileRemoved;

  vm.$onInit = onInit;
  vm.$onChanges = onChanges;
  //////////
  function onInit() {
    /**
     * Model for advanced:
     *
     * timeout
     * schedule_rule
     * envfiles
     * envvars
     */
    vm.model = {
      envfiles: [],
      envvars: [],
      schedule_rule: '0 8 * * *',
    };

    vm.model = Object.assign(vm.model, vm.init);
    vm.envfiles$ = rbEnvfileDataStore.data$;

    vm.isUpdate = vm.mode === 'UPDATE';
    vm.initialized = (vm.isUpdate && vm.init) || !vm.isUpdate;
  }

  function onChanges({ init }) {
    if (init && init.currentValue && !vm.initialized) {
      vm.model = Object.assign(vm.model, vm.init);
      vm.scheduleRuleEnabled = !!vm.model.schedule_rule;
      vm.initialized = true;

      vm.initEnvfiles = vm.model.envfiles.map(envfile => envfile.name);
    }
  }

  function isModelValid() {
    return $scope.form.$valid;
  }

  function submitForm() {
    const model = { ...vm.model };

    // Filter out empty env vars.
    model.envvars = model.envvars.filter(({ name }) => !!name);

    if (!vm.scheduleRuleEnabled) {
      model.schedule_rule = undefined;
    }

    $scope.form.$setSubmitted();
    // Clone and return
    return model;
  }

  function isSubmitted() {
    return $scope.form.$submitted;
  }

  function envfileAdded(envfile) {
    if (
      vm.model.envfiles.some(item => {
        return item.uuid === envfile.uuid;
      })
    ) {
      return;
    }
    vm.model.envfiles.push({
      name: envfile.name,
      uuid: envfile.uuid,
    });
  }

  function envfileRemoved(envfile) {
    vm.model.envfiles = vm.model.envfiles.filter(
      _file => envfile.name !== _file.name,
    );
  }
}
