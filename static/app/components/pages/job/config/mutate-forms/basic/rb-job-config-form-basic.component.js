import { RUN_COMMAND_TYPES } from '../../../rb-job.constants';
const templateStr = require('app/components/pages/job/config/mutate-forms/basic/rb-job-config-form-basic.component.html');
angular.module('app.components.pages').component('rbJobConfigFormBasic', {
  bindings: {
    /**
     * { image_name, registry_name, project_name, registry_index }
     * refer to: adaptImageInfoFromImageSelect
     */
    imageInfo: '<',

    /**
     * Initial model.
     */
    init: '<',

    /**
     * type = 'UPDATE' | 'CREATE'
     */
    mode: '<',
  },
  controller: rbJobConfigFormBasicController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbJobConfigFormBasicController(
  $scope,
  WEBLABS,
  rbSafeApply,
  rbToast,
  translateService,
  rbQuotaSpaceDataService,
  rbRegionDataStore,
  rbRepositoryDataService,
) {
  const vm = this;

  vm.onSpaceChanged = onSpaceChanged;
  vm.onRegionChange = onRegionChange;
  vm.onInstanceSizeChange = onInstanceSizeChange;
  vm.onImageTagChange = onImageTagChange;
  vm.onRunCommandTypeChange = onRunCommandTypeChange;

  vm.isCustomTypeSelected = isCustomTypeSelected;

  // The following will be called from parent
  vm.isModelValid = isModelValid;
  vm.submitForm = submitForm;
  vm.isSubmitted = isSubmitted;

  vm.$onInit = onInit;
  vm.$onChanges = onChanges;
  vm.$onDestroy = onDestroy;
  //////////
  function onInit() {
    /**
     * Model for basic fields.
     *
     * List of fields:
     *
     * config_name
     *
     * space_name
     * space_uuid
     *
     * image_tag
     *
     * region_name
     * region_uuid
     *
     * command_type
     * command
     *
     * cpu
     * memory
     */
    vm.model = {};

    vm.model.cpu = 0.5;
    vm.model.memory = 512;

    vm.quotaEnabled = WEBLABS['quota_enabled'];
    vm.regions$ = rbRegionDataStore.allNonEmpty$;

    if (vm.quotaEnabled) {
      rbQuotaSpaceDataService.getConsumableSpaces().then(spaces => {
        vm.spaces = spaces;
        vm.loadingSpaces = false;
      });
    }

    vm.runCommandTypes = [
      {
        name: translateService.get('use_default'),
        value: RUN_COMMAND_TYPES.DEFAULT,
      },
      {
        name: translateService.get('use_custom'),
        value: RUN_COMMAND_TYPES.CUSTOM,
      },
    ];

    vm.isUpdate = vm.mode === 'UPDATE';
    vm.initialized = (vm.isUpdate && vm.init) || !vm.isUpdate;
  }

  function onChanges({ imageInfo, init }) {
    // When imageInfo reference changes, re-fetch tags
    if (imageInfo && imageInfo.currentValue) {
      onImageChanged();
    }

    if (init && init.currentValue && !vm.initialized) {
      vm.model = Object.assign(vm.model, vm.init);
      vm.initialized = true;
    }
  }

  function onDestroy() {
    vm.destroyed = true;
  }

  async function onImageChanged() {
    vm.imageTagsLoading = true;
    vm.fullImageName =
      vm.imageInfo.registry_index +
      '/' +
      (vm.imageInfo.registry_project ? vm.imageInfo.project_name + '/' : '') +
      vm.imageInfo.image_name;
    try {
      vm.imageTags = await getImageTags();
    } catch (e) {
      // ???
      vm.imageTags = [];
    }
    vm.imageTagsLoading = false;

    if (vm.imageTags.length === 0 && !vm.destroyed) {
      rbToast.warning(translateService.get('get_image_tags_failed'));
    }

    rbSafeApply();
  }

  // Copied from rbServiceCreateBasicInfo
  function getImageTags() {
    if (vm.imageInfo.registry_name) {
      const repoName = vm.imageInfo.image_name.includes('/')
        ? vm.imageInfo.image_name.split('/')[1]
        : vm.imageInfo.image_name;

      return rbRepositoryDataService.getRepositoryTags({
        registry_name: vm.imageInfo.registry_name,
        project_name: vm.imageInfo.project_name,
        repo_name: repoName,
      });
    } else {
      return rbRepositoryDataService.getPublicRepositoryTags({
        image_name: vm.imageInfo.registry_index + '/' + vm.imageInfo.image_name,
      });
    }
  }

  function onInstanceSizeChange({ cpu, memory }) {
    vm.model.memory = memory;
    vm.model.cpu = cpu;
  }

  function onSpaceChanged(space) {
    vm.model.space_name = space.name;
    vm.model.space_uuid = space.uuid;
  }

  function onRegionChange(region) {
    vm.model.region_name = region.name;
    vm.model.region_uuid = region.id;
  }

  function onImageTagChange(option) {
    vm.model.image_tag = option.value;
  }

  function onRunCommandTypeChange(option) {
    vm.model.command_type = option.value;
  }

  function isCustomTypeSelected() {
    return vm.model.command_type === RUN_COMMAND_TYPES.CUSTOM;
  }

  function isModelValid() {
    return $scope.form.$valid;
  }

  function submitForm() {
    $scope.form.$setSubmitted();
    // Clone and return
    return { ...vm.model };
  }

  function isSubmitted() {
    return $scope.form.$submitted;
  }
}
