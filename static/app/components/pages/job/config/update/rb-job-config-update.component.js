const templateStr = require('app/components/pages/job/config/update/rb-job-config-update.component.html');
angular.module('app.components.pages').component('rbJobConfigUpdate', {
  bindings: {
    name: '<',
  },
  controller: rbJobConfigUpdateController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbJobConfigUpdateController(
  $element,
  $state,
  translateService,
  rbToast,
  WEBLABS,
  rbConfirmBox,
  rbRegionDataStore,
  rbJobDataService,
  rbSafeApply,
) {
  const vm = this;
  let config;

  vm.shouldDisableSubmit = shouldDisableSubmit;
  vm.update = update;
  vm.cancel = cancel;

  vm.$onInit = onInit;
  vm.$onDestroy = onDestroy;
  //////////
  async function onInit() {
    vm.weblabs = WEBLABS;
    vm.regions$ = rbRegionDataStore.allNonEmpty$;

    vm.loading = true;

    try {
      await refetchConfig();
    } catch (e) {
      $state.go('^.list');
      rbToast.error(translateService.get('job_config_not_exist'));
      return;
    }

    vm.loading = false;
    rbSafeApply();
  }

  function onDestroy() {
    vm.destroyed = true;
  }

  async function refetchConfig() {
    const BASIC_FIELDS = [
      'config_name',
      'image_tag',
      'image_name',
      'region_name',
      'region_uuid',
      'command_type',
      'command',
      'cpu',
      'memory',
    ];

    const ADVANCED_FIELDS = ['timeout', 'schedule_rule', 'envfiles', 'envvars'];

    config = await rbJobDataService.getConfigByName(vm.name);

    // Pick the model values from config, and pass them into the sub-forms.
    vm.basicModelInit = BASIC_FIELDS.reduce(
      (model, key) => ({ [key]: config[key], ...model }),
      {},
    );
    vm.advancedModelInit = ADVANCED_FIELDS.reduce(
      (model, key) => ({ [key]: config[key], ...model }),
      {},
    );

    vm.imageInfo = {
      image_name: config.image_name,
      registry_name: config.registry_name,
      project_name: config.registry_project,
      registry_index: config.registry_index,
    };

    rbSafeApply();
  }

  // Cache the vm for efficiency
  const childViewModelMap = {};

  function _getChildViewModel(component) {
    if (!childViewModelMap[component]) {
      try {
        childViewModelMap[component] = $element
          .find(component)
          .isolateScope().vm;
      } catch (e) {
        // means the child vm is still unavailable
      }
    }
    return childViewModelMap[component];
  }

  function _getBasicFormViewModel() {
    return _getChildViewModel('rb-job-config-form-basic');
  }

  function _getAdvancedFormViewModel() {
    return _getChildViewModel('rb-job-config-form-advanced');
  }

  function getAllForms() {
    return [_getBasicFormViewModel(), _getAdvancedFormViewModel()];
  }

  function shouldDisableSubmit() {
    const formVms = getAllForms();
    return (
      vm.loading ||
      vm.updating ||
      formVms.some(formVm => formVm.isSubmitted() && !formVm.isModelValid())
    );
  }

  async function update() {
    const formVms = getAllForms();
    const models = formVms.map(formVm => formVm.submitForm());
    if (!shouldDisableSubmit()) {
      try {
        await rbConfirmBox.show({
          title: translateService.get('update'),
          textContent: translateService.get('job_update_config_hint', {
            name: config.config_name,
          }),
        });
        vm.updating = true;
        const payload = models.reduce(
          (accum, model) => Object.assign(accum, model),
          {},
        );
        await rbJobDataService.updateConfig(config.config_uuid, payload);
        !vm.destroyed &&
          $state.go('job.config.detail', { name: config.config_uuid });
      } catch (e) {
        /// ?
      }
      vm.updating = false;
    }
    rbSafeApply();
  }

  function cancel() {
    $state.go('job.config.detail', { name: config.config_uuid });
  }
}
