import moment from 'moment';
import { filter, first, map } from 'rxjs/operators';

import {
  JOB_STATUS,
  JOB_STATUS_ICON_STATUS_MAP,
  RUN_COMMAND_TYPES,
} from '../../rb-job.constants';
import { resource_types as RESOURCE_TYPES } from '../../../rbac_role/rb-rbac-helper.constant';

import templateStr from './rb-job-history-detail.component.html';

angular.module('app.components.pages').component('rbJobHistoryDetail', {
  bindings: {
    uuid: '<',
  },
  controller: rbJobHistoryDetailController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbJobHistoryDetailController(
  $log,
  $state,
  translateService,
  rbDelay,
  rbToast,
  rbRegionDataStore,
  formatUtcStrFilter,
  rbDurationFilter,
  rbRouterStateHelper,
  rbSafeApply,
  rbConfirmBox,
  rbJobUtility,
  rbRoleUtilities,
  rbJobDataService,
  rbEnvfileDataStore,
) {
  const vm = this;

  vm.restartClicked = restartClicked;
  vm.deleteClicked = deleteClicked;
  vm.buttonDisplayExpr = buttonDisplayExpr;
  vm.canRestart = canRestart;

  vm.$onInit = onInit;
  vm.$onDestroy = onDestroy;
  //////////
  async function onInit() {
    vm.firstPoll = true;
    vm.basicInfoFields = await constructBasicInfoFields();
    vm.statusIconMap = JOB_STATUS_ICON_STATUS_MAP;
    await startPolling();
    rbSafeApply();
  }

  function onDestroy() {
    vm.destroyed = true;
  }

  async function startPolling() {
    const isFirstPoll = vm.firstPoll;
    vm.firstPoll = false;
    try {
      await refetchHistory();
    } catch (err) {
      $log.error('Failed to poll detail');
      if (isFirstPoll) {
        $log.log('Failed to load job config ' + vm.name);
        rbToast.error(translateService.get('job_history_not_exist'));
        // Fetch error, go back to previous page.
        !vm.destroyed && rbRouterStateHelper.back();
      }
    }

    await rbDelay(10000);
    if (_shouldPollHistory()) {
      startPolling();
    }
  }

  function _shouldPollHistory() {
    return (
      !vm.destroyed &&
      (!vm.history ||
        ![JOB_STATUS.STOPPED, JOB_STATUS.SUCCEEDED, JOB_STATUS.FAILED].includes(
          vm.history.status,
        ))
    );
  }

  async function refetchHistory() {
    vm.history = await rbJobDataService.getHistoryById(vm.uuid);

    if (!vm.config) {
      const config_name = vm.history.config_uuid;
      rbJobDataService
        .getConfigByName(config_name)
        .then(config => (vm.config = config));
    }

    const envfileUUIDs = vm.history.envfiles.map(envfile => envfile.uuid);
    vm.envfiles$ = rbEnvfileDataStore.data$.pipe(
      filter(envfiles => !!envfiles),
      map(envfiles =>
        envfiles.filter(envfile => envfileUUIDs.includes(envfile.uuid)),
      ),
    );

    vm.basicInfoFields = await constructBasicInfoFields(vm.history);

    rbSafeApply();
  }

  // TODO move to utils?
  function _calculateTimeCost(history) {
    if (history.started_at && history.ended_at) {
      const startedAt = moment(history.started_at);
      const endedAt = moment(history.ended_at);

      return endedAt - startedAt;
    } else {
      return -1;
    }
  }

  async function restartClicked() {
    vm.restarting = true;
    try {
      const configName = vm.history.config_uuid;
      const displayName = vm.history.config_name;
      const { job_uuid } = await rbJobDataService.start(
        configName,
        displayName,
      );
      !vm.destroyed && $state.go('job.history.detail', { uuid: job_uuid });
    } catch (e) {
      ///?
    }
    vm.restarting = false;
    rbSafeApply();
  }

  async function deleteClicked() {
    try {
      await rbConfirmBox.show({
        title: translateService.get('delete'),
        textContent: translateService.get('job_delete_history_hint', {
          uuid: vm.uuid,
        }),
      });
      vm.deleting = true;
      await rbJobDataService.deleteHistoryById(vm.uuid);
      !vm.destroyed && $state.go('job.history.list');
    } catch (e) {
      // ??
    }
    vm.deleting = false;
    rbSafeApply();
  }

  function buttonDisplayExpr(action) {
    return rbRoleUtilities.resourceHasPermission(
      vm.history,
      RESOURCE_TYPES.JOB_HISTORY,
      action,
    );
  }

  function canRestart() {
    return (
      !!vm.config &&
      !_shouldPollHistory() &&
      rbRoleUtilities.resourceHasPermission(
        vm.config,
        RESOURCE_TYPES.JOB_CONFIG,
        'trigger',
      )
    );
  }

  async function constructBasicInfoFields(history = {}) {
    const fullImageName = rbJobUtility.getFullImageName(history);

    const region = history.region_id
      ? await rbRegionDataStore.data$
          .pipe(
            filter(data => !!data),
            map(regions =>
              regions.find(region => region.id === history.region_id),
            ),
            first(),
          )
          .toPromise()
      : undefined;

    return [
      {
        name: 'cluster',
        value: region ? region.display_name : '-',
      },
      {
        name: 'created_by',
        value: getCreatorName(history),
      },
      {
        name: 'created_at',
        value: formatUtcStrFilter(history.created_at),
      },
      {
        name: 'started_at',
        value: formatUtcStrFilter(history.started_at),
      },
      {
        name: 'ended_time',
        value: formatUtcStrFilter(history.ended_at),
      },
      {
        name: 'time_cost',
        value: rbDurationFilter(_calculateTimeCost(history)),
      },
      {
        name: 'image',
        value: fullImageName ? fullImageName + ':' + history.image_tag : '',
      },
      {
        name: 'cmd',
        value:
          history.command && history.command_type === RUN_COMMAND_TYPES.CUSTOM
            ? history.command
            : translateService.get('use_default'),
      },
    ];
  }

  function getCreatorName(item) {
    return item.is_auto_scheduled
      ? translateService.get('auto_trigger')
      : item.created_by;
  }
}
