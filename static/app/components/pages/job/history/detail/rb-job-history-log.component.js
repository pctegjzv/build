angular.module('app.components.pages').component('rbJobHistoryLog', {
  controller: rbJobHistoryLogController,
  controllerAs: 'vm',
  bindings: {
    history: '<',
  },
  template: `
<ul class="rb-log-list">
  <li class="rb-log-item" ng-show="!vm.logs.length && vm.loading"><span class="log-message">{{ 'logs_poll_waiting' | translate }}</span></li>
  <li class="rb-log-item" ng-show="!vm.logs.length && !vm.loading && !vm.waiting"><span class="log-message">{{ 'logs_none' | translate }}</span></li>
  <li class="rb-log-item" ng-show="vm.waiting"><span class="log-message">{{ 'logs_server_busy' | translate }}</span></li>
</ul>
`,
});

function rbJobHistoryLogController(
  $element,
  $filter,
  $timeout,
  rbJobDataService,
) {
  const vm = this;
  const interval = 10000;
  let timer;
  let componentDestroyed = false;
  vm.logs = [];
  vm.loading = false;

  vm.$onInit = _init;
  vm.$onDestroy = _onDestroy;
  ///////////

  async function _init() {
    vm.waiting = false;
    await startPolling();
  }

  function _onDestroy() {
    componentDestroyed = true;
    $timeout.cancel(timer);
  }

  async function startPolling() {
    vm.logs = [];
    await _polling();
  }

  function _nextTick() {
    if (componentDestroyed) {
      return;
    }
    timer = $timeout(_polling, interval);
  }

  function _renderLogs(logs = []) {
    let scroll = false;
    const $ul = $element.find('.rb-log-list');
    if (vm.logs === 0) {
      // this is the first time render, should scroll to bottom automaticlly
      scroll = true;
    } else {
      // else if the scrollbar is not at the bottom, we should keep it
      scroll = $ul.innerHeight() + $ul.scrollTop() + 5 >= $ul[0].scrollHeight;
    }
    const fragment = _createLogsFragment(logs);
    $ul.append(fragment);
    $timeout(() => {
      if (scroll) {
        $ul.scrollTop($ul[0].scrollHeight);
      }
    }, 100);
  }

  function _createLogsFragment(logs) {
    // instance_id, instance_id_full, message, time
    const fragment = document.createDocumentFragment();
    logs.forEach(log => {
      const li = document.createElement('li');
      li.className = 'rb-log-item';
      li.innerHTML = `<span>${$filter('formatDate')(
        log.time * 1000,
        true,
      )}</span>
        <span class="log-message">${_.escape(log.message)}</span>`;
      fragment.appendChild(li);
    });
    return fragment;
  }

  function _getParams() {
    const params = {
      uuid: vm.history.job_uuid,
    };
    if (vm.logs.length) {
      params['start_time'] = vm.logs[vm.logs.length - 1].time + 1;
    } else {
      params['start_time'] = convertToTimestamp(vm.history.created_at) - 1800;
    }
    if (vm.history.ended_at) {
      params['end_time'] = convertToTimestamp(vm.history.ended_at) + 3600;
    } else {
      params['end_time'] = params['start_time'] + 3600 * 10;
    }
    return params;
  }

  async function _polling() {
    const status = vm.history.status;
    if (['WAITING', 'BLOCKED'].includes(status)) {
      // waiting, wait for next tick
      vm.waiting = true;
      _nextTick();
    } else if (status === 'RUNNING') {
      // inprogress, continue
      vm.waiting = false;
      await _pollLogs();
      _nextTick();
    } else {
      // finished, poll once
      vm.waiting = false;
      await _pollLogs();
    }
  }

  async function _pollLogs() {
    const params = _getParams();
    vm.loading = true;
    const limit = 2000;
    let logsFraction = [];
    let logs = [];
    let retry = false; // 如果为false, 则允许一次retry机会, 不然就直接调用error.
    do {
      try {
        const res = await rbJobDataService.getHistoryLogs(params);
        retry = false;
        logsFraction = res.result;
        // Newly fetched logs should be at the front
        logs = _.concat(logsFraction, logs);
        // Calculate the params for the next round
        if (logsFraction.length >= limit) {
          const previousEndTime = params.end_time;
          // There may be duplicates since the 'time' for logs is not with enough precision
          // we may need to fix it (remove the duplicates)
          params.end_time = logsFraction[0].time + 1;
          if (
            previousEndTime === params.end_time ||
            params.start_time >= params.end_time
          ) {
            break;
          }
        }
      } catch (e) {
        if (retry) {
          this.error(e);
          break;
        } else {
          retry = true; // retry one more time
        }
      }
    } while ((logsFraction.length >= limit || retry) && !componentDestroyed);
    vm.loading = false;
    vm.logs = vm.logs.concat(logs);
    logs.length && _renderLogs(logs);
  }

  function convertToTimestamp(dateStr) {
    const DATE_REG = /^(\d{1,4})-(\d{1,2})-(\d{1,2})T(\d{1,2}):(\d{1,2}):(\d{1,2})\.(.+)$/;
    const result = dateStr.match(DATE_REG);
    const dt = new Date(
      Date.UTC(
        result[1],
        result[2] - 1,
        result[3],
        result[4],
        result[5],
        result[6],
      ),
    );
    return +(dt.getTime() / 1000).toFixed();
  }
}
