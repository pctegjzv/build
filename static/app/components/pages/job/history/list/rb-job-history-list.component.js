import moment from 'moment';
import {
  JOB_STATUS_CSS_MAP,
  JOB_STATUS_ICON_STATUS_MAP,
} from '../../rb-job.constants';
const templateStr = require('app/components/pages/job/history/list/rb-job-history-list.component.html');
angular.module('app.components.pages').component('rbJobHistoryList', {
  bindings: {
    configName: '<',
  },
  controller: rbJobHistoryListController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbJobHistoryListController(
  translateService,
  $state,
  rbDelay,
  rbModal,
  rbSafeApply,
  rbJobUtility,
  rbJobDataService,
) {
  const vm = this;

  vm.pagination = {
    pageSize: 20,
    pageno: 1,
    totalItems: 0,
  };

  vm.pageNoChange = pageNoChange;
  vm.onSearchChanged = onSearchChanged;
  vm.onSearchChanged = onSearchChanged;

  vm.getImageAddress = getImageAddress;
  vm.getCreatorName = getCreatorName;

  vm.showStartDialog = showStartDialog;

  // Life cycle hooks
  vm.$onInit = onInit;
  vm.$onDestroy = onDestroy;
  //////////
  async function onInit() {
    vm.statusCssMap = JOB_STATUS_CSS_MAP;
    vm.statusIconMap = JOB_STATUS_ICON_STATUS_MAP;
    _startPollTimer();
    rbSafeApply();
  }

  // Simply poll data every 30 seconds
  async function _startPollTimer() {
    while (!vm.destroyed) {
      vm.pageNoChange();
      await rbDelay(30 * 1000);
    }
  }

  function onDestroy() {
    vm.destroyed = true;
  }

  let _lastSearchCounter = 0;

  async function onSearchChanged(search) {
    const searchCounter = ++_lastSearchCounter;
    vm.search = search;
    vm.searching = true;
    await pageNoChange(1);
    if (searchCounter === _lastSearchCounter) {
      vm.searching = false;
    }
    rbSafeApply();
  }

  async function pageNoChange(
    page = vm.pagination.pageno,
    size = vm.pagination.pageSize,
  ) {
    vm.pageNoChanged = vm.pagination.pageno !== page;
    vm.pagination.pageno = page;
    vm.pagination.pageSize = size;
    vm.loading = true;
    try {
      const { count, results } = await _refetchData({
        pageno: vm.pagination.pageno,
        page_size: vm.pagination.pageSize,
        search: vm.search,
      });
      vm.pagination.totalItems = count;
      vm.list = results;
      // Attach duration to list items:
      vm.list &&
        vm.list.forEach(item => {
          item.duration = _calculateTimeCost(item);
        });
      vm.loadError = false;
    } catch (e) {
      if (vm.pageNoChanged) {
        vm.list = [];
      }
    }
    vm.loading = false;
    rbSafeApply();
  }

  async function _refetchData({ pageno, page_size, search }) {
    return await rbJobDataService.getHistories({
      pageno,
      page_size,
      config_name: vm.configName,
      search,
    });
  }

  // TODO move to utils
  function _calculateTimeCost(history) {
    if (history.started_at && history.ended_at) {
      const startedAt = moment(history.started_at);
      const endedAt = moment(history.ended_at);

      return endedAt - startedAt;
    } else {
      return -1;
    }
  }

  function showStartDialog() {
    rbModal
      .show({
        title: translateService.get('job_start'),
        component: 'rbStartJob',
      })
      .then(uuid => {
        $state.go('job.history.detail', { uuid });
      })
      .catch(() => {});
  }

  function getImageAddress(item) {
    return rbJobUtility.getFullImageName(item) + ':' + item.image_tag;
  }

  function getCreatorName(item) {
    return item.is_auto_scheduled
      ? translateService.get('auto_trigger')
      : item.created_by;
  }
}
