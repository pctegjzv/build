import { resource_types as RESOURCE_TYPES } from '../../../rbac_role/rb-rbac-helper.constant';
const templateStr = require('app/components/pages/job/history/list/rb-job-start-dialog.component.html');
angular.module('app.components.pages').component('rbStartJob', {
  bindings: {},
  controller: triggerBuildController,
  controllerAs: 'vm',
  template: templateStr,
});

function triggerBuildController(
  $log,
  rbRoleUtilities,
  rbSafeApply,
  rbModal,
  rbJobDataService,
) {
  const vm = this;
  vm.$onInit = onInit;

  vm.hide = buildId => {
    rbModal.hide(buildId);
  };
  vm.cancel = () => {
    rbModal.cancel();
  };
  vm.start = async () => {
    vm.submitting = true;
    try {
      const configName = vm.uuid;
      const displayName = vm.name;
      const { job_uuid } = await rbJobDataService.start(
        configName,
        displayName,
      );
      vm.hide(job_uuid);
    } catch (e) {
      $log.error('Failed to start job ' + vm.name);
    }
    vm.submitting = false;
    rbSafeApply();
  };

  async function onInit() {
    // page_size = 0 means returns all job configs
    const jobConfigs = (await rbJobDataService.getConfigs({ page_size: 0 }))
      .results;
    vm.jobConfigs = jobConfigs
      .filter(jobConfig =>
        rbRoleUtilities.resourceHasPermission(
          jobConfig,
          RESOURCE_TYPES.JOB_CONFIG,
          'trigger',
        ),
      )
      .map(item => {
        item.option_display_name = item.space_name
          ? `${item.config_name}(${item.space_name})`
          : item.config_name;
        return item;
      });
    rbSafeApply();
  }
}
