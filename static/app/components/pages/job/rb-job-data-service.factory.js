angular
  .module('app.components.pages')
  .factory('rbJobDataService', rbJobDataServiceFactory);

/**
 * Job related data services.
 */
function rbJobDataServiceFactory(
  translateService,
  rbHttp,
  rbToast,
  rbAccountService,
) {
  const namespace = rbAccountService.getCurrentNamespace();
  const URLS = {
    CONFIGS: `/ajax/job_configs/${namespace}/`,
    JOBS: `/ajax/jobs/${namespace}/`,
  };

  return {
    getConfigs,
    getConfigByName,

    deleteConfig,
    createConfig,
    updateConfig,

    start,

    getHistories,
    getHistoryById,
    deleteHistoryById,
    getHistoryLogs,
  };

  //////////
  function getConfigs({ search, page_size, pageno }) {
    return rbHttp.sendRequest({
      url: URLS.CONFIGS,
      method: 'GET',
      params: { search, page_size, page: pageno },
    });
  }

  function getConfigByName(name) {
    return rbHttp.sendRequest({
      url: URLS.CONFIGS + name,
      method: 'GET',
    });
  }

  function start(name, displayname) {
    return rbHttp
      .sendRequest({
        url: URLS.JOBS,
        method: 'POST',
        data: { config_name: name },
      })
      .then(res => {
        const display_name = displayname ? displayname : name;
        rbToast.success(
          translateService.get('job_start_info_success', {
            name: display_name,
          }),
        );
        return res;
      });
  }

  function createConfig(payload) {
    return rbHttp
      .sendRequest({
        url: URLS.CONFIGS,
        method: 'POST',
        data: payload,
      })
      .then(res => {
        rbToast.success(
          translateService.get('job_config_create_info_success', {
            name: payload.config_name,
          }),
        );
        return res;
      });
  }

  function updateConfig(uuid, payload) {
    return rbHttp
      .sendRequest({
        url: URLS.CONFIGS + uuid,
        method: 'PUT',
        data: payload,
      })
      .then(res => {
        rbToast.success(
          translateService.get('job_config_update_info_success', {
            name: payload.config_name,
          }),
        );
        return res;
      });
  }

  function deleteConfig({ name = '', uuid = '' }) {
    return rbHttp
      .sendRequest({
        url: URLS.CONFIGS + uuid,
        method: 'DELETE',
      })
      .then(res => {
        rbToast.success(
          translateService.get('job_config_delete_info_success', { name }),
        );
        return res;
      });
  }

  /**
   * Get job histories.
   *
   * @param config_name: string. Search jobs belonging to the specified config_name
   * @param search: string. Search by name using 'contains' algorithm
   * @param page_size: number. Number of items returned in one page, defaults to 20
   * @param pageno: number. Select the desired page, defaults to 1
   * @return { num_pages: number, page_size: number, count: number, results: JobHistory[] }
   */
  function getHistories({ config_name, search, page_size = 20, pageno = 1 }) {
    return rbHttp.sendRequest({
      url: URLS.JOBS,
      method: 'GET',
      params: { config_name, search, page_size, page: pageno },
    });
  }

  /**
   * Get a single job history.
   * @param uuid: string
   */
  function getHistoryById(uuid) {
    return rbHttp.sendRequest({
      url: URLS.JOBS + uuid,
      method: 'GET',
    });
  }

  function deleteHistoryById(uuid) {
    return rbHttp
      .sendRequest({
        url: URLS.JOBS + uuid,
        method: 'DELETE',
      })
      .then(res => {
        rbToast.success(
          translateService.get('job_history_delete_info_success', { uuid }),
        );
        return res;
      });
  }

  function getHistoryLogs({ uuid, start_time, end_time, limit = 2000 }) {
    start_time = +start_time.toFixed();
    end_time = +end_time.toFixed();
    return rbHttp.sendRequest({
      url: URLS.JOBS + uuid + '/logs',
      method: 'GET',
      params: { start_time, end_time, limit },
    });
  }
}
