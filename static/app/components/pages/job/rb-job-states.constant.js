module.exports = [
  {
    baseStateName: 'job.config',
    states: [
      {
        url: '/list',
        name: 'list',
        component: 'rbJobConfigList',
      },
      {
        url: '/create',
        name: 'create',
        component: 'rbJobConfigCreate',
      },
      {
        url: '/detail/:name',
        name: 'detail',
        component: 'rbJobConfigDetail',
        resolve: {
          name: $stateParams => {
            'ngInject';
            return $stateParams['name'];
          },
        },
      },
      {
        url: '/update/:name',
        name: 'update',
        component: 'rbJobConfigUpdate',
        resolve: {
          name: $stateParams => {
            'ngInject';
            return $stateParams['name'];
          },
        },
      },
    ],
  },
  {
    baseStateName: 'job.history',
    states: [
      {
        url: '/list',
        name: 'list',
        component: 'rbJobHistoryList',
      },
      {
        url: '/detail/:uuid',
        name: 'detail',
        component: 'rbJobHistoryDetail',
        resolve: {
          uuid: $stateParams => {
            'ngInject';
            return $stateParams['uuid'];
          },
        },
      },
    ],
  },
];
