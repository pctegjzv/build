angular
  .module('app.components.pages')
  .factory('rbJobUtility', rbJobUtilityFactory);

/**
 * Job related data services.
 */
function rbJobUtilityFactory() {
  return {
    getFullImageName,
  };
  //////////
  /**
   * Return full image name of a job config/history
   * @param jobConfigOrHistory
   * @returns {string}
   */
  function getFullImageName(jobConfigOrHistory) {
    if (jobConfigOrHistory.registry_index) {
      return (
        jobConfigOrHistory.registry_index +
        '/' +
        (jobConfigOrHistory.registry_project
          ? jobConfigOrHistory.registry_project + '/'
          : '') +
        jobConfigOrHistory.image_name
      );
    } else {
      return '';
    }
  }
}
