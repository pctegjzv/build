export const JOB_STATUS = {
  BLOCKED: 'BLOCKED',
  WAITING: 'WAITING',
  RUNNING: 'RUNNING',
  STOPPED: 'STOPPED',
  SUCCEEDED: 'SUCCEEDED',
  FAILED: 'FAILED',
};

// UI -> API
export const RUN_COMMAND_TYPES = {
  DEFAULT: 'DEFAULT_COMMAND',
  CUSTOM: 'CUSTOM_COMMAND',
};

export const JOB_STATUS_CSS_MAP = {
  BLOCKED: 'pending',
  WAITING: 'in-progress',
  RUNNING: 'in-progress',
  STOPPED: 'error',
  FAILED: 'error',
  SUCCEEDED: 'success',
};

export const JOB_STATUS_ICON_STATUS_MAP = {
  BLOCKED: 'waiting',
  WAITING: 'in-progress',
  RUNNING: 'in-progress',
  STOPPED: 'error',
  FAILED: 'error',
  SUCCEEDED: 'completed',
};
