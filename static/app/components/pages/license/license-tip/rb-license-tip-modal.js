const templateStr = require('app/components/pages/license/license-tip/rb-license-tip-modal.html');
angular.module('app.components.pages').component('rbLicenseTipModal', {
  controller: rbLicenseTipModalController,
  controllerAs: 'vm',
  template: templateStr,
  bindings: {
    isadmin: '<',
  },
});

function rbLicenseTipModalController(
  translateService,
  rbHttp,
  rbModal,
  $log,
  rbAccountService,
) {
  const vm = this;
  vm.$onInit = _init;
  vm.productName = null;
  vm.submitting = null;
  vm.lgOut = lgOut;
  vm.upLicense = upLicense;

  async function _init() {
    try {
      vm.submitting = true;

      const registerInfo = await rbHttp.sendRequest({
        method: 'GET',
        url: `/ajax/license-auth/${rbAccountService.getCurrentNamespace()}/registry-code`,
      });

      vm.productName = registerInfo['product_name'];
    } catch (err) {
      $log.error(err);
    } finally {
      vm.submitting = false;
    }
  }

  function lgOut() {
    rbModal.cancel();
    location.href = '/ap/logout';
  }

  function upLicense() {
    rbModal.cancel();
    location.replace(`/landing/license-activation`);
  }
}
