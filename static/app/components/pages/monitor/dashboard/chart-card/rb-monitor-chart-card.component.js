/**
 * Created by liudong on 2017/1/16.
 */
import { resource_types as RESOURCE_TYPES } from '../../../rbac_role/rb-rbac-helper.constant';
const moment = require('moment');
const _ = require('lodash');
const templateStr = require('app/components/pages/monitor/dashboard/chart-card/rb-monitor-chart-card.html');
angular.module('app.components.pages').component('rbMonitorChartCard', {
  controller: rbMonitorChartCardController,
  controllerAs: 'vm',
  bindings: {
    metrics: '<',
    dashboardUuid: '<',
    chart: '<',
    period: '<',
    initTimeRange: '<',
  },
  require: {
    rbMonitorDashboardDetail: '?^^',
  },
  template: templateStr,
});

function rbMonitorChartCardController(
  $element,
  WEBLABS,
  $state,
  $location,
  ngRouter,
  rbMonitorDataService,
  rbMonitorUtilities,
  rbConfirmBox,
  translateService,
  $timeout,
  rbRoleUtilities,
  rbSafeApply,
  rbModal,
) {
  const vm = this;
  let destroyed = false;
  let lastUpdateTime = 0,
    lastUpdateTimeRange;
  const $pageContainer = $('.page-scroll-container');

  vm.$onInit = _init;
  vm.$onDestroy = _destroy;
  vm.$onChanges = _onChanges;
  vm.actionAlarmCreate = actionAlarmCreate;
  vm.actionChartDelete = actionChartDelete;
  vm.actionChartUpdate = actionChartUpdate;
  vm.actionChartRefresh = actionChartRefresh;
  vm.actionChartDetail = actionChartDetail;
  vm.buttonDisplayExpr = buttonDisplayExpr;

  vm.getChartData = getChartData;
  //////////

  function _init() {
    vm.pointTooltipFn = rbMonitorUtilities.getPointFormat;
    vm.initialized = false;
    vm.hideLegend = true;
    vm.highChartsOptions = {
      chart: {
        type: _.get(vm.chart, 'metrics[0].type', 'line'),
        spacing: [20, 20, 20, 10],
      },
    };

    getChartData({
      timeRange: vm.initTimeRange,
      refresh: false,
    }).then(() => {
      vm.initialized = true;
    });
    vm.rbMonitorDashboardDetail.registerChartController({
      uuid: vm.chart.uuid,
      controller: vm,
    });
  }

  function _destroy() {
    destroyed = true;
    vm.rbMonitorDashboardDetail.unRegisterChartController(vm.chart.uuid);
  }

  function _onChanges({ chart }) {
    // update chart, refresh
    if (chart && chart.currentValue) {
      vm.initialized &&
        getChartData({
          timeRange: lastUpdateTimeRange,
          refresh: true,
        });
    }
  }

  function getChartData({ timeRange, refresh = false, periodChange = false }) {
    if (destroyed || vm.loading) {
      return Promise.resolve();
    }
    if (refresh) {
      return _getChartData({
        timeRange,
      });
    }
    if (
      lastUpdateTimeRange &&
      lastUpdateTimeRange.start === timeRange.start &&
      lastUpdateTimeRange.end === timeRange.end
    ) {
      return;
    }
    // consider if current chart data is out of date, consider current chart card is visible to user
    if (_chartDataOutofDate({ periodChange }) && _chartCardInViewport()) {
      return _getChartData({
        timeRange,
      });
    } else {
      return Promise.resolve();
    }
  }

  function _getChartData({ timeRange }) {
    vm.loading = true;
    lastUpdateTimeRange = {
      start: timeRange.start,
      end: timeRange.end,
    };
    lastUpdateTime = moment().valueOf();
    return rbMonitorUtilities
      .getChartData({
        metrics: vm.chart.metrics,
        timeRange,
      })
      .then(result => {
        _initChart(result);
        vm.loading = false;
      });
  }

  function _initChart(result) {
    if (!result) {
      vm.series = null;
      return;
    }
    const { axisOptions, series } = result;
    vm.series = series;
    vm.axisOptions = axisOptions;
  }

  function actionChartUpdate() {
    if (!vm.metrics) {
      return;
    }
    rbModal
      .show({
        title: translateService.get('monitor_chart_update'),
        template:
          '<rb-monitor-chart-create item="item" metrics="metrics" dashboard-uuid="dashboardUuid"></rb-monitor-chart-create>',
        locals: {
          dashboardUuid: vm.dashboardUuid,
          metrics: vm.metrics,
          item: vm.chart,
        },
      })
      .then(data => {
        // update it in parent controller
        vm.rbMonitorDashboardDetail.actionChartUpdate(data);
      })
      .catch(() => {});
  }

  function actionChartRefresh() {
    vm.refreshing = true;
    let _timeRange;
    if (vm.period > 0) {
      const end = moment().valueOf();
      _timeRange = {
        start: end - vm.period,
        end,
      };
    } else {
      _timeRange = lastUpdateTimeRange;
    }
    getChartData({
      refresh: true,
      timeRange: _timeRange,
    }).then(() => {
      vm.refreshing = false;
      rbSafeApply();
    });
  }

  function actionChartDetail() {
    rbModal
      .show({
        width: 900,
        component: 'rbMonitorChartDetail',
        locals: {
          timeRange: lastUpdateTimeRange,
          chart: _.cloneDeep(vm.chart),
          initPeriod: vm.period,
        },
      })
      .catch(() => {});
  }

  function actionChartDelete() {
    rbConfirmBox
      .show({
        title: translateService.get('delete'),
        textContent: `${translateService.get('delete')} ${translateService.get(
          'monitor_chart',
        )} ${vm.chart.display_name}?`,
      })
      .then(() => {
        _deleteChart();
      });
  }

  function _deleteChart() {
    vm.deleteSubmiting = true;
    rbMonitorDataService
      .deleteChart({
        dashboard_uuid: vm.dashboardUuid,
        uuid: vm.chart.uuid,
      })
      .then(() => {
        // remove it from parent controller
        vm.rbMonitorDashboardDetail.actionChartDelete(vm.chart.uuid);
        vm.deleteSubmiting = false;
      });
  }

  function _chartCardInViewport() {
    const pageContainerHeight = $pageContainer.height(),
      elementTop = $element.offset().top,
      elementHeight = $element.height();

    const topConstraint = elementTop > 0,
      bottomConstraint = pageContainerHeight > elementTop + elementHeight / 2,
      heightConstraint = pageContainerHeight > elementHeight / 2;

    return topConstraint && bottomConstraint && heightConstraint;
  }

  function _chartDataOutofDate({ periodChange = false }) {
    if (periodChange) {
      return true;
    }
    const now = moment().valueOf();
    return now - lastUpdateTime >= 60 * 1000;
  }

  function actionAlarmCreate() {
    const metric = _.get(vm.chart, 'metrics[0]', null);
    if (!metric) {
      return;
    }
    const query = {
      metric_name: metric.metric,
      aggregator: metric.aggregator,
      tags: metric.over.split(','),
      group_by: metric.group_by,
    };
    ngRouter.navigateByUrl(
      `alarm/alarm_create?metric=${JSON.stringify(query)}`,
    );
  }

  function buttonDisplayExpr(action) {
    return rbRoleUtilities.resourceHasPermission(
      vm.chart,
      RESOURCE_TYPES.DASHBOARD_PANEL,
      action,
    );
  }
}
