/**
 * Created by liudong on 2017/1/16.
 */
const _ = require('lodash');
const moment = require('moment');
const templateStr = require('app/components/pages/monitor/dashboard/chart-create/rb-monitor-chart-create.html');
angular.module('app.components.pages').component('rbMonitorChartCreate', {
  controller: rbMonitorChartCreateController,
  controllerAs: 'vm',
  bindings: {
    dashboardUuid: '<',
    metrics: '<',
    item: '<',
  },
  template: templateStr,
});

function rbMonitorChartCreateController(
  rbModal,
  rbMonitorUtilities,
  rbMonitorDataService,
  rbToast,
  translateService,
  rbSafeApply,
) {
  const vm = this;
  const selectedSourceOptions = [];
  const selectGroupOptions = [];
  let getChartData, timeRange;

  vm.$onInit = _init;
  vm.confirm = confirm;
  vm.cancel = cancel;
  vm.onMetricSelected = _.debounce(onMetricSelected, 500);
  vm.onMetricSourceAdd = onMetricSourceAdd;
  vm.onMetricSourceRemove = onMetricSourceRemove;
  vm.onMetricGroupAdd = onMetricGroupAdd;
  vm.onMetricGroupRemove = onMetricGroupRemove;
  vm.onMetricAggregatorSelected = onMetricAggregatorSelected;
  vm.onChartTypeSelected = onChartTypeSelected;

  //////////
  function _init() {
    vm.initialized = false;
    vm.submitting = false;
    vm.model = {};
    vm.selectedMetric = null;
    vm.metricOptions = rbMonitorUtilities.getMonitorMetricOptions(vm.metrics);
    vm.metricSourceOptions = [];
    vm.metricGroupOptions = [];
    vm.metricAggregatorOptions = rbMonitorUtilities.getMonitorAggregatorOptions();
    vm.chartTypeOptions = rbMonitorUtilities.getMonitorChartTypeOptions();

    // highchart options
    vm.pointTooltipFn = rbMonitorUtilities.getPointFormat;
    vm.highChartsOptions = {
      chart: {
        height: 155,
        spacing: [10, 20, 0, 10],
      },
    };
    vm.hideLegend = true;

    if (vm.item) {
      // init update data
      vm.display_name = vm.item.display_name;
      const [metric] = vm.item.metrics;
      vm.model.type = metric.type;
      vm.model.metric = metric.metric;
      vm.model.aggregator = metric.aggregator;
      vm.selectedSourceOptions = metric.over.split(',');
      vm.selectGroupOptions = metric.group_by.split(',');
    }
    getChartData = _.debounce(_getChartData, 500);
    const end = moment().valueOf();
    timeRange = {
      start: end - 30 * 60 * 1000,
      end,
    };
    vm.initialized = true;
  }

  function _getChartData() {
    if (!vm.initialized || !vm.model.metric) {
      return false;
    }
    vm.loading = true;
    const metrics = [
      {
        metric: vm.model.metric,
        over: selectedSourceOptions.join(','),
        group_by: selectGroupOptions.join(','),
        aggregator: vm.model.aggregator,
      },
    ];
    return rbMonitorUtilities
      .getChartData({
        metrics,
        timeRange,
        cache: true,
      })
      .then(result => {
        _initChart(result);
        vm.loading = false;
      });
  }

  function _initChart(result) {
    if (!result) {
      vm.series = null;
      return;
    }
    const { axisOptions, series } = result;
    vm.series = series;
    vm.axisOptions = axisOptions;
  }

  async function onMetricSelected(option) {
    vm.selectedMetric = option;
    if (option.metric_name && vm.model.metric !== option.metric_name) {
      vm.display_name = option.metric_name;
    }
    vm.model.metric = option.metric_name;
    if (!option.metric_name) {
      return Promise.resolve();
    }

    // get metrics detail
    vm.metricSourceOptions = [];
    vm.metricGroupOptions = [];
    vm.metricsLoading = true;
    const metrics = await rbMonitorDataService.getMetricsDetail({
      metrics: option.metric_name,
      cache: true,
    });
    vm.metricsLoading = false;
    if (metrics) {
      vm.metricSourceOptions = rbMonitorUtilities.getMonitorSourceOptions(
        metrics,
      );
      vm.metricGroupOptions = rbMonitorUtilities.getMonitorGroupOption(metrics);
    }
    rbSafeApply();
    getChartData();
  }

  function onMetricSourceAdd(option) {
    selectedSourceOptions.push(option.value);
    getChartData();
  }

  function onMetricSourceRemove(option) {
    _.remove(selectedSourceOptions, item => {
      return item === option.value;
    });
    getChartData();
  }

  function onMetricGroupAdd(option) {
    selectGroupOptions.push(option.value);
    getChartData();
  }

  function onMetricGroupRemove(option) {
    _.remove(selectGroupOptions, item => {
      return item === option.value;
    });
    getChartData();
  }

  function onMetricAggregatorSelected(option) {
    vm.model.aggregator = option.value;
    getChartData();
  }

  function onChartTypeSelected(option) {
    vm.model.type = option.value;
    vm.highChartsOptions = {
      chart: {
        height: 155,
        spacing: [10, 20, 0, 10],
        type: option.value,
      },
    };
  }

  function confirm() {
    vm.form.$setSubmitted();
    if (!vm.form.$valid) {
      return;
    }
    vm.submitting = true;
    if (!vm.item) {
      // create chart
      rbMonitorDataService
        .createChart({
          dashboard_uuid: vm.dashboardUuid,
          metrics: [
            {
              type: vm.model.type,
              metric: vm.model.metric,
              over: selectedSourceOptions.join(','),
              group_by: selectGroupOptions.join(','),
              aggregator: vm.model.aggregator,
            },
          ],
          display_name: vm.display_name,
        })
        .then(({ data }) => {
          rbModal.hide(data);
        })
        .catch(() => {})
        .then(() => {
          vm.submitting = false;
        });
    } else {
      // update chart
      rbMonitorDataService
        .updateChart({
          dashboard_uuid: vm.dashboardUuid,
          uuid: vm.item.uuid,
          metrics: [
            {
              type: vm.model.type,
              metric: vm.model.metric,
              over: selectedSourceOptions.join(','),
              group_by: selectGroupOptions.join(','),
              aggregator: vm.model.aggregator,
            },
          ],
          display_name: vm.display_name,
        })
        .then(({ data }) => {
          rbModal.hide(data);
        })
        .catch(() => {})
        .then(() => {
          vm.submitting = false;
        });
    }
  }

  function cancel() {
    rbModal.cancel();
  }
}
