/**
 * Created by liudong on 2017/1/18.
 */
const moment = require('moment');
const templateStr = require('app/components/pages/monitor/dashboard/chart-detail/rb-monitor-chart-detail.html');
angular.module('app.components.pages').component('rbMonitorChartDetail', {
  controller: rbMonitorChartDetailController,
  controllerAs: 'vm',
  bindings: {
    chart: '<',
    timeRange: '<',
    initPeriod: '<',
  },
  template: templateStr,
});

function rbMonitorChartDetailController(
  rbModal,
  rbMonitorDataService,
  rbToast,
  translateService,
  rbMonitorUtilities,
) {
  const vm = this;
  let currentPeriod;

  vm.$onInit = _init;
  vm.$onDestroy = _destroy;

  vm.cancel = cancel;
  vm.onPeriodChange = onPeriodChange;
  vm.onMetricAggregatorSelected = onMetricAggregatorSelected;
  vm.confirmTimeRange = confirmTimeRange;
  vm.isCustomTimeRange = isCustomTimeRange;

  //////////
  function _init() {
    vm.highchartsOptions = {
      chart: {
        height: 600,
        type: _.get(vm.chart, 'metrics[0].type', 'line'),
        spacing: [20, 20, 10, 10],
        ignoreHiddenSeries: false,
      },
    };
    vm.dateTimeOptions = rbMonitorUtilities.getDateTimeOptions();
    vm.pointTooltipFn = rbMonitorUtilities.getPointFormat;
    vm.initialized = false;
    // TODO: currently every chart has only one metric
    vm.aggregator = vm.chart.metrics[0]['aggregator'];
    vm.metricAggregatorOptions = rbMonitorUtilities.getMonitorAggregatorOptions();
    vm.periodOptions = rbMonitorUtilities.getPeriodOptions();
    getChartData().then(() => {
      vm.initialized = true;
    });
  }

  function _destroy() {}

  function cancel() {
    rbModal.cancel();
  }

  function onPeriodChange(option) {
    currentPeriod = option.period;
    if (!isCustomTimeRange()) {
      const end = moment().valueOf();
      vm.timeRange = {
        start: end - option.period,
        end,
      };
    }
    vm.initialized && getChartData();
  }

  function onMetricAggregatorSelected(option) {
    vm.aggregator = option.value;
    vm.chart.metrics.forEach(item => {
      item.aggregator = option.value;
    });
    vm.initialized && getChartData();
  }

  function getChartData() {
    vm.loading = true;
    return rbMonitorUtilities
      .getChartData({
        metrics: vm.chart.metrics,
        timeRange: vm.timeRange,
      })
      .then(result => {
        _initChart(result);
        vm.loading = false;
      });
  }

  function _initChart(result) {
    if (!result || !result.axisOptions || !result.series) {
      return;
    }
    const { axisOptions, series } = result;
    vm.series = series;
    vm.axisOptions = axisOptions;
  }

  function confirmTimeRange() {
    if (vm.timeRange.end - vm.timeRange.start < 30 * 60 * 1000) {
      rbToast.warning(translateService.get('metric_timerange_should_less_30'));
      return;
    }
    vm.initialized && getChartData();
  }

  function isCustomTimeRange() {
    return currentPeriod === -1;
  }
}
