/**
 * Created by liudong on 2017/1/17.
 */
const templateStr = require('app/components/pages/monitor/dashboard/dashboard-create/rb-monitor-dashboard-create.html');
angular.module('app.components.pages').component('rbMonitorDashboardCreate', {
  controller: rbMonitorDashboardCreateController,
  controllerAs: 'vm',
  bindings: {
    item: '<',
  },
  template: templateStr,
});

function rbMonitorDashboardCreateController(
  rbModal,
  rbMonitorDataService,
  WEBLABS,
  rbSafeApply,
  rbQuotaSpaceDataService,
) {
  const vm = this;

  vm.$onInit = _init;
  vm.confirm = confirm;
  vm.cancel = cancel;
  vm.onSpaceChanged = onSpaceChanged;

  //////////
  async function _init() {
    vm.model = {};
    vm.selectedMetric = null;
    if (vm.item) {
      vm.model.display_name = vm.item.display_name;
    }
    vm.quotaEnabled = WEBLABS['quota_enabled'];
    if (vm.quotaEnabled) {
      vm.spaces = await rbQuotaSpaceDataService.getConsumableSpaces();
    }
  }

  function onSpaceChanged(space) {
    vm.space_name = space.name;
  }

  function confirm() {
    vm.form.$setSubmitted();
    if (!vm.form.$valid) {
      return;
    }
    const data = {
      dashboard_name: vm.model.name,
      display_name: vm.model.display_name,
    };
    if (vm.space_name) {
      data.space_name = vm.space_name;
    }
    vm.submitting = true;
    if (!vm.item) {
      rbMonitorDataService
        .createDashboard(data)
        .then(({ data }) => {
          rbModal.hide(data);
        })
        .catch(() => {})
        .then(() => {
          vm.submitting = false;
        });
    } else {
      rbMonitorDataService
        .updateDashboard({
          uuid: vm.item.uuid,
          display_name: vm.model.display_name,
        })
        .then(({ data }) => {
          rbModal.hide(data);
        })
        .catch(() => {})
        .then(() => {
          vm.submitting = false;
        });
    }
  }

  function cancel() {
    rbModal.cancel();
  }
}
