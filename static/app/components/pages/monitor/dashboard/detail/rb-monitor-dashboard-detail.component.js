/**
 * Created by liudong on 2017/1/13.
 */
import { resource_types as RESOURCE_TYPES } from '../../../rbac_role/rb-rbac-helper.constant';

const _ = require('lodash');
const moment = require('moment');
const dragula = require('dragula');
const ElementQueries = require('css-element-queries/src/ElementQueries');
const templateStr = require('app/components/pages/monitor/dashboard/detail/rb-monitor-dashboard-detail.html');
angular.module('app.components.pages').component('rbMonitorDashboardDetail', {
  controller: rbMonitorDashboardDetailController,
  controllerAs: 'vm',
  bindings: {
    uuid: '<',
  },
  template: templateStr,
});

function rbMonitorDashboardDetailController(
  $state,
  $element,
  $interval,
  rbMonitorDataService,
  rbMonitorUtilities,
  rbAccountService,
  rbPrivilegeService,
  rbModal,
  rbToast,
  rbSafeApply,
  $timeout,
  rbRoleUtilities,
  rbSuperbar,
  WEBLABS,
  translateService,
) {
  const vm = this;
  let timer = null;
  let destroyed = false;
  const chartControllers = [];
  let pageContainerScrollListener;
  let drake;
  let chartIndex;
  const $pageContainer = $('.page-scroll-container');

  vm.$onInit = _init;
  vm.$onDestroy = _destroy;
  vm.$postLink = _postLink;

  vm.addMonitorChart = addMonitorChart;
  vm.onPeriodChange = onPeriodChange;
  vm.actionChartUpdate = actionChartUpdate;
  vm.actionChartDelete = actionChartDelete;
  vm.getDashboardDetail = getDashboardDetail;
  vm.confirmTimeRange = confirmTimeRange;
  vm.isCustomTimeRange = isCustomTimeRange;

  vm.registerChartController = registerChartController;
  vm.unRegisterChartController = unRegisterChartController;

  //////////

  async function _init() {
    const end = moment().valueOf();
    vm.initTimeRange = {
      start: end - 30 * 60 * 1000,
      end,
    };
    vm.loading = false;
    vm.submitting = false;
    vm.metricsReady = false;
    vm.loadError = false;
    vm.charts = [];
    vm.dateTimeOptions = rbMonitorUtilities.getDateTimeOptions();
    vm.periodOptions = rbMonitorUtilities.getPeriodOptions();
    _getMetricsData();
    await getDashboardDetail();
    if (!vm.dashboard) {
      rbToast.error(translateService.get('dashboard_not_exist'));
      $state.go('^.list');
      return;
    }
    // init create dashbaord chart permission
    const resourceCreatePermission = await rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.DASHBOARD_PANEL,
      {
        dashboard_name: vm.dashboard.dashboard_name,
      },
    );
    vm.chartCreateEnabled = resourceCreatePermission;
    vm.initialized = true;
  }

  function _destroy() {
    destroyed = true;
    $pageContainer.off('scroll', pageContainerScrollListener);
    drake && drake.destroy();
    rbSuperbar.destroy();
  }

  function _postLink() {
    _initDragula();
    ElementQueries.init();
    // register superbar
    rbSuperbar.register($element.find('.rb-page-heading'), {
      targetPadding: 0,
    });
  }

  function _initDragula() {
    drake = dragula([$('.rb-dashboard-charts')[0]], {
      direction: 'horizontal',
      moves: (el, source, handle) => {
        return $(handle).hasClass('chart-card-title');
      },
    });
    drake.on('drop', () => {
      // get cards order and update to server
      _updateCardsOrder();
    });
  }

  function _updateCardsOrder() {
    const orders = $element
      .find('rb-monitor-chart-card')
      .toArray()
      .map(card => {
        return $(card).data('uuid');
      });
    const reorderedCharts = orders.map(uuid =>
      vm.charts.find(chart => chart.uuid === uuid),
    );
    vm.charts = reorderedCharts;
    rbMonitorDataService
      .updateDashboard({
        uuid: vm.uuid,
        orders: orders.join(','),
      })
      .then(() => {})
      .catch(() => {})
      .then(() => {});
  }

  function _getMetricsData() {
    rbMonitorDataService
      .getMetrics()
      .then(data => {
        vm.metrics = data.sort();
      })
      .catch(() => {})
      .then(() => {
        vm.metricsReady = !!vm.metrics;
      });
  }

  async function getDashboardDetail() {
    vm.loadError = false;
    vm.loading = true;
    vm.dashboard = await rbMonitorDataService.getDashboardDetail({
      uuid: vm.uuid,
    });
    vm.loading = false;
    if (!vm.dashboard) {
      vm.loadError = true;
    } else {
      vm.dashboard.charts.forEach((chart, index) => {
        chart.privilege = vm.dashboard.privilege;
        chart.index = 'chart' + index;
      });
      const charts = vm.dashboard.charts || [];
      chartIndex = charts.length;
      if (charts.length) {
        const reorderedCharts = vm.dashboard.orders
          .split(',')
          .map(uuid => charts.find(chart => chart.uuid === uuid));
        vm.charts = reorderedCharts;
      }
      _initTimer();
      _initPageContainerEventListener();
    }
    rbSafeApply();
  }

  function onPeriodChange(option) {
    vm.currentPeriod = option.period;
    if (isCustomTimeRange()) {
      return;
    }
    _notifyChartCard({
      periodChange: true,
    });
    const end = moment().valueOf();
    vm.timeRange = {
      start: end - vm.currentPeriod,
      end,
    };
  }

  function addMonitorChart() {
    rbModal
      .show({
        title: translateService.get('monitor_add'),
        template:
          '<rb-monitor-chart-create metrics="metrics" dashboard-uuid="dashboardUuid"></rb-monitor-chart-create>',
        locals: {
          dashboardUuid: vm.uuid,
          metrics: vm.metrics,
        },
      })
      .then(data => {
        data.index = 'chart' + chartIndex++;
        vm.charts.unshift(data);
        rbSafeApply();
      })
      .catch(() => {});
  }

  function actionChartUpdate(data) {
    const index = _.findIndex(vm.charts, chart => {
      return chart.uuid === data.uuid;
    });
    if (index >= 0) {
      data.index = 'chart' + chartIndex++;
      vm.charts[index] = data;
    }
    rbSafeApply();
  }

  function actionChartDelete(uuid) {
    _.remove(vm.charts, chart => {
      return chart.uuid === uuid;
    });
    rbSafeApply();
  }

  function _initTimer() {
    $interval.cancel(timer);
    timer = $interval(() => {
      _notifyChartCard({}); // use time period
    }, 60 * 1000);
  }

  function _initPageContainerEventListener() {
    pageContainerScrollListener = _.throttle(() => {
      if (vm.currentPeriod > 0) {
        _notifyChartCard({}); // use time period
      } else {
        _notifyChartCard({
          timeRange: vm.timeRange, // use custom time range
        });
      }
    }, 500);
    $pageContainer.on('scroll', pageContainerScrollListener);
  }

  function _notifyChartCard({ timeRange = null, periodChange = false }) {
    if (destroyed || (!timeRange && isCustomTimeRange())) {
      return;
    }
    if (!timeRange) {
      const end = moment().valueOf();
      timeRange = {
        start: end - vm.currentPeriod,
        end,
      };
    }
    chartControllers.forEach(item => {
      if (item.controller.getChartData) {
        item.controller.getChartData({
          timeRange,
          periodChange,
        });
      }
    });
  }

  function registerChartController({ chart_uuid, controller }) {
    chartControllers.push({
      chart_uuid,
      controller,
    });
  }

  function unRegisterChartController(chart_uuid) {
    _.remove(chartControllers, item => {
      return chart_uuid === item.chart_uuid;
    });
  }

  function confirmTimeRange() {
    if (vm.timeRange.end - vm.timeRange.start < 30 * 60 * 1000) {
      rbToast.warning(translateService.get('metric_timerange_should_less_30'));
      return;
    }
    _notifyChartCard({
      timeRange: vm.timeRange,
      periodChange: true,
    });
  }

  function isCustomTimeRange() {
    return vm.currentPeriod === -1;
  }
}
