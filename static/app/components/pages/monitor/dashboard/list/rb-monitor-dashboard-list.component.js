/**
 * Created by liudong on 2017/1/13.
 */
import { resource_types as RESOURCE_TYPES } from '../../../rbac_role/rb-rbac-helper.constant';
const _ = require('lodash');
const templateStr = require('app/components/pages/monitor/dashboard/list/rb-monitor-dashboard-list.html');
angular.module('app.components.pages').component('rbMonitorDashboardList', {
  controller: rbMonitorDashboardListController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbMonitorDashboardListController(
  $state,
  translateService,
  rbMonitorDataService,
  rbConfirmBox,
  rbSafeApply,
  rbRoleUtilities,
  rbModal,
) {
  const vm = this;

  vm.$onInit = _init;
  vm.$onDestroy = _destroy;

  vm.showDashboardDetail = showDashboardDetail;
  vm.createDashbaord = createDashbaord;
  vm.deleteDashboard = deleteDashboard;
  vm.updateDashboard = updateDashboard;
  vm.buttonDisplayExpr = buttonDisplayExpr;

  //////////
  async function _init() {
    vm.initialized = false;
    vm.filterKey = '';
    vm.page = 1;

    await getDashboardList();
    vm.initialized = true;
    vm.createEnabled = await rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.DASHBOARD,
    );
    rbSafeApply();
  }

  function _destroy() {}

  async function getDashboardList() {
    return rbMonitorDataService
      .getDashboardList({
        page: vm.page,
      })
      .then(result => {
        vm.dashboardList = result.data.sort((a, b) => {
          return b.created_at - a.created_at;
        });
        vm.num_pages = result.num_pages;
        vm.count = result.count;
        vm.dashboardList.forEach(dashboard => {
          dashboard.chart_count = dashboard.orders
            ? dashboard.orders.split(',').length
            : 0;
        });
      });
  }

  async function createDashbaord() {
    rbModal
      .show({
        title: translateService.get('monitor_dashboard_create'),
        template: '<rb-monitor-dashboard-create></rb-monitor-dashboard-create>',
      })
      .then(data => {
        data.chart_count = 0;
        vm.dashboardList.unshift(data);
        rbSafeApply();
      })
      .catch(() => {});
  }

  async function deleteDashboard(dashboard) {
    if (dashboard.deleting) {
      return;
    }
    await rbConfirmBox.show({
      title: translateService.get('delete'),
      textContent: `${translateService.get('delete')} ${translateService.get(
        'monitor_dashboard',
      )} ${dashboard.display_name}?`,
    });
    dashboard.deleting = true;
    try {
      await _deleteDashboard(dashboard);
    } catch (err) {
      dashboard.deleting = false;
    }
    rbSafeApply();
  }

  function _deleteDashboard(dashboard) {
    rbMonitorDataService
      .deleteDashboard({
        uuid: dashboard.uuid,
      })
      .then(() => {
        _.remove(vm.dashboardList, item => {
          return item.uuid === dashboard.uuid;
        });
      })
      .catch(() => {});
  }

  function updateDashboard(item) {
    rbModal
      .show({
        title: translateService.get('monitor_dashboard_update'),
        template:
          '<rb-monitor-dashboard-create item="item"></rb-monitor-dashboard-create>',
        locals: {
          item,
        },
      })
      .then(data => {
        const index = _.findIndex(vm.dashboardList, _item => {
          return _item.uuid === item.uuid;
        });
        data.chart_count = data.orders ? data.orders.split(',').length : 0;
        vm.dashboardList[index] = data;
        rbSafeApply();
      })
      .catch(() => {});
  }

  function showDashboardDetail(dashboard) {
    $state.go('monitor.dashboard.state_dashboard_detail', {
      display_name: dashboard.display_name,
      uuid: dashboard.uuid,
      crumbParams: ['display_name'],
    });
  }

  function buttonDisplayExpr(dashboard, action) {
    return rbRoleUtilities.resourceHasPermission(
      dashboard,
      RESOURCE_TYPES.DASHBOARD,
      action,
    );
  }
}
