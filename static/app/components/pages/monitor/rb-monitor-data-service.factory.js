/**
 * Created by liudong on 2017/1/13.
 */
angular
  .module('app.components.pages')
  .factory('rbMonitorDataService', rbMonitorDataServiceFactory);

function rbMonitorDataServiceFactory(
  rbHttp,
  rbToast,
  translateService,
  rbAccountService,
) {
  const namespace = rbAccountService.getCurrentNamespace();
  const MONITOR_URL = `/ajax/v2/monitor/${namespace}/`;
  const DASHBOARDS_URL = `${MONITOR_URL}dashboards/`;
  const METRICS_URL = `${MONITOR_URL}metrics/`;

  return {
    getDashboardList,
    getDashboardDetail,
    createDashboard,
    updateDashboard,
    deleteDashboard,
    createChart,
    updateChart,
    deleteChart,
    getMetrics,
    getMetricsDetail,
    queryMetricsData,
  };

  //////////
  function getDashboardList({ page = 1, page_size = 100 }) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: DASHBOARDS_URL,
        params: {
          page,
          page_size,
        },
      })
      .catch(() => {
        return {
          data: [],
        };
      });
  }

  function getDashboardDetail({ uuid }) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: DASHBOARDS_URL + uuid,
      })
      .then(({ data }) => {
        return data;
      })
      .catch(() => {
        return null;
      });
  }

  function createDashboard(data) {
    return rbHttp.sendRequest({
      method: 'POST',
      url: DASHBOARDS_URL,
      data,
    });
  }

  function updateDashboard({ uuid, display_name = '', orders = '' }) {
    const data = {};
    display_name && (data['display_name'] = display_name);
    orders && (data['orders'] = orders);
    return rbHttp.sendRequest({
      method: 'PUT',
      url: DASHBOARDS_URL + uuid,
      data,
    });
  }

  function deleteDashboard({ uuid }) {
    return rbHttp
      .sendRequest({
        method: 'DELETE',
        url: DASHBOARDS_URL + uuid,
      })
      .then(res => {
        rbToast.success(translateService.get('dashboard_delete_success'));
        return res;
      });
  }

  function createChart({ dashboard_uuid, metrics, display_name }) {
    return rbHttp.sendRequest({
      method: 'POST',
      url: DASHBOARDS_URL + dashboard_uuid + '/charts',
      data: {
        metrics,
        display_name,
      },
    });
  }

  function updateChart({ dashboard_uuid, uuid, metrics, display_name }) {
    return rbHttp.sendRequest({
      method: 'PUT',
      url: DASHBOARDS_URL + dashboard_uuid + '/charts/' + uuid,
      data: {
        metrics,
        display_name,
      },
    });
  }

  function deleteChart({ dashboard_uuid, uuid }) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url: DASHBOARDS_URL + dashboard_uuid + '/charts/' + uuid,
    });
  }

  function getMetrics() {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: METRICS_URL,
        params: {
          simple: 'true',
        },
      })
      .then(({ result }) => result)
      .catch(() => {
        return [];
      });
  }

  function getMetricsDetail({ metrics = '', cache = false }) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: METRICS_URL,
        params: {
          metrics,
          window: 86400,
        },
        cache,
      })
      .then(({ result }) => result[0])
      .catch(() => {
        return null;
      });
  }

  function queryMetricsData({ query_list, start, end, cache = false }) {
    const q = query_list
      .map(query => {
        return `q=${encodeURIComponent(query)}`;
      })
      .join('&');
    const url = `${METRICS_URL}query?${q}&start=${start}&end=${end}`;
    return rbHttp
      .sendRequest({
        method: 'GET',
        url,
        cache,
      })
      .catch(() => {
        return { result: null };
      });
  }
}
