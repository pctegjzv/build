/**
 * Created by liudong on 2017/1/16.
 */
const _ = require('lodash');
const moment = require('moment');
import { TRANSLATE_METRIC_TAG_KEYS } from './rb-monitor.constant';

angular
  .module('app.components.pages')
  .factory('rbMonitorUtilities', rbMonitorUtilities);

function rbMonitorUtilities(translateService, rbMonitorDataService, $filter) {
  const filteredTagsKey = ['region_id'];
  return {
    getMetricsQueryList,
    getPeriodOptions,
    getMonitorMetricOptions,
    getMonitorSourceOptions,
    getMonitorAggregatorOptions,
    getMonitorGroupOption,
    getChartData,
    getPointFormat,
    getDateTimeOptions,
    getMonitorChartTypeOptions,
  };

  function getMetricsQueryList(metrics) {
    // agg:metric_name{where1,where2}exclude{exclude1,exclude2}by{group_by}
    const query_list = [];
    metrics.forEach(item => {
      let q;
      if (item.exclude) {
        q = `${item.aggregator}:${item.metric}{${item.over || ''}}exclude{${
          item.exclude
        }}by{${item.group_by || ''}}`;
      } else {
        q = `${item.aggregator}:${item.metric}{${item.over ||
          ''}}by{${item.group_by || ''}}`;
      }
      query_list.push(q);
    });
    return query_list;
  }

  function getPeriodOptions() {
    // period: time interval in millisecond
    const hour = 60 * 60 * 1000;
    return [
      {
        title: translateService.get('chart_half_hour'),
        period: 0.5 * hour,
      },
      {
        title: translateService.get('chart_one_hour'),
        period: hour,
      },
      // {
      //   title: translateService.get('chart_three_hour'),
      //   period: 3 * hour
      // },
      {
        title: translateService.get('chart_six_hour'),
        period: 6 * hour,
      },
      // {
      //   title: translateService.get('chart_twelve_hour'),
      //   period: 12 * hour
      // },
      {
        title: translateService.get('chart_one_day'),
        period: 24 * hour,
      },
      {
        title: translateService.get('chart_three_day'),
        period: 3 * 24 * hour,
      },
      {
        title: translateService.get('chart_one_week'),
        period: 7 * 24 * hour,
      },
      {
        title: translateService.get('chart_two_week'),
        period: 14 * 24 * hour,
      },
      {
        title: translateService.get('custom_time_range'),
        period: -1,
      },
    ];
  }

  function getMonitorMetricOptions(metrics) {
    const options = [];
    metrics.forEach(item => {
      options.push({
        // currently they have the same value
        metric_name: item,
        display_name: item,
        // tags: item.tags
      });
    });
    return options;
  }

  function getMonitorSourceOptions(metric) {
    const options = [];
    const tags = metric.tags || {};
    for (const key in tags) {
      if (!['region_id', 'instance_id'].includes(key)) {
        const tagItem = tags[key];
        tagItem.forEach(item => {
          const display_name = getMetricTagItemDisplayName(key, item);
          const display_key = TRANSLATE_METRIC_TAG_KEYS.includes(key)
            ? translateService.get(`metric_tag_${key}`)
            : key;
          options.push({
            name: `${display_key}:${display_name}`,
            value: `${key}=${item}`,
          });
        });
      }
    }

    return options;
  }

  function getMetricTagItemDisplayName(key, item) {
    let display_name = item;
    if (key === 'service_name') {
      const arr = item.split('.');
      if (arr.length === 3) {
        // space_name.app_name.service_name
        const [space_name, app_name, service_name] = arr;
        if (app_name === '#') {
          display_name = `${service_name} (${space_name})`;
        } else {
          display_name = `${app_name}.${service_name} (${space_name})`;
        }
      }
    } else if (key === 'app_name') {
      const arr = item.split('.');
      // TODO: space_name.#.app_name is not a valid pattern , wati for API udpate
      if (arr.length === 3) {
        // space_name.app_name
        const [space_name, , app_name] = arr;
        if (space_name === '#') {
          display_name = app_name;
        } else {
          display_name = `${app_name} (${space_name})`;
        }
      }
    } else if (key === 'instance_id') {
      display_name = item.length > 16 ? item.substring(0, 16) : item;
    }
    return display_name;
  }

  function getMonitorAggregatorOptions() {
    return [
      {
        name: translateService.get('aggregator_avg'),
        value: 'avg',
      },
      {
        name: translateService.get('aggregator_sum'),
        value: 'sum',
      },
      {
        name: translateService.get('aggregator_min'),
        value: 'min',
      },
      {
        name: translateService.get('aggregator_max'),
        value: 'max',
      },
    ];
  }

  function getMonitorGroupOption(metric, emptyIncluded = false) {
    const options = [];
    if (emptyIncluded) {
      options.push({
        name: translateService.get('monitor_group_placeholder'),
        value: '',
      });
    }
    const tags = metric.tags || {};
    for (const key in tags) {
      if (!filteredTagsKey.includes(key)) {
        const display_name = TRANSLATE_METRIC_TAG_KEYS.includes(key)
          ? translateService.get(`metric_tag_${key}`)
          : key;
        options.push({
          name: display_name,
          value: key,
        });
      }
    }
    return options;
  }

  // chart releated
  function getChartData({
    query_list,
    metrics,
    timeRange,
    seriesName = '',
    cache = false,
    accumulatedData = false,
  }) {
    if (!query_list) {
      query_list = getMetricsQueryList(metrics);
    }
    // const end = moment().valueOf();
    // const start = end - period;
    return rbMonitorDataService
      .queryMetricsData({
        query_list,
        start: timeRange.start,
        end: timeRange.end,
        cache,
      })
      .then(({ result }) => {
        if (result && result.length) {
          return _parseMetricsToSeries({
            metrics: result,
            seriesName,
            accumulatedData,
          });
        } else {
          return {
            axisOptions: null,
            series: null,
          };
        }
      })
      .catch(() => {});
  }

  function _getChartNameFromTags(result) {
    const tags = result.tags;
    const name = Object.keys(tags)
      .filter(key => {
        return !filteredTagsKey.includes(key);
      })
      .sort(a => {
        if (a === 'instance_id') {
          return 1;
        }
      })
      .map(key => {
        const displayKey = TRANSLATE_METRIC_TAG_KEYS.includes(key)
          ? translateService.get(`metric_tag_${key}`)
          : key;
        const tagDisplayName = getMetricTagItemDisplayName(key, tags[key]);
        return `${displayKey}=${tagDisplayName}`;
      })
      .join(',');
    return name;
  }

  function _parseMetricsToSeries({
    metrics,
    seriesName,
    accumulatedData = false,
  }) {
    const categories = _calculateCategories(metrics);
    let _categories;
    if (accumulatedData) {
      _categories = _.clone(categories);
      categories.splice(0, 1);
    }
    const series = metrics.map((result, index) => {
      const dps = result.dps;
      let data = [];
      if (accumulatedData) {
        data = categories.map((time, index) => {
          if (dps[time] === null || dps[_categories[index]] === null) {
            return null;
          } else if (dps[time] < dps[_categories[index]]) {
            return dps[time];
          } else {
            return (dps[time] || 0) - (dps[_categories[index]] || 0);
          }
        });
      } else {
        data = categories.map(time => {
          return dps[time];
        });
      }
      const name = seriesName
        ? `${seriesName}.${index}`
        : _getChartNameFromTags(result);
      return {
        unit: result.unit,
        tags: result.tags,
        metric_name: result.metric_name,
        name,
        data,
      };
    });
    const axisOptions = _computeAxisOptions(series, categories);

    return { axisOptions, series };
  }

  function _calculateCategories(results) {
    const _result = results[0];
    return Object.keys(_result.dps);
  }

  function _computeAxisOptions(series, categories) {
    const xAxis = {
      categories,
      labels: {
        step: Math.floor(categories.length / 4),
        formatter: function _formatter() {
          return moment(parseInt(this.value) * 1000).format('MM-DD HH:mm');
        },
      },
    };

    const yAxis = {
      max: _getYAxisMaxValue(series),
      min: _getYAxisMinValue(series),
      title: {
        text: '',
      },
      labels: {
        enabled: true,
      },
    };

    const _series = _.uniqWith(series, (a, b) => {
      return a.unit === b.unit;
    });
    if (_series.length === 1) {
      const unit = _series[0]['unit'];
      yAxis.labels.formatter = function _formatter() {
        return _pointFormatter({
          value: this.value,
          unit,
          precision: 2,
        });
      };
    }

    return { xAxis, yAxis };
  }

  function _getYAxisMinValue(series) {
    let data = [];
    _.forEach(series, s => {
      data = data.concat(s.data);
    });
    const min = _.min(data);
    if (min === 0) {
      return 0;
    } else if (min > 0) {
      return min * 0.8;
    } else {
      return min * 1.2;
    }
  }

  function _getYAxisMaxValue(series) {
    let data = [];
    _.forEach(series, s => {
      data = data.concat(s.data);
    });
    const max = _.max(data);
    if (max === 0) {
      return 1;
    } else if (max > 0) {
      return max * 1.2;
    } else {
      return max * 0.8;
    }
  }

  function _pointFormatter({ value, unit, precision = 4 }) {
    let data = value;
    // format data when unit is 'Percent' or 'Bytes'
    if (unit === 'Percent') {
      data = data === 0 ? 0 : (value * 100).toPrecision(precision + 1) + '%';
    } else if (unit === 'Percent__real') {
      data = data === 0 ? 0 : value.toPrecision(precision + 1) + '%';
    } else if (['Bytes', 'Bytes/Second'].includes(unit)) {
      data = $filter('rbBytesFormatter')(value, precision - 1);
    } else {
      data = _.isInteger(value)
        ? value.toLocaleString()
        : value.toFixed(precision);
    }
    return data;
  }

  function getPointFormat(point) {
    const options = point.series.options;
    const tags = _.get(point, 'series.options.tags', []);
    const tagsStr = [];
    Object.keys(tags)
      .filter(key => {
        return !filteredTagsKey.includes(key);
      })
      .sort(a => {
        if (a === 'instance_id') {
          return 1;
        }
      })
      .forEach(key => {
        const display_key = TRANSLATE_METRIC_TAG_KEYS.includes(key)
          ? translateService.get(`metric_tag_${key}`)
          : key;
        tagsStr.push(
          `${display_key}: ${getMetricTagItemDisplayName(key, tags[key])}`,
        );
      });
    const data = _pointFormatter({
      value: point.y,
      unit: options.unit ? options.unit.split('__')[0] : 'Unknown',
    });
    // use custom data in series: point.series.options.customData
    const str = moment(point.category * 1000).format('YYYY-MM-DD HH:mm:ss');
    return `<span style="font-size: 10px">${str}</span>
<br>${translateService.get('data')}: ${data}
<br>${translateService.get('unit')}: ${options.unit}
<br>${tagsStr.join('<br>')}`;
  }

  function getDateTimeOptions() {
    const current_time = moment();
    const start_time = current_time
      .clone()
      .startOf('day')
      .subtract(29, 'days');
    return {
      maxDate: current_time.clone().endOf('day'),
      minDate: start_time,
      icons: {
        date: 'fa fa-calendar',
      },
    };
  }

  function getMonitorChartTypeOptions() {
    return [
      {
        name: translateService.get('chart_type_line'),
        value: 'line',
        html: `<span>${translateService.get(
          'chart_type_line',
        )}<i class="fa fa-line-chart margin-left-2"><span>`,
      },
      {
        name: translateService.get('chart_type_column'),
        value: 'column',
        html: `<span>${translateService.get(
          'chart_type_column',
        )}<i class="fa fa-bar-chart margin-left-2"><span>`,
      },
      {
        name: translateService.get('chart_type_area'),
        value: 'area',
        html: `<span>${translateService.get(
          'chart_type_area',
        )}<i class="fa fa-area-chart margin-left-2"><span>`,
      },
    ];
  }
}
