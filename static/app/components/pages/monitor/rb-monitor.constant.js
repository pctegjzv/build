const TRANSLATE_METRIC_TAG_KEYS = [
  'region_name',
  'app_name',
  'service_name',
  'instance_id',
  'host',
];

const MONITOR_CONSTANTS = {
  TRANSLATE_METRIC_TAG_KEYS,
};

module.exports = MONITOR_CONSTANTS;

angular
  .module('app.components.pages')
  .constant('MONITOR_CONSTANTS', MONITOR_CONSTANTS);
