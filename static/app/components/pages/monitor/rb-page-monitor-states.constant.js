/**
 * Created by liudong on 2017/1/13.
 */
module.exports = {
  baseStateName: 'monitor.dashboard',
  states: [
    {
      name: 'list',
      url: '',
      component: 'rbMonitorDashboardList',
    },
    {
      name: 'state_dashboard_detail',
      url: '/detail/:uuid?display_name?crumbParams',
      component: 'rbMonitorDashboardDetail',
      params: {
        crumbParams: { array: true },
      },
      resolve: {
        uuid: $stateParams => {
          'ngInject';
          return $stateParams['uuid'];
        },
      },
    },
  ],
};

angular
  .module('app.components.pages')
  .constant('rbPageMonitorStates', module.exports);
