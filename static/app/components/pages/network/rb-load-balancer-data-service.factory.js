/**
 * Created by liudong on 2016/11/15.
 */
angular
  .module('app.components.pages')
  .factory('rbLoadBalancerDataService', rbLoadBalancerDataServiceFactory);

function rbLoadBalancerDataServiceFactory(rbHttp, rbAccountService) {
  const namespace = rbAccountService.getCurrentNamespace();
  const LB_URL = `/ajax/load_balancers/${namespace}/`;

  return {
    getLoadBalancers,
    bindService,
  };

  function getLoadBalancers({
    region_name,
    region_id,
    service_id = '',
    detail = false,
    action = 'view',
    frontend = false,
  }) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: LB_URL,
      params: {
        region_name,
        region_id,
        service_id,
        detail,
        action,
        frontend,
      },
    });
  }

  function bindService(name, data) {
    return rbHttp.sendRequest({
      method: 'PUT',
      url: LB_URL + name,
      data,
    });
  }
}
