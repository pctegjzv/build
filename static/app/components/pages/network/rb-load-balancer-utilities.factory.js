/**
 * Created by liudong on 2016/11/17.
 */
const _ = require('lodash');
angular
  .module('app.components.pages')
  .factory('rbLoadBalancerUtilities', rbLoadBalancerUtilities);

function rbLoadBalancerUtilities(
  LOAD_BALANCER_TYPES,
  LOAB_BALANCER_SERVICES_TYPE,
  LOAD_BALANCER_PROTOCOLS,
) {
  return {
    isSupportCheck,
    supportProtocols,
  };

  // check doc:http://confluence.alaudatech.com/pages/viewpage.action?pageId=23370357
  function isSupportCheck(LBDetail, serviceType) {
    const type = _.get(LBDetail, 'type');
    const ver = _.get(LBDetail, 'version');
    switch (serviceType) {
      case LOAB_BALANCER_SERVICES_TYPE.HTTPS:
        return !(type === LOAD_BALANCER_TYPES.SLB && ver === 1);
      case LOAB_BALANCER_SERVICES_TYPE.MULITPLE_SERVICE:
        return (
          [
            LOAD_BALANCER_TYPES.HAPROXY,
            LOAD_BALANCER_TYPES.NGINX,
            LOAD_BALANCER_TYPES.CLB,
          ].includes(type) ||
          (ver === 2 && type === LOAD_BALANCER_TYPES.SLB)
        );
      case LOAB_BALANCER_SERVICES_TYPE.DOMAINSUFFIX:
        return (
          [LOAD_BALANCER_TYPES.HAPROXY, LOAD_BALANCER_TYPES.NGINX].includes(
            type,
          ) ||
          (ver === 2 && type === LOAD_BALANCER_TYPES.SLB)
        );
    }
  }

  function supportProtocols(LBDetail) {
    const type = _.get(LBDetail, 'type');
    const ver = _.get(LBDetail, 'version');
    if (type === LOAD_BALANCER_TYPES.SLB && ver === 1) {
      return LOAD_BALANCER_PROTOCOLS.filter(r => r.name !== 'HTTPS');
    } else {
      return _.cloneDeep(LOAD_BALANCER_PROTOCOLS);
    }
  }
}
