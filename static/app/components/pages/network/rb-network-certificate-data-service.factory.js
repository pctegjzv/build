angular
  .module('app.components.pages')
  .factory('rbCertificateDataService', rbCertificateDataServiceFactory);

function rbCertificateDataServiceFactory(rbHttp, rbAccountService) {
  const namespace = rbAccountService.getCurrentNamespace();
  const CERTIFICATES_URL = `/ajax/certificates/${namespace}/`;

  function getCertificates(params) {
    return rbHttp.sendRequest({
      url: CERTIFICATES_URL,
      params,
    });
  }

  function getServicesInuseCertificate(certName) {
    return rbHttp.sendRequest({
      url: CERTIFICATES_URL + certName,
      params: { with_service: true },
    });
  }

  function deleteCertificateByUuid(cert) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url: CERTIFICATES_URL + cert.uuid,
    });
  }

  function createCertificate(form) {
    return rbHttp.sendRequest({
      method: 'POST',
      url: CERTIFICATES_URL,
      data: form,
    });
  }

  function getConfigName(config) {
    return config.uuid;
  }

  return {
    getCertificates,
    getServicesInuseCertificate,
    deleteCertificateByUuid,
    createCertificate,
    getConfigName,
  };
}
