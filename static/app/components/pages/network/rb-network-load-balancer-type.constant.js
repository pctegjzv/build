const LOAD_BALANCER_TYPES = {
  HAPROXY: 'haproxy',
  NGINX: 'nginx',
  SLB: 'slb',
  ELB: 'elb',
  CLB: 'clb',
};

const LOAB_BALANCER_SERVICES_TYPE = {
  DOMAIN: 'domain',
  MULITPLE_SERVICE: 'mulitple_service',
  HTTPS: 'https',
  DOMAINSUFFIX: 'domainsuffix',
  REG: 'reg',
};

const LOAD_BALANCER_PROTOCOLS = [
  {
    name: 'HTTP',
    value: 'http',
  },
  {
    name: 'TCP',
    value: 'tcp',
  },
  {
    name: 'HTTPS',
    value: 'https',
  },
];

module.exports = LOAD_BALANCER_TYPES;

angular
  .module('app.components.pages')
  .constant('LOAD_BALANCER_TYPES', LOAD_BALANCER_TYPES)
  .constant('LOAB_BALANCER_SERVICES_TYPE', LOAB_BALANCER_SERVICES_TYPE)
  .constant('LOAD_BALANCER_PROTOCOLS', LOAD_BALANCER_PROTOCOLS);
