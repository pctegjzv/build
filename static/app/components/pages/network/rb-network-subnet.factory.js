/**
 * Created by zhouwenbo on 17/4/4.
 */
angular
  .module('app.components.pages')
  .factory('rbNetworkSubnetService', rbNetworkSubnetService);

function rbNetworkSubnetService(rbHttp, rbAccountService) {
  const namespace = rbAccountService.getCurrentNamespace();
  const SUBNETS_URL = `/ajax/subnets/${namespace}/`;

  return {
    addSubnet,
    deleteSubnet,
    updateSubnet,
    getSubnetList,
    getSubnetDetail,
    getSubnetIps,
    loadSubnetIps,
    deleteSubnetIp,
  };

  function addSubnet(data) {
    return rbHttp.sendRequest({
      method: 'POST',
      url: SUBNETS_URL,
      data,
    });
  }

  function deleteSubnet(params) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url: SUBNETS_URL + params.subnet_name,
    });
  }

  function updateSubnet(params, data) {
    return rbHttp.sendRequest({
      method: 'PUT',
      url: SUBNETS_URL + params.subnet_name,
      data,
    });
  }

  function getSubnetList(params) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: SUBNETS_URL,
      params,
    });
  }

  function getSubnetDetail(params) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: SUBNETS_URL + params.subnet_name,
    });
  }

  function getSubnetIps(params) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: SUBNETS_URL + params.subnet_name + '/private_ips',
      params,
    });
  }

  function loadSubnetIps(params, data) {
    return rbHttp.sendRequest({
      method: 'POST',
      url: SUBNETS_URL + params.subnet_name + '/private_ips',
      data,
    });
  }

  function deleteSubnetIp(params) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url:
        SUBNETS_URL + params.subnet_name + '/private_ips/' + params.private_ip,
    });
  }
}
