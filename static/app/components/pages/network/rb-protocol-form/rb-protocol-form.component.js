/**
 * containerPorts: container expose ports, get from parent component, should be number of array
 * protocols: current supported ALB protocol, dropdown options of array
 * listeners: get or set HaProxy listeners, listener object of array
 *
 * usage:
 * let protocols = [{name:"HTTP",value:'http'}]
 * let listeners = [{listener_port:'',container_ports:80,protocol:'http',domains:[""]}]
 * <rb-protocol-form container-ports="vm.containerPorts" protocols="protocols" listeners="listeners">
 */
import * as _ from 'lodash';
import { resource_types as RESOURCE_TYPES } from '../../rbac_role/rb-rbac-helper.constant';
const templateStr = require('app/components/pages/network/rb-protocol-form/rb-protocol-form.html');
angular.module('app.components.common').component('rbProtocolForm', {
  bindings: {
    containerPorts: '<',
    protocols: '<',
    listeners: '<',
    loadBalancerDetail: '<',
  },
  controller: rbProtocolFormController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbProtocolFormController(
  rbCertificateDataService,
  rbRoleUtilities,
  LOAB_BALANCER_SERVICES_TYPE,
  rbLoadBalancerUtilities,
) {
  const vm = this;
  const defaultDomain = 'myalauda.cn';
  let isInit = true;

  vm.$onInit = _init;
  vm.addListener = addListener;
  vm.deleteListener = deleteListener;
  vm.$onChanges = _onChanges;
  vm.protocolChanged = protocolChanged;
  vm.onDomainsChange = onDomainsChange;
  vm.isHttp = isHttp;
  vm.domainValidateFn = domainValidateFn;
  vm.containerPortChange = containerPortChange;
  vm.certChange = certChange;
  vm.httpsVaild = httpsVaild;

  // page size = 0, get cert list all
  function _init() {
    if (vm.listeners && vm.listeners.length) {
      vm.listeners.map(listener => {
        if (listener.rules) {
          listener.domains = listener.rules.map(r => {
            return `${r.domain}${r.url}`;
          });
        }
        const domainInfo = listener.domain_info || [];
        const customDomains = domainInfo
          .filter(item => item.type === 'loadbalancer-domain')
          .map(item => item.domain);

        if (listener.domains && listener.domains.length) {
          listener.domains = _.uniq(
            listener.domains.filter(domain => {
              return (
                domain.indexOf(defaultDomain) === -1 &&
                customDomains.indexOf(domain) < 0
              );
            }),
          );
        } else {
          listener.domains = [];
        }
        return listener;
      });
    }
  }

  function getCertList() {
    return rbCertificateDataService
      .getCertificates({
        page_size: 0,
        order_by: '-created_at',
        load_balancer_id: _.get(vm.loadBalancerDetail, 'load_balancer_id'),
      })
      .then(data => {
        const tempList =
          data &&
          data.results
            .filter(c =>
              rbRoleUtilities.resourceHasPermission(
                c,
                RESOURCE_TYPES.CERTIFICATE,
                'bind',
              ),
            )
            .map(item => {
              return {
                name: item.name,
                value: item.name,
                certificate_id: item.uuid,
              };
            });
        vm.certList = tempList;
        vm.initialized = true;
      });
  }

  function isSupportUrl() {
    return rbLoadBalancerUtilities.isSupportCheck(
      vm.loadBalancerDetail,
      LOAB_BALANCER_SERVICES_TYPE.HTTPS,
    );
  }

  function _onChanges({ loadBalancerDetail }) {
    if (vm.containerPorts.length && isInit && !vm.listeners.length) {
      vm.containerPorts.forEach(port => {
        vm.listeners.push({
          listener_port: '',
          container_port: port,
          protocol: port === 80 ? 'http' : 'tcp',
          domains: [],
          certificate_name: '',
          certificate_id: '',
        });
      });
      isInit = false;
    }
    if (loadBalancerDetail && loadBalancerDetail.currentValue) {
      getCertList();
    }
  }

  function addListener() {
    const containerPort = vm.containerPorts.length ? vm.containerPorts[0] : '';
    const protocol = containerPort === 80 ? 'http' : 'tcp';
    vm.listeners.push({
      listener_port: '',
      container_port: containerPort,
      protocol,
      domains: [],
      certificate_name: '',
      certificate_id: '',
    });
  }

  function deleteListener($index) {
    vm.listeners.splice($index, 1);
  }

  function protocolChanged(item, option) {
    if (item.protocol === option.value) {
      return;
    }
    item.protocol = option.value;
    item.domains = [];
    if (option.value === 'tcp' || option.value === 'http') {
      item.certificate_name = '';
    } else {
      item.certificate_name = vm.certList.length ? vm.certList[0].value : '';
    }
  }

  function containerPortChange(item, option) {
    if (option.value === null) {
      item.container_port = undefined;
    } else {
      item.container_port = option.value;
    }
  }

  function isHttp(item) {
    return item.protocol === 'http' || item.protocol === 'https';
  }

  function httpsVaild(item) {
    return item.protocol === 'https' && item.domains.length === 0;
  }

  function onDomainsChange(item, tags) {
    item.domains = _.cloneDeep(tags).map(t => t.toLowerCase());
  }

  function domainValidateFn(domain) {
    const splitChar = domain.indexOf('^') !== -1 ? '^' : '/';
    const dm = domain.split(splitChar)[0];
    const rule = domain.replace(dm, '');
    if (dm.length > 80 || rule.length > 80) {
      return false;
    }
    try {
      new RegExp(rule);
    } catch (e) {
      return false;
    }
    const urlTest = /^[0-9A-Za-z\-/._]+$/;
    const domainTest = /^[0-9A-Za-z\-.]+$/;
    const regexURLTest = /^[0-9A-Za-z\-.]+$/;
    const reg = isSupportUrl() ? urlTest : domainTest;
    return splitChar === '^'
      ? dm
        ? regexURLTest.test(dm)
        : true
      : reg.test(domain);
  }

  function certChange(item, option) {
    if (item.protocol === 'https' && option.value === '') {
      item.certificate_name = vm.certList.length ? vm.certList[0].value : '';
      return;
    }
    item.certificate_name = option.value;
    item.certificate_id = option.certificate_id;
  }
}
