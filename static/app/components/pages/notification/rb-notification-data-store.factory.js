angular
  .module('app.components.pages')
  .factory('rbNotificationDataStore', rbNotificationDataStoreFactory);

/**
 * Observable data store for registry
 */
function rbNotificationDataStoreFactory(RbFetchDataStore, notificationService) {
  const fetchRequest = () => notificationService.notificationList();

  class NotificationDataStore extends RbFetchDataStore {
    constructor() {
      super(fetchRequest);
    }
  }
  return new NotificationDataStore();
}
