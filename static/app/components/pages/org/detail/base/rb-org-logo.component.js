const templateStr = require('app/components/pages/org/detail/base/rb-org-logo.html');
angular.module('app.components.pages').component('rbOrgLogo', {
  controller: rbOrgLogoController,
  controllerAs: 'vm',
  template: templateStr,
  bindings: {
    org: '<',
    canEdit: '<',
  },
});

function rbOrgLogoController(
  rbAccountService,
  rbToast,
  $element,
  rbSafeApply,
  translateService,
) {
  const vm = this;
  vm.$onInit = _init;
  vm.$postLink = _link;

  function _init() {
    vm.preview = false;
    vm.filePath = vm.org.logo_file;
    vm.fileObj = null;
    vm.submitting = false;
  }

  vm.cancelUpdate = () => {
    vm.filePath = vm.org.logo_file;
    vm.preview = false;
    vm.fileObj = null;
    const fileInput = $element.find('.org-logo-choose-file').eq(0);
    fileInput.val('');
    rbSafeApply();
  };

  vm.update = () => {
    if (vm.fileObj) {
      vm.submitting = true;
      rbAccountService
        .updateNamespaceLogo(vm.org.name, vm.fileObj)
        .then(() => {
          rbToast.success(translateService.get('org_logo_update_success'));
          vm.preview = false;
          vm.fileOjb = null;
        })
        .catch(() => {})
        .then(() => {
          vm.submitting = false;
        });
    }
  };

  function _link() {
    const fileInput = $element.find('.org-logo-choose-file').eq(0);
    $element.delegate('.org-edit-logo', 'click', () => {
      fileInput.click();
    });
    fileInput.on('change', fileChoosed);

    function fileChoosed() {
      const files = fileInput.get(0).files;
      if (files && files.length) {
        const file = files[0];
        const reader = new FileReader();
        reader.onload = e => {
          vm.preview = true;
          vm.filePath = e.target.result;
          vm.fileObj = file;
          rbSafeApply();
        };
        reader.readAsDataURL(file);
      }
    }
  }
}
