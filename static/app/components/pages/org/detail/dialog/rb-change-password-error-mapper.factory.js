angular
  .module('app.components.pages')
  .factory('rbChangePasswordErrorMapper', rbChangePasswordErrorMapper);

function rbChangePasswordErrorMapper(rbPatternHelper, translateService) {
  const passwordPattern = rbPatternHelper.getPasswordPattern();
  return {
    map: (key, error, control) => {
      let translateKey = '';
      if (key === 'required') {
        translateKey =
          control.name === 'old_password'
            ? 'old_password_not_empty'
            : 'password_not_empty';
      } else {
        switch (key) {
          case 'same_password':
            translateKey = 'password_not_same';
            break;
          case 'minlength':
            translateKey = passwordPattern.errorMin;
            break;
          case 'maxlength':
            translateKey = passwordPattern.errorMax;
            break;
          case 'pattern':
            translateKey = passwordPattern.errorPattern;
            break;
          case 'strength':
            translateKey = passwordPattern.errorStrength;
            break;
        }
      }
      return translateService.get(translateKey);
    },
  };
}
