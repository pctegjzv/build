const templateStr = require('app/components/pages/org/detail/dialog/rb-company-update.html');
angular.module('app.components.pages').component('rbCompanyUpdate', {
  controller: rbCompanyUpdateController,
  controllerAs: 'vm',
  bindings: {
    formerCompanyName: '<',
    orgName: '<',
  },
  template: templateStr,
});

function rbCompanyUpdateController(
  rbToast,
  rbModal,
  translateService,
  orgService,
  rb2DefaultErrorMapperService,
) {
  const vm = this;

  vm.confirm = () => {
    vm.updateCompanyForm.$setSubmitted();
    if (vm.updateCompanyForm.$invalid) {
      return;
    }
    _updateOrg();
  };

  vm.errorMapper = {
    map: (key, error, control) => {
      if (key === 'pattern') {
        return translateService.get('company_name_invalid');
      } else {
        return rb2DefaultErrorMapperService.map(key, error, control);
      }
    },
  };

  vm.cancel = () => {
    rbModal.cancel();
  };

  function _updateOrg() {
    vm.submitting = true;
    orgService
      .updateOrg(vm.orgName, vm.companyName)
      .then(data => {
        rbToast.success(translateService.get('update_company_success'));
        rbModal.hide(data);
      })
      .catch(() => {})
      .then(() => {
        vm.submitting = false;
      });
  }
}
