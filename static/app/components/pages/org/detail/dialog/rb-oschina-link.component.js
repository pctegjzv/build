/**
 * Created by zhouwenbo on 17/2/20.
 */
const templateStr = require('app/components/pages/org/detail/dialog/rb-oschina-link.html');
angular.module('app.components.pages').component('rbOschinaLink', {
  controller: rbOschinaLinkController,
  controllerAs: 'vm',
  bindings: {
    codeClientName: '<',
    state: '<',
    beforeLink: '<', // Call back to be invoked before sending link request
  },
  template: templateStr,
});

function rbOschinaLinkController(
  translateService,
  rbBuildDataService,
  rbToast,
  rbModal,
) {
  const vm = this;

  // info form
  vm.info = {
    infoName: '',
    infoPass: '',
  };

  vm.confirm = () => {
    vm.form.$setSubmitted();
    if (vm.form.$invalid) {
      return;
    }
    vm.beforeLink();
    vm.submitting = true;
    rbBuildDataService
      .linkPrivateBuildCodeClient({
        codeClientName: vm.codeClientName,
        state: vm.state,
        oschinaUserEmail: vm.info.infoName,
        oschinaUserPwd: vm.info.infoPass,
      })
      .then(() => {
        vm.submitting = true;
        rbToast.success(translateService.get('oschina_link_success'));
        rbModal.hide();
        //绑定=》解除绑定
      })
      .catch(() => {
        //Do some
      })
      .then(() => {
        vm.submitting = false;
      });
  };

  vm.cancel = () => {
    rbModal.cancel();
  };
}
