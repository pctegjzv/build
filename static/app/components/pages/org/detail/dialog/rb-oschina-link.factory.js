/**
 * Created by zhouwenbo on 17/3/7.
 */
angular
  .module('app.components.pages')
  .factory('rbOschinaLinkDialog', rbOschinaLinkDialog);

function rbOschinaLinkDialog(rbModal, translateService, rbAccountService) {
  return {
    show: ({ oauthCodeClient, beforeLink }) => {
      return rbModal
        .show({
          title: translateService.get('oschina_link'),
          component: 'rbOschinaLink',
          locals: {
            codeClientName: oauthCodeClient.name,
            state: rbAccountService.getCurrentNamespace(),
            beforeLink,
          },
        })
        .catch(() => {});
    },
  };
}
