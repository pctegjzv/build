/**
 * Created by liudong on 2016/12/7.
 */
const teamplateStr = require('app/components/pages/org/detail/dialog/rb-profile-update.html');
angular.module('app.components.pages').component('rbProfileUpdate', {
  controller: rbProfileUpdateController,
  controllerAs: 'vm',
  bindings: {
    field: '<',
    profile: '<',
  },
  template: teamplateStr,
});

function rbProfileUpdateController(
  rbAccountService,
  rbModal,
  rbToast,
  translateService,
) {
  const vm = this;
  vm.model = {};
  vm.model[vm.field] = vm.profile[vm.field];
  vm.submitting = false;

  vm.cancel = cancel;
  vm.save = save;

  //////////
  function cancel() {
    rbModal.cancel();
  }

  function save() {
    vm.form.$setSubmitted();
    if (!vm.form.$valid) {
      return;
    }
    vm.submitting = true;
    rbAccountService
      .updateProfile(vm.model)
      .then(data => {
        rbToast.success(translateService.get('update_success'));
        rbModal.hide(data);
      })
      .catch(() => {})
      .then(() => {
        vm.submitting = false;
      });
  }
}
