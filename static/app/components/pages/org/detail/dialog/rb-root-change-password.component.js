const templateStr = require('app/components/pages/org/detail/dialog/rb-root-change-password.html');
angular.module('app.components.pages').component('rbRootChangePassword', {
  controller: rbRootChangePasswordController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbRootChangePasswordController(
  rbModal,
  translateService,
  rbAccountService,
  rbToast,
  rbChangePasswordErrorMapper,
  rbPatternHelper,
) {
  const vm = this;
  vm.$onInit = _init;

  function _init() {
    vm.passwordPatternObj = rbPatternHelper.getPasswordPattern();
    vm.getPasswordStrength = rbPatternHelper.getPasswordStrength;
    vm.errorMapper = rbChangePasswordErrorMapper;
  }

  vm.checkPasswordStrength = value =>
    !value || vm.getPasswordStrength(value) >= vm.passwordPatternObj.strength;

  vm.validateSamePassword = () => {
    vm.passwordNotSame =
      vm.confirm_new_password && vm.new_password !== vm.confirm_new_password;
  };

  vm.confirm = () => {
    vm.updatePasswordForm.$setSubmitted();
    if (vm.updatePasswordForm.$invalid) {
      return;
    }
    if (vm.new_password !== vm.confirm_new_password) {
      return;
    }
    const params = {
      password: vm.new_password,
      old_password: vm.old_password,
    };
    vm.submitting = true;
    rbAccountService
      .updateProfile(params)
      .then(
        () => {
          rbToast.success(translateService.get('update_password_success'));
          rbModal.hide();
        },
        obj => {
          const errors = obj.data.errors;
          if (errors.length > 0) {
            rbToast.error(translateService.get(errors[0]['code']));
          }
        },
      )
      .catch(() => {})
      .then(() => {
        vm.submitting = false;
      });
  };

  vm.cancel = () => {
    rbModal.cancel();
  };
}
