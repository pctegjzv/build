const templateStr = require('app/components/pages/org/detail/rb-org-detail.html');
angular.module('app.components.pages').component('rbOrgDetail', {
  controller: rbOrgDetailController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbOrgDetailController(
  orgService,
  translateService,
  rbAccountService,
  WEBLABS,
  rbModal,
  rbSafeApply,
  rb2RouterUtil,
) {
  const vm = this;
  vm.$onInit = _init;

  async function _init() {
    rb2RouterUtil.go('rbac');
    vm.orgName = rbAccountService.getCurrentNamespace();
    const userName = rbAccountService.getCurrentUsername();
    vm.accountInfo = userName ? `${userName}@${vm.orgName}` : vm.orgName;

    vm.editingOrgCompany = false;
    vm.editingOrgLogo = false;
    vm.isSubAccount = rbAccountService.isSubAccount();
    vm.weblabs = WEBLABS;
    vm.initialized = false;
    await getOrgDetail();
    vm.initialized = true;
  }

  vm.updateCompany = () => {
    rbModal
      .show({
        title: translateService.get('update_company_name'),
        component: 'rbCompanyUpdate',
        locals: {
          formerCompanyName: vm.orgCompany,
          orgName: vm.orgName,
        },
      })
      .then(data => {
        vm.org = data;
        rbSafeApply();
      })
      .catch(() => {});
  };

  function getOrgDetail() {
    return orgService
      .getOrgDetail(vm.orgName)
      .then(data => {
        vm.org = data;
        vm.orgCompany = data.company;
      })
      .catch(() => {});
  }

  vm.gotoUserDetail = () => rb2RouterUtil.go('user.detail');
}
