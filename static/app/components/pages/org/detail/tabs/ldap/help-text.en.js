export const DESCRIPTION = `
**When synchronizing user names, adding custom filter conditions under the same "object type" and "login field" is supported. 
LDAP syntax is also supported in filter conditions. The following provides some basic examples.
**

- **Equal**: (attribute=abc), e.g.(&(objectclass=user)(displayName=Foeckeler)
- **Negative**: (!(attribute=abc)), e.g.(!objectClass=group)
- **Existence**: (attribute=\\*), e.g.(mailNickName=\\*)
- **Not equal to**: (!(attribute=\\*)), e.g.(!proxyAddresses=\\*)
- **Greater than**: (attribute>=abc), e.g.(mdbStorageQuota>=100000)
- **Less than**: (attribute<=abc), e.g.(mdbStorageQuota<=100000)
- **Approximate**: (attribute~=abc), e.g.(displayName~=Foeckeler) 
- **Or**：e.g.(|(uid=james\\*)(uid=john\\*))
- **And**：e.g (&(cn=Test\\*)(cn=Admin\\*)))
- **Wildcards**: e.g. (sn=F\\*) or (mail=\\*@cerrotorre.de) or (givenName=\\*Paul\\*)

<a target="_blank" href="https://technet.microsoft.com/en-us/library/aa996205(v=exchg.65).aspx">To learn more</a>
`;

export default {
  DESCRIPTION,
};
