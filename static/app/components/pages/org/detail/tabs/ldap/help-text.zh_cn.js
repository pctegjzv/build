export const DESCRIPTION = `
**在同步用户名时支持在相同“对象类型”和“登陆字段”下添加自定义过滤条件，过滤条件支持LDAP语法，下面提供了一些基本例子。
**

- **等于**: (attribute=abc), e.g.(&(objectclass=user)(displayName=Foeckeler)
- **否定**: (!(attribute=abc)), e.g.(!objectClass=group)
- **存在**: (attribute=\\*), e.g.(mailNickName=\\*)
- **不存在**: (!(attribute=\\*)), e.g.(!proxyAddresses=\\*)
- **大于**: (attribute>=abc), e.g.(mdbStorageQuota>=100000)
- **小于**: (attribute<=abc), e.g.(mdbStorageQuota<=100000)
- **接近**: (attribute~=abc), e.g.(displayName~=Foeckeler) 
- **或者**：e.g.(|(uid=james\\*)(uid=john\\*))
- **与**：e.g (&(cn=Test\\*)(cn=Admin\\*)))
- **通配符**: e.g. (sn=F\\*) or (mail=\\*@cerrotorre.de) or (givenName=\\*Paul\\*)

<a target="_blank" href="https://technet.microsoft.com/en-us/library/aa996205(v=exchg.65).aspx">了解更多</a>
`;

export default {
  DESCRIPTION,
};
