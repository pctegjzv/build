import HELP_TEXTS_EN from './help-text.en';
import HELP_TEXTS_CN from './help-text.zh_cn';
const templateStr = require('app/components/pages/org/detail/tabs/ldap/rb-org-ldap.html');
angular.module('app.components.pages').component('rbOrgLdap', {
  controller: rbOrgLdapController,
  controllerAs: 'vm',
  bindings: {
    orgName: '<',
  },
  template: templateStr,
});

function rbOrgLdapController(
  orgService,
  ORG_CONSTANTS,
  translateService,
  rbMarkdownInfoDialog,
) {
  const vm = this;
  vm.$onInit = _init;
  vm.$onDestroy = _destroy;

  vm.selectLdapType = selectLdapType;
  vm.editLdapConfig = editLdapConfig;
  vm.getTaskHelpTextPreview = getTaskHelpTextPreview;
  vm.helpLinkClicked = helpLinkClicked;
  vm.save = save;
  vm.cancel = cancel;

  vm.selectLdapAzureCountry = selectLdapAzureCountry;

  //////////
  async function _init() {
    vm.HELP_TEXTS =
      translateService.getLanguage() === 'zh_cn'
        ? HELP_TEXTS_CN
        : HELP_TEXTS_EN;
    vm.AzureCountrys = [
      {
        name: translateService.get('org_ldap_account_country_cn'),
        value: 'CN',
      },
      {
        name: translateService.get('org_ldap_account_country_us'),
        value: 'US',
      },
    ];
    vm.submitting = false;
    vm.ldapConfigLoading = true;
    vm.orgLdapPageMap = ORG_CONSTANTS.ORG_LDAP_PAGE_MAP;
    try {
      vm.ldapConfig = await orgService.getOrgLdap();
    } catch (e) {
      vm.ldapConfig = null;
    }
    vm.ldapConfigLoading = false;
    vm.ldapMode =
      vm.ldapConfig && !vm.ldapConfig.empty
        ? ORG_CONSTANTS.ORG_LDAP_PAGE_MAP.DISPLAY
        : ORG_CONSTANTS.ORG_LDAP_PAGE_MAP.CREATE;
    if (vm.ldapMode === ORG_CONSTANTS.ORG_LDAP_PAGE_MAP.CREATE) {
      selectLdapType('OpenLDAP');
    }
  }

  function _destroy() {}

  function selectLdapType(type = 'OpenLDAP') {
    if (!vm.ldapConfig || vm.ldapConfig.type !== type) {
      vm.ldapConfig = ORG_CONSTANTS.ORG_LDAP_TYPE_MAP[type];
    }
  }

  function selectLdapAzureCountry(option) {
    vm.ldapConfig.config.country = option.value;
  }

  function editLdapConfig() {
    vm.ldapMode = ORG_CONSTANTS.ORG_LDAP_PAGE_MAP.UPDATE;
  }
  function getHelpText() {
    return vm.HELP_TEXTS['DESCRIPTION'];
  }
  function getTaskHelpTextPreview() {
    const helpText = getHelpText();
    return helpText.replace(/\s+/g, ' ');
  }
  function helpLinkClicked() {
    const title = translateService.get('ldap_filter_condition_demo');
    rbMarkdownInfoDialog.show({
      md: getHelpText(),
      title,
    });
  }

  function save() {
    vm.ldapConfigForm.$setSubmitted();
    if (vm.ldapConfigForm.$invalid) {
      return;
    }
    vm.submitting = true;
    if (vm.ldapMode === ORG_CONSTANTS.ORG_LDAP_PAGE_MAP.CREATE) {
      orgService
        .createOrgLdap({ config: vm.ldapConfig, orgName: vm.orgName })
        .then(() => {
          _init(true);
        })
        .catch(() => {})
        .then(() => {
          vm.submitting = false;
        });
    } else if (vm.ldapMode === ORG_CONSTANTS.ORG_LDAP_PAGE_MAP.UPDATE) {
      orgService
        .updateOrgLdap({ config: vm.ldapConfig, orgName: vm.orgName })
        .then(() => {
          _init(true);
        })
        .catch(() => {})
        .then(() => {
          vm.submitting = false;
        });
    }
  }

  function cancel() {
    vm.ldapMode = ORG_CONSTANTS.ORG_LDAP_PAGE_MAP.DISPLAY;
  }
}
