// 页面状态：已配置，创建，更新
const ORG_LDAP_PAGE_MAP = {
  DISPLAY: 'initial',
  CREATE: 'create',
  UPDATE: 'update',
};

const ORG_LDAP_TYPE_MAP = {
  OpenLDAP: {
    type: 'OpenLDAP',
    config: {
      host: '',
      port: '',
      tls: false,
      service_account: '',
      service_password: '',
      search_base: 'dc=example,dc=org',
      server_cert: '',
      client_key: '',
      domain: '',
    },
    schema: {
      users: {
        class_type: 'inetOrgPerson',
        login_field: 'uid',
        search: '',
      },
    },
    test: {
      username: '',
      password: '',
    },
  },
  ActiveDirectory: {
    type: 'ActiveDirectory',
    config: {
      host: '',
      port: '',
      tls: false,
      service_account: '',
      service_password: '',
      search_base: 'dc=example,dc=org',
      domain: '',
    },
    schema: {
      users: {
        class_type: 'person',
        login_field: 'sAMAccountName',
        search: '',
      },
    },
    test: {
      username: '',
      password: '',
    },
  },
  AzureAD: {
    type: 'AzureAD',
    config: {
      service_account: '',
      service_password: '',
      tenant_id: '',
      country: '',
      domain: '',
    },
    schema: {},
    test: {
      username: '',
      password: '',
    },
  },
};

const ORG_LDAP_STATUS_MAP = {
  bare: 'bare',
  synced: 'synced',
  syncing: 'syncing',
  failure: 'failure',
};

//const ORG_LDAP_

const ORG_CONSTANTS = {
  ORG_LDAP_PAGE_MAP,
  ORG_LDAP_TYPE_MAP,
  ORG_LDAP_STATUS_MAP,
};

module.exports = ORG_CONSTANTS;

angular.module('app.components.pages').constant('ORG_CONSTANTS', ORG_CONSTANTS);
