module.exports = {
  baseStateName: 'org',
  redirectTo: 'org.detail',
  states: [
    {
      name: 'role_create',
      url: '/role_create?:fromRole',
      component: 'rbRoleCreate',
      resolve: {
        fromRole: $stateParams => {
          'ngInject';
          return $stateParams['fromRole'];
        },
      },
    },
    {
      name: 'role_detail',
      url: '/role_detail/:roleName',
      component: 'rbRoleDetail',
      resolve: {
        roleName: $stateParams => {
          'ngInject';
          return $stateParams['roleName'];
        },
      },
    },
    {
      name: 'detail',
      url: '',
      component: 'rbOrgDetail',
    },
    {
      name: 'accounts',
      url: '/accounts',
      component: 'rbAccountList',
    },
  ],
};

angular
  .module('app.components.pages')
  .constant('rbPageOrgStates', module.exports);
