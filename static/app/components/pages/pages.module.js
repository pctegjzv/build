const req = require.context('.', true, /states.constant.js$/);

angular
  .module('app.components.pages', ['app.core'])
  .config(registerPageStates)
  .run(initTransitionHooks);

function registerPageStates($stateProvider) {
  // Special rule for project_detail
  $stateProvider.state('project_detail', {
    url: '/project_detail/:project_name',
    redirectTo: 'project_detail.dashboard',
  });

  let allStates = req.keys().map(path => req(path));
  allStates = _.flatten(allStates);
  allStates.forEach(stateConfig => {
    _registerNestedPageState(stateConfig);
  });

  //////////

  function _registerNestedPageState(pageStates) {
    // Automatically register parent states:
    if (pageStates.baseStateName) {
      registerBaseState(
        pageStates.baseStateName,
        pageStates.data,
        pageStates.redirectTo,
      );
    }

    pageStates.states.forEach(pageState => {
      _registerPageState(pageState, pageStates.baseStateName);
    });
  }

  function registerBaseState(
    baseStateName,
    data,
    redirectTo = transition => defaultRedirectTo(transition),
  ) {
    const subStates = baseStateName.split('.').reduce((accum, curr) => {
      let stateName = curr;
      if (accum.length > 0) {
        stateName = accum[accum.length - 1][0] + '.' + stateName;
      }
      return [...accum, [stateName, curr]];
    }, []);

    subStates.forEach(([name, url]) => {
      const state = {
        url: '/' + url,
        redirectTo,
        data,
      };

      // Skip base state register if already registered
      if (!$stateProvider.$get().get(name)) {
        $stateProvider.state(name, state);
      }
    });

    //////////
    function defaultRedirectTo(transition) {
      const allStates = transition.router.stateService.get();
      const originToStateName = transition.to().name;
      const redirectState = allStates.find(state =>
        state.name.startsWith(originToStateName + '.'),
      );
      return redirectState ? redirectState.name : 'home';
    }
  }

  function _registerPageState(pageState, parent) {
    const view = {};

    // Predefined values
    const state = Object.assign(
      {
        reloadOnSearch: true,
        views: {
          '@': view,
        },
      },
      pageState,
    );

    // Remove view related properties since we are attaching them to '@'
    // It may be safe to keep them, but may cause confusion when debugging states
    [
      'component',
      'controller',
      'templateUrl',
      'template',
      'controllerAs',
    ].forEach(key => delete state[key]);

    // Attach view related configs
    if (pageState.component) {
      // Use component instead of controller/template
      view.component = pageState.component;
    } else {
      // For legacy views:
      view.controller = pageState.controller;
      view.templateUrl = pageState.templateUrl;
      view.template = pageState.template;
      if (
        !pageState.controllerAs &&
        (!pageState.controller || pageState.controller.indexOf(' as ') === -1)
      ) {
        view.controllerAs = 'vm';
      }
    }

    $stateProvider.state(
      parent ? parent + '.' + pageState.name : pageState.name,
      state,
    );
  }
}

function initTransitionHooks($transitions, rbSidePanel) {
  $transitions.onFinish({}, () => {
    if (rbSidePanel.contentExists) {
      rbSidePanel.close();
    }
  });
}
