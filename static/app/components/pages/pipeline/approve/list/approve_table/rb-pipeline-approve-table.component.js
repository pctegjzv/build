import templateStr from 'app/components/pages/pipeline/approve/list/approve_table/rb-pipeline-approve-table.html';

angular.module('app.components.pages').component('rbPipelineApproveTable', {
  controller: rbPipelineApproveTableController,
  controllerAs: 'vm',
  template: templateStr,
  bindings: {
    type: '<',
  },
});

function rbPipelineApproveTableController(
  rbSafeApply,
  rbPipelineDataService,
  rbProjectService,
  $log,
  $interval,
  rbConfirmBox,
  translateService,
) {
  const vm = this;
  let timer = null;
  vm.loading = false;
  vm.loadError = false;
  vm.list = [];
  vm.pagination = {
    pageSize: 20,
    pageno: 1,
    totalItems: 0,
  };

  vm.$onInit = onInit;
  vm.$onDestroy = onDestroy;
  vm.fetchData = fetchData;
  vm.approve = approve;
  vm.getConfigName = getConfigName;

  function onInit() {
    vm.projectName = rbProjectService.get();
    fetchData();
    rbSafeApply();
  }

  async function fetchData(
    pageno = vm.pagination.pageno,
    size = vm.pagination.pageSize,
  ) {
    vm.loading = true;
    try {
      const { total, data } = await rbPipelineDataService.getApproveList({
        type: vm.type,
        pageno,
        size,
        project_name: vm.projectName,
      });
      vm.pagination.totalItems = total;
      vm.list = data;
      polling();
    } catch (error) {
      vm.loadError = true;
    }
    vm.loading = false;
    rbSafeApply();
  }

  function polling() {
    $interval.cancel(timer);
    timer = $interval(() => {
      fetchData();
    }, 30 * 1000);
  }

  async function approve(action, item) {
    try {
      await rbConfirmBox.show({
        title: translateService.get(`pipeline_approval_${action}_title`),
        textContent: translateService.get(
          `pipeline_approval_${action}_content`,
          {
            name: item.pipeline_name,
          },
        ),
      });
    } catch (error) {
      return false;
    }
    try {
      await rbPipelineDataService.approve({
        action,
        name: item.pipeline_name,
        history_id: item.history_uuid,
        task_id: item.task_uuid,
      });
      this.fetchData();
    } catch (error) {
      $log.error('Approve failed', 'put', error);
    }
  }

  function getConfigName(item) {
    return rbPipelineDataService.getConfigName({
      name: item.pipeline_name,
      uuid: item.pipeline_uuid,
    });
  }

  function onDestroy() {
    $interval.cancel(timer);
  }
}
