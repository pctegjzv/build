const templateStr = require('app/components/pages/pipeline/approve/list/rb-pipeline-approve-list.html');
angular.module('app.components.pages').component('rbPipelineApproveList', {
  controller: rbPipelineApproveListController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbPipelineApproveListController() {
  const vm = this;
  vm.type = 'approver';

  vm.$onInit = onInit;
  vm.onTabSelected = onTabSelected;

  function onInit() {
    vm.initialized = false;
    setTimeout(() => {
      vm.initialized = true;
    }, 1000);
  }

  function onTabSelected(type = '') {
    vm.type = type;
  }
}
