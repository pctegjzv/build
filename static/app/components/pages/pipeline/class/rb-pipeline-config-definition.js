import { RbPipelineConfigTriggers } from './rb-pipeline-config-trigger-definition';
import { RbPipelineConfigStage } from './rb-pipeline-config-stage-definition';
import { RbPipelineConfigTask } from './rb-pipeline-config-task-definition';

export class RbPipelineConfig {
  /**
   * Defines the primitive fields of a config
   */
  static get PRIMITIVE_FIELDS() {
    return [
      'name',
      'description',
      'uuid',
      'created_by',
      'created_at',
      'updated_at',
      'privilege',
      'resource_actions',
      'space_name',
      'artifact_enabled',
      'shared_dir_enabled',
    ];
  }

  constructor({ triggers = {}, stages = [], on_end = [], ...otherProperties }) {
    this.triggers = new RbPipelineConfigTriggers(triggers);

    // Pipeline stages. There is only one stage in P2
    this.stages = stages.map(stage => new RbPipelineConfigStage(stage));

    // Add an empty notification task if the list is empty
    if (on_end.length === 0) {
      on_end = [
        {
          name: 'task-final',
          type: 'notification',
          order: 1,
        },
      ];
    }

    // Tasks to run at the end of pipeline
    // For P2, there should be only one notification task
    this.on_end = on_end.map(task => new RbPipelineConfigTask(task));

    RbPipelineConfig.PRIMITIVE_FIELDS.forEach(field => {
      if (otherProperties[field] !== undefined) {
        this[field] = otherProperties[field];
      }
    });

    // This value should be mapped to boolean
    this.artifact_enabled = !!this.artifact_enabled;
    this.shared_dir_enabled = !!this.shared_dir_enabled;
  }

  recalculateOrders() {
    for (let i = 0; i < this.stages.length; i++) {
      this.stages[i].order = i + 1; // start from 1
      this.stages[i].recalculateOrders();
    }
  }

  /**
   * Remove values which are filled by API.
   */
  polish() {
    delete this['uuid'];
    delete this['created_by'];
    delete this['created_at'];
    delete this['updated_at'];
    delete this['resource_actions'];

    this.triggers.polish();
    this.stages.forEach(stage => stage.polish());
    this.on_end.forEach(task => task.polish());
  }
}
