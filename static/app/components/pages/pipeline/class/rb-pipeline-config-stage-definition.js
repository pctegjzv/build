import { RbPipelineConfigTask } from './rb-pipeline-config-task-definition';

/**
 * Definition for a single stage of a pipeline config
 */
export class RbPipelineConfigStage {
  static get PRIMITIVE_FIELDS() {
    return [
      'uuid',
      'order',
      'name',

      // Following are deprecated fields. From April 2017, region field is moved to each task instead.
      'region',
      'region_uuid',
    ];
  }

  constructor({ tasks = [], ...otherProperties }) {
    RbPipelineConfigStage.PRIMITIVE_FIELDS.forEach(field => {
      if (otherProperties[field] !== undefined) {
        this[field] = otherProperties[field];
      }
    });

    this.tasks = tasks.map(task => new RbPipelineConfigTask(task));
  }

  // Add a new task at order. After insert, the tasks ordering will be recalculated
  // Note, order starts from 1, and will append the item to the end by default
  addTask({ type, name, order = this.tasks.length + 1 }) {
    const newTask = new RbPipelineConfigTask({
      type,
      name,
    });

    this.tasks.splice(order - 1, 0, newTask);
    this.recalculateOrders();
    return newTask;
  }

  replaceTask({ order, type, name }) {
    const newTask = new RbPipelineConfigTask({
      type,
      name,
      order,
    });

    this.tasks[order - 1] = newTask;

    return newTask;
  }

  removeTask({ order }) {
    this.tasks.splice(order - 1, 1);
    this.recalculateOrders();
  }

  recalculateOrders() {
    for (let i = 0; i < this.tasks.length; i++) {
      this.tasks[i].order = i + 1; // start from 1
    }
  }

  polish() {
    delete this['uuid'];
    this.tasks.forEach(task => task.polish());
  }
}
