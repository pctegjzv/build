import { RbPipelineConfigTaskDataTestContainer } from './task-data/rb-pipeline-config-task-data-test-container-definition';
import { RbPipelineConfigTaskDataDeployApp } from './task-data/rb-pipeline-config-task-data-deploy-app-definition';
import { RbPipelineConfigTaskDataNotification } from './task-data/rb-pipeline-config-task-data-notification-definition';
import { RbPipelineConfigTaskDataManualControl } from './task-data/rb-pipeline-config-task-data-manual-control-definition';
import { RbPipelineConfigTaskExec } from './task-data/rb-pipeline-config-task-data-exec-definition';
import { RbPipelineConfigTaskUpdateService } from './task-data/rb-pipeline-config-task-data-update-service-definition';
import { RbPipelineConfigTaskDataSyncRegistry } from './task-data/rb-pipeline-config-task-data-sync-registry-definition';
import { RbPipelineConfigTaskDataBuild } from './task-data/rb-pipeline-config-task-data-build-definition';
import { RbPipelineConfigTaskDataArtifactDownload } from './task-data/rb-pipeline-config-task-data-artifact-download-definition';
import { RbPipelineConfigTaskDataArtifactUpload } from './task-data/rb-pipeline-config-task-data-artifact-upload-definition';
import { RbPipelineConfigTaskCiReport } from './task-data/rb-pipeline-config-task-data-ci-report-definition';
import { RbPipelineConfigTaskTriggerPipeline } from './task-data/rb-pipeline-config-task-data-trigger-pipeline-definition';
import { RbPipelineConfigTaskDataApprove } from './task-data/rb-pipeline-config-task-data-approve-definition';

export const TASK_DATA_CLASSES = {
  'test-container': RbPipelineConfigTaskDataTestContainer,
  'deploy-application': RbPipelineConfigTaskDataDeployApp,
  notification: RbPipelineConfigTaskDataNotification,
  'manual-control': RbPipelineConfigTaskDataManualControl,
  exec: RbPipelineConfigTaskExec,
  'update-service': RbPipelineConfigTaskUpdateService,
  'update-service-new': RbPipelineConfigTaskUpdateService,
  'sync-registry': RbPipelineConfigTaskDataSyncRegistry,
  'artifact-upload': RbPipelineConfigTaskDataArtifactUpload,
  'artifact-download': RbPipelineConfigTaskDataArtifactDownload,
  'ci-report': RbPipelineConfigTaskCiReport,
  'trigger-pipeline': RbPipelineConfigTaskTriggerPipeline,
  approve: RbPipelineConfigTaskDataApprove,

  // Special task item which is only available to pipeline history:
  build: RbPipelineConfigTaskDataBuild,
};

/**
 * Represents a single task of a pipeline config
 */
export class RbPipelineConfigTask {
  static get PRIMITIVE_FIELDS() {
    return ['uuid', 'order', 'name', 'type', 'timeout', 'region_uuid'];
  }

  constructor({ data = {}, ...restProperties }) {
    RbPipelineConfigTask.PRIMITIVE_FIELDS.forEach(field => {
      if (restProperties[field] !== undefined) {
        this[field] = restProperties[field];
      }
    });

    if (restProperties.type !== 'task_type_selection') {
      this.data = new TASK_DATA_CLASSES[restProperties.type](data);
    }
  }

  polish() {
    delete this['uuid'];
  }
}
