/**
 * Base for all pipeline config triggers
 */
export class RbPipelineConfigTrigger {
  constructor({ type }) {
    this.type = type;
  }
}

export class RbPipelineConfigRepositoryTrigger extends RbPipelineConfigTrigger {
  static get PRIMITIVE_FIELDS() {
    return [
      'registry',
      'registry_display_name',
      'repository',
      'registry_uuid',
      'repository_uuid',
      'schedule_rule',
      'image_tag',
      'auto_trigger_enabled',
    ];
  }

  constructor({ ...properties }) {
    super({ type: 'repository' });

    RbPipelineConfigRepositoryTrigger.PRIMITIVE_FIELDS.forEach(field => {
      if (properties[field] !== undefined) {
        this[field] = properties[field];
      }
    });
  }

  get project() {
    return this.repository && this.repository.includes('/')
      ? this.repository.split('/')[0]
      : undefined;
  }

  set project(project) {
    if (project !== this.project) {
      const repo = this.repo;
      this.repository = project ? project + '/' + repo : repo;
    }
  }

  // This is the alternative name for the second part if project presents
  get repo() {
    return this.repository && this.repository.includes('/')
      ? this.repository.split('/')[1]
      : this.repository;
  }

  set repo(repo) {
    if (repo !== this.repo) {
      const project = this.project;
      this.repository = project ? project + '/' + repo : repo;
    }
  }
}

export class RbPipelineConfigBuildTrigger extends RbPipelineConfigTrigger {
  static get PRIMITIVE_FIELDS() {
    return [
      'build_config_uuid',
      'build_config_name',
      'schedule_rule',
      'auto_trigger_enabled',
    ];
  }

  constructor({ ...properties }) {
    super({ type: 'build' });

    RbPipelineConfigBuildTrigger.PRIMITIVE_FIELDS.forEach(field => {
      if (properties[field] !== undefined) {
        this[field] = properties[field];
      }
    });
  }
}

/**
 * List all types of pipeline config triggers
 */
const TYPE_TO_CLASS = {
  repository: RbPipelineConfigRepositoryTrigger,
  build: RbPipelineConfigBuildTrigger,
};

/**
 * Represents the 'triggers' field of a pipeline config
 */
export class RbPipelineConfigTriggers {
  constructor(triggers = { repository: {} }) {
    this.triggers = Object.entries(TYPE_TO_CLASS).map(
      ([type, TriggerClass]) => new TriggerClass(triggers[type] || {}),
    );

    if (triggers.repository) {
      this.triggerType = 'repository';
    } else if (triggers.build) {
      this.triggerType = 'build';
    }
  }

  // To be used by JSON.stringify
  toJSON() {
    return {
      [this.activeTrigger.type]: this.activeTrigger,
    };
  }

  get repository() {
    return this.triggers.find(trigger => trigger.type === 'repository');
  }

  get build() {
    return this.triggers.find(trigger => trigger.type === 'build');
  }

  get triggerType() {
    return this.triggers.find(trigger => trigger.active).type;
  }

  set triggerType(type) {
    this.triggers.forEach(trigger => (trigger.active = false));
    this[type].active = true;
  }

  get activeTrigger() {
    return this[this.triggerType];
  }

  polish() {
    // Do nothing
  }
}
