/**
 * Approval
 */
export class RbPipelineConfigTaskDataApprove {
  static get PRIMITIVE_FIELDS() {
    return ['approver', 'recipient'];
  }

  constructor(data) {
    RbPipelineConfigTaskDataApprove.PRIMITIVE_FIELDS.forEach(field => {
      if (data[field] !== undefined) {
        this[field] = data[field];
      }
    });
  }
}
