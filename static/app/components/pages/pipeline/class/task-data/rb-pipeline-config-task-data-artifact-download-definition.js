/**
 * Artifact download
 */
export class RbPipelineConfigTaskDataArtifactDownload {
  static get PRIMITIVE_FIELDS() {
    return ['share_path'];
  }

  constructor(data) {
    RbPipelineConfigTaskDataArtifactDownload.PRIMITIVE_FIELDS.forEach(field => {
      if (data[field] !== undefined) {
        this[field] = data[field];
      }
    });
  }
}
