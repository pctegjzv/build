/**
 * Artifact Upload
 */
export class RbPipelineConfigTaskDataArtifactUpload {
  static get PRIMITIVE_FIELDS() {
    return ['share_path'];
  }

  constructor(data) {
    RbPipelineConfigTaskDataArtifactUpload.PRIMITIVE_FIELDS.forEach(field => {
      if (data[field] !== undefined) {
        this[field] = data[field];
      }
    });
  }
}
