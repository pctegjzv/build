/**
 * Build
 */
export class RbPipelineConfigTaskDataBuild {
  static get PRIMITIVE_FIELDS() {
    return [
      'build_config_uuid',
      'build_config_name',
      'head_commit_id',
      'code_repo_client',
      'build_id',
    ];
  }

  constructor({ ...data }) {
    RbPipelineConfigTaskDataBuild.PRIMITIVE_FIELDS.forEach(field => {
      if (data[field] !== undefined) {
        this[field] = data[field];
      }
    });
  }
}
