export class RbPipelineConfigTaskCiReport {
  static get PRIMITIVE_FIELDS() {
    return ['sonar_project_key'];
  }

  constructor({ email_receivers = '', ...data }) {
    RbPipelineConfigTaskCiReport.PRIMITIVE_FIELDS.forEach(field => {
      if (data[field] !== undefined) {
        this[field] = data[field];
      }
    });
    this.email_receivers = email_receivers;
    this.emails = email_receivers.split(',').map(el => {
      return {
        email: el,
      };
    });
  }

  toJSON() {
    const cloned = Object.assign({}, this);
    // cloned.emails = [];

    // this.emails.forEach((value) => {
    //   if (value) {
    //     cloned.emails.push(value.email);
    //   }
    // });

    // cloned.email_receivers = cloned.emails.join(',');
    cloned.email_receivers = this.email_receivers;
    return cloned;
  }
}
