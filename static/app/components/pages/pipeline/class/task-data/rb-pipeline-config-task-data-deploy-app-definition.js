/**
 * deploy-application
 */
export class RbPipelineConfigTaskDataDeployApp {
  static get PRIMITIVE_FIELDS() {
    return ['application_template', 'application_template_uuid'];
  }

  constructor({ services = [], ...data }) {
    this.services = services;
    RbPipelineConfigTaskDataDeployApp.PRIMITIVE_FIELDS.forEach(field => {
      if (data[field] !== undefined) {
        this[field] = data[field];
      }
    });
  }
}
