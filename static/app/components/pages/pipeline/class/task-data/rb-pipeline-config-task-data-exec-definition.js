/**
 * exec
 *
 *  "link": {
 *		"type": "service",
 *		"parent": "",
 *		"parent_uuid": "",
 *		"name": "service-name"
 *	},
 *  "instance": "service-name.0",
 *  "containers": ["container"],
 *  "entrypoint": "/bin/sh",
 *  "command": ["",""]
 */
export class RbPipelineConfigTaskExec {
  static get PRIMITIVE_FIELDS() {
    return [
      'link',
      'command', // an array
      'containers', // containers for new k8s
      'instance', // {service-name}.{index}
      'entrypoint',
    ];
  }

  constructor({ ...data }) {
    RbPipelineConfigTaskExec.PRIMITIVE_FIELDS.forEach(field => {
      if (data[field] !== undefined) {
        this[field] = data[field];
      }
    });

    this.entrypoint = this.entrypoint || '/bin/sh';
  }
}
