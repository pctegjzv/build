/**
 * Manual control
 */
export class RbPipelineConfigTaskDataManualControl {
  static get PRIMITIVE_FIELDS() {
    return ['link', 'exec_enabled'];
  }

  constructor({ ...data }) {
    RbPipelineConfigTaskDataManualControl.PRIMITIVE_FIELDS.forEach(field => {
      if (data[field] !== undefined) {
        this[field] = data[field];
      }
    });
  }
}
