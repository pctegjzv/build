/**
 * notification
 */
export class RbPipelineConfigTaskDataNotification {
  static get PRIMITIVE_FIELDS() {
    return ['notification', 'notification_uuid'];
  }

  constructor(data) {
    RbPipelineConfigTaskDataNotification.PRIMITIVE_FIELDS.forEach(field => {
      if (data[field] !== undefined) {
        this[field] = data[field];
      }
    });
  }
}
