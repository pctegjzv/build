/**
 * Sync registry task data model
 */
export class RbPipelineConfigTaskDataSyncRegistry {
  static get PRIMITIVE_FIELDS() {
    return [
      'config_name',
      'sync_ids', // array of sync ids triggered by a single sync config task
      'share_path',
    ];
  }

  constructor({ ...data }) {
    RbPipelineConfigTaskDataSyncRegistry.PRIMITIVE_FIELDS.forEach(field => {
      if (data[field] !== undefined) {
        this[field] = data[field];
      }
    });
  }
}
