/**
 * test-container
 */
export class RbPipelineConfigTaskDataTestContainer {
  static get PRIMITIVE_FIELDS() {
    return [
      'registry_name',
      'registry_uuid',
      'command',
      'registry_index',
      'docker_image_tag',
      'env',
      'envFrom',
      'env_file',
      'env_file_uuid',
      'k8s_namespace',
      'k8s_namespace_uuid',
      'automatic_rollback',
      'link', // { type, parent, parent_uuid, name }
    ];
  }

  constructor({ env_vars, ...restData }) {
    RbPipelineConfigTaskDataTestContainer.PRIMITIVE_FIELDS.forEach(field => {
      if (restData[field] !== undefined) {
        this[field] = restData[field];
      }
    });

    this.env_vars = []; // The frontend stores values as an array instead of an object
    if (env_vars) {
      this.env_vars = Object.entries(env_vars).map(([k, v]) => ({
        name: k,
        value: v,
      }));
    }
  }

  toJSON() {
    const cloned = Object.assign({}, this);
    cloned.env_vars = {};

    if (this.env_vars) {
      this.env_vars.forEach(({ name, value }) => {
        if (name) {
          cloned.env_vars[name] = value;
        }
      });
    }

    return cloned;
  }

  /**
   * Get the registry_type based on some facts
   * Not required by the endpoint.
   */
  get registry_type() {
    if (this.registry_index === 'index.docker.io') {
      return 'registry_type_docker_official';
    } else if (this.registry_uuid) {
      return this.registry_name;
    } else {
      return 'registry_type_custom';
    }
  }

  isPrivateRegistrySelected() {
    return !['registry_type_docker_official', 'registry_type_custom'].includes(
      this.registry_type,
    );
  }

  get repo_name() {
    return this.full_repo_name && this.full_repo_name.includes('/')
      ? this.full_repo_name.split('/')[1]
      : this.full_repo_name;
  }

  set repo_name(repo_name) {
    if (repo_name !== this.repo_name) {
      this.full_repo_name = this.project
        ? this.project + '/' + repo_name
        : repo_name;
    }
  }

  get project() {
    return this.full_repo_name && this.full_repo_name.includes('/')
      ? this.full_repo_name.split('/')[0]
      : '';
  }

  set project(project) {
    if (project !== this.project) {
      if (project) {
        this.repo_name = '';
      }
      this.full_repo_name = project
        ? project + '/' + this.repo_name
        : this.repo_name;
    }
  }

  get full_repo_name() {
    return this.docker_image_tag
      ? this.docker_image_tag.split(':')[0]
      : this.docker_image_tag;
  }

  set full_repo_name(full_repo_name) {
    if (full_repo_name !== this.full_repo_name) {
      this.docker_image_tag = full_repo_name + ':' + this.image_tag;
    }
  }

  get image_tag() {
    return this.docker_image_tag && this.docker_image_tag.includes(':')
      ? this.docker_image_tag.split(':')[1]
      : '';
  }

  set image_tag(image_tag) {
    if (image_tag !== this.image_tag) {
      this.docker_image_tag = this.full_repo_name + ':' + image_tag;
    }
  }
}
