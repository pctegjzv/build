export class RbPipelineConfigTaskTriggerPipeline {
  static get PRIMITIVE_FIELDS() {
    return [
      'pipeline_name',
      'pipeline_uuid',
      'execute_type',
      'version_from',
      'version',
      'trigger_type',
      'project_name',
    ];
  }

  constructor(data) {
    RbPipelineConfigTaskTriggerPipeline.PRIMITIVE_FIELDS.forEach(field => {
      if (data[field] !== undefined) {
        this[field] = data[field];
      }
    });
  }
}
