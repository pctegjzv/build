import * as _ from 'lodash';
/**
 * update-service
 *
 *  "service": {
 *		"type": "service",
 *		"parent": "",
 *		"parent_uuid": "",
 *		"name": "service-name"
 *	},
 *  "automatic_rollback",
 *  "env_vars",
 *  "env_files"
 */
export class RbPipelineConfigTaskUpdateService {
  static get PRIMITIVE_FIELDS() {
    return ['automatic_rollback', 'service'];
  }

  constructor({
    env_vars = [],
    env_files = [],
    mount_points = [],
    ...restData
  }) {
    RbPipelineConfigTaskUpdateService.PRIMITIVE_FIELDS.forEach(field => {
      if (restData[field] !== undefined) {
        this[field] = restData[field];
      }
    });

    // The frontend stores values as an array instead of an object
    this.env_files = env_files;
    this.env_vars = Object.entries(env_vars).map(([k, v]) => ({
      name: k,
      value: v,
    }));
    this.mount_points = mount_points.map(item => {
      const cloned = Object.assign({}, item);
      if (item.type === 'raw') {
        cloned.rawValue = item.value;
      } else {
        delete cloned.value;
        cloned.config = _.get(item.value, 'name');
        cloned.key = _.get(item.value, 'key');
      }

      return cloned;
    });
  }

  toJSON() {
    const cloned = Object.assign({}, this);
    cloned.env_vars = {};

    this.env_vars.forEach(({ name, value }) => {
      if (name) {
        cloned.env_vars[name] = value;
      }
    });

    cloned.mount_points = this.mount_points.map(item => {
      const config = {
        path: item.path,
        type: item.type,
      };
      if (item.type === 'raw') {
        config.value = item.rawValue;
      } else {
        config.value = {
          name: item.config,
          key: item.key,
        };
      }
      return config;
    });

    return cloned;
  }
}
