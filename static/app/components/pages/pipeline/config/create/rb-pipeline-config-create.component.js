import { INITIAL_CREATE_CONFIG } from '../../rb-pipeline-constant';
import { RbPipelineConfig } from '../../class/rb-pipeline-config-definition';
const templateStr = require('app/components/pages/pipeline/config/create/rb-pipeline-config-create.html');
angular.module('app.components.pages').component('rbPipelineConfigCreate', {
  controller: rbPipelineConfigCreateController,
  controllerAs: 'vm',
  bindings: {
    configCopy: '<', // Can provide a default config (e.g., copied from another object)
  },
  template: templateStr,
});

function rbPipelineConfigCreateController(
  $state,
  $log,
  $element,
  rbSafeApply,
  rbRouterStateHelper,
  rbQuotaSpaceDataService,
  rb2DefaultErrorMapperService,
  WEBLABS,
  rbConfirmBox,
  translateService,
) {
  const vm = this;

  vm.locale = translateService.getLanguage();
  vm.isConfigValid = isConfigValid;
  vm.createClicked = createClicked;
  vm.onSpaceChanged = onSpaceChanged;
  vm.goBack = goBack;
  vm.$onInit = onInit;
  vm.onArtifactEnabledChange = onArtifactEnabledChange;
  //////////
  async function onInit() {
    // Forms for basic info fields. i.e., name/description
    vm.basicForm = undefined;

    vm.config = vm.configCopy || new RbPipelineConfig(INITIAL_CREATE_CONFIG);
    vm.config.polish();
    vm.config.artifact_enabled = true;
    vm.quotaEnabled = WEBLABS['quota_enabled'];

    vm.errorMapper = {
      map: (key, error, control) => {
        if (key === 'pattern') {
          return translateService.get('pipeline_name_hint');
        } else {
          return rb2DefaultErrorMapperService.map(key, error, control);
        }
      },
    };

    if (vm.quotaEnabled) {
      vm.spaces = await rbQuotaSpaceDataService.getConsumableSpaces();
      rbSafeApply();
    }
  }

  function onSpaceChanged(space) {
    vm.config.space_name = space.name;
  }

  async function createClicked() {
    vm.basicForm.$setSubmitted();
    vm.basicForm.$setDirty();

    if (!isConfigValid()) {
      return;
    }

    try {
      await rbConfirmBox.show({
        title: translateService.get('create'),
        textContent: translateService.get('pipeline_create_config_content', {
          name: vm.config.name,
        }),
      });

      vm.submitting = true;
      const { uuid } = await vm.config.push();
      $state.go('pipeline.config.detail', {
        name: uuid,
      });
    } catch (e) {
      $log.warn('Failed to create pipeline: ' + e);
      // TODO .. handle errors
    }

    vm.submitting = false;
    rbSafeApply();
  }

  function isConfigValid() {
    return (
      vm.basicForm &&
      (vm.basicForm.$pristine ||
        !vm.basicForm.$submitted ||
        vm.basicForm.$valid) &&
      // isValid is bound via rbPipelineConfigWorkflow ...
      vm.config.isValid &&
      vm.config.isValid()
    );
  }

  function goBack() {
    rbRouterStateHelper.back();
  }

  function onArtifactEnabledChange() {
    const elem = angular.element('rb-pipeline-config-workflow', $element);
    if (!elem) {
      return;
    }
    elem
      .isolateScope()
      .vm.workflow.artifactEnabled$.next(vm.config.artifact_enabled);
  }
}
