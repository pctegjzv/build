import { resource_types as RESOURCE_TYPES } from '../../../rbac_role/rb-rbac-helper.constant';
import { RbPipelineConfig } from '../../class/rb-pipeline-config-definition';

/**
 * Detail page for a pipeline config.
 * Can switch between different modes:
 *   view / update
 */
import template from './rb-pipeline-config-detail.html';

angular.module('app.components.pages').component('rbPipelineConfigDetail', {
  bindings: {
    name: '<',
  },
  controller: rbPipelineConfigDetailController,
  controllerAs: 'vm',
  template,
});

function rbPipelineConfigDetailController(
  $log,
  $state,
  $element,
  rbRouterStateHelper,
  rbToast,
  rbSafeApply,
  rbConfirmBox,
  rbAccountService,
  rbRoleUtilities,
  rbPipelineDataService,
  rbTriggerPipelineDialog,
  translateService,
) {
  const vm = this;
  vm.deleteClicked = deleteClicked;
  vm.updateClicked = updateClicked;
  vm.cancelClicked = cancelClicked;
  vm.saveClicked = saveClicked;
  vm.cloneClicked = cloneClicked;
  vm.showStartPipelineDialog = showStartPipelineDialog;
  vm.buttonDisplayExpr = buttonDisplayExpr;
  vm.onArtifactEnabledChange = onArtifactEnabledChange;

  vm.$onInit = onInit;
  /////////
  async function onInit() {
    vm.loading = true;
    vm.pipelineConfigCreateEnabled = await rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.PIPELINE_CONFIG,
    );
    vm.config = await _loadConfig();
    vm.piplineDisplayName = vm.config.name;
    vm.loading = false;
    vm.mode = 'view';
    vm.namespace = rbAccountService.getCurrentNamespace();
    rbSafeApply();
  }

  async function _loadConfig() {
    try {
      const config = await rbPipelineDataService
        .getPipeline({ name: vm.name })
        .catch(e => {
          // requesting repo does not exist
          if (!vm.initialized && e.status !== 403) {
            rbToast.warning(translateService.get('pipeline_not_exist'));
          }
          rbRouterStateHelper.back();
        });
      return new RbPipelineConfig(config);
    } catch (e) {
      $log.error('Failed to load pipeline config for ' + vm.name);
      // Shall we prompt the user some error info?
      $state.go('pipeline.config.list');
    }
  }

  let _configBackup = undefined;

  function updateClicked() {
    _configBackup = _.cloneDeep(vm.config);

    // Attempt to select the workflow tab
    const mdTabsCtrl = angular.element('md-tabs', $element).isolateScope()
      .$mdTabsCtrl;

    mdTabsCtrl.select(1, true);

    vm.mode = 'update';
  }

  function cancelClicked() {
    // This will re-init the pipeline config workflow
    vm.config = _configBackup;
    vm.mode = 'view';
  }

  async function saveClicked() {
    try {
      await rbConfirmBox.show({
        title: translateService.get('update'),
        textContent: translateService.get('pipeline_update_config_content', {
          name: vm.piplineDisplayName,
        }),
      });
      vm.loading = true;
      await vm.config.push();
      // Only change to view state / update config when success
      vm.mode = 'view';
      // Refetch to update 'updated_at' timestamp etc.
      vm.config = await _loadConfig();
    } catch (e) {
      // TODO .. handle errors
      $log.error(e);
    }
    vm.loading = false;

    rbSafeApply();
  }

  async function deleteClicked($event) {
    try {
      await rbConfirmBox.show({
        title: translateService.get('pipeline_delete_config_title'),
        textContent: translateService.get('pipeline_delete_config_content', {
          name: vm.piplineDisplayName,
        }),
        targetEvent: $event,
      });

      vm.loading = true;
      await rbPipelineDataService.deletePipeline({ name: vm.name });
      $state.go('pipeline.config.list');
    } catch (e) {
      // Do nothing
    }
    vm.loading = false;
  }

  function showStartPipelineDialog() {
    return rbTriggerPipelineDialog
      .show({
        pipeline: vm.config,
      })
      .then(({ pipelineName, historyId }) =>
        $state.go('pipeline.history.detail', { pipelineName, historyId }),
      )
      .catch(() => {});
  }

  function cloneClicked() {
    const configCopy = _.cloneDeep(vm.config);
    configCopy.name += '-copy';

    // Clean up UUIDs
    delete configCopy.uuid;
    configCopy.stages.forEach(stage => {
      delete stage.uuid;
      stage.tasks.forEach(task => {
        delete task.uuid;
      });
    });

    $state.go('pipeline.config.create', { configCopy });
  }

  function buttonDisplayExpr(action) {
    return rbRoleUtilities.resourceHasPermission(
      vm.config,
      RESOURCE_TYPES.PIPELINE_CONFIG,
      action,
    );
  }

  function onArtifactEnabledChange() {
    const elem = angular.element('rb-pipeline-config-workflow', $element);
    if (!elem) {
      return;
    }
    elem
      .isolateScope()
      .vm.workflow.artifactEnabled$.next(vm.config.artifact_enabled);
  }
}
