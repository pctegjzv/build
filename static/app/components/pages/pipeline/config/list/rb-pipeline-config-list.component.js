import { resource_types as RESOURCE_TYPES } from '../../../rbac_role/rb-rbac-helper.constant';

import template from './rb-pipeline-config-list.html';

angular.module('app.components.pages').component('rbPipelineConfigList', {
  controller: rbPipelineConfigListController,
  controllerAs: 'vm',
  template,
});

function rbPipelineConfigListController(
  $state,
  rbRegistryDataStore,
  rbPipelineDataService,
  translateService,
  rbRoleUtilities,
  rbConfirmBox,
  rbTriggerPipelineDialog,
  rbSafeApply,
) {
  const vm = this;

  vm.pagination = {
    pageSize: 20,
    pageno: 1,
    totalItems: 0,
  };

  vm.pageNoChange = pageNoChange;
  vm.deleteClicked = deleteClicked;
  vm.showStartPipelineDialog = showStartPipelineDialog;
  vm.onSearchChanged = onSearchChanged;
  vm.getTriggerTypeIcon = getTriggerTypeIcon;
  vm.getTriggerType = getTriggerType;
  vm.getRegistryDisplayName$ = getRegistryDisplayName$;
  vm.buttonDisplayExpr = buttonDisplayExpr;
  vm.getConfigName = getConfigName;
  vm.$onInit = onInit;

  ///////////
  async function onInit() {
    vm.search = '';
    vm.pipelineConfigCreateEnabled = await rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.PIPELINE_CONFIG,
    );
    pageNoChange();
    rbSafeApply();
  }

  async function pageNoChange(
    page = vm.pagination.pageno,
    size = vm.pagination.pageSize,
  ) {
    const pageNoChanged = vm.pagination.pageno !== page;
    vm.pagination.pageno = page;
    vm.pagination.pageSize = size;
    vm.loading = true;
    try {
      const { count, results } = await _refetchData({
        pageno: vm.pagination.pageno,
        page_size: vm.pagination.pageSize,
        search: vm.search,
      });

      vm.pagination.totalItems = count;
      vm.list = results;
      vm.loadError = false;
    } catch (e) {
      if (pageNoChanged) {
        vm.list = [];
      }
      vm.loadError = true;
    }
    vm.loading = false;
    rbSafeApply();
  }

  let _lastSearchCounter = 0;

  async function onSearchChanged(search) {
    const searchCounter = ++_lastSearchCounter;
    vm.search = search;
    vm.searching = true;
    await pageNoChange(1);
    if (searchCounter === _lastSearchCounter) {
      vm.searching = false;
    }
    rbSafeApply();
  }

  function _refetchData({ pageno, page_size, search }) {
    return rbPipelineDataService.getPipelines({ pageno, page_size, search });
  }

  async function deleteClicked({ config, $event }) {
    try {
      const configName = vm.getConfigName(config);
      await rbConfirmBox.show({
        title: translateService.get('pipeline_delete_config_title'),
        textContent: translateService.get('pipeline_delete_config_content', {
          name: config.name,
        }),
        targetEvent: $event,
      });

      config._deleting = true;
      rbSafeApply();
      await rbPipelineDataService.deletePipeline({ name: configName });
      pageNoChange();
    } catch (e) {
      // Do nothing
    }
  }

  function getTriggerType(config) {
    return Object.keys(config.triggers)[0];
  }

  function getTriggerTypeIcon(config) {
    const TRIGGER_TYPE_ICONS = {
      build: 'fa fa-gavel',
      repository: 'fa fa-building',
    };

    return TRIGGER_TYPE_ICONS[getTriggerType(config)];
  }

  function getRegistryDisplayName$(registryName) {
    return rbRegistryDataStore.getRegistryDisplayName$(registryName);
  }

  function showStartPipelineDialog($event, pipeline) {
    return rbTriggerPipelineDialog
      .show({
        pipeline,
        $event,
      })
      .then(({ pipelineName, historyId }) =>
        $state.go('pipeline.history.detail', { pipelineName, historyId }),
      )
      .catch(() => {});
  }

  function buttonDisplayExpr(item, action) {
    return rbRoleUtilities.resourceHasPermission(
      item,
      RESOURCE_TYPES.PIPELINE_CONFIG,
      action,
    );
  }
  function getConfigName(config) {
    return rbPipelineDataService.getConfigName(config);
  }
}
