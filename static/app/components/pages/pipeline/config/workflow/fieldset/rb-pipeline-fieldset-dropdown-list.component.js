const templateStr = require('app/components/pages/pipeline/config/workflow/fieldset/rb-pipeline-fieldset-dropdown-list.html');
angular
  .module('app.components.pages')
  .component('rbPipelineFieldsetDropdownList', {
    controller: rbPipelineFieldsetDropdownListController,
    controllerAs: 'vm',
    bindings: {
      items: '<',
      isDisabled: '<',
      options: '<',
    },
    template: templateStr,
  });

function rbPipelineFieldsetDropdownListController() {
  const vm = this;

  vm.addItem = addItem;
  vm.deleteItem = deleteItem;

  //////////
  function addItem() {
    vm.items.push('');
  }

  function deleteItem(index) {
    vm.items.splice(index, 1);
  }
}
