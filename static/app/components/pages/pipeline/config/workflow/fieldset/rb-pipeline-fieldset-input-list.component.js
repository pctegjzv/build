const templateStr = require('app/components/pages/pipeline/config/workflow/fieldset/rb-pipeline-fieldset-input-list.html');
angular
  .module('app.components.pages')
  .component('rbPipelineFieldsetInputList', {
    controller: rbPipelineFieldsetInputListContoller,
    controllerAs: 'vm',
    bindings: {
      items: '<',
      isDisabled: '<',
    },
    template: templateStr,
  });

function rbPipelineFieldsetInputListContoller() {
  const vm = this;

  vm.$onInit = _init;
  vm.addItem = addItem;
  vm.deleteItem = deleteItem;

  //////////

  function _init() {
    if (vm.items && vm.items.length === 0) {
      addItem();
    }
  }

  function addItem() {
    vm.items.push('');
  }

  function deleteItem(index) {
    vm.items.splice(index, 1);
  }
}
