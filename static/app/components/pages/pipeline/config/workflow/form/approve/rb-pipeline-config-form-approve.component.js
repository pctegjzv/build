const templateStr = require('app/components/pages/pipeline/config/workflow/form' +
  '/approve/rb-pipeline-config-form-approve.component.html');
angular
  .module('app.components.pages')
  .component('rbPipelineConfigFormApprove', {
    bindings: {
      item: '<',
      workflow: '<',
      form: '<',
    },
    controller: rbPipelineConfigFormApproveController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbPipelineConfigFormApproveController(
  orgService,
  rbProjectService,
  rbAccountService,
) {
  const vm = this;

  vm.$onInit = onInit;

  vm.approverOptions = [];
  vm.initApprovers = [];
  vm.initRecipients = [];
  vm.onApproverAdd = onApproverAdd;
  vm.onApproverRemove = onApproverRemove;
  vm.onRecipientChange = onRecipientChange;
  vm.emailValidateFn = emailValidateFn;

  /////////
  async function onInit() {
    const currentProjectName = rbProjectService.get();
    const namespace = rbAccountService.getCurrentNamespace();
    vm.approverLoading = true;
    try {
      const { results } = await orgService.listOrgFilteredAccounts({
        page_size: 0,
        page: 1,
        project_name: currentProjectName,
      });
      vm.approverOptions = results.filter(user => !!user.username).map(user => {
        return `${namespace}/${user.username}`;
      });
    } catch (error) {
      vm.approverOptions = [];
    }
    vm.approverLoading = false;
    vm.initApprovers = vm.item.model.data.approver || [];
    vm.initRecipients = vm.item.model.data.recipient || [];
  }

  function onApproverAdd(option) {
    if (!vm.approvers) {
      vm.approvers = [option.key];
      vm.item.model.data.approver = vm.approvers;
    } else {
      vm.approvers.push(option.key);
    }
  }

  function onApproverRemove(option) {
    const existIndex = vm.approvers.findIndex(item => {
      return item === option.key;
    });
    vm.approvers.splice(existIndex, 1);
  }

  function onRecipientChange(emails) {
    vm.item.model.data.recipient = emails;
  }

  function emailValidateFn(email) {
    return /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/.test(email);
  }
}
