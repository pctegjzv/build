import { filter } from 'rxjs/operators';

import templateStr from './rb-pipeline-config-form-artifact-download.component.html';

angular
  .module('app.components.pages')
  .component('rbPipelineConfigFormArtifactDownload', {
    bindings: {
      item: '<',
      workflow: '<',
    },
    controller: rbPipelineConfigFormArtifactDownloadController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbPipelineConfigFormArtifactDownloadController() {
  const vm = this;
  let downloadPathSub;

  vm.$onInit = onInit;
  vm.$onDestroy = onDestroy;

  /////////
  function onInit() {
    if (!vm.item.model.data.share_path) {
      downloadPathSub = vm.workflow.triggerRepository$
        .pipe(filter(data => !!data))
        .subscribe(repo => {
          if (repo.namespace && repo.name) {
            vm.item.model.data.share_path = `/images/${
              repo.namespace
            }/${(repo.project && repo.project.project_name) ||
              repo.namespace}/${repo.name}`;
          } else {
            vm.item.model.data.share_path = '/';
          }
        });
    }
  }

  function onDestroy() {
    if (downloadPathSub) {
      downloadPathSub.unsubscribe();
    }
  }
}
