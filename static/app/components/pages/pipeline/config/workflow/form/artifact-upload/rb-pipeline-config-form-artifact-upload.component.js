import { filter } from 'rxjs/operators';

import templateStr from './rb-pipeline-config-form-artifact-upload.component.html';

angular
  .module('app.components.pages')
  .component('rbPipelineConfigFormArtifactUpload', {
    bindings: {
      item: '<',
      workflow: '<',
    },
    controller: rbPipelineConfigFormArtifactUploadController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbPipelineConfigFormArtifactUploadController() {
  const vm = this;
  let uploadPathSub;

  vm.$onInit = onInit;
  vm.$onDestroy = onDestroy;

  /////////
  function onInit() {
    if (!vm.item.model.data.share_path) {
      uploadPathSub = vm.workflow.triggerRepository$
        .pipe(filter(data => !!data))
        .subscribe(repo => {
          if (repo.namespace && repo.name) {
            vm.item.model.data.share_path = `/images/${
              repo.namespace
            }/${(repo.project && repo.project.project_name) ||
              repo.namespace}/${repo.name}`;
          } else {
            vm.item.model.data.share_path = '/';
          }
        });
    }
  }

  function onDestroy() {
    if (uploadPathSub) {
      uploadPathSub.unsubscribe();
    }
  }
}
