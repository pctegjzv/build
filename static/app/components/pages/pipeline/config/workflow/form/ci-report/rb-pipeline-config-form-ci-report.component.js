const templateStr = require('app/components/pages/pipeline/config/workflow/' +
  'form/ci-report/rb-pipeline-config-form-ci-report.component.html');

angular
  .module('app.components.pages')
  .component('rbPipelineConfigFormCiReport', {
    bindings: {
      form: '<',
      item: '<',
      workflow: '<',
      taskTimeoutLimit: '<',
    },
    controller: rbPipelineConfigFormCiReportController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbPipelineConfigFormCiReportController() {
  const vm = this;
  vm.EMAIL_REGEX = /^(([a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?),)*([a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)$/;
  vm.$onInit = onInit;
  vm.$onDestroy = onDestroy;

  function onInit() {}

  function onDestroy() {}
}
