angular
  .module('app.components.pages')
  .factory('rbCommonCommandsDialog', rbCommonCommandsDialog);

function rbCommonCommandsDialog(rbModal, translateService) {
  return {
    show: () => {
      return rbModal
        .show({
          title: translateService.get('pipeline_start'),
          template: `<rb-common-commands></rb-common-commands>`,
        })
        .catch(() => {});
    },
  };
}
