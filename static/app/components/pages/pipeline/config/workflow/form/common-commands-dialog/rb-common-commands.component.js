import * as HELP_TEXTS_EN from '../../help-text.en';
import * as HELP_TEXTS_CN from '../../help-text.zh_cn';

const templateStr = require('app/components/pages/pipeline/config/workflow/form/common-commands-dialog/rb-common-commands.html');
angular.module('app.components.pages').component('rbCommonCommands', {
  controller: rbCommonCommandsController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbCommonCommandsController(rbModal, translateService) {
  const vm = this;
  vm.cancel = () => {
    rbModal.cancel();
  };
  vm.$onInit = onInit;

  ///////
  function onInit() {
    vm.HELP_TEXTS =
      translateService.getLanguage() === 'zh_cn'
        ? HELP_TEXTS_CN
        : HELP_TEXTS_EN;
  }
}
