import template from './rb-pipeline-config-container-info.component.html';

angular
  .module('app.components.pages')
  .component('rbPipelineConfigContainerInfo', {
    template,
    bindings: {
      containers: '<',
      configMaps: '<',
      regionId: '<',
      workflow: '<',
    },
    controllerAs: 'vm',
    controller(rbPipelineDataService, rbModal) {
      const vm = this;
      Object.assign(vm, {
        configMapsCache: {},
        addEnv(container) {
          if (vm.workflow.readonly) {
            return;
          }
          const { env = [] } = container;
          container.env = [...env, {}];
        },
        removeEnv(container, index) {
          if (vm.workflow.readonly) {
            return;
          }
          container.env.splice(index, 1);
          container.env = [...container.env];
        },
        onRefChange(envItem, refEnabled) {
          envItem.value = undefined;
          envItem.valueFrom = refEnabled ? { configMapKeyRef: {} } : undefined;
        },
        configMapChange({ name, namespace, uuid }, configMapKeyRef) {
          Object.assign(configMapKeyRef, {
            name,
            uuid,
          });

          if (vm.configMapsCache[uuid]) {
            return;
          }

          rbPipelineDataService
            .getConfigMap({
              clusterId: vm.regionId,
              namespace,
              name,
            })
            .then(configMap => {
              const configMapData = configMap.kubernetes.data;
              vm.configMapsCache[uuid] = {
                keys: Object.keys(configMapData),
                map: configMapData,
              };
            });
        },
        configMapKeyChange(key, configMapKeyRef) {
          configMapKeyRef.key = key;
        },
        previewConfigMapValue(configMapKeyRef) {
          const { key, uuid } = configMapKeyRef;
          if (!key) {
            return;
          }
          const configMap = vm.configMapsCache[uuid].map;
          const value = configMap[key];
          rbModal.show({
            locals: { content: value },
            template:
              '<rb-config-preview content="content"></rb-config-preview>',
            closable: true,
            autofocus: true,
          });
        },
        initConfigMaps:
          vm.containers &&
          vm.containers.map(
            ({ envFrom }) =>
              envFrom && envFrom.map(({ configMapRef: { uuid } }) => uuid),
          ),
        configMapAdded(container, { name, uuid }) {
          const { envFrom = [] } = container;
          if (envFrom.find(({ configMapRef }) => uuid === configMapRef.uuid)) {
            return;
          }
          container.envFrom = [...envFrom, { configMapRef: { name, uuid } }];
        },
        configMapRemoved(container, { uuid }) {
          container.envFrom = container.envFrom.filter(
            ({ configMapRef }) => uuid !== configMapRef.uuid,
          );
        },
      });
    },
  });
