import * as _ from 'lodash';
import { filter, first, switchMap, tap } from 'rxjs/operators';

const templateStr = require('app/components/pages/pipeline/config/workflow/' +
  'form/deploy-application/rb-pipeline-config-form-deploy-application.component.html');
angular
  .module('app.components.pages')
  .component('rbPipelineConfigFormDeployApplication', {
    bindings: {
      form: '<',
      item: '<',
      workflow: '<',
    },
    controller: rbPipelineConfigFormDeployApplicationController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbPipelineConfigFormDeployApplicationController(
  $scope,
  rbSafeApply,
  rbCreateObservableFunction,
  rbTemplateDataStore,
  rbTemplateServiceDataStore,
  RbPipelineWorkflowItemViewModel,
  RbPipelineWorkflowItemFieldViewModel,
) {
  const vm = this;

  vm.serviceAdded = serviceAdded;
  vm.serviceRemoved = serviceRemoved;

  vm.templatesDropdownClicked = _.throttle(templatesDropdownClicked, 30000);

  vm.$onInit = onInit;
  vm.$onDestroy = onDestroy;

  //////////
  function onInit() {
    vm.deployedAppServices = null;
    vm.templateServices = null;
    vm.templates$ = rbTemplateDataStore.all;
    vm.templates$
      .pipe(
        filter(templates => !!templates),
        first(),
      )
      .subscribe(templates => {
        if (!templates.length) {
          vm.templateServices = [];
        }
      });
    vm.selectedServiceLength = _.get(vm, 'item.model.data.services.length');
    const deployedServices$ = rbCreateObservableFunction(
      vm,
      'templateChanged',
    ).pipe(
      filter(template => !!template),
      // Apply template name change first:
      tap(template => {
        vm.item.loading = true;
        if (template.uuid !== vm.item.model.data.application_template_uuid) {
          vm.item.model.data.application_template_uuid = template.uuid;
          vm.item.model.data.application_template = template.name;
        }
        rbSafeApply();
      }),
      switchMap(template => rbTemplateServiceDataStore.list(template.uuid)),
    );

    vm.templateChangedSubscriber = deployedServices$.subscribe(services => {
      vm.item.loading = !services;
      vm.templateServices = services;
      if (services) {
        vm.item.model.data.services = _.intersection(
          vm.item.model.data.services,
          services.map(service => service.name),
        );
        vm.selectedServiceLength = _.get(vm, 'item.model.data.services.length');
      }
      vm.noTemplateServices = services && services.length === 0;

      emitAppServicesChanged(services);

      // Generating service fields should never be undefined
      services = services || [];
      // Extend the fields for deploy-application card
      const fields = RbPipelineWorkflowItemViewModel.generateTaskFields({
        task: vm.item.model,
      });
      const serviceNameFields =
        services &&
        services.map(service => {
          return new RbPipelineWorkflowItemFieldViewModel({
            type: 'service_name',
            // Search the data fields first
            label: service.name,
            // Decorate which services are selected
            css: () =>
              vm.item.model.data.services.includes(service.name)
                ? 'active'
                : '',
          });
        });
      vm.item.fields = [...fields, ...serviceNameFields];
      rbSafeApply();
    });

    // Whenever item name changes, resend the deployed app services list
    $scope.$watch('vm.item.model.name', () => {
      emitAppServicesChanged(vm.deployedAppServices);
    });
  }

  function onDestroy() {
    vm.templateChangedSubscriber.unsubscribe();
  }

  function serviceAdded(service) {
    if (!vm.item.model.data.services.includes(service.name)) {
      vm.item.model.data.services.push(service.name);
      vm.selectedServiceLength = _.get(vm, 'item.model.data.services.length');
    }
  }

  function serviceRemoved(service) {
    vm.item.model.data.services = vm.item.model.data.services.filter(
      serviceName => serviceName !== service.name,
    );
    vm.selectedServiceLength = _.get(vm, 'item.model.data.services.length');
  }

  function templatesDropdownClicked() {
    rbTemplateDataStore.refetch();
  }

  function emitAppServicesChanged(services) {
    vm.deployedAppServices =
      services &&
      services.map(service => ({
        parent: vm.item.model.name,
        type: 'deployed-app-service',
        name: service.name,
      }));

    // This will trigger deployedAppServicesSubject$ events
    vm.item.deployedAppServices = vm.deployedAppServices;
  }
}
