import { Subject, of } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';

import templateStr from './rb-pipeline-config-form-exec.component.html';

angular.module('app.components.pages').component('rbPipelineConfigFormExec', {
  bindings: {
    item: '<',
    workflow: '<',
    form: '<',
  },
  controller: rbPipelineConfigFormExecController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbPipelineConfigFormExecController(
  translateService,
  rbGlobalSetting,
  applicationService,
  rbPipelineConfigUtility,
  rbRegionDataStore,
  rbRegionUtilities,
  rbServiceDataService,
  rbCommonCommandsDialog,
) {
  const vm = this;

  const linkServiceSubject = new Subject();
  let linkServiceSubscription = null;

  vm.initContainers = vm.item.model.data.containers;
  vm.showRegionAsLabel = showRegionAsLabel;
  vm.showInstanceDropdown = showInstanceDropdown;
  vm.selectServiceTypeChanged = selectServiceTypeChanged;
  vm.instanceSelectionChanged = instanceSelectionChanged;
  vm.getInitServiceType = getInitServiceType;
  vm.getInitInstanceSelection = getInitInstanceSelection;
  vm.regionChanged = regionChanged;
  vm.instanceChanged = instanceChanged;
  vm.linkServiceChanged = linkServiceChanged;
  vm.commandChanged = commandChanged;
  vm.getServiceDisplayName = service =>
    rbPipelineConfigUtility.getServiceDisplayName(service);
  vm.commandHelpLinkClicked = commandHelpLinkClicked;
  vm.$onInit = onInit;
  vm.$onDestroy = onDestroy;

  /////////
  function onInit() {
    vm.selectServiceType = getInitServiceType();
    vm.selectServiceTypes = [
      {
        type: 'service',
        displayName: translateService.get(
          'pipeline_config_link_service_type_service',
        ),
      },
      {
        type: 'deployed-application',
        displayName: translateService.get(
          'pipeline_config_link_service_type_deployed_application',
        ),
      },
    ];

    vm.instanceSelection = getInitInstanceSelection();
    vm.instanceSelections = [
      {
        type: 'auto',
        displayName: translateService.get('auto'),
      },
      {
        type: 'manual',
        displayName: translateService.get('manual'),
      },
    ];

    vm.regions$ = rbRegionDataStore.data$.pipe(map(regionsWithExecFeature));
    vm.editorOptions = rbGlobalSetting.getEditorOptions('shell');

    // The region may depend on the selected deploy-app task's region
    linkServiceSubscription = linkServiceSubject
      .pipe(
        filter(() => this.selectServiceType === 'deployed-application'),
        switchMap(service => {
          const parentItem =
            service &&
            vm.workflow.allTasks.find(
              task => task.model.name === service.parent,
            );
          return parentItem ? parentItem.region$ : of({});
        }),
      )
      .subscribe(region => regionChanged(region));

    vm.commands = (vm.item.model.data.command || []).join('\n');
  }

  function getInitServiceType() {
    const type = _.get(vm, 'item.model.data.link.type', '');
    return type === 'deployed-app-service' ? 'deployed-application' : 'service';
  }

  function getInitInstanceSelection() {
    const instance = _.get(vm, 'item.model.data.instance', '');
    // Non empty instance value indicates Manual.
    return instance ? 'manual' : 'auto';
  }

  function regionChanged(region) {
    vm.item.region = region;
    vm.isNewK8sRegion = rbRegionUtilities.isNewK8sRegion(region);
  }

  function instanceChanged(instance) {
    vm.item.model.data.instance = instance ? instance.instance_name : undefined;
  }

  vm.onInstanceGroupChange = group => {
    const { data } = vm.item.model;
    data.instance = vm.serviceContainerGroup = group.name;
    data.containers = null;
    vm.serviceContainers = group.containers;
  };

  vm.onContainerAdded = ({ name }) => {
    const { data } = vm.item.model;
    if (!data.containers) {
      data.containers = [];
    }
    data.containers.push(name);
  };

  function showRegionAsLabel() {
    return vm.selectServiceType !== 'service';
  }

  function showInstanceDropdown() {
    return (
      vm.instanceSelection === 'manual' && vm.selectServiceType === 'service'
    );
  }

  const regionServices$ = rbPipelineConfigUtility.getRegionServices$({
    region$: vm.item.region$,
  });

  const previousDeployedServices$ = rbPipelineConfigUtility.getPreviousDeployedServices$(
    {
      tasks$: vm.workflow.tasks$,
      item: vm.item,
      skipFilterByRegion: true,
    },
  );

  function selectServiceTypeChanged(type) {
    if (type !== vm.selectServiceType) {
      vm.selectServiceType = type;
      vm.item.model.data.link = {};
    }
    if (type === 'service') {
      vm.availableServices$ = regionServices$;
    } else if (type === 'deployed-application') {
      vm.availableServices$ = previousDeployedServices$;
    }
  }

  function instanceSelectionChanged(type) {
    if (type === vm.instanceSelection) {
      return;
    }
    delete vm.item.model.data.instance;
    delete vm.item.model.data.containers;
    vm.instanceSelection = type;
    refreshInstanceOptions();
  }

  function linkServiceChanged(service) {
    vm.form['link_service_name'].$setDirty();
    if (service !== vm.item.model.data.link) {
      vm.item.model.data.link = service;
      linkServiceSubject.next(service);
      refreshInstanceOptions();
    }
  }

  function commandChanged() {
    vm.commands = vm.commands || '';
    vm.item.model.data.command = vm.commands.split('\n');
  }

  async function refreshInstanceOptions() {
    vm.instances = null;

    const { link } = vm.item.model.data;

    const uuid = link && link.uuid;

    if (showInstanceDropdown() && uuid) {
      if (vm.isNewK8sRegion) {
        rbServiceDataService.getNewK8sServiceInstances(uuid).then(instances => {
          vm.serviceInstances = instances.map(
            ({ metadata: { name, namespace, uid }, spec: { containers } }) => ({
              uuid: uid,
              name,
              namespace,
              containers,
            }),
          );
        });
      } else {
        const params = {
          service_name: uuid,
        };
        const { detail } = await applicationService
          .applicationDetail(params)
          .catch(() => ({ detail: { uuid } }));
        vm.instances = detail.instances || [];
      }
    } else {
      vm.instances = [];
    }
  }

  function regionsWithExecFeature(regions) {
    return (regions || []).filter(region => {
      const features = [
        ..._.get(region, 'features.service.features', []),
        ..._.get(region, 'features.service.metrics.features', []),
      ];

      return features.includes('exec');
    });
  }

  function commandHelpLinkClicked() {
    rbCommonCommandsDialog.show();
  }

  function onDestroy() {
    linkServiceSubscription && linkServiceSubscription.unsubscribe();
  }
}
