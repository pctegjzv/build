import { Subject, of } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';

import templateStr from './rb-pipeline-config-form-manual-control.component.html';

angular
  .module('app.components.pages')
  .component('rbPipelineConfigFormManualControl', {
    bindings: {
      item: '<',
      workflow: '<',
      form: '<',
    },
    controller: rbPipelineConfigFormManualControlController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbPipelineConfigFormManualControlController(
  WEBLABS,
  translateService,
  rbGlobalSetting,
  applicationService,
  rbPipelineConfigUtility,
  rbRegionDataStore,
) {
  const vm = this;

  const linkServiceSubject = new Subject();
  let linkServiceSubscription = null;

  vm.showRegionAsLabel = showRegionAsLabel;
  vm.selectServiceTypeChanged = selectServiceTypeChanged;
  vm.getInitServiceType = getInitServiceType;
  vm.regionChanged = regionChanged;
  vm.linkServiceChanged = linkServiceChanged;
  vm.getServiceDisplayName = service =>
    rbPipelineConfigUtility.getServiceDisplayName(service);
  vm.onExecEnabledChange = onExecEnabledChange;
  vm.$onInit = onInit;
  vm.$onDestroy = onDestroy;

  /////////
  function onInit() {
    vm.selectServiceType = getInitServiceType();
    vm.selectServiceTypes = [
      {
        type: 'service',
        displayName: translateService.get(
          'pipeline_config_link_service_type_service',
        ),
      },
      {
        type: 'deployed-application',
        displayName: translateService.get(
          'pipeline_config_link_service_type_deployed_application',
        ),
      },
    ];

    vm.regions$ = rbRegionDataStore.data$.pipe(map(regionsWithExecFeature));

    // The region may depend on the selected deploy-app task's region
    linkServiceSubscription = linkServiceSubject
      .pipe(
        filter(() => this.selectServiceType === 'deployed-application'),
        switchMap(service => {
          const parentItem =
            service &&
            vm.workflow.allTasks.find(
              task => task.model.name === service.parent,
            );
          return parentItem ? parentItem.region$ : of({});
        }),
      )
      .subscribe(region => regionChanged(region));
  }

  function onExecEnabledChange() {
    if (!vm.item.model.data.exec_enabled) {
      delete vm.item.model.data.link;
      delete vm.item.model.region;
      delete vm.item.model.region_uuid;
    }
  }

  function getInitServiceType() {
    const type = _.get(vm, 'item.model.data.link.type', '');
    return type === 'deployed-app-service' ? 'deployed-application' : 'service';
  }

  function regionChanged(region) {
    vm.item.region = region;
  }

  function showRegionAsLabel() {
    return vm.selectServiceType !== 'service';
  }

  const regionServices$ = rbPipelineConfigUtility.getRegionServices$({
    region$: vm.item.region$,
  });

  const previousDeployedServices$ = rbPipelineConfigUtility.getPreviousDeployedServices$(
    {
      tasks$: vm.workflow.tasks$,
      item: vm.item,
      skipFilterByRegion: true,
    },
  );

  function selectServiceTypeChanged(type) {
    if (type !== vm.selectServiceType) {
      vm.selectServiceType = type;
      delete vm.item.model.data.link;
    }
    if (type === 'service') {
      vm.availableServices$ = regionServices$;
    } else if (type === 'deployed-application') {
      vm.availableServices$ = previousDeployedServices$;
    }
  }

  function linkServiceChanged(service) {
    vm.form['link_service_name'].$setDirty();
    if (service !== vm.item.model.data.link) {
      vm.item.model.data.link = service;
      linkServiceSubject.next(service);
    }
  }

  function regionsWithExecFeature(regions) {
    return (regions || []).filter(region => {
      const features = [
        ..._.get(region, 'features.service.features', []),
        ..._.get(region, 'features.service.metrics.features', []),
      ];

      return features.includes('exec');
    });
  }

  function onDestroy() {
    linkServiceSubscription && linkServiceSubscription.unsubscribe();
  }
}
