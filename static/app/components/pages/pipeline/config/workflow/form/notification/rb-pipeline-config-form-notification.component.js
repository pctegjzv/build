import { filter, map } from 'rxjs/operators';

import templateStr from './rb-pipeline-config-form-notification.component.html';

angular
  .module('app.components.pages')
  .component('rbPipelineConfigFormNotification', {
    bindings: {
      item: '<',
      workflow: '<',
    },
    controller: rbPipelineConfigFormNotificationController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbPipelineConfigFormNotificationController(
  rbSafeApply,
  translateService,
  rbNotificationDataStore,
) {
  const vm = this;

  vm.onNotificationListClicked = _.throttle(
    () => rbNotificationDataStore.refetch(),
    10000,
  );
  vm.notificationChanged = notificationChanged;

  vm.$onInit = onInit;
  vm.$onDestroy = onDestroy;
  /////////
  function onInit() {
    vm.item.loading = () => vm.item.touched && !vm.notifications;
    vm.notificationsSubscriber = rbNotificationDataStore.data$
      .pipe(
        filter(notifications => !!notifications),
        map(notifications =>
          notifications.map(notification => ({
            displayName: notification.space_name
              ? `${notification.name}(${notification.space_name})`
              : notification.name,
            name: notification.name,
            uuid: notification.uuid,
          })),
        ),
      )
      .subscribe(notifications => {
        vm.notifications = notifications;
        rbSafeApply();
      });
  }

  function onDestroy() {
    vm.notificationsSubscriber.unsubscribe();
  }

  function notificationChanged({ name, uuid }) {
    vm.item.model.data.notification = name;
    vm.item.model.data.notification_uuid = uuid;
  }
}
