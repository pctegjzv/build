import * as _ from 'lodash';
import { filter, map } from 'rxjs/operators';

import templateStr from './rb-pipeline-config-form-on-end.component.html';

angular.module('app.components.pages').component('rbPipelineConfigFormOnEnd', {
  bindings: {
    item: '<',
    workflow: '<',
  },
  controller: rbPipelineConfigFormOnEndController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbPipelineConfigFormOnEndController(
  rbSafeApply,
  translateService,
  rbNotificationDataStore,
) {
  const vm = this;

  vm.onNotificationListClicked = _.throttle(
    () => rbNotificationDataStore.refetch(),
    10000,
  );
  vm.notificationChanged = notificationChanged;

  vm.$onInit = onInit;
  vm.$onDestroy = onDestroy;
  /////////
  function onInit() {
    vm.item.loading = () => vm.item.touched && !vm.notifications;
    const doNotSelectNotification = {
      displayName: translateService.get('pipeline_select_none'),
      name: '',
      uuid: '',
    };
    vm.notificationsSubscriber = rbNotificationDataStore.data$
      .pipe(
        filter(notifications => !!notifications),
        map(notifications =>
          notifications.map(notification => ({
            displayName: notification.space_name
              ? `${notification.name}(${notification.space_name})`
              : notification.name,
            name: notification.name,
            uuid: notification.uuid,
          })),
        ),
        map(notifications => [doNotSelectNotification, ...notifications]),
      )
      .subscribe(notifications => {
        vm.notifications = notifications;
        rbSafeApply();
      });
  }

  function onDestroy() {
    vm.notificationsSubscriber.unsubscribe();
  }

  function notificationChanged({ name, uuid }) {
    vm.item.model.data.notification = name;
    vm.item.model.data.notification_uuid = uuid;
  }
}
