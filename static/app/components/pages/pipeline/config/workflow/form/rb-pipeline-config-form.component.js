import * as _ from 'lodash';
import { filter, map } from 'rxjs/operators';

import * as HELP_TEXTS_EN from '../help-text.en';
import * as HELP_TEXTS_CN from '../help-text.zh_cn';

const TASK_TIMEOUT_LIMIT = 24 * 60; // Total minutes of a day

const NON_TASK_TYPES = [
  'triggers',
  'task_type_selection',
  'on_end',
  // All other types are considered as tasks
];

const TASK_TYPES = [
  'test-container',
  'deploy-application',
  'notification',
  'manual-control',
  'exec',
  'update-service',
  'update-service-new',
  'sync-registry',
  'artifact-upload',
  'artifact-download',
  'ci-report',
  'trigger-pipeline',
  'approve',
];

const COMMON_BINDINGS = [
  ['item', 'vm.item'],
  ['form', 'vm.form'],
  ['workflow', 'vm.workflow'],
  ['mode', 'vm.mode'],
  ['isNewKxsRegion', 'vm.isNewK8sRegion$ | rbAsync:this'],
];

const TYPES_WITH_REGION_SELECTION = [
  'test-container',
  'deploy-application',
  'update-service',
  'update-service-new',
  // For exec, we need more precise control on how region field is displayed
  // so it is not included here.
];

const TYPES_NOT_SUPPORT_NEW_K8S = ['deploy-application'];

// Pipeline Config Forms factory component.
// - a factory as well as a component to be used within a template.
// - generates inner component based on the type
// - pass through params from workflow to individual form element.
// - provides basic container decoration, i.e., title header, helper text.

// TODO:
//   Consider use observable for the available services options.
const templateStr = require('app/components/pages/pipeline/config/workflow/form/rb-pipeline-config-form.component.html');
angular.module('app.components.pages').component('rbPipelineConfigForm', {
  bindings: {
    mode: '<', // view / update / create / copyCreate
    item: '<',
    workflow: '<',
    form: '=', // reference to the form created within this component
  },
  controller: rbPipelineConfigFormController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbPipelineConfigFormController(
  $scope,
  rbRegionDataStore,
  rbRegionUtilities,
  rbMarkdownInfoDialog,
  translateService,
) {
  const vm = this;
  vm.getTitle = getTitle;
  vm.showRegionSelection = showRegionSelection;
  vm.regionChanged = regionChanged;
  vm.isTaskSelectionType = isTaskSelectionType;
  vm.getTaskHelpTextPreview = getTaskHelpTextPreview;
  vm.newK8sNotSupported = newK8sNotSupported;

  vm.$onInit = onInit;
  vm.$postLink = postLink;
  vm.helpLinkClicked = helpLinkClicked;

  const isNewK8sRegion = region => rbRegionUtilities.isNewK8sRegion(region);

  const displayType = type =>
    type === 'update-service-new' ? 'update-service' : type;

  vm.isNewK8sRegion$ = vm.item.regionSubject$.pipe(
    filter(region => !!region),
    map(isNewK8sRegion),
  );

  function newK8sNotSupported() {
    return TYPES_NOT_SUPPORT_NEW_K8S.includes(vm.item.type);
  }

  function onInit() {
    vm.taskTimeoutLimit = TASK_TIMEOUT_LIMIT;
    vm.HELP_TEXTS =
      translateService.getLanguage() === 'zh_cn'
        ? HELP_TEXTS_CN
        : HELP_TEXTS_EN;

    vm.regions$ = rbRegionDataStore.allNonEmpty$.pipe(
      map(
        regions =>
          newK8sNotSupported()
            ? regions.map(region => ({
                ...region,
                op_disabled: isNewK8sRegion(region),
              }))
            : regions,
      ),
    );
  }

  function postLink() {
    vm.form = $scope.form;
    updateFormTemplate();
  }

  function getTitle() {
    if (NON_TASK_TYPES.includes(vm.item.type)) {
      return translateService.get('pipeline_config_form_title', {
        type: translateService.get('pipeline_item_title_' + vm.item.type),
      });
    } else {
      return getTaskFormTitle(displayType(vm.item.type));
    }
  }

  function getTaskFormTitle(taskType) {
    return translateService.get('pipeline_config_form_title_task', {
      type: translateService.get('pipeline_task_type_' + taskType),
    });
  }

  function regionChanged(region) {
    vm.item.region = region;
    if (['update-service', 'update-service-new'].includes(vm.item.type)) {
      const taskType = isNewK8sRegion(region)
        ? 'update-service-new'
        : 'update-service';

      vm.item.type = vm.item.model.type = taskType;
    }
  }

  function showRegionSelection() {
    return TYPES_WITH_REGION_SELECTION.includes(vm.item.type);
  }

  function isTaskSelectionType() {
    return vm.item.type === 'task_type_selection';
  }

  function helpLinkClicked(type = vm.item.type) {
    const title = NON_TASK_TYPES.includes(type)
      ? translateService.get('pipeline_item_title_' + type)
      : getTaskFormTitle(type);
    rbMarkdownInfoDialog.show({
      md: getHelpText(type),
      title,
    });
  }

  function getHelpText(type) {
    return vm.HELP_TEXTS[_.snakeCase(type).toUpperCase()];
  }

  function getTaskHelpTextPreview() {
    return getHelpText(displayType(vm.item.type)).replace(/\s+/g, ' ');
  }

  /**
   * Update the inner form template when:
   *   - component onInit
   *   - type changed (?)
   */
  function updateFormTemplate(type = vm.item.type) {
    if (![...NON_TASK_TYPES, ...TASK_TYPES].includes(type)) {
      throw new Error(
        'Pipeline config form: Unknown form template type ' + type,
      );
    }

    const componentTag =
      'rb-pipeline-config-form-' + _.kebabCase(displayType(type));

    vm.template = _generateComponentTemplate(componentTag);
  }

  // e.g.:
  // _generateComponentTemplate('asdAsdA', [[binding1, expr1], [binding2, expr2]])
  //   => "<asd-asd-a a="a" b="b" c-asd-s="cAsdS"></asd-asd-a>"
  function _generateComponentTemplate(componentTag) {
    const attrs = COMMON_BINDINGS.map(
      ([binding, expr]) => `${_.kebabCase(binding)}="${expr}"`,
    ).join(' ');
    return `<${componentTag} ${attrs}></${componentTag}>`;
  }
}
