import * as _ from 'lodash';
import { combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  first,
  map,
  startWith,
} from 'rxjs/operators';

import { resource_types as RESOURCE_TYPES } from '../../../../../rbac_role/rb-rbac-helper.constant';
const templateStr = require('app/components/pages/pipeline/config/workflow/form/sync-registry/rb-pipeline-config-form-sync-registry.component.html');
angular
  .module('app.components.pages')
  .component('rbPipelineConfigFormSyncRegistry', {
    bindings: {
      form: '<',
      mode: '<',
      item: '<',
      workflow: '<',
    },
    controller: rbPipelineConfigFormSyncRegistryController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbPipelineConfigFormSyncRegistryController(
  rbSafeApply,
  rbRoleUtilities,
  rbSyncRegistryDataStore,
  rbImageSyncCenterCreateDialog,
  WEBLABS,
) {
  const vm = this;
  let uploadPathSub;
  let artifactEnabledSub;

  vm.configChanged = configChanged;
  vm.editConfig = editConfig;
  vm.createConfig = createConfig;

  vm.buttonDisplayExpr = buttonDisplayExpr;

  vm.$onInit = onInit;
  vm.$onDestroy = onDestroy;
  vm.uploadArtifactsChanged = uploadArtifactsChanged;
  vm.uploadPathChanged = uploadPathChanged;

  /////////

  function configChanged(config) {
    vm.currentConfig = config;
    vm.item.model.data.config_name = config && config.config_name;
    vm.item.model.data.config_uuid = config && config.config_id;
  }

  function uploadArtifactsChanged(enabled) {
    if (enabled) {
      vm.item.model.data.share_path = vm.uploadPath;
    } else {
      vm.item.model.data.share_path = '';
    }
  }

  function uploadPathChanged(path) {
    vm.item.model.data.share_path = path;
  }

  async function editConfig() {
    await rbImageSyncCenterCreateDialog.show({
      config: vm.currentConfig,
      disableRepoSelection: true,
    });
    await rbSyncRegistryDataStore.refetch();
    rbSafeApply();
  }

  async function createConfig() {
    const repository = await vm.workflow.triggerRepository$
      .pipe(
        filter(data => !!data),
        first(),
      )
      .toPromise();
    const tRegistry = repository.registry.name;
    const tProject = repository.project ? repository.project.project_name : '';
    const tRepository = repository.name;

    const newConfig = await rbImageSyncCenterCreateDialog.show({
      registry: tRegistry,
      project: tProject,
      repository: tRepository,
      disableRepoSelection: true,
    });

    if (newConfig) {
      configChanged(newConfig);
      await rbSyncRegistryDataStore.refetch();
      rbSafeApply();
    }
  }

  function onInit() {
    vm.weblabs = WEBLABS;

    // The config list should be filtered with the current selected repository
    vm.configs$ = combineLatest([
      vm.workflow.triggerRepository$,
      rbSyncRegistryDataStore.data$,
    ]).pipe(
      map(
        ([repository, configs]) =>
          repository &&
          !repository.disabled &&
          configs &&
          configs.filter(config => {
            const cRegistry = _.get(config, 'source.info.registry_name');
            const cProject = _.get(config, 'source.info.project_name') || '';
            const cRepository = _.get(config, 'source.info.repository_name');

            const tRegistry = repository.registry.name;
            const tProject = repository.project
              ? repository.project.project_name
              : '';
            const tRepository = repository.name;

            return (
              tRegistry === cRegistry &&
              tProject === cProject &&
              tRepository === cRepository
            );
          }),
      ),
    );
    vm.configsSubscriber = vm.configs$
      .pipe(
        filter(data => !!data),
        first(),
      )
      .subscribe(configs => {
        vm.loading = false;
        const currentCurrentInOptions = configs.find(
          config =>
            config.config_name === vm.currentConfig &&
            vm.currentConfig.config_name,
        );
        configChanged(currentCurrentInOptions);
      });

    vm.canCreate$ = rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.SYNC_CONFIG,
    );

    uploadPathSub = vm.workflow.triggerRepository$
      .pipe(
        filter(data => !!data),
        map(
          repo =>
            `/images/${repo.namespace}/${(repo.project &&
              repo.project.project_name) ||
              repo.namespace}/${repo.name}`,
        ),
        startWith(vm.mode === 'create' ? '' : vm.item.model.data.share_path),
        filter(path => !!path),
        distinctUntilChanged(),
      )
      .subscribe(path => {
        vm.uploadPath = path;
        rbSafeApply();
      });

    artifactEnabledSub = vm.workflow.artifactEnabled$
      .pipe(
        filter(value => !value),
        startWith(
          vm.mode === 'create' ? true : !!vm.item.model.data.share_path,
        ),
      )
      .subscribe(value => {
        vm.uploadArtifactsEnabled = value;
        uploadArtifactsChanged(vm.uploadArtifactsEnabled);
        rbSafeApply();
      });
  }

  function onDestroy() {
    if (uploadPathSub) {
      uploadPathSub.unsubscribe();
    }

    if (artifactEnabledSub) {
      artifactEnabledSub.unsubscribe();
    }
  }

  function buttonDisplayExpr(action) {
    return rbRoleUtilities.resourceHasPermission(
      vm.currentConfig,
      RESOURCE_TYPES.SYNC_CONFIG,
      action,
    );
  }
}
