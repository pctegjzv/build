import * as _ from 'lodash';

import { TASK_TYPES } from '../../../../rb-pipeline-constant';
import * as HELP_TEXTS_EN from '../../help-text.en';
import * as HELP_TEXTS_CN from '../../help-text.zh_cn';
const templateStr = require('app/components/pages/pipeline/config/workflow/' +
  'form/task-type-selection/rb-pipeline-config-form-task-type-selection.component.html');
angular
  .module('app.components.pages')
  .component('rbPipelineConfigFormTaskTypeSelection', {
    bindings: {
      item: '<',
      workflow: '<',
    },
    controller: rbPipelineConfigFormTaskTypeSelectionController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbPipelineConfigFormTaskTypeSelectionController(
  translateService,
  WEBLABS,
) {
  const vm = this;
  let subscription = null;
  vm.taskTypeSelected = taskTypeSelected;
  vm.getShortTaskHelpText = getShortTaskHelpText;

  if (
    WEBLABS.TRIGGER_PIPELINE_BY_PIPELINE_ENABLED &&
    !TASK_TYPES.includes('trigger-pipeline')
  ) {
    TASK_TYPES.push('trigger-pipeline');
  }

  vm.$onInit = onInit;
  vm.$onDestroy = onDestroy;
  /////////
  function onInit() {
    vm.HELP_TEXTS =
      translateService.getLanguage() === 'zh_cn'
        ? HELP_TEXTS_CN
        : HELP_TEXTS_EN;
    if (WEBLABS['HUATAI_ENABLED']) {
      if (!TASK_TYPES.includes('ci-report')) {
        TASK_TYPES.push('ci-report');
      }
      if (!TASK_TYPES.includes('approve')) {
        TASK_TYPES.push('approve');
      }
    }
    vm.taskTypes = TASK_TYPES.map(taskType => {
      const disabledFunction = getTypeDisabledFunction(taskType);
      return {
        name: taskType,
        get disabled() {
          return disabledFunction();
        },
      };
    });

    subscription = vm.workflow.triggerRepositorySubject$.subscribe(repo => {
      vm.repoDisabled = repo && repo.disabled;
    });
  }

  function onDestroy() {
    subscription && subscription.unsubscribe();
  }

  function getShortTaskHelpText(taskType) {
    const mdKey = (_.snakeCase(taskType.name) + '_SHORT').toUpperCase();
    return vm.HELP_TEXTS[mdKey];
  }

  function taskTypeSelected(taskType) {
    !taskType.disabled && vm.item.replaceWithType(taskType.name);
  }

  function getTypeDisabledFunction(taskType) {
    switch (taskType) {
      case 'deploy-application':
      case 'update-service':
      case 'registry-scan': // not ready
        return () =>
          vm.repoDisabled
            ? 'pipeline-config-task-disabled-because-no-image'
            : '';
      case 'sync-registry':
        return () =>
          vm.repoDisabled
            ? 'pipeline-config-task-disabled-because-no-image'
            : '';
      case 'artifact-upload':
      case 'artifact-download':
        return () =>
          !vm.workflow.model.artifact_enabled
            ? 'pipeline-config-task-disabed-because-no-artifact'
            : '';
      default:
        return () => '';
    }
  }
}
