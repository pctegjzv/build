import * as _ from 'lodash';
import { of } from 'rxjs';
import { first, filter, map, switchMap } from 'rxjs/operators';

import templateStr from './rb-pipeline-config-form-test-container.component.html';

const REGISTRY_TYPES = [
  'registry_type_manual',
  'registry_type_docker_official',
  'registry_type_custom',
];

angular
  .module('app.components.pages')
  .component('rbPipelineConfigFormTestContainer', {
    bindings: {
      form: '<',
      item: '<',
      workflow: '<',
    },
    controller: rbPipelineConfigFormTestContainerController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbPipelineConfigFormTestContainerController(
  translateService,
  rbModal,
  rbRegionDataService,
  rbRegionUtilities,
  rbRegistryDataStore,
  rbEnvfileDataStore,
  rbRepositoryTagDataStore,
  rbPipelineConfigUtility,
  rbPipelineDataService,
  rbCreateObservableFunction,
  WEBLABS,
  rbGlobalSetting,
) {
  const vm = this;

  vm.currentRegistryType = currentRegistryType;
  vm.useExistingRegistries = useExistingRegistries;
  vm.registryTypeChanged = registryTypeChanged;
  vm.imageTagBlurred = imageTagBlurred;
  vm.selectServiceTypeChanged = selectServiceTypeChanged;
  vm.linkServiceChanged = linkServiceChanged;
  vm.envfileChanged = envfileChanged;
  vm.getServiceDisplayName = getServiceDisplayName;
  vm.onDropdownTagChanged = onDropdownTagChanged;
  vm.editorOptions = rbGlobalSetting.getEditorOptions('shell');

  const { envFrom } = vm.item.model.data;
  vm.initConfigMaps =
    envFrom && envFrom.map(({ configMapRef: { uuid } }) => uuid);

  vm.loadConfigMaps = loadConfigMaps;
  vm.addEnv = addEnv;
  vm.removeEnv = removeEnv;
  vm.onRefChange = onRefChange;
  vm.configMapChange = configMapChange;
  vm.configMapKeyChange = configMapKeyChange;
  vm.previewConfigMapValue = previewConfigMapValue;
  vm.configMapAdded = configMapAdded;
  vm.configMapRemoved = configMapRemoved;
  vm.namespaceChanged = namespaceChanged;

  vm.servicesDropdownClicked = _.throttle(_servicesDropdownClicked, 10000);

  vm.getInitServiceType = getInitServiceType;

  vm.$onInit = onInit;
  vm.$onDestroy = onDestroy;

  vm.item.regionSubject$
    .pipe(filter(region => !!region))
    .subscribe(async region => {
      vm.isNewK8sRegion = rbRegionUtilities.isNewK8sRegion(region);
      const { data } = vm.item.model;
      if (vm.isNewK8sRegion) {
        vm.regionId = region.id;
        data.env_file = undefined;
        data.env_file_uuid = undefined;
        data.env_vars = undefined;
        vm.namespaces = undefined;
        vm.namespaces = await rbRegionDataService.getRegionNamespaceOptions(
          vm.regionId,
        );
        if (
          data.k8s_namespace_uuid &&
          !vm.namespaces.find(({ uuid }) => uuid === data.k8s_namespace_uuid)
        ) {
          data.k8s_namespace = data.k8s_namespace_uuid = undefined;
        }

        if (!vm.namespaces.length) {
          vm.loadConfigMaps(vm.regionId);
        }
      } else {
        data.env_vars = data.env_vars || [];
        data.env = data.envFrom = data.k8s_namespace = data.k8s_namespace_uuid = undefined;
      }
    });

  /////////
  function onInit() {
    vm.selectServiceType = getInitServiceType();
    vm.selectServiceTypes = [
      {
        type: '',
        displayName: translateService.get(
          'pipeline_config_link_service_type_none',
        ),
      },
      {
        type: 'service',
        displayName: translateService.get(
          'pipeline_config_link_service_type_service',
        ),
      },
      {
        type: 'deployed-application',
        displayName: translateService.get(
          'pipeline_config_link_service_type_deployed_application',
        ),
      },
    ];

    vm.envfiles$ = rbEnvfileDataStore.data$.pipe(
      map(
        envfiles =>
          envfiles && [
            {
              name: '',
              displayName: translateService.get('pipeline_select_none'),
              uuid: '',
            },
            ...envfiles.map(envfile => ({
              name: envfile.name,
              displayName: envfile.space_name
                ? `${envfile.name}(${envfile.space_name})`
                : envfile.name,
              uuid: envfile.uuid,
            })),
          ],
      ),
    );

    vm.registryTypes = REGISTRY_TYPES.map(type => ({
      type,
      displayName: translateService.get('pipeline_config_' + type),
    }));

    vm.repository$ = rbCreateObservableFunction(vm, 'repositoryChanged');

    vm.repositoryTags$ = vm.repository$.pipe(
      switchMap(repository => {
        if (repository && repository.name) {
          const registry_name = repository.registry.name;
          const project_name =
            repository.project && repository.project.project_name;
          const repository_name = repository.name;

          // Some side effects here, but I think its OK!
          vm.item.model.data.project = project_name;
          vm.item.model.data.repo_name = repository_name;
          vm.item.model.data.registry_uuid = repository.registry.uuid;
          vm.item.model.data.registry_index = repository.registry.endpoint;
          vm.item.model.data.registry_name = registry_name;
          vm.item.model.data.repository_name = repository.name;
          vm.item.model.data.repository_uuid = repository.uuid;
          return rbRepositoryTagDataStore.getInstance({
            registry_name,
            project_name,
            repository_name,
          }).data$;
        } else {
          return of([]);
        }
      }),
      map(tags => tags && tags.map(tag => ({ name: tag }))),
    );

    vm.repositoryInitObject = getRepositoryInitObject();
  }

  function currentRegistryType() {
    return vm.item.model.data.isPrivateRegistrySelected()
      ? 'registry_type_manual'
      : vm.item.model.data.registry_type;
  }

  function useExistingRegistries() {
    return vm.item.model.data.isPrivateRegistrySelected();
  }

  function registryTypeChanged(type) {
    if (type === currentRegistryType()) {
      return;
    }

    if (type === 'registry_type_manual') {
      rbRegistryDataStore.data$
        .pipe(
          filter(data => !!data),
          first(),
        )
        .subscribe(([registry]) => {
          vm.item.model.data.registry_uuid = registry.uuid;
          vm.item.model.data.registry_index = registry.endpoint;
          vm.item.model.data.registry_name = registry.name;
        });
    } else {
      vm.item.model.data.registry_uuid = undefined;
      vm.item.model.data.registry_index =
        type === 'registry_type_docker_official'
          ? 'index.docker.io'
          : undefined;
      vm.item.model.data.registry_name = undefined;
    }
  }

  function imageTagBlurred() {
    if (vm.item.model.data.repo_name && !vm.item.model.data.image_tag) {
      vm.item.model.data.image_tag = 'latest';
    }
  }

  function selectServiceTypeChanged(type) {
    if (type !== vm.selectServiceType) {
      vm.selectServiceType = type;
      vm.item.model.data.link = {};
    }
    if (type === 'service') {
      vm.availableServices$ = rbPipelineConfigUtility.getRegionServices$({
        region$: vm.item.region$,
      });
    } else if (type === 'deployed-application') {
      vm.availableServices$ = rbPipelineConfigUtility.getPreviousDeployedServices$(
        {
          tasks$: vm.workflow.tasks$,
          item: vm.item,
        },
      );
    }
  }

  function _servicesDropdownClicked() {
    if (vm.selectServiceType === 'service') {
      rbPipelineConfigUtility.refetchRegionServices(vm.item);
    }
  }

  function linkServiceChanged(service) {
    vm.item.model.data.link = service;
  }

  function getInitServiceType() {
    const type = _.get(vm, 'item.model.data.link.type', '');
    switch (type) {
      case 'service':
      case 'application-service':
        return 'service';
      case 'deployed-app-service':
        return 'deployed-application';
      default:
        return '';
    }
  }

  function envfileChanged(envfile) {
    vm.item.model.data.env_file = envfile.name ? envfile.name : undefined;
    vm.item.model.data.env_file_uuid = envfile.uuid ? envfile.uuid : undefined;
  }

  function getRepositoryInitObject() {
    return {
      get repository() {
        return vm.item.model.data.repo_name;
      },
      get project() {
        return vm.item.model.data.project;
      },
      get registry() {
        return vm.item.model.data.registry_name;
      },
    };
  }

  function onDropdownTagChanged(option) {
    if (option && option.name) {
      vm.item.model.data.image_tag = option.name;
    }
  }

  async function loadConfigMaps(clusterId, namespace) {
    vm.configMapList = [];

    vm.configMaps = await rbPipelineDataService.getConfigMapOptions({
      clusterId,
      namespace,
    });

    if (vm.item.model.data.envFrom) {
      vm.item.model.data.envFrom = vm.item.model.data.envFrom.filter(
        ({ configMapRef: { uuid } }) =>
          vm.configMaps.find(configMap => configMap.uuid === uuid),
      );
    }

    if (vm.item.model.data.env) {
      vm.item.model.data.env.forEach(({ uuid, valueFrom }) => {
        if (uuid && !vm.configMaps.find(configMap => configMap.uuid === uuid)) {
          valueFrom.configMapKeyRef = {};
        }
      });
    }
  }

  function addEnv() {
    if (vm.workflow.readonly) {
      return;
    }

    const { data } = vm.item.model;

    if (!data.env) {
      data.env = [];
    }

    data.env = [...data.env, {}];
  }

  function removeEnv(index) {
    if (vm.workflow.readonly) {
      return;
    }

    vm.item.model.data.env = vm.item.model.data.env.filter(
      (item, i) => i !== index,
    );
  }

  function onRefChange(envItem, refEnabled) {
    envItem.value = undefined;
    envItem.valueFrom = refEnabled ? { configMapKeyRef: {} } : undefined;
  }

  function configMapChange({ name, namespace, uuid }, configMapKeyRef, index) {
    Object.assign(configMapKeyRef, {
      name,
      uuid,
    });
    rbPipelineDataService
      .getConfigMap({
        clusterId: vm.regionId,
        namespace,
        name,
      })
      .then(configMap => {
        const configMapData = configMap.kubernetes.data;
        vm.configMapList[index] = {
          keys: Object.keys(configMapData),
          map: configMapData,
        };
      });
  }

  function configMapKeyChange(key, configMapKeyRef) {
    configMapKeyRef.key = key;
  }

  function previewConfigMapValue(index, configMapKeyRef) {
    const { key } = configMapKeyRef;
    if (!key) {
      return;
    }
    const configMap = vm.configMapList[index].map;
    const value = configMap[key];
    rbModal.show({
      locals: { content: value },
      template: '<rb-config-preview content="content"></rb-config-preview>',
      closable: true,
      autofocus: true,
    });
  }

  function configMapAdded({ name, uuid }) {
    const { envFrom = [] } = vm.item.model.data;
    if (envFrom.find(({ configMapRef }) => uuid === configMapRef.uuid)) {
      return;
    }
    vm.item.model.data.envFrom = [...envFrom, { configMapRef: { name, uuid } }];
  }

  function configMapRemoved({ uuid }) {
    vm.item.model.data.envFrom = vm.item.model.data.envFrom.filter(
      ({ configMapRef }) => uuid !== configMapRef.uuid,
    );
  }

  function namespaceChanged({ name, uuid }) {
    if (!vm.namespaces) {
      return;
    }

    Object.assign(vm.item.model.data, {
      k8s_namespace: name,
      k8s_namespace_uuid: uuid,
    });

    vm.loadConfigMaps(vm.regionId, name);
  }

  function getServiceDisplayName(service) {
    return rbPipelineConfigUtility.getServiceDisplayName(service);
  }

  function onDestroy() {}
}
