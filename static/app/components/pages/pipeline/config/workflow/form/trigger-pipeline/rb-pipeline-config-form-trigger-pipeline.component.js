import template from './rb-pipeline-config-form-trigger-pipeline.component.html';

angular
  .module('app.components.pages')
  .component('rbPipelineConfigFormTriggerPipeline', {
    bindings: {
      mode: '<',
      item: '<',
      workflow: '<',
      form: '<',
    },
    controller(
      rbPipelineDataService,
      translateService,
      rbRepositoryDataService,
    ) {
      const vm = this;
      vm.isBuildOrRepository = vm.item.model.data.trigger_type === 'build';
      vm.item.model.data.version_from = 'manual';
      vm.item.model.data.execute_type = 'async';
      Object.assign(vm, {
        async loadVersions({ registry, repository }) {
          if (vm.isBuildOrRepository) {
            return;
          }

          let repo_name = repository;
          let project_name;

          if (repo_name.includes('/')) {
            const names = repo_name.split('/');
            project_name = names[0];
            repo_name = names[1];
          }

          vm.versions = undefined;
          vm.versions = await rbRepositoryDataService.getRepositoryTags({
            registry_name: registry,
            repo_name,
            project_name,
            page_size: 20,
          });

          const { data } = vm.item.model;

          if (
            vm.versions.includes('latest') &&
            (!data.version || !vm.versions.includes(data.version))
          ) {
            data.version = 'latest';
          }
        },
        async $onInit() {
          const { results } = await rbPipelineDataService.getPipelines();
          const { uuid } = vm.workflow.model;
          const {
            data: { pipeline_uuid, pipeline_name, project_name, version },
          } = vm.item.model;

          const pipelines = results.filter(pipeline => pipeline.uuid !== uuid);

          // in case selected pipeline is hidden for current user, resolve #DEV-5602
          if (
            pipeline_uuid &&
            !pipelines.some(({ uuid }) => uuid === pipeline_uuid)
          ) {
            pipelines.push({
              uuid: pipeline_uuid,
              name: pipeline_name,
              project_name,
              triggers: {
                build: vm.isBuildOrRepository,
              },
            });
            vm._versions = [version];
          }

          vm.pipelines = pipelines;
        },
        pipelineChanged({
          uuid,
          name,
          project_name,
          triggers: { build, repository },
        }) {
          vm.isBuildOrRepository = !!build;
          const { pipeline_uuid } = vm.item.model.data;
          Object.assign(vm.item.model.data, {
            pipeline_uuid: uuid,
            pipeline_name: name,
            project_name,
            trigger_type: vm.isBuildOrRepository ? 'build' : 'repository',
          });
          if (!vm.isBuildOrRepository) {
            // if build === false, it is a mock pipeline item, simply set versions to stored version
            if (build === false && vm._versions) {
              vm.versions = vm._versions;
            } else {
              vm.loadVersions(repository);
            }
          } else if (pipeline_uuid !== uuid) {
            vm.item.model.data.version = undefined;
          }
        },
        versionTypes: ['trigger', 'manual'].map(type => ({
          name: translateService.get(`pipeline_config_version_${type}`),
          type,
        })),
        versionTypeChanged(type) {
          const { data } = vm.item.model;
          if (data.version_from === type) {
            return;
          }
          data.version = undefined;
          data.version_from = type;
        },
        executeTypes: ['sync', 'async'].map(type => ({
          name: translateService.get(`pipeline_config_execute_${type}`),
          type,
        })),
      });
    },
    controllerAs: 'vm',
    template,
  });
