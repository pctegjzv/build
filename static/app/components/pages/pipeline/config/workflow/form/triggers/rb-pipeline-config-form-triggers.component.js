import * as _ from 'lodash';
import { of } from 'rxjs';
import { filter, first, map, switchMap } from 'rxjs/operators';

import templateStr from './rb-pipeline-config-form-triggers.component.html';

import { resource_types as RESOURCE_TYPES } from '../../../../../rbac_role/rb-rbac-helper.constant';

const TRIGGER_TYPES = ['build', 'repository'];

const DEFAULT_SCHEDULE_RULE = '0 8 * * *';
const DEFAULT_IMAGE_TAG = 'latest';

angular
  .module('app.components.pages')
  .component('rbPipelineConfigFormTriggers', {
    bindings: {
      mode: '<',
      item: '<',
      workflow: '<',
      form: '<',
    },
    controller: rbPipelineConfigFormTriggersController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbPipelineConfigFormTriggersController(
  $scope,
  rbModal,
  rbFloatingPage,
  rbRoleUtilities,
  translateService,
  rbBuildCodeRepoClientSelectDialog,
  rbBuildConfigDataStore,
  rbRepositoryDataStore,
  rbRepositoryTagDataStore,
  WEBLABS,
) {
  const vm = this;

  vm.$onInit = onInit;
  vm.$onDestroy = onDestroy;

  vm.triggerTypeChanged = triggerTypeChanged;
  vm.getTriggerType = getTriggerType;
  vm.getBuildTrigger = getBuildTrigger;
  vm.isRepositoryTrigger = isRepositoryTrigger;
  vm.isBuildTrigger = isBuildTrigger;
  vm.isAutoTriggerDisabled = isAutoTriggerDisabled;
  vm.isBuildImageDisabled = isBuildImageDisabled;
  vm.isTimingTriggerDisabled = isTimingTriggerDisabled;

  vm.buildConfigButtonDisplayExpr = buildConfigButtonDisplayExpr;

  vm.buildChanged = buildChanged;
  vm.editBuildConfig = _.throttle(editBuildConfig, 1000);
  vm.createBuildConfig = _.throttle(createBuildConfig, 1000);

  vm.registryChanged = registryChanged;
  vm.repositoryChanged = repositoryChanged;
  vm.projectChanged = projectChanged;

  vm.timingSwitchChanged = timingSwitchChanged;
  vm.autoTriggerChanged = autoTriggerChanged;
  vm.timingRuleChanged = timingRuleChanged;
  vm.imageTagChanged = imageTagChanged;
  /////////

  function getRepositoryTrigger() {
    return _.get(vm, 'item.model.repository', {});
  }

  function getBuildTrigger() {
    return _.get(vm, 'item.model.build', {});
  }

  function getTriggerType() {
    return _.get(vm, 'item.model.triggerType');
  }

  function isRepositoryTrigger() {
    return getTriggerType() === 'repository';
  }

  function isBuildTrigger() {
    return getTriggerType() === 'build';
  }

  function isAutoTriggerDisabled() {
    return (
      vm.currentBuildConfig &&
      !vm.currentBuildConfig.ci_enabled &&
      vm.isBuildTrigger()
    );
  }

  function isTimingTriggerDisabled() {
    return (
      vm.currentBuildConfig &&
      vm.currentBuildConfig.code_repo.code_repo_type_value.includes('*') &&
      vm.isBuildTrigger()
    );
  }

  function isBuildImageDisabled() {
    return (
      vm.currentBuildConfig &&
      !vm.currentBuildConfig.build_image_enabled &&
      vm.isBuildTrigger()
    );
  }

  function registryChanged(registry) {
    if (!registry || !isRepositoryTrigger()) {
      return;
    }
    getRepositoryTrigger().registry = registry.name;
    getRepositoryTrigger().registry_uuid = registry.uuid;
    getRepositoryTrigger().registry_display_name = registry.display_name;
  }

  function projectChanged(project) {
    if (!project || !isRepositoryTrigger()) {
      return;
    }
    getRepositoryTrigger().project =
      !project || project.is_default ? undefined : project.project_name;
  }

  function repositoryChanged(repository) {
    if (!repository || !isRepositoryTrigger()) {
      return;
    }
    getRepositoryTrigger().repo = repository.name;
    getRepositoryTrigger().repository_uuid = repository.uuid;

    vm.workflow.triggerChanged();
  }

  function buildChanged(build) {
    if (!build || !isBuildTrigger()) {
      return;
    }

    vm.currentBuildConfig = build;

    getBuildTrigger().build_config_uuid = build.uuid;
    getBuildTrigger().build_config_name = build.name;

    if (vm.isAutoTriggerDisabled()) {
      vm.autoTriggerEnabled = false;
    }

    if (vm.isTimingTriggerDisabled()) {
      vm.timingTriggerEnabled = false;
    }

    vm.workflow.triggerChanged();
  }

  function timingSwitchChanged(enabled) {
    if (!enabled) {
      timingRuleChanged('');
      imageTagChanged('');
    } else {
      if (!vm.timingRule) {
        vm.timingRule = DEFAULT_SCHEDULE_RULE;
      }
      if (!vm.imageTag) {
        vm.imageTag = DEFAULT_IMAGE_TAG;
      }
      timingRuleChanged(vm.timingRule);
      imageTagChanged(vm.imageTag);

      if (vm.weblabs.PIPELINE_AUTO_TRIGGER_CONFIG_ENABLED) {
        vm.autoTriggerEnabled = false;
        autoTriggerChanged(vm.autoTriggerEnabled);
      }
    }
  }

  function autoTriggerChanged(enabled) {
    getRepositoryTrigger().auto_trigger_enabled = enabled;
    getBuildTrigger().auto_trigger_enabled = enabled;
  }

  function timingRuleChanged(rule) {
    getBuildTrigger().schedule_rule = rule;
    getRepositoryTrigger().schedule_rule = rule;
  }

  function imageTagChanged(tag) {
    getRepositoryTrigger().image_tag = tag;
  }

  function getRepositoryInitObject() {
    return {
      get repository() {
        return getRepositoryTrigger().repo;
      },
      get project() {
        return getRepositoryTrigger().project;
      },
      get registry() {
        return getRepositoryTrigger().registry;
      },
    };
  }

  function triggerTypeChanged(type) {
    vm.item.model.triggerType = type;

    if (type === 'build') {
      vm.loading = true;
      vm.buildConfigs$
        .pipe(
          filter(data => !!data),
          first(),
        )
        .subscribe(() => (vm.loading = false));
    }
  }

  async function editBuildConfig() {
    await rbFloatingPage.show({
      title: translateService.get('build_config_update'),
      component: 'rbBuildConfigUpdate',
      locals: {
        buildConfigName: getBuildTrigger().build_config_name,
        buildConfig: vm.currentBuildConfig,
        onUpdate: res => {
          rbBuildConfigDataStore.refetch();
          rbFloatingPage.hide(res);
        },
        onCancel: () => rbFloatingPage.cancel(),
      },
    });
  }

  function createBuildConfig() {
    const onCodeRepoSelected = ({ oauthCodeRepoClientName } = {}) => {
      rbModal.hide();
      rbFloatingPage.show({
        title: translateService.get('build_config_create'),
        component: 'rbBuildConfigCreate',
        locals: {
          oauthCodeRepoClientName,
          onCreate: async newConfigName => {
            const data = await rbBuildConfigDataStore.refetch();
            rbFloatingPage.hide();

            if (data && !(data instanceof Error)) {
              const newBuildConfig = data.find(
                build => build.name === newConfigName,
              );
              if (newBuildConfig) {
                buildChanged(newBuildConfig);
                // Repository may be updated, need to update it
                rbRepositoryDataStore
                  .getInstance({
                    registry_name: newBuildConfig.image_repo.registry.name,
                    project_name:
                      newBuildConfig.image_repo.project &&
                      newBuildConfig.image_repo.project.project_name,
                  })
                  .refetch();
              }
            }
          },
          onCancel: () => rbFloatingPage.cancel(),
        },
      });
    };

    rbBuildCodeRepoClientSelectDialog.show({
      onSelect: onCodeRepoSelected,
    });
  }

  function onInit() {
    vm.weblabs = WEBLABS;
    vm.item.loading = () => vm.item.touched && vm.loading;
    vm.buildConfigs$ = rbBuildConfigDataStore.data$;
    vm.repositoryInitObject = getRepositoryInitObject();

    vm.triggerTypes = TRIGGER_TYPES.map(type => ({
      displayName: translateService.get('pipeline_trigger_type_' + type),
      type,
    }));

    if (vm.mode === 'create') {
      vm.autoTriggerEnabled = true;
      autoTriggerChanged(vm.autoTriggerEnabled);
    } else {
      vm.autoTriggerEnabled = isRepositoryTrigger()
        ? getRepositoryTrigger().auto_trigger_enabled
        : getBuildTrigger().auto_trigger_enabled;
    }

    // We only allow trigger type selection when mode is create
    if (vm.mode !== 'create') {
      vm.triggerTypes = vm.triggerTypes.filter(
        ({ type }) => type === getTriggerType(),
      );
    }

    vm.canCreateBuild$ = rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.BUILD_CONFIG,
    );

    vm.repositoryTags$ = vm.workflow.triggerRepository$.pipe(
      switchMap(repository => {
        if (repository) {
          const registry_name = repository.registry.name;
          const project_name =
            repository.project && repository.project.project_name;
          const repository_name = repository.name;

          return rbRepositoryTagDataStore.getInstance({
            registry_name,
            project_name,
            repository_name,
          }).data$;
        }
        return of([]);
      }),
      map(tags => tags && tags.map(tag => ({ name: tag }))),
    );

    if (
      getRepositoryTrigger().schedule_rule ||
      getBuildTrigger().schedule_rule
    ) {
      vm.timingTriggerEnabled = true;
      vm.timingRule =
        getRepositoryTrigger().schedule_rule || getBuildTrigger().schedule_rule;
      vm.imageTag = getRepositoryTrigger().image_tag;
    }
  }

  function onDestroy() {
    rbFloatingPage.cancel();
  }

  function buildConfigButtonDisplayExpr(action) {
    return rbRoleUtilities.resourceHasPermission(
      vm.currentBuildConfig,
      RESOURCE_TYPES.BUILD_CONFIG,
      action,
    );
  }
}
