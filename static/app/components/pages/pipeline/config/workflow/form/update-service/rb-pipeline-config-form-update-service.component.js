import * as _ from 'lodash';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import templateStr from './rb-pipeline-config-form-update-service.component.html';

angular
  .module('app.components.pages')
  .component('rbPipelineConfigFormUpdateService', {
    bindings: {
      form: '<',
      item: '<',
      workflow: '<',
      taskTimeoutLimit: '<',
      isNewKxsRegion: '<',
    },
    controller: rbPipelineConfigFormUpdateServiceController,
    controllerAs: 'vm',
    template: templateStr,
  });

function rbPipelineConfigFormUpdateServiceController(
  rbPipelineConfigUtility,
  rbPipelineDataService,
  rbEnvfileDataStore,
  rbServiceDataService,
) {
  const vm = this;

  vm.getServiceDisplayName = service =>
    rbPipelineConfigUtility.getServiceDisplayName(service);
  vm.serviceChanged = serviceChanged;
  vm.containerAdded = containerAdded;
  vm.containerRemoved = containerRemoved;
  vm.envfileAdded = envfileAdded;
  vm.envfileRemoved = envfileRemoved;
  vm.servicesDropdownClicked = _.throttle(_servicesDropdownClicked, 10000);

  vm.$onInit = onInit;
  vm.$onDestroy = onDestroy;

  function onInit() {
    initServiceObservables();
    const {
      item: {
        model: { data },
        region$,
      },
    } = vm;
    const containers = data.service && data.service.containers;
    vm.initContainers = containers && containers.map(({ name }) => name);
    vm.envfiles$ = rbEnvfileDataStore.data$;
    vm.initEnvfiles = data.env_files.map(envfile => envfile.uuid);
    data.env_files = [];
    data.mount_points = data.mount_points || [];
    vm.configFileEnabled$ = region$.pipe(
      map(region => _.get(region, 'features.service.features', [])),
      map(features => features.includes('mount-points')),
    );
    vm.containers$ = new Subject();
    vm.configMaps$ = new Subject();
  }

  function initServiceObservables() {
    vm.availableServices$ = rbPipelineConfigUtility.getRegionServices$({
      region$: vm.item.region$,
      repository$: vm.workflow.triggerRepository$,
    });
  }

  async function serviceChanged(service) {
    const { data, region_uuid } = vm.item.model;

    if (service && vm.isNewKxsRegion) {
      const serviceContainers = data.service && data.service.containers;

      data.service = _.clone(service);

      const { namespace } = service;

      const [{ kubernetes }, configMaps] = await Promise.all([
        rbServiceDataService.getNewK8sService(service.uuid),
        rbPipelineDataService.getConfigMapOptions({
          clusterId: region_uuid,
          namespace,
        }),
      ]);

      const kubernete = kubernetes.find(({ kind }) =>
        ['Deployment', 'StatefulSet', 'DaemonSet'].includes(kind),
      );

      const { containers } = kubernete.spec.template.spec;

      if (serviceContainers) {
        data.service.containers = serviceContainers.filter(({ name }) =>
          containers.find(container => container.name === name),
        );
      }

      vm.containers$.next(containers);
      vm.configMaps$.next(configMaps);
    } else {
      data.service = _.clone(service);
    }
  }

  function containerAdded(container) {
    const { service } = vm.item.model.data;

    let { containers } = service;
    if (!containers) {
      containers = service.containers = [];
    }

    const containerName = container.name;

    if (containers.find(({ name }) => containerName === name)) {
      return;
    }

    containers.push({
      name: containerName,
      use_image_in_trigger:
        container.use_image_in_trigger ||
        service.triggerImage === container.image,
    });
  }

  function containerRemoved(container) {
    const { service } = vm.item.model.data;
    service.containers = service.containers.filter(
      ({ name }) => name !== container.name,
    );
  }

  function envfileAdded(envfile) {
    vm.item.model.data.env_files.push({
      name: envfile.name,
      uuid: envfile.uuid,
    });
  }

  function envfileRemoved(envfile) {
    vm.item.model.data.env_files = vm.item.model.data.env_files.filter(
      _file => envfile.name !== _file.name,
    );
  }

  function _servicesDropdownClicked() {
    rbPipelineConfigUtility.refetchRegionServices(vm.item);
  }

  function onDestroy() {}
}
