export const TRIGGERS = `
In order to start a pipeline please select one of the following types of triggers:

- ** Repository **: Whenever a new image is pushed to the selected repository, a new pipeline execution will start automatically.

A pipeline can also be manually triggered using a pre-existing image in the repository.

- ** Build **: Select a new build configuration to be the starting point of this Pipeline.
Once started, the pipeline will automatically start process and will only continue further tasks once the build is completed successfully.


Note: You can use the image to deploy temporary applications or to update existing services.
`;

export const ON_END = `
Add a notification of your preference to be notified once a pipeline execution ends.

*Note: This notification will be sent containing the result of the pipeline execution whether it fails or succeeds*.
`;

export const DEPLOY_APPLICATION_SHORT = `
Use the triggered image to deploy an app using an app template.
`;

export const DEPLOY_APPLICATION = `
**${DEPLOY_APPLICATION_SHORT}**

<br>
You can select one or more services inside your app template to be deployed with the new image.
The remaining services will be deployed with the image described inside the **app template**.

After the pipeline execution, the deployed application will be removed automatically.
`;

export const UPDATE_SERVICE_SHORT = `
Automatically update a service already deployed to your cluster using the triggered image.
`;

export const UPDATE_SERVICE = `
**${UPDATE_SERVICE_SHORT}**

<br>

**Cluster**: Select a cluster that the tasks will run on. It should be previously setup and configured.

**Select Service**: The available service options are the services deployed by the image defined in the "trigger" config step.

**Environment File**: If selected, one or more environment variable files will be applied to the service during the update.

**Environment Variables**: Will add custom environment variables to the service during the update.

**Automatic rollback**: If selected will automatically rollback the service to the version previous
 to the update if any task fails during the execution of the pipeline.

<br>
*Note: In order to keep environment consistency, a Pipeline execution will enter in a queue once
 it gets to this task. The task will only be executed once other pipelines in the same cluster that started previously finish execution.*
`;

export const EXEC_SHORT = `
Run one or multiple terminal commands inside a running service.
`;

export const EXEC = `
**${EXEC_SHORT}**


Used to execute scripts, run data migrations, or any other necessary tasks.
It is recommended that the commands to be previously tested and avoid run-time errors.

- **Select Service**: Select a pre-existing service or an application deployed during the pipeline execution.
- **Cluster**: Select a cluster that the tasks will run on.
Note, if you select a service in a Pipeline Deployed App, the cluster will be subject to it.
- **Instance**: Select an instance either manually or automatically.
  - **Auto**: the first instance will be selected. This is the only option for service deployed within this pipeline.
  - **Manual**: choose an instance of the service.
- **Command**: the commands you want to execute
- **Entrypoint**: the entrypoint to run the container
`;

export const MANUAL_CONTROL_SHORT = `
Interrupt temporarily the pipeline execution until manual confirmation.
`;

export const MANUAL_CONTROL = `
**${MANUAL_CONTROL_SHORT}**

<br>
During the execution the user can manualy set the result of the task and control to either execute the next tasks or fail the pipeline.

During the manual confirmation the user can also insert more detailed information regarding the decision.
`;

export const NOTIFICATION_SHORT = `
Send a pre-configured notification.
`;

export const NOTIFICATION = `
**${NOTIFICATION_SHORT}**
`;

export const ARTIFACT_UPLOAD_SHORT = `
Upload artifacts to shared space
`;

export const ARTIFACT_UPLOAD = `
**${ARTIFACT_UPLOAD_SHORT}**

The artifacts produced by this pipeline (prior to this task) will be uploaded to the
provided shared path.

Once the artifacts are uploaded completely, the pipeline will proceed to the next task.
`;

export const ARTIFACT_DOWNLOAD_SHORT = `
Download artifacts from shared space to this task
`;

export const ARTIFACT_DOWNLOAD = `
**${ARTIFACT_DOWNLOAD_SHORT}**

The artifact files in the targeting shared path will be copied to the location subject
to this pipeline task.

Once the artifacts are downloaded completely, the pipeline will proceed to the next task.
`;

export const TEST_CONTAINER_SHORT = `
Run a test inside a docker container.
`;

export const TEST_CONTAINER = `
**${TEST_CONTAINER_SHORT}**

<br>
The task failure or success is defined by the exit status of the container:
If a container exits with code 0 it will be considered successful, any other codes are considered as failure.

- **Cluster**: Select a cluster that the tasks will run on.
- **Image registry and Image**: Select a registry and image in which your Docker image is stored.
- **Image Tag**: Insert the version tag of the image you setup. Defaults to **latest**.
- **Link to Service**: Select a pre-existing service or an application deployed during the pipeline execution.
During the execution the docker container will be initiated with some extra environment variables that can be used to connect to the linked service.
For example: If a service called \`web\` is deployed using \`HTTP\` protocol
and port \`80\` the following environment variables will be automatically added to the *linked* services:
  - \`WEB_PORT_80_HTTP_ADDR\`: Full qualified domain name to linked service - \`myservice.myapp-myorg.myalauda.cn\`
  - \`WEB_PORT_80_HTTP_PORT\`: Port number used to access the service
  - \`WEB_PORT_80_HTTP_PROTO\`: Procol used to access the service - \`http\`
  - \`WEB_PORT_80_HTTP\`: Full address including port, eg. \`http://myservice.myapp-myorg.myalauda.cn:80\`

- **Environment Variables File**: Used to initial the container with pre-defined environment variables.
- **Environment Variables**: Used to set custom Environment Variables to the container.
- **Exec Commands**: Single line command will be used as the command of the image to start containers, and multiple lines commands will be used as the entrypoint of the image to start containers. When entering multiple lines commands, the first line of the command, such as /bin/sh, can not be omitted.
- **Timeout**: Time limit in minutes. If the task does not finish executing after the defined timeout it will automatically fail.
`;

export const SYNC_REGISTRY_SHORT = `
Registry Sync Task
`;

export const SYNC_REGISTRY = `
**${SYNC_REGISTRY_SHORT}**

To easily sync repositories accross registries select or create a Sync config with the necessary data.

During the execution of the pipeline, the Sync process will start and be monitored.
If any of the sync processes fail, the pipeline will fail automatically and the execution will be interrupted.
`;

export const CI_REPORT_SHORT = `
Send the build report to specific mailboxes
`;

export const CI_REPORT = `
**${CI_REPORT_SHORT}**

Send the build report to specific mailboxes.
Build report covers build durations at each step, and other details, such as EMMA coverage ratio. The build report can be sent to several mailboxes.
`;

export const TRIGGER_PIPELINE_SHORT = `
Trigger other pipelines asynchronously on the platform.
`;

export const TRIGGER_PIPELINE = `
**${TRIGGER_PIPELINE_SHORT}**

Pipeline Name: you can only select the pipelines which are either within your operation account authority or the previous triggered ones.
Code Version: if the Trigger type of the pipelines is code repository, enter the version of the code.
Image Version: if the Trigger type of the pipelines is image repository, enter the version of the image.
`;

export const APPROVE_SHORT = `
Apply to some users for approval
`;

export const APPROVE = `
**${APPROVE_SHORT}**

Used for scenarios that require approval. After the approval is passed, the pipeline can continue execution. It is recommended to inform the approver of the assembly line information in advance.

- **Assign Approvers**: Required, select users with permissions related to the project.
- **Send Emails**: Optional, need to input manually, add more by carriage return or space.
`;

export const TASK_TYPE_SELECTION = `
**Select a Task Type**

- **Deploy App**: ${DEPLOY_APPLICATION_SHORT}
- **Update Service**: ${UPDATE_SERVICE_SHORT}
- **EXEC Service**: ${EXEC_SHORT}
- **Manual Control**: ${MANUAL_CONTROL_SHORT}
- **Notification**: ${NOTIFICATION_SHORT}
- **Test**: ${TEST_CONTAINER_SHORT}
- **Registry Sync Task**: ${SYNC_REGISTRY_SHORT}
- **Artifact Upload**: ${ARTIFACT_UPLOAD_SHORT}
- **Artifact Download**: ${ARTIFACT_DOWNLOAD_SHORT}
- **CI Report**: ${CI_REPORT_SHORT}
- **Tigger Pipeline**: ${TRIGGER_PIPELINE_SHORT}
- **Approve**: ${APPROVE_SHORT}
`;

export const EXEC_EXAMPLES = [
  {
    description: `Download output to container:`,
    command: `eval "curl -X GET \${__ARTIFACTS_HOST__}\${__ARTIFACTS_PATH__}/\${__ARTIFACTS_PACKAGE_NAME__} --silent -O  -H 'authorization: token \${__USER_TOKEN__}' -H 'Content-Type: application/octet-stream; charset=utf-8'"`,
  },
  {
    description: `Upload files to server:`,
    command: `eval "curl -X PUT -T \${__ARTIFACTS_PACKAGE_NAME__} -H 'authorization: token \${__USER_TOKEN__}' -H 'Content-Type: application/octet-stream; charset=utf-8' \${__ARTIFACTS_HOST__}\${__ARTIFACTS_PATH__}/\${__ARTIFACTS_PACKAGE_NAME__}"`,
  },
];
