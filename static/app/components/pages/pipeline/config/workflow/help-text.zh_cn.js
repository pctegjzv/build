export const TRIGGERS = `
流水线运行可以选择以下触发条件：

- ** 镜像仓库 **: 当新镜像被推送到镜像仓库时，流水线会自动启动执行。你还可以使用仓库中已存在的镜像手动触发流水线。

- ** 代码仓库 **: 选择或创建构建项目作为流水线的触发条件。当代码更新到代码仓库时，流水线会先执行构建过程。
预先定义的任务将在构建成功后开始执行。


注：你可以使用触发器镜像（或是构建过程中生成的镜像）来部署临时应用、或是更新已有服务。
`;

export const ON_END = `
添加一个流水线完成时发送的可选通知。

*Note: 如果选择了通知后， 不论流水线执行结果是否成功或是失败， 都会通过指定的通知任务把结果发送给用户。*
`;

export const DEPLOY_APPLICATION_SHORT = `
用指定的应用模板部署应用
`;

export const DEPLOY_APPLICATION = `
**${DEPLOY_APPLICATION_SHORT}**

你可以选择应用模板中一个或多个服务使用该镜像进行部署，其余的服务会使用模板里定义好的镜像来部署。

当流水线执行完毕后， 临时部署的应用会被自动移除。
`;

export const UPDATE_SERVICE_SHORT = `
用触发流水线的镜像自动更新到集群中已有服务
`;

export const UPDATE_SERVICE = `
**${UPDATE_SERVICE_SHORT}**


**集群**: 选择执行流水线任务的集群。

**选择服务**: 可选择的服务为以**当前选择的触发器镜像**创建的服务。

**环境变量文件**：使用一个或多个环境变量文件更新服务

**环境变量**：定义一些环境变量更新服务

**自动回滚**: 如果选择了自动回滚， 当任何流水线任务失败时将会自动把此任务更新的服务回滚到上一版本。

*Note：为了保证环境的一致性，流水线执行更新服务任务时会加入到一个队列，当前面包含同样任务的流水线执行完毕后，本次更新服务操作将会开始*
`;

export const EXEC_SHORT = `
在运行的服务里执行一个或多个命令
`;

export const EXEC = `
**${EXEC_SHORT}**


用于执行命令，比如数据迁移脚本等。建议要执行的命令经过测试以避免执行错误。

- **集群**: 选择执行流水线任务的集群。如果服务为流水线部署的应用，则此集群为相应部署应用的集群
- **选择服务**：选择集群里已创建的服务或者临时应用里的某个服务
- **容器实例**：手动或自动选择容器实例
  - **自动**：默认选择服务的第一个实例
  - **手动**：让用户从容器实例列表中选择一个实例
- **命令**: 即 EXEC，任务将在任务实例中执行此命令
- **接入点**：即 Entrypoint，默认为 \`/bin/sh\`
`;

export const MANUAL_CONTROL_SHORT = `
暂停流水线执行直到人工确认
`;

export const MANUAL_CONTROL = `
**${MANUAL_CONTROL_SHORT}**


在流水线执行过程中，你可以手动控制任务执行结果，来决定是否继续执行流水线剩下的任务或终止流水线。

确定时，不管何种结果，均可以填写详细描述信息。
`;

export const NOTIFICATION_SHORT = `
发送指定通知
`;

export const NOTIFICATION = `
**${NOTIFICATION_SHORT}**
`;

export const ARTIFACT_UPLOAD_SHORT = `
将此任务之前的产出物上传至公共空间
`;

export const ARTIFACT_UPLOAD = `
**${ARTIFACT_UPLOAD_SHORT}**

流水线将会把当前流水线所产生的产出物， 拷贝到指定公共目录中。


此任务将在上传完成后结束， 并开始执行后面的任务。
`;

export const ARTIFACT_DOWNLOAD_SHORT = `
将公共空间的产出物拷贝至此流水线
`;

export const ARTIFACT_DOWNLOAD = `
**${ARTIFACT_DOWNLOAD_SHORT}**

流水线将会把指定公共目录中所有文件， 拷贝到当前任务在服务器上所对应的目录中。


此任务将在复制完成后结束， 并开始执行后面的任务。
`;

export const TEST_CONTAINER_SHORT = `
在容器里执行已定义好的命令或者写好的脚本、程序
`;

export const TEST_CONTAINER = `
**${TEST_CONTAINER_SHORT}**

该任务的结果取决于容器的退出码。如果退出码为0，则认为执行成功，否则视为失败。


- **集群**: 选择执行流水线任务的集群。
- **镜像仓库源与镜像**：选择镜像仓库源和镜像。
- **镜像版本**：输入已配置镜像的版本，默认是latest。
- **链接服务**：选择集群里已创建的服务或者临时应用里的某个服务。在任务容器启动时，会有一些额外的环境变量传入容器，这样可以访问链接的服务了。例如，某web服务部署时使用HTTP协议和80端口，下面的环境变量会自动传入到任务容器里。
  - \`WEB_PORT_80_HTTP_ADDR\`: 链接服务的域名，比如\`myservice.myapp-myorg.myalauda.cn\`
  - \`WEB_PORT_80_HTTP_PORT\`: 链接服务的访问端口
  - \`WEB_PORT_80_HTTP_PROTO\`: 链接服务的访问协议
  - \`WEB_PORT_80_HTTP\`: 链接服务的完整访问地址，比如\`http://myservice.myapp-myorg.myalauda.cn:80\`
- **环境变量文件**: 用预先定义的环境变量文件初始化容器的环境变量值。
- **环境变量**: 用于设置容器的自定义环境变量值。
- **执行命令**: 单行命令会作为镜像的command来启动容器，多行的命令会作为镜像的entrypoint来启动容器；如果输入多行命令，首行的解释器说明（例如#!/bin/sh）不能省略。
- **超时**: 以分钟为单位的超时时间。如果任务在超时时限内没有完成时， 会自动标记为失败状态
`;

export const SYNC_REGISTRY_SHORT = `
执行同步镜像任务
`;

export const SYNC_REGISTRY = `
**${SYNC_REGISTRY_SHORT}**

将构建的镜像或触发器里设置的镜像同步到指定目标镜像中心。
同步结束后，在目标镜像中心将有一个同名镜像仓库。

同步任务失败时，流水线也将会被中止。
`;

export const CI_REPORT_SHORT = `
将构建报告发送到指定邮箱
`;

export const CI_REPORT = `
**${CI_REPORT_SHORT}**

配置构建报告页面：
发送构建报告到指定邮箱
构建报告包含每步耗时，以及类似EMMA覆盖率等详细内容；可发送到多个邮箱
`;

export const TRIGGER_PIPELINE_SHORT = `
异步触发平台上的其他流水线项目
`;

export const TRIGGER_PIPELINE = `
**${TRIGGER_PIPELINE_SHORT}**

流水线名称：可选择的流水线列表为“当前登陆用户有权限操作配置的流水线列表以及原被触发流水线”
代码版本：若被触发的流水线的触发器为代码仓库，则需输入代码版本
镜像版本：若被触发的流水线的触发器为镜像仓库，则需选择镜像版本
`;

export const APPROVE_SHORT = `
向某些用户申请审批
`;

export const APPROVE = `
**${APPROVE_SHORT}**

用于需要审批的场景，审批通过后，该流水线才可以继续执行。建议提前告知审批人该流水线信息。

- **指定审批人**：必选，可选择具备该项目相关权限的用户。
- **发送邮箱**：可选，需要手动输入，通过回车或空格添加多个。
`;

export const TASK_TYPE_SELECTION = `
**选择一个流水线任务**

- **部署应用**: ${DEPLOY_APPLICATION_SHORT}
- **更新服务**: ${UPDATE_SERVICE_SHORT}
- **执行命令**: ${EXEC_SHORT}
- **手动控制**: ${MANUAL_CONTROL_SHORT}
- **通知**: ${NOTIFICATION_SHORT}
- **测试**: ${TEST_CONTAINER_SHORT}
- **镜像同步**: ${SYNC_REGISTRY_SHORT}
- **产出物上传**: ${ARTIFACT_UPLOAD_SHORT}
- **产出物下载**: ${ARTIFACT_DOWNLOAD_SHORT}
- **构建报告**: ${CI_REPORT_SHORT}
- **触发流水线**: ${TRIGGER_PIPELINE_SHORT}
- **流水线审批**: ${APPROVE_SHORT}
`;

export const EXEC_EXAMPLES = [
  {
    description: `下载产出物到容器：`,
    command: `eval "curl -X GET \${__ARTIFACTS_HOST__}\${__ARTIFACTS_PATH__}/\${__ARTIFACTS_PACKAGE_NAME__} --silent -O  -H 'authorization: token \${__USER_TOKEN__}' -H 'Content-Type: application/octet-stream; charset=utf-8'"`,
  },
  {
    description: `上传文件到服务器：`,
    command: `eval "curl -X PUT -T \${__ARTIFACTS_PACKAGE_NAME__} -H 'authorization: token \${__USER_TOKEN__}' -H 'Content-Type: application/octet-stream; charset=utf-8' \${__ARTIFACTS_HOST__}\${__ARTIFACTS_PATH__}/\${__ARTIFACTS_PACKAGE_NAME__}"`,
  },
];
