import { flatMap, flatten } from 'lodash';
import { combineLatest, from, of } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  first,
  map,
  switchMap,
} from 'rxjs/operators';

angular
  .module('app.components.pages')
  .factory('rbPipelineConfigUtility', rbPipelineConfigUtilityFactory);

function rbPipelineConfigUtilityFactory(
  rbAppDataStore,
  rbServiceDataService,
  rbServiceDataStore,
  rbRegionUtilities,
) {
  return {
    getRegionServices$,
    getPreviousDeployedServices$,
    getServiceDisplayName,
    refetchRegionServices,
  };

  //////////

  function getRegionServices$({ region$, repository$ }) {
    let services$ = getServicesByRegion(region$);

    if (repository$) {
      services$ = getFilteredServicesByRepository(services$, repository$);
    }

    return services$.pipe(
      map(
        services =>
          services &&
          services.map(service => ({
            displayName: getServiceDisplayName(service),
            uuid: service.uuid,
            value: service,
          })),
      ),
    );
  }

  function getServicesByRegion(region$) {
    return region$.pipe(
      distinctUntilChanged((prev, next) => prev && prev.id === next.id),
      switchMap(region => {
        if (region) {
          if (rbRegionUtilities.isNewK8sRegion(region)) {
            return combineLatest(
              from(
                rbServiceDataService
                  .getNewK8sServices({
                    cluster: region.name,
                  })
                  .catch(() => []),
              ),
            );
          }

          const appStore = rbAppDataStore.getInstance({
            regionName: region.name,
          });

          const serviceStore = rbServiceDataStore.getInstance({
            regionName: region.name,
          });

          return combineLatest(appStore.data$, serviceStore.data$);
        } else {
          // Returning empty values so that the switch map can respond more eagerly.
          return of([null, null]);
        }
      }),
      map(latests => {
        if (latests.length === 1) {
          const newK8sServices = latests[0];
          return newK8sServices.map(
            ({ resource: { name, uuid }, namespace, parent }) => ({
              name,
              type: parent.uuid ? 'application-service' : 'service',
              uuid,
              parent: parent.name,
              parent_uuid: parent.uuid,
              namespace: namespace.name,
            }),
          );
        }

        const [apps, services] = latests;

        if (!apps || !services) {
          return null;
        }

        const appServices = flatMap(apps, app =>
          app.services.map(appService => ({
            name: appService.service_name,
            type: 'application-service',
            uuid: appService.uuid,
            parent: app.app_name,
            parent_uuid: app.uuid,
            image_name: appService.image_name,
            is_stateful: appService.is_stateful,
          })),
        );

        const _services = services.map(service => ({
          name: service.service_name,
          type: 'service',
          uuid: service.uuid,
          image_name: service.image_name,
          is_stateful: service.is_stateful,
        }));

        return [...appServices, ..._services];
      }),
    );
  }

  function getFilteredServicesByRepository(services$, repository$) {
    return combineLatest(repository$, services$).pipe(
      map(([repository, allServices]) => {
        if (!repository || !repository.registry || !allServices) {
          return null;
        } else {
          const triggerImage = [
            repository.registry.endpoint,
            repository.registry.is_public
              ? repository.namespace
              : repository.project &&
                !repository.project.is_default &&
                repository.project.project_name,
            repository.name,
          ]
            .filter(fraction => !!fraction)
            .join('/');

          return allServices.filter(service => {
            service.triggerImage = triggerImage;
            return service.namespace || service.image_name === triggerImage;
          });
        }
      }),
    );
  }

  /**
   * Get the deployed services which are available for chosen for workflow item {$item}
   *
   * @param tasks$ - the Observable of tasks of a workflow
   * @param item - the workflow item in subject
   * @param skipFilterByRegion - should skip same region check
   */
  function getPreviousDeployedServices$({
    tasks$,
    item,
    skipFilterByRegion = false,
  }) {
    const deployAppTasks$ = tasks$
      // task should has lower order and deploy-application type
      .pipe(
        map(tasks =>
          tasks.filter(
            task =>
              task.order < item.order && task.type === 'deploy-application',
          ),
        ),
      );

    // Should update on latest item's region / task's region and tasks' deployed app service changes
    const deployedAppServicesNested$ = deployAppTasks$.pipe(
      switchMap(tasks => {
        const ob$tuples = tasks.map(task => {
          if (skipFilterByRegion) {
            return combineLatest(of(null), of(null), task.deployedAppServices$);
          } else {
            return combineLatest(
              item.region$,
              task.region$,
              task.deployedAppServices$,
            );
          }
        });
        return ob$tuples.length ? combineLatest(...ob$tuples) : of([]);
      }),
      map(ob$tuples =>
        ob$tuples
          .filter(
            ([region0, region1, deployedAppServices]) =>
              deployedAppServices &&
              (skipFilterByRegion ||
                (region0 && region1 && region0.name === region1.name)),
          )
          .map(([, , deployedAppServices]) => deployedAppServices),
      ),
    );
    return deployedAppServicesNested$.pipe(
      map(deployedAppServicesNested =>
        flatten(deployedAppServicesNested).map(service => ({
          displayName: getServiceDisplayName(service),
          value: service,
        })),
      ),
    );
  }

  function getServiceDisplayName(service = {}) {
    return service.namespace
      ? (service.parent ? service.parent + '.' : '') +
          service.name +
          '(' +
          service.namespace +
          ')'
      : (service.parent ? service.parent + ' / ' : '') + service.name;
  }

  function refetchRegionServices(item) {
    item.region$
      .pipe(
        filter(data => !!data),
        first(),
      )
      .toPromise()
      .then(region => {
        const appStore = rbAppDataStore.getInstance({
          regionName: region.name,
        });
        const serviceStore = rbServiceDataStore.getInstance({
          regionName: region.name,
        });

        appStore.refetch();
        serviceStore.refetch();
      });
  }
}
