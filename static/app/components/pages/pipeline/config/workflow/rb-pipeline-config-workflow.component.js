import * as _ from 'lodash';

/**
 * Config Workflow for a pipeline config.
 * Can switch between different modes:
 *   view / update / create
 *
 * Config object should be passed in, and will be modified in this class.
 * Also, this component will bind a new function `isValid` to the config object
 * to let the user knows whether or not the config is valid.
 */
const templateStr = require('app/components/pages/pipeline/config/workflow/rb-pipeline-config-workflow.html');
angular.module('app.components.pages').component('rbPipelineConfigWorkflow', {
  bindings: {
    config: '<', // pipeline config
    mode: '<', // view / update / create / copyCreate
  },
  controller: rbPipelineConfigWorkflowController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbPipelineConfigWorkflowController(
  $scope,
  $timeout,
  RbPipelineWorkflowViewModel,
  rbPipelineDataService,
  rbBuildConfigDataStore,
  rbSafeApply,
  translateService,
  rbToast,
) {
  const vm = this;

  vm._nameSuffixCounter =
    vm.config && vm.config.stages[0].tasks.length
      ? vm.config.stages[0].tasks.length + 1
      : 1;

  vm.templateNameToServices = {};

  vm.regionToServices = {};

  vm.envfiles = undefined;

  vm.forms = {}; // id -> form

  vm.getTaskFormTitle = getTaskFormTitle;
  vm.getConfigName = getConfigName;
  vm.$onChanges = onChanges;
  /////////

  function onChanges({ mode, config }) {
    if (config && config.currentValue) {
      _initWorkflow();
    }

    if (mode && mode.currentValue) {
      _onModeChanges(mode);
    }
  }

  /**
   * Checks all forms and see if all forms are valid
   */
  function isConfigValid() {
    // Config is valid when:
    // - triggers is active
    // - no error tasks
    // - there is at least one active test task or the trigger is build
    if (vm.workflow) {
      const errorTasks = vm.workflow.allItems.filter(item => item.error);
      const activeTasks = vm.workflow.tasks.filter(item => item.active);
      return (
        vm.workflow.triggers.active &&
        errorTasks.length === 0 &&
        (activeTasks.length > 0 ||
          vm.workflow.triggers.model.triggerType === 'build')
      );
    } else {
      return false;
    }
  }

  /**
   * Initialize the main workflow component
   * @private
   */
  function _initWorkflow() {
    // Bind some controls to the config
    // Yeah I know this is a bad design ... If you see this line, please consider refactoring.
    vm.config.isValid = isConfigValid;

    // Push data to remote. Will either be creating or updating based on mode
    vm.config.push = _pushData;

    vm.workflow = RbPipelineWorkflowViewModel.fromPipelineConfig({
      config: vm.config,
    });
    vm.workflow.readonly = () => vm.submitting || vm.mode === 'view';
    vm.workflow.allItems.forEach(item => {
      if (vm.mode !== 'create') {
        item.touched = true;
      }
    });

    _initTriggersWorkflowItem(vm.workflow.triggers);
    _initOnEndWorkflowItem(vm.workflow.onEnd);
    vm.workflow.tasks.forEach(_initTaskWorkflowItem);
  }

  function _decorateItemArrow(item) {
    item.arrowType = () => {
      const nextItem = vm.workflow.getNextItem(item);
      if (!vm.workflow.readonly && nextItem.touched && nextItem.isTask()) {
        return 'insert';
      } else {
        return '';
      }
    };

    item.onArrowClick = () => {
      if (item.arrowType() === 'insert') {
        const newTask = _addEmptyTask(item.order + 1);
        newTask.onClick();
      }
    };
  }

  function _initFixedWorkflowItem(item) {
    item.onClick = () => {
      vm.workflow.deselectAll();
      item.selected = true;
      vm.selectedItem = item;
      _submitAllForms();
    };

    _decorateItemArrow(item);

    return item;
  }

  function _initTriggersWorkflowItem(triggersItem) {
    _initFixedWorkflowItem(triggersItem);

    triggersItem.state = () => {
      const form = vm.forms[triggersItem.id];
      if (triggersItem.touched) {
        if (form && form.$invalid) {
          return 'error';
        } else {
          return 'active';
        }
      } else {
        return 'inactive';
      }
    };

    // Triggers should be selected initially
    triggersItem.onClick();
  }

  async function _initOnEndWorkflowItem(onEndItem) {
    _initFixedWorkflowItem(onEndItem);

    onEndItem.state = () => (onEndItem.touched ? 'active' : 'inactive');
  }

  /**
   * Attach listeners to a task workflow item
   */
  function _initTaskWorkflowItem(taskItem) {
    // Configure the view model
    taskItem.state = () => {
      const form = vm.forms[taskItem.id];
      if (taskItem.touched) {
        if (
          form &&
          form.$submitted &&
          (form.$invalid || taskItem.type === 'task_type_selection')
        ) {
          return 'error';
        } else {
          return 'active';
        }
      } else {
        return 'inactive';
      }
    };

    // User should not be able to add new tasks when there is no triggers
    const requiredItems = [vm.workflow.triggers];
    taskItem.disabled = () =>
      !requiredItems.every(item => item.active && !item.loading);

    taskItem.onClick = () => {
      if (vm.selectedItem === taskItem) {
        return;
      }

      _submitAllForms();

      vm.workflow.deselectAll();
      vm.selectedItem = taskItem;
      vm.currentTask = taskItem.model;
      taskItem.selected = true;

      // User is clicking a untouched item. We should add a new item
      if (!taskItem.touched && !vm.workflow.readonly) {
        // Add the item in next digest cycle
        $timeout(() => {
          _addEmptyTask();
        });
      }
    };

    taskItem.onRemove = () => {
      vm.config.stages[0].removeTask({ order: taskItem.order });
      vm.workflow.removeTask({ order: taskItem.order });
      delete vm.forms[taskItem.id];
      _getAllForms(true);

      if (taskItem.selected) {
        // Select the last item
        const allTouchedItems = vm.workflow.allItems.filter(
          item => item.touched,
        );
        allTouchedItems[allTouchedItems.length - 1].onClick();
      }
    };

    // Bind a special event handler when the user wants to swap this item with a new item
    taskItem.replaceWithType = type => {
      _replaceTask({ taskItem, type });
    };

    _decorateItemArrow(taskItem);

    switch (taskItem.type) {
      case 'test-container':
        _initTaskTest(taskItem);
        break;
      case 'update-service':
        _initTaskUpdateService(taskItem);
        break;
      case 'exec':
        _initTaskExec(taskItem);
        break;
      case 'notification':
        break;
      case 'deploy-application':
        _initTaskDeployApp(taskItem);
        break;
      case 'artifact-upload':
      case 'artifact-download':
        _initTaskArtifactShare(taskItem);
        break;
    }

    return taskItem;
  }

  function _initTaskTest() {}

  function _initTaskExec() {}

  function _initTaskUpdateService(taskItem) {
    taskItem.state = () => {
      const form = vm.forms[taskItem.id];
      if (taskItem.touched) {
        if (
          form &&
          form.$submitted &&
          (form.$invalid ||
            !taskItem.model.data.service ||
            !taskItem.model.data.service.name)
        ) {
          return 'error';
        } else {
          return 'active';
        }
      } else {
        return 'inactive';
      }
    };
  }

  function _initTaskDeployApp(taskItem) {
    taskItem.state = () => {
      const form = vm.forms[taskItem.id];
      if (taskItem.touched) {
        if (form && form.$submitted && form.$invalid) {
          return 'error';
        } else {
          return 'active';
        }
      } else {
        return 'inactive';
      }
    };
  }

  function _initTaskArtifactShare(taskItem) {
    taskItem.state = () => {
      const form = vm.forms[taskItem.id];
      if (taskItem.touched) {
        if (
          (form && form.$submitted && form.$invalid) ||
          !vm.config.artifact_enabled
        ) {
          return 'error';
        } else {
          return 'active';
        }
      } else {
        return 'inactive';
      }
    };

    taskItem.disabled = () => !vm.config.artifact_enabled;

    taskItem.message = () =>
      !vm.config.artifact_enabled
        ? translateService.get('pipeline_artifact_share_disabled_hint')
        : undefined;
  }

  // Add an empty task to the config and then reflect the change to the workflow
  function _addEmptyTask(order = vm.workflow.tasks.length + 1) {
    if (order === vm.workflow.tasks.length + 1) {
      // Do not add task if last item is an untouched task
      const lastItem =
        vm.workflow.tasks.length > 0 &&
        vm.workflow.tasks[vm.workflow.tasks.length - 1];

      if (lastItem && !lastItem.touched) {
        return;
      }
    }

    // Generate task name based on te current length
    // Note: the default name must also meet the name pattern
    const taskConfig = vm.config.stages[0].addTask({
      // The user need to select the task type manually if the type is not given
      type: 'task_type_selection',
      order,
    });
    const taskItem = vm.workflow.addTaskFromConfig({ taskConfig, order });
    taskConfig.name = `task-${vm._nameSuffixCounter++}`;
    _initTaskWorkflowItem(taskItem);
    _getAllForms(true);

    rbSafeApply();

    return taskItem;
  }

  // Replace a task with the given task type
  function _replaceTask({ taskItem, type }) {
    const newTaskConfig = vm.config.stages[0].replaceTask({
      type,
      order: taskItem.order,
      name: taskItem.title,
    });

    Object.assign(
      newTaskConfig,
      _defaultTaskProps(type, {
        stage: vm.config.stages[0],
        order: newTaskConfig.order,
      }),
    );

    const newTaskItem = vm.workflow.replaceTaskFromConfig({
      id: taskItem.id,
      taskConfig: newTaskConfig,
    });

    _initTaskWorkflowItem(newTaskItem);
    _getAllForms(true);

    newTaskItem.onClick();

    rbSafeApply();
  }

  function _defaultTaskProps(type, options) {
    switch (type) {
      case 'exec':
        return _defaultExecTaskProps(options);
      case 'manual-control':
        return _defaultManualControlTaskProps(options);
      default:
        return {};
    }
  }

  // Set default `exec` task props, inherit prev closest exec setting
  function _defaultExecTaskProps({ stage, order }) {
    const prevTasks = _.dropRightWhile(
      stage.tasks,
      task => task.order >= order,
    );
    const prevIndex = _.findLastIndex(
      prevTasks,
      task =>
        (task.type === 'manual-control' &&
          task.data &&
          task.data.exec_enabled) ||
        task.type === 'exec',
    );

    if (prevIndex < 0) {
      return {};
    }

    const {
      region,
      region_uuid,
      data: { link, entrypoint },
    } = prevTasks[prevIndex];
    return {
      region,
      region_uuid,
      data: {
        link,
        entrypoint: entrypoint || '/bin/sh',
      },
    };
  }

  // Set default `manual-control` task props, inherit prev closest exec setting
  function _defaultManualControlTaskProps({ stage, order }) {
    const prevTasks = _.dropRightWhile(
      stage.tasks,
      task => task.order >= order,
    );
    const prevIndex = _.findLastIndex(
      prevTasks,
      task =>
        (task.type === 'manual-control' &&
          task.data &&
          task.data.exec_enabled) ||
        task.type === 'exec',
    );

    if (prevIndex < 0) {
      return {};
    }

    const {
      region,
      region_uuid,
      data: { link },
    } = prevTasks[prevIndex];
    return {
      region,
      region_uuid,
      data: {
        exec_enabled: true,
        link,
      },
    };
  }

  // Gather all forms. Using lazy initialization.
  let _allForms = undefined;

  function _getAllForms(refresh) {
    if (!_allForms || refresh) {
      const fixedForms = [vm.forms.triggers, vm.forms.onEnd];
      const touchedTaskForms = vm.workflow.tasks
        .filter(task => task.touched && vm.forms[task.id])
        .map(task => vm.forms[task.id]);
      _allForms = fixedForms.concat(touchedTaskForms);
    }

    // Some forms may not yet available
    return _allForms.filter(form => !!form);
  }

  /**
   * Set submit state for all task forms (only touched ones) to true
   * @private
   */
  function _submitAllForms() {
    _getAllForms(true).forEach(form => {
      // 模拟form提交事件。在tooltip组件中监听的是submitted值，改变后每次提交后都会重新渲染tooltip。
      // 这在这里写是因为流水线在切换卡片的时候会提交所有表单并且隐藏之前的表单，这时候如果表单刚要渲染tooltip的时候，
      // semantic-ui无法正确渲染tooltip。因此需要给tooltip一个新的事件，告诉他需要再重新渲染错误提示。
      form.$submitted = !form.$submitted ? 1 : form.$submitted + 1;
      form.$setDirty();
    });
  }

  function _onModeChanges({ currentValue }) {
    if (currentValue !== 'view') {
      _addEmptyTask();
    } else {
      // Remove all untouched task items
      vm.workflow.allItems.forEach(
        item => item.isTask() && !item.touched && item.onRemove(),
      );

      // TODO select the last selected item instead
      vm.workflow.triggers.onClick();
    }
  }

  function getTaskFormTitle(taskType) {
    return translateService.get('pipeline_config_form_title_task', {
      type: translateService.get('pipeline_task_type_' + taskType),
    });
  }

  /**
   * Push data to remote. Either update or create.
   * @private
   */
  async function _pushData() {
    _submitAllForms();

    rbSafeApply();

    if (!isConfigValid()) {
      throw new Error('workflow_forms_invalid');
    }

    const allTitles = vm.workflow.allItems
      .filter(item => item.touched)
      .map(item => item.title);
    const dedupTitles = _.uniq(allTitles);

    if (allTitles.length !== dedupTitles.length) {
      rbToast.error(translateService.get('pipeline_task_no_duplicate_hint'));
      throw new Error('workflow_forms_invalid');
    }

    vm.config.recalculateOrders();
    const rawConfig = _.cloneDeep(vm.config);

    // Remove untouched tasks (should only be the last one)
    // Also remember in P1, there is only one stage
    rawConfig.stages[0].tasks.pop();

    // Tasks with no timeout value should be reset to 0
    rawConfig.stages[0].tasks.forEach(
      task => (task.timeout = task.timeout || 0),
    );

    // If on end task is not valid (no notification is provided), then remove it from the list
    rawConfig.on_end = rawConfig.on_end.filter(task => task.data.notification);

    let res;

    try {
      vm.submitting = true;
      // Ready to submit
      if (vm.mode === 'update') {
        res = await rbPipelineDataService.updatePipeline({
          config: rawConfig,
          name: vm.getConfigName(rawConfig),
        });
      } else {
        res = await rbPipelineDataService.createPipeline({ config: rawConfig });
      }
    } finally {
      vm.submitting = false;
    }

    return res;
  }

  function getConfigName(config) {
    return rbPipelineDataService.getConfigName(config);
  }
}
