import moment from 'moment';

angular
  .module('app.components.pages')
  .factory('rbPipelineHistoryLogService', rbPipelineHistoryLogServicesFactory);

function rbPipelineHistoryLogServicesFactory(LogPollingBase, rbAccountService) {
  const namespace = rbAccountService.getCurrentNamespace();

  class PipelineHistoryLogService extends LogPollingBase {
    constructor({ url, params, flagFunction }) {
      super(url);
      this.params = params;
      this.pollInterval = 5000;
      this._pollCountDown = 0;
      this._getPollFlag = flagFunction || (() => 1);
      this._polled = false;
    }

    getQueryTimes() {
      const lastLogTime = this.lastLogTime;
      if (!lastLogTime) {
        return {};
      }

      const start_time = lastLogTime + 1;
      const end_time = parseInt(moment().valueOf() / 1000);

      return { start_time, end_time };
    }

    getPollParams() {
      const times = this.getQueryTimes();

      const params = {};
      Object.assign(params, this.params, times);

      return params;
    }

    getPollFlag() {
      return this._getPollFlag();
    }

    /**
     * There are latency issues with the log and task status. We need to poll 3 more times.
     * @override
     */
    async poll() {
      const flag = this.getPollFlag();

      if (this.isPollStopped || flag === 0) {
        return;
      }

      await this.pollLogs();

      // Flag = 2 indicates that we need to continue polling until no more logs after 3 more times poll retries
      if (flag === 2) {
        if (this._polled && this.newLogsFlag) {
          this._pollCountDown = 3;
        } else if (this._pollCountDown === 0) {
          this.isPollStopped = true;
        } else {
          this._pollCountDown--;
        }
      }

      this._polled = true;
    }
  }

  function getLogInstance({ type, params, flagFunction }) {
    let url;
    if (type === 'pipeline') {
      url = `/ajax/pipelines/${namespace}/history/${params.name}/${
        params.uuid
      }/logs`;
    } else {
      url = `/ajax/pipelines/${namespace}/history/${params.name}/${
        params.history_uuid
      }/tasks/${params.uuid}/logs`;
    }

    return new PipelineHistoryLogService({
      url,
      params,
      flagFunction,
    });
  }

  return {
    getLogInstance,
  };
}
