const templateStr = require('app/components/pages/pipeline/history/detail/log/rb-pipeline-history-log.html');
angular.module('app.components.pages').component('rbPipelineHistoryLog', {
  controller: rbPipelineHistoryLogController,
  controllerAs: 'vm',
  bindings: {
    type: '@',
    params: '<',
    flagFunction: '&', // Indicates whether or not to pull logs
  },
  template: templateStr,
});

function rbPipelineHistoryLogController(rbPipelineHistoryLogService) {
  const vm = this;

  vm.logService = undefined;
  vm.$onInit = onInit;

  ////////
  function onInit() {
    const logService = rbPipelineHistoryLogService.getLogInstance({
      type: vm.type,
      params: vm.params,
      flagFunction: vm.flagFunction,
    });

    vm.loading = true;
    logService.registerSuccessCb(_registerNewLogsCallback);
    logService.start();
    vm.logService = logService;
  }

  function _registerNewLogsCallback(newLogs) {
    if (newLogs.length > 0) {
      vm.loading = false;
    }
  }
}
