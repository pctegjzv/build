import {
  PIPELINE_STATUS_TO_ITEM_STATE,
  SERVICE_STATUS_TO_CSS_MAP,
} from '../../rb-pipeline-constant';
import { resource_types as RESOURCE_TYPES } from '../../../rbac_role/rb-rbac-helper.constant';
import _ from 'lodash';
import moment from 'moment';

import templateStr from 'app/components/pages/pipeline/history/detail/rb-pipeline-history-detail.html';

angular.module('app.components.pages').component('rbPipelineHistoryDetail', {
  bindings: {
    pipelineName: '<',
    historyId: '<',
  },
  controller: rbPipelineHistoryDetailController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbPipelineHistoryDetailController(
  $log,
  $element,
  $state,
  rbDelay,
  rbToast,
  rbModal,
  translateService,
  rbRouterStateHelper,
  rbConfirmBox,
  rbSafeApply,
  rbRoleUtilities,
  rbBuildDataService,
  rbImageSyncDataService,
  rbRepositoryDataService,
  applicationService,
  rbRegistryDataStore,
  ARTIFACT_UPLOAD_STATE,
  WEBLABS,
  RbPipelineWorkflowViewModel,
  RbPipelineWorkflowItemViewModel,
  RbPipelineWorkflowItemFieldViewModel,
  rbPipelineDataService,
) {
  const vm = this;

  vm.isPipelineFinished = () =>
    ['completed', 'failed'].includes(vm.history.status);
  vm.deleteHistory = deleteHistory;
  vm.stopHistory = stopHistory;
  vm.getRegistryDisplayName$ = getRegistryDisplayName$;
  vm.getCreatorName = getCreatorName;
  vm.buttonDisplayExpr = buttonDisplayExpr;
  vm.taskControlButtonDisplayExpr = taskControlButtonDisplayExpr;

  vm.$onInit = onInit;
  vm.$onDestroy = _destroy;

  //////////
  function onInit() {
    vm.WEBLABS = WEBLABS;
    // Note: we are relying on references of the following data. Don't accidentally overwrite them.
    vm.history = {
      data: {},
    };

    vm.stage = {
      tasks: [],
    };

    vm.onEnd = {};

    _initWorkflow();
  }

  function _destroy() {
    vm.destroyed = true;
  }

  async function _initWorkflow() {
    try {
      await _refreshPipelineHistory();

      vm.workflow = new RbPipelineWorkflowViewModel({
        readonly: true,
        triggers: _initTrigger(),
        // onEnd and tasks will be init later
      });
    } catch (error) {
      $log.error(
        `Init workflow failed: ${(error && error.message) || 'unknown'}`,
      );
      if (
        error.data &&
        error.data.errors &&
        error.data.errors[0].code === 'resource_not_exist'
      ) {
        $state.go('pipeline.history.list');
      }
      vm.loadError = error;
      return;
    }

    _initPipelineLog();

    await _refreshPipelineHistoryTasks();

    _startHistoryPolling();
  }

  async function _getArtifactPrefices() {
    vm.pipelinePrefix = `pipeline/${vm.history.pipeline_uuid}/${
      vm.history.uuid
    }`;
    const buildId = await _getBuildId();
    if (buildId) {
      return [vm.pipelinePrefix, `build/${buildId}/`];
    }
    return [vm.pipelinePrefix];
  }

  async function _getBuildId() {
    return _getBuildIdFromBuildTask() || _getBuildIdFromImageInfo();
  }

  function _getBuildIdFromBuildTask() {
    const buildTask = vm.stage.tasks.find(item => item.type === 'build');
    return buildTask ? _.get(buildTask, 'data.build_id') : undefined;
  }

  async function _getBuildIdFromImageInfo() {
    if (vm.history.data && vm.history.data.image_tag) {
      const projectAndRepo = vm.history.data.repository.split('/');
      let project;
      let repository;
      if (projectAndRepo.length === 2) {
        [project, repository] = projectAndRepo;
      } else {
        [repository] = projectAndRepo;
      }

      const { build_id } = (await rbRepositoryDataService
        .getRepositoryTagArtifacts({
          repo_name: repository,
          registry_name: vm.history.data.registry,
          project_name: project,
          tag_name: vm.history.data.image_tag,
        })
        .catch(() => {})) || { build_id: '' };

      return build_id;
    }
  }

  function _initPipelineLog() {
    vm.pipelineLogParams = {
      name: vm.pipelineName,
      uuid: vm.history.uuid,
    };

    vm.pipelineLogFlagFunction = () => {
      if (vm.destroyed) {
        return 0;
      } else {
        return vm.isPipelineFinished() ? 2 : 1;
      }
    };
  }

  function _initTrigger() {
    const trigger = RbPipelineWorkflowItemViewModel.fromHistoryTrigger({
      triggerData: vm.history.data,
      type: vm.history.trigger,
    });
    trigger.onClick = () => {
      trigger.selected = true;
      focusDetailTabByCardId(trigger.id);
    };
    trigger.onClick(); // Trigger is selected by default
    trigger.title = translateService.get(trigger.title);
    trigger.loading = () =>
      Object.keys(vm.history.data).length === 0 && isPollingHistory();
    return trigger;
  }

  async function _startHistoryPolling() {
    let taskToPoll = undefined;

    do {
      await _refreshPipelineHistory();

      // Find the current polling task
      const allTasks = _.concat([], vm.stage.tasks, [vm.onEnd]);
      // Get the first 'pending', 'ongoing', 'deploying' task
      const pollIndex = allTasks.findIndex(task =>
        ['pending', 'ongoing', 'deploying'].includes(task.status),
      );

      if (
        pollIndex !== -1 &&
        (pollIndex === 0 || allTasks[pollIndex - 1].status !== 'failed')
      ) {
        taskToPoll = allTasks[pollIndex];
        // If the previous one failed, do not poll this task.
        $log.log(`Polling for ${taskToPoll.name}`);
        _refreshPipelineHistoryTask(taskToPoll);
      } else {
        taskToPoll = undefined;
      }

      _refreshArtifactsTable();
      await rbDelay(5000);
    } while (!vm.destroyed && (isPollingHistory() || taskToPoll));

    // To be safe, poll the last time for all tasks
    _refreshPipelineHistoryTasks();

    while (
      !vm.destroyed &&
      vm.workflow.onEnd &&
      ['pending', 'ongoing', 'deploying'].includes(vm.workflow.onEnd.status)
    ) {
      // We need to continue polling on end task until its status changed
      await _refreshPipelineHistoryTask(vm.onEnd);

      await rbDelay(1000);
    }
  }

  function _refreshArtifactsTable() {
    const artifactsTableScope = angular
      .element('rb-artifacts-table', $element)
      .isolateScope();
    if (artifactsTableScope) {
      artifactsTableScope.vm.refreshArtifactList();
    }
  }

  async function _refreshPipelineHistory() {
    const history = await rbPipelineDataService
      .getPipelineHistory({
        pipelineName: vm.pipelineName,
        historyId: vm.historyId,
      })
      .catch(e => {
        // requesting history does not exist
        if (!vm.initialized && e.status !== 403) {
          rbToast.warning(translateService.get('pipeline_his_not_exist'));
        }
        rbRouterStateHelper.back();
      });
    vm.piplineDisplayName = history.name;

    // Using assign so that the reference does not change
    _.merge(vm.history, history);
    vm.history.duration = _calculateTimeCost(history);
    vm.artifactPrefices = (await _getArtifactPrefices()).join(',');
  }

  function _constructTaskItem(task) {
    const item = RbPipelineWorkflowItemViewModel.fromTask({ task });
    item.touched = true;
    item.onClick = () => {
      item.selected = true;
      focusDetailTabByCardId(item.id);
    };
    item.running = () => item.state === 'active';
    item.disabled = () => ['inactive', 'pending'].includes(item.state);
    item.onStateChange = _onItemStateChange(item);
    item.state = () => PIPELINE_STATUS_TO_ITEM_STATE[task.status];
    item.resource_actions = task.resource_actions || [];
    return item;
  }

  // If state changed from pending to active, select this item
  function _onItemStateChange(item) {
    return ({ current, previous }) => {
      if (
        current === 'active' &&
        (previous === 'pending' || !previous) &&
        !item.selected
      ) {
        item.onClick();
      }
    };
  }

  // Fetch tasks data and update status
  // Note, this function will update the whole pipeline. We should try to break it down to reduce duplicate calls.
  async function _refreshPipelineHistoryTasks() {
    const {
      stages,
      on_end = [{}],
    } = await rbPipelineDataService.getPipelineHistoryTasks({
      pipelineName: vm.pipelineName,
      historyId: vm.historyId,
    });

    // Using assign so that the reference does not change
    _.merge(vm.stage, stages[0]);
    // There should be only one on_end task
    _.merge(vm.onEnd, on_end[0]);

    if (!vm.workflow.tasks) {
      vm.workflow.tasks = vm.stage.tasks.map(_constructTaskItem);

      if (vm.onEnd && vm.onEnd.type) {
        vm.workflow.onEnd = _constructTaskItem(vm.onEnd);

        // Backend returns on end task of types notification, but we need to
        // migrate it to on_end
        vm.workflow.onEnd.type = 'on_end';
        vm.workflow.onEnd.title = translateService.get(
          'pipeline_item_title_on_end',
        );
      }
    }

    const allTasks = _.concat(vm.stage.tasks, vm.onEnd);

    allTasks.filter(task => !!task.type).forEach(_enhanceItemWithTask);
  }

  // Refresh a single task
  async function _refreshPipelineHistoryTask(task) {
    const updatedTask = await rbPipelineDataService.getPipelineHistoryTask({
      pipelineName: vm.pipelineName,
      historyId: vm.historyId,
      taskId: task.uuid,
    });

    const taskToMerge =
      vm.stage.tasks.find(_task => _task.uuid === task.uuid) || vm.onEnd;
    _.merge(taskToMerge, updatedTask);

    _enhanceItemWithTask(updatedTask);
  }

  // Some task properties will be updated while running. We need to do something about it
  function _enhanceItemWithTask(task) {
    const item = vm.workflow.allTasks.find(
      _item => task.uuid === _item.model.uuid,
    );

    if (!item) {
      // A new task ???
      return;
    }

    item.duration = _calculateTimeCost(task);

    // Some tasks does not have logs
    // Build and repo sync has its own log lifecycle.
    if (
      !['manual-control', 'notification', 'build', 'sync-registry'].includes(
        task.type,
      )
    ) {
      item.logParams = {
        name: vm.pipelineName,
        history_uuid: vm.history.uuid,
        uuid: task.uuid,
      };

      // TODO: exec / test task should continue polling for 1+ minute even ended
      // TODO: Deploy app / service update should continue updating logs until stage ends
      item.logFlagFunction = () => {
        if (vm.destroyed) {
          return 0;
        } else {
          return ['completed', 'failed'].includes(vm.stage.status) ? 2 : 1;
        }
      };
    }

    if (task.type === 'deploy-application') {
      _decorateTaskDeployAppItem(item);
    } else if (task.type === 'manual-control') {
      _decorateManualControlItem(item);
    } else if (task.type === 'build') {
      _decorateBuildItem(item);
    } else if (task.type === 'sync-registry') {
      _decorateSyncRegistryItem(item);
    }
  }

  // Enhance display for task deploy app
  // This should be invoked multiple times
  function _decorateTaskDeployAppItem(item) {
    // Extend the fields for deploy-application card
    const fields = RbPipelineWorkflowItemViewModel.generateTaskFields({
      task: item.model,
    });

    const services = _.values(_.get(item, 'model.services', {}) || []);

    const serviceNameFields = services.map(service => {
      service.logParams = _.merge(
        { service: service.service_name },
        item.logParams,
      );

      service.logFlagFunction = () => {
        if (vm.destroyed) {
          return 0;
        } else {
          return ['failed', 'ongoing'].includes(service.current_status) ? 2 : 1;
        }
      };

      return new RbPipelineWorkflowItemFieldViewModel({
        type: 'service_name',
        label: service.service_name,
        // Decorate which services are selected
        css: SERVICE_STATUS_TO_CSS_MAP[service.current_status],
      });
    });

    item.fields = fields.concat(serviceNameFields);
  }

  /**
   * Attach new callbacks to manual control items
   */
  function _decorateManualControlItem(item) {
    const { data, status } = item.model;
    // We only need to decorate once
    if ((status !== 'ongoing' && status !== 'completed') || item.decorated) {
      return;
    }
    item.artifactPrefix = vm.pipelinePrefix + '/' + item.model.name;
    item.uploadReady = true;
    item.uploadStateChanged = state => {
      item.uploadReady = ARTIFACT_UPLOAD_STATE.READY === state;
    };

    if (status !== 'completed' && data && data.exec_enabled) {
      Promise.resolve(data.link)
        .then(link => {
          if (!link) {
            throw { status: 404 };
          }

          const is_new = link.namespace;

          if (link.type === 'deployed-app-service') {
            const deployTask = vm.stage.tasks.find(
              task =>
                task.name === link.parent &&
                task.services &&
                task.services[link.name],
            );

            if (!deployTask) {
              throw { status: 404 };
            }

            return {
              service_name: deployTask.services[link.name].uuid,
              application: null,
              is_new,
            };
          } else {
            return {
              service_name: link.uuid || link.name,
              application: link.uuid ? null : link.parent,
              is_new,
            };
          }
        })
        .then(params => applicationService.applicationDetail(params))
        .then(data => {
          item.serviceData = data.detail;
          rbSafeApply();
        })
        .catch(e => {
          if (e.status !== 403) {
            rbToast.warning(translateService.get('service_not_exist'));
          }
        });
    }

    item.onProceedClicked = () => {
      if (!item.manualControlDisabled()) {
        item.proceeded = true;
        item.declined = false;
        item.manualControlTouched = true;
      }
    };

    item.onDeclineClicked = () => {
      if (!item.manualControlDisabled()) {
        item.proceeded = false;
        item.declined = true;
        item.manualControlTouched = true;
      }
    };

    item.onExecClicked = () => {
      rbModal
        .show({
          title: 'EXEC',
          component: 'rbServiceExecDialog',
          locals: {
            serviceData: item.serviceData,
          },
        })
        .catch(() => {});
    };

    item.manualControlDisabled = () => {
      return (
        item.submitting ||
        !item.uploadReady ||
        item.model.status !== 'ongoing' ||
        !vm.taskControlButtonDisplayExpr(item.model)
      );
    };

    item.manualControlExecDisabled = () => {
      return (
        !item.serviceData ||
        !rbRoleUtilities.resourceHasPermission(
          item.serviceData,
          RESOURCE_TYPES.SERVICE,
          'update',
        )
      );
    };

    if (!item.manualControlTouched) {
      item.proceeded = item.model.status === 'completed';
      item.declined = item.model.status === 'failed';
    }

    if (item.model.status === 'ongoing') {
      item.message = translateService.get(
        'pipeline_history_manual_control_required',
      );
    } else {
      item.message = undefined;
      item.submitting = false;
      item.feedbackMessage = item.model.data.message;
    }

    item.submit = async $event => {
      await rbConfirmBox.show({
        title: translateService.get('pipeline_task_type_manual-control'),
        textContent: translateService.get(
          'pipeline_manual_control_submit_info',
        ),
        targetEvent: $event,
      });

      item.submitting = true;

      rbSafeApply();
      try {
        await rbPipelineDataService.updatePipelineHistoryTask({
          pipelineName: vm.pipelineName,
          historyId: vm.historyId,
          taskId: item.model.uuid,
          task: {
            action: item.proceeded ? 'proceed' : 'decline',
            message: item.feedbackMessage,
          },
        });
      } catch (e) {
        // Failed to submit .. do something?
        item.submitting = false;
      }

      rbSafeApply();
    };

    item.decorated = true;
  }

  function _decorateBuildItem(item) {
    // We only need to decorate once
    if (item.buildDetail) {
      return;
    }
    startBuildPolling();

    ////////
    // Start polling. It has a sideeffect to attach the newest build to the item.
    async function startBuildPolling() {
      while (!buildFinished(item.buildDetail) && !vm.destroyed) {
        const buildId = _getBuildIdFromBuildTask();
        try {
          item.buildDetail = buildId
            ? await rbBuildDataService.getPrivateBuildDetail(
                _getBuildIdFromBuildTask(),
              )
            : null;
        } catch (err) {
          $log.error('Failed to poll build: ' + err.message);
        }
        await rbDelay(5000);
      }
    }

    function buildFinished(buildDetail) {
      return buildDetail ? ['S', 'F', 'D'].includes(buildDetail.status) : false;
    }
  }

  function _decorateSyncRegistryItem(item) {
    // We only need to decorate once
    if (item.syncDetails || !item.model.data.sync_ids) {
      return;
    }

    item.syncDetails = item.model.data.sync_ids.map(history_id => ({
      history_id,
    }));

    item.syncDetails.forEach((detail, index) => {
      startSyncPolling(detail, index);
    });

    ////////
    // Start polling. It has a sideeffect to attach the newest build to the item.
    async function startSyncPolling(detail, index) {
      while (!syncFinished(detail) && !vm.destroyed) {
        const historyId = detail.history_id;
        try {
          item.syncDetails[index] = await rbImageSyncDataService.getHistory(
            historyId,
          );
        } catch (err) {
          $log.error('Failed to poll sync: ' + err.message);
        }
        await rbDelay(5000);
      }
    }

    function syncFinished(detail) {
      return detail ? ['S', 'F', 'D'].includes(detail.status) : false;
    }
  }

  function getRegistryDisplayName$() {
    return rbRegistryDataStore.getRegistryDisplayName$(
      vm.history && vm.history.data.registry,
    );
  }

  function isPollingHistory() {
    return (
      !vm.destroyed && !['failed', 'completed'].includes(vm.history.status)
    );
  }

  function _calculateTimeCost(itemModel) {
    if (itemModel.started_at && itemModel.ended_at) {
      const startedAt = moment(itemModel.started_at);
      const endedAt = moment(itemModel.ended_at);

      return endedAt - startedAt;
    } else {
      return -1;
    }
  }

  async function focusDetailTabByCardId(id) {
    const $accordionTitle = $(`[card-id='${id}']`, $element);
    const $accordionContainer = $('.accordions-outer', $element);
    const $accordionContent = $('+ .content', $accordionTitle);

    if ($accordionTitle.length > 0 && $accordionContainer.length > 0) {
      $accordionTitle.removeClass('focused');
      $accordionContent.removeClass('focused');

      const newScrollTop =
        $accordionContainer.scrollTop() +
        ($accordionTitle.offset().top - $accordionContainer.offset().top);
      await rbDelay();
      $accordionContainer.animate({ scrollTop: newScrollTop }, 500);
      $accordionTitle.addClass('focused');
      $accordionContent.addClass('focused');
    }
  }

  async function deleteHistory() {
    try {
      await rbConfirmBox.show({
        title: translateService.get('delete'),
        textContent: translateService.get('pipeline_delete_history_content', {
          pipeline_name: vm.piplineDisplayName,
        }),
      });

      vm.deleting = true;
      await rbPipelineDataService.deletePipelineHistory({
        pipelineName: vm.pipelineName,
        historyId: vm.historyId,
      });

      $state.go('pipeline.history.list');
    } catch (e) {
      vm.deleting = false;
    }
  }

  async function stopHistory() {
    try {
      await rbConfirmBox.show({
        title: translateService.get('stop'),
        textContent: translateService.get('pipeline_stop_history_content', {
          pipeline_name: vm.piplineDisplayName,
        }),
      });
      vm.stopping = true;

      await rbPipelineDataService.stopPipelineHistory({
        pipelineName: vm.pipelineName,
        historyId: vm.historyId,
      });
    } catch (e) {
      // ??
      vm.stopping = false;
    }
  }

  function buttonDisplayExpr(action) {
    return rbRoleUtilities.resourceHasPermission(
      vm.history,
      RESOURCE_TYPES.PIPELINE_HISTORY,
      action,
    );
  }

  function taskControlButtonDisplayExpr(task) {
    return rbRoleUtilities.resourceHasPermission(
      task,
      RESOURCE_TYPES.PIPELINE_TASK,
      'control',
    );
  }

  function getCreatorName(item) {
    return item.auto_trigger
      ? translateService.get('auto_trigger')
      : item.created_by;
  }
}
