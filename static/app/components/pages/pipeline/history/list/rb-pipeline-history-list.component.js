import moment from 'moment';

import {
  PIPELINE_STATUS_CSS_MAP,
  PIPELINE_STATUS_ICON_STATUS_MAP,
} from '../../rb-pipeline-constant';
import { resource_types as RESOURCE_TYPES } from '../../../rbac_role/rb-rbac-helper.constant';

import template from './rb-pipeline-history-list.html';

angular.module('app.components.pages').component('rbPipelineHistoryList', {
  controller: rbPipelineHistoryListController,
  controllerAs: 'vm',
  bindings: {
    pipelineConfigName: '<?',
  },
  template,
});

function rbPipelineHistoryListController(
  rbDelay,
  rbSafeApply,
  translateService,
  rbTriggerPipelineDialog,
  rbPipelineDataService,
  rbRegistryDataStore,
  rbRoleUtilities,
) {
  const vm = this;

  vm.pagination = {
    pageSize: 20,
    pageno: 1,
    totalItems: 0,
  };

  vm.statusCssMap = PIPELINE_STATUS_CSS_MAP;
  vm.statusIconMap = PIPELINE_STATUS_ICON_STATUS_MAP;
  vm.historyStatusTooltipFunction = historyStatusTooltipFunction;
  vm.showStartPipelineDialog = showStartPipelineDialog;
  vm.getRegistryDisplayName$ = getRegistryDisplayName$;
  vm.getCreatorName = getCreatorName;
  vm.getConfigName = getConfigName;
  vm.getPipelineConfigName = getPipelineConfigName;
  vm.$onInit = _init;
  vm.$onDestroy = _destroy;
  vm.pageNoChange = pageNoChange;
  vm.onSearchChanged = onSearchChanged;
  //////////

  /**
   * Initialize the component.
   */
  async function _init() {
    vm.pipelineCreateEnabled = await rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.PIPELINE_CONFIG,
    );
    _startPollTimer();
  }

  // Simply poll data every 30 seconds
  async function _startPollTimer() {
    while (!vm.destroyed) {
      vm.pageNoChange();
      await rbDelay(30 * 1000);
    }
  }

  function _destroy() {
    vm.destroyed = true;
  }

  let _lastSearchCounter = 0;

  async function onSearchChanged(search) {
    const searchCounter = ++_lastSearchCounter;
    vm.search = search;
    vm.searching = true;
    await pageNoChange(1);
    if (searchCounter === _lastSearchCounter) {
      vm.searching = false;
    }
    rbSafeApply();
  }

  async function pageNoChange(
    page = vm.pagination.pageno,
    size = vm.pagination.pageSize,
  ) {
    vm.pageNoChanged = vm.pagination.pageno !== page;
    vm.pagination.pageno = page;
    vm.pagination.pageSize = size;
    vm.loading = true;
    try {
      const { count, results } = await _refetchData({
        pageno: vm.pagination.pageno,
        page_size: vm.pagination.pageSize,
        search: vm.search,
      });
      vm.pagination.totalItems = count;
      vm.list = results;
      // Attach duration to list items:
      vm.list &&
        vm.list.forEach(item => {
          item.duration = _calculateTimeCost(item);
        });
      vm.loadError = false;
    } catch (e) {
      if (vm.pageNoChanged) {
        vm.list = [];
      }
    }
    vm.loading = false;
    rbSafeApply();
  }

  function showStartPipelineDialog($event) {
    return rbTriggerPipelineDialog
      .show({
        $event,
      })
      .then(() => vm.pageNoChange())
      .catch(() => {});
  }

  async function _refetchData({ pageno, page_size, search }) {
    return await rbPipelineDataService.getPipelineHistories({
      pageno,
      page_size,
      pipeline: vm.pipelineConfigName,
      search,
    });
  }

  // TODO move to utils
  function _calculateTimeCost(history) {
    if (history.started_at && history.ended_at) {
      const startedAt = moment(history.started_at);
      const endedAt = moment(history.ended_at);

      return endedAt - startedAt;
    } else {
      return -1;
    }
  }

  function historyStatusTooltipFunction(item) {
    return translateService.get('pipeline_tasks_status_tooltip', item);
  }

  function getRegistryDisplayName$(registryName) {
    return rbRegistryDataStore.getRegistryDisplayName$(registryName);
  }

  function getCreatorName(item) {
    return item.auto_trigger
      ? translateService.get('auto_trigger')
      : item.created_by;
  }

  function getConfigName(config) {
    return rbPipelineDataService.getConfigName(config);
  }
  function getPipelineConfigName(config) {
    return config.pipeline_uuid;
  }
}
