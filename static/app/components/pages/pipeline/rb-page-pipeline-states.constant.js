const pipelineRedirect = trans => {
  const rbPipelineDataService = trans.injector().get('rbPipelineDataService');
  return rbPipelineDataService
    .getPipelineValid()
    .then(({ is_enabled }) => {
      if (!is_enabled) {
        return 'blank_page';
      }
      return {}; // Trans to default state
    })
    .catch(() => 'blank_page');
};

module.exports = [
  {
    baseStateName: 'pipeline.config',
    states: [
      {
        url: '/list',
        name: 'list',
        component: 'rbPipelineConfigList',
        redirectTo: pipelineRedirect,
      },
      {
        url: '/create',
        name: 'create',
        component: 'rbPipelineConfigCreate',
        params: {
          configCopy: undefined,
        },
        resolve: {
          configCopy: $stateParams => {
            'ngInject';
            return $stateParams['configCopy'];
          },
        },
      },
      {
        url: '/detail?name',
        name: 'detail',
        component: 'rbPipelineConfigDetail',
        resolve: {
          name: $stateParams => {
            'ngInject';
            return $stateParams['name'];
          },
        },
      },
    ],
  },
  {
    baseStateName: 'pipeline.history',
    states: [
      {
        url: '/list',
        name: 'list',
        component: 'rbPipelineHistoryList',
        redirectTo: pipelineRedirect,
      },
      {
        url: '/detail?pipelineName&historyId',
        name: 'detail',
        component: 'rbPipelineHistoryDetail',
        resolve: {
          pipelineName: $stateParams => {
            'ngInject';
            return $stateParams['pipelineName'];
          },
          historyId: $stateParams => {
            'ngInject';
            return $stateParams['historyId'];
          },
        },
      },
    ],
  },
  {
    baseStateName: 'pipeline.approve',
    states: [
      {
        url: '/list',
        name: 'list',
        component: 'rbPipelineApproveList',
        redirectTo: pipelineRedirect,
      },
    ],
  },
];
