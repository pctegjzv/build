export const INITIAL_CREATE_CONFIG = {
  triggers: {
    repository: {},
  },
  stages: [
    {
      order: 1,
      name: 'first-stage',
    },
  ],
  on_end: [
    {
      order: 1,
      name: 'task-final',
      type: 'notification',
    },
  ],
};

export const PIPELINE_STATUS_CSS_MAP = {
  completed: 'success',
  failed: 'error',
  pending: 'pending',
  ongoing: 'in-progress',
  deleted: 'deleted',
};

export const PIPELINE_STATUS_ICON_STATUS_MAP = {
  completed: 'completed',
  failed: 'error',
  pending: 'waiting',
  ongoing: 'in-progress',
  deleted: 'deleted',
};

export const PIPELINE_STATUS_TO_ITEM_STATE = {
  completed: 'success',
  pending: 'pending',
  failed: 'error',
  deploying: 'active',
  ongoing: 'active',
};

export const SERVICE_STATUS_TO_CSS_MAP = {
  deploying: 'active',
  pending: 'pending',
  failed: 'error',
  ongoing: 'success',
};

export const TASK_TYPES = [
  'deploy-application',
  'update-service',
  'manual-control',
  'notification',
  'test-container',
  'exec',
  'sync-registry',
  'artifact-upload',
  'artifact-download',
];
