const _ = require('lodash');

angular
  .module('app.components.pages')
  .factory('rbPipelineDataService', rbPipelineDataServiceFactory);

function rbPipelineDataServiceFactory(
  rbHttp,
  rbToast,
  translateService,
  rbAccountService,
) {
  const namespace = rbAccountService.getCurrentNamespace();
  const PIPELINES_URL = `/ajax/pipelines/${namespace}/`;
  const CONFIG_URL = `${PIPELINES_URL}config/`;
  const HISTORY_URL = `${PIPELINES_URL}history/`;
  const CLUSTERS_URL = '/ajax/v2/kubernetes/clusters/';

  return {
    getPipelineValid,
    getPipeline,
    getPipelines,
    deletePipeline,
    createPipeline,
    updatePipeline,
    triggerPipeline,

    getPipelineHistories,
    getPipelineHistory,
    getPipelineHistoryTask,
    getPipelineHistoryTasks,

    deletePipelineHistory,
    stopPipelineHistory,

    updatePipelineHistoryTask,
    getConfigName,

    getConfigMaps,
    getConfigMapOptions,
    getConfigMap,

    getApproveList,
    approve,
  };

  function getPipelineValid() {
    return rbHttp.sendRequest({
      method: 'GET',
      cache: true,
      url: PIPELINES_URL + 'is_enabled',
    });
  }

  //////////
  function getPipeline({ name }) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: CONFIG_URL + name,
    });
  }

  function deletePipeline({ name }) {
    return rbHttp
      .sendRequest({
        method: 'DELETE',
        url: CONFIG_URL + name,
      })
      .then(res => {
        rbToast.success(translateService.get('pipeline_config_delete_success'));
        return res;
      });
  }

  function getPipelines({
    pageno = 1,
    page_size = 20,
    search = '',
    action = 'view',
  } = {}) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: CONFIG_URL,
      params: { page: pageno, page_size, search, action },
    });
  }

  function createPipeline({ config }) {
    return rbHttp
      .sendRequest({
        method: 'POST',
        url: CONFIG_URL,
        data: config,
      })
      .then(res => {
        rbToast.success(translateService.get('pipeline_config_create_success'));
        return res;
      });
  }

  function updatePipeline({ config, name }) {
    return rbHttp
      .sendRequest({
        method: 'PUT',
        url: CONFIG_URL + name,
        data: config,
      })
      .then(res => {
        rbToast.success(translateService.get('pipeline_config_update_success'));
        return res;
      });
  }

  function triggerPipeline({ pipeline, trigger, head_commit_id, imageTag }) {
    const triggerData = _.cloneDeep(trigger);
    const pipelineName = getConfigName(pipeline);
    triggerData.image_tag = imageTag;
    triggerData.head_commit_id = head_commit_id;

    const payload = {
      pipeline: pipelineName,
      trigger: triggerData.type,
      data: triggerData,
    };

    return rbHttp
      .sendRequest({
        method: 'POST',
        url: HISTORY_URL + pipelineName,
        data: payload,
      })
      .then(res => {
        rbToast.success(
          translateService.get('pipeline_config_trigger_success'),
        );
        return res;
      });
  }

  function getPipelineHistories({
    pageno = 1,
    page_size = 20,
    search = '',
    pipeline, // pipeline name
  } = {}) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: pipeline ? HISTORY_URL + pipeline : HISTORY_URL,
      params: { page: pageno, page_size, search },
    });
  }

  function getPipelineHistory({ pipelineName, historyId }) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: `${HISTORY_URL}${pipelineName}/${historyId}`,
    });
  }

  function getPipelineHistoryTasks({ pipelineName, historyId }) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: `${HISTORY_URL}${pipelineName}/${historyId}/tasks`,
    });
  }

  function getPipelineHistoryTask({ pipelineName, historyId, taskId }) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: `${HISTORY_URL}${pipelineName}/${historyId}/tasks/${taskId}`,
    });
  }

  function deletePipelineHistory({ pipelineName, historyId }) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url: `${HISTORY_URL}${pipelineName}/${historyId}`,
    });
  }

  function stopPipelineHistory({ pipelineName, historyId }) {
    return rbHttp.sendRequest({
      method: 'PUT',
      url: `${HISTORY_URL}${pipelineName}/${historyId}/stop`,
    });
  }

  function updatePipelineHistoryTask({
    pipelineName,
    historyId,
    taskId,
    task,
  }) {
    return rbHttp.sendRequest({
      method: 'PUT',
      url: `${HISTORY_URL}${pipelineName}/${historyId}/tasks/${taskId}`,
      data: task,
    });
  }

  function getConfigName(config) {
    return config.uuid;
  }

  function getConfigMaps({ clusterId, namespace }) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url:
          CLUSTERS_URL +
          clusterId +
          '/configmaps/' +
          (namespace ? namespace + '/' : ''),
        addNamespace: false,
      })
      .then(({ result }) => result);
  }

  function getConfigMapOptions({ clusterId, namespace }) {
    return getConfigMaps({ clusterId, namespace }).then(configMaps =>
      configMaps.map(
        ({
          kubernetes: {
            metadata: { name, namespace, uid },
          },
        }) => ({
          name,
          namespace,
          uuid: uid,
        }),
      ),
    );
  }

  function getConfigMap({ clusterId, namespace, name }) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: CLUSTERS_URL + clusterId + '/configmaps/' + namespace + '/' + name,
      addNamespace: false,
    });
  }

  function getApproveList({
    type = 'proposer',
    size = 20,
    pageno = 1,
    project_name = '',
  }) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: PIPELINES_URL + 'approve',
      params: {
        type,
        size,
        pageno,
        project_name,
      },
      addNamespace: false,
    });
  }

  function approve({ action, name, history_id, task_id }) {
    return rbHttp.sendRequest({
      method: 'PUT',
      url: PIPELINES_URL + `history/${name}/${history_id}/tasks/${task_id}`,
      data: {
        action,
        namespace,
      },
    });
  }
}
