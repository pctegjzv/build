import { RbPipelineConfig } from '../class/rb-pipeline-config-definition';

angular
  .module('app.components.pages')
  .factory('rbTriggerPipelineDialog', rbTriggerPipelineDialog);

function rbTriggerPipelineDialog(rbModal, translateService) {
  return {
    /**
     * Shows a trigger pipeline dialog.
     *
     * @param pipeline
     * @param pipelines
     * @returns {historyId, pipelineName} when success
     */
    show: ({ pipeline, pipelines }) => {
      if (pipeline instanceof RbPipelineConfig) {
        // Use a hack to get raw pipeline object:
        pipeline = JSON.parse(JSON.stringify(pipeline));
      }

      return rbModal.show({
        title: translateService.get('pipeline_start'),
        locals: {
          pipeline,
          pipelines,
        },
        template: `<rb-trigger-pipeline
                     pipeline="pipeline" pipelines="pipelines">
                   </rb-trigger-pipeline>`,
      });
    },
  };
}
