import * as _ from 'lodash';
import { map } from 'rxjs/operators';

import { RbPipelineConfigTriggers } from '../class/rb-pipeline-config-trigger-definition';
import templateStr from './rb-trigger-pipeline.html';

angular.module('app.components.pages').component('rbTriggerPipeline', {
  bindings: {
    pipeline: '<',
  },
  controller: rbTriggerPipelineController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbTriggerPipelineController(
  rbAccountService,
  rbModal,
  $log,
  rbSafeApply,
  rbRepositoryTagDataStore,
  rbPipelineDataService,
) {
  const vm = this;

  vm.hide = buildId => {
    rbModal.hide(buildId);
  };
  vm.cancel = () => {
    rbModal.cancel();
  };

  const namespace = rbAccountService.getCurrentNamespace();

  vm.backendUrl = `/ajax/pipelines/${namespace}/config?page=1&page_size=20&action=trigger`;
  vm.hasPipeline = !!vm.pipeline;

  vm.onPipelineChanged = onPipelineChanged;
  vm.triggerPipeline = triggerPipeline;
  vm.getConfigName = getConfigName;
  vm.responseItemFilter = responseItemFilter;
  vm.$onInit = onInit;
  ///////
  function onInit() {
    vm.triggering = false;
    if (vm.pipeline) {
      onPipelineChanged(_.clone(vm.pipeline));
    }
  }

  async function triggerPipeline() {
    vm.triggering = true;
    try {
      const { uuid } = await rbPipelineDataService.triggerPipeline({
        pipeline: vm.pipeline,
        imageTag: vm.imageTag,
        head_commit_id: vm.head_commit_id,
        trigger: vm.activeTrigger,
      });
      vm.hide({
        historyId: uuid,
        pipelineName: vm.getConfigName(vm.pipeline),
      });
    } catch (e) {
      $log.error('Failed to trigger pipeline ' + vm.pipeline.name);
    }
    vm.triggering = false;
    rbSafeApply();
  }

  async function onPipelineChanged(pipeline) {
    if (pipeline === vm.pipeline) {
      return;
    }
    vm.pipeline = pipeline;
    vm.imageTag = undefined;
    rbSafeApply();

    const triggersConfig = new RbPipelineConfigTriggers(pipeline.triggers);
    const activeTrigger = triggersConfig.activeTrigger;

    vm.activeTrigger = activeTrigger;

    if (triggersConfig.triggerType === 'build') {
      // ... ?
    } else if (triggersConfig.triggerType === 'repository') {
      vm.imageTags$ = rbRepositoryTagDataStore
        .getInstance({
          registry_name: activeTrigger.registry,
          project_name: activeTrigger.project,
          repository_name: activeTrigger.repo,
        })
        .data$.pipe(map(tags => tags && tags.map(tag => ({ name: tag }))));
    }

    rbSafeApply();
  }
  function getConfigName(config) {
    return rbPipelineDataService.getConfigName(config);
  }

  function responseItemFilter(item) {
    item.display_name = item.space_name
      ? `${item.name}(${item.space_name})`
      : item.name;
    return item;
  }
}
