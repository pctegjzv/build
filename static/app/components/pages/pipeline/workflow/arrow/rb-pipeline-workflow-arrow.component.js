const templateStr = require('app/components/pages/pipeline/workflow/arrow/rb-pipeline-workflow-arrow.html');
angular.module('app.components.pages').component('rbPipelineWorkflowArrow', {
  bindings: {
    state: '<',
    type: '<',
  },
  controllerAs: 'vm',
  template: templateStr,
});
