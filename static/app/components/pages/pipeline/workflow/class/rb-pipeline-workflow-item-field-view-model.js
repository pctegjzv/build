angular
  .module('app.components.pages')
  .factory(
    'RbPipelineWorkflowItemFieldViewModel',
    RbPipelineWorkflowItemFieldViewModelFactory,
  );

/**
 * Since we may need to use angular injector,
 * this class is not using standard es6 modules, but wrapped in angular factories.
 */
function RbPipelineWorkflowItemFieldViewModelFactory(
  translateService,
  rbDurationFilter,
) {
  const LABEL_FIELD_TYPES = {
    pipeline_task_type: {
      logo: 'fa fa-bookmark',
      order: 0, // Should always be the first
      decorator: label =>
        label ? translateService.get('pipeline_task_type_' + label) : '',
    },
    timeout: {
      logo: 'fa fa-clock-o',
      order: 999, // Should always be the last
      decorator: label => (label ? rbDurationFilter(label * 60000) : ''),
    },
    image_registry_name: {
      logo: 'fa fa-building-o',
    },
    image_repository_name: {
      logo: 'fa fa-building',
    },
    build_config_name: {
      logo: 'fa fa-gavel',
    },
    manual_code_commit: {
      logo: 'fa fa-code',
    },
    service_name: {
      logo: 'fa fa-cube',
    },
    pipeline_test_link_to_service: {
      logo: 'fa fa-cube',

      decorator: label => (label && label.name) || '',
    },
    pipeline_task_selected_service: {
      logo: 'fa fa-cube',

      decorator: label => (label && label.name) || '',
    },
    envfile_name: {
      logo: 'fa fa-file-o',
    },
    image_tag: {
      logo: 'fa fa-tag',
    },
    region: {
      logo: 'fa fa-server',
    },
    notification: {
      logo: 'fa fa-bullhorn',
    },
    template: {
      logo: 'fa fa-columns',
    },
    username: {
      logo: 'fa fa-user',
    },
    image_sync_center_name: {
      logo: 'rb-icon-sync-registry',
    },
    artifact_share_path: {
      logo: 'fa fa-clone',
    },
    exec_enabled: {
      logo: 'field-logo-exec',
      decorator: label =>
        label
          ? translateService.get('switch_on')
          : translateService.get('switch_off'),
    },
    assign_approver: {},
    target_email: {},
  };

  /**
   * View Model class for a single field in a pipeline Workflow Item
   */
  class RbPipelineWorkflowItemFieldViewModel {
    /**
     * @param label Text label
     * @param type Type of the field. See LABEL_FIELD_TYPES
     * @param css Css added to this field
     */
    constructor({ label, type, css }) {
      this.type = type;
      this.label = label;
      this.css = css;
    }

    get logo() {
      return LABEL_FIELD_TYPES[this.type].logo || 'fa';
    }

    set label(label) {
      // convert all labels to functions
      const labelFn = label instanceof Function ? label : () => label;

      // decorate if needed
      this._label = LABEL_FIELD_TYPES[this.type].decorator
        ? () => LABEL_FIELD_TYPES[this.type].decorator(labelFn())
        : labelFn;
    }

    // In case label is a callback function, use getter function to do the trick
    get label() {
      return this._label();
    }

    set css(css) {
      this._css = css;
    }

    // In case css is a callback function, use getter function to do the trick
    get css() {
      return this._css instanceof Function ? this._css() : this._css;
    }

    get order() {
      return LABEL_FIELD_TYPES[this.type].order !== undefined
        ? LABEL_FIELD_TYPES[this.type].order
        : 1;
    }
  }

  return RbPipelineWorkflowItemFieldViewModel;
}
