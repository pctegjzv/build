import { BehaviorSubject } from 'rxjs';
import { filter, first } from 'rxjs/operators';

angular
  .module('app.components.pages')
  .factory(
    'RbPipelineWorkflowItemViewModel',
    RbPipelineWorkflowItemViewModelFactory,
  );

/**
 * Since we may need to use angular injector,
 * this class is not using standard es6 modules, but wrapped in angular factories.
 */
function RbPipelineWorkflowItemViewModelFactory(
  RbPipelineWorkflowItemFieldViewModel,
  rbRegionDataStore,
  translateService,
) {
  const COMMON_TASK_FIELDS = {
    region: 'region',
    type: 'pipeline_task_type',
    timeout: 'timeout',
  };

  const TRIGGER_TYPE_TO_FIELDS = {
    repository: {
      registry_display_name: 'image_registry_name',
      repository: 'image_repository_name',
    },
    build: {
      build_config_name: 'build_config_name',
    },
  };

  const TASK_TYPE_TO_FIELDS = {
    'test-container': {
      link: 'pipeline_test_link_to_service',
      env_file: 'envfile_name',
      docker_image_tag: 'image_tag',
    },
    notification: {
      notification: 'notification',
    },
    'manual-control': {
      username: 'username',
    },
    'deploy-application': {
      application_template: 'template',
      service_name: 'service_name',
    },
    exec: {
      link: 'pipeline_task_selected_service',
    },
    'update-service': {
      service: 'pipeline_task_selected_service',
    },
    'update-service-new': {
      service: 'pipeline_task_selected_service',
    },
    'sync-registry': {
      config_name: 'image_sync_center_name',
    },
    'artifact-upload': {
      share_path: 'artifact_share_path',
    },
    'artifact-download': {
      share_path: 'artifact_share_path',
    },

    // Special task build for pipeline history
    build: {
      build_config_name: 'build_config_name',
      head_commit_id: 'manual_code_commit',
    },
    // TODO add new types
    'ci-report': {
      share_path: 'artifact_share_path',
    },
    'trigger-pipeline': {
      share_path: 'artifact_share_path',
    },
    approve: {
      assign_approver: 'assign_approver',
      target_email: 'target_email',
    },
  };

  Object.assign(TASK_TYPE_TO_FIELDS['manual-control'], {
    exec_enabled: 'exec_enabled',
    link: 'pipeline_task_selected_service',
  });

  /**
   * The 'color' related states:
   * active: Item is active / running (touched)
   * inactive: Item is not yet activated  (not yet touched)
   * success: Item is finished successfully
   * error: Item is finished with error.
   * pending: Item is waiting for something.
   */
  // eslint-disable-next-line
  const ITEM_STATES = ['active', 'inactive', 'success', 'pending', 'error'];

  /**
   * Used for workflow item ID generation (front end use only)
   * @type {number}
   * @private
   */
  let _itemId = 0;

  /**
   * Base View model class for a Pipeline Workflow Item
   * Note, workflow items only represents visual info for Pipeline.
   * Some of the values are calculated dynamically using getters.
   */
  class RbPipelineWorkflowItemViewModel {
    /**
     *
     * Dynamic fields: title, order, state, loading, message
     *
     * @param id Id for the item. If not given, a unique id will be generated (starting from 0)
     * @param model The actual data model this view model is linked to
     * @param title The title to display in the header. Assume to be translated.
     * @param type type of the item.
     * @param fields Array of RbPipelineWorkflowItemFieldModel.
     * @param order The order number to display. Starting from 1. Leave undefined if you don't want to show it.
     * @param state ['active', 'inactive', 'complete', 'error'].
     * @param running Decorate with running animation
     * @param selected whether this item is selected or not
     * @param loading item is loading data
     * @param message Message to display instead of fields
     * @param touched Whether the user has touched this item or not
     * @param disabled Whether this one should be disabled or not
     * @param onClick Call back to be invoked when clicked
     * @param onRemove Will be called when remove icon is clicked
     * @param onStateChange Will be called when state is changed
     */
    constructor({
      id,
      model,
      title,
      type,
      fields,
      order,
      state,
      running,
      message,
      touched,
      disabled = false,
      loading = false,
      selected = false,
      onClick = () => {},
      onRemove = () => {},
      onStateChange = () => {},
    }) {
      this.model = model;
      this.title = title;
      this.fields = fields;
      this.type = type;
      this.order = order;
      this.state = state;
      this.running = running;
      this.message = message;
      this.selected = selected;
      this.touched = touched;
      this.loading = loading;
      this.onClick = onClick;
      this.disabled = disabled;
      this.onRemove = onRemove;
      this.onStateChange = onStateChange;

      // Should emit region change values
      this.regionSubject$ = new BehaviorSubject(null);

      // Broadcast the initial region selection
      if (this.model && this.model.region_uuid) {
        rbRegionDataStore.data$
          .pipe(
            filter(data => !!data),
            first(),
          )
          .subscribe(regions => {
            const region = regions.find(
              region => region.id === this.model.region_uuid,
            );
            this.regionSubject$.next(region);
          });
      }

      // Should emit services deployed by this task
      // Only valid if type is 'deploy-application'
      this.deployedAppServicesSubject$ = new BehaviorSubject([]);

      // Generate an ID for ease of selection
      this.id = id || _itemId++;
    }

    get active() {
      return this.state === 'active';
    }

    get inactive() {
      return this.state === 'inactive';
    }

    get error() {
      return this.state === 'error';
    }

    get success() {
      return this.state === 'success';
    }

    get pending() {
      return this.state === 'pending';
    }

    // Getters / Setters
    // TODO Is it possible to automate the following getter/setters using meta programming?
    set state(state) {
      this._state = state;
    }

    get state() {
      const current =
        this._state instanceof Function ? this._state() : this._state;
      const previous = this._previousState;
      if (current !== this._previousState) {
        this._previousState = current;
        setTimeout(() => {
          this.onStateChange({ current, previous });
        }, 0);
      }

      return current;
    }

    set running(running) {
      this._running = running;
    }

    get running() {
      return this._running instanceof Function
        ? this._running()
        : this._running;
    }

    set order(order) {
      this._order = order;
    }

    get order() {
      return this._order instanceof Function ? this._order() : this._order;
    }

    set title(title) {
      this._title = title;
    }

    get title() {
      return this._title instanceof Function ? this._title() : this._title;
    }

    set fields(fields) {
      this._fields = fields;
    }

    // Hmm .. since fields will be generated via new operators, use this with care.
    get fields() {
      return this._fields instanceof Function ? this._fields() : this._fields;
    }

    set loading(loading) {
      this._loading = loading;
    }

    get loading() {
      return this._loading instanceof Function
        ? this._loading()
        : this._loading;
    }

    set message(message) {
      this._message = message;
    }

    get message() {
      return this._message instanceof Function
        ? this._message()
        : this._message;
    }

    set disabled(disabled) {
      this._disabled = disabled;
    }

    get disabled() {
      return this._disabled instanceof Function
        ? this._disabled()
        : this._disabled;
    }

    set onClick(onClick) {
      // Decorate the click with some additional checks
      this._onClick = () => {
        if (!this.disabled) {
          const res = onClick();

          // Set touched after the passed in onClick, in case there are some checks over this value.
          this.touched = true;
          return res;
        }
      };
    }

    get onClick() {
      return this._onClick;
    }

    isTask() {
      return !['triggers', 'on_end'].includes(this.type);
    }

    set region(region) {
      if (!region) {
        return;
      }
      this.regionSubject$.next(region);
      this.model.region_uuid = region.id;
      this.model.region = region.name;
    }

    get region$() {
      return this.regionSubject$.asObservable();
    }

    set deployedAppServices(deployedAppServices) {
      this.deployedAppServicesSubject$.next(deployedAppServices);
    }

    get deployedAppServices$() {
      return this.deployedAppServicesSubject$.asObservable();
    }

    /**
     * Factory method. From triggers in a pipeline config
     * @param {RbPipelineConfigTriggers} triggers
     * @returns {RbPipelineWorkflowItemViewModel}
     */
    static fromTriggers(triggersConfig) {
      const { triggers } = triggersConfig;
      const fields = _.flatMap(triggers, trigger =>
        Object.entries(TRIGGER_TYPE_TO_FIELDS[trigger.type]).map(
          ([triggerField, labelType]) =>
            new RbPipelineWorkflowItemFieldViewModel({
              type: labelType,

              // Use getter function to make sure the latest field will be displayed
              label: () => trigger.active && trigger[triggerField],
            }),
        ),
      );

      return new RbPipelineWorkflowItemViewModel({
        model: triggersConfig,
        title: translateService.get('pipeline_item_title_triggers'),
        fields,
        type: 'triggers',
        id: 'triggers',
      });
    }

    /**
     * Factory method. From a task in a pipeline config
     * @param {RbPipelineConfigTask} task
     * @returns {RbPipelineWorkflowItemViewModel}
     */
    static fromTask({ task }) {
      const viewModel = new RbPipelineWorkflowItemViewModel({
        model: task,
        title: () => task.name,
        order: () => task.order,
        type: task.type,
      });

      if (task.type !== 'task_type_selection') {
        viewModel.fields = RbPipelineWorkflowItemViewModel.generateTaskFields({
          task,
        });
      } else {
        viewModel.message = translateService.get(
          'pipeline_config_empty_task_hint',
        );
      }

      return viewModel;
    }

    static generateTaskFields({ task }) {
      if (task.type !== 'task_type_selection') {
        return Object.entries(TASK_TYPE_TO_FIELDS[task.type])
          .concat(Object.entries(COMMON_TASK_FIELDS))
          .map(
            ([taskFieldKey, labelType]) =>
              new RbPipelineWorkflowItemFieldViewModel({
                type: labelType,

                // Search the data fields first
                label: () =>
                  task.data[taskFieldKey] !== undefined
                    ? task.data[taskFieldKey]
                    : task[taskFieldKey],
              }),
          );
      }
    }

    /**
     * Return a history trigger item from a base history json
     * See example at https://secretshop.aerobatic.io/#pipelines__namespace__history__name___uuid__get
     */
    static fromHistoryTrigger({ triggerData, type }) {
      const fields = Object.entries(TRIGGER_TYPE_TO_FIELDS[type]).map(
        ([triggerKey, labelType]) =>
          new RbPipelineWorkflowItemFieldViewModel({
            type: labelType,
            label: () => triggerData[triggerKey],
          }),
      );

      return new RbPipelineWorkflowItemViewModel({
        title: 'pipeline_item_title_triggers',
        type: 'triggers',
        fields,
        id: 'triggers',
        state: 'success', // Should always be success
      });
    }
  }

  return RbPipelineWorkflowItemViewModel;
}
