import { BehaviorSubject, of } from 'rxjs';
import { filter, first, map, switchMap } from 'rxjs/operators';

angular
  .module('app.components.pages')
  .factory('RbPipelineWorkflowViewModel', RbPipelineWorkflowViewModelFactory);

/**
 * Since we may need to use angular injector,
 * this class is not using standard es6 modules, but wrapped in angular factories.
 */
function RbPipelineWorkflowViewModelFactory(
  RbPipelineWorkflowItemViewModel,
  rbRepositoryDataStore,
  rbBuildConfigDataStore,
  rbRegionDataStore,
  translateService,
) {
  function getRepositoryByTriggersConfig(triggersConfig) {
    if (!triggersConfig) {
      return Promise.resolve(null);
    }
    const trigger = triggersConfig.activeTrigger;
    let repoObservable = null;

    switch (trigger.type) {
      case 'repository':
        repoObservable = of({
          registry_name: trigger.registry,
          project_name: trigger.project,
          repository_name: trigger.repo,
        });
        break;
      case 'build':
        repoObservable = rbBuildConfigDataStore
          .getByName$(trigger.build_config_name)
          .pipe(
            filter(data => !!data),
            map(buildConfig => ({
              registry_name: buildConfig.image_repo.registry.name,
              project_name:
                buildConfig.image_repo.project &&
                buildConfig.image_repo.project.project_name,
              repository_name: buildConfig.image_repo.name,
            })),
          );
        break;
    }

    return repoObservable
      .pipe(
        switchMap(
          repo =>
            repo.repository_name
              ? rbRepositoryDataStore.getRepositoryByName$(repo)
              : of({ disabled: true }),
        ),
        filter(data => !!data),
        first(),
      )
      .toPromise();
  }

  /**
   * ViewModel for Pipeline workflow.
   * This is to enable flexible control over the component.
   *
   * Note: the viewmodel only provides a single dataflow direction from config to
   * the data object (RbPipelineConfig).
   */
  class RbPipelineWorkflowViewModel {
    constructor({ triggers, tasks, onEnd, readonly, model } = {}) {
      this.triggerRepositorySubject$ = new BehaviorSubject(null);

      this.triggers = triggers;
      this.tasks = tasks;
      this.onEnd = onEnd;
      this.readonly = readonly;
      this.model = model; // raw model
      this.artifactEnabled$ = new BehaviorSubject(
        this.model ? this.model.artifact_enabled : false,
      );

      this.tasksSubject$ = new BehaviorSubject(this.tasks);

      // Fill in initial trigger repository
      this.triggerChanged();
    }

    triggerChanged() {
      getRepositoryByTriggersConfig(this.triggers.model).then(repository => {
        this.triggerRepositorySubject$.next(repository);
      });
    }

    set readonly(readonly) {
      this._readonly = readonly;
    }

    get readonly() {
      return this._readonly instanceof Function
        ? this._readonly()
        : this._readonly;
    }

    get triggerRepository$() {
      return this.triggerRepositorySubject$.asObservable();
    }

    /**
     * Generate workflow model from a RbPipelineConfig
     * @param {RbPipelineConfig} config
     * @returns {RbPipelineWorkflowViewModel}
     */
    static fromPipelineConfig({ config }) {
      const triggers = RbPipelineWorkflowItemViewModel.fromTriggers(
        config.triggers,
      );

      const tasks = config.stages[0].tasks.map(task =>
        RbPipelineWorkflowItemViewModel.fromTask({
          task,
        }),
      );

      // For backwards compatibility where region is subject to each stage instead of task
      if (config.stages[0].region || config.stages[0].region_uuid) {
        rbRegionDataStore.data$
          .pipe(
            filter(data => !!data),
            first(),
          )
          .subscribe(regions => {
            const region = regions.find(
              region =>
                region.name === config.stages[0].region ||
                region.id === config.stages[0].region_uuid,
            );
            region && tasks.forEach(task => (task.region = region));
          });
      }

      const onEnd = RbPipelineWorkflowItemViewModel.fromTask({
        task: config.on_end[0],
      });

      // For P2, the on end item is simply a notification task
      // This is to make sure the UI correctly renders the card
      onEnd.type = 'on_end';
      onEnd.title = translateService.get('pipeline_item_title_on_end');
      onEnd.id = 'onEnd';

      return new RbPipelineWorkflowViewModel({
        triggers,
        tasks,
        onEnd,
        model: config,
      });
    }

    /**
     * Add a task from a {RbPipelineConfigTask}
     * @param {RbPipelineConfigTask} taskConfig
     * @param order The insert position
     * @returns {RbPipelineWorkflowItemViewModel}
     */
    addTaskFromConfig({ taskConfig, order }) {
      const taskItem = RbPipelineWorkflowItemViewModel.fromTask({
        task: taskConfig,
      });
      this.tasks.splice(order - 1, 0, taskItem);

      this.tasksSubject$.next(this.tasks);
      return taskItem;
    }

    replaceTaskFromConfig({ id, taskConfig }) {
      const index = this.tasks.findIndex(task => task.id === id);
      const taskItem = RbPipelineWorkflowItemViewModel.fromTask({
        id,
        task: taskConfig,
      });
      this.tasks[index] = taskItem;

      this.tasksSubject$.next(this.tasks);
      return taskItem;
    }

    /**
     * Remove an item either by ID or order
     * @param id
     * @param order
     */
    removeTask({ id, order }) {
      const index = this.tasks.findIndex(
        task => task.id === id || task.order === order,
      );
      this.tasks.splice(index, 1);

      this.tasksSubject$.next(this.tasks);
    }

    get allItems() {
      return [this.triggers, ...(this.tasks || []), this.onEnd];
    }

    // For P2, onEnd is also a task
    get allTasks() {
      return [...(this.tasks || []), this.onEnd].filter(
        task => task && task.type !== 'task_type_selection',
      );
    }

    // The tasks observable
    get tasks$() {
      return this.tasksSubject$.asObservable();
    }

    // Utility function, return the next item in the pipeline
    getNextItem(item) {
      const allItems = this.allItems;
      const index = allItems.findIndex(_item => _item.id === item.id);
      return index < allItems.length && index >= 0
        ? allItems[index + 1]
        : undefined;
    }

    /**
     * Reset selections for all items
     */
    deselectAll() {
      this.allItems.forEach(item => (item.selected = false));
    }
  }

  return RbPipelineWorkflowViewModel;
}
