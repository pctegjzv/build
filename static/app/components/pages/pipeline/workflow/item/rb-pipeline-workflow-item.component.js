const templateStr = require('app/components/pages/pipeline/workflow/item/rb-pipeline-workflow-item.html');
angular.module('app.components.pages').component('rbPipelineWorkflowItem', {
  controller: rbPipelineWorkflowItemController,
  bindings: {
    item: '<',
    workflow: '<',
  },
  controllerAs: 'vm',
  template: templateStr,
});

/**
 * A component representing a workflow item in a pipeline
 *
 *  - item: RbPipelineWorkflowItemModel
 *      The main data object to be bound to one pipeline item.
 */
function rbPipelineWorkflowItemController() {}
