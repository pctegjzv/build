const templateStr = require('app/components/pages/pipeline/workflow/rb-pipeline-workflow.html');
angular
  .module('app.components.pages')
  /**
   * A general purpose component representing the workflow for a P2 pipeline.
   *
   * A pipeline contains:
   *  - Triggers
   *    - Currently only support image repository
   *
   *  - Tasks.
   *    - For P2, there are 6 types
   *
   *  - On end tasks
   *    - For P2, there can only be one notification task
   */
  .component('rbPipelineWorkflow', {
    bindings: {
      workflow: '<', // Instance of RbPipelineWorkflowViewModel
    },
    controllerAs: 'vm',
    template: templateStr,
  });
