import _ from 'lodash';
import { RESOURCE_NAME_BASE } from 'app/components/common/config/common-pattern';
const templateStr = require('app/components/pages/project/create/rb-project-create.component.html');
angular.module('app.components.pages').component('rbProjectCreate', {
  controller: rbProjectCreateController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbProjectCreateController(
  $state,
  rbModal,
  translateService,
  rbRouterStateHelper,
  rbProjectDataService,
  rbProjectService,
  rbProjectTemplatesDataStore,
) {
  const vm = this;

  vm.$onInit = _init;
  vm.onTemplateSelected = onTemplateSelected;
  vm.previewRole = previewRole;
  vm.getResourceNameDisplay = getResourceNameDisplay;
  vm.create = create;
  vm.cancel = cancel;
  //////////

  function _init() {
    vm.resourceNameBaseRegExp = RESOURCE_NAME_BASE;
    vm.templates$ = rbProjectTemplatesDataStore.data$;
    vm.projectData = {};
    vm.roles = [];
    vm.resources = [];
  }

  function getResourceNameDisplay(name) {
    const projectName = vm.projectData.name;
    if (projectName) {
      return _.replace(name, '[name]', projectName);
    } else {
      return name;
    }
  }

  function onTemplateSelected(option) {
    vm.projectData.template = option.name;
    vm.roles = option.roles.map(role => {
      return {
        name: role.name,
        required: role.required,
        checked: role.required,
        permissions: role.permissions,
        id: role.id,
      };
    });
    vm.resources = option.resources.map(resource => {
      return {
        name: resource.name,
        id: resource.id,
        type: resource.type,
        required: resource.type,
        checked: resource.required,
      };
    });
  }

  function previewRole(role) {
    rbModal
      .show({
        width: 600,
        title: translateService.get('role'),
        component: 'rbParentRolePreviewDialog',
        locals: {
          roleName: role.name,
          permissions: role.permissions,
        },
      })
      .catch(() => {});
  }

  function cancel() {
    rbRouterStateHelper.back();
  }

  function create() {
    vm.form.$setSubmitted();

    if (!vm.form.$valid) {
      return;
    }
    vm.projectData.roles = vm.roles
      .filter(role => {
        return role.checked;
      })
      .map(role => {
        return {
          id: role.id,
        };
      });
    vm.projectData.resources = vm.resources
      .filter(resource => {
        return resource.checked;
      })
      .map(resource => {
        return {
          id: resource.id,
        };
      });
    // console.log(vm.projectData);
    vm.submitting = true;
    rbProjectDataService
      .createProject(vm.projectData)
      .then(data => {
        rbProjectService.set(data.name);
        $state.go('project_detail', { project_name: data.name });
      })
      .catch(() => {})
      .then(() => {
        vm.submitting = false;
      });
  }
}
