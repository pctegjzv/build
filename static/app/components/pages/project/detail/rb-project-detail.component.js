import { resource_types as RESOURCE_TYPES } from '../../rbac_role/rb-rbac-helper.constant';
const templateStr = require('app/components/pages/project/detail/rb-project-detail.component.html');
angular.module('app.components.pages').component('rbProjectDetail', {
  controller: rbProjectDetailController,
  controllerAs: 'vm',
  bindings: {
    projectName: '<',
  },
  template: templateStr,
});

function rbProjectDetailController(
  $state,
  rbProjectDataService,
  rbConfirmBox,
  rbRoleUtilities,
  translateService,
  rbProjectService,
  rbSafeApply,
) {
  const vm = this;

  vm.$onInit = _init;
  vm.deleteProject = deleteProject;
  vm.buttonDisplayExpr = buttonDisplayExpr;
  //////////

  async function _init() {
    vm.initialized = false;
    try {
      vm.project = await rbProjectDataService.getProjectDetail(vm.projectName);
      vm.initialized = true;
      rbSafeApply();
    } catch (Error) {
      vm.initialized = true;
    }
  }

  function deleteProject() {
    const title = translateService.get('delete');
    const textContent = translateService.get('delete_project_confirm', {
      name: vm.projectName,
    });
    rbConfirmBox
      .show({
        title,
        textContent,
      })
      .then(() => {
        _deleteProject();
      });
  }

  function _deleteProject() {
    vm.submitting = true;
    rbProjectDataService
      .deleteProject(vm.projectName)
      .then(() => {
        if (rbProjectService.get() === vm.projectName) {
          rbProjectService.set('');
        }
        $state.go('app_service.app');
      })
      .catch(() => {})
      .then(() => {
        vm.submitting = false;
      });
  }

  function buttonDisplayExpr(action) {
    return rbRoleUtilities.resourceHasPermission(
      vm.project,
      RESOURCE_TYPES.PROJECT,
      action,
    );
  }
}
