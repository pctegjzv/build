module.exports = [
  {
    baseStateName: 'project_create',
    states: [
      {
        name: 'list',
        url: '',
        component: 'rbProjectCreate',
      },
    ],
  },
  {
    // Base registered as a special rule in pages.module.js
    baseStateName: 'project_detail',
    states: [
      {
        name: 'dashboard',
        url: '',
        component: 'rbProjectDetail',
        resolve: {
          projectName: $stateParams => {
            'ngInject';
            return $stateParams['project_name'];
          },
        },
      },
      {
        name: 'role_create',
        url: '/role_create?:fromRole',
        component: 'rbRoleCreate',
        resolve: {
          fromRole: $stateParams => {
            'ngInject';
            return $stateParams['fromRole'];
          },
        },
      },
      {
        name: 'role_detail',
        url: '/role_detail/:roleName',
        component: 'rbRoleDetail',
        resolve: {
          roleName: $stateParams => {
            'ngInject';
            return $stateParams['roleName'];
          },
        },
      },
      {
        name: 'resource_list',
        url: '/resource_list',
        component: 'rbProjectResource',
        resolve: {
          projectName: $stateParams => {
            'ngInject';
            return $stateParams['project_name'];
          },
        },
      },
    ],
  },
];

angular
  .module('app.components.pages')
  .constant('rbPageProjectStates', module.exports);
