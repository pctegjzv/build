/**
 * Created by liudong on 2017/4/17.
 */
angular
  .module('app.components.utility')
  .factory('rbProjectDataService', rbProjectDataServiceFactory);

function rbProjectDataServiceFactory(rbHttp, rbAccountService) {
  const namespace = rbAccountService.getCurrentNamespace();
  const PROJECTS_URL = `/ajax/projects/${namespace}/`;
  const PROJECT_TEMPLATES_URL = `/ajax/project-templates/${namespace}/`;

  return {
    getProjects,
    getProjectDetail,
    getProjectStatus,
    createProject,
    deleteProject,
    updateProject,
    getProjectTemplates,
  };

  function getProjects() {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: PROJECTS_URL,
      })
      .then(({ result }) => result)
      .catch(() => []);
  }

  function getProjectDetail(project_name) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: PROJECTS_URL + project_name,
    });
  }

  function getProjectStatus(project_name) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: PROJECTS_URL + project_name + '/status',
      })
      .then(data => data)
      .catch(() => null);
  }

  function createProject(data) {
    return rbHttp.sendRequest({
      method: 'POST',
      url: PROJECTS_URL,
      data,
    });
  }

  function deleteProject(project_name) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url: PROJECTS_URL + project_name,
    });
  }

  function updateProject(project_name, data) {
    return rbHttp.sendRequest({
      method: 'PUT',
      url: PROJECTS_URL + project_name,
      data,
    });
  }

  function getProjectTemplates() {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: PROJECT_TEMPLATES_URL,
      })
      .then(({ result }) => result)
      .catch(() => []);
  }
}
