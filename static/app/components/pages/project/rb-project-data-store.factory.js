/**
 * Data store for project (in speak of multi-tenancy feature).
 */
angular
  .module('app.components.utility')
  .factory('rbProjectDataStore', rbProjectDataStoreFactory);

function rbProjectDataStoreFactory(
  rbHttp,
  RbFetchDataStore,
  rbProjectDataService,
) {
  const fetchRequest = () => rbProjectDataService.getProjects();

  class ProjectDataStore extends RbFetchDataStore {
    constructor() {
      super(fetchRequest);
    }
  }

  return new ProjectDataStore();
}
