/**
 * Created by liudong on 2017/4/15.
 */
angular
  .module('app.components.utility')
  .factory('rbProjectTemplatesDataStore', rbProjectTemplatesDataStoreFactory);

function rbProjectTemplatesDataStoreFactory(
  rbHttp,
  RbFetchDataStore,
  rbProjectDataService,
) {
  const fetchRequest = () => rbProjectDataService.getProjectTemplates();

  class ProjectTemplatesDataStore extends RbFetchDataStore {
    constructor() {
      super(fetchRequest);
    }
  }

  return new ProjectTemplatesDataStore();
}
