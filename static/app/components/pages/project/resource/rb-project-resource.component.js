import _ from 'lodash';
const templateStr = require('app/components/pages/project/resource/rb-project-resource.component.html');
angular.module('app.components.pages').component('rbProjectResource', {
  controller: rbProjectResourceController,
  controllerAs: 'vm',
  bindings: {
    projectName: '<',
  },
  template: templateStr,
});

function rbProjectResourceController(
  rbProjectDataService,
  rbDelay,
  rbRouterStateHelper,
  $log,
  rbToast,
  translateService,
  rbSafeApply,
) {
  const vm = this;
  vm.$onInit = _init;
  vm.$onDestroy = _destroy;
  //////////

  function _init() {
    vm.initialized = false;
    vm.filters = {};
    startPolling();
  }

  async function startPolling() {
    try {
      await refetch();
    } catch (err) {
      $log.error('Failed to poll detail');
    }

    await rbDelay(15000);
    if (_shouldPoll()) {
      startPolling();
    }
  }

  function _shouldPoll() {
    return !vm.destroyed;
  }

  async function refetch() {
    try {
      const status = await rbProjectDataService.getProjectStatus(
        vm.projectName,
      );
      groupResourceByType(status.resources);
    } catch ({ data }) {
      if (data && data.errors && data.errors[0].code === 'resource_not_exist') {
        rbToast.error(translateService.get('project_not_exist'));
        rbRouterStateHelper.back();
      }
    }

    vm.initialized = true;
    rbSafeApply();
  }

  function groupResourceByType(resources) {
    vm.resources = _.groupBy(resources, item => {
      return item.type;
    });
    vm.resourceTypes = _.uniq(Object.keys(vm.resources));
  }

  function _destroy() {
    vm.destroyed = true;
  }
}
