/**
 * Created by liudong on 2017/3/17.
 */
const { pick, remove } = require('lodash');
const templateStr = require('app/components/pages/rbac_account/dialog/accounts-create/rb-accounts-create-dialog.html');
angular.module('app.components.pages').component('rbAccountsCreateDialog', {
  controller: rbAccountsCreateDialogController,
  controllerAs: 'vm',
  bindings: {
    userName: '<',
  },
  template: templateStr,
});

function rbAccountsCreateDialogController(
  rbRoleDataService,
  orgService,
  rbAccountService,
  rbPatternHelper,
  translateService,
  rbChangePasswordErrorMapper,
  ENVIRONMENTS,
  rbModal,
) {
  const vm = this;
  const selectedRoles = [];
  vm.$onInit = _init;
  vm.confirmButtonDisabled = confirmButtonDisabled;
  vm.onAccountNamesChange = onAccountNamesChange;
  vm.accountNameValidateFn = accountNameValidateFn;
  vm.responseItemFilter = responseItemFilter;
  vm.onRoleAdd = onRoleAdd;
  vm.onRoleRemove = onRoleRemove;
  vm.confirm = confirm;
  vm.cancel = cancel;
  vm.ok = ok;

  //////////
  async function _init() {
    const namespace = rbAccountService.getCurrentNamespace();
    vm.roleBackendUrl = `/ajax/roles/${namespace}/?page=1&page_size=50&assign=true&namespace=${namespace}&order_by=created_at&action=view`;
    vm.accountNames = [];
    vm.privateDeploy = ENVIRONMENTS['is_private_deploy_enabled'];
    vm.profile = await rbAccountService.getUserProfile();
    vm.passwordPatternObj = rbPatternHelper.getPasswordPattern();
    vm.rbChangePasswordErrorMapper = rbChangePasswordErrorMapper;
    vm.password = '';
    vm.randomPassword = false;
    vm.showMoreInfo = false;
    vm.passwordView = false;
    vm.helpText = translateService.get('org_account_create_help', {
      email: vm.profile.email,
    });

    vm.initialized = true;
  }

  function onAccountNamesChange(tags) {
    vm.accountNames = tags;
  }

  function accountNameValidateFn(value) {
    const reg = /^[A-Za-z][A-Za-z0-9]{3,30}$/;
    return reg.test(value);
  }

  function responseItemFilter(option) {
    return !selectedRoles.find(item => item.uuid === option.uuid);
  }

  function onRoleAdd(option) {
    selectedRoles.push(option);
  }

  function onRoleRemove(option) {
    remove(selectedRoles, item => item.uuid === option.uuid);
  }

  function confirmButtonDisabled() {
    return !vm.initialized || !vm.accountNames.length || vm.form.$invalid;
  }

  async function confirm() {
    vm.form.$setSubmitted();
    if (vm.form.$invalid) {
      return;
    }
    vm.submitting = true;
    try {
      const data = await orgService.createRoleBasedAccounts({
        usernames: vm.accountNames,
        roles: selectedRoles.map(item => {
          return {
            name: item.name,
            uuid: item.uuid,
          };
        }),
        password: vm.randomPassword ? '' : vm.password,
        ...pick(vm, 'department', 'position', 'office_address'),
      });

      if (vm.passwordView) {
        vm.users = data.result;
        vm.createSuccess = true;
      } else {
        ok();
      }
    } catch (e) {
      vm.createSuccess = false;
    } finally {
      vm.submitting = false;
    }
  }

  function cancel() {
    rbModal.cancel();
  }

  function ok() {
    rbModal.hide();
  }
}
