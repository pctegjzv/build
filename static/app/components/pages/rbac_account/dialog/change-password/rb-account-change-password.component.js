const templateStr = require('app/components/pages/rbac_account/dialog/change-password/rb-account-change-password.html');
angular.module('app.components.pages').component('rbAccountChangePassword', {
  controller: rbAccountChangePasswordController,
  controllerAs: 'vm',
  bindings: {
    username: '<',
  },
  template: templateStr,
});

function rbAccountChangePasswordController(
  rbModal,
  translateService,
  rbAccountService,
  rbChangePasswordErrorMapper,
  rbToast,
  rbPatternHelper,
) {
  const vm = this;
  let currentUserName;
  vm.$onInit = _init;
  vm.showOldPasswordField = showOldPasswordField;

  //////////
  async function _init() {
    vm.isSubAccount = rbAccountService.isSubAccount();
    vm.passwordNotSame = false;
    vm.passwordPatternObj = rbPatternHelper.getPasswordPattern();
    vm.getPasswordStrength = rbPatternHelper.getPasswordStrength;
    vm.rbChangePasswordErrorMapper = rbChangePasswordErrorMapper;
    currentUserName = rbAccountService.getCurrentUsername();
  }

  vm.checkPasswordStrength = value =>
    !value || vm.getPasswordStrength(value) >= vm.passwordPatternObj.strength;

  vm.validateSamePassword = () => {
    vm.passwordNotSame =
      vm.confirm_new_password && vm.new_password !== vm.confirm_new_password;
  };

  vm.confirm = () => {
    vm.updatePasswordForm.$setSubmitted();
    if (vm.updatePasswordForm.$invalid) {
      return;
    }
    const params = {
      username: vm.username,
      password: vm.new_password,
    };
    if (vm.isSubAccount) {
      params['old_password'] = vm.old_password;
    }
    vm.submitting = true;
    rbAccountService
      .updateSubAccountPassword(params)
      .then(() => {
        rbToast.success(translateService.get('update_password_success'));
        rbModal.hide();
      })
      .catch(() => {})
      .then(() => {
        vm.submitting = false;
      });
  };

  vm.cancel = () => {
    rbModal.cancel();
  };

  function showOldPasswordField() {
    return vm.isSubAccount && currentUserName === vm.username;
  }
}
