import { debounce } from 'lodash';

import template from './rb-account-clean-ldap.component.html';

angular.module('app.components.pages').component('rbAccountCleanLdap', {
  template,
  controllerAs: 'vm',
  bindings: {
    invalidAccounts: '<',
    searchText: '<',
  },
  controller(orgService, rbModal, rbConfirmBox, rbToast, translateService) {
    const vm = this;
    vm.search = vm.searchText;

    Object.assign(vm, {
      rbModal,
      async fetchAccountList() {
        vm.accountsLoading = true;
        try {
          const { results, count } = await orgService.listOrgFilteredAccounts({
            search: vm.search,
            page_size: 0,
            invalid_ldap: true,
          });

          vm.accounts = results;
          vm.count = count;
        } catch (e) {
          vm.accounts = [];
        } finally {
          vm.accountsLoading = false;
        }
      },
      async deleteUsers(username) {
        const $modal = rbModal.getModalInstance().parent();
        $modal.css('cssText', 'display:none !important');
        try {
          await rbConfirmBox.show({
            title: translateService.get('delete_invalid_accounts'),
            textContent: `<h4>${translateService.get(
              username
                ? 'confrim_delete_invalid_user_tip'
                : 'confrim_delete_all_invalid_users_tip',
              {
                username,
                invalid_accounts: vm.invalidAccounts,
              },
            )}</h4><div style="color:#a1a1a1">${translateService.get(
              'confirm_delete_invalid_users_tip',
            )}</div>`,
          });

          try {
            await orgService.deleteLdapAccounts({
              delete_all: !username,
              usernames: username,
            });

            rbToast.success(
              translateService.get(
                username ? 'delete_success' : 'delete_invalid_accounts_success',
                {
                  invalid_accounts: vm.invalidAccounts,
                },
              ),
            );

            rbModal.hide({
              reopen: username && vm.invalidAccounts > 1,
              search: vm.search,
            });
            $modal.css('cssText', 'display:none !important');
          } catch (e) {
            //
          }
        } catch (e) {
          $modal.show();
        }
      },
    });

    vm.searchAccounts = debounce(vm.fetchAccountList, 300);

    vm.fetchAccountList();
  },
});
