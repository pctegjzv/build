/**
 * Created by liudong on 2017/3/17.
 */
const templateStr = require('app/components/pages/rbac_account/dialog/roles-add/rb-account-roles-add-dialog.html');
angular.module('app.components.pages').component('rbAccountRolesAddDialog', {
  controller: rbAccountRolesAddController,
  controllerAs: 'vm',
  bindings: {
    userName: '<',
  },
  template: templateStr,
});

function rbAccountRolesAddController(
  rbAccountService,
  rbRoleDataService,
  rbProjectService,
  orgService,
  WEBLABS,
  rbModal,
) {
  const vm = this;
  const selectedRoles = (vm.selectedRoles = []);
  let accountRoles = [];

  vm.$onInit = _init;
  vm.onRoleAdd = onRoleAdd;
  vm.onRoleRemove = onRoleRemove;
  vm.rolesFilter = rolesFilter;
  vm.confirm = confirm;
  vm.cancel = cancel;

  //////////
  async function _init() {
    const namespace = rbAccountService.getCurrentNamespace();
    const currentProject = rbProjectService.get();
    vm.initialized = false;
    vm.avaliableRoles = [];
    vm.roleBackendUrl = `/ajax/roles/${namespace}/?page=1&page_size=50&assign=true&namespace=${namespace}&order_by=created_at&action=view`;
    if (WEBLABS['projects_enabled'] && currentProject) {
      vm.roleBackendUrl += `&project_name=${currentProject}`;
    }
    await fetchAccountRoles();
    vm.initialized = true;
  }

  function fetchAccountRoles() {
    return orgService
      .getAccountRoles(vm.userName)
      .then(data => {
        accountRoles = data.result;
      })
      .catch(() => {
        accountRoles = [];
      });
  }

  function onRoleAdd(value) {
    selectedRoles.push(value);
  }

  function onRoleRemove(value) {
    const index = _.findIndex(selectedRoles, item => {
      return item.uuid === value;
    });
    selectedRoles.splice(index, 1);
  }

  function rolesFilter(item) {
    return !_.find(accountRoles, accountRole => {
      return item.uuid === accountRole.role_uuid;
    });
  }

  function confirm() {
    vm.form.$setSubmitted();
    if (!selectedRoles.length) {
      return;
    }
    _addRoles();
  }

  function cancel() {
    rbModal.cancel();
  }

  function _addRoles() {
    vm.submitting = true;
    const _roles = selectedRoles.map(item => {
      return {
        name: item,
      };
    });
    return orgService
      .addAccountRoles(vm.userName, _roles)
      .then(() => {
        rbModal.hide(_roles);
      })
      .catch(() => {})
      .then(() => {
        vm.submitting = false;
      });
  }
}
