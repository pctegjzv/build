/**
 * Created by liudong on 2017/3/16.
 */
import { resource_types as RESOURCE_TYPES } from '../../rbac_role/rb-rbac-helper.constant';

const _ = require('lodash');
const templateStr = require('app/components/pages/rbac_account/list/rb-account-list.html');
const popoverTemplate = require('ngtemplate-loader!app/components/pages/rbac_account/popover_template/rb-account-roles-popover.html');
angular.module('app.components.pages').component('rbAccountList', {
  controller: rbAccountListController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbAccountListController(
  rbAccountService,
  rbRoleDataService,
  rbRoleUtilities,
  orgService,
  rbModal,
  $state,
  $timeout,
  rbSafeApply,
  rbConfirmBox,
  WEBLABS,
  rbProjectService,
  rb2RouterUtil,
  translateService,
) {
  const vm = this;
  const userRolesCache = {};

  let search = '';
  let ldapConfig;

  vm.$onInit = _init;

  vm.onSearchChanged = onSearchChanged;
  vm.deleteInvalidAccounts = deleteInvalidAccounts;
  vm.pageNoChange = pageNoChange;
  vm.updatePassword = updatePassword;
  vm.addRole = addRole;
  vm.deleteAccount = deleteAccount;
  vm.fetchAccountRoles = fetchAccountRoles;
  vm.revokeRole = revokeRole;
  vm.createAccounts = createAccounts;
  vm.addUsers = addUsers;
  vm.buttonDisplayExpr = buttonDisplayExpr;
  vm.isInvalidAccount = isInvalidAccount;
  vm.checkRoleResourceAction = checkRoleResourceAction;
  vm.triggerLdapSync = triggerLdapSync;
  vm.getAccountRoleNameDisplay = getAccountRoleNameDisplay;
  vm.viewAccountRoleDetail = viewAccountRoleDetail;
  vm.shouldShowRoleLink = shouldShowRoleLink;
  vm.gotoUserDetail = gotoUserDetail;

  //////////
  async function _init() {
    vm.resourceHasPermission = rbRoleUtilities.resourceHasPermission;
    vm.isSubAccount = rbAccountService.isSubAccount();
    vm.initialized = false;
    vm.accounts = [];
    try {
      if (!vm.isSubAccount) {
        ldapConfig = await orgService.getOrgLdap();
      } else {
        ldapConfig = await orgService.getOrgLdapSync();
      }
    } catch (e) {
      ldapConfig = null;
    }
    vm.ldapConfigTrue = ldapConfig && !ldapConfig.empty;
    vm.pagination = {
      page: 1,
      count: 0,
      page_size: 20,
    };
    vm.popoverConfig = {
      list: [],
      loading: false,
      username: '',
      noDataText: translateService.get('account_has_no_role'),
      template: popoverTemplate,
    };
    vm.user = await orgService.getOrgAccount({
      username: rbAccountService.getCurrentUsername(),
    });

    await fetchBasicInfo();

    vm.initialized = true;

    if (WEBLABS['projects_enabled'] && rbProjectService.get()) {
      vm.inProject = true;
    }
    //TPAccount ，Third party account type
    vm.createAccountEnabled =
      (await rbRoleUtilities.resourceTypeSupportPermissions(
        RESOURCE_TYPES.SUBACCOUNT,
      )) &&
      !vm.inProject &&
      vm.user.type !== 'organizations.TPAccount';
    vm.roleAssignEnabled = await rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.ROLE,
      {},
      'assign',
    );
    vm.ldapSyncEnabled = await rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.ORGANIZATION,
      {},
      'sync_users',
    );
    vm.addUserEnabled = vm.roleAssignEnabled && vm.inProject;

    rbSafeApply();
  }

  async function fetchAccountList() {
    vm.accountsLoading = true;
    try {
      const { results, count } = await orgService.listOrgFilteredAccounts({
        search,
        page: vm.pagination.page,
        page_size: vm.pagination.page_size,
      });

      vm.accounts = results;
      vm.pagination.count = count;
    } catch (e) {
      vm.accounts = [];
    } finally {
      vm.accountsLoading = false;
    }
  }

  async function getOrgLdapInfo() {
    const {
      result: { invalid_accounts },
    } = await orgService.getOrgLdapInfo();
    vm.invalid_accounts = invalid_accounts;
  }

  function fetchBasicInfo() {
    let promises = fetchAccountList();
    if (!vm.isSubAccount) {
      promises = Promise.all([promises, getOrgLdapInfo()]);
    }
    return promises;
  }

  function onSearchChanged(value) {
    search = value;
    vm.pagination.page = 1;
    vm.searching = true;
    fetchAccountList().then(() => {
      vm.searching = false;
    });
  }

  async function deleteInvalidAccounts(searchText) {
    try {
      const { reopen, search } = await rbModal.show({
        title: translateService.get('clean_invalid_ldap_users'),
        component: 'rbAccountCleanLdap',
        width: 800,
        locals: {
          invalidAccounts: vm.invalid_accounts,
          searchText,
        },
      });

      // wait fetch latest invalid_accounts number first
      await fetchBasicInfo();

      if (reopen) {
        deleteInvalidAccounts(search);
      }
    } catch (e) {
      //
    }
  }

  function pageNoChange(page) {
    vm.pagination.page = page;
    fetchAccountList();
  }

  function updatePassword(item) {
    rbModal.show({
      title: translateService.get('update_password'),
      component: 'rbAccountChangePassword',
      locals: {
        username: item.username,
      },
    });
  }

  function addRole(account) {
    rbModal
      .show({
        width: 600,
        title: translateService.get('add_role'),
        component: 'rbAccountRolesAddDialog',
        locals: {
          userName: account.username,
        },
      })
      .then(() => {
        // remove account releated roles cache
        delete userRolesCache[account.username];
      })
      .catch(() => {});
  }

  async function deleteAccount(item) {
    try {
      await rbConfirmBox.show({
        title: translateService.get('delete'),
        textContent: translateService.get('confirm_remove_account_from_org', {
          username: item.username,
        }),
      });
      _deleteAccount(item);
    } catch (e) {
      //
    }
  }

  async function _deleteAccount(item) {
    vm.submitting = true;
    try {
      await (isInvalidAccount(item)
        ? orgService.deleteLdapAccounts({
            usernames: item.username,
          })
        : orgService.removeOrgAccount({
            username: item.username,
          }));

      await fetchBasicInfo();
    } finally {
      vm.submitting = false;
    }
  }

  function fetchAccountRoles(username) {
    vm.popoverConfig.username = username;
    vm.popoverConfig.list = [];
    vm.popoverConfig.loading = true;
    $timeout(() => {
      orgService
        .getAccountRoles(username)
        .then(data => {
          userRolesCache[username] = data.result;
          vm.popoverConfig.list = userRolesCache[username];
        })
        .catch(() => {})
        .then(() => {
          vm.popoverConfig.loading = false;
        });
    }, 200);
  }

  function revokeRole(role_name) {
    const users = [
      {
        user: vm.popoverConfig.username,
      },
    ];
    const list = userRolesCache[vm.popoverConfig.username];
    vm.popoverConfig.loading = true;
    rbRoleDataService
      .deleteRoleUsers(role_name, users)
      .then(() => {
        _.forEach(list, (item, index) => {
          if (item.role_name === role_name) {
            list.splice(index, 1);
            return false;
          }
        });
      })
      .catch(() => {})
      .then(() => {
        vm.popoverConfig.loading = false;
      });
  }

  function createAccounts() {
    rbModal
      .show({
        width: 600,
        title: translateService.get('create_account'),
        component: 'rbAccountsCreateDialog',
      })
      .then(() => {
        // refetch accounts list
        fetchAccountList();
      })
      .catch(() => {});
  }

  function addUsers() {
    rbModal
      .show({
        width: 600,
        title: translateService.get('add_user'),
        component: 'rbRoleDetailUserAddDialog',
        locals: {
          users: vm.accounts,
        },
      })
      .then(() => {
        fetchAccountList();
      })
      .catch(() => {});
  }

  function buttonDisplayExpr(item, action) {
    const accountTypeTest =
      item.type !== 'organizations.LDAPAccount' ||
      (item.is_valid
        ? !['delete', 'update_password'].includes(action)
        : action === 'delete' && !vm.isSubAccount);
    return (
      accountTypeTest &&
      rbRoleUtilities.resourceHasPermission(
        item,
        RESOURCE_TYPES.SUBACCOUNT,
        action,
      )
    );
  }

  function isInvalidAccount(account) {
    return account.type === 'organizations.LDAPAccount' && !account.is_valid;
  }

  function checkRoleResourceAction(item, action) {
    return rbRoleUtilities.resourceHasPermission(
      item,
      RESOURCE_TYPES.ROLE,
      action,
    );
  }

  async function triggerLdapSync() {
    vm.triggerLdapSyncIng = true;
    try {
      await orgService.triggerOrgLdapSync({
        orgName: rbAccountService.getCurrentNamespace(),
      });
      await fetchBasicInfo();
    } finally {
      vm.triggerLdapSyncIng = false;
    }
  }

  function getAccountRoleNameDisplay(item) {
    return item.project_name
      ? `${item.project_name}/${item.role_name}`
      : item.role_name;
  }

  function viewAccountRoleDetail(item, $event) {
    $event.preventDefault();
    const currentProjectName = rbProjectService.get();
    if (
      WEBLABS['projects_enabled'] &&
      (item.project_name && item.project_name === currentProjectName)
    ) {
      $state.go('project_detail.role_detail', {
        project_name: item.project_name,
        roleName: item.role_name,
      });
    } else {
      $state.go('org.role_detail', {
        roleName: item.role_name,
      });
    }
  }

  function shouldShowRoleLink(item) {
    if (WEBLABS['projects_enabled']) {
      const currentProjectName = rbProjectService.get();
      return (
        !item.project_name ||
        (currentProjectName && currentProjectName === item.project_name)
      );
    }
    return true;
  }

  function gotoUserDetail(username) {
    rb2RouterUtil.go('user.detail', { username });
  }
}
