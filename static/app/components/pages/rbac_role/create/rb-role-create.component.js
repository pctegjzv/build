/**
 * Created by liudong on 2017/3/6.
 */
const _ = require('lodash');
const templateStr = require('app/components/pages/rbac_role/create/rb-role-create.html');
angular.module('app.components.pages').component('rbRoleCreate', {
  controller: rbRoleCreateController,
  bindings: {
    fromRole: '<',
  },
  controllerAs: 'vm',
  template: templateStr,
});

function rbRoleCreateController(
  rbRoleDataService,
  translateService,
  rbToast,
  rbRouterStateHelper,
  WEBLABS,
  rbProjectService,
  $state,
) {
  const vm = this;

  vm.$onInit = _init;
  vm.create = create;
  vm.cancel = cancel;

  //////////

  async function _init() {
    if (vm.fromRole) {
      vm.role = await rbRoleDataService.getRole(vm.fromRole);
      vm.role.name = '';
      if (!vm.role.parents) {
        vm.role.parents = [];
      }
      if (!vm.role.permissions) {
        vm.role.permissions = [];
      }
      Object.keys(vm.role).forEach(key => {
        if (!['name', 'permissions', 'parents'].includes(key)) {
          delete vm.role[key];
        }
      });
    } else {
      vm.role = {
        name: '',
        parents: [],
        permissions: [],
      };
    }
    vm.initialized = true;
  }

  function create() {
    vm.form.$setSubmitted();

    if (!vm.form.$valid) {
      return;
    }
    // filter out parent roles with empty name
    vm.role.parents = vm.role.parents.filter(item => !!item.name);

    // permissions or parent roles is required
    if (!vm.role.permissions.length && !vm.role.parents.length) {
      rbToast.warning(translateService.get('role_create_permission_required'));
      return;
    }
    vm.submitting = true;
    rbRoleDataService
      .createRole(vm.role)
      .then(data => {
        const roleName = _.get(data, 'result[0].name');
        if (WEBLABS['projects_enabled'] && rbProjectService.get()) {
          $state.go('project_detail.role_detail', { roleName });
        } else {
          $state.go('org.role_detail', { roleName });
        }
      })
      .catch(() => {})
      .then(() => {
        vm.submitting = false;
      });
  }

  function cancel() {
    rbRouterStateHelper.back();
  }
}
