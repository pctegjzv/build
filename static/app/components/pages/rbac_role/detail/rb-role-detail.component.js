/**
 * Created by liudong on 2017/3/14.
 */
import { resource_types as RESOURCE_TYPES } from '../rb-rbac-helper.constant';
const templateStr = require('app/components/pages/rbac_role/detail/rb-role-detail.html');
angular.module('app.components.pages').component('rbRoleDetail', {
  controller: rbRoleDetailController,
  controllerAs: 'vm',
  bindings: {
    roleName: '<',
  },
  template: templateStr,
});

function rbRoleDetailController(
  rbRoleDataService,
  $state,
  translateService,
  rbSafeApply,
  rbRoleUtilities,
  rbModal,
  rbConfirmBox,
) {
  const vm = this;

  vm.$onInit = _init;

  vm.copyRole = copyRole;
  vm.addPermission = addPermission;
  vm.updatePermission = updatePermission;
  vm.deletePermission = deletePermission;
  vm.previewParent = previewParent;
  vm.addParent = addParent;
  vm.deleteParent = deleteParent;
  vm.buttonDisplayExpr = buttonDisplayExpr;
  vm.parentRoleButtonDisplayExpr = parentRoleButtonDisplayExpr;
  //////////

  async function _init() {
    vm.getPermissionResourceDisplay =
      rbRoleUtilities.getPermissionResourceDisplay;
    vm.getPermissionActionsDisplay =
      rbRoleUtilities.getPermissionActionsDisplay;
    vm.getPermissionConstraintsDisplay =
      rbRoleUtilities.getPermissionConstraintsDisplay;

    vm.initialized = false;
    vm.role = null;
    if (!vm.roleName) {
      $state.go('role.list');
    }
    vm.createRoleEnabled = await rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.ROLE,
    );
    getRoleDetail().then(() => {
      vm.initialized = true;
    });
  }

  function getRoleDetail() {
    return rbRoleDataService.getRole(vm.roleName).then(role => {
      vm.role = role;
      vm.role.permissions = _.sortBy(vm.role.permissions, item => {
        return item.resource.join('');
      });
    });
  }

  function copyRole() {
    if ($state.current.name.split('.').includes('project_detail')) {
      $state.go('project_detail.role_create', {
        fromRole: vm.roleName,
      });
    } else {
      $state.go('org.role_create', {
        fromRole: vm.roleName,
      });
    }
  }

  function addPermission() {
    rbModal
      .show({
        width: 650,
        title: translateService.get('add_permission'),
        component: 'rbRolePermissionEditDialog',
      })
      .then(permission => {
        _addPermission(permission);
        rbSafeApply();
      })
      .catch(() => {});
  }

  // TODO: we do not allow update of permission temporarily
  function updatePermission(item) {
    rbModal
      .show({
        width: 650,
        title: translateService.get('update_permission'),
        component: 'rbRolePermissionEditDialog',
        locals: {
          permission: item,
        },
      })
      .then(permission => {
        _updatePermission(permission.uuid, permission);
        rbSafeApply();
      })
      .catch(() => {});
  }

  function deletePermission(item) {
    const title = translateService.get('delete');
    const textContent = translateService.get('delete_permission_confirm');
    rbConfirmBox
      .show({
        title,
        textContent,
      })
      .then(() => {
        _deletePermission(item.uuid);
      });
  }

  function _addPermission(permission) {
    rbRoleDataService
      .addPermissionOfRole(vm.role.name, permission)
      .then(() => {
        getRoleDetail();
      })
      .catch(() => {});
  }

  function _updatePermission(uuid, permission) {
    rbRoleDataService
      .updatePermissionOfRole(vm.role.name, uuid, permission)
      .then(() => {
        getRoleDetail();
      })
      .catch(() => {});
  }

  function _deletePermission(uuid) {
    rbRoleDataService
      .deletePermissionOfRole(vm.role.name, uuid)
      .then(() => {
        // getRoleDetail();
        const index = _.findIndex(vm.role.permissions, item => {
          return item.uuid === uuid;
        });
        index >= 0 && vm.role.permissions.splice(index, 1);
      })
      .catch(() => {});
  }

  function previewParent(item) {
    rbModal
      .show({
        width: 600,
        title: translateService.get('parent_role'),
        component: 'rbParentRolePreviewDialog',
        locals: {
          roleName: item.name,
        },
      })
      .catch(() => {});
  }

  function deleteParent(item) {
    const title = translateService.get('delete');
    const textContent = translateService.get('delete_parent_role_confirm', {
      name: item.name,
    });
    rbConfirmBox
      .show({
        title,
        textContent,
      })
      .then(() => {
        _deleteParent(item.uuid);
      });
  }

  function _deleteParent(uuid) {
    rbRoleDataService
      .deleteParent(vm.role.name, uuid)
      .then(() => {
        const index = _.findIndex(vm.role.parents, item => {
          return item.uuid === uuid;
        });
        index >= 0 && vm.role.parents.splice(index, 1);
      })
      .catch(() => {});
  }

  function addParent() {
    rbModal
      .show({
        width: 600,
        title: translateService.get('add_parent_role'),
        component: 'rbRoleDetailParentAddDialog',
        locals: {
          role: vm.role,
        },
      })
      .then(() => {
        getRoleDetail();
      })
      .catch(() => {});
  }

  function buttonDisplayExpr(action) {
    return rbRoleUtilities.resourceHasPermission(
      vm.role,
      RESOURCE_TYPES.ROLE,
      action,
    );
  }

  function parentRoleButtonDisplayExpr(item, action) {
    return rbRoleUtilities.resourceHasPermission(
      item,
      RESOURCE_TYPES.ROLE,
      action,
    );
  }
}
