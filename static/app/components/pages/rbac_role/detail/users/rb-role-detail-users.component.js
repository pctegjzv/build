/**
 * Created by liudong on 2017/3/15.
 */

import { resource_types as RESOURCE_TYPES } from '../../rb-rbac-helper.constant';
const _ = require('lodash');
const templateStr = require('app/components/pages/rbac_role/detail/users/rb-role-detail-users.html');
angular.module('app.components.pages').component('rbRoleDetailUsers', {
  controller: rbRoleDetailUsersController,
  controllerAs: 'vm',
  bindings: {
    role: '<',
  },
  template: templateStr,
});

function rbRoleDetailUsersController(
  rbRoleDataService,
  rbModal,
  rbConfirmBox,
  rbRoleUtilities,
  translateService,
) {
  const vm = this;
  let search = '';

  vm.$onInit = _init;

  vm.addUser = addUser;
  vm.deleteUser = deleteUser;
  vm.buttonDisplayExpr = buttonDisplayExpr;
  vm.pageNoChange = pageNoChange;
  vm.onSearchChanged = onSearchChanged;
  ///////////

  function _init() {
    vm.roleName = vm.role.name;
    vm.pagination = {
      page: 1,
      count: 0,
      page_size: 20,
    };
    vm.initialized = true;
    fetchRoleUsers();
  }

  function fetchRoleUsers() {
    vm.loading = true;
    return rbRoleDataService
      .listRoleUsers({
        page: vm.pagination.page,
        page_size: vm.pagination.page_size,
        role_name: vm.roleName,
        search,
      })
      .then(data => {
        vm.users = data.results;
        vm.pagination.count = data.count;
      })
      .catch(() => {
        vm.users = [];
      })
      .then(() => {
        vm.loading = false;
      });
  }

  function addUser() {
    rbModal
      .show({
        width: 600,
        title: translateService.get('add_user'),
        component: 'rbRoleDetailUserAddDialog',
        locals: {
          users: vm.users,
          roleName: vm.roleName,
        },
      })
      .then(() => {
        fetchRoleUsers();
      })
      .catch(() => {});
  }

  function deleteUser(item) {
    const title = translateService.get('delete');
    const textContent = translateService.get('delete_user_role_confirm', {
      name: item.user,
    });
    rbConfirmBox
      .show({
        title,
        textContent,
      })
      .then(() => {
        _deleteUser(item.user);
      });
  }

  function _deleteUser(user) {
    const _users = [
      {
        user,
      },
    ];
    return rbRoleDataService
      .deleteRoleUsers(vm.roleName, _users)
      .then(() => {
        const index = _.findIndex(vm.users, item => {
          return item.user === user;
        });
        index >= 0 && vm.users.splice(index, 1);
      })
      .catch(() => {});
  }

  function onSearchChanged(value) {
    search = value;
    vm.pagination.page = 1;
    vm.searching = true;
    fetchRoleUsers().then(() => {
      vm.searching = false;
    });
  }

  function pageNoChange(page) {
    vm.pagination.page = page;
    fetchRoleUsers();
  }

  function buttonDisplayExpr(action) {
    return rbRoleUtilities.resourceHasPermission(
      vm.role,
      RESOURCE_TYPES.ROLE,
      action,
    );
  }
}
