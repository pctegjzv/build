/**
 * Created by liudong on 2017/3/15.
 */
const _ = require('lodash');
const templateStr = require('app/components/pages/rbac_role/dialog/parent_add/rb-role-detail-parent-add-dialog.html');
angular
  .module('app.components.pages')
  .component('rbRoleDetailParentAddDialog', {
    controller: rbRoleDetailParentAddController,
    controllerAs: 'vm',
    bindings: {
      role: '<',
    },
    template: templateStr,
  });

function rbRoleDetailParentAddController(
  rbAccountService,
  rbRoleDataService,
  rbModal,
) {
  const vm = this;
  const parent = {};
  vm.$onInit = _init;
  vm.onParentRoleSelected = onParentRoleSelected;
  vm.roleFilter = roleFilter;
  vm.confirm = confirm;
  vm.cancel = cancel;

  //////////

  function _init() {
    const namespace = rbAccountService.getCurrentNamespace();
    vm.roleBackendUrl = `/ajax/roles/${namespace}/?page=1&page_size=50&assign=true&namespace=${namespace}&order_by=created_at&action=view`;
    vm.initialized = true;
  }

  function roleFilter(option) {
    return (
      option.uuid !== vm.role.uuid &&
      !_.find(vm.role.parents, parent => {
        return parent.uuid === option.uuid;
      })
    );
  }

  function onParentRoleSelected(option) {
    parent.name = option.name;
    parent.uuid = option.uuid;
  }

  function confirm() {
    if (!parent.name) {
      return;
    }
    _addParent();
  }

  function cancel() {
    rbModal.cancel();
  }

  function _addParent() {
    return rbRoleDataService
      .addParent(vm.role.name, parent)
      .then(() => {
        rbModal.hide();
      })
      .catch(() => {})
      .then(() => {});
  }
}
