/**
 * Created by liudong on 2017/3/14.
 */
const templateStr = require('app/components/pages/rbac_role/dialog/parent_preview/rb-parent-role-preview-dialog.html');
angular.module('app.components.pages').component('rbParentRolePreviewDialog', {
  controller: rbParentRolePreviewController,
  controllerAs: 'vm',
  bindings: {
    roleName: '<',
    permissions: '<',
  },
  template: templateStr,
});

function rbParentRolePreviewController(rbRoleDataService, rbModal) {
  const vm = this;

  vm.$onInit = _init;
  vm.cancel = cancel;
  //////////

  function _init() {
    vm.initialized = false;
    if (vm.permissions) {
      vm.initialized = true;
    } else {
      getRoleDetail().then(() => {
        vm.initialized = true;
      });
    }
  }

  function getRoleDetail() {
    return rbRoleDataService.getRole(vm.roleName).then(role => {
      vm.permissions = role.permissions;
    });
  }

  function cancel() {
    rbModal.cancel();
  }
}
