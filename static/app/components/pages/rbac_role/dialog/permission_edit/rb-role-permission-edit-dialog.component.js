/**
 * Created by liudong on 2017/3/13.
 */
import { resource_types as RESOURCE_TYPES } from '../../rb-rbac-helper.constant';

import { cloneDeep, flatten, merge, uniq } from 'lodash';

import templateStr from 'app/components/pages/rbac_role/dialog/permission_edit/rb-role-permission-edit-dialog.html';

angular.module('app.components.pages').component('rbRolePermissionEditDialog', {
  controller: rbRolePermissionEditController,
  controllerAs: 'vm',
  bindings: {
    permission: '<',
  },
  template: templateStr,
});

function rbRolePermissionEditController(
  rbRoleDataService,
  rbSafeApply,
  rbRoleUtilities,
  rbModal,
  WEBLABS,
  rbProjectService,
) {
  const vm = this;
  let userInProject;

  vm.$onInit = _init;

  vm.onResourceTypeSelected = onResourceTypeSelected;
  vm.onResourceActionAdd = onResourceActionAdd;
  vm.onResourceActionRemove = onResourceActionRemove;
  vm.resourceNameValiadteFn = resourceNameValiadteFn;
  vm.onResourceNamesChange = onResourceNamesChange;
  // vm.addConstraint = addConstraint;
  vm.addConstraintToGroup = addConstraintToGroup;
  vm.deleteConstraintFromGroup = deleteConstraintFromGroup;
  vm.onConstraintFieldSelected = onConstraintFieldSelected;
  vm.getActionsValidationState = getActionsValidationState;
  vm.addConstraintGroup = addConstraintGroup;
  vm.deleteConstraintGroup = deleteConstraintGroup;
  vm.confirm = confirm;
  vm.cancel = cancel;

  //////////

  function _init() {
    vm.initialized = false;
    vm.schemaList = [];
    vm.selectedResourceType = '';
    // form init values
    vm.initActions = [];
    vm.initResourceNames = [];
    // form fields
    vm.resourceTypes = [];
    vm.resourceActions = [];
    vm.resourceConstraintFields = [];
    if (!vm.permission) {
      initEmptyPermissionObject();
    } else {
      initExistedPermissionObject();
    }
    userInProject = WEBLABS['projects_enabled'] && rbProjectService.get();
    fetchRoleSchemas();
  }

  function initEmptyPermissionObject() {
    vm.resourceConstraints = [
      {
        selectedConstraintName: '',
        inputConstraintValue: '',
        constraints: [],
      },
    ];
    vm.permission = {
      resource: [],
      actions: [],
      constraints: [],
    };
  }

  function initExistedPermissionObject() {
    vm.permission = cloneDeep(vm.permission);
    vm.initActions = vm.permission.actions;
    vm.initResourceNames = vm.permission.resource;
    let resources = vm.permission.actions.map(action => {
      return action.split(':')[0];
    });
    resources = uniq(resources);
    if (resources.length > 1) {
      vm.selectedResourceType = '*';
    } else {
      vm.selectedResourceType = resources[0];
    }
    // init vm.resourceConstraints from vm.permission.constraintsvm.permission.constraintsvm.permission.constraints.permission.constraints
    vm.resourceConstraints = [];
    vm.permission.constraints &&
      vm.permission.constraints.forEach(constraint => {
        const group = {
          selectedConstraintName: '',
          inputConstraintValue: '',
          constraints: [],
        };
        Object.keys(constraint).forEach(key => {
          group.constraints.push({
            name: key,
            value: constraint[key],
          });
        });
        vm.resourceConstraints.push(group);
      });
  }

  function fetchRoleSchemas() {
    rbRoleDataService.getRoleSchemaList().then(resourceSchemaList => {
      vm.schemaList = resourceSchemaList.filter(schema => {
        return (
          WEBLABS['projects_enabled'] ||
          ![RESOURCE_TYPES.PROJECT, RESOURCE_TYPES.PROJECT_TEMPLATE].includes(
            schema.resource,
          )
        );
      });
      vm.initialized = true;
      rbSafeApply();
    });
  }

  function _generateResourceConstraintsFields(constraints) {
    return Object.keys(constraints)
      .filter(item => {
        const [, field] = item.split(':');
        if (
          (!WEBLABS['projects_enabled'] || userInProject) &&
          field === 'project'
        ) {
          return false;
        }
        return true;
      })
      .sort()
      .map(item => {
        const [, field] = item.split(':');
        return {
          name: field,
          value: item,
        };
      });
  }

  function onResourceTypeSelected(option) {
    if (option.general) {
      vm.resourceActions = flatten(vm.schemaList.map(item => item.actions));
      let constraints = {};
      vm.schemaList
        .filter(item => {
          return !item.general;
        })
        .forEach(item => {
          constraints = merge(constraints, item.constraints);
        });
      vm.resourceConstraintFields = _generateResourceConstraintsFields(
        constraints,
      );
    } else {
      vm.resourceActions = option.actions.concat(`${option.resource}:*`);
      vm.resourceConstraintFields = _generateResourceConstraintsFields(
        option.constraints,
      );
    }
    if (option.resource !== vm.selectedResourceType) {
      vm.resourceConstraints = [
        {
          selectedConstraintName: '',
          inputConstraintValue: '',
          constraints: [],
        },
      ];
    }
    vm.selectedResourceType = option.resource;
  }

  function getActionsValidationState() {
    return vm.form.$submitted && !vm.permission.actions.length;
  }

  function onResourceActionAdd(option) {
    if (vm.permission.actions.indexOf(option.value) === -1) {
      vm.permission.actions.push(option.value);
    }
  }

  function onResourceActionRemove(option) {
    vm.permission.actions.forEach((action, index) => {
      if (action === option.value) {
        vm.permission.actions.splice(index, 1);
        return false;
      }
    });
  }

  function onResourceNamesChange(tags) {
    vm.permission.resource = tags;
  }

  function resourceNameValiadteFn(item) {
    const regs = [
      /^(\*|\w)(\w|-|\.)*\*?$/,
      /^(\*|\w)(\w|-|\.)+((\w|-|\.)*|\*?)$/,
      /^(\*|\w)(\w|-|\.)+\*?(\w|-|\.)+\*?$/,
    ];
    return regs.some(reg => reg.test(item));
  }

  function addConstraintToGroup(group) {
    const constraint = group.constraints.find(item => {
      return item.name === group.selectedConstraintName;
    });
    if (constraint) {
      constraint.value = group.inputConstraintValue;
    } else {
      group.constraints.push({
        name: group.selectedConstraintName,
        value: group.inputConstraintValue,
      });
    }
  }

  function deleteConstraintFromGroup(group, index) {
    group.constraints.splice(index, 1);
  }

  function onConstraintFieldSelected(option, group) {
    group.selectedConstraintName = option.value;
  }

  function addConstraintGroup() {
    vm.resourceConstraints.push({
      selectedConstraintName: '',
      inputConstraintValue: '',
      constraints: [],
    });
  }

  function deleteConstraintGroup(index) {
    vm.resourceConstraints.splice(index, 1);
  }

  function confirm() {
    vm.form.$setSubmitted();
    const constraints = [];
    const globalTypes = rbRoleUtilities.getGlobalResourceTypes(vm.schemaList);
    vm.resourceConstraints.forEach(group => {
      const constraintObject = {};
      group.constraints.forEach(item => {
        constraintObject[item.name] = item.value;
      });
      if (WEBLABS['projects_enabled'] && rbProjectService.get()) {
        if (!globalTypes.includes(vm.selectedResourceType)) {
          constraintObject['res:project'] = rbProjectService.get();
        }
      }
      constraints.push(constraintObject);
    });

    vm.permission.constraints = constraints;
    vm.permission.resource_type = vm.selectedResourceType;
    // resource and actions in permission is required, if resource is empty, use * as default
    if (!vm.permission.actions.length) {
      return;
    }
    if (!vm.permission.resource.length) {
      vm.permission.resource.push('*');
    }
    rbModal.hide(vm.permission);
  }

  function cancel() {
    rbModal.cancel();
  }
}
