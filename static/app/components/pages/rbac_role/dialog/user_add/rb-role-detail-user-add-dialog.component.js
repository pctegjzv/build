/**
 * Created by liudong on 2017/3/15.
 */
const templateStr = require('app/components/pages/rbac_role/dialog/user_add/rb-role-detail-user-add-dialog.html');

angular.module('app.components.pages').component('rbRoleDetailUserAddDialog', {
  controller: rbRoleDetailUserAddController,
  controllerAs: 'vm',
  bindings: {
    users: '<',
    roleName: '<',
  },
  template: templateStr,
});

function rbRoleDetailUserAddController(
  WEBLABS,
  rbRoleDataService,
  orgService,
  rbAccountService,
  rbProjectService,
  rbModal,
) {
  const vm = this;
  const users = [];
  vm.$onInit = _init;
  vm.responseItemFilter = responseItemFilter;
  vm.onRoleChanged = onRoleChanged;
  vm.onUserRemove = onUserRemove;
  vm.onUserAdd = onUserAdd;
  vm.confirm = confirm;
  vm.cancel = cancel;

  //////////

  async function _init() {
    const namespace = rbAccountService.getCurrentNamespace();
    const currentProject = rbProjectService.get();
    vm.backendUrl = `/ajax/org/account/filtered_list?page=1&page_size=20&action=view&assign=true&order_by=username&namespace=${namespace}`;
    vm.roleBackendUrl = `/ajax/roles/${namespace}/?page=1&page_size=50&assign=true&namespace=${namespace}&order_by=created_at&action=view`;
    if (WEBLABS['projects_enabled'] && currentProject) {
      vm.roleBackendUrl += `&project_name=${currentProject}`;
    }
    vm.initialized = true;
  }

  function responseItemFilter(options) {
    return !users.find(user => user === options.username);
  }

  function onRoleChanged(value) {
    vm.selectedRoleName = value;
  }

  function onUserAdd(value) {
    users.push(value);
  }

  function onUserRemove(value) {
    users.splice(users.findIndex(item => item === value), 1);
  }

  function confirm() {
    vm.form.$setSubmitted();
    if (!users.length || (!vm.selectedRoleName && !vm.roleName)) {
      return;
    }
    _addUsers();
  }

  function cancel() {
    rbModal.cancel();
  }

  function _addUsers() {
    vm.submitting = true;
    const _users = users.map(item => {
      return {
        user: item,
      };
    });
    return rbRoleDataService
      .addRoleUsers(vm.selectedRoleName || vm.roleName, _users)
      .then(() => {
        rbModal.hide();
      })
      .catch(() => {})
      .then(() => {
        vm.submitting = false;
      });
  }
}
