/**
 * Created by liudong on 2017/3/13.
 */
const templateStr = require('app/components/pages/rbac_role/fieldset/parent_roles/rb-parent-roles-fieldset.html');
angular.module('app.components.pages').component('rbParentRolesFieldset', {
  controller: rbParentRolesFieldsetController,
  controllerAs: 'vm',
  bindings: {
    parentRoles: '<',
  },
  template: templateStr,
});

function rbParentRolesFieldsetController(
  $log,
  rbAccountService,
  rbRoleDataService,
  rbModal,
  translateService,
) {
  const vm = this;

  vm.$onInit = _init;

  vm.onParentRoleSelect = onParentRoleSelect;
  vm.previewRole = previewRole;
  vm.addRole = addRole;
  vm.deleteAddedRole = deleteAddedRole;
  vm.deleteClonedRole = deleteClonedRole;

  //////////

  async function _init() {
    const namespace = rbAccountService.getCurrentNamespace();
    vm.roleBackendUrl = `/ajax/roles/${namespace}/?page=1&page_size=50&assign=true&namespace=${namespace}&order_by=created_at&action=view`;
    if (vm.parentRoles && vm.parentRoles.length) {
      vm.clonedParentRoles = _.clone(vm.parentRoles);
    }
    vm.addedParentRoles = [];
    vm.initialized = true;
  }

  function onParentRoleSelect(option, item) {
    item.name = option.name;
    item.uuid = option.uuid;
  }

  function previewRole(item) {
    rbModal
      .show({
        width: 700,
        title: translateService.get('parent_role'),
        component: 'rbParentRolePreviewDialog',
        locals: {
          roleName: item.name,
        },
      })
      .catch(() => {});
  }

  function deleteAddedRole(index, item) {
    vm.addedParentRoles.splice(index, 1);
    _.remove(vm.parentRoles, _item => {
      return item.uuid === _item.uuid;
    });
  }

  function addRole() {
    const role = {
      name: '',
    };
    vm.parentRoles.push(role);
    vm.addedParentRoles.push(role);
  }

  function deleteClonedRole(index, item) {
    vm.clonedParentRoles.splice(index, 1);
    _.remove(vm.parentRoles, _item => {
      return item.uuid === _item.uuid;
    });
  }
}
