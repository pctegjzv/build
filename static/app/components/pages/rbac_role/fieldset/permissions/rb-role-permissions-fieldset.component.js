/**
 * Created by liudong on 2017/3/13.
 */
const templateStr = require('app/components/pages/rbac_role/fieldset/permissions/rb-role-permissions-fieldset.html');
angular.module('app.components.pages').component('rbRolePermissionsFieldset', {
  controller: rbRolePermissionsFieldsetController,
  controllerAs: 'vm',
  bindings: {
    preview: '<',
    permissions: '<',
  },
  template: templateStr,
});

function rbRolePermissionsFieldsetController(
  rbModal,
  translateService,
  rbRoleUtilities,
  rbSafeApply,
) {
  const vm = this;

  vm.$onInit = _init;

  vm.addPermission = addPermission;
  vm.updatePermission = updatePermission;
  vm.deletePermission = deletePermission;

  //////////

  function _init() {
    vm.getPermissionResourceDisplay =
      rbRoleUtilities.getPermissionResourceDisplay;
    vm.getPermissionActionsDisplay =
      rbRoleUtilities.getPermissionActionsDisplay;
    vm.getPermissionConstraintsDisplay =
      rbRoleUtilities.getPermissionConstraintsDisplay;
  }

  function deletePermission(index) {
    vm.permissions.splice(index, 1);
  }

  function addPermission() {
    rbModal
      .show({
        width: 650,
        title: translateService.get('add_permission'),
        component: 'rbRolePermissionEditDialog',
      })
      .then(permission => {
        vm.permissions.push(permission);
        rbSafeApply();
      })
      .catch(() => {});
  }

  function updatePermission(index, item) {
    rbModal
      .show({
        width: 650,
        title: translateService.get('update_permission'),
        component: 'rbRolePermissionEditDialog',
        locals: {
          permission: item,
        },
      })
      .then(permission => {
        vm.permissions[index] = permission;
        rbSafeApply();
      })
      .catch(() => {});
  }
}
