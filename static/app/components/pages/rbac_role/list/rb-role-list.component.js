/**
 * Created by liudong on 2017/3/15.
 */
import { resource_types as RESOURCE_TYPES } from '../rb-rbac-helper.constant';
const templateStr = require('app/components/pages/rbac_role/list/rb-role-list.html');
const popoverTemplate = require('ngtemplate-loader!app/components/pages/rbac_role/popover_template/rb-role-users-popover.html');
angular.module('app.components.pages').component('rbRoleList', {
  controller: rbRoleListController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbRoleListController(
  $state,
  rbSafeApply,
  rbConfirmBox,
  translateService,
  rbRoleUtilities,
  rbDelay,
  rbRoleDataService,
  rbProjectService,
  WEBLABS,
  ngRouter,
) {
  const vm = this;
  let search = '';
  const roleUsersCache = [];

  vm.$onInit = _init;

  vm.onSearchChanged = onSearchChanged;
  vm.pageNoChange = pageNoChange;
  vm.createRole = createRole;
  vm.showRoleDetail = showRoleDetail;
  vm.deleteRole = deleteRole;
  vm.fetchRoleUsers = fetchRoleUsers;
  vm.deleteUserOfRole = deleteUserOfRole;
  vm.buttonDisplayExpr = buttonDisplayExpr;

  //////////
  async function _init() {
    vm.initialized = false;
    vm.roles = [];
    vm.pagination = {
      page: 1,
      count: 0,
      page_size: 20,
    };
    vm.popoverConfig = {
      list: [],
      loading: false,
      role_name: '',
      role: null, // used for resource_actions check
      noDataText: translateService.get('role_has_no_user'),
      template: popoverTemplate,
    };
    vm.createRoleEnabled = await rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.ROLE,
    );
    vm.hasTemplatePermission = await rbRoleUtilities.resourceTypeSupportPermissions(
      RESOURCE_TYPES.ROLE,
      {},
      'view',
    );
    fetchRoleList().then(() => {
      rbSafeApply();
      vm.initialized = true;
    });
  }

  function fetchRoleList() {
    vm.rolesLoading = true;
    return rbRoleDataService
      .getRoleList({
        search,
        page: vm.pagination.page,
        page_size: vm.pagination.page_size,
      })
      .then(data => {
        vm.roles = data.results;
        vm.pagination.count = data.count;
      })
      .catch(() => {
        vm.roles = [];
      })
      .then(() => {
        vm.rolesLoading = false;
      });
  }

  function onSearchChanged(value) {
    search = value;
    vm.pagination.page = 1;
    vm.searching = true;
    fetchRoleList().then(() => {
      vm.searching = false;
    });
  }

  function pageNoChange(page) {
    vm.pagination.page = page;
    fetchRoleList();
  }

  function createRole() {
    if (WEBLABS['projects_enabled'] && rbProjectService.get()) {
      $state.go('project_detail.role_create');
    } else {
      ngRouter.navigateByUrl('/rbac/roles/create');
    }
  }

  function showRoleDetail(item) {
    if (WEBLABS['projects_enabled'] && rbProjectService.get()) {
      $state.go('project_detail.role_detail', { roleName: item.name });
    } else {
      $state.go('org.role_detail', { roleName: item.name });
    }
  }

  function deleteRole(item) {
    const title = translateService.get('delete_role');
    const textContent = translateService.get('delete_role_confirm', {
      name: item.name,
    });
    rbConfirmBox
      .show({
        title,
        textContent,
      })
      .then(() => {
        _deleteRole(item);
      });
  }

  function _deleteRole(role) {
    rbRoleDataService
      .deleteRole(role.name)
      .then(() => {
        fetchRoleList();
      })
      .catch(() => {});
  }

  function fetchRoleUsers(role) {
    const role_name = role.name;
    vm.popoverConfig.role = role;
    vm.popoverConfig.role_name = role_name;
    vm.popoverConfig.list = [];
    vm.popoverConfig.loading = true;
    rbDelay(200);
    rbRoleDataService
      .listRoleUsers({
        page_size: 50,
        role_name,
      })
      .then(data => {
        roleUsersCache[role_name] = data.results;
        vm.popoverConfig.list = roleUsersCache[role_name];
      })
      .catch(() => {})
      .then(() => {
        vm.popoverConfig.loading = false;
      });
  }

  function deleteUserOfRole(username) {
    const users = [
      {
        user: username,
      },
    ];
    const list = roleUsersCache[vm.popoverConfig.role_name];
    vm.popoverConfig.loading = true;
    rbRoleDataService
      .deleteRoleUsers(vm.popoverConfig.role_name, users)
      .then(() => {
        _.forEach(list, (item, index) => {
          if (item.user === username) {
            list.splice(index, 1);
            return false;
          }
        });
      })
      .catch(() => {})
      .then(() => {
        vm.popoverConfig.loading = false;
      });
  }

  function buttonDisplayExpr(item, action) {
    return rbRoleUtilities.resourceHasPermission(
      item,
      RESOURCE_TYPES.ROLE,
      action,
    );
  }
}
