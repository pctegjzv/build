/**
 * Created by liudong on 2017/3/6.
 */
angular
  .module('app.components.pages')
  .factory('rbRoleDataService', rbRoleDataServiceFactory);

function rbRoleDataServiceFactory(rbHttp, rbAccountService) {
  const ENDPOINT_ROLE = '/ajax/';
  const NAMESPACE = rbAccountService.getCurrentNamespace();
  return {
    getRoleList,
    createRole,
    getRole,
    deleteRole,
    addPermissionOfRole,
    updatePermissionOfRole,
    deletePermissionOfRole,
    addParent,
    deleteParent,
    getRoleSchemaList,
    getRoleSchema,
    listRoleUsers,
    addRoleUsers,
    deleteRoleUsers,
    createRoleUser,
    deleteRoleUser,
    getContextPermissions,
  };

  function getRoleList({
    search = '',
    page = 1,
    page_size = 20,
    action = 'view',
  }) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: `${ENDPOINT_ROLE}roles/${NAMESPACE}/`,
      params: {
        search,
        page,
        page_size,
        order_by: 'created_at',
        action,
      },
    });
  }

  function createRole(data) {
    return rbHttp.sendRequest({
      method: 'POST',
      url: `${ENDPOINT_ROLE}roles/${NAMESPACE}/`,
      data,
    });
  }

  function getRole(role_name) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: `${ENDPOINT_ROLE}roles/${NAMESPACE}/${role_name}/`,
    });
  }

  function deleteRole(role_name) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url: `${ENDPOINT_ROLE}roles/${NAMESPACE}/${role_name}/`,
    });
  }

  function addPermissionOfRole(role_name, data) {
    return rbHttp.sendRequest({
      method: 'POST',
      url: `${ENDPOINT_ROLE}roles/${NAMESPACE}/${role_name}/permissions/`,
      data,
    });
  }

  function updatePermissionOfRole(role_name, permission_uuid, data) {
    return rbHttp.sendRequest({
      method: 'PUT',
      url: `${ENDPOINT_ROLE}roles/${NAMESPACE}/${role_name}/permissions/${permission_uuid}`,
      data,
    });
  }

  function deletePermissionOfRole(role_name, permission_uuid) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url: `${ENDPOINT_ROLE}roles/${NAMESPACE}/${role_name}/permissions/${permission_uuid}`,
    });
  }

  function addParent(role_name, data) {
    return rbHttp.sendRequest({
      method: 'POST',
      url: `${ENDPOINT_ROLE}roles/${NAMESPACE}/${role_name}/parents/`,
      data,
    });
  }

  function deleteParent(role_name, parent_uuid) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url: `${ENDPOINT_ROLE}roles/${NAMESPACE}/${role_name}/parents/${parent_uuid}`,
    });
  }

  function getRoleSchemaList() {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: ENDPOINT_ROLE + 'role-schema/',
      })
      .then(data => data.result)
      .catch(() => {
        return [];
      });
  }

  function getRoleSchema(resource_type = '') {
    return rbHttp.sendRequest({
      method: 'GET',
      url: `${ENDPOINT_ROLE}role-schema/${resource_type}`,
    });
  }

  function listRoleUsers({ role_name, page = 1, page_size = 20, search = '' }) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: `${ENDPOINT_ROLE}roles/${NAMESPACE}/${role_name}/users/`,
      params: {
        page,
        page_size,
        search,
      },
    });
  }

  function addRoleUsers(role_name, users) {
    return rbHttp.sendRequest({
      method: 'POST',
      url: `${ENDPOINT_ROLE}roles/${NAMESPACE}/${role_name}/users/`,
      data: users,
      addNamespace: false,
    });
  }

  function deleteRoleUsers(role_name, users) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      headers: {
        'Content-Type': 'Application/json',
      },
      url: `${ENDPOINT_ROLE}roles/${NAMESPACE}/${role_name}/users/`,
      data: users,
      addNamespace: false,
    });
  }

  function createRoleUser(role_name, data) {
    return rbHttp.sendRequest({
      headers: {
        'Content-Type': 'Application/json',
      },
      method: 'POST',
      url: `${ENDPOINT_ROLE}roles/${NAMESPACE}/${role_name}/users/`,
      data,
    });
  }

  function deleteRoleUser(role_name, data) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url: `${ENDPOINT_ROLE}roles/${NAMESPACE}/${role_name}/users/`,
      data,
    });
  }

  function getContextPermissions(resource_type, context) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: `/ajax/permissions/${NAMESPACE}/${resource_type}/`,
        params: context,
        addNamespace: false,
        cache: false,
      })
      .then(data => {
        return data.result || [];
      })
      .catch(() => []);
  }
}
