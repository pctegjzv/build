/**
 * Created by liudong on 2017/3/6.
 */

angular.module('app.components.pages').config(roleStatesConfig);

roleStatesConfig.$inject = ['$stateProvider'];

function roleStatesConfig($stateProvider) {
  $stateProvider
    .state('role', {
      url: '/role',
      abstract: true,
      legacy: true,
    })
    .state('role.list', {
      url: '/list',
      component: 'rbRoleList',
      legacy: true,
    });
}
