angular
  .module('app.components.pages')
  .factory('rbRegionDataService', rbRegionDataService);

function rbRegionDataService(rbHttp, rbAccountService) {
  const namespace = rbAccountService.getCurrentNamespace();
  const REGIONS_URL = `/ajax/regions/${namespace}/`;
  const CLUSTERS_URL = `/ajax/v2/kubernetes/clusters/`;

  // eslint-disable-next-line no-unused-vars
  function getRegions(useCached = false, filterEmpty = true) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: REGIONS_URL,
      })
      .then(({ result }) => ({ regions: result }))
      .then(res => {
        if (filterEmpty) {
          res.regions = res.regions.filter(region => {
            return (
              region.container_manager !== 'NONE' && region.state === 'RUNNING'
            );
          });
        }
        return res;
      });
  }

  function getRegionDetail(regionName) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: REGIONS_URL + regionName,
      })
      .then(result => ({ result }));
  }

  function getRegionLabels(regionName) {
    return rbHttp.sendRequest({
      url: REGIONS_URL + regionName + '/labels',
    });
  }

  function checkAvailableNodes(labelsStr, region_name) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: REGIONS_URL + region_name + '/node-selector',
      params: {
        labels: labelsStr,
      },
    });
  }

  function getRegionNamespaces(regionId) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: CLUSTERS_URL + regionId + '/namespaces',
        addNamespace: false,
      })
      .then(({ result }) => result);
  }

  function getRegionNamespaceOptions(regionId) {
    return getRegionNamespaces(regionId).then(namespaces =>
      namespaces.map(({ kubernetes: { metadata: { name, uid } } }) => ({
        name,
        uuid: uid,
      })),
    );
  }

  return {
    getRegions,
    getRegionDetail,
    getRegionLabels,
    checkAvailableNodes,
    getRegionNamespaces,
    getRegionNamespaceOptions,
  };
}
