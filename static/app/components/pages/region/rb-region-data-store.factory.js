import { map } from 'rxjs/operators';

angular
  .module('app.components.pages')
  .factory('rbRegionDataStore', rbRegionDataStoreFactory);

function rbRegionDataStoreFactory(RbFetchDataStore, rbRegionDataService) {
  const fetchRequest = () => {
    return rbRegionDataService
      .getRegions(false, false)
      .then(({ regions }) => regions);
  };

  class RegionDataStore extends RbFetchDataStore {
    constructor() {
      super(fetchRequest);
    }

    get all$() {
      return this.data$;
    }

    get allNonEmpty$() {
      return this.all$.pipe(
        map(
          regions =>
            regions
              ? regions.filter(
                  region =>
                    region.container_manager !== 'NONE' &&
                    region.state === 'RUNNING',
                )
              : [],
        ),
      );
    }
  }

  return new RegionDataStore();
}
