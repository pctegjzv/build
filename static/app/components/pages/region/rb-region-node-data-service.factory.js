angular
  .module('app.components.pages')
  .factory('rbRegionNodeDataService', rbRegionNodeDataService);

function rbRegionNodeDataService(rbHttp, translateService, rbAccountService) {
  const namespace = rbAccountService.getCurrentNamespace();
  const REGIONS_URL = `/ajax/regions/${namespace}/`;
  const NODES_POINT = '/nodes/';

  function nodeList(regionName) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: REGIONS_URL + regionName + NODES_POINT,
    });
  }

  function nodeTags(regionName) {
    return nodeList(regionName).then(data => {
      let nodes =
        data.result.filter(nodeItem => nodeItem.node_tag !== undefined) || [];
      nodes = nodes.map(node => {
        if (node.resources && node.resources.available_mem) {
          let cpu = node.resources.available_cpus
            ? node.resources.available_cpus
            : node.resources.avaliable_cpus;
          const mem = node.resources.available_mem;
          cpu = parseFloat(cpu).toFixed(3);
          node['node_tag_key'] = `${node.node_tag} (${translateService.get(
            'status_free',
          )} CPU:${cpu}/MEM:${mem})`;
        } else {
          node['node_tag_key'] = node['node_tag'];
        }
        return node;
      });
      return nodes;
    });
  }

  return {
    nodeTags,
  };
}
