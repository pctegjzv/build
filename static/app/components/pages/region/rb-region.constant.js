/**
 * Created by liudong on 2017/2/21.
 */

const CONTAINER_MANAGERS = {
  LEGACY: 'LEGACY',
  MESOS: 'MESOS',
  SWARM: 'SWARM',
  KUBERNETES: 'KUBERNETES',
};

const NODE_TYPE_MAP = {
  MESOS: 'mesos-slave',
  SWARM: 'swarm_worker',
  KUBERNETES: 'k8s-slave',
};

const REGION_CLOUD_NAMES = {
  PRIVATE: 'PRIVATE',
  AWS: 'AWS',
  ALI: 'ALI',
  QING: 'QING',
  UCLOUD: 'UCLOUD',
  QCLOUD: 'QCLOUD',
  OPENSTACK: 'OPENSTACK',
  BAIDU: 'BAIDU',
};

const CONTAINER_MANAGER_ARR = _.values(CONTAINER_MANAGERS);

const REGION_CONSTANTS = {
  CONTAINER_MANAGERS,
  CONTAINER_MANAGER_ARR,
  REGION_CLOUD_NAMES,
  NODE_TYPE_MAP,
};

module.exports = REGION_CONSTANTS;

angular
  .module('app.components.pages')
  .constant('REGION_CONSTANTS', REGION_CONSTANTS);
