angular
  .module('app.components.pages')
  .factory('rbQuotaSpaceDataService', rbQuotaSpaceDataServiceFactory);
import { resource_types as RESOURCE_TYPES } from '../../rbac_role/rb-rbac-helper.constant';

function rbQuotaSpaceDataServiceFactory(
  rbHttp,
  rbToast,
  translateService,
  rbRoleUtilities,
  rbAccountService,
) {
  const namespace = rbAccountService.getCurrentNamespace();
  const SPACES_URL = `/ajax/spaces/${namespace}/`;
  const SPACE_URL = `${SPACES_URL}space/`;

  return {
    getSpaces,
    getConsumableSpaces,
    getSpace,
    getQuotaCharges,
    getQuotaConfig,
    getSpaceResources,
    createSpace,
    deleteSpace,
    updateSpace,
  };

  function getSpaces({ action = 'view' } = {}) {
    return rbHttp
      .sendRequest({
        url: SPACES_URL,
        params: {
          action,
        },
      })
      .then(({ result }) => result);
  }

  function getConsumableSpaces() {
    return getSpaces({ action: 'consume' }).then(list => {
      return list.filter(item => {
        return rbRoleUtilities.resourceHasPermission(
          item,
          RESOURCE_TYPES.SPACE,
          'consume',
        );
      });
    });
  }

  function getSpace({ name }) {
    return rbHttp.sendRequest({
      url: SPACE_URL + name,
    });
  }

  function getSpaceResources({ name }) {
    return rbHttp
      .sendRequest({
        url: SPACE_URL + name + '/resources',
      })
      .then(({ result }) => result);
  }

  function deleteSpace({ name }) {
    return rbHttp
      .sendRequest({
        url: SPACE_URL + name,
        method: 'DELETE',
      })
      .then(res => {
        rbToast.success(translateService.get('quota_space_delete_success'));
        return res;
      });
  }

  function getQuotaCharges({ spaceName, quotaName }) {
    return rbHttp
      .sendRequest({
        url: SPACE_URL + spaceName + '/quota/' + quotaName,
      })
      .then(({ result }) => result);
  }

  function getQuotaConfig() {
    return rbHttp
      .sendRequest({
        url: SPACES_URL + 'config',
        cache: true,
      })
      .then(({ configs }) => configs);
  }

  function createSpace(payload) {
    return rbHttp
      .sendRequest({
        method: 'POST',
        data: payload,
        url: SPACES_URL,
      })
      .then(res => {
        rbToast.success(translateService.get('quota_space_create_success'));
        return res;
      });
  }

  function updateSpace(payload) {
    return rbHttp
      .sendRequest({
        method: 'PUT',
        data: payload,
        url: SPACE_URL + payload.name,
      })
      .then(res => {
        rbToast.success(translateService.get('quota_space_update_success'));
        return res;
      });
  }
}
