angular
  .module('app.components.pages')
  .factory('rbStorageDataService', rbStorageDataServiceFactory);

function rbStorageDataServiceFactory(
  rbHttp,
  translateService,
  rbToast,
  rbAccountService,
) {
  const namespace = rbAccountService.getCurrentNamespace();

  const STORAGE_URL = `/ajax/storage/${namespace}/`;
  const VOLUMES_URL = `${STORAGE_URL}volumes/`;
  const SNAPSHOTS_URL = `${STORAGE_URL}snapshots/`;

  return {
    deleteVolume,
    getVolume,
    getVolumes,
    createVolume,
    deleteSnapshot,
    getSnapshots,
    createSnapshot,
    getVolumeDrivers,
  };

  ///////////
  function getVolume({ id }) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: VOLUMES_URL + id,
    });
  }

  function getVolumes({
    regionId,
    pageno = 1,
    size = 20,
    volumeNames,
    listAll = false, // This will list all volumes, with lite info
    action = 'view',
  }) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: VOLUMES_URL,
      params: {
        region_id: regionId,
        pageno,
        size,
        volume_names: volumeNames,
        list_all: listAll,
        action,
      },
    });
  }

  function createVolume({ volume }) {
    return rbHttp
      .sendRequest({
        method: 'POST',
        url: VOLUMES_URL,
        data: volume,
        timeout: 3 * 60 * 1000,
      })
      .then(res => {
        rbToast.success(translateService.get('storage_volume_create_success'));
        return res;
      });
  }

  function deleteVolume({ id }) {
    return rbHttp
      .sendRequest({
        method: 'DELETE',
        url: VOLUMES_URL + id,
      })
      .then(() => {
        rbToast.success(translateService.get('storage_volume_delete_success'));
      });
  }

  function deleteSnapshot({ id }) {
    return rbHttp
      .sendRequest({
        method: 'DELETE',
        url: SNAPSHOTS_URL + id,
      })
      .then(() => {
        rbToast.success(
          translateService.get('storage_snapshot_delete_success'),
        );
      });
  }

  function getSnapshots({
    regionId,
    pageno,
    size,
    listAll = false, // This will list all snapshots, with lite info
  }) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: SNAPSHOTS_URL,
      params: {
        region_id: regionId,
        pageno,
        size,
        list_all: listAll,
      },
    });
  }

  function createSnapshot({ snapshot }) {
    return rbHttp
      .sendRequest({
        method: 'POST',
        url: SNAPSHOTS_URL,
        data: snapshot,
        timeout: 3 * 60 * 1000,
      })
      .then(res => {
        rbToast.success(
          translateService.get('storage_snapshot_create_success'),
        );
        return res;
      });
  }

  function getVolumeDrivers({ regionName }) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: STORAGE_URL + regionName + '/drivers',
        cache: true,
      })
      .then(rep => rep.result)
      .catch(() => []);
  }
}
