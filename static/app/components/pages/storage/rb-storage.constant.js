export const DRIVERS = {
  ebs: ['size', 'volume_type'],
  glusterfs: ['size'],
  local: ['bricks'],
  loongstore: ['size'],
  ceph_rbd: ['size'],
};

// Translate volume states to Rubick status
export const VOLUME_STATES = {
  available: 'free',
  'in-use': 'used',
  error: 'error',
  pending: 'pending',
};

// Translate snapshot states to Rubick status
export const SNAPSHOT_STATES = {
  completed: 'completed',
  error: 'error',
  pending: 'pending',
};

export const SNAPSHOT_SUPPORTED_DRIVERS = ['ebs', 'glusterfs'];

export const VOLUME_TYPES = ['io1', 'gp2'];

angular.module('app.components.pages').constant('STORAGE_CONSTANTS', {
  DRIVERS,
  VOLUME_STATES,
  SNAPSHOT_STATES,
  VOLUME_TYPES,
});
