const _ = require('lodash');

const { VOLUME_STATES } = require('../../../rb-storage.constant');
const templateStr = require('app/components/pages/storage/volume/list/table/rb-storage-volume-list-table.html');
angular.module('app.components.pages').component('rbStorageVolumeListTable', {
  bindings: {
    region: '<',
    // User can provide a service detail object to preload the volume results (e.g., in service detail page)
    serviceDetail: '<',
    disableControls: '@',
  },
  controller: rbStorageVolumeListTableController,
  controllerAs: 'vm',
  template: templateStr,
});

function rbStorageVolumeListTableController(
  $timeout,
  $state,
  rbStorageDataService,
) {
  const vm = this;

  // Region id => pageno => volumes
  const volumeListCache = {};
  let _currentRegionName;

  vm.pageNoChange = pageNoChange;
  vm.viewServiceDetail = viewServiceDetail;

  vm.$onInit = onInit;
  vm.$onChanges = onChanges;

  ///////////
  async function onInit() {
    vm.pagination = {
      pageSize: 20,
      pageno: 1,
      totalItems: 0,
    };
    vm.deletingMap = {};
    vm.driversByRegionId = {};
    vm.loadError = false;
    vm.volumes = null;
    vm.volumeStateMap = VOLUME_STATES;
    await initRegionInfo();
    vm.initialized = true;
  }

  async function onChanges({ region }) {
    if (
      region &&
      region.currentValue &&
      region.currentValue.name !== _currentRegionName
    ) {
      vm.volumeCreateAllowed = false;
      vm.initialized && initRegionInfo();
    }
  }

  async function initRegionInfo() {
    const region = vm.region;
    _currentRegionName = region.name;
    vm.pagination.pageno = 1;
    vm.pagination.totalItems = 0;
    pageNoChange();
    // Init the cache for the region
    volumeListCache[region.id] = volumeListCache[region.id] || {};
    volumeListCache[region.id][vm.pagination.pageno] =
      volumeListCache[region.id][vm.pagination.pageno] || {};

    rbStorageDataService
      .getVolumeDrivers({ regionName: region.name })
      .then(drivers => {
        vm.driversByRegionId[region.id] = drivers;
      });
  }

  async function pageNoChange(
    page = vm.pagination.pageno,
    size = vm.pagination.pageSize,
  ) {
    // For volume list displayed within service detail, we will skip pagination
    if (vm.serviceDetail) {
      size = 100; // 100 should be enough
    }

    vm.pagination.pageno = page;
    vm.pagination.pageSize = size;
    loadDataFromCache();
    try {
      vm.loading = true;
      await _updateVolumeList({
        regionId: vm.region.id,
        pageno: vm.pagination.pageno,
        size: vm.pagination.pageSize,
      });
      loadDataFromCache();
      vm.loadError = false;
    } catch (e) {
      vm.loadError = true;
    }
    vm.loading = false;
  }

  function loadDataFromCache() {
    const data = _.get(
      volumeListCache,
      `${vm.region.id}.${vm.pagination.pageno}`,
      {},
    );
    //const data = volumeListCache[vm.region.id][vm.pagination.pageno] || {};
    vm.pagination.totalItems = vm.pagination.totalItems || data.total_items;
    vm.volumes = data.volumes;
    vm.loadError = false;
  }

  /**
   * Update data in the cache for regionId/pageno
   */
  async function _updateVolumeList({ regionId, pageno, size }) {
    const updatedVolumes = await rbStorageDataService.getVolumes({
      regionId,
      pageno,
      size,
      volumeNames:
        vm.serviceDetail &&
        _.uniq(vm.serviceDetail.volumes.map(volume => volume.volume_name)),
    });

    // Decorate results for service detail page
    if (vm.serviceDetail && vm.serviceDetail.volumes) {
      const serviceVolumesMap = _.groupBy(
        vm.serviceDetail.volumes,
        'volume_name',
      );
      updatedVolumes.volumes.forEach(volume => {
        // for host path volumes, the stat is empty returned from API. We need to manually generate it here:
        if (!volume.stats) {
          volume.stats = serviceVolumesMap[volume.name].map(serviceVolume => ({
            app_volume_dir: serviceVolume.app_volume_dir,
          }));
          // Set date to null for 'hostPath'
          volume.created_at = null;
        }
      });
    }
    volumeListCache[regionId][pageno] = updatedVolumes;
  }

  function viewServiceDetail(stat) {
    $state.go('app_service.service.service_detail', {
      service_name: stat.service_id,
    });
  }
}
