angular
  .module('app.components.utility')
  .factory('envfileService', envfileService);
function envfileService(rbHttp, rbAccountService) {
  const namespace = rbAccountService.getCurrentNamespace();
  const ENVFILE_ENDPOINT = '/ajax/v1/env-files/' + namespace;
  let exported = '';

  function envfileList({ action = 'view' } = {}) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: ENVFILE_ENDPOINT,
        params: {
          action,
          detail: true,
        },
        addNamespace: false,
      })
      .then(({ result }) => ({ envfiles: result }));
  }

  function envfileDetail(envfileName) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: ENVFILE_ENDPOINT + '/' + envfileName,
        addNamespace: false,
      })
      .then(res => {
        const content = res.content;
        const envvar_lines = [];
        for (let i = 0; i < content.length; i++) {
          envvar_lines.push(content[i].join('='));
        }
        res.content = envvar_lines.join('\n');
        return { envfile: res };
      });
  }

  function envfileCreate(data) {
    delete data.namespace;
    return rbHttp
      .sendRequest({
        method: 'POST',
        url: ENVFILE_ENDPOINT,
        data,
        addNamespace: false,
      })
      .then(res => ({ env_file: res }));
  }

  function envfileUpdate(data) {
    const name = data.origin_name;
    delete data.namespace;
    delete data.origin_name;
    return rbHttp
      .sendRequest({
        method: 'PUT',
        url: ENVFILE_ENDPOINT + '/' + name,
        data,
        addNamespace: false,
      })
      .then(res => ({ env_file: res }));
  }

  function envfileDelete(envfileName) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url: ENVFILE_ENDPOINT + '/' + envfileName,
      addNamespace: false,
    });
  }

  function setExport(content) {
    exported = content;
  }

  function getExport() {
    return exported;
  }

  return {
    envfileList: envfileList,
    envfileDetail: envfileDetail,
    envfileCreate: envfileCreate,
    envfileUpdate: envfileUpdate,
    envfileDelete: envfileDelete,
    setExport: setExport,
    getExport: getExport,
  };
}
