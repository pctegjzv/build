angular.module('app.components.utility').factory('eventService', eventService);

function eventService(rbHttp, rbAccountService) {
  const namespace = rbAccountService.getCurrentNamespace();
  const URL = `/ajax/events/${namespace}/`;

  // params: namespace, event_type, pk, start_time, end_time, pageno
  function eventList(params) {
    let endpoint;
    const reqParams = {
      start_time: params.start_time / 1000,
      end_time: params.end_time / 1000,
      pageno: params.pageno,
      query_string: params.query_string,
      size: params.size,
    };

    const event_pk = params.event_pk;
    const event_type = params.event_type;
    const event_types = params.event_types;

    if (!event_types) {
      if (!event_pk) {
        endpoint = URL;
      } else {
        endpoint = URL + event_type + '/' + event_pk;
      }
    } else {
      endpoint = URL;
      if (!event_pk) {
        reqParams.resource_type = event_types;
      } else {
        reqParams.resource_type = event_types;
        reqParams.resource_id = event_pk;
      }
    }

    return rbHttp.sendRequest({
      method: 'GET',
      url: endpoint,
      params: reqParams,
      addNamespace: false,
    });
  }

  return {
    eventList: eventList,
  };
}
