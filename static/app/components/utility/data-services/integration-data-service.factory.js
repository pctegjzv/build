angular
  .module('app.components.utility')
  .factory('rbIntegrationDataService', rbIntegrationDataServiceFactory);

function rbIntegrationDataServiceFactory(rbHttp, rbAccountService) {
  const URLS = {
    CATALOG: '/ajax/integration-catalog',
    INTEGRATIONS: `/ajax/integrations/${rbAccountService.getCurrentNamespace()}/`,
  };

  return { getIntegrationList };

  function getIntegrationList({
    families,
    types,
    project_name,
    page = 1,
    page_size = 20,
  } = {}) {
    return rbHttp
      .sendRequest({
        url: URLS.INTEGRATIONS,
        method: 'GET',
        params: {
          families,
          types,
          project_name,
          page,
          page_size,
        },
      })
      .then(rep => rep.results);
  }
}
