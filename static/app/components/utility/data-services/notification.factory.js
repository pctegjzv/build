/**
 * Created by jiaoguangxin on 2016/04/15.
 */

angular
  .module('app.components.utility')
  .factory('notificationService', notificationService);

function notificationService(rbHttp, rbAccountService) {
  const namespace = rbAccountService.getCurrentNamespace();
  const NOTIF_URL = `/ajax/notifications/${namespace}/`;

  function notificationList() {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: NOTIF_URL,
      })
      .then(({ result }) =>
        result.map(item => {
          item.display_name = item.space_name
            ? `${item.name}(${item.space_name})`
            : item.name;
          return item;
        }),
      );
  }

  return {
    notificationList,
  };
}
