angular.module('app.components.utility').factory('orgService', orgService);

function orgService(rbHttp, rbToast, translateService, rbAccountService) {
  const NAMESPACE = rbAccountService.getCurrentNamespace();

  function updateOrg(name, company) {
    const url = `/ajax/orgs/${name}`;
    return rbHttp.sendRequest({
      method: 'PUT',
      url,
      params: { name: name, company: company },
    });
  }

  function getOrgDetail(orgName) {
    const url = `/ajax/orgs/${orgName}`;
    return rbHttp.sendRequest({
      method: 'GET',
      url,
      params: { org_name: orgName },
    });
  }

  //***  ldap start
  function createOrgLdap({ config, orgName }) {
    return rbHttp
      .sendRequest({
        method: 'POST',
        url: `/ajax/orgs/${orgName}/config/ldap`,
        data: config,
      })
      .then(res => {
        rbToast.success(translateService.get('org_ldap_create_success'));
        return res;
      });
  }

  function getOrgLdap({ cache = false } = {}) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: `/ajax/orgs/${NAMESPACE}/config/ldap`,
      cache,
    });
  }

  function deleteOrgLdap({ orgName }) {
    return rbHttp
      .sendRequest({
        method: 'DELETE',
        url: `/ajax/orgs/${orgName}/config/ldap`,
      })
      .then(res => {
        rbToast.success(translateService.get('org_ldap_delete_success'));
        return res;
      });
  }

  function updateOrgLdap({ config, orgName }) {
    config.org_name = orgName;
    return rbHttp
      .sendRequest({
        method: 'PUT',
        url: `/ajax/orgs/${orgName}/config/ldap`,
        data: config,
      })
      .then(res => {
        rbToast.success(translateService.get('org_ldap_update_success'));
        return res;
      });
  }

  function triggerOrgLdapSync({ orgName }) {
    return rbHttp
      .sendRequest({
        method: 'PUT',
        url: `/ajax/orgs/${orgName}/config/ldap/sync`,
      })
      .then(res => {
        rbToast.success(translateService.get('org_ldap_sync_trigger_success'));
        return res;
      });
  }

  function getOrgLdapSync() {
    return rbHttp.sendRequest({
      method: 'GET',
      url: `/ajax/orgs/${NAMESPACE}/config/ldap/sync`,
    });
  }

  function getOrgLdapInfo() {
    return rbHttp.sendRequest({
      method: 'GET',
      url: `/ajax/orgs/${NAMESPACE}/config/ldap/info`,
    });
  }

  function deleteLdapAccounts(params) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url: `/ajax/orgs/${NAMESPACE}/config/ldap/sync`,
      params,
    });
  }

  // *** ldap end

  function listOrgAccounts({
    page = 1,
    page_size = 20,
    order_by = 'username',
    search = '',
    team_name_filter = '',
    assign = '',
  }) {
    const url = `/ajax/orgs/${NAMESPACE}/accounts/`;
    return rbHttp.sendRequest({
      method: 'GET',
      url,
      params: {
        page,
        page_size,
        order_by,
        search,
        team_name_filter,
        assign,
      },
    });
  }

  function listOrgFilteredAccounts({
    org_name = '',
    page = 1,
    page_size = 20,
    order_by = 'username',
    search = '',
    team_name_filter = '',
    assign = '',
    invalid_ldap,
    action = 'view',
    project_name = '',
  }) {
    const url = '/ajax/org/account/filtered_list';
    return rbHttp.sendRequest({
      method: 'GET',
      url,
      params: {
        org_name,
        page,
        page_size,
        order_by,
        search,
        team_name_filter,
        assign,
        invalid_ldap,
        action,
        project_name,
      },
    });
  }

  function getOrgAccount({ username, cache = false }) {
    const url = `/ajax/orgs/${NAMESPACE}/accounts/${username}`;
    return rbHttp.sendRequest({
      method: 'GET',
      url,
      cache,
    });
  }

  function removeOrgAccount(data) {
    const url = `/ajax/orgs/${NAMESPACE}/accounts/${data.username}`;
    return rbHttp.sendRequest({
      method: 'DELETE',
      url,
      data,
    });
  }

  function getAccountRoles(username) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: `/ajax/orgs/${NAMESPACE}/accounts/${username}/roles/`,
    });
  }

  function addAccountRoles(username, roles) {
    return rbHttp.sendRequest({
      method: 'POST',
      url: `/ajax/orgs/${NAMESPACE}/accounts/${username}/roles/`,
      data: roles,
      addNamespace: false,
    });
  }

  function createRoleBasedAccounts(data) {
    return rbHttp.sendRequest({
      method: 'POST',
      url: `/ajax/orgs/${NAMESPACE}/accounts`,
      data,
    });
  }

  return {
    listOrgAccounts,
    listOrgFilteredAccounts,
    getOrgLdapInfo,
    deleteLdapAccounts,
    getOrgAccount,
    removeOrgAccount,
    updateOrg,
    createOrgLdap,
    updateOrgLdap,
    deleteOrgLdap,
    getOrgLdap,
    triggerOrgLdapSync,
    getOrgLdapSync,
    getOrgDetail,
    getAccountRoles,
    addAccountRoles,
    createRoleBasedAccounts,
  };
}
