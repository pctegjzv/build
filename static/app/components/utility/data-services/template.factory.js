angular
  .module('app.components.utility')
  .factory('templateService', templateService);

function templateService(rbHttp, rbAccountService) {
  const namespace = rbAccountService.getCurrentNamespace();
  const TEMPLATE_ENDPOINT = '/ajax/v1/application-templates/' + namespace;
  const FILEVIEW_ENDPOINT =
    '/ajax-sp/v1/fileview/application-templates/' + namespace;
  let exported = '';

  function templateList() {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: TEMPLATE_ENDPOINT,
        addNamespace: false,
      })
      .then(({ result }) => ({ template_list: result }));
  }

  function templateDetail(templateName) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: FILEVIEW_ENDPOINT + '/' + templateName,
        headers: { 'Read-Files': 'template', 'Files-Type': 'yaml' },
        addNamespace: false,
      })
      .then(res => ({ template: res }));
  }

  function templateServices(templateName) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: TEMPLATE_ENDPOINT + `/${templateName}/services`,
        addNamespace: false,
      })
      .then(({ result }) => result);
  }

  function templateCreate(data) {
    data.files = { template: ['compose.yaml', data.template] };
    delete data.template;
    delete data.namespace;
    return rbHttp
      .sendRequest({
        method: 'POST',
        url: FILEVIEW_ENDPOINT,
        data,
        addNamespace: false,
      })
      .then(res => ({ app_template: res }));
  }

  function templateUpdate(data) {
    const name = data.origin_name;
    data.files = { template: ['compose.yaml', data.template] };
    delete data.name;
    delete data.namespace;
    delete data.template;
    return rbHttp
      .sendRequest({
        method: 'PUT',
        url: FILEVIEW_ENDPOINT + '/' + name,
        data,
        addNamespace: false,
      })
      .then(res => ({ app_template: res }));
  }

  function templateDelete(templateName) {
    return rbHttp.sendRequest({
      method: 'DELETE',
      url: TEMPLATE_ENDPOINT + '/' + templateName,
      addNamespace: false,
    });
  }

  function setExport(yaml) {
    exported = yaml;
  }

  function getExport() {
    return exported;
  }

  return {
    templateList,
    templateServices,
    templateDetail,
    templateCreate,
    templateUpdate,
    templateDelete,
    setExport,
    getExport,
  };
}
