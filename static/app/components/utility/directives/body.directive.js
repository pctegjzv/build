angular;
angular
  .module('app.components.utility')
  // initiate body
  .directive('body', () => {
    return {
      restrict: 'E',
      link: (scope, element) => {
        element.on('click', 'a[href="#"], [data-toggle]', e => {
          e.preventDefault();
        });
      },
    };
  });
