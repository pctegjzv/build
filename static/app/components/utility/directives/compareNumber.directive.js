/**
 * Created by jiaoguangxin on 2016/05/16.
 */
angular
  .module('app.components.utility')
  .directive('rbLowerThan', () => {
    return {
      require: 'ngModel',
      link: ($scope, $element, $attrs, ctrl) => {
        const validate = viewValue => {
          const comparisonModel = $attrs.rbLowerThan;
          if (!viewValue || !comparisonModel) {
            // It's valid because we have nothing to compare against
            ctrl.$setValidity('rbLowerThan', true);
          }
          // It's valid if model is lower than the model we're comparing against
          ctrl.$setValidity(
            'rbLowerThan',
            parseFloat(viewValue) < parseFloat(comparisonModel),
          );
          return viewValue;
        };

        ctrl.$parsers.unshift(validate);
        ctrl.$formatters.push(validate);

        $attrs.$observe('rbLowerThan', () => {
          // Whenever the comparison model changes we'll re-validate
          return validate(ctrl.$viewValue);
        });
      },
    };
  })
  .directive('rbHigherThan', () => {
    return {
      require: 'ngModel',
      link: ($scope, $element, $attrs, ctrl) => {
        const validate = viewValue => {
          const comparisonModel = $attrs.rbHigherThan;
          if (!viewValue || !comparisonModel) {
            // It's valid because we have nothing to compare against
            ctrl.$setValidity('rbHigherThan', true);
          }
          // It's valid if model is lower than the model we're comparing against
          ctrl.$setValidity(
            'rbHigherThan',
            parseFloat(viewValue) > parseFloat(comparisonModel),
          );
          return viewValue;
        };

        ctrl.$parsers.unshift(validate);
        ctrl.$formatters.push(validate);

        $attrs.$observe('rbHigherThan', () => {
          // Whenever the comparison model changes we'll re-validate
          return validate(ctrl.$viewValue);
        });
      },
    };
  });
