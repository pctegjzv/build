const moment = require('moment');

angular.module('app.components.utility').directive('rbDateTimePicker', () => {
  return {
    restrict: 'A',
    scope: {
      timestamp: '=',
      options: '=',
    },
    link: ($scope, $element) => {
      // var enabledDates = [];
      const config = {
        format: 'YYYY-MM-DD HH:mm:ss',
        locale: 'zh-cn',
        sideBySide: true,
      };
      if ($scope.options) {
        angular.extend(config, $scope.options);
      }
      if ($scope.timestamp) {
        config.defaultDate = new Date($scope.timestamp);
      }
      $($element).datetimepicker(config);

      $scope.$watch('timestamp', newValue => {
        $($element)
          .data('DateTimePicker')
          .date(newValue === null ? null : moment(newValue));
      });
      $element.on('dp.change', e => {
        $scope.$evalAsync(() => {
          $scope.timestamp = Math.floor(e.date.valueOf());
        });
      });
    },
  };
});
