angular.module('app.components.utility').directive('rbKeyCode', () => {
  return {
    restrict: 'A',
    link: ($scope, $element, $attrs) => {
      $element.bind('keypress', event => {
        const keyCode = event.which || event.keyCode;
        if (keyCode === parseInt($attrs.keycode)) {
          $scope.$evalAsync(() => {
            $scope.$eval($attrs.rbKeyCode, { $event: event });
          });
        }
      });
    },
  };
});
