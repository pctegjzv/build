import {
  DEPLOYING_STATUS_NAMES,
  RUNNING_STATUS_NAMES,
  ERROR_STATUS_NAMES,
  WAITING_STATUS_NAMES,
  MESSAGE_ERROR_STATUS_NAMES,
} from 'app/components/utility/rb-status-names.constant';

const templateStr = require('app/components/utility/directives/statusIcon.directive.html');
angular.module('app.components.utility').directive('rubickStatusIcon', () => {
  return {
    restrict: 'A',
    replace: true,
    scope: {
      status: '=', //
      tooltipFn: '&',
      mode: '@', //detail list
      statusText: '@', //detail list
    },
    template: templateStr,
    controller: $scope => {
      $scope.statusArray = {
        deploying: false,
        running: false,
        stopped: false,
        draining: false,
      };
      $scope.toolTipMessageKey = '';
      $scope.updateStatus = () => {
        $scope.statusArray.deploying = DEPLOYING_STATUS_NAMES.includes(
          $scope.status_name,
        );
        $scope.statusArray.running = RUNNING_STATUS_NAMES.includes(
          $scope.status_name,
        );
        $scope.statusArray.stopped = ERROR_STATUS_NAMES.includes(
          $scope.status_name,
        );
        $scope.statusArray.waiting = WAITING_STATUS_NAMES.includes(
          $scope.status_name,
        );

        // add tooltip message type for service error, updateerror
        $scope.message_error = MESSAGE_ERROR_STATUS_NAMES.includes(
          $scope.status_name,
        );

        // blue, unstable status, without flag
        $scope.statusArray.deploying_not_flag =
          $scope.statusArray.deploying && ['free'].includes($scope.status_name);
      };

      $scope.$watchGroup(['status', 'statusText'], newValue => {
        if (newValue !== undefined && !!newValue[0]) {
          $scope.status_name = $scope.status.toLowerCase();
          $scope.updateStatus();
          if (!$scope.statusText) {
            $scope.status_text = 'status_' + $scope.status_name;
          } else {
            $scope.status_text = $scope.statusText;
          }
        }
      });
    },
    link: ($scope, $element) => {
      $element.popover({
        content: function _contentFn() {
          return $scope.tooltipFn() || '';
        },
        placement: 'top',
        trigger: 'hover',
        selector: '.badge',
      });
    },
  };
});
