const moment = require('moment');

angular.module('app.components.utility').filter('formatDate', () => {
  return (timestamp, fractional) => {
    if (!timestamp) {
      return '';
    }
    if (fractional) {
      return moment(timestamp).format('YYYY-MM-DD HH:mm:ss.SSS');
    } else {
      return moment(timestamp).format('YYYY-MM-DD HH:mm:ss');
    }
  };
});
