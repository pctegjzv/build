const moment = require('moment');

angular.module('app.components.utility').filter('formatUtcStr', () => {
  return str => {
    if (!str) {
      return 'N/A';
    }
    return moment(moment.utc(str).valueOf()).format('YYYY-MM-DD HH:mm:ss');
  };
});
