angular.module('app.components.utility').filter('limitedString', $filter => {
  return (input, limit, begin) => {
    const value = input;
    input = $filter('limitTo')(input, limit, begin);
    if (value !== input) {
      input = input + '...';
    }
    return input;
  };
});
