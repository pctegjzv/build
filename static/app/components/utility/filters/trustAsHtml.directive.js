angular.module('app.components.utility').filter('trustAsHtml', $sce => {
  return input => {
    return $sce.trustAsHtml(input);
  };
});
