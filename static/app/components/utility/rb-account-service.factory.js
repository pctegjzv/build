angular
  .module('app.components.utility')
  .factory('rbAccountService', rbAccountServiceFactory);

function rbAccountServiceFactory(rbHttp) {
  const ORGS_ACCOUNTS_URL = `/ajax/orgs/${getCurrentNamespace()}/accounts/`;

  return {
    getUserProfile,
    getCurrentNamespace,
    updateNamespaceLogo,
    getCurrentUsername,
    isSubAccount,
    getUserPermission,
    getUserAccountType,
    updateSubAccountPassword,
    updateProfile,
    getUserToken,
  };

  ////////
  function getUserProfile(useCached = true) {
    return rbHttp.sendRequest({
      url: '/ajax/auth/profile',
      method: 'GET',
      cache: useCached,
    });
  }

  function getCurrentNamespace() {
    return $('#cp-namespace')
      .html()
      .trim();
  }

  function getCurrentUsername() {
    return $('#console-username')
      .html()
      .trim();
  }

  function isSubAccount() {
    return !_.isEmpty(getCurrentUsername());
  }

  function getOrgAccount({ username, cache = false }) {
    const url = `/ajax/orgs/${getCurrentNamespace()}/accounts/${username ||
      ''}`;
    return rbHttp.sendRequest({
      method: 'GET',
      url,
      cache,
    });
  }

  /**
   * Returns the permission for the current user
   * Note, only use this function when enabled with sub-account feature
   * @returns {string} none, admin, create
   */
  async function getUserPermission() {
    let permission = 'admin';
    if (isSubAccount()) {
      const member = await getOrgAccount({
        cache: true,
      });
      permission = member.permission;
    }

    return permission;
  }

  /**
   * Return the user account type, only if it is sub-account
   * @returns {string} => organizations.Account if is subaccount, organizations.LDAPAccount if it is LDAP synced
   */
  async function getUserAccountType() {
    let type = 'organizations.Account';
    if (isSubAccount()) {
      const member = await getOrgAccount({
        cache: true,
      });
      type = member.type ? member.type : type;
    }
    return type;
  }

  function updateNamespaceLogo(namespace, file) {
    const params = {
      logo_file: file,
    };
    return rbHttp.sendRequest({
      url: `/ajax/users/${namespace}/logo`,
      method: 'POST',
      data: params,
      useFormData: true,
    });
  }

  function updateSubAccountPassword(data) {
    return rbHttp.sendRequest({
      url: ORGS_ACCOUNTS_URL + data.username,
      method: 'PUT',
      data,
    });
  }

  function updateProfile(data) {
    return rbHttp.sendRequest({
      url: '/ajax/auth/profile',
      method: 'PUT',
      data,
    });
  }

  function getUserToken() {
    return rbHttp.sendRequest({
      url: '/ajax-sp/account/token',
      method: 'GET',
      cache: true,
    });
  }
}
