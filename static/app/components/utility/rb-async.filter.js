angular.module('app.components.utility').filter('rbAsync', rbAsyncFilter);

/**
 * Mimic Angular2's async pipe.
 * Usage:
 *   {{ vm.promiseOrObservable | rbAsync:this }}
 * The `this` parameter refers to the $scope object in the current compiling context.
 * If this is not provided, we won't be able to do unsubscription.
 *
 * Original source:
 * The following lib is a bit outdated that it still depends on rxjs 4, but we are using rxjs 5.
 * - https://github.com/cvuorinen/angular1-async-filter/blob/master/src/async-filter.js
 */
function rbAsyncFilter(rbSafeApply) {
  const values = {};
  const subscriptions = {};
  // Need a way to tell the input objects apart from each other (so we only subscribe to them once)
  let nextObjectID = 0;

  function objectId(obj) {
    if (!obj.hasOwnProperty('__asyncFilterObjectID__')) {
      obj.__asyncFilterObjectID__ = ++nextObjectID;
    }

    return obj.__asyncFilterObjectID__;
  }

  function _async(input, scope) {
    // Make sure we have an Observable or a Promise
    if (!input || !(input.subscribe || input.then)) {
      return input;
    }

    const inputId = objectId(input);
    if (!(inputId in subscriptions)) {
      const subscriptionStrategy =
        (input.subscribe && input.subscribe.bind(input)) ||
        input.then.bind(input);
      subscriptions[inputId] = subscriptionStrategy(value => {
        values[inputId] = value;
        rbSafeApply();
      });

      if (scope && scope.$on) {
        // Clean up subscription and its last value when the scope is destroyed.
        scope.$on('$destroy', () => {
          if (subscriptions[inputId] && subscriptions[inputId].unsubscribe) {
            subscriptions[inputId].unsubscribe();
          }
          delete subscriptions[inputId];
          delete values[inputId];
        });
      }
    }
    return values[inputId];
  }

  // So that Angular does not cache the return value
  _async.$stateful = true;

  return _async;
}
