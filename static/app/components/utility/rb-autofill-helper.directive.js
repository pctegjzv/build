/**
 * Created by liudong on 16/10/19.
 */
angular
  .module('app.components.common')
  .directive('rbAutofillHelper', rbAutofillHelper);

function rbAutofillHelper() {
  return {
    restrict: 'A',
    link,
    scope: false,
    require: 'ngModel',
  };

  function link(scope, element, attrs, ngModel) {
    $(element).bind('input', () => {
      ngModel.$setViewValue($(element).val());
    });
  }
}
