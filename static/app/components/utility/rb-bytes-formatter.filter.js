/**
 * Created by liudong on 2017/2/6.
 */
angular
  .module('app.components.utility')
  .filter('rbBytesFormatter', rbBytesFormatter);

// Filter a size number to pretty format
// Eg, returns '5.54 KB' for 5678
// @params: vague need exact divisible
//      eg: vague === true , 1024M return 1g,1025M return 1g
//      eg: vague === false , 1024M return 1g,1025M return 1025M
function rbBytesFormatter() {
  const units = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];

  return (bytes, precision, base = 1, vague = true) => {
    if (bytes === 0) {
      return bytes;
    }
    if (bytes < 1) {
      return `${bytes} Bytes`;
    }
    bytes = parseFloat(bytes);
    if (isNaN(bytes) || !isFinite(bytes)) {
      return '?';
    }

    let unit = Math.floor(Math.log(bytes * base) / Math.log(1024));
    const minUnit = Math.floor(Math.log(base) / Math.log(1024));

    if (!vague && (bytes * base) % Math.pow(1024, unit) !== 0) {
      unit = Math.max(0, unit - 1, minUnit);
    }
    unit = Math.min(unit, units.length - 1);

    return (
      ((bytes * base) / Math.pow(1024, unit)).toFixed(+precision) +
      ' ' +
      units[unit]
    );
  };
}
