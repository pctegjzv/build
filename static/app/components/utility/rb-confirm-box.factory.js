const templateStr = require('app/components/utility/rb-confirm-box.html');
angular.module('app.components.utility').factory('rbConfirmBox', rbConfirmBox);

function rbConfirmBox($mdDialog, rbSafeApply) {
  return {
    show: show,
  };

  ////////
  function show({ title, textContent, targetEvent, callback } = {}) {
    const parentEl = angular.element(document.body);
    return $mdDialog.show({
      parent: parentEl,
      controller: confirmDialogController,
      controllerAs: 'vm',
      template: templateStr,
      targetEvent: targetEvent,
      clickOutsideToClose: true,
    });

    function confirmDialogController() {
      const vm = this;
      vm.title = title;
      vm.content = textContent || title;
      vm.callback = callback || (() => true);

      vm.hide = async () => {
        vm.loading = true;
        let res;
        try {
          res = await vm.callback();
          $mdDialog.hide(res);
        } catch (e) {
          ///
        }
        vm.loading = false;
        rbSafeApply();
      };
      vm.cancel = () => {
        $mdDialog.cancel();
      };
    }
  }
}
