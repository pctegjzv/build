import { Observable } from 'rxjs';
import { share } from 'rxjs/operators';

/**
 * Wraps a function to an observable.
 * Migrates from https://github.com/onshape/rx.angular.js/blob/rxjs5/src/factory.js
 *
 * It is maybe possible to do this with a decorator?
 *
 * Usage example:
 *   const observable = rbCreateObservableFunction(vm, 'click')
 */
angular
  .module('app.components.utility')
  .factory('rbCreateObservableFunction', rbCreateObservableFunctionFactory);

function rbCreateObservableFunctionFactory() {
  const errorObj = { e: {} };

  function tryCatcherGen(tryCatchTarget) {
    return function tryCatcher() {
      try {
        return tryCatchTarget.apply(this, arguments);
      } catch (e) {
        errorObj.e = e;
        return errorObj;
      }
    };
  }

  function tryCatch(fn) {
    if (!angular.isFunction(fn)) {
      throw new TypeError('fn must be a function');
    }
    return tryCatcherGen(fn);
  }

  return (scope, functionName, listener) => {
    const subscribeCore = subscriber => {
      scope[functionName] = function _fn() {
        const len = arguments.length;
        const args = new Array(len);
        for (let i = 0; i < len; i++) {
          args[i] = arguments[i];
        }

        if (angular.isFunction(listener)) {
          const result = tryCatch(listener).apply(this, args);
          if (result === errorObj) {
            return subscriber.error(result.e);
          }
          subscriber.next(result);
        } else if (args.length === 1) {
          subscriber.next(args[0]);
        } else {
          subscriber.next(args);
        }
      };

      return () => {
        delete scope[functionName];
      };
    };

    return new Observable(subscribeCore).pipe(share());
  };
}
