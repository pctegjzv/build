import parser from 'cron-parser';

angular
  .module('app.components.utility')
  .directive('rbCronValidator', rbCronValidator);

function rbCronValidator() {
  return {
    require: 'ngModel',
    link,
  };
}

const CRON_FORMAT_ERROR = 'cronFormatError';
const CRON_MIN_INTERVAL_ERROR = 'cronMinIntervalError';

function link($scope, $el, attrs, ctrl) {
  // string expression
  const { rbCronValidator } = attrs;
  ctrl.$parsers.push(val => validate(val, $scope, ctrl, rbCronValidator));
  if (!rbCronValidator) {
    return;
  }
  // watch actually value
  $scope.$watch(
    () => $scope.$eval(rbCronValidator),
    expVal => {
      if (expVal) {
        validate($el.val(), $scope, ctrl, rbCronValidator);
      } else {
        ctrl.$setValidity(CRON_FORMAT_ERROR, true);
      }
    },
    true,
  );
}

function validate(val, $scope, ctrl, validator) {
  // if CRON_FORMAT_ERROR is false, ignore CRON_MIN_INTERVAL_ERROR, so set it to true
  if (val && !isFiledCountCorrect(val)) {
    ctrl.$setValidity(CRON_FORMAT_ERROR, false);
    ctrl.$setValidity(CRON_MIN_INTERVAL_ERROR, true);
    return val;
  }
  try {
    const iterator = parser.parseExpression(val);

    let minIntervalValidity = true;

    if (val && validator) {
      const options = $scope.$eval(validator);

      if (options && typeof options === 'object' && options.minInterval) {
        const next1 = iterator.next();
        const next2 = iterator.next();

        // minute
        if ((next2._date - next1._date) / 1000 / 60 < options.minInterval) {
          minIntervalValidity = false;
        }
      }
    }

    ctrl.$setValidity(CRON_FORMAT_ERROR, true);
    ctrl.$setValidity(CRON_MIN_INTERVAL_ERROR, minIntervalValidity);
  } catch (err) {
    ctrl.$setValidity(CRON_FORMAT_ERROR, false);
    ctrl.$setValidity(CRON_MIN_INTERVAL_ERROR, true);
  }
  return val;
}

function isFiledCountCorrect(exp) {
  const spaceCount = exp.split('').filter(val => val === ' ').length;
  const filedCount = exp.split(' ').filter(val => val).length;
  return spaceCount === 4 && filedCount === 5;
}
