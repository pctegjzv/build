import parser from 'cron-parser'; //  https://github.com/harrisiirak/cron-parser
import moment from 'moment';

angular
  .module('app.components.utility')
  .filter('rbCrontabNext', rbCrontabNextFilter);

/*
 * parse crontab expression to next run time
 * */

function rbCrontabNextFilter() {
  const INPUT_FORMAT = 'ddd MMM DD YYYY HH:mm:ss GMTZ';

  function _parseNext(
    rule,
    step = 1,
    options = {
      utc: false,
      format: 'YYYY-MM-DD HH:mm:ss',
      tz: 'Asia/Shanghai',
    },
  ) {
    let dateString = 'N/A';
    if (!rule || !isFiledCountCorrect(rule)) {
      return dateString;
    }
    try {
      const interval = parser.parseExpression(rule, options);
      for (let i = 0; i < step; i++) {
        dateString = interval.next().toString();
      }
      const momentInstance = moment(dateString, INPUT_FORMAT, 'en');
      if (options.utc) {
        dateString =
          momentInstance.utcOffset(0).format(options.format) + ' UTC';
      } else if (options.tz === 'Asia/Shanghai') {
        dateString =
          momentInstance.utcOffset(480).format(options.format) + ' GMT+8';
      } else {
        dateString = momentInstance.format(options.format);
      }
    } catch (err) {
      dateString = 'N/A';
    }
    return dateString;
  }

  return _parseNext;
}

function isFiledCountCorrect(exp) {
  const spaceCount = exp.split('').filter(val => val === ' ').length;
  const filedCount = exp.split(' ').filter(val => val).length;
  return spaceCount === 4 && filedCount === 5;
}
