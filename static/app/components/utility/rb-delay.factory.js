angular.module('app.components.utility').factory('rbDelay', rbDelayFactory);

function rbDelayFactory($timeout) {
  return milliseconds => {
    return new Promise(res => {
      return $timeout(res, milliseconds);
    });
  };
}
