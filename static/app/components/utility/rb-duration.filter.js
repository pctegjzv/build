const moment = require('moment');

angular.module('app.components.utility').filter('rbDuration', rbDurationFilter);

// Filter a moment instance to a duration field
// Eg, returns '1分6秒' for 66000
function rbDurationFilter(translateService) {
  return milliseconds => {
    let message = '';
    if (milliseconds >= 1000) {
      const duration = moment.duration(milliseconds);

      const days = duration.days();
      const hours = duration.hours();
      const minutes = duration.minutes();
      const seconds = duration.seconds();

      message += days ? days + translateService.get('day') : '';
      message += hours ? hours + translateService.get('hour') : '';
      message += minutes ? minutes + translateService.get('minute') : '';
      message += seconds ? seconds + translateService.get('second') : '';
    } else if (milliseconds > 0) {
      message = translateService.get('less_than_a_second');
    } else {
      message = '-';
    }

    return message;
  };
}
