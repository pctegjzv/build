import { BehaviorSubject, defer, merge } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
} from 'rxjs/operators';

angular
  .module('app.components.utility')
  .factory('RbFetchDataStore', rbFetchDataStoreFactory);

/**
 * Utility class for wrapping a fetch request using Observable.
 *
 * TODO:
 * - pagination: currently we do not consider cases where the requested resources have pagination
 * - optimistic update: we should be able to do some optimistic update when update a single resource if it is
 *     contained within a list.
 * @returns {FetchDataStore}
 */
function rbFetchDataStoreFactory() {
  const INIT_VALUE_TOKEN = '__INIT_VALUE_TOKEN__';
  const FETCH_START_TOKEN = '__FETCH_START_TOKEN__';

  const SPECIAL_TOKENS = [INIT_VALUE_TOKEN, FETCH_START_TOKEN];

  /**
   * Simple data store based on fetch request.
   */
  class FetchDataStore {
    constructor(request) {
      this._fetchSubject$ = new BehaviorSubject(INIT_VALUE_TOKEN);
      this._request = request;

      // In case the refetch function is invoked too many times in a queue
      this._refetch = () => {
        this._fetchSubject$.next(FETCH_START_TOKEN);
        return this._request()
          .then(data => {
            // Only cache when fetch success
            this._cached = data;
            this._fetchSubject$.next(data);
            return data;
          })
          .catch(error => {
            const wrappedError = new Error(error);
            this._fetchSubject$.next(wrappedError);
            return wrappedError;
          });
      };

      // The resource data will be fetched upon first subscription
      // Future subscriptions will firstly see the cached result.
      // Whenever refetch is called, new data will be returned.
      this._data$ = merge(
        defer(() => this.refetch()),
        this._fetchSubject$,
      ).pipe(
        map(
          data =>
            data instanceof Error || SPECIAL_TOKENS.includes(data)
              ? null
              : data,
        ),
        map(data => data || this._cached),
        distinctUntilChanged(),
        publishReplay(1),
        refCount(),
      );

      this._empty$ = this._fetchSubject$.asObservable().pipe(
        map(() => !this.snapshot || this.snapshot.length === 0),
        publishReplay(1),
        refCount(),
      );

      this._error$ = this._fetchSubject$.asObservable().pipe(
        map(data => (data instanceof Error ? data : null)),
        publishReplay(1),
        refCount(),
      );

      this._loading$ = this._fetchSubject$.asObservable().pipe(
        map(data => data === FETCH_START_TOKEN && !this.snapshot),
        publishReplay(1),
        refCount(),
      );
    }

    /**
     * Fetch data stream.
     */
    get data$() {
      return this._data$;
    }

    /**
     * Empty states.
     */
    get empty$() {
      return this._empty$;
    }

    /**
     * Errors for each fetch. Will emit null when no error.
     */
    get error$() {
      return this._error$;
    }

    /**
     * Loading state for the store.
     */
    get loading$() {
      return this._loading$;
    }

    get snapshot() {
      return this._cached;
    }

    refetch() {
      return this._refetch();
    }
  }

  return FetchDataStore;
}
