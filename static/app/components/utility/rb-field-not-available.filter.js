angular
  .module('app.components.utility')
  .filter('rbFieldNotAvailable', rbFieldNotAvailable);

function rbFieldNotAvailable() {
  return field => {
    if (field === null || field === undefined || field === '') {
      return '-';
    }
    return field;
  };
}
