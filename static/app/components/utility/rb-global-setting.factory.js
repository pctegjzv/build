angular
  .module('app.components.utility')
  .factory('rbGlobalSetting', rbGlobalSettingFactory);

function rbGlobalSettingFactory() {
  return {
    getAlaudaImageIndex,
    getSetting,
    getEditorOptions,
  };

  function getAlaudaImageIndex() {
    return getSetting('alauda_image_index');
  }

  /**
   * Read global setting.
   * If there is no such setting in the env, the value should be `undefined`;
   * if the setting is not configured, it will be empty.
   */
  function getSetting(settingId) {
    let content = $('#' + settingId.toLowerCase(), '.alauda-settings').html();
    if (content === 'None') {
      content = '';
    }
    return content;
  }

  // mode: yaml, env
  function getEditorOptions(mode, readonly = false) {
    return {
      mode: mode,
      styleActiveLine: true,
      matchBrackets: true,
      lineNumbers: true,
      theme: 'material',
      scrollbarStyle: 'simple',
      viewportMargin: Infinity,
      lineWrapping: true,
      readOnly: readonly,
      extraKeys: {
        // from https://github.com/codemirror/CodeMirror/issues/988
        Tab: cm => {
          if (cm.somethingSelected()) {
            cm.indentSelection('add');
          } else {
            cm.replaceSelection(
              cm.getOption('indentWithTabs')
                ? '\t'
                : Array(cm.getOption('indentUnit') + 1).join(' '),
              'end',
              '+input',
            );
          }
        },
      },
    };
  }
}
