const uuidv1 = require('uuid/v1');

angular.module('app.components.utility').factory('rbHttp', rbHttp);

function rbHttp($http, WEBLABS, rbProjectService) {
  const namespace = $('#cp-namespace')
    .html()
    .trim();

  return {
    sendRequest,
  };

  ////////////
  /**
   *
   * @param method HTTP method (e.g. 'GET', 'POST', etc)
   * @param url Absolute or relative URL of the resource that is being requested.
   * @param params Map of strings or objects which will be serialized with the paramSerializer and appended as GET parameters.
   * @param data Data to be sent as the request message data.
   * @param timeout timeout in milliseconds, or promise that should abort the request when resolved.
   * @param binary Whether or not to expect the response is binary
   * @param cache A boolean value or object created with $cacheFactory to enable or disable caching of the HTTP response.
   *        Typically, use this only if you know this value will not be updated.
   * @param useFormData Indicates that the data passed in should be transformed to FormData
   * @param addNamespace add namespace to params by default
   */
  function sendRequest({
    method = 'GET',
    url,
    data,
    params = {},
    timeout = 1000 * 30,
    binary = false,
    cache = false,
    useFormData = false,
    addNamespace = true,
    headers = {},
  }) {
    // Most rubick ajax calls are subject to namespace
    if (!params.namespace && addNamespace) {
      params.namespace = namespace;
    }

    if (WEBLABS['projects_enabled'] && rbProjectService.get()) {
      params.project_name = rbProjectService.get();
    }

    // combine params & data and assign to data
    if (!(data instanceof FormData) && !(data instanceof Array)) {
      data = Object.assign({}, data, params);
      delete data.project_name;
    }

    if (!headers['Alauda-Request-ID']) {
      const buffer = new Array(32);
      let request_id = '';
      uuidv1(null, buffer, 0).forEach(item => {
        request_id += item.toString(16);
      });
      headers['Alauda-Request-ID'] = request_id;
    }

    const config = {
      method,
      url,
      timeout,
      cache,
      data,
      params,
      xsrfHeaderName: 'X-CSRFToken',
      xsrfCookieName: '294f62ecd0',
      client: 'rbHttp',
      headers,
    };

    if (useFormData) {
      if (!(data instanceof FormData)) {
        config.data = _buildFormData(data);
      }
      config.headers = { 'Content-Type': undefined };
      config.transformRequest = angular.identity;
    }

    if (binary) {
      config.responseType = 'arraybuffer';
    }
    return $http(config).then(response => response.data);
  }

  function _buildFormData(data) {
    const formData = new FormData();
    for (const key in data) {
      const value = data[key];
      if (angular.isArray(value)) {
        for (let i = 0; i < value.length; i++) {
          formData.append(key, value[i]);
        }
      } else {
        formData.append(key, value);
      }
    }
    return formData;
  }
}
