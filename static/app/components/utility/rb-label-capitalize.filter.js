/**
 * Created by wsk on 1/12/17.
 */
angular
  .module('app.components.utility')
  .filter('rbCapitalizeAll', rbCapitalizeAll);

function rbCapitalizeAll() {
  return field => {
    if (!field) {
      return '';
    }
    const tokens = field.split(' ');
    const formatField = tokens
      .map(
        token => token.charAt(0).toUpperCase() + token.slice(1).toLowerCase(),
      )
      .join(' ');
    return formatField;
  };
}
