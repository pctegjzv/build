/**
 * Created by xyhe on 05/08/2017.
 */

angular
  .module('app.components.utility')
  .factory('rbLabelsDataService', rbLabelsDataService);

function rbLabelsDataService(
  rbHttp,
  rbModal,
  translateService,
  rbAccountService,
) {
  /**
   *
   * @param {obj} params
   *  {
   *    namespace: 'namespace',
   *    name: 'source_name',
   *    source_type: 'type'
   *  }
   */
  function getSourceLabels({ namespace, name, source_type }) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: `/ajax/v1/${_typeToUrl(source_type)}/${namespace}/${name}/labels`,
    });
  }

  /**
   *
   * @param {obj} params
   *  {
   *    namespace: 'namespace',
   *    name: 'source_name',
   *    source_type: 'type',
   *    labels: [ { key: 'aaa', value: 'aaaa' }, { key: 'bbb', value: '666' } ]
   *  }
   */
  function updateSourceLabels({
    namespace = '',
    name = '',
    source_type,
    labels,
  }) {
    return rbHttp.sendRequest({
      method: 'PUT',
      addNamespace: false,
      url: `/ajax/v1/${_typeToUrl(source_type)}/${namespace}/${name}/labels`,
      data: labels,
    });
  }

  function getAllLebels() {
    return rbHttp.sendRequest({
      method: 'GET',
      url: `/ajax/labels/${rbAccountService.getCurrentNamespace()}`,
    });
  }

  /**
   *
   * @param {obj} params
   *  {
   *    region_name: 'region_name',
   *    source_type: 'type',
   *    label: 'key:value,key:value23,key2:value2',
   *    name: 'aaa,bbb,ccc'
   *  }
   * this method is useless, stay here for now
   */
  function searchLabels({ region_name, source_type, label = '', name = '' }) {
    const rqData = {
      region_name: region_name,
      region: region_name,
      label: label,
      name: name,
      labels: 1,
    };
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: `/ajax/v1/${_typeToUrl(
          source_type,
        )}/${rbAccountService.getCurrentNamespace()}`,
        data: rqData,
      })
      .then(data => data.results || data.result || []);
  }

  function _typeToUrl(source_type) {
    switch (source_type) {
      case 'service':
        return 'services';
      case 'app':
        return 'applications';
      default:
        return 'services';
    }
  }

  function showLabelsDialog(namespace, sourceType, sourceName) {
    return rbModal
      .show({
        title: translateService.get('labels_update'),
        component: 'rbLabelsDialog',
        locals: {
          namespace,
          sourceType,
          sourceName,
        },
      })
      .then(labels => labels)
      .catch(() => null);
  }

  return {
    getSourceLabels,
    getAllLebels,
    updateSourceLabels,
    searchLabels,
    showLabelsDialog,
  };
}
