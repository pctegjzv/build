const templateStr = require('app/components/utility/rb-markdown-info-dialog.html');
angular
  .module('app.components.utility')
  .factory('rbMarkdownInfoDialog', rbMarkdownInfoDialogFactory);

function rbMarkdownInfoDialogFactory($mdDialog) {
  return {
    show: show,
  };

  ////////
  function show({ title, md, targetEvent } = {}) {
    const parentEl = angular.element(document.body);
    return $mdDialog.show({
      parent: parentEl,
      controller: rbMarkdownInfoDialogController,
      controllerAs: 'vm',
      template: templateStr,
      targetEvent: targetEvent,
      clickOutsideToClose: true,
    });

    function rbMarkdownInfoDialogController() {
      const vm = this;
      vm.title = title;
      vm.md = md;

      vm.hide = async () => {
        $mdDialog.hide();
      };
    }
  }
}
