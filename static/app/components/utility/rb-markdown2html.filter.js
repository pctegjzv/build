const marked = require('marked');

angular.module('app.components.utility').filter('markdown2html', markdown2html);

function markdown2html() {
  return str => {
    return marked(str);
  };
}
