/*
 * 验证数值是否是参数的整数倍
 * 传入参数可以是 number 或 array
 * */

angular
  .module('app.components.utility')
  .directive('rbMultipleValidator', rbMultipleValidator);

function rbMultipleValidator() {
  return {
    require: 'ngModel',
    link,
  };
}

function link(scope, ele, attrs, ctrl) {
  const message = 'multipleValError';
  const arr = [];
  const param = scope.$eval(attrs.rbMultipleValidator);
  if (!param) {
    return;
  } else if (param instanceof Array) {
    arr.push(...param);
  } else if (typeof param === 'number') {
    arr.push(param);
  } else {
    throw new Error('rbMultipleValidator Param Type Error');
  }
  if (arr.length > 0) {
    ctrl.$parsers.push(val => {
      const valid = arr.findIndex(item => val % item === 0) >= 0;
      ctrl.$setValidity(message, valid);
      return val;
    });
  }
}
