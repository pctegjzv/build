angular
  .module('app.components.utility')
  .factory('rbNaturalSort', rbNaturalSortFactory);

function rbNaturalSortFactory() {
  return {
    ipSort,
  };

  function ipSort(a, b) {
    const aa = a.split('.'),
      bb = b.split('.');

    for (let i = 0, n = Math.max(aa.length, bb.length); i < n; i++) {
      if (aa[i] !== bb[i]) {
        return aa[i] - bb[i];
      }
    }

    return 0;
  }
}
