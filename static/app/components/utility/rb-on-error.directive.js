/**
 * Created by wsk on 11/15/16.
 */
angular.module('app.components.common').directive('rbOnError', rbOnError);

function rbOnError() {
  return {
    restrict: 'A',
    scope: false,
    link: (scope, element, attrs) => {
      const onErrorHandler = attrs.rbOnError;
      element.on('error', event => {
        if (event.currentTarget === element.get(0)) {
          if (onErrorHandler) {
            scope.$eval(onErrorHandler);
          }
        }
        event.stopPropagation();
      });
    },
  };
}
