import * as _ from 'lodash';
import JsSha from 'jssha';

angular
  .module('app.components.utility')
  .factory('rbOssClient', rbOssClient)
  .constant('OSS_CLIENT_CODES', {
    ERROR_BUCKET_NOT_EXIST: 'ERROR_OSS_BUCKET_NOT_EXIST',
    ERROR_NO_BUCKET_PERMISSION: 'ERROR_OSS_NO_BUCKET_PERMISSION',
    ERROR_UNKNOWN: 'ERROR_OSS_UNKNOWN',
    ERROR_SERVER_ERROR: 'ERROR_INTERNAL_SERVER_ERROR',
  });

/**
 * Use this service to talk to the OSS server directly.
 */
function rbOssClient(
  $http,
  rbGlobalSetting,
  rbXmlParser,
  rbAccountService,
  ENVIRONMENTS,
  OSS_CLIENT_CODES,
) {
  return {
    checkBucketExistence,
    createBucket,
    listBucketFiles,
    generateLink,
    uploadFile,
    deleteFile,
  };

  /////////
  function ossSendRequest(config) {
    return $http({ client: 'rbOssClient', ...config });
  }

  /**
   * Check existence of a bucket
   * @param bucket
   * @returns {Promise.<*>}
   */
  async function checkBucketExistence({
    bucket = rbAccountService.getCurrentNamespace(),
  } = {}) {
    const ERROR_CODE_MAP = {
      403: OSS_CLIENT_CODES.ERROR_NO_BUCKET_PERMISSION,
      404: OSS_CLIENT_CODES.ERROR_BUCKET_NOT_EXIST,
      502: OSS_CLIENT_CODES.ERROR_SERVER_ERROR,
      503: OSS_CLIENT_CODES.ERROR_SERVER_ERROR,
    };

    const defaultParams = await _ossDefaultParams();

    return ossSendRequest({
      url: `/oss/${bucket}/`,
      method: 'HEAD',
      ...defaultParams,
    })
      .catch(err => {
        const errorCode = ERROR_CODE_MAP[err.status] || 'ERROR_OSS_UNKNOWN';
        if (errorCode === OSS_CLIENT_CODES.ERROR_BUCKET_NOT_EXIST) {
          // We should map 400 to `false` later.
          return { status: 400 };
        } else {
          throw errorCode;
        }
      })
      .then(res => res.status === 200);
  }

  /**
   * Create bucket if not exist
   * @param bucket
   * @returns {Promise.<*>}
   */
  async function createBucket({
    bucket = rbAccountService.getCurrentNamespace(),
  } = {}) {
    const defaultParams = await _ossDefaultParams();

    return ossSendRequest({
      url: `/oss/${bucket}/`,
      method: 'PUT',
      ...defaultParams,
    }).catch(error => {
      return rbXmlParser(error.data).then(({ Error }) => {
        throw Error.Code;
      });
    });
  }

  /**
   * List files of a bucket.
   * TODO: this API may have its result truncated. We may later check this field to
   * TODO: make sure all files are listed
   *
   * returns - [{ key, size }]
   *
   * @param bucket
   * @param prefix file name prefix
   * @returns {Promise.<[{ key, size }]>}
   */
  async function listBucketFiles({
    bucket = rbAccountService.getCurrentNamespace(),
    prefix,
  } = {}) {
    const defaultParams = await _ossDefaultParams();

    const { data } = await ossSendRequest({
      url: `/oss/${bucket}/`,
      method: 'GET',
      params: {
        prefix,
      },
      ...defaultParams,
    });

    const json = await rbXmlParser(data);
    const contents = _.get(json, 'ListBucketResult.Contents', []);

    return contents.map(content => ({
      key: content.Key[0],
      size: content.Size[0],
    }));
  }

  /**
   * Generate a link for the user to upload/download artifacts.
   *
   * @param skipMiddleLayer should send to Wukong directly
   * @param bucket
   * @param path
   * @returns {Promise.<string>}
   */
  async function generateLink({
    skipMiddleLayer = false,
    bucket = rbAccountService.getCurrentNamespace(),
    path,
  }) {
    const domain = skipMiddleLayer
      ? ENVIRONMENTS['oss_server_url']
      : location.origin + '/oss';
    return (
      domain + `/${bucket}/${path}?` + $.param(await generateOssSignature())
    );
  }

  async function _ossDefaultParams() {
    const { token } = await rbAccountService.getUserToken();

    return {
      headers: {
        Authorization: `Token ${token}`,
      },
    };
  }

  async function uploadFile({
    bucket = rbAccountService.getCurrentNamespace(),
    path,
    file,
    uploadEventHandlers,
    timeout,
  }) {
    if (!(await checkBucketExistence({ bucket }))) {
      await createBucket({ bucket });
    }
    const link = await generateLink({
      bucket,
      path: (path ? path + '/' : '') + encodeURIComponent(file.name),
    });
    return ossSendRequest({
      url: link,
      method: 'PUT',
      data: file,
      headers: { 'Content-Type': undefined },
      uploadEventHandlers,
      timeout,
    });
  }

  async function deleteFile({
    bucket = rbAccountService.getCurrentNamespace(),
    path,
  }) {
    const link = await generateLink({ bucket, path });
    return ossSendRequest({
      url: link,
      method: 'DELETE',
    });
  }

  /**
   * There are two options to set Auth OSS client.
   *  - By URL query string
   *  - By feeding Token in the request header
   *
   * This function is to generate an OSS signature for the first case.
   * @param bucket
   * @returns {Promise.<{username, signature, timestamp}>}
   */
  async function generateOssSignature({
    bucket = rbAccountService.getCurrentNamespace(),
  } = {}) {
    const { token } = await rbAccountService.getUserToken();

    const jssha = new JsSha('SHA-256', 'TEXT');
    jssha.setHMACKey(token, 'TEXT');
    const timestamp = Math.floor(new Date().valueOf() / 1000);
    let username = rbAccountService.getCurrentUsername();
    username =
      rbAccountService.getCurrentNamespace() + (username ? '/' + username : '');
    jssha.update(`${username}/${bucket}/${timestamp}`);

    const signature = jssha.getHMAC('HEX');

    return {
      username,
      signature,
      timestamp,
    };
  }
}
