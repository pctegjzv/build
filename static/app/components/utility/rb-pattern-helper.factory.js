/**
 * Created by jiaoguangxin on 2016/11/17.
 */

angular
  .module('app.components.utility')
  .factory('rbPatternHelper', rbPatternHelper);

function rbPatternHelper(translateService, ENVIRONMENTS) {
  return {
    getPasswordPattern,
    getPasswordStrength,
  };

  function getPasswordPattern() {
    const isPrivateDeployEnabled = ENVIRONMENTS['is_private_deploy_enabled'];
    const passwordPatternType = isPrivateDeployEnabled
      ? ENVIRONMENTS['private_deploy_password_pattern_type']
      : '';
    let passwordPatternObj = {};
    switch (passwordPatternType) {
      case 'A':
        passwordPatternObj = {
          min: 8,
          errorMin: translateService.get('warning_min_length', {
            length: 8,
          }),
          max: 64,
          errorMax: translateService.get('warning_max_length', {
            length: 64,
          }),
          pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[`~!@#$%^&?,;:"'])[A-Za-z\d`~!@#$%^&?,;:"']{8,}$/,
          errorPattern: translateService.get('passowrd_pattern_type_a_error'),
          strength: 0,
        };
        break;
      default:
        passwordPatternObj = {
          min: 6,
          errorMin: translateService.get('warning_min_length', {
            length: 6,
          }),
          max: 64,
          errorMax: translateService.get('warning_max_length', {
            length: 64,
          }),
          pattern: '',
          errorPattern: '',
          errorStrength: translateService.get('password_too_weak'),
          strength: 45,
        };
        break;
    }
    return passwordPatternObj;
  }

  function getPasswordStrength(pass) {
    let score = 0;
    if (!pass) {
      return score;
    }
    // award every unique letter until 5 repetitions
    const letters = {};
    for (let i = 0; i < pass.length; i++) {
      letters[pass[i]] = (letters[pass[i]] || 0) + 1;
      score += 5.0 / letters[pass[i]];
    }

    // bonus points for mixing it up
    const variations = {
      digits: /\d/.test(pass),
      lower: /[a-z]/.test(pass),
      upper: /[A-Z]/.test(pass),
      nonWords: /\W/.test(pass),
    };

    let variationCount = 0;
    for (const check in variations) {
      variationCount += variations[check] === true ? 1 : 0;
    }
    score += (variationCount - 1) * 10;
    return +score;
  }
}
