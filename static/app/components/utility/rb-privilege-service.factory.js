/**
 * Created by liudong on 16/9/24.
 */
angular
  .module('app.components.utility')
  .factory('rbPrivilegeService', rbPrivilegeServiceFactory);

function rbPrivilegeServiceFactory() {
  return {
    canRead,
    canWrite,
    canAdmin,
  };

  //////////

  function canRead(resource) {
    return !!resource && ['R', 'W', 'A'].includes(resource.privilege);
  }

  function canWrite(resource) {
    return !!resource && ['A', 'W'].includes(resource.privilege);
  }

  function canAdmin(resource) {
    return !!resource && resource.privilege === 'A';
  }
}
