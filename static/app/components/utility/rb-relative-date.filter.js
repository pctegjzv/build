import moment from 'moment';

angular
  .module('app.components.utility')
  .filter('rbRelativeDate', rbRelativeDateFilter);

// Filter a relative date string to a relative time for display subject to locale
function rbRelativeDateFilter() {
  return dateString => {
    return dateString ? moment.utc(dateString).fromNow() : '';
  };
}
