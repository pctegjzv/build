angular
  .module('app.components.utility')
  .factory('rbRouterStateHelper', rbRouterStateHelperFactory);

function rbRouterStateHelperFactory(
  $transitions,
  $state,
  $stateParams,
  rb2RouterUtil,
) {
  const stateHistory = [];
  let isBackFlag = false;
  // Register previous states
  $transitions.onStart(null, transition => {
    if (isBackFlag) {
      isBackFlag = false;
      return;
    }
    if (transition.from().name) {
      stateHistory.push({
        state: transition.from(),
        params: _.cloneDeep($stateParams),
      });
    }
  });

  return {
    back,
    stateHistory,
  };

  ///////

  /**
   * Go back to the previous state. If there is no such state, go back to the parent state.
   */
  function back() {
    if (stateHistory.length > 0) {
      const previousState = stateHistory.pop();
      rb2RouterUtil.go(previousState.state.name, previousState.params);
      isBackFlag = true;
    } else {
      $state.go('^');
    }
  }
}
