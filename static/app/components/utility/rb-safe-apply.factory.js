angular.module('app.components.utility').factory('rbSafeApply', rbSafeApply);

function rbSafeApply($rootScope) {
  return fn => {
    // http://stackoverflow.com/questions/16602870/decorator-for-scope
    const phase = $rootScope.$$phase;
    if (phase === '$apply' || phase === '$digest') {
      if (fn && typeof fn === 'function') {
        fn();
      }
    } else {
      $rootScope.$apply(fn);
    }
  };
}
