export const DEPLOYING_STATUS_NAMES = [
  'deploying',
  'preparing',
  'pending',
  'starting',
  'updating',
  'deleting',
  'disorderdeploying',
  'creating',
  'stopping',
  'shutting_down',
  'scaling',
  'insufficient_data',
  'insufficient_res',
  'in-progress',
  'free',
  'ongoing',
  'draining',
];

export const RUNNING_STATUS_NAMES = [
  'running',
  'partialrunning',
  'bluegreen',
  'healthy',
  'ok',
  'used',
  'completed',
  'success',
  'schedule',
];

export const ERROR_STATUS_NAMES = [
  'unhealthy',
  'alarm',
  'error',
  'updateerror',
  'createerror',
  'multierror',
  'systemerror',
  'starterror',
  'bluegreenerror',
  'stoperror',
  'scaleerror',
  'deleteerror',
  'failed',
  'critical',
  'unschedule',
];

export const WAITING_STATUS_NAMES = ['waiting', 'blocked'];

export const MESSAGE_ERROR_STATUS_NAMES = ['error', 'updateerror', 'failed'];

export default {
  DEPLOYING_STATUS_NAMES,
  RUNNING_STATUS_NAMES,
  ERROR_STATUS_NAMES,
  WAITING_STATUS_NAMES,
  MESSAGE_ERROR_STATUS_NAMES,
};
