angular
  .module('app.components.utility')
  .factory('rbTencentService', rbTencentServiceFactory);

function rbTencentServiceFactory() {
  return {
    getSsoLogoutUrl,
    getSsoSource,
  };
  function getSsoLogoutUrl() {
    return localStorage['sso_logout_url'];
  }
  function getSsoSource() {
    return localStorage['sso_source'];
  }
}
