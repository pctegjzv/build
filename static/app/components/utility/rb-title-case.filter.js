angular.module('app.components.utility').filter('rbTitleCase', rbTitleCase);

function rbTitleCase() {
  /**
   * Articles, conjunctions, and prepositions less than 5 (not more than 4) letters are
   * changed to lower case unless they are at the beginning or ending of the string.
   *
   * The rules is based on
   * https://developer.apple.com/library/content/documentation/UserExperience/Conceptual/OSXHIGuidelines/TerminologyWording.html
   */

  const smallWords = [
    'a',
    'amid',
    'an',
    'and',
    'am',
    'apud',
    'are',
    'as',
    'at',
    'atop',
    'but',
    'by',
    'down',
    'for',
    'from',
    'in',
    'into',
    'is',
    'lest',
    'like',
    'mid',
    'near',
    'nor',
    'of',
    'off',
    'on',
    'onto',
    'or',
    'out',
    'over',
    'pace',
    'past',
    'per',
    'plus',
    'pro',
    'qua',
    'sans',
    'save',
    'so',
    'than',
    'the',
    'thru',
    'till',
    'to',
    'unto',
    'up',
    'upon',
    'via',
    'vice',
    'with',
    'yet',
  ];

  const terms = ['MySQL', 'DBProxy'];

  function _capitalize(s) {
    return s.charAt(0).toUpperCase() + s.substr(1).toLowerCase();
  }

  return input => {
    if (!input) {
      return '';
    }
    let output = input.toString();

    //  make each word capitalize
    output = output.replace(/([^\W_]+[^\s-]*) */g, _capitalize);

    //  Certain small words should be left lowercase unless they are the first or
    //  last words in the string.
    smallWords.forEach(word => {
      const regexp = new RegExp('\\s' + _capitalize(word) + '\\s', 'g');
      output = output.replace(regexp, s => s.toLowerCase());
    });
    terms.forEach(word => {
      const regexp = new RegExp(word, 'ig');
      output = output.replace(regexp, word);
    });

    return output;
  };
}
