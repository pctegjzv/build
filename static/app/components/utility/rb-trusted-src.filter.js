angular.module('app.components.utility').filter('trustedSrc', trustedSrcFilter);

function trustedSrcFilter($sce) {
  return url => {
    return $sce.trustAsResourceUrl(url);
  };
}
