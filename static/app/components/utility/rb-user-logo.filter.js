angular.module('app.components.utility').filter('userLogo', userLogoFilter);

function userLogoFilter() {
  return logo => {
    if (!logo) {
      return '/static/images/user/default-logo.png';
    } else {
      return logo;
    }
  };
}
