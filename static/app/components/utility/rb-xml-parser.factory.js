import * as xml2js from 'xml2js';

/**
 * A simple factory to parse XML string to JSON data.
 */
angular.module('app.components.utility').factory('rbXmlParser', rbXmlParser);

function rbXmlParser() {
  return data =>
    new Promise(res => xml2js.parseString(data, (err, json) => res(json)));
}
