angular
  .module('app.components.utility')
  .factory('applicationService', applicationService);

function applicationService(rbAccountService, rbHttp) {
  const namespace = rbAccountService.getCurrentNamespace();
  const SERVICE_ENDPOINT = '/ajax/v1/services/' + namespace;
  const SERVICE_ENDPOINT2 = '/ajax/v2/services';

  function applicationList(params) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: SERVICE_ENDPOINT,
        params,
        addNamespace: false,
      })
      .then(res => {
        res.list = res.results;
        delete res.results;
        return res;
      });
  }

  function applicationDetail({ application, is_new, service_name }) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url:
          (is_new ? SERVICE_ENDPOINT2 : SERVICE_ENDPOINT) + `/${service_name}`,
        params: { application },
        addNamespace: false,
      })
      .then(res => ({ detail: res }));
  }

  function applicationUpdate(params) {
    const data = params.image_data;
    if (params.volume_data.length) {
      data.volumes = params.volume_data;
    }
    return rbHttp.sendRequest({
      method: 'PUT',
      url: SERVICE_ENDPOINT + `/${data.service_name}`,
      params: { application: params.application },
      data,
      addNamespace: false,
    });
  }

  function applicationLogs(params) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: SERVICE_ENDPOINT + `/${params.service_name}`,
      params: params.params,
      addNamespace: false,
    });
  }

  function applicationLogsQuery(params) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: SERVICE_ENDPOINT + `/${params.service_name}/logs`,
      params,
      addNamespace: false,
    });
  }

  function applicationLogsAggregations(params) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: SERVICE_ENDPOINT + `/${params.service_name}/logs/aggregations`,
      params,
      addNamespace: false,
    });
  }

  function applicationLogSource(params) {
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: SERVICE_ENDPOINT + `/${params.service_name}/logs/sources`,
        params: params.application ? { application: params.application } : {},
        addNamespace: false,
      })
      .then(res => ({ sources: res.result }));
  }

  function applicationLogContext(params) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: SERVICE_ENDPOINT + `/${params.service_name}/logs/context`,
      params,
      addNamespace: false,
    });
  }

  function getNetworkFeatures(features) {
    const featuresMap = {
      'host-network': {
        display_name: 'HOST',
        value: 'HOST',
      },
    };
    const enabledFeatures = [
      {
        display_name: 'BRIDGE',
        value: 'BRIDGE',
      },
    ];
    _.forEach(features, feature => {
      if (feature in featuresMap) {
        enabledFeatures.push(featuresMap[feature]);
      }
    });
    return enabledFeatures;
  }

  function getLoadBalancerFeatures(features) {
    const enabledFeatures = [];
    const reg = /^\w{1}lb$/;
    const featuresMap = {
      haproxy: {
        display_name: 'HAProxy',
        value: 'HAProxy',
      },
      nginx: {
        display_name: 'Nginx',
        value: 'Nginx',
      },
      'raw-container': {
        display_name: 'Raw Container',
        value: 'RAW',
      },
    };
    _.forEach(features, feature => {
      if (feature in featuresMap) {
        enabledFeatures.push(featuresMap[feature]);
      } else if (reg.test(feature.toLowerCase())) {
        enabledFeatures.push({
          display_name: feature.toUpperCase(),
          value: feature.toUpperCase(),
        });
      }
    });
    enabledFeatures.sort((a, b) => {
      return a.value.charCodeAt(0) - b.value.charCodeAt(0);
    });
    return enabledFeatures;
  }

  function checkServiceName(service_name, space_name = '') {
    const params = {
      service_name,
      space_name,
    };
    return rbHttp.sendRequest({
      method: 'GET',
      url: SERVICE_ENDPOINT + `/${params.service_name}/check`,
      params: { space_name: params.space_name },
      addNamespace: false,
    });
  }

  function loadBalancerHealthInfo(params) {
    return rbHttp.sendRequest({
      method: 'GET',
      url: SERVICE_ENDPOINT + `/${params.service_name}/lb-health-info`,
      params: { application: params.application },
      addNamespace: false,
    });
  }

  function applicationAction(params) {
    // params: {
    //   'action': action,
    //   'pk': {
    //     'service_name': app.service_name,
    //     'namespace': app.namespace,
    //     'application': app.application
    //   }
    const isDel = params.action === 'delete';
    return rbHttp.sendRequest({
      method: isDel ? 'DELETE' : 'PUT',
      url:
        SERVICE_ENDPOINT +
        `/${params.pk.service_name}/` +
        (isDel ? '' : params.action),
      params: { application: params.pk.application },
      addNamespace: false,
    });
  }

  return {
    getNetworkFeatures: getNetworkFeatures,
    getLoadBalancerFeatures: getLoadBalancerFeatures,
    applicationUpdate: applicationUpdate,
    applicationList: applicationList,
    applicationDetail: applicationDetail,
    applicationLogs: applicationLogs,
    applicationLogsQuery: applicationLogsQuery,
    applicationLogsAggregations: applicationLogsAggregations,
    applicationLogSource: applicationLogSource,
    applicationLogContext: applicationLogContext,
    checkServiceName: checkServiceName,
    loadBalancerHealthInfo: loadBalancerHealthInfo,
    applicationAction: applicationAction,
  };
}
