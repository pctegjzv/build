angular
  .module('app.components.utility')
  .factory('errorResponseHandler', errorResponseHandler);
const _ = require('lodash');
function errorResponseHandler(
  errorResponseHelper,
  $injector,
  $window,
  translateService,
  $filter,
) {
  return {
    ajaxGenericOnError: ajaxGenericOnError,
  };

  //////////
  // temp method for compatiable with leagacy code
  function _genericErrorMessage(message) {
    const rbToast = $injector.get('rbToast');
    rbToast.error(message);
  }

  function _genericWarningMessage(message) {
    const rbToast = $injector.get('rbToast');
    rbToast.warning(message);
  }

  async function ajaxGenericOnError(response) {
    const errors = (response.data && response.data.errors) || [];
    let fatalErrorMessage, fieldErrorMessage;
    let fatalErrors = [],
      fieldErrors = [];

    if (_.isEmpty(errors)) {
      // If no error presents, use the http status code instead
      const errorCode = errorResponseHelper.httpCodeMapper(response.status);
      fatalErrorMessage = translateService.get(errorCode);
    } else {
      // ********** fatalErrors ********** //
      // filter out non-fatal errors
      fatalErrors = _.filter(errors, error => {
        return !_.includes(errorResponseHelper.nonFatalErrorCodes, error.code);
      });
      // filter out field errors, field errors should be handled in each biz-controller
      fatalErrors = _.filter(fatalErrors, error => {
        return _.isEmpty(error.fields);
      });
      // filter out duplicated error code
      fatalErrors = _.uniq(fatalErrors, fatalError => {
        return fatalError.code;
      });
      fatalErrorMessage = _.map(fatalErrors, fatalError => {
        // code 'server_error' / 'unknown_issue' is not clear , use error message instead
        if (
          ['unknown_issue', 'server_error', 'invalid_args'].includes(
            fatalError.code,
          )
        ) {
          return _.escape(fatalError.message);
        }
        // TODO: message parse for particular error code
        // code 'partial_permission_denied' used for rbac, parse message instead
        if (fatalError.code === 'partial_permission_denied') {
          const messageFields = JSON.parse(fatalError.message);
          const message = messageFields
            .map(field => {
              return `${field.action} (${field.name})`;
            })
            .join('<br>');
          return translateService.get(fatalError.code) + '<br>' + message;
        }
        // code 'dns_error ' used for set custom domain suffix
        if (fatalError.code === 'dns_error') {
          const messageFields = JSON.parse(fatalError.message);
          const message = messageFields
            .map(field => {
              return `${field.domain} (${field.error})`;
            })
            .join('<br>');
          return translateService.get(fatalError.code) + '<br>' + message;
        }
        // code 'project_resources_exist' used for project delete, parse message instead
        // code 'child_resources_exist' used for resource delete (when sub resources exist)
        if (
          ['project_resources_exist', 'child_resources_exist'].includes(
            fatalError.code,
          )
        ) {
          let messageFields = JSON.parse(fatalError.message);
          messageFields = _.sortBy(messageFields, item => {
            return item.type;
          });
          const message = messageFields
            .map(field => {
              return `${$filter('rbCapitalizeAll')(
                translateService.get(field.type.toLowerCase()),
              )}: ${field.name}`;
            })
            .join('<br>');
          return translateService.get(fatalError.code) + '<br>' + message;
        }
        const translatedCode = translateService.get(fatalError.code);
        // If the code is not translated, use the message instead
        return translatedCode !== fatalError.code
          ? translatedCode
          : _.escape(fatalError.message);
      }).join('<br>');

      // ********** fieldErrors ********** //
      fieldErrors = _.filter(errors, error => {
        return !_.isEmpty(error.fields);
      });

      fieldErrorMessage = _.map(fieldErrors, fieldError => {
        const translatedCode = translateService.get(fieldError.code);

        let message;

        if (typeof fieldError.fields === 'string') {
          message = fieldError.message;
        } else {
          const field = _.get(fieldError.fields, '[0]', {}); // only deals with the first field object
          message = Object.keys(field)
            .map(key => {
              const _message = _.isArray(field[key])
                ? _.get(field[key], '[0]', 'field error')
                : field[key];
              return key + ': ' + _message;
            })
            .join('<br>');
        }

        return (
          (translatedCode !== fieldError.code
            ? translatedCode
            : _.escape(fieldError.message)) +
          '<br>' +
          message
        );
      }).join('<br>');
    }

    // Log out the user when API returns 401
    if (response.status === 401) {
      const rbTencentService = $injector.get('rbTencentService');
      const redirectUrl =
        response.data.redirectUrl ||
        rbTencentService.getSsoLogoutUrl() ||
        '/ap/logout';
      setTimeout(() => ($window.location.href = redirectUrl), 0);
      const message = translateService.get('logging_out');
      _genericErrorMessage(message);
      return;
    } else if (
      response.status === 403 &&
      response.data.errors &&
      response.data.errors[0].code === 'expired'
    ) {
      if (document.getElementById('license-expired-modal-mask')) {
        return;
      }
      const profile = await $injector
        .get('rbHttp')
        .sendRequest({ method: 'GET', url: '/ajax/auth/profile' });

      const rbModal = $injector.get('rbModal');
      rbModal.show({
        title: '',
        width: profile['is_admin'] ? 600 : 420,
        template: `
        <rb-license-tip-modal id="license-expired-modal-mask" isadmin=${
          profile['is_admin']
        }></rb-license-tip-modal>
      `,
      });
      return;
    }

    const errorMessage = fieldErrorMessage || fatalErrorMessage;
    if (errorMessage !== '') {
      // For GET requests, we should not display the messages.
      // Except that the http status code in the blacklist:
      // 403 for permission denied, 502 for bad gateway such as Nginx crashed.
      if (
        !response.config ||
        response.config.method !== 'GET' ||
        [403, 502].includes(response.status)
      ) {
        if (response.status === 409) {
          _genericWarningMessage(errorMessage);
        } else {
          _genericErrorMessage(errorMessage);
        }
      }
    }
  }
}
