const moment = require('moment');

angular
  .module('app.components.utility')
  .factory('LogPollingBase', logPollingBase);

function logPollingBase($interval, rbHttp) {
  function _utcToTimestamp(dateStr) {
    return moment.utc(dateStr).valueOf();
  }

  function _logPollingBase(url) {
    this.url = url;
    this.isPollStopped = false;
    this.hasPendingAjax = false;
    this.logs = null; // cache for last polled logs
    this.lastLogTime = null;
    this.beforeSendCbs = [];
    this.successCbs = [];
    this.errorCbs = [];
    this.finallyCbs = [];
    this.pollingTimer = null;
    this.pollInterval = 10000;
  }

  _logPollingBase.prototype.getPollParams = function getPollParams() {
    return;
  };

  _logPollingBase.prototype.getPollFlag = function getPollFlag() {
    return 1; // 0 - cannot poll, 1 - always poll, 2 - one more poll
  };

  _logPollingBase.prototype.pollLogs = function pollLogs() {
    const self = this;
    if (self.hasPendingAjax) {
      return;
    }
    this.beforeSend();
    return rbHttp
      .sendRequest({
        method: 'GET',
        url: this.url,
        params: this.getPollParams(),
      })
      .then(
        data => {
          self.success(data);
        },
        () => {
          self.error();
        },
      )
      .finally(() => {
        self.finally();
      });
  };

  _logPollingBase.prototype.destroy = function destroy() {
    $interval.cancel(this.pollingTimer);
    this.pollingTimer = null;
  };

  _logPollingBase.prototype.stop = function stop() {
    this.isPollStopped = true;
  };

  _logPollingBase.prototype.start = function start() {
    const self = this;
    // poll for the first time
    if (this.pollingTimer) {
      return; // already start
    }
    this.logs = [];
    this.isPollStopped = false;
    this.poll();
    this.pollingTimer = $interval(() => {
      self.poll();
    }, this.pollInterval);
  };

  _logPollingBase.prototype.poll = function poll() {
    if (this.isPollStopped) {
      return;
    }
    const flag = this.getPollFlag();
    if (flag > 0) {
      this.pollLogs();
    }
    if (flag === 2) {
      this.isPollStopped = true;
    }
  };

  _logPollingBase.prototype.getQueryTimes = function getQueryTimes(
    created_at,
    ended_at,
  ) {
    let start_time;
    let end_time;
    const now = moment().valueOf();

    if (this.lastLogTime) {
      start_time = (this.lastLogTime * 1000 * 1000 + 1) / 1000; // convert to ms
    } else if (created_at) {
      start_time = _utcToTimestamp(created_at) - 1800 * 1000; // the time gap among different hosts may be minutes or seconds
    } else {
      start_time = ((now - 3600 * 24 * 7 * 1000) / 1000) * 1000;
    }

    if (ended_at) {
      end_time = _utcToTimestamp(ended_at) + 3600 * 1000;
    } else if (created_at) {
      end_time = start_time + 3600 * 24 * 1000; // make the scope of first query big enough
    } else {
      end_time = now;
    }

    return { start_time: start_time, end_time: end_time };
  };

  _logPollingBase.prototype.beforeSend = function beforeSend() {
    const self = this;
    this.hasPendingAjax = true;
    _.each(this.beforeSendCbs, fn => {
      _.bind(fn, self)();
    });
  };

  _logPollingBase.prototype.error = function error() {
    const self = this;
    this.hasPendingAjax = false;
    _.each(this.errorCbs, fn => {
      _.bind(fn, self)();
    });
  };

  _logPollingBase.prototype.finally = function _finally() {
    const self = this;
    this.hasPendingAjax = false;
    _.each(this.finallyCbs, fn => {
      _.bind(fn, self)();
    });
  };

  _logPollingBase.prototype.success = function success(data) {
    const self = this;
    const _logs = data.result || data.logs;
    const logs = [];
    this.hasPendingAjax = false;
    for (let i = 0; _logs && i < _logs.length; i++) {
      if (typeof _logs[i] === 'string') {
        logs.push({ message: _logs[i] });
      } else {
        logs.push(_logs[i]);
      }
    }
    // Indicates there are new logs for this poll
    this.newLogsFlag = logs.length > 0;
    if (this.newLogsFlag) {
      this.logs = this.logs.concat(logs);
      this.lastLogTime = logs[logs.length - 1].time;
    }
    _.each(this.successCbs, fn => {
      _.bind(fn, self)(logs);
    });
  };

  _logPollingBase.prototype.registerSuccessCb = function registerSuccessCb(fn) {
    this.successCbs.push(fn);
  };

  _logPollingBase.prototype.registerErrorCb = function registerErrorCb(fn) {
    this.errorCbs.push(fn);
  };

  _logPollingBase.prototype.registerBeforeSendCb = function registerBeforeSendCb(
    fn,
  ) {
    this.beforeSendCbs.push(fn);
  };

  _logPollingBase.prototype.registerFinallyCb = function registerFinallyCb(fn) {
    this.finallyCbs.push(fn);
  };

  _logPollingBase.prototype.clearLogs = function clearLogs() {
    this.logs = [];
  };

  return _logPollingBase;
}
