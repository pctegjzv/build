angular
  .module('app.components.utility')
  .factory('parserService', parserService);

function parserService() {
  function loadEnvfile(envfile, format) {
    format = format !== undefined && format !== null ? format : 'dict';

    function EnvfileLoadError(line, message) {
      message =
        message !== undefined && message !== null
          ? message
          : 'envfile load failed';

      this.line = line;
      this.message = message;
    }

    if (['dict', 'list'].indexOf(format) < 0) {
      throw new EnvfileLoadError(
        -1,
        'format ' + format + ' was not supported!',
      );
    }

    const envvarDict = {};
    const envvarList = [];
    if (!envfile) {
      return envvarDict;
    }
    const envvars = envfile.split('\n');
    for (let i = 0; i < envvars.length; i++) {
      const envvar = envvars[i];
      //envvar = envvar.replace(/ /g, '');
      if (envvar === '') {
        continue;
      }

      if (!envvar.includes('=')) {
        throw new EnvfileLoadError(i + 1);
      }

      // Split key / value
      // 'abc=edf'    => ['abc', 'edf']
      // 'abc='       => ['abc', '']
      // 'abc=edf=gh' => ['abc', 'edf=gh']
      const eqIndex = envvar.indexOf('=');
      const key = envvar.substring(0, eqIndex);
      const value = envvar.substring(eqIndex + 1, envvar.length);

      envvarDict[key] = value;
      envvarList.push([key, value]);
    }

    if (format === 'dict') {
      return envvarDict;
    } else {
      return envvarList;
    }
  }

  return {
    loadEnvfile: loadEnvfile,
  };
}
