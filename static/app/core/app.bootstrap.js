angular.module('app.config', []);

export function registerConfig(token, value) {
  angular.module('app.config').constant(token, value);
}
