/**
 * Modules to be shared across different modules
 */
import { ANGULAR_DOWNGRADE_MODULE_NAME } from '../app.constant';

angular
  .module('app.core', [
    'ui.bootstrap',
    'ui.codemirror',
    'ui.router',
    'ui.validate',
    'ui.router',
    'ngSanitize',
    'ngAnimate',
    'ngMessages',
    'ngFileSaver',
    'ngMaterial',
    'angular-bind-html-compile',
    'rzModule',
    'ngTagsInput',
    // Config module containing constants
    'app.config',
    // Submodules under core
    'app.core.translation',

    ANGULAR_DOWNGRADE_MODULE_NAME,
  ])
  .config(
    (
      $compileProvider,
      $httpProvider,
      $animateProvider,
      $uibTooltipProvider,
    ) => {
      $compileProvider.preAssignBindingsEnabled(true);

      // animations provided by ng-animate will only be performed on elements that successfully match the filter expression 'rb-animate'
      // add class 'rb-animate' to element to enable animation provided by ng-animate
      $animateProvider.classNameFilter(/rb-animate/);

      $httpProvider.interceptors.push(rubickInterceptor);

      // To fix uib-tooltip blinking issues globally
      $uibTooltipProvider.options({
        popupCloseDelay: 100,
      });

      function rubickInterceptor(errorResponseHandler) {
        return {
          request: config => {
            config.headers['RUBICK-AJAX-REQUEST'] = true;
            config.headers['X-REQUESTED-WITH'] = 'XMLHttpRequest';
            return config;
          },

          responseError: response => {
            response.config &&
              response.config.client === 'rbHttp' &&
              errorResponseHandler.ajaxGenericOnError(response);
            return Promise.reject(response);
          },
        };
      }
    },
  );
