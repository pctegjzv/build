const platform = require('platform');

$(() => {
  function browserChecker() {
    const browserMinVersions = {
      Chrome: 43,
      Safari: 9,
      Firefox: 46,
      'Microsoft Edge': 0,
    };

    if (
      !(platform.name in browserMinVersions) ||
      getMajorVersionNumber(browserMinVersions[platform.name]) >
        getMajorVersionNumber(platform.version)
    ) {
      let innerText = '';
      innerText +=
        '我们控制台暂不支持您的浏览器版本 ' +
        platform.name +
        ' ' +
        platform.version +
        ', ';
      innerText += '建议使用最新 Chrome 浏览器。';
      showTopMessage(innerText);
    }
  }

  function getMajorVersionNumber(version) {
    const majorVersion =
      typeof version === 'string' ? version.split('.')[0] : version;
    return parseInt(majorVersion);
  }

  function showTopMessage(htmlString) {
    const stickyNote = document.createElement('div');
    stickyNote.style.backgroundColor = '#fbf1ac';
    stickyNote.style.textAlign = 'center';
    stickyNote.style.fontSize = '14px';
    stickyNote.style.padding = '5px';
    stickyNote.style.cursor = 'pointer';
    stickyNote.style.position = 'relative';
    stickyNote.style.zIndex = '3';
    stickyNote.innerHTML = htmlString;
    stickyNote.style.flexShrink = '0';
    $('body').prepend(stickyNote);

    stickyNote.onclick = () => {
      stickyNote.remove();
    };
  }

  browserChecker();
});
