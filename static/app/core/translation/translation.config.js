import { setGlobal } from 'app2/app-global.js';

angular.module('app.core.translation').config(config);
// Docs:
// https://angular-translate.github.io/docs/#/guide/12_asynchronous-loading
function config($translateProvider, TRANSLATIONS) {
  // TRANSLATIONS should be preloaded through ajax call in angular bootstrap stage
  const defaultLanguage = TRANSLATIONS.getLocale();

  $translateProvider.useSanitizeValueStrategy('escapeParameters');

  $translateProvider.translations(defaultLanguage, TRANSLATIONS.translations());

  $translateProvider.preferredLanguage(defaultLanguage);

  // Set fallback language in case there are missing keys
  $translateProvider.fallbackLanguage(defaultLanguage);

  // Set stateful filter to false so they are not executed
  // every digest cycle
  $translateProvider.statefulFilter(true);

  initGlobalTranslateProvider($translateProvider);
}

function initGlobalTranslateProvider($translateProvider) {
  setGlobal('$translateProvider', $translateProvider);
}
