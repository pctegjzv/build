// // https://angular-translate.github.io/docs/#/guide/02_getting-started
import { setGlobal } from 'app2/app-global.js';

angular
  .module('app.core.translation', ['pascalprecht.translate', 'app.config'])
  .run(initGlobalTranslate);

function initGlobalTranslate($translate) {
  setGlobal('$translate', $translate);
}
