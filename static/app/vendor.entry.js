import 'jquery-ui';
import 'jquery-form';
import 'jquery-validation';

/* The basic File Upload plugin required*/
import 'imports-loader?define=>false&exports=>false!blueimp-file-upload/js/jquery.iframe-transport.js';
/* The basic File Upload plugin */
import 'imports-loader?define=>false&exports=>false!blueimp-file-upload/js/jquery.fileupload.js';
/* The File Upload processing plugin */
import 'imports-loader?define=>false&exports=>false!blueimp-file-upload/js/jquery.fileupload-process.js';
/* The File Upload validation plugin */
import 'imports-loader?define=>false&exports=>false!blueimp-file-upload/js/jquery.fileupload-validate.js';

// semantic ui components
import 'semantic-ui-api/api.min.js';
import 'semantic-ui-transition/transition.min.js';
import 'semantic-ui-dropdown/dropdown.min.js';
import 'semantic-ui-search/search.min.js';
import 'semantic-ui-checkbox/checkbox.min.js';
import 'semantic-ui-popup/popup.min.js';
import 'semantic-ui-checkbox/checkbox.min.js';
import 'semantic-ui-dimmer/dimmer.min.js';
import 'semantic-ui-modal/modal.min.js';
// We may not need the js
// import 'semantic-ui-accordion/accordion.min.js';

import 'bootstrap';
import 'bootstrap-markdown/js/bootstrap-markdown.js';
import 'eonasdan-bootstrap-datetimepicker-npm';
import 'expose-loader?CodeMirror!codemirror';
import 'codemirror';
import 'codemirror/mode/yaml/yaml';
import 'codemirror/mode/shell/shell';
import 'codemirror/mode/groovy/groovy';
import 'codemirror/addon/scroll/simplescrollbars';
import 'codemirror/addon/display/autorefresh';
import 'angular';
import '@uirouter/angularjs';
import 'angular-animate';
import 'angular-aria';
import 'angular-messages';
import 'angular-sanitize';
import 'angular-ui-bootstrap';
import 'angular-ui-validate';
import 'angular-bind-html-compile-ci-dev';
import 'angular-file-saver';
import 'angularjs-slider';
import 'angular-ui-codemirror';
import 'angular-translate';

// require angular material modules
import 'angular-material';

// Only require zh-cn for the moment
import 'moment/locale/zh-cn';

// api: http://mbenford.github.io/ngTagsInput/documentation/api
// custom style: app/components/common/style/ng-tags-input.style.scss
import 'ng-tags-input';
