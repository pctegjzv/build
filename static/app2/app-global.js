if (!window['rubick']) {
  window['rubick'] = new Map();
}

export function setGlobal(key, value) {
  window['rubick'].set(key, value);
}

export function getGlobal(key) {
  return window['rubick'].get(key);
}
