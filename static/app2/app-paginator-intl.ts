import { Injectable } from '@angular/core';
import { PaginatorIntl } from 'alauda-ui';

@Injectable()
export class AppPaginatorIntl extends PaginatorIntl {
  itemsPerPageLabel = '条 / 每页';
  nextPageLabel = '下一页';
  previousPageLabel = '上一页';
  firstPageLabel = '首页';
  lastPageLabel = '尾页';

  getTotalLabel = (length: number) => `共${length}条`;
}
