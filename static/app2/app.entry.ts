import { ApplicationRef, enableProdMode } from '@angular/core';
import { enableDebugTools } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

// FIXME: there are still somes codes related, they should be removed in future
import 'expose-loader?$!expose-loader?jQuery!jquery';
import 'semantic-ui-dimmer/dimmer.min.js';
import 'semantic-ui-dropdown/dropdown.min.js';
import 'semantic-ui-modal/modal.min.js';
import 'semantic-ui-transition/transition.min.js';

import 'font-awesome/css/font-awesome.min.css';
import 'semantic-ui-button/button.min.css';
import 'semantic-ui-dimmer/dimmer.min.css';
import 'semantic-ui-dropdown/dropdown.min.css';
import 'semantic-ui-icon/icon.min.css';
import 'semantic-ui-modal/modal.min.css';
import 'semantic-ui-transition/transition.min.css';

import {
  ajax,
  initAccount,
  initEnvironments,
  initI18nManifest,
  initTranslations,
  initWeblabs,
} from 'app2/utils/bootstrap';

import { setGlobal } from './app-global';
import { AppModule } from './app.module';
import { DEFAULT_ADMIN_NAV_CONFIG } from './core/nav/default-admin-nav-config';
import { DEFAULT_NAV_CONFIG } from './core/nav/default-nav-config';
import {
  ACCOUNT,
  ENVIRONMENTS,
  EXAMPLE_YAML,
  I18NMANIFEST,
  INSTANCE_SIZE_CONFIG,
  NAV_CONFIG,
  TENCENTSSO,
  TRANSLATIONS,
  WEBLABS,
} from './core/tokens';
import { Weblabs } from './core/types';

// Custom assets dir
const customAssetsDir = '/static/assets/custom';
const customNavConfigName = 'nav-config.json';
const customCssName = 'custom.css';
const customExampleYamlName = 'sample-app.yaml';
const customInstanceSizeConfigName = 'instance-size.json';

const initNavConfig = (
  privateDeployEnabled: boolean,
  weblabs: Weblabs = {},
) => {
  if (privateDeployEnabled) {
    return ajax(
      `${customAssetsDir}/${customNavConfigName}?_=${Date.now()}`,
    ).catch(
      () =>
        weblabs.USER_VIEW_ENABLED
          ? DEFAULT_ADMIN_NAV_CONFIG
          : DEFAULT_NAV_CONFIG,
    );
  } else {
    return Promise.resolve(
      weblabs.USER_VIEW_ENABLED ? DEFAULT_ADMIN_NAV_CONFIG : DEFAULT_NAV_CONFIG,
    );
  }
};

const initCustomExampleYaml = (privateDeployEnabled: boolean) =>
  privateDeployEnabled &&
  ajax(`${customAssetsDir}/${customExampleYamlName}`, false).catch(() => null);

const initInstanceSizeConfig = (privateDeployEnabled: boolean) =>
  privateDeployEnabled &&
  ajax(`${customAssetsDir}/${customInstanceSizeConfigName}`).catch(() => null);

async function main() {
  const account = initAccount();

  const [weblabs, environments, i18nManifest] = await Promise.all([
    initWeblabs(account.namespace),
    initEnvironments(),
    initI18nManifest(),
  ]);

  //must before initTranslations
  setGlobal(I18NMANIFEST, i18nManifest);

  const [
    navConfig,
    translations,
    customExampleYaml,
    instanceSizeConfig,
  ] = await Promise.all([
    initNavConfig(environments.is_private_deploy_enabled, weblabs),
    initTranslations(environments.is_private_deploy_enabled),
    initCustomExampleYaml(environments.is_private_deploy_enabled),
    initInstanceSizeConfig(environments.is_private_deploy_enabled),
  ]);

  // Enable custom css:
  if (environments.is_private_deploy_enabled) {
    const elLink = document.createElement('link');
    elLink.href = `${customAssetsDir}/${customCssName}`;
    elLink.rel = 'stylesheet';
    document.head.appendChild(elLink);
  }

  setGlobal(ACCOUNT, account);
  setGlobal(TENCENTSSO, {
    ssoSource: localStorage.getItem('sso_source'),
    ssoLogoutUrl: localStorage.getItem('sso_logout_url'),
  });
  setGlobal(WEBLABS, weblabs);
  setGlobal(TRANSLATIONS, translations);
  setGlobal(ENVIRONMENTS, environments);
  setGlobal(NAV_CONFIG, navConfig);
  setGlobal(EXAMPLE_YAML, customExampleYaml);
  setGlobal(INSTANCE_SIZE_CONFIG, instanceSizeConfig);

  if (!environments.debug) {
    enableProdMode();
  } else {
    Error.stackTraceLimit = Infinity;
    require('zone.js/dist/long-stack-trace-zone');
  }

  const platformRef = await platformBrowserDynamic().bootstrapModule(AppModule);

  if (environments.debug) {
    const appRef = platformRef.injector.get(ApplicationRef);
    const appComponent = appRef.components[0];
    enableDebugTools(appComponent);
  }
}

main();
