import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {
  CodeEditorModule,
  IconModule,
  IconRegistryService,
  MessageModule,
  NOTIFICATION_CONFIG,
  NotificationModule,
  PaginatorIntl,
} from 'alauda-ui';
import { AppPaginatorIntl } from 'app2/app-paginator-intl';
import { ServicesModule } from 'app2/shared/services/services.module';
import { AccountEffects } from 'app2/state-container/core/effects/account';
import { ProjectEffects } from 'app2/state-container/core/effects/project';
import { metaReducers, reducers } from 'app2/state-container/core/reducers';
import { effects as storeEffects } from 'app2/state-store/effects';
import { reducers as storeReducers } from 'app2/state-store/reducers';
import { GlobalTranslateModule } from 'app2/translate/translate.module';
import { TranslateService } from 'app2/translate/translate.service';

import { getGlobal } from './app-global';
import { AppRoutingModule } from './app.routing.module';
import { CoreModule } from './core/core.module';
import {
  ACCOUNT,
  ENVIRONMENTS,
  NAV_CONFIG,
  TENCENTSSO,
  WEBLABS,
} from './core/tokens';
import { LayoutComponent } from './layout/layout.component';
import { LayoutModule } from './layout/layout.module';

const basicIconsUrl = require('alauda-ui/assets/basic-icons.svg');
const DEFAULT_MONACO_OPTIONS = {
  fontSize: 12,
  folding: true,
  scrollBeyondLastLine: false,
  minimap: { enabled: false },
  find: { seedSearchStringFromSelection: false, autoFindInSelection: false },
  mouseWheelZoom: true,
  scrollbar: {
    vertical: 'visible',
    horizontal: 'visible',
  },
  fixedOverflowWidgets: true,
};

/**
 * 应用的根模块
 *
 * 坚持在应用的根目录创建一个 Angular 模块
 */
@NgModule({
  // Imports:
  //  Specifies a list of modules whose exported directives/pipes
  //  should be available to templates in this module.
  imports: [
    BrowserModule,
    CoreModule,
    BrowserAnimationsModule,
    GlobalTranslateModule,
    LayoutModule,
    ServicesModule,
    NotificationModule,
    MessageModule,
    IconModule,
    // App routing module should stay at the bottom
    AppRoutingModule,
    StoreModule.forRoot(Object.assign({}, reducers, storeReducers), {
      metaReducers,
    }),
    process.env.NODE_ENV === 'development'
      ? StoreDevtoolsModule.instrument({ maxAge: 25 })
      : [],
    EffectsModule.forRoot([AccountEffects, ProjectEffects, ...storeEffects]),
    CodeEditorModule.forRoot({
      baseUrl: '/static/assets/monaco_lib',
      defaultOptions: DEFAULT_MONACO_OPTIONS,
    }),
  ],

  // Defines the components that should be bootstrapped when
  // this module is bootstrapped. The components listed here
  bootstrap: [LayoutComponent],

  providers: [
    // Since WEBLABS and NAV_CONFIGS are constructed during bootstrap,
    // we inject in the root app instead.
    { useFactory: () => getGlobal(WEBLABS), provide: WEBLABS },
    { useFactory: () => getGlobal(ENVIRONMENTS), provide: ENVIRONMENTS },
    { useFactory: () => getGlobal(ACCOUNT), provide: ACCOUNT },
    { useFactory: () => getGlobal(NAV_CONFIG), provide: NAV_CONFIG },
    { useFactory: () => getGlobal(TENCENTSSO), provide: TENCENTSSO },
    {
      provide: PaginatorIntl,
      useFactory: (translate: TranslateService) =>
        translate.currentLang === 'en'
          ? new PaginatorIntl()
          : new AppPaginatorIntl(),
      deps: [TranslateService],
    },
    {
      provide: NOTIFICATION_CONFIG,
      useFactory: () => ({
        duration: getGlobal(ENVIRONMENTS).notification_duration,
        maxStack: getGlobal(ENVIRONMENTS).notification_max_stack,
      }),
    },
  ],
})
export class AppModule {
  constructor(iconRegistryService: IconRegistryService) {
    iconRegistryService.registrySvgSymbolsByUrl(basicIconsUrl);
  }
}
