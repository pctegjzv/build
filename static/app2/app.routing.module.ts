import { Component, Inject, NgModule } from '@angular/core';
import { Router, RouterModule, Routes } from '@angular/router';

import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';

import { HomeComponent } from './core/home.component';
import { DelayPreloadingStrategy } from './delay-preloading-strategy';

@Component({
  template: '',
})
class EmptyComponent {}

/**
 * Define base level App base routes.
 */
export const appBaseRoutes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'empty-route',
    component: EmptyComponent,
  },
  // Lazy loading feature modules:
  {
    path: 'overview',
    loadChildren: './features/dashboard/dashboard.module#DashboardModule',
  },
  {
    path: 'k8s_app',
    loadChildren: './features/application/application.module#ApplicationModule',
  },
  {
    path: 'k8s_pod',
    loadChildren: './features/pod/pod.module#K8sPodModule',
  },
  {
    path: 'resource_management',
    loadChildren: './features/rsrc-manage/module#RSRCManagementModule',
  },
  {
    path: 'app-catalog',
    loadChildren: './features/app-catalog/app-catalog.module#AppCatalogModule',
  },
  {
    path: 'app-platform',
    loadChildren:
      './features/app-platform/app-platform.module#AppPlatformModule',
  },
  {
    path: 'image',
    loadChildren: './features/image/image.module#ImageModule',
  },
  {
    path: 'event',
    loadChildren: './features/event/event.module#EventModule',
  },
  {
    path: 'log',
    loadChildren: './features/log/log.module#LogModule',
  },
  {
    path: 'notification',
    loadChildren:
      './features/notification/notification.module#NotificationModule',
  },
  {
    path: 'k8s_configmap',
    loadChildren:
      './features/configmap/k8s-configmap.module#K8sConfigMapModule',
  },
  {
    path: 'subnet',
    loadChildren: './features/network/subnet.module#SubnetModule',
  },
  {
    path: 'certificate',
    loadChildren: './features/network/certificate.module#CertificateModule',
  },
  {
    path: 'network_policy',
    loadChildren:
      './features/network/network-policy.module#NetworkPolicyModule',
  },
  {
    path: 'load_balancer',
    loadChildren: './features/network/loadbalancer.module#LoadbalancerModule',
  },
  {
    path: 'quota',
    loadChildren: './features/resource/quota.module#QuotaModule',
  },
  {
    path: 'service_usage',
    loadChildren: './features/resource/service-usage.module#ServiceUsageModule',
  },
  {
    path: 'plugin',
    loadChildren: './features/plugin-center/plugin.module#PluginModule',
  },
  {
    path: 'cluster',
    loadChildren: './features/cluster/cluster.module#ClusterModule',
  },
  {
    path: 'rbac',
    loadChildren: './features/rbac/rbac.module#RBACModule',
  },
  {
    path: 'service_catalog',
    loadChildren:
      './features/service-catalog/service-catalog.module#ServiceCatalogModule',
  },
  {
    path: 'role-template',
    loadChildren: './features/rbac/role-template.module#RoleTemplateModule',
  },
  {
    path: 'storage',
    loadChildren: './features/storage/storage.module#StorageModule',
  },
  {
    path: 'alarm',
    loadChildren: './features/alarm/alarm.module#AlarmModule',
  },
  {
    path: 'jenkins',
    loadChildren: './features/jenkins/jenkins.module#JenkinsModule',
  },
  {
    path: 'user',
    loadChildren: './features/user/user.module#UserModule',
  },
  {
    path: 'integration_center',
    loadChildren:
      './features/integration-center/integration-center.module#IntegrationCenterModule',
  },
  {
    path: 'license',
    loadChildren: './features/license/license.module#LicenseModule',
  },
  {
    path: 'projects',
    loadChildren: './features/projects/projects.module#ProjectsModule',
  },
  {
    path: 'domain',
    loadChildren: './features/domain/domain.module#DomainModule',
  },
];

// Refer to https://angular.io/docs/ts/latest/guide/router.html
// see why we may want to use a module to define
@NgModule({
  imports: [
    RouterModule.forRoot(appBaseRoutes, {
      preloadingStrategy: DelayPreloadingStrategy,
    }),
  ],
  exports: [RouterModule],
  declarations: [EmptyComponent],
  providers: [DelayPreloadingStrategy],
})
export class AppRoutingModule {
  constructor(
    private router: Router,
    @Inject(WEBLABS) private weblabs: Weblabs,
  ) {
    this.router.config.push(
      this.weblabs.LEGACY_ANGULAR_JS_ENABLED && !this.weblabs.USER_VIEW_ENABLED
        ? // unrecognized routes should be considered as legacy Angular.js route
          {
            path: '**',
            loadChildren:
              './features/downgrade/downgrade.module#DowngradeModule',
          }
        : {
            path: '**',
            redirectTo: 'home',
          },
    );
  }
}
