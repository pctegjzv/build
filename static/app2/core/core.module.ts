import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TranslateService } from '../translate/translate.service';

import { VendorCustomerService } from './customization/vendor-customer.service';
import { HomeComponent } from './home.component';
import { RouterUtilService } from './services/router-util.service';

import './styles/rubick.global.scss';

/**
 * 核心特性模块:
 *
 * 坚持把那些“只用一次”的类收集到CoreModule中，并对外隐藏它们的实现细节。
 * 简化的AppModule会导入CoreModule，并且把它作为整个应用的总指挥。
 *
 * 坚持在core目录下创建一个名叫CoreModule的特性模块
 *
 * 坚持把一个要共享给整个应用的单例服务放进CoreModule中
 *
 * 坚持导入CoreModule中的资产所需要的全部模块
 *
 * 坚持把应用级、只用一次的组件收集到CoreModule中。
 * 只在应用启动时从AppModule中导入它一次，以后再也不要导入它（例如NavComponent和SpinnerComponent）。
 *
 * 避免在AppModule之外的任何地方导入CoreModule。
 *
 * 坚持从CoreModule中导出AppModule需导入的所有符号，使它们在所有特性模块中可用。
 */
@NgModule({
  imports: [CommonModule],
  providers: [RouterUtilService, TranslateService, VendorCustomerService],
  declarations: [HomeComponent],
})
export class CoreModule {}
