import {
  CustomModule,
  CustomModuleName,
  VendorCustomer,
} from './vendor-customer.types';

/**
 * Manage modules and features need to be controlled by different customers.
 * The naming follow: module+feature, camel case. e.g., regionCreate
 * Example:
 * {
 *   name: 'MODULE_NAME',
 *   supports: ['CMB']
 * }
 */
export const CustomModules: CustomModule[] = [
  {
    name: CustomModuleName.CREATE_CLUSTER_DISABLED,
    supports: [VendorCustomer.CMB],
  },
  {
    name: CustomModuleName.MIRROR_CLUSTER,
    supports: [VendorCustomer.CMB],
  },
  {
    name: CustomModuleName.APP_USER_PVC,
    supports: [VendorCustomer.CMB],
  },
  {
    name: CustomModuleName.APP_USER_CONFIGMAP,
    supports: [VendorCustomer.CMB],
  },
  {
    name: CustomModuleName.APP_TOPOLOTY,
    supports: [VendorCustomer.HUATAI],
  },
];
