import { Inject, Injectable } from '@angular/core';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';

import { CustomModules } from './custom-module-config';
import { CustomModule, CustomModuleName } from './vendor-customer.types';

@Injectable()
export class VendorCustomerService {
  constructor(@Inject(ENVIRONMENTS) private environments: Environments) {}

  support(moduleName: CustomModuleName) {
    const customModule = CustomModules.find(
      (module: CustomModule) => module.name === moduleName,
    );
    if (!customModule) {
      return true;
    }
    if (!customModule.supports.length) {
      return false;
    }
    // read from enviroments
    const customer = this.environments.vendor_customer;
    return customModule.supports.includes(customer);
  }
}
