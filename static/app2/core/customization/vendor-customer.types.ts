/**
 * Vendor customers
 */
export enum VendorCustomer {
  PLATFORM = 'PLATFORM',
  HUATAI = 'HUATAI',
  CMB = 'CMB',
  CMBC = 'CMBC',
}

export enum CustomModuleName {
  CREATE_CLUSTER_DISABLED,
  MIRROR_CLUSTER,
  APP_USER_PVC,
  APP_USER_CONFIGMAP,
  APP_TOPOLOTY,
}

/**
 * Custom module
 */
export interface CustomModule {
  name: CustomModuleName;
  description?: string;
  supports: VendorCustomer[];
}
