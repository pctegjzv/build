import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { NavConfig } from './nav/nav.types';
import { NAV_CONFIG } from './tokens';

@Component({
  template: 'Redirecting ...',
})
export class HomeComponent implements OnInit {
  constructor(
    private router: Router,
    @Inject(NAV_CONFIG) public nav: NavConfig,
  ) {}

  ngOnInit(): void {
    setTimeout(() => {
      this.router.navigateByUrl(this.nav.homeUrl, { replaceUrl: true });
    });
  }
}
