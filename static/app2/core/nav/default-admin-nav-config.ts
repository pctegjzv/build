import { NavCategoryConfig, NavConfig, NavItemConfig } from './nav.types';

const NAV_ITEM_CONFIG_IMAGE: NavItemConfig = {
  name: 'image',
  icon: 'fa fa-building',
  children: [
    {
      name: 'image_repository',
      stateName: 'image.repository',
      resourceType: 'registry',
    },
    {
      name: 'image_sync_center',
      stateName: 'image.sync-center',
      resourceType: 'sync_config',
    },
    {
      name: 'image_sync_history',
      stateName: 'image.sync-history',
      resourceType: 'sync_history',
    },
  ],
};

const NAV_ITEM_CONFIG_CLUSTER: NavItemConfig = {
  name: 'region',
  icon: 'fa fa-server',
  children: [
    {
      name: 'cluster',
      stateName: 'cluster',
      resourceType: 'cluster',
    },
    {
      name: 'namespace',
      stateName: 'cluster.namespace',
      resourceType: 'cluster',
      k8sFlag: 'SHOW',
    },
    {
      name: 'k8s_event',
      stateName: 'cluster.k8s_event',
      resourceType: 'event',
      k8sFlag: 'SHOW',
    },
  ],
};

const NAV_ITEM_CONFIG_NETWORK: NavItemConfig = {
  name: 'network',
  icon: 'fa fa-sitemap',
  children: [
    {
      name: 'load_balancer',
      stateName: 'load_balancer',
      resourceType: 'load_balancer',
    },
    {
      name: 'subnet',
      stateName: 'subnet',
      resourceType: 'subnet',
    },
    {
      name: 'certificate',
      stateName: 'certificate',
      resourceType: 'certificate',
    },
  ],
};

const NAV_ITEM_CONFIG_STORAGE: NavItemConfig = {
  name: 'storage',
  icon: 'fa fa-archive',
  children: [
    {
      name: 'storage_volume',
      stateName: 'storage.volume',
      resourceType: 'volume',
    },
    {
      name: 'pv',
      stateName: 'storage.pv',
      resourceType: 'persistentvolume',
      k8sFlag: 'SHOW',
    },
    {
      name: 'storageclass',
      stateName: 'storage.storageclass',
      resourceType: 'k8s_storageclasses',
      k8sFlag: 'SHOW',
    },
  ],
};

// Categories
const NAV_CATEGORY_CONFIG_BOARD: NavCategoryConfig = {
  name: 'board',
  children: [
    {
      name: 'overview',
      icon: 'rb-icon-dashboard',
      stateName: 'overview',
    },
  ],
};

const NAV_CATEGORY_CONFIG_INFRASTRUCTURE: NavCategoryConfig = {
  name: 'infrastructure',
  children: [
    NAV_ITEM_CONFIG_CLUSTER,
    NAV_ITEM_CONFIG_NETWORK,
    NAV_ITEM_CONFIG_STORAGE,
    NAV_ITEM_CONFIG_IMAGE,
  ],
};

const NAV_CATEGORY_CONFIG_OPS: NavCategoryConfig = {
  name: 'ops',
  children: [
    {
      name: 'event',
      stateName: 'event',
      icon: 'fa fa-calendar',
    },
    {
      name: 'logs',
      stateName: 'log',
      icon: 'fa fa-list-alt',
    },
    {
      name: 'notification',
      stateName: 'notification',
      icon: 'fa fa fa-bullhorn',
      resourceType: 'notification',
    },
    {
      name: 'alarm',
      stateName: 'alarm',
      icon: 'fa fa-bell',
      resourceType: ['alarm', 'log_alarm'],
    },
  ],
};

const NAV_CATEGORY_CONFIG_EXTENSION: NavCategoryConfig = {
  name: 'extension',
  children: [
    {
      name: 'jenkins_templates',
      stateName: 'jenkins.templates',
      icon: 'fa fa-cubes',
      resourceType: 'jenkins_pipeline_template',
    },
    {
      name: 'catalog_repo_manager',
      resourceType: 'helm_template_repo',
      stateName: 'app-catalog',
      icon: 'fa fa-cubes',
    },
    {
      name: 'service_broker',
      resourceType: 'clusterservicebroker',
      stateName: 'service_catalog.service_broker',
      icon: 'fa fa-cubes',
    },
    {
      name: 'integration_center',
      stateName: 'integration_center',
      icon: 'rb-icon-integration',
      resourceType: 'integration',
    },
    {
      name: 'plugin_center',
      icon: 'fa fa-plug',
      stateName: 'plugin',
      resourceType: 'application',
    },
  ],
};

const NAV_CATEGORY_CONFIG_MANAGEMENT: NavCategoryConfig = {
  name: 'management',
  children: [
    {
      name: 'projects',
      icon: 'fa fa-cubes',
      stateName: 'projects',
    },
    {
      name: 'rbac',
      icon: 'fa fa-users',
      stateName: 'rbac',
    },
    {
      name: 'license',
      icon: 'fa fa-book',
      stateName: 'license',
    },
  ],
};

const NAV_CATEGORY_CONFIG_OTHER: NavCategoryConfig = {
  name: 'others',
  children: [
    {
      name: 'docs',
      icon: 'fa fa-file',
      href: 'env:user_docs_url',
    },
    {
      name: 'language_picker',
      icon: 'fa fa-globe',
    },
  ],
};

/**
 * 默认原始导航配置。
 */
export const DEFAULT_ADMIN_NAV_CONFIG: NavConfig = {
  categories: [
    NAV_CATEGORY_CONFIG_BOARD,
    NAV_CATEGORY_CONFIG_INFRASTRUCTURE,
    NAV_CATEGORY_CONFIG_OPS,
    NAV_CATEGORY_CONFIG_MANAGEMENT,
    NAV_CATEGORY_CONFIG_EXTENSION,
    NAV_CATEGORY_CONFIG_OTHER,
  ],
  expandMode: 'SINGLE',
  homeUrl: '/overview',
};
