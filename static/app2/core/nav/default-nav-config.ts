import { NavCategoryConfig, NavConfig, NavItemConfig } from './nav.types';

// Items:
const NAV_ITEM_CONFIG_CONTAINER: NavItemConfig = {
  name: 'container',
  icon: 'fa fa-cubes',
  children: [
    {
      name: 'application',
      stateName: 'app_service.app',
      resourceType: 'application',
      k8sFlag: 'HIDE',
      weblab: 'LEGACY_ANGULAR_JS_ENABLED',
    },
    {
      name: 'service',
      stateName: 'app_service.service',
      resourceType: 'service',
      k8sFlag: 'HIDE',
      weblab: 'LEGACY_ANGULAR_JS_ENABLED',
    },
    {
      name: 'application',
      stateName: 'k8s_app',
      resourceType: 'application',
      k8sFlag: 'SHOW',
    },
    {
      name: 'pod',
      stateName: 'k8s_pod',
      resourceType: 'service',
      k8sFlag: 'SHOW',
    },
    {
      name: 'template',
      stateName: 'app_service.template',
      resourceType: 'application_template',
      k8sFlag: 'HIDE',
      weblab: 'LEGACY_ANGULAR_JS_ENABLED',
    },
    {
      name: 'envfile',
      stateName: 'app_service.envfile',
      resourceType: 'env_file',
      k8sFlag: 'HIDE',
      weblab: 'LEGACY_ANGULAR_JS_ENABLED',
    },
    {
      name: 'configuration',
      stateName: 'app_service.configuration',
      resourceType: 'configuration_file',
      k8sFlag: 'HIDE',
      weblab: 'LEGACY_ANGULAR_JS_ENABLED',
    },
    {
      name: 'configmap',
      stateName: 'k8s_configmap',
      resourceType: 'configmap',
      k8sFlag: 'SHOW',
    },
    {
      name: 'resource_management',
      stateName: 'resource_management',
      weblab: 'RSRC_MANAGE_ENABLED',
      k8sFlag: 'SHOW',
    },
  ],
};

const NAV_ITEM_CONFIG_JOB: NavItemConfig = {
  name: 'job',
  icon: 'fa fa-calendar-check-o',
  weblab: 'LEGACY_ANGULAR_JS_ENABLED',
  children: [
    {
      name: 'job_config',
      stateName: 'job.config',
      resourceType: 'job_config',
    },
    {
      name: 'job_history',
      stateName: 'job.history',
      resourceType: 'job_history',
    },
  ],
};

const NAV_ITEM_CONFIG_IMAGE: NavItemConfig = {
  name: 'image',
  icon: 'fa fa-building',
  children: [
    {
      name: 'image_repository',
      stateName: 'image.repository',
      resourceType: 'registry',
    },
    {
      name: 'image_sync_center',
      stateName: 'image.sync-center',
      resourceType: 'sync_config',
    },
    {
      name: 'image_sync_history',
      stateName: 'image.sync-history',
      resourceType: 'sync_history',
    },
  ],
};

const NAV_ITEM_CONFIG_BUILD: NavItemConfig = {
  name: 'build',
  icon: 'fa fa-gavel',
  weblab: 'LEGACY_ANGULAR_JS_ENABLED',
  children: [
    {
      name: 'build_config',
      stateName: 'build.config',
      resourceType: 'build_config',
    },
    {
      name: 'build_history',
      stateName: 'build.history',
      resourceType: 'build_history',
    },
  ],
};

const NAV_ITEM_CONFIG_PIPELINE: NavItemConfig = {
  name: 'pipeline',
  icon: 'fa fa-wrench',
  weblab: 'LEGACY_ANGULAR_JS_ENABLED',
  children: [
    {
      name: 'pipeline_config',
      stateName: 'pipeline.config',
      resourceType: 'pipeline_config',
    },
    {
      name: 'pipeline_approve',
      stateName: 'pipeline.approve',
      weblab: 'HUATAI_ENABLED',
    },
    {
      name: 'pipeline_history',
      stateName: 'pipeline.history',
      resourceType: 'pipeline_history',
    },
  ],
};

const NAV_ITEM_CONFIG_CLUSTER: NavItemConfig = {
  name: 'region',
  icon: 'fa fa-server',
  children: [
    {
      name: 'cluster',
      stateName: 'cluster',
      resourceType: 'cluster',
    },
    {
      name: 'namespace',
      stateName: 'cluster.namespace',
      resourceType: 'cluster',
      k8sFlag: 'SHOW',
    },
    {
      name: 'k8s_event',
      stateName: 'cluster.k8s_event',
      resourceType: 'event',
      k8sFlag: 'SHOW',
    },
  ],
};

// const NAV_ITEM_CONFIG_DOMAIN: NavItemConfig = {
//   name: 'domain',
//   stateName: 'domain',
//   icon: 'fa fa-dashboard',
// };

const NAV_ITEM_CONFIG_NETWORK: NavItemConfig = {
  name: 'network',
  icon: 'fa fa-sitemap',
  children: [
    {
      name: 'load_balancer',
      stateName: 'load_balancer',
      resourceType: 'load_balancer',
    },
    {
      weblab: 'NETWORK_POLICY_ENABLED',
      name: 'network_policy',
      stateName: 'network_policy',
      resourceType: 'k8s_networkpolicies',
      k8sFlag: 'SHOW',
      beta: true,
    },
    {
      name: 'subnet',
      stateName: 'subnet',
      resourceType: 'subnet',
    },
    {
      name: 'certificate',
      stateName: 'certificate',
      resourceType: 'certificate',
    },
  ],
};

const NAV_ITEM_CONFIG_STORAGE: NavItemConfig = {
  name: 'storage',
  icon: 'fa fa-archive',
  children: [
    {
      name: 'storage_volume',
      stateName: 'storage.volume',
      resourceType: 'volume',
    },
    // {
    //   name: 'storage_snapshot',
    //   stateName: 'storage.snapshot',
    //   resourceType: 'snapshot',
    // },
    {
      name: 'pvc',
      stateName: 'storage.pvc',
      resourceType: 'persistentvolumeclaim',
      k8sFlag: 'SHOW',
    },
    {
      name: 'pv',
      stateName: 'storage.pv',
      resourceType: 'persistentvolume',
      k8sFlag: 'SHOW',
    },
    {
      name: 'storageclass',
      stateName: 'storage.storageclass',
      resourceType: 'k8s_storageclasses',
      k8sFlag: 'SHOW',
    },
  ],
};

const NAV_ITEM_CONFIG_RESOURCE: NavItemConfig = {
  name: 'resource',
  icon: 'fa fa-bar-chart',
  children: [
    {
      name: 'resource_quota_space',
      stateName: 'quota',
      weblab: 'QUOTA_ENABLED',
      resourceType: 'space',
    },
    {
      name: 'resource_service_usage',
      stateName: 'service_usage',
    },
  ],
};

const NAV_ITEM_CONFIG_JENKINS: NavItemConfig = {
  name: 'jenkins',
  icon: 'fa fa-wrench',
  weblab: 'JENKINS_ENABLED',
  children: [
    {
      name: 'jenkins_pipelines',
      stateName: 'jenkins.pipelines',
      resourceType: 'jenkins_pipeline',
    },
    {
      name: 'jenkins_histories',
      stateName: 'jenkins.histories',
      resourceType: 'jenkins_pipeline_history',
    },
    {
      name: 'jenkins_templates',
      stateName: 'jenkins.templates',
      resourceType: 'jenkins_pipeline_template',
    },
  ],
};

// Categories
const NAV_CATEGORY_CONFIG_BOARD: NavCategoryConfig = {
  name: 'board',
  children: [
    {
      name: 'overview',
      icon: 'rb-icon-dashboard',
      stateName: 'overview',
    },
  ],
};

const NAV_CATEGORY_CONFIG_DELIVERY: NavCategoryConfig = {
  name: 'delivery',
  children: [
    NAV_ITEM_CONFIG_CONTAINER,
    NAV_ITEM_CONFIG_JOB,
    NAV_ITEM_CONFIG_IMAGE,
    NAV_ITEM_CONFIG_BUILD,
    NAV_ITEM_CONFIG_PIPELINE,
    NAV_ITEM_CONFIG_JENKINS,
  ],
};

const NAV_CATEGORY_CONFIG_INFRASTRUCTURE: NavCategoryConfig = {
  name: 'infrastructure',
  children: [
    NAV_ITEM_CONFIG_CLUSTER,
    NAV_ITEM_CONFIG_STORAGE,
    NAV_ITEM_CONFIG_NETWORK,
    // 招商环境自定义nav使用此目录
    // NAV_ITEM_CONFIG_DOMAIN,
    NAV_ITEM_CONFIG_RESOURCE,
  ],
};

const NAV_CATEGORY_CONFIG_OPS: NavCategoryConfig = {
  name: 'ops',
  children: [
    {
      name: 'monitor_dashboard',
      stateName: 'monitor.dashboard',
      icon: 'fa fa-dashboard',
      resourceType: 'dashboard',
      weblab: 'LEGACY_ANGULAR_JS_ENABLED',
    },
    {
      name: 'monitor_dashboard',
      icon: 'fa fa-dashboard',
      href: 'javascript:openGrafanaTab()',
      weblab: 'USER_VIEW_ENABLED',
    },
    {
      name: 'event',
      stateName: 'event',
      icon: 'fa fa-calendar',
    },
    {
      name: 'logs',
      stateName: 'log',
      icon: 'fa fa-list-alt',
    },
    {
      name: 'notification',
      stateName: 'notification',
      icon: 'fa fa fa-bullhorn',
      resourceType: 'notification',
    },
    {
      name: 'alarm',
      stateName: 'alarm',
      icon: 'fa fa-bell',
      resourceType: ['alarm', 'log_alarm'],
    },
  ],
};

const NAV_CATECORY_CONFIG_PLATFORM: NavCategoryConfig = {
  name: 'app_platform',
  children: [
    {
      name: 'middleware',
      icon: 'fa fa-puzzle-piece',
      stateName: 'app_catalog.middleware',
      resourceType: 'application',
      k8sFlag: 'HIDE',
      beta: true,
    },
    {
      name: 'micro_service',
      icon: 'fa fa-th-large',
      stateName: 'app_catalog.micro_service',
      resourceType: 'application',
      k8sFlag: 'HIDE',
      beta: true,
    },
    {
      name: 'big_data',
      icon: 'fa fa-database',
      stateName: 'app_catalog.big_data',
      resourceType: 'application',
      k8sFlag: 'HIDE',
      beta: true,
    },
    {
      name: 'middleware',
      icon: 'fa fa-puzzle-piece',
      stateName: 'app-platform.middleware',
      k8sFlag: 'SHOW',
      beta: true,
    },
    {
      name: 'micro_service',
      icon: 'fa fa-th-large',
      stateName: 'app-platform.micro_service',
      k8sFlag: 'SHOW',
      beta: true,
    },
    {
      name: 'big_data',
      icon: 'fa fa-database',
      stateName: 'app-platform.big_data',
      k8sFlag: 'SHOW',
      beta: true,
    },
    {
      name: 'catalog',
      icon: 'fa fa-file',
      weblab: 'CATALOG_V2_ENABLED',
      beta: true,
      children: [
        {
          name: 'catalog_repo_manager',
          resourceType: 'helm_template_repo',
          stateName: 'app-catalog',
          k8sFlag: 'SHOW',
        },
        {
          name: 'application_template',
          resourceType: 'helm_template_repo',
          stateName: 'app-catalog.templates',
          k8sFlag: 'SHOW',
        },
      ],
    },
    {
      name: 'service_catalog',
      icon: 'fa fa-book',
      weblab: 'SERVICE_CATALOG_ENABLED',
      beta: true,
      children: [
        {
          name: 'service_broker',
          resourceType: 'clusterservicebroker',
          stateName: 'service_catalog.service_broker',
          k8sFlag: 'SHOW',
        },
        {
          name: 'service_class',
          resourceType: 'clusterservicebroker',
          stateName: 'service_catalog.service_class',
          k8sFlag: 'SHOW',
        },
        {
          name: 'service_instance',
          resourceType: 'serviceinstance',
          stateName: 'service_catalog.service_instance',
          k8sFlag: 'SHOW',
        },
      ],
    },
  ],
};

const NAV_CATEGORY_CONFIG_EXTENSION: NavCategoryConfig = {
  name: 'extension',
  children: [
    {
      name: 'integration_center',
      stateName: 'integration_center',
      icon: 'rb-icon-integration',
      resourceType: 'integration',
      beta: true,
    },
    {
      name: 'plugin_center',
      icon: 'fa fa-plug',
      stateName: 'plugin',
      resourceType: 'application',
      beta: true,
    },
  ],
};

const NAV_CATEGORY_CONFIG_MANAGEMENT: NavCategoryConfig = {
  name: 'management',
  children: [
    {
      name: 'projects',
      icon: 'fa fa-cubes',
      stateName: 'projects',
      weblab: 'PROJECTS_ENABLED',
    },
    {
      name: 'rbac',
      icon: 'fa fa-users',
      stateName: 'rbac',
    },
    {
      name: 'license',
      icon: 'fa fa-book',
      stateName: 'license',
    },
  ],
};

const NAV_CATEGORY_CONFIG_OTHER: NavCategoryConfig = {
  name: 'others',
  children: [
    {
      name: 'docs',
      icon: 'fa fa-file',
      href: 'env:user_docs_url',
    },
    {
      name: 'language_picker',
      icon: 'fa fa-globe',
    },
  ],
};

/**
 * 默认原始导航配置。
 */
export const DEFAULT_NAV_CONFIG: NavConfig = {
  categories: [
    NAV_CATEGORY_CONFIG_BOARD,
    NAV_CATEGORY_CONFIG_DELIVERY,
    NAV_CATEGORY_CONFIG_INFRASTRUCTURE,
    NAV_CATEGORY_CONFIG_OPS,
    NAV_CATECORY_CONFIG_PLATFORM,
    NAV_CATEGORY_CONFIG_EXTENSION,
    NAV_CATEGORY_CONFIG_MANAGEMENT,
    NAV_CATEGORY_CONFIG_OTHER,
  ],
  expandMode: 'SINGLE',
  homeUrl: '/overview',
};
