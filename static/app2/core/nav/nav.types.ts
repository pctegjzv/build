import { Environment, ResourceType, Weblab } from '../types';

/**
 * 完整的导航配置。
 * - 定义整体规则
 * - 整合导航分类
 */
export interface NavConfig {
  /**
   * 导航分类数组。顺序与展示有关。
   */
  categories: NavCategoryConfig[];

  /**
   * 导航一级项目展开的模式。
   * - SINGLE: 每次只能展开一个(默认)
   * - MULTIPLE: 每次可展开多个
   * - NO_COLLAPSE: 默认全展开并且不可折叠
   */
  expandMode: 'SINGLE' | 'MULTIPLE' | 'NO_COLLAPSE';

  /**
   * 默认导航路由状态名。
   */
  homeUrl: string;
}

/**
 * 导航分类。
 * - 整合导航项目
 */
export interface NavCategoryConfig {
  /**
   * 导航分类名。
   * 这个值将在添加'nav_'前缀后，被国际化翻译。
   */
  name: string;

  /**
   * 此导航分类下的项目数组。顺序与展示有关。
   */
  children: NavItemConfig[];
}

/**
 * 导航项目。一级、二级项目共用相同的模型。
 */
export interface NavItemConfig {
  /**
   * 项目的名称。
   * 这个值将在添加'nav_'前缀后，被国际化翻译。
   * 注：将作为唯一标示。
   * 不可为空。
   *
   * 特殊值：language_picker。用以切换语言。
   */
  name: string;

  /**
   * 项目关联的路由状态名。跟href冲突。
   * 例子：
   *   "state": "app_service.app_service"
   */
  stateName?: string;

  /**
   * 外部链接。跟stateName冲突。
   * 有两种类型：
   * - 一般超链接：
   *   "href": "alauda.cn"
   * - 环境变量href，以 `env:` 开头。当指定的环境变量不存在时，会隐藏掉这个项目。
   *   "href": "env:user_docs_url"
   */
  href?: string;

  /**
   * 图标，为FontAwesome的超集（加入灵雀云自定义的几个图标）。
   *
   * 例子：'fa fa-sitemap'
   */
  icon?: string;

  /**
   * 此导航项目的二级项目数组。顺序与展示有关。
   * 注：此属性只对一级项目有效。
   *
   * 一级项目可以没有items，二级项目不能有items。
   * 当为非空时，表示此项目为一级项目，可以展开或折叠。
   */
  children?: NavItemConfig[];

  /**
   * 是否显示beta图标。默认不显示。
   */
  beta?: boolean;

  /**
   * 相关的Weblab。只有Weblab为真时，显示此项目。
   */
  weblab?: Weblab;

  /**
   * 相关的Environment。只有Environment的值为真时，显示此项目。
   */
  environment?: Environment;

  /**
   * 相关联的资源类型。
   * 当当前用户的资源类型不能访问时，将隐藏掉此导航项目。
   * 当resourceType为数组时，则使用 "或" 逻辑判断
   */
  resourceType?: ResourceType | ResourceType[];

  /**
   * 新版k8s集群相关功能标志位，SHOW 表示切换到该集群下显示的导航项目
   * 新版k8s集群判断条件： container_manager === KUBERNETES && platform_version === v3
   */
  k8sFlag?: 'SHOW' | 'HIDE';
}
