import { APP_BASE_HREF } from '@angular/common';
import { Component, NgModule } from '@angular/core';
import { TestBed, inject } from '@angular/core/testing';
import { RouterModule } from '@angular/router';

import { DefaultBreadcrumbStrategy } from 'app2/layout/default-breadcrumb-strategy.service';
import { RouterStateLinkDirective } from 'app2/shared/directives/router-state-link/router-state-link.directive';
import { translateService } from 'app2/testing/fake-translate.service';
import { delay } from 'app2/testing/helpers';

import { TranslateService } from '../../translate/translate.service';

import { RouterUtilService } from './router-util.service';

@Component({
  template: `Test Component
  <a [rcRouterStateLink]="stateName" [params]="params">Some Link</a>`,
})
export class TestComponent {
  params: any;
  stateName: string;
}

const TEST_BASE = '/TEST_BASE/';

@NgModule({
  imports: [
    RouterModule.forRoot([
      {
        path: 'animal/:animalParam',
        children: [
          {
            path: 'cato',
            redirectTo: 'cat/:mirana',
            pathMatch: 'full',
          },
          {
            path: 'cat/:catParam',
            component: TestComponent,
          },
          {
            path: 'dog',
            children: [
              {
                path: 'moe/:dogParam',
                component: TestComponent,
              },
            ],
          },
        ],
      },
    ]),
  ],
  exports: [RouterModule],
  declarations: [TestComponent, RouterStateLinkDirective],
  providers: [
    {
      provide: APP_BASE_HREF,
      useValue: TEST_BASE,
    },
    {
      provide: TranslateService,
      useValue: translateService,
    },

    // I think we don't need to mock this one ...
    DefaultBreadcrumbStrategy,
  ],
})
class RouterTestModule {}

describe('RouterUtilService', () => {
  let routerUtil: RouterUtilService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RouterUtilService],
      imports: [RouterTestModule],
    });
  });

  // Inject related services
  beforeEach(inject([RouterUtilService], (u: RouterUtilService) => {
    routerUtil = u;
  }));

  it('href: can render ui-router state href correctly', async done => {
    expect(await routerUtil.href('foo.bar')).toBe('/TEST_BASE/foo/bar');
    done();
  });

  describe('href: can render Angular state href correctly', () => {
    it(`animal.cat, { animalParam: 'lovely', catParam: 'luna' }`, async done => {
      expect(
        await routerUtil.href('animal.cat', {
          animalParam: 'lovely',
          catParam: 'luna',
        }),
      ).toBe('/TEST_BASE/animal/lovely/cat/luna');
      done();
    });

    it(`animal.cato, { animalParam: 'lovely', catParam: 'luna' }`, async done => {
      expect(
        await routerUtil.href('animal.cato', {
          animalParam: 'lovely',
          catParam: 'luna',
        }),
      ).toBe('/TEST_BASE/animal/lovely/cato?catParam=luna');
      done();
    });

    it(`animal.dog.moe, { animalParam: 'lovely', dogParam: 'doge', otherParam: 'lol' }`, async done => {
      expect(
        await routerUtil.href('animal.dog.moe', {
          animalParam: 'lovely',
          dogParam: 'doge',
          otherPram: 'lol',
        }),
      ).toBe('/TEST_BASE/animal/lovely/dog/moe/doge?otherPram=lol');
      done();
    });
  });

  it('RouterStateLinkDirective can render correctly', async done => {
    const fixture = TestBed.createComponent(TestComponent);
    fixture.componentInstance.stateName = 'animal.dog.moe';
    fixture.componentInstance.params = {
      animalParam: 'lovely',
      dogParam: 'doge',
      otherPram: 'lol',
    };
    fixture.detectChanges();

    // Link will be update in next event.
    await delay(0);
    fixture.detectChanges();
    done();
  });
});
