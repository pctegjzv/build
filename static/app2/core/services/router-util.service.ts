import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  NavigationEnd,
  Route,
  Router,
  RouterState,
} from '@angular/router';
import { Observable } from 'rxjs';
import {
  filter,
  map,
  publishReplay,
  refCount,
  startWith,
} from 'rxjs/operators';

import { delay } from 'app2/shared/services/utility/delay';

/* tslint:disable-next-line */
type Params = { [name: string]: any };

/**
 * Unified Rubick Router state consisted with basic router state info.
 */
export class RcRouterStateSnapshot {
  /**
   * A string representing the state in UI-Router style, i.e.: foo.bar
   */
  name: string;

  /**
   * Params data associated with this route state, combined with path/matrix/query parameters
   */
  params: Params;

  /**
   * Url associated with this state.
   */
  url: string;

  /**
   * Original route state object
   */
  config: Route;

  /**
   * The associate data object.
   */
  data: any;
}

/**
 * Define useful router related functions.
 */
@Injectable()
export class RouterUtilService {
  private _routerState: Observable<RcRouterStateSnapshot>;
  private routerStateSnapshot: RcRouterStateSnapshot;

  constructor(private router: Router, private location: Location) {
    this._routerState = this.getRouterState().pipe(
      publishReplay(1),
      refCount(),
    );
    this._routerState.subscribe(
      snapshot => (this.routerStateSnapshot = snapshot),
    );
  }

  /**
   * Unified Rubick Router state consisted with basic router state info.
   *
   * For compatibility, ui-router and Angular router is merged together.
   * @returns {Observable<RcRouterStateSnapshot>}
   */
  get routerState(): Observable<RcRouterStateSnapshot> {
    return this._routerState;
  }

  /**
   * Return snapshot of routerState.
   */
  getRouterStateSnapshot(): RcRouterStateSnapshot {
    return this.routerStateSnapshot;
  }

  /**
   * Go to a state.
   * Note, the state name should only contain characters and dots
   */
  async go(stateName: string, params?: Params): Promise<any> {
    const href = await this.href(stateName, params);

    if (href) {
      return this.router.navigateByUrl(this.location.normalize(href));
    }
  }

  /**
   * Replace a state.
   */
  async replace(stateName: string, params?: Params): Promise<any> {
    const href = await this.href(stateName, params);

    if (href) {
      return this.router.navigateByUrl(this.location.normalize(href), {
        replaceUrl: true,
      });
    }
  }

  getLinkUrl(commands: string | string[], queryParams?: Params) {
    if (!Array.isArray(commands)) {
      commands = [commands];
    }
    return this.location.prepareExternalUrl(
      this.router.serializeUrl(
        this.router.createUrlTree(commands, {
          queryParams,
        }),
      ),
    );
  }

  /**
   * Render a state name to href.
   * Support both UI-router state or Angular Router state.
   *
   * Note, the url is prefixed with base URL.
   *
   * @param stateName
   * @param params Path/query parameters.
   * @returns {Promise<string>}
   */
  async href(stateName: string, params?: Params): Promise<string> {
    try {
      // TODO: add error handling for Angular router
      const unConsumedParams = { ...params };
      let routeConfigRef = this.router.config;
      const urlComponents = stateName.split('.').reduce((accum, key) => {
        const res = [...accum, key];
        // lazy loading route
        if (!routeConfigRef) {
          return res;
        }
        const config = routeConfigRef.find(_config => {
          return _config.path.includes('/')
            ? _config.path.split('/')[0] === key
            : _config.path === key;
        });
        const pathSplits = config.path.split('/');
        pathSplits.forEach((node, index) => {
          if (node.startsWith(':')) {
            res.push(params[pathSplits[index].substr(1)]);
            delete unConsumedParams[pathSplits[index].substr(1)];
          }
        });
        routeConfigRef = config.children;
        return res;
      }, []);
      // Un consumed parameters will be rendered as query parameters:
      return this.getLinkUrl(urlComponents, unConsumedParams);
    } catch (e) {
      return this.getLinkUrl(stateName.split('.').join('/'));
    }
  }

  /**
   * Reload state.
   * Note: we should not rely on this function to do router reload any longer.
   * A view should not depend on specific router state in its life-cycle.
   */
  async reload() {
    const originalState = await this.routerState.toPromise();
    await this.go('empty-route');
    await delay();
    this.go(originalState.name, originalState.params);
  }

  /**
   * Angular Router state
   * @returns {Observable<RouterState>}
   */
  private getNgRouterState(): Observable<RouterState> {
    return this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      startWith(null),
      map(() => this.router.routerState),
    );
  }

  /**
   * Merge UI-Router and Angular Router states
   * @returns {Observable<RcRouterStateSnapshot>}
   */
  private getRouterState(): Observable<RcRouterStateSnapshot> {
    return this.getNgRouterState().pipe(
      map((state: RouterState) => {
        const leafActivatedRouteSnapshot = getLeafActivatedRouteSnapshot(state);

        // Combine both path/matrix params with query params
        const params = Object.assign(
          {},
          leafActivatedRouteSnapshot.params,
          leafActivatedRouteSnapshot.queryParams,
        );
        const stateName = leafActivatedRouteSnapshot.pathFromRoot
          .map(interSnapshot => interSnapshot.url)
          .filter(urlSegments => urlSegments.length)
          // Note: for ActivatedRoute, we have an assumption that only the first segment is used for the state name
          .map(urlSegments => urlSegments[0])
          .join('.');

        const routeConfig = leafActivatedRouteSnapshot.routeConfig;

        return {
          params,
          name: stateName,
          url: this.location.prepareExternalUrl(state.snapshot.url),
          config: routeConfig,
          data: leafActivatedRouteSnapshot.data,
        };
      }),
      filter(state => !!state.name),
    );
  }
}

/**
 * Helper function to get the leaf ActivatedRouteSnapshot of a RouterState
 * @param routerState
 * @returns {ActivatedRouteSnapshot}
 */
function getLeafActivatedRouteSnapshot(
  routerState: RouterState,
): ActivatedRouteSnapshot {
  let leafActivatedRouteSnapshot = routerState.root.snapshot;
  while (leafActivatedRouteSnapshot.firstChild) {
    leafActivatedRouteSnapshot = leafActivatedRouteSnapshot.firstChild;
  }
  return leafActivatedRouteSnapshot;
}
