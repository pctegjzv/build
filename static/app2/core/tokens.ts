/**
 * Make weblabs or other non class ts interfaces to be injectable
 */
import { InjectionToken } from '@angular/core';

import { NavConfig } from './nav/nav.types';
import {
  Environments,
  I18nManifest,
  InstanceSizeConfig,
  RcAccount,
  RcTencentSso,
  Weblabs,
} from './types';

export const WEBLABS = new InjectionToken<Weblabs>('WEBLABS');
export const ENVIRONMENTS = new InjectionToken<Environments>('ENVIRONMENTS');
export const ACCOUNT = new InjectionToken<RcAccount>('ACCOUNT');
export const EXAMPLE_YAML = new InjectionToken<RcAccount>('EXAMPLE_YAML');
export const TENCENTSSO = new InjectionToken<RcTencentSso>('TENCENTSSO');
export const NAV_CONFIG = new InjectionToken<NavConfig>('NAV_CONFIG');
export const TRANSLATIONS = new InjectionToken<Weblabs>('TRANSLATIONS');
export const I18NMANIFEST = new InjectionToken<I18nManifest>('I18NMANIFEST');
export const INSTANCE_SIZE_CONFIG = new InjectionToken<InstanceSizeConfig>(
  'INSTANCE_SIZE_CONFIG',
);
