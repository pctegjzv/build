// Defines core types here

import _angular from 'angular';

declare global {
  const angular: _angular.IAngularStatic;
}

import { VendorCustomer } from './customization/vendor-customer.types';

export * from './nav/nav.types';
/**
 *  Authenticated user's account information
 */
export enum ConsoleViewType {
  USER = 'user',
  ADMIN = 'admin',
}

export declare interface RcAccount {
  namespace: string;
  username: string;
  roles?: AccountType[];
  type?: string;
  sub_type?: string;
  is_valid?: boolean;
  view_type: ConsoleViewType;
}

export declare interface AccountType {
  role_uuid: string;
  role_name: string;
  template_display_name: string;
  template_name?: string;
  template_uuid: string;
}

export declare interface RcRole {
  name?: string;
  uuid?: string;
  namespace?: string;
  admin_role?: boolean;
  role_uuid?: string;
  template_uuid?: string;
}

/**
 *  third part like tencentsso information
 */
export declare interface RcTencentSso {
  ssoSource: string;
  ssoLogoutUrl: string;
}

/**
 * Model for user profile
 * /v1/auth/profile
 */
export declare interface RcProfile {
  account_type: number;
  company: string;
  email: string;
  logo_file: string;
  mobile: string;
  username: string;
  is_admin: boolean;
}

export declare interface RcProject {
  name: string;
  namespace: string;
  resource_actions: string[];
  status: string;
  template: string;
  template_uuid: string;
  token: string;
  updated_at: string;
  created_at: string;
  uuid: string;
}

export declare interface RcRegisterInfo {
  product_name: string;
  registration_code: string;
}

/**
 * All weblabs used in Rubick
 */
export interface Weblabs {
  QUOTA_ENABLED?: boolean;
  PROJECTS_ENABLED?: boolean;
  JENKINS_ENABLED?: boolean;
  CATALOG_V2_ENABLED?: boolean;
  NEW_KUBERNETES_ENABLED?: boolean;
  K8S_VERSION_110_ENABLED?: boolean;
  SERVICE_CATALOG_ENABLED?: boolean;
  TAG_MANAGE_ENABLED?: boolean;
  NETWORK_POLICY_ENABLED?: boolean;
  HUATAI_ENABLED?: boolean;
  ACCESS_CLUSTER_ENABLED?: boolean;
  RSRC_MANAGE_ENABLED?: boolean;
  LEGACY_ANGULAR_JS_ENABLED?: boolean;
  USER_VIEW_ENABLED?: boolean;
}

export interface I18nManifest {
  en: { [key: string]: string };
  zh_cn: { [key: string]: string };
}

export interface Environments {
  debug?: boolean;
  alauda_image_index?: string;
  lycan_url?: string;
  is_private_deploy_enabled?: boolean;
  cluster_create_cloud_type?: string;
  private_deploy_password_pattern_type?: string;
  third_party_login_type?: string;
  user_docs_url?: string;
  oss_server_url?: string;
  remove_default_image_registry?: boolean;
  predefined_login_organization?: string;
  overridden_logo_sources?: string;
  nav_states_ordering?: string;
  license_required?: boolean;
  vendor_customer?: VendorCustomer;
  alarm_severity_level_enabled?: boolean;
  projects_v2_enabled?: boolean;
  outsourcing_image_index?: string;
  default_quota?: string;
  sock_server_url?: string;
  cmb_nodeport_domain?: string;
  cmb_image_upload_url?: string;
  notification_duration: string;
  notification_max_stack: string;
  label_base_domain: string;
  auto_logout_latency: number;
}

export interface InstanceSizeConfig {
  fixed: {
    [key: string]: {
      cpu: number;
      mem: number;
    };
  };
  custom?: {
    cpu: {
      min?: number;
    };
    mem: {
      min?: number;
    };
  };
}

export type Translations = any;

export type Weblab = keyof Weblabs;

export type Environment = keyof Environments;

/**
 * Console related status.
 *
 * All feature related status should be mapped into one of the categories
 */
export enum UnifiedStatus {
  SUCCESS = 'SUCCESS',
  ERROR = 'ERROR',
  PENDING = 'PENDING',
  IN_PROGRESS = 'IN_PROGRESS',
  INACTIVE = 'INACTIVE',
}

/**
 * Resource types
 */
export type ResourceType =
  | 'alarm'
  | 'application'
  | 'application_template'
  | 'build_config'
  | 'build_history'
  | 'cloud_account'
  | 'cloud_instance'
  | 'cluster'
  | 'cluster_node'
  | 'namespace'
  | 'configuration_file'
  | 'dashboard'
  | 'dashboard_panel'
  | 'env_file'
  | 'events'
  | 'k8s_networkpolicies'
  | 'ip'
  | 'subnet'
  | 'load_balancer'
  | 'notification'
  | 'organization'
  | 'pipeline_config'
  | 'pipeline_history'
  | 'pipeline_task'
  | 'registry'
  | 'registry_project'
  | 'repository'
  | 'role'
  | 'service'
  | 'snapshot'
  | 'space'
  | 'subaccount'
  | 'sync_config'
  | 'sync_history'
  | 'volume'
  | 'project'
  | 'project_template'
  | 'private_ip'
  | 'certificate'
  | 'job_config'
  | 'job_history'
  | 'log_filter'
  | 'log_alarm'
  | 'integration'
  | 'configmap'
  | 'jenkins_pipeline'
  | 'jenkins_pipeline_history'
  | 'jenkins_pipeline_template'
  | 'jenkins_pipeline_template_repository'
  | 'jenkins_credential'
  | 'helm_template'
  | 'helm_template_repo'
  | 'public_helm_template'
  | 'public_helm_template_repo'
  | 'persistentvolume'
  | 'persistentvolumeclaim'
  | 'role_template'
  | 'clusterservicebroker'
  | 'serviceinstance'
  | 'pipeline_template'
  | 'k8s_storageclasses'
  | 'operation_view'
  | 'event'
  | 'k8s_others'
  | 'k8s_resourcequotas'
  | 'domain';

export interface Dictionary<T = any> {
  [key: string]: T;
}

export interface RcLog {
  message: string;
  time: number;
  level: number;
}

export * from './nav/nav.types';
