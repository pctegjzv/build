import { Observable } from 'rxjs';
import { pluck } from 'rxjs/operators';

export const observablePluck = <T, R>(
  observable: Observable<T>,
  ...payloads: string[]
) => observable.pipe(pluck<T, R>(...payloads));
