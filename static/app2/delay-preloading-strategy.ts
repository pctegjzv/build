import { Injectable } from '@angular/core';
import { PreloadingStrategy, Route } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, delay, switchMap } from 'rxjs/operators';

@Injectable()
export class DelayPreloadingStrategy implements PreloadingStrategy {
  private count = 1;

  preload(_route: Route, fn: () => Observable<any>): Observable<any> {
    return of(null).pipe(
      delay(this.count++ * 10000),
      switchMap(() => fn().pipe(catchError(() => of(null)))),
    );
  }
}
