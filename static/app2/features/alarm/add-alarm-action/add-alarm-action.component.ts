import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as _ from 'lodash';
import { Observable, Subscription } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

import { AlarmUtilitiesService } from 'app2/features/alarm/alarm-utilities.service';
import { NotificationService } from 'app2/shared/services/features/notification.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { TranslateService } from 'app2/translate/translate.service';

interface TriggerType {
  name: string;
  value: string;
}
interface ActionType {
  name: string;
  value: string;
}
interface ServiceOption {
  uuid: string;
  display_name: string;
}
@Component({
  selector: 'rc-add-alarm-action',
  templateUrl: './add-alarm-action.component.html',
  styleUrls: ['./add-alarm-action.component.scss'],
})
export class AddAlarmActionComponent implements OnInit, OnDestroy {
  @Output()
  finish = new EventEmitter<any>();
  form: FormGroup;
  loading = false;
  submitting = false;
  triggerTypes: TriggerType[] = [];
  actionTypes: ActionType[] = [];
  notifications: any[];
  clusters: any[];
  services: ServiceOption[];
  selectedActionType = 'notification';
  clusterObservable: Observable<string>;
  clusterSubscription: Subscription;
  selectedNotifications: any[] = [];
  selectedServices: ServiceOption[] = [];

  submitDisabled = true;
  constructor(
    private fb: FormBuilder,
    private translate: TranslateService,
    private notificationService: NotificationService,
    private regionService: RegionService,
    private alarmUtil: AlarmUtilitiesService,
  ) {
    this.form = this.fb.group({
      triggerType: '',
      actionType: '',
      notifications: '',
      cluster: '',
      services: '',
    });
  }

  async ngOnInit() {
    this.triggerTypes = [
      {
        name: this.translate.get('alarm_status_triggered'),
        value: 'alarm_status_triggered',
      },
      {
        name: this.translate.get('alarm_status_released'),
        value: 'alarm_status_released',
      },
    ];
    if (!location.pathname.includes('log_alarm')) {
      this.triggerTypes.push({
        name: this.translate.get('insufficient_data'),
        value: 'insufficient_data',
      });
    }
    this.actionTypes = [
      {
        name: this.translate.get('notification'),
        value: 'notification',
      },
      {
        name: this.translate.get('scaling_in'),
        value: 'scaling_in',
      },
      {
        name: this.translate.get('scaling_out'),
        value: 'scaling_out',
      },
    ];
    this.clusterObservable = this.form
      .get('cluster')
      .valueChanges.pipe(distinctUntilChanged());

    this.form.patchValue({
      triggerType: this.triggerTypes[0].value,
      actionType: this.actionTypes[0].value,
    });
  }

  async onActionTypeChange(option: ActionType) {
    if (!option.value) {
      return;
    }
    switch (option.value) {
      case 'notification':
        this.selectedActionType = 'notification';
        if (!this.notifications) {
          try {
            const {
              result,
            } = await this.notificationService.getNotifications();
            this.notifications = result.map((item: any) => {
              item.display_name = item.space_name
                ? `${item.name}(${item.space_name})`
                : item.name;
              return item;
            });
          } catch (error) {
            this.notifications = [];
          }
        }
        break;
      case 'scaling_in':
      case 'scaling_out':
        this.selectedActionType = 'cluster';
        // region
        if (!this.clusters) {
          try {
            const result = await this.regionService.getRegions();
            const allRegions = result;
            this.clusters = allRegions.filter((region: Region) => {
              return (
                region.container_manager !== 'NONE' &&
                region.state === 'RUNNING'
              );
            });
            this.clusterSubscription = this.clusterObservable.subscribe(
              async (value: string) => {
                const region = _.find(this.clusters, ['id', value]);
                try {
                  const result = await this.alarmUtil.getRegionServices(region);
                  this.services = result;
                } catch ({ errors }) {
                  this.services = [];
                }
              },
            );
            this.form.get('cluster').patchValue(this.clusters[0].id);
          } catch ({ errors }) {
            this.clusters = [];
          }
        }
        break;
    }
  }

  onNotificationChange(values: any[]) {
    this.submitDisabled = !values.length;
  }

  onNotificationAdd(opt: any) {
    this.selectedNotifications.push(opt);
  }

  onNotificationRemove(opt: any) {
    _.remove(this.selectedNotifications, (item: any) => {
      return item.name === opt.name;
    });
  }

  onServiceChange(values: any[]) {
    this.submitDisabled = !values.length;
  }

  onServiceAdd(opt: ServiceOption) {
    this.selectedServices.push(opt);
  }

  onServiceRemove(opt: ServiceOption) {
    _.remove(this.selectedServices, (item: ServiceOption) => {
      return item.uuid === opt.uuid;
    });
  }

  submit() {
    let res: any;
    const values = this.form.value;
    if (values.actionType === 'notification') {
      res = this.selectedNotifications.map((item: any) => {
        item.action_type = values.actionType;
        item.trigger_option = values.triggerType;
        return item;
      });
    } else {
      res = this.selectedServices.map((item: any) => {
        item.action_type = values.actionType;
        item.trigger_option = values.triggerType;
        return item;
      });
    }
    this.complete(res);
  }

  ngOnDestroy() {
    if (this.clusterSubscription) {
      this.clusterSubscription.unsubscribe();
    }
  }

  private complete(res?: any) {
    this.finish.next(res);
    this.finish.complete();
  }

  cancel() {
    this.complete();
  }
}
