import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import { find } from 'lodash';
import { first } from 'rxjs/operators';

// import { ToastService } from 'alauda-ui';
import { AddAlarmActionComponent } from '../add-alarm-action/add-alarm-action.component';

@Component({
  selector: 'rc-alarm-action',
  templateUrl: './alarm-action.component.html',
})
export class AlarmActionComponent {
  @Output()
  alarmActionAdd = new EventEmitter<any[]>();
  @Input()
  actions: any[];
  constructor(
    private modal: ModalService,
    private translate: TranslateService,
  ) {}

  addAlarmAction() {
    const modalRef = this.modal.open(AddAlarmActionComponent, {
      title: this.translate.get('alarm_add_actions'),
    });
    modalRef.componentInstance.finish.pipe(first()).subscribe((result: any) => {
      modalRef.close();
      if (result && result.length) {
        result.forEach((newItem: any) => {
          const existItem = find(this.actions, (item: any) => {
            return (
              newItem.uuid === item.uuid &&
              newItem.action_type === item.action_type &&
              newItem.trigger_option === item.trigger_option
            );
          });
          if (existItem) {
            return;
          } else {
            this.actions.push(newItem);
            this.alarmActionAdd.emit(this.actions);
          }
        });
      }
    });
  }

  getActionResourceDisplayName(action: any) {
    return action.display_name || action.name || action.uuid;
  }

  removeActionResource(index: number) {
    this.actions.splice(index, 1);
    this.alarmActionAdd.emit(this.actions);
  }
}
