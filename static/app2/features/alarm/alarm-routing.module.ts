import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AlarmDashboardComponent } from './dashboard/alarm-dashboard.component';
import { CreateLogAlarmComponent } from './log-alarm/create/create-log-alarm.component';
import { LogAlarmDetailComponent } from './log-alarm/detail/log-alarm-detail.component';
import { UpdateLogAlarmComponent } from './log-alarm/update/update-log-alarm.component';
import { CreateAlarmComponent } from './metric-alarm/create/create-alarm.component';
import { AlarmDetailComponent } from './metric-alarm/detail/alarm-detail.component';
import { UpdateAlarmComponent } from './metric-alarm/update/update-alarm.component';

const alarmRoutes: Routes = [
  {
    path: '',
    component: AlarmDashboardComponent,
  },
  {
    path: 'alarm_detail/:uuid',
    component: AlarmDetailComponent,
  },
  {
    path: 'alarm_create',
    component: CreateAlarmComponent,
  },
  {
    path: 'alarm_update/:uuid',
    component: UpdateAlarmComponent,
  },
  {
    path: 'log_alarm_create',
    component: CreateLogAlarmComponent,
  },
  {
    path: 'log_alarm_update/:uuid',
    component: UpdateLogAlarmComponent,
  },
  {
    path: 'log_alarm_detail/:uuid',
    component: LogAlarmDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(alarmRoutes)],
  exports: [RouterModule],
})
export class AlarmRoutingModule {}
