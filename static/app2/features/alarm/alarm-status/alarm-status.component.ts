import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { get as _get } from 'lodash';

@Component({
  selector: 'rc-alarm-status',
  templateUrl: './alarm-status.component.html',
  styleUrls: ['./alarm-status.component.scss'],
})
export class AlarmStatusComponent implements OnChanges {
  @Input()
  alarm: any;

  statusStat = {
    alarm: 0,
    alarm_ack: 0,
    insufficient_data: 0,
  };

  ngOnChanges(changes: SimpleChanges) {
    const alarm = changes['alarm'];
    if (alarm) {
      this.statusStat = {
        alarm: 0,
        alarm_ack: 0,
        insufficient_data: 0,
      };
      const children = _get(alarm.currentValue, 'children', []);
      children.forEach((child: any) => {
        if (child.status === 'ALARM') {
          _get(child, 'actions.ack.status')
            ? this.statusStat.alarm_ack++
            : this.statusStat.alarm++;
        }
        if (
          child.status === 'INSUFFICIENT_DATA' &&
          !_get(child, 'actions.ack.status')
        ) {
          this.statusStat.insufficient_data++;
        }
      });
    }
  }

  get statusClass(): string {
    return this.statusStat.alarm === 0
      ? 'rc-alarm-status-ok'
      : 'rc-alarm-status-alarm';
  }
}
