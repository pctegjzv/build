import { Inject, Injectable } from '@angular/core';
import * as _ from 'lodash';

import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';
import { DurationToConsumedPipe } from 'app2/shared/pipes/duration-to-consumed.pipe';
import { AppService } from 'app2/shared/services/features/app.service';
import { Region } from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { TranslateService } from 'app2/translate/translate.service';

@Injectable()
export class AlarmUtilitiesService {
  constructor(
    private translate: TranslateService,
    private appService: AppService,
    private serviceService: ServiceService,
    private regionUtil: RegionUtilitiesService,
    @Inject(WEBLABS) public weblabs: Weblabs,
  ) {}

  isThresholdDecimal(name: string) {
    if (!name) {
      return true;
    }
    const list = [
      'cpu_utilization',
      'memory_utilization',
      'cpu_utilization_real',
      'mem_utilization_real',
      'region-cpus-percent',
      'region-mem-percent',
      'node-cpus-percent',
      'node-mem-percent',
      'node-max-disk-percent',
      'node_load_norm_1',
      'node_load_norm_5',
      'node_load_norm_15',
    ];
    return list.indexOf(name) >= 0;
  }

  getStatisticTypes() {
    return [
      {
        name: this.translate.get('SampleCount'),
        value: 'SampleCount',
      },
      {
        name: this.translate.get('Average'),
        value: 'Average',
      },
      {
        name: this.translate.get('Sum'),
        value: 'Sum',
      },
      {
        name: this.translate.get('Minimum'),
        value: 'Minimum',
      },
      {
        name: this.translate.get('Maximum'),
        value: 'Maximum',
      },
    ];
  }

  getPeriodTypes() {
    return [
      {
        name: '1' + this.translate.get('minutes'),
        value: 60,
        readable_value: '1m',
      },
      {
        name: '5' + this.translate.get('minutes'),
        value: 300,
        readable_value: '5m',
      },
      {
        name: '15' + this.translate.get('minutes'),
        value: 900,
        readable_value: '15m',
      },
      {
        name: '1' + this.translate.get('hours'),
        value: 3600,
        readable_value: '1h',
      },
      {
        name: '6' + this.translate.get('hours'),
        value: 21600,
        readable_value: '6h',
      },
      {
        name: '12' + this.translate.get('hours'),
        value: 43200,
        readable_value: '12h',
      },
    ];
  }

  getComparisonOperators() {
    return [
      {
        name: this.translate.get('GreaterThanOrEqualToThreshold'),
        value: 'GreaterThanOrEqualToThreshold',
      },
      {
        name: this.translate.get('GreaterThanThreshold'),
        value: 'GreaterThanThreshold',
      },
      {
        name: this.translate.get('LessThanThreshold'),
        value: 'LessThanThreshold',
      },
      {
        name: this.translate.get('LessThanOrEqualToThreshold'),
        value: 'LessThanOrEqualToThreshold',
      },
    ];
  }

  getResourceTypes() {
    return [
      {
        name: this.translate.get('SERVICE'.toLowerCase()),
        value: 'SERVICE',
      },
      {
        name: this.translate.get('REGION'.toLowerCase()),
        value: 'REGION',
      },
      {
        name: this.translate.get('NODE'.toLowerCase()),
        value: 'NODE',
      },
    ];
  }

  getThresholdOperatorList() {
    return ['>', '>=', '==', '<=', '<', '!='];
  }

  getNotifyIntervalOptions() {
    return [
      {
        name: this.translate.get('second'),
        value: 1,
        readable_value: 's',
      },
      {
        name: this.translate.get('minutes'),
        value: 60,
        readable_value: 'm',
      },
      {
        name: this.translate.get('hours'),
        value: 3600,
        readable_value: 'h',
      },
      {
        name: this.translate.get('day'),
        value: 86400,
        readable_value: 'd',
      },
    ];
  }

  async getRegionServices(region: Region) {
    const isNewK8s = this.regionUtil.isNewK8sRegion(region);
    if (isNewK8s) {
      return this.serviceService
        .getK8sServices({
          pageNo: 1,
          pageSize: 100,
          params: {
            cluster: region.name,
          },
        })
        .then(({ results }: any) => {
          const ret = results
            .filter((result: any) => result.resource.kind !== 'DaemonSet')
            .map((result: any) => {
              let display_name = result.resource.name;
              if (result.parent.name) {
                display_name = `${result.parent.name}.${result.resource.name}`;
              }
              if (result.namespace) {
                display_name += ` (${result.namespace.name})`;
              }
              return {
                uuid: result.resource.uuid,
                display_name,
              };
            });
          return _.sortBy(ret, (item: any) => {
            return item.display_name;
          });
        });
    } else {
      return Promise.all([
        this.appService.getApps({ regionName: region.name, allowMeta: true }),
        this.serviceService.getV1Services({
          region_name: region.name,
          basic: true,
          pageSize: 100,
        }),
      ]).then(([apps, services]) => {
        const appServices = _.flatMap(apps, app =>
          app.services
            .filter(
              (service: any) =>
                !service.pod_controller || this.isDeploymentMode(service),
            )
            .map((appService: any) => ({
              uuid: appService.uuid,
              display_name: this.getServiceDisplayName(appService, app),
            })),
        );
        const newServices = services.results
          .filter(
            (service: any) =>
              !service.pod_controller || this.isDeploymentMode(service),
          )
          .map((service: any) => ({
            uuid: service.uuid,
            display_name: this.getServiceDisplayName(service),
          }));

        return _.sortBy([...appServices, ...newServices], item => {
          return item.display_name;
        });
      });
    }
  }

  isDeploymentMode(service: any) {
    return !['StatefulSet', 'DaemonSet'].includes(service.pod_controller);
  }

  private getServiceDisplayName(service: any, app?: any) {
    if (app) {
      if (app.space_name) {
        return `${app.app_name}.${service.service_name} (${app.space_name})`;
      } else {
        return `${app.app_name}.${service.service_name}`;
      }
    } else {
      if (service.space_name) {
        return `${service.service_name} (${service.space_name})`;
      } else {
        return service.service_name;
      }
    }
  }

  getSeverityLevelOptions() {
    return [
      {
        name: 'Major',
        value: 'major',
      },
      {
        name: 'Minor',
        value: 'minor',
      },
    ];
  }

  getLogAlarmThresholdTypeList() {
    return [
      {
        name: this.translate.get('alarm_log_count_unit'),
        value: 'count',
      },
    ];
  }

  extractAlarmActions(alarm: any) {
    const alarmActions: any[] = [];
    const _alarm = _.cloneDeep(alarm);
    const triggerOptionMap = {
      alarm_actions: 'alarm_status_triggered',
      insufficient_actions: 'insufficient_data',
      ok_actions: 'alarm_status_released',
    };
    const actionsType = ['alarm_actions', 'insufficient_actions', 'ok_actions'];
    actionsType.forEach(type => {
      const notificationsType = `${type}.notifications`;
      const servicesType = `${type}.services`;
      _.get(_alarm, notificationsType, []).forEach(async (item: any) => {
        item.action_type = 'notification';
        item.trigger_option = triggerOptionMap[type];
        item.display_name = item.name;
        item.url = `notification/detail/${item.uuid}`;
        alarmActions.push(item);
      });
      _.get(_alarm, servicesType, []).forEach(async (item: any) => {
        item.action_type = item.type;
        item.trigger_option = triggerOptionMap[type];
        const [
          display_name,
          url,
        ] = await this.extractServiceDisplayNameAndUrlForActionResource(item);
        item.display_name = display_name;
        item.url = url;
        alarmActions.push(item);
      });
    });
    return alarmActions;
  }

  private async extractServiceDisplayNameAndUrlForActionResource(service: any) {
    let url, displayName;
    const oldApiVersion = service.name && service.name.includes('.');
    const isNewK8s = !!service.kube_namespace;
    if (!oldApiVersion) {
      const appName = service.app_name || '';
      const space = service.kube_namespace || service.space_name || '';
      displayName =
        (appName ? `${appName}.${service.name}` : service.name) +
        (space ? ` (${space})` : '');
      if (isNewK8s) {
        url = `k8s_app/detail/component/${service.uuid}`;
      } else {
        url = `app_service/service/service_detail/${service.uuid}`;
      }
    } else {
      const [space_name, app_name, service_name] = service.name.split('.');
      url = `app_service/service/service_detail/${service.uuid}`;
      if (app_name === '#') {
        displayName = `${service_name} (${space_name})`;
      } else {
        displayName = `${app_name}.${service_name} (${space_name})`;
      }
    }

    return [displayName, url];
  }

  isAlarmAckDisabled(alarm: any, child?: any) {
    if (child) {
      return _.get(child, 'actions.ack.status', false);
    } else {
      return _.every(alarm.children, (child: any) => {
        return (
          !!_.get(child, 'actions.ack.status', false) ||
          child.status !== 'ALARM'
        );
      });
    }
  }

  getAlarmAckToolTip(alarm: any, child: any) {
    if (child) {
      const ackStatus = _.get(child, 'actions.ack.status', false);
      return ackStatus
        ? this.translate.get('alarm_ack_sent')
        : this.translate.get('alarm_ack');
    } else {
      const flag = _.every(alarm.children, child => {
        return _.get(child, 'actions.ack.status', false);
      });
      return flag
        ? this.translate.get('alarm_ack_sent')
        : this.translate.get('alarm_ack_all');
    }
  }

  getAlarmTagsDisplay(tags: string[]) {
    if (!tags.length) {
      return '-';
    } else {
      return tags
        .map(tag => {
          let display_name;
          if (tag.indexOf(':') !== -1) {
            const [_key, item] = tag.split(':');
            display_name = this.getTagDisplayName(_key, item);
          } else {
            const [_key, item] = tag.split('=');
            display_name = this.getTagDisplayName(_key, item);
          }
          return display_name;
        })
        .join('<br>');
    }
  }

  private getTagDisplayName(key: string, item: string) {
    let display_name = item;
    if (key === 'service_name') {
      const arr = item.split('.');
      if (arr.length === 3) {
        // space_name.app_name.service_name
        const [space_name, app_name, service_name] = arr;
        if (app_name === '#') {
          display_name = `${service_name} (${space_name})`;
        } else {
          display_name = `${app_name}.${service_name} (${space_name})`;
        }
      }
    } else if (key === 'app_name') {
      // TODO: space_name.#.app_name is not a valid pattern , wati for API udpate
      const arr = item.split('.');
      if (arr.length === 3) {
        // space_name.#.app_name
        const [space_name, , app_name] = arr;
        if (space_name === '#') {
          display_name = app_name;
        } else {
          display_name = `${app_name} (${space_name})`;
        }
      } else if (arr.length === 2) {
        // 'space_name.app_name
        const [space_name, app_name] = arr;
        display_name = `${app_name} (${space_name})`;
      }
    }
    return `${key}:${display_name}`;
  }

  getAlarmTimeUnitDisplay(timestr: string) {
    if (!timestr) {
      return '-';
    }
    if (parseInt(timestr, 10) === +timestr) {
      return new DurationToConsumedPipe(this.translate).transform(
        parseInt(timestr, 10),
      );
    } else {
      const unitMap = {
        s: this.translate.get('second'),
        m: this.translate.get('minutes'),
        h: this.translate.get('hours'),
        d: this.translate.get('day'),
      };
      const arr = timestr.split('');
      const unit = arr.splice(-1, 1).join('');
      const time = arr.join('');
      return `${time} ${unitMap[unit]}`;
    }
  }

  getAlarmThresholdDisplay(unit: string, threshold: any) {
    if (unit && unit.toLowerCase() === 'percent') {
      let fixed = 0;
      const thresholdStr = threshold + '';
      if (thresholdStr.split('.').length > 1) {
        const decimalPart = thresholdStr.split('.')[1];
        fixed = decimalPart.length > 2 ? decimalPart.length - 2 : 0;
      }
      return +(threshold * 100).toFixed(fixed);
    } else {
      return threshold;
    }
  }

  getAlarmChildDisplayName(child: any, alarm_name: string) {
    if (!child.alert_key) {
      return alarm_name;
    } else {
      return child.alert_key
        .split(',')
        .map((tag: string) => {
          const [key, tag_value] = tag.split('=');
          return this.getTagDisplayName(key, tag_value);
        })
        .join(',');
    }
  }

  getAlarmTimestampUnitAndDisplayValue(time: number) {
    if (!time) {
      return [0, 's', 'second'];
    }
    const types = [
      {
        unit: 'd',
        readable_unit: 'day',
        factor: 86400,
      },
      {
        unit: 'h',
        readable_unit: 'hour',
        factor: 3600,
      },
      {
        unit: 'm',
        readable_unit: 'minute',
        factor: 60,
      },
      {
        unit: 's',
        readable_unit: 'second',
        factor: 1,
      },
    ];
    const type = _.find(types, type => {
      return time % type.factor === 0;
    });
    return type
      ? [time / type.factor, type.unit, type.readable_unit]
      : [0, 's', 'second'];
  }

  getAlarmActionDisplay(alarm: any) {
    const actions = [];
    const notifications = _.concat(
      _.get(alarm, 'alarm_actions.notifications', []),
      _.get(alarm, 'insufficient_actions.notifications', []),
      _.get(alarm, 'ok_actions.notifications', []),
    );
    const services = _.concat(
      _.get(alarm, 'alarm_actions.services', []),
      _.get(alarm, 'insufficient_actions.services', []),
      _.get(alarm, 'ok_actions.services', []),
    );
    if (notifications.length) {
      actions.push(this.translate.get('notification'));
    }
    const scalingInServices = services.filter(service => {
      return service.type === 'scaling_in';
    });
    const scalingOutServices = services.filter(service => {
      return service.type === 'scaling_out';
    });
    if (scalingInServices.length) {
      actions.push(this.translate.get('scaling_in'));
    }
    if (scalingOutServices.length) {
      actions.push(this.translate.get('scaling_out'));
    }
    return actions.length ? actions.join('<br>') : '-';
  }

  getLogAlarmPollingTypeList() {
    return [
      {
        name: this.translate.get('alarm_polling_standard'),
        value: 'interval',
      },
      {
        name: this.translate.get('alarm_polling_custom'),
        value: 'crontab',
      },
    ];
  }

  processAlarmActionsPayload(alarm: any, actions: any[]) {
    if (!alarm) {
      return;
    }
    alarm.alarm_actions = {
      notifications: [],
      services: [],
    };
    alarm.insufficient_actions = {
      notifications: [],
      services: [],
    };
    alarm.ok_actions = {
      notifications: [],
      services: [],
    };
    actions.forEach(item => {
      switch (item.trigger_option) {
        case 'alarm_status_triggered':
          this.addAlarmAction(alarm, 'alarm_actions', item);
          break;
        case 'insufficient_data':
          this.addAlarmAction(alarm, 'insufficient_actions', item);
          break;
        case 'alarm_status_released':
          this.addAlarmAction(alarm, 'ok_actions', item);
          break;
      }
    });
  }

  private addAlarmAction(alarm: any, triggerType: string, item: any) {
    switch (item.action_type) {
      case 'notification':
        alarm[triggerType]['notifications'].push({
          name: item.name,
          uuid: item.uuid,
        });
        break;
      case 'scaling_in':
      case 'scaling_out':
        alarm[triggerType]['services'].push({
          type: item.action_type,
          uuid: item.uuid,
        });
        break;
    }
  }

  preciseCalculateAlarmThreshold(threshold: any) {
    if (typeof threshold === 'number') {
      return parseFloat(threshold.toFixed(10));
    }
    return threshold;
  }
}
