import { NgModule } from '@angular/core';

import { AlarmRoutingModule } from 'app2/features/alarm/alarm-routing.module';
import { SharedModule } from 'app2/shared/shared.module';

import { AddAlarmActionComponent } from './add-alarm-action/add-alarm-action.component';
import { AlarmActionComponent } from './alarm-action/alarm-action.component';
import { AlarmStatusComponent } from './alarm-status/alarm-status.component';
import { AlarmUtilitiesService } from './alarm-utilities.service';
import { AlarmDashboardComponent } from './dashboard/alarm-dashboard.component';
import { CreateLogAlarmComponent } from './log-alarm/create/create-log-alarm.component';
import { LogAlarmDetailComponent } from './log-alarm/detail/log-alarm-detail.component';
import { LogAlarmListComponent } from './log-alarm/list/log-alarm-list.component';
import { LogAlarmFormComponent } from './log-alarm/log-alarm-form/log-alarm-form.component';
import { UpdateLogAlarmComponent } from './log-alarm/update/update-log-alarm.component';
import { AlarmFormComponent } from './metric-alarm/alarm-form/alarm-form.component';
import { CreateAlarmComponent } from './metric-alarm/create/create-alarm.component';
import { AlarmDetailComponent } from './metric-alarm/detail/alarm-detail.component';
import { AlarmListComponent } from './metric-alarm/list/alarm-list.component';
import { UpdateAlarmComponent } from './metric-alarm/update/update-alarm.component';

@NgModule({
  imports: [SharedModule, AlarmRoutingModule],
  declarations: [
    AlarmDashboardComponent,
    AlarmListComponent,
    LogAlarmListComponent,
    AlarmDetailComponent,
    AlarmStatusComponent,
    AlarmFormComponent,
    CreateAlarmComponent,
    UpdateAlarmComponent,
    AlarmActionComponent,
    AddAlarmActionComponent,
    LogAlarmFormComponent,
    CreateLogAlarmComponent,
    UpdateLogAlarmComponent,
    LogAlarmDetailComponent,
  ],
  entryComponents: [AddAlarmActionComponent],
  providers: [AlarmUtilitiesService],
})
export class AlarmModule {}
