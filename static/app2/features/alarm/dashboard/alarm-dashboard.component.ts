import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  templateUrl: './alarm-dashboard.component.html',
  styleUrls: ['./alarm-dashboard.component.scss'],
})
export class AlarmDashboardComponent implements OnInit, OnDestroy {
  labels = ['metric_alarm', 'log_alarm'];
  types = ['metric', 'log'];
  type: string;
  querySubscription: Subscription;
  constructor(private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.type = this.types[0];
    this.querySubscription = this.route.queryParamMap.subscribe(query => {
      const type = query.get('type');
      this.type = this.types.includes(type) ? type : this.type;
    });
  }

  select(selectedIndex: number) {
    this.type = this.types[selectedIndex];
    this.router.navigateByUrl(`/alarm?type=${this.type}`);
  }

  get index() {
    return this.types.indexOf(this.type);
  }

  ngOnDestroy() {
    this.querySubscription.unsubscribe();
  }
}
