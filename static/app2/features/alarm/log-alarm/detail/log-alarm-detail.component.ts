import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import moment from 'moment';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { AlarmUtilitiesService } from 'app2/features/alarm/alarm-utilities.service';
import { CrontabParsePipe } from 'app2/shared/pipes/crontab.pipe';
import { AlarmService } from 'app2/shared/services/features/alarm.service';
import { LogService } from 'app2/shared/services/features/log.service';
import { MonitorUtilitiesService } from 'app2/shared/services/features/monitor.utilities.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './log-alarm-detail.component.html',
  styleUrls: ['./log-alarm-detail.component.scss'],
})
export class LogAlarmDetailComponent implements OnInit, OnDestroy {
  loading = false;
  loadError = false;
  initialized = false;
  chartLoading = false;
  inAction = false;
  uuid: string;
  alarm: any;
  alarmDisplay: any;
  alarmActions: any[] = [];
  paramsSubscription: Subscription;
  chart: any;
  pollingTimer: any;
  statusMap = {
    ALARM: 'alarm',
    OK: 'ok',
    INVALID: 'invalid',
  };
  constructor(
    private route: ActivatedRoute,
    private alarmService: AlarmService,
    private alarmUtil: AlarmUtilitiesService,
    private translate: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private modal: ModalService,
    private auiNotificationService: NotificationService,
    private logService: LogService,
    private monitorUtil: MonitorUtilitiesService,
    private router: Router,
    private errorsToast: ErrorsToastService,
  ) {}

  ngOnInit() {
    this.chart = {
      data: [],
      scheme: { domain: ['#8BD6FA'] },
      xAxisTickFormatting: (val: number) => {
        return moment.unix(val).format('MM-DD HH:mm');
      },
      yAxisTickFormatting: (val: number) => {
        if (val >= 1000) {
          return val / 1000 + 'k';
        } else {
          return val;
        }
      },
    };
    this.paramsSubscription = this.route.params
      .pipe(first())
      .subscribe(async params => {
        this.uuid = params['uuid'];
        this.load();
      });
  }

  async load() {
    this.loading = true;
    try {
      const result = await this.alarmService.getLogAlarmDetail(this.uuid);
      this.alarm = result;
      this.alarm.canDelete = this.hasPermission(this.alarm, 'delete');
      this.alarm.canUpdate = this.hasPermission(this.alarm, 'update');
      this.alarm.canAck =
        this.alarm.canUpdate && ['ALARM'].includes(this.alarm.status);
      this.initAlarmDisplay();
      this.initQueryHistory();
      this.initAlarmActions();
      this.initialized = true;
      this.polling();
    } catch ({ status, errors }) {
      if (status === 403) {
        this.auiNotificationService.warning(
          this.translate.get('permission_denied'),
        );
      } else if (status === 404 && !this.initialized) {
        this.auiNotificationService.warning(
          this.translate.get('alarm_not_exist'),
        );
      }
      return this.router.navigateByUrl('alarm?type=log');
    }
    this.loading = false;
  }

  polling() {
    clearTimeout(this.pollingTimer);
    this.pollingTimer = setTimeout(() => {
      this.load();
    }, 30 * 1000);
  }

  async initAlarmDisplay() {
    this.alarmDisplay = {
      queryConditionTimeRange: this.logService.getQueryConditionTimeRangeDisplay(
        this.alarm.saved_search_detail,
        ' ',
      ),
      queryCondition: this.logService.getQueryConditionStringDisplay(
        this.alarm.saved_search_detail,
      ),
    };

    const thresholdTypeList = this.alarmUtil.getLogAlarmThresholdTypeList();
    const type = thresholdTypeList.find(
      item => item.value === this.alarm.statistic,
    )['name'];

    this.alarmDisplay.threshold = `${this.alarm.comparison_operator} ${
      this.alarm.threshold
    } ${type}`;

    if (this.alarm.check_mode === 'interval') {
      const intervalType = this.alarmUtil.getPeriodTypes().find((item: any) => {
        return item.value === +this.alarm.check_period_value;
      })['name'];
      this.alarmDisplay.schedule_rule = intervalType;
    } else {
      this.alarmDisplay.schedule_rule = `${this.alarm.check_period_value} 
      (${this.translate.get(
        'build_config_next_trigger_time',
      )} ${new CrontabParsePipe().transform(this.alarm.check_period_value)})`;
    }
  }

  async initQueryHistory() {
    const query_list = [
      `avg:log_alarm.search_result{log_alarm_id=${this.alarm.uuid}}by{}`,
    ];
    const end = Date.now();
    const weekOffset = 7 * 24 * 3600 * 1000;
    let offset;
    if (this.alarm.check_mode === 'interval') {
      offset = parseInt(this.alarm.check_period_value, 10) * 30 * 1000;
    } else {
      offset = weekOffset;
    }
    const timeRange = {
      start: end - offset,
      end,
    };
    const queryParams = {
      query_list,
      timeRange,
    };
    this.chartLoading = true;
    try {
      const { data } = await this.monitorUtil.getChartData(queryParams);
      this.chart.data = data;
    } catch (error) {}
    this.chartLoading = false;
  }

  initAlarmActions() {
    this.alarmActions = this.alarmUtil.extractAlarmActions(this.alarm);
  }

  hasPermission(item: any, action: string) {
    return this.roleUtil.resourceHasPermission(item, 'log_alarm', action);
  }

  updateAlarm() {
    this.router.navigate(['/alarm/log_alarm_update', this.alarm.uuid]);
  }

  async deleteAlarm() {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete_alarm_title'),
        content: this.translate.get('delete_alarm_content', {
          alarm_name: this.alarm.name,
        }),
      });
      this.inAction = false;
    } catch (error) {
      return false;
    }
    this.inAction = true;
    try {
      await this.alarmService.deleteLogAlarm(this.alarm.uuid);
      this.auiNotificationService.success(
        this.translate.get('alarm_delete_success'),
      );
      this.router.navigate(['/alarm']);
    } catch (error) {
      this.errorsToast.error(error);
    }
  }

  ngOnDestroy() {
    clearTimeout(this.pollingTimer);
  }
}
