import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import moment from 'moment';

import { NotificationService } from 'alauda-ui';
import { AlarmUtilitiesService } from 'app2/features/alarm/alarm-utilities.service';
import { AlarmService } from 'app2/shared/services/features/alarm.service';
import { LogService } from 'app2/shared/services/features/log.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-log-alarm-list',
  templateUrl: './log-alarm-list.component.html',
  styleUrls: ['../../metric-alarm/list/alarm-list.component.scss'],
})
export class LogAlarmListComponent implements OnInit, OnDestroy {
  hasCreateAlarmPermission = false;
  loading = false;
  initialized = false;
  destroyed = false;
  alarms: any[] = [];
  alarmsCache: any[] = [];
  emptyMessage = 'no_data';
  searching = false;
  pagination: any = {
    currentPage: 1,
    count: 0,
    size: 20,
  };
  statusMap = {
    ALARM: 'alarm',
    OK: 'ok',
    INVALID: 'invalid',
  };
  keyword = '';
  pollingTimer: any;
  constructor(
    private roleUtil: RoleUtilitiesService,
    private alarmService: AlarmService,
    private logService: LogService,
    private alarmUtil: AlarmUtilitiesService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private modal: ModalService,
    private router: Router,
    private errorToast: ErrorsToastService,
  ) {}

  async ngOnInit() {
    await this.load();
    this.hasCreateAlarmPermission = await this.roleUtil.resourceTypeSupportPermissions(
      'log_alarm',
    );
  }

  polling() {
    clearTimeout(this.pollingTimer);
    this.pollingTimer = setTimeout(() => {
      this.load();
    }, 30 * 1000);
  }

  searchChanged(keyword: string) {
    this.keyword = keyword;
    if (!keyword.length) {
      this.alarms = this.alarmsCache;
      this.polling();
      return;
    }
    this.alarms = this.alarmsCache.filter((alarm: any) => {
      return alarm.name.includes(keyword);
    });
    this.polling();
  }

  async load() {
    if (this.destroyed || this.loading) {
      return;
    }
    this.loading = true;
    try {
      const { result } = await this.alarmService.getLogAlarms({
        page: this.pagination.currentPage,
        page_size: this.pagination.size,
      });
      this.alarms = result.sort((prev: any, next: any) => {
        return moment(prev.created_at).isAfter(next.created_at) ? -1 : 1;
      });
      this.handleAlarms();
      this.alarmsCache = this.alarms;
      this.searchChanged(this.keyword);
    } catch (error) {
      this.errorToast.error(error);
    }
    this.loading = false;
    this.initialized = true;
  }

  handleAlarms() {
    this.alarms.forEach((alarm: any) => {
      alarm.canUpdate = this.hasPermission(alarm, 'update');
      alarm.canDelete = this.hasPermission(alarm, 'delete');
      alarm.inAction = false;
    });
  }

  queryConditionContent(item: any) {
    return this.logService.getQueryConditionValuesDisplay(item);
  }

  timeRange(item: any) {
    return this.logService.getQueryConditionTimeRangeDisplay(item);
  }

  getThresholdDisplay(alarm: any) {
    return `${alarm.comparison_operator} ${alarm.threshold}`;
  }

  getAlarmActionDisplay(alarm: any) {
    return this.alarmUtil.getAlarmActionDisplay(alarm);
  }

  hasPermission(item: any, action: string) {
    return this.roleUtil.resourceHasPermission(item, 'log_alarm', action);
  }

  viewDetail(uuid: string) {
    this.router.navigate(['/alarm/log_alarm_detail', uuid]);
  }

  createAlarm() {
    this.router.navigate(['/alarm/log_alarm_create']);
  }

  async delete(alarm: any) {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete_alarm_title'),
        content: this.translate.get('delete_alarm_content', {
          alarm_name: alarm.name,
        }),
      });
    } catch (error) {
      return false;
    }
    alarm.inAction = true;
    try {
      await this.alarmService.deleteLogAlarm(alarm.uuid);
      this.auiNotificationService.success(
        this.translate.get('alarm_delete_success'),
      );
      this.load();
    } catch (error) {
      this.errorToast.error(error);
    }
    alarm.inAction = false;
  }

  update(alarm: any) {
    this.router.navigate(['/alarm/log_alarm_update', alarm.uuid]);
  }

  trackByIndex(index: number) {
    return index;
  }

  ngOnDestroy() {
    clearTimeout(this.pollingTimer);
    this.destroyed = true;
  }
}
