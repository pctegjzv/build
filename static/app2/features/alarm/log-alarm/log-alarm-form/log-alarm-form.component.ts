import {
  AfterViewInit,
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';
import moment from 'moment';
import { BehaviorSubject, Subscription, fromEvent } from 'rxjs';
import { debounceTime, filter } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';
import { AlarmUtilitiesService } from 'app2/features/alarm/alarm-utilities.service';
import { CrontabParsePipe } from 'app2/shared/pipes/crontab.pipe';
import { AlarmService } from 'app2/shared/services/features/alarm.service';
import { LogService } from 'app2/shared/services/features/log.service';
import { QuotaSpaceService } from 'app2/shared/services/features/quota-space.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

import { RESOURCE_NAME_BASE } from '../../../../../app/components/common/config/common-pattern';

@Component({
  selector: 'rc-log-alarm-form',
  templateUrl: './log-alarm-form.component.html',
  styleUrls: [
    '../../metric-alarm/alarm-form/alarm-form.component.scss',
    './log-alarm-form.component.scss',
  ],
})
export class LogAlarmFormComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input()
  uuid = '';
  scrollContainer: HTMLElement;
  scrollSubscription: Subscription;
  scrolling = false;
  loading = false;
  destroyed = false;
  initialized = false;
  submitting = false;
  chartLoading = false;
  scrollBarWidth: number;
  xAxisTickFormatting: any;
  chart: any;
  paramsSubscription: Subscription;
  alarm: any = {};
  originAlarm: any = {};
  form: FormGroup;
  queryConditionSubject: BehaviorSubject<any>;
  queryConditionSubscription: Subscription;
  logQueryConditionList: any[] = [];
  logQueryConditionContent = '';
  logQueryConditionTimeRange = '';
  spaces: string[] = [];
  quotaEnabled = false;
  baseNamePattern = RESOURCE_NAME_BASE.pattern;
  errorsMapper: any;
  actions: any[] = [];
  thresholdTypeOptions: any[] = [];
  thresholdOperatorOptions: string[] = [];
  pollingTypeOptions: any[] = [];
  pollingIntervalOptions: any[] = [];
  selectedPollingType = '';
  crontabStr = '0 8 * * *';
  logTypes: any;
  queryLogid: string;
  @ViewChild('alarmForm')
  alarmForm: NgForm;
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private alarmUtil: AlarmUtilitiesService,
    private logService: LogService,
    private translate: TranslateService,
    private quotaSpaceService: QuotaSpaceService,
    private modal: ModalService,
    private alarmService: AlarmService,
    private auiNotificationService: NotificationService,
    private router: Router,
    private errorsToast: ErrorsToastService,
    @Inject(WEBLABS) public weblabs: Weblabs,
  ) {}

  async ngOnInit() {
    const quuid = this.route.snapshot.paramMap.get('quuid');
    const service = this.route.snapshot.paramMap.get('service');
    if (quuid) {
      this.queryLogid = quuid;
    }
    if (service) {
      const serviceData = JSON.parse(service);
      this.actions.push({
        action_type: serviceData.action_type,
        trigger_option: 'alarm_status_triggered',
        uuid: serviceData.uuid,
        display_name: serviceData.display_name,
      });
    }

    this.chart = {
      totalCount: 0,
      timeRangeType: 'last_30_minites',
      data: [],
      scheme: { domain: ['#8BD6FA'] },
      roundDomains: true,
      roundEdges: false,
      barPadding: 8,
      xAxis: true,
      yAxis: true,
      xAxisTickFormatting: (val: Date) => {
        return moment(val).format('MM-DD HH:mm');
      },
      yAxisTickFormatting: (val: number) => {
        if (val >= 1000) {
          return val / 1000 + 'k';
        } else {
          return val;
        }
      },
    };
    this.errorsMapper = {
      name: { pattern: this.translate.get(RESOURCE_NAME_BASE.tip) },
      number: { pattern: this.translate.get('invalid_pattern') },
      crontab: {
        crontabInvalid: this.translate.get('invalid_pattern'),
        cronFormatError: this.translate.get('cron_format_error'),
        cronMinIntervalError: this.translate.get('cron_min_interval_error', {
          minInterval: 60,
        }),
      },
    };
    this.thresholdTypeOptions = this.alarmUtil.getLogAlarmThresholdTypeList();
    this.thresholdOperatorOptions = this.alarmUtil.getThresholdOperatorList();
    this.pollingTypeOptions = this.alarmUtil.getLogAlarmPollingTypeList();
    this.pollingIntervalOptions = this.alarmUtil.getPeriodTypes();
    this.loading = true;
    this.quotaEnabled = !!this.weblabs.QUOTA_ENABLED;
    await Promise.all([this.getQueryConditions(), this.initQuotaSpace()]).catch(
      () => {
        this.loading = false;
      },
    );
    this.initLogTypes();
    if (this.updating) {
      await this.getAlarmData();
    }
    this.initForm();
    this.initObservables();

    this.loading = false;
    this.initialized = true;
  }

  async initQuotaSpace() {
    if (this.quotaEnabled && !this.updating) {
      try {
        this.spaces = _.map(
          await this.quotaSpaceService.getConsumableSpaces(),
          (space: any) => space.name,
        );
      } catch (error) {
        this.spaces = [];
      }
    }
  }

  async getAlarmData() {
    try {
      const result = await this.alarmService.getLogAlarmDetail(this.uuid);
      // Init alarm
      this.originAlarm = result;
      this.actions = this.alarmUtil.extractAlarmActions(result);
    } catch ({ errors }) {
      this.loading = false;
    }
  }

  ngAfterViewInit() {
    this.scrollContainer = document.querySelector('.page-scroll-container');
    this.scrollSubscription = fromEvent(this.scrollContainer, 'scroll')
      .pipe(debounceTime(100))
      .subscribe((event: any) => {
        const target = event.target;
        if (!this.scrollBarWidth) {
          this.scrollBarWidth = target.offsetWidth - target.clientWidth;
        }
        this.scrolling = target.scrollTop > 0;
      });
  }

  get updating() {
    return !!this.uuid.length;
  }

  async getQueryConditions() {
    try {
      const { result } = await this.logService.getLogQueryConditions({
        all: true,
      });
      result.forEach((item: any) => {
        if (item.end_time !== 0) {
          item.disabled = true;
          const display_name = item.display_name;
          const timeRange = `${this.translate.get('from')} ${moment(
            item.start_time * 1000,
          ).format('YYYY-MM-DD HH:mm:ss')}${this.translate.get('to')} ${moment(
            item.end_time * 1000,
          ).format('YYYY-MM-DD HH:mm:ss')}`;
          item.display_name = `${display_name} (${timeRange})`;
        }
      });
      this.logQueryConditionList = result.filter((item: any) => {
        return !item.disabled;
      });
    } catch ({ errors }) {}
  }

  async initLogTypes() {
    try {
      this.logTypes = await this.logService.getTypes();
    } catch (e) {
      this.logTypes = {};
      this.loading = false;
    }
  }

  initForm() {
    this.form = this.fb.group({
      logQueryCondition: this.originAlarm.saved_search_detail
        ? this.originAlarm.saved_search_detail.uuid
        : this.queryLogid ||
          (this.logQueryConditionList.length
            ? this.logQueryConditionList[0].uuid
            : ''),
      quotaSpace:
        this.originAlarm.space_name || this.spaces.length ? this.spaces[0] : '',
      alarmName: this.originAlarm.name || '',
      description: this.originAlarm.description || '',
      thresholdType:
        this.originAlarm.statistic || this.thresholdTypeOptions[0].value,
      thresholdOperator:
        this.originAlarm.comparison_operator ||
        this.thresholdOperatorOptions[0],
      threshold: this.originAlarm.threshold || '',
      pollingType: '',
      pollingInterval: this.pollingIntervalOptions[0].value,
      pollingRuleCustom: this.crontabStr,
    });

    if (this.updating) {
      if (this.originAlarm.check_mode === 'interval') {
        this.form
          .get('pollingInterval')
          .setValue(this.originAlarm.check_period_value);
      } else {
        this.form
          .get('pollingRuleCustom')
          .setValue(this.originAlarm.check_period_value);
      }
    }

    setTimeout(() => {
      const pollingType =
        this.originAlarm.check_mode || this.pollingTypeOptions[0].value;
      this.form.get('pollingType').setValue(pollingType);
      this.selectedPollingType = pollingType;
    }, 0);
  }

  initObservables() {
    this.queryConditionSubject = new BehaviorSubject(null);
    this.queryConditionSubscription = this.queryConditionSubject
      .pipe(
        filter((item: any) => !!item),
        debounceTime(800),
      )
      .subscribe((item: any) => {
        this.getChartData(item);
        this.getQueryConditionStringDisplay(item);
        this.getQueryConditionTimeRangeDisplay(item);
      });
  }

  onLogQueryConditionChange(opt: any) {
    this.queryConditionSubject.next(opt);
  }

  async getChartData(queryCondition: any) {
    this.chartLoading = true;
    const {
      tags,
      timeRangeType,
      timeRange,
    } = this.logService.parseQueryConditionToTagsAndTimeRange(queryCondition);
    const tagsQueryParams =
      this.logService.generateQuerysFromTagObjects(tags) || {};
    const params: any = Object.assign(tagsQueryParams, timeRange);
    // 补充 paths
    if (!params.paths) {
      params.paths = _.get(this.logTypes, 'paths', []).join(',') || 'stdout';
    }

    // 补充 query_string
    if (params.search) {
      params.query_string = params.search;
      delete params.search;
    }
    params.start_time /= 1000;
    params.end_time /= 1000;

    this.chartLoading = true;
    try {
      const { buckets } = await this.logService.getAggregations(params);
      if (this.destroyed || !buckets || !buckets.length) {
        return;
      }
      const data = buckets.map((item: any) => {
        return {
          value: item.count,
          name: item.time * 1000,
        };
      });
      const totalCount = buckets.reduce(
        (result, bucket) => result + bucket.count,
        0,
      );
      this.chart.data = data;
      this.chart.totalCount = totalCount;
      this.chart.timeRangeType = this.translate.get(timeRangeType);
    } catch (error) {}
    this.chartLoading = false;
  }

  getChartTitle() {
    return this.translate.get('alarm_log_aggregation_title', {
      time_range: this.chart.timeRangeType,
      count: this.chart.totalCount,
    });
  }

  getQueryConditionStringDisplay(condition: any) {
    this.logQueryConditionContent = this.logService.getQueryConditionStringDisplay(
      condition,
    );
  }

  getQueryConditionTimeRangeDisplay(condition: any) {
    this.logQueryConditionTimeRange = this.logService.getQueryConditionTimeRangeDisplay(
      condition,
      ' ',
    );
  }

  onAlarmActionAdd(actions: any[]) {
    this.actions = actions;
  }

  onPollingTypeChange(option: any) {
    this.selectedPollingType = option.value;
  }

  parseCrontabText(_crontab: string) {
    return `${this.translate.get(
      'build_config_next_trigger_time',
    )}：${new CrontabParsePipe().transform(this.crontabStr)}`;
  }

  getChartTooltipDisplay(time: Date) {
    return moment(time).format('MM-DD HH:mm');
  }

  async submit() {
    this.alarmForm.onSubmit(null);
    if (this.form.invalid || this.submitting) {
      return;
    }
    const values = this.form.value;
    try {
      await this.modal.confirm({
        title: this.translate.get('alarm'),
        content: this.translate.get(
          this.updating ? 'alarm_confirm_update' : 'alarm_confirm_create',
          {
            alarm_name: values.alarmName,
          },
        ),
      });
    } catch (error) {
      return false;
    }

    // build alarm
    Object.assign(this.alarm, {
      check_mode: values.pollingType,
      check_period_value:
        values.pollingType === 'interval'
          ? values.pollingInterval
          : values.pollingRuleCustom,
      comparison_operator: values.thresholdOperator,
      statistic: values.thresholdType,
      space_name: values.quotaSpace,
      description: values.description,
      name: values.alarmName || this.originAlarm.name,
      saved_search_uuid: values.logQueryCondition,
      threshold: values.threshold,
    });
    if (this.quotaEnabled) {
      this.alarm.space_name = values.quotaSpace || this.originAlarm.space_name;
    }
    this.alarmUtil.processAlarmActionsPayload(this.alarm, this.actions);
    this.submitting = true;
    try {
      const res = this.updating
        ? await this.alarmService.updateLogAlarm(this.uuid, this.alarm)
        : await this.alarmService.createLogAlarm(this.alarm);
      this.router.navigate(['/alarm/log_alarm_detail', res.uuid]);
      this.auiNotificationService.success(
        this.translate.get(this.updating ? 'update_success' : 'create_success'),
      );
    } catch (error) {
      this.errorsToast.error(error);
    }
    this.submitting = false;
  }

  cancel() {
    this.router.navigateByUrl('/alarm?type=log');
  }

  ngOnDestroy() {
    this.destroyed = true;
    if (this.queryConditionSubscription) {
      this.queryConditionSubscription.unsubscribe();
    }
  }
}
