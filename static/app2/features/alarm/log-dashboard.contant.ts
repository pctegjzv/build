import moment from 'moment';

export const TIME_STAMP_OPTIONS = [
  {
    type: 'custom_time_range',
    offset: 0,
  },
  {
    type: 'last_10_minites',
    offset: 10 * 60 * 1000,
  },
  {
    type: 'last_30_minites',
    offset: 30 * 60 * 1000,
  },
  {
    type: 'last_hour',
    offset: 60 * 60 * 1000,
  },
  {
    type: 'last_3_hour',
    offset: 3 * 3600 * 1000,
  },
  {
    type: 'last_6_hour',
    offset: 6 * 3600 * 1000,
  },
  {
    type: 'last_12_hour',
    offset: 12 * 3600 * 1000,
  },
  {
    type: 'last_day',
    offset: 24 * 3600 * 1000,
  },
  {
    type: 'last_2_days',
    offset: 2 * 24 * 3600 * 1000,
  },
  {
    type: 'last_3_days',
    offset: 3 * 24 * 3600 * 1000,
  },
  {
    type: 'last_5_days',
    offset: 5 * 24 * 3600 * 1000,
  },
  {
    type: 'last_7_days',
    offset: 7 * 24 * 3600 * 1000,
  },
];

export const LOG_CHART_DEFAULT_CONFIG = {
  credits: false,
  chart: {
    type: 'column',
    marginBottom: 24,
  },
  title: {
    align: 'left',
    style: {
      fontSize: '10px',
    },
  },
  legend: {
    enabled: false,
  },
  xAxis: {
    labels: {
      step: 4,
      formatter: function thisXAxisFormatter() {
        return moment(this.value).format('MM-DD HH:mm:ss');
      },
      style: {
        fontSize: '8px',
      },
    },
  },
  yAxis: {
    allowDecimals: false,
    title: {
      text: '',
    },
    tickAmount: 4,
    labels: {
      style: {
        fontSize: '8px',
      },
    },
  },
  plotOptions: {
    series: {
      cursor: 'pointer',
      point: { events: {} },
      tooltip: {
        headerFormat:
          '<span style="font-size: 12px">{point.y} Logs</span><br/>',
        pointFormatter: function tooltipFormatter() {
          const str = moment(this.category).format('YYYY-MM-DD HH:mm:ss');
          return '<span style="font-size: 10px">' + str + '</span>';
        },
      },
    },
  },
  series: [
    {
      name: 'Logs',
    },
  ],
};

export const LOG_ALARM_CHART_DEFAULT_CONFIG = {
  credits: false,
  chart: {
    type: 'column',
    marginBottom: 24,
  },
  title: {
    align: 'center',
    style: {
      fontSize: '14px',
    },
  },
  legend: {
    enabled: false,
  },
  xAxis: {
    labels: {
      step: 4,
      formatter: function thisXAxisFormatter() {
        return moment(this.value).format('MM-DD HH:mm:ss');
      },
      style: {
        fontSize: '12px',
      },
    },
  },
  yAxis: {
    allowDecimals: false,
    title: {
      text: '',
    },
    tickAmount: 4,
    labels: {
      style: {
        fontSize: '12px',
      },
    },
  },
  plotOptions: {
    series: {
      cursor: 'pointer',
      point: { events: {} },
      tooltip: {
        headerFormat:
          '<span style="font-size: 12px">{point.y} Logs</span><br/>',
        pointFormatter: function tooltipFormatter() {
          const str = moment(this.category).format('YYYY-MM-DD HH:mm:ss');
          return '<span style="font-size: 10px">' + str + '</span>';
        },
      },
    },
  },
  series: [
    {
      name: 'Logs',
    },
  ],
};
