import {
  AfterViewInit,
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';
import moment from 'moment';
import {
  BehaviorSubject,
  Observable,
  Subscription,
  combineLatest,
  fromEvent,
} from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  skip,
  startWith,
} from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { ENVIRONMENTS, WEBLABS } from 'app2/core/tokens';
import { Environments, Weblabs } from 'app2/core/types';
import { AlarmUtilitiesService } from 'app2/features/alarm/alarm-utilities.service';
import { AlarmService } from 'app2/shared/services/features/alarm.service';
import { MonitorService } from 'app2/shared/services/features/monitor.service';
import { MonitorUtilitiesService } from 'app2/shared/services/features/monitor.utilities.service';
import { QuotaSpaceService } from 'app2/shared/services/features/quota-space.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

import { RESOURCE_NAME_BASE } from '../../../../../app/components/common/config/common-pattern';

@Component({
  selector: 'rc-alarm-form',
  templateUrl: './alarm-form.component.html',
  styleUrls: ['./alarm-form.component.scss'],
})
export class AlarmFormComponent implements AfterViewInit, OnDestroy, OnInit {
  @Input()
  uuid = '';
  scrollContainer: HTMLElement;
  scrollSubscription: Subscription;
  scrolling = false;
  loading = false;
  initialized = false;
  submitting = false;
  scrollBarWidth: number;
  thresholdUnit = '';
  threshodUnitDisplay = '';
  shouldShowThresholdUnit = false;
  querySubscription: Subscription;
  alarm: any = {};
  originAlarm: any = {};
  initialValues: any = {
    metric_name: '',
    aggregator: '',
    filterTags: [],
    group_by: [],
  };
  allTags: any[];
  chartLoading = false;
  actions: any[] = [];
  chart: any = {
    data: [],
  };
  form: FormGroup;
  metricOptions: any[] = [];
  metricDisplay: string;
  aggregatorOptions: any[] = [];
  metricGroupOptions: any[] = [];
  metricFilterTagOptions: any[] = [];
  metricExcludeTagOptions: any[] = [];
  severityLevelOptions: any[] = [];
  spaces: string[] = [];
  periods: any[] = [];
  thresholdOperatorOptions: string[] = [];
  notifyIntervalOptions: any[] = [];
  quotaEnabled = false;
  baseNamePattern = RESOURCE_NAME_BASE.pattern;
  errorsMapper: any;
  chartSubscription: Subscription;
  aggregatorObservable: Observable<string>;
  filterTagsObservable: Observable<string[]>;
  excludeTagsObservable: Observable<string[]>;
  periodSubject: BehaviorSubject<number>;
  groupByObservable: Observable<string[]>;
  metricSubscription: Subscription;
  notifyIntervalFactor = 60;
  severityLevelEnabled = false;
  @ViewChild('alarmForm')
  alarmForm: NgForm;
  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private monitorService: MonitorService,
    private monitorUtil: MonitorUtilitiesService,
    private translate: TranslateService,
    private alarmUtil: AlarmUtilitiesService,
    private quotaSpaceService: QuotaSpaceService,
    private alarmService: AlarmService,
    private modal: ModalService,
    private auiNotificationService: NotificationService,
    private router: Router,
    private errorsToast: ErrorsToastService,
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {}

  async ngOnInit() {
    this.loading = true;
    this.severityLevelEnabled = this.env.alarm_severity_level_enabled;
    this.querySubscription = this.route.queryParams.subscribe(query => {
      this.initValues(query);
    });
    this.aggregatorOptions = this.monitorUtil.getMonitorAggregatorOptions();
    this.thresholdOperatorOptions = this.alarmUtil.getThresholdOperatorList();
    this.notifyIntervalOptions = this.alarmUtil.getNotifyIntervalOptions();
    this.periods = this.alarmUtil.getPeriodTypes();
    this.severityLevelOptions = this.alarmUtil.getSeverityLevelOptions();
    this.errorsMapper = {
      name: { pattern: this.translate.get(RESOURCE_NAME_BASE.tip) },
      number: { pattern: this.translate.get('invalid_pattern') },
    };
    this.quotaEnabled = !!this.weblabs.QUOTA_ENABLED;
    if (this.quotaEnabled && !this.updating) {
      this.spaces = _.map(
        await this.quotaSpaceService.getConsumableSpaces(),
        (space: any) => space.name,
      );
    }
    await this.getMetrics();
    if (this.updating) {
      await this.getAlarmData();
    } else {
      await this.getMetricData(
        this.initialValues.metric_name || this.metricOptions[0].metric_name,
      );
    }
    this.initForm();
    this.initObservables();
    this.loading = false;
    this.initialized = true;
  }

  get updating() {
    return !!this.uuid.length;
  }

  // Get data when update
  async getAlarmData() {
    try {
      const result = await this.alarmService.getAlarmDetail(this.uuid);
      await this.getMetricData(result.metric_name);
      // Init alarm
      this.originAlarm = {
        metric: result.metric_name,
        aggregator: result.statistic,
        filterTags: result.tags.map((tag: string) => {
          return tag.replace(':', '=');
        }),
        excludedTags: result.exclude_tags.map((tag: string) => {
          return tag.replace(':', '=');
        }),
        thresholdOperator: result.comparison_operator,
        alarmDescription: result.description,
        threshold: this.alarmUtil.getAlarmThresholdDisplay(
          this.thresholdUnit,
          result.threshold,
        ),
        groupBy: result.group_by.split(','),
        quotaSpace: result.space_name,
        alarmName: result.name,
        period: result.period,
      };
      if (!result.notify_interval) {
        this.originAlarm.notifyIntervalUnit = 'm';
      } else {
        const [
          displayValue,
          unit,
        ] = this.alarmUtil.getAlarmTimestampUnitAndDisplayValue(
          result.notify_interval,
        );
        this.originAlarm.notifyIntervalUnit = unit;
        this.originAlarm.notifyInterval = displayValue;
      }
      this.actions = this.alarmUtil.extractAlarmActions(result);
    } catch ({ errors }) {}
  }

  initValues(query: any) {
    if (query['service']) {
      const service = JSON.parse(query['service']);
      this.actions.push({
        action_type: service.action_type,
        trigger_option: 'alarm_status_triggered',
        uuid: service.uuid,
        display_name: service.display_name,
      });
    }
    if (query['metric']) {
      const metric = JSON.parse(query['metric']);
      metric.tags = metric.tags
        .filter((tag: string) => !!tag.length)
        .map((tag: string) => tag.replace('cluster_name', 'region_name'));
      this.initialValues = {
        metric_name: metric.metric_name,
        aggregator: metric.aggregator,
        filterTags: metric.tags || [],
        group_by: metric.group_by ? metric.group_by.split(',') : [],
      };
    }
  }

  initForm() {
    if (this.originAlarm.threshold === 0) {
      this.originAlarm.threshold = '0';
    }
    this.form = this.fb.group({
      metric:
        this.originAlarm.metric ||
        this.initialValues.metric_name ||
        this.metricOptions[0].metric_name,
      aggregator:
        this.originAlarm.aggregator ||
        this.initialValues.aggregator ||
        this.aggregatorOptions[0].value,
      filterTags: [
        this.originAlarm.filterTags || this.initialValues.filterTags,
      ],
      excludeTags: [this.originAlarm.excludedTags || []],
      groupBy: [this.originAlarm.groupBy || this.initialValues.group_by],
      quotaSpace: this.spaces.length ? this.spaces[0] : '',
      alarmName: '',
      severityLevel:
        this.originAlarm.severity_level || this.severityLevelOptions[0].value,
      alarmDescription: this.originAlarm.alarmDescription || '',
      period: this.originAlarm.period || this.periods[0].value,
      thresholdOperator:
        this.originAlarm.thresholdOperator || this.thresholdOperatorOptions[0],
      threshold: this.originAlarm.threshold || '',
      notifyInterval: this.originAlarm.notifyInterval || '',
      notifyIntervalUnit:
        this.originAlarm.notifyIntervalUnit ||
        this.notifyIntervalOptions[1].readable_value,
    });
  }

  ngAfterViewInit() {
    this.scrollContainer = document.querySelector('.page-scroll-container');
    this.scrollSubscription = fromEvent(this.scrollContainer, 'scroll')
      .pipe(debounceTime(100))
      .subscribe((event: any) => {
        const target = event.target;
        if (!this.scrollBarWidth) {
          this.scrollBarWidth = target.offsetWidth - target.clientWidth;
        }
        this.scrolling = target.scrollTop > 0;
      });
  }

  initObservables() {
    // metric
    this.metricSubscription = this.form
      .get('metric')
      .valueChanges.pipe(
        distinctUntilChanged(),
        skip(1),
      )
      .subscribe(async (value: string) => {
        await this.getMetricData(value);
      });
    // aggregator
    this.aggregatorObservable = this.form
      .get('aggregator')
      .valueChanges.pipe(distinctUntilChanged());

    this.filterTagsObservable = this.form.get('filterTags').valueChanges.pipe(
      startWith(
        this.originAlarm.filterTags || this.initialValues.filterTags || [],
      ),
      map((tags: any[]) => {
        const filterKeys: string[] = _.map(
          tags,
          (tag: string) => tag.split('=')[0],
        );
        this.metricExcludeTagOptions = _.filter(
          this.allTags,
          (excludeTag: any) => {
            const [key] = excludeTag.value.split('=');
            return _.indexOf(filterKeys, key) === -1;
          },
        );
        return tags;
      }),
    );

    this.excludeTagsObservable = this.form
      .get('excludeTags')
      .valueChanges.pipe(startWith(this.originAlarm.excludedTags || []));

    this.groupByObservable = this.form
      .get('groupBy')
      .valueChanges.pipe(
        startWith(
          this.originAlarm.groupBy || this.initialValues.group_by || [],
        ),
      );

    this.periodSubject = new BehaviorSubject(this.periods[0].value);

    this.chartSubscription = combineLatest(
      this.periodSubject.pipe(
        distinctUntilChanged(),
        filter((value: number) => !!value),
      ),
      this.aggregatorObservable,
      this.filterTagsObservable,
      this.excludeTagsObservable,
      this.groupByObservable,
    ).subscribe(([period, aggregator, filterTags, excludeTags, groupBy]) => {
      this.getChartData(period, aggregator, filterTags, excludeTags, groupBy);
    });
  }

  async getMetrics() {
    try {
      const metrics = await this.monitorService.getMetrics();
      this.metricOptions = this.monitorUtil.getMonitorMetricOptions(
        metrics.sort(),
      );
    } catch ({ errors }) {}
  }

  async getMetricData(metricName: string) {
    this.metricDisplay = '';
    this.alarm.metric_name = metricName;
    try {
      const metric = await this.monitorService.getMetricsDetail({
        metrics: metricName,
      });
      if (metric) {
        this.metricDisplay = metric.display_name;
        this.thresholdUnit = metric.unit || '';
        this.shouldShowThresholdUnit = metric.unit && metric.unit !== 'UNKNOWN';
        this.threshodUnitDisplay =
          this.thresholdUnit.toLowerCase() === 'percent'
            ? '%'
            : this.thresholdUnit;
        const tags = this.monitorUtil.getMonitorSourceOptions(metric);
        this.allTags = tags;
        this.metricGroupOptions = this.monitorUtil.getMonitorGroupOption(
          metric,
        );
        this.metricFilterTagOptions = _.cloneDeep(tags);
        this.metricExcludeTagOptions = _.cloneDeep(tags);
      } else {
        this.metricGroupOptions = [];
        this.metricFilterTagOptions = [];
        this.metricExcludeTagOptions = [];
      }
    } catch (error) {}
  }

  getChartTooltipDisplay(time: number) {
    return moment.unix(time).format('MM-DD HH:mm');
  }

  async getChartData(
    period: number,
    aggregator: string,
    filterTags: string[],
    excludeTags: string[],
    groupBy: string[],
  ) {
    if (!this.alarm.metric_name) {
      return;
    }
    this.chartLoading = true;
    this.alarm.statistic = aggregator;
    const metrics = [
      {
        metric: this.alarm.metric_name,
        over: filterTags.join(','),
        exclude: excludeTags.join(','),
        aggregator: aggregator,
        group_by: groupBy.join(','),
      },
    ];
    const end = moment().valueOf();
    const timeRange = {
      start: end - 30 * period * 1000,
      end,
    };
    try {
      const result = await this.monitorUtil.getChartData({
        metrics,
        timeRange,
        cache: true,
      });
      this.chart = result;
    } catch ({ errors }) {}
    this.chart.data.forEach((item: any, index: number) => {
      item.name = item.name || `Series ${index}`;
    });
    this.chartLoading = false;
  }

  onNotifyIntervalChange(opt: any) {
    this.notifyIntervalFactor = opt.value;
  }

  onPeriodChange(opt: any) {
    this.periodSubject.next(opt.value);
  }

  onAlarmActionAdd(actions: any[]) {
    this.actions = actions;
  }

  get scrollPaddingRight(): number {
    return 32 + 20 + this.scrollBarWidth;
  }

  get scrollPaddingLeft(): number {
    return 32 + 20;
  }

  async submit() {
    this.alarmForm.onSubmit(null);
    if (this.form.invalid || this.submitting) {
      return;
    }
    const values = this.form.value;
    try {
      await this.modal.confirm({
        title: this.translate.get('alarm'),
        content: this.translate.get(
          this.updating ? 'alarm_confirm_update' : 'alarm_confirm_create',
          {
            alarm_name: values.alarmName,
          },
        ),
      });
    } catch (error) {
      return false;
    }
    // build alarm
    Object.assign(this.alarm, {
      comparison_operator: values.thresholdOperator,
      threshold:
        this.thresholdUnit.toLowerCase() === 'percent'
          ? this.alarmUtil.preciseCalculateAlarmThreshold(
              values.threshold / 100,
            )
          : values.threshold,
      exclude_tags: values.excludeTags.map((tag: string) => {
        return tag.replace('=', ':');
      }),
      group_by: values.groupBy.join(','),
      notify_interval: values.notifyInterval
        ? values.notifyInterval * this.notifyIntervalFactor
        : 0,
      period: values.period,
      tags: values.filterTags.map((tag: string) => {
        return tag.replace('=', ':');
      }),
      description: values.alarmDescription,
      name: values.alarmName || this.originAlarm.alarmName,
    });
    if (this.severityLevelEnabled) {
      this.alarm.severity_level = values.severityLevel;
    }
    if (this.quotaEnabled) {
      this.alarm.space_name = values.quotaSpace || this.originAlarm.quotaSpace;
    }
    this.alarmUtil.processAlarmActionsPayload(this.alarm, this.actions);
    this.submitting = true;
    try {
      const res = this.updating
        ? await this.alarmService.updateAlarm(this.uuid, this.alarm)
        : await this.alarmService.createAlarm(this.alarm);
      this.router.navigate(['/alarm/alarm_detail', res.uuid]);
      this.auiNotificationService.success(
        this.translate.get(this.updating ? 'update_success' : 'create_success'),
      );
    } catch (error) {
      this.errorsToast.error(error);
    }
    this.submitting = false;
  }

  cancel() {
    this.router.navigate(['/alarm']);
  }

  ngOnDestroy() {
    this.scrollSubscription.unsubscribe();
    this.chartSubscription.unsubscribe();
    this.querySubscription.unsubscribe();
  }
}
