import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { AlarmUtilitiesService } from 'app2/features/alarm/alarm-utilities.service';
import { AlarmService } from 'app2/shared/services/features/alarm.service';
import { MonitorService } from 'app2/shared/services/features/monitor.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './alarm-detail.component.html',
})
export class AlarmDetailComponent implements OnInit, OnDestroy {
  loading = false;
  loadError = false;
  initialized = false;
  inAction = false;
  uuid: string;
  alarm: any;
  alarmDisplay: any;
  metricsDetail: any;
  alarmActions: any[] = [];
  paramsSubscription: Subscription;
  statusMap = {
    OK: 'ok',
    ALARM: 'alarm',
    INSUFFICIENT_DATA: 'insufficient_data',
    INSUFFICIENT_RES: 'insufficient_res',
  };
  pollingTimer: any;
  constructor(
    private route: ActivatedRoute,
    private alarmService: AlarmService,
    private alarmUtil: AlarmUtilitiesService,
    private translate: TranslateService,
    private monitorService: MonitorService,
    private roleUtil: RoleUtilitiesService,
    private modal: ModalService,
    private auiNotificationService: NotificationService,
    private router: Router,
    private errorToast: ErrorsToastService,
  ) {}

  ngOnInit() {
    this.paramsSubscription = this.route.params
      .pipe(first())
      .subscribe(async params => {
        this.uuid = params['uuid'];
        this.load();
      });
  }

  async load() {
    this.loading = true;
    try {
      const result = await this.alarmService.getAlarmDetail(this.uuid);
      this.alarm = result;
      this.alarm.canDelete = this.hasPermission(this.alarm, 'delete');
      this.alarm.canUpdate = this.hasPermission(this.alarm, 'update');
      this.alarm.canAck =
        this.alarm.canUpdate && ['ALARM'].includes(this.alarm.status);
      await this.initAlarmDisplay();
      this.initAlarmActions();
      this.initialized = true;
      this.polling();
    } catch ({ status, errors }) {
      if (status === 403) {
        this.auiNotificationService.warning(
          this.translate.get('permission_denied'),
        );
      } else if (status === 404 && !this.initialized) {
        this.auiNotificationService.warning(
          this.translate.get('alarm_not_exist'),
        );
      }
      return this.router.navigateByUrl('alarm');
    }
    this.loading = false;
  }

  polling() {
    clearTimeout(this.pollingTimer);
    this.pollingTimer = setTimeout(() => {
      this.load();
    }, 30 * 1000);
  }

  async initAlarmDisplay() {
    this.alarmDisplay = {
      threshold_unit: '',
      group_by: this.alarm.group_by,
      tags: this.alarmUtil.getAlarmTagsDisplay(this.alarm.tags),
      exclude_tags: this.alarmUtil.getAlarmTagsDisplay(this.alarm.exclude_tags),
    };

    const [
      period_value,
      period_unit,
    ] = this.alarmUtil.getAlarmTimestampUnitAndDisplayValue(this.alarm.period);
    this.alarmDisplay.period =
      period_value + this.translate.get(period_unit.toString());
    const [
      interval_value,
      interval_unit,
    ] = this.alarmUtil.getAlarmTimestampUnitAndDisplayValue(
      this.alarm.notify_interval,
    );
    this.alarmDisplay.notify_interval = !interval_value
      ? '-'
      : interval_value + this.translate.get(interval_unit.toString());

    if (this.alarm.children) {
      this.alarm.children.forEach((child: any) => {
        child.display_name = this.alarmUtil.getAlarmChildDisplayName(
          child,
          this.alarm.name,
        );
        child.canAck = this.alarm.canUpdate && ['ALARM'].includes(child.status);
        child.ackDisabled = this.alarmUtil.isAlarmAckDisabled(
          this.alarm,
          child,
        );
        child.inAction = false;
      });
    }

    if (!this.metricsDetail) {
      try {
        this.metricsDetail = await this.monitorService.getMetricsDetail({
          metrics: this.alarm.metric_name,
          cache: true,
        });
        if (this.metricsDetail && this.metricsDetail.unit === 'UNKNOWN') {
          this.metricsDetail.unit = '';
        }
      } catch (error) {}
    }
  }

  initAlarmActions() {
    this.alarmActions = this.alarmUtil.extractAlarmActions(this.alarm);
  }

  getThresholdDisplay() {
    let displayValue = this.alarm.threshold;
    let displayPercent = '';
    if (this.metricsDetail.unit) {
      displayValue = this.alarmUtil.getAlarmThresholdDisplay(
        this.metricsDetail.unit,
        this.alarm.threshold,
      );
      displayPercent =
        this.metricsDetail.unit.toLowerCase() === 'percent'
          ? '%'
          : this.metricsDetail.unit;
    }
    return `${
      this.alarm.comparison_operator
    } ${displayValue} ${displayPercent}`;
  }

  hasPermission(item: any, action: string) {
    return this.roleUtil.resourceHasPermission(item, 'alarm', action);
  }

  async deleteAlarm() {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete_alarm_title'),
        content: this.translate.get('delete_alarm_content', {
          alarm_name: this.alarm.name,
        }),
      });
    } catch (error) {
      return false;
    }
    this.inAction = true;
    try {
      await this.alarmService.deleteAlarm(this.alarm.uuid);
      this.auiNotificationService.success(
        this.translate.get('alarm_delete_success'),
      );
      this.router.navigate(['/alarm']);
    } catch (error) {
      this.errorToast.error(error);
    }
    this.inAction = false;
  }

  async deleteChild(child: any) {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete_sub_alarm_title'),
        content: this.translate.get('delete_sub_alarm_content', {
          alarm_name: this.alarm.name,
          sub_alarm_name: child.display_name,
        }),
      });
    } catch (error) {
      return;
    }

    child.inAction = true;
    try {
      await this.alarmService.deleteSubAlarm(
        this.alarm.uuid,
        child.alert_key_uuid,
      );
      this.auiNotificationService.success(
        this.translate.get('alarm_sub_alarm_delete_success'),
      );
      this.load();
    } catch (error) {
      this.errorToast.error(error);
    }
  }

  updateAlarm() {
    this.router.navigate(['/alarm/alarm_update', this.uuid]);
  }

  async ackChild(child: any) {
    const alertKeys: string[] = [child.alert_key_uuid];
    child.inAction = true;
    try {
      await this.alarmService.ackAlarm(this.alarm.uuid, alertKeys);
      this.load();
      this.auiNotificationService.success(
        this.translate.get('alarm_ack_success'),
      );
    } catch (error) {
      this.errorToast.error(error);
    }
    child.inAction = false;
  }

  ngOnDestroy() {
    clearTimeout(this.pollingTimer);
  }
}
