import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { get, some } from 'lodash';
import moment from 'moment';

import { NotificationService } from 'alauda-ui';
import { AlarmUtilitiesService } from 'app2/features/alarm/alarm-utilities.service';
import { AlarmService } from 'app2/shared/services/features/alarm.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-alarm-list',
  templateUrl: './alarm-list.component.html',
  styleUrls: ['./alarm-list.component.scss'],
})
export class AlarmListComponent implements OnInit, OnDestroy {
  hasCreateAlarmPermission = false;
  loading = false;
  initialized = false;
  destroyed = false;
  alarms: any[] = [];
  alarmsCache: any[] = [];
  expandedAlarms: any = {};
  emptyMessage = 'no_data';
  statusMap = {
    OK: 'ok',
    ALARM: 'alarm',
    INSUFFICIENT_DATA: 'insufficient_data',
    INSUFFICIENT_RES: 'insufficient_res',
  };
  pollingTimer: any;
  keyword = '';

  @ViewChild('alarmTable')
  table: any;

  constructor(
    public alarmService: AlarmService,
    public alarmUtil: AlarmUtilitiesService,
    public roleUtil: RoleUtilitiesService,
    public modal: ModalService,
    public translate: TranslateService,
    public auiNotificationService: NotificationService,
    private router: Router,
    private errorToast: ErrorsToastService,
  ) {}

  async ngOnInit() {
    await this.load();
    this.hasCreateAlarmPermission = await this.roleUtil.resourceTypeSupportPermissions(
      'alarm',
    );
  }

  async load() {
    if (this.destroyed || this.loading) {
      return;
    }
    this.loading = true;
    try {
      const result = await this.alarmService.getAlarms();
      this.alarms = result.sort((prev: any, next: any) => {
        return moment(prev.created_at).isAfter(next.created_at) ? -1 : 1;
      });
      this.handleAlarms();
      this.alarmsCache = this.alarms;
      this.searchChanged(this.keyword);
    } catch (error) {
      this.errorToast.error(error);
    }
    this.loading = false;
    this.initialized = true;
  }

  handleAlarms() {
    this.alarms.forEach((alarm: any, index: number) => {
      alarm.actions_display = this.alarmUtil.getAlarmActionDisplay(alarm);
      alarm.canUpdate = this.hasPermission(alarm, 'update');
      alarm.canAck = alarm.canUpdate && ['ALARM'].includes(alarm.status);
      alarm.canDelete = this.hasPermission(alarm, 'delete');
      alarm.inAction = false;
      alarm.ackDisabled = this.alarmUtil.isAlarmAckDisabled(alarm);
      if (alarm.children) {
        alarm.children.forEach((child: any) => {
          child.display_name = this.alarmUtil.getAlarmChildDisplayName(
            child,
            alarm.name,
          );
          child.alarm_index = index;
          child.inAction = false;
          child.ackDisabled = this.alarmUtil.isAlarmAckDisabled(alarm, child);
          child.canDelete = alarm.canDelete;
          child.canAck = alarm.canUpdate && ['ALARM'].includes(child.status);
        });
      }
    });
  }

  shouldShowAlarmIcon(alarm: any) {
    const children = get(alarm, 'children', []);
    return some(children, child => {
      return (
        child.status === 'ALARM' && get(child, 'actions.ack.status') === false
      );
    });
  }

  polling() {
    clearTimeout(this.pollingTimer);
    this.pollingTimer = setTimeout(() => {
      this.load();
    }, 30 * 1000);
  }

  createAlarm() {
    this.router.navigate(['/alarm/alarm_create']);
  }

  update(alarm: any) {
    this.router.navigate(['/alarm/alarm_update', alarm.uuid]);
  }

  async ack(alarm: any, child?: any) {
    const alertKeys: string[] = [];
    if (child) {
      alertKeys.push(child.alert_key_uuid);
    } else {
      const children: any[] = get(alarm, 'children', []);
      children.forEach(item => {
        alertKeys.push(item.alert_key_uuid);
      });
    }
    try {
      await this.alarmService.ackAlarm(alarm.uuid, alertKeys);
      this.load();
      this.auiNotificationService.success(
        this.translate.get('alarm_ack_success'),
      );
    } catch (error) {
      this.errorToast.error(error);
    }
  }

  ackChild(child: any) {
    const parentAlarm = this.alarms[child.alarm_index];
    this.ack(parentAlarm, child);
  }

  async delete(alarm: any) {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete_alarm_title'),
        content: this.translate.get('delete_alarm_content', {
          alarm_name: alarm.name,
        }),
      });
    } catch (error) {
      return false;
    }
    alarm.inAction = true;
    try {
      await this.alarmService.deleteAlarm(alarm.uuid);
      this.auiNotificationService.success(
        this.translate.get('alarm_delete_success'),
      );
      this.load();
    } catch (error) {
      this.errorToast.error(error);
    }
    alarm.inAction = false;
  }

  async deleteChild(child: any) {
    const alarm = this.alarms[child.alarm_index];
    try {
      await this.modal.confirm({
        title: this.translate.get('delete_sub_alarm_title'),
        content: this.translate.get('delete_sub_alarm_content', {
          alarm_name: alarm.name,
          sub_alarm_name: child.display_name,
        }),
      });
    } catch (error) {
      return false;
    }
    child.inAction = true;
    try {
      await this.alarmService.deleteSubAlarm(alarm.uuid, child.alert_key_uuid);
      this.auiNotificationService.success(
        this.translate.get('alarm_sub_alarm_delete_success'),
      );
      this.load();
    } catch (error) {
      this.errorToast.error(error);
    }
  }

  searchChanged(keyword: string) {
    this.keyword = keyword;
    if (!keyword.length) {
      this.alarms = this.alarmsCache;
      this.polling();
      return;
    }
    this.alarms = this.alarmsCache.filter((alarm: any) => {
      return alarm.name.includes(keyword);
    });
    this.polling();
  }

  clickRow(alarm: any) {
    this.expandedAlarms[alarm.uuid] = !this.expandedAlarms[alarm.uuid];
  }

  viewDetail(uuid: string) {
    this.router.navigate(['/alarm/alarm_detail', uuid]);
  }

  hasPermission(item: any, action: string) {
    return this.roleUtil.resourceHasPermission(item, 'alarm', action);
  }

  trackByIndex(index: number) {
    return index;
  }

  getThresholdDisplay(alarm: any) {
    let threshold = '';
    let unit = '';
    if (alarm.metric_unit) {
      if (alarm.metric_unit.toLowerCase() === 'percent') {
        threshold = this.alarmUtil.preciseCalculateAlarmThreshold(
          alarm.threshold * 100,
        );
        unit = '%';
      } else {
        threshold = alarm.threshold;
        unit = alarm.metric_unit;
      }
    } else {
      threshold = alarm.threshold;
    }
    return `${alarm.comparison_operator} ${threshold} ${unit}`;
  }

  ngOnDestroy() {
    this.destroyed = true;
    clearTimeout(this.pollingTimer);
  }
}
