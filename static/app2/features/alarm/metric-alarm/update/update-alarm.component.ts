import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

@Component({
  templateUrl: './update-alarm.component.html',
})
export class UpdateAlarmComponent implements OnInit {
  paramsSubscription: Subscription;
  uuid: string;
  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.paramsSubscription = this.route.params
      .pipe(first())
      .subscribe(async params => {
        this.uuid = params['uuid'];
      });
  }
}
