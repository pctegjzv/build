import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppCatalogTemplatesComponent } from './components/app-templates/app-catalog-templates.component';
import { AppCatalogAppCreatePageComponent } from './components/pages/app-catalog-create-app-page.component';
import { AppCatalogTemplateListPageComponent } from './components/pages/app-catalog-template-list-page.component';
import { AppCatalogRepoListComponent } from './components/repository/app-catalog-repo-list.component';

const clusterRoutes: Routes = [
  {
    path: '',
    redirectTo: 'repository',
    pathMatch: 'full',
  },
  {
    path: 'repository',
    children: [
      {
        path: '',
        redirectTo: 'repo_list',
        pathMatch: 'full',
      },
      {
        path: 'repo_list',
        component: AppCatalogRepoListComponent,
      },
      {
        path: 'repo_detail/:uuid',
        component: AppCatalogTemplateListPageComponent,
      },
      {
        path: 'app-create/:template_id',
        component: AppCatalogAppCreatePageComponent,
      },
    ],
  },
  {
    path: 'templates',
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        component: AppCatalogTemplatesComponent,
      },
      {
        path: 'app-create/:template_id',
        component: AppCatalogAppCreatePageComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(clusterRoutes)],
  exports: [RouterModule],
})
export class AppCatalogRoutingModule {}
