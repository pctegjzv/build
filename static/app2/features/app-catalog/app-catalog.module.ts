import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { AppCatalogRoutingModule } from 'app2/features/app-catalog/app-catalog-routing.module';
import { AppCatalogTemplatesComponent } from 'app2/features/app-catalog/components/app-templates/app-catalog-templates.component';
import { AppCatalogTemplateEmptyViewComponent } from 'app2/features/app-catalog/components/empty-view/app-catalog-template-empty-view.component';
import { AppCatalogTemplateRepoImportDialogComponent } from 'app2/features/app-catalog/components/import-dialog/app-catalog-template-repo-import-dialog.component';
import { AppCatalogTemplateRepositoryImportCircleComponent } from 'app2/features/app-catalog/components/import-progress/app-catalog-template-repo-import-circle.component';
import { AppCatalogTemplateRepoImportProgressComponent } from 'app2/features/app-catalog/components/import-progress/app-catalog-template-repo-import-progress.component';
import { AppCatalogAppCreatePageComponent } from 'app2/features/app-catalog/components/pages/app-catalog-create-app-page.component';
import { AppCatalogTemplateListPageComponent } from 'app2/features/app-catalog/components/pages/app-catalog-template-list-page.component';
import { AppCatalogRepoListComponent } from 'app2/features/app-catalog/components/repository/app-catalog-repo-list.component';
import { AppCatalogTemplateCardComponent } from 'app2/features/app-catalog/components/template-card/app-catalog-template-card.component';
import { AppCatalogCatalogTemplateEffects } from 'app2/features/app-catalog/effects/catalog-template';
import { SharedModule } from 'app2/shared/shared.module';

import { FEATURE_NAME, reducers } from './reducers';

@NgModule({
  imports: [
    SharedModule,
    AppCatalogRoutingModule,
    StoreModule.forFeature(FEATURE_NAME, reducers),
    EffectsModule.forFeature([AppCatalogCatalogTemplateEffects]),
  ],
  declarations: [
    AppCatalogTemplateListPageComponent,
    AppCatalogAppCreatePageComponent,
    AppCatalogTemplateRepoImportProgressComponent,
    AppCatalogTemplateRepoImportDialogComponent,
    AppCatalogTemplateRepositoryImportCircleComponent,
    AppCatalogTemplateEmptyViewComponent,
    AppCatalogTemplateCardComponent,
    AppCatalogRepoListComponent,
    AppCatalogTemplatesComponent,
  ],
  exports: [],
  entryComponents: [AppCatalogTemplateRepoImportDialogComponent],
})
export class AppCatalogModule {}
