import {
  AfterViewInit,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';

import { MessageService, NotificationService } from 'alauda-ui';
import { RepoLoadAction } from 'app2/features/app-catalog/actions/catalog-template';
import * as fromAppCatalog from 'app2/features/app-catalog/reducers';
import { DynamicFormComponent } from 'app2/shared/components/dynamic-form/dynamic-form.component';
import { DynamicFormFieldDefinition } from 'app2/shared/form-field-control';
import {
  AppCatalogService,
  AppCatalogTemplateRepository,
  VcsType,
} from 'app2/shared/services/features/app-catalog.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { ModalRef } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-app-catalog-template-repo-import-dialog',
  templateUrl: 'app-catalog-template-repo-import-dialog.component.html',
  styleUrls: ['app-catalog-template-repo-import-dialog.component.scss'],
})
export class AppCatalogTemplateRepoImportDialogComponent
  implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(DynamicFormComponent)
  private dynForm: DynamicFormComponent;
  fieldModels: DynamicFormFieldDefinition[];
  errorState: boolean;
  submitting = false;
  repo_uuid: string;

  private typeSub: Subscription;
  private formCaches: any = {
    [VcsType.Git as string]: {},
    [VcsType.SVN as string]: {},
  };

  constructor(
    private appCatalog: AppCatalogService,
    private translate: TranslateService,
    private modalRef: ModalRef,
    private modal: ModalService,
    private store: Store<fromAppCatalog.State>,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private router: Router,
    @Inject(MODAL_DATA) private defaultValue: AppCatalogTemplateRepository,
  ) {}

  ngOnInit(): void {
    this.repo_uuid = this.defaultValue && this.defaultValue.uuid;
    const type = this.defaultValue && this.defaultValue.type;
    this.fieldModels = this.getFormConfig(type);
  }

  ngAfterViewInit(): void {
    this.typeSub = this.dynForm.form
      .get('type')
      .valueChanges.subscribe(type => {
        this.onVcsTypeChanged(type);
      });
  }

  ngOnDestroy(): void {
    this.typeSub.unsubscribe();
  }

  getFormConfig(type?: VcsType): DynamicFormFieldDefinition[] {
    const fields: DynamicFormFieldDefinition[] = [
      {
        name: 'display_name',
        controlType: 'rc-input',
        label: this.translate.get('display_name'),
        maxlength: 120,
        required: false,
      },
      {
        name: 'type',
        controlType: 'rc-radio-group',
        type: 'toggle',
        label: this.translate.get('build_code_repo_client'),
        options: _.values(VcsType),
      },
      {
        name: 'url',
        controlType: 'rc-input',
        label: this.translate.get('build_code_repo_path'),
        pattern:
          '(https?)://[-A-Za-z0-9+&@#/%?=~_|!:,.;]+[-A-Za-z0-9+&@#/%=~_|]',
        maxlength: 1024,
      },
      // ...branch info will be inserted here.
      {
        name: 'username',
        controlType: 'rc-input',
        label: this.translate.get('username'),
        maxlength: 120,
        required: false,
      },
      {
        name: 'password',
        controlType: 'rc-input',
        type: 'password',
        label: this.translate.get('password'),
        maxlength: 120,
        required: false,
      },
    ];

    if (this.defaultValue.name) {
      fields.splice(0, 0, {
        name: 'name',
        controlType: 'rc-input',
        label: this.translate.get('name'),
        pattern: '^[a-z0-9]([-a-z0-9]*[a-z0-9])?$',
        maxlength: 120,
        required: true,
        readOnly: true,
      });
    } else {
      fields.splice(0, 0, {
        name: 'name',
        controlType: 'rc-input',
        label: this.translate.get('name'),
        pattern: '^[a-z0-9]([-a-z0-9]*[a-z0-9])?$',
        maxlength: 120,
        required: true,
      });
    }

    if (type === VcsType.Git) {
      fields.splice(4, 0, {
        name: 'path',
        controlType: 'rc-input',
        label: this.translate.get('build_code_repo_dir'),
        maxlength: 120,
        pattern: '[-A-Za-z0-9/]+',
        description: this.translate.get('app_catalog_dir_hint'),
      });
      fields.splice(4, 0, {
        name: 'branch',
        controlType: 'rc-input',
        label: this.translate.get('build_code_repo_branch'),
        maxlength: 120,
        pattern: '[-A-Za-z0-9/]+',
      });
    }

    // Assign default values:
    if (this.defaultValue) {
      fields.forEach(field => (field.value = this.defaultValue[field.name]));
    }

    return fields;
  }

  onVcsTypeChanged(type: VcsType) {
    if (this.dynForm.form.value.type !== type) {
      this.formCaches[
        type === VcsType.Git ? VcsType.SVN : VcsType.Git
      ] = this.dynForm.form.value;
      this.fieldModels = this.getFormConfig(type);

      Object.entries(this.dynForm.form.controls)
        .filter(([key]) => key !== 'type')
        .forEach(([key, control]) => {
          control.reset(this.formCaches[type][key]);
        });
    }
    if (this.defaultValue.name) {
      this.dynForm.form.controls.name.reset(this.defaultValue.name);
    }
  }

  onStatusChanges() {
    this.errorState = this.dynForm.errorState;
  }

  async cancel() {
    if (this.dynForm.ngForm.touched) {
      try {
        await this.modal.confirm({
          title: this.translate.get('cancel'),
          content: this.translate.get(
            'app_catalog_cancel_import_dialog_confirm',
          ),
        });
      } catch (e) {
        return false;
      }
    }
    this.modalRef.close();
  }

  async submit(model: any) {
    if (this.dynForm.errorState) {
      return;
    }

    if (this.defaultValue.uuid) {
      try {
        await this.modal.confirm({
          title: this.translate.get('app_catalog_import_template_confirm'),
          content: this.translate.get(
            'app_catalog_import_template_confirm_content',
          ),
        });
      } catch (e) {
        return false;
      }
    }

    this.submitting = true;
    if (this.repo_uuid) {
      try {
        const res = await this.appCatalog.importTemplateRepository(
          model,
          this.repo_uuid,
        );
        this.modalRef.close(res);
        this.auiMessageService.success({
          content: this.translate.get(
            'app_catalog_update_repo_trigger_success',
            { repo_name: res.display_name ? res.display_name : res.name },
          ),
        });
        this.router.navigateByUrl(
          `app-catalog/repository/repo_detail/${res.uuid}`,
        );
        this.store.dispatch(new RepoLoadAction(this.repo_uuid));
      } catch (err) {
        if (err.errors && err.errors[0].code === 'Invalid_remote_code_repo') {
          const message = this.translate.get('app_catalog_cant_checkout_code');
          this.auiNotificationService.error({
            duration: 0,
            title: this.translate.get('app_catalog_failed_to_import'),
            content: message,
          });
        } else if (
          err.errors &&
          err.errors[0].code === 'Invalid_remote_code_repo_branch'
        ) {
          const message = this.translate.get('app_catalog_invalid_branch');
          this.auiNotificationService.error({
            duration: 0,
            title: this.translate.get('app_catalog_failed_to_import'),
            content: message,
          });
        } else {
          this.auiNotificationService.error({
            duration: 0,
            title: this.translate.get('app_catalog_failed_to_import'),
            content: err.errors[0].message,
          });
        }
      }
    } else {
      try {
        const res = await this.appCatalog.importTemplateRepository(model);
        this.modalRef.close(res);
        this.auiMessageService.success({
          content: this.translate.get(
            'app_catalog_import_repo_trigger_success',
            { repo_name: res.display_name ? res.display_name : res.name },
          ),
        });
        this.store.dispatch(new RepoLoadAction(res.uuid));
        this.router.navigateByUrl(
          `app-catalog/repository/repo_detail/${res.uuid}`,
        );
      } catch (err) {
        if (err.errors && err.errors[0].code === 'Invalid_remote_code_repo') {
          const message = this.translate.get('app_catalog_cant_checkout_code');
          this.auiNotificationService.error({
            duration: 0,
            title: this.translate.get('app_catalog_failed_to_import'),
            content: message,
          });
        } else if (
          err.errors &&
          err.errors[0].code === 'Invalid_remote_code_repo_branch'
        ) {
          const message = this.translate.get('app_catalog_invalid_branch');
          this.auiNotificationService.error({
            duration: 0,
            title: this.translate.get('app_catalog_failed_to_import'),
            content: message,
          });
        } else {
          this.auiNotificationService.error({
            duration: 0,
            title: this.translate.get('app_catalog_failed_to_import'),
            content: err.errors[0].message,
          });
        }
      }
    }

    this.submitting = false;
  }
}
