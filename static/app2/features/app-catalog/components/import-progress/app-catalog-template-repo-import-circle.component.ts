import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component, Input } from '@angular/core';

export enum StepStatus {
  Success = 'success',
  InProgress = 'in_progress',
  Failed = 'failed',
  InActive = 'in_active',
}

@Component({
  selector: 'rc-app-catalog-template-repository-import-circle',
  templateUrl: 'app-catalog-template-repo-import-circle.component.html',
  styleUrls: ['app-catalog-template-repo-import-circle.component.scss'],
  animations: [
    trigger('scaleInOut', [
      state('*', style({ transform: 'scale(1)', opacity: '1' })),
      transition(':enter', [
        style({ transform: 'scale(0.8)', opacity: '0' }),
        animate('.3s', style({ transform: 'scale(1.5)', opacity: '1' })),
        animate('.2s', style({ transform: 'scale(1)' })),
      ]),
      transition(
        ':leave',
        animate('.1s', style({ transform: 'scale(0.8)', opacity: '0' })),
      ),
    ]),
    trigger('opacityInOut', [
      transition(':enter', [
        style({ opacity: '0' }),
        animate('.2s', style({ opacity: '1' })),
      ]),
      transition(':leave', animate('.2s', style({ opacity: '0' }))),
    ]),
  ],
})
export class AppCatalogTemplateRepositoryImportCircleComponent {
  readonly StepStatus = StepStatus;
  @Input()
  status: StepStatus;
  @Input()
  label: string;
  @Input()
  size: number;
}
