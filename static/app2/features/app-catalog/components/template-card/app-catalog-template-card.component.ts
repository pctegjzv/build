import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { AppCatalogTemplate } from 'app2/shared/services/features/app-catalog.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-app-catalog-template-card',
  templateUrl: 'app-catalog-template-card.component.html',
  styleUrls: ['app-catalog-template-card.component.scss'],
})
export class AppCatalogTemplateCardComponent implements OnInit {
  @Input()
  template: AppCatalogTemplate;
  @Input()
  showRepoName = false;
  @Input()
  applicationPermission: boolean;

  @Output()
  createAppClick = new EventEmitter<Event>();

  warm_tips = '';

  constructor(private translate: TranslateService) {}

  ngOnInit() {
    if (this.template.resource_actions.indexOf('helm_template:use') < 0) {
      this.warm_tips = this.translate.get(
        'app_catalog_no_permission_create_app',
      );
    }
  }

  createAppClicked(event: Event) {
    event.stopPropagation();
    if (
      this.template.resource_actions.indexOf('helm_template:use') < 0 ||
      !this.applicationPermission
    ) {
      return false;
    }
    this.createAppClick.emit(event);
  }
}
