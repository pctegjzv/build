import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppPlatformAppCreatePageComponent } from './components/pages/app-platform-create-app-page.component';
import { AppPlatformTemplateListPageComponent } from './components/pages/app-platform-template-list-page.component';

const clusterRoutes: Routes = [
  {
    path: 'middleware',
    component: AppPlatformTemplateListPageComponent,
  },
  {
    path: 'micro_service',
    component: AppPlatformTemplateListPageComponent,
  },
  {
    path: 'big_data',
    component: AppPlatformTemplateListPageComponent,
  },
  {
    path: 'middleware/app-create/:template_id',
    component: AppPlatformAppCreatePageComponent,
  },
  {
    path: 'micro_service/app-create/:template_id',
    component: AppPlatformAppCreatePageComponent,
  },
  {
    path: 'big_data/app-create/:template_id',
    component: AppPlatformAppCreatePageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(clusterRoutes)],
  exports: [RouterModule],
})
export class AppPlatformRoutingModule {}
