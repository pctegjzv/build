import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { AppPlatformRoutingModule } from 'app2/features/app-platform/app-platform-routing.module';
import { AppPlatformTemplateEmptyViewComponent } from 'app2/features/app-platform/components/empty-view/app-platform-template-empty-view.component';
import { AppPlatformAppCreatePageComponent } from 'app2/features/app-platform/components/pages/app-platform-create-app-page.component';
import { AppPlatformTemplateListPageComponent } from 'app2/features/app-platform/components/pages/app-platform-template-list-page.component';
import { AppPlatformTemplateCardComponent } from 'app2/features/app-platform/components/template-card/app-platform-template-card.component';
import { AppCatalogCatalogTemplateEffects } from 'app2/features/app-platform/effects/catalog-template';
import { AppAppCatalogSharedModule } from 'app2/features/lazy/app-app_catalog.shared.module';
import { SharedModule } from 'app2/shared/shared.module';

import { FEATURE_NAME, reducers } from './reducers';

@NgModule({
  imports: [
    SharedModule,
    AppAppCatalogSharedModule,
    AppPlatformRoutingModule,
    StoreModule.forFeature(FEATURE_NAME, reducers),
    EffectsModule.forFeature([AppCatalogCatalogTemplateEffects]),
  ],
  declarations: [
    AppPlatformTemplateListPageComponent,
    AppPlatformAppCreatePageComponent,
    AppPlatformTemplateEmptyViewComponent,
    AppPlatformTemplateCardComponent,
  ],
  exports: [],
})
export class AppPlatformModule {}
