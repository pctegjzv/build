import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map, pluck, switchMap } from 'rxjs/operators';

import { TemplateLoadAction } from 'app2/features/app-platform/actions/catalog-template';
import * as fromAppCatalog from 'app2/features/app-platform/reducers';
import * as catalogTemplate from 'app2/features/app-platform/reducers/catalog-template';
import { SingleSelectionDropdownComponent } from 'app2/shared/components/select/single-selection-dropdown.component';
import { YamlCommentFormComponent } from 'app2/shared/components/yaml-comment-form/yaml-comment-form.component';
import { AppCatalogService } from 'app2/shared/services/features/app-catalog.service';
import {
  NamespaceOption,
  NamespaceService,
} from 'app2/shared/services/features/namespace.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ModalMode } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: 'app-platform-create-app-page.component.html',
  styleUrls: ['app-platform-create-app-page.component.scss'],
})
export class AppPlatformAppCreatePageComponent implements OnInit {
  templateState$: Observable<catalogTemplate.TemplateState>;
  yaml$: Observable<string>;
  basic_info: any;
  showLoadingMask$: Observable<boolean>;
  showComment = false;
  namespaceOptions: NamespaceOption[];
  namespaceOptionsLoading = false;
  regionOptions: any[];
  catalogPayload: any;
  submitting = false;

  @ViewChild('appCatalogForm')
  form: NgForm;
  @ViewChild('yamlModalTemplate', { read: TemplateRef })
  yamlModalTemplateRef: TemplateRef<any>;
  @ViewChild(YamlCommentFormComponent)
  yamlCommentForm: YamlCommentFormComponent;
  @ViewChild('namespaceSingleDropdown')
  namespaceDropdown: SingleSelectionDropdownComponent;

  constructor(
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private store: Store<fromAppCatalog.State>,
    private modal: ModalService,
    private namespaceService: NamespaceService,
    private regionService: RegionService,
    private regionUtilitiesService: RegionUtilitiesService,
    private router: Router,
    private appCatalogService: AppCatalogService,
    private errorsToastService: ErrorsToastService,
  ) {
    this.catalogPayload = {
      name: '',
      display_name: '',
      values_yaml_content: '',
      template: {
        uuid: '',
        name: '',
        version: {
          uuid: '',
        },
      },
      namespace: {},
      cluster: {},
    };
    this.basic_info = {
      display_name: '',
      name: '',
      version: '',
      description: '',
      icon: '',
    };
  }

  ngOnInit(): void {
    this.templateId.subscribe(uuid => {
      this.catalogPayload.template.uuid = uuid;
      this.store.dispatch(new TemplateLoadAction(uuid));
    });

    this.templateState$ = this.templateId.pipe(
      switchMap(uuid =>
        this.store
          .select(fromAppCatalog.getTemplateStateById)
          .pipe(map(storeFn => storeFn(uuid))),
      ),
    );

    this.yaml$ = this.templateState$.pipe(
      filter(
        state =>
          state.template &&
          state.template.versions &&
          state.template.versions.length > 0,
      ),
      map(state => {
        this.catalogPayload.template.version.uuid =
          state.template.versions[0].uuid;
        this.catalogPayload.template.name = state.template.name;

        this.basic_info.display_name = state.template.display_name;
        this.basic_info.description = state.template.description;
        this.basic_info.name = state.template.name;
        this.basic_info.version = state.template.versions[0].version;
        this.basic_info.icon = state.template.icon;

        return state.template.versions[0].values_yaml_content;
      }),
    );

    this.showLoadingMask$ = this.templateState$.pipe(
      map(state => state.loading && !state.template),
    );

    this.regionService
      .getRegions()
      .then(regions => {
        regions = regions.filter(region =>
          this.regionUtilitiesService.isNewK8sRegion(region),
        );
        this.regionOptions = regions.map(result => {
          return {
            name: result.display_name,
            uuid: result.id,
            clusterName: result.name,
          };
        });
      })
      .catch(err => {
        this.errorsToastService.error(err);
      });
  }

  get templateId() {
    return this.activatedRoute.params.pipe(
      pluck<Params, string>('template_id'),
    );
  }

  onRegionOptionChange(option: any) {
    this.catalogPayload.cluster.name = option.clusterName;
    this.catalogPayload.cluster.uuid = option.uuid;
    if (this.namespaceDropdown) {
      this.namespaceDropdown.value = null;
    }
    this.getNamespaces(option.uuid);
  }

  onNamespaceOptionChange(option: NamespaceOption) {
    if (!option) {
      return;
    }
    this.catalogPayload.namespace.name = option.name;
    this.catalogPayload.namespace.uuid = option.uuid;
  }

  async getNamespaces(regionId: string) {
    this.namespaceOptionsLoading = true;
    this.namespaceOptions = await this.namespaceService.getNamespaceOptions(
      regionId,
    );
    setTimeout(() => {
      if (this.namespaceOptions.length) {
        this.namespaceDropdown.value = this.namespaceOptions[0].uuid;
      }
    });
    this.namespaceOptionsLoading = false;
  }

  viewYamlConfigFile() {
    this.modal.open(this.yamlModalTemplateRef, {
      title: this.translate.get('app_catalog_yaml_config'),
      mode: ModalMode.RIGHT_SLIDER,
      width: 1200,
    });
  }

  async onYamlFormSubmit(newYaml: string) {
    try {
      await this.modal.confirm({
        title: this.translate.get('create'),
        content: this.translate.get('app_catalog_create_app_confirm', {
          app_name: this.catalogPayload.name,
        }),
      });
    } catch (e) {
      return false;
    }
    this.catalogPayload.values_yaml_content = newYaml;

    this.submitting = true;

    try {
      const res = await this.appCatalogService.createAppWithTemplate(
        this.catalogPayload,
      );
      this.router.navigate([`/k8s_app/detail/${res.resource.uuid}`]);
    } catch (err) {
      this.errorsToastService.error(err);
    }
    this.submitting = false;
  }

  cancelCreateAppCatalog() {
    const publicRepoType = this.activatedRoute.snapshot.routeConfig.path.split(
      '/',
    )[0];
    this.router.navigate([`/app-platform/${publicRepoType}`]);
  }
}
