import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import * as _ from 'lodash';
import { Observable, Subscription } from 'rxjs';
import { distinctUntilChanged, first, map } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import {
  ListPageModeChangeAction,
  ListPageViewMode,
  RepoLoadAction,
} from 'app2/features/app-platform/actions/catalog-template';
import * as fromAppCatalog from 'app2/features/app-platform/reducers';
import * as catalogTemplate from 'app2/features/app-platform/reducers/catalog-template';
import {
  AppCatalogTemplate,
  AppCatalogTemplateRepository,
  RepositoryStatus,
} from 'app2/shared/services/features/app-catalog.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { RoleService } from 'app2/shared/services/features/role.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: 'app-platform-template-list-page.component.html',
  styleUrls: ['app-platform-template-list-page.component.scss'],
  providers: [AppPlatformTemplateListPageComponent],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppPlatformTemplateListPageComponent implements OnInit, OnDestroy {
  static readonly POLLING_INTERVAL = 3000;
  readonly ListPageViewMode = ListPageViewMode;
  pageState$: Observable<catalogTemplate.TemplateListState>;
  repoLoading$: Observable<boolean>;
  repository$: Observable<AppCatalogTemplateRepository>;
  templates$: Observable<AppCatalogTemplate[]>;
  mode$: Observable<ListPageViewMode>;
  selectedTemplate$: Observable<AppCatalogTemplate>;
  pageLoading$: Observable<boolean>;
  regionName$: Observable<string>;
  allAppsNumber$: Observable<number>;
  selectedTemplateName$: Observable<string>;
  searchQuery$: Observable<string>;
  uuid$: Observable<string>;
  refreshLoading$: Observable<boolean>;
  appsLoading = false;
  applicationPermission = false;
  selectedTemplateIndex = -1;
  publicRepoType = '';

  private storeSub: Subscription;
  private destroyed = false;

  private changeRouteParams = _.debounce(
    (
      mode: ListPageViewMode,
      templateId: string = '',
      searchQuery: string = '',
    ) => {
      const queryParams = {
        mode,
        template_id: templateId,
        search: searchQuery,
      };
      this.router.navigate([], {
        replaceUrl: true,
        queryParams: _.pickBy(queryParams, value => !!value),
      });
    },
  );

  constructor(
    private store: Store<fromAppCatalog.State>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private regionService: RegionService,
    private translate: TranslateService,
    private roleService: RoleService,
    private auiNotificationService: NotificationService,
  ) {}

  ngOnInit(): void {
    this.roleService
      .getPluralContextPermissions(['application'])
      .then(res => {
        this.applicationPermission =
          res.application.indexOf('application:create') >= 0;
      })
      .catch(err => {
        this.auiNotificationService.error(err.errors[0].message);
      });

    this.publicRepoType = this.activatedRoute.snapshot.routeConfig.path;
    this.refetchRepositoryData(this.publicRepoType);

    this.regionName$ = this.regionService.regionName$;

    // Stores:
    this.pageState$ = this.store.select(
      fromAppCatalog.getTemplateListPageState,
    );
    this.repoLoading$ = this.store.select(
      fromAppCatalog.getTemplateListLoadingState,
    );
    this.mode$ = this.store.select(fromAppCatalog.getTemplateListModeState);
    this.repository$ = this.store.select(fromAppCatalog.getRepositoryState);
    this.templates$ = this.store.select(fromAppCatalog.getTemplatesState);

    this.pageLoading$ = this.pageState$.pipe(
      map(state => state.loading && !state.repository),
    );

    this.refreshLoading$ = this.store
      .select(fromAppCatalog.getRefreshLoadingState)
      .pipe(map(state => state));

    this.selectedTemplate$ = this.pageState$.pipe(
      map(state => {
        if (state.repository && state.selectedTemplateId) {
          return state.repository.templates.find(
            template => template.uuid === state.selectedTemplateId,
          );
        }
      }),
    );

    this.selectedTemplateName$ = this.selectedTemplate$.pipe(
      map(template => {
        return template
          ? template.display_name
          : this.translate.get('app_catalog_all_template_apps');
      }),
    );

    this.uuid$ = this.repository$.pipe(
      map(rep => {
        if (rep) {
          return rep.uuid;
        }
      }),
    );

    this.searchQuery$ = this.pageState$.pipe(map(state => state.searchQuery));

    this.allAppsNumber$ = this.pageState$.pipe(
      map(state => {
        if (state.repository) {
          return state.repository.templates.reduce((accum, template) => {
            return accum + template.installed_app_num;
          }, 0);
        } else {
          return 0;
        }
      }),
    );

    this.storeSub = this.pageState$
      .pipe(distinctUntilChanged())
      .subscribe(state => {
        this.checkRepoStateAndNavigate(state);
      });

    this.activatedRoute.queryParams.subscribe(queryParams => {
      this.handleQueryParamsChange(queryParams as { mode: ListPageViewMode });
    });
  }

  ngOnDestroy(): void {
    this.storeSub.unsubscribe();
    this.destroyed = true;
  }

  refetchRepositoryData(type: string) {
    this.store.dispatch(new RepoLoadAction(type));
  }

  trackByFn(index: number) {
    return index;
  }

  closeAppListPanel() {
    this.changeRouteParams(ListPageViewMode.Templates);
  }

  createAppClicked(template: AppCatalogTemplate) {
    this.router.navigate([
      `/app-platform/${this.publicRepoType}/app-create/${template.uuid}`,
    ]);
  }

  /**
   * Check the given state and navigate route if necessary.
   */
  private checkRepoStateAndNavigate(state: catalogTemplate.TemplateListState) {
    if (state.mode === ListPageViewMode.Templates) {
      // Load failed / empty repository, go to empty view
      if (
        (!state.repository && state.error) ||
        (state.repository &&
          state.repository.status === RepositoryStatus.Success &&
          state.repository.templates.length === 0)
      ) {
        this.changeRouteParams(ListPageViewMode.Empty, '');
      } else if (
        state.repository &&
        state.repository.status !== RepositoryStatus.Success
      ) {
        this.changeRouteParams(ListPageViewMode.Import, '');
      }
    }

    if (state.repository && ListPageViewMode.Import === state.mode) {
      // After State changes to success in ImportPregress view, we need to
      // head back to templates view in 2 seconds.
      if (state.repository.status === RepositoryStatus.Success) {
        setTimeout(() => {
          if (!this.destroyed) {
            this.changeRouteParams(ListPageViewMode.Templates, '');
          }
        }, 2000);
      }
    }

    if (state.repository && ListPageViewMode.Empty === state.mode) {
      if (state.repository.status !== RepositoryStatus.Success) {
        this.changeRouteParams(ListPageViewMode.Import, '');
      } else if (state.repository.templates.length !== 0) {
        this.changeRouteParams(ListPageViewMode.Templates, '');
      }
    }
  }

  private async handleQueryParamsChange({ mode }: { mode: ListPageViewMode }) {
    const state = await this.pageState$.pipe(first()).toPromise();

    this.checkRepoStateAndNavigate(state);

    if (!_.values(ListPageViewMode).includes(mode)) {
      return this.changeRouteParams(ListPageViewMode.Templates);
    }

    if (mode !== state.mode) {
      this.store.dispatch(new ListPageModeChangeAction(mode));
    }
  }
}
