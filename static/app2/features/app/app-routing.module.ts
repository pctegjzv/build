import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { K8sAppDetailComponent } from 'app2/features/app/detail/k8s-app-detail.component';
import { AppCreatePageComponent } from 'app2/features/app/page/app-create-page.component';
import { K8sAppListPageComponent } from 'app2/features/app/page/k8s-app-list-page.component';
import { K8sAppUpdateComponent } from 'app2/features/app/update/k8s-app-update.component';

const k8sAppRoutes: Routes = [
  {
    path: '',
    component: K8sAppListPageComponent,
  },
  {
    path: 'create',
    component: AppCreatePageComponent,
  },
  // {
  //   path: 'create-ui',
  //   component: AppCreateUiComponent,
  // },
  {
    path: 'update/:uuid',
    component: K8sAppUpdateComponent,
  },
  {
    path: 'detail/:uuid',
    component: K8sAppDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(k8sAppRoutes)],
  exports: [RouterModule],
  declarations: [],
})
export class K8sAppRoutingModule {}
