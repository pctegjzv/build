import { NgModule } from '@angular/core';

import { K8sAppRoutingModule } from 'app2/features/app/app-routing.module';
import { AppAddServiceComponent } from 'app2/features/app/create-ui/app-add-service.component';
import { AppCreateUiComponent } from 'app2/features/app/create-ui/app-create-ui.component';
import { K8sAppCreateComponent } from 'app2/features/app/create/k8s-app-create.component';
import { K8sAppDetailComponent } from 'app2/features/app/detail/k8s-app-detail.component';
import { AppMetricsComponent } from 'app2/features/app/metric/app-metrics.component';
import { AppCreatePageComponent } from 'app2/features/app/page/app-create-page.component';
import { K8sAppListPageComponent } from 'app2/features/app/page/k8s-app-list-page.component';
import { ApplicationTopologyGraphComponent } from 'app2/features/app/topology-graph/application-topology-graph.component';
import { K8sAppUpdateComponent } from 'app2/features/app/update/k8s-app-update.component';
import { AppAppCatalogSharedModule } from 'app2/features/lazy/app-app_catalog.shared.module';
import { ServiceAppSharedModule } from 'app2/features/lazy/service-app.shared.module';
import { ServiceUtilitiesService } from 'app2/features/service/service-utilities.service';

import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    ServiceAppSharedModule,
    AppAppCatalogSharedModule,
    K8sAppRoutingModule,
  ],
  declarations: [
    K8sAppListPageComponent,
    K8sAppCreateComponent,
    K8sAppUpdateComponent,
    K8sAppDetailComponent,
    AppCreateUiComponent,
    AppCreatePageComponent,
    AppAddServiceComponent,
    AppMetricsComponent,
    ApplicationTopologyGraphComponent,
  ],
  entryComponents: [AppAddServiceComponent],
  providers: [ServiceUtilitiesService],
})
export class K8sAppModule {}
