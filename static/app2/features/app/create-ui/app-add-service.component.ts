import {
  AfterViewInit,
  Component,
  EventEmitter,
  Inject,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ServiceContainerFieldsComponent } from 'app2/features/service/create-fields/service-container.component';
import { ImageSelectComponent } from 'app2/features/service/image-select/image-select.component';
import { RcImageSelection } from 'app2/features/service/service.type';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';
import * as _ from 'lodash';
import { first } from 'rxjs/operators';

import {
  INT_PATTERN,
  K8S_RESOURCE_NAME_BASE,
} from '../../../../app/components/common/config/common-pattern';

@Component({
  selector: 'rc-app-add-service',
  templateUrl: './app-add-service.component.html',
  styleUrls: [
    '../../service/create-ui/service-create-ui.common.scss',
    './app-add-service.component.scss',
  ],
})
export class AppAddServiceComponent
  implements OnInit, OnDestroy, AfterViewInit {
  private containerIndex = 0;

  @Output()
  close = new EventEmitter();
  @ViewChild('serviceForm')
  serviceForm: NgForm;
  @ViewChildren(ServiceContainerFieldsComponent)
  containerFields: QueryList<ServiceContainerFieldsComponent>;

  numberReg = K8S_RESOURCE_NAME_BASE;
  intReg = INT_PATTERN;
  showAdvanceOptions = false;

  namespace: string;
  namespace_uuid: string;
  service: any;
  serviceYaml: string;

  containerPorts: any[] = [];
  containerNames: any = [];
  containerPortsOptions: any[] = [];
  serviceKindOptions: any[] = [];

  constructor(
    @Inject(MODAL_DATA)
    private modalData: {
      namespace: string;
      namespace_uuid: string;
      image: string;
      service?: any;
    },
    private serviceService: ServiceService,
    private modalService: ModalService,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    this.serviceKindOptions = ['Deployment', 'DaemonSet', 'StatefulSet'];

    this.namespace = this.modalData.namespace;
    this.namespace_uuid = this.modalData.namespace_uuid;
    if (this.modalData.service) {
      this.service = this.modalData.service;
    } else {
      this.initServcie();
    }
  }

  async ngAfterViewInit() {}

  ngOnDestroy() {}

  initServcie() {
    this.service = {
      namespace: '',
      name: '',
      kind: this.serviceKindOptions[0], // 部署模式
      replicas: 1, // 实例数
      labels: [],
      minReplicas: 1,
      maxReplicas: 20,
      nodeTags: [],
      affinity: null, // 亲和性，包括container亲和反亲和，key为 `service.${label_base_domain}/name`
      maxSurge: '',
      maxUnavailable: '',
      kubeServices: [],
      containers: [],
      networkMode: {
        hostNetwork: false,
        subnet: null,
      },
    };
    this.addContainer(true);
  }
  async addContainer(isDefault = false) {
    const config: any = {};
    if (isDefault) {
      const image = this.modalData.image;
      config.image = image;
      this.service.containers.push({
        config,
        index: this.containerIndex++,
      });
      this.addContainerPorts();
    } else {
      try {
        const modalRef = await this.modalService.open(ImageSelectComponent, {
          title: this.translateService.get('nav_select_image'),
          width: 800,
        });
        modalRef.componentInstance.close
          .pipe(first())
          .subscribe((res: RcImageSelection) => {
            modalRef.close();
            if (res) {
              config.image = res.full_image_name;
              this.service.containers.push({
                config,
                index: this.containerIndex++,
              });
              this.addContainerPorts();
            }
          });
      } catch (error) {
        //
      }
    }
  }

  removeContainer(index: number) {
    _.remove(this.service.containers, (container: any) => {
      return index === container.index;
    });
    this.containerNames[index] = undefined;
    this.removeContainerPorts(index);
  }

  containerNameChange(name: string, index: number) {
    setTimeout(() => {
      this.containerNames[index] = name;
    });
  }

  containerPortsChange(ports: number[], index: number) {
    this.containerPorts[index] = ports;
    this.refreshContainerPortsOptions();
  }

  private addContainerPorts() {
    if (!this.containerPorts) {
      this.containerPorts = [];
    }
    this.containerPorts.push([]);
  }

  private removeContainerPorts(index: number) {
    this.containerPorts[index] = [];
    this.refreshContainerPortsOptions();
  }

  private refreshContainerPortsOptions() {
    if (!this.containerPorts) {
      return;
    }
    this.containerPortsOptions = _.chain(this.containerPorts)
      .flatten()
      .uniq()
      .value();
  }

  // 实例数
  shouldShowReplicas() {
    return this.service.kind !== 'DaemonSet';
  }
  // 最大可超出
  shouldShowMaxSurge() {
    return this.service.kind === 'Deployment';
  }
  // 最多不可用
  shouldShowMaxUnavliable() {
    return ['Deployment', 'DaemonSet'].includes(this.service.kind);
  }
  // 亲和性
  shouldShowAffinity() {
    return this.service.kind !== 'DaemonSet';
  }

  trackByContainerIndex(_index: number, container: any) {
    return container.index;
  }

  save() {
    this.triggerSubmit();
    if (this.serviceForm.invalid) {
      return;
    }
    if (!this.serviceService.checkServicePayload(this.service)) {
      return;
    }
    this.serviceYaml = this.serviceService.generateServiceYamlPayload(
      this.service,
    );
    this.close.next({
      data: this.service,
      yaml: this.serviceYaml,
    });
  }

  cancel() {
    this.close.next(null);
  }

  private triggerSubmit() {
    this.containerFields.forEach(
      (container: ServiceContainerFieldsComponent) => {
        container.triggerSubmit();
      },
    );
    this.serviceForm.onSubmit(null);
  }
}
