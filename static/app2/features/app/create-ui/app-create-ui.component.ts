import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { AppAddServiceComponent } from 'app2/features/app/create-ui/app-add-service.component';
import { ImageSelectComponent } from 'app2/features/service/image-select/image-select.component';
import { ServiceUtilitiesService } from 'app2/features/service/service-utilities.service';
import { RcImageSelection } from 'app2/features/service/service.type';
import { AppService } from 'app2/shared/services/features/app.service';
import {
  NamespaceOption,
  NamespaceService,
} from 'app2/shared/services/features/namespace.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ModalMode } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import * as jsyaml from 'js-yaml';
import * as _ from 'lodash';
import { first } from 'rxjs/operators';

import { K8S_RESOURCE_NAME_BASE } from '../../../../app/components/common/config/common-pattern';

@Component({
  selector: 'rc-app-create-ui',
  templateUrl: './app-create-ui.component.html',
  styleUrls: [
    '../../service/create-ui/service-create-ui.common.scss',
    './app-create-ui.component.scss',
  ],
})
export class AppCreateUiComponent implements OnInit, OnDestroy {
  @ViewChild('appForm')
  appForm: NgForm;
  resourceNameReg = K8S_RESOURCE_NAME_BASE;

  initialized = false;

  submitting = false;
  yamlPreview: boolean;
  yamlReadonly = true;

  appPayload: any;
  appYaml: string;
  serviceList: any[] = []; // data, yaml

  namespaceOptions: NamespaceOption[];

  private ready = new Promise(resolve => {
    this.readyResolve = resolve;
  });

  private readyResolve: Function;

  constructor(
    private regionService: RegionService,
    private namespaceService: NamespaceService,
    private modalService: ModalService,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private appService: AppService,
    private errorsToastService: ErrorsToastService,
    private serviceUtilities: ServiceUtilitiesService,
  ) {
    this.initAppPayload();
  }

  async ngOnInit() {
    const region = await this.regionService.getCurrentRegion();
    this.appPayload.cluster.uuid = region.id;
    this.appPayload.cluster.name = region.name;
    this.namespaceOptions = await this.namespaceService.getNamespaceOptions(
      region.id,
    );
    if (this.namespaceOptions.length) {
      this.appPayload.namespace.name = this.namespaceOptions[0].name;
    }
    const image = this.activatedRoute.snapshot.queryParamMap.get('image');
    if (image) {
      await this.ready; // 校验service name需要namespace uuid，需要namespace相关api返回并选中默认值后才打开添加服务的modal
      this.addService(image);
    }
  }

  ngOnDestroy() {}

  initAppPayload() {
    this.appPayload = {
      resource: {},
      namespace: {},
      cluster: {},
      kubernetes: [],
    };
  }

  onNamespaceOptionChange(option: any) {
    this.appPayload.namespace.uuid = option.uuid;
    this.readyResolve();
  }

  async toggleAddService() {
    try {
      const modalRef = await this.modalService.open(ImageSelectComponent, {
        title: this.translateService.get('nav_select_image'),
        width: 800,
      });
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe((res: RcImageSelection) => {
          modalRef.close();
          if (res) {
            setTimeout(() => {
              this.addService(res.full_image_name);
            }, 200);
          }
        });
    } catch (error) {
      //
    }
  }

  private async addService(image: string) {
    const modalRef = await this.modalService.open(AppAddServiceComponent, {
      width: 1000,
      mode: ModalMode.RIGHT_SLIDER,
      title: this.translateService.get('add'),
      data: {
        namespace: this.appPayload.namespace.name,
        namespace_uuid: this.appPayload.namespace.uuid,
        image: image,
        service: null,
      },
    });
    modalRef.componentInstance.close.pipe(first()).subscribe((res: any) => {
      if (res) {
        this.saveService(res);
      }
      modalRef.close();
    });
  }

  async toggleUpdateService(service: any) {
    const modalRef = await this.modalService.open(AppAddServiceComponent, {
      width: 1000,
      mode: ModalMode.RIGHT_SLIDER,
      title: this.translateService.get('update'),
      data: {
        namespace: this.appPayload.namespace.name,
        namespace_uuid: this.appPayload.namespace.uuid,
        service: _.cloneDeep(service.data),
      },
    });
    modalRef.componentInstance.close.pipe(first()).subscribe((res: any) => {
      if (res) {
        this.saveService(res, service);
      }
      modalRef.close();
    });
  }

  saveService(res: any, originService: any = null) {
    if (!res) {
      return;
    }
    const imageDisplayParams = this.serviceUtilities.getServiceImageDisplayParams(
      { data: res.data },
      'viewModel',
    );
    if (originService) {
      originService.data = res.data;
      originService.yaml = res.yaml;
      originService.imageDisplayParams = imageDisplayParams;
    } else {
      const newService = {
        data: res.data,
        yaml: res.yaml,
        imageDisplayParams,
      };
      this.serviceList.push(newService);
    }
    this.serviceList = [...this.serviceList];
    this.updateAppYaml();
  }

  removeService(index: number) {
    this.serviceList.splice(index, 1);
    this.updateAppYaml();
  }

  private updateAppYaml() {
    this.appYaml = this.serviceList
      .map((item: any) => {
        return item.yaml;
      })
      .join('---\r\n');
  }

  async toggleYamlReadonly() {
    try {
      await this.modalService.confirm({
        title: this.translateService.get('confirm'),
        content: this.translateService.get('confirm_to_enter_yaml_edit_mode'),
      });
      this.yamlReadonly = false;
    } catch (e) {
      return false;
    }
  }

  async confirm() {
    this.appForm.onSubmit(null);
    if (this.appForm.invalid) {
      return;
    }
    this.appPayload.resource.create_method = this.yamlReadonly ? 'UI' : 'YAML';
    try {
      this.appPayload.kubernetes = jsyaml.safeLoadAll(this.appYaml);
    } catch (error) {
      this.auiNotificationService.error(error);
      return;
    }
    // this.logger.log('create service data: ', this.servicePayload);
    try {
      await this.modalService.confirm({
        title: this.translateService.get('create'),
        content: this.translateService.get('app_service_create_app_confirm', {
          app_name: this.appPayload.resource.name,
        }),
      });
      this.createApp();
    } catch (rejection) {}
  }

  cancel() {
    return this.router.navigateByUrl('k8s_app');
  }

  private async createApp() {
    this.submitting = true;
    try {
      const response: any = await this.appService.createK8sApp(this.appPayload);
      return this.router.navigateByUrl(
        `k8s_app/detail/${response.resource.uuid}`,
      );
    } catch (err) {
      this.errorsToastService.handleGenericAjaxError({
        errors: err.errors,
        handleNonGenericCodes: true,
        fallbackMessage: this.translateService.get('create_failed'),
      });
    }
    this.submitting = false;
  }
}
