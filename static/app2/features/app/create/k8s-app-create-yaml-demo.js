const yaml_template = `---
apiVersion: apps/v1beta1
kind: Deployment
metadata:
  name: nginx-app-1
  labels:
    aaa: nginx-app-1
spec:
  replicas: 1
  template:
    metadata:
      labels:
        run: nginx-app-1

    spec:
      containers:
      - name: nginx-app-1
        image: nginx:1.9.0
        ports:
        - containerPort: 80
          name: web
---
apiVersion: v1
kind: Service
metadata:
  labels:
    run: nginx-app-1
  name: nginx-app-1
  namespace: default
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    run: nginx-app-1
  sessionAffinity: None
  type: ClusterIP
---
apiVersion: apps/v1beta1
kind: Deployment
metadata:
  name: nginx-app-2
  labels:
    aaa: nginx-app-2
spec:
  replicas: 1
  template:
    metadata:
      labels:
        run: nginx-app-2

    spec:
      containers:
      - name: nginx-app-2
        image: nginx:1.9.0
        ports:
        - containerPort: 80
          name: web
---
apiVersion: v1
kind: Service
metadata:
  labels:
    run: nginx-app-2
  name: nginx-app-2
  namespace: default
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    run: nginx-app-2
  sessionAffinity: None
  type: ClusterIP
---`;

export default yaml_template;
