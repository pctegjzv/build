import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import * as jsyaml from 'js-yaml';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { AppService } from 'app2/shared/services/features/app.service';
import {
  NamespaceOption,
  NamespaceService,
} from 'app2/shared/services/features/namespace.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

import yaml_template from './k8s-app-create-yaml-demo';

@Component({
  selector: 'rc-app-create',
  templateUrl: './k8s-app-create.component.html',
  styleUrls: ['./k8s-app-create.component.scss'],
})
export class K8sAppCreateComponent implements OnInit, OnDestroy {
  private regionSubscription: Subscription;

  namespaceOptions: NamespaceOption[];
  appPayload: any;
  appYaml: string;
  submitting = false;

  @ViewChild('appCreateForm')
  form: NgForm;

  constructor(
    private namespaceService: NamespaceService,
    private regionService: RegionService,
    private appService: AppService,
    private modalService: ModalService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private errorsToastService: ErrorsToastService,
    private router: Router,
  ) {
    /**
     * @type {{resource: {}; namespace: {}; cluster: {}; kubernetes: Array}}
     * {
        "resource": {
          "name": "app-name",
          "description": "Test apps"
        },
        "namespace": {
          "uuid": "4928E1EE-ED87-4BD1-99F4-1227E1192DBC",
          "name": "default"
        },
        "cluster": {
          "uuid": "EDC70F4A-EE71-40B1-9CC7-CD5569BEAD2D",
          "name": "cluster-1"
        },
        "kubernetes": [
          "<example-app-yaml-for-kubernetes>"
        ]
      }
     */
    this.appPayload = {
      resource: {},
      namespace: {},
      cluster: {},
      kubernetes: [],
    };
  }

  ngOnInit() {
    this.regionSubscription = this.regionService.region$
      .pipe(filter(region => !!region))
      .subscribe(async region => {
        this.appPayload.cluster.uuid = region.id;
        this.appPayload.cluster.name = region.name;
        this.namespaceOptions = await this.namespaceService.getNamespaceOptions(
          region.id,
        );
      });
  }

  ngOnDestroy() {
    this.regionSubscription.unsubscribe();
  }

  onNamespaceOptionChange(option: any) {
    this.appPayload.namespace.name = option.name;
  }

  async createApp() {
    this.submitting = true;
    try {
      const response: any = await this.appService.createK8sApp(this.appPayload);
      return this.router.navigateByUrl(
        `k8s_app/detail/${response.resource.uuid}`,
      );
    } catch (err) {
      this.errorsToastService.handleGenericAjaxError({
        errors: err.errors,
        handleNonGenericCodes: true,
        fallbackMessage: this.translateService.get('create_failed'),
      });
    }
    this.submitting = false;
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.valid) {
      try {
        this.appPayload.kubernetes = jsyaml.safeLoadAll(this.appYaml);
      } catch (error) {
        // yaml parse error
        this.auiNotificationService.error(error);
        return;
      }
      // this.logger.log('create app data: ', this.appPayload);
      try {
        await this.modalService.confirm({
          title: this.translateService.get('create'),
          content: this.translateService.get('app_service_create_app_confirm', {
            app_name: this.appPayload.resource.name,
          }),
        });
        this.createApp();
      } catch (rejection) {}
    }
  }

  cancel() {
    return this.router.navigateByUrl('k8s_app');
  }

  useDemoTemplate() {
    this.appYaml = yaml_template;
  }
}
