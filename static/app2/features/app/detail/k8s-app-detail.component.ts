import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { VendorCustomerService } from 'app2/core/customization/vendor-customer.service';
import { CustomModuleName } from 'app2/core/customization/vendor-customer.types';
import {
  AppService,
  Application,
} from 'app2/shared/services/features/app.service';
import { AppUtilitiesService } from 'app2/shared/services/features/app.utilities.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ErrorResponse } from 'app2/shared/services/http/http.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { LoggerUtilitiesService } from 'app2/shared/services/utility/logger.service';
import { TranslateService } from 'app2/translate/translate.service';
import { get } from 'lodash';

@Component({
  templateUrl: './k8s-app-detail.component.html',
  styleUrls: ['./k8s-app-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class K8sAppDetailComponent implements OnInit, OnDestroy {
  private routeParamsSubscription: Subscription;
  private pollingTimer: any;
  private uuid: string;
  private _loading: boolean;
  private _loadError: any;

  appData: Application;
  appYaml: any;
  destroyed = false;
  initialized = false;
  appTopologySupport = false;

  constructor(
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
    private router: Router,
    private appService: AppService,
    private appUtilities: AppUtilitiesService,
    private errorsToastService: ErrorsToastService,
    private logger: LoggerUtilitiesService,
    private regionService: RegionService,
    private customerService: VendorCustomerService,
  ) {}

  ngOnInit() {
    this.appTopologySupport = this.customerService.support(
      CustomModuleName.APP_TOPOLOTY,
    );
    this.routeParamsSubscription = this.route.params
      .pipe(first())
      .subscribe(async params => {
        this.uuid = params['uuid'];
        await this.refetch();
        this.setDefaultRegion();
        this.initialized = true;
      });
  }

  ngOnDestroy() {
    this.routeParamsSubscription.unsubscribe();
    clearTimeout(this.pollingTimer);
    this.destroyed = true;
  }

  private setDefaultRegion() {
    if (this.appData) {
      this.regionService.setRegionByName(this.appData.cluster.name);
    }
  }

  async refetch() {
    if (this.destroyed) {
      return;
    }
    this._loading = true;
    try {
      this.appData = await this.appService.getK8sApp(this.uuid);
      this._loadError = null;
    } catch ({ status, errors }) {
      this._loadError = errors;
      if (status === 403) {
        this.auiNotificationService.warning(
          this.translateService.get('permission_denied'),
        );
      } else if (status === 404 && !this.initialized) {
        this.auiNotificationService.warning(
          this.translateService.get('application_not_exist'),
        );
      }
      return this.router.navigateByUrl('k8s_app');
    }

    this._loading = false;
    this.refetchAppYaml(); // no need to await
    this.resetPollingTimer();
    if (!this.destroyed) {
      this.cdr.detectChanges();
    }
  }

  get empty() {
    return !this.appData;
  }

  get loading() {
    return this._loading;
  }

  get loadError() {
    return this._loadError;
  }

  private resetPollingTimer() {
    clearTimeout(this.pollingTimer);
    if (this.destroyed) {
      return;
    }
    const waitTime =
      get(this.appData, 'resource.status', '').toLowerCase() === 'deploying'
        ? 3000
        : 10000;
    this.pollingTimer = setTimeout(() => {
      this.refetch();
    }, waitTime);
  }

  private async refetchAppYaml() {
    try {
      this.appYaml = await this.appService.getK8sAppYaml(this.uuid);
    } catch (err) {
      this.logger.log('Failed to get app yaml');
    }
  }

  async startApp() {
    try {
      const response = await this.appUtilities.startApp(this.appData);
      this.refetch();
      return response;
    } catch (rejection) {
      if (rejection instanceof ErrorResponse) {
        this.errorsToastService.handleGenericAjaxError({
          errors: rejection.errors,
          handleNonGenericCodes: true,
        });
      }
    }
  }

  async stopApp() {
    try {
      const response = await this.appUtilities.stopApp(this.appData);
      this.refetch();
      return response;
    } catch (rejection) {
      if (rejection instanceof ErrorResponse) {
        this.errorsToastService.handleGenericAjaxError({
          errors: rejection.errors,
          handleNonGenericCodes: true,
        });
      }
    }
  }

  async rollbackApp() {
    try {
      const response = await this.appUtilities.rollbackApp(this.appData);
      this.refetch();
      return response;
    } catch (rejection) {
      if (rejection instanceof ErrorResponse) {
        this.errorsToastService.handleGenericAjaxError({
          errors: rejection.errors,
          handleNonGenericCodes: true,
        });
      }
    }
  }

  updateApp() {
    return this.router.navigateByUrl(`k8s_app/update/${this.uuid}`);
  }

  async deleteApp() {
    try {
      await this.appUtilities.deleteApp(this.appData);
      return this.router.navigateByUrl('/workspace/app');
    } catch (rejection) {
      if (rejection instanceof ErrorResponse) {
        this.errorsToastService.handleGenericAjaxError({
          errors: rejection.errors,
          handleNonGenericCodes: true,
        });
      }
    }
  }
}
