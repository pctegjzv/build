import {
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { debounce, get } from 'lodash';
import { Subscription } from 'rxjs';

import { TableComponent } from 'app2/shared/components/table/table.component';
import { AppService } from 'app2/shared/services/features/app.service';
import { AppUtilitiesService } from 'app2/shared/services/features/app.utilities.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import {
  Pagination,
  PaginationDataWrapper,
} from 'app2/shared/services/utility/pagination-data';

@Component({
  selector: 'rc-k8s-app-list',
  templateUrl: './k8s-app-list.component.html',
  styleUrls: ['./k8s-app-list.component.scss'],
})
export class K8sAppListComponent implements OnInit, OnDestroy, OnChanges {
  @Input()
  searchQuery: string;
  @Input()
  regionName: string;
  @Input()
  repositoryUUID: string; // refers to App Catalog template repository
  @Input()
  templateUUID: string; // refers to App Catalog template

  @ViewChild('appTable')
  table: TableComponent;

  paginationDataWrapper: PaginationDataWrapper<any>;

  paginationDataSubscription: Subscription;

  appCreateEnabled: boolean;

  pageSize = 20;

  count = 0;

  appItems: any[] = [];

  initialized = false;

  private pollingTimer: any = null;

  private destroyed = false;

  private _onRequestParamsChange = debounce(
    this.onRequestParamsChange.bind(this),
    100,
  );

  constructor(
    private appService: AppService,
    public appUtilities: AppUtilitiesService,
    private errorsToastService: ErrorsToastService,
    private changeDetectorRef: ChangeDetectorRef,
    private router: Router,
  ) {
    const fetchRequest = (pageNo: number, pageSize: number, params: any) => {
      return this.appService.getK8sApps({
        pageNo,
        pageSize,
        params,
      });
    };
    this.paginationDataWrapper = new PaginationDataWrapper(
      fetchRequest,
      this.pageSize,
    );
  }

  async ngOnInit() {
    this.paginationDataSubscription = this.paginationDataWrapper.pagination.subscribe(
      (paginationData: Pagination<any>) => {
        this.initialized = true;
        if (!paginationData.loadError) {
          this.errorsToastService.error(paginationData.loadError);
          this.count = paginationData.count;
          // TODO: FIX THIS
          // const appItems = paginationData.results;
          // this.appItems = generateMergedListData(
          //   appItems,
          //   this.appItems,
          //   'resource.uuid',
          // );
          this.appItems = paginationData.results;
          if (!this.destroyed) {
            this.changeDetectorRef.detectChanges();
          }
        }
        this.resetPollingTimer();
      },
    );
  }

  ngOnDestroy() {
    this.destroyed = true;
    this.paginationDataSubscription.unsubscribe();
    clearTimeout(this.pollingTimer);
  }

  ngOnChanges({
    searchQuery,
    regionName,
    repositoryUUID,
    templateUUID,
  }: SimpleChanges): void {
    if (searchQuery || regionName || repositoryUUID || templateUUID) {
      this._onRequestParamsChange({
        name: this.searchQuery,
        cluster: this.regionName,
        repository_uuid: this.repositoryUUID,
        template_uuid: this.templateUUID,
      });
    }
  }

  get loading() {
    return this.paginationDataWrapper.loading;
  }

  get loadError() {
    return this.paginationDataWrapper.loadError;
  }

  get empty() {
    return !this.appItems || !this.appItems.length;
  }

  get currentPage() {
    return this.paginationDataWrapper.pageNo;
  }

  refetch() {
    return this.paginationDataWrapper.refetch();
  }

  pageChanged(page: number) {
    this.paginationDataWrapper.setPageNo(page);
  }

  shouldShowToggleExpand(app: any) {
    return get(app, 'resource.services.total', 0) > 0;
  }

  toggleExpandRow(row: any, rowIndex: number) {
    this.table.toggleExpandRow(row, rowIndex); // context里包含rowIndex，在rcDataTable中用来设置rowWrapper的展开class 'rc-row-wrapper-expanded'
  }

  viewApp(app: any) {
    return this.router.navigateByUrl(`k8s_app/detail/${app.resource.uuid}`);
  }

  async startApp(app: any) {
    try {
      const response = await this.appUtilities.startApp(app);
      this.refetch();
      return response;
    } catch (e) {
      this.errorsToastService.error(e);
    }
  }

  async stopApp(app: any) {
    try {
      const response = await this.appUtilities.stopApp(app);
      this.refetch();
      return response;
    } catch (e) {
      this.errorsToastService.error(e);
    }
  }

  updateApp(app: any) {
    return this.router.navigateByUrl(`k8s_app/update/${app.resource.uuid}`);
  }

  async deleteApp(app: any) {
    try {
      const response = await this.appUtilities.deleteApp(app);
      this.refetch();
      return response;
    } catch (e) {
      this.errorsToastService.error(e);
    }
  }

  private onRequestParamsChange({
    name,
    cluster,
    repository_uuid,
    template_uuid,
  }: {
    name: string;
    cluster: string;
    repository_uuid: string;
    template_uuid: string;
  }) {
    this.appItems = [];
    this.paginationDataWrapper.setParams({
      name,
      cluster,
      repository_uuid,
      template_uuid,
    });
  }

  /**
   * paginationData 返回后重置下次轮询时间 若有部署中的应用，等待时间为3s
   */
  private resetPollingTimer() {
    clearTimeout(this.pollingTimer);
    if (this.destroyed) {
      return;
    }
    const hasDeployingApps = this.appItems.some(
      app => app.resource.status.toLowerCase() === 'deploying',
    );
    const waitTime = hasDeployingApps ? 3000 : 10000;
    this.pollingTimer = setTimeout(() => this.refetch(), waitTime);
  }
}
