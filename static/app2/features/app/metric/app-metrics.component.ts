import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { MessageService } from 'alauda-ui';
import { ServiceMetricsBase } from 'app2/features/service/metric/service-metrics-base';
import { MonitorService } from 'app2/shared/services/features/monitor.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-app-metrics',
  templateUrl: './app-metrics.component.html',
  styleUrls: ['../../service/metric/service-metrics.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppMetricsComponent extends ServiceMetricsBase implements OnInit {
  constructor(
    protected messageService: MessageService,
    protected translateService: TranslateService,
    protected monitorService: MonitorService,
  ) {
    super(messageService, translateService, monitorService);

    this.groupNameFormatting = function(tags: any, index: number) {
      if (tags.service_name) {
        return tags.service_name;
      } else if (tags.service_id) {
        return tags.service_id.substr(0, 8);
      } else {
        return `Service${index}`;
      }
    };
  }

  ngOnInit() {
    this.metrics = [
      {
        metricDisplayName: this.translateService.get('cpu_utilization'),
        metricQuery: {
          name: 'service.cpu.utilization',
          agg: 'avg',
          where: {
            app_id: this.uuid,
          },
          by: ['service_name'],
        },
      },
      {
        metricDisplayName: this.translateService.get('memory_utilization'),
        metricQuery: {
          name: 'service.mem.utilization',
          agg: 'avg',
          where: {
            app_id: this.uuid,
          },
          by: ['service_name'],
        },
      },
      {
        metricDisplayName: this.translateService.get('sent_bytes'),
        metricQuery: {
          name: 'service.net.bytes_sent',
          agg: 'avg',
          where: {
            app_id: this.uuid,
          },
          by: ['service_name'],
        },
      },
      {
        metricDisplayName: this.translateService.get('received_bytes'),
        metricQuery: {
          name: 'service.net.bytes_rcvd',
          agg: 'avg',
          where: {
            app_id: this.uuid,
          },
          by: ['service_name'],
        },
      },
    ];
  }
}
