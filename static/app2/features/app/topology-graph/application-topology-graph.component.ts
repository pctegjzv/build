import {
  AfterViewInit,
  Component,
  ElementRef,
  Inject,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { Application } from 'app2/shared/services/features/app.service';
import { get, uniq } from 'lodash';
import { DataSet, Network } from 'vis';

interface TopologyNode {
  id: string;
  label: string;
  shape?: string;
  color?: {
    [key: string]: string;
  };
  borderWidth?: number;
}
interface TopologyEdge {
  from: string;
  to: string;
  color?: string;
  arrows?: string;
}
interface TopologyData {
  nodes: TopologyNode[];
  edges: TopologyEdge[];
}

@Component({
  selector: 'rc-application-topology-graph',
  templateUrl: './application-topology-graph.component.html',
  styleUrls: ['./application-topology-graph.component.scss'],
})
export class ApplicationTopologyGraphComponent
  implements OnInit, AfterViewInit {
  @ViewChild('graphPanel')
  panel: ElementRef;
  @Input()
  app: Application;
  private TOPOLOGY_SUPPORTS: string[] = ['Deployment'];
  network: any;
  private defaultColor = '#999';
  private activeColor = '#009ce3';
  data: TopologyData;
  graphOptions = {
    physics: {
      enabled: true,
    },
    interaction: {
      dragNodes: true,
      zoomView: false,
      hover: true,
    },
    nodes: {
      shape: 'box',
      margin: {
        top: 12,
        right: 20,
        bottom: 12,
        left: 20,
      },
      color: {
        background: this.defaultColor,
        highlight: {
          background: this.activeColor,
        },
        hover: {
          background: this.activeColor,
        },
      },
      borderWidth: 0,
      font: {
        size: 14,
        color: '#fff',
        face:
          "'ff-tisa-web-pro-1', 'ff-tisa-web-pro-2', 'Lucida Grande', 'Helvetica Neue', 'Helvetica', 'Arial', 'PingFangSC', 'Hiragino Sans GB', 'Hiragino Sans GB W3', 'Microsoft YaHei', sans-serif",
      },
      chosen: {
        label: this.labelChosenFunc,
      },
    },
    edges: {
      arrows: 'to',
      smooth: {
        enabled: true,
        type: 'continuous',
      },
      color: {
        color: this.defaultColor,
        highlight: this.activeColor,
        hover: this.activeColor,
      },
      chosen: {
        edge: this.edgeChosenFunc,
      },
    },
  };

  constructor(@Inject(ENVIRONMENTS) private env: Environments) {}

  ngOnInit() {
    this.data = this.genetateTopologyData();
  }

  ngAfterViewInit() {
    if (this.data && this.data.nodes.length) {
      this.network = new Network(
        this.panel.nativeElement,
        {
          nodes: new DataSet(this.data.nodes),
          edges: new DataSet(this.data.edges),
        },
        this.graphOptions as any,
      );
    }
  }

  private edgeChosenFunc(values: any) {
    values.width = 1;
    values.color = '#009ce3';
  }

  private labelChosenFunc(values: any) {
    values.mod = 'normal';
  }

  genetateTopologyData(): TopologyData {
    let allNodes: string[] = [];
    let edges: TopologyEdge[] = [];
    this.app.kubernetes.forEach((kube: any) => {
      if (this.TOPOLOGY_SUPPORTS.includes(kube.kind)) {
        const link = get(
          kube,
          `metadata.annotations["service.${this.env.label_base_domain}/link"]`,
        );
        if (link) {
          const links = link.split(',');
          allNodes = [...allNodes, kube.metadata.name, ...links];
          edges = [
            ...edges,
            ...links.map((item: string) => {
              return {
                from: kube.metadata.name,
                to: item,
              };
            }),
          ];
        }
      }
    });
    return {
      nodes: uniq(allNodes).map((node: string) => {
        return {
          id: node,
          label: node,
        };
      }),
      edges,
    };
  }
}
