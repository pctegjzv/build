import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as jsyaml from 'js-yaml';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { AppService } from 'app2/shared/services/features/app.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './k8s-app-update.component.html',
  styleUrls: ['./k8s-app-update.component.scss'],
})
export class K8sAppUpdateComponent implements OnInit, OnDestroy {
  private routeParamsSubscription: Subscription;
  uuid: string;
  appData: any;
  appYaml: string;
  appPayload: any;
  submitting = false;
  initialized = false;
  loadingYaml = false;

  @ViewChild('appUpdateForm')
  form: NgForm;

  constructor(
    private route: ActivatedRoute,
    private appService: AppService,
    private modalService: ModalService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private router: Router,
    private errorsToastService: ErrorsToastService,
  ) {
    this.appPayload = {
      kubernetes: [],
    };
  }

  ngOnInit() {
    this.routeParamsSubscription = this.route.params
      .pipe(first())
      .subscribe(async params => {
        this.uuid = params['uuid'];
        this.loadingYaml = true;
        // [this.appData, this.appYaml] = await Promise.all([
        //   this.appService.getK8sApp(this.uuid),
        //   this.appService.getK8sAppYaml(this.uuid),
        // ]);
        this.appData = await this.appService.getK8sApp(this.uuid);
        this.appYaml = await this.appService.getK8sAppYaml(this.uuid);
        this.initialized = true;
        this.loadingYaml = false;
      });
  }

  ngOnDestroy() {
    this.routeParamsSubscription.unsubscribe();
  }

  async updateApp() {
    this.submitting = true;
    try {
      const response: any = await this.appService.patchK8sApp(
        this.uuid,
        this.appPayload,
      );
      this.auiNotificationService.success(
        this.translateService.get('update_success'),
      );
      return this.router.navigateByUrl(
        `k8s_app/detail/${response.resource.uuid}`,
      );
    } catch (err) {
      this.errorsToastService.handleGenericAjaxError({
        errors: err.errors,
        handleNonGenericCodes: true,
        fallbackMessage: this.translateService.get('create_failed'),
      });
    }
    this.submitting = false;
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.valid) {
      try {
        this.appPayload.kubernetes = jsyaml.safeLoadAll(this.appYaml);
      } catch (error) {
        // yaml parse error
        this.auiNotificationService.error(error);
        return;
      }
      try {
        await this.modalService.confirm({
          title: this.translateService.get('update'),
          content: this.translateService.get('app_service_update_app_confirm', {
            app_name: this.appData.name,
          }),
        });
        this.updateApp();
      } catch (rejection) {}
    }
  }

  cancel() {
    return this.router.navigateByUrl('k8s_app');
  }
}
