import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ApplicationDetailComponent } from 'app2/features/application/components/detail/application-detail.component';
import { ComponentDetailComponent } from 'app2/features/application/components/detail/component-detail.component';
import { ApplicationCreatePageComponent } from 'app2/features/application/pages/application-create-page.component';
import { ApplicationListPageComponent } from 'app2/features/application/pages/application-list-page.component';
import { ApplicationUpdatePageComponent } from 'app2/features/application/pages/application-update-page.component';

const appRoutes: Routes = [
  {
    path: '',
    component: ApplicationListPageComponent,
  },
  {
    path: 'create',
    component: ApplicationCreatePageComponent,
  },
  {
    path: 'update/:uuid',
    component: ApplicationUpdatePageComponent,
  },
  {
    path: 'detail/:uuid',
    component: ApplicationDetailComponent,
  },
  {
    path: 'detail/component/:uuid',
    component: ComponentDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule],
  declarations: [],
})
export class ApplicationRoutingModule {}
