import { NgModule } from '@angular/core';

import { ApplicationTopologyGraphComponent } from 'app2/features/app/topology-graph/application-topology-graph.component';
import { ApplicationRoutingModule } from 'app2/features/application/application-routing.module';
import { ApplicationAddServiceComponent } from 'app2/features/application/components/create/application-add-service.component';
import { ApplicationCreateStagedComponent } from 'app2/features/application/components/create/application-create-staged.component';
import { ApplicationCreateUiComponent } from 'app2/features/application/components/create/application-create-ui.component';
import { ApplicationCreateComponent } from 'app2/features/application/components/create/application-create.component';
import { ApplicationDetailComponent } from 'app2/features/application/components/detail/application-detail.component';
import { ComponentDetailComponent } from 'app2/features/application/components/detail/component-detail.component';
import { ApplicationListComponent } from 'app2/features/application/components/list/application-list.component';
import { ApplicationMetricsComponent } from 'app2/features/application/components/metric/application-metrics.component';
import { ApplicationServiceListComponent } from 'app2/features/application/components/service-list/application-service-list.component';
import { ApplicationCreatePageComponent } from 'app2/features/application/pages/application-create-page.component';
import { ApplicationListPageComponent } from 'app2/features/application/pages/application-list-page.component';
import { ApplicationUpdatePageComponent } from 'app2/features/application/pages/application-update-page.component';
import { ServiceAppSharedModule } from 'app2/features/lazy/service-app.shared.module';
import { ServiceCatalogServiceSharedModule } from 'app2/features/lazy/service_catalog-service.shared.module';
import { ServiceUtilitiesService } from 'app2/features/service/service-utilities.service';
import { SharedModule } from 'app2/shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    ServiceAppSharedModule,
    ServiceCatalogServiceSharedModule,
    ApplicationRoutingModule,
  ],
  declarations: [
    ApplicationListPageComponent,
    ApplicationListComponent,
    ApplicationCreateComponent,
    ApplicationCreateUiComponent,
    ApplicationCreatePageComponent,
    ApplicationAddServiceComponent,
    ApplicationCreateStagedComponent,
    ApplicationDetailComponent,
    ApplicationMetricsComponent,
    ApplicationServiceListComponent,
    ComponentDetailComponent,
    ApplicationUpdatePageComponent,
    ApplicationTopologyGraphComponent,
  ],
  providers: [ServiceUtilitiesService],
})
export class ApplicationModule {}
