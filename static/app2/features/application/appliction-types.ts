import { ImageDisplayParams } from 'app2/features/service/image-display/image-display.component';
import { ComponentViewModel } from 'app2/features/service/service.type';

export interface AppPayload {
  resource: any;
  namespace?: {
    uuid?: string;
    name?: string;
  };
  cluster?: {
    uuid?: string;
    name?: string;
  };
  kubernetes: any[]; // TODO: @type - YAML
}

export interface ServiceListItem {
  data: ComponentViewModel;
  yaml: string;
  imageDisplayParams?: ImageDisplayParams[];
}

export interface StagedConfimAction {
  type: string;
  data?: any;
  index?: number;
}
