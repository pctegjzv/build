import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { ServiceContainerFieldsComponent } from 'app2/features/service/create-fields/service-container.component';
import { ImageSelectComponent } from 'app2/features/service/image-select/image-select.component';
import {
  ComponentViewModel,
  ContainerViewModel,
  RcImageSelection,
} from 'app2/features/service/service.type';
import { NamespaceOption } from 'app2/shared/services/features/namespace.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import * as _ from 'lodash';
import { first } from 'rxjs/operators';

import {
  INT_PATTERN,
  K8S_RESOURCE_NAME_BASE,
} from '../../../../../app/components/common/config/common-pattern';

@Component({
  selector: 'rc-application-add-service',
  templateUrl: './application-add-service.component.html',
  styleUrls: [
    './application-create-ui.common.scss',
    './application-create-ui.component.scss',
  ],
})
export class ApplicationAddServiceComponent implements OnInit, OnDestroy {
  @Input()
  appUpdate: boolean;
  @Input()
  namespace: { name: string; uuid: string }; // selected namespace in service basic form
  @Input()
  serviceViewModel: ComponentViewModel; // view model of service to update
  @Output()
  onConfirmed = new EventEmitter<{ data: ComponentViewModel; yaml: string }>();
  numberReg = INT_PATTERN;
  resourceNameReg = K8S_RESOURCE_NAME_BASE;

  @ViewChild('serviceForm')
  serviceForm: NgForm;
  @ViewChildren(ServiceContainerFieldsComponent)
  private containerFields: QueryList<ServiceContainerFieldsComponent>;
  private containerIndex = 0;
  update = false;

  initialized = false;
  submitting = false;
  yamlPreview: boolean;
  yamlReadonly = true;

  showAdvanceOptions = false;

  service: ComponentViewModel;
  serviceYaml: string;

  containerPorts: number[][] = [];
  containerPortsOptions: number[] = [];

  serviceKindOptions: string[] = [];
  namespaceOptions: NamespaceOption[] = [];

  constructor(
    private modalService: ModalService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private activatedRoute: ActivatedRoute,
    private serviceService: ServiceService,
  ) {}

  async ngOnInit() {
    this.update = !!this.serviceViewModel;

    this.serviceKindOptions = ['Deployment', 'DaemonSet', 'StatefulSet'];
    this.initServcie();
    this.initialized = true;
  }

  ngOnDestroy() {}

  private async initServcie() {
    if (!this.update) {
      this.service = {
        namespace: '',
        name: '',
        // from kubernetes
        kind: '', // 部署模式
        replicas: 1, // 实例数
        labels: [],
        minReplicas: 1,
        maxReplicas: 20,
        nodeTags: [],
        affinity: null, // 亲和性，包括container亲和反亲和，key为 `service.${label_base_domain}/name`
        maxSurge: '',
        maxUnavailable: '',
        kubeServices: [],
        containers: [],
        networkMode: {
          hostNetwork: false,
          subnet: null,
        },
      };

      this.service.namespace = this.namespace.name;
      this.service.kind = this.serviceKindOptions[0];
      this.addContainer();
    } else {
      this.service = this.serviceViewModel;
      this.service.containers.forEach(
        (container: { config: ContainerViewModel; index?: number }) => {
          container.index = this.containerIndex++;
          this.containerPorts.push([]);
        },
      );
    }
  }

  async addContainer(isDefault = false) {
    const config: ContainerViewModel = {
      name: '',
      image: '',
      imageTag: '',
    };
    if (isDefault) {
      config.name =
        this.activatedRoute.snapshot.queryParamMap.get('full_image_name') || '';
      config.name =
        this.activatedRoute.snapshot.queryParamMap.get('repository_name') || '';
      this.service.containers.push({
        config,
        index: this.containerIndex++,
      });
      this.addContainerPorts();
    } else {
      try {
        setTimeout(async () => {
          const modalRef = await this.modalService.open(ImageSelectComponent, {
            title: this.translateService.get('nav_select_image'),
            width: 800,
          });
          modalRef.afterClosed.subscribe(res => {
            if (res) {
              config.image = res.full_image_name;
              config.name = res.repository_name || '';
              if (!this.service.name) {
                this.service.name = config.name;
              }
              this.service.containers.push({
                config,
                index: this.containerIndex++,
              });
              this.addContainerPorts();
            } else {
              this.onConfirmed.emit({
                data: null,
                yaml: '',
              });
            }
          });
          modalRef.componentInstance.close
            .pipe(first())
            .subscribe((res: RcImageSelection) => {
              modalRef.close(res);
            });
        });
      } catch (error) {
        // ..
      }
    }
  }

  removeContainer(index: number) {
    _.remove(
      this.service.containers,
      (container: { config: ContainerViewModel; index: number }) => {
        return index === container.index;
      },
    );
    this.removeContainerPorts(index);
  }

  containerPortsChange(ports: number[], index: number) {
    this.containerPorts[index] = ports;
    this.refreshContainerPortsOptions();
  }

  private addContainerPorts() {
    if (!this.containerPorts) {
      this.containerPorts = [];
    }
    this.containerPorts.push([]);
  }

  private removeContainerPorts(index: number) {
    this.containerPorts[index] = [];
    this.refreshContainerPortsOptions();
  }

  private refreshContainerPortsOptions() {
    if (!this.containerPorts) {
      return;
    }
    this.containerPortsOptions = _.chain(this.containerPorts)
      .flatten()
      .uniq()
      .value();
  }

  // 实例数
  shouldShowReplicas() {
    return this.service.kind !== 'DaemonSet';
  }

  // 最大可超出
  shouldShowMaxSurge() {
    return this.service.kind === 'Deployment';
  }

  // 最多不可用
  shouldShowMaxUnavliable() {
    return ['Deployment', 'DaemonSet'].includes(this.service.kind);
  }

  // 亲和性
  shouldShowAffinity() {
    return this.service.kind !== 'DaemonSet';
  }

  trackByContainerIndex(
    _index: number,
    container: {
      config: ContainerViewModel;
      index: number;
    },
  ) {
    return container.index;
  }

  generateYaml() {
    this.triggerSubmit();
    if (this.serviceForm.invalid) {
      return;
    }
    if (!this.serviceService.checkServicePayload(this.service)) {
      return;
    }
    this.serviceYaml = this.serviceService.generateServiceYamlPayload(
      this.service,
    );
    this.yamlPreview = true;
  }

  async confirm() {
    this.triggerSubmit();
    if (this.serviceForm.invalid) {
      return;
    }
    if (!this.serviceYaml) {
      if (!this.serviceService.checkServicePayload(this.service)) {
        return;
      }
      try {
        this.serviceYaml = this.serviceService.generateServiceYamlPayload(
          this.service,
        );
      } catch (error) {
        this.auiNotificationService.error(error);
        return;
      }
    }

    this.emitAppService();
  }

  cancel() {
    this.onConfirmed.emit({
      data: null,
      yaml: '',
    });
  }

  backToUi() {
    this.yamlPreview = false;
    this.serviceYaml = '';
  }

  async toggleYamlReadonly() {
    try {
      await this.modalService.confirm({
        title: this.translateService.get('confirm'),
        content: this.translateService.get(
          this.translateService.get('service_toggle_yaml_preview_confirm'),
        ),
      });
      this.yamlReadonly = false;
    } catch (e) {
      return false;
    }
  }

  private triggerSubmit() {
    this.containerFields.forEach(
      (container: ServiceContainerFieldsComponent) => {
        container.triggerSubmit();
      },
    );
    this.serviceForm.onSubmit(null);
  }

  emitAppService() {
    this.onConfirmed.emit({
      data: this.service,
      yaml: this.serviceYaml,
    });
  }
}
