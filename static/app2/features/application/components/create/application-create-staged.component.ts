import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import {
  ServiceListItem,
  StagedConfimAction,
} from 'app2/features/application/appliction-types';
import { ServiceUtilitiesService } from 'app2/features/service/service-utilities.service';
import {
  NamespaceOption,
  NamespaceService,
} from 'app2/shared/services/features/namespace.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import { updateActions, viewActions } from 'app2/utils/code-editor-config';

import { K8S_RESOURCE_NAME_BASE } from '../../../../../app/components/common/config/common-pattern';

@Component({
  selector: 'rc-application-create-staged',
  templateUrl: './application-create-staged.component.html',
  styleUrls: [
    './application-create-ui.common.scss',
    './application-create-staged.component.scss',
  ],
})
export class ApplicationCreateStagedComponent implements OnInit, OnDestroy {
  /**
   * @Input
   */
  @Input()
  update: boolean;
  private _namespace: { name: string; uuid: string };
  @Input()
  set namespace(namespace: { name: string; uuid: string }) {
    this._namespace = namespace;
    this.appNamespace = namespace;
  }
  get namespace(): { name: string; uuid: string } {
    return this._namespace;
  }

  @Input()
  appName: string;

  private _description: string;
  @Input()
  set description(value: string) {
    this._description = value;
  }
  get description(): string {
    return this._description;
  }

  private _serviceList: ServiceListItem[];
  @Input()
  set serviceList(serviceList: ServiceListItem[]) {
    this._serviceList = serviceList;
    this.addImageDisplayParams();
  }
  get serviceList(): ServiceListItem[] {
    return this._serviceList;
  }

  /**
   * @Output
   */
  @Output()
  onConfirmed = new EventEmitter<StagedConfimAction>();

  @ViewChild('appForm')
  appForm: NgForm;

  resourceNameReg = K8S_RESOURCE_NAME_BASE;
  initialized = false;
  submitting = false;
  yamlPreview: boolean;
  yamlReadonly = true;
  appYaml: string;
  namespaceOptions: NamespaceOption[];
  appNamespace = { name: '', uuid: '' };
  codeEditorOptions = {
    language: 'yaml',
    readOnly: true,
  };
  codeActionsConfig = viewActions;
  originalYaml = '';

  constructor(
    private regionService: RegionService,
    private namespaceService: NamespaceService,
    private modalService: ModalService,
    private translateService: TranslateService,
    private router: Router,
    private serviceUtilities: ServiceUtilitiesService,
  ) {}

  async ngOnInit() {
    const region = await this.regionService.getCurrentRegion();
    this.namespaceOptions = await this.namespaceService.getNamespaceOptions(
      region.id,
    );
    this.appNamespace.name = this.namespace.name;
    this.addImageDisplayParams();
    this.initialized = true;
  }

  ngOnDestroy() {}

  onNamespaceOptionChange(option: NamespaceOption) {
    if (option) {
      this.appNamespace.uuid = option.uuid;
      this.appNamespace.name = option.name;
    }
  }

  async toggleAddService() {
    this.onConfirmed.emit({
      type: 'ADD',
      data: {
        appName: this.appName,
        namespace: this.appNamespace,
        description: this.description,
      },
    });
  }

  toggleUpdateService(index: number) {
    this.onConfirmed.emit({
      type: 'UPDATE_SERVICE',
      data: {
        appName: this.appName,
        namespace: this.appNamespace,
        description: this.description,
        serviceList: this.serviceList,
      },
      index,
    });
  }

  addImageDisplayParams() {
    this.serviceList.forEach((item: ServiceListItem) => {
      item.imageDisplayParams = this.serviceUtilities.getServiceImageDisplayParams(
        item,
        'viewModel',
      );
    });
  }

  removeService(index: number) {
    this.onConfirmed.emit({
      type: 'DELETE_SERVICE',
      index,
    });
  }

  private updateAppYaml() {
    this.appYaml = this.serviceList
      .map((item: ServiceListItem) => {
        return item.yaml;
      })
      .join('---\r\n');
    this.originalYaml = this.appYaml;
  }

  async toggleYamlReadonly() {
    try {
      await this.modalService.confirm({
        title: this.translateService.get('confirm'),
        content: this.translateService.get('confirm_to_enter_yaml_edit_mode'),
      });
      this.yamlReadonly = false;
      this.codeEditorOptions = {
        ...this.codeEditorOptions,
        readOnly: false,
      };
      this.codeActionsConfig = updateActions;
    } catch (e) {
      return false;
    }
  }

  generateYaml() {
    this.appForm.onSubmit(null);
    if (this.appForm.invalid) {
      return;
    }
    this.updateAppYaml();
    this.yamlPreview = true;
  }

  backToUi() {
    this.yamlPreview = false;
    this.appYaml = '';
  }

  async confirm() {
    this.appForm.onSubmit(null);
    if (this.appForm.invalid) {
      return;
    }
    this.submitting = true;
    if (this.update) {
      this.onConfirmed.emit({
        type: 'UPDATE',
        data: {
          description: this.description,
          appYaml: this.appYaml,
        },
      });
    } else {
      this.onConfirmed.emit({
        type: 'CREATE',
        data: {
          appName: this.appName,
          namespace: this.appNamespace,
          description: this.description,
          serviceList: this.serviceList,
          appYaml: this.appYaml,
        },
      });
    }
  }

  cancel() {
    return this.router.navigateByUrl('k8s_app');
  }
}
