import {
  Component,
  Input,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import {
  AppPayload,
  ServiceListItem,
  StagedConfimAction,
} from 'app2/features/application/appliction-types';
import { ApplicationCreateStagedComponent } from 'app2/features/application/components/create/application-create-staged.component';
import { ServiceContainerFieldsComponent } from 'app2/features/service/create-fields/service-container.component';
import { ImageSelectComponent } from 'app2/features/service/image-select/image-select.component';
import {
  ComponentViewModel,
  ContainerViewModel,
  K8sComponent,
  RcImageSelection,
} from 'app2/features/service/service.type';
import {
  AppService,
  Application,
} from 'app2/shared/services/features/app.service';
import {
  NamespaceOption,
  NamespaceService,
} from 'app2/shared/services/features/namespace.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { updateActions, viewActions } from 'app2/utils/code-editor-config';
import * as jsyaml from 'js-yaml';
import _ from 'lodash';
import { first } from 'rxjs/operators';

import {
  INT_PATTERN,
  K8S_RESOURCE_NAME_BASE,
} from '../../../../../app/components/common/config/common-pattern';

@Component({
  selector: 'rc-application-create-ui',
  templateUrl: './application-create-ui.component.html',
  styleUrls: [
    './application-create-ui.common.scss',
    './application-create-ui.component.scss',
  ],
})
export class ApplicationCreateUiComponent implements OnInit, OnDestroy {
  /**
   * @Input - use for updating
   */
  @Input()
  appData: Application;
  @ViewChild(ApplicationCreateStagedComponent)
  private stagedComponent: ApplicationCreateStagedComponent;

  numberReg = INT_PATTERN;
  resourceNameReg = K8S_RESOURCE_NAME_BASE;

  @ViewChild('appForm')
  appForm: NgForm;
  @ViewChildren(ServiceContainerFieldsComponent)
  private containerFields: QueryList<ServiceContainerFieldsComponent>;
  private containerIndex = 0;
  update = false;

  initialized = false;
  submitting = false;
  yamlPreview: boolean;
  yamlReadonly = true;

  showAdvanceOptions = false;

  service: ComponentViewModel;
  serviceYaml: string;

  containerPorts: number[][] = [];
  containerPortsOptions: number[] = [];

  serviceKindOptions: string[] = [];
  namespaceOptions: NamespaceOption[] = [];

  appPayload: AppPayload;
  appYaml: string;
  serviceList: Array<ServiceListItem> = []; // data, yaml

  updatingService: ComponentViewModel;
  updatingServiceIndex: number;

  currentStep = ''; // 'basic' | 'add' | 'staged'
  codeEditorOptions = {
    language: 'yaml',
    readOnly: true,
  };
  codeActionsConfig = viewActions;
  originalYaml = '';

  constructor(
    private regionService: RegionService,
    private namespaceService: NamespaceService,
    private modalService: ModalService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private serviceService: ServiceService,
    private errorsToastService: ErrorsToastService,
    private appService: AppService,
  ) {
    this.initAppPayload();
  }

  async ngOnInit() {
    if (this.appData) {
      this.update = true;
      this.appPayload.namespace = this.appData.namespace;
      this.appPayload.resource.name = this.appData.resource.name;
      this.appPayload.resource.description = this.appData.resource.description;
      this.serviceList = this.generateServiceList(this.appData);
      this.currentStep = 'staged';
    } else {
      this.currentStep = 'basic';
      const region = await this.regionService.getCurrentRegion();
      this.appPayload.cluster.uuid = region.id;
      this.appPayload.cluster.name = region.name;
      this.namespaceOptions = await this.namespaceService.getNamespaceOptions(
        region.id,
      );
      if (this.namespaceOptions.length) {
        this.appPayload.namespace.name = this.namespaceOptions[0].name;
      }
      const appName = this.activatedRoute.snapshot.queryParamMap.get(
        'repository_name',
      );
      if (appName) {
        this.appPayload.resource.name = appName;
        this.appPayload.resource.description = appName;
      }
      this.serviceKindOptions = ['Deployment', 'DaemonSet', 'StatefulSet'];
      this.initServcie();
    }
    this.initialized = true;
  }

  ngOnDestroy() {}

  initAppPayload() {
    this.appPayload = {
      resource: {},
      namespace: {},
      cluster: {},
      kubernetes: [],
    };
  }

  private async initServcie() {
    this.service = {
      namespace: '',
      name: '',
      // from kubernetes
      kind: '', // 部署模式
      replicas: 1, // 实例数
      labels: [],
      minReplicas: 1,
      maxReplicas: 20,
      nodeTags: [],
      affinity: null, // 亲和性，包括container亲和反亲和，key为 `service.${label_base_domain}/name`
      maxSurge: '',
      maxUnavailable: '',
      kubeServices: [],
      containers: [],
      networkMode: {
        hostNetwork: false,
        subnet: null,
      },
    };
    const appName = this.activatedRoute.snapshot.queryParamMap.get(
      'repository_name',
    );
    if (appName) {
      this.service.name = appName;
    }
    this.service.kind = this.serviceKindOptions[0];
    this.addContainer(true);
  }

  generateServiceList(appData: Application): ServiceListItem[] {
    return appData.services.map((item: K8sComponent) => {
      const service = {
        namespace: item.namespace.name,
        name: item.resource.name,
      };
      const serviceData = Object.assign(
        service,
        this.serviceService.getServiceViewModel(item.kubernetes),
      );
      const serviceYaml = this.serviceService.generateServiceYamlPayload(
        serviceData,
      );
      return {
        data: serviceData,
        yaml: serviceYaml,
      };
    });
  }

  onNamespaceOptionChange(option: NamespaceOption) {
    this.appPayload.namespace.name = option.name;
    this.appPayload.namespace.uuid = option.uuid;
    this.service.namespace = option.name;
  }

  async addContainer(isDefault = false) {
    const config: ContainerViewModel = {
      name: '',
      image: '',
      imageTag: '',
    };
    if (isDefault) {
      config.image =
        this.activatedRoute.snapshot.queryParamMap.get('full_image_name') || '';
      config.name =
        this.activatedRoute.snapshot.queryParamMap.get('repository_name') || '';
      this.service.containers.push({
        config,
        index: this.containerIndex++,
      });
      this.addContainerPorts();
    } else {
      try {
        const modalRef = await this.modalService.open(ImageSelectComponent, {
          title: this.translateService.get('nav_select_image'),
          width: 800,
        });
        modalRef.componentInstance.close
          .pipe(first())
          .subscribe((res: RcImageSelection) => {
            modalRef.close();
            if (res) {
              config.image = res.full_image_name;
              config.name = res.repository_name || '';
              this.service.containers.push({
                config,
                index: this.containerIndex++,
              });
              this.addContainerPorts();
            }
          });
      } catch (error) {
        //
      }
    }
  }

  removeContainer(index: number) {
    _.remove(
      this.service.containers,
      (container: { config: ContainerViewModel; index: number }) => {
        return index === container.index;
      },
    );
    this.removeContainerPorts(index);
  }

  containerPortsChange(ports: number[], index: number) {
    this.containerPorts[index] = ports;
    this.refreshContainerPortsOptions();
  }

  private addContainerPorts() {
    if (!this.containerPorts) {
      this.containerPorts = [];
    }
    this.containerPorts.push([]);
  }

  private removeContainerPorts(index: number) {
    this.containerPorts[index] = [];
    this.refreshContainerPortsOptions();
  }

  private refreshContainerPortsOptions() {
    if (!this.containerPorts) {
      return;
    }
    this.containerPortsOptions = _.chain(this.containerPorts)
      .flatten()
      .uniq()
      .value();
  }

  // 实例数
  shouldShowReplicas() {
    return this.service.kind !== 'DaemonSet';
  }

  // 最大可超出
  shouldShowMaxSurge() {
    return this.service.kind === 'Deployment';
  }

  // 最多不可用
  shouldShowMaxUnavliable() {
    return ['Deployment', 'DaemonSet'].includes(this.service.kind);
  }

  // 亲和性
  shouldShowAffinity() {
    return this.service.kind !== 'DaemonSet';
  }

  trackByContainerIndex(
    _index: number,
    container: {
      config: ContainerViewModel;
      index: number;
    },
  ) {
    return container.index;
  }

  generateYaml() {
    this.triggerSubmitApp();
    if (this.appForm.invalid) {
      return;
    }
    if (!this.serviceService.checkServicePayload(this.service)) {
      return;
    }
    this.serviceYaml = this.serviceService.generateServiceYamlPayload(
      this.service,
    );
    this.originalYaml = this.serviceYaml;
    this.yamlPreview = true;
  }

  cancel() {
    return this.router.navigateByUrl('k8s_app');
  }

  backToUi() {
    this.yamlPreview = false;
    this.serviceYaml = '';
  }

  async toggleYamlReadonly() {
    try {
      await this.modalService.confirm({
        title: this.translateService.get('confirm'),
        content: this.translateService.get(
          this.translateService.get('service_toggle_yaml_preview_confirm'),
        ),
      });
      this.yamlReadonly = false;
      this.codeEditorOptions = {
        ...this.codeEditorOptions,
        readOnly: false,
      };
      this.codeActionsConfig = updateActions;
    } catch (e) {
      return false;
    }
  }

  addAppService() {
    this.triggerSubmitApp();
    if (this.appForm.invalid) {
      return;
    }
    this.saveAppService();
    this.updateAppYaml();
    this.currentStep = 'add';
  }

  saveAppService(appService?: { data: ComponentViewModel; yaml: string }) {
    if (this.currentStep === 'basic') {
      if (!this.serviceService.checkServicePayload(this.service)) {
        return;
      }
      try {
        this.serviceYaml = this.serviceService.generateServiceYamlPayload(
          this.service,
        );
      } catch (error) {
        this.auiNotificationService.error(error);
        return;
      }
      const newService = {
        data: this.service,
        yaml: this.serviceYaml,
      };
      this.serviceList = [newService];
    } else {
      if (!appService || !appService.data) {
        this.currentStep = 'staged'; // cancel => staged page
        return;
      }
      if (this.updatingService) {
        this.serviceList[this.updatingServiceIndex] = _.cloneDeep(appService);
        this.currentStep = 'staged';
      } else {
        const newService = {
          data: appService.data,
          yaml: appService.yaml,
        };
        this.serviceList.push(newService);
        this.serviceList = [...this.serviceList];
        this.currentStep = 'staged';
      }
    }
  }

  private updateAppYaml() {
    this.appYaml = this.serviceList
      .map((item: ServiceListItem) => {
        return item.yaml;
      })
      .join('---\r\n');
  }

  private triggerSubmitApp() {
    this.containerFields.forEach(
      (container: ServiceContainerFieldsComponent) => {
        container.triggerSubmit();
      },
    );
    this.appForm.onSubmit(null);
  }

  async confirmApp() {
    if (this.currentStep === 'basic') {
      this.triggerSubmitApp();
      if (this.appForm.invalid) {
        return;
      }
    }
    if (!this.yamlReadonly) {
      this.appYaml = this.serviceYaml;
    } else if (this.stagedComponent && !this.stagedComponent.yamlReadonly) {
      this.yamlReadonly = false;
    } else {
      this.saveAppService();
      this.updateAppYaml();
    }

    this.appPayload.resource.create_method = this.yamlReadonly ? 'UI' : 'YAML';

    try {
      this.appPayload.kubernetes = jsyaml.safeLoadAll(this.appYaml);
    } catch (error) {
      this.auiNotificationService.error(error);
      if (this.stagedComponent) {
        this.stagedComponent.submitting = false;
      }
      return;
    }

    try {
      await this.modalService.confirm({
        title: this.translateService.get('create'),
        content: this.translateService.get('app_service_create_app_confirm', {
          app_name: this.appPayload.resource.name,
        }),
      });
      this.createApp();
    } catch (rejection) {
      if (this.stagedComponent) {
        this.stagedComponent.submitting = false;
      }
    }
  }

  private async createApp() {
    this.submitting = true;
    try {
      const response: Application = await this.appService.createK8sApp(
        this.appPayload,
      );
      return this.router.navigateByUrl(
        `k8s_app/detail/${response.resource.uuid}`,
      );
    } catch (err) {
      this.errorsToastService.handleGenericAjaxError({
        errors: err.errors,
        handleNonGenericCodes: true,
        fallbackMessage: this.translateService.get('create_failed'),
      });
    }
    this.submitting = false;
    if (this.stagedComponent) {
      this.stagedComponent.submitting = false;
    }
  }

  private async updateApp(action: StagedConfimAction) {
    const appUpdatePayload: AppPayload = {
      resource: {},
      kubernetes: [],
    };
    appUpdatePayload.resource.description = action.data.description;

    this.updateAppYaml();

    if (this.stagedComponent && !this.stagedComponent.yamlReadonly) {
      appUpdatePayload.resource.create_method = 'YAML';
      this.appYaml = action.data.appYaml;
    }

    try {
      appUpdatePayload.kubernetes = jsyaml.safeLoadAll(this.appYaml);
    } catch (error) {
      this.auiNotificationService.error(error);
      if (this.stagedComponent) {
        this.stagedComponent.submitting = false;
      }
      return;
    }

    try {
      await this.modalService.confirm({
        title: this.translateService.get('update'),
        content: this.translateService.get('app_service_update_app_confirm'),
      });
    } catch (rejection) {
      if (this.stagedComponent) {
        this.stagedComponent.submitting = false;
      }
      return;
    }

    this.submitting = true;
    try {
      const response: Application = await this.appService.patchK8sApp(
        this.appData.resource.uuid,
        appUpdatePayload,
      );
      this.auiNotificationService.success(
        this.translateService.get('update_success'),
      );
      return this.router.navigateByUrl(
        `k8s_app/detail/${response.resource.uuid}`,
      );
    } catch (err) {
      this.errorsToastService.handleGenericAjaxError({
        errors: err.errors,
        handleNonGenericCodes: true,
        fallbackMessage: this.translateService.get('create_failed'),
      });
    }
    this.submitting = false;
    if (this.stagedComponent) {
      this.stagedComponent.submitting = false;
    }
  }

  stagedConfirm(action: StagedConfimAction) {
    switch (action.type) {
      case 'ADD':
        this.appPayload.resource.name = action.data.appName;
        this.appPayload.resource.description = action.data.description;
        this.appPayload.namespace.name = action.data.namespace.name;
        this.appPayload.namespace.uuid = action.data.namespace.uuid;
        this.updatingService = null;
        this.currentStep = 'add';
        break;
      case 'CREATE':
        this.appPayload.resource.name = action.data.appName;
        this.appPayload.resource.description = action.data.description;
        this.appPayload.namespace.name = action.data.namespace.name;
        this.appPayload.namespace.uuid = action.data.namespace.uuid;
        this.serviceList = action.data.serviceList;
        this.appYaml = action.data.appYaml;
        this.confirmApp();
        break;
      case 'DELETE_SERVICE':
        this.serviceList.splice(action.index, 1);
        break;
      case 'UPDATE_SERVICE':
        this.appPayload.resource.name = action.data.appName;
        this.appPayload.resource.description = action.data.description;
        this.appPayload.namespace.name = action.data.namespace.name;
        this.appPayload.namespace.uuid = action.data.namespace.uuid;
        this.updatingService = _.cloneDeep(this.serviceList[action.index].data);
        this.updatingServiceIndex = action.index;
        this.currentStep = 'add';
        break;
      case 'UPDATE':
        this.updateApp(action);
        break;
      default:
        break;
    }
  }
}
