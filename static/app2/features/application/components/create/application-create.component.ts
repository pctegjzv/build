import {
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import * as jsyaml from 'js-yaml';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { DialogService, DialogSize, NotificationService } from 'alauda-ui';
import yaml_template from 'app2/features/app/create/k8s-app-create-yaml-demo';
import { AppPayload } from 'app2/features/application/appliction-types';
import {
  AppService,
  Application,
} from 'app2/shared/services/features/app.service';
import {
  NamespaceOption,
  NamespaceService,
} from 'app2/shared/services/features/namespace.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import {
  createActions,
  updateActions,
  viewActions,
} from 'app2/utils/code-editor-config';

@Component({
  selector: 'rc-application-create',
  templateUrl: './application-create.component.html',
  styleUrls: ['./application-create.component.scss'],
})
export class ApplicationCreateComponent
  implements OnInit, OnDestroy, OnChanges {
  @Input()
  appData: Application;
  @Input()
  appYaml: string;

  private regionSubscription: Subscription;

  namespaceOptions: NamespaceOption[];
  appPayload: AppPayload;
  submitting = false;
  update: boolean;
  codeEditorOptions = {
    language: 'yaml',
    readOnly: false,
    renderLineHighlight: 'none',
  };
  codeEditorOptionsUpdate = {
    language: 'yaml',
    readOnly: false,
    renderLineHighlight: 'all',
  };

  actionsConfigCreate = createActions;
  actionsConfigUpdate = updateActions;
  actionsConfigView = viewActions;
  originalYaml: string;

  yamlTemplate = yaml_template;
  @ViewChild('appCreateForm')
  form: NgForm;

  constructor(
    private namespaceService: NamespaceService,
    private regionService: RegionService,
    private appService: AppService,
    private modalService: ModalService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private errorsToastService: ErrorsToastService,
    private router: Router,
    private dialogService: DialogService,
  ) {
    this.appPayload = {
      resource: {},
      namespace: {},
      cluster: {},
      kubernetes: [],
    };
  }

  ngOnInit() {
    if (this.appData) {
      // update
      this.update = true;
      this.appPayload = {
        resource: {},
        kubernetes: [],
      };
      this.appPayload.resource.description =
        this.appData.resource.description || '';
    } else {
      // create
      this.regionSubscription = this.regionService.region$
        .pipe(filter(region => !!region))
        .subscribe(async region => {
          this.appPayload.cluster.uuid = region.id;
          this.appPayload.cluster.name = region.name;
          this.namespaceOptions = await this.namespaceService.getNamespaceOptions(
            region.id,
          );
        });
    }
  }

  ngOnChanges({ appYaml }: SimpleChanges): void {
    if (appYaml) {
      this.originalYaml = this.appYaml;
    }
  }

  ngOnDestroy() {
    return this.regionSubscription && this.regionSubscription.unsubscribe();
  }

  onNamespaceOptionChange(option: { name: string; uuid: string }) {
    this.appPayload.namespace.name = option.name;
  }

  async createApp() {
    this.submitting = true;
    try {
      const response: Application = await this.appService.createK8sApp(
        this.appPayload,
      );
      return this.router.navigateByUrl(
        `k8s_app/detail/${response.resource.uuid}`,
      );
    } catch (err) {
      this.errorsToastService.handleGenericAjaxError({
        errors: err.errors,
        handleNonGenericCodes: true,
        fallbackMessage: this.translateService.get('create_failed'),
      });
    }
    this.submitting = false;
  }

  async updateApp() {
    this.submitting = true;
    try {
      const response: Application = await this.appService.patchK8sApp(
        this.appData.resource.uuid,
        this.appPayload,
      );
      this.auiNotificationService.success(
        this.translateService.get('update_success'),
      );
      return this.router.navigateByUrl(
        `k8s_app/detail/${response.resource.uuid}`,
      );
    } catch (err) {
      this.errorsToastService.handleGenericAjaxError({
        errors: err.errors,
        handleNonGenericCodes: true,
        fallbackMessage: this.translateService.get('create_failed'),
      });
    }
    this.submitting = false;
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.valid) {
      try {
        this.appPayload.kubernetes = jsyaml.safeLoadAll(this.appYaml);
      } catch (error) {
        // yaml parse error
        this.auiNotificationService.error({
          title: this.translateService.get('yaml_error'),
          content: error.message,
        });
        return;
      }
      if (this.update) {
        try {
          await this.modalService.confirm({
            title: this.translateService.get('update'),
            content: this.translateService.get(
              'app_service_update_app_confirm',
              {
                app_name: this.appData.resource.name,
              },
            ),
          });
          this.updateApp();
        } catch (rejection) {}
      } else {
        try {
          await this.modalService.confirm({
            title: this.translateService.get('create'),
            content: this.translateService.get(
              'app_service_create_app_confirm',
              {
                app_name: this.appPayload.resource.name,
              },
            ),
          });
          this.createApp();
        } catch (rejection) {}
      }
    }
  }

  cancel() {
    return this.router.navigateByUrl('k8s_app');
  }

  useDemoTemplate() {
    this.appYaml = yaml_template;
  }

  showDemoTemplate(template: TemplateRef<any>) {
    this.dialogService.open(template, {
      size: DialogSize.Big,
    });
  }
}
