import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { get } from 'lodash';

import { NotificationService } from 'alauda-ui';
import { VendorCustomerService } from 'app2/core/customization/vendor-customer.service';
import { CustomModuleName } from 'app2/core/customization/vendor-customer.types';
import {
  AppService,
  Application,
} from 'app2/shared/services/features/app.service';
import { AppUtilitiesService } from 'app2/shared/services/features/app.utilities.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ErrorResponse } from 'app2/shared/services/http/http.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { LoggerUtilitiesService } from 'app2/shared/services/utility/logger.service';
import { TranslateService } from 'app2/translate/translate.service';
import { viewActions } from 'app2/utils/code-editor-config';

@Component({
  templateUrl: './application-detail.component.html',
  styleUrls: ['./application-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationDetailComponent implements OnInit, OnDestroy {
  private pollingTimer: number;
  private uuid: string;
  private _loading: boolean;
  private _loadError: boolean;

  appData: Application;
  appYaml: string;
  destroyed = false;
  initialized = false;
  actionControl: {
    show: boolean;
    actionName: string;
    action: () => void;
    disabled: boolean;
  } = {
    show: false,
    actionName: '',
    action: () => null,
    disabled: true,
  };
  appTopologySupport = false;
  actionsConfigView = viewActions;

  constructor(
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
    private router: Router,
    private appService: AppService,
    private appUtilities: AppUtilitiesService,
    private errorsToastService: ErrorsToastService,
    private logger: LoggerUtilitiesService,
    private regionService: RegionService,
    private customerService: VendorCustomerService,
  ) {}

  ngOnInit() {
    this.appTopologySupport = this.customerService.support(
      CustomModuleName.APP_TOPOLOTY,
    );
    this.route.params.subscribe(async params => {
      this.uuid = params['uuid'];
      await this.refetch();
      this.setDefaultRegion();
      this.initialized = true;
    });
  }

  ngOnDestroy() {
    clearTimeout(this.pollingTimer);
    this.destroyed = true;
  }

  private initActionControl() {
    this.actionControl.show =
      this.appUtilities.showStart(this.appData) ||
      this.appUtilities.showStop(this.appData);
    this.actionControl.actionName = this.appUtilities.canStart(this.appData)
      ? 'start'
      : 'stop';
    this.actionControl.disabled = !this.appUtilities.canUpdate(this.appData);
    this.actionControl.action = () => {
      if (this.actionControl.actionName === 'start') {
        this.startApp();
      } else {
        this.stopApp();
      }
    };
  }

  private setDefaultRegion() {
    if (this.appData) {
      this.regionService.setRegionByName(this.appData.cluster.name);
    }
  }

  async refetch() {
    if (this.destroyed) {
      return;
    }
    this._loading = true;
    try {
      this.appData = await this.appService.getK8sApp(this.uuid);
      this.initActionControl();
      this._loadError = false;
    } catch ({ status, errors }) {
      this._loadError = true;
      if (status === 403) {
        this.auiNotificationService.warning(
          this.translateService.get('permission_denied'),
        );
      } else if (status === 404 && !this.initialized) {
        this.auiNotificationService.warning(
          this.translateService.get('application_not_exist'),
        );
      }
      return this.router.navigateByUrl('k8s_app');
    }

    this._loading = false;
    this.refetchAppYaml(); // no need to await
    this.resetPollingTimer();
    if (!this.destroyed) {
      this.cdr.detectChanges();
    }
  }

  get empty() {
    return !this.appData;
  }

  get loading() {
    return this._loading;
  }

  get loadError() {
    return this._loadError;
  }

  private resetPollingTimer() {
    clearTimeout(this.pollingTimer);
    if (this.destroyed) {
      return;
    }
    const waitTime =
      get(this.appData, 'resource.status', '').toLowerCase() === 'deploying'
        ? 3000
        : 10000;
    this.pollingTimer = window.setTimeout(() => {
      this.refetch();
    }, waitTime);
  }

  private async refetchAppYaml() {
    try {
      this.appYaml = await this.appService.getK8sAppYaml(this.uuid);
    } catch (err) {
      this.logger.log('Failed to get app yaml');
    }
  }

  async startApp() {
    try {
      const response = await this.appUtilities.startApp(this.appData);
      this.refetch();
      return response;
    } catch (rejection) {
      if (rejection instanceof ErrorResponse) {
        this.errorsToastService.handleGenericAjaxError({
          errors: rejection.errors,
          handleNonGenericCodes: true,
        });
      }
    }
  }

  async stopApp() {
    try {
      const response = await this.appUtilities.stopApp(this.appData);
      this.refetch();
      return response;
    } catch (rejection) {
      if (rejection instanceof ErrorResponse) {
        this.errorsToastService.handleGenericAjaxError({
          errors: rejection.errors,
          handleNonGenericCodes: true,
        });
      }
    }
  }

  async rollbackApp() {
    try {
      const response = await this.appUtilities.rollbackApp(this.appData);
      this.refetch();
      return response;
    } catch (rejection) {
      if (rejection instanceof ErrorResponse) {
        this.errorsToastService.handleGenericAjaxError({
          errors: rejection.errors,
          handleNonGenericCodes: true,
        });
      }
    }
  }

  updateApp() {
    return this.router.navigateByUrl(`k8s_app/update/${this.uuid}`);
  }

  async deleteApp() {
    try {
      await this.appUtilities.deleteApp(this.appData);
      return this.router.navigateByUrl('k8s_app');
    } catch (rejection) {
      if (rejection instanceof ErrorResponse) {
        this.errorsToastService.handleGenericAjaxError({
          errors: rejection.errors,
          handleNonGenericCodes: true,
        });
      }
    }
  }
}
