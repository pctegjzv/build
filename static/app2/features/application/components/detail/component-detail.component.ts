import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';

import { NotificationService } from 'alauda-ui';
import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';
import { ServiceEndpointsComponent } from 'app2/features/service/endpoints/service-endpoints.component';
import { ImageDisplayParams } from 'app2/features/service/image-display/image-display.component';
import { ServiceInstancesComponent } from 'app2/features/service/instances/service-instances.component';
import { ServiceUtilitiesService } from 'app2/features/service/service-utilities.service';
import { K8sComponent } from 'app2/features/service/service.type';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { ErrorResponse } from 'app2/shared/services/http/http.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { LoggerUtilitiesService } from 'app2/shared/services/utility/logger.service';
import { TranslateService } from 'app2/translate/translate.service';
import { viewActions } from 'app2/utils/code-editor-config';
import { Observable, Subject } from 'rxjs';
import {
  filter,
  first,
  map,
  pluck,
  publishReplay,
  refCount,
  takeUntil,
} from 'rxjs/operators';

import { GetByRegion } from 'app2/state-store/log_source/actions';
import { selectFeature } from 'app2/state-store/log_source/reducers';
import { AppState } from 'app2/state-store/reducers';

@Component({
  templateUrl: './component-detail.component.html',
  styleUrls: ['./component-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ComponentDetailComponent implements OnInit, OnDestroy {
  private pollingTimer: number;
  private uuid: string;
  private instances: ServiceInstancesComponent;
  private _loading: boolean;
  private _loadError: boolean;

  @ViewChild('instances')
  set instancesComponent(instances: ServiceInstancesComponent) {
    this.instances = instances;
  }

  @ViewChild(ServiceEndpointsComponent)
  serviceEndpoints: ServiceEndpointsComponent;

  serviceData: K8sComponent;
  serviceYaml: string;
  destroyed = false;
  initialized = false;

  logSourceOptions: {
    name: string;
    value: string;
  }[] = [];
  logDisplayOptions: Array<{ key: string; value: string }>;
  logSource = 'stdout';
  logDisplayType = 'polling';

  imageDisplayParams: ImageDisplayParams[] = [];

  isK8sV4$: Observable<boolean>;

  private destroy$ = new Subject();
  actionsConfigView = viewActions;
  logSourceId$: Observable<string>;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private serviceService: ServiceService,
    public serviceUtilities: ServiceUtilitiesService,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
    private cdr: ChangeDetectorRef,
    private errorsToastService: ErrorsToastService,
    private logger: LoggerUtilitiesService,
    private regionService: RegionService,
    private store: Store<AppState>,
    @Inject(WEBLABS) public weblabs: Weblabs,
  ) {}

  ngOnInit() {
    this.isK8sV4$ = this.regionService.region$.pipe(
      filter(r => !!r),
      map(region => region.platform_version === 'v4'),
    );

    this.logDisplayOptions = [
      {
        key: this.translateService.get('auto_polling'),
        value: 'polling',
      },
      {
        key: this.translateService.get('search'),
        value: 'search',
      },
    ];
    this.route.params.subscribe(async params => {
      this.uuid = params['uuid'];
      await this.refetch();
      this.fetchServiceYaml();
      this.fetchServiceLogSource();
      this.setDefaultRegion();
      this.getLogSource();
      this.initialized = true;
    });
  }

  ngOnDestroy() {
    clearTimeout(this.pollingTimer);
    this.destroyed = true;
    this.destroy$.next();
  }

  private setDefaultRegion() {
    if (this.serviceData) {
      this.regionService.setRegionByName(this.serviceData.cluster.name);
    }
  }

  getLogSource() {
    if (this.serviceData) {
      this.store.dispatch(new GetByRegion(this.serviceData.cluster.name));
      this.logSourceId$ = this.store
        .select(selectFeature)
        .pipe(
          map(entities => entities[this.serviceData.cluster.name]),
          filter(data => !!data),
          publishReplay(1),
          refCount(),
        )
        .pipe(pluck('read_log_source'))
        .pipe(map((data: any) => (data.length ? data[0].id : 'default')));
    }
  }

  async refetch() {
    if (this.destroyed) {
      return;
    }
    this._loading = true;
    try {
      this.serviceData = await this.serviceService.getK8sService(this.uuid);
      this.imageDisplayParams = this.serviceUtilities.getServiceImageDisplayParams(
        this.serviceData,
        'api',
      );
      // wait for `set instancesComponent()` method being called after ngIf become true
      setTimeout(() => {
        if (this.instances) {
          this.instances.refetch(); //  refresh pod list
        }
      });

      this._loadError = false;
    } catch ({ status, errors }) {
      this._loadError = true;
      if (status === 403) {
        this.auiNotificationService.warning(
          this.translateService.get('permission_denied'),
        );
        return this.router.navigate['/k8s_app'];
      } else if (status === 404 && !this.initialized) {
        this.auiNotificationService.warning(
          this.translateService.get('service_not_exist'),
        );
        return this.router.navigate['/k8s_app'];
      }
    }

    this._loading = false;
    this.resetPollingTimer();
    if (!this.destroyed) {
      this.cdr.detectChanges();
    }
  }

  tabSelected(index: number) {
    switch (index) {
      case 5:
        this.fetchServiceYaml();
        break;
      case 4:
        this.fetchServiceLogSource();
        break;
      default:
        break;
    }
  }

  private async fetchServiceYaml() {
    try {
      this.serviceYaml = await this.serviceService.getK8sServiceYaml(this.uuid);
    } catch (err) {
      this.logger.log('Failed to get service yaml');
    }
  }

  private async fetchServiceLogSource() {
    const res = await this.serviceService.getK8sServiceLogSource(
      this.serviceData.resource.uuid,
    );
    if (!res.includes('stdout')) {
      res.push('stdout');
    }
    this.logSourceOptions = res.map((path: string) => {
      return {
        name: path,
        value: path,
      };
    });
  }

  private resetPollingTimer() {
    if (!this.serviceData) {
      return;
    }
    clearTimeout(this.pollingTimer);
    if (this.destroyed) {
      return;
    }
    const waitTime =
      this.serviceData.resource.status.toLowerCase() === 'deploying'
        ? 3000
        : 10000;
    this.pollingTimer = window.setTimeout(async () => {
      await this.refetch();
    }, waitTime);
  }

  get empty() {
    return !this.serviceData;
  }

  get loading() {
    return this._loading;
  }

  get loadError() {
    return this._loadError;
  }

  async startService() {
    try {
      const response = await this.serviceUtilities.startService(
        this.serviceData.resource.name,
        this.serviceData.resource.uuid,
      );
      this.refetch();
      return response;
    } catch (rejection) {
      if (rejection instanceof ErrorResponse) {
        this.errorsToastService.handleGenericAjaxError({
          errors: rejection.errors,
          handleNonGenericCodes: true,
        });
      }
    }
  }

  async stopService() {
    try {
      const response = await this.serviceUtilities.stopService(
        this.serviceData.resource.name,
        this.serviceData.resource.uuid,
      );
      this.refetch();
      return response;
    } catch (rejection) {
      if (rejection instanceof ErrorResponse) {
        this.errorsToastService.handleGenericAjaxError({
          errors: rejection.errors,
          handleNonGenericCodes: true,
        });
      }
    }
  }

  updateService() {
    const type =
      this.serviceData.resource.create_method === 'UI' ? 'repo' : 'yaml';
    this.router.navigateByUrl(
      `k8s_app/update/${this.serviceData.parent.uuid}?type=${type}`,
    );
  }

  updateReplicas() {
    this.serviceUtilities
      .updateReplicas(this.serviceData.resource.uuid)
      .pipe(first())
      .subscribe((res: boolean) => {
        if (res) {
          this.refetch();
        }
      });
  }

  updateScalingConfig() {
    return this.serviceUtilities.updateScalingConfig(
      this.serviceData.resource.uuid,
      this.serviceData.resource.name,
    );
  }

  updateImageTag() {
    const { kubernetes } = this.serviceData;
    const { containers } = this.serviceService.getServiceViewModel(kubernetes);
    this.serviceUtilities
      .updateImageTag({
        uuid: this.serviceData.resource.uuid,
        containers,
        kubernetes,
      })
      .pipe(first())
      .subscribe((finish: boolean) => {
        if (finish) {
          this.refetch();
        }
      });
  }

  updateResourceSize() {
    const { kubernetes } = this.serviceData;
    const { containers } = this.serviceService.getServiceViewModel(kubernetes);
    this.serviceUtilities
      .updateResourceSize({
        uuid: this.serviceData.resource.uuid,
        containers,
        kubernetes,
      })
      .pipe(first())
      .subscribe((finish: boolean) => {
        if (finish) {
          this.refetch();
        }
      });
  }

  updateConfigmap() {
    const { kubernetes } = this.serviceData;
    const { containers } = this.serviceService.getServiceViewModel(kubernetes);
    this.serviceUtilities
      .updateConfigmap({
        uuid: this.serviceData.resource.uuid,
        containers,
        kubernetes,
        namespace: this.serviceData.namespace.name,
      })
      .pipe(first())
      .subscribe((finish: boolean) => {
        if (finish) {
          this.refetch();
        }
      });
  }

  updateEnvvars() {
    const { kubernetes } = this.serviceData;
    const { containers } = this.serviceService.getServiceViewModel(kubernetes);
    this.serviceUtilities
      .updateEnvvars({
        uuid: this.serviceData.resource.uuid,
        containers,
        kubernetes,
        namespace: this.serviceData.namespace.name,
      })
      .pipe(first())
      .subscribe((finish: boolean) => {
        if (finish) {
          this.refetch();
        }
      });
  }

  updateEndpoints() {
    this.serviceUtilities
      .updateEndpoints(this.serviceData.resource.uuid)
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        if (this.serviceEndpoints) {
          this.serviceEndpoints.refetch();
        }
      });
  }

  async rollbackService() {
    try {
      const response = await this.serviceUtilities.rollbackService(
        this.serviceData.resource.name,
        this.serviceData.resource.uuid,
      );
      this.refetch();
      return response;
    } catch (rejection) {
      if (rejection instanceof ErrorResponse) {
        this.errorsToastService.handleGenericAjaxError({
          errors: rejection.errors,
          handleNonGenericCodes: true,
        });
      }
    }
  }

  /**
   * 回滚到历史版本
   */
  async revisionService() {
    const res = await this.serviceUtilities.revisionService(
      this.serviceData.resource.uuid,
    );
    res.pipe(first()).subscribe((finish: boolean) => {
      if (finish) {
        this.refetch();
      }
    });
  }

  async retryUpdateService() {
    try {
      const response = await this.serviceUtilities.retryUpdateService(
        this.serviceData.resource.name,
        this.serviceData.resource.uuid,
      );
      this.refetch();
      return response;
    } catch (rejection) {
      if (rejection instanceof ErrorResponse) {
        this.errorsToastService.handleGenericAjaxError({
          errors: rejection.errors,
          handleNonGenericCodes: true,
        });
      }
    }
  }

  async deleteService() {
    try {
      await this.serviceUtilities.deleteService(
        this.serviceData.resource.name,
        this.serviceData.resource.uuid,
      );
      this.router.navigate(['k8s_app']);
    } catch (rejection) {
      if (rejection instanceof ErrorResponse) {
        this.errorsToastService.handleGenericAjaxError({
          errors: rejection.errors,
          handleNonGenericCodes: true,
        });
      }
    }
  }

  getServiceStrategyDataDisplay(strategyData: any) {
    if (!strategyData) {
      return '';
    }
    return Object.keys(strategyData)
      .map(key => {
        return `${key}: ${strategyData[key]}`;
      })
      .join(', ');
  }

  updateHealthCheck() {
    const { kubernetes } = this.serviceData;
    const { containers } = this.serviceService.getServiceViewModel(kubernetes);
    this.serviceUtilities.updateHealthCheck({
      uuid: this.serviceData.resource.uuid,
      containers,
      kubernetes,
    });
  }

  serviceExec() {
    this.serviceUtilities.serviceExec({
      serviceId: this.serviceData.resource.uuid,
      serviceApplication: this.serviceData.parent.uuid,
    });
  }
}
