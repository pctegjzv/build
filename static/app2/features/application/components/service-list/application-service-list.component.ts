import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'lodash';

import { ServiceUtilitiesService } from 'app2/features/service/service-utilities.service';
import { K8sComponent } from 'app2/features/service/service.type';
import { TableComponent } from 'app2/shared/components/table/table.component';
import { AppService } from 'app2/shared/services/features/app.service';
import { ErrorResponse } from 'app2/shared/services/http/http.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { LoggerUtilitiesService } from 'app2/shared/services/utility/logger.service';

@Component({
  selector: 'rc-application-service-list',
  templateUrl: './application-service-list.component.html',
  styleUrls: ['./application-service-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationServiceListComponent implements OnInit, OnDestroy {
  /**
   * app uuid to which this service list belongs
   */
  @Input()
  appUuid: string;
  /**
   * optional, app detail including service list
   */
  @Input()
  app: string;

  @ViewChild('serviceTable')
  table: TableComponent;

  serviceItems: any[] = [];
  pollingTimer: any = null;
  loading = false;
  destroyed = false;
  imageDisplayParamsMap: any = {};

  constructor(
    private appService: AppService,
    private changeDetectorRef: ChangeDetectorRef,
    private router: Router,
    private logger: LoggerUtilitiesService,
    public serviceUtilities: ServiceUtilitiesService,
    private errorsToastService: ErrorsToastService,
  ) {
    this.serviceItems = [];
  }

  ngOnDestroy() {
    clearTimeout(this.pollingTimer);
    this.destroyed = true;
  }

  ngOnInit() {
    if (this.appUuid) {
      this.refetch();
    }
  }

  get empty() {
    return this.serviceItems.length === 0;
  }

  viewService(service: any) {
    return this.router.navigateByUrl(
      `k8s_app/detail/component/${service.resource.uuid}`,
    );
  }

  async startService(service: any) {
    try {
      const response = await this.serviceUtilities.startService(
        service.resource.name,
        service.resource.uuid,
      );
      this.refetch();
      return response;
    } catch (rejection) {
      if (rejection instanceof ErrorResponse) {
        this.errorsToastService.handleGenericAjaxError({
          errors: rejection.errors,
          handleNonGenericCodes: true,
        });
      }
    }
  }

  async stopService(service: any) {
    try {
      const response = await this.serviceUtilities.stopService(
        service.resource.name,
        service.resource.uuid,
      );
      this.refetch();
      return response;
    } catch (rejection) {
      if (rejection instanceof ErrorResponse) {
        this.errorsToastService.handleGenericAjaxError({
          errors: rejection.errors,
          handleNonGenericCodes: true,
        });
      }
    }
  }

  async deleteService(service: any) {
    try {
      const response = await this.serviceUtilities.deleteService(
        service.resource.name,
        service.resource.uuid,
      );
      this.refetch();
      return response;
    } catch (rejection) {
      if (rejection instanceof ErrorResponse) {
        this.errorsToastService.handleGenericAjaxError({
          errors: rejection.errors,
          handleNonGenericCodes: true,
        });
      }
    }
  }

  private async refetch() {
    this.loading = true;
    try {
      const app: any = await this.appService.getK8sApp(this.appUuid);
      this.serviceItems = app.services;
      this.serviceItems.forEach((item: any) => {
        this.imageDisplayParamsMap[
          item.resource.uuid
        ] = this.serviceUtilities.getServiceImageDisplayParams(item, 'api');
      });
    } catch (error) {
      this.logger.error('fetch service list failed', error);
    }
    this.loading = false;
    this.resetPollingTimer();
    if (!this.destroyed) {
      this.changeDetectorRef.detectChanges();
    }
  }

  private async resetPollingTimer() {
    clearTimeout(this.pollingTimer);
    const hasDeployingServices = _.some(this.serviceItems, (service: any) => {
      return service.resource.status.toLowerCase() === 'deploying';
    });
    const waitTime = hasDeployingServices ? 3000 : 10000;
    this.pollingTimer = setTimeout(() => {
      this.refetch();
    }, waitTime);
  }

  trackByFn(_index: number, item: K8sComponent) {
    return item.resource.uuid;
  }
}
