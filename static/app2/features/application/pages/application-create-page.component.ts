import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  templateUrl: './application-create-page.component.html',
})
export class ApplicationCreatePageComponent implements OnInit, OnDestroy {
  type: string;

  constructor(private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.type = this.route.snapshot.queryParamMap.get('type'); // repo, yaml
    if (!['repo', 'yaml'].includes(this.type)) {
      this.router.navigateByUrl('k8s_app');
    }
  }

  ngOnDestroy() {}
}
