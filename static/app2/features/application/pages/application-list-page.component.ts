import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import { DialogService } from 'alauda-ui';
import { ApplicationListComponent } from 'app2/features/application/components/list/application-list.component';
import { ServiceCreateMethodSelectComponent } from 'app2/features/service/create-method-select/service-create-method-select.component';
import { ImageSelectComponent } from 'app2/features/service/image-select/image-select.component';
import { RcImageSelection } from 'app2/features/service/service.type';
import { RegionService } from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: 'application-list-page.component.html',
  styleUrls: ['application-list-page.component.scss'],
})
export class ApplicationListPageComponent implements OnInit, OnDestroy {
  appCreateEnabled = false;

  loading = false;
  searching = false;
  searchQuery = '';
  regionName: Observable<string>;
  private regionSubscrption: Subscription;
  @ViewChild(ApplicationListComponent)
  private ApplicationListComponent: ApplicationListComponent;

  constructor(
    private regionService: RegionService,
    private modal: ModalService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private router: Router,
  ) {}
  ngOnInit(): void {
    this.regionName = this.regionService.regionName$;

    this.regionSubscrption = this.regionService.region$
      .pipe(filter(region => !!region))
      .subscribe(async region => {
        if (region) {
          this.appCreateEnabled = await this.roleUtil.resourceTypeSupportPermissions(
            'application',
            { cluster_name: region.name },
            'create',
          );
        } else {
          this.appCreateEnabled = false;
        }
      });
  }

  createApp() {
    this.openCreateSelectDialog();
  }

  async openCreateSelectDialog() {
    const modalRef = await this.dialogService.open(
      ServiceCreateMethodSelectComponent,
    );
    modalRef.componentInstance.initResourceType('app');
    modalRef.componentInstance.finish.pipe(first()).subscribe((res: string) => {
      modalRef.close();
      if (res === 'repo') {
        setTimeout(() => {
          this.openImageSelectionDialog();
        }, 200);
      } else if (res === 'yaml') {
        this.router.navigateByUrl('k8s_app/create?type=yaml');
      }
    });
  }

  private async openImageSelectionDialog() {
    const modalRef = await this.modal.open(ImageSelectComponent, {
      title: this.translate.get('nav_select_image'),
      width: 800,
    });
    modalRef.componentInstance.close
      .pipe(first())
      .subscribe((res: RcImageSelection) => {
        modalRef.close();
        if (res) {
          const queryParams = Object.assign(
            {
              type: 'repo',
            },
            res,
          );
          this.router.navigate(['k8s_app/create'], { queryParams });
        }
      });
  }

  async searchChanged(searchQuery: string) {
    this.searching = true;
    this.searchQuery = searchQuery;

    await this.ApplicationListComponent.paginationDataWrapper.pagination
      .pipe(first())
      .toPromise();
    if (this.searchQuery === searchQuery) {
      this.searching = false;
    }
  }

  async refetch() {
    this.loading = true;
    await this.ApplicationListComponent.refetch();
    this.loading = false;
  }

  ngOnDestroy(): void {
    this.regionSubscrption.unsubscribe();
  }
}
