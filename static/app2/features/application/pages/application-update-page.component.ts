import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NotificationService } from 'alauda-ui';
import {
  AppService,
  Application,
} from 'app2/shared/services/features/app.service';
import { LoggerUtilitiesService } from 'app2/shared/services/utility/logger.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-application-update-page',
  templateUrl: './application-update-page.component.html',
})
export class ApplicationUpdatePageComponent implements OnInit, OnDestroy {
  private uuid: string;
  private _loadError: boolean;

  appData: Application;
  appYaml: string;
  initialized = false;
  type: string;

  constructor(
    private route: ActivatedRoute,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
    private router: Router,
    private appService: AppService,
    private logger: LoggerUtilitiesService,
  ) {}

  ngOnInit() {
    this.route.params.subscribe(async params => {
      this.uuid = params['uuid'];
      await this.refetchAppData();
      await this.refetchAppYaml();
      this.type =
        this.appData.resource.create_method === 'UI' ? 'repo' : 'yaml';
      this.initialized = true;
    });
  }

  get empty() {
    return !this.appData;
  }

  get loadError() {
    return this._loadError;
  }

  private async refetchAppYaml() {
    try {
      this.appYaml = await this.appService.getK8sAppYaml(this.uuid);
    } catch (err) {
      this.logger.log('Failed to get app yaml');
    }
  }

  private async refetchAppData() {
    try {
      this.appData = await this.appService.getK8sApp(this.uuid);
      this._loadError = false;
    } catch ({ status, errors }) {
      this._loadError = true;
      if (status === 403) {
        this.auiNotificationService.warning(
          this.translateService.get('permission_denied'),
        );
      } else if (status === 404 && !this.initialized) {
        this.auiNotificationService.warning(
          this.translateService.get('application_not_exist'),
        );
      }
      return this.router.navigateByUrl('k8s_app');
    }
  }

  ngOnDestroy() {}
}
