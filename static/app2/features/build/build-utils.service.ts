import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { BuildTriggerComponent } from 'app2/features/build/trigger/build.trigger.component';
import {
  BuildService,
  PrivateBuildConfig,
  PrivateBuildSonarqube,
} from 'app2/shared/services/features/build.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import { first } from 'rxjs/operators';

@Injectable()
export class BuildUtilsService {
  constructor(
    private modal: ModalService,
    private translate: TranslateService,
    private router: Router,
    private build: BuildService,
  ) {}

  triggerBuild(
    config:
      | PrivateBuildConfig
      | {
          buildDisplayName: string;
          buildConfigName: string;
          commitId: string;
          codeBranch: string;
          defaultCodeBranch: string;

          // To filter configs by image repo (in repo detail page)
          registryName?: string;
          repositoryName?: string;
          projectName?: string;
          onError?: (e: Error) => void;
        },
  ) {
    const buildConfig = config as PrivateBuildConfig;
    const modalRef = this.modal.open(BuildTriggerComponent, {
      title: this.translate.get('build_start_build'),
      data: buildConfig.config_id
        ? {
            buildDisplayName: buildConfig.name,
            buildConfigName: buildConfig.name,
            registryName: buildConfig.image_repo.registry.name,
            repositoryName: buildConfig.image_repo.name,
            projectName:
              buildConfig.image_repo.project &&
              buildConfig.image_repo.project.project_name,
          }
        : config,
    });

    modalRef.componentInstance.close.pipe(first()).subscribe((id: string) => {
      if (id) {
        this.router.navigate(['/build/history/detail'], {
          queryParams: {
            id,
          },
        });
      }
    });
  }

  async deleteConfig(config: PrivateBuildConfig) {
    try {
      await this.modal.confirm({
        title: this.translate.get('build_config_confirm_delete_title'),
        content: this.translate.get('build_config_confirm_delete_content', {
          name: config.name,
        }),
      });
    } catch (e) {
      return;
    }

    (config as any)._deleting = true;

    try {
      await this.build.deletePrivateBuildConfig(config);
      return true;
    } catch (e) {
      delete (config as any)._deleting;
      return;
    }
  }

  getOauthRepoLink({
    clientName,
    repoPath,
  }: {
    clientName: string;
    repoPath: string;
  }) {
    switch (clientName) {
      case 'GITHUB':
        return 'https://github.com/' + repoPath;
      case 'BITBUCKET':
        return 'https://bitbucket.org/' + repoPath;
      case 'OSCHINA':
        return 'https://git.oschina.net/' + repoPath;
    }
  }

  getSonarqubeStatus(sonarqube: PrivateBuildSonarqube) {
    switch (sonarqube.execution_status) {
      case 'B':
      case 'W':
        return 'waiting';
      case 'I':
        return 'in-progress';
      case 'S':
        if (sonarqube.integration_execution.quality_gate_value === 'OK') {
          return 'ok';
        } else {
          return 'failed';
        }
      case 'D':
      case 'F':
      default:
        return 'none';
    }
  }

  getSonarqubeStatusText(sonarqube: PrivateBuildSonarqube) {
    let status;
    switch (sonarqube.execution_status) {
      case 'B':
        status = 'BLOCKED';
        break;
      case 'W':
        status = 'WAITING';
        break;
      case 'I':
        status = sonarqube.step_at;
        break;
      case 'S':
        if (sonarqube.integration_execution.quality_gate_value === 'OK') {
          status = 'PASS';
        } else {
          status = 'NOPASS';
        }
        break;
      case 'D':
        status = 'DELETED';
        break;
      case 'F':
      default:
        status = 'FAILED';
        break;
    }
    return 'build_sonarqube_status_' + status;
  }

  getOauthRepoCommitLink({
    clientName,
    repoPath,
    commitId,
  }: {
    clientName: string;
    repoPath: string;
    commitId: string;
  }) {
    if (!commitId) {
      return;
    }

    switch (clientName) {
      case 'GITHUB':
        return `https://github.com/${repoPath}/commit/${commitId}`;
      case 'BITBUCKET':
        return `https://bitbucket.org/${repoPath}/commits/${commitId}`;
      case 'OSCHINA':
        return `https://git.oschina.net/${repoPath}/commit/${commitId}`;
      default:
        return;
    }
  }
}
