// Build step number to its value
export const BUILD_STEP_MAP = {
  '10': 'build_step_prepare_workspace',
  '11': 'build_step_validate_task_data',
  '12': 'build_step_login_registry',
  '20': 'build_step_setup_git_proxy',
  '21': 'build_step_setup_code_repo_trusty',
  '22': 'build_step_pull_code',
  '23': 'build_step_checkout_commit',
  '25': 'build_step_load_ci_config',
  '26': 'build_step_pull_ci_image',
  '27': 'build_step_run_ci_container',
  '28': 'build_step_upload_artifacts',
  '30': 'build_step_prepare_build_context',
  '31': 'build_step_pull_old_image',
  '32': 'build_step_create_image',
  '33': 'build_step_tag_image',
  '40': 'build_step_push_image',
  '41': 'build_step_clear_temp_folder',
};

export const BUILD_NEW_STEP_MAP = {
  '1100': 'build_step_prepare_workspace',
  '1110': 'build_step_validate_task_data',
  '1120': 'build_step_login_registry',
  '2200': 'build_step_setup_git_proxy',
  '2210': 'build_step_setup_code_repo_trusty',
  '2220': 'build_step_pull_code',
  '2230': 'build_step_checkout_commit',
  '2250': 'build_step_load_ci_config',
  '2254': 'build_step_run_sonarscanner',
  '2260': 'build_step_pull_ci_image',
  '2270': 'build_step_run_ci_container',
  '2280': 'build_step_upload_artifacts',
  '3300': 'build_step_prepare_build_context',
  '3310': 'build_step_pull_old_image',
  '3320': 'build_step_create_image',
  '3330': 'build_step_tag_image',
  '4400': 'build_step_push_image',
  '4410': 'build_step_clear_temp_folder',
};

// Status Enums
export const BUILD_STATUS_MAP = {
  S: 'success',
  F: 'fail',
  B: 'blocked',
  W: 'waiting',
  I: 'in-progress',
  D: 'deleted',
};

export const BUILD_STATUS_CSS_MAP = {
  S: 'success',
  F: 'error',
  B: 'pending',
  W: 'pending',
  I: 'in-progress',
};

export const BUILD_STATUS_ICON_STATUS_MAP = {
  S: 'success',
  F: 'fail',
  B: 'blocked',
  W: 'waiting',
  I: 'in-progress',
  D: 'deleted',
};

export const BUILD_AUTO_TAG_TYPES = [
  'NONE',
  'TIME' /* DEFAULT */,
  'COMMIT',
  'CUSTOM_RULE',
];

export const BUILD_CODE_REPO_CLIENT_TYPES = ['SIMPLE_GIT', 'SIMPLE_SVN'];

export const BUILD_CODE_REPO_TYPE_VALUE_DEFAULTS_MAP = {
  SIMPLE_GIT: 'master',
  SIMPLE_SVN: '/',
};

export const BUILD_OAUTH_CODE_REPOS = {
  GITHUB: 'GitHub',
  BITBUCKET: 'Bitbucket',
  OSCHINA: 'OSChina',
};

export const BUILD_CODE_CLIENT_LOGOS = {
  QUICK_BUILD: 'fa fa-gavel',
  GITHUB: 'fa fa-github',
  BITBUCKET: 'fa fa-bitbucket',
  OSCHINA: 'rb-icon-oschina',
};

export const BUILD_CODE_REPO_TYPES = ['BRANCH', 'DIR'];

export const BUILD_CODE_REPO_TYPES_MAP = {
  SIMPLE_GIT: 'BRANCH',
  BITBUCKET: 'BRANCH',
  GITHUB: 'BRANCH',
  SIMPLE_SVN: 'DIR',
};

export const BUILD_CONFIG_DEFAULT_CONTAINER_SIZE = {
  CPU: 0.5,
  MEMORY: 512,
};

export const BUILD_CONFIG_CONTAINER_SIZE_OPTIONS = {
  CPU: {
    floor: 0.25,
    ceil: 1,
    step: 0.25,
    precision: 2,
    hidePointerLabels: true,
    hideLimitLabels: true,
    showSelectionBar: true,
  },
  MEMORY: {
    floor: 256,
    ceil: 2048,
    step: 256,
    precision: 0,
    hidePointerLabels: true,
    hideLimitLabels: true,
    showSelectionBar: true,
  },
};

export const BUILD_CUSTOM_IMAGE_TAGS = {
  GIT: [
    'branch_name',
    'commit_hash',
    'author_name',
    'committer_name',
    'author_date',
    'commit_date',
    'version',
  ],
  SVN: [
    'commit_author',
    'commit_date',
    'commit_revision',
    'repository_uuid',
    'version',
  ],
};

export const BUILD_CODE_PATH_PLACEHOLDER = {
  GIT:
    'example: https://github.com/example/example.git or git@github.com:example/example.git',
  SVN: 'example: https://svn.riouxsvn.com/example-project/trunk/',
};

export const BUILD_CI_TASK_TYPES = ['custom-command', 'sonarqube'];
