import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';

import { AccountInfoOsChinaLinkComponent } from 'app2/features/user/account-info/account-info-os-china-link.component';
import { AccountService } from 'app2/shared/services/features/account.service';
import {
  BuildService,
  CodeClient as _CodeClient,
} from 'app2/shared/services/features/build.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';

import { BUILD_CODE_CLIENT_LOGOS } from '../build.constant';

interface CodeClient extends _CodeClient {
  logo?: string;
  displayName?: string;
}

@Component({
  templateUrl: './build-create-select.component.html',
  styleUrls: ['./build-create-select.component.scss'],
})
export class BuildCreateSelectComponent implements OnInit {
  initialized = false;
  isSubAccount: boolean;
  clients: CodeClient[];
  fetchingAuthUrl: boolean;

  clientAttributes: {
    [key: string]: {
      displayName: string;
      logo?: string;
    };
  } = {
    QUICK_BUILD: {
      displayName: 'build_quick_build',
    },
    GITHUB: {
      displayName: 'GitHub',
    },
    BITBUCKET: {
      displayName: 'Bitbucket',
    },
    OSCHINA: {
      displayName: 'OSChina',
    },
  };

  constructor(
    private router: Router,
    private build: BuildService,
    private account: AccountService,
    private translate: TranslateService,
    private modal: ModalService,
  ) {
    this.isSubAccount = this.account.isSubAccount();
  }

  close() {
    this.modal.closeAll();
  }

  async ngOnInit() {
    Object.entries(this.clientAttributes).forEach(kv => {
      kv[1].logo = BUILD_CODE_CLIENT_LOGOS[kv[0]];
    });

    let clients: CodeClient[];

    try {
      clients = await this.build.getPrivateBuildCodeClients();
    } catch (e) {
      clients = [];
    }

    clients.unshift({
      name: 'QUICK_BUILD',
      is_authed: true,
    });

    this.clients = clients.map(client => ({
      ...client,
      logo: this.clientAttributes[client.name].logo,
      displayName: this.clientAttributes[client.name].displayName,
    }));

    this.initialized = true;
  }

  canSelectClient(client: CodeClient) {
    // There is only one case that the user cannot select a client repo:
    // - When the client is not authed AND its a sub account
    return (
      client.is_authed || !this.isSubAccount || client.name === 'QUICK_BUILD'
    );
  }

  getClientTooltip(client: CodeClient) {
    return this.canSelectClient(client)
      ? ''
      : this.translate.get('build_cannot_link_hint', {
          name: this.clientAttributes[client.name].displayName,
        });
  }

  async clientClicked(client: CodeClient) {
    if (!this.canSelectClient(client)) {
      return;
    }

    if (client.is_authed) {
      this.close();

      return this.router.navigate(['/build/config/create'], {
        queryParams:
          client.name === 'QUICK_BUILD'
            ? undefined
            : {
                oauthCodeRepoClientName: client.name,
              },
      });
    }

    if (client.name === 'OSCHINA') {
      this.close();
      const modalRef = this.modal.open(AccountInfoOsChinaLinkComponent, {
        title: this.translate.get('oschina_link'),
        data: {
          codeClientName: client.name,
          beforeLink: () => (client.is_authed = 'linking'),
        },
      });
      modalRef.componentInstance.close
        .pipe(take(1))
        .subscribe(() => modalRef.close());
      return;
    }

    this.fetchingAuthUrl = true;

    location.href = await this.build.getPrivateBuildCodeClientAuthUrl({
      codeClientName: client.name,
      redirectParams: {
        nextStateName: 'build.config.create',
      },
    });
  }
}
