import { Component, Input } from '@angular/core';

import { PrivateBuildConfig } from 'app2/shared/services/features/build.service';

@Component({
  selector: 'rc-build-ci-config',
  templateUrl: './build-ci-config.component.html',
})
export class BuildCiConfigComponent {
  @Input()
  buildConfig: PrivateBuildConfig;
}
