import { Component, Input } from '@angular/core';

import {
  BUILD_CODE_CLIENT_LOGOS,
  BUILD_CODE_REPO_TYPES_MAP,
} from 'app2/features/build/build.constant';
import { PrivateBuildConfig } from 'app2/shared/services/features/build.service';

@Component({
  selector: 'rc-build-code-repository',
  templateUrl: './build-code-repository.component.html',
})
export class BuildCodeRepositoryComponent {
  codeRepoTypesMap = BUILD_CODE_REPO_TYPES_MAP;
  logoMap = BUILD_CODE_CLIENT_LOGOS;

  @Input()
  buildConfig: PrivateBuildConfig;
  @Input()
  oauthRepoLink: string;
}
