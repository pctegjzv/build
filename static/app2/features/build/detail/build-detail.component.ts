import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { BuildUtilsService } from 'app2/features/build/build-utils.service';
import {
  BuildService,
  PrivateBuildConfig,
} from 'app2/shared/services/features/build.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';

@Component({
  templateUrl: './build-detail.component.html',
})
export class BuildDetailComponent implements OnInit {
  name: string;
  buildConfig: PrivateBuildConfig;
  buildConfigName: string;
  oauthRepoLink: string;

  constructor(
    private route: ActivatedRoute,
    private build: BuildService,
    private roleUtilities: RoleUtilitiesService,
    private buildUtils: BuildUtilsService,
    private router: Router,
    private errorsToast: ErrorsToastService,
    private location: Location,
  ) {}

  ngOnInit() {
    this.route.queryParamMap.subscribe(async params => {
      const name = params.get('name');
      if (!name || name === this.name) {
        return;
      }
      this.name = name;
      try {
        this.buildConfig = await this.build.getPrivateBuildConfig(name);
      } catch (e) {
        this.errorsToast.error(e);
        this.location.back();
        return;
      }
      this.buildConfigName = this.build.getConfigName(this.buildConfig);
      this.oauthRepoLink = this.buildUtils.getOauthRepoLink({
        clientName: this.buildConfig.code_repo.code_repo_client,
        repoPath: this.buildConfig.code_repo.code_repo_path,
      });
    });
  }

  buttonDisplayExpr(action: string) {
    return this.roleUtilities.resourceHasPermission(
      this.buildConfig,
      'build_config',
      action,
    );
  }

  triggerBuild() {
    this.buildUtils.triggerBuild(this.buildConfig);
  }

  async deleteConfig() {
    if (await this.buildUtils.deleteConfig(this.buildConfig)) {
      this.router.navigateByUrl('/build/config');
    }
  }
}
