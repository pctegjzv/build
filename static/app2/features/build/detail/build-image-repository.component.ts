import { Component, Input, OnInit } from '@angular/core';

import { PrivateBuildConfig } from 'app2/shared/services/features/build.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-build-image-repository',
  templateUrl: './build-image-repository.component.html',
})
export class BuildImageRepositoryComponent implements OnInit {
  @Input()
  buildConfig: PrivateBuildConfig;

  translatedAutoTagType: string;

  constructor(private translate: TranslateService) {}

  ngOnInit() {
    if (
      this.buildConfig.auto_tag_type === 'CUSTOM_RULE' &&
      this.buildConfig.customize_tag_rule
    ) {
      const translatePrefix =
        'build_image_repo_custom_tag' +
        (this.buildConfig.code_repo.code_repo_client === 'SIMPLE_SVN'
          ? '_svn_'
          : '_git_');
      this.translatedAutoTagType = this.buildConfig.customize_tag_rule
        .split('-')
        .map(
          value =>
            /^{[a-z_]+}$/.test(value)
              ? this.translate.get(translatePrefix + value.replace(/[{}]/g, ''))
              : value,
        )
        .join('-');
    } else {
      this.translatedAutoTagType = this.translate.get(
        'build_image_repo_auto_tag_' + this.buildConfig.auto_tag_type,
      );
    }
  }
}
