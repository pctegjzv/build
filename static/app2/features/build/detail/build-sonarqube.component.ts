import { Component, Input, OnInit } from '@angular/core';

import {
  BuildService,
  PrivateBuildSonarqube,
} from 'app2/shared/services/features/build.service';

@Component({
  selector: 'rc-build-sonarqube',
  templateUrl: './build-sonarqube.component.html',
})
export class BuildSonarqubeComponent implements OnInit {
  @Input()
  sonarqube: PrivateBuildSonarqube;

  language: string;
  gateName: string;

  constructor(private build: BuildService) {}

  async ngOnInit() {
    const config = this.sonarqube.build_integration_config;
    const integration_id = this.sonarqube.integration_instance_id;

    try {
      const [{ languages }, { qualitygates }] = await Promise.all([
        this.build.getSonarqubeLanguages(integration_id),
        this.build.getSonarqubeQualityGates(integration_id),
      ]);

      this.language = languages.find(
        ({ key }) => key === config.code_language,
      ).name;

      this.gateName = qualitygates.find(
        ({ id }) => id === config.quality_gate,
      ).name;
    } catch (e) {
      this.language = config.code_language;
      this.gateName = config.quality_gate;
    }
  }
}
