import { Component, Input, OnInit } from '@angular/core';

import { BuildUtilsService } from 'app2/features/build/build-utils.service';
import { BUILD_CODE_CLIENT_LOGOS } from 'app2/features/build/build.constant';
import { PrivateBuild } from 'app2/shared/services/features/build.service';

@Component({
  selector: 'rc-build-history-code',
  templateUrl: './build-history-code.component.html',
})
export class BuildHistoryCodeComponent implements OnInit {
  @Input()
  buildDetail: PrivateBuild;

  logoMap = BUILD_CODE_CLIENT_LOGOS;

  oauthRepoLink: string;
  oauthRepoCommitLink: string;

  constructor(private buildUtils: BuildUtilsService) {}

  ngOnInit() {
    this.oauthRepoLink = this.buildUtils.getOauthRepoLink({
      clientName: this.buildDetail.code_repo.code_repo_client,
      repoPath: this.buildDetail.code_repo.code_repo_path,
    });

    this.oauthRepoCommitLink = this.buildUtils.getOauthRepoCommitLink({
      clientName: this.buildDetail.code_repo.code_repo_client,
      repoPath: this.buildDetail.code_repo.code_repo_path,
      commitId: this.buildDetail.code_repo.code_head_commit,
    });
  }
}
