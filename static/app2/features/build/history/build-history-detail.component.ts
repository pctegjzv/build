import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import * as moment from 'moment';

import { BuildUtilsService } from 'app2/features/build/build-utils.service';
import {
  BuildService,
  PrivateBuild,
  PrivateBuildArtifact,
} from 'app2/shared/services/features/build.service';
import { BuildUtilitiesService } from 'app2/shared/services/features/build.utilities.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './build-history-detail.component.html',
})
export class BuildHistoryDetailComponent implements OnInit, OnDestroy {
  id: string;
  buildId: string;
  buildDetail: PrivateBuild;
  artifacts: PrivateBuildArtifact[];

  sonarqubeStatus: string;
  sonarqubeStatusText: string;
  buildTriggerEnabled: boolean;
  codeBranchConfig: string;

  destroyed: boolean;
  loading: boolean;
  timer: number;

  constructor(
    private route: ActivatedRoute,
    private build: BuildService,
    private buildUtilities: BuildUtilitiesService,
    private roleUtilities: RoleUtilitiesService,
    private buildUtils: BuildUtilsService,
    private modal: ModalService,
    private translate: TranslateService,
    private router: Router,
    private errorsToast: ErrorsToastService,
    private notification: NotificationService,
    private location: Location,
  ) {}

  ngOnInit() {
    this.route.queryParamMap.subscribe(async params => {
      const id = params.get('id');

      if (!id || id === this.buildId) {
        return;
      }

      this.buildId = id;

      await this.loadBuildDetail();

      this.buildTriggerEnabled = await this.roleUtilities.resourceTypeSupportPermissions(
        'build_config',
        {
          build_config_name: this.buildDetail.build_config_name,
        },
        'trigger',
      );

      if (this.buildTriggerEnabled) {
        const buildConfigName = this.getBuildConfigName();

        const config = await this.build.getPrivateBuildConfig(buildConfigName);

        this.codeBranchConfig = config.code_repo.code_repo_type_value;
      }
    });
  }

  ngOnDestroy() {
    clearTimeout(this.timer);
    this.destroyed = true;
  }

  private calculateBuildDuration() {
    const { buildDetail } = this;
    if (buildDetail.ended_at) {
      return +moment(buildDetail.ended_at) - +moment(buildDetail.started_at);
    } else {
      return 0;
    }
  }

  private buildFinished() {
    return (
      this.buildDetail && ['S', 'F', 'D'].includes(this.buildDetail.status)
    );
  }

  private sonarqubeFinished() {
    return (
      this.buildDetail &&
      (!this.buildDetail.sonarqube ||
        ['S', 'F', 'D'].includes(this.buildDetail.sonarqube.execution_status))
    );
  }

  private async loadBuildDetail() {
    try {
      this.buildDetail = await this.build.getPrivateBuild(this.buildId);
      if (this.buildDetail.artifacts) {
        const artifactPrefix = 'build/' + this.buildDetail.build_id + '/';
        this.artifacts = this.buildDetail.artifacts.map(artifact => {
          let key = artifact.key || '';
          if (key.startsWith('/')) {
            key = key.substr(1);
          }

          return {
            ...artifact,
            path: artifactPrefix + key,
          };
        });
      }
    } catch (e) {
      this.errorsToast.error(e);
      this.router.navigateByUrl('/build/history');
      return;
    }

    if (this.buildDetail.sonarqube) {
      this.sonarqubeStatus = this.buildUtils.getSonarqubeStatus(
        this.buildDetail.sonarqube,
      );
      this.sonarqubeStatusText = this.buildUtils.getSonarqubeStatusText(
        this.buildDetail.sonarqube,
      );
    }

    (this.buildDetail as any).duration = this.calculateBuildDuration();

    // If build detail & sonarqube is not in finished state, retrieve it again in 5 seconds
    if (
      (!this.buildFinished() || !this.sonarqubeFinished()) &&
      !this.destroyed
    ) {
      this.timer = window.setTimeout(() => this.loadBuildDetail(), 5000);
    }
  }

  buttonDisplayExpr(action: string) {
    return this.roleUtilities.resourceHasPermission(
      this.buildDetail,
      'build_history',
      action,
    );
  }

  getBuildIconStatus() {
    return this.buildUtilities.getBuildIconStatus(this.buildDetail.status);
  }

  getBuildStatusIconText() {
    if (!this.buildDetail) {
      return;
    }
    return this.buildUtilities.getBuildStatusIconText(this.buildDetail);
  }

  getBuildConfigName() {
    if (!this.buildDetail) {
      return;
    }
    return this.buildDetail.config_id;
  }

  triggerBuild() {
    this.buildUtils.triggerBuild({
      buildDisplayName: this.buildDetail.build_config_name,
      buildConfigName: this.getBuildConfigName(),
      commitId: this.buildDetail.code_repo.code_head_commit,
      codeBranch:
        this.codeBranchConfig ||
        this.buildDetail.code_repo.code_repo_type_value,
      defaultCodeBranch: this.buildDetail.code_repo.code_repo_type_value,
      onError: (e: any) => {
        if (e.status) {
          // requesting history does not exist
          if (e.status !== 403) {
            this.notification.warning(this.translate.get('build_not_exist'));
          }
          // whatever, users can not see a page not exist
          this.location.back();
        }
      },
    });
  }

  async deleteBuild() {
    try {
      await this.modal.confirm({
        title: this.translate.get('build_confirm_delete_title'),
        content: this.translate.get('build_confirm_delete_content', {
          id: this.buildId,
        }),
      });
    } catch (e) {
      return;
    }

    this.loading = true;

    try {
      await this.build.deletePrivateBuild(this.buildId);
      this.router.navigateByUrl('/build/history');
    } catch (e) {
      this.errorsToast.error(e);
    }

    this.loading = false;
  }
}
