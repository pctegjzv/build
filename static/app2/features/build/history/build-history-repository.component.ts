import { Component, Input } from '@angular/core';

import { PrivateBuild } from 'app2/shared/services/features/build.service';

@Component({
  selector: 'rc-build-history-repository',
  templateUrl: './build-history-repository.component.html',
})
export class BuildHistoryRepositoryComponent {
  @Input()
  buildDetail: PrivateBuild;
}
