import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BuildCreateSelectComponent } from 'app2/features/build/create/build-create-select.component';
import { BuildTriggerComponent } from 'app2/features/build/trigger/build.trigger.component';
import {
  BuildService,
  PrivateBuild as _PrivateBuild,
} from 'app2/shared/services/features/build.service';
import { BuildUtilitiesService } from 'app2/shared/services/features/build.utilities.service';
import {
  ImageRegistry,
  ImageRegistryService,
} from 'app2/shared/services/features/image-registry.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import * as moment from 'moment';
import { take } from 'rxjs/operators';

import { BUILD_STATUS_CSS_MAP } from '../build.constant';

interface PrivateBuild extends _PrivateBuild {
  duration?: number;
}

@Component({
  selector: 'rc-build-history',
  templateUrl: './build-history.component.html',
  styleUrls: ['./build-history.component.scss'],
})
export class BuildHistoryComponent implements OnInit {
  @Input()
  buildConfigName: string;
  @Input()
  registryName: string;
  @Input()
  repositoryName: string;
  @Input()
  projectName: string;

  statusCssMap = BUILD_STATUS_CSS_MAP;

  buildConfigCreateEnabled: boolean;
  buildConfigTriggerEnabled: boolean;

  numPages: number;
  page = 1;
  pageSize = 20;
  loading: boolean;
  initialized: boolean;
  registries: ImageRegistry[];
  privateBuilds: PrivateBuild[];

  constructor(
    private router: Router,
    private roleUtilities: RoleUtilitiesService,
    private buildUtilities: BuildUtilitiesService,
    private imageRegistry: ImageRegistryService,
    private build: BuildService,
    private modal: ModalService,
    private translate: TranslateService,
  ) {}

  async ngOnInit() {
    const [
      [buildConfigCreateEnabled, buildConfigTriggerEnabled],
      registries,
    ] = await Promise.all([
      this.roleUtilities.resourceTypeSupportPermissions('build_config', {}, [
        'create',
        'trigger',
      ]),
      this.imageRegistry.find(),
      this.getPrivateBuilds(),
    ]);

    this.buildConfigCreateEnabled = buildConfigCreateEnabled;
    this.buildConfigTriggerEnabled = buildConfigTriggerEnabled;
    this.registries = registries;

    this.initialized = true;
  }

  async getPrivateBuilds() {
    this.loading = true;

    try {
      const { num_pages, results } = await this.build.getPrivateBuilds({
        build_config_name: this.buildConfigName,
        registry_name: this.registryName,
        repository_name: this.repositoryName,
        project_name: this.projectName,
        page: this.page,
      });

      results.forEach(build => {
        (build as PrivateBuild).duration = this.calculateTimeCost(build);
      });

      this.numPages = num_pages || 0;
      this.privateBuilds = results;
    } finally {
      this.loading = false;
    }
  }

  pageChanged($event: number) {
    this.page = $event;
    this.getPrivateBuilds();
  }

  calculateTimeCost(build: PrivateBuild) {
    if (build.started_at && build.ended_at) {
      const startedAt = moment(build.started_at);
      const endedAt = moment(build.ended_at);
      return +endedAt - +startedAt;
    } else {
      return -1;
    }
  }

  getConfigNameByHistory(build: PrivateBuild) {
    return this.buildUtilities.getConfigNameByHistory(build);
  }

  getOauthRepoCommitLink(build: PrivateBuild) {
    return this.buildUtilities.getOauthRepoCommitLink({
      clientName: build.code_repo.code_repo_client,
      repoPath: build.code_repo.code_repo_path,
      commitId: build.code_repo.code_head_commit,
    });
  }

  getBuildHistoryIconStatus(build: PrivateBuild) {
    return this.buildUtilities.getBuildIconStatus(build.status);
  }

  getBuildStatusIconText(build: PrivateBuild) {
    return this.buildUtilities.getBuildStatusIconText(build);
  }

  showStartBuildDialog() {
    const modalRef = this.modal.open(BuildTriggerComponent, {
      title: this.translate.get('build_start_build'),
      data: {
        buildConfigName: this.buildConfigName,
        registryName: this.registryName,
        repositoryName: this.repositoryName,
        projectName: this.projectName,
      },
    });

    modalRef.componentInstance.close.pipe(take(1)).subscribe((id: string) => {
      if (id) {
        this.router.navigate(['/build/history/detail'], {
          queryParams: {
            id,
          },
        });
      }
    });
  }

  openCodeRepoClientSelectDialog() {
    if (this.registries.length === 0) {
      return;
    }

    this.modal.open(BuildCreateSelectComponent, {
      title: this.translate.get('build_config_create'),
    });
  }
}
