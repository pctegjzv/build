import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { BuildUtilsService } from 'app2/features/build/build-utils.service';
import { BuildCreateSelectComponent } from 'app2/features/build/create/build-create-select.component';
import {
  BuildService,
  PrivateBuildConfig,
} from 'app2/shared/services/features/build.service';
import {
  ImageRegistry,
  ImageRegistryService,
} from 'app2/shared/services/features/image-registry.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';

import * as BUILD_CONSTANTS from '../build.constant';

@Component({
  templateUrl: './build-list.component.html',
  styleUrls: ['./build-list.component.scss'],
})
export class BuildListComponent implements OnInit {
  buildConfigCreateEnabled: boolean;

  count: number;
  pages: number;
  pageSize = 20;
  page = 1;
  search = '';
  registries: ImageRegistry[];
  buildConfigs: PrivateBuildConfig[];

  logoMap = BUILD_CONSTANTS.BUILD_CODE_CLIENT_LOGOS;

  loading: boolean;
  loadError: boolean;

  triggerBuild = this.buildUtils.triggerBuild;

  constructor(
    private roleUtilities: RoleUtilitiesService,
    private imageRegistry: ImageRegistryService,
    private modal: ModalService,
    private translate: TranslateService,
    private build: BuildService,
    private router: Router,
    private buildUtils: BuildUtilsService,
  ) {}

  async ngOnInit() {
    const [buildConfigCreateEnabled, registries] = await Promise.all([
      this.roleUtilities.resourceTypeSupportPermissions('build_config'),
      this.imageRegistry.find(),
      this.loadData({ force: true }),
    ]);

    this.buildConfigCreateEnabled = buildConfigCreateEnabled;
    this.registries = registries;
  }

  openCodeRepoClientSelectDialog() {
    if (!this.registries.length) {
      return;
    }

    this.modal.open(BuildCreateSelectComponent, {
      title: this.translate.get('build_config_create'),
    });
  }

  async loadData({
    search = this.search,
    page = this.page,
    force,
  }: { search?: string; page?: number; force?: boolean } = {}) {
    if (!force && search === this.search && page === this.page) {
      return;
    }

    this.loading = true;

    try {
      const {
        results,
        num_pages,
        count,
      } = await this.build.getPrivateBuildConfigs({
        page,
        page_size: this.pageSize,
        search: search == null ? this.search : search,
      });

      this.search = search;

      if (page > num_pages) {
        this.loadData({ page: num_pages });
        return;
      }

      this.page = page;
      this.pages = num_pages;
      this.count = count;

      this.buildConfigs = results;

      this.loadError = false;
    } catch (e) {
      this.loadError = true;
    }

    this.loading = false;
  }

  getConfigName(config: PrivateBuildConfig) {
    return this.build.getConfigName(config);
  }

  getBuildConfigRepoName(config: PrivateBuildConfig) {
    return config.image_repo.project
      ? `${config.image_repo.project.project_name}/${config.image_repo.name}`
      : config.image_repo.name;
  }

  buttonDisplayExpr(config: PrivateBuildConfig, action: string) {
    return this.roleUtilities.resourceHasPermission(
      config,
      'build_config',
      action,
    );
  }

  updateConfig(config: PrivateBuildConfig) {
    this.router.navigate(['/build/config/update', this.getConfigName(config)]);
  }

  async deleteConfig(config: PrivateBuildConfig) {
    if (await this.buildUtils.deleteConfig(config)) {
      this.loadData({ force: true });
    }
  }
}
