import { Component, Input, OnInit } from '@angular/core';

import { PrivateBuildSonarqube } from 'app2/shared/services/features/build.service';

@Component({
  selector: 'rc-build-code-quality-sonarqube',
  templateUrl: './build-code-quality-sonarqube.component.html',
})
export class BuildCodeQualitySonarqubeComponent implements OnInit {
  @Input()
  integrationExecution: PrivateBuildSonarqube['integration_execution'];

  metricGroup: any = {};

  ngOnInit() {
    const measures = this.integrationExecution.analysis_result.measures;
    measures.forEach(item => {
      switch (item.metric) {
        case 'alert_status':
          this.metricGroup[item.metric] = {
            value: item.value,
          };
          break;
        case 'security_rating':
        case 'sqale_rating':
        case 'reliability_rating':
          this.metricGroup[item.metric] = {
            value: String.fromCharCode(64 + parseInt(item.value, 10)),
          };
          break;
        case 'coverage':
        case 'duplicated_lines_density':
          this.metricGroup[item.metric] = this.getPercentMetric(
            item.value,
            item.metric,
          );
          break;
      }
    });
  }

  private getPercentMetric(valueStr: string, type: string) {
    const value = parseFloat(valueStr) / 100;
    const result: any = {
      value: valueStr + '%',
    };
    result.status =
      type === 'coverage'
        ? this.getCoverageStatus(value)
        : this.getRepetitionStatus(value);
    return result;
  }

  getCoverageStatus(value: number) {
    if (value >= 0.8) {
      return 'A';
    } else if (value >= 0.7) {
      return 'B';
    } else if (value >= 0.5) {
      return 'C';
    } else if (value >= 0.3) {
      return 'D';
    } else {
      return 'E';
    }
  }

  getRepetitionStatus(value: number) {
    if (value < 0.03) {
      return 'A';
    } else if (value < 0.05) {
      return 'B';
    } else if (value < 0.1) {
      return 'C';
    } else if (value < 0.2) {
      return 'D';
    } else {
      return 'E';
    }
  }
}
