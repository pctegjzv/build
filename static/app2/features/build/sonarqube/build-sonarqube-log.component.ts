import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';

import { RcLog } from 'app2/core/types';
import {
  BuildService,
  PrivateBuild,
} from 'app2/shared/services/features/build.service';

@Component({
  selector: 'rc-build-sonarqube-log',
  templateUrl: './build-sonarqube-log.component.html',
})
export class BuildSonarqubeLogComponent
  implements OnInit, OnDestroy, AfterViewInit {
  @Input()
  buildDetail: PrivateBuild;

  @ViewChild('logsContainer')
  logsContainer: ElementRef;

  logsEl: HTMLUListElement;

  private timer: number;
  private destroyed: boolean;

  blocking: boolean;
  lastLogTime: number;
  logs: RcLog[];

  constructor(private build: BuildService) {}

  ngOnInit() {
    this.polling();
  }

  ngAfterViewInit() {
    this.logsEl = this.logsContainer.nativeElement;
  }

  ngOnDestroy() {
    clearTimeout(this.timer);
    this.destroyed = true;
  }

  private nextTick() {
    this.timer = window.setTimeout(() => this.polling(), 10000);
  }

  private async polling() {
    if (this.destroyed) {
      return;
    }

    const { execution_status: status } = this.buildDetail.sonarqube;

    if (['W', 'B'].includes(status)) {
      this.blocking = true;
      this.nextTick();
    } else if (status === 'I') {
      this.blocking = false;
      await this.getLogs();
      this.nextTick();
    } else {
      this.blocking = false;
      await this.getLogs();
    }
  }

  private getParams() {
    const params: any = {
      type: 'sonarqube',
    };
    if (this.lastLogTime) {
      params.start_time = this.lastLogTime + 1;
    } else {
      params.start_time =
        this.convertToTimestamp(this.buildDetail.sonarqube.created_at) - 1800;
    }
    if (this.buildDetail.sonarqube.ended_at) {
      params.end_time =
        this.convertToTimestamp(this.buildDetail.sonarqube.ended_at) + 3600;
    } else {
      params.end_time = params.start_time + 3600 * 10;
    }
    return params;
  }

  private convertToTimestamp(dateStr: string): number {
    const DATE_REG = /^(\d{1,4})-(\d{1,2})-(\d{1,2})T(\d{1,2}):(\d{1,2}):(\d{1,2})\.(.+)$/;
    const result = dateStr.match(DATE_REG);
    const dt = new Date(
      Date.UTC(
        +result[1],
        +result[2] - 1,
        +result[3],
        +result[4],
        +result[5],
        +result[6],
      ),
    );
    return dt.getTime() / 1000;
    return;
  }

  private async getLogs() {
    const limit = 2000;

    let logsFraction: RcLog[] = [];
    let logs: RcLog[] = [];
    let retry = false;

    const params = this.getParams();

    do {
      try {
        logsFraction = await this.build.getPrivateBuildLogs({
          build_id: this.buildDetail.build_id,
          ...params,
        });

        retry = false;

        // Newly fetched logs should be at the front
        logs = logsFraction.concat(logs);

        if (logsFraction.length >= limit) {
          const previousEndTime = params.end_time;
          // There may be duplicates since the 'time' for logs is not with enough precision
          // we may need to fix it (remove the duplicates)
          params.end_time = logsFraction[0].time + 1;
          if (
            previousEndTime === params.end_time ||
            params.start_time >= params.end_time
          ) {
            break;
          }
        }
      } catch (e) {
        if (retry) {
          break;
        } else {
          retry = true; // retry one more time
        }
      }
    } while ((logsFraction.length >= limit || retry) && !this.destroyed);

    if (!logs.length) {
      return;
    }

    let scroll = false;

    if (!this.lastLogTime) {
      scroll = true;
    } else {
      scroll =
        this.logsEl.scrollTop + this.logsEl.clientHeight >=
        this.logsEl.scrollHeight;
    }

    this.lastLogTime = logs[logs.length - 1].time;

    if (this.logs) {
      this.logs.push(...logs);
    } else {
      this.logs = logs;
    }

    if (scroll) {
      setTimeout(() => (this.logsEl.scrollTop = this.logsEl.scrollHeight), 100);
    }
  }
}
