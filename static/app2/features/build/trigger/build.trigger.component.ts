import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { NotificationService } from 'alauda-ui';

import {
  BuildService,
  PrivateBuildConfig,
} from 'app2/shared/services/features/build.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './build.trigger.component.html',
  styleUrls: ['./build.trigger.component.scss'],
})
export class BuildTriggerComponent implements OnInit {
  @Output()
  close = new EventEmitter<string>();

  @ViewChild(NgForm)
  ngForm: NgForm;

  loading: boolean;
  codeHeadCommit: string;
  codeRepoTypeValue: string;
  buildConfigName: string;
  buildConfigs: PrivateBuildConfig[];
  codeBranch: string;
  isWildcardMode: boolean;
  branchPattern: RegExp;

  constructor(
    @Inject(MODAL_DATA)
    public data: {
      buildConfigName: string;
      buildDisplayName: string;
      registryName?: string;
      repositoryName?: string;
      projectName?: string;
      commitId?: string;
      codeBranch?: string;
      defaultCodeBranch?: string;
      onError?: (e: Error) => void;
    },
    private modal: ModalService,
    private build: BuildService,
    private notification: NotificationService,
    private translate: TranslateService,
    private errorsToast: ErrorsToastService,
  ) {
    this.buildConfigName = this.data.buildConfigName;
    this.codeHeadCommit = this.data.commitId;
    this.codeRepoTypeValue = this.data.defaultCodeBranch;
    this.codeBranch = this.data.codeBranch;
  }

  async ngOnInit() {
    if (this.buildConfigName) {
      return;
    }

    this.loading = true;

    let results: PrivateBuildConfig[];

    try {
      results = (await this.build.getPrivateBuildConfigs()).results;
    } catch (e) {
      if (this.data.onError) {
        this.data.onError(e);
      } else {
        this.errorsToast.error(e);
      }
      return;
    }

    this.buildConfigs = results.filter(config => {
      if (!(config.resource_actions || []).includes('build_config:trigger')) {
        return false;
      }

      const imageRepo = config.image_repo;

      return (
        !this.data.repositoryName ||
        (this.data.repositoryName === imageRepo.name &&
          this.data.projectName ===
            (imageRepo.project && imageRepo.project.project_name) &&
          this.data.registryName === imageRepo.registry.name)
      );
    });

    if (
      this.buildConfigs.length &&
      !this.buildConfigs.find(({ name }) => name === this.buildConfigName)
    ) {
      const firstConfig = this.buildConfigs[0];
      this.buildConfigName = firstConfig.uuid;
    }

    this.loading = false;
  }

  onBuildConfigChange(config: PrivateBuildConfig) {
    this.codeBranch = config.code_repo.code_repo_type_value;
    this.isWildcardMode = this.codeBranch.includes('*');
    if (this.isWildcardMode) {
      this.branchPattern = this.getBranchPattern(this.codeBranch);
    }
  }

  getBranchPattern(branch: string) {
    // js正则不支持前瞻匹配
    const placeholder = '"/~{[(reg-pl)]}~/"';
    const r1 = '[A-Za-z0-9_-]' + placeholder;
    const r2 = '\\S' + placeholder;
    branch = branch.replace(/\*\*/g, r2);
    branch = branch.replace(/\*/g, r1);
    branch = branch.split(placeholder).join('*');
    return new RegExp(`^${branch}$`, 'i');
  }

  cancel() {
    this.modal.closeAll();
  }

  async confirm() {
    if (this.ngForm.invalid) {
      return;
    }

    this.loading = true;

    try {
      const { build_id } = await this.build.createPrivateBuild({
        build_config_name: this.buildConfigName,
        code_head_commit: this.codeHeadCommit,
        code_repo_type_value: this.codeRepoTypeValue,
      });

      this.modal.closeAll();
      this.close.emit(build_id);

      this.notification.success(this.translate.get('build_create_success'));
    } catch (e) {
      this.errorsToast.error(e);
    }

    this.loading = false;
  }
}
