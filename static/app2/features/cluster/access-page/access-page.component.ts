import { Location } from '@angular/common';
import { Component, Inject, OnDestroy, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { merge } from 'rxjs';
import { map, pluck, publishReplay, refCount, tap } from 'rxjs/operators';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import {
  ClusterAccessData,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { ErrorResponse } from 'app2/shared/services/http/http.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { AppState } from 'app2/state-store/reducers';
import * as regionActions from 'app2/state-store/region/actions';

const CLUSTER_TYPES = [
  {
    display: 'OpenShift',
    value: 'openshift',
  },
  {
    display: 'Original kubernetes',
    value: 'original',
  },
];

const CLOUD_HOST_TYPES = [
  {
    display: 'Private',
    value: 'PRIVATE',
  },
  {
    display: 'VMWare',
    value: 'VMWARE',
  },
];

const DOCKER_VERSIONS = ['1.12.6'];

const NETWORK_MODES = ['cvs'];

@Component({
  templateUrl: './access-page.component.html',
  styleUrls: ['./access-page.component.scss'],
})
export class AccessPageComponent implements OnDestroy {
  @ViewChild(NgForm)
  ngForm: NgForm;

  CLUSTER_TYPES = CLUSTER_TYPES;
  CLOUD_HOST_TYPES = CLOUD_HOST_TYPES;
  DOCKER_VERSIONS = DOCKER_VERSIONS;
  NETWORK_MODES = NETWORK_MODES;

  endpoint: string;
  token: string;
  k8sVersion: string;

  name: string;
  displayName: string;

  clusterType = CLUSTER_TYPES[0].value;
  cloudHostType = CLOUD_HOST_TYPES[0].value;
  dockerVersion = DOCKER_VERSIONS[0];
  dockerPath = '/var/lib/docker';
  networkMode = NETWORK_MODES[0];
  nicName = 'eth0';

  loading: boolean;
  isNextStep: boolean;

  loading$ = merge(
    this.actions.ofType(regionActions.CREATE),
    this.actions
      .ofType(regionActions.CREATE_FAIL)
      .pipe(pluck('rawError'))
      .pipe(tap((rawError: ErrorResponse) => this.errorsToast.error(rawError))),
    this.actions
      .ofType(regionActions.CREATE_SUCC)
      .pipe(
        tap((_action: regionActions.CreateSucc) =>
          this.router.navigate(['/cluster/detail-v4/', this.name]),
        ),
      ),
  )
    .pipe(
      pluck('type'),
      map((type: string) => type === regionActions.CREATE),
      publishReplay(1),
      refCount(),
    )
    .subscribe(loading => (this.loading = loading));

  constructor(
    @Inject(ACCOUNT) private account: RcAccount,
    private router: Router,
    private location: Location,
    private region: RegionService,
    private errorsToast: ErrorsToastService,
    private store: Store<AppState>,
    private actions: Actions,
  ) {}

  ngOnDestroy() {
    this.loading$.unsubscribe();
  }

  cancel() {
    this.location.back();
  }

  async confirm() {
    if (this.ngForm.invalid) {
      return;
    }

    if (this.isNextStep) {
      const params: ClusterAccessData = {
        name: this.name,
        display_name: this.displayName,
        namespace: this.account.namespace,
        attr: {
          cluster: {
            nic: this.nicName,
          },
          docker: {
            path: this.dockerPath,
            version: this.dockerVersion,
          },
          cloud: {
            name: this.cloudHostType,
          },
          kubernetes: {
            type: this.clusterType,
            endpoint: this.endpoint,
            token: this.token,
            version: this.k8sVersion,
            cni: {
              type: this.networkMode,
            },
          },
        },
        features: {
          logs: {
            type: 'third-party',
            storage: {
              read_log_source: 'default',
              write_log_source: 'default',
            },
          },
          'service-catalog': {
            type: 'official',
          },
        },
      };

      this.store.dispatch(new regionActions.Create(params));
    } else {
      this.loading = true;

      try {
        const { version } = await this.region.regionCheck({
          endpoint: this.endpoint,
          token: this.token,
        });

        this.k8sVersion = version;
      } catch (e) {
        this.errorsToast.error(e);
        return;
      } finally {
        this.loading = false;
      }

      const { endpoint, token } = this;

      this.isNextStep = true;

      this.ngForm.resetForm();

      this.endpoint = endpoint;
      this.token = token;
    }
  }
}
