import { NgModule } from '@angular/core';

import { AccessPageModule } from 'app2/features/cluster/access-page/access-page.module';
import { ClusterRoutingModule } from 'app2/features/cluster/cluster.routing.module';
import { CreatePageModule } from 'app2/features/cluster/create-page/create-page.module';
import { DeployPageModule } from 'app2/features/cluster/deploy-page/deploy-page.module';
import { DetailPageModule } from 'app2/features/cluster/detail-page/detail-page.module';
import { DetailV4Module } from 'app2/features/cluster/detail-v4/detail-v4.module';
import { K8sEventModule } from 'app2/features/cluster/k8s-event/k8s-event.module';
import { ListPageModule } from 'app2/features/cluster/list-page/list-page.module';
import { MirrorPageModule } from 'app2/features/cluster/mirror-page/mirror-page.module';
import { ClusterNamespaceModule } from 'app2/features/cluster/namespace/cluster-namespace.module';
import { NodeDetailPageModule } from 'app2/features/cluster/node-detail-page/node-detail-page.module';

@NgModule({
  imports: [
    ClusterRoutingModule,
    ListPageModule,
    DetailPageModule,
    CreatePageModule,
    MirrorPageModule,
    AccessPageModule,
    DetailV4Module,
    DeployPageModule,
    NodeDetailPageModule,
    ClusterNamespaceModule,
    K8sEventModule,
  ],
  exports: [ListPageModule, DetailPageModule, CreatePageModule, K8sEventModule],
})
export class ClusterModule {}
