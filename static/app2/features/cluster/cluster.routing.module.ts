import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccessPageComponent } from 'app2/features/cluster/access-page/access-page.component';
import { CreatePageComponent } from 'app2/features/cluster/create-page/create-page.component';
import { DeployPageComponent } from 'app2/features/cluster/deploy-page/deploy-page.component';
import { DetailPageComponent } from 'app2/features/cluster/detail-page/detail-page.component';
import { DetailV4Component } from 'app2/features/cluster/detail-v4/detail-v4.component';
import { FeaturesComponent } from 'app2/features/cluster/detail-v4/features/features.component';
import { NodeDetailComponent } from 'app2/features/cluster/detail-v4/node-detail/node-detail.component';
import { K8sEventListComponent } from 'app2/features/cluster/k8s-event/list/k8s-event-list.component';
import { ListPageComponent } from 'app2/features/cluster/list-page/list-page.component';
import { MirrorDetailComponent } from 'app2/features/cluster/mirror-page/mirror-detail.component';
import { MirrorEditComponent } from 'app2/features/cluster/mirror-page/mirror-edit.component';
import { MirrorPageComponent } from 'app2/features/cluster/mirror-page/mirror-page.component';
import { ClusterNamespaceDetailComponent } from 'app2/features/cluster/namespace/detail/cluster-namespace-detail.component';
import { ClusterNamespaceListComponent } from 'app2/features/cluster/namespace/list/cluster-namespace-list.component';
import { ClusterNamespaceEditQuotaComponent } from 'app2/features/cluster/namespace/quota/cluster-namespace-edit-quota.component';
import { NodeDetailPageComponent } from 'app2/features/cluster/node-detail-page/node-detail-page.component';

const clusterRoutes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'list',
    component: ListPageComponent,
  },
  {
    path: 'detail/:name',
    component: DetailPageComponent,
  },
  {
    path: 'create',
    component: CreatePageComponent,
  },
  {
    path: 'mirror',
    component: MirrorPageComponent,
    pathMatch: 'full',
  },
  {
    path: 'mirror/detail/:name',
    component: MirrorDetailComponent,
  },
  {
    path: 'mirror/create',
    component: MirrorEditComponent,
  },
  {
    path: 'mirror/update/:name',
    component: MirrorEditComponent,
  },
  {
    path: 'access',
    component: AccessPageComponent,
  },
  {
    path: 'detail-v4/:name',
    component: DetailV4Component,
  },
  {
    path: 'node_detail-v4/:clusterName/:privateIp',
    component: NodeDetailComponent,
  },
  {
    path: ':regionName/features',
    component: FeaturesComponent,
  },
  {
    path: 'deploy/:name',
    component: DeployPageComponent,
  },
  {
    path: 'node_detail/:clusterName/:privateIp',
    component: NodeDetailPageComponent,
  },
  {
    path: 'namespace',
    component: ClusterNamespaceListComponent,
    pathMatch: 'full',
  },
  {
    path: 'namespace/detail/:clusterId/:namespace',
    component: ClusterNamespaceDetailComponent,
  },
  {
    path: 'namespace/create-quota/:clusterId/:namespace',
    component: ClusterNamespaceEditQuotaComponent,
  },
  {
    path: 'namespace/update-quota/:clusterId/:namespace/:quotaName',
    component: ClusterNamespaceEditQuotaComponent,
  },
  {
    path: 'k8s_event',
    component: K8sEventListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(clusterRoutes)],
  exports: [RouterModule],
})
export class ClusterRoutingModule {}
