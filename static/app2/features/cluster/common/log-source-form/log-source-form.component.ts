import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { IntegrationService } from 'app2/shared/services/features/integration.service';
import { RegionLogSource } from 'app2/shared/services/features/region.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-log-source-form',
  templateUrl: 'log-source-form.component.html',
  styleUrls: ['log-source-form.component.scss'],
})
export class LogSourceFormComponent implements OnInit {
  @Input()
  init: RegionLogSource;

  @ViewChild('form')
  form: NgForm;

  get value() {
    return (
      this.form && {
        write_log_source: this.form.value.write_log_source.join(','),
        read_log_source: this.form.value.read_log_source,
      }
    );
  }

  writeLogSource: string[];
  readLogSource: string;

  queryLogSources: { id: string; name: string }[] = [
    {
      id: '',
      name: this.translateService.get('pipeline_select_none'),
    },
  ];
  outputLogSources: { id: string; name: string; readable: boolean }[] = [];

  constructor(
    private translateService: TranslateService,
    private integrationService: IntegrationService,
  ) {}

  async ngOnInit() {
    try {
      this.outputLogSources = (await this.integrationService.getIntegrations({
        page: 1,
        page_size: 200,
        families: 'LogSource',
      }))
        .filter(item => item.enabled)
        .map(item =>
          Object.assign({}, item, { readable: !!item.fields.query_address }),
        )
        .map(({ id, name, readable }) => ({ id, name, readable }));
    } catch (err) {
    } finally {
      this.outputLogSources.unshift({
        id: 'default',
        name: 'default',
        readable: true,
      });
      setTimeout(() => {
        if (this.init) {
          this.writeLogSource = this.init.write_log_source.map(
            (item: any) => item.id || '',
          );
          this.readLogSource = this.init.read_log_source[0]
            ? this.init.read_log_source[0].id
            : '';
        } else {
          this.writeLogSource = ['default'];
          this.readLogSource = 'default';
        }
        this.updateQueryLogOptions(this.writeLogSource);
      });
    }
  }

  updateQueryLogOptions(value: string[]) {
    this.queryLogSources = this.outputLogSources
      .filter(item => value.includes(item.id))
      .filter(item => item.readable);
    this.queryLogSources.unshift({
      id: '',
      name: this.translateService.get('pipeline_select_none'),
    });
  }
}
