import { NgModule } from '@angular/core';

import { LogSourceFormComponent } from 'app2/features/cluster/common/log-source-form/log-source-form.component';
import { SharedModule } from 'app2/shared/shared.module';

@NgModule({
  imports: [SharedModule],
  declarations: [LogSourceFormComponent],
  exports: [LogSourceFormComponent],
})
export class LogSourceFormModule {}
