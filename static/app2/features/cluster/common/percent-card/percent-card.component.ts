import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { SerieChartData } from 'app2/features/dashboard/charts/ngx-charts.types';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-percent-card',
  templateUrl: 'percent-card.component.html',
  styleUrls: ['percent-card.component.scss'],
})
export class PercentCardComponent implements OnChanges {
  static readonly OK_COLOR = '#67d387';
  static readonly WARNING_COLOR = '#ffbb00';
  static readonly ERROR_COLOR = '#fc5d68';
  static readonly SILVER_COLOR = '#dde8ed';

  @Input()
  allocated: number;
  @Input()
  total: number;
  @Input()
  utilization: number;
  @Input()
  title: string;
  @Input()
  vertical = false;
  @Input()
  fixed = 0;

  results: SerieChartData[];

  usedText: string;
  residueText: string;
  colors: [string, string] = [
    PercentCardComponent.OK_COLOR,
    PercentCardComponent.SILVER_COLOR,
  ];
  legendColor = 'green';
  toFloat = parseFloat;

  constructor(private translateService: TranslateService) {
    this.usedText = this.translateService.get('used');
    this.residueText = this.translateService.get('residue');
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.allocated || changes.total) {
      this.results = [
        {
          name: this.usedText,
          value: this.allocated || 0,
        },
        {
          name: this.residueText,
          value: this.total - this.allocated || 0,
        },
      ];
    }
    if (changes.utilization) {
      const val = changes.utilization.currentValue;
      if (val < 0.75) {
        this.legendColor = 'green';
        this.colors[0] = PercentCardComponent.OK_COLOR;
      } else if (val < 0.9) {
        this.legendColor = 'yellow';
        this.colors[0] = PercentCardComponent.WARNING_COLOR;
      } else {
        this.legendColor = 'red';
        this.colors[0] = PercentCardComponent.ERROR_COLOR;
      }
    }
  }
}
