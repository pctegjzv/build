import { NgModule } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { PercentCardComponent } from 'app2/features/cluster/common/percent-card/percent-card.component';

@NgModule({
  imports: [NgxChartsModule],
  declarations: [PercentCardComponent],
  exports: [PercentCardComponent],
})
export class PercentCardModule {}
