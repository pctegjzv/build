import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { merge } from 'rxjs';
import { map, pluck, publishReplay, refCount, tap } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import {
  CLOUD_CONFIGS,
  CloudNames,
  combineSubmitData,
} from 'app2/features/cluster/utils';
import { DynamicFormComponent } from 'app2/shared/components/dynamic-form/dynamic-form.component';
import { DynamicFormFieldDefinition } from 'app2/shared/form-field-control';
import {
  ContainerManagers,
  RegionCreateData,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { AppState } from 'app2/state-store/reducers';
import * as regionActions from 'app2/state-store/region/actions';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-create-page',
  templateUrl: 'create-page.component.html',
  styleUrls: ['create-page.component.scss'],
})
export class CreatePageComponent implements OnInit {
  @ViewChild('configForm')
  configForm: FormGroupDirective;
  @ViewChild('cloudForm')
  cloudForm: DynamicFormComponent;

  cloudOptions: any[];
  cloudFields: DynamicFormFieldDefinition[] = [];
  dockerVersions: string[] = [];
  loading$ = merge(
    this.actions.ofType(regionActions.CREATE),
    this.actions
      .ofType(regionActions.CREATE_FAIL)
      .pipe(pluck('payload'))
      .pipe(
        tap((payload: string[]) => {
          this.auiNotificationService.error(
            payload.join(';') || this.translateService.get('unknown_issue'),
          );
        }),
      ),
    this.actions.ofType(regionActions.CREATE_SUCC).pipe(
      tap((action: regionActions.CreateSucc) => {
        this.router.navigateByUrl(`/cluster/detail/${action.payload.name}`);
      }),
    ),
  ).pipe(
    pluck('type'),
    map((type: string) => type === regionActions.CREATE),
    publishReplay(1),
    refCount(),
  );

  baseConfig: RegionCreateData = {
    name: '',
    display_name: '',
    over_commit: 1,
    container_manager: ContainerManagers.None,
    namespace: '',
    attr: {
      cloud: {
        name: CloudNames.Private,
      },
      docker: {
        path: '/var/lib/docker',
        version: '',
      },
      nic: 'eth0',
    },
  };

  constructor(
    private translateService: TranslateService,
    private regionService: RegionService,
    @Inject(ACCOUNT) account: RcAccount,
    private store: Store<AppState>,
    private actions: Actions,
    private router: Router,
    private auiNotificationService: NotificationService,
  ) {
    this.baseConfig.namespace = account.namespace;
  }

  ngOnInit() {
    this.cloudOptions = this.buildCloudOptions();

    this.regionService.getSupportedDockerVersions().then(res => {
      this.baseConfig.attr.docker.version = res.default;
      this.dockerVersions = res.versions;
    });
  }

  onCloudChanged(option: any) {
    this.cloudFields = this.buildFormFieldDefinition(option.value);
    this.cloudForm.onReset();
  }

  buildCloudOptions() {
    return CLOUD_CONFIGS.map(item => {
      const displayName = this.translateService.get(
        item.name !== CloudNames.Private
          ? item.name.toLocaleLowerCase()
          : 'region_cloud_private',
      );
      const iconSrc = `/static/images/cloud/${item.name.toLowerCase()}.svg`;
      return {
        display: `<img class="image" src="${iconSrc}"><span>${displayName}</span>`,
        value: item.name,
      };
    });
  }

  buildFormFieldDefinition(
    cloudName: CloudNames,
  ): DynamicFormFieldDefinition[] {
    return CLOUD_CONFIGS.find(item => item.name === cloudName)
      .fields.filter(field => !field.hidden)
      .map(field => ({
        controlType: 'rc-input',
        type: field.type,
        value: field.default,
        name: field.key,
        label: this.translateService.get('region_' + field.key),
        required: field.required || false,
        pattern: field.pattern,
        description: field.desc && this.translateService.get(field.desc),
      }));
  }

  create() {
    this.configForm.onSubmit(null);
    if (!this.configForm.valid || !this.cloudForm.valid) {
      return;
    }
    this.store.dispatch(
      new regionActions.Create(
        combineSubmitData(this.baseConfig, this.cloudForm.value),
      ),
    );
  }
}
