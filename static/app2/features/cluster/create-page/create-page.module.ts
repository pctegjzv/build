import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CreatePageComponent } from 'app2/features/cluster/create-page/create-page.component';
import { SharedModule } from 'app2/shared/shared.module';

@NgModule({
  imports: [SharedModule, RouterModule],
  declarations: [CreatePageComponent],
})
export class CreatePageModule {}
