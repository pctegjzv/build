import { Component, Inject, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { NotificationService } from 'alauda-ui';
import {
  BehaviorSubject,
  Observable,
  ReplaySubject,
  combineLatest,
} from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  first,
  map,
  pluck,
  publishReplay,
  refCount,
  scan,
  switchMap,
  tap,
} from 'rxjs/operators';

import { ENVIRONMENTS, WEBLABS } from 'app2/core/tokens';
import { Environments, Weblabs } from 'app2/core/types';
import { TagsInputComponent } from 'app2/shared/components/tags-input/tags-input.component';
import { AccountService } from 'app2/shared/services/features/account.service';
import {
  ContainerManagers,
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { GetByRegion as GetNodesByRegion } from 'app2/state-store/node/actions';
import { selectFeature as selectNodeFeature } from 'app2/state-store/node/reducers';
import { AppState } from 'app2/state-store/reducers';
import * as regionActions from 'app2/state-store/region/actions';
import * as fromRegion from 'app2/state-store/region/reducers';
import { Entities } from 'app2/state-store/region/reducers';
import { TranslateService } from 'app2/translate/translate.service';

import { LogSourceFormComponent } from '../common/log-source-form/log-source-form.component';
import { CLUSTER_DEPLOY_TEMPLATES, CloudNames } from '../utils';

interface ScCreateChoice {
  type: string;
  enabled: boolean;
  text: string;
  active: boolean;
  default: boolean;
}

@Component({
  selector: 'rc-deploy-page',
  templateUrl: 'deploy-page.component.html',
  styleUrls: ['deploy-page.component.scss'],
})
export class DeployPageComponent {
  @ViewChild('form')
  form: NgForm;
  @ViewChild('logSource')
  logSource: LogSourceFormComponent;
  @ViewChild('nodeDrive')
  nodeDrive: TagsInputComponent;

  arr = Array;
  ipOrDomainPattern =
    '(^([0-9]{1,3}\\.){3}[0-9]{1,3}$)|(^([a-zA-Z0-9]([a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?\\.)+[a-zA-Z]{2,6}[/0-9a-zA-z]*$)';
  controllerHAPattern = `${
    this.ipOrDomainPattern
  }|(^[a-z0-9]([-a-z0-9]*[a-z0-9])?$)`;

  platformVersionOptions = [
    {
      name: this.translateService.get('region_new_k8s'),
      value: 'v3',
    },
    {
      name: this.translateService.get('region_old_k8s'),
      value: 'v2',
    },
  ];
  featureOptions: { optional: boolean; value: string }[] = [];
  lbTypeOptions = [
    {
      name: this.translateService.get('load_balancer_haproxy'),
      value: 'haproxy',
    },
    {
      name: this.translateService.get('load_balancer_nginx'),
      value: 'nginx',
    },
  ];
  registryStorageSettingOptions = [
    {
      name: 'local',
      settings: ['LOCAL_HOSTDIR'],
    },
    {
      name: 'oss',
      settings: [
        'OSS_ACCESS_KEY',
        'OSS_SERECT_KEY',
        'OSS_REGION',
        'OSS_BUCKET',
        'OSS_ROOTDIR',
      ],
    },
    {
      name: 's3',
      settings: [
        'S3_ACCESS_KEY',
        'S3_SERECT_KEY',
        'S3_REGION',
        'S3_BUCKET',
        'S3_ROOTDIR',
      ],
    },
  ];
  registryStorageSettings: string[] = [];
  volumeDrivers = {
    glusterfs: false,
    ebs: false,
  };
  lbType = 'haproxy';

  model = {
    features: [] as string[],
    platform_version: 'v3',
    container_manager: ContainerManagers.Kubernetes,
    is_compute: false,
    template_type: 'poc',
    master: {},
    slave_username: '',
    slave_ssh_port: 22,
    slave: [] as string[],
    controller_haproxy: {
      iaas_lb: false,
      floating_ip: '',
      monitor_ip: '',
      nodes: [] as string[],
    },
    token: '',
    cni: {
      type: 'flannel',
      flannel: {
        type: 'vxlan',
        access_key: '',
        secret_access_key: '',
      },
      network_policy: 'empty',
    },
    cidr: '',
    over_commit: {
      cpu: 1,
      memory: 1,
    },
    logs: {
      storage: {},
    },
  };
  features: { [key: string]: boolean } = {};
  featureConfigs: { [key: string]: any } = {
    load_balancer: {
      iaas_lb: false,
      private_ip: '',
      public_ip: '',
      nodes: [],
    },
    registry: {
      node: '',
      registry_settings: {
        storage: this.registryStorageSettingOptions[0].name,
      },
    },
    volume: {
      volume_settings: {
        glusterfs: {
          replica: 1,
          nodes: {},
        },
        ebs: {
          zone: '',
          aws_access_key: '',
          aws_secret_key: '',
        },
      },
      storageclasses: [],
    },
  };

  scCreateChoices: ScCreateChoice[] = [
    {
      type: 'glusterfs',
      enabled: this.volumeDrivers.glusterfs,
      text: 'GlusterFS',
      active: false,
      default: false,
    },
    {
      type: 'ebs',
      enabled: this.volumeDrivers.ebs,
      text: 'EBS',
      active: false,
      default: false,
    },
  ];

  glusterfsNodeTopologies: Array<{
    node: string;
    drives: string[];
  }> = [];

  clusterName$ = this.activatedRoute.paramMap.pipe(
    pluck<ParamMap, string>('params', 'name'),
    tap(name => {
      this.store.dispatch(new regionActions.GetByName(name));
      this.store.dispatch(new GetNodesByRegion(name));
    }),
    publishReplay(1),
    refCount(),
  );

  nodes$ = combineLatest(
    this.store.select(selectNodeFeature),
    this.clusterName$,
  ).pipe(
    map(([state, cluster]) => state[cluster]),
    filter(data => !!data),
    map(state => state.ips.map(ip => state.entities[ip])),
    publishReplay(1),
    refCount(),
  );

  clusterDetail$ = combineLatest(
    this.store.select(fromRegion.selectEntities),
    this.clusterName$,
  ).pipe(
    map(([entities, name]: [Entities, string]) => entities[name]),
    filter(data => !!data),
    publishReplay(1),
    refCount(),
  );

  cloudName$ = this.clusterDetail$.pipe(
    pluck<Region, CloudNames>('attr', 'cloud', 'name'),
    distinctUntilChanged(),
    publishReplay(1),
    refCount(),
  );

  templateTypeOptions$ = this.nodes$.pipe(
    map(nodes => {
      const templates = [];
      if (nodes.length > 0) {
        templates.push('poc');
      }
      if (nodes.length > 2) {
        templates.push('prod');
      }
      return templates;
    }),
    map(templates =>
      templates.map(value => ({
        name: this.translateService.get('region_acmp_template_' + value),
        value,
      })),
    ),
    publishReplay(1),
    refCount(),
  );

  networkTypeOptions$ = this.cloudName$.pipe(
    map(name => [
      {
        name: 'Flannel',
        value: 'flannel',
      },
      {
        name: name === CloudNames.QCloud ? 'Ipvlan' : 'Macvlan',
        value: 'macvlan',
      },
      {
        name: 'Calico',
        value: 'calico',
      },
    ]),
    publishReplay(1),
    refCount(),
  );

  hostGwTypeOptions$ = this.cloudName$.pipe(
    map(name => {
      const options = [
        {
          name: 'Vxlan',
          value: 'vxlan',
        },
        {
          name: 'Host-gw',
          value: 'host-gw',
        },
      ];
      if (name === CloudNames.Ali) {
        options.push({
          name: 'Ali-VPC',
          value: 'ali-vpc',
        });
      } else if (name === CloudNames.Aws) {
        options.push({
          name: 'AWS-VPC',
          value: 'aws-vpc',
        });
      }
      return options;
    }),
    publishReplay(1),
    refCount(),
  );

  allIps$ = this.nodes$.pipe(
    map(nodes =>
      nodes.map(
        node =>
          'private_ip' in node
            ? node.private_ip
            : node.status.addresses.find(addr => addr.type === 'InternalIP')
                .address,
      ),
    ),
    publishReplay(1),
    refCount(),
  );
  slaveIps$ = new BehaviorSubject<string[]>([]);
  ipOptions$ = combineLatest(this.allIps$, this.slaveIps$).pipe(
    map(([allIps, slaveIps]) => allIps.filter(ip => !slaveIps.includes(ip))),
    publishReplay(1),
    refCount(),
  );
  masterIps$ = new BehaviorSubject<string[]>([]);
  slaveIpOptions$ = combineLatest(this.allIps$, this.masterIps$).pipe(
    map(([allIps, masterIps]) => allIps.filter(ip => !masterIps.includes(ip))),
    publishReplay(1),
    refCount(),
  );
  masterSlaveIps$ = combineLatest(this.masterIps$, this.slaveIps$).pipe(
    map(([masterIps, slaveIps]) => [...masterIps, ...slaveIps]),
    publishReplay(1),
    refCount(),
  );

  templateTypeChange$ = new ReplaySubject<string>(1);
  slaveIpsMaxLength$ = combineLatest(
    this.templateTypeChange$,
    this.allIps$,
  ).pipe(map(([template, ips]) => ips.length - (template === 'poc' ? 1 : 3)));

  masterMultiIpOptions$ = this.createMultiIpOptions(this.slaveIps$);
  slaveMultiIpOptions$ = this.createMultiIpOptions(this.masterIps$);

  networkPolicyOptions$ = new BehaviorSubject<
    { name: string; value: string }[]
  >([]);

  cidrPlaceholder$ = this.allIps$.pipe(
    map(
      ips =>
        ips[0] && ips[0].startsWith('10.') ? '192.168.0.0/16' : '10.1.0.0/16',
    ),
    publishReplay(1),
    refCount(),
  );

  submitting = false;

  constructor(
    private translateService: TranslateService,
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(ENVIRONMENTS) private environments: Environments,
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private modalService: ModalService,
    private auiNotificationService: NotificationService,
    private regionService: RegionService,
    private router: Router,
    private accountService: AccountService,
  ) {
    this.accountService.getUserToken().then(token => {
      this.model.token = token;
    });
  }

  private createMultiIpOptions(filterIps$: Observable<string[]>) {
    return this.allIps$.pipe(
      switchMap(allIps =>
        filterIps$.pipe(
          scan<
            string[],
            Array<{
              value: string;
              $$rcItemDisabled: boolean;
            }>
          >(
            (source, filterIps) => {
              source.forEach(item => {
                item.$$rcItemDisabled = filterIps.includes(item.value);
              });
              return source;
            },
            allIps.map(ip => ({
              value: ip,
              $$rcItemDisabled: false,
            })),
          ),
        ),
      ),
    );
  }

  onTemplateTypeChanged(template: string) {
    this.templateTypeChange$.next(template);
    this.resetFeatureOptions(template);

    this.model.master = template === 'poc' ? {} : [];
    this.model.slave = [];
    this.masterIps$.next([]);
    this.slaveIps$.next([]);

    this.featureConfigs.load_balancer = {
      type: this.featureConfigs.load_balancer.type,
    };
  }

  onRegistryStorageChanged({ settings }: { settings: string[] }) {
    const { name, storage } = this.featureConfigs.registry.registry_settings;
    this.featureConfigs.registry.registry_settings = { name, storage };
    this.registryStorageSettings = settings;
  }

  onLbTypeChanged() {
    this.featureConfigs.load_balancer = {};
  }

  onNetworkModeChanged(value: string) {
    const options =
      value === 'calico'
        ? []
        : [
            {
              name: this.translateService.get('none'),
              value: 'empty',
            },
          ];

    if (['flannel', 'calico'].includes(value)) {
      options.push({
        name: 'Calico',
        value: 'calico',
      });
    }

    const { cni } = this.model;

    if (!options.find(({ value }) => value === cni.network_policy)) {
      cni.network_policy = options[0].value;
    }

    this.networkPolicyOptions$.next(options);
  }

  private resetFeatureOptions(template: string) {
    const templateConfig = CLUSTER_DEPLOY_TEMPLATES[template];
    const requiredFeatures = templateConfig.features.map(value => ({
      optional: false,
      value,
    }));
    const optionalFeatures = templateConfig.optionalFeatures.map(value => ({
      optional: true,
      value,
    }));
    this.featureOptions = [...requiredFeatures, ...optionalFeatures];
    this.features = {};
    this.featureOptions.forEach(feature => {
      this.features[feature.value] =
        feature.value !== 'tunnel' ||
        !this.environments.is_private_deploy_enabled;
    });
  }

  glusterfsSelect(checked: boolean) {
    const choice = this.scCreateChoices[0];
    choice.enabled = checked;
    if (!choice.enabled) {
      choice.active = false;
      choice.default = false;
    }
  }

  scCreateSelect(choice: ScCreateChoice) {
    if (!choice.enabled) {
      return;
    }
    // Reset default
    choice.default = false;
    choice.active = !choice.active;
  }

  scSetDefault(choice: ScCreateChoice) {
    if (!choice.default) {
      this.scCreateChoices.forEach(item => (item.default = false));
    }
    choice.default = !choice.default;
  }

  addNode(selectedNode: string, inputDrives: string[]) {
    const existNode = this.glusterfsNodeTopologies.find(
      topo => topo.node === selectedNode,
    );
    if (existNode) {
      inputDrives.forEach((drive: string) => {
        if (!existNode.drives.includes(drive)) {
          existNode.drives.push(drive);
        }
      });
    } else {
      this.glusterfsNodeTopologies.push({
        node: selectedNode,
        drives: inputDrives.slice(0),
      });
    }
    this.nodeDrive.setTags([]);
  }

  removeNode(index: number) {
    this.glusterfsNodeTopologies.splice(index, 1);
  }

  deploy() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }

    const payload = JSON.parse(JSON.stringify(this.model));

    const allFeatures = Object.assign({}, this.features);
    allFeatures.alb = true;

    if (allFeatures.load_balancer) {
      allFeatures[this.lbType] = true;
      payload[this.lbType] = this.featureConfigs.load_balancer;
      allFeatures.load_balancer = false;
    }

    if (allFeatures.volume) {
      const storageClasses = this.scCreateChoices
        .filter(choice => choice.active)
        .map(choice => ({
          name: choice.type,
          default: choice.default,
        }));
      payload.volume = { volume_settings: {}, storageclasses: storageClasses };
      if (this.volumeDrivers.glusterfs) {
        const nodes = {};
        payload.volume.volume_settings.glusterfs = this.featureConfigs.volume.volume_settings.glusterfs;
        this.glusterfsNodeTopologies.forEach(topo => {
          nodes[topo.node] = topo.drives;
        });
        payload.volume.volume_settings.glusterfs.nodes = nodes;
      }
      if (this.volumeDrivers.ebs) {
        payload.volume.volume_settings.ebs = this.featureConfigs.volume.volume_settings.ebs;
      }
    }

    if (allFeatures.registry) {
      payload.registry = this.featureConfigs.registry;
    }

    Object.keys(allFeatures).forEach(key => {
      if (allFeatures[key]) {
        payload.features.push(key);
      }
    });
    payload.features.push(payload.cni.type);

    if (this.weblabs.K8S_VERSION_110_ENABLED) {
      payload.version = '1.10';
    } else {
      payload.version = '1.9';
    }

    if (payload.template_type !== 'prod') {
      delete payload.controller_haproxy;
    }

    if (payload.cni.type === 'macvlan') {
      delete payload.cidr;
      delete payload.cni.flannel;
    }

    if (payload.cni.flannel && payload.cni.flannel.type !== 'ali-vpc') {
      delete payload.cni.flannel.access_key;
      delete payload.cni.flannel.secret_access_key;
    }

    const isNewK8s = payload.platform_version === 'v3';

    if (isNewK8s) {
      delete payload.over_commit;
    } else if (!this.weblabs.NETWORK_POLICY_ENABLED) {
      delete payload.cni.network_policy;
    }

    if (payload.cni.network_policy === 'empty') {
      payload.cni.network_policy = '';
    }

    if (this.logSource) {
      payload.logs.storage = this.logSource.value;
    }

    payload.template_type = 'kubernetes_' + payload.template_type;

    this.clusterDetail$.pipe(first()).subscribe(({ name, display_name }) => {
      this.modalService
        .confirm({
          title: this.translateService.get('region_deploy_acmp'),
          content: this.translateService.get(
            'region_deploy_acmp_confirm_hint',
            { name: display_name },
          ),
        })
        .then(() => {
          this.regionService
            .updateRegion(name, payload)
            .then(() => {
              this.submitting = false;
              this.router.navigate(['/cluster/detail', name]);
            })
            .catch(err => {
              this.submitting = false;
              this.auiNotificationService.error(
                err.errors.map((err: any) => err.message).join(';'),
              );
            });
        })
        .catch(() => {});
    });
  }
}
