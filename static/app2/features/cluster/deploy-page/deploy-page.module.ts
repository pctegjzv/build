import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LogSourceFormModule } from 'app2/features/cluster/common/log-source-form/log-source-form.module';
import { DeployPageComponent } from 'app2/features/cluster/deploy-page/deploy-page.component';
import { SharedModule } from 'app2/shared/shared.module';

@NgModule({
  imports: [SharedModule, RouterModule, LogSourceFormModule],
  declarations: [DeployPageComponent],
  exports: [],
})
export class DeployPageModule {}
