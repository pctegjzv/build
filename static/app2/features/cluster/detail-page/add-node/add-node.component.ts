import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import clipboard from 'clipboard-polyfill';
import { get } from 'lodash';

import { NotificationService } from 'alauda-ui';
import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';
import { BaseDialog } from 'app2/features/cluster/detail-page/base-dialog/base-dialog';
import { CloudNames } from 'app2/features/cluster/utils';
import {
  ContainerManagers,
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';

interface DropdownOption {
  name: string;
  value: string;
}

@Component({
  selector: 'rc-add-node',
  templateUrl: 'add-node.component.html',
  styleUrls: ['add-node.component.scss', '../base-dialog/dialog.scss'],
})
export class AddNodeComponent extends BaseDialog implements OnInit {
  static nodeScriptCache: { [key: string]: string } = {};

  @ViewChild('autoCreateForm')
  autoCreateForm: NgForm;

  cluster: Region;

  nodeTypeOptions: DropdownOption[];
  nodeType: string;
  createMethodOptions: DropdownOption[];
  createMethod: string;
  nodeScript = '';
  instanceTypeOptions: DropdownOption[] = [];
  sshKeyPairOptions: DropdownOption[] = [];
  createModel: any = {};

  constructor(
    @Inject(MODAL_DATA) modalData: { cluster: Region },
    private translateService: TranslateService,
    private regionService: RegionService,
    @Inject(WEBLABS) public weblabs: Weblabs,
    private auiNotificationService: NotificationService,
  ) {
    super();
    this.cluster = modalData.cluster;
  }

  ngOnInit() {
    const nodeTypes = ['empty'];
    if (this.cluster.container_manager !== ContainerManagers.None) {
      nodeTypes.push('slave');
    }
    this.nodeTypeOptions = nodeTypes.map(value => {
      return {
        name: this.translateService.get('region_node_type_' + value),
        value,
      };
    });
    this.nodeType = nodeTypes[0];

    const createMethods = ['manual'];
    if (get(this.cluster, 'features.node.features', []).includes('instance')) {
      createMethods.push('auto');
    }
    this.createMethodOptions = createMethods.map(value => {
      return {
        name: this.translateService.get('region_node_create_method_' + value),
        value,
      };
    });
    this.createMethod = createMethods[0];
  }

  onCreateMethodChanged(method: string) {
    if (
      method === 'auto' &&
      this.cluster.attr.cloud.name !== CloudNames.VMware
    ) {
      this.loadAutoCreateOptions();
    }
  }

  async loadAutoCreateOptions() {
    try {
      const [config, keys] = await Promise.all([
        this.regionService.getRegionConfig(this.cluster.name),
        this.regionService.getRegionSSHKeyPairs(this.cluster.name),
      ]);

      const cloudName = get(this.cluster, 'attr.cloud.name');

      if (
        cloudName === CloudNames.OpenStack ||
        cloudName === CloudNames.KingSoft
      ) {
        this.instanceTypeOptions = Object.keys(config).map(key => ({
          name: key,
          value: config[key],
        }));
        this.createModel.instance_type = this.instanceTypeOptions[0].value;
      } else {
        this.instanceTypeOptions = config.instance_types.map(value => {
          return {
            name: value,
            value: value,
          };
        });
        this.createModel.instance_type = config.default_scale_type;
      }

      this.sshKeyPairOptions = keys.key_pairs.map(value => {
        return {
          name: value,
          value: value,
        };
      });
      this.createModel.ssh_key_name = keys.key_pairs[0];
    } catch (err) {
      this.auiNotificationService.error(
        this.translateService.get('region_create_options_load_error'),
      );
    }
  }

  loadNodeScript() {
    const nodeType = this.getApiNodeType();
    const cacheKey = this.cluster.id + this.nodeType;
    if (AddNodeComponent.nodeScriptCache[cacheKey]) {
      this.nodeScript = AddNodeComponent.nodeScriptCache[cacheKey];
    } else {
      this.nodeScript = '';
      this.regionService
        .getRegionNodeScripts(this.cluster.name, nodeType)
        .then(res => {
          this.nodeScript = res.commands.install;
          AddNodeComponent.nodeScriptCache[cacheKey] = this.nodeScript;
        });
    }
  }

  getApiNodeType() {
    let nodeType = 'empty';
    if (
      this.nodeType === 'slave' &&
      this.cluster.container_manager === ContainerManagers.Kubernetes
    ) {
      nodeType = 'k8s-slave';
    }
    return nodeType;
  }

  async createNode() {
    this.autoCreateForm.onSubmit(null);
    if (this.autoCreateForm.invalid) {
      return;
    }
    this.submitting = true;
    const params = Object.assign(
      { node_type: this.getApiNodeType() },
      this.createModel,
    );
    try {
      await this.regionService.createNode(this.cluster.name, params);
      this.close.next(true);
    } catch (err) {
      this.auiNotificationService.error(
        err.errors.map((err: any) => err.message).join(';'),
      );
    } finally {
      this.submitting = false;
    }
  }

  copyScript() {
    clipboard
      .writeText(this.nodeScript)
      .then(() => {
        this.auiNotificationService.success(
          this.translateService.get('copy_clipboard_success'),
        );
        this.close.next();
      })
      .catch(() => {
        this.auiNotificationService.error(
          this.translateService.get('copy_clipboard_fail'),
        );
      });
  }
}
