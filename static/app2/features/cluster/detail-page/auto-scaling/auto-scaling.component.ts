import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { BaseDialog } from 'app2/features/cluster/detail-page/base-dialog/base-dialog';
import { CloudNames } from 'app2/features/cluster/utils';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';

@Component({
  selector: 'rc-auto-scaling',
  templateUrl: 'auto-scaling.component.html',
  styleUrls: ['auto-scaling.component.scss', '../base-dialog/dialog.scss'],
})
export class AutoScalingComponent extends BaseDialog implements OnInit {
  @ViewChild('form')
  form: NgForm;

  cluster: Region;

  data = {
    enable_scale: false,
  };
  scale_up = {
    max_cpus_utilization: 0.95,
    max_mem_utilization: 0.9,
    increase_delta: 1,
    instance_type: 't2.micro',
    max_scale_num: 10,
    ssh_key_name: '',
  };
  scale_down = {
    min_cpus_utilization: 0.1,
    min_mem_utilization: 0.2,
    decrease_delta: 1,
    min_scale_num: 1,
    forbid_delete_occupied_node: true,
  };

  sshKeyPairOptions: { display: string; value: string }[] = [];
  instanceTypeOptions: { display: string; value: string }[] = [];

  constructor(
    @Inject(MODAL_DATA) modalData: { cluster: Region },
    private regionService: RegionService,
  ) {
    super();
    this.cluster = modalData.cluster;
  }

  ngOnInit() {
    const cloudName = this.cluster.attr.cloud.name;
    if (cloudName !== CloudNames.VMware) {
      this.regionService
        .getRegionSSHKeyPairs(this.cluster.name)
        .then(res => {
          this.scale_up.ssh_key_name = res.key_pairs[0];
          this.sshKeyPairOptions = res.key_pairs.map(value => ({
            display: value,
            value,
          }));
        })
        .catch(() => {});
      this.regionService
        .getRegionConfig(this.cluster.name)
        .then(res => {
          if (
            cloudName === CloudNames.OpenStack ||
            cloudName === CloudNames.KingSoft
          ) {
            this.instanceTypeOptions = Object.keys(res).map(key => ({
              display: key,
              value: res[key],
            }));
            this.scale_up.instance_type = this.instanceTypeOptions[0].value;
          } else {
            this.scale_up.instance_type = res.default_scale_type;
            this.instanceTypeOptions = res.instance_types.map(value => ({
              display: value,
              value,
            }));
          }
        })
        .catch(() => {});
    }

    if (this.cluster.attr.node_scaling) {
      const init = this.cluster.attr.node_scaling;
      this.data.enable_scale = init.enable_scale;
      this.scale_up = Object.assign({}, this.scale_up, init.scale_up);
      this.scale_down = Object.assign({}, this.scale_down, init.scale_down);
    }

    this.scale_up.max_cpus_utilization =
      this.scale_up.max_cpus_utilization * 100;
    this.scale_up.max_mem_utilization = this.scale_up.max_mem_utilization * 100;

    this.scale_down.min_cpus_utilization =
      this.scale_down.min_cpus_utilization * 100;
    this.scale_down.min_mem_utilization =
      this.scale_down.min_mem_utilization * 100;
  }

  async save() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;

    const scale_up = Object.assign({}, this.scale_up);
    scale_up.max_cpus_utilization =
      Math.round(scale_up.max_cpus_utilization) / 100;
    scale_up.max_mem_utilization =
      Math.round(scale_up.max_mem_utilization) / 100;

    const scale_down = Object.assign({}, this.scale_down);
    scale_down.min_cpus_utilization =
      Math.round(scale_down.min_cpus_utilization) / 100;
    scale_down.min_mem_utilization =
      Math.round(scale_down.min_mem_utilization) / 100;

    const data = Object.assign({}, this.data, { scale_up, scale_down });

    try {
      await this.regionService.updateNodeScaling(this.cluster.name, data);
      this.close.next(true);
    } catch (err) {
    } finally {
      this.submitting = false;
    }
  }
}
