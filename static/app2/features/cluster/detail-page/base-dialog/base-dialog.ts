import { EventEmitter } from '@angular/core';

export class BaseDialog {
  close = new EventEmitter();

  submitting = false;

  cancel() {
    this.close.next();
  }
}
