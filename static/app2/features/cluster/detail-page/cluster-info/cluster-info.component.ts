import {
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { get } from 'lodash';
import { Observable, ReplaySubject, combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  publishReplay,
  refCount,
  tap,
} from 'rxjs/operators';

import { observablePluck } from 'app2/core/utils';
import { getCloudInfos } from 'app2/features/cluster/utils';
import { DEFAULT_REGION_LIST_FILTER } from 'app2/shared/directives/region-badge-option/region-badge-option.service';
import {
  ContainerManagers,
  Region,
  RegionComponent,
  RegionStats,
} from 'app2/shared/services/features/region.service';
import { AppState } from 'app2/state-store/reducers';
import * as componentActions from 'app2/state-store/region_component/actions';
import * as fromComponent from 'app2/state-store/region_component/reducers';
import * as statsActions from 'app2/state-store/region_stats/actions';
import * as fromStats from 'app2/state-store/region_stats/reducers';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-cluster-info',
  templateUrl: 'cluster-info.component.html',
  styleUrls: ['cluster-info.component.scss'],
})
export class ClusterInfoComponent implements OnChanges, OnInit, OnDestroy {
  @Input()
  cluster: Region;
  @Input()
  polling = true;
  @Input()
  pollingInterval = 30 * 1000;

  publicCloudInfos: { label: string; value: string | number }[] = [];

  get isDeployed() {
    return (
      this.cluster && this.cluster.container_manager !== ContainerManagers.None
    );
  }

  get manager() {
    return (
      this.cluster &&
      (this.cluster.features.kubernetes ||
        this.cluster.features.swarm ||
        this.cluster.features.marathon)
    );
  }

  get volumes() {
    return get(this.cluster, 'features.volume.features', [])
      .map((item: string) =>
        this.translateService.get('region_acmp_volume_type_' + item),
      )
      .join(',');
  }

  safeGet = get;

  clusterName$ = new ReplaySubject<string>(1);
  components$: Observable<RegionComponent[]>;
  stats$: Observable<RegionStats>;
  podsCount$: Observable<number>;

  intervalId: number;

  couldLink2Services = DEFAULT_REGION_LIST_FILTER;

  pluck = observablePluck;

  constructor(
    private store: Store<AppState>,
    private translateService: TranslateService,
  ) {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.cluster && changes.cluster.currentValue) {
      this.publicCloudInfos = getCloudInfos(changes.cluster.currentValue);
      this.clusterName$.next(changes.cluster.currentValue.name);
    }
  }

  ngOnInit() {
    if (this.polling) {
      this.intervalId = window.setInterval(() => {
        this.store.dispatch(
          new componentActions.GetByRegion(this.cluster.name),
        );
        this.store.dispatch(new statsActions.GetByRegion(this.cluster.name));
      }, this.pollingInterval);
    }

    this.components$ = combineLatest(
      this.store.select(fromComponent.selectFeature),
      this.clusterName$.pipe(
        distinctUntilChanged(),
        tap(cluster => {
          this.store.dispatch(new componentActions.GetByRegion(cluster));
        }),
      ),
    ).pipe(
      map(([state, cluster]) => state[cluster]),
      filter(data => !!data),
      publishReplay(1),
      refCount(),
    );

    this.stats$ = combineLatest(
      this.store.select(fromStats.selectFeature),
      this.clusterName$.pipe(
        distinctUntilChanged(),
        tap(cluster => {
          this.store.dispatch(new statsActions.GetByRegion(cluster));
        }),
      ),
    ).pipe(
      map(([state, cluster]) => state[cluster]),
      filter(data => !!data),
      publishReplay(1),
      refCount(),
    );

    this.podsCount$ = this.stats$.pipe(
      map(
        stats =>
          stats.pods_count !== undefined
            ? stats.pods_count
            : stats.containers_count,
      ),
      publishReplay(1),
      refCount(),
    );
  }

  ngOnDestroy() {
    clearInterval(this.intervalId);
  }
}
