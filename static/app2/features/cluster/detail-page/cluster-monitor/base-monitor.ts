import { Input } from '@angular/core';
import * as d3 from 'd3';
import { ReplaySubject } from 'rxjs';

import { COLOR_OK } from 'app2/features/dashboard/charts/colors';
import { AreaChartConfig } from 'app2/features/dashboard/charts/ngx-charts.types';
import { RegionService } from 'app2/shared/services/features/region.service';
import { TranslateService } from 'app2/translate/translate.service';

export class BaseMonitor {
  @Input()
  set clusterName(value: string) {
    this.clusterName$.next(value);
  }

  timeHorizon = 60 * 60;
  timeHorizonOptions = [
    {
      display: this.translateService.get('chart_one_hour'),
      value: 60 * 60,
      period: 60,
    },
    {
      display: this.translateService.get('chart_three_hour'),
      value: 60 * 60 * 3,
      period: 5 * 60,
    },
    {
      display: this.translateService.get('chart_six_hour'),
      value: 60 * 60 * 6,
      period: 15 * 60,
    },
    {
      display: this.translateService.get('chart_twelve_hour'),
      value: 60 * 60 * 12,
      period: 30 * 60,
    },
    {
      display: this.translateService.get('chart_one_day'),
      value: 60 * 60 * 24,
      period: 3600,
    },
    {
      display: this.translateService.get('chart_three_day'),
      value: 60 * 60 * 24 * 3,
      period: 3600 * 4,
    },
    {
      display: this.translateService.get('chart_one_week'),
      value: 60 * 60 * 24 * 7,
      period: 3600 * 12,
    },
    {
      display: this.translateService.get('chart_two_week'),
      value: 60 * 60 * 24 * 14,
      period: 3600 * 12,
    },
  ];

  chartConfig: AreaChartConfig = {
    scheme: { domain: [COLOR_OK] },
    xAxis: true,
    yAxis: true,
    showGridLines: true,
    gradient: false,
    autoScale: false,
    curve: d3.curveMonotoneX,
  };

  clusterName$ = new ReplaySubject<string>(1);
  update$ = new ReplaySubject<number>(1);

  constructor(
    private translateService: TranslateService,
    protected regionService: RegionService,
  ) {}

  percentTickFormatting(val: number): string {
    return val * 100 + '%';
  }

  convertData(name: string, res: any) {
    if (!res.data.length) {
      return null;
    }
    return [
      {
        name: this.translateService.get(name),
        series: res.data.map((item: any) => {
          return {
            name: new Date(item.timestamp),
            value: item.value || 0,
          };
        }),
      },
    ];
  }
}
