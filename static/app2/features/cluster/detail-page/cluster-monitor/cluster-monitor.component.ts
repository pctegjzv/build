import { Component } from '@angular/core';
import { EMPTY, combineLatest, from } from 'rxjs';
import {
  catchError,
  map,
  pluck,
  publishReplay,
  refCount,
  switchMap,
} from 'rxjs/operators';

import { BaseMonitor } from 'app2/features/cluster/detail-page/cluster-monitor/base-monitor';
import { RegionService } from 'app2/shared/services/features/region.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-cluster-monitor',
  templateUrl: 'cluster-monitor.component.html',
  styleUrls: ['cluster-monitor.component.scss'],
})
export class ClusterMonitorComponent extends BaseMonitor {
  statsHistory$ = combineLatest(this.clusterName$, this.update$).pipe(
    switchMap(([cluster, horizon]) => {
      const end_time = new Date().getTime();
      const start_time = end_time - horizon * 1000;
      const option = this.timeHorizonOptions.find(
        item => item.value === horizon,
      );

      if (!option) {
        return EMPTY;
      }

      return from(
        this.regionService.getRegionStatsHistory(cluster, {
          end_time,
          start_time,
          period: option.period,
        }),
      ).pipe(catchError(() => EMPTY));
    }),
    publishReplay(1),
    refCount(),
  );

  cpuPercent$ = this.statsHistory$.pipe(
    pluck('region-cpus-percent'),
    map((res: any) => {
      return this.convertData('region-cpus-percent', res);
    }),
    publishReplay(1),
    refCount(),
  );
  memPercent$ = this.statsHistory$.pipe(
    pluck('region-mem-percent'),
    map((res: any) => {
      return this.convertData('region-mem-percent', res);
    }),
    publishReplay(1),
    refCount(),
  );
  nodeCount$ = this.statsHistory$.pipe(
    pluck('region-nodes-count'),
    map((res: any) => {
      return this.convertData('region-nodes-count', res);
    }),
    publishReplay(1),
    refCount(),
  );
  serviceCount$ = this.statsHistory$.pipe(
    pluck('region-services-count'),
    map((res: any) => {
      return this.convertData('region-services-count', res);
    }),
    publishReplay(1),
    refCount(),
  );

  constructor(
    translateService: TranslateService,
    regionService: RegionService,
  ) {
    super(translateService, regionService);
  }
}
