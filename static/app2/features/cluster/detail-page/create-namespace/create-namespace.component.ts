import { Component, Inject, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { NotificationService } from 'alauda-ui';
import { BaseDialog } from 'app2/features/cluster/detail-page/base-dialog/base-dialog';
import { NamespaceService } from 'app2/shared/services/features/namespace.service';
import { Region } from 'app2/shared/services/features/region.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';

@Component({
  selector: 'rc-create-namespace',
  templateUrl: 'create-namespace.component.html',
  styleUrls: ['create-namespace.component.scss', '../base-dialog/dialog.scss'],
})
export class CreateNamespaceComponent extends BaseDialog {
  @ViewChild('form')
  form: NgForm;

  cluster: Region;
  name: string;

  constructor(
    @Inject(MODAL_DATA) modalData: { cluster: Region },
    private namespaceService: NamespaceService,
    private auiNotificationService: NotificationService,
  ) {
    super();
    this.cluster = modalData.cluster;
  }

  async create() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;
    try {
      await this.namespaceService.createNamespace(this.cluster.id, this.name);
      this.close.next(true);
    } catch (err) {
      this.auiNotificationService.error(
        err.errors.map((err: any) => err.message).join(';'),
      );
    } finally {
      this.submitting = false;
    }
  }
}
