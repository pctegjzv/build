import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NotificationService } from 'alauda-ui';
import { get } from 'lodash';

import {
  ContainerManagers,
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

import { BaseDialog } from '../base-dialog/base-dialog';

const NODE_TYPE_MAP = {
  MESOS: 'mesos-slave',
  SWARM: 'swarm_worker',
  KUBERNETES: 'k8s-slave',
};

const REGION_CLOUD_NAMES = {
  PRIVATE: 'PRIVATE',
  AWS: 'AWS',
  ALI: 'ALI',
  QING: 'QING',
  UCLOUD: 'UCLOUD',
  QCLOUD: 'QCLOUD',
  OPENSTACK: 'OPENSTACK',
  BAIDU: 'BAIDU',
};

const CONTAINER_MANAGER_ARR = Object.values(ContainerManagers);

@Component({
  templateUrl: './create-node.component.html',
  styleUrls: ['./create-node.component.scss'],
})
export class CreateNodeComponent extends BaseDialog implements OnInit {
  @ViewChild(NgForm)
  form: NgForm;

  cluster: Region;
  loading: boolean;
  step = 0;
  onlyStep1: boolean;
  createOptionsLoadError: boolean;
  autoCreateOptionsReady: boolean;
  nodeInstallScripts: string;
  addNodeAttribute: boolean;
  count: string;

  nodeType = 'empty';
  nodeTypeOptions: Array<{
    displayName: string;
    type: string;
  }> = [];

  createMethod = 'manual';
  createMethodOptions: Array<{
    displayName: string;
    type: string;
  }> = [];

  instanceType: string;
  instanceTypes: Array<{
    key: string;
    value: string;
  }> = [];

  sshKeyName: string;
  sshKeyPairs: string[] = [];

  constructor(
    private auiNotificationService: NotificationService,
    private errorsToast: ErrorsToastService,
    private translate: TranslateService,
    private region: RegionService,
    @Inject(MODAL_DATA) data: { cluster: Region },
  ) {
    super();
    this.cluster = { ...data.cluster };
  }

  ngOnInit() {
    const nodeTypes = ['empty'];

    if (this.cluster.container_manager !== ContainerManagers.None) {
      nodeTypes.push('slave');
    }

    this.nodeTypeOptions = nodeTypes.map(option => ({
      displayName: this.translate.get('region_node_type_' + option),
      type: option,
    }));

    const createMethods = ['manual'];

    if (get(this.cluster, 'features.node.features', []).includes('instance')) {
      createMethods.push('auto');
    }

    this.createMethodOptions = createMethods.map(method => ({
      displayName: this.translate.get('region_node_create_method_' + method),
      type: method,
    }));

    if (nodeTypes.length === 1 && createMethods.length === 1) {
      this.onlyStep1 = true;
      this.onNodeTypeChanged(nodeTypes[0]);
      this.createMethod = createMethods[0];
      this.toNextStep();
    }
  }

  onNodeTypeChanged(nodeType: string) {
    if (nodeType !== 'slave') {
      this.addNodeAttribute = false;
    }
  }

  canAddTag() {
    const { container_manager } = this.cluster;
    return (
      CONTAINER_MANAGER_ARR.includes(container_manager) &&
      ContainerManagers.Kubernetes !== container_manager
    );
  }

  private async getNodeCreateOptions() {
    this.autoCreateOptionsReady = false;

    const [nodes, keys] = await Promise.all([
      this.region.getRegionConfig(this.cluster.name),
      this.region.getRegionSSHKeyPairs(this.cluster.name),
    ]);
    if (get(this.cluster, 'attr.cloud.name') === REGION_CLOUD_NAMES.OPENSTACK) {
      Object.keys(nodes || {}).forEach(r => {
        this.instanceTypes.push({
          key: r,
          value: nodes[r],
        });
      });
    } else {
      this.instanceTypes = (nodes.instanceTypes || []).map(r => ({
        key: r,
        value: r,
      }));
    }

    this.sshKeyPairs = keys.key_pairs || [];
    this.autoCreateOptionsReady = true;
  }

  private getApiNodeType() {
    let nodeType = 'empty';
    if (this.nodeType === 'slave') {
      if (this.cluster.container_manager) {
        nodeType = NODE_TYPE_MAP[this.cluster.container_manager] || nodeType;
      }
    }

    if (this.addNodeAttribute) {
      nodeType += '-attribute';
    }

    return nodeType;
  }

  async toNextStep() {
    this.step = 1;

    this.loading = true;
    this.createOptionsLoadError = false;

    try {
      switch (this.createMethod) {
        case 'auto': {
          await this.getNodeCreateOptions();
          break;
        }
        case 'manual': {
          // There are three possible values of node type:
          // ['mesos-slave', 'mesos-slave-attribute', 'swarm_worker', 'empty']
          const nodeType = this.getApiNodeType();

          const data = await this.region.nodeScripts({
            regionName: this.cluster.name,
            nodeType,
          });
          this.nodeInstallScripts = data.commands.install;
          break;
        }
      }
    } catch (e) {
      this.createOptionsLoadError = true;
    }

    this.loading = false;
  }

  async create() {
    this.form.ngSubmit.emit();
    (this.form as any).submitted = true;
    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    try {
      await this.region.nodeCreate({
        region_name: this.cluster.name,
        num: this.count,
        instance_type: this.instanceType,
        ssh_key_name: this.sshKeyName,
        node_type: this.getApiNodeType(),
      });
      this.close.emit(true);
    } catch (e) {
      return this.errorsToast.error(e);
    } finally {
      this.loading = false;
    }

    this.auiNotificationService.success(
      this.translate.get('create_node_success'),
    );
  }
}
