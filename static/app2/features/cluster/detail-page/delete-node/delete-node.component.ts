import { Component, Inject } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import clipboard from 'clipboard-polyfill';
import { first } from 'rxjs/operators';

import { BaseDialog } from 'app2/features/cluster/detail-page/base-dialog/base-dialog';
import { RegionService } from 'app2/shared/services/features/region.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { DELETE_BY_IP_SUCC, DeleteByIp } from 'app2/state-store/node/actions';
import { AppState } from 'app2/state-store/reducers';

@Component({
  selector: 'rc-delete-node',
  templateUrl: './delete-node.component.html',
  styleUrls: ['../base-dialog/dialog.scss', './delete-node.component.scss'],
})
export class DeleteNodeComponent extends BaseDialog {
  uninstallScript: string;
  scriptCopied = false;

  constructor(
    @Inject(MODAL_DATA)
    public modalData: { regionName: string; privateIp: string },
    private regionService: RegionService,
    private store: Store<AppState>,
    private actions: Actions,
  ) {
    super();

    this.regionService
      .getRegionNodeScripts(this.modalData.regionName)
      .then(res => {
        this.uninstallScript = res.commands.uninstall;
      });
  }

  copyScript() {
    if (this.uninstallScript) {
      clipboard.writeText(this.uninstallScript).then(() => {
        this.scriptCopied = true;
      });
    }
  }

  delete() {
    this.submitting = true;
    this.store.dispatch(new DeleteByIp(this.modalData));
    this.actions
      .ofType(DELETE_BY_IP_SUCC)
      .pipe(first())
      .subscribe(() => {
        this.submitting = false;
        this.close.next(true);
      });
  }
}
