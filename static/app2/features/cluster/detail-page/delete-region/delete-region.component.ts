import { Component, Inject } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import clipboard from 'clipboard-polyfill';
import { filter, first, pluck } from 'rxjs/operators';

import { BaseDialog } from 'app2/features/cluster/detail-page/base-dialog/base-dialog';
import { RegionService } from 'app2/shared/services/features/region.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { AppState } from 'app2/state-store/reducers';
import {
  DELETE_BY_NAME_SUCC,
  DeleteByName,
} from 'app2/state-store/region/actions';

@Component({
  selector: 'rc-delete-region',
  templateUrl: './delete-region.component.html',
  styleUrls: ['../base-dialog/dialog.scss', './delete-region.component.scss'],
})
export class DeleteRegionComponent extends BaseDialog {
  clusterName: string;
  uninstallScript: string;
  scriptCopied = false;

  constructor(
    @Inject(MODAL_DATA) modalData: { clusterName: string },
    private regionService: RegionService,
    private store: Store<AppState>,
    private actions: Actions,
  ) {
    super();
    this.clusterName = modalData.clusterName;

    this.regionService.getRegionNodeScripts(this.clusterName).then(res => {
      this.uninstallScript = res.commands.uninstall;
    });
  }

  copyScript() {
    if (this.uninstallScript) {
      clipboard.writeText(this.uninstallScript).then(() => {
        this.scriptCopied = true;
      });
    }
  }

  delete() {
    this.submitting = true;
    this.store.dispatch(new DeleteByName(this.clusterName));
    this.actions
      .ofType(DELETE_BY_NAME_SUCC)
      .pipe(pluck('payload'))
      .pipe(filter((name: string) => name === this.clusterName))
      .pipe(first())
      .subscribe(() => {
        this.submitting = false;
        this.close.next(true);
      });
  }
}
