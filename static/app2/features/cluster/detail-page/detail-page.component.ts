import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
  Type,
} from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { NotificationService } from 'alauda-ui';
import { get } from 'lodash';
import { Observable, combineLatest, from } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  first,
  map,
  mergeMap,
  pluck,
  publishReplay,
  refCount,
  startWith,
  tap,
} from 'rxjs/operators';

import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';
import { observablePluck } from 'app2/core/utils';
import { CloudNames } from 'app2/features/cluster/utils';
import {
  ContainerManagers,
  Node,
  NodeState,
  Region,
  RegionService,
  RegionStatus,
} from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import * as logSourceActions from 'app2/state-store/log_source/actions';
import * as nodeActions from 'app2/state-store/node/actions';
import * as fromNodes from 'app2/state-store/node/reducers';
import { AppState } from 'app2/state-store/reducers';
import * as regionActions from 'app2/state-store/region/actions';
import * as fromRegion from 'app2/state-store/region/reducers';
import { Entities } from 'app2/state-store/region/reducers';
import { TranslateService } from 'app2/translate/translate.service';

import { AddNodeComponent } from './add-node/add-node.component';
import { AutoScalingComponent } from './auto-scaling/auto-scaling.component';
import { CreateNodeComponent } from './create-node/create-node.component';
import { DeleteRegionComponent } from './delete-region/delete-region.component';
import { UpdateLogSourceComponent } from './update-log-source/update-log-source.component';

@Component({
  templateUrl: 'detail-page.component.html',
  styleUrls: ['detail-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DetailPageComponent implements OnInit {
  clusterName$ = this.route.paramMap.pipe(
    pluck<ParamMap, string>('params', 'name'),
    tap(name => {
      this.regionService.setRegionByName(name);
      this.store.dispatch(new regionActions.GetByName(name));
    }),
    publishReplay(1),
    refCount(),
  );

  clusterDetail$ = combineLatest(
    this.store.select(fromRegion.selectEntities),
    this.clusterName$,
  ).pipe(
    map(([entities, name]: [Entities, string]) => entities[name]),
    filter(data => !!data),
    publishReplay(1),
    refCount(),
  );

  clusterId$ = this.clusterDetail$.pipe(map(cluster => cluster.id));

  nodes$ = combineLatest(
    this.store.select(fromNodes.selectFeature),
    this.clusterName$,
  ).pipe(
    map(([state, cluster]) => state[cluster]),
    filter(data => !!data),
    map(state => state.ips.map(ip => state.entities[ip])),
    publishReplay(1),
    refCount(),
  ) as Observable<Node[]>;

  clusterState$ = this.clusterDetail$.pipe(
    pluck('state'),
    publishReplay(1),
    refCount(),
  );

  isNewK8s$ = this.clusterDetail$.pipe(
    map(region => this.regionUtilitiesService.isNewK8sRegion(region)),
    publishReplay(1),
    refCount(),
  );

  cloudUpdate$ = this.clusterDetail$.pipe(
    map(
      cluster =>
        cluster.state !== RegionStatus.Deploying &&
        cluster.state !== RegionStatus.Preparing,
    ),
    publishReplay(1),
    refCount(),
  );

  cloudDelete$ = this.clusterDetail$.pipe(
    map(
      cluster =>
        cluster.container_manager === ContainerManagers.None &&
        cluster.state !== RegionStatus.Deploying &&
        cluster.state !== RegionStatus.Preparing,
    ),
    publishReplay(1),
    refCount(),
  );

  cloudDeploy$ = this.clusterDetail$.pipe(
    map(
      cluster =>
        cluster.container_manager === ContainerManagers.None &&
        cluster.state !== RegionStatus.Deploying &&
        cluster.state !== RegionStatus.Preparing,
    ),
    publishReplay(1),
    refCount(),
  );

  enableDeploy$ = this.nodes$.pipe(
    map(
      nodes =>
        nodes.filter(
          node =>
            ![
              NodeState.Error,
              NodeState.Critical,
              NodeState.Deploying,
            ].includes(node.state),
        ).length,
    ),
    publishReplay(1),
    refCount(),
  );

  hasCreateNodePermission$ = this.clusterName$.pipe(
    mergeMap(name =>
      from(
        this.roleUtil.resourceTypeSupportPermissions(
          'cluster_node',
          { cluster_name: name },
          'create',
        ),
      ),
    ),
    startWith(false),
    publishReplay(1),
    refCount(),
  );

  createNodeDisabled$ = this.clusterDetail$.pipe(
    map(cluster =>
      get(cluster, 'features.node.features', []).includes('disable-creation'),
    ),
    startWith(false),
    publishReplay(1),
    refCount(),
  );

  autoScalingEnabled$ = this.clusterDetail$.pipe(
    map(cluster =>
      get(cluster, 'features.node.features', []).includes('auto-scaling'),
    ),
    startWith(false),
    publishReplay(1),
    refCount(),
  );

  isOpenStackRegion$ = this.clusterDetail$.pipe(
    map(cluster => cluster.attr.cloud.name === CloudNames.OpenStack),
    publishReplay(1),
    refCount(),
  );

  private permissionCache: { [key: string]: Observable<boolean> } = {};

  regionBadgeOption = {
    onChange: (region: Region) => {
      this.router.navigate(['/cluster/detail', region.name]);
    },
    filterFunction: () => true,
  };

  pluck = observablePluck;

  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute,
    private router: Router,
    private regionService: RegionService,
    private regionUtilitiesService: RegionUtilitiesService,
    @Inject(WEBLABS) public weblabs: Weblabs,
    private modalService: ModalService,
    private translateService: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private auiNotificationService: NotificationService,
  ) {}

  ngOnInit() {
    this.clusterDetail$.pipe(first()).subscribe(cluster => {
      if (cluster.platform_version === 'v4') {
        this.router.navigate(['/cluster/detail-v4', cluster.name], {
          replaceUrl: true,
        });
      }
    });
  }

  hasPermission(action: string) {
    if (!this.permissionCache[action]) {
      this.permissionCache[action] = this.clusterDetail$.pipe(
        map(cluster =>
          this.roleUtil.resourceHasPermission(cluster, 'cluster', action),
        ),
        distinctUntilChanged(),
        publishReplay(1),
        refCount(),
      );
    }
    return this.permissionCache[action];
  }

  addNode() {
    this.clusterDetail$.pipe(first()).subscribe(cluster => {
      const dialogRef = this.modalService.open(
        ([ContainerManagers.None, ContainerManagers.Kubernetes].includes(
          cluster.container_manager,
        )
          ? AddNodeComponent
          : CreateNodeComponent) as Type<any>,
        {
          title: this.translateService.get('add_node'),
          data: {
            cluster,
          },
        },
      );

      dialogRef.componentInstance.close.subscribe((added: boolean) => {
        dialogRef.close();
        if (added) {
          this.store.dispatch(new nodeActions.GetByRegion(cluster.name));
        }
      });
    });
  }

  autoScaling() {
    this.clusterDetail$.pipe(first()).subscribe(cluster => {
      const dialogRef = this.modalService.open(AutoScalingComponent, {
        title: this.translateService.get('auto_scaling'),
        width: 800,
        data: {
          cluster,
        },
      });

      dialogRef.componentInstance.close.subscribe((updated: boolean) => {
        dialogRef.close();
        if (updated) {
          this.store.dispatch(new regionActions.GetByName(cluster.name));
        }
      });
    });
  }

  updateLogSource() {
    this.clusterName$.pipe(first()).subscribe(clusterName => {
      const dialogRef = this.modalService.open(UpdateLogSourceComponent, {
        title: this.translateService.get('region_log_source_update'),
        data: {
          clusterName,
        },
      });

      dialogRef.componentInstance.close.subscribe((updated: boolean) => {
        dialogRef.close();
        if (updated) {
          this.store.dispatch(new logSourceActions.GetByRegion(clusterName));
        }
      });
    });
  }

  clearCluster() {
    this.clusterDetail$.pipe(first()).subscribe(cluster => {
      this.modalService
        .confirm({
          title: this.translateService.get('region_empty_region'),
          content: this.translateService.get('region_empty_prompt', {
            regionName: cluster.display_name,
          }),
        })
        .then(() =>
          this.regionService
            .updateRegion(cluster.name, {
              container_manager: ContainerManagers.None,
            })
            .then(() => {
              this.store.dispatch(new regionActions.GetByName(cluster.name));
            })
            .catch(() => {
              this.auiNotificationService.error(
                this.translateService.get('region_not_empty'),
              );
            }),
        )
        .catch(() => {});
    });
  }

  deleteCluster() {
    this.clusterName$.pipe(first()).subscribe(clusterName => {
      const dialogRef = this.modalService.open(DeleteRegionComponent, {
        title:
          this.translateService.get('delete') +
          this.translateService.get('region'),
        data: {
          clusterName,
        },
      });

      dialogRef.componentInstance.close.subscribe((deleted: boolean) => {
        dialogRef.close();
        if (deleted) {
          this.router.navigateByUrl('/cluster/list', {
            replaceUrl: true,
          });
        }
      });
    });
  }
}
