import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LogSourceFormModule } from 'app2/features/cluster/common/log-source-form/log-source-form.module';
import { PercentCardModule } from 'app2/features/cluster/common/percent-card/percent-card.module';
import { SharedModule } from 'app2/shared/shared.module';

import { AddNodeComponent } from './add-node/add-node.component';
import { AutoScalingComponent } from './auto-scaling/auto-scaling.component';
import { ClusterInfoComponent } from './cluster-info/cluster-info.component';
import { ClusterMonitorComponent } from './cluster-monitor/cluster-monitor.component';
import { CreateNamespaceComponent } from './create-namespace/create-namespace.component';
import { CreateNodeComponent } from './create-node/create-node.component';
import { DeleteNodeComponent } from './delete-node/delete-node.component';
import { DeleteRegionComponent } from './delete-region/delete-region.component';
import { DetailPageComponent } from './detail-page.component';
import { LogSourceTableComponent } from './log-source-table/log-source-table.component';
import { NodeListComponent } from './node-list/node-list.component';
import { UpdateLogSourceComponent } from './update-log-source/update-log-source.component';
import { UpdateNodeLabelsComponent } from './update-node-labels/update-node-labels.component';

@NgModule({
  imports: [SharedModule, RouterModule, PercentCardModule, LogSourceFormModule],
  declarations: [
    DetailPageComponent,
    ClusterInfoComponent,
    LogSourceTableComponent,
    NodeListComponent,
    AddNodeComponent,
    AutoScalingComponent,
    UpdateLogSourceComponent,
    DeleteRegionComponent,
    CreateNamespaceComponent,
    DeleteNodeComponent,
    UpdateNodeLabelsComponent,
    ClusterMonitorComponent,
    CreateNodeComponent,
  ],
  entryComponents: [
    AddNodeComponent,
    AutoScalingComponent,
    UpdateLogSourceComponent,
    DeleteRegionComponent,
    CreateNamespaceComponent,
    DeleteNodeComponent,
    UpdateNodeLabelsComponent,
    CreateNodeComponent,
  ],
  exports: [],
})
export class DetailPageModule {}
