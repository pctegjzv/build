import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, ReplaySubject, combineLatest } from 'rxjs';
import { filter, map, publishReplay, refCount, tap } from 'rxjs/operators';

import { observablePluck } from 'app2/core/utils';
import { RegionLogSource } from 'app2/shared/services/features/region.service';
import { GetByRegion } from 'app2/state-store/log_source/actions';
import { selectFeature } from 'app2/state-store/log_source/reducers';
import { AppState } from 'app2/state-store/reducers';

@Component({
  selector: 'rc-log-source-table',
  templateUrl: 'log-source-table.component.html',
  styleUrls: ['log-source-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LogSourceTableComponent implements OnChanges, OnInit {
  @Input()
  clusterName: string;

  private clusterName$ = new ReplaySubject<string>(1);

  logSource$: Observable<RegionLogSource>;

  pluck = observablePluck;

  constructor(private store: Store<AppState>) {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.clusterName) {
      this.clusterName$.next(changes.clusterName.currentValue);
    }
  }

  ngOnInit() {
    this.logSource$ = combineLatest(
      this.store.select(selectFeature),
      this.clusterName$.pipe(
        tap(region => {
          this.store.dispatch(new GetByRegion(region));
        }),
      ),
    ).pipe(
      map(([entities, region]) => entities[region]),
      filter(data => !!data),
      publishReplay(1),
      refCount(),
    );
  }
}
