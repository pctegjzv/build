import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import {
  BehaviorSubject,
  Observable,
  ReplaySubject,
  combineLatest as oCcombineLatest,
  merge,
} from 'rxjs';
import {
  combineLatest,
  distinctUntilKeyChanged,
  filter,
  first,
  map,
  mapTo,
  pluck,
  publishReplay,
  refCount,
  startWith,
  tap,
} from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';
import { DeleteNodeComponent } from 'app2/features/cluster/detail-page/delete-node/delete-node.component';
import { UpdateNodeLabelsComponent } from 'app2/features/cluster/detail-page/update-node-labels/update-node-labels.component';
import { nodeStatusButtonDisplayExpr } from 'app2/features/cluster/utils';
import {
  ContainerManagers,
  Node,
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import * as nodeActions from 'app2/state-store/node/actions';
import * as fromNodes from 'app2/state-store/node/reducers';
import { AppState } from 'app2/state-store/reducers';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-node-list',
  templateUrl: 'node-list.component.html',
  styleUrls: ['node-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NodeListComponent implements OnInit, OnDestroy {
  static UndeepAction = ['tag', 'uncordon', 'cordon', 'drain'];

  @Input()
  get cluster() {
    return this._cluster;
  }
  set cluster(val) {
    this._cluster = val;
    this.cluster$.next(val);
  }
  @Input()
  polling = true;
  @Input()
  pollingInterval = 30 * 1000;

  private allNodes$: Observable<Node[]>;
  cluster$ = new ReplaySubject<Region>(1);
  clusterName$ = this.cluster$.pipe(
    filter(region => !!region),
    distinctUntilKeyChanged('name'),
    map(region => region.name),
    publishReplay(1),
    refCount(),
  );
  filter$ = new BehaviorSubject<string>('');
  nodes$: Observable<Node[]>;

  nodesLoadingState$ = merge(
    this.actions.ofType(nodeActions.GET_BY_REGION).pipe(
      map(({ payload }: any) => payload.regionName || payload),
      combineLatest(this.clusterName$),
      filter(([a, b]: string[]) => a === b),
      mapTo('loading'),
    ),
    this.actions.ofType(nodeActions.GET_BY_REGION_SUCC).pipe(
      pluck<Action, string>('payload', 'regionName'),
      combineLatest(this.clusterName$),
      filter(([a, b]: string[]) => a === b),
      mapTo('succ'),
    ),
    this.actions.ofType(nodeActions.GET_BY_REGION_FAIL).pipe(
      pluck<Action, string>('payload', 'regionName'),
      combineLatest(this.clusterName$),
      filter(([a, b]: string[]) => a === b),
      mapTo('fail'),
    ),
  ).pipe(
    startWith('loading'),
    publishReplay(1),
    refCount(),
  );

  intervalId: number;

  private _cluster: Region;

  constructor(
    private store: Store<AppState>,
    private actions: Actions,
    @Inject(WEBLABS) public weblabs: Weblabs,
    private roleUtil: RoleUtilitiesService,
    private modalService: ModalService,
    private translateService: TranslateService,
    private regionService: RegionService,
    private auiNotificationService: NotificationService,
    private router: Router,
  ) {}

  ngOnInit() {
    if (this.polling) {
      this.intervalId = window.setInterval(() => {
        this.store.dispatch(new nodeActions.GetByRegion(this.cluster.name));
      }, this.pollingInterval);
    }

    this.allNodes$ = oCcombineLatest(
      this.store.select(fromNodes.selectFeature),
      this.clusterName$.pipe(
        tap(clusterName => {
          this.store.dispatch(new nodeActions.GetByRegion(clusterName));
        }),
      ),
    ).pipe(
      map(([state, cluster]) => state[cluster]),
      filter(data => !!data),
      map(state => state.ips.map(ip => state.entities[ip])),
    ) as Observable<Node[]>;

    this.nodes$ = oCcombineLatest(this.allNodes$, this.filter$).pipe(
      map(([nodes, filter]) =>
        nodes.filter(node => node.private_ip.includes(filter)),
      ),
      publishReplay(1),
      refCount(),
    );
  }

  ngOnDestroy() {
    clearInterval(this.intervalId);
  }

  isK8sContainer() {
    return this.cluster.container_manager === ContainerManagers.Kubernetes;
  }

  isEmptyCluster() {
    return this.cluster.container_manager === ContainerManagers.None;
  }

  isAutoCreateNode(node: Node) {
    return (
      node.attr &&
      (node.attr.source === 'api-create' || node.attr.source === 'scale-up')
    );
  }

  couldAction(node: Node, action: string): boolean {
    const hasPermission = this.roleUtil.resourceHasPermission(
      node,
      'cluster_node',
      NodeListComponent.UndeepAction.includes(action) ? 'update' : action,
    );
    return hasPermission && nodeStatusButtonDisplayExpr(node, action);
  }

  updateNodeTag(node: Node) {
    this.clusterName$.pipe(first()).subscribe(regionName => {
      const dialogRef = this.modalService.open(UpdateNodeLabelsComponent, {
        title:
          this.translateService.get('update_node_tags') +
          '(' +
          node.private_ip +
          ')',
        data: {
          regionName,
          node,
        },
      });

      dialogRef.componentInstance.close.subscribe((updated: boolean) => {
        dialogRef.close();
        if (updated) {
          this.store.dispatch(
            new nodeActions.GetByIp({ regionName, privateIp: node.private_ip }),
          );
        }
      });
    });
  }

  deleteNode(node: Node) {
    this.clusterName$.pipe(first()).subscribe(clusterName => {
      const dialogRef = this.modalService.open(DeleteNodeComponent, {
        title: this.translateService.get('region_delete_node'),
        data: {
          regionName: clusterName,
          privateIp: node.private_ip,
        },
      });

      dialogRef.componentInstance.close.subscribe(() => {
        dialogRef.close();
      });
    });
  }

  clearNode(node: Node) {
    this.clusterName$.pipe(first()).subscribe(regionName => {
      this.modalService
        .confirm({
          title: this.translateService.get('region_empty_node'),
          content: this.translateService.get('region_empty_node_prompt', {
            privateIp: node.private_ip,
          }),
        })
        .then(() =>
          this.regionService.updateNodeType(
            regionName,
            node.private_ip,
            'empty',
          ),
        )
        .then(() => {
          this.store.dispatch(
            new nodeActions.GetByIp({
              regionName,
              privateIp: node.private_ip,
            }),
          );
        })
        .catch(() => {});
    });
  }

  startNode(node: Node) {
    this.clusterName$.pipe(first()).subscribe(regionName => {
      this.regionService
        .startNode(regionName, node.private_ip)
        .then(() => {
          this.store.dispatch(
            new nodeActions.GetByIp({
              regionName,
              privateIp: node.private_ip,
            }),
          );
          const msg =
            this.translateService.get('start') +
            ' ' +
            node.private_ip +
            ' ' +
            this.translateService.get('success');
          this.auiNotificationService.success(msg);
        })
        .catch(() => {});
    });
  }

  stopNode(node: Node) {
    this.clusterName$.pipe(first()).subscribe(regionName => {
      this.modalService
        .confirm({
          title: this.translateService.get('stop'),
          content: this.translateService.get('region_stop_node_prompt', {
            privateIp: node.private_ip,
          }),
        })
        .then(() => this.regionService.stopNode(regionName, node.private_ip))
        .then(() => {
          this.store.dispatch(
            new nodeActions.GetByIp({
              regionName,
              privateIp: node.private_ip,
            }),
          );
        })
        .catch(() => {});
    });
  }

  deployNode(node: Node) {
    this.clusterName$.pipe(first()).subscribe(regionName => {
      this.modalService
        .confirm({
          title: this.translateService.get('update') + ': ' + node.private_ip,
          content: this.translateService.get('region_node_update_prompt'),
        })
        .then(() =>
          this.regionService.updateNodeType(
            regionName,
            node.private_ip,
            'k8s-slave',
          ),
        )
        .then(() => {
          this.store.dispatch(
            new nodeActions.GetByIp({
              regionName,
              privateIp: node.private_ip,
            }),
          );
        })
        .catch(() => {});
    });
  }

  doAction(node: Node, action: string) {
    this.clusterName$.pipe(first()).subscribe(regionName => {
      this.modalService
        .confirm({
          title: this.translateService.get('region_node_' + action),
          content: this.translateService.get(
            `region_node_maintain_${action}_confirm_message`,
          ),
        })
        .then(() =>
          this.regionService.actionNode(regionName, node.private_ip, action),
        )
        .then(() => {
          this.store.dispatch(
            new nodeActions.GetByIp({
              regionName,
              privateIp: node.private_ip,
            }),
          );
        })
        .catch(() => {});
    });
  }

  goToNode(node: Node) {
    this.router.navigate([
      '/cluster/node_detail',
      this.cluster.name,
      node.private_ip,
    ]);
  }

  getPodsCount(node: Node): string | number {
    const res = node.resources;
    return !res
      ? '-'
      : res.pods_count !== undefined
        ? res.pods_count
        : res.containers_count;
  }
}
