import { Component, Inject, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { LogSourceFormComponent } from 'app2/features/cluster/common/log-source-form/log-source-form.component';
import { BaseDialog } from 'app2/features/cluster/detail-page/base-dialog/base-dialog';
import {
  RegionLogSource,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { GetByRegion } from 'app2/state-store/log_source/actions';
import { selectFeature } from 'app2/state-store/log_source/reducers';
import { AppState } from 'app2/state-store/reducers';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'rc-update-log-source',
  templateUrl: 'update-log-source.component.html',
  styleUrls: ['update-log-source.component.scss', '../base-dialog/dialog.scss'],
})
export class UpdateLogSourceComponent extends BaseDialog {
  @ViewChild('data')
  data: LogSourceFormComponent;

  logSource$: Observable<RegionLogSource>;

  constructor(
    @Inject(MODAL_DATA) private modalData: { clusterName: string },
    private store: Store<AppState>,
    private regionService: RegionService,
  ) {
    super();
    this.store.dispatch(new GetByRegion(modalData.clusterName));
    this.logSource$ = this.store
      .select(selectFeature)
      .pipe(map(all => all[modalData.clusterName]));
  }

  async save() {
    this.submitting = true;
    try {
      await this.regionService.updateRegionLogSource(
        this.modalData.clusterName,
        this.data.value,
      );
      this.store.dispatch(new GetByRegion(this.modalData.clusterName));
      this.close.next(true);
    } catch (err) {
    } finally {
      this.submitting = false;
    }
  }
}
