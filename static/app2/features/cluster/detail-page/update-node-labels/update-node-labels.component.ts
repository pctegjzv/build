import { Component, Inject } from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

import { NotificationService } from 'alauda-ui';
import { BaseDialog } from 'app2/features/cluster/detail-page/base-dialog/base-dialog';
import {
  Node,
  NodeLabel,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-update-node-labels',
  templateUrl: 'update-node-labels.component.html',
  styleUrls: [
    'update-node-labels.component.scss',
    '../base-dialog/dialog.scss',
  ],
})
export class UpdateNodeLabelsComponent extends BaseDialog {
  labels: NodeLabel[] = [];

  modelForm = new FormGroup({
    key: new FormControl('', [
      Validators.required,
      Validators.pattern('^([A-Za-z0-9][-A-Za-z0-9_.]*)?[A-Za-z0-9]$'),
      Validators.maxLength(60),
      this.checkDuplicateKey.bind(this),
    ]),
    value: new FormControl('', [
      Validators.required,
      Validators.pattern('^([A-Za-z0-9][-A-Za-z0-9_.]*)?[A-Za-z0-9]$'),
      Validators.maxLength(60),
    ]),
  });

  errorMapper = {
    keyDuplicated: this.translateService.get('node_tags_key_unique'),
  };

  checking = false;

  constructor(
    @Inject(MODAL_DATA) public modalData: { node: Node; regionName: string },
    private translateService: TranslateService,
    private regionService: RegionService,
    private auiNotificationService: NotificationService,
  ) {
    super();
    this.labels = JSON.parse(JSON.stringify(modalData.node.labels)).sort(
      (label: NodeLabel) => (label.editable ? 0 : 1),
    );
  }

  trackByKey(_index: number, label: NodeLabel) {
    return label.key;
  }

  checkDuplicateKey(control: AbstractControl) {
    const invalid = !!this.labels.filter(label => label.key === control.value)
      .length;
    return invalid ? { keyDuplicated: { value: control.value } } : null;
  }

  async addLabel() {
    if (this.modelForm.invalid) {
      return;
    }
    this.checking = true;
    try {
      await this.regionService.checkNodeLabel(
        this.modalData.regionName,
        this.modelForm.value,
      );
      this.labels.unshift({
        ...this.modelForm.value,
        editable: true,
      });
      this.modelForm.reset({ key: '', value: '' });
    } catch (res) {
      const err = res.data;
      if (err.errors) {
        this.auiNotificationService.warning(
          err.errors[0] && err.errors[0].message,
        );
      }
    } finally {
      this.checking = false;
    }
  }

  deleteLabel(key: string) {
    this.labels = this.labels.filter(label => label.key !== key);
  }

  async confirm() {
    this.submitting = true;

    const labels = {};
    this.labels.filter(label => label.editable).forEach(label => {
      labels[label.key] = label.value;
    });

    try {
      await this.regionService.updateNodeLabel(
        this.modalData.regionName,
        this.modalData.node.private_ip,
        labels,
      );
      this.close.next(true);
    } catch (err) {
    } finally {
      this.submitting = false;
    }
  }
}
