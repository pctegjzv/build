import { Location } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { get } from 'lodash';
import { filter, first, pluck } from 'rxjs/operators';

import { VendorCustomer } from 'app2/core/customization/vendor-customer.types';
import { ENVIRONMENTS, WEBLABS } from 'app2/core/tokens';
import { Environments, Weblabs } from 'app2/core/types';
import {
  Cluster,
  RegionFeature,
  RegionService,
  RegionStatus,
} from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { AppState } from 'app2/state-store/reducers';
import {
  DELETE_BY_NAME_FAIL,
  DELETE_BY_NAME_SUCC,
  DeleteByName,
} from 'app2/state-store/region/actions';
import { TranslateService } from 'app2/translate/translate.service';

import { getRegionFeatureStatus } from '../utils';

@Component({
  templateUrl: './detail-v4.component.html',
  styleUrls: ['./detail-v4.component.scss', '../common-badge.component.scss'],
})
export class DetailV4Component implements OnInit {
  isCMB = true;
  clusterName: string;
  cluster: Cluster;
  columns = ['member', 'create_time'];
  regionFeatures?: {
    log: RegionFeature;
    metric: RegionFeature;
  };

  getRegionFeatureStatus = getRegionFeatureStatus;

  loading: boolean;

  get autoScalingEnabled() {
    return get(this.cluster, 'features.node.features', []).includes(
      'auto-scaling',
    );
  }

  get cloudUpdate() {
    return (
      this.cluster &&
      this.cluster.state !== RegionStatus.Deploying &&
      this.cluster.state !== RegionStatus.Preparing
    );
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private region: RegionService,
    private errorsToast: ErrorsToastService,
    private roleUtil: RoleUtilitiesService,
    private modal: ModalService,
    private translate: TranslateService,
    private store: Store<AppState>,
    private actions: Actions,
    private location: Location,
    @Inject(ENVIRONMENTS) private environments: Environments,
    @Inject(WEBLABS) public weblabs: Weblabs,
  ) {}

  ngOnInit() {
    this.isCMB = this.environments.vendor_customer === VendorCustomer.CMB;
    this.route.paramMap.subscribe(params => {
      this.clusterName = params.get('name');
      this.region.setRegionByName(this.clusterName);
      this.fetchCluster();
    });
  }

  trackByFn(index: number) {
    return index;
  }

  async fetchCluster() {
    this.loading = true;

    try {
      const [cluster, regionFeatures] = await Promise.all([
        this.region.getCluster(this.clusterName),
        this.region.getRegionFeatures(this.clusterName),
      ]);

      if (cluster.platform_version !== 'v4') {
        return this.router.navigate(['/cluster/detail', cluster.name], {
          replaceUrl: true,
        });
      }

      this.regionFeatures = regionFeatures;
      this.cluster = cluster;
    } catch (e) {
      this.errorsToast.error(e);
      return this.location.back();
    } finally {
      this.loading = false;
    }
  }

  shouldDisplay(action: string) {
    return this.roleUtil.resourceHasPermission(this.cluster, 'cluster', action);
  }

  async deleteCluster() {
    const name = this.clusterName;

    try {
      await this.modal.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('confirm_delete_cluster', {
          name,
        }),
      });
    } catch (e) {
      return;
    }

    this.loading = true;
    this.store.dispatch(
      new DeleteByName({
        name: this.clusterName,
        isNewCluster: true,
      }),
    );
    this.actions
      .ofType(DELETE_BY_NAME_SUCC)
      .pipe(pluck('payload'))
      .pipe(filter((name: string) => name === this.clusterName))
      .pipe(first())
      .subscribe(() => {
        this.loading = false;
        this.router.navigateByUrl('/cluster/list', {
          replaceUrl: true,
        });
      });

    this.actions
      .ofType(DELETE_BY_NAME_FAIL)
      .pipe(pluck('payload'))
      .pipe(
        filter(
          ({ name }: { name: string; error: any }) => name === this.clusterName,
        ),
      )
      .pipe(first())
      .subscribe(({ error }) => {
        this.loading = false;
        this.errorsToast.error(error);
      });
  }
}
