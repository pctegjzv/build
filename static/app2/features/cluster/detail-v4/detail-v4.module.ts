import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app2/shared/shared.module';

import { PercentCardModule } from 'app2/features/cluster/common/percent-card/percent-card.module';
import { ClusterMonitorComponent } from 'app2/features/cluster/detail-v4/monitor/cluster-monitor.component';
import { NodeDetailComponent } from 'app2/features/cluster/detail-v4/node-detail/node-detail.component';

import { DetailV4Component } from './detail-v4.component';
import { EditFeatureComponent } from './edit-feature/edit-feature.component';
import { FeaturesComponent } from './features/features.component';
import { ClusterNodesComponent } from './nodes/cluster-nodes.component';

@NgModule({
  imports: [SharedModule, RouterModule, PercentCardModule],
  declarations: [
    DetailV4Component,
    ClusterMonitorComponent,
    ClusterNodesComponent,
    FeaturesComponent,
    EditFeatureComponent,
    NodeDetailComponent,
  ],
  entryComponents: [EditFeatureComponent],
})
export class DetailV4Module {}
