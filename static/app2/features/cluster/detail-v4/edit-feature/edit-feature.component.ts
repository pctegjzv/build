import {
  Component,
  EventEmitter,
  Inject,
  Output,
  ViewChild,
} from '@angular/core';
import { DIALOG_DATA } from 'alauda-ui';

import { YamlCommentFormComponent } from 'app2/shared/components/yaml-comment-form/yaml-comment-form.component';
import { RegionFeature } from 'app2/shared/services/features/region.service';
import { viewActions } from 'app2/utils/code-editor-config';

@Component({
  templateUrl: './edit-feature.component.html',
  styleUrls: ['./edit-feature.component.scss'],
})
export class EditFeatureComponent {
  @ViewChild('yamlForm')
  yamlForm: YamlCommentFormComponent;

  @Output()
  confirmed = new EventEmitter<string>();

  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: {
      enabled: false,
    },
    readOnly: true,
  };

  codeEditorActions = viewActions;

  showComments: boolean;

  yamlConfig: string;
  showingYaml: boolean;
  loading: boolean;

  constructor(@Inject(DIALOG_DATA) public feature: RegionFeature) {
    this.yamlConfig = this.feature.template.versions[0].values_yaml_content;
  }

  showYamlConfig() {
    this.yamlConfig = this.yamlForm.dump();
    this.showingYaml = true;
  }

  confirm() {
    this.yamlConfig = this.yamlForm.dump();
    this.confirmed.emit(this.yamlConfig);
  }
}
