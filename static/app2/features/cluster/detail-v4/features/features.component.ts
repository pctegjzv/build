import { Location } from '@angular/common';
import {
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DialogService, DialogSize } from 'alauda-ui';
import { get } from 'lodash';

import { getRegionFeatureStatus } from 'app2/features/cluster/utils';
import {
  Integration,
  IntegrationService,
} from 'app2/shared/services/features/integration.service';
import {
  RegionFeature,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

import { EditFeatureComponent } from '../edit-feature/edit-feature.component';

@Component({
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.scss', '../../common-badge.component.scss'],
})
export class FeaturesComponent implements OnInit, OnDestroy {
  @ViewChild('dockThirdPartyTemp')
  dockThirdPartyTemp: TemplateRef<any>;

  getRegionFeatureStatus = getRegionFeatureStatus;

  regionName: string;

  regionFeatures: {
    log: RegionFeature;
    metric: RegionFeature;
  };

  checkingForUpdates: boolean;

  timeoutId: number;

  integrations: Integration[];
  integrationId: string;
  isAddOrUpdate: boolean;

  checkForUpdatesEnabled: boolean;
  installEnabled: boolean;

  constructor(
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private location: Location,
    private dialog: DialogService,
    private region: RegionService,
    private integration: IntegrationService,
    private translate: TranslateService,
    private errorsToast: ErrorsToastService,
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(async () => {
      await this.refetch();
      this.cdr.detectChanges();
    });
  }

  private async fetch() {
    this.regionName = this.route.snapshot.paramMap.get('regionName');

    this.region.setRegionByName(this.regionName);

    try {
      this.regionFeatures = await this.region.getRegionFeatures(
        this.regionName,
      );
    } catch (e) {
      this.errorsToast.error(e);
      if (!this.regionFeatures && e.status === 404) {
        return this.location.back();
      }
    }
  }

  async refetch(immediate = true) {
    clearTimeout(this.timeoutId);

    if (immediate) {
      await this.fetch();
    }

    if (
      !this.regionFeatures ||
      ['deploying', 'unknown'].includes(
        this.getRegionFeatureStatus(this.regionFeatures.log),
      )
    ) {
      this.timeoutId = window.setTimeout(() => this.refetch(), 5000);
    }
  }

  ngOnDestroy() {
    clearTimeout(this.timeoutId);
  }

  async checkForUpdates() {
    // TODO: waiting for backend
  }

  async dockThirdParty(isAddOrUpdate = false) {
    this.isAddOrUpdate = isAddOrUpdate;
    this.integrationId = get(
      this.regionFeatures.metric,
      'config.integration_uuid',
    );

    if (!this.integrations) {
      this.integrations = await this.integration.getIntegrations({
        families: 'Features',
        page_size: 1000,
      });
    }

    this.dialog.open(this.dockThirdPartyTemp);
  }

  closeModal() {
    this.dialog.closeAll();
  }

  async editRegionFeature({
    featureName,
    type,
    yamlContent,
    isAddOrUpdate,
  }: {
    featureName: 'log' | 'metric';
    type: 'official' | 'prometheus';
    yamlContent?: string;
    isAddOrUpdate?: boolean;
  }) {
    const isOfficial = type === 'official';

    if (!isOfficial && !this.integrationId) {
      return;
    }

    try {
      this.regionFeatures[featureName] = await this.region.editRegionFeature(
        this.regionName,
        featureName,
        {
          config: {
            type,
            integration_uuid: isOfficial ? undefined : this.integrationId,
          },
          values_yaml_content: yamlContent,
        },
        isAddOrUpdate,
      );
    } catch (e) {
      return this.errorsToast.error(e);
    }

    this.closeModal();
  }

  editOfficialFeature(featureName: 'log' | 'metric', isAddOrUpdate?: boolean) {
    const dialogRef = this.dialog.open(EditFeatureComponent, {
      data: this.regionFeatures[featureName],
      size: DialogSize.Large,
    });

    dialogRef.componentInstance.confirmed.subscribe(
      async (yamlContent: string) => {
        dialogRef.componentInstance.loading = true;

        await this.editRegionFeature({
          featureName,
          type: 'official',
          yamlContent,
          isAddOrUpdate,
        });

        dialogRef.componentInstance.loading = false;

        this.refetch(false);
      },
    );
  }

  async deleteFeature(featureName: 'log' | 'metric') {
    try {
      await this.dialog.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('confirm_delete_region_feature'),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
      await this.region.deleteRegionFeature(this.regionName, featureName);
    } catch (e) {
      return this.errorsToast.error(e);
    }

    this.regionFeatures[featureName].config = null;
  }
}
