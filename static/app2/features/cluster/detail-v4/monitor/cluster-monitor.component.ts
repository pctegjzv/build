import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { LogService } from 'app2/shared/services/features/log.service';
import { MonitorUtilitiesService } from 'app2/shared/services/features/monitor.utilities.service';
import { HttpService } from 'app2/shared/services/http/http.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import {
  ChartData,
  MetricQueries,
  MetricsDetails,
  YAxisScale,
} from 'app_user/features/application/monitor/component-monitor.component';
import {
  AGGREGATORS,
  MONITOR_COLOR_SET,
  TIME_STAMP_OPTIONS,
} from 'app_user/features/application/monitor/component-monitor.constant';
import moment from 'moment';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

const QUERY_TIME_STAMP_OPTIONS = TIME_STAMP_OPTIONS.concat([
  {
    type: 'custom_time_range',
    offset: 0,
  },
]);

@Component({
  selector: 'rc-cluster-monitor-v4',
  templateUrl: 'cluster-monitor.component.html',
  styleUrls: ['cluster-monitor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClusterMonitorComponent implements OnInit, OnDestroy {
  @Input()
  privateIp: string;
  @Input()
  clusterName: string;
  private onDestroy$ = new Subject<void>();
  private intervalTimer: number;

  private loadMetrics$ = new BehaviorSubject(null);

  aggregators = AGGREGATORS.map(aggregator => {
    return {
      key: aggregator.key,
      name: aggregator.name,
    };
  });
  timeStampOptions = QUERY_TIME_STAMP_OPTIONS.map(option => {
    return {
      ...option,
      name: option.type,
    };
  });
  private selectedAggregator = this.aggregators[0].key;
  private selectedTimeStampOption = this.timeStampOptions[0].type;
  current_time = moment();
  start_time = this.current_time
    .clone()
    .startOf('day')
    .subtract(6, 'days');
  step: number;
  end_time: number;
  queryDates = {
    start_time: this.start_time.valueOf(),
    end_time: this.current_time.valueOf(),
  };
  queryDatesShown = {
    start_time: this.logService.dateNumToStr(this.queryDates.start_time),
    end_time: this.logService.dateNumToStr(this.queryDates.end_time),
  };
  dateTimeOptions = {
    maxDate: this.current_time
      .clone()
      .endOf('day')
      .valueOf(),
    enableTime: true,
  };
  chart = {
    scheme: {
      domain: MONITOR_COLOR_SET,
    },
    showGridLines: false,
    gradient: true,
    xAxisTickFormatting: (tick: string) => {
      return moment(parseInt(tick, 10) * 1000).format('MM-DD HH:mm');
    },
    yAxisTickFormattingPercent: (val: number) => {
      if (val * 100 > 0.01 || val === 0) {
        return (val * 100).toPrecision(3) + '%';
      }
      return (val * 100).toFixed(4) + '%';
    },
  };
  chartLoading = false;
  cpuData: ChartData[] = [];
  memData: ChartData[] = [];
  sentBytesData: ChartData[] = [];
  receivedBytesData: ChartData[] = [];
  cpuChartScale: YAxisScale;
  memChartScale: YAxisScale;
  sentBytesChartScale: YAxisScale;
  receivedBytesChartScale: YAxisScale;

  constructor(
    private cdr: ChangeDetectorRef,
    private httpService: HttpService,
    private monitorUtilitiesService: MonitorUtilitiesService,
    private logService: LogService,
    private errorsToastService: ErrorsToastService,
    @Inject(ACCOUNT) public account: RcAccount,
  ) {}

  async ngOnInit() {
    this.queryMetrics();
    this.intervalTimer = window.setInterval(() => {
      if (!this.chartLoading) {
        if (this.selectedTimeStampOption !== 'custom_time_range') {
          this.resetTimeRange();
        }
        this.loadCharts();
      }
    }, 60000);
  }

  ngOnDestroy() {
    window.clearInterval(this.intervalTimer);
    this.onDestroy$.next();
  }

  onTimeStampOptionSelect() {
    if (this.selectedTimeStampOption !== 'custom_time_range') {
      this.resetTimeRange();
    }
    this.loadCharts();
  }

  onLoadCharts() {
    this.loadCharts();
  }

  private resetTimeRange() {
    this.queryDates = this.logService.getTimeRangeByRangeType(
      this.selectedTimeStampOption,
    );
    this.fillCalendar(this.queryDates.start_time, this.queryDates.end_time);
  }

  fillCalendar(start_time: number, end_time: number) {
    this.queryDatesShown.start_time = this.logService.dateNumToStr(start_time);
    this.queryDatesShown.end_time = this.logService.dateNumToStr(end_time);
  }

  onStartTimeSelect(event: string) {
    this.queryDates.start_time = this.logService.dateStrToNum(event);
  }

  onEndTimeSelect(event: string) {
    this.queryDates.end_time = this.logService.dateStrToNum(event);
  }

  loadCharts() {
    this.chartLoading = true;
    this.loadMetrics$.next({
      aggregator: this.selectedAggregator,
      start: parseInt((this.queryDates.start_time / 1000).toFixed(0), 10),
      end: parseInt((this.queryDates.end_time / 1000).toFixed(0), 10),
    });
  }

  private getYAxisValueScale(data: ChartData[]) {
    let {
      yScaleMin,
      yScaleMax,
    } = this.monitorUtilitiesService.getYAxisValueScale(data);
    if (yScaleMin < 0.000001) {
      yScaleMin = 0;
    }
    if (yScaleMax < 0.000001) {
      yScaleMax = 0.000001;
    }
    return { yScaleMin, yScaleMax };
  }

  private queryUtilization(query: any, aggregator: string, metricType: string) {
    const queries: MetricQueries = {
      aggregator: aggregator,
    };
    if (this.privateIp) {
      queries.labels = [
        {
          type: 'EQUAL',
          name: '__name__',
          value: `node.${metricType}`,
        },
        {
          type: 'EQUAL',
          name: 'name',
          value: this.privateIp,
        },
        {
          type: 'EQUAL',
          name: 'kind',
          value: 'Node',
        },
      ];
    } else {
      queries.labels = [
        {
          type: 'EQUAL',
          name: '__name__',
          value: `cluster.${metricType}`,
        },
        {
          type: 'EQUAL',
          name: 'kind',
          value: 'Cluster',
        },
      ];
    }
    const endpoint = `/ajax/v1/metrics/${this.clusterName}/query_range`;
    return this.httpService.request<MetricsDetails>(endpoint, {
      method: 'POST',
      body: {
        ...query,
        queries: [queries],
      },
      addNamespace: false,
    });
  }

  private parseMetricsResponse(result: any) {
    if (result === undefined || result.result === undefined) {
      return [];
    }
    return result.result.map((el: any) => {
      // fill in the chart when result is less than 30 points
      if (el.values.length < 30) {
        let dateNewest = this.end_time;
        const obj = {};
        for (let i = 0; i < 30; i++) {
          obj[(dateNewest -= this.step)] = 0;
        }
        if (el.values.length > 0) {
          el.values.forEach((element: any) => {
            obj[element[0]] = element[1];
          });
        }
        el.values = Object.keys(obj).map(function(key) {
          return [Number(key), obj[key]];
        });
      }
      return {
        name: this.privateIp || this.clusterName,
        series: el.values.map((value: number[]) => {
          return {
            value: value[1],
            name: value[0] + '',
          };
        }),
      };
    });
  }

  private queryMetrics() {
    this.loadMetrics$.pipe(takeUntil(this.onDestroy$)).subscribe(args => {
      if (args === null) {
        return;
      }
      this.step = parseInt(((args.end - args.start) / 30).toFixed(0), 10);
      this.end_time = args.end;
      const query = {
        start: args.start + this.step,
        end: args.end,
        step: this.step,
      };
      const requestCpuUtilization = this.queryUtilization(
        query,
        args.aggregator,
        'cpu.utilization',
      );
      const requestMemUtilization = this.queryUtilization(
        query,
        args.aggregator,
        'memory.utilization',
      );
      Promise.all([requestCpuUtilization, requestMemUtilization])
        .then(results => {
          this.cpuData = this.parseMetricsResponse(results[0]);
          this.cpuChartScale = this.getYAxisValueScale(this.cpuData);
          this.memData = this.parseMetricsResponse(results[1]);
          this.memChartScale = this.getYAxisValueScale(this.memData);
        })
        .catch(error => {
          this.errorsToastService.error(error);
          this.cpuData = [];
          this.memData = [];
        })
        .finally(() => {
          this.chartLoading = false;
          this.cdr.markForCheck();
        });
    });

    this.resetTimeRange();
    this.loadCharts();
  }
}
