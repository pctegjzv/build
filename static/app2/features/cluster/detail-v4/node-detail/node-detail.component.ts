import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { VendorCustomer } from 'app2/core/customization/vendor-customer.types';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { formatCommonUnit, formatNumUnit } from 'app2/features/cluster/utils';
import {
  ClusterNode,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';

interface NodeVm {
  name: string;
  privateIp: string;
  labels: Array<{
    name: string;
    value: string;
  }>;
  status: {
    state: 'Ready' | 'NotReady' | 'Warning';
    schedulable: boolean;
    taint: boolean;
  };
  cpu: number;
  memory: number | string;
  type: 'master' | 'node';
}

@Component({
  selector: 'rc-node-detail',
  templateUrl: 'node-detail.component.html',
  styleUrls: ['node-detail.component.scss'],
})
export class NodeDetailComponent implements OnInit {
  isCMB = true;
  clusterName: string;
  filterIp = '';
  nodeVm: NodeVm;

  constructor(
    private region: RegionService,
    private regionUtilities: RegionUtilitiesService,
    private route: ActivatedRoute,
    private errorsToast: ErrorsToastService,
    @Inject(ENVIRONMENTS) private environments: Environments,
  ) {}

  ngOnInit() {
    this.isCMB = this.environments.vendor_customer === VendorCustomer.CMB;
    this.route.paramMap.subscribe(
      async params => {
        this.clusterName = params.get('clusterName');
        this.filterIp = params.get('privateIp');
        const { items } = await this.region.getClusterNodes(this.clusterName);
        const nodes = items.filter(node =>
          node.status.addresses
            .find(addr => addr.type === 'InternalIP')
            .address.includes(this.filterIp),
        );
        const node = nodes[0];
        this.nodeVm = {
          name: node.metadata.name,
          privateIp: node.status.addresses.find(
            addr => addr.type === 'InternalIP',
          ).address,
          labels: this.getLabels(node),
          status: {
            state: this.regionUtilities.getNodeState(node),
            schedulable: !node.spec.unschedulable,
            taint: node.spec.taints && !!node.spec.taints.length,
          },
          cpu: formatNumUnit(node.status.allocatable.cpu),
          memory: formatCommonUnit(node.status.allocatable.memory, true),
          type:
            'node-role.kubernetes.io/master' in node.metadata.labels
              ? 'master'
              : 'node',
        };
      },
      e => {
        this.errorsToast.error(e);
      },
    );
  }

  getLabels(node: ClusterNode) {
    return Object.entries(node.metadata.labels).reduce(
      (prev, [name, value]) => {
        prev.push({ name, value });
        return prev;
      },
      [] as Array<{
        name: string;
        value: string;
      }>,
    );
  }
}
