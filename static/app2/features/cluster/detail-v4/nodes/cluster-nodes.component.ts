import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject, ReplaySubject, combineLatest } from 'rxjs';
import { debounceTime, map, publishReplay, refCount } from 'rxjs/operators';

import { formatCommonUnit, formatNumUnit } from 'app2/features/cluster/utils';
import {
  ClusterNode,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';

interface NodeVm {
  name: string;
  privateIp: string;
  labels: Array<{
    name: string;
    value: string;
  }>;
  status: {
    state: 'Ready' | 'NotReady' | 'Warning';
    schedulable: boolean;
    taint: boolean;
  };
  cpu: number;
  memory: number;
  type: 'master' | 'node';
}

@Component({
  selector: 'rc-cluster-nodes',
  templateUrl: './cluster-nodes.component.html',
  styleUrls: ['./cluster-nodes.component.scss'],
})
export class ClusterNodesComponent implements OnInit {
  @Input()
  clusterName: string;

  columns = [
    'name',
    'privateIp',
    'nodeTags',
    'status',
    'cpu',
    'memory',
    'nodeType',
  ];

  filterIp = '';
  pageSize = 20;

  filterIp$ = new BehaviorSubject<string>(this.filterIp);
  nodeVms$ = new ReplaySubject<NodeVm[]>();
  pageIndex$ = new BehaviorSubject<number>(0);

  filteredNodes$ = combineLatest(this.filterIp$, this.nodeVms$).pipe(
    debounceTime(100),
    map(([filterIp, nodeVms]) => {
      filterIp = filterIp.trim();
      return filterIp
        ? nodeVms.filter(nodeVm => nodeVm.privateIp.includes(filterIp))
        : nodeVms;
    }),
    publishReplay(),
    refCount(),
  );

  filertedAndPagedNodes$ = combineLatest(
    this.filteredNodes$,
    this.pageIndex$,
  ).pipe(
    map(([filteredNodes, pageIndex]) =>
      filteredNodes.slice(
        pageIndex * this.pageSize,
        (pageIndex + 1) * this.pageSize,
      ),
    ),
    publishReplay(),
    refCount(),
  );

  constructor(
    private region: RegionService,
    private regionUtilities: RegionUtilitiesService,
    private errorsToast: ErrorsToastService,
  ) {}

  async ngOnInit() {
    let nodes: ClusterNode[];

    try {
      const { items } = await this.region.getClusterNodes(this.clusterName);
      nodes = items;
    } catch (e) {
      this.errorsToast.error(e);
      this.nodeVms$.next([]);
      return;
    }

    this.nodeVms$.next(
      nodes.map(
        node =>
          ({
            name: node.metadata.name,
            privateIp: node.status.addresses.find(
              addr => addr.type === 'InternalIP',
            ).address,
            labels: this.getLabels(node),
            status: {
              state: this.regionUtilities.getNodeState(node),
              schedulable: !node.spec.unschedulable,
              taint: node.spec.taints && !!node.spec.taints.length,
            },
            cpu: formatNumUnit(node.status.allocatable.cpu),
            memory: formatCommonUnit(node.status.allocatable.memory, true),
            type:
              'node-role.kubernetes.io/master' in node.metadata.labels
                ? 'master'
                : 'node',
          } as NodeVm),
      ),
    );
  }

  getLabels(node: ClusterNode) {
    return Object.entries(node.metadata.labels).reduce(
      (prev, [name, value]) => {
        prev.push({ name, value });
        return prev;
      },
      [] as Array<{
        name: string;
        value: string;
      }>,
    );
  }
}
