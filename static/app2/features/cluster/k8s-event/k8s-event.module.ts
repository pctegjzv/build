import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { K8sEventListComponent } from 'app2/features/cluster/k8s-event/list/k8s-event-list.component';
import { SharedModule } from 'app2/shared/shared.module';

@NgModule({
  imports: [SharedModule, RouterModule],
  declarations: [K8sEventListComponent],
})
export class K8sEventModule {}
