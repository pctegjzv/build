import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NotificationService } from 'alauda-ui';
import {
  K8sEvent,
  K8sEventQuery,
  K8sEventsResult,
} from 'app2/features/event/event.types';
import { AccountService } from 'app2/shared/services/features/account.service';
import { EventService } from 'app2/shared/services/features/event.service';
import {
  NamespaceOption,
  NamespaceService,
} from 'app2/shared/services/features/namespace.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import * as _ from 'lodash';
import moment from 'moment';
import { Subject } from 'rxjs';

import { RESOURCE_TYPES, TIMESTAMP_OPTIONS } from '../k8s-event.constant';

interface TimeRangeOption {
  name: string;
  type: string;
  offset: number;
}
@Component({
  selector: 'rc-k8s-event-list',
  templateUrl: './k8s-event-list.component.html',
  styleUrls: ['./k8s-event-list.component.scss'],
})
export class K8sEventListComponent implements OnInit, OnDestroy {
  @Input()
  kind: string;
  @Input()
  name: string;
  @Input()
  namespace: string;
  @Input()
  cluster: string;
  @Input()
  embedded = false;
  private onDestroy$ = new Subject<void>();
  loading = false;
  initialized = false;
  eventQuery: K8sEventQuery;
  region: Region;
  timeRangeOptions: TimeRangeOption[] = TIMESTAMP_OPTIONS.map(option => {
    return {
      ...option,
      name: this.translate.get(option.type),
    };
  });
  timeRangeOption = 'last_30_minites';
  timeRangeOutOfDate = false;
  queryDatesShown: {
    startTime: string;
    endTime: string;
  };
  calendarOptions: {
    maxDate: number;
    minDate: number;
    enableTime: true;
  };
  currentTime = moment();
  endTime: moment.Moment;
  startTime: moment.Moment;
  events: K8sEvent[] = [];
  columns: string[] = [
    'namespace',
    'type',
    'resource_name',
    'time_range',
    'count',
    'reason',
    'message',
  ];
  pagination = {
    size: 20,
    current: 1,
    pages: 0,
  };
  clusterNamspaces: string[] = [];
  resourceTypes: string[] = [];
  suggestions: string[] = [];
  conditions: string[] = [];
  queryCondition = {};
  queryDate = {};
  // @ViewChild('conditionsInput') conditionsInput: TagsInputComponent;
  constructor(
    private eventService: EventService,
    private errorToast: ErrorsToastService,
    private regionService: RegionService,
    private translate: TranslateService,
    private namespaceService: NamespaceService,
    private auiNotificationService: NotificationService,
    private accountService: AccountService,
  ) {}

  async ngOnInit() {
    if (this.isUserView) {
      this.columns.splice(0, 1);
      this.region = {
        name: this.cluster,
      };
    } else {
      this.region = await this.regionService.getCurrentRegion();
    }
    this.startTime = this.currentTime
      .clone()
      .startOf('minutes')
      .subtract(30, 'minutes');
    this.endTime = this.currentTime.clone();
    this.queryDatesShown = {
      startTime: this.dateFormat(this.startTime.valueOf()),
      endTime: this.dateFormat(this.endTime.valueOf()),
    };
    this.calendarOptions = {
      maxDate: this.currentTime
        .clone()
        .endOf('day')
        .valueOf(),
      minDate: this.currentTime
        .clone()
        .startOf('day')
        .subtract(6, 'days')
        .valueOf(),
      enableTime: true,
    };
    this.initSuggestions();
    this.fetchEvents();
    this.initialized = true;
  }

  get isUserView() {
    return this.accountService.isUserView();
  }

  async initSuggestions() {
    this.resourceTypes = RESOURCE_TYPES.map((type: string) => {
      return `kind: ${type}`;
    });
    try {
      const namespaces = await this.namespaceService.getNamespaceOptions(
        this.region.name,
      );
      this.clusterNamspaces = namespaces.map((namespace: NamespaceOption) => {
        return `namespace: ${namespace.name}`;
      });
    } catch (error) {
      this.clusterNamspaces = [];
    }
    this.suggestions = [...this.clusterNamspaces, ...this.resourceTypes];
  }

  async fetchEvents(pageChanged = false) {
    this.queryDate = {
      start_time: this.startTime.valueOf() / 1000,
      end_time: this.endTime.valueOf() / 1000,
    };

    if (!this.checkQueryDates()) {
      return;
    }
    this.pagination.current = pageChanged ? this.pagination.current : 1;
    const conditions = this.generateCondition();
    if (this.kind) {
      conditions.kind = conditions.kind
        ? `${conditions.kind},${this.kind}`
        : this.kind;
    }
    if (this.name) {
      conditions.name = conditions.name
        ? `${conditions.name},${this.name}`
        : this.name;
    }
    if (this.namespace) {
      conditions.namespace = conditions.namespace
        ? `${conditions.namespace},${this.namespace}`
        : this.namespace;
    }
    this.eventQuery = {
      cluster: this.region.name,
      ...this.queryDate,
      ...conditions,
      page: this.pagination.current,
      page_size: this.pagination.size,
    } as K8sEventQuery;
    this.loading = true;
    try {
      const eventsResut: K8sEventsResult = await this.eventService.getK8sEvents(
        this.eventQuery,
      );
      this.events = eventsResut.results;
      this.pagination.pages = eventsResut.num_pages;
    } catch (error) {
      this.errorToast.error(error);
    }
    this.loading = false;
  }

  trackByFn(_index: number, item: K8sEvent) {
    return item.metadata.uid;
  }

  onTimeRangeChanged(value: string) {
    const option: TimeRangeOption = _.find(this.timeRangeOptions, {
      type: value,
    });
    if (option.type !== 'custom_time_range') {
      this.resetTimeRange();
      this.fetchEvents();
    }
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  private resetTimeRange() {
    const timeRange = TIMESTAMP_OPTIONS.find(
      option => option.type === this.timeRangeOption,
    );
    this.endTime = this.currentTime.clone();
    this.startTime = this.endTime
      .clone()
      .startOf('seconds')
      .subtract(timeRange.offset);
    this.fillCalendar(this.startTime.valueOf(), this.endTime.valueOf());
  }

  private fillCalendar(start_time: number, end_time: number) {
    this.queryDatesShown.startTime = this.dateFormat(start_time);
    this.queryDatesShown.endTime = this.dateFormat(end_time);
  }

  onStartTimeSelect(time: string) {
    this.startTime = moment(time, 'YYYY-MM-DD HH:mm:ss');
    this.fetchEvents();
  }

  onEndTimeSelect(time: string) {
    this.endTime = moment(time, 'YYYY-MM-DD HH:mm:ss');
    this.fetchEvents();
  }

  setConditions(_conditions: string[]) {
    this.fetchEvents();
  }

  private checkQueryDates() {
    if (!this.queryDate['start_time'] || !this.queryDate['end_time']) {
      this.auiNotificationService.warning(
        this.translate.get('log_query_timerange_required'),
      );
      return false;
    }
    if (
      this.timeRangeOption === 'custom_time_range' &&
      this.queryDate['start_time'] >= this.queryDate['end_time']
    ) {
      this.auiNotificationService.warning(
        this.translate.get('log_query_timerange_warning'),
      );
      return false;
    }
    return true;
  }

  private generateCondition(): {
    kind?: string;
    namespace?: string;
    name?: string;
  } {
    const ret = _.chain(this.conditions)
      .map((condition: string) => {
        const param = {};
        const match = condition.match(/^(.+):\s(.+)$/);
        if (match && match[0]) {
          const value = (match[2] || '').trim();
          param[match[1]] = value;
        } else {
          param['name'] = condition;
        }
        return param;
      })
      .reduce((result, param) =>
        _.mergeWith(result, param, (pre, cur) => {
          if (pre) {
            return [pre, cur].join(',');
          } else {
            return cur;
          }
        }),
      )
      .value();
    return ret ? ret : {};
  }

  search() {
    this.fetchEvents();
  }

  pageChanged(page: number) {
    this.pagination.current = page;
    this.eventQuery.page = this.pagination.current;
    this.fetchEvents(true);
  }

  private dateFormat(stamp: number) {
    return moment(stamp).format('YYYY-MM-DD HH:mm:ss');
  }
}
