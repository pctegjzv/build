import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { filter, map, publishReplay, refCount, tap } from 'rxjs/operators';

import {
  Cluster,
  ClusterNode,
  Node,
  NodeState,
  Region,
  RegionStatus,
} from 'app2/shared/services/features/region.service';
import {
  ClusterStateColors,
  RegionUtilitiesService,
} from 'app2/shared/services/features/region.utilities.service';
import { GetByRegion } from 'app2/state-store/node/actions';
import * as fromNode from 'app2/state-store/node/reducers';
import { AppState } from 'app2/state-store/reducers';

const CLUSTER_STATUS_MAP = {
  running: {
    status: 'running',
    icon: 'check_s',
    color: '#1bb393',
  },
  deploying: {
    status: 'deploying',
    icon: 'basic:sync',
    color: '#009ce3',
  },
  error: {
    status: 'error',
    icon: 'basic:exclamation',
    color: '#f34235',
  },
};

@Component({
  selector: 'rc-cluster-card',
  templateUrl: './cluster-card.component.html',
  styleUrls: ['./cluster-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClusterCardComponent {
  private _cluster: Region | Cluster;

  get cluster() {
    return this._cluster;
  }

  @Input()
  set cluster(cluster) {
    this._cluster = cluster;
    this.cluster$.next(cluster);
  }

  cluster$ = new BehaviorSubject(this.cluster);

  clusterStatus$ = this.cluster$.pipe(
    map(cluster => {
      let status = 'error';

      switch (cluster.state) {
        case RegionStatus.Running:
        case RegionStatus.Deploying:
          status = this.cluster.state.toLowerCase();
          break;
      }

      return CLUSTER_STATUS_MAP[status];
    }),
  );

  clusterNodes$ = combineLatest(
    this.store.select(fromNode.selectFeature),
    this.cluster$.pipe(
      tap(cluster =>
        this.store.dispatch(
          new GetByRegion({
            regionName: cluster.name,
            isK8sV4: cluster.platform_version === 'v4',
          }),
        ),
      ),
    ),
  ).pipe(
    map(([state, region]) => state[region.name]),
    filter(data => !!data),
    map(state => state.ips.map(ip => state.entities[ip])),
    publishReplay(1),
    refCount(),
  );

  masterNodes$ = this.clusterNodes$.pipe(
    map(nodes =>
      nodes.filter(node => {
        if ('type' in node) {
          return ['SYS', 'SYSLAVE'].includes(node.type);
        }

        return 'node-role.kubernetes.io/master' in node.metadata.labels;
      }),
    ),
  );

  unnormalNodes$ = this.clusterNodes$.pipe(
    map(nodes => nodes.filter(this.isUnnormalNode)),
  );

  isUnnormalNode = (node: Node | ClusterNode) => {
    if ('state' in node) {
      return [
        NodeState.Unknown,
        NodeState.Offline,
        NodeState.Error,
        NodeState.Critical,
      ].includes(node.state);
    }

    return [ClusterStateColors.NotReady, ClusterStateColors.Warning].includes(
      this.regionUtilities.getNodeState(node),
    );
  };

  constructor(
    private store: Store<AppState>,
    private regionUtilities: RegionUtilitiesService,
  ) {}
}
