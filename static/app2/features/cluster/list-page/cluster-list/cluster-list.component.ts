import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Cluster, Region } from 'app2/shared/services/features/region.service';

@Component({
  selector: 'rc-cluster-list',
  templateUrl: 'cluster-list.component.html',
})
export class ClusterListComponent implements OnInit {
  @Input()
  clusters$: Observable<Array<Region | Cluster>>;

  orderedClusters$: Observable<Array<Region | Cluster>>;

  trackById(_i: number, region: Region | Cluster) {
    return region.id;
  }

  ngOnInit() {
    this.orderedClusters$ = this.clusters$.pipe(
      map(clusters =>
        clusters.sort(
          (a, b) =>
            new Date(b.created_at).getTime() - new Date(a.created_at).getTime(),
        ),
      ),
    );
  }
}
