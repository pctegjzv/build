import { Component, Inject, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { BehaviorSubject, Observable, combineLatest } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';

import { VendorCustomerService } from 'app2/core/customization/vendor-customer.service';
import { CustomModuleName } from 'app2/core/customization/vendor-customer.types';
import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';
import { Cluster, Region } from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { AppState } from 'app2/state-store/reducers';
import * as regionActions from 'app2/state-store/region/actions';
import * as fromRegion from 'app2/state-store/region/reducers';

@Component({
  templateUrl: 'list-page.component.html',
  styleUrls: ['list-page.component.scss'],
})
export class ListPageComponent implements OnInit {
  filter$ = new BehaviorSubject('');

  clusters$: Observable<Array<Region | Cluster>>;

  hasCreatePermission = false;

  get createClusterDisabled() {
    return this.vendorCustomer.support(
      CustomModuleName.CREATE_CLUSTER_DISABLED,
    );
  }

  get showMirrorCluster() {
    return this.vendorCustomer.support(CustomModuleName.MIRROR_CLUSTER);
  }

  constructor(
    @Inject(WEBLABS) public weblabs: Weblabs,
    private store: Store<AppState>,
    private roleUtilities: RoleUtilitiesService,
    private vendorCustomer: VendorCustomerService,
  ) {}

  async ngOnInit() {
    this.clusters$ = combineLatest(
      this.store.select(fromRegion.selectList),
      this.filter$.pipe(
        map(filter => filter.trim()),
        distinctUntilChanged(),
      ),
    ).pipe(
      map(
        ([clusters, filter]) =>
          filter
            ? clusters.filter(({ display_name }) =>
                display_name.includes(filter),
              )
            : clusters,
      ),
    );
    this.hasCreatePermission = await this.roleUtilities.resourceTypeSupportPermissions(
      'cluster',
    );
  }

  async refreshRegions() {
    this.store.dispatch(new regionActions.GetAll());
  }
}
