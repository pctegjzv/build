import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { first } from 'rxjs/operators';

import {
  Mirror,
  MirrorService,
} from 'app2/features/cluster/mirror-page/mirror.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { GET_ALL_SUCC } from 'app2/state-store/region/actions';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './mirror-detail.component.html',
  styleUrls: ['./mirror-detail.component.scss'],
})
export class MirrorDetailComponent {
  name: string;

  loading: boolean;
  initialized: boolean;
  mirror: Mirror;

  columns = ['region_name', 'create_time'];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private mirrorService: MirrorService,
    private roleUtilities: RoleUtilitiesService,
    private modal: ModalService,
    private translate: TranslateService,
    private errorsToast: ErrorsToastService,
    private region: RegionService,
    private actions: Actions,
    private location: Location,
  ) {
    this.route.paramMap.subscribe(params => {
      this.name = params.get('name');
      this.lodaData();
    });
  }

  async lodaData() {
    try {
      this.mirror = await this.mirrorService.getMirror(this.name);
      this.initialized = true;
    } catch (e) {
      this.errorsToast.error(e);
      if (!this.initialized && e.status === 404) {
        this.location.back();
      }
    }
  }

  showButton(action: string) {
    return this.roleUtilities.resourceHasPermission(
      this.mirror,
      'cluster',
      action,
    );
  }

  async deleteMirror() {
    const { name } = this.mirror;

    try {
      await this.modal.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('confirm_delete_mirror_cluster', {
          name,
        }),
      });
    } catch (e) {
      return;
    }

    this.loading = true;

    try {
      await this.mirrorService.deleteMirror(name);
    } catch (e) {
      return this.errorsToast.error(e);
    } finally {
      this.loading = false;
    }

    this.region.refetch();

    this.actions
      .ofType(GET_ALL_SUCC)
      .pipe(first())
      .subscribe(() => {
        this.router.navigate(['/cluster/mirror']);
      });
  }
}
