import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable, Subject, combineLatest } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import {
  Mirror,
  MirrorService,
} from 'app2/features/cluster/mirror-page/mirror.service';
import {
  Cluster,
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { delay } from 'app2/shared/services/utility/delay';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { AppState } from 'app2/state-store/reducers';
import { GET_ALL_SUCC } from 'app2/state-store/region/actions';
import * as fromRegion from 'app2/state-store/region/reducers';
import { TranslateService } from 'app2/translate/translate.service';

export const MIRROR_COLORS = [
  '#37d9f0',
  '#4da8ee',
  '#aa17d0',
  '#82ccd2',
  '#89b0cd',
  '#9389b0',
  '#def3f3',
  '#cde0ef',
  '#dfdbec',
  '#00a3af',
  '#3876a6',
  '#765c83',
];

@Component({
  templateUrl: './mirror-edit.component.html',
  styleUrls: ['./mirror-edit.component.scss'],
})
export class MirrorEditComponent implements OnInit {
  isUpdating: boolean;

  name: string;
  allColors = MIRROR_COLORS;
  mirror = {} as Mirror;
  disabledColors: string[] = [];
  regionIds: string[];
  loading: boolean;

  mirror$ = new Subject<Mirror>();
  regions$: Observable<Array<Region | Cluster>>;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>,
    private location: Location,
    private mirrorService: MirrorService,
    private auiNotificationService: NotificationService,
    private errorsToast: ErrorsToastService,
    private region: RegionService,
    private translate: TranslateService,
    private actions: Actions,
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(async params => {
      this.name = params.get('name');
      this.isUpdating = !!this.name;

      if (this.isUpdating) {
        this.mirror = await this.mirrorService.getMirror(this.name);
        this.regionIds = this.mirror.regions.map(({ id }) => id);
      } else {
        this.mirror = {
          regions: [],
          flag: MIRROR_COLORS[0],
        } as Mirror;
        this.regionIds = [];
        await delay(0);
      }

      this.mirror$.next(this.mirror);
    });

    this.regions$ = combineLatest(
      this.store.select(fromRegion.selectList) as Observable<
        Array<Region | Cluster>
      >,
      this.mirror$,
    ).pipe(
      map(([regions, mirror]) => {
        const result: Cluster[] = [];
        regions.forEach(region => {
          if (region.platform_version !== 'v4') {
            return;
          }

          if (
            'mirror' in region &&
            region.mirror &&
            region.mirror.name &&
            (!this.isUpdating || mirror.flag !== region.mirror.flag)
          ) {
            this.disabledColors.push(region.mirror.flag);
          }

          if (
            !('mirror' in region && region.mirror && region.mirror.name) ||
            region.mirror.name === mirror.name
          ) {
            result.push(Object.assign(region, {
              _display: `${region.display_name}(${region.name})`,
            }) as Cluster);
          }
        });

        if (
          !this.isUpdating &&
          this.disabledColors.includes(this.mirror.flag)
        ) {
          this.mirror.flag = this.allColors.find(
            color => !this.disabledColors.includes(color),
          );
        }

        return result;
      }),
    );
  }

  onRegionAdded({ name, id }: Region | Cluster) {
    this.mirror.regions.push({
      name,
      id,
    });
  }

  onRegionRemoved({ id }: Region | Cluster) {
    this.mirror.regions = this.mirror.regions.filter(
      region => region.id !== id,
    );
  }

  cancel() {
    this.location.back();
  }

  async confirm(ngForm: NgForm) {
    if (ngForm.invalid) {
      return;
    }

    if (this.mirror.regions.length < 2) {
      return this.auiNotificationService.error(
        this.translate.get('mirror_regions_limit'),
      );
    }

    this.loading = true;

    try {
      await this.mirrorService[
        this.isUpdating ? 'updateMirror' : 'createMirror'
      ](this.mirror);
    } catch (e) {
      this.errorsToast.error(e);
      this.loading = false;
      return;
    }

    this.region.refetch();

    this.actions
      .ofType(GET_ALL_SUCC)
      .pipe(first())
      .subscribe(() => {
        this.loading = false;
        this.router.navigate(['/cluster/mirror/detail', this.mirror.name]);
      });
  }
}
