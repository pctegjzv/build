import { Component, OnInit } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { Observable, Subject, combineLatest, from } from 'rxjs';
import { catchError, first, map, startWith } from 'rxjs/operators';

import { RegionService } from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { GET_ALL_SUCC } from 'app2/state-store/region/actions';
import { TranslateService } from 'app2/translate/translate.service';

import { Mirror as _Mirror, MirrorService } from './mirror.service';

interface Mirror extends _Mirror {
  displayedRegions?: _Mirror['regions'];
}

@Component({
  templateUrl: './mirror-page.component.html',
  styleUrls: ['./mirror-page.component.scss'],
})
export class MirrorPageComponent implements OnInit {
  columns = ['name', 'member', 'create_time', 'action'];
  loading: boolean;
  hasCreatePermission: boolean;

  mirrors$: Observable<Mirror[]>;
  keyword$ = new Subject<string>();

  constructor(
    private mirror: MirrorService,
    private roleUtilities: RoleUtilitiesService,
    private modal: ModalService,
    private translate: TranslateService,
    private errorsToast: ErrorsToastService,
    private region: RegionService,
    private actions: Actions,
  ) {}

  trackByFn(mirror: Mirror) {
    return mirror.name;
  }

  async ngOnInit() {
    this.mirrors$ = combineLatest(
      from(
        this.mirror.getMirrors().then(mirrors =>
          mirrors.map(mirror =>
            Object.assign(mirror, {
              displayedRegions: mirror.regions.slice(0, 3),
            }),
          ),
        ),
      ),
      this.keyword$.pipe(startWith(null)),
    ).pipe(
      map(([mirrors, keyword]) => {
        keyword = keyword && keyword.trim();
        return keyword
          ? mirrors.filter(
              ({ name, display_name }) =>
                name.includes(keyword) || display_name.includes(keyword),
            )
          : mirrors;
      }),
      catchError(() => []),
    );

    this.hasCreatePermission = await this.roleUtilities.resourceTypeSupportPermissions(
      'cluster',
    );
  }

  isButtonDisabled(mirror: Mirror, action: string) {
    return !this.roleUtilities.resourceHasPermission(mirror, 'cluster', action);
  }

  async deleteMirror(mirror: Mirror) {
    const { name } = mirror;
    try {
      await this.modal.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('confirm_delete_mirror_cluster', {
          name,
        }),
      });
    } catch (e) {
      return;
    }

    this.loading = true;

    try {
      await this.mirror.deleteMirror(name);
    } catch (e) {
      this.errorsToast.error(e);
      this.loading = false;
      return;
    }

    this.mirrors$ = this.mirrors$.pipe(
      map(mirrors => mirrors.filter(({ name }) => name !== mirror.name)),
    );

    this.region.refetch();

    this.actions
      .ofType(GET_ALL_SUCC)
      .pipe(first())
      .subscribe(() => {
        this.loading = false;
      });
  }
}
