import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app2/shared/shared.module';

import { MirrorDetailComponent } from './mirror-detail.component';
import { MirrorEditComponent } from './mirror-edit.component';
import { MirrorPageComponent } from './mirror-page.component';

import { MirrorService } from './mirror.service';

@NgModule({
  imports: [RouterModule, SharedModule],
  declarations: [
    MirrorPageComponent,
    MirrorDetailComponent,
    MirrorEditComponent,
  ],
  providers: [MirrorService],
})
export class MirrorPageModule {}
