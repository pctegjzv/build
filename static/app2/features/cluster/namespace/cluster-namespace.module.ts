import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ClusterNamespaceService } from 'app2/features/cluster/namespace/cluster-namespace.service';
import { ClusterNamespaceDetailComponent } from 'app2/features/cluster/namespace/detail/cluster-namespace-detail.component';
import { ClusterNamespaceResourcesComponent } from 'app2/features/cluster/namespace/detail/cluster-namespace-resources.component';
import { ClusterNamespaceCreateComponent } from 'app2/features/cluster/namespace/list/cluster-namespace-create.component';
import { ClusterNamespaceEditQuotaComponent } from 'app2/features/cluster/namespace/quota/cluster-namespace-edit-quota.component';
import { ClusterNamespaceViewQuotaComponent } from 'app2/features/cluster/namespace/quota/cluster-namespace-view-quota.component';
import { SharedModule } from 'app2/shared/shared.module';

import { ClusterNamespaceListComponent } from './list/cluster-namespace-list.component';

const components = [
  ClusterNamespaceListComponent,
  ClusterNamespaceCreateComponent,
  ClusterNamespaceDetailComponent,
  ClusterNamespaceResourcesComponent,
  ClusterNamespaceViewQuotaComponent,
  ClusterNamespaceEditQuotaComponent,
];

@NgModule({
  imports: [SharedModule, RouterModule],
  declarations: components,
  exports: components,
  entryComponents: [
    ClusterNamespaceCreateComponent,
    ClusterNamespaceViewQuotaComponent,
  ],
  providers: [ClusterNamespaceService],
})
export class ClusterNamespaceModule {}
