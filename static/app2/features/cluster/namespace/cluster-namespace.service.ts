import { Injectable } from '@angular/core';

import { HttpService } from 'app2/shared/services/http/http.service';

export interface QuotaItemDetail {
  pods: string;
  persistentvolumeclaims: string;
  'requests.cpu': string;
  'requests.memory': string;
  'requests.storage': string;
}

export interface ResourceQuota {
  resources_actions: {};
  kubernetes: {
    apiVersion: string;
    kind: 'ResourceQuota';
    metadata: {
      creationTimestamp: string;
      name: string;
      namespace: string;
      resourceVersion: string;
      selfLink: string;
      uid: string;
    };
    spec: {
      hard: QuotaItemDetail;
    };
    status: {
      hard: QuotaItemDetail;
      used: QuotaItemDetail;
    };
  };
}

export interface ResourceItem {
  uuid: string;
  name: string;
  created_at: string;
}

export enum ResourceType {
  APP = 'apps',
  SERVICE = 'alaudaservices',
  CONFIG_MAP = 'configmaps',
  POD = 'pods',
  PVC = 'persistentvolumeclaims',
  NETWORK_POLICY = 'networkpolicies',
  SERVICE_INSTANCE = 'serviceinstances',
  SERVICE_BINDING = 'servicebindings',
}

export interface Resources {
  [type: string]: ResourceItem[];
}

@Injectable()
export class ClusterNamespaceService {
  CLUSTERS_URL = '/ajax/v2/kubernetes/clusters/';

  constructor(private httpService: HttpService) {}

  getResourceQuotas(
    cluster_id: string,
    namespace: string,
  ): Promise<ResourceQuota[]> {
    return this.httpService
      .request({
        method: 'GET',
        url:
          this.CLUSTERS_URL + cluster_id + '/resourcequotas/' + namespace + '/',
        addNamespace: false,
      })
      .then(({ result }) => result);
  }

  getResourceQuota({
    cluster_id,
    namespace,
    quota_name,
  }: {
    cluster_id: string;
    namespace: string;
    quota_name: string;
  }) {
    return this.httpService.request<ResourceQuota>({
      method: 'GET',
      url:
        this.CLUSTERS_URL +
        cluster_id +
        '/resourcequotas/' +
        namespace +
        '/' +
        quota_name,
      addNamespace: false,
    });
  }

  createResourceQuota(
    cluster_id: string,
    resourceQuota: ResourceQuota['kubernetes'],
  ) {
    return this.httpService.request({
      method: 'POST',
      url: this.CLUSTERS_URL + cluster_id + '/resourcequotas',
      addNamespace: false,
      body: resourceQuota,
    });
  }

  updateResourceQuota(
    cluster_id: string,
    resourceQuota: ResourceQuota['kubernetes'],
  ) {
    return this.httpService.request({
      method: 'PUT',
      url:
        this.CLUSTERS_URL +
        cluster_id +
        '/resourcequotas/' +
        resourceQuota.metadata.namespace +
        '/' +
        resourceQuota.metadata.name,
      addNamespace: false,
      body: resourceQuota,
    });
  }

  deleteResourceQuota({
    cluster_id,
    namespace,
    quota_name,
  }: {
    cluster_id: string;
    namespace: string;
    quota_name: string;
  }) {
    return this.httpService.request({
      method: 'DELETE',
      url:
        this.CLUSTERS_URL +
        cluster_id +
        '/resourcequotas/' +
        namespace +
        '/' +
        quota_name,
      addNamespace: false,
    });
  }

  getResources(clusterId: string, namespace: string) {
    return this.httpService.request<Resources>({
      method: 'GET',
      url:
        this.CLUSTERS_URL +
        clusterId +
        '/namespaces/' +
        namespace +
        '/resources',
      addNamespace: false,
    });
  }
}
