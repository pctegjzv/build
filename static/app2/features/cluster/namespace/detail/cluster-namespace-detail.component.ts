import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import {
  ClusterNamespaceService,
  QuotaItemDetail,
  ResourceQuota,
} from 'app2/features/cluster/namespace/cluster-namespace.service';
import { ClusterNamespaceViewQuotaComponent } from 'app2/features/cluster/namespace/quota/cluster-namespace-view-quota.component';
import { formatCommonUnit, formatNumUnit } from 'app2/features/cluster/utils';
import { SerieChartData } from 'app2/features/dashboard/charts/ngx-charts.types';
import {
  Namespace,
  NamespaceService,
} from 'app2/shared/services/features/namespace.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ModalMode } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './cluster-namespace-detail.component.html',
  styleUrls: ['./cluster-namespace-detail.component.scss'],
})
export class ClusterNamespaceDetailComponent implements OnInit {
  readonly POD_USED_COLOR = '#63d283';
  readonly PVC_USED_COLOR = '#24a7e3';
  readonly UNUSED_COLOR = '#e8e8e8';

  clusterId: string;
  namespace: string;
  clusterNamespace: Namespace;

  quotaPermissions: string[];

  resourceQuotas: ResourceQuota[];
  defaultResourceQuota: ResourceQuota & {
    _deleting?: boolean;
  };
  containsSystemQuota: boolean;
  podColorScheme = {
    domain: [this.POD_USED_COLOR, this.UNUSED_COLOR],
  };
  pvcColorScheme = {
    domain: [this.PVC_USED_COLOR, this.UNUSED_COLOR],
  };
  chartResults: {
    pods?: SerieChartData[];
    persistentvolumeclaims?: SerieChartData[];
  };
  chartPercents: {
    pods?: number;
    persistentvolumeclaims?: number;
  };

  deleting: boolean;

  constructor(
    private clusterNamespaceService: ClusterNamespaceService,
    private namespaceService: NamespaceService,
    private roleUtilities: RoleUtilitiesService,
    private modal: ModalService,
    private translate: TranslateService,
    private errorsToast: ErrorsToastService,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(async params => {
      this.clusterId = params.get('clusterId');
      this.namespace = params.get('namespace');

      try {
        [
          this.clusterNamespace,
          { k8s_resourcequotas: this.quotaPermissions },
        ] = await Promise.all([
          this.namespaceService.getNamespace(this.clusterId, this.namespace),
          this.roleUtilities.resourceTypesPermissions(['k8s_resourcequotas']),
        ]);
      } catch (e) {
        this.errorsToast.error(e);

        if (e.status === 404 && !this.clusterNamespace) {
          this.location.back();
        }

        return;
      }

      this.setUI();
    });
  }

  formatNum(value: string) {
    return formatNumUnit(value);
  }

  formatValue(value: string, fixed?: boolean) {
    return formatCommonUnit(value, fixed);
  }

  async setUI() {
    const resourceQuotas = await this.clusterNamespaceService.getResourceQuotas(
      this.clusterId,
      this.namespace,
    );

    const systemQuotaIndex = resourceQuotas.findIndex(
      quota => quota.kubernetes.metadata.name === 'default',
    );

    if (systemQuotaIndex === -1) {
      this.containsSystemQuota = false;
      this.defaultResourceQuota = null;
    } else {
      this.containsSystemQuota = true;

      const systemQuota = resourceQuotas[systemQuotaIndex];

      if (resourceQuotas.length === 1) {
        if (!systemQuota.kubernetes.status.hard) {
          systemQuota.kubernetes.status.hard = {} as QuotaItemDetail;
        }
        if (!systemQuota.kubernetes.status.used) {
          systemQuota.kubernetes.status.used = {} as QuotaItemDetail;
        }

        this.defaultResourceQuota = systemQuota;
        const { status } = this.defaultResourceQuota.kubernetes;

        this.chartResults = {};
        this.chartPercents = {};

        ['pods', 'persistentvolumeclaims'].forEach(type => {
          const used = formatNumUnit(status.used[type]) || 0;
          const total = formatNumUnit(status.hard[type]) || 0;
          const unused = Math.max(total - used, 0);
          this.chartResults[type] = [
            {
              name: this.translate.get('used'),
              value: used,
            },
            {
              name: this.translate.get('unused'),
              value: unused,
            },
          ];
          this.chartPercents[type] = this.getPercentText(
            used ? used / total : 0,
          );
        });
      } else if (systemQuotaIndex) {
        resourceQuotas.splice(systemQuotaIndex, 1);
        resourceQuotas.unshift(systemQuota);
      }
    }

    this.resourceQuotas = resourceQuotas;
  }

  hasNamespacePermission(action: string) {
    return this.roleUtilities.resourceHasPermission(
      this.clusterNamespace.resource_actions
        ? this.clusterNamespace
        : (this.clusterNamespace.kubernetes as any),
      'namespace',
      action,
    );
  }

  hasQuotaPermission(action: string) {
    return (
      this.quotaPermissions &&
      this.quotaPermissions.includes('k8s_resourcequotas:' + action)
    );
  }

  private getPercentText(scale: number) {
    scale = scale ? (isFinite(scale) ? scale : 1) : 0;
    return (scale * 100).toFixed(1).replace(/\.0$/, '');
  }

  private getScale(type: string) {
    const used = this.defaultResourceQuota.kubernetes.status.used[type] || '';
    const hard = this.defaultResourceQuota.kubernetes.status.hard[type] || '';

    if (type === 'requests.cpu') {
      const formattedCPU = this.formatNum(used);
      return formattedCPU ? formattedCPU / this.formatNum(hard) : 0;
    }
    const formattedValue = this.formatValue(used) as number;
    return formattedValue
      ? formattedValue / (this.formatValue(hard) as number)
      : 0;
  }

  getScaleText(type: string) {
    return this.getPercentText(this.getScale(type));
  }

  getCalcText(type: string) {
    const scale = this.getScale(type);
    return { maxWidth: `calc(${scale * 100}% + ${56 * (1 - scale) - 32}px)` };
  }

  async deleteNamespace() {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('region_namespace_delete', {
          namespace: this.clusterNamespace.kubernetes.metadata.name,
        }),
      });
    } catch (e) {
      return;
    }

    this.deleting = true;

    try {
      await this.namespaceService.deleteNamespace(
        this.clusterId,
        this.namespace,
      );
    } catch (e) {
      this.deleting = false;
      return this.errorsToast.error(e);
    }

    this.router.navigate(['/cluster/namespace'], {
      replaceUrl: true,
    });
  }

  getStatus() {
    return this.clusterNamespace.kubernetes.status.phase === 'Active'
      ? 'running'
      : 'deleting';
  }

  viewResourceQuota(resourceQuota: ResourceQuota) {
    this.modal.open(ClusterNamespaceViewQuotaComponent, {
      title: this.translate.get(resourceQuota.kubernetes.metadata.name),
      mode: ModalMode.RIGHT_SLIDER,
      width: 800,
      data: {
        clusterNamespace: this.clusterNamespace,
        resourceQuota,
        clusterId: this.clusterId,
      },
    });
  }

  async deleteResourceQuota(resourceQuota: ResourceQuota) {
    const quotaName = resourceQuota.kubernetes.metadata.name;
    try {
      await this.modal.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('quota_delete_confirm_tips', {
          name: quotaName,
        }),
      });
    } catch (e) {
      return;
    }

    (resourceQuota as any)._deleting = true;

    try {
      await this.clusterNamespaceService.deleteResourceQuota({
        cluster_id: this.clusterId,
        namespace: this.namespace,
        quota_name: quotaName,
      });
    } catch (e) {
      return this.errorsToast.error(e);
    } finally {
      (resourceQuota as any)._deleting = false;
    }

    this.resourceQuotas = this.resourceQuotas.filter(
      quota => quota !== resourceQuota,
    );

    this.setUI();
  }
}
