import { Component, Input, OnInit } from '@angular/core';

import {
  ClusterNamespaceService,
  ResourceItem,
  ResourceType,
  Resources,
} from 'app2/features/cluster/namespace/cluster-namespace.service';
import { Namespace } from 'app2/shared/services/features/namespace.service';

@Component({
  selector: 'rc-cluster-namespace-resources',
  templateUrl: './cluster-namespace-resources.component.html',
  styleUrls: ['./cluster-namespace-resources.component.scss'],
})
export class ClusterNamespaceResourcesComponent implements OnInit {
  @Input()
  clusterId: string;
  @Input()
  clusterNamespace: Namespace;

  resources: Resources;
  resourceTypes: ResourceType[];
  activeResourceType: ResourceType;

  filterText: string;

  get activeResources() {
    if (this.resources) {
      const items = this.resources[this.activeResourceType];

      return items && this.filterText
        ? items.filter(({ name }) => name.includes(this.filterText.trim()))
        : items;
    }
  }

  constructor(private clusterNamespaceService: ClusterNamespaceService) {}

  async ngOnInit() {
    this.resources = await this.clusterNamespaceService.getResources(
      this.clusterId,
      this.clusterNamespace.kubernetes.metadata.name,
    );

    this.resourceTypes = Object.keys(this.resources) as ResourceType[];
    this.activeResourceType = this.resourceTypes[0];
  }

  changeActiveResourceType(resourceType: ResourceType) {
    this.activeResourceType = resourceType;
  }

  getResourceLink(resource: ResourceItem) {
    let routerLink: string;

    switch (this.activeResourceType) {
      case ResourceType.APP:
        routerLink = '/k8s_app/detail/' + resource.uuid;
        break;
      case ResourceType.SERVICE:
        routerLink = '/k8s_app/detail/component/' + resource.uuid;
        break;
      case ResourceType.CONFIG_MAP:
        routerLink = `/k8s_configmap/detail/${this.clusterId}/${
          this.clusterNamespace.kubernetes.metadata.name
        }/${resource.name}`;
        break;
      case ResourceType.POD:
        routerLink = '/k8s_pod';
        break;
      case ResourceType.PVC:
        routerLink = `/storage/pvc/detail/${this.clusterId}/${
          this.clusterNamespace.kubernetes.metadata.name
        }/${resource.name}`;
        break;
      case ResourceType.NETWORK_POLICY:
        routerLink =
          '/network_policy/detail/' +
          this.clusterId +
          '/' +
          this.clusterNamespace.kubernetes.metadata.name +
          '/' +
          resource.name;
        break;
      case ResourceType.SERVICE_INSTANCE:
        routerLink =
          '/service_catalog/service_instance/instance_detail/' + resource.uuid;
        break;
    }

    return routerLink;
  }
}
