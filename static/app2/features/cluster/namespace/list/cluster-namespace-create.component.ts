import { Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

import { NamespaceService } from 'app2/shared/services/features/namespace.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';

@Component({
  templateUrl: './cluster-namespace-create.component.html',
})
export class ClusterNamespaceCreateComponent {
  @Output()
  close = new EventEmitter<boolean>();

  namespaceName: string;
  loading: boolean;

  constructor(
    private region: RegionService,
    private namespaceService: NamespaceService,
    private errorsToast: ErrorsToastService,
  ) {}

  async confirm(form: NgForm) {
    if (form.invalid) {
      return;
    }

    const currRegion = await this.region.getCurrentRegion();

    this.loading = true;

    try {
      await this.namespaceService.createNamespace(
        currRegion.id,
        this.namespaceName,
      );
    } catch (e) {
      return this.errorsToast.error(e);
    } finally {
      this.loading = false;
    }

    this.close.emit(true);
  }
}
