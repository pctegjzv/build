import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import { Router } from '@angular/router';
import { ClusterNamespaceCreateComponent } from 'app2/features/cluster/namespace/list/cluster-namespace-create.component';
import {
  Namespace,
  NamespaceService,
} from 'app2/shared/services/features/namespace.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import {
  Pagination,
  PaginationDataWrapper,
} from 'app2/shared/services/utility/pagination-data';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './cluster-namespace-list.component.html',
  styleUrls: ['./cluster-namespace-list.component.scss'],
})
export class ClusterNamespaceListComponent implements OnInit, OnDestroy {
  searching: boolean;
  search: string;

  showCreateNamespace: boolean;
  clusterNamespaces: Namespace[];
  count: number;
  pageSize = 20;
  timeoutId: number;
  clusterId: string;

  regionSubscription: Subscription;

  paginationDataWrapper: PaginationDataWrapper<any>;
  paginationDataSubscription: Subscription;
  pollingTimer: number;
  destroyed: boolean;

  get currentPage(): number {
    return this.paginationDataWrapper.pageNo - 1;
  }

  get currentPageSize(): number {
    return this.paginationDataWrapper.pageSize;
  }

  get loading() {
    return this.paginationDataWrapper.loading;
  }

  get empty() {
    return !this.clusterNamespaces || !this.clusterNamespaces.length;
  }

  constructor(
    private modal: ModalService,
    private translate: TranslateService,
    private namespace: NamespaceService,
    private roleUtilities: RoleUtilitiesService,
    private regionUtilities: RegionUtilitiesService,
    private region: RegionService,
    private errorsToast: ErrorsToastService,
    private cdr: ChangeDetectorRef,
    private router: Router,
  ) {}

  ngOnInit() {
    const fetchRequest = (
      page: number,
      page_size: number,
      params: {
        clusterId: string;
      },
    ) => {
      return this.namespace.getNamespaces({
        page,
        page_size,
        ...params,
      });
    };
    this.paginationDataWrapper = new PaginationDataWrapper(
      fetchRequest,
      this.pageSize,
    );

    this.paginationDataSubscription = this.paginationDataWrapper.pagination.subscribe(
      (paginationData: Pagination<Namespace>) => {
        this.searching = false;
        if (!paginationData.loadError) {
          this.count = paginationData.count;
          this.clusterNamespaces = paginationData.results || [];
          if (!this.clusterNamespaces.length && paginationData.pageNo > 1) {
            this.paginationDataWrapper.refetch(--paginationData.pageNo);
            return;
          }
          this.cdr.detectChanges();
        }
        this.resetPollingTimer();
      },
    );

    this.regionSubscription = this.region.region$
      .pipe(filter(r => !!r))
      .subscribe(region => {
        this.clusterId = region.id;
        this.clusterNamespaces = [];
        this.paginationDataWrapper.setParams({
          clusterId: region.id,
        });
        this.checkCreateNamespace(region);
      });
  }

  ngOnDestroy() {
    this.destroyed = true;
    this.regionSubscription.unsubscribe();
    this.paginationDataSubscription.unsubscribe();
    clearTimeout(this.pollingTimer);
  }

  async checkCreateNamespace(region: Region) {
    this.showCreateNamespace =
      this.regionUtilities.isNewK8sRegion(region) &&
      (await this.roleUtilities.resourceTypeSupportPermissions(
        'namespace',
        {
          cluster_name: region.name,
        },
        'create',
      ));
  }

  createNamespace() {
    const modalRef = this.modal.open(ClusterNamespaceCreateComponent, {
      title: this.translate.get('create_namespace'),
    });

    modalRef.componentInstance.close
      .pipe(first())
      .subscribe((isConfirm: boolean) => {
        if (isConfirm) {
          this.refetch();
        }

        modalRef.close();
      });
  }

  viewNamespace(namespace: Namespace) {
    return this.router.navigateByUrl(
      `/cluster/namespace/detail/${this.clusterId}/${
        namespace.kubernetes.metadata.name
      }`,
    );
  }

  triggerSearch(queryString: string) {
    this.searching = true;
    this.paginationDataWrapper.setParams({
      name: queryString,
    });
  }

  getStatus(clusterNamespace: Namespace) {
    return clusterNamespace.kubernetes.status.phase === 'Active'
      ? 'running'
      : 'deleting';
  }

  buttonDisplay(clusterNamespace: Namespace, action: string) {
    return this.roleUtilities.resourceHasPermission(
      clusterNamespace,
      'namespace',
      action,
    );
  }

  async deleteNamespace(clusterNamespace: Namespace) {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('region_namespace_delete', {
          namespace: clusterNamespace.kubernetes.metadata.name,
        }),
      });
    } catch (e) {
      return;
    }

    (clusterNamespace as any)._deleting = true;

    try {
      await this.namespace.deleteNamespace(
        this.clusterId,
        clusterNamespace.kubernetes.metadata.name,
      );
    } catch (e) {
      (clusterNamespace as any)._deleting = false;
      return this.errorsToast.error(e);
    }

    this.clusterNamespaces.splice(
      this.clusterNamespaces.indexOf(clusterNamespace),
      1,
    );

    this.refetch();
  }

  trackByFn(_index: number, item: Namespace) {
    return item.kubernetes.metadata.uid;
  }

  pageChanged(pageEvent: { pageIndex: number; pageSize: number }) {
    this.paginationDataWrapper.setPageNo(++pageEvent.pageIndex);
    this.paginationDataWrapper.setPageSize(pageEvent.pageSize);
  }

  private refetch() {
    return this.paginationDataWrapper.refetch();
  }

  private resetPollingTimer() {
    clearTimeout(this.pollingTimer);
    if (this.destroyed) {
      return;
    }
    const hasUnActiveNamespaces = this.clusterNamespaces.find(
      clusterNamespace => clusterNamespace.kubernetes.status.phase !== 'Active',
    );
    const waitTime = hasUnActiveNamespaces ? 5000 : 10000;
    this.pollingTimer = window.setTimeout(() => this.refetch(), waitTime);
  }
}
