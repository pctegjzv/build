import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { safeDump, safeLoad } from 'js-yaml';
import { cloneDeep } from 'lodash';
import { Observable, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import {
  ClusterNamespaceService,
  ResourceQuota,
} from 'app2/features/cluster/namespace/cluster-namespace.service';
import { formatCommonUnit, formatNumUnit } from 'app2/features/cluster/utils';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

const COMMON_FIELDS = {
  apiVersion: 'v1',
  kind: 'ResourceQuota',
};

const QUOTA_NAME_REGEXP = /^[a-z0-9]([-a-z0-9]*[a-z0-9])?(\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*$/;

@Component({
  templateUrl: './cluster-namespace-edit-quota.component.html',
  styleUrls: ['./cluster-namespace-edit-quota.component.scss'],
})
export class ClusterNamespaceEditQuotaComponent {
  uiMode: boolean;

  clusterId: string;
  namespace: string;
  quotaName: string;
  yaml: string;

  loading: boolean;
  submitting: boolean;

  resourceQuota: ResourceQuota['kubernetes'];

  clusterName$: Observable<string> = this.region.regions$.pipe(
    map(regions => {
      const region =
        regions && regions.find(region => region.id === this.clusterId);
      return region && region.name;
    }),
  );

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private clusterNamespaceService: ClusterNamespaceService,
    private errorsToast: ErrorsToastService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private region: RegionService,
  ) {
    combineLatest(this.route.paramMap, this.route.queryParamMap).subscribe(
      async ([params, queryParams]) => {
        const mode = queryParams.get('mode');
        const clusterId = params.get('clusterId');
        const namespace = params.get('namespace');
        this.quotaName = params.get('quotaName');

        if (clusterId !== this.clusterId || namespace !== this.namespace) {
          this.clusterId = clusterId;
          this.namespace = namespace;
        }

        if (this.quotaName) {
          this.loading = true;

          this.uiMode = this.quotaName === 'default';

          const {
            kubernetes,
          } = await this.clusterNamespaceService.getResourceQuota({
            cluster_id: this.clusterId,
            namespace,
            quota_name: this.quotaName,
          });

          if (this.uiMode) {
            if (!kubernetes.spec) {
              kubernetes.spec = {} as any;
            }

            if (!kubernetes.spec.hard) {
              kubernetes.spec.hard = {} as any;
            }

            const { hard } = kubernetes.spec;
            Object.assign(hard, {
              pods: hard.pods && formatNumUnit(hard.pods),
              persistentvolumeclaims:
                hard.persistentvolumeclaims &&
                formatNumUnit(hard.persistentvolumeclaims),
              ['requests.cpu']:
                hard['requests.cpu'] && formatNumUnit(hard['requests.cpu']),
              ['requests.memory']:
                hard['requests.memory'] &&
                formatCommonUnit(hard['requests.memory']),
              ['requests.storage']:
                hard['requests.storage'] &&
                formatCommonUnit(hard['requests.storage']),
            });
          }

          this.resourceQuota = kubernetes;

          if (!this.uiMode) {
            this.yaml = safeDump(kubernetes);
          }
        } else {
          this.uiMode = mode === 'UI';

          if (this.uiMode) {
            this.resourceQuota = {
              ...COMMON_FIELDS,
              metadata: {
                name: 'default',
                namespace,
              },
              spec: {
                hard: {},
              },
            } as ResourceQuota['kubernetes'];
          } else {
            this.yaml = safeDump({
              metadata: {
                name: '',
                namespace,
              },
            });
          }
        }

        this.loading = false;
      },
    );
  }

  yamlChanged(yaml: string) {
    if (typeof yaml !== 'string') {
      return;
    }
    this.yaml = yaml;
  }

  cancel() {
    this.location.back();
  }

  async confirm() {
    try {
      this.submitting = true;

      const errorMessage = this.quotaName
        ? await this.update()
        : await this.create();

      if (errorMessage) {
        this.auiNotificationService.error(this.translate.get(errorMessage));
      } else {
        this.router.navigate([
          '/cluster/namespace/detail',
          this.clusterId,
          this.namespace,
        ]);
      }
    } catch (e) {
      this.errorsToast.error(e);
    }

    this.submitting = false;
  }

  formatQuota() {
    const resourceQuota = cloneDeep(this.resourceQuota);

    const {
      hard: { ['requests.memory']: memory, ['requests.storage']: storage },
    } = this.resourceQuota.spec;
    const { hard } = resourceQuota.spec;

    Object.assign(hard, {
      ['requests.memory']: memory ? memory + 'G' : undefined,
      ['requests.storage']: storage ? storage + 'G' : undefined,
    });

    return resourceQuota;
  }

  async create(): Promise<string> {
    let resourceQuota: ResourceQuota['kubernetes'];

    if (this.uiMode) {
      resourceQuota = this.formatQuota();
    } else {
      let info;

      try {
        info = safeLoad(this.yaml) as ResourceQuota['kubernetes'];
      } catch (e) {
        return 'invalid_yaml';
      }

      const quotaName = info.metadata.name;

      if (quotaName === 'default' || !QUOTA_NAME_REGEXP.test(quotaName)) {
        return 'invalid_quota_name';
      }

      info.metadata.namespace = this.namespace;

      resourceQuota = info;
    }

    await this.clusterNamespaceService.createResourceQuota(
      this.clusterId,
      resourceQuota,
    );
  }

  async update(): Promise<string> {
    let resourceQuota: ResourceQuota['kubernetes'];

    if (this.uiMode) {
      resourceQuota = this.formatQuota();
    } else {
      let info;

      try {
        info = safeLoad(this.yaml) as ResourceQuota['kubernetes'];
      } catch (e) {
        return 'invalid_yaml';
      }

      Object.assign(info.metadata, {
        name: this.quotaName,
        namespace: this.namespace,
      });

      resourceQuota = info;
    }

    await this.clusterNamespaceService.updateResourceQuota(
      this.clusterId,
      resourceQuota,
    );
  }
}
