import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import $ from 'jquery';
import { safeDump } from 'js-yaml';
import { pick } from 'lodash';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { ResourceQuota } from 'app2/features/cluster/namespace/cluster-namespace.service';
import { FloatingPageService } from 'app2/layout/floating-page.service';
import { Namespace } from 'app2/shared/services/features/namespace.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';

@Component({
  templateUrl: './cluster-namespace-view-quota.component.html',
  styleUrls: ['./cluster-namespace-view-quota.component.scss'],
})
export class ClusterNamespaceViewQuotaComponent implements OnInit, OnDestroy {
  clusterName$: Observable<string> = this.region.regions$.pipe(
    map(regions => {
      const region =
        regions && regions.find(region => region.id === this.data.clusterId);
      return region && region.name;
    }),
  );
  clusterNamespace: Namespace;
  resourceQuota: ResourceQuota;

  quotaInfo: string;

  floatingPageOptionSubscription: Subscription;

  constructor(
    private modal: ModalService,
    private region: RegionService,
    @Inject(MODAL_DATA)
    private data: {
      clusterNamespace: Namespace;
      resourceQuota: ResourceQuota;
      clusterId: string;
    },
    private floatingPage: FloatingPageService,
  ) {}

  ngOnInit() {
    this.clusterNamespace = this.data.clusterNamespace;
    this.resourceQuota = this.data.resourceQuota;

    this.quotaInfo = safeDump(
      pick(
        this.resourceQuota.kubernetes,
        'apiVersion',
        'kind',
        'metadata',
        'spec',
      ),
    );

    const $modals = $('.ui.modals');

    this.floatingPageOptionSubscription = this.floatingPage.option$.subscribe(
      option => {
        if (option) {
          $modals.removeClass('visible').hide();
        } else {
          $modals.addClass('visible').show();
        }
      },
    );
  }

  ngOnDestroy() {
    this.floatingPageOptionSubscription.unsubscribe();
  }

  closeModal() {
    this.modal.closeAll();
  }
}
