import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, Subscription, combineLatest, interval } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  first,
  map,
  pluck,
  publishReplay,
  refCount,
  switchMap,
  tap,
} from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { observablePluck } from 'app2/core/utils';
import { DeleteNodeComponent } from 'app2/features/cluster/detail-page/delete-node/delete-node.component';
import { NodeListComponent } from 'app2/features/cluster/detail-page/node-list/node-list.component';
import { UpdateNodeLabelsComponent } from 'app2/features/cluster/detail-page/update-node-labels/update-node-labels.component';
import { nodeStatusButtonDisplayExpr } from 'app2/features/cluster/utils';
import {
  ContainerManagers,
  Node,
  NodeState,
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import * as nodeActions from 'app2/state-store/node/actions';
import * as fromNode from 'app2/state-store/node/reducers';
import { AppState } from 'app2/state-store/reducers';
import * as regionActions from 'app2/state-store/region/actions';
import * as fromRegion from 'app2/state-store/region/reducers';
import { Entities } from 'app2/state-store/region/reducers';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-node-detail-page',
  templateUrl: 'node-detail-page.component.html',
  styleUrls: ['node-detail-page.component.scss'],
})
export class NodeDetailPageComponent implements OnInit, OnDestroy {
  routeParams$ = this.route.paramMap.pipe(
    pluck<ParamMap, { clusterName: string; privateIp: string }>('params'),
    tap(params => {
      this.regionService.setRegionByName(params.clusterName);
      this.store.dispatch(new regionActions.GetByName(params.clusterName));
      this.store.dispatch(
        new nodeActions.GetByIp({
          regionName: params.clusterName,
          privateIp: params.privateIp,
        }),
      );
    }),
    publishReplay(1),
    refCount(),
  );

  clusterDetail$ = combineLatest(
    this.store.select(fromRegion.selectEntities),
    this.routeParams$.pipe(pluck('clusterName')),
  ).pipe(
    map(([entities, name]: [Entities, string]) => entities[name]),
    filter(data => !!data),
    publishReplay(1),
    refCount(),
  );

  nodeDetail$ = combineLatest(
    this.store.select(fromNode.selectFeature),
    this.routeParams$,
  ).pipe(
    map(
      ([state, params]) =>
        state[params.clusterName] &&
        state[params.clusterName].entities[params.privateIp],
    ),
    filter(data => !!data),
    publishReplay(1),
    refCount(),
  ) as Observable<Node>;

  nodeState$ = this.nodeDetail$.pipe(
    pluck<Node, NodeState>('state'),
    publishReplay(1),
    refCount(),
  );

  isK8s$ = this.clusterDetail$.pipe(
    map(cluster => cluster.container_manager === ContainerManagers.Kubernetes),
    publishReplay(1),
    refCount(),
  );

  isAutoCreate$ = this.nodeDetail$.pipe(
    map(
      node =>
        node.attr &&
        (node.attr.source === 'api-create' || node.attr.source === 'scale-up'),
    ),
    publishReplay(1),
    refCount(),
  );

  private permissionCache: { [key: string]: Observable<boolean> } = {};

  regionBadgeOption = {
    onChange: (region: Region) => {
      this.router.navigate(['/cluster/detail', region.name]);
    },
    filterFunction: () => true,
  };

  intervalSub: Subscription;

  pluck = observablePluck;

  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute,
    private regionService: RegionService,
    private roleUtil: RoleUtilitiesService,
    private translateService: TranslateService,
    private modalService: ModalService,
    private router: Router,
    private auiNotificationService: NotificationService,
  ) {}

  ngOnInit() {
    this.intervalSub = interval(30 * 1000)
      .pipe(switchMap(() => this.routeParams$.pipe(first())))
      .subscribe(params => {
        this.store.dispatch(
          new nodeActions.GetByIp({
            regionName: params.clusterName,
            privateIp: params.privateIp,
          }),
        );
      });
  }

  ngOnDestroy() {
    this.intervalSub.unsubscribe();
  }

  couldAction(action: string) {
    if (!this.permissionCache[action]) {
      this.permissionCache[action] = this.nodeDetail$.pipe(
        map(node => {
          const hasPermission = this.roleUtil.resourceHasPermission(
            node,
            'cluster_node',
            NodeListComponent.UndeepAction.includes(action) ? 'update' : action,
          );
          return hasPermission && nodeStatusButtonDisplayExpr(node, action);
        }),
        distinctUntilChanged(),
        publishReplay(1),
        refCount(),
      );
    }
    return this.permissionCache[action];
  }

  updateNodeTag() {
    combineLatest(this.routeParams$, this.nodeDetail$)
      .pipe(first())
      .subscribe(([{ clusterName }, node]) => {
        const dialogRef = this.modalService.open(UpdateNodeLabelsComponent, {
          title:
            this.translateService.get('update_node_tags') +
            '(' +
            node.private_ip +
            ')',
          data: {
            regionName: clusterName,
            node,
          },
        });

        dialogRef.componentInstance.close.subscribe((updated: boolean) => {
          dialogRef.close();
          if (updated) {
            this.store.dispatch(
              new nodeActions.GetByIp({
                regionName: clusterName,
                privateIp: node.private_ip,
              }),
            );
          }
        });
      });
  }

  deleteNode() {
    this.routeParams$.pipe(first()).subscribe(({ clusterName, privateIp }) => {
      const dialogRef = this.modalService.open(DeleteNodeComponent, {
        title: this.translateService.get('region_delete_node'),
        data: {
          regionName: clusterName,
          privateIp: privateIp,
        },
      });

      dialogRef.componentInstance.close.subscribe((deleted: boolean) => {
        dialogRef.close();

        if (deleted) {
          this.router.navigate(['/cluster/detail', clusterName]);
        }
      });
    });
  }

  clearNode() {
    this.routeParams$.pipe(first()).subscribe(({ clusterName, privateIp }) => {
      this.modalService
        .confirm({
          title: this.translateService.get('region_empty_node'),
          content: this.translateService.get('region_empty_node_prompt', {
            privateIp,
          }),
        })
        .then(() =>
          this.regionService.updateNodeType(clusterName, privateIp, 'empty'),
        )
        .then(() => {
          this.store.dispatch(
            new nodeActions.GetByIp({
              regionName: clusterName,
              privateIp,
            }),
          );
        })
        .catch(() => {});
    });
  }

  startNode() {
    this.routeParams$.pipe(first()).subscribe(({ clusterName, privateIp }) => {
      this.regionService
        .startNode(clusterName, privateIp)
        .then(() => {
          this.store.dispatch(
            new nodeActions.GetByIp({
              regionName: clusterName,
              privateIp,
            }),
          );
          const msg =
            this.translateService.get('start') +
            ' ' +
            privateIp +
            ' ' +
            this.translateService.get('success');
          this.auiNotificationService.success(msg);
        })
        .catch(() => {});
    });
  }

  stopNode() {
    this.routeParams$.pipe(first()).subscribe(({ clusterName, privateIp }) => {
      this.modalService
        .confirm({
          title: this.translateService.get('stop'),
          content: this.translateService.get('region_stop_node_prompt', {
            privateIp,
          }),
        })
        .then(() => this.regionService.stopNode(clusterName, privateIp))
        .then(() => {
          this.store.dispatch(
            new nodeActions.GetByIp({
              regionName: clusterName,
              privateIp,
            }),
          );
        })
        .catch(() => {});
    });
  }

  deployNode() {
    this.routeParams$.pipe(first()).subscribe(({ clusterName, privateIp }) => {
      this.modalService
        .confirm({
          title: this.translateService.get('update') + ': ' + privateIp,
          content: this.translateService.get('region_node_update_prompt'),
        })
        .then(() =>
          this.regionService.updateNodeType(
            clusterName,
            privateIp,
            'k8s-slave',
          ),
        )
        .then(() => {
          this.store.dispatch(
            new nodeActions.GetByIp({
              regionName: clusterName,
              privateIp,
            }),
          );
        })
        .catch(() => {});
    });
  }

  doAction(action: string) {
    this.routeParams$.pipe(first()).subscribe(({ clusterName, privateIp }) => {
      this.modalService
        .confirm({
          title: this.translateService.get('region_node_' + action),
          content: this.translateService.get(
            `region_node_maintain_${action}_confirm_message`,
          ),
        })
        .then(() =>
          this.regionService.actionNode(clusterName, privateIp, action),
        )
        .then(() => {
          this.store.dispatch(
            new nodeActions.GetByIp({
              regionName: clusterName,
              privateIp,
            }),
          );
        })
        .catch(() => {});
    });
  }
}
