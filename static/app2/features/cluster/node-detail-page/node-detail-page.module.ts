import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PercentCardModule } from 'app2/features/cluster/common/percent-card/percent-card.module';
import { NodeDetailPageComponent } from 'app2/features/cluster/node-detail-page/node-detail-page.component';
import { NodeInfoComponent } from 'app2/features/cluster/node-detail-page/node-info/node-info.component';
import { NodeMonitorComponent } from 'app2/features/cluster/node-detail-page/node-monitor/node-monitor.component';
import { SharedModule } from 'app2/shared/shared.module';

@NgModule({
  imports: [SharedModule, RouterModule, PercentCardModule],
  declarations: [
    NodeDetailPageComponent,
    NodeInfoComponent,
    NodeMonitorComponent,
  ],
  exports: [],
})
export class NodeDetailPageModule {}
