import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { Observable, ReplaySubject, combineLatest, from, interval } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
} from 'rxjs/operators';

import { observablePluck } from 'app2/core/utils';
import {
  ContainerManagers,
  Node,
  NodeStats,
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';

@Component({
  selector: 'rc-node-info',
  templateUrl: './node-info.component.html',
  styleUrls: ['./node-info.component.scss'],
})
export class NodeInfoComponent implements OnChanges, OnInit {
  @Input()
  cluster: Region;
  @Input()
  node: Node;
  @Input()
  polling = true;
  @Input()
  pollingInterval = 30 * 1000;

  clusterName$ = new ReplaySubject<string>(1);
  privateIp$ = new ReplaySubject<string>(1);
  statsParams$ = combineLatest(
    this.clusterName$.pipe(distinctUntilChanged()),
    this.privateIp$.pipe(distinctUntilChanged()),
  ).pipe(
    debounceTime(0),
    map(([clusterName, privateIp]) => ({ clusterName, privateIp })),
  );

  stats$: Observable<NodeStats>;
  podsCount$: Observable<number>;

  pluck = observablePluck;

  constructor(private regionService: RegionService) {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.cluster && changes.cluster.currentValue) {
      this.clusterName$.next(changes.cluster.currentValue.name);
    }
    if (changes.node && changes.node.currentValue) {
      this.privateIp$.next(changes.node.currentValue.private_ip);
    }
  }

  ngOnInit() {
    this.stats$ = combineLatest(
      this.statsParams$,
      this.polling ? interval(this.pollingInterval).pipe(startWith(0)) : null,
    ).pipe(
      map(([params]) => params),
      switchMap(({ clusterName, privateIp }) =>
        from(
          this.regionService.getNodeStats(clusterName, privateIp).catch(() => {
            return null;
          }),
        ),
      ),
      filter(stats => !!stats),
      publishReplay(1),
      refCount(),
    );

    this.podsCount$ = this.stats$.pipe(
      map(
        stats =>
          stats.pods_count !== undefined
            ? stats.pods_count
            : stats.containers_count,
      ),
      publishReplay(1),
      refCount(),
    );
  }

  isK8sContainer() {
    return this.cluster.container_manager === ContainerManagers.Kubernetes;
  }

  isEmptyCluster() {
    return this.cluster.container_manager === ContainerManagers.None;
  }
}
