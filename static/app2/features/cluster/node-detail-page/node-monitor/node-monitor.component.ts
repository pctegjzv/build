import { Component, Input } from '@angular/core';
import { EMPTY, ReplaySubject, combineLatest, from } from 'rxjs';
import {
  catchError,
  debounceTime,
  distinctUntilKeyChanged,
  filter,
  map,
  pluck,
  publishReplay,
  refCount,
  switchMap,
} from 'rxjs/operators';

import { BaseMonitor } from 'app2/features/cluster/detail-page/cluster-monitor/base-monitor';
import {
  Node,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-node-monitor',
  templateUrl: 'node-monitor.component.html',
  styleUrls: ['node-monitor.component.scss'],
})
export class NodeMonitorComponent extends BaseMonitor {
  @Input()
  set node(value: Node) {
    this.node$.next(value);
  }

  node$ = new ReplaySubject<Node>(1);
  update$ = new ReplaySubject<number>(1);
  params$ = combineLatest(
    this.clusterName$,
    this.node$.pipe(
      filter(data => !!data),
      distinctUntilKeyChanged('private_ip'),
    ),
  ).pipe(
    debounceTime(0),
    map(([cluster, node]) => ({
      clusterName: cluster,
      privateIp: node.private_ip,
      nodeType: node.type,
    })),
    publishReplay(1),
    refCount(),
  );
  statsHistory$ = combineLatest(this.params$, this.update$).pipe(
    switchMap(([params, horizon]) => {
      const end_time = new Date().getTime();
      const start_time = end_time - horizon * 1000;
      const period = this.timeHorizonOptions.find(
        item => item.value === horizon,
      ).period;
      return from(
        this.regionService.getNodeStatsHistory(
          params.clusterName,
          params.privateIp,
          {
            node_type: params.nodeType,
            end_time,
            start_time,
            period,
          },
        ),
      ).pipe(catchError(() => EMPTY));
    }),
    publishReplay(1),
    refCount(),
  );

  cpuPercent$ = this.statsHistory$.pipe(
    pluck('cpu_utilization_real'),
    map(res => this.convertData('cpu_utilization_real', res)),
    publishReplay(1),
    refCount(),
  );
  memPercent$ = this.statsHistory$.pipe(
    pluck('mem_utilization_real'),
    map(res => this.convertData('mem_utilization_real', res)),
    publishReplay(1),
    refCount(),
  );
  maxDiskPercent$ = this.statsHistory$.pipe(
    pluck('node-max-disk-percent'),
    map(res => this.convertData('node-max-disk-percent', res)),
    publishReplay(1),
    refCount(),
  );

  constructor(
    translateService: TranslateService,
    regionService: RegionService,
  ) {
    super(translateService, regionService);
  }
}
