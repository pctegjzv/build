import { get, set } from 'lodash';

import {
  Region,
  RegionCreateData,
} from 'app2/shared/services/features/region.service';

export enum CloudNames {
  Private = 'PRIVATE',
  VMware = 'VMWARE',
  Aws = 'AWS',
  Ali = 'ALI',
  Qing = 'QING',
  UCloud = 'UCLOUD',
  QCloud = 'QCLOUD',
  OpenStack = 'OPENSTACK',
  BaiDu = 'BAIDU',
  KingSoft = 'KINGSOFT',
}

export interface CloudField {
  key: string;
  type: string;
  path: string[] | string;
  required?: boolean;
  pattern?: RegExp;
  hidden?: boolean;
  default?: string | number;
  mirrorPath?: string;
  isArray?: boolean;
  desc?: string;
}

export interface CloudConfig {
  name: CloudNames;
  fields: CloudField[];
}

export const CLOUD_CONFIGS: CloudConfig[] = [
  {
    name: CloudNames.Private,
    fields: [],
  },
  {
    name: CloudNames.VMware,
    fields: [
      {
        key: 'address',
        type: 'text',
        required: true,
        pattern: /^(http(s?):\/\/.+)/,
        path: 'attr.cloud.address',
        desc: 'cloud_filed_desc_vmware_address',
      },
      {
        key: 'user',
        type: 'text',
        required: true,
        path: 'features.node.instance.user',
        desc: 'cloud_filed_desc_vmware_user',
      },
      {
        key: 'password',
        type: 'password',
        required: true,
        path: 'features.node.instance.password',
        desc: 'cloud_filed_desc_vmware_password',
      },
      {
        key: 'template_name',
        type: 'text',
        required: true,
        path: 'features.node.instance.template_name',
        desc: 'cloud_filed_desc_vmware_template_name',
      },
      {
        key: 'template_user',
        type: 'text',
        required: true,
        path: 'features.node.instance.template_user',
        desc: 'cloud_filed_desc_vmware_template_user',
      },
      {
        key: 'template_password',
        type: 'password',
        required: true,
        path: 'features.node.instance.template_password',
        desc: 'cloud_filed_desc_vmware_template_password',
      },
    ],
  },
  {
    name: CloudNames.OpenStack,
    fields: [
      {
        key: 'login_url',
        type: 'text',
        required: true,
        path: 'features.node.instance.login_url',
      },
      {
        key: 'api_url',
        type: 'text',
        required: true,
        path: 'features.node.instance.api_url',
      },
      {
        key: 'user_name',
        type: 'text',
        required: true,
        path: 'features.node.instance.user_name',
      },
      {
        key: 'password',
        type: 'text',
        required: true,
        path: 'features.node.instance.password',
      },
      {
        key: 'domain_name',
        type: 'text',
        required: true,
        path: 'features.node.instance.domain_name',
      },
      {
        key: 'image_id',
        type: 'text',
        required: true,
        path: 'features.node.instance.image_id',
      },
      {
        key: 'subnet_id',
        type: 'text',
        required: true,
        path: 'features.node.instance.subnet_id',
      },
    ],
  },
  {
    name: CloudNames.Aws, // Value to be used in API
    fields: [
      {
        key: 'access_key', // the ID of the field. Also will be used in translation
        type: 'text', // field input type
        required: true, // used when creating regions validation stage.

        // The path to the region mode.
        // If the value is an array,
        //   the value will be written to multiple paths at creating stage,
        //   but for read, only the first value will be displayed
        path: [
          'features.service.elb.access_key',
          'features.node.instance.access_key',
        ],
      },
      {
        key: 'secret_access_key',
        type: 'text',
        required: true,
        path: [
          'features.service.elb.secret_access_key',
          'features.node.instance.secret_access_key',
        ],
      },
      {
        key: 'region_id',
        type: 'text',
        required: true,
        path: 'attr.cloud.region_id',
      },
      {
        key: 'subnets',
        type: 'text',
        required: true,
        path: 'features.service.elb.subnets',
        isArray: true,
      },
      {
        key: 'subnet_id',
        type: 'text',
        path: 'features.node.instance.subnet_id',

        // If hidden is true, then this field will not be displayed in the UI
        hidden: true,

        // When creating a region, this value is mirrored from the following path
        mirrorPath: 'features.service.elb.subnets[0]',
      },
      {
        key: 'security_groups',
        type: 'text',
        required: true,
        path: [
          'features.service.elb.security_groups',
          'features.node.instance.security_groups',
        ],
        isArray: true,
      },
      {
        key: 'image_id',
        type: 'text',
        path: 'features.node.instance.image_id',
      },
      {
        key: 'iam_instance_profile',
        type: 'text',
        path: 'features.node.instance.iam_instance_profile',
        default: '',
      },
    ],
  },
  {
    name: CloudNames.Ali,
    fields: [
      {
        key: 'access_key',
        type: 'text',
        required: true,
        path: 'features.service.slb.access_key',
      },
      {
        key: 'secret_access_key',
        type: 'text',
        required: true,
        path: 'features.service.slb.secret_access_key',
      },
      {
        key: 'region_id',
        type: 'text',
        required: true,
        path: 'attr.cloud.region_id',
      },
      {
        key: 'vswitch_id',
        type: 'text',
        path: 'features.service.slb.vswitchid',
      },
    ],
  },
  {
    name: CloudNames.QCloud,
    fields: [
      {
        key: 'secret_key',
        type: 'text',
        required: true,
        path: 'features.service.clb.secret_key',
      },
      {
        key: 'secret_id',
        type: 'text',
        required: true,
        path: 'features.service.clb.secret_id',
      },
      {
        key: 'region_id',
        type: 'text',
        path: 'attr.cloud.region_id',
        required: true,
      },
      {
        key: 'lion_host',
        type: 'text',
        path: 'features.service.clb.lion_host',
        hidden: true,
        default: '',
      },
      {
        key: 'lion_user',
        type: 'text',
        path: 'features.service.clb.lion_user',
        hidden: true,
        default: '',
      },
      {
        key: 'lion_passwd',
        type: 'text',
        path: 'features.service.clb.lion_passwd',
        hidden: true,
        default: '',
      },
    ],
  },
  {
    name: CloudNames.KingSoft,
    fields: [
      {
        key: 'login_url',
        type: 'text',
        required: true,
        path: 'features.node.instance.login_url',
      },
      {
        key: 'nova_url',
        type: 'text',
        required: true,
        path: 'features.node.instance.nova_url',
      },
      {
        key: 'user_name',
        type: 'text',
        required: true,
        path: 'features.node.instance.user_name',
      },
      {
        key: 'password',
        type: 'password',
        required: true,
        path: 'features.node.instance.password',
      },
      {
        key: 'tenant_name',
        type: 'text',
        required: true,
        path: 'features.node.instance.tenant_name',
      },
      {
        key: 'image_id',
        type: 'text',
        required: true,
        path: 'features.node.instance.image_id',
      },
      {
        key: 'subnet_id',
        type: 'text',
        required: true,
        path: 'features.node.instance.subnet_id',
      },
    ],
  },
  {
    name: CloudNames.Qing,
    fields: [
      {
        key: 'access_key',
        type: 'text',
        required: true,
        path: 'features.service.qlb.access_key',
      },
      {
        key: 'secret_access_key',
        type: 'text',
        required: true,
        path: 'features.service.qlb.secret_access_key',
      },
      {
        key: 'region_id',
        type: 'text',
        required: true,
        path: 'attr.cloud.region_id',
      },
    ],
  },
  {
    name: CloudNames.UCloud,
    fields: [
      {
        key: 'public_key',
        type: 'text',
        required: true,
        path: 'features.service.ulb.public_key',
      },
      {
        key: 'private_key',
        type: 'text',
        required: true,
        path: 'features.service.ulb.private_key',
      },
      {
        key: 'project_id',
        type: 'text',
        path: 'features.service.ulb.project_id',
      },
      {
        key: 'bandwidth',
        type: 'number',
        path: 'features.service.ulb.bandwidth',
        default: 0,
      },
    ],
  },
  {
    name: CloudNames.BaiDu,
    fields: [
      {
        key: 'access_key',
        type: 'text',
        required: true,
        path: 'features.service.blb.access_key',
      },
      {
        key: 'secret_access_key',
        type: 'text',
        required: true,
        path: 'features.service.blb.secret_access_key',
      },
      {
        key: 'region_id',
        type: 'text',
        required: true,
        path: 'attr.cloud.region_id',
      },
    ],
  },
];

export function getCloudInfos(
  cluster: Region,
): { label: string; value: string | number }[] {
  const cloudName = cluster.attr.cloud.name.toUpperCase();
  return CLOUD_CONFIGS.find(item => item.name === cloudName)
    .fields.filter(item => !item.hidden && item.type !== 'password')
    .map(item => {
      return {
        label: `region_${item.key}`,
        value: get(
          cluster,
          item.path instanceof Array ? item.path[0] : item.path,
          null,
        ),
      };
    })
    .filter(item => item.value !== null);
}

export function combineSubmitData(
  baseData: RegionCreateData,
  cloudData: { [key: string]: any },
): RegionCreateData {
  const data: RegionCreateData = JSON.parse(JSON.stringify(baseData));
  const fields = CLOUD_CONFIGS.find(
    item => item.name === baseData.attr.cloud.name,
  ).fields;
  fields.sort(item => +!!item.mirrorPath).forEach(field => {
    if (!field.mirrorPath) {
      let value = cloudData[field.key] || field.default;
      if (field.isArray) {
        value = value.split(',').map((item: string) => item.trim());
      }
      setFieldValue(data, field.path, value);
    } else {
      setFieldValue(data, field.path, get(data, field.mirrorPath));
    }
  });
  if (data.features) {
    Object.values(data.features).forEach(value => {
      value.features = Object.keys(value);
    });
  }
  return data;
}

function setFieldValue(data: object, path: string | string[], value: any) {
  if (typeof path === 'string') {
    path = [path];
  }
  path.forEach(path => {
    set(data, path, value);
  });
}
