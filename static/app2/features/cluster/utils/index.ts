import { get } from 'lodash';

import {
  Node,
  NodeState,
  RegionStatus,
} from 'app2/shared/services/features/region.service';

export * from './cloud';
export * from './status';
export * from './unit';

export function parseRegionStateColor(state: RegionStatus): string {
  switch (state) {
    case RegionStatus.Running:
      return 'green';
    case RegionStatus.Warning:
      return 'yellow';
    case RegionStatus.Error:
    case RegionStatus.Critical:
      return 'red';
    case RegionStatus.Deploying:
    case RegionStatus.Preparing:
      return 'blue';
    case RegionStatus.Stopped:
    default:
      return 'gray';
  }
}

export function parseNodeStateColor(state: NodeState) {
  switch (state) {
    case NodeState.Deploying:
    case NodeState.ShuttingDown:
    case NodeState.Stopping:
    case NodeState.Preparing:
    case NodeState.Draining:
      return 'blue';
    case NodeState.Running:
      return 'green';
    case NodeState.Warning:
      return 'yellow';
    case NodeState.Error:
    case NodeState.Critical:
      return 'red';
    default:
      return 'gray';
  }
}

export const CLUSTER_DEPLOY_TEMPLATES: {
  [key: string]: {
    features: string[];
    optionalFeatures: string[];
  };
} = {
  poc: {
    features: [
      'load_balancer', // Will be mapped to haproxy or nginx when submitting
      'exec',
      'volume',
      'chronos',
      'pipeline',
      'registry',
    ],
    optionalFeatures: ['tunnel'],
  },
  prod: {
    features: ['chronos', 'pipeline', 'registry'],
    optionalFeatures: [
      'load_balancer', // Will be mapped to haproxy or nginx when submitting
      'exec',
      'volume',
      'tunnel',
    ],
  },
};

export function nodeStatusButtonDisplayExpr(
  node: Node,
  action: string,
): boolean {
  const scheduleActionState = [
    'DEPLOYING',
    'ERROR',
    'CRITICAL',
    'UNKNOWN',
    'OFFLINE',
  ];
  const scheduleAction = ['cordon', 'uncordon', 'drain'];
  if (
    (node.type === 'EMPTY' || scheduleActionState.includes(node.state)) &&
    scheduleAction.includes(action)
  ) {
    return false;
  }
  const schedulable = get(node, 'attr.schedulable', false);
  switch (action) {
    case 'start':
      return node.state === 'STOPPED';
    case 'stop':
      return (
        ['RUNNING', 'ERROR', 'CRITICAL', 'OFFLINE', 'UNKNOWN'].includes(
          node.state,
        ) && node.type === 'EMPTY'
      );
    case 'clear':
      return (
        (node.type === 'EMPTY' &&
          ['RUNNING', 'ERROR', 'CRITICAL'].includes(node.state)) ||
        (node.type === 'SLAVE' &&
          node.state !== 'DEPLOYING' &&
          node.state !== 'DRAINING')
      );
    case 'deploy':
      return node.type === 'EMPTY' && node.state === 'RUNNING';
    case 'delete':
      return (
        node.type === 'EMPTY' &&
        node.state !== 'DEPLOYING' &&
        node.state !== 'PREPARING'
      );
    case 'tag':
      return (
        (node.type === 'SLAVE' || node.type === 'SYSLAVE') &&
        node.state === 'RUNNING'
      );
    case 'drain':
      return (
        node.type === 'SLAVE' &&
        (node.state === 'RUNNING' || node.state === 'DRAINING')
      );
    case 'uncordon':
      return (node.type === 'SLAVE' || node.type === 'SYSLAVE') && !schedulable;
    case 'cordon':
      return (node.type === 'SLAVE' || node.type === 'SYSLAVE') && schedulable;
  }
  return false;
}
