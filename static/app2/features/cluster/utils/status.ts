import { RegionFeature } from 'app2/shared/services/features/region.service';

export type RegionFeatureStatus =
  | 'not_added'
  | 'running'
  | 'error'
  | 'deploying'
  | 'docked'
  | 'unknown';

export const getRegionFeatureStatus = (
  regionFeature: RegionFeature,
): RegionFeatureStatus => {
  if (!regionFeature || !regionFeature.config || !regionFeature.config.type) {
    return 'not_added';
  }

  if (regionFeature.config.type !== 'official') {
    return 'docked';
  }

  if (!regionFeature.application_info.status) {
    return 'unknown';
  }

  if (
    ['Running', 'Deploying'].includes(regionFeature.application_info.status)
  ) {
    return regionFeature.application_info.status.toLowerCase() as RegionFeatureStatus;
  }

  return 'error';
};
