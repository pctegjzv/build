// could be 500m, 0.5, 5k, return number
export const formatNumUnit = (value: string) => {
  let number = parseFloat(value);

  if (number === 0) {
    return 0;
  }

  if (!number) {
    return null;
  }

  switch (value[value.length - 1]) {
    case 'm':
      number = number / 1000;
      break;
    case 'k':
      number = number * 1000;
      break;
  }

  return number;
};

function formatCommonUnit(value: string): string;
function formatCommonUnit(value: string, fixed: boolean): string | number;
function formatCommonUnit(value: string = '', fixed?: boolean) {
  value = value.replace(/i$/, '');

  let number = parseFloat(value);

  if (!number) {
    return 0;
  }

  switch (value[value.length - 1]) {
    case 'M':
      number = number / 1024;
      break;
    case 'm':
      number = number / 1000 / 1024 / 1024 / 1024;
      break;
    case 'G':
      break;
    case 'K':
    case 'k':
      number = number / 1024 / 1024;
      break;
    case 'T':
      number = number * 1024;
      break;
    case 'P':
      number = number * 1024 * 1024;
      break;
    case 'E':
      number = number * 1024 * 1024 * 1024;
      break;
    default:
      number = number / 1024 / 1024 / 1024;
      break;
  }

  if (!fixed) {
    return number;
  }

  return number
    .toFixed(2)
    .replace(/0+$/, '')
    .replace(/\.$/, '');
}

export { formatCommonUnit };
