import { Component } from '@angular/core';

import { ModalService } from 'app2/shared/services/modal/modal.service';

@Component({
  templateUrl: './configmap-create-select.component.html',
  styleUrls: ['./configmap-create-select.component.scss'],
})
export class ConfigMapCreateSelectComponent {
  constructor(private modal: ModalService) {}

  close() {
    this.modal.closeAll();
  }
}
