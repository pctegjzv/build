import { Location } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import * as jsyaml from 'js-yaml';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import {
  ConfigmapService,
  K8sConfigMap,
} from 'app2/shared/services/features/configmap.service';
import {
  NamespaceOption,
  NamespaceService,
} from 'app2/shared/services/features/namespace.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { LoggerUtilitiesService } from 'app2/shared/services/utility/logger.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './configmap-create-yaml.component.html',
  styleUrls: ['./configmap-create-yaml.component.scss'],
})
export class ConfigmapCreateYamlComponent implements OnInit, OnDestroy {
  private regionSubscription: Subscription;

  namespaceOptions: NamespaceOption[];
  clusterId: string;
  configmapPayload: K8sConfigMap['kubernetes'];
  configmapYaml: string;
  submitting = false;
  description: string;

  @ViewChild('appCreateForm')
  form: NgForm;

  get descriptionKey() {
    return `resource.${this.env.label_base_domain}/description`;
  }

  constructor(
    private namespaceService: NamespaceService,
    private regionService: RegionService,
    private configmapService: ConfigmapService,
    private modalService: ModalService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private logger: LoggerUtilitiesService,
    private router: Router,
    private location: Location,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {
    this.configmapPayload = {
      metadata: {
        annotations: {},
      },
    } as K8sConfigMap['kubernetes'];
  }

  ngOnInit() {
    this.regionSubscription = this.regionService.region$
      .pipe(filter(r => !!r))
      .subscribe(async region => {
        this.clusterId = region.id;
        this.namespaceOptions = await this.namespaceService.getNamespaceOptions(
          region.id,
        );
        if (this.namespaceOptions.length) {
          this.configmapPayload.metadata.namespace = this.namespaceOptions[0].name;
        }
      });
  }

  ngOnDestroy() {
    this.regionSubscription.unsubscribe();
  }

  onNamespaceOptionChange(option: NamespaceOption) {
    this.configmapPayload.metadata.namespace = option.name;
  }

  async createConfigmap() {
    this.submitting = true;
    if (this.description) {
      this.configmapPayload.metadata.annotations =
        this.configmapPayload.metadata.annotations || ({} as any);
      this.configmapPayload.metadata.annotations[
        this.descriptionKey
      ] = this.description;
    }
    try {
      await this.configmapService.createK8sConfigmap(
        this.clusterId,
        this.configmapPayload,
      );
      this.router.navigate([
        'k8s_configmap/detail',
        this.clusterId,
        this.configmapPayload.metadata.namespace,
        this.configmapPayload.metadata.name,
      ]);
    } catch ({ errors }) {
      this.auiNotificationService.error(errors[0].message);
    }
    this.submitting = false;
  }

  async confirm() {
    if (this.form.invalid) {
      return;
    }

    try {
      const yaml: any = jsyaml.safeLoad(this.configmapYaml);
      if ('metadata' in yaml) {
        yaml.metadata = Object.assign(yaml.metadata || {}, {
          namespace: this.configmapPayload.metadata.namespace,
          name: this.configmapPayload.metadata.name,
        });
      }
      Object.assign(this.configmapPayload, yaml);
    } catch (e) {
      this.logger.error(e);
      return;
    }

    try {
      await this.modalService.confirm({
        title: this.translateService.get('create'),
        content: this.translateService.get(
          'configmap_service_create_configmap_confirm',
          {
            configmap_name: this.configmapPayload.metadata.name,
          },
        ),
      });
    } catch (e) {
      return;
    }

    this.createConfigmap();
  }

  cancel() {
    this.location.back();
  }
}
