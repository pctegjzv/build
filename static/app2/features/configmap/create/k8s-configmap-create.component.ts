import { Location } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import {
  ConfigmapService,
  K8sConfigMap,
} from 'app2/shared/services/features/configmap.service';
import {
  NamespaceOption,
  NamespaceService,
} from 'app2/shared/services/features/namespace.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

interface ConfigMapItem {
  key: string;
  value: string;
}

@Component({
  templateUrl: './k8s-configmap-create.component.html',
  styleUrls: ['./k8s-configmap-create.component.scss'],
})
export class K8sConfigmapCreateComponent implements OnInit, OnDestroy {
  private regionSubscription: Subscription;

  namespaceOptions: NamespaceOption[];
  submitting = false;

  clusterId: string;
  configMap: K8sConfigMap = {
    kubernetes: {
      apiVersion: 'v1',
      kind: 'ConfigMap',
      metadata: {
        annotations: {},
      },
    },
  } as K8sConfigMap;
  configItems: ConfigMapItem[] = [];

  @ViewChild('appCreateForm')
  form: NgForm;

  get descriptionKey() {
    return `resource.${this.env.label_base_domain}/description`;
  }

  constructor(
    private namespaceService: NamespaceService,
    private regionService: RegionService,
    private configmapService: ConfigmapService,
    private modalService: ModalService,
    private translate: TranslateService,
    private router: Router,
    private location: Location,
    private errorsToast: ErrorsToastService,
    private auiNotificationService: NotificationService,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {}

  ngOnInit() {
    this.regionSubscription = this.regionService.region$
      .pipe(filter(r => !!r))
      .subscribe(async region => {
        this.namespaceOptions = await this.namespaceService.getNamespaceOptions(
          (this.clusterId = region.id),
        );
        if (this.namespaceOptions.length) {
          this.configMap.kubernetes.metadata.namespace = this.namespaceOptions[0].name;
        }
      });
  }

  ngOnDestroy() {
    this.regionSubscription.unsubscribe();
  }

  onNamespaceOptionChange(namespace: string) {
    this.configMap.kubernetes.metadata.namespace = namespace;
  }

  addConfigItem() {
    this.configItems.push({} as ConfigMapItem);
  }

  deleteConfigItem(index: number) {
    this.configItems.splice(index, 1);
  }

  async confirm() {
    if (this.form.invalid) {
      return;
    }

    const keys = this.configItems.map(({ key }) => key);
    const duplicateKeys: string[] = [];

    keys.forEach((key, index) => {
      if (!duplicateKeys.includes(key) && index !== keys.lastIndexOf(key)) {
        duplicateKeys.push(key);
      }
    });

    if (duplicateKeys.length) {
      this.auiNotificationService.error(
        this.translate.get('configmap_duplicate_keys') +
          duplicateKeys.join(','),
      );
      return;
    }

    if (this.configItems.length) {
      this.configMap.kubernetes.data = {};
      this.configItems.forEach(configItem => {
        this.configMap.kubernetes.data[configItem.key] = configItem.value || '';
      });
    }

    try {
      await this.modalService.confirm({
        title: this.translate.get('create'),
        content: this.translate.get(
          'configmap_service_create_configmap_confirm',
          {
            configmap_name: this.configMap.kubernetes.metadata.name,
          },
        ),
      });
    } catch (e) {
      return;
    }

    this.submitting = true;

    try {
      await this.configmapService.createK8sConfigmap(
        this.clusterId,
        this.configMap.kubernetes,
      );
      this.router.navigate([
        'k8s_configmap/detail',
        this.clusterId,
        this.configMap.kubernetes.metadata.namespace,
        this.configMap.kubernetes.metadata.name,
      ]);
    } catch (e) {
      this.errorsToast.error(e);
    }

    this.submitting = false;
  }

  cancel() {
    this.location.back();
  }
}
