import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { K8sConfigmapYamlDialogComponent } from 'app2/features/configmap/dialog/k8s-configmap-yaml-dialog.component';
import {
  ConfigmapService,
  K8sConfigMap,
} from 'app2/shared/services/features/configmap.service';
import { ConfigmapUtilitiesService } from 'app2/shared/services/features/configmap.utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ModalMode } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';
import { cloneDeep } from 'lodash';
import { first } from 'rxjs/operators';

@Component({
  templateUrl: './k8s-configmap-detail.component.html',
  styleUrls: ['./k8s-configmap-detail.component.scss'],
})
export class K8sConfigmapDetailComponent implements OnInit {
  private _loading: boolean;
  private _loadError: any;

  private clusterId: string;
  private namespace: string;
  private name: string;
  configmapData: K8sConfigMap;
  initialized = false;
  referenced_list: any[] = [];

  get descriptionKey() {
    return `resource.${this.env.label_base_domain}/description`;
  }

  constructor(
    private modalService: ModalService,
    private translateService: TranslateService,
    private route: ActivatedRoute,
    private configmapService: ConfigmapService,
    private auiNotificationService: NotificationService,
    public configmapUtilities: ConfigmapUtilitiesService,
    private router: Router,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {}

  async ngOnInit() {
    this.route.paramMap.subscribe(async params => {
      this.clusterId = params.get('clusterId');
      this.namespace = params.get('namespace');
      this.name = params.get('name');
      await this.refetch();
      this.initialized = true;
    });
  }

  async refetch() {
    this._loading = true;
    try {
      this.configmapData = await this.configmapService.getK8sConfigmap({
        clusterId: this.clusterId,
        namespace: this.namespace,
        name: this.name,
      });
      this._loadError = null;
    } catch ({ status, errors }) {
      this._loadError = errors;
      if (status === 403) {
        // TODO: back to pervious state or parent state
        this.auiNotificationService.warning(
          this.translateService.get('permission_denied'),
        );
        return this.router.navigateByUrl('k8s_configmap');
      }
    }
    this._loading = false;
  }

  showYamlDialog(title: string, addConfig = false, readOnly = false) {
    const modelRef = this.modalService.open(K8sConfigmapYamlDialogComponent, {
      title: this.translateService.get(title),
      mode: ModalMode.RIGHT_SLIDER,
      width: 900,
    });
    modelRef.componentInstance.addConfiguration = addConfig;
    modelRef.componentInstance.configmapData = cloneDeep(this.configmapData);
    modelRef.componentInstance.readOnly = readOnly;
    modelRef.componentInstance.clusterId = this.clusterId;
    modelRef.componentInstance.finished.pipe(first()).subscribe(() => {
      this.refetch();
      modelRef.close();
    });
  }

  get empty() {
    return !this.configmapData;
  }

  get keyEmpty() {
    if (this.configmapData) {
      return !this.configmapData.hasOwnProperty('referenced_by');
    }
    return false;
  }

  get loading() {
    return this._loading;
  }

  get loadError() {
    return this._loadError;
  }

  handleData() {
    let data: any;
    if (this.configmapData) {
      data = Object.entries(this.configmapData.kubernetes.data);
      data = data.sort((kvArr: any, kvArr_next: any) => {
        if (kvArr[0] < kvArr_next[0]) {
          return -1;
        }
        if (kvArr[0] > kvArr_next[0]) {
          return 1;
        }
        return 0;
      });
    }
    return data;
  }

  showKeyReferenced(list: any) {
    this.referenced_list = list || [];
  }

  async deleteConfigmap() {
    try {
      await this.configmapUtilities.deleteConfigmap(
        this.clusterId,
        this.configmapData,
      );
      return this.router.navigateByUrl('k8s_configmap');
    } catch (rejection) {
      if (rejection && rejection.errors) {
        this.auiNotificationService.error(rejection.errors[0].message);
      }
    }
  }

  redirctReferencedResource(item: any) {
    if (item.type === 'AlaudaService') {
      return this.router.navigateByUrl(`k8s_app/detail/component/${item.uuid}`);
    } else if (item.type === 'AlaudaApp') {
      return this.router.navigateByUrl(`k8s_app/detail/${item.uuid}`);
    }
  }

  async doRefetch() {
    await this.refetch();
  }
}
