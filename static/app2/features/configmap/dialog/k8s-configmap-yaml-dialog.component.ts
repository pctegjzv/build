import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { NotificationService } from 'alauda-ui';
import { editor } from 'monaco-editor';

import {
  ConfigmapService,
  K8sConfigMap,
  MirrorConfigmap,
} from 'app2/shared/services/features/configmap.service';
import { ConfigmapUtilitiesService } from 'app2/shared/services/features/configmap.utilities.service';
import { DefaultErrorMapperService } from 'app2/shared/services/form/default-error-mapper.service';
import { ErrorMapper } from 'app2/shared/types';
import { TranslateService } from 'app2/translate/translate.service';
import * as jsyaml from 'js-yaml';

@Component({
  selector: 'rc-k8s-configmap-yaml-dialog',
  templateUrl: './k8s-configmap-yaml-dialog.component.html',
  styleUrls: ['./k8s-configmap-yaml-dialog.component.scss'],
})
export class K8sConfigmapYamlDialogComponent implements OnInit {
  @Input()
  isKey = false;
  @Input()
  readOnly = false;
  @Input()
  addConfiguration = false;
  @Input()
  configItem = false;
  @Input()
  configmapData: K8sConfigMap;
  @Input()
  key = '';
  @Input()
  value = '';
  @Input()
  params: {
    clusterId: string;
    namespace: string;
    name: string;
    clusterName?: string;
  };
  @Input()
  clusterId: string;
  @Output()
  finished = new EventEmitter<any>();
  @ViewChild(NgForm)
  accountForm: NgForm;

  submitting = false;
  providerProperties: { [key: string]: any };
  errorMapper: ErrorMapper;
  originMirrorConfigmaps: MirrorConfigmap[] = [];
  selectCluster: string[];
  model: any;
  configmapYaml = '';
  originalValue = '';
  codeEditorOptions: editor.IEditorConstructionOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: true,
  };

  constructor(
    private translate: TranslateService,
    private defaultErrorMapper: DefaultErrorMapperService,
    public configmapUtilities: ConfigmapUtilitiesService,
    private auiNotificationService: NotificationService,
    private configmapService: ConfigmapService,
  ) {}

  async ngOnInit() {
    this.codeEditorOptions.readOnly = this.readOnly;
    this.model = {
      key: '',
      value: '',
    };
    this.errorMapper = {
      map: this.errorMap.bind(this),
    };
    if (this.isKey) {
      this.configmapYaml = this.value;
    } else if (this.params) {
      this.clusterId = this.clusterId || this.params.clusterId;
      try {
        this.configmapData = await this.configmapService.getK8sConfigmap(
          this.params,
        );
        if (this.params.clusterName) {
          this.getMirrorConfigmaps();
        }
        this.originalValue = this.configmapYaml = this.getUpdateYmal();
      } catch (errors) {
        this.auiNotificationService.error(errors.message);
      }
    } else {
      this.originalValue = this.configmapYaml = this.getUpdateYmal();
    }
  }

  async handleData() {
    this.submitting = true;
    if (this.addConfiguration) {
      if (!this.configmapData.kubernetes.data) {
        this.configmapData.kubernetes['data'] = {};
      }
      this.configmapData.kubernetes.data[this.model.key] = this.model.value;
      try {
        const response = await this.configmapUtilities.updateConfigmapAddKey(
          this.clusterId,
          this.configmapData,
          this.model.key,
        );
        this.complete(response);
        return response;
      } catch ({ errors }) {
        if (errors) {
          this.auiNotificationService.error(errors[0].message);
        }
      }
    } else {
      if (this.isKey) {
        this.configmapData.kubernetes.data[this.key] = this.configmapYaml;
        try {
          const response = await this.configmapUtilities.updateConfigmapUpdateKey(
            this.clusterId,
            this.configmapData,
            this.key,
          );
          this.complete(response);
          return response;
        } catch ({ errors }) {
          if (errors) {
            this.auiNotificationService.error(errors[0].message);
          }
        }
      } else {
        try {
          this.configmapData.kubernetes = jsyaml.load(
            this.configmapYaml,
          ) as K8sConfigMap['kubernetes'];
        } catch (error) {
          this.submitting = false;
          this.auiNotificationService.error(error.message);
          return false;
        }
        if (
          !this.configmapData.kubernetes.metadata ||
          !this.configmapData.kubernetes.metadata.name ||
          !this.configmapData.kubernetes.metadata.namespace
        ) {
          this.auiNotificationService.error(
            this.translate.get('yaml_format_hint'),
          );
          this.submitting = false;
          return false;
        }
        try {
          const response = await this.configmapUtilities.updateConfigmap(
            this.clusterId,
            this.configmapData,
            this.selectCluster,
          );
          this.complete(response);
          return response;
        } catch ({ errors }) {
          if (errors) {
            this.auiNotificationService.error(errors[0].message);
          }
        }
      }
    }
    this.submitting = false;
  }

  confirm() {
    if (this.readOnly) {
      this.complete();
    } else {
      this.handleData();
    }
  }

  cancel() {
    this.complete();
  }

  private complete(res?: any) {
    this.finished.next(res);
    this.finished.complete();
  }

  private errorMap(key: string, error: any): string {
    switch (key) {
      case 'pattern':
        return this.translate.get('cloud_name_pattern_invalid');
      default:
        return this.defaultErrorMapper.map(key, error);
    }
  }

  getUpdateYmal() {
    const kubernetes = this.configmapData.kubernetes;
    return jsyaml.safeDump(kubernetes);
  }

  async getMirrorConfigmaps() {
    const mirrorConfigmaps = await this.configmapService.getMirrorConfigmaps(
      this.params.clusterName,
      this.params.namespace,
      this.params.name,
    );
    this.originMirrorConfigmaps = mirrorConfigmaps;
    this.selectCluster = mirrorConfigmaps.map((item: MirrorConfigmap) => {
      return item.cluster_name;
    });
  }
}
