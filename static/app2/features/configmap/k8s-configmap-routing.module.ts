import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfigmapCreateYamlComponent } from 'app2/features/configmap/create/configmap-create-yaml.component';
import { K8sConfigmapCreateComponent } from 'app2/features/configmap/create/k8s-configmap-create.component';
import { K8sConfigmapDetailComponent } from 'app2/features/configmap/detail/k8s-configmap-detail.component';
import { K8sConfigmapListComponent } from 'app2/features/configmap/list/k8s-configmap-list.component';

const k8sConfigmapRoutes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'list',
    component: K8sConfigmapListComponent,
  },
  {
    path: 'create',
    component: K8sConfigmapCreateComponent,
  },
  {
    path: 'create-yaml',
    component: ConfigmapCreateYamlComponent,
  },
  {
    path: 'detail/:clusterId/:namespace/:name',
    component: K8sConfigmapDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(k8sConfigmapRoutes)],
  exports: [RouterModule],
  declarations: [],
})
export class K8sConfigmapRoutingModule {}
