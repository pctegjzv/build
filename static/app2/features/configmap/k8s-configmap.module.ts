import { NgModule } from '@angular/core';

import { ConfigMapCreateSelectComponent } from 'app2/features/configmap/create/configmap-create-select.component';
import { ConfigmapCreateYamlComponent } from 'app2/features/configmap/create/configmap-create-yaml.component';
import { K8sConfigmapCreateComponent } from 'app2/features/configmap/create/k8s-configmap-create.component';
import { K8sConfigmapDetailComponent } from 'app2/features/configmap/detail/k8s-configmap-detail.component';
import { K8sConfigmapYamlDialogComponent } from 'app2/features/configmap/dialog/k8s-configmap-yaml-dialog.component';
import { K8sConfigmapRoutingModule } from 'app2/features/configmap/k8s-configmap-routing.module';
import { K8sConfigmapKeyValueListComponent } from 'app2/features/configmap/key-value-list/k8s-configmap-key-value-list.component';
import { K8sConfigmapListComponent } from 'app2/features/configmap/list/k8s-configmap-list.component';

import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [SharedModule, K8sConfigmapRoutingModule],
  declarations: [
    K8sConfigmapListComponent,
    K8sConfigmapDetailComponent,
    K8sConfigmapYamlDialogComponent,
    K8sConfigmapCreateComponent,
    K8sConfigmapKeyValueListComponent,
    ConfigMapCreateSelectComponent,
    ConfigmapCreateYamlComponent,
  ],
  entryComponents: [
    K8sConfigmapYamlDialogComponent,
    ConfigMapCreateSelectComponent,
  ],
})
export class K8sConfigMapModule {}
