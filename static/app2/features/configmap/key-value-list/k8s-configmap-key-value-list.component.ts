import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';
import { NotificationService } from 'alauda-ui';
import { VendorCustomerService } from 'app2/core/customization/vendor-customer.service';
import { CustomModuleName } from 'app2/core/customization/vendor-customer.types';
import { K8sConfigmapYamlDialogComponent } from 'app2/features/configmap/dialog/k8s-configmap-yaml-dialog.component';
import {
  ConfigmapService,
  K8sConfigMap,
} from 'app2/shared/services/features/configmap.service';
import { ConfigmapUtilitiesService } from 'app2/shared/services/features/configmap.utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ModalMode } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';
import * as _ from 'lodash';
import { first } from 'rxjs/operators';

@Component({
  selector: 'rc-k8s-configmap-key-value-list',
  templateUrl: './k8s-configmap-key-value-list.component.html',
  styleUrls: ['./k8s-configmap-key-value-list.component.scss'],
})
export class K8sConfigmapKeyValueListComponent
  implements OnInit, OnDestroy, AfterViewInit {
  @Input()
  set _configmapData(_configmapData: K8sConfigMap) {
    this.configmapData = _configmapData;
    this.keyValueList = this.handleData(_configmapData);
    this.keyValueList_show = this.keyValueList.concat();
  }
  @Input()
  params: {
    clusterId: string;
    namespace: string;
    name: string;
  };
  @Input()
  clusterId: string;
  @Input()
  tooltipType = 'list';
  @Input()
  tooltipTemplate: TemplateRef<any>;
  @Output()
  actionComplete = new EventEmitter<void>();

  SHOW_KEYS_NUM = 10;
  configmapData = {} as K8sConfigMap;
  loading: boolean;
  deleteKey = '';
  referenced_list: any[] = [];
  keyValueList: any[] = [];
  keyValueList_show: any[] = [];
  appUserConfigmap = false;

  constructor(
    public configmapUtilities: ConfigmapUtilitiesService,
    private modalService: ModalService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private configmapService: ConfigmapService,
    private vendorCustomerService: VendorCustomerService,
  ) {}

  ngOnInit() {
    this.appUserConfigmap = this.vendorCustomerService.support(
      CustomModuleName.APP_USER_CONFIGMAP,
    );
    if (this.params) {
      this.clusterId = this.clusterId || this.params.clusterId;
      this.refetch();
    } else {
      this.keyValueList = this.handleData(this.configmapData);
      this.keyValueList_show = this.keyValueList.concat();
    }
  }

  ngAfterViewInit() {}

  ngOnDestroy() {}

  async refetch() {
    this.loading = true;
    try {
      this.configmapData = await this.configmapService.getK8sConfigmap(
        this.params,
      );
      this.keyValueList = this.handleData(this.configmapData);
      this.keyValueList_show = this.keyValueList
        .concat()
        .slice(0, this.SHOW_KEYS_NUM);
    } catch (errors) {
      this.auiNotificationService.error(errors.message);
    }
    this.loading = false;
  }

  handleData(rowData: K8sConfigMap) {
    let data;
    if (rowData) {
      if (!rowData.kubernetes.data) {
        rowData.kubernetes.data = {};
      }
      data = Object.entries(rowData.kubernetes.data);
      data = data.sort((kvArr: any, kvArr_next: any) => {
        if (kvArr[0] < kvArr_next[0]) {
          return -1;
        }
        if (kvArr[0] > kvArr_next[0]) {
          return 1;
        }
        return 0;
      });
    }
    return data;
  }

  iskeyReferencedByResource(rowData: any) {
    if (this.configmapData['key_referenced_by']) {
      if (this.configmapData['key_referenced_by'].hasOwnProperty(rowData[0])) {
        return true;
      }
    }
    return false;
  }

  showUpdateKeyYamlDialog(data: any, readOnly = false) {
    const modelRef = this.modalService.open(K8sConfigmapYamlDialogComponent, {
      title: this.translateService.get('key') + `: ${data[0]}`,
      mode: ModalMode.RIGHT_SLIDER,
      width: 900,
    });
    modelRef.componentInstance.isKey = true;
    modelRef.componentInstance.configmapData = _.cloneDeep(this.configmapData);
    modelRef.componentInstance.key = data[0];
    modelRef.componentInstance.value = data[1];
    modelRef.componentInstance.readOnly = readOnly;
    modelRef.componentInstance.clusterId = this.clusterId;
    modelRef.componentInstance.configItem = true;
    modelRef.componentInstance.finished.pipe(first()).subscribe(() => {
      if (this.params) {
        this.refetch();
      } else {
        this.actionComplete.next();
      }
      modelRef.close();
    });
  }

  showReferencedList(list: any) {
    this.referenced_list = list || [];
  }

  deleteConfigmapKey(rowData: any) {
    this.deleteKey = rowData[0];
    this.deleteConfigmapKeyAction();
  }

  async deleteConfigmapKeyAction() {
    const configmapDataCopy = _.cloneDeep(this.configmapData);
    delete configmapDataCopy.kubernetes.data[this.deleteKey];
    try {
      await this.configmapUtilities.deleteConfigmapKey(
        this.clusterId || this.params.clusterId,
        configmapDataCopy,
        this.deleteKey,
      );
      if (this.params) {
        this.refetch();
      } else {
        this.actionComplete.next();
      }
    } catch (rejection) {
      if (rejection && rejection.errors) {
        this.auiNotificationService.error(rejection.errors[0].message);
      }
    }
  }

  onKeywordChange(event: any) {
    this.keyValueList_show = this.keyValueList.filter(item => {
      return item[0].indexOf(event.target.value) >= 0;
    });
  }
}
