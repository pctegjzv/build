import {
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { ENVIRONMENTS } from 'app2/core/tokens';
// tslint:disable-next-line:ordered-imports
import { Environments } from 'app2/core/types';
import { ConfigMapCreateSelectComponent } from 'app2/features/configmap/create/configmap-create-select.component';
import { K8sConfigmapYamlDialogComponent } from 'app2/features/configmap/dialog/k8s-configmap-yaml-dialog.component';
import {
  ConfigmapService,
  K8sConfigMap,
} from 'app2/shared/services/features/configmap.service';
import { ConfigmapUtilitiesService } from 'app2/shared/services/features/configmap.utilities.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ModalMode } from 'app2/shared/services/modal/modal.types';
import {
  Pagination,
  PaginationDataWrapper,
} from 'app2/shared/services/utility/pagination-data';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './k8s-configmap-list.component.html',
  styleUrls: ['./k8s-configmap-list.component.scss'],
})
export class K8sConfigmapListComponent implements OnInit, OnDestroy {
  @ViewChild('configmapTable')
  table: any;

  SHOW_KEYS_NUM = 10;
  paginationDataWrapper: PaginationDataWrapper<any>;
  paginationDataSubscription: Subscription;
  regionSubscription: Subscription;
  configItems: K8sConfigMap[] = [];
  referenced_list: any[] = [];
  initialized = false;
  configmapCreateEnabled: boolean;
  showAll_list = {};
  pageSize = 20;
  count = 0;
  deleteKey = '';
  deleteKey_configData: any;
  searching: boolean;
  clusterId: string;
  destroyed: boolean;

  private pollingTimer: number;

  get currentPage(): number {
    return this.paginationDataWrapper.pageNo - 1;
  }

  get currentPageSize(): number {
    return this.paginationDataWrapper.pageSize;
  }

  get descriptionKey() {
    return `resource.${this.env.label_base_domain}/description`;
  }

  constructor(
    private translateService: TranslateService,
    private configmapService: ConfigmapService,
    private regionService: RegionService,
    private regionUtilities: RegionUtilitiesService,
    public configmapUtilities: ConfigmapUtilitiesService,
    private auiNotificationService: NotificationService,
    private roleUtil: RoleUtilitiesService,
    private modalService: ModalService,
    private router: Router,
    private cdr: ChangeDetectorRef,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {}

  ngOnInit() {
    this.paginationDataWrapper = new PaginationDataWrapper(
      (
        page: number,
        page_size: number,
        params: {
          clusterId: string;
          namespace?: string;
        },
      ) =>
        this.configmapService.getK8sConfigmaps({
          page,
          page_size,
          ...params,
        }),
      this.pageSize,
    );

    this.paginationDataSubscription = this.paginationDataWrapper.pagination.subscribe(
      (paginationData: Pagination<K8sConfigMap>) => {
        this.searching = false;
        if (!paginationData.loadError) {
          this.count = paginationData.count;
          this.configItems = paginationData.results || [];
          if (!this.configItems.length && paginationData.pageNo > 1) {
            this.paginationDataWrapper.refetch(--paginationData.pageNo);
            return;
          }
          this.cdr.detectChanges();
        }
        this.resetPollingTimer();
      },
    );

    this.regionSubscription = this.regionService.region$
      .pipe(filter(region => !!region))
      .subscribe(region => {
        if (this.regionUtilities.isNewK8sRegion(region)) {
          this.onRegionChanged(region);
        } else {
          this.router.navigate(['home']);
        }
      });
  }

  ngOnDestroy() {
    this.destroyed = true;
    this.paginationDataSubscription.unsubscribe();
    this.regionSubscription.unsubscribe();
    clearTimeout(this.pollingTimer);
  }

  private refetch() {
    this.paginationDataWrapper.refetch();
  }

  async onRegionChanged(region: Region) {
    this.clusterId = region.id;
    this.configItems = [];
    await this.paginationDataWrapper.setParams({
      clusterId: region.id,
    });
    this.configmapCreateEnabled = await this.roleUtil.resourceTypeSupportPermissions(
      'configmap',
      { cluster_name: region.name },
      'create',
    );
    this.initialized = true;
  }

  createConfigmap() {
    this.modalService.open(ConfigMapCreateSelectComponent, {
      title: this.translateService.get('please_select_create_mode'),
    });
  }

  toggleExpandRow(row: any, rowIndex: number) {
    this.table.toggleExpandRow(row, rowIndex);
  }

  onDetailToggle(event: any) {
    if (event.value.showDetail) {
      event.value.showDetail = !event.value.showDetail;
    } else {
      event.value['showDetail'] = true;
    }
  }

  async deleteConfigmap(configmap: K8sConfigMap) {
    try {
      const response = await this.configmapUtilities.deleteConfigmap(
        this.clusterId,
        configmap,
      );
      this.refetch();
      return response;
    } catch (rejection) {
      if (rejection && rejection.errors) {
        this.auiNotificationService.error(rejection.errors[0].message);
      }
    }
  }

  get empty() {
    return !this.configItems || !this.configItems.length;
  }

  get loading() {
    return this.paginationDataWrapper.loading;
  }

  get loadError() {
    return this.paginationDataWrapper.loadError;
  }

  showReferencedList(list: any) {
    this.referenced_list = list || [];
  }

  redirctReferencedResource(item: any) {
    if (item.type === 'AlaudaService') {
      this.router.navigateByUrl(`k8s_app/detail/component/${item.uuid}`);
    } else if (item.type === 'AlaudaApp') {
      this.router.navigateByUrl(`k8s_app/detail/${item.uuid}`);
    }
  }

  showYamlDialog(
    title: string,
    configmapData: K8sConfigMap,
    addConfig = false,
    readOnly = false,
  ) {
    const modelRef = this.modalService.open(K8sConfigmapYamlDialogComponent, {
      title: this.translateService.get(title),
      mode: ModalMode.RIGHT_SLIDER,
      width: 900,
    });
    modelRef.componentInstance.addConfiguration = addConfig;
    modelRef.componentInstance.params = {
      clusterId: this.clusterId,
      namespace: configmapData.kubernetes.metadata.namespace,
      name: configmapData.kubernetes.metadata.name,
    };
    modelRef.componentInstance.readOnly = readOnly;
    modelRef.componentInstance.finished.pipe(first()).subscribe(() => {
      this.refetch();
      modelRef.close();
    });
  }

  pageChanged(pageEvent: { pageIndex: number; pageSize: number }) {
    this.paginationDataWrapper.setPageNo(++pageEvent.pageIndex);
    this.paginationDataWrapper.setPageSize(pageEvent.pageSize);
  }

  searchChanged(queryString: string) {
    this.searching = true;
    this.paginationDataWrapper.setParams({
      name: queryString,
    });
  }

  private resetPollingTimer() {
    clearTimeout(this.pollingTimer);
    if (this.destroyed) {
      return;
    }
    this.pollingTimer = window.setTimeout(() => this.refetch(), 30000);
  }
}
