import {
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  Input,
  OnInit,
  Type,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';

import { BoardConfig } from 'app2/features/dashboard/boards/boards.types';
import { BaseChart } from 'app2/features/dashboard/charts/chart.types';

@Component({
  selector: 'rc-board',
  template: '<ng-template #chartContainer></ng-template>',
})
export class BoardComponent implements OnInit {
  @Input()
  chartConfig: BoardConfig;
  @ViewChild('chartContainer', { read: ViewContainerRef })
  chartContainer: ViewContainerRef;

  constructor(private resolver: ComponentFactoryResolver) {}

  ngOnInit() {
    const chartComponent = this.createChartComponent(this.chartConfig.type);
    if (this.chartConfig.config) {
      chartComponent.instance.config = this.chartConfig.config;
    }
  }

  createChartComponent(type: Type<BaseChart>): ComponentRef<BaseChart> {
    this.chartContainer.clear();
    const factory = this.resolver.resolveComponentFactory(type);
    return this.chartContainer.createComponent(factory);
  }
}
