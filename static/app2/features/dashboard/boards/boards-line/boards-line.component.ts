import { Component, Input } from '@angular/core';

import { BoardsLineConfig } from 'app2/features/dashboard/boards/boards.types';

@Component({
  selector: 'rc-boards-line',
  templateUrl: 'boards-line.component.html',
  styleUrls: ['boards-line.component.scss'],
})
export class BoardsLineComponent {
  @Input()
  config: BoardsLineConfig;
}
