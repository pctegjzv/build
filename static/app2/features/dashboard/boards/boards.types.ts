export interface BoardsLineConfig {
  title?: string;
  base?: number;
  vSpan: number;
  boards: BoardConfig[];
}

export interface BoardConfig {
  type: any;
  hSpan: number;
  config?: object;
  lines?: BoardsLineConfig[];
}
