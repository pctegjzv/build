import {
  BoardConfig,
  BoardsLineConfig,
} from 'app2/features/dashboard/boards/boards.types';
import { AlarmChartComponent } from 'app2/features/dashboard/charts/alarm/alarm-chart.component';
import { BuildChartComponent } from 'app2/features/dashboard/charts/build/build-chart.component';
import { EventChartComponent } from 'app2/features/dashboard/charts/event/event-chart.component';
import { LogAlarmChartComponent } from 'app2/features/dashboard/charts/log-alarm/log-alarm-chart.component';
import { LogChartComponent } from 'app2/features/dashboard/charts/log/log-chart.component';
import { PipelineChartComponent } from 'app2/features/dashboard/charts/pipeline/pipeline-chart.component';
import { RegionContainerChartComponent } from 'app2/features/dashboard/charts/region-container/region-container-chart.component';
import { RegionMetricChartComponent } from 'app2/features/dashboard/charts/region-metric/region-metric-chart.component';
import { RegionNodeChartComponent } from 'app2/features/dashboard/charts/region-node/region-node-chart.component';
import { RegionRequestChartComponent } from 'app2/features/dashboard/charts/region-request/region-request-chart.component';
import { RegionStatsChartComponent } from 'app2/features/dashboard/charts/region-stats/region-stats-chart.component';
import { RegionVolumeChartComponent } from 'app2/features/dashboard/charts/region-volume/region-volume-chart.component';
import { ServiceChartComponent } from 'app2/features/dashboard/charts/service/service-chart.component';

const LINE_1_CARD_1: BoardConfig = {
  type: ServiceChartComponent,
  config: { dataType: 'app' },
  hSpan: 4,
};

const LINE_1_CARD_2: BoardConfig = {
  type: ServiceChartComponent,
  config: { dataType: 'service' },
  hSpan: 4,
};

const LINE_1_CARD_3: BoardConfig = {
  type: RegionStatsChartComponent,
  hSpan: 8,
};

const LINE_1_GROUP_1: BoardConfig = {
  type: 'lines',
  hSpan: 8,
  lines: [
    {
      base: 8,
      vSpan: 1,
      boards: [LINE_1_CARD_1, LINE_1_CARD_2],
    },
    {
      base: 8,
      vSpan: 1,
      boards: [LINE_1_CARD_3],
    },
  ],
};

const LINE_1_CARD_4: BoardConfig = {
  type: RegionRequestChartComponent,
  hSpan: 12,
};

const LINE_1: BoardsLineConfig = {
  title: 'region_resources',
  vSpan: 2,
  boards: [LINE_1_GROUP_1, LINE_1_CARD_4],
};

const LINE_2: BoardsLineConfig = {
  vSpan: 1,
  boards: [
    {
      type: RegionContainerChartComponent,
      hSpan: 4,
    },
    {
      type: RegionNodeChartComponent,
      hSpan: 4,
    },
    {
      type: RegionVolumeChartComponent,
      hSpan: 4,
    },
    {
      type: RegionMetricChartComponent,
      hSpan: 4,
      config: { dataType: 'CPU_AVG' },
    },
    {
      type: RegionMetricChartComponent,
      hSpan: 4,
      config: { dataType: 'MEM_AVG' },
    },
  ],
};

const LINE_3: BoardsLineConfig = {
  title: 'platform_resources',
  vSpan: 2,
  boards: [
    {
      type: BuildChartComponent,
      hSpan: 5,
    },
    {
      type: PipelineChartComponent,
      hSpan: 5,
    },
    {
      type: AlarmChartComponent,
      hSpan: 5,
    },
    {
      type: LogAlarmChartComponent,
      hSpan: 5,
    },
  ],
};

const LINE_4: BoardsLineConfig = {
  vSpan: 2,
  boards: [
    {
      type: LogChartComponent,
      hSpan: 15,
    },
    {
      type: EventChartComponent,
      hSpan: 5,
    },
  ],
};

export const DEFAULT_BOARDS_CONFIG: BoardsLineConfig[] = [
  LINE_1,
  LINE_2,
  LINE_3,
  LINE_4,
];
