import { Component, Input } from '@angular/core';

export interface Legend {
  color: string;
  shape: 'rect' | 'circle';
  label: string;
}

@Component({
  selector: 'rc-chart-container',
  templateUrl: 'chart-container.component.html',
  styleUrls: ['chart-container.component.scss'],
})
export class ChartContainerComponent {
  @Input()
  loading: boolean;
  @Input()
  noData: boolean;
  @Input()
  legends: Legend[];
  @Input()
  noRegion: boolean;
}
