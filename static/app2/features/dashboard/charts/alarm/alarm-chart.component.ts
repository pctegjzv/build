import { Component, OnInit } from '@angular/core';

import { Legend } from 'app2/features/dashboard/chart-container/chart-container.component';
import { BaseChart } from 'app2/features/dashboard/charts/chart.types';
import {
  COLOR_ERROR,
  COLOR_INFO,
  COLOR_OK,
} from 'app2/features/dashboard/charts/colors';
import {
  PieChartConfig,
  SerieChartData,
} from 'app2/features/dashboard/charts/ngx-charts.types';
import { AlarmService } from 'app2/shared/services/features/alarm.service';
import { SourceStatsCount } from 'app2/shared/services/features/model.types';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-alarm-chart',
  templateUrl: 'alarm-chart.component.html',
  styleUrls: ['../base-chart.component.scss', 'alarm-chart.component.scss'],
})
export class AlarmChartComponent extends BaseChart implements OnInit {
  chartConfig: PieChartConfig = {
    scheme: { domain: [COLOR_OK, COLOR_ERROR, COLOR_INFO] },
    doughnut: true,
  };

  statusList: string[] = [
    'chart_status_OK',
    'chart_status_ALARM',
    'chart_status_INSUFFICIENT_DATA',
  ];

  legends: Legend[] = [
    {
      label: this.statusList[0],
      color: COLOR_OK,
      shape: 'rect',
    },
    {
      label: this.statusList[1],
      color: COLOR_ERROR,
      shape: 'rect',
    },
    {
      label: this.statusList[2],
      color: COLOR_INFO,
      shape: 'rect',
    },
  ];
  totalCount: number;
  data: SerieChartData[];
  fakeData: SerieChartData[] = [];

  constructor(
    private alarmService: AlarmService,
    private translateService: TranslateService,
  ) {
    super();
  }

  ngOnInit() {
    this.alarmService
      .getAlarmsCount()
      .then(data => {
        this.totalCount = data.count;
        return data;
      })
      .then(this.convertData)
      .then(data => {
        this.data = data;
        this.initialized = true;
      })
      .catch(() => {
        this.initialized = true;
      });
  }

  convertData = (data: SourceStatsCount): SerieChartData[] => {
    const OK = data.count_result.find(result => result.custom_status === 'OK');
    const ERROR = data.count_result.find(
      result => result.custom_status === 'ALARM',
    );
    const OTHER = data.count_result.find(
      result => result.custom_status === 'INSUFFICIENT_DATA',
    );
    return [
      {
        name: this.translateService.get(this.statusList[0]),
        value: OK ? OK.count : 0,
      },
      {
        name: this.translateService.get(this.statusList[1]),
        value: ERROR ? ERROR.count : 0,
      },
      {
        name: this.translateService.get(this.statusList[2]),
        value: OTHER ? OTHER.count : 0,
      },
    ];
  };
}
