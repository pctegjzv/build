import { Input, OnDestroy } from '@angular/core';
import * as moment from 'moment';
import { Subscription } from 'rxjs';

import { QuerySection } from 'app2/features/dashboard/charts/chart.types';
import { MultiChartData } from 'app2/features/dashboard/charts/ngx-charts.types';
import { HistoryStatsCount } from 'app2/shared/services/features/model.types';
import {
  MetricQuery,
  MetricQueryParams,
} from 'app2/shared/services/features/monitor.service';

export interface ConvertFunParams {
  data: HistoryStatsCount;
  startTime: number;
  days?: number;
  groupSize?: number;
  status?: string[];
}

export class BaseChart implements OnDestroy {
  protected subscriptions: Subscription[] = [];
  protected pollingCycle = 10000;

  @Input()
  config: any;

  data: any;
  fakeData: any;
  initialized: boolean;

  getQuerySection(interval = 30 * 60): QuerySection {
    const now = new Date().getTime() / 1000;
    return {
      start_time: now - interval,
      end_time: now,
    };
  }

  /**
   * 现在到 n 天前的时间区间，包含今天
   * @param {number} days
   * @returns {QuerySection}
   */
  getQuerySectionByDay(days: number = 28): QuerySection {
    const now = new Date().getTime();
    const before = new Date(now - (days - 1) * 24 * 60 * 60 * 1000);
    return {
      start_time:
        new Date(
          before.getFullYear(),
          before.getMonth(),
          before.getDate(),
        ).getTime() / 1000,
      end_time: Math.floor(now / 1000),
    };
  }

  /**
   * 过滤掉空值并返回最近时间点的值
   * @param {Object} dps
   * @returns {{time: number; value: number}}
   */
  getLatestMetricData(dps: object): { time: number; value: number } {
    const times = Object.keys(dps)
      .filter(key => dps[key] !== null)
      .map(key => parseInt(key, 10))
      .sort((a, b) => b - a);
    const latestTime = times[0];
    return {
      time: latestTime * 1000,
      value: dps[latestTime] || 0,
    };
  }

  buildMetricQueryParams = (
    base: MetricQuery,
    where?: object,
    by?: string[],
    section?: QuerySection,
  ): MetricQueryParams => {
    const querySection = section || this.getQuerySection();
    const metricQuery = Object.assign({}, base, { where, by });
    return {
      start: querySection.start_time * 1000,
      end: querySection.end_time * 1000,
      qs: [metricQuery],
    };
  };

  /**
   * 根据起止时间和步长生成时间点序列
   * @param {number} start
   * @param {number} end
   * @param {number} step
   * @returns {number[]}
   */
  getTimeLine = (start: number, end: number, step: number): number[] => {
    if (start >= end) {
      return [start];
    } else {
      return [start, ...this.getTimeLine(start + step, end, step)];
    }
  };

  /**
   *
   * @param {HistoryStatsCount} data
   * @param {number} startTime 查询的开始时间
   * @param {number} days 总天数
   * @param {number} groupSize 分组大小
   * @param {string[]} status 要筛选出得状态列表
   * @returns {MultiChartData[]}
   */
  convertHistoryStatsData = ({
    data,
    startTime,
    days = 28,
    groupSize = 7,
    status = ['OK', 'ERROR'],
  }: ConvertFunParams): MultiChartData[] => {
    const daySec = 24 * 60 * 60;
    const countResults = data.count_result;
    const sourceArr = [];
    const length = Math.ceil(days / groupSize);
    for (let i = 0; i < length; i++) {
      sourceArr.push(i);
    }
    return sourceArr
      .map(value => value * groupSize)
      .map(day => {
        return {
          start: startTime + day * daySec,
          end: startTime + (day + groupSize + 1) * daySec - 1,
        };
      })
      .map(day => {
        const resultsGroup = countResults.filter(value => {
          return value.start >= day.start && value.end <= day.end;
        });
        const series = status.map(key => {
          let sum = 0;
          resultsGroup.map(item => item[key].count).forEach(value => {
            sum += value;
          });
          return {
            name: key,
            value: sum,
          };
        });
        return {
          name: this.getHistoryXTickName(day.start, day.end - daySec),
          series,
        };
      });
  };

  private getHistoryXTickName(start: number, end: number): string {
    const format = 'MM/DD';
    const startDate = moment.unix(start).format(format);
    const endDate = moment.unix(end).format(format);
    return `${startDate}-${endDate}`;
  }

  /**
   * 交换横坐标和分组
   * @param {MultiChartData[]} data
   * @returns {MultiChartData[]}
   */
  chartXYDataExchange(data: MultiChartData[]): MultiChartData[] {
    return data[0].series.map(serie => serie.name).map(status => {
      return {
        name: <string>status,
        series: data.map(item => {
          return {
            name: item.name,
            value: data
              .find(value => value.name === item.name)
              .series.find(serie => serie.name === status).value,
          };
        }),
      };
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }
}

export interface QuerySection {
  start_time: number;
  end_time: number;
}
