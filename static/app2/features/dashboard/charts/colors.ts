export const COLOR_OK = '#83E39F';
export const COLOR_WARNING = '#FDD17C';
export const COLOR_ERROR = '#F08C93';
export const COLOR_INFO = '#53CFF6';
