import { Component, OnInit } from '@angular/core';

import { Legend } from 'app2/features/dashboard/chart-container/chart-container.component';
import { BaseChart } from 'app2/features/dashboard/charts/chart.types';
import {
  COLOR_ERROR,
  COLOR_OK,
  COLOR_WARNING,
} from 'app2/features/dashboard/charts/colors';
import {
  MultiChartData,
  PieChartConfig,
  SerieChartData,
} from 'app2/features/dashboard/charts/ngx-charts.types';
import { EventService } from 'app2/shared/services/features/event.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-event-chart',
  templateUrl: 'event-chart.component.html',
  styleUrls: [
    '../base-chart.component.scss',
    '../alarm/alarm-chart.component.scss',
  ],
})
export class EventChartComponent extends BaseChart implements OnInit {
  chartConfig: PieChartConfig = {
    scheme: { domain: [COLOR_OK, COLOR_ERROR, COLOR_WARNING] },
    doughnut: true,
  };

  statusList: string[] = [
    'chart_status_INFO',
    'chart_status_WARNING',
    'chart_status_ERROR',
  ];

  legends: Legend[] = [
    {
      label: this.statusList[0],
      color: COLOR_OK,
      shape: 'rect',
    },
    {
      label: this.statusList[1],
      color: COLOR_WARNING,
      shape: 'rect',
    },
    {
      label: this.statusList[2],
      color: COLOR_ERROR,
      shape: 'rect',
    },
  ];
  totalCount: number;
  data: SerieChartData[];
  fakeData: SerieChartData[] = [];

  constructor(
    private eventService: EventService,
    private translateService: TranslateService,
  ) {
    super();
  }

  ngOnInit() {
    const querySection = this.getQuerySectionByDay(30);
    this.eventService
      .getEventsCount(querySection)
      .then(data => {
        this.totalCount = data.count;
        return data;
      })
      .then(data =>
        this.convertHistoryStatsData({
          data,
          startTime: querySection.start_time,
          days: 30,
          groupSize: 30,
          status: ['OK', 'ERROR', 'OTHER'],
        }),
      )
      .then(data => this.translate(data))
      .then(data => {
        this.data = data[0].series;
        this.initialized = true;
      })
      .catch(() => {
        this.initialized = true;
      });
  }

  translate(data: MultiChartData[]): MultiChartData[] {
    return data.map(({ name, series }) => ({
      name,
      series: series.map(({ name, value }) => {
        let translatedName;
        switch (name) {
          case 'OK':
            translatedName = this.translateService.get('chart_status_INFO');
            break;
          case 'ERROR':
            translatedName = this.translateService.get('chart_status_ERROR');
            break;
          default:
            translatedName = this.translateService.get('chart_status_WARNING');
        }
        return {
          name: translatedName,
          value,
        };
      }),
    }));
  }
}
