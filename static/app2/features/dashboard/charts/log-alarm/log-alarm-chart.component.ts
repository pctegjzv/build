import { Component, OnInit } from '@angular/core';

import { Legend } from 'app2/features/dashboard/chart-container/chart-container.component';
import { BaseChart } from 'app2/features/dashboard/charts/chart.types';
import {
  COLOR_ERROR,
  COLOR_INFO,
  COLOR_OK,
} from 'app2/features/dashboard/charts/colors';
import {
  SerieChartData,
  VerticalBarChartConfig,
} from 'app2/features/dashboard/charts/ngx-charts.types';
import { LogAlarmService } from 'app2/shared/services/features/log-alarm.service';
import { SourceStatsCount } from 'app2/shared/services/features/model.types';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-log-alarm-chart',
  templateUrl: 'log-alarm-chart.component.html',
  styleUrls: ['../base-chart.component.scss'],
})
export class LogAlarmChartComponent extends BaseChart implements OnInit {
  chartConfig: VerticalBarChartConfig = {
    scheme: { domain: [COLOR_OK, COLOR_ERROR, COLOR_INFO] },
    xAxis: true,
    yAxis: true,
    showGridLines: true,
    roundDomains: false,
    roundEdges: false,
    barPadding: 48,
    yAxisTickFormatting: (val: number) => {
      if (val >= 1000) {
        return val / 1000 + 'k';
      } else {
        return val;
      }
    },
  };

  statusList: string[] = [
    'chart_status_OK',
    'chart_status_ALARM',
    'chart_status_INVALID',
  ];

  legends: Legend[] = [
    {
      label: this.statusList[0],
      color: COLOR_OK,
      shape: 'rect',
    },
    {
      label: this.statusList[1],
      color: COLOR_ERROR,
      shape: 'rect',
    },
    {
      label: this.statusList[2],
      color: COLOR_INFO,
      shape: 'rect',
    },
  ];
  data: SerieChartData[];
  fakeData: SerieChartData[] = [];

  constructor(
    private logAlarmService: LogAlarmService,
    private translateService: TranslateService,
  ) {
    super();
  }

  ngOnInit() {
    this.logAlarmService
      .getLogAlarmsCount()
      .then(this.convertData)
      .then(data => {
        this.data = data;
        this.initialized = true;
      })
      .catch(() => {
        this.initialized = true;
      });
  }

  convertData = (data: SourceStatsCount): SerieChartData[] => {
    const OK = data.count_result.find(result => result.status === 'OK');
    const ERROR = data.count_result.find(result => result.status === 'ERROR');
    const OTHER = data.count_result.find(result => result.status === 'OTHER');
    return [
      {
        name: this.translateService.get(this.statusList[0]),
        value: OK ? OK.count : 0,
      },
      {
        name: this.translateService.get(this.statusList[1]),
        value: ERROR ? ERROR.count : 0,
      },
      {
        name: this.translateService.get(this.statusList[2]),
        value: OTHER ? OTHER.count : 0,
      },
    ];
  };
}
