import { Component, OnInit } from '@angular/core';
import moment from 'moment';

import { BaseChart } from 'app2/features/dashboard/charts/chart.types';
import {
  SerieChartData,
  VerticalBarChartConfig,
} from 'app2/features/dashboard/charts/ngx-charts.types';
import {
  Aggregations,
  AggregationsQueryParams,
  LogService,
} from 'app2/shared/services/features/log.service';

@Component({
  selector: 'rc-build-chart',
  templateUrl: 'log-chart.component.html',
  styleUrls: ['../base-chart.component.scss'],
})
export class LogChartComponent extends BaseChart implements OnInit {
  private paths: string;

  chartConfig: VerticalBarChartConfig = {
    scheme: { domain: ['#8BD6FA'] },
    xAxis: true,
    yAxis: true,
    showGridLines: true,
    roundDomains: false,
    roundEdges: false,
    barPadding: 8,
    xAxisTickFormatting: (val: number) => {
      return moment.unix(val).format('MM-DD HH:mm');
    },
    yAxisTickFormatting: (val: number) => {
      if (val >= 1000) {
        return val / 1000 + 'k';
      } else {
        return val;
      }
    },
  };
  data: SerieChartData[];
  fakeData: SerieChartData[];

  constructor(private logService: LogService) {
    super();
    this.fakeData = this.createFakeData();
  }

  async ngOnInit() {
    try {
      this.paths = (await this.logService.getTypes()).paths.join(',');
      this.data = await this.loadData({
        paths: this.paths,
        ...this.getQuerySection(),
      }).then(this.convertData);
    } catch (err) {
    } finally {
      this.initialized = true;
    }
  }

  async loadData(params: AggregationsQueryParams) {
    return await this.logService.getAggregations(params);
  }

  convertData({ buckets }: Aggregations): SerieChartData[] {
    return buckets.map(bucket => ({
      name: bucket.time,
      value: bucket.count,
    }));
  }

  createFakeData(): SerieChartData[] {
    const now = new Date().getTime() / 1000;
    const timeLine = this.getTimeLine(now, now + 30 * 60, 3 * 60);
    return timeLine.map(time => ({
      name: moment.unix(time).format('MM-DD HH:mm'),
      value: 0,
    }));
  }

  getChartTooltipDisplay(time: number) {
    return moment.unix(time).format('MM-DD HH:mm');
  }
}
