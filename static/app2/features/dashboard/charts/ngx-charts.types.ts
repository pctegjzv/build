import { TemplateRef } from '@angular/core';
import { Path } from 'd3-path';
import { CurveGenerator } from 'd3-shape';

export interface SerieChartData {
  name: string | number | Date;
  value: number;
}

export interface MultiChartData {
  name: string;
  series: SerieChartData[];
}

interface BaseChartConfig {
  view?: number[];
  scheme?: { domain: string[] };
  schemeType?: string;
  customColors?: object;
  animations?: boolean;
  legend?: boolean;
  legendTitle?: string;
  xAxis?: boolean;
  yAxis?: boolean;
  showGridLines?: boolean;
  roundDomains?: boolean;
  showXAxisLabel?: boolean;
  showYAxisLabel?: boolean;
  xAxisLabel?: string;
  yAxisLabel?: string;
  gradient?: boolean;
  activeEntries?: object[];
  tooltipDisabled?: boolean;
  tooltipTemplate?: TemplateRef<any>;
  yScaleMax?: number;

  xAxisTickFormatting?(val: number | string | Date): number | string;

  yAxisTickFormatting?(val: number): number | string;
}

export interface VerticalBarChartConfig extends BaseChartConfig {
  barPadding?: number;
  roundEdges?: boolean;
}

export interface AreaChartConfig extends BaseChartConfig {
  timeline?: boolean;
  autoScale?: boolean;
  curve?: (context: CanvasRenderingContext2D | Path) => CurveGenerator;
  seriesTooltipTemplate?: TemplateRef<any>;
  xScaleMin?: any;
  xScaleMax?: any;
  yScaleMin?: number;
}

export interface PieChartConfig extends BaseChartConfig {
  labels?: boolean;
  explodeSlices?: boolean;
  doughnut?: boolean;
  arcWidth?: number;

  labelFormatting?(val: number | string): number | string;
}

export interface GaugeChartConfig extends BaseChartConfig {
  min?: number;
  max?: number;
  units?: string;
  bigSegments?: number;
  smallSegments?: number;
  showAxis?: boolean;
  angleSpan?: number;
  startAngle?: number;

  valueFormatting?(val: number): number | string;

  axisTickFormatting?(val: number): number | string;
}
