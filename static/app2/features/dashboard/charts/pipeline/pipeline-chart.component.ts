import { Component, OnInit } from '@angular/core';

import { Legend } from 'app2/features/dashboard/chart-container/chart-container.component';
import { BaseChart } from 'app2/features/dashboard/charts/chart.types';
import { COLOR_ERROR, COLOR_OK } from 'app2/features/dashboard/charts/colors';
import {
  AreaChartConfig,
  MultiChartData,
} from 'app2/features/dashboard/charts/ngx-charts.types';
import { PipelineService } from 'app2/shared/services/features/pipeline.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-pipeline-chart',
  templateUrl: 'pipeline-chart.component.html',
})
export class PipelineChartComponent extends BaseChart implements OnInit {
  chartConfig: AreaChartConfig = {
    scheme: { domain: [COLOR_OK, COLOR_ERROR] },
    xAxis: true,
    yAxis: true,
    showGridLines: false,
    gradient: false,
    xAxisTickFormatting: (val: string) => {
      const arr = val.split('-').map(time => time.split('/'));
      return `${arr[0][1]}-${arr[1][1]}`;
    },
    yAxisTickFormatting: (val: number) => {
      if (val >= 1000) {
        return val / 1000 + 'k';
      } else {
        return val;
      }
    },
  };

  statusList: string[] = ['chart_status_SUCCESS', 'chart_status_FAILED'];

  legends: Legend[] = [
    {
      label: this.statusList[0],
      color: COLOR_OK,
      shape: 'circle',
    },
    {
      label: this.statusList[1],
      color: COLOR_ERROR,
      shape: 'circle',
    },
  ];
  data: MultiChartData[];
  fakeData: MultiChartData[] = [];

  constructor(
    private pipelineService: PipelineService,
    private translateService: TranslateService,
  ) {
    super();
  }

  ngOnInit() {
    const querySection = this.getQuerySectionByDay();
    this.pipelineService
      .getPipelinesCount(querySection)
      .then(data =>
        this.convertHistoryStatsData({
          data,
          startTime: querySection.start_time,
        }),
      )
      .then(data => this.translate(data))
      .then(this.chartXYDataExchange)
      .then(data => {
        this.data = data;
        this.initialized = true;
      })
      .catch(() => {
        this.initialized = true;
      });
  }

  translate(data: MultiChartData[]): MultiChartData[] {
    return data.map(({ name, series }) => ({
      name,
      series: series.map(({ name, value }) => {
        let translatedName;
        switch (name) {
          case 'OK':
            translatedName = this.translateService.get('chart_status_SUCCESS');
            break;
          case 'ERROR':
            translatedName = this.translateService.get('chart_status_FAILED');
            break;
          default:
            translatedName = this.translateService.get('chart_status_OTHER');
        }
        return {
          name: translatedName,
          value,
        };
      }),
    }));
  }
}
