import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import {
  distinctUntilKeyChanged,
  filter,
  map,
  pluck,
  tap,
} from 'rxjs/operators';

import { BaseChart } from 'app2/features/dashboard/charts/chart.types';
import {
  Region,
  RegionService,
  RegionStats,
} from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { AppState } from 'app2/state-store/reducers';
import * as statsActions from 'app2/state-store/region_stats/actions';
import * as fromStats from 'app2/state-store/region_stats/reducers';
import { State } from 'app2/state-store/region_stats/reducers';

@Component({
  selector: 'rc-region-container-chart',
  templateUrl: 'region-container-chart.component.html',
  styleUrls: [
    '../base-chart.component.scss',
    '../region-metric/region-metric-chart.component.scss',
  ],
})
export class RegionContainerChartComponent extends BaseChart implements OnInit {
  data: RegionStats;
  time: number;
  regionName: string;
  region: Region;

  constructor(
    private store: Store<AppState>,
    private regionService: RegionService,
    private regionUtilitiesService: RegionUtilitiesService,
  ) {
    super();
  }

  ngOnInit() {
    const statsSub = combineLatest(
      this.store.select(fromStats.selectFeature),
      this.regionService.region$.pipe(
        filter(region => !!region),
        distinctUntilKeyChanged('name'),
        tap((region: Region) => {
          this.region = region;
        }),
        pluck('name'),
        tap((cluster: string) => {
          this.regionName = cluster;
          this.store.dispatch(new statsActions.GetByRegion(cluster));
        }),
      ),
    )
      .pipe(
        map(([state, cluster]: [State, string]) => state[cluster]),
        filter(data => !!data),
      )
      .subscribe(
        data => {
          this.data = data;
          this.time = new Date().getTime();
          this.initialized = true;
        },
        () => {
          this.initialized = true;
        },
      );

    this.subscriptions.push(statsSub);
  }

  getContainerRouterLink() {
    if (this.regionUtilitiesService.isNewK8sRegion(this.region)) {
      return ['/k8s_pod'];
    } else {
      return ['/cluster/detail', this.regionName];
    }
  }
}
