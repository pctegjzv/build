import { Component, Input, OnInit } from '@angular/core';
import { EMPTY, from } from 'rxjs';
import {
  catchError,
  distinctUntilKeyChanged,
  filter,
  map,
  pluck,
  switchMap,
} from 'rxjs/operators';

import { BaseChart } from 'app2/features/dashboard/charts/chart.types';
import {
  COLOR_ERROR,
  COLOR_OK,
  COLOR_WARNING,
} from 'app2/features/dashboard/charts/colors';
import {
  Metric,
  MetricQuery,
  MetricQueryParams,
  MonitorService,
} from 'app2/shared/services/features/monitor.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { TranslateService } from 'app2/translate/translate.service';

export interface ChartData {
  time: number;
  value: number;
  unit: string;
}

@Component({
  selector: 'rc-region-metric',
  templateUrl: 'region-metric-chart.component.html',
  styleUrls: [
    '../base-chart.component.scss',
    'region-metric-chart.component.scss',
  ],
})
export class RegionMetricChartComponent extends BaseChart implements OnInit {
  static CONTAINER_SUM: MetricQuery = {
    agg: 'sum',
    name: 'node.containers.count',
  };

  static CPU_AVG: MetricQuery = {
    agg: 'avg',
    name: 'node.cpu.utilization',
  };

  static MEM_AVG: MetricQuery = {
    agg: 'avg',
    name: 'node.mem.utilization',
  };

  @Input()
  config: {
    dataType: string;
  };
  data: ChartData;

  constructor(
    private regionService: RegionService,
    private monitorService: MonitorService,
    private translateService: TranslateService,
  ) {
    super();
  }

  ngOnInit() {
    const regionSub = this.regionService.region$
      .pipe(
        filter(region => !!region),
        distinctUntilKeyChanged('id'),
        pluck('name'),
        map(region_name =>
          this.buildMetricQueryParams(
            RegionMetricChartComponent[this.config.dataType],
            {
              region_name,
            },
          ),
        ),
        switchMap(params =>
          from(this.loadData(params)).pipe(
            catchError(() => {
              this.data = null;
              this.initialized = true;
              return EMPTY;
            }),
          ),
        ),
        pluck('0'),
        map((data: Metric) => ({
          ...this.getLatestMetricData(data.dps),
          unit: data.unit,
        })),
        map(this.metricValueFilter),
      )
      .subscribe(
        data => {
          this.data = data;
          this.initialized = true;
        },
        () => {
          this.initialized = true;
        },
      );

    this.subscriptions.push(regionSub);
  }

  get title() {
    switch (this.config.dataType) {
      case 'CONTAINER_SUM':
        return this.translateService.get('chart_title_container_count');
      case 'CPU_AVG':
        return this.translateService.get('chart_title_cpu_avg');
      case 'MEM_AVG':
        return this.translateService.get('chart_title_mem_avg');
    }
  }

  get color(): string {
    if (!this.data) {
      return;
    }
    switch (this.config.dataType) {
      case 'CONTAINER_SUM':
        return COLOR_OK;
      case 'CPU_AVG':
      case 'MEM_AVG':
        return this.getPercentColor(this.data.value);
    }
  }

  getPercentColor(value: number): string {
    if (value <= 75) {
      return COLOR_OK;
    } else if (value <= 90) {
      return COLOR_WARNING;
    } else {
      return COLOR_ERROR;
    }
  }

  metricValueFilter({ time, value, unit }: ChartData) {
    switch (unit.toLowerCase()) {
      case 'count':
        unit = '';
        break;
      case 'percent':
        value = parseFloat((value * 100).toFixed(1));
        unit = '%';
        break;
    }
    return { time, value, unit };
  }

  async loadData(params: MetricQueryParams) {
    return await this.monitorService.queryMetrics(params);
  }
}
