import { Component, OnInit } from '@angular/core';
import { EMPTY, from } from 'rxjs';
import {
  catchError,
  distinctUntilKeyChanged,
  filter,
  map,
  pluck,
  switchMap,
  tap,
} from 'rxjs/operators';

import { BaseChart } from 'app2/features/dashboard/charts/chart.types';
import { SourceStatsCount } from 'app2/shared/services/features/model.types';
import { RegionService } from 'app2/shared/services/features/region.service';

@Component({
  selector: 'rc-region-node-chart',
  templateUrl: 'region-node-chart.component.html',
  styleUrls: [
    '../base-chart.component.scss',
    '../region-metric/region-metric-chart.component.scss',
  ],
})
export class RegionNodeChartComponent extends BaseChart implements OnInit {
  data: { okCount: number; totalCount: number };
  time: number;
  regionName: string;

  constructor(private regionService: RegionService) {
    super();
  }

  ngOnInit() {
    const regionSub = this.regionService.region$
      .pipe(
        filter(region => !!region),
        distinctUntilKeyChanged('id'),
        pluck('name'),
        tap((name: string) => {
          this.regionName = name;
        }),
        switchMap((regionName: string) =>
          from(this.loadData(regionName)).pipe(
            catchError(() => {
              this.data = null;
              this.time = null;
              this.initialized = true;
              return EMPTY;
            }),
          ),
        ),
        map((data: SourceStatsCount) => ({
          totalCount: data.count,
          okCount: data.count_result.find(item => item.status === 'OK').count,
        })),
      )
      .subscribe(
        data => {
          this.data = data;
          this.time = new Date().getTime();
          this.initialized = true;
        },
        () => {
          this.data = null;
          this.time = null;
          this.initialized = true;
        },
      );

    this.subscriptions.push(regionSub);
  }

  async loadData(regionName: string) {
    return this.regionService.getRegionNodesCount(regionName);
  }
}
