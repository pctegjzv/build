import { Component, OnDestroy, OnInit } from '@angular/core';
import * as d3 from 'd3';
import * as moment from 'moment';
import { EMPTY, from } from 'rxjs';
import {
  catchError,
  distinctUntilKeyChanged,
  filter,
  map,
  pluck,
  switchMap,
} from 'rxjs/operators';

import { BaseChart } from 'app2/features/dashboard/charts/chart.types';
import { COLOR_OK } from 'app2/features/dashboard/charts/colors';
import {
  AreaChartConfig,
  MultiChartData,
} from 'app2/features/dashboard/charts/ngx-charts.types';
import {
  MetricQuery,
  MetricQueryParams,
  MonitorService,
} from 'app2/shared/services/features/monitor.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-region-request-chart',
  templateUrl: 'region-request-chart.component.html',
  styleUrls: ['../base-chart.component.scss'],
})
export class RegionRequestChartComponent extends BaseChart
  implements OnInit, OnDestroy {
  static REQUESTS_SUM: MetricQuery = {
    name: 'comp.alb_haproxy.haproxy.frontend.requests.rate',
    agg: 'sum',
  };

  chartConfig: AreaChartConfig = {
    scheme: { domain: [COLOR_OK] },
    xAxis: true,
    yAxis: true,
    showGridLines: false,
    gradient: false,
    autoScale: false,
    curve: d3.curveMonotoneX,
    xAxisTickFormatting: (val: Date) => {
      return moment(val).format('HH:mm');
    },
    yAxisTickFormatting: (val: number) => {
      if (val >= 1000) {
        return val / 1000 + 'k';
      } else {
        return val;
      }
    },
  };
  data: MultiChartData[];
  fakeData: MultiChartData[];
  title = 'chart_title_region_request_count';
  translatedTitle: string;

  constructor(
    private regionService: RegionService,
    private monitorService: MonitorService,
    private translateService: TranslateService,
  ) {
    super();
  }

  ngOnInit() {
    this.translatedTitle = this.translateService.get(this.title);
    this.fakeData = this.createFakeData(this.translatedTitle);

    const regionSub = this.regionService.region$
      .pipe(
        filter(region => !!region),
        distinctUntilKeyChanged('id'),
        pluck('name'),
        map(region_name =>
          this.buildMetricQueryParams(
            RegionRequestChartComponent.REQUESTS_SUM,
            {
              region_name,
            },
          ),
        ),
        switchMap(params =>
          from(this.loadData(params)).pipe(
            catchError(() => {
              this.data = null;
              this.initialized = true;
              return EMPTY;
            }),
          ),
        ),
        pluck('0', 'dps'),
        map(
          (dps): MultiChartData[] => [
            {
              name: this.translatedTitle,
              series: this.convertData(dps),
            },
          ],
        ),
      )
      .subscribe(
        data => {
          this.data = data;
          this.initialized = true;
        },
        () => {
          this.initialized = true;
        },
      );

    this.subscriptions.push(regionSub);
  }

  async loadData(params: MetricQueryParams) {
    return await this.monitorService.queryMetrics(params);
  }

  convertData(dps: object) {
    return Object.keys(dps)
      .filter(key => dps[key] !== null)
      .map(key => parseInt(key, 10))
      .sort((a, b) => a - b)
      .map(key => ({
        name: new Date(key * 1000),
        value: dps[key],
      }));
  }

  createFakeData(name: string): MultiChartData[] {
    const now = new Date().getTime() / 1000;
    const timeLine = this.getTimeLine(now - 30 * 60, now, 60);
    return [
      {
        name,
        series: timeLine.map(time => ({
          name: new Date(time * 1000),
          value: 0,
        })),
      },
    ];
  }
}
