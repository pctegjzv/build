import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import {
  distinctUntilKeyChanged,
  filter,
  map,
  pluck,
  tap,
} from 'rxjs/operators';

import { BaseChart } from 'app2/features/dashboard/charts/chart.types';
import {
  RegionService,
  RegionStats,
} from 'app2/shared/services/features/region.service';
import { AppState } from 'app2/state-store/reducers';
import * as statsActions from 'app2/state-store/region_stats/actions';
import * as fromStats from 'app2/state-store/region_stats/reducers';
import { State } from 'app2/state-store/region_stats/reducers';

@Component({
  selector: 'rc-region-stats-chart',
  templateUrl: 'region-stats-chart.component.html',
  styleUrls: [
    '../base-chart.component.scss',
    'region-stats-chart.component.scss',
  ],
})
export class RegionStatsChartComponent extends BaseChart implements OnInit {
  data: RegionStats;

  constructor(
    private store: Store<AppState>,
    private regionService: RegionService,
  ) {
    super();
  }

  ngOnInit() {
    const statsSub = combineLatest(
      this.store.select(fromStats.selectFeature),
      this.regionService.region$.pipe(
        filter(region => !!region),
        distinctUntilKeyChanged('name'),
        pluck('name'),
        tap((cluster: string) => {
          this.store.dispatch(new statsActions.GetByRegion(cluster));
        }),
      ),
    )
      .pipe(
        map(([state, cluster]: [State, string]) => state[cluster]),
        filter(data => !!data),
      )
      .subscribe(data => {
        this.data = data;
      });

    this.subscriptions.push(statsSub);
  }
}
