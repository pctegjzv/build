import { Component, OnInit } from '@angular/core';
import { EMPTY, from } from 'rxjs';
import {
  catchError,
  distinctUntilKeyChanged,
  filter,
  map,
  pluck,
  switchMap,
} from 'rxjs/operators';

import { BaseChart } from 'app2/features/dashboard/charts/chart.types';
import { Mem, fitMem } from 'app2/shared/pipes/fit-mem.pipe';
import { RegionService } from 'app2/shared/services/features/region.service';
import { StorageService } from 'app2/shared/services/features/storage.service';

@Component({
  selector: 'rc-region-volume-chart',
  templateUrl: 'region-volume-chart.component.html',
  styleUrls: [
    '../base-chart.component.scss',
    '../region-metric/region-metric-chart.component.scss',
  ],
})
export class RegionVolumeChartComponent extends BaseChart implements OnInit {
  data: Mem;
  time: number;

  constructor(
    private regionService: RegionService,
    private storageService: StorageService,
  ) {
    super();
  }

  ngOnInit() {
    const regionSub = this.regionService.region$
      .pipe(
        filter(region => !!region),
        distinctUntilKeyChanged('id'),
        pluck('id'),
        switchMap((regionId: string) =>
          from(this.loadData(regionId)).pipe(
            catchError(() => {
              this.data = null;
              this.time = null;
              this.initialized = true;
              return EMPTY;
            }),
          ),
        ),
        map((mem: Mem) => fitMem(mem.value, mem.unit)),
        map(({ value, unit }) => ({
          value: parseFloat(value.toFixed(2)),
          unit,
        })),
      )
      .subscribe(
        data => {
          this.data = data;
          this.time = new Date().getTime();
          this.initialized = true;
        },
        () => {
          this.data = null;
          this.time = null;
          this.initialized = true;
        },
      );

    this.subscriptions.push(regionSub);
  }

  async loadData(regionId: string) {
    return this.storageService.getRegionVolumeSize(regionId);
  }
}
