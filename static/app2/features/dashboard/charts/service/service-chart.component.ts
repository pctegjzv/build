import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { EMPTY, from } from 'rxjs';
import {
  catchError,
  distinctUntilKeyChanged,
  filter,
  map,
  switchMap,
  tap,
} from 'rxjs/operators';

import { BaseChart } from 'app2/features/dashboard/charts/chart.types';
import {
  COLOR_ERROR,
  COLOR_INFO,
  COLOR_OK,
} from 'app2/features/dashboard/charts/colors';
import {
  GaugeChartConfig,
  SerieChartData,
} from 'app2/features/dashboard/charts/ngx-charts.types';
import { AppService } from 'app2/shared/services/features/app.service';
import { SourceStatsCount } from 'app2/shared/services/features/model.types';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-service-chart',
  templateUrl: 'service-chart.component.html',
  styleUrls: ['../base-chart.component.scss', 'service-chart.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ServiceChartComponent extends BaseChart implements OnInit {
  @Input()
  config: { dataType: 'service' | 'app' };

  chartConfig: GaugeChartConfig = {
    startAngle: -90,
    angleSpan: 180,
    scheme: { domain: [COLOR_OK, COLOR_ERROR, COLOR_INFO] },
    showAxis: false,
  };

  statusList: string[] = [
    'chart_status_RUNNING',
    'chart_status_ERROR',
    'chart_status_OTHER',
  ];
  max: number;
  data: SerieChartData[];
  title: string;

  private region: Region;
  constructor(
    private regionService: RegionService,
    private regionUtilitiesService: RegionUtilitiesService,
    private serviceService: ServiceService,
    private appService: AppService,
    private translateService: TranslateService,
  ) {
    super();
  }

  ngOnInit() {
    this.title = 'chart_title_' + this.config.dataType;

    const regionSub = this.regionService.region$
      .pipe(
        filter(region => !!region),
        distinctUntilKeyChanged('id'),
        tap((region: Region) => {
          this.region = region;
        }),
        switchMap(region =>
          from(this.loadData(this.config.dataType, region)).pipe(
            catchError(() => {
              this.data = null;
              this.initialized = true;
              return EMPTY;
            }),
          ),
        ),
        tap((data: SourceStatsCount) => {
          this.max = data.count;
        }),
        map(this.convertData),
      )
      .subscribe(
        data => {
          this.data = data;
          this.initialized = true;
        },
        () => {
          this.initialized = true;
        },
      );

    this.subscriptions.push(regionSub);
  }

  loadData(sourceType: 'service' | 'app', region: Region) {
    if (this.regionUtilitiesService.isNewK8sRegion(region)) {
      return sourceType === 'app'
        ? this.appService.getK8sAppsCount(region.name)
        : this.serviceService.getK8sServicesCount(region.name);
    } else {
      return sourceType === 'app'
        ? this.appService.getAppsCount(region.name)
        : this.serviceService.getServicesCount(region.name);
    }
  }

  convertData = (data: SourceStatsCount): SerieChartData[] => {
    const OK = data.count_result.find(result => result.status === 'OK');
    const ERROR = data.count_result.find(result => result.status === 'ERROR');
    const OTHER = data.count_result.find(result => result.status === 'OTHER');
    return [
      {
        name: this.translateService.get(this.statusList[0]),
        value: OK ? OK.count : 0,
      },
      {
        name: this.translateService.get(this.statusList[1]),
        value: ERROR ? ERROR.count : 0,
      },
      {
        name: this.translateService.get(this.statusList[2]),
        value: OTHER ? OTHER.count : 0,
      },
    ];
  };

  getResourceState() {
    if (this.regionUtilitiesService.isNewK8sRegion(this.region)) {
      return 'k8s_app';
    } else {
      return `app_service.${this.config.dataType}`;
    }
  }
}
