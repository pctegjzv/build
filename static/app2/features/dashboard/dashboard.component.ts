import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { BoardsLineConfig } from 'app2/features/dashboard/boards/boards.types';
import { DEFAULT_BOARDS_CONFIG } from 'app2/features/dashboard/boards/default-boards-config';
import { Region } from 'app2/shared/services/features/region.service';
import { AppState } from 'app2/state-store/reducers';
import * as fromRegion from 'app2/state-store/region/reducers';

@Component({
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  defaultBoardsConfig: BoardsLineConfig[] = DEFAULT_BOARDS_CONFIG;
  regions$: Observable<Region[]>;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.regions$ = this.store.select(fromRegion.selectList);
  }
}
