import { NgModule } from '@angular/core';

import { ListPageModule as ClusterListPageModule } from 'app2/features/cluster/list-page/list-page.module';
import { BoardComponent } from 'app2/features/dashboard/boards/boards-line/board.component';
import { BoardsLineComponent } from 'app2/features/dashboard/boards/boards-line/boards-line.component';
import { ChartContainerComponent } from 'app2/features/dashboard/chart-container/chart-container.component';
import { AlarmChartComponent } from 'app2/features/dashboard/charts/alarm/alarm-chart.component';
import { BuildChartComponent } from 'app2/features/dashboard/charts/build/build-chart.component';
import { EventChartComponent } from 'app2/features/dashboard/charts/event/event-chart.component';
import { LogAlarmChartComponent } from 'app2/features/dashboard/charts/log-alarm/log-alarm-chart.component';
import { LogChartComponent } from 'app2/features/dashboard/charts/log/log-chart.component';
import { PipelineChartComponent } from 'app2/features/dashboard/charts/pipeline/pipeline-chart.component';
import { RegionContainerChartComponent } from 'app2/features/dashboard/charts/region-container/region-container-chart.component';
import { RegionMetricChartComponent } from 'app2/features/dashboard/charts/region-metric/region-metric-chart.component';
import { RegionNodeChartComponent } from 'app2/features/dashboard/charts/region-node/region-node-chart.component';
import { RegionRequestChartComponent } from 'app2/features/dashboard/charts/region-request/region-request-chart.component';
import { RegionStatsChartComponent } from 'app2/features/dashboard/charts/region-stats/region-stats-chart.component';
import { RegionVolumeChartComponent } from 'app2/features/dashboard/charts/region-volume/region-volume-chart.component';
import { ServiceChartComponent } from 'app2/features/dashboard/charts/service/service-chart.component';
import { DashboardComponent } from 'app2/features/dashboard/dashboard.component';
import { SharedModule } from 'app2/shared/shared.module';

import { DashboardRoutingModule } from './dashboard-routing.module';

@NgModule({
  imports: [SharedModule, DashboardRoutingModule, ClusterListPageModule],
  declarations: [
    DashboardComponent,
    BoardsLineComponent,
    BoardComponent,
    BuildChartComponent,
    LogChartComponent,
    RegionMetricChartComponent,
    RegionRequestChartComponent,
    RegionStatsChartComponent,
    LogAlarmChartComponent,
    ChartContainerComponent,
    PipelineChartComponent,
    EventChartComponent,
    AlarmChartComponent,
    RegionVolumeChartComponent,
    RegionNodeChartComponent,
    ServiceChartComponent,
    RegionContainerChartComponent,
  ],
  entryComponents: [
    BuildChartComponent,
    LogChartComponent,
    RegionMetricChartComponent,
    RegionRequestChartComponent,
    RegionStatsChartComponent,
    LogAlarmChartComponent,
    PipelineChartComponent,
    EventChartComponent,
    AlarmChartComponent,
    RegionVolumeChartComponent,
    RegionNodeChartComponent,
    ServiceChartComponent,
    RegionContainerChartComponent,
  ],
})
export class DashboardModule {}
