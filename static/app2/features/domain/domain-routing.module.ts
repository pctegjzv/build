import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DomainListComponent } from 'app2/features/domain/list/domain-list.component';

const eventRoutes: Routes = [
  {
    path: '',
    component: DomainListComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(eventRoutes)],
  exports: [RouterModule],
})
export class DomainRoutingModule {}
