import { NgModule } from '@angular/core';

import { BindProjectComponent } from 'app2/features/domain/bind-project/bind-project.component';
import { CreateDomainComponent } from 'app2/features/domain/create/create-domain.component';
import { DomainRoutingModule } from 'app2/features/domain/domain-routing.module';
import { DomainListComponent } from 'app2/features/domain/list/domain-list.component';
import { SharedModule } from 'app2/shared/shared.module';

@NgModule({
  imports: [SharedModule, DomainRoutingModule],
  declarations: [
    DomainListComponent,
    CreateDomainComponent,
    BindProjectComponent,
  ],
  entryComponents: [CreateDomainComponent, BindProjectComponent],
})
export class DomainModule {}
