import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject, from } from 'rxjs';
import { filter, first, takeUntil } from 'rxjs/operators';

import {
  DialogRef,
  DialogService,
  DialogSize,
  NotificationService,
} from 'alauda-ui';
import { Dictionary } from 'app2/core/types';
import { BindProjectComponent } from 'app2/features/domain/bind-project/bind-project.component';
import { CreateDomainComponent } from 'app2/features/domain/create/create-domain.component';
import {
  Domain,
  DomainService,
} from 'app2/shared/services/features/domain.service';
import {
  Project,
  ProjectService,
} from 'app2/shared/services/features/project.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { ResourceList } from 'app_user/core/types';

import {
  BaseResourceListComponent,
  FetchParams,
} from 'app_user/shared/abstract/base-resource-list.component';

@Component({
  templateUrl: 'domain-list.component.html',
  styleUrls: ['domain-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DomainListComponent extends BaseResourceListComponent<any>
  implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<void>();
  private projectMap: Dictionary<string> = {};
  createEnabled: boolean;
  initialized: boolean;
  pageSize = 20;

  columns = ['domain', 'type', 'project', 'action'];

  constructor(
    http: HttpClient,
    router: Router,
    activedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    private errorsToastService: ErrorsToastService,
    private notificationService: NotificationService,
    private dialogService: DialogService,
    private domainService: DomainService,
    private translateService: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private projectService: ProjectService,
    private regionService: RegionService,
  ) {
    super(http, router, activedRoute, cdr);
  }

  async ngOnInit() {
    const projects: Project[] = await this.projectService.getProjects();
    projects.map((item: Project) => {
      this.projectMap[item.uuid] = item.name;
    });
    this.regionService.region$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(region => !!region),
      )
      .subscribe(async region => {
        this.createEnabled = await this.roleUtil.resourceTypeSupportPermissions(
          'domain',
          { cluster_name: region.name },
          'create',
        );
      });
    super.ngOnInit();
    this.list$.pipe(takeUntil(this.onDestroy$)).subscribe(list => {
      if (list.length) {
        this.initialized = true;
      }
    });
  }

  fetchResources(params: FetchParams): Observable<ResourceList> {
    return from(
      this.domainService.getDomains(true, {
        page: params.pageParams.pageIndex,
        page_size: params.pageParams.pageSize,
        search: params.search,
      }),
    );
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  trackByFn(_index: number, item: Domain) {
    return item.domain;
  }

  async createDomain() {
    let modalRef: DialogRef<CreateDomainComponent>;
    try {
      modalRef = this.dialogService.open(CreateDomainComponent, {
        size: DialogSize.Medium,
      });
    } catch (e) {
      return this.errorsToastService.error(e);
    }
    modalRef.componentInstance.close
      .pipe(first())
      .subscribe(async (res: Domain) => {
        modalRef.close();
        if (!res) {
          return;
        }
        try {
          await this.domainService.createDomain(res);
        } catch (err) {
          return this.errorsToastService.error(err);
        }
        this.notificationService.success({
          title: this.translateService.get('success'),
          content: this.translateService.get('create_domain_success', {
            name: res.domain || '',
          }),
        });
        this.onUpdate(null);
      });
  }

  bindProject(domain: Domain) {
    let modalRef: DialogRef<BindProjectComponent>;
    try {
      modalRef = this.dialogService.open(BindProjectComponent, {
        size: DialogSize.Medium,
        data: {
          projects: domain.projects,
        },
      });
    } catch (e) {
      return this.errorsToastService.error(e);
    }
    modalRef.componentInstance.close
      .pipe(first())
      .subscribe(async (res: string[]) => {
        modalRef.close();
        if (!res) {
          return;
        }
        try {
          await this.domainService.updateDomainBindProject(
            domain.domain_id,
            res,
          );
        } catch (err) {
          return this.errorsToastService.error(err);
        }

        this.notificationService.success({
          title: this.translateService.get('success'),
          content: this.translateService.get('bind_project_success', {
            name: domain.domain || '',
          }),
        });
        this.onUpdate(null);
      });
  }

  pageChanged(page: number) {
    this.onPageEvent({
      pageIndex: page,
      pageSize: this.pageSize,
    });
  }

  canShowUpdate(item: Domain) {
    return this.roleUtil.resourceHasPermission(item, 'domain', 'update');
  }

  canShowDelete(item: Domain) {
    return this.roleUtil.resourceHasPermission(item, 'domain', 'delete');
  }

  getDomainType(item: Domain) {
    return item.domain.startsWith('*.') ? 'extensive_domain' : 'domain';
  }

  getDomainProjectName(item: Domain) {
    switch (item.projects[0]) {
      case '*':
        return this.translateService.get('all_project');
        break;
      case undefined:
        return this.translateService.get('no_project');
        break;
      default:
        return this.projectMap[item.projects[0]] || item.projects[0];
    }
  }

  async delete(domain: Domain) {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('delete'),
        content: this.translateService.get('delete_domain_confirm', {
          name: domain.domain,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
      await this.domainService.deleteDomain(domain.domain_id);
    } catch (err) {
      return this.errorsToastService.error(err);
    }

    this.notificationService.success({
      title: this.translateService.get('success'),
      content: this.translateService.get('delete_domain_success', {
        name: domain.domain || '',
      }),
    });
    this.onUpdate(null);
  }
}
