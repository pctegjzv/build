import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UpgradeModule, setAngularJSGlobal } from '@angular/upgrade/static';

import { BuildUtilsService } from 'app2/features/build/build-utils.service';
import { BuildCiConfigComponent } from 'app2/features/build/detail/build-ci-config.component';
import { BuildCodeRepositoryComponent } from 'app2/features/build/detail/build-code-repository.component';
import { BuildDetailComponent } from 'app2/features/build/detail/build-detail.component';
import { BuildImageRepositoryComponent } from 'app2/features/build/detail/build-image-repository.component';
import { BuildSonarqubeComponent } from 'app2/features/build/detail/build-sonarqube.component';
import { BuildHistoryCodeComponent } from 'app2/features/build/history/build-history-code.component';
import { BuildHistoryDetailComponent } from 'app2/features/build/history/build-history-detail.component';
import { BuildHistoryLogComponent } from 'app2/features/build/history/build-history-log.component';
import { BuildHistoryRepositoryComponent } from 'app2/features/build/history/build-history-repository.component';
import { BuildListComponent } from 'app2/features/build/list/build-list.component';
import { BuildCodeQualitySonarqubeComponent } from 'app2/features/build/sonarqube/build-code-quality-sonarqube.component';
import { BuildSonarqubeLogComponent } from 'app2/features/build/sonarqube/build-sonarqube-log.component';
import { BuildImageDowngradeSharedModule } from 'app2/features/lazy/build-image-downgrade.shared.module';
import { SharedModule } from 'app2/shared/shared.module';

import '../../../app/vendor.entry';
// tslint:disable-next-line:ordered-imports
import '../../../app/app.entry';

import { DowngradeComponent } from './downgrade.component';
import './downgrades';

setAngularJSGlobal(angular);

const downgradedComponents = [
  BuildListComponent,
  BuildCiConfigComponent,
  BuildCodeRepositoryComponent,
  BuildDetailComponent,
  BuildImageRepositoryComponent,
  BuildSonarqubeComponent,
  BuildHistoryCodeComponent,
  BuildHistoryDetailComponent,
  BuildHistoryLogComponent,
  BuildHistoryRepositoryComponent,
  BuildCodeQualitySonarqubeComponent,
  BuildSonarqubeLogComponent,
];

@NgModule({
  imports: [
    UpgradeModule,
    SharedModule,
    BuildImageDowngradeSharedModule,
    RouterModule.forChild([
      {
        path: '**',
        component: DowngradeComponent,
      },
    ]),
  ],
  declarations: [DowngradeComponent, ...downgradedComponents],
  providers: [BuildUtilsService],
  entryComponents: downgradedComponents,
})
export class DowngradeModule {}
