import { LocationStrategy } from '@angular/common';
import { Router } from '@angular/router';
import {
  downgradeComponent,
  downgradeInjectable,
} from '@angular/upgrade/static';
import { NotificationService } from 'alauda-ui';
import angular from 'angular';

import { getGlobal } from 'app2/app-global';
import { VendorCustomerService } from 'app2/core/customization/vendor-customer.service';
import { RouterUtilService } from 'app2/core/services/router-util.service';
import {
  ENVIRONMENTS,
  EXAMPLE_YAML,
  INSTANCE_SIZE_CONFIG,
  NAV_CONFIG,
  TRANSLATIONS,
  WEBLABS,
} from 'app2/core/tokens';
import { BuildDetailComponent } from 'app2/features/build/detail/build-detail.component';
import { BuildHistoryDetailComponent } from 'app2/features/build/history/build-history-detail.component';
import { BuildHistoryComponent } from 'app2/features/build/history/build-history.component';
import { BuildListComponent } from 'app2/features/build/list/build-list.component';
import { BuildTriggerComponent } from 'app2/features/build/trigger/build.trigger.component';
import { RegionBadgeOptionService } from 'app2/shared/directives/region-badge-option/region-badge-option.service';
import { ProjectService } from 'app2/shared/services/features/project.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { DefaultErrorMapperService } from 'app2/shared/services/form/default-error-mapper.service';
import { TranslateService } from 'app2/translate/translate.service';
import { getLocale } from 'app2/utils/bootstrap';

import { ANGULAR_DOWNGRADE_MODULE_NAME } from '../../../app/app.constant';
import { registerConfig } from '../../../app/core/app.bootstrap';

// semantic-ui dropdown plugin conflicts with bootstrap dropdown plugin
// rename bootstrap dropdown plugin to bootstrapDropdown
($.fn as any).bootstrapDropdown = ($.fn as any).dropdown.noConflict();
($.fn as any).bootstrapModal = ($.fn as any).modal.noConflict();

registerConfig('CUSTOM_EXAMPLE_YAML', getGlobal(EXAMPLE_YAML));
registerConfig('CUSTOM_INSTANCE_SIZE_CONFIG', getGlobal(INSTANCE_SIZE_CONFIG));
registerConfig('ENVIRONMENTS', getGlobal(ENVIRONMENTS));
registerConfig('NAV_CONFIG', getGlobal(NAV_CONFIG));
registerConfig('TRANSLATIONS', {
  getLocale,
  translations: () => getGlobal(TRANSLATIONS),
});
registerConfig('WEBLABS', getGlobal(WEBLABS));

const downgrade = angular.module(ANGULAR_DOWNGRADE_MODULE_NAME);

const downgradableServices = {
  ngLocationStrategy: LocationStrategy,
  ngRouter: Router,
  rb2DefaultErrorMapperService: DefaultErrorMapperService,
  rb2RouterUtil: RouterUtilService,
  rbCustomerService: VendorCustomerService,
  rbProjectService: ProjectService,
  rbRegionBadgeOptionService: RegionBadgeOptionService,
  rbRegionService: RegionService,
  rbRegionUtilities: RegionUtilitiesService,
  rbToast: NotificationService,
  translateService: TranslateService,
};

const downgradableComponents = {
  rbBuildHistoryList: BuildHistoryComponent,
  rbTriggerBuild: BuildTriggerComponent,
  rbBuildConfigList: BuildListComponent,
  rbBuildConfigDetail: BuildDetailComponent,
  rbBuildHistoryDetail: BuildHistoryDetailComponent,
};

Object.entries(downgradableServices).forEach(([name, Service]) =>
  downgrade.factory(name, downgradeInjectable(Service)),
);

Object.entries(downgradableComponents).forEach(([name, component]) =>
  downgrade.directive(
    name,
    downgradeComponent({
      component,
    }),
  ),
);

downgrade.component('rbRegionBadgeOption', {
  controller: [
    'rbRegionBadgeOptionService',
    '$element',
    function(
      rbRegionBadgeOptionService: RegionBadgeOptionService,
      $element: JQLite,
    ) {
      this.$onInit = () => {
        rbRegionBadgeOptionService.setRegionBadgeOptionByDirective(this);
        this.id = rbRegionBadgeOptionService.optionCounter;
        $element.css('display', 'none');
      };

      this.$onDestroy = () => {
        if (this.id === rbRegionBadgeOptionService.optionCounter) {
          rbRegionBadgeOptionService.setRegionBadgeOption(
            rbRegionBadgeOptionService.getDefaultOption(),
          );
        }
      };
    },
  ],
  template: 'A placeholder element to ',
  bindings: {
    onChangeState: '<',
    onChangeConfirm: '<',
    filterMode: '<',
    option: '<',
  },
});

type Scope = angular.IScope & {
  routerLink: string | string[];
  queryParams?: {
    [key: string]: any;
  };
  replaceUrl?: boolean;
};

downgrade.directive<Scope>('routerLink', [
  'ngLocationStrategy',
  'ngRouter',
  (ngLocationStrategy: LocationStrategy, ngRouter: Router) => ({
    restrict: 'A',
    scope: {
      routerLink: '=',
      queryParams: '=',
      replaceUrl: '=',
    },
    link: (() => {
      const build = (scope: Scope, $el: JQLite) => {
        let { routerLink } = scope;
        const { queryParams, replaceUrl } = scope;

        if (!Array.isArray(routerLink)) {
          routerLink = [routerLink];
        }

        const isLink = $el.prop('tagName') === 'A';

        if (isLink) {
          $el.attr(
            'href',
            ngLocationStrategy.prepareExternalUrl(
              ngRouter.serializeUrl(
                ngRouter.createUrlTree(routerLink, {
                  queryParams,
                }),
              ),
            ),
          );
        }

        return { routerLink, queryParams, replaceUrl, isLink };
      };

      return (scope: Scope, $el: JQLite) => {
        let { routerLink, queryParams, replaceUrl, isLink } = build(scope, $el);

        scope.$watch(
          (scope: Scope) => ({
            routerLink: scope.routerLink,
            queryParams: scope.queryParams,
            replaceUrl: scope.replaceUrl,
          }),
          () => {
            const built = build(scope, $el);
            routerLink = built.routerLink;
            queryParams = built.queryParams;
            replaceUrl = built.replaceUrl;
            isLink = built.isLink;
          },
          true,
        );

        $el.one('click', e => {
          if (isLink) {
            e.preventDefault();
          }
          ngRouter.navigate(routerLink as string[], {
            queryParams,
            replaceUrl,
          });
        });
      };
    })(),
  }),
]);
