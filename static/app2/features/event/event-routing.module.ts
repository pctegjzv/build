import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventListComponent } from 'app2/features/event/list/event-list.component';

const eventRoutes: Routes = [
  {
    path: '',
    component: EventListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(eventRoutes)],
  exports: [RouterModule],
})
export class EventRoutingModule {}
