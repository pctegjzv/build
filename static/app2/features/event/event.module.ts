import { NgModule } from '@angular/core';

import { EventRoutingModule } from 'app2/features/event/event-routing.module';
import { SharedModule } from 'app2/shared/shared.module';

@NgModule({
  imports: [SharedModule, EventRoutingModule],
})
export class EventModule {}
