import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { get } from 'lodash';
import moment from 'moment';

import { RouterUtilService } from 'app2/core/services/router-util.service';
import { Event } from 'app2/features/event/event.types';
import { EventService } from 'app2/shared/services/features/event.service';
import { LoggerUtilitiesService } from 'app2/shared/services/utility/logger.service';
import { TranslateService } from 'app2/translate/translate.service';

interface Params {
  [name: string]: any;
}

@Component({
  selector: 'rc-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss'],
})
export class EventListComponent implements OnInit, OnDestroy {
  @Input()
  type: string; // 1. resource type: 'service'  2. multiple resource types: 'service,region,node'
  @Input()
  primaryKey: string; // resource uuid
  @Input()
  polling = true;
  @Input()
  pollingInterval: number = 30 * 1000;
  loading = false;
  pagination: any = {
    count: 0,
    pageSize: 20,
    currentPage: 1,
    disabled: false,
  };
  keyword = '';
  events: Array<any> = [];
  expandedEvents: Array<any> = [];
  eventStateMap: any = {
    '0': 'success',
    '1': 'warning',
    '2': 'error',
  };

  notFoundTips = '';

  destroyed = false;

  pollingTimer: any;

  constructor(
    private eventService: EventService,
    private routerUtil: RouterUtilService,
    private logger: LoggerUtilitiesService,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,
  ) {
    this.notFoundTips = this.translate.get('event_not_found_tips');
  }

  ngOnInit() {
    this.loadEvents();
  }

  ngOnDestroy() {
    clearTimeout(this.pollingTimer);
    this.destroyed = true;
  }

  private resetPollingTimer() {
    clearTimeout(this.pollingTimer);
    this.pollingTimer = setTimeout(() => {
      if (this.pagination.currentPage === 1) {
        this.loadEvents();
      }
    }, this.pollingInterval);
  }

  /**
   * Search handler
   *
   * @param {string} keyword
   * @memberof EventListComponent
   */
  search(keyword: string): void {
    this.keyword = keyword;
    // reset current page
    this.pagination.currentPage = 1;
    this.loadEvents();
  }

  /**
   * Page change handler
   *
   * @param {number} pageNumber
   * @memberof EventListComponent
   */
  pageChanged(pageNumber: number) {
    this.pagination.currentPage = pageNumber;
    this.loadEvents();
  }

  /**
   * Load events
   */
  async loadEvents() {
    // init query
    if (this.loading) {
      return;
    }
    this.loading = true;
    const now = moment();
    const start = now
      .clone()
      .startOf('day')
      .subtract(29, 'days');
    const query = {
      start_time: start.valueOf() / 1000,
      end_time: now.valueOf() / 1000,
      pageno: this.pagination.currentPage,
      size: this.pagination.pageSize,
    };
    if (this.primaryKey) {
      query['event_pk'] = this.primaryKey;
    }
    if (this.type) {
      const types = this.type.split(',');
      const typeParamName = types.length > 1 ? 'event_types' : 'event_type';
      query[typeParamName] = this.type;
    }
    if (this.keyword) {
      query['query_string'] = this.keyword;
    }

    // cache current events state
    this.expandedEvents = this.events.length
      ? this.events.filter(event => event.expanded)
      : [];

    // start query events
    try {
      const data = await this.eventService.getEvents(query);
      this.pagination.count =
        data.total_items > 10000 ? 10000 : data.total_items;
      this.events = this.handleEvents(data.results);
      if (this.polling) {
        this.resetPollingTimer();
      }
    } catch (err) {
      this.logger.error(err);
    } finally {
      this.loading = false;
      if (!this.destroyed) {
        this.cdr.detectChanges();
      }
    }
  }

  /**
   * Handle loaded events
   */
  handleEvents(events: Array<any>) {
    return events.map((event: Event) => {
      // detail text
      event.jsonDetail = JSON.stringify(event, null, 4);
      // set expand state
      event.expanded = this.expandedEvents.find(expandedEvent => {
        return (
          expandedEvent.resource_id === event.resource_id &&
          expandedEvent.time === event.time
        );
      });
      // event time
      event.formattedTime = moment(event.time * 1000).format(
        'YYYY-MM-DD HH:mm:ss',
      );
      // event message
      this.generateMessage(event);
      return event;
    });
  }

  href(stateName: string, params?: Params) {
    return this.routerUtil.getLinkUrl(stateName.split('.').join('/'), params);
  }

  /**
   * Generate message
   * @param event
   */
  generateMessage(event: Event) {
    let message = '';
    let sublink = '';
    let resource = '';
    let parentResource = '';
    const detail = event.detail;
    const stateItem = this.getStateItem(event);
    const href = stateItem.routerLink
      ? this.routerUtil.getLinkUrl(stateItem.routerLink, stateItem.params)
      : this.href(stateItem.name);
    let link = `<a href=${href}>${event.resource_name}</a>`;
    switch (event.template_id) {
      case 'generic':
        if (
          [
            'project',
            'project_template',
            'role',
            'log_filter',
            'namespace',
            'certificate',
            'svc_binding',
            'k8s_resourcequotas',
            'priv_regis_proj',
          ].includes(event.resource_type)
        ) {
          link = event.resource_name;
        }
        if (event.resource_type === 'k8s_networkpolicies') {
          const [
            cluster_id,
            ,
            network_policy_namespace,
            network_policy_name,
          ] = event.resource_id.split('_');
          // resource_id of network_policy is ${cluster_id}_networkpolicies_${namespace}_${name}
          this.href('network.network_policy', {
            cluser_id: cluster_id,
            namespace: network_policy_namespace,
            name: network_policy_name,
          });
        }
        message += this.generateSubMessage(
          event.detail.operator,
          'event_' + event.detail.operation,
          event.resource_type,
          link,
        );
        break;
      case 'sub_resource':
        if (event.detail.parent) {
          resource = this.getResourceLink(event);
          parentResource = this.getResourceLink(event.detail.parent, true);
          message += this.generateSubMessage(
            event.detail.operator,
            `event_${event.detail.operation}`,
            event.detail.parent.resource_type,
            parentResource,
            this.translate.get('event_of'),
            event.resource_type,
            resource,
          );
        }
        break;
      case 'build':
        link = `<a href=${href}>${event.resource_id}</a>`;
        message += this.generateSubMessage(
          event.detail.operator,
          'event_' + event.detail.operation,
          event.resource_type,
          link + this.translate.get('event_of'),
          'build',
          event.resource_name,
        );
        break;
      case 'build_result':
        sublink = `<a href="${this.getSubLink(event.detail)}">${
          event.detail.sub_resource_name
        }</a>`;
        message += this.generateSubMessage(
          event.resource_type,
          link + this.translate.get('event_of'),
          'build',
          sublink,
          'event_' + event.detail.operation,
          event.detail.status,
        );
        break;
      case 'build_v2':
        sublink = `<a href="${this.getSubLink(event.detail)}">${
          event.detail.sub_resource_name
        }</a>`;
        message += this.generateSubMessage(
          event.detail.operator,
          'event_' + event.detail.operation,
          event.resource_type,
          link + this.translate.get('event_of'),
          'build',
          sublink,
        );
        break;
      case 'repo_tag':
        message += this.generateSubMessage(
          event.detail.operator,
          'event_' + event.detail.operation,
          event.resource_type,
          link + this.translate.get('event_of'),
          event.detail.sub_resource_type,
          `<span>${event.detail.sub_resource_name}</span>`,
        );
        break;
      case 'node':
        message += this.generateSubMessage(
          event.detail.operator,
          'event_' + event.detail.operation,
          event.resource_type,
          event.namespace +
            '/' +
            event.detail.region_display_name +
            this.translate.get('event_of'),
          link,
        );
        break;
      case 'result':
        message += this.generateSubMessage(
          event.resource_type,
          link,
          event.detail.operation,
          event.detail.status,
        );
        break;
      case 'service_status':
      case 'region_status':
        message += this.generateSubMessage(
          event.resource_type,
          link,
          'entered',
          `status_${event.detail.status}`,
          'status',
        );
        break;
      case 'app_task_result':
        const links: string[] = [];
        let linkStr = '';
        event.detail[`sub_resource_id`]
          .split(',')
          .forEach((e: string, i: number) => {
            // if there's not any service
            if (
              event.detail.operation === 'create' &&
              event.detail.status === 'failure'
            ) {
              links.push(e);
            } else {
              const href = this.href('app_service.service.service_detail', {
                service_name: e,
              });
              links.push(`
              <a href="${href}">${
                event.detail.sub_resource_name.split(',')[i]
              }</a>
            `);
            }
          });
        linkStr = links.join(',&nbsp;');
        message += this.generateSubMessage(
          `event_${event.resource_type}`,
          link,
          'event_of',
          event.detail.sub_resource_type,
          linkStr,
          `event_result_${event.detail.operation}`,
          `status_${event.detail.status}`,
        );
        break;
      case 'permission-change':
        message += this.generateSubMessage(
          detail.operator,
          'event_' + detail.operation,
          detail.object_type,
          detail['object'],
          'event_for',
          event.resource_type,
          link + this.translate.get('event_of'),
          detail.sub_attr_type,
          detail.sub_attr_name,
        );
        break;
      case 'team-update':
        message += this.generateSubMessage(
          detail.operator,
          'event_' + detail.operation,
          event.resource_type,
          link + this.translate.get('event_of'),
          detail.sub_attr_type,
          'event_to',
          detail.sub_attr_value,
        );
        break;
      case 'generic-svoa':
        if (event.resource_type === 'role') {
          link = event.resource_name;
        } else {
        }
        message += this.generateSubMessage(
          detail.operator,
          'event_' + detail.operation,
          event.resource_type,
          link + this.translate.get('event_of'),
          detail.attribute_type,
          detail.attribute,
        );
        break;
      case 'registry-gc':
        message += this.translate.get('event_registry_gc', {
          href,
          resource_name: event.resource_name,
        });
        break;
      case 'registry-gc-completed':
        message += this.translate.get('event_registry_gc_completed', {
          href,
          resource_name: event.resource_name,
        });
        break;
      default:
        break;
    }
    event.message = message;
  }

  generateSubMessage(...terms: Array<any>) {
    return terms
      .map(term => {
        if (term === 'service') {
          term = 'component';
        }
        return /<a/.test(term) || !term
          ? term
          : this.translate.get(term.toLowerCase());
      })
      .join(this.translate.get('event_term_delimiter'));
  }

  getSubLink(eventDetail: any): string {
    switch (eventDetail.sub_resource_type) {
      case 'build':
        return this.href('image.build.detail', {
          build_id: eventDetail.sub_resource_id,
        });
      default:
        return '';
    }
  }

  getResourceLink(event: Event, isParent = false): string {
    if (!event) {
      return;
    }
    switch (event.resource_type) {
      case 'priv_build':
        return `<a href="${this.routerUtil.getLinkUrl('/build/history/detail', {
          id: event.resource_id,
        })}">${event.resource_id}</a>`;
      case 'pipeline_history':
        return `<a href="${this.routerUtil.getLinkUrl(
          '/pipeline/history/detail',
          {
            pipelineName: event.detail.parent.resource_id,
            historyId: event.resource_id,
          },
        )}">${event.resource_id}</a>`;
      case 'sync_regis_conf':
        return `<a href="${this.routerUtil.getLinkUrl(
          '/image/sync-center/detail/' + event.resource_id,
        )}">${event.resource_name}</a>`;
      case 'sync_regis_history':
        return `<a href="${this.routerUtil.getLinkUrl(
          '/image/sync-history/detail/' + event.resource_id,
        )}">${event.resource_id}</a>`;
      case 'job_history':
        return `<a href="${this.routerUtil.getLinkUrl(
          '/job/history/detail/' + event.resource_id,
        )}">${event.resource_id}</a>`;
      case 'node':
        return `<a href="${this.routerUtil.getLinkUrl(
          '/cloud/instance/list',
        )}">${event.resource_name}</a>`;
      case 'permission':
        return '';
      // is parent
      case 'builds_config':
        return `<a href="${this.routerUtil.getLinkUrl('/build/config/detail', {
          name: event.resource_id,
        })}">${event.resource_name}</a>`;
      case 'pipeline_config':
        return `<a href="${this.routerUtil.getLinkUrl(
          '/pipeline/config/detail',
          {
            name: event.resource_id,
          },
        )}">${event.resource_name}</a>`;
      case 'job_config':
        return `<a href="${this.routerUtil.getLinkUrl(
          '/job/config/detail/' + event.resource_id,
        )}">${event.resource_name}</a>`;
      case 'cloud_account':
        return `<a href="${this.routerUtil.getLinkUrl(
          '/cloud/account/list',
        )}">${event.resource_name}</a>`;
      case 'role':
        return event.resource_name;
      case 'jenkins_pipeline':
        return `<a href="${this.routerUtil.getLinkUrl(
          `/jenkins/pipelines/${event.resource_id}`,
        )}">${event.resource_name}</a>`;
      case 'jenkins_pipeline_history':
        return `<a href="${this.routerUtil.getLinkUrl(
          `/jenkins/histories/${event.detail.parent.resource_id}/${
            event.resource_id
          }`,
        )}">${event.resource_name}</a>`;
      default:
        return isParent ? event.resource_name : event.resource_id;
    }
    return '';
  }

  /**
   * Get state info: state name and state params
   *
   * @param {*} event
   * @returns
   * @memberof EventListComponent
   */
  getStateItem(event: Event) {
    const name = 'app_service.app.list';
    let routerLink;
    let params = null;
    const region_platform_version: string = get(
      event,
      'detail.platform_version',
      '',
    );
    switch (event.resource_type.toLowerCase()) {
      case 'service':
        if (+region_platform_version.substr(1) > 2) {
          routerLink = '/k8s_app/detail/component/' + event.resource_id;
        } else {
          routerLink =
            '/app_service/service/service_detail/' + event.resource_id;
        }
        break;
      case 'application':
        if (+region_platform_version.substr(1) > 2) {
          routerLink = '/k8s_app/detail/' + event.resource_id;
        } else {
          routerLink = '/app_service/app/app_detail/' + event.resource_id;
        }
        break;
      case 'node':
        routerLink = `/cluster/node_detail/${event.detail.region_name}/${
          event.resource_name
        }`;
        break;
      case 'notification':
        routerLink = `/notification/detail/${event.resource_id}`;
        break;
      case 'alarm':
        routerLink = `/alarm/alarm_detail/${event.resource_id}`;
        break;
      case 'log_alarm':
        routerLink = `/alarm/log_alarm_detail/${event.resource_id}`;
        break;
      case 'repository':
        routerLink = '/image/repository/detail';
        params = {
          repositoryName: event.resource_name,
          registryName: event.detail.registry_name,
        };
        break;
      case 'repo':
        routerLink = '/image/repository/detail';
        params = {
          repo_ns: event.namespace,
          repositoryName: event.resource_name,
        };
        break;
      case 'priv_registry':
        routerLink = '/image/repository';
        params = {
          registryName: event.resource_name,
        };
        break;
      case 'priv_regis_repo':
        routerLink = `/image/repository/detail?repositoryName=${
          event.resource_name
        }&registryName=${event.detail.registry_name}&projectName=${
          event.detail.project_name
        }`;
        break;
      case 'sync_regis_conf':
        routerLink = '/image/sync-center/detail/' + event.resource_id;
        break;
      case 'sync_regis_history':
        routerLink = '/image/sync-history/detail/' + event.resource_id;
        break;
      case 'build':
        routerLink = '/image/build/detail/' + event.resource_id;
        break;
      case 'env_file':
      case 'envfile':
        routerLink = '/app_service/envfile/detail';
        params = {
          envfileName: event.resource_id,
        };
        break;
      case 'template':
      case 'application_temp':
        routerLink = '/app_service/template/detail/' + event.resource_id;
        break;
      case 'config':
        routerLink = '/app_service/configuration/detail';
        params = {
          name: event.resource_id,
        };
        break;
      case 'team':
        routerLink = '/org/team';
        params = {
          orgName: event.namespace,
          teamName: event.resource_name,
        };
        break;
      case 'organization':
      case 'ldap_config':
      case 'ldap_sync':
        routerLink = '/org';
        params = {
          orgName: event.namespace,
        };
        break;
      case 'pipeline':
      case 'pipeline_config':
        routerLink = '/pipeline/config/detail';
        params = {
          name: event.resource_id,
        };
        break;
      case 'pipeline_history':
        routerLink = '/pipeline/history/detail';
        params = {
          pipelineName: event.resource_id,
          historyId: event.resource_id,
        };
        break;
      case 'builds_config':
        routerLink = '/build/config/detail';
        params = {
          name: event.resource_id,
        };
        break;
      case 'cloud_account':
      case 'cmp':
        routerLink = 'cloud/account/list';
        break;
      case 'volume':
        routerLink = `/storage/volume/detail/${event.resource_id}`;
        break;
      case 'snapshot':
        routerLink = '/storage/snapshot';
        break;
      case 'region':
        routerLink = `/cluster/detail/${event.resource_name}`;
        break;
      case 'feature':
        routerLink = '/cluster/detail';
        params = {
          name: event.detail.region_name,
        };
        break;
      case 'dashboard':
        routerLink = `/monitor/dashboard/detail/${event.resource_id}`;
        params = {
          display_name: event.resource_name,
          crumbParams: 'display_name',
        };
        break;
      case 'space':
      case 'quota':
        routerLink = '/quota/detail/' + event.resource_name;
        break;
      case 'role':
        routerLink = '/org/role_detail';
        params = {
          roleName: event.resource_name,
        };
        break;
      case 'lb':
      case 'load_balancer':
        routerLink = `/load_balancer/detail/${event.resource_name}`;
        break;
      case 'job_config':
        routerLink = `/job/config/detail/${event.resource_id}`;
        break;
      case 'job_history':
        routerLink = `/job/history/detail/${event.resource_id}`;
        break;
      case 'integration':
        routerLink = `/integration_center/detail/${event.resource_id}`;
        break;
      case 'configmap': {
        routerLink = `/k8s_configmap/detail/${event.detail.region_id}/${
          event.resource_name
        }`;
        break;
      }
      case 'k8s_networkpolicies':
        routerLink = `/network_policy/detail/${event.detail.region_id}/${
          event.resource_name
        }`;
        break;
      case 'pv':
      case 'persistentvolume':
        routerLink = `/storage/pv/detail/${event.resource_name}`;
        break;
      case 'pvc':
      case 'persistentvolumeclaim':
        routerLink = `/storage/pvc/detail/${event.detail.region_id}/${
          event.resource_name
        }`;
        break;
      case 'svc_broker':
        routerLink = `service_catalog/service_broker/broker_detail/${
          event.resource_id
        }`;
        break;
      case 'svc_instance':
        routerLink = `service_catalog/service_instance/instance_detail/${
          event.resource_id
        }`;
        break;
      case 'helm_template_repo':
        routerLink = `app-catalog/repository/repo_detail/${event.resource_id}`;
        break;
      case 'jenkins_pipeline':
        routerLink = `/jenkins/pipelines/${event.resource_id}`;
        break;
      case 'jenkins_pipeline_template_source':
        routerLink = `/jenkins/templates`;
        break;
      case 'jenkins_pipeline_history':
        routerLink = `/jenkins/histories/${event.detail.pipeline_uuid}/${
          event.resource_name
        }`;
        break;
      case 'k8s_storageclasses':
        routerLink = `/storage/storageclass/detail/${event.resource_name}`;
        break;
    }
    return {
      name,
      routerLink,
      params,
    };
  }

  /**
   * Show or hide event's detail
   *
   * @param {*} event
   * @memberof EventListComponent
   */
  expandEvent(event: any, e: any): void {
    if (e.target.tagName === 'A') {
      return;
    }
    event.expanded = !event.expanded;
  }
}
