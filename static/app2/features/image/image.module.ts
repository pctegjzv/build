import { NgModule } from '@angular/core';

import { CustomInstanceSizeComponent } from 'app2/features/image/sync-center/custom-instance-size/custom-instance-size.component';
import { ImageSyncTriggerComponent } from 'app2/features/image/sync-center/image-sync-trigger.component';
import { RepositoryDropdownGroupComponent } from 'app2/features/image/sync-center/repository-dropdown-group/repository-dropdown-group.component';
import { ThirdPartyRegistriesComponent } from 'app2/features/image/sync-center/third-party-registries/third-party-registries.component';
import { SharedModule } from 'app2/shared/shared.module.ts';

import { BuildImageDowngradeSharedModule } from '../lazy/build-image-downgrade.shared.module';

import { ImageRoutingModule } from './image.routing.module';
import { ImageService } from './image.service';
import { ImageRepositoryCardComponent } from './repository/card/image-repository-card.component';
import { ImageRepositoryTagLevelLabelComponent } from './repository/common/image-repository-tag-level-label.component';
import { ImageRepositoryCreateComponent } from './repository/create/image-repository-create.component';
import { ImageRepositoryDetailComponent } from './repository/detail/image-repository-detail.component';
import { ImageRepositoryHelpComponent } from './repository/help/image-repository-help.component';
import { ImageRepositoryListComponent } from './repository/list/image-repository-list.component';
import { ImageRepositoryAddProjectComponent } from './repository/projects/image-repository-add-project.component';
import { ImageRepositoryGarbageComponent } from './repository/projects/image-repository-garbage.component';
import { ImageRepositoryProjectsComponent } from './repository/projects/image-repository-projects.component';
import { ImageRepositoryScanComponent } from './repository/projects/image-repository-scan.component';
import { ImageRepositoryTagMaxNumberComponent } from './repository/projects/image-repository-tag-max-number.component';
import { ImageRepositoryTagArtifactsComponent } from './repository/tags/image-repository-tag-artifacts.component';
import { ImageRepositoryTagsComponent } from './repository/tags/image-repository-tags.component';
import { ImageRepositoryVulnerabilityComponent } from './repository/vulnerability/image-repository-vulnerability.component';
import { ImageSyncCenterComponent } from './sync-center/image-sync-center.component';
import { ImageSyncConfigComponent } from './sync-center/image-sync-config.component';
import { ImageSyncCreateComponent } from './sync-center/image-sync-create.component';
import { ImageSyncHistoryDetailComponent } from './sync-history/image-sync-history-detail.component';
import { ImageSyncHistoryLogComponent } from './sync-history/image-sync-history-log.component';
import { ImageSyncHistoryComponent } from './sync-history/image-sync-history.component';

@NgModule({
  imports: [SharedModule, BuildImageDowngradeSharedModule, ImageRoutingModule],
  providers: [ImageService],
  declarations: [
    ImageRepositoryListComponent,
    ImageRepositoryProjectsComponent,
    ImageRepositoryAddProjectComponent,
    ImageRepositoryCardComponent,
    ImageRepositoryCreateComponent,
    ImageRepositoryDetailComponent,
    ImageRepositoryTagLevelLabelComponent,
    ImageRepositoryTagsComponent,
    ImageRepositoryTagArtifactsComponent,
    ImageRepositoryVulnerabilityComponent,
    ImageRepositoryHelpComponent,
    ImageSyncCenterComponent,
    ImageSyncConfigComponent,
    ImageSyncHistoryComponent,
    ImageSyncHistoryDetailComponent,
    ImageSyncHistoryLogComponent,
    ImageRepositoryScanComponent,
    ImageRepositoryTagMaxNumberComponent,
    ImageRepositoryGarbageComponent,
    ImageSyncCreateComponent,
    ImageSyncTriggerComponent,
    RepositoryDropdownGroupComponent,
    ThirdPartyRegistriesComponent,
    CustomInstanceSizeComponent,
  ],
  entryComponents: [
    ImageRepositoryAddProjectComponent,
    ImageRepositoryScanComponent,
    ImageRepositoryTagMaxNumberComponent,
    ImageRepositoryGarbageComponent,
    ImageSyncCreateComponent,
    ImageSyncTriggerComponent,
  ],
})
export class ImageModule {}
