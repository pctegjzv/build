import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ImageRepositoryCreateComponent } from './repository/create/image-repository-create.component';
import { ImageRepositoryDetailComponent } from './repository/detail/image-repository-detail.component';
import { ImageRepositoryHelpComponent } from './repository/help/image-repository-help.component';
import { ImageRepositoryListComponent } from './repository/list/image-repository-list.component';
import { ImageRepositoryVulnerabilityComponent } from './repository/vulnerability/image-repository-vulnerability.component';
import { ImageSyncCenterComponent } from './sync-center/image-sync-center.component';
import { ImageSyncConfigComponent } from './sync-center/image-sync-config.component';
import { ImageSyncHistoryDetailComponent } from './sync-history/image-sync-history-detail.component';
import { ImageSyncHistoryComponent } from './sync-history/image-sync-history.component';

const routes: Routes = [
  {
    path: 'repository',
    component: ImageRepositoryListComponent,
  },
  {
    path: 'repository/create',
    component: ImageRepositoryCreateComponent,
    pathMatch: 'full',
  },
  {
    path: 'repository/edit',
    component: ImageRepositoryCreateComponent,
    pathMatch: 'full',
  },
  {
    path: 'repository/detail',
    component: ImageRepositoryDetailComponent,
  },
  {
    path: 'repository/vulnerability',
    component: ImageRepositoryVulnerabilityComponent,
  },
  {
    path: 'repository/help',
    component: ImageRepositoryHelpComponent,
  },
  {
    path: 'sync-center',
    component: ImageSyncCenterComponent,
  },
  {
    path: 'sync-center/detail/:configName',
    component: ImageSyncConfigComponent,
  },
  {
    path: 'sync-history',
    component: ImageSyncHistoryComponent,
  },
  {
    path: 'sync-history/detail/:historyId',
    component: ImageSyncHistoryDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ImageRoutingModule {}
