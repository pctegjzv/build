import { Inject, Injectable } from '@angular/core';

import { NotificationService } from 'alauda-ui';
import { ACCOUNT, WEBLABS } from 'app2/core/tokens';
import { RcAccount, Weblabs } from 'app2/core/types';
import { HttpService } from 'app2/shared/services/http/http.service';
import { TranslateService } from 'app2/translate/translate.service';

export interface SyncRegistryConfig {
  config_id: string;
  display_name?: string;
  config_name: string;
  space_name: string;
  cpu: number;
  memory: number;
  dest: Array<{
    dest_id?: string;
    endpoint?: string;
    dest_type: string;
    internal_id: string;
  }>;
  source: {
    info: {
      repository_name: string;
      project_name: string;
      registry_name: string;
    };
  };
  uuid?: string;
  resource_actions: string[];
}

export interface SyncRegistryHistory {
  history_id: string;
  created_at: string;
  started_at: string;
  finished_at: string;
  duration?: number;
  status: string;
  config_name: string;
  dest_id: string;
  resource_actions: string[];
}

export interface SyncRegistryHistoryLog {
  message: string;
  time: number;
  level: number;
}

export interface SyncRegistryHistoryLogParams {
  history_id: string;
  start_time?: number;
  end_time?: number;
  limit?: number;
}

@Injectable()
export class ImageService {
  private SYNC_REGISTRY_ENDPOINT = `/ajax/sync-registry/${
    this.account.namespace
  }`;

  constructor(
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(WEBLABS) public weblabs: Weblabs,
    private http: HttpService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
  ) {}

  getSyncRegistryConfigs(action?: string): Promise<SyncRegistryConfig[]> {
    return this.http
      .request({
        url: `${this.SYNC_REGISTRY_ENDPOINT}/configs`,
        method: 'GET',
        params: {
          action,
        },
      })
      .then(({ result }) => result);
  }

  getSyncRegistryConfig(configName: string): Promise<SyncRegistryConfig> {
    return this.http.request(
      `${this.SYNC_REGISTRY_ENDPOINT}/configs/${configName}`,
    );
  }

  deleteSyncRegistryConfig(
    nameOrConfig: string | SyncRegistryConfig,
  ): Promise<void> {
    return this.http.request(
      `${this.SYNC_REGISTRY_ENDPOINT}/configs/${
        typeof nameOrConfig === 'string'
          ? nameOrConfig
          : this.getConfigName(nameOrConfig)
      }`,
      {
        method: 'DELETE',
      },
    );
  }

  getConfigName(config: SyncRegistryConfig) {
    return config.uuid || config.config_id;
  }

  getSyncHistoryIconStatus(history: SyncRegistryHistory) {
    if (!history || !history.status) {
      return 'pending';
    }
    switch (history.status) {
      case 'B':
      case 'W':
        return 'waiting';
      case 'I':
        return 'deploying';
      case 'S':
        return 'ok';
      case 'D':
      case 'F':
        return 'error';
    }
  }

  getSyncRegistryHistories(params?: {
    registry_name?: string;
    repository_project_name?: string;
    repo_name?: string;
    config_name?: string;
    page?: number;
    num_per_page?: number;
    project_name?: string;
  }): Promise<{
    num_pages: number;
    result_list: SyncRegistryHistory[];
  }> {
    return this.http.request(`${this.SYNC_REGISTRY_ENDPOINT}/histories`, {
      method: 'GET',
      params,
    });
  }

  getSyncRegistryHistory(historyId: string): Promise<SyncRegistryHistory> {
    return this.http.request(
      `${this.SYNC_REGISTRY_ENDPOINT}/histories/${historyId}`,
    );
  }

  getSyncRegistryHistoryLogs({
    history_id,
    start_time,
    end_time,
    limit = 2000,
  }: SyncRegistryHistoryLogParams): Promise<SyncRegistryHistoryLog[]> {
    return this.http
      .request(`${this.SYNC_REGISTRY_ENDPOINT}/histories/${history_id}/logs`, {
        method: 'GET',
        params: {
          start_time,
          end_time,
          limit,
        },
      })
      .then(({ result }) => result);
  }

  deleteHistory(history_id: string) {
    return this.http.request(
      `${this.SYNC_REGISTRY_ENDPOINT}/histories/${history_id}`,
      {
        method: 'DELETE',
      },
    );
  }

  createImageSyncConfig(config: SyncRegistryConfig) {
    return this.http.request<SyncRegistryConfig>({
      url: `${this.SYNC_REGISTRY_ENDPOINT}/configs`,
      method: 'POST',
      body: config,
    });
  }

  updateImageSyncConfig(config: SyncRegistryConfig) {
    return this.http.request<SyncRegistryConfig>({
      url: `${this.SYNC_REGISTRY_ENDPOINT}/configs/${config.config_name}`,
      method: 'PUT',
      body: config,
    });
  }

  triggerSync(data: {
    config_name: string;
    tag: string;
    dest_id_list: string[];
  }): Promise<string[]> {
    return this.http
      .request({
        method: 'POST',
        url: this.SYNC_REGISTRY_ENDPOINT + '/histories',
        body: data,
      })
      .then((res: any) => {
        this.auiNotificationService.success(
          this.translate.get('image_sync_center_start_success'),
        );
        return res.result;
      });
  }
}
