import { Component, Input, OnInit } from '@angular/core';
import { sortBy } from 'lodash';

import { ImageRepositoryTagArtifact } from 'app2/shared/services/features/image-repository.service';
import { OssClientService } from 'app2/shared/services/features/oss-client.service';

@Component({
  selector: 'rc-artifacts-table',
  templateUrl: './artifacts-table.component.html',
})
export class ArtifactsTableComponent implements OnInit {
  @Input()
  artifacts: ImageRepositoryTagArtifact[];

  loading = true;
  artifactList: Array<
    ImageRepositoryTagArtifact & {
      downloadUrl: string;
    }
  >;

  constructor(private ossClient: OssClientService) {}

  async postProcessArtifacts() {
    this.loading = true;
    // TODO: the user may expect to see the artifacts in a structural table
    // TODO: but right now we only do a simple ordering
    return sortBy(
      await Promise.all(
        this.artifacts.map(async artifact => ({
          ...artifact,
          downloadUrl: await this.ossClient.generateLink({
            skipMiddleLayer: true,
            path: artifact.path,
          }),
        })),
      ),
      'key',
    );
  }

  async ngOnInit() {
    this.artifactList = await this.postProcessArtifacts();
    this.loading = false;
  }
}
