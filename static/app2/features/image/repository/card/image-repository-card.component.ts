import {
  Component,
  EventEmitter,
  Inject,
  Input,
  Output,
  forwardRef,
} from '@angular/core';

import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';
import { ImageRepository } from 'app2/shared/services/features/image-repository.service';

import { ImageRepositoryProjectsComponent } from '../projects/image-repository-projects.component';

@Component({
  selector: 'rc-image-repository-card',
  templateUrl: './image-repository-card.component.html',
  styleUrls: ['./image-repository-card.component.scss'],
})
export class ImageRepositoryCardComponent {
  @Input()
  repository: ImageRepository;
  @Output()
  deleteRepository: EventEmitter<{
    before: () => void;
    after: () => void;
  }> = new EventEmitter();

  weblabs: Weblabs;
  $projects: ImageRepositoryProjectsComponent;

  deleting: boolean;

  constructor(
    @Inject(WEBLABS) weblabs: Weblabs,
    @Inject(forwardRef(() => ImageRepositoryProjectsComponent))
    $projects: ImageRepositoryProjectsComponent,
  ) {
    this.weblabs = weblabs;
    this.$projects = $projects;
  }

  deleteRepo(e: Event) {
    e.stopPropagation();
    if (this.deleting) {
      return;
    }
    this.deleteRepository.emit({
      before: () => {
        this.deleting = true;
      },
      after: () => {
        this.deleting = false;
      },
    });
  }
}
