import { Location } from '@angular/common';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { NotificationService } from 'alauda-ui';
import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';
import {
  ImageRegistry,
  ImageRegistryService,
} from 'app2/shared/services/features/image-registry.service';
import {
  ImageRepository,
  ImageRepositoryService,
} from 'app2/shared/services/features/image-repository.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './image-repository-create.component.html',
  styleUrls: ['./image-repository-create.component.scss'],
})
export class ImageRepositoryCreateComponent implements OnInit {
  @ViewChild('form')
  form: NgForm;

  registryName: string;
  projectName: string;
  integrationId: string;

  isEditing: boolean;

  registry: ImageRegistry = {} as ImageRegistry;
  repository: ImageRepository = {
    is_public: false,
  } as ImageRepository;

  loading = true;
  types: Array<{
    display: string;
    value: boolean;
  }>;
  weblabs: Weblabs;

  constructor(
    @Inject(WEBLABS) weblabs: Weblabs,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private imageRegistry: ImageRegistryService,
    private imageRepository: ImageRepositoryService,
    private errorsToast: ErrorsToastService,
  ) {
    this.weblabs = weblabs;

    this.route.queryParamMap.subscribe(async params => {
      this.registryName = params.get('registryName');
      this.projectName = params.get('projectName');
      this.integrationId = params.get('integrationId');

      const repositoryName = params.get('repositoryName');

      this.loading = true;

      if (this.registryName) {
        const promises: [Promise<ImageRegistry>, Promise<ImageRepository>] = [
          this.imageRegistry.getRegistry(this.registryName),
          null,
        ];

        if (repositoryName) {
          this.isEditing = true;
          promises[1] = this.imageRepository.getRepository(
            this.registryName,
            repositoryName,
            this.projectName,
          );
        }

        const [registry, repository] = await Promise.all(promises);

        this.registry = registry;

        if (repositoryName) {
          this.repository = repository;
          this.integrationId = repository.registry.integration_id;
        }
      }

      this.loading = false;
    });
  }

  ngOnInit() {
    this.types = [
      {
        display: this.translate.get('private'),
        value: false,
      },
      {
        display: this.translate.get('public'),
        value: true,
      },
    ];
  }

  async create() {
    if (this.form.invalid) {
      return;
    }

    this.loading = true;

    try {
      await this.imageRepository[
        this.isEditing ? 'updateRepository' : 'addRepository'
      ](this.registryName, this.repository, this.projectName);
    } catch (e) {
      this.errorsToast.error(e);
      return;
    } finally {
      this.loading = false;
    }

    this.auiNotificationService.success({
      content: this.translate.get(
        this.isEditing ? 'repo_update_success' : 'repo_create_success',
      ),
    });

    this.router.navigate(['/image/repository/detail'], {
      queryParams: {
        repositoryName: this.repository.name,
        registryName: this.registryName,
        projectName: this.projectName,
      },
    });
  }

  cancel() {
    this.location.back();
  }
}
