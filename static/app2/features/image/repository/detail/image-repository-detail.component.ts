import { Location } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NotificationService } from 'alauda-ui';
import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';
import { ImageRegistryService } from 'app2/shared/services/features/image-registry.service';
import {
  ImageRepository,
  ImageRepositoryService,
} from 'app2/shared/services/features/image-repository.service';
import { ImageRepositoryUtilitiesService } from 'app2/shared/services/features/image-repository.utilities.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './image-repository-detail.component.html',
})
export class ImageRepositoryDetailComponent implements OnInit {
  registryName: string;
  repositoryName: string;
  projectName: string;

  initialized: boolean;
  repository: ImageRepository = {} as ImageRepository;
  imageAddress: string;
  deleting: boolean;

  weblabs: Weblabs;
  tagsCount: number;
  tagProtectedCount: number;
  tagMaxNumber: number;
  tagProtectedMaxNumber: number;

  get dockerPullTip() {
    return `${this.translate.get('click_to_copy')} "docker pull ${
      this.imageAddress
    }"`;
  }

  get imageAddressTip() {
    return this.translate.get('click_to_copy') + ' ' + this.imageAddress;
  }

  constructor(
    @Inject(WEBLABS) weblabs: Weblabs,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private modal: ModalService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private roleUtilities: RoleUtilitiesService,
    private imageRegistry: ImageRegistryService,
    private imageRepository: ImageRepositoryService,
    private imageRepositoryUtilities: ImageRepositoryUtilitiesService,
  ) {
    this.weblabs = weblabs;
  }

  ngOnInit() {
    this.route.queryParamMap.subscribe(async params => {
      this.registryName = params.get('registryName');
      this.repositoryName = params.get('repositoryName');
      this.projectName = params.get('projectName');

      if (this.repositoryName && this.repositoryName.includes('/')) {
        const registry = await this.imageRegistry.getRegistry(
          this.registryName,
        );
        if (!registry.is_public) {
          const [projectName, repositoryName] = this.repositoryName.split('/');
          return this.router.navigate(['/image/repository/detail'], {
            queryParams: {
              registryName: this.registryName,
              repositoryName,
              projectName,
            },
            replaceUrl: true,
          });
        }
      }

      this.initialized = false;

      let registry: ImageRepository['registry'];

      try {
        const repository = (this.repository = await this.imageRepository.getRepository(
          this.registryName,
          this.repositoryName,
          this.projectName,
        ));

        registry = repository.registry;
      } catch (e) {
        if (!this.initialized && e.status !== 403) {
          this.auiNotificationService.warning({
            content: this.translate.get('image_repository_not_exist'),
          });
        }

        return this.location.back();
      }

      this.tagMaxNumber = registry.tag_max_number;

      if (this.weblabs.TAG_MANAGE_ENABLED && this.tagMaxNumber) {
        try {
          const tags = await this.imageRepository.getRepositoryTags({
            registry_name: this.registryName,
            repository_name: this.repositoryName,
            project_name: this.projectName,
          });

          this.tagsCount = tags.length;
          this.tagProtectedCount = this.repository.tag_protected_count;
          this.tagProtectedMaxNumber = registry.tag_protected_max_number;
        } catch (e) {}
      }

      this.imageAddress = this.imageRepositoryUtilities.getImageAddress(
        this.repository,
      );

      this.initialized = true;
    });
  }

  buttonDisplayExpr(action: string) {
    return this.roleUtilities.resourceHasPermission(
      this.repository,
      'repository',
      action,
    );
  }

  async deleteRepository() {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete'),
        content:
          this.translate.get('delete_confirm') +
          this.translate.get('repository') +
          ' ' +
          this.repositoryName +
          ' ?',
      });
    } catch (e) {
      return;
    }

    this.deleting = true;

    try {
      await this.imageRepository.deleteRepository(
        this.registryName,
        this.repositoryName,
        this.projectName,
      );

      this.auiNotificationService.success({
        content: this.translate.get('repo_delete_success'),
      });

      this.router.navigateByUrl('/image/repository', {
        replaceUrl: true,
      });
    } finally {
      this.deleting = false;
    }
  }
}
