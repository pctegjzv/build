import { Component, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ACCOUNT, ENVIRONMENTS } from 'app2/core/tokens';
import { Environments, RcAccount } from 'app2/core/types';
import { ImageRepositoryService } from 'app2/shared/services/features/image-repository.service';

@Component({
  templateUrl: './image-repository-help.component.html',
  styleUrls: ['./image-repository-help.component.scss'],
})
export class ImageRepositoryHelpComponent {
  initialized: boolean;
  registryName: string;
  repositoryName: string;
  projectName: string;
  repositoryIndex: string;
  textPull: string;
  textPush: string;

  constructor(
    @Inject(ACCOUNT) private account: RcAccount,
    @Inject(ENVIRONMENTS) private environments: Environments,
    private route: ActivatedRoute,
    private imageRepository: ImageRepositoryService,
  ) {
    this.route.queryParamMap.subscribe(async params => {
      this.registryName = params.get('registryName');
      this.repositoryName = params.get('repositoryName');
      this.projectName = params.get('projectName');

      this.initialized = false;

      let isPublic = false;

      if (this.registryName) {
        const repository = await this.imageRepository.getRepository(
          this.registryName,
          this.repositoryName,
          this.projectName,
        );

        isPublic = repository.registry.is_public;
        this.repositoryIndex = repository.registry.endpoint;
      } else {
        this.repositoryIndex = this.environments.alauda_image_index;
      }

      let imageName = this.repositoryIndex + '/';

      imageName +=
        this.registryName && !isPublic
          ? `${this.projectName ? this.projectName + '/' : ''}`
          : this.account.namespace + '/';

      imageName += this.repositoryName;

      this.textPull = `$ sudo docker login ${this.repositoryIndex}
$ sudo docker pull ${imageName}`;
      this.textPush = `$ sudo docker login ${this.repositoryIndex}
$ sudo docker tag <span class="hljs-comment">image id</span> ${imageName}:<span class="hljs-comment">tag</span>
$ sudo docker push ${imageName}:<span class="hljs-comment">tag`;

      this.initialized = true;
    });
  }
}
