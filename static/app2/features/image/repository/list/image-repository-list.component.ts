import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { chain } from 'lodash';

import {
  ImageRegistry,
  ImageRegistryService,
} from 'app2/shared/services/features/image-registry.service';
import { CookieService } from 'app2/shared/services/utility/cookie.service';

const REGISTRY_COOKIE = 'registry';

@Component({
  templateUrl: './image-repository-list.component.html',
  styleUrls: ['./image-repository-list.component.scss'],
})
export class ImageRepositoryListComponent implements OnInit {
  registries: ImageRegistry[];
  selectedIndex: number;

  constructor(
    private imageRegistry: ImageRegistryService,
    private cookie: CookieService,
    private route: ActivatedRoute,
  ) {}

  async ngOnInit() {
    const registries = await this.imageRegistry.find();
    this.registries = chain(registries)
      .sortBy(({ display_name, name }) => (display_name || name).toLowerCase())
      .sortBy(({ is_public }) => is_public)
      .value();

    const registryName =
      this.route.snapshot.queryParams.registryName ||
      this.cookie.getCookie(REGISTRY_COOKIE);

    const selectedIndex = this.registries.findIndex(
      ({ name }) => name === registryName,
    );

    this.selectedIndex = selectedIndex === -1 ? 0 : selectedIndex;
  }

  onSelectedIndexChange(selectedIndex: number) {
    this.selectedIndex = selectedIndex;
    this.cookie.setCookie(REGISTRY_COOKIE, this.registries[selectedIndex].name);
  }
}
