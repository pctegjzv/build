import {
  Component,
  EventEmitter,
  Inject,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { ImageProjectService } from 'app2/shared/services/features/image-project.service';
import { ImageRegistry } from 'app2/shared/services/features/image-registry.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';

@Component({
  templateUrl: './image-repository-add-project.component.html',
})
export class ImageRepositoryAddProjectComponent {
  @Input()
  registry: ImageRegistry;
  @Output()
  confirmed: EventEmitter<boolean> = new EventEmitter();

  @ViewChild('form')
  form: NgForm;

  name: string;
  loading: boolean;

  constructor(
    @Inject(MODAL_DATA) private data: { registry: ImageRegistry },
    private imageProject: ImageProjectService,
    private errorsToast: ErrorsToastService,
  ) {}

  async addProject() {
    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    try {
      await this.imageProject.addProject(this.data.registry.name, this.name);
    } catch (e) {
      return this.errorsToast.error(e);
    } finally {
      this.loading = false;
    }
    this.confirmed.next(true);
  }
}
