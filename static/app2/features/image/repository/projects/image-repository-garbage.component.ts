import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import {
  ImageRegistry,
  ImageRegistryService,
} from 'app2/shared/services/features/image-registry.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './image-repository-garbage.component.html',
  styleUrls: ['./image-repository-garbage.component.scss'],
})
export class ImageRepositoryGarbageComponent implements OnInit {
  @Output()
  close = new EventEmitter<boolean>();

  @ViewChild(NgForm)
  ngForm: NgForm;

  errorMapper: {
    [key: string]: string;
  };

  submitting: boolean;

  registry: ImageRegistry;
  auto_garbage_collect: boolean;
  garbage_collect_schedule_rule: string;

  constructor(
    private translate: TranslateService,
    private imageRegistry: ImageRegistryService,
    @Inject(MODAL_DATA) private data: { registry: ImageRegistry },
  ) {
    this.registry = this.data.registry;
    this.auto_garbage_collect = this.registry.auto_garbage_collect;
    this.garbage_collect_schedule_rule = this.registry.garbage_collect_schedule_rule;
  }

  ngOnInit() {
    this.errorMapper = {
      cronFormatError: this.translate.get('cron_format_error'),
      cronMinIntervalError: this.translate.get('cron_min_interval_error', {
        minInterval: 60,
      }),
    };
  }

  async confirm() {
    if (this.ngForm.invalid) {
      return;
    }

    this.submitting = true;

    try {
      const { auto_garbage_collect, garbage_collect_schedule_rule } = this;

      await this.imageRegistry.updateRegistry({
        ...this.registry,
        auto_garbage_collect,
        garbage_collect_schedule_rule,
      });

      Object.assign(this.registry, {
        auto_garbage_collect,
        garbage_collect_schedule_rule,
      });

      this.close.emit(true);
    } finally {
      this.submitting = false;
    }
  }
}
