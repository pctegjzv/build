import { Component, Inject, Input, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';
import { ImageRepositoryAddProjectComponent } from 'app2/features/image/repository/projects/image-repository-add-project.component';
import { ImageRepositoryGarbageComponent } from 'app2/features/image/repository/projects/image-repository-garbage.component';
import { ImageRepositoryScanComponent } from 'app2/features/image/repository/projects/image-repository-scan.component';
import { ImageRepositoryTagMaxNumberComponent } from 'app2/features/image/repository/projects/image-repository-tag-max-number.component';
import {
  ImageProject as _ImageProject,
  ImageProjectService,
} from 'app2/shared/services/features/image-project.service';
import {
  ImageRegistry,
  ImageRegistryService,
} from 'app2/shared/services/features/image-registry.service';
import {
  ImageRepository,
  ImageRepositoryService,
} from 'app2/shared/services/features/image-repository.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';

type ImageProject = _ImageProject & { is_default?: boolean };

@Component({
  selector: 'rc-image-repository-projects',
  templateUrl: './image-repository-projects.component.html',
  styleUrls: ['./image-repository-projects.component.scss'],
})
export class ImageRepositoryProjectsComponent implements OnInit {
  @Input()
  registry: ImageRegistry;

  weblabs: Weblabs;

  repositoryCreateDisplay: boolean;
  projectCreateDisplay: boolean;
  repositoryCreateEnabled: boolean;
  repositoryLoading: boolean;

  defaultProject = {
    project_name: this.translate.get('default_project_name'),
    is_default: true,
  } as ImageProject;

  currentProject: ImageProject;
  projects: ImageProject[];
  repositories: ImageRepository[] = [];

  count: number;
  page = 1;
  search: string;

  constructor(
    @Inject(WEBLABS) weblabs: Weblabs,
    private roleUtilities: RoleUtilitiesService,
    private imageRegistry: ImageRegistryService,
    private imageRepository: ImageRepositoryService,
    private imageProject: ImageProjectService,
    private translate: TranslateService,
    private modal: ModalService,
    private auiNotificationService: NotificationService,
  ) {
    this.weblabs = weblabs;
  }

  async ngOnInit() {
    const registry_name = this.registry.name;

    this.repositoryCreateDisplay = await this.roleUtilities.resourceTypeSupportPermissions(
      'registry',
      {
        registry_name,
      },
    );

    this.selectProject(this.defaultProject);

    let projects: ImageProject[] = [];

    if (!this.registry.is_public) {
      projects = await this.getProjects();
      this.projectCreateDisplay = await this.roleUtilities.resourceTypeSupportPermissions(
        'registry_project',
        {
          registry_name,
        },
      );
    }

    this.projects = [this.defaultProject].concat(projects);
  }

  trackByUuid(repository: ImageRepository) {
    return repository.uuid;
  }

  registryButtonDisplayExpr(action: string) {
    return this.roleUtilities.resourceHasPermission(
      this.registry,
      'registry',
      action,
    );
  }

  repositoryButtonDisplayExpr(repository: ImageRepository, action: string) {
    return this.roleUtilities.resourceHasPermission(
      repository,
      'repository',
      action,
    );
  }

  projectButtonDisplayExpr(project: ImageProject, action: string) {
    return this.roleUtilities.resourceHasPermission(
      project,
      'registry_project',
      action,
    );
  }

  getProjects() {
    return this.imageProject.getProjects(this.registry.name);
  }

  async selectProject(project: ImageProject) {
    if (this.repositoryLoading || this.currentProject === project) {
      return;
    }

    this.currentProject = project;

    this.repositoryCreateEnabled =
      project.is_default ||
      (await this.roleUtilities.resourceTypeSupportPermissions('repository', {
        registry_name: this.registry.name,
        registry_project: project.project_name,
      }));

    this.getRepositories();
  }

  async getRepositories(page = 1) {
    this.repositoryLoading = true;

    const project = this.currentProject;

    try {
      const {
        count,
        num_pages,
        results,
      } = await this.imageRepository.getRepositories({
        registry_name: this.registry.name,
        project_name: project.is_default ? null : project.project_name,
        page,
        search: this.search,
      });

      if (num_pages && page > num_pages) {
        this.getRepositories();
        return;
      }

      this.page = page;
      this.count = count;
      this.repositories = results;
    } catch (e) {}

    this.repositoryLoading = false;
  }

  async addProject() {
    const modalRef = await this.modal.open(ImageRepositoryAddProjectComponent, {
      title: this.translate.get('repository_project_create'),
      data: {
        registry: this.registry,
      },
    });

    modalRef.componentInstance.confirmed.subscribe(
      async (isConfirm: boolean) => {
        modalRef.close(isConfirm);

        if (!isConfirm) {
          return;
        }

        const projects = await this.getProjects();
        this.projects = [this.defaultProject].concat(projects);
        this.selectProject(projects[0]);
      },
    );
  }

  async deleteProject(project: ImageProject) {
    if (project.repo_count > 0) {
      return this.auiNotificationService.warning({
        content: this.translate.get('deleted_project_not_empty'),
      });
    }

    try {
      await this.modal.confirm({
        title: this.translate.get('delete'),
        content: `${this.translate.get('delete')}${this.translate.get(
          'repository_project',
        )} ${project.project_name}?`,
      });
    } catch (e) {
      return;
    }

    try {
      await this.imageProject.deleteProject(
        this.registry.name,
        project.project_name,
      );
      this.projects = this.projects.filter(
        ({ project_name }) => project_name !== project.project_name,
      );
      this.auiNotificationService.success({
        content: this.translate.get('delete_success'),
      });
    } catch (e) {
      this.auiNotificationService.warning({
        content: this.translate.get('delete_failed'),
      });
    } finally {
      this.selectProject(this.projects[0]);
    }
  }

  async deleteRepository(
    repository: ImageRepository,
    {
      before,
      after,
    }: {
      before: () => void;
      after: () => void;
    },
  ) {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete'),
        content: `${this.translate.get('delete')}${this.translate.get(
          'repository',
        )} ${repository.name}?`,
      });
    } catch (e) {
      return;
    }

    before();

    try {
      await this.imageRepository.deleteRepository(
        this.registry.name,
        repository.name,
        this.currentProject.is_default
          ? null
          : this.currentProject.project_name,
      );
    } catch (e) {
      return this.auiNotificationService.warning({
        content: this.translate.get('repo_delete_failed'),
      });
    }

    const { project } = repository;

    if (!project) {
      this.removeRepository(repository);
    } else if (project.project_name === this.currentProject.project_name) {
      this.removeRepository(repository);
      this.currentProject.repo_count--;
    }

    this.auiNotificationService.success({
      content: this.translate.get('repo_delete_success'),
    });

    after();
  }

  removeRepository(repository: ImageRepository) {
    this.repositories = this.repositories.filter(
      ({ name }) => name !== repository.name,
    );
  }

  showImageScanDialog() {
    const modalRef = this.modal.open(ImageRepositoryScanComponent, {
      title: this.registry.integration_id
        ? this.translate.get('image_repository_scan_update')
        : this.translate.get('image_repository_scan_configure'),
      data: {
        registry: this.registry,
      },
    });

    modalRef.componentInstance.close
      .pipe(take(1))
      .subscribe(async (isConfirm: boolean) => {
        if (isConfirm) {
          this.auiNotificationService.success({
            content: this.translate.get('image_scan_update_success'),
          });

          this.registry = await this.imageRegistry.getRegistry(
            this.registry.name,
          );
        }

        modalRef.close();
      });
  }

  showTagMountDialog() {
    const modalRef = this.modal.open(ImageRepositoryTagMaxNumberComponent, {
      title: this.translate.get('image_repository_tag_amount_configure'),
      data: {
        registry: this.registry,
      },
    });

    modalRef.componentInstance.close
      .pipe(take(1))
      .subscribe(async () => modalRef.close());
  }

  showCleanTimeDialog() {
    const modalRef = this.modal.open(ImageRepositoryGarbageComponent, {
      title: this.translate.get('image_repository_clean_time_configure'),
      data: {
        registry: this.registry,
      },
    });

    modalRef.componentInstance.close
      .pipe(take(1))
      .subscribe(async () => modalRef.close());
  }
}
