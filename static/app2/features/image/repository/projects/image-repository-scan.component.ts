import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { uniqBy } from 'lodash';

import {
  ImageRegistry,
  ImageRegistryService,
} from 'app2/shared/services/features/image-registry.service';
import { IntegrationService } from 'app2/shared/services/features/integration.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './image-repository-scan.component.html',
  styleUrls: ['./image-repository-scan.component.scss'],
})
export class ImageRepositoryScanComponent implements OnInit {
  @Output()
  close = new EventEmitter<boolean>();

  @ViewChild(NgForm)
  ngForm: NgForm;

  registry: ImageRegistry;

  loading = true;
  options: Array<{
    id: string;
    name: string;
  }>;
  submitting: boolean;
  selected: string;

  constructor(
    private imageRegistry: ImageRegistryService,
    private integration: IntegrationService,
    private translate: TranslateService,
    private errorsToast: ErrorsToastService,
    @Inject(MODAL_DATA) private data: { registry: ImageRegistry },
  ) {
    this.registry = this.data.registry;
  }

  async ngOnInit() {
    const options = [
      {
        name: this.translate.get('image_scan_select_none'),
        id: '#',
      },
    ];

    if (this.registry.integration_id) {
      try {
        const res = (await this.integration.getIntegration(
          this.registry.integration_id,
        )) as any;

        options.push({
          id: res.id,
          name: res.name,
        });

        this.selected = res.id;
      } catch (e) {}
    } else {
      this.selected = '#';
    }

    try {
      const res = (await this.integration.getIntegrations({
        page: 1,
        page_size: 999,
        types: 'Clair,Tenable',
      })) as any;

      res.forEach(
        (item: any) =>
          item.enabled &&
          item.resource_actions.includes('integration:view') &&
          options.push({ name: item.name, id: item.id }),
      );
    } catch (e) {}

    this.options = uniqBy(options, 'id');

    this.loading = false;
  }

  async confirm() {
    if (this.ngForm.invalid) {
      return;
    }

    this.submitting = true;

    const integration_id = this.selected === '#' ? null : this.selected;

    try {
      await this.imageRegistry.updateRegistry({
        ...this.registry,
        integration_id,
      });

      this.registry.integration_id = integration_id;

      this.close.emit(true);
    } catch (e) {
      this.errorsToast.error(e);
    }

    this.submitting = false;
  }
}
