import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import {
  ImageRegistry,
  ImageRegistryService,
} from 'app2/shared/services/features/image-registry.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';

@Component({
  templateUrl: './image-repository-tag-max-number.component.html',
  styleUrls: ['./image-repository-tag-max-number.component.scss'],
})
export class ImageRepositoryTagMaxNumberComponent implements OnInit {
  @Output()
  close = new EventEmitter<boolean>();

  @ViewChild(NgForm)
  ngForm: NgForm;

  registry: ImageRegistry;
  checked: boolean;
  tag_max_number: number;
  pattern: {
    test: (val: string) => boolean;
  };

  constructor(
    @Inject(MODAL_DATA) private data: { registry: ImageRegistry },
    private imageRegistry: ImageRegistryService,
  ) {
    this.registry = this.data.registry;
  }

  ngOnInit() {
    this.tag_max_number = this.registry.tag_max_number || null;
    this.checked = !!this.tag_max_number;

    const pattern = /^[1-9][0-9]{0,3}$/;

    this.pattern = {
      test: (value: string) => !this.checked || pattern.test(value),
    };
  }

  async confirm() {
    if (this.ngForm.invalid) {
      return;
    }

    const tag_max_number = (this.checked && this.tag_max_number) || 0;

    await this.imageRegistry.updateRegistry({
      ...this.registry,
      tag_max_number,
    });

    this.registry.tag_max_number = tag_max_number;
    this.close.emit(true);
  }
}
