import { Component, Input, OnInit } from '@angular/core';

import {
  ImageRepositoryService,
  ImageRepositoryTagArtifact,
} from 'app2/shared/services/features/image-repository.service';

@Component({
  selector: 'rc-image-repository-tag-artifacts',
  templateUrl: './image-repository-tag-artifacts.component.html',
})
export class ImageRepositoryTagArtifactsComponent implements OnInit {
  @Input()
  registryName: string;
  @Input()
  repositoryName: string;
  @Input()
  projectName: string;
  @Input()
  tagName: string;

  loading = true;
  artifacts: ImageRepositoryTagArtifact[];

  constructor(private imageRepository: ImageRepositoryService) {}

  async ngOnInit() {
    try {
      const {
        artifacts,
        build_id,
      } = await this.imageRepository.getImageRepositoryTagArtifacts({
        registry_name: this.registryName,
        repository_name: this.repositoryName,
        project_name: this.projectName,
        tag_name: this.tagName,
      });

      const prefix = 'build/' + build_id + '/';

      artifacts.forEach(artifact => {
        let key = artifact.key || '';
        if (key.startsWith('/')) {
          key = key.substr(1);
        }
        artifact.path = prefix + key;
      });

      this.artifacts = artifacts;
    } finally {
      this.loading = false;
    }
  }
}
