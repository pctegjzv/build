import {
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  TemplateRef,
  forwardRef,
} from '@angular/core';
import { sortBy } from 'lodash';

import { NotificationService } from 'alauda-ui';
import { ENVIRONMENTS, WEBLABS } from 'app2/core/tokens';
import { Environments, Weblabs } from 'app2/core/types';
import { ImageRepositoryDetailComponent } from 'app2/features/image/repository/detail/image-repository-detail.component';
import { SidePanelService } from 'app2/layout/side-panel.service';
import {
  ImageRepository,
  ImageRepositoryService,
  ImageRepositoryTag as _ImageRepositoryTag,
} from 'app2/shared/services/features/image-repository.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { delay } from 'app2/testing/helpers';
import { TranslateService } from 'app2/translate/translate.service';

interface ImageRepositoryTag extends _ImageRepositoryTag {
  summaryList?: Array<{
    level: string;
    count: string;
  }>;
}

@Component({
  selector: 'rc-image-repository-tags',
  templateUrl: './image-repository-tags.component.html',
  styleUrls: ['./image-repository-tags.component.scss'],
})
export class ImageRepositoryTagsComponent implements OnInit, OnDestroy {
  @Input()
  repository: ImageRepository;

  count: number;
  page: number;
  pageSize = 20;

  weblabs: Weblabs;

  initialized: boolean;
  destroyed: boolean;
  deleteTagEnabled: boolean;
  manageTags: boolean;
  hasTagScanInAction: boolean;
  loading: boolean;
  tags: ImageRepositoryTag[];

  get tagMaxNumber() {
    return this.$parent.tagMaxNumber;
  }

  get tagProtectedMaxNumber() {
    return this.$parent.tagProtectedMaxNumber;
  }

  get tagProtectedCount() {
    return this.$parent.tagProtectedCount;
  }

  constructor(
    @Inject(WEBLABS) weblabs: Weblabs,
    @Inject(forwardRef(() => ImageRepositoryDetailComponent))
    @Inject(ENVIRONMENTS)
    private environments: Environments,
    private $parent: ImageRepositoryDetailComponent,
    private imageRepository: ImageRepositoryService,
    private modal: ModalService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private sidePanel: SidePanelService,
  ) {
    this.weblabs = weblabs;
  }

  async ngOnInit() {
    this.manageTags = this.$parent.buttonDisplayExpr('manage_tags');
    this.deleteTagEnabled =
      this.manageTags &&
      (this.weblabs.TAG_MANAGE_ENABLED ||
        (!this.environments.is_private_deploy_enabled &&
          this.repository.registry.is_public));

    this.startPolling();
  }

  ngOnDestroy() {
    this.destroyed = true;
    this.sidePanel.close();
  }

  async startPolling() {
    try {
      await this.getTags();
    } catch (e) {}

    await delay(this.hasTagScanInAction ? 5000 : 30000);

    if (!this.destroyed) {
      this.startPolling();
    }
  }

  async getTags() {
    const { registry, project } = this.repository;

    try {
      this.loading = true;

      const {
        count,
        results: tags,
      } = await this.imageRepository.getRepositoryTags({
        registry_name: registry.name,
        repository_name: this.repository.name,
        project_name: project && project.project_name,
        view_type: 'detail',
        page_size: this.pageSize,
        page: this.page,
      });

      this.count = count;

      tags.forEach(tag => {
        const summaryList = Object.keys(tag.summary).map(key => ({
          level: key,
          count: tag.summary[key],
        }));
        (tag as ImageRepositoryTag).summaryList = sortBy(
          summaryList,
          item => (item.level === 'Fixable' ? 1 : -1),
        );
      });

      this.tags = tags;
      this.hasTagScanInAction = !!tags.find(tag => tag.level === 'Analyzing');
    } catch (e) {
      this.tags = [];
    }

    this.initialized = true;
    this.loading = false;
  }

  shouldShowScanResults() {
    return (
      this.repository.scan_enabled && this.repository.registry.integration_id
    );
  }

  shouldShowAction() {
    return (
      this.shouldShowTriggerButton() ||
      (this.weblabs.TAG_MANAGE_ENABLED &&
        this.manageTags &&
        this.$parent.tagMaxNumber) ||
      this.deleteTagEnabled
    );
  }

  shouldShowTriggerButton() {
    return (
      this.$parent.buttonDisplayExpr('trigger_scan') &&
      this.shouldShowScanResults()
    );
  }

  shouldDisableTriggerButton(tag: ImageRepositoryTag) {
    return !['', null, 'Failed'].includes(tag.level);
  }

  async toggleTagProtection(tag: ImageRepositoryTag) {
    const tag_protected = !tag.protected;

    if (
      !this.manageTags ||
      (tag_protected &&
        this.$parent.tagProtectedCount >= this.$parent.tagProtectedMaxNumber)
    ) {
      return;
    }

    const { repository } = this;

    await this.imageRepository.updateRepositoryTag({
      registry_name: repository.registry.name,
      repository_name: repository.name,
      project_name: repository.project && repository.project.project_name,
      tag_name: tag.tag_name,
      tag_protected,
    });

    tag.protected = tag_protected;

    this.$parent.tagProtectedCount += tag_protected ? 1 : -1;
  }

  viewImageTagArtifacts(tagArtifacts: TemplateRef<any>) {
    this.sidePanel.open({
      position: 'bottom',
      resizable: true,
      size: '25%',
      templateRef: tagArtifacts,
    });
  }

  async deleteTag(tag: ImageRepositoryTag) {
    const tagName = tag.tag_name;

    try {
      await this.modal.confirm({
        title: this.translate.get('delete'),
        content:
          this.translate.get('delete_confirm') +
          this.translate.get('image_tag') +
          ' ' +
          tagName +
          ' ?',
      });
    } catch (e) {
      return;
    }

    const { repository } = this;

    try {
      await this.imageRepository.deleteRepositoryTag({
        registry_name: repository.registry.name,
        repository_name: repository.name,
        project_name: repository.project && repository.project.project_name,
        tag_name: tagName,
      });
    } catch (e) {
      return;
    }

    this.tags = this.tags.filter(({ tag_name }) => tag_name !== tagName);
    this.$parent.tagsCount = this.tags.length;

    this.auiNotificationService.success({
      content: this.translate.get('repo_tag_delete_success'),
    });
  }

  async startImageTagScan(tag: ImageRepositoryTag) {
    tag.level = 'Analyzing';

    const { repository } = this;

    try {
      await this.imageRepository.startRepositoryTagScan({
        registry_name: repository.registry.name,
        repository_name: repository.name,
        project_name: repository.project && repository.project.project_name,
        tag_name: tag.tag_name,
      });
    } catch (e) {}

    await this.getTags();
  }
}
