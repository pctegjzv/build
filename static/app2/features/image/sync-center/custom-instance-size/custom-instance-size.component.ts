import { Component, Input, OnInit } from '@angular/core';
import { get } from 'lodash';

import { getGlobal } from 'app2/app-global';
import { INSTANCE_SIZE_CONFIG } from 'app2/core/tokens';
import { InstanceSizeConfig } from 'app2/core/types';

@Component({
  selector: 'rc-custom-instance-size',
  templateUrl: './custom-instance-size.component.html',
  styleUrls: ['./custom-instance-size.component.scss'],
})
export class CustomInstanceSizeComponent implements OnInit {
  @Input()
  instanceSize: {
    cpu: number;
    memory: number;
  };
  @Input()
  initMin: {
    cpu: number;
    memory: number;
  };

  min: {
    cpu: number;
    memory: number;
  };

  unitsOptions = [
    {
      display: 'MB',
      value: 1,
    },
    {
      display: 'GB',
      value: 1024,
    },
  ];

  baseUnit = 1;
  memDisplay = 512;
  resourceNameInt = {
    pattern: /^-?\d+$/,
    tip: 'integer_pattern',
  };
  instanceSizeConfig: InstanceSizeConfig;

  constructor() {
    this.instanceSizeConfig = getGlobal(INSTANCE_SIZE_CONFIG);
  }

  ngOnInit() {
    this.getMin();
  }

  getMin() {
    const cpu: number =
      get(this.initMin, 'cpu') ||
      get(this.instanceSizeConfig, 'custom.cpu.min', 0.125);
    const memory: number =
      get(this.initMin, 'memory') ||
      get(this.instanceSizeConfig, 'custom.memory.min', 64);

    this.min = this.baseUnit === 1 ? { cpu, memory } : { cpu, memory: 1 };
  }

  onFilterByChange() {
    this.getMin();
    this.onCustomSizeChange();
  }

  onCustomSizeChange() {
    if (this.memDisplay) {
      this.instanceSize.memory = this.memDisplay * this.baseUnit;
    }
  }
}
