import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, ReplaySubject, combineLatest } from 'rxjs';
import {
  debounceTime,
  map,
  publishReplay,
  refCount,
  take,
} from 'rxjs/operators';

import { ImageSyncCreateComponent } from 'app2/features/image/sync-center/image-sync-create.component';
import { ImageSyncTriggerComponent } from 'app2/features/image/sync-center/image-sync-trigger.component';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';

import { ImageService, SyncRegistryConfig } from '../image.service';

const SYNC_CONFIG = 'sync_config';

@Component({
  styleUrls: ['./image-sync-center.component.scss'],
  templateUrl: './image-sync-center.component.html',
})
export class ImageSyncCenterComponent implements OnInit {
  configs: SyncRegistryConfig[] = [];
  pageSize = 20;

  loading = true;
  loadError: boolean;

  searchString = '';

  syncConfigCreateEnabled: boolean;

  searchString$ = new BehaviorSubject<string>(this.searchString);
  configs$ = new ReplaySubject<SyncRegistryConfig[]>();
  pageIndex$ = new BehaviorSubject<number>(0);

  filteredConfigs$ = combineLatest(this.searchString$, this.configs$).pipe(
    debounceTime(100),
    map(([searchString, configs]) => {
      searchString = searchString.trim();
      return searchString
        ? configs.filter((config: SyncRegistryConfig) =>
            config.config_name.includes(searchString),
          )
        : configs;
    }),
    publishReplay(),
    refCount(),
  );

  filertedAndPagedConfigs$ = combineLatest(
    this.filteredConfigs$,
    this.pageIndex$,
  ).pipe(
    map(([filteredConfigs, pageIndex]) =>
      filteredConfigs.slice(
        pageIndex * this.pageSize,
        (pageIndex + 1) * this.pageSize,
      ),
    ),
    publishReplay(),
    refCount(),
  );

  constructor(
    private roleUtilities: RoleUtilitiesService,
    private image: ImageService,
    private modal: ModalService,
    private translate: TranslateService,
  ) {}

  async ngOnInit() {
    this.syncConfigCreateEnabled = await this.roleUtilities.resourceTypeSupportPermissions(
      SYNC_CONFIG,
    );

    await this.loadData();
  }

  async loadData() {
    this.loading = true;
    try {
      const configs = await this.image.getSyncRegistryConfigs();

      configs.forEach(config => {
        config.dest = config.dest.filter(({ endpoint }) => endpoint); // remove empty endpoint
        config.source.info = config.source.info || ({} as any);
      });

      this.configs = configs;
      this.configs$.next(configs);
      this.loadError = false;
    } catch (e) {
      this.loadError = true;
    } finally {
      this.loading = false;
    }
  }

  getConfigName(config: SyncRegistryConfig) {
    return this.image.getConfigName(config);
  }

  buttonDisplayExpr(item: any, action: string) {
    return this.roleUtilities.resourceHasPermission(item, SYNC_CONFIG, action);
  }

  startConfig(config: SyncRegistryConfig) {
    this.modal.open(ImageSyncTriggerComponent, {
      title: this.translate.get('image_sync_start'),
      data: {
        config,
      },
    });
  }

  editConfig(config?: SyncRegistryConfig) {
    const modalRef = this.modal.open(ImageSyncCreateComponent, {
      title: this.translate.get(config ? 'update' : 'image_sync_center_create'),
      width: 900,
      data: {
        config,
      },
    });

    modalRef.componentInstance.close
      .pipe(take(1))
      .subscribe((config: SyncRegistryConfig) => {
        if (config) {
          this.loadData();
        }

        modalRef.close();
      });
  }

  async deleteConfig(config: SyncRegistryConfig & { _deleting?: boolean }) {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('image_sync_delete_confirm_info', {
          name: config.config_name,
        }),
      });
    } catch (e) {
      return;
    }

    config._deleting = true;

    try {
      await this.image.deleteSyncRegistryConfig(config);
      this.configs = this.configs.filter(c => c !== config);
      this.configs$.next(this.configs);
    } finally {
      config._deleting = false;
    }
  }
}
