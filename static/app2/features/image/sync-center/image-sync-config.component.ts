import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { ImageSyncCreateComponent } from 'app2/features/image/sync-center/image-sync-create.component';
import { ImageSyncTriggerComponent } from 'app2/features/image/sync-center/image-sync-trigger.component';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import { take } from 'rxjs/operators';

import { ImageService, SyncRegistryConfig } from '../image.service';
import { ImageSyncHistoryComponent } from '../sync-history/image-sync-history.component';

const SYNC_CONFIG = 'sync_config';

@Component({
  templateUrl: './image-sync-config.component.html',
})
export class ImageSyncConfigComponent implements OnInit {
  loading = true;
  initialized = false;

  configName: string;

  config: SyncRegistryConfig = {
    source: {
      info: {},
    },
  } as SyncRegistryConfig;

  @ViewChild(ImageSyncHistoryComponent)
  syncHistoryComp: ImageSyncHistoryComponent;

  constructor(
    private image: ImageService,
    private router: Router,
    private route: ActivatedRoute,
    private roleUtilities: RoleUtilitiesService,
    private modal: ModalService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private location: Location,
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(async map => {
      this.configName = map.get('configName');
      this.initialized = false;
      await this.loadData();
      this.initialized = true;
    });
  }

  async loadData() {
    try {
      const config = await this.image.getSyncRegistryConfig(this.configName);
      config.dest = config.dest.filter(({ endpoint }) => endpoint); // remove empty endpoint
      config.source.info = config.source.info || ({} as any);
      this.config = config;
    } catch (e) {
      if (!this.initialized && e.status !== 403) {
        this.auiNotificationService.warning(
          this.translate.get('image_repository_not_exist'),
        );
        this.location.back();
      }
    }
  }
  getConfigName() {
    return this.image.getConfigName(this.config);
  }

  buttonDisplayExpr(action: string) {
    return this.roleUtilities.resourceHasPermission(
      this.config,
      SYNC_CONFIG,
      action,
    );
  }

  startConfig() {
    const modalRef = this.modal.open(ImageSyncTriggerComponent, {
      title: this.translate.get('image_sync_start'),
      data: {
        config: this.config,
      },
    });

    modalRef.componentInstance.close
      .pipe(take(1))
      .subscribe((historyIds: string[]) => {
        if (historyIds) {
          this.syncHistoryComp.loadData();
        }
      });
  }

  editConfig() {
    const modalRef = this.modal.open(ImageSyncCreateComponent, {
      title: this.translate.get('update'),
      width: 900,
      data: {
        config: this.config,
      },
    });

    modalRef.componentInstance.close
      .pipe(take(1))
      .subscribe((config: SyncRegistryConfig) => {
        if (config) {
          this.loadData();
        }

        modalRef.close();
      });
  }

  async deleteConfig() {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('image_sync_delete_confirm_info', {
          name: this.config.config_name,
        }),
      });
    } catch (e) {
      return;
    }

    this.initialized = false;
    try {
      await this.image.deleteSyncRegistryConfig(this.config);
      this.router.navigate(['/image/sync-center'], {
        replaceUrl: true,
      });
    } catch (e) {
      this.initialized = true;
    }
  }
}
