import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { cloneDeep, get, set } from 'lodash';

import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';
import {
  IMAGE_SYNC_DEFAULT_CONTAINER_SIZE,
  IMAGE_SYNC_DEST_TYPE,
} from 'app2/features/image/image-sync.constants';
import {
  ImageService,
  SyncRegistryConfig,
} from 'app2/features/image/image.service';
import {
  ImageRegistryService,
  ThirdPartyRegistry,
} from 'app2/shared/services/features/image-registry.service';
import {
  QuotaSpace,
  QuotaSpaceService,
} from 'app2/shared/services/features/quota-space.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';

import { RepositoryInfo } from 'app2/features/image/sync-center/repository-dropdown-group/repository-dropdown-group.component';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';

interface InternalRegistry {
  display_name: string;
  internal_id: string;
}

@Component({
  templateUrl: './image-sync-create.component.html',
  styleUrls: ['./image-sync-create.component.scss'],
})
export class ImageSyncCreateComponent implements OnInit {
  @Output()
  close = new EventEmitter<SyncRegistryConfig>();

  @ViewChild(NgForm)
  ngForm: NgForm;

  initialized: boolean;
  submitting: boolean;
  config: SyncRegistryConfig;
  disableRepoSelection: boolean;
  isUpdate: boolean;

  spaces: QuotaSpace[] = [];
  externalRegistries: ThirdPartyRegistry[] = [];
  initInternalRegistries: any[] = [];
  containerSizeCustomized: boolean;
  instanceSize: {
    cpu: number;
    memory: number;
  };
  ownRegistries: InternalRegistry[];
  registriesLoaded: boolean;
  repositoryInfo: RepositoryInfo;
  quotaEnabled: boolean;

  constructor(
    @Inject(MODAL_DATA)
    private data: {
      config: SyncRegistryConfig;
      registryName: string;
      repositoryName: string;
      projectName: string;
      disableRepoSelection: boolean;
    },
    @Inject(WEBLABS) private weblabs: Weblabs,
    private imageRegistry: ImageRegistryService,
    private image: ImageService,
    private quotaSpace: QuotaSpaceService,
    private errorToast: ErrorsToastService,
  ) {
    this.disableRepoSelection = this.data.disableRepoSelection;
  }

  async ngOnInit() {
    if (this.data.config) {
      this.config = cloneDeep(this.data.config);
      this.externalRegistries = this.config.dest.filter(
        item => item.dest_type === IMAGE_SYNC_DEST_TYPE.EXTERNAL,
      ) as ThirdPartyRegistry[];
      this.initInternalRegistries = this.config.dest
        .filter(item => item.dest_type === IMAGE_SYNC_DEST_TYPE.INTERNAL)
        .map(item => item.internal_id);
      this.isUpdate = true;
    } else {
      this.config = {
        source: {
          info: {
            registry_name: this.data.registryName,
            project_name: this.data.projectName,
            repository_name: this.data.repositoryName,
          },
        },
        dest: [],
      } as SyncRegistryConfig;
    }

    const { CPU, MEMORY } = IMAGE_SYNC_DEFAULT_CONTAINER_SIZE;

    if (
      (this.config.cpu && this.config.cpu !== CPU) ||
      (this.config.memory && this.config.memory !== MEMORY)
    ) {
      this.containerSizeCustomized = true;
      this.instanceSize = {
        cpu: this.config.cpu,
        memory: this.config.memory,
      };
    } else {
      this.instanceSize = {
        cpu: CPU,
        memory: MEMORY,
      };
    }

    const registries = await this.imageRegistry.find();

    this.ownRegistries = registries.map(item => ({
      display_name: item.display_name,
      internal_id: item.uuid,
    }));

    this.registriesLoaded = true;

    this.config.dest = this.config.dest.filter(
      destRegistry =>
        destRegistry.dest_type !== 'INTERNAL_REGISTRY' ||
        this.ownRegistries.some(
          ownRegistry => ownRegistry.internal_id === destRegistry.internal_id,
        ),
    );

    this.repositoryInfo = {
      registry: get(this, 'config.source.info.registry_name'),
      project: get(this, 'config.source.info.project_name'),
      repository: get(this, 'config.source.info.repository_name'),
    };

    this.quotaEnabled = this.weblabs.QUOTA_ENABLED;

    if (this.quotaEnabled) {
      this.spaces = await this.quotaSpace.getConsumableSpaces().catch(() => []);
      if (
        this.spaces.length &&
        !this.spaces.find(({ name }) => name === this.config.space_name)
      ) {
        this.config.space_name = this.spaces[0].name;
      }
    }

    this.initialized = true;
  }

  onRepositoryInfoChange(repositoryInfo: RepositoryInfo) {
    set(this, 'config.source.info.registry_name', repositoryInfo.registry);
    set(this, 'config.source.info.project_name', repositoryInfo.project);
    set(this, 'config.source.info.repository_name', repositoryInfo.repository);
  }

  showExternalRegistry() {
    this.externalRegistries.push({} as ThirdPartyRegistry);
    this.onExternalRegistriesChange(this.externalRegistries);
  }

  onInternalRegistryAdd(registry: InternalRegistry) {
    if (
      !this.config.dest.some(item => item.internal_id === registry.internal_id)
    ) {
      this.config.dest.push({
        dest_type: IMAGE_SYNC_DEST_TYPE.INTERNAL,
        internal_id: registry.internal_id,
      });
    }
  }

  onInternalRegistryRemove(registry: InternalRegistry) {
    const index = this.config.dest.findIndex(
      item => item.internal_id === registry.internal_id,
    );
    this.config.dest.splice(index, 1);
  }

  onExternalRegistriesChange(registries: ThirdPartyRegistry[]) {
    this.config.dest = this.config.dest
      .filter(item => item.dest_type === IMAGE_SYNC_DEST_TYPE.INTERNAL)
      .concat(registries);
  }

  async confirm() {
    if (this.ngForm.invalid) {
      return;
    }

    this.submitting = true;

    this.config.cpu = this.instanceSize.cpu;
    this.config.memory = this.instanceSize.memory;

    try {
      let config: SyncRegistryConfig;

      if (this.isUpdate) {
        config = await this.image.updateImageSyncConfig(this.config);
      } else {
        config = await this.image.createImageSyncConfig(this.config);
      }

      this.close.emit(config);
    } catch (e) {
      this.errorToast.error(e);
    }

    this.submitting = false;
  }
}
