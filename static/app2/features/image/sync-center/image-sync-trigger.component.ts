import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { cloneDeep, get } from 'lodash';

import {
  ImageService,
  SyncRegistryConfig,
} from 'app2/features/image/image.service';
import { ImageRepositoryService } from 'app2/shared/services/features/image-repository.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { delay } from 'app2/shared/services/utility/delay';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';

@Component({
  templateUrl: './image-sync-trigger.component.html',
  styleUrls: ['./image-sync-trigger.component.scss'],
})
export class ImageSyncTriggerComponent implements OnInit {
  @Output()
  close = new EventEmitter<string[]>();

  @ViewChild(NgForm)
  ngForm: NgForm;

  configName: string;
  config: SyncRegistryConfig;

  configs: SyncRegistryConfig[];

  initialized: boolean;
  submitting: boolean;
  allowConfigSelection: boolean;
  destIds: string[];

  imageTags: string[];
  imageTag: string;

  cache: any = {};

  constructor(
    @Inject(MODAL_DATA)
    private data: {
      configName: string;
      config: SyncRegistryConfig;
      initDestId: string;
      registry: string;
      project: string;
      repository: string;
    },
    private modal: ModalService,
    private image: ImageService,
    private imageRepository: ImageRepositoryService,
    private errorsToast: ErrorsToastService,
  ) {}

  async ngOnInit() {
    if (this.data.configName) {
      this.onConfigChanged(
        await this.image.getSyncRegistryConfig(this.data.configName),
      );
    } else if (this.data.config) {
      this.onConfigChanged(cloneDeep(this.data.config));
    }

    this.allowConfigSelection = !this.config;

    if (this.allowConfigSelection) {
      let configs = (await this.image.getSyncRegistryConfigs('trigger')).map(
        config => ({
          ...config,
          display_name: config.space_name
            ? `${config.config_name}(${config.space_name})`
            : config.config_name,
        }),
      );

      if (this.data.repository) {
        configs = configs.filter(config => {
          const registry = get(config, 'source.info.registry_name');
          const repository = get(config, 'source.info.repository_name');
          const project = get(config, 'source.info.project_name') || '';

          return (
            registry === this.data.registry &&
            repository === this.data.repository &&
            project === (this.data.project || '')
          );
        });
      }

      this.configs = configs;

      if (configs.length) {
        this.configName = this.configs[0].config_name;
      }
    }

    this.initialized = true;
  }

  async onConfigChanged(config: SyncRegistryConfig) {
    if (this.config === config) {
      return;
    }

    const destIds: string[] = [];

    if (this.data.initDestId) {
      this.destIds = config.dest
        .map(({ dest_id }) => dest_id)
        .filter(dest_id => dest_id === this.data.initDestId);
    }

    this.destIds = destIds;

    if (this.internalRepoName(config) !== this.internalRepoName(this.config)) {
      this.imageTags = null;
      this.imageTag = null;
    }

    this.config = config;

    const registry: string = get(config, 'source.info.registry_name');
    const repository: string = get(config, 'source.info.repository_name');
    const project: string = get(config, 'source.info.project_name');

    if (!registry || !repository) {
      this.imageTags = [];
      this.imageTag = null;
      return;
    }

    const internalRegistryName = this.internalRegistryName({
      registry_name: registry,
      project_name: project,
    });

    this.cache[internalRegistryName] = this.cache[internalRegistryName] || {};

    if (!this.cache[internalRegistryName][repository]) {
      this.cache[internalRegistryName][
        repository
      ] = await this.imageRepository.getRepositoryTags({
        registry_name: registry,
        project_name: project,
        repository_name: repository,
      });
    }

    this.imageTags = this.cache[internalRegistryName][repository];

    if (this.imageTags.length) {
      await delay(100);
      this.imageTag = this.imageTags[0];
    }
  }

  private internalRegistryName({
    registry_name,
    project_name,
  }: {
    registry_name: string;
    project_name: string;
  }) {
    return (project_name ? project_name + '/' : '') + registry_name;
  }

  private internalRepoName(config: SyncRegistryConfig) {
    const registry = get(config, 'source.info.registry_name');
    const repository = get(config, 'source.info.repository_name');
    const project = get(config, 'source.info.project_name');
    return [registry, project, repository].join('/');
  }

  cancel() {
    this.modal.closeAll();
  }

  async confirm() {
    if (this.ngForm.invalid) {
      return;
    }

    this.submitting = true;

    try {
      this.close.emit(
        await this.image.triggerSync({
          config_name: this.image.getConfigName(this.config),
          tag: this.imageTag,
          dest_id_list: this.destIds,
        }),
      );

      this.modal.closeAll();
    } catch (e) {
      this.errorsToast.error(e);
    } finally {
      this.submitting = false;
    }
  }
}
