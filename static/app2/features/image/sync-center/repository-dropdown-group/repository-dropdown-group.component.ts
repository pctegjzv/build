import { Component, EventEmitter, Input, Output } from '@angular/core';
import { debounce } from 'lodash';

import {
  ImageProject,
  ImageProjectService,
} from 'app2/shared/services/features/image-project.service';
import {
  ImageRegistry,
  ImageRegistryService,
} from 'app2/shared/services/features/image-registry.service';
import {
  ImageRepository,
  ImageRepositoryService,
} from 'app2/shared/services/features/image-repository.service';
import { delay } from 'app2/shared/services/utility/delay';

export interface RepositoryInfo {
  registry: string;
  project: string;
  repository: string;
}

@Component({
  selector: 'rc-repository-dropdown-group',
  templateUrl: './repository-dropdown-group.component.html',
})
export class RepositoryDropdownGroupComponent {
  @Input()
  disabled: boolean;

  @Output()
  repositoryInfoChange = new EventEmitter<RepositoryInfo>();

  @Input()
  set repositoryInfo(repositoryInfo: RepositoryInfo) {
    this.onRepositoryInfoChange(repositoryInfo);
  }

  registryName: string;
  projectName: string;
  repositoryName: string;

  registries: ImageRegistry[];
  registry: ImageRegistry;
  projects: ImageProject[];
  projectLoading: boolean;
  repositories: ImageRepository[];
  repositoryLoading: boolean;

  getRepositories = debounce(async function(
    this: RepositoryDropdownGroupComponent,
  ) {
    if (!this.registryName) {
      return;
    }

    this.repositoryLoading = true;

    this.repositories = await this.imageRepository.getRepositories(
      this.registryName,
      this.projectName === '@@shared' ? null : this.projectName,
    );

    let repositoryName = this.repositoryName;

    if (!this.repositories.length) {
      repositoryName = null;
    } else if (
      !repositoryName ||
      !this.repositories.find(repository => repository.name === repositoryName)
    ) {
      repositoryName = this.repositories[0].name;
    }

    await delay(100);

    this.repositoryName = repositoryName;

    this.repositoryLoading = false;
  },
  100);

  constructor(
    private imageRegistry: ImageRegistryService,
    private imageProject: ImageProjectService,
    private imageRepository: ImageRepositoryService,
  ) {}

  async onRepositoryInfoChange(repositoryInfo: RepositoryInfo) {
    if (!repositoryInfo) {
      this.registryName = this.projectName = this.repositoryName = null;
      this.repositories = this.projects = null;
      return;
    }

    this.registryName = repositoryInfo.registry;
    this.projectName = repositoryInfo.project;
    this.repositoryName = repositoryInfo.repository;

    if (!this.registries) {
      this.registries = await this.imageRegistry.find();
    }

    if (!this.registryName) {
      this.registry = this.registries[0];
      this.registryName = this.registry.name;
      await this.registryChange(this.registry);
    }
  }

  emitChange() {
    if (!this.repositoryName) {
      return;
    }

    this.repositoryInfoChange.emit({
      registry: this.registryName,
      project: this.projectName === '@@shared' ? null : this.projectName,
      repository: this.repositoryName,
    });
  }

  async registryChange(registry: ImageRegistry) {
    if (!registry) {
      return;
    }

    this.registry = registry;

    if (this.registry.is_public) {
      this.projectName = null;
    } else {
      this.projectLoading = true;

      this.projects = await this.imageProject.find(this.registryName);

      if (!this.projects.length) {
        this.projectName = null;
      } else if (
        !this.projectName ||
        !this.projects.find(
          project => project.project_name === this.projectName,
        )
      ) {
        this.projectName = this.projects[0].project_name;
      }

      this.projectLoading = false;
    }

    await this.getRepositories();
  }

  async projectChange(_project: ImageProject) {
    await this.getRepositories();
  }
}
