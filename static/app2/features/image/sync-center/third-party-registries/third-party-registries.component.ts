import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { IMAGE_SYNC_DEST_TYPE } from 'app2/features/image/image-sync.constants';
import { ThirdPartyRegistry } from 'app2/shared/services/features/image-registry.service';

@Component({
  selector: 'rc-third-party-registries',
  templateUrl: './third-party-registries.component.html',
})
export class ThirdPartyRegistriesComponent implements OnInit {
  @Input()
  registries: ThirdPartyRegistry[];
  @Output()
  registriesChange = new EventEmitter<ThirdPartyRegistry[]>();

  addressModes = ['HTTP', 'HTTPS'];

  ngOnInit() {
    this.registries.forEach(registry => {
      (registry as any)._type = registry.is_http ? 'HTTP' : 'HTTPS';
      registry.dest_type = IMAGE_SYNC_DEST_TYPE.EXTERNAL;
    });
  }

  deleteRow(index: number) {
    this.registries.splice(index, 1);
    this.registriesChange.emit(this.registries);
  }

  addRow() {
    this.registries.push({
      _type: 'HTTPS',
      is_http: false,
      dest_type: IMAGE_SYNC_DEST_TYPE.EXTERNAL,
    } as any);
    this.registriesChange.emit(this.registries);
  }
}
