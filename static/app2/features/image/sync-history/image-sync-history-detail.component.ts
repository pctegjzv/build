import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { ImageSyncTriggerComponent } from 'app2/features/image/sync-center/image-sync-trigger.component';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { delay } from 'app2/shared/services/utility/delay';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

import { ImageService, SyncRegistryHistory } from '../image.service';

const SYNC_CONFIG = 'sync_config';
const SYNC_HISTORY = 'sync_history';

@Component({
  templateUrl: './image-sync-history-detail.component.html',
})
export class ImageSyncHistoryDetailComponent implements OnInit, OnDestroy {
  historyId: string;

  initialized: boolean;
  destroyed: boolean;
  restartTriggerEnabled: boolean;
  history: SyncRegistryHistory = {} as SyncRegistryHistory;

  constructor(
    private roleUtilities: RoleUtilitiesService,
    private route: ActivatedRoute,
    private image: ImageService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private location: Location,
    private router: Router,
    private modal: ModalService,
    private errorsToast: ErrorsToastService,
  ) {}

  async ngOnInit() {
    combineLatest(this.route.paramMap, this.route.queryParamMap).subscribe(
      async ([paramMap, queryParamMap]) => {
        this.historyId = paramMap.get('historyId');
        this.initialized = false;

        this.restartTriggerEnabled = await this.roleUtilities.resourceTypeSupportPermissions(
          SYNC_CONFIG,
          { sync_config_name: queryParamMap.get('configName') },
          'trigger',
        );

        await this.startPolling();

        this.initialized = true;
      },
    );
  }

  ngOnDestroy() {
    this.destroyed = true;
  }

  private async startPolling() {
    try {
      await this.loadData();
    } catch (e) {}

    await delay(10000);

    if (this.shouldPollHistory()) {
      this.startPolling();
    }
  }

  private shouldPollHistory() {
    return (
      !this.destroyed &&
      (!this.history || !['F', 'S', 'D'].includes(this.history.status))
    );
  }

  buttonDisplayExpr(action: string) {
    return this.roleUtilities.resourceHasPermission(
      this.history,
      SYNC_HISTORY,
      action,
    );
  }

  restartClicked() {
    const modalRef = this.modal.open(ImageSyncTriggerComponent, {
      title: this.translate.get('image_sync_start'),
      data: {
        configName: this.history.config_name,
        initDestId: this.history.dest_id,
      },
    });

    modalRef.componentInstance.close
      .pipe(take(1))
      .subscribe((ids: string[]) => {
        if (ids) {
          this.router.navigate(['/image/sync-history/detail/', ids[0]]);
        }
      });
  }

  async deleteClicked() {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('image_sync_history_delete_confirm_info'),
      });
    } catch (e) {
      return;
    }

    try {
      await this.image.deleteHistory(this.historyId);
    } catch (e) {
      return this.errorsToast.error(e);
    }

    this.router.navigateByUrl('/image/sync-history');
  }

  async loadData() {
    try {
      this.history = await this.image.getSyncRegistryHistory(this.historyId);
    } catch (e) {
      if (!this.initialized && e.status !== 403) {
        this.auiNotificationService.warning(
          this.translate.get('history_repository_not_exist'),
        );

        this.location.back();
      }

      return;
    }

    this.history.duration = this.calculateTimeCost(this.history);
  }

  calculateTimeCost(history: SyncRegistryHistory) {
    if (history.started_at && history.finished_at) {
      const startedAt = +new Date(history.started_at);
      const endedAt = +new Date(history.finished_at);
      return endedAt - startedAt;
    } else {
      return -1;
    }
  }

  getSyncHistoryIconStatus() {
    return this.image.getSyncHistoryIconStatus(this.history);
  }
}
