import {
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';

import {
  ImageService,
  SyncRegistryHistory,
  SyncRegistryHistoryLog,
  SyncRegistryHistoryLogParams,
} from 'app2/features/image/image.service';

@Component({
  selector: 'rc-image-sync-history-log',
  templateUrl: './image-sync-history-log.component.html',
})
export class ImageSyncHistoryLogComponent implements OnInit, OnDestroy {
  @Input()
  history: SyncRegistryHistory;
  @ViewChild('logList')
  logList: ElementRef;

  lastLogTime: number;

  destroyed: boolean;
  timer: any;
  loading: boolean;
  waiting: boolean;
  limit = 2000;
  logs: SyncRegistryHistoryLog[] = [];

  constructor(private image: ImageService) {}

  ngOnInit() {
    this.waiting = false;
    this.polling();
  }

  ngOnDestroy() {
    this.destroyed = true;
    clearTimeout(this.timer);
  }

  private nextTick() {
    if (this.destroyed) {
      return;
    }
    this.timer = setTimeout(() => {
      this.polling();
    }, 10000);
  }

  private convertToTimestamp(dateStr: string) {
    const DATE_REG = /^(\d{1,4})-(\d{1,2})-(\d{1,2})T(\d{1,2}):(\d{1,2}):(\d{1,2})\.(.+)$/;
    const result = dateStr.match(DATE_REG);
    const dt = new Date(
      Date.UTC(
        +result[1],
        +result[2] - 1,
        +result[3],
        +result[4],
        +result[5],
        +result[6],
      ),
    );
    return dt.getTime() / 1000;
  }

  private getParams() {
    const params: SyncRegistryHistoryLogParams = {
      history_id: this.history.history_id,
    };
    if (this.lastLogTime) {
      params.start_time = this.lastLogTime + 1;
    } else {
      params.start_time =
        this.convertToTimestamp(this.history.created_at) - 1800;
    }
    if (this.history.finished_at) {
      params.end_time =
        this.convertToTimestamp(this.history.finished_at) + 3600;
    } else {
      params.end_time = params.start_time + 3600 * 10;
    }
    return params;
  }

  private async polling() {
    const status = this.history.status;
    if (['W', 'B'].includes(status)) {
      // waiting, wait for next tick
      this.waiting = true;
      this.nextTick();
    } else if (status === 'I') {
      // inprogress, continue
      this.waiting = false;
      await this.loadData();
      this.nextTick();
    } else {
      // finished, poll once
      this.waiting = false;
      await this.loadData();
    }
  }

  private async loadData() {
    this.loading = true;
    let retry = false;
    let logsFraction: SyncRegistryHistoryLog[] = [];
    let logs: SyncRegistryHistoryLog[] = [];
    const params = this.getParams();
    do {
      try {
        logsFraction = await this.image.getSyncRegistryHistoryLogs(
          this.getParams(),
        );
        logs = logsFraction.concat(logs);
        retry = false;
        if (logsFraction.length >= this.limit) {
          const previousEndTime = params.end_time;
          // There may be duplicates since the 'time' for logs is not with enough precision
          // we may need to fix it (remove the duplicates)
          params.end_time = logsFraction[0].time + 1;
          if (
            previousEndTime === params.end_time ||
            params.start_time >= params.end_time
          ) {
            break;
          }
        }
      } catch (e) {
        if (retry) {
          break;
        } else {
          retry = true; // retry one more time
        }
      }
    } while ((logsFraction.length >= this.limit || retry) && !this.destroyed);

    this.loading = false;
    if (logs.length) {
      this.lastLogTime = logs[logs.length - 1].time;
      this.logs = this.logs.concat(logs);
      let scroll = false;
      const logList = this.logList.nativeElement as HTMLUListElement;
      if (!this.lastLogTime) {
        scroll = true;
      } else {
        scroll =
          logList.clientHeight + logList.scrollTop + 5 >= logList.scrollHeight;
      }
      if (scroll) {
        setTimeout(() => {
          logList.scrollTop = logList.scrollHeight;
        }, 100);
      }
    }
  }

  clearLogs() {
    this.logs = [];
  }
}
