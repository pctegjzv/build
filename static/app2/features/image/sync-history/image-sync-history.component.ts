import { Component, Input, OnInit } from '@angular/core';
import { ImageSyncCreateComponent } from 'app2/features/image/sync-center/image-sync-create.component';
import { ImageSyncTriggerComponent } from 'app2/features/image/sync-center/image-sync-trigger.component';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import { take } from 'rxjs/operators';

import {
  ImageService,
  SyncRegistryConfig,
  SyncRegistryHistory,
} from '../image.service';

const SYNC_CONFIG = 'sync_config';

@Component({
  selector: 'rc-image-sync-history',
  styleUrls: ['./image-sync-history.component.scss'],
  templateUrl: './image-sync-history.component.html',
})
export class ImageSyncHistoryComponent implements OnInit {
  @Input()
  configName: string;
  @Input()
  registryName: string;
  @Input()
  repositoryName: string;
  @Input()
  projectName: string;

  createEnabled: boolean;
  triggerEnabled: boolean;

  histories: SyncRegistryHistory[];
  loading: boolean;
  pageSize = 20;
  pageCount: number;

  constructor(
    private roleUtilities: RoleUtilitiesService,
    private image: ImageService,
    private modal: ModalService,
    private translate: TranslateService,
  ) {}

  async ngOnInit() {
    const [
      createEnabled,
      triggerEnabled,
    ] = await this.roleUtilities.resourceTypeSupportPermissions(
      SYNC_CONFIG,
      {},
      ['create', 'trigger'],
    );

    this.createEnabled = createEnabled;
    this.triggerEnabled = triggerEnabled;

    await this.loadData();
  }

  calculateTimeCost(history: SyncRegistryHistory) {
    if (history.started_at && history.finished_at) {
      const startedAt = +new Date(history.started_at);
      const endedAt = +new Date(history.finished_at);
      return endedAt - startedAt;
    } else {
      return -1;
    }
  }

  async loadData(page?: number) {
    this.loading = true;
    const {
      num_pages: pageCount,
      result_list: histories,
    } = await this.image.getSyncRegistryHistories({
      config_name: this.configName,
      repo_name: this.repositoryName,
      registry_name: this.registryName,
      repository_project_name: this.projectName,
      num_per_page: this.pageSize,
      page,
    });
    histories.forEach(history => {
      history.duration = this.calculateTimeCost(history);
    });
    this.histories = histories;
    this.pageCount = pageCount;
    this.loading = false;
  }

  startSync() {
    const modalRef = this.modal.open(ImageSyncTriggerComponent, {
      title: this.translate.get('image_sync_start'),
      data: {
        registry: this.registryName,
        project: this.projectName,
        repository: this.repositoryName,
      },
    });

    modalRef.componentInstance.close
      .pipe(take(1))
      .subscribe((historyIds: string[]) => {
        if (historyIds) {
          this.loadData();
        }
      });
  }

  getConfigName(config: SyncRegistryConfig) {
    return this.image.getConfigName(config);
  }

  showCreateDialog() {
    const modalRef = this.modal.open(ImageSyncCreateComponent, {
      title: this.translate.get('image_sync_center_create'),
      width: 900,
      data: {
        registryName: this.registryName,
        projectName: this.projectName,
        repositoryName: this.repositoryName,
        disableRepoSelection: !!this.repositoryName,
      },
    });

    modalRef.componentInstance.close
      .pipe(take(1))
      .subscribe((config: SyncRegistryConfig) => {
        if (config) {
          this.loadData();
        }

        modalRef.close();
      });
  }

  getSyncHistoryIconStatus(history: SyncRegistryHistory) {
    return this.image.getSyncHistoryIconStatus(history);
  }
}
