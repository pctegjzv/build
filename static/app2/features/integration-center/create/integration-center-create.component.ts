import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { IntegrationCenterService } from 'app2/shared/services/features/integration-center.service';
import { QuotaSpaceService } from 'app2/shared/services/features/quota-space.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';

@Component({
  templateUrl: './integration-center-create.component.html',
  styleUrls: ['./integration-center-create.component.scss'],
})
export class IntegrationCenterCreateComponent implements OnInit, OnDestroy {
  private routeParamsSubscription: Subscription;
  spaces: any[];
  configFields: any[];
  typeName: string;
  integrationId: string;
  type: any;
  basicFields: any;
  updateMode = false;
  submitting = false;
  loading = true;
  isBasicInfoCollapse: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private quotaSpaceService: QuotaSpaceService,
    private integrationCenterService: IntegrationCenterService,
    private errorsToastService: ErrorsToastService,
  ) {}

  ngOnInit() {
    this.routeParamsSubscription = this.route.params
      .pipe(first())
      .subscribe(async params => {
        if (params.typeName) {
          this.typeName = params.typeName;
          await this.createFlow();
        } else {
          this.integrationId = params.integrationId;
          await this.updateFlow();
        }
      });
  }

  ngOnDestroy() {
    this.routeParamsSubscription.unsubscribe();
  }

  async createFlow() {
    this.basicFields = {
      space_name: '',
      name: '',
      description: '',
      enabled: true,
    };
    this.quotaSpaceService
      .getConsumableSpaces()
      .then(spaces => {
        this.spaces = spaces || [];
        if (spaces[0]) {
          this.basicFields.space_name = spaces[0].name;
        }
      })
      .catch(() => {
        this.spaces = [];
      });

    try {
      this.type = await this.integrationCenterService.getTypeByName(
        this.typeName,
      );
      if (!this.type) {
        throw new Error('Unknown Integration Type');
      }
      this.loading = false;
    } catch (err) {
      this.errorsToastService.error(err);
    }

    this.buildConfigFields();
  }

  async updateFlow() {
    this.updateMode = true;
    this.isBasicInfoCollapse = true;

    const p1 = this.integrationCenterService.getIntegrationById(
      this.integrationId,
    );
    const p2 = this.integrationCenterService.getIntegrationCatalog();
    try {
      let integration: any, catalog: any;
      [integration, catalog] = await Promise.all([p1, p2]);

      this.type = catalog.types.find(
        (type: any) => type.name === integration.type,
      );
      if (!this.type) {
        throw new Error('Unknown Integration Type');
      }

      this.spaces = [{ name: integration.space_name }];
      this.basicFields = {
        space_name: integration.space_name,
        name: integration.name,
        description: integration.description,
        enabled: integration.enabled,
      };
      this.buildConfigFields(integration);
      this.loading = false;
    } catch (err) {
      this.errorsToastService.error(err);
    }
  }

  buildConfigFields(integration?: any) {
    this.configFields = [];
    const schema = this.type.schema;
    Object.entries(schema)
      .sort(([, item1], [, item2]) => {
        return item1['order'] - item2['order'];
      })
      .forEach(([key, item]) => {
        if (item['type'] === 'template') {
          return;
        }
        this.configFields.push(
          this.parseConfigField(
            key,
            item,
            integration && integration.fields[key],
          ),
        );
      });
  }

  parseConfigField(key: any, item: any, value: any) {
    let type, pattern, multiple_of;
    if (item.enum) {
      type = 'enum';
    } else {
      switch (item.type) {
        case 'boolean':
        case 'password':
        case 'number':
          type = item.type;
          break;
        case 'integer':
          type = 'number';
          break;
        default:
          type = 'text';
      }
    }
    if (type === 'text') {
      pattern = new RegExp(item.pattern);
    } else if (item.type === 'integer') {
      multiple_of = [1];
      if (item.multiple_of) {
        multiple_of = item.multiple_of;
      }
    } else if (item.type === 'number') {
      multiple_of = item.multiple_of;
    }
    const required = item.required !== false;
    return Object.assign({}, item, {
      name: key,
      type,
      required,
      pattern,
      multiple_of,
      value,
    });
  }

  async submit() {
    this.submitting = true;
    const fields = {};
    this.configFields.forEach(field => {
      fields[field.name] = field.value;
    });
    try {
      let integration: any;
      const params = Object.assign({}, this.basicFields, {
        fields,
        type: this.typeName,
      });
      if (this.updateMode) {
        integration = await this.integrationCenterService.updateIntegration(
          params,
          this.integrationId,
        );
      } else {
        integration = await this.integrationCenterService.createIntegration(
          params,
        );
      }
      this.router.navigateByUrl(`integration_center/detail/${integration.id}`);
    } catch (err) {
      this.errorsToastService.error(err);
    } finally {
      this.submitting = false;
    }
  }

  getImg(type: string) {
    return `/static/images/integration-logo/${type}.png`;
  }

  cancel() {
    this.location.back();
  }
}
