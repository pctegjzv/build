import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';

import { IntegrationCenterCreateComponent } from './create/integration-center-create.component';
import { IntegrationCenterDetailComponent } from './detail/integration-center-detail.component';
import { IntegrationCenterRoutingModule } from './integration-center-routing.module';
import { IntegrationUtilitiesService } from './integration-center.utilities.service';
import { IntegrationCenterCardsComponent } from './list/cards/integration-center-list-cards.component';
import { IntegrationCenterListComponent } from './list/integration-center-list.component';
import { SelectIntegrationTypeDialogComponent } from './select-integration-type-dialog/select-integration-type.component';

@NgModule({
  imports: [SharedModule, IntegrationCenterRoutingModule],
  declarations: [
    IntegrationCenterListComponent,
    IntegrationCenterCardsComponent,
    IntegrationCenterDetailComponent,
    IntegrationCenterCreateComponent,
    SelectIntegrationTypeDialogComponent,
  ],
  entryComponents: [SelectIntegrationTypeDialogComponent],
  providers: [IntegrationUtilitiesService],
})
export class IntegrationCenterModule {}
