import { Injectable } from '@angular/core';

import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';

@Injectable()
export class IntegrationUtilitiesService {
  constructor(private roleUtilities: RoleUtilitiesService) {}
  showIntegrationUpdate(integration: any) {
    return this.roleUtilities.resourceHasPermission(
      integration,
      'integration',
      'update',
    );
  }

  showIntegrationDelete(integration: any) {
    return this.roleUtilities.resourceHasPermission(
      integration,
      'integration',
      'delete',
    );
  }
}
