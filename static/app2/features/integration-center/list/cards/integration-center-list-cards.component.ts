import { Component, Input, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import { first } from 'rxjs/operators';

import { SelectIntegrationTypeDialogComponent } from '../../select-integration-type-dialog/select-integration-type.component';

@Component({
  selector: 'rc-integration-center-list-cards',
  templateUrl: './integration-center-list-cards.component.html',
  styleUrls: ['./integration-center-list-cards.component.scss'],
})
export class IntegrationCenterCardsComponent implements OnChanges {
  @Input()
  familyName: any;
  @Input()
  integrations: any;
  @Input()
  couldCreate: any;
  @Input()
  catalog: any;
  keyWord = '';
  showIntegrations: any;

  constructor(
    private router: Router,
    private modal: ModalService,
    private translate: TranslateService,
  ) {}

  ngOnChanges() {
    this.showIntegrations = this.integrations;
  }

  getDetailHref(id: string) {
    this.router.navigateByUrl(`integration_center/detail/${id}`);
  }

  getImg(type: string) {
    return `/static/images/integration-logo/${type}.png`;
  }

  filterIntegrations() {
    this.showIntegrations = this.integrations.filter((integration: any) => {
      return integration.name.indexOf(this.keyWord) >= 0;
    });
  }

  searchChanged(queryString: string) {
    this.keyWord = queryString;
    this.filterIntegrations();
  }

  addIntegration() {
    const modelRef = this.modal.open(SelectIntegrationTypeDialogComponent, {
      title: this.translate.get('integration_add_integration'),
    });
    modelRef.componentInstance.catalog = this.catalog;
    modelRef.componentInstance.finished
      .pipe(first())
      .subscribe(() => modelRef.close());
  }
}
