import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './select-integration-type.component.html',
  styleUrls: ['./select-integration-type.component.scss'],
})
export class SelectIntegrationTypeDialogComponent implements OnInit {
  @Input()
  catalog: any;
  @Output()
  finished = new EventEmitter<any>();
  families: any;
  allTypes: any;
  types: any;
  selectedFamily: any;
  loading = false;

  constructor(
    private translate: TranslateService,
    private errorsToastService: ErrorsToastService,
    private router: Router,
  ) {}

  cancel() {
    this.complete();
  }

  private complete(res?: any) {
    this.finished.next(res);
    this.finished.complete();
  }

  async ngOnInit() {
    this.loading = true;
    try {
      this.families = [
        {
          displayName: this.translate.get('all'),
          name: '_all',
        },
      ];
      this.catalog.families.forEach((family: any) => {
        this.families.push({
          displayName: this.translate.get('integration_family_name_' + family),
          name: family,
        });
      });
      this.allTypes = this.catalog.types;
      this.selectedFamily = this.families[1].name;
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
    this.loading = false;
  }

  onFamilyChanged(familyName: string) {
    if (familyName === '_all') {
      this.types = this.allTypes;
    } else {
      this.types = this.allTypes.filter((type: any) =>
        type.families.includes(familyName),
      );
    }
  }

  onSelected(typeName: string) {
    this.complete();
    this.router.navigateByUrl(`integration_center/create/${typeName}`);
  }
}
