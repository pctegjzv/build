import { Action } from '@ngrx/store';

import { createTypes } from '../utils/types';

enum ActionTypes {
  Get = 'Get',
  Delete = 'Delete',
  Find = 'Find',
  FindComplete = 'FindComplete',
  Sources = 'Sources',
}

export const types = createTypes('@jenkins.template', ActionTypes);

export class Find implements Action {
  public readonly type = types.Find;
  constructor(
    public search: string,
    public template_type: string,
    public source_uuids: string[],
    public official: boolean,
  ) {}
}

export class Get implements Action {
  public readonly type = types.Get;
  constructor(public id: string, public template_uuid: string) {}
}

export class Delete implements Action {
  public readonly type = types.Delete;
  constructor(public id: string, public template_uuid: string) {}
}

export class FindComplete implements Action {
  public readonly type = types.FindComplete;
  constructor(public items: any[]) {}
}

export type All = Get | Delete | Find | FindComplete;
