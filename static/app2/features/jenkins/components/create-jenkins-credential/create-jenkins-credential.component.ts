import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { JenkinsService } from 'app2/shared/services/features/jenkins.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';

import { JenkinsMessageService } from '../../services/jenkins-message.service';

@Component({
  styleUrls: ['./create-jenkins-credential.component.scss'],
  templateUrl: './create-jenkins-credential.component.html',
})
export class CreateJenkinsCredentialComponent {
  @Output()
  close = new EventEmitter<string>();
  form: FormGroup;

  constructor(
    private jenkins: JenkinsService,
    private message: JenkinsMessageService,
    private fb: FormBuilder,
    @Inject(MODAL_DATA) public data: any,
  ) {
    this.form = this.fb.group({
      name: '',
      username: '',
      password: '',
    });
  }

  onSubmit() {
    if (this.form.status === 'VALID') {
      this.jenkins.credentials
        .create({
          ...this.form.value,
          type: 'Username with password',
          jenkins_integration_id: this.data.jenkins_integration_id,
        })
        .then((response: any) => {
          this.close.next(response.name);
        })
        .catch(error => this.message.error(error));
    }
  }

  closeModal() {
    this.close.next();
  }
}
