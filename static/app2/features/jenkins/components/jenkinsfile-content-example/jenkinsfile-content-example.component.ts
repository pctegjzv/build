import { Component, Inject } from '@angular/core';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';

@Component({
  templateUrl: './jenkinsfile-content-example.component.html',
  styleUrls: ['./jenkinsfile-content-example.component.scss'],
})
export class JenkinsfileContentExampleComponent {
  content: string;
  constructor(@Inject(MODAL_DATA) private data: { content: string }) {
    this.content = this.data.content;
  }
}
