import { Component } from '@angular/core';

import { ZH_HELP } from '../../template-sources-help.zh_cn';
@Component({
  templateUrl: './pipeline-custom-template-man.component.html',
  styleUrls: ['./pipeline-custom-template-man.component.scss'],
})
export class PipelineCustomTemplateManComponent {
  help = ZH_HELP;
}
