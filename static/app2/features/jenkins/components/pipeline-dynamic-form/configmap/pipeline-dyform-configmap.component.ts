import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  Component,
  EventEmitter,
  HostBinding,
  Input,
  Optional,
  Output,
  Self,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';

import { FormFieldControl } from 'app2/shared/form-field-control';
import { Subject } from 'rxjs';
let nextUniqueId = 0;

/**
 * value:
 * [{configMapRef: {"name"}}]
 */
interface ConfigMapRef {
  configMapRef: { name: string };
}

@Component({
  selector: 'rc-pipeline-dyform-configmap',
  templateUrl: './pipeline-dyform-configmap.component.html',
  providers: [
    {
      provide: FormFieldControl,
      useExisting: PipelineDyfromConfigMapComponent,
    },
  ],
})
export class PipelineDyfromConfigMapComponent
  implements FormFieldControl, ControlValueAccessor {
  private _uniqueId = `rc-pipeline-dyform-configmap-${++nextUniqueId}`;

  _values: { configMapRef: { name: string } }[];

  stateChanges = new Subject<void>();

  configmaps: string[];

  _required: any;
  _disabled: any;

  @Output()
  changed = new EventEmitter<ConfigMapRef[]>();

  @Input()
  id = this._uniqueId;

  @Input()
  options: { name: string }[];

  @Input()
  get value(): ConfigMapRef[] {
    return this._values;
  }

  set value(value: ConfigMapRef[]) {
    this.stateChanges.next();
    this.valueChanged(value);
    this.changed.emit(value);
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }

    this._disabled = disabled;
    this.stateChanges.next();
  }

  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);

    return !!(isInvalid && (isDirty || isSubmitted));
  }

  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() private parentForm: NgForm,
    @Optional() private parentFormGroup: FormGroupDirective,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }

  writeValue(value: any[]) {
    if (!value) {
      return;
    }
    this._values = value;
  }

  private valueChanged = (_: any) => {};

  updateModelValue() {
    this._values = this.configmaps.map((item: string) => {
      return {
        configMapRef: { name: item },
      };
    });
    this.value = this._values;
  }

  registerOnChange(fn: any) {
    this.valueChanged = fn;
  }

  registerOnTouched() {}
}
