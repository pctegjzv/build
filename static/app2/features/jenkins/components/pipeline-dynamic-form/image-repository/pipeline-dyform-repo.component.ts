import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnInit,
  Optional,
  Output,
  Self,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';
import { Subject } from 'rxjs';
import { first } from 'rxjs/operators';

import { ImageSelectComponent } from 'app2/features/service/image-select/image-select.component';
import { RcImageSelection } from 'app2/features/service/service.type';
import { FormFieldControl } from 'app2/shared/form-field-control';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';

let nextUniqueId = 0;

@Component({
  selector: 'rc-pipeline-dyform-repository',
  templateUrl: './pipeline-dyform-repo.component.html',
  styleUrls: ['./pipeline-dyform-repo.component.scss'],
  providers: [
    {
      provide: FormFieldControl,
      useExisting: PipelineDyformrRepositoryComponent,
    },
  ],
})
export class PipelineDyformrRepositoryComponent
  implements FormFieldControl, ControlValueAccessor, OnInit {
  _uniqueId = `rc-pipeline-dyform-repository-${++nextUniqueId}`;
  repo_addr: string;
  _required: any;
  _disabled: any;
  stateChanges = new Subject<void>();
  @Output()
  change = new EventEmitter<string>();
  @Input()
  id = this._uniqueId;

  @Input()
  name: string | null = null;
  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() private parentForm: NgForm,
    @Optional() private parentFormGroup: FormGroupDirective,
    private modalService: ModalService,
    private translate: TranslateService,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }

  ngOnInit() {}

  @Input()
  get value(): any {
    return this.ngControl ? this.ngControl.value : this.repo_addr;
  }

  set value(value: any) {
    if (value === this.value) {
      return;
    }
    this.stateChanges.next();
    this.onChange(value);
    this.change.emit(value);
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }

    this._disabled = disabled;
    this.stateChanges.next();
  }

  writeValue(value: any): void {
    this.repo_addr = value && `${value.registry}/${value.repository}`;
  }

  onChange(_value: any) {}

  registerOnChange(fn: (_: any) => void) {
    this.onChange = fn;
  }

  registerOnTouched() {}

  async selectImageRepo() {
    try {
      const modalRef = await this.modalService.open(ImageSelectComponent, {
        title: this.translate.get('nav_select_image'),
        width: 800,
      });
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe((res: RcImageSelection) => {
          modalRef.close();
          if (res) {
            this.repo_addr = res.full_image_name;
            this.value = this.handleRepoInfo(res.full_image_name);
          }
        });
    } catch (error) {
      //
    }
  }

  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);
    return !!(isInvalid && (isDirty || isSubmitted));
  }

  private handleRepoInfo(path: string) {
    const temp = path && path.split('/');
    if (temp.length) {
      return {
        registry: temp.shift(),
        registryUUID: '',
        repository: temp.join('/'),
        repositoryUUID: '',
      };
    }
  }
}
