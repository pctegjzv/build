import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  AfterViewInit,
  Component,
  Directive,
  HostBinding,
  Input,
  OnChanges,
  OnDestroy,
  Optional,
  QueryList,
  Self,
  SimpleChange,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormGroupDirective,
  NG_VALIDATORS,
  NgControl,
  NgForm,
  Validator,
} from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { SingleSelectionDropdownComponent } from 'app2/shared/components/select/single-selection-dropdown.component';
import { FormFieldControl } from 'app2/shared/form-field-control';
import {
  ConfigMapOption,
  ConfigmapService,
} from 'app2/shared/services/features/configmap.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { TranslateService } from 'app2/translate/translate.service';

import * as _ from 'lodash';

let nextUniqueId = 0;

@Component({
  selector: 'rc-pipeline-dyform-env',
  templateUrl: './pipeline-dyform-env.componet.html',
  styleUrls: ['./pipeline-dyform-env.componet.scss'],
  providers: [
    {
      provide: FormFieldControl,
      useExisting: PipelineDyformEnvComponent,
    },
  ],
})
export class PipelineDyformEnvComponent
  implements
    FormFieldControl,
    ControlValueAccessor,
    OnDestroy,
    OnChanges,
    AfterViewInit {
  private _value: any;
  private _uniqueId = `rc-container-fields-envvars-${++nextUniqueId}`;
  private _required = false;
  protected _disabled = false;

  @ViewChild('form')
  form: NgForm;

  @Input()
  id = this._uniqueId;

  stateChanges = new Subject<void>();

  @Input()
  get value(): Array<any> {
    return this.ngControl ? this.ngControl.value : this._value;
  }

  set value(value: Array<any>) {
    this._value = value;
    this.updateModelValue();
    this.applyToNative();
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }
    this._disabled = disabled;
    this.stateChanges.next();
  }

  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);

    return !!(isInvalid && (isDirty || isSubmitted));
  }

  controlType = 'rc-container-fields-envvars';

  /***** FormFieldControl end *****/
  private index = 0;
  private envVars: Array<any>;
  @ViewChildren('configmapDropdown')
  configmapDropdown: QueryList<SingleSelectionDropdownComponent>;
  @ViewChildren('configmapKeyDropdown')
  configmapKeyDropdown: QueryList<SingleSelectionDropdownComponent>;

  @Input()
  namespace: string;

  @Input()
  clusterName: string;

  region: Region;
  envVarReg = {
    pattern: /^[-._a-zA-Z][-._a-zA-Z0-9]*$/,
    tip: 'regexp_tip_k8s_env_vars_name',
  };
  errorMappers = {
    name: {
      pattern: this.translate.get('regexp_tip_k8s_env_vars_name'),
    },
  };
  configmapOptions: ConfigMapOption[] = [];
  configmapKeyOptionsCache: {
    [key: string]: Array<{
      name: string;
      value: string;
    }>;
  } = {};
  private formSubscription: Subscription;

  addEnvVar() {
    if (this.disabled) {
      return;
    }
    if (!this.envVars) {
      this.envVars = [];
      return;
    }
    this.envVars.push({
      index: this.index++,
      name: '',
      value: '',
      reference: false,
      configmap: '',
      configmapKey: '',
    });
  }

  deleteEnvVar(index: number) {
    if (this.disabled) {
      return;
    }
    _.remove(this.envVars, (item: any) => {
      return index === item.index;
    });
  }

  trackByIndex(_index: number, item: any) {
    return item.index;
  }

  getK8sConfigmapOptions(): Promise<ConfigMapOption[]> {
    return this.configmapService.getConfigMapOptions({
      clusterId: this.region.id,
      namespace: this.namespace,
    });
  }

  configmapKeyOptions(name: string) {
    return this.configmapKeyOptionsCache[name]
      ? this.configmapKeyOptionsCache[name]
      : [];
  }

  async getConfigmapKeys(config: ConfigMapOption) {
    const configmap = await this.configmapService
      .getK8sConfigmap({
        clusterId: this.region.id,
        namespace: config.namespace,
        name: config.name,
      })
      .then(res => res.kubernetes.data);
    this.configmapKeyOptionsCache[config.name] = Object.keys(configmap).map(
      k => ({
        name: k,
        value: configmap[k],
      }),
    );
  }

  onConfigmapChange(change: ConfigMapOption) {
    if (!change || typeof change === 'symbol') {
      return;
    }
    if (this.configmapKeyOptionsCache[change.name]) {
      return;
    } else {
      this.getConfigmapKeys(change);
    }
  }

  onConfigmapModelChange(change: string, i: number) {
    if (change) {
      const found = _.find(this.envVars, item => {
        return i === item.index;
      });
      if (found) {
        found.configmapKey = '';
      }
    }
  }

  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() private parentForm: NgForm,
    @Optional() private parentFormGroup: FormGroupDirective,
    private translate: TranslateService,
    private regionService: RegionService,
    private configmapService: ConfigmapService,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
    this.addEnvVar();
  }

  async ngOnChanges({
    namespace,
    clusterName,
  }: {
    namespace: SimpleChange;
    clusterName: SimpleChange;
  }) {
    if (
      namespace &&
      namespace.currentValue &&
      clusterName &&
      clusterName.currentValue
    ) {
      this.region = await this.regionService.getRegionByName(
        clusterName.currentValue,
      );

      this.configmapOptions = await this.getK8sConfigmapOptions();
      if (namespace) {
        this.namespace = namespace.currentValue;
        this.configmapOptions = this.configmapOptions.filter(
          item => item.namespace === namespace.currentValue,
        );
      }
      this.configmapDropdown.forEach((item: any) => {
        item.setSelectedValue('');
      });
      this.configmapKeyDropdown.forEach((item: any) => {
        item.setSelectedValue('');
      });
    }
  }

  ngAfterViewInit() {
    this.formSubscription = this.form.form.valueChanges
      .pipe(debounceTime(300))
      .subscribe(() => {
        const value = _.cloneDeep(this.envVars).filter(item => {
          return (
            !!item.name && (item.reference ? !!item.configmapKey : !!item.value)
          );
        });
        this._value = this.formatOutputValue(value);
        this.updateModelValue();
      });
  }

  ngOnDestroy() {
    this.formSubscription.unsubscribe();
  }

  formatOutputValue(list: any) {
    const result: any[] = [];
    list.forEach((item: any) => {
      let cur;
      if (item.reference) {
        cur = {
          name: item.name,
          valueFrom: {
            configMapKeyRef: {
              name: item.configmap,
              key: item.configmapKey,
            },
          },
        };
      } else {
        cur = {
          name: item.name,
          value: item.value,
        };
      }
      result.push(cur);
    });
    return result;
  }
  formatInputValue(list: any) {
    const result: any[] = [];
    list.forEach((item: any) => {
      const cur = {
        index: this.index++,
        name: item.name,
        value: '',
        reference: false,
        configmap: '',
        configmapKey: '',
      };
      if (item.value) {
        cur.value = item.value;
      } else {
        cur.reference = true;
        cur.configmap = item.valueFrom.configMapKeyRef.name;
        cur.configmapKey = item.valueFrom.configMapKeyRef.key;
      }
      result.push(cur);
    });
    return result;
  }

  // set ngModel value when native view changes
  private updateModelValue() {
    this.emitToModel();
  }

  private emitToModel() {
    this.onChangeCallback(this._value);
    this.onTouchedCallback();
  }

  private applyToNative() {
    this.envVars = this.formatInputValue(this._value);
  }

  /** Following Methods Implements ControlValueAccessor **/

  writeValue(value: Array<any>) {
    if (!value || !_.isArray(value)) {
      return;
    }
    this._value = value;
    this.applyToNative();
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}

@Directive({
  selector: 'rc-container-fields-envvars[rcEnvVarsValid]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: ContainerFieldsEnvvarsValidatorDirective,
      multi: true,
    },
  ],
})
export class ContainerFieldsEnvvarsValidatorDirective implements Validator {
  validate(control: AbstractControl): { [key: string]: any } {
    return this.envvarsValidator(control);
  }

  envvarsValidator(control: AbstractControl): { [key: string]: any } {
    const hasVal =
      control.value &&
      control.value.every(
        (item: any) =>
          item.name !== '' &&
          (item.reference ? item.configmapKey : item.value !== ''),
      );
    if (control.value) {
      return !hasVal
        ? {
            env_vars_invalid: true,
          }
        : null;
    }
  }
}
