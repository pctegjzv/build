import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnInit,
  Optional,
  Output,
  Self,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';
import {
  K8sComponent,
  K8sComponentInfo,
} from 'app2/features/service/service.type';
import { FormFieldControl } from 'app2/shared/form-field-control';
import {
  NamespaceOption,
  NamespaceService,
} from 'app2/shared/services/features/namespace.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { find, flatten, get } from 'lodash';
import { Subject } from 'rxjs';

interface ServiceContainerMix {
  clusterName: string;
  namespace: string;
  serviceName: string;
  containerName: string;
}
let nextUniqueId = 0;

@Component({
  selector: 'rc-pipeline-template-service-container',
  templateUrl: './pipeline-service-container.component.html',
  styleUrls: ['./pipeline-service-container.component.scss'],
  providers: [
    {
      provide: FormFieldControl,
      useExisting: PipelineServiceContainerComponent,
    },
  ],
})
export class PipelineServiceContainerComponent
  implements FormFieldControl, ControlValueAccessor, OnInit {
  _model: ServiceContainerMix;
  _uniqueId = `rc-pipeline-template-service-container-${++nextUniqueId}`;

  _required: any;
  _disabled: any;
  stateChanges = new Subject<void>();

  clusterOptions: any[] = [];
  namespaceOptions: NamespaceOption[] = [];
  serviceOptions: K8sComponentInfo[] = [];
  containerOptions: any[] = [];

  clusterLoading = false;
  namespaceLoading = false;
  serviceLoading = false;
  containerLoading = false;

  editBindingMode = false;

  controlType = 'rc-pipeline-template-service-container';

  @Input()
  id = this._uniqueId;

  @Input()
  name: string | null = null;
  @Output()
  change = new EventEmitter<ServiceContainerMix>();
  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() private parentForm: NgForm,
    @Optional() private parentFormGroup: FormGroupDirective,
    private regionService: RegionService,
    private regionUtilitiesService: RegionUtilitiesService,
    private namespaceService: NamespaceService,
    private serviceService: ServiceService,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
    this._model = {
      clusterName: '',
      namespace: '',
      serviceName: '',
      containerName: '',
    };
  }

  ngOnInit() {
    Promise.all([
      this.getCluster(),
      this.getNamespace(),
      this.getServiceName(),
      this.getContainers(),
    ]);
  }

  getCluster() {
    this.clusterLoading = true;
    return this.regionService
      .getRegions()
      .then((regions: Region[]) => {
        this.clusterOptions = regions.filter(region =>
          this.regionUtilitiesService.isNewK8sRegion(region),
        );
      })
      .catch(() => {})
      .then(() => {
        this.clusterLoading = false;
      });
  }

  async getNamespace() {
    if (!this.value || !this.value.clusterName) {
      return;
    }
    this.namespaceLoading = true;
    const region = await this.regionService.getRegionByName(
      this.value.clusterName,
    );
    this.namespaceOptions = await this.namespaceService.getNamespaceOptions(
      region.id,
    );
    this.namespaceLoading = false;
  }

  getServiceName() {
    if (!this.value || !this.value.clusterName) {
      return;
    }
    this.serviceLoading = true;
    return this.serviceService
      .getRegionServices({
        region_name: this.value.clusterName,
        basic: true,
        region_platform_version: 'v3',
      })
      .then((services: K8sComponentInfo[]) => {
        if (this.value.namespace) {
          this.serviceOptions = services.filter(
            s => get(s, 'service_namespace') === this.value.namespace,
          );
          if (this.serviceOptions.length) {
            this.getContainers();
          }
        }
      })
      .catch(() => {})
      .then(() => {
        this.serviceLoading = false;
      });
  }

  getContainers() {
    const currentService = find(this.serviceOptions, {
      service_name: this.value && this.value.serviceName,
    });
    if (!currentService) {
      return;
    }
    this.containerLoading = true;
    return this.serviceService
      .getK8sService(currentService.uuid)
      .then((service: K8sComponent) => {
        this.containerOptions = flatten(
          this.findContainers(get(service, 'kubernetes')),
        );
      })
      .catch(() => {})
      .then(() => {
        this.containerLoading = false;
      });
  }

  private findContainers(kubernetes: any): any[] {
    try {
      return kubernetes
        .filter((service: any) => {
          return ['Deployment', 'StatefulSet', 'DaemonSet'].includes(
            service.kind,
          );
        })
        .map((service: any) => {
          return get(service, 'spec.template.spec.containers');
        });
    } catch (e) {}
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }

    this._disabled = disabled;
    this.stateChanges.next();
  }

  @Input()
  get value(): ServiceContainerMix {
    return this.ngControl ? this.ngControl.value : this._model;
  }

  set value(value: ServiceContainerMix) {
    if (value === this.value) {
      return;
    }
    this.stateChanges.next();
    this.onChange(value);
    this.change.emit(value);
  }

  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);
    return !!(isInvalid && (isDirty || isSubmitted));
  }

  @Input()
  get clusterName() {
    return this.value ? this.value.clusterName : '';
  }
  set clusterName(clusterName: any) {
    if (this._model.clusterName && clusterName !== this._model.clusterName) {
      this.editBindingMode = false;
    }
    this.value = { ...this.value, clusterName };
    if (clusterName && this.clusterOptions.length) {
      if (!this.editBindingMode) {
        this.namespace = '';
        this.serviceName = '';
        this.containerName = '';
      }
      this.getNamespace();
    }
  }

  @Input()
  get namespace() {
    return this.value ? this.value.namespace : '';
  }
  set namespace(namespace: string) {
    this.value = { ...this.value, namespace };
    if (namespace && this.namespaceOptions.length) {
      if (!this.editBindingMode) {
        this.serviceName = '';
        this.containerName = '';
      }
      this.getServiceName();
    }
  }

  @Input()
  get serviceName() {
    return this.value ? this.value.serviceName : '';
  }
  set serviceName(serviceName: string) {
    this.value = { ...this.value, serviceName };
    if (serviceName && this.serviceOptions.length) {
      if (!this.editBindingMode) {
        this.containerName = '';
      }
      this.getContainers();
    }
  }

  @Input()
  get containerName() {
    return this.value ? this.value.containerName : '';
  }
  set containerName(containerName: string) {
    this.value = { ...this.value, containerName };
  }

  writeValue(value: any): void {
    this.editBindingMode = !!value;
    this._model = value || {
      clusterName: '',
      namespace: '',
      serviceName: '',
      containerName: '',
    };
  }

  onChange(value: any) {
    this.value = value;
  }

  registerOnChange(fn: (_: any) => void) {
    this.onChange = fn;
  }

  registerOnTouched() {}
}
