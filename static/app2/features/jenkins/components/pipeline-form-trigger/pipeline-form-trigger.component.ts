import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ImageProjectService } from 'app2/shared/services/features/image-project.service';
import { ImageRegistryService } from 'app2/shared/services/features/image-registry.service';
import { ImageRepositoryService } from 'app2/shared/services/features/image-repository.service';
import { TranslateService } from 'app2/translate/translate.service';
import { head } from 'lodash';
import { Subject, Subscription } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

import { JenkinsMessageService } from '../../services/jenkins-message.service';
import { CancelableTask } from '../../utils/cancelable-task';

@Component({
  selector: 'rc-pipeline-form-trigger',
  styleUrls: ['pipeline-form-trigger.component.scss'],
  templateUrl: 'pipeline-form-trigger.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineFormTriggerComponent implements OnInit, OnDestroy {
  @Input()
  form: FormGroup;
  @Input()
  codeRepoSource: string;
  @Output()
  remove = new EventEmitter<void>();
  onImageRegistryChange$ = new Subject<string>();
  onImageProjectChange$ = new Subject<string>();
  useImageTagFilterOptions = [
    {
      displayName: this.translate.get('trigger_all'),
      value: false,
    },
    {
      displayName: this.translate.get('trigger_target'),
      value: true,
    },
  ];

  private _imageRegistries: any[];
  private _imageProjects: any[];
  private _imageRepositories: any[];
  private destroyed = false;
  private subs: Subscription[] = [];

  private fetchImageRegistries = new CancelableTask(async () => {
    return await this.imageRegistryService.find().catch(error => {
      this.errorMessage(error);
      return [];
    });
  });

  private fetchImageProjects = new CancelableTask(async (registry: string) => {
    return await this.imageProjectService.find(registry).catch(error => {
      this.errorMessage(error);
      return [];
    });
  });

  private fetchImageRepositories = new CancelableTask(
    async (registry: string, project: string) => {
      return await this.imageRepositoryService
        .getRepositories(registry, project === '@@shared' ? '' : project)
        .catch(error => {
          this.errorMessage(error);
          return [];
        });
    },
  );

  get imageRegistries() {
    return this._imageRegistries;
  }

  set imageRegistries(value: any[]) {
    this.safeSet(() => (this._imageRegistries = value));
  }

  get imageProjects() {
    return this._imageProjects;
  }

  set imageProjects(value: any[]) {
    this.safeSet(() => (this._imageProjects = value));
  }

  set imageRepositories(value: any[]) {
    this.safeSet(() => (this._imageRepositories = value));
  }

  get imageRepositories() {
    return this._imageRepositories;
  }

  get isPublic() {
    const registry = this.form.value.registry;
    const options = this.imageRegistries;

    if (!registry || !options || !options.length) {
      return false;
    }

    const registryOption = options.find(option => option.name === registry);

    if (!registryOption) {
      return false;
    }

    return registryOption.is_public;
  }

  get notUseCodeRepo() {
    return this.codeRepoSource === 'NOT_USE';
  }

  constructor(
    private imageRegistryService: ImageRegistryService,
    private imageProjectService: ImageProjectService,
    private imageRepositoryService: ImageRepositoryService,
    private message: JenkinsMessageService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
  ) {
    this.subs.push(
      this.onImageRegistryChange$
        .pipe(distinctUntilChanged())
        .subscribe(value => this.onImageRegistryChange(value)),
      this.onImageProjectChange$
        .pipe(distinctUntilChanged())
        .subscribe(value => this.onImageProjectChange(value)),
    );
  }

  ngOnInit() {
    this.init().catch(() => {});
  }

  ngOnDestroy() {
    this.destroyed = true;
    this.fetchImageRegistries.cancel();
    this.fetchImageProjects.cancel();
    this.fetchImageRepositories.cancel();
    this.subs.forEach(sub => sub.unsubscribe());
  }

  onImageRegistryChange(value: string) {
    if (this.form.value && this.form.value.registry !== value) {
      this.setValuePrisine('project', '');
      this.setValuePrisine('repository', '');
    }
    if (value) {
      this.getImageProjects(value).catch(() => {});
    }
  }

  onImageProjectChange(value: string) {
    if (this.form.value && this.form.value.project !== value) {
      this.setValuePrisine('repository', '');
    }
    if (value) {
      this.getImageRepositories(this.form.value.registry, value).catch(
        () => {},
      );
    }
  }

  async init() {
    if (this.form.value.type === 'image_change') {
      this.imageRegistries = [];
      this.imageProjects = [];
      this.imageRepositories = [];
      await Promise.all([
        this.getImageRegistries(),
        this.getImageProjects(this.form.value.registry),
        this.getImageRepositories(
          this.form.value.registry,
          this.form.value.project,
        ),
      ]);
    }
  }

  async getImageRegistries() {
    this.imageRegistries = null;
    this.imageRegistries = await this.fetchImageRegistries.run();
    this.setDefaultValue(
      registry => registry.name,
      'registry',
      this.imageRegistries,
      '',
    );
    this.cdr.markForCheck();
  }

  async getImageProjects(registry: string) {
    if (!registry) {
      return;
    }
    this.imageProjects = null;
    this.imageProjects = await this.fetchImageProjects.run(registry);
    this.setDefaultValue(
      project => project.project_name,
      'project',
      this.imageProjects,
      '@@shared',
    );
    this.cdr.markForCheck();
  }

  async getImageRepositories(registry: string, project: string) {
    if (!registry || !project) {
      return;
    }
    this.imageRepositories = null;
    this.imageRepositories = await this.fetchImageRepositories.run(
      registry,
      project,
    );
    this.setDefaultValue(
      repository => repository.name,
      'repository',
      this.imageRepositories,
      '',
    );
    this.cdr.markForCheck();
  }

  safeSet(fn: any) {
    if (this.destroyed) {
      return;
    }
    fn();
  }

  setValuePrisine(field: string, value: any) {
    if (this.destroyed) {
      return;
    }
    if (this.form.controls[field].value === value) {
      return;
    }
    this.form.controls[field].setValue(value);
    this.form.controls[field].markAsPristine();
  }

  setDefaultValue<TValue>(
    getter: (option: any) => TValue,
    field: string,
    options: any[],
    defaultTo: TValue,
  ) {
    if (this.destroyed) {
      return;
    }
    const control = this.form.controls[field];
    if (!control.value) {
      const first = head(options);
      const value = first && getter(first);

      control.setValue(value || defaultTo);
      control.markAsPristine();
    }
  }

  setSubmited() {
    //TODO: error-tooltip not support reactive parent form submmited.
    Object.keys(this.form.controls).forEach(key =>
      this.form.controls[key].markAsDirty(),
    );
  }

  toJson() {
    const {
      type,
      cron,
      registry,
      project,
      repository,
      image_tag_filter,
      useImageTagFilter,
    } = this.form.value;
    switch (type) {
      case 'cron':
        return {
          type,
          meta: { cron },
        };
      case 'code_change_hook':
        return {
          type,
        };
      case 'image_change':
        const { uuid, display_name, namespace } = this.imageRegistries.find(
          item => item.name === registry,
        );
        const repositoryEntity = this.imageRepositories.find(
          item => item.name === repository,
        );
        return {
          type,
          meta: {
            registry,
            registry_uuid: uuid,
            registry_display_name: display_name,
            repository:
              project === '@@shared' ? repository : `${project}/${repository}`,
            repository_uuid: repositoryEntity.uuid,
            namespace,
            image_tag_filter: useImageTagFilter ? image_tag_filter : '',
          },
        };
    }
  }

  errorMessage(error: any) {
    this.message.error(error);
  }
}
