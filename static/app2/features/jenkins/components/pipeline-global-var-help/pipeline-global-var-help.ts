export const VARIABLE_HELP_CN = `
1. 填写表单时，可通过 $\{变量名}方式实现动态值的引用。  
例：构建过程中，若欲生成的镜像 tag 为 git commit，则可在 tag 的输入框中输入 $\{GIT_COMMIT}，运行过程中会自动将该引用解析为当前的 git 版本号。

2. 变量之间支持拼接。  
例：$\{GIT\\_BRANCH}\\_$\{GIT_COMMIT}

3. 全局变量包括 Jenkins 支持的所有环境变量，但不限于此。  
注意: 修改仓库类型会使可用的全局变量发生变化，所以在修改仓库类型后，请务必核对引用的全局变量是否可用。
`;

export const VARIABLE_HELP_EN = `
1. when you fill in the form, you can achieve dynamic value reference by means of \${variable name}.  
Example: In the construction process, if the desired image tag is git commit, the \${GIT_COMMIT} can be entered in the tag's input box, and the reference will automatically be parsed as the current git version number during the operation.

2. Support splicing between variables.  
Example: \${GIT_BRANCH}_\${GIT_COMMIT}

3. global variables include all environmental variables supported by Jenkins, but not limited to this.  
Note: modifying the warehouse type will change the available global variables, so after modifying the warehouse type, be sure to check whether the referenced global variables are available.
`;
