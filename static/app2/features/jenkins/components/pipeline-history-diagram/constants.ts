export const NODE_RADIUS = 16;
export const END_RADIUS = 8;
export const NODE_DISTANCE_X = 120;
export const NODE_DISTANCE_Y = 60;
export const PADDING = 40;
export const TEXT_OFFSET = -24;
