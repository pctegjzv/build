import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { DiagramConfig } from '../pipeline-history-diagram.component';
import { Link } from '../types';

@Component({
  // tslint:disable-next-line:component-selector
  selector: '[rc-jenkins-pipeline-history-diagram-link]',
  styleUrls: ['link.component.scss'],
  templateUrl: 'link.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineHistoryDiagramLinkComponent {
  @Input()
  link: Link;
  @Input()
  diagramConfig: DiagramConfig;
  get d() {
    if (!this.link) {
      return 'M0,0';
    }
    const { x1, y1, x2, y2, radius } = toShape(this.link, this.diagramConfig);
    if (y1 === y2) {
      return `M${x1} ${y1} L${x2} ${y2}`;
    }
    return radiusCornerLink(x1, y1, x2, y2, radius);
  }
}

function radiusCornerLink(
  x1: number,
  y1: number,
  x2: number,
  y2: number,
  r: number,
) {
  const interpolateX = interpolate(x1, x2);
  const turningX = y2 > y1 ? interpolateX(0.6) : interpolateX(0.4);

  const qx1 = x2 > x1 ? turningX - r : turningX + r;
  const qx2 = x2 > x1 ? turningX + r : turningX - r;

  const qy1 = y2 > y1 ? y1 + r : y1 - r;
  const qy2 = y2 < y1 ? y2 + r : y2 - r;

  return `M${x1} ${y1} L${qx1} ${y1}
          Q${turningX} ${y1}, ${turningX} ${qy1} L${turningX} ${qy2}
          Q${turningX} ${y2}, ${qx2} ${y2} L${x2} ${y2}`;
}

function interpolate(a: number, b: number) {
  return (t: number) => (1 - t) * a + b * t;
}

function toShape([from, to]: Link, config: any) {
  return {
    x1: from.x * config.NODE_DISTANCE_X,
    y1: from.y * config.NODE_DISTANCE_Y,
    x2: to.x * config.NODE_DISTANCE_X,
    y2: to.y * config.NODE_DISTANCE_Y,
    radius: 18,
  };
}
