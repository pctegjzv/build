import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  Component,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChange,
  SimpleChanges,
} from '@angular/core';
import { head, last } from 'lodash';

import { JenkinsService } from 'app2/shared/services/features/jenkins.service';

import { JenkinsMessageService } from '../../services/jenkins-message.service';
import { CancelableTask } from '../../utils/cancelable-task';

@Component({
  selector: 'rc-jenkins-pipeline-history-logs',
  styleUrls: ['pipeline-history-logs.component.scss'],
  templateUrl: 'pipeline-history-logs.component.html',
  animations: [
    trigger('slide', [
      state('active', style({ height: '*' })),
      state('*', style({ height: 0 })),
      transition('active => *', [
        style({ height: '*' }),
        animate(250, style({ height: 0 })),
      ]),
      transition('* => active', [
        style({ height: 0 }),
        animate(250, style({ height: '*' })),
      ]),
    ]),
  ],
})
export class PipelineHistoryLogsComponent implements OnChanges, OnDestroy {
  @Input()
  historyId: string;
  @Input()
  pipelineId: string;
  stages: any[];
  stagesInited = false;
  startId: string;
  stageUserSelected: boolean;
  stepUserSelected: boolean;
  selectedStageId: string;
  waitApprovals: number;
  steps: any[];
  approvaling: boolean;
  logs: {
    [stepId: string]: {
      text: string;
      next_start: number;
      more: boolean;
    };
  };
  showFullLogs: boolean;
  fullLogs: {
    text: string;
    next_start: number;
    more: boolean;
  };

  get selectedStage() {
    if (!this.selectedStageId) {
      return null;
    }
    return this.stages.find(stage => stage.id === this.selectedStageId);
  }

  get waitApprovalsTranslateParams() {
    return {
      waitApprovals: this.waitApprovals,
    };
  }

  private fetchStages = new CancelableTask(params => {
    return this.jenkins.stages.find(params).then(result => {
      const stages = result.stages || [];
      const startId = result.start_stage_id || '';
      const runnings = stages
        .filter(
          (stage: any) =>
            stage.status && !['FINISHED', 'NOT_BUILT'].includes(stage.status),
        )
        .sort((a: any, b: any) => +a.id - +b.id);
      const passed = stages.filter(
        (stage: any) => stage.result && stage.result !== 'NOT_BUILT',
      );

      const selectedStageId =
        (runnings.length && (head(runnings) as any).id) ||
        (passed.length && (last(passed) as any).id) ||
        '';
      const waitApprovals = runnings.reduce(
        (count: number, running: any) =>
          running.status === 'PAUSED' ? count + 1 : count,
        0,
      );
      return {
        stages,
        startId,
        runnings,
        selectedStageId,
        waitApprovals,
      };
    });
  });

  private fetchSteps = new CancelableTask(params => {
    return this.jenkins.steps.find(params).then(response => response.steps);
  });

  private fetchLogs = new CancelableTask((paramsByStep: any[]) => {
    return Promise.all(
      paramsByStep.map(params =>
        this.jenkins.stepLogs.find(params).then(response => ({
          ...response,
          step_id: params.step_id,
        })),
      ),
    );
  });

  private fetchFullLogs = new CancelableTask(params => {
    return this.jenkins.logs.find(params);
  });

  private requestApproval = new CancelableTask(params => {
    return params.abort
      ? this.jenkins.steps.abort(params)
      : this.jenkins.steps.proceed(params);
  });

  private sleep = new CancelableTask(() => {
    return new Promise(resolve => setTimeout(() => resolve(), 5 * 1000));
  });

  constructor(
    private jenkins: JenkinsService,
    private message: JenkinsMessageService,
  ) {}

  private inputChanged(change: SimpleChange) {
    return change && change.currentValue !== change.previousValue;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (
      this.inputChanged(changes.historyId) ||
      this.inputChanged(changes.pipelineId)
    ) {
      this.refresh(true).catch(error => {
        this.message.error(error);
      });
    }
  }

  ngOnDestroy() {
    this.fetchStages.cancel();
    this.fetchSteps.cancel();
    this.fetchLogs.cancel();
    this.fetchFullLogs.cancel();
    this.sleep.cancel();
  }

  async refresh(reset = false) {
    if (reset) {
      this.stages = null;
      this.startId = null;
      this.stageUserSelected = false;
      this.stepUserSelected = false;
      this.selectedStageId = null;
      this.waitApprovals = 0;
      this.steps = null;
      this.approvaling = false;
      this.logs = {};
      this.fullLogs = null;
    }

    const fullLogsFinished = await this.updateFullLogs();
    const stagesFinished = await this.updateStages();
    const stepsFinished = await this.updateSteps();
    const logsFinisehd = await this.updateLogs();

    if (
      !fullLogsFinished ||
      !stagesFinished ||
      !stepsFinished ||
      !logsFinisehd
    ) {
      await this.sleep.run();
      this.refresh().catch(() => {});
    }
  }

  onSelectedStageChange(stageId: string) {
    this.stageUserSelected = true;
    this.selectedStageId = stageId;
    this.steps = null;
    this.logs = {};
    this.refresh().catch(error => this.message.error(error));
  }

  onSelectedStepChange(step: any) {
    step.active = !step.active;
    this.stepUserSelected = true;
  }

  toggleFullLogs() {
    if (this.stages && this.stages.length) {
      this.showFullLogs = !this.showFullLogs;
    }
  }

  stepTracker(_index: number, step: any) {
    return step.id;
  }

  async approval(inputId: string, abort = false, stepId: string) {
    this.approvaling = true;
    try {
      await this.requestApproval.run({
        history_id: this.historyId,
        pipeline_uuid: this.pipelineId,
        stage_id: this.selectedStageId,
        step_id: stepId,
        input_id: inputId,
        abort,
      });
    } catch (error) {
      this.message.error(error);
    }
    this.approvaling = false;
    await this.updateStages();
  }

  async updateStages() {
    if (!this.historyId || !this.pipelineId) {
      return true;
    }

    if (!this.fullLogs.more && this.stages && !this.stages.length) {
      return true;
    }

    if (
      this.stages &&
      this.stages.length &&
      this.stages.every(stage =>
        ['FINISHED', 'NOT_BUILT'].includes(stage.status),
      )
    ) {
      return true;
    }

    const {
      stages,
      startId,
      waitApprovals,
      selectedStageId,
    } = await this.fetchStages.run({
      history_id: this.historyId,
      pipeline_uuid: this.pipelineId,
    });
    this.stages = stages;
    this.startId = startId;
    this.waitApprovals = waitApprovals;

    if (!this.stageUserSelected && this.selectedStageId !== selectedStageId) {
      this.selectedStageId = selectedStageId;
      this.steps = null;
      this.logs = {};
    }

    if (!this.fullLogs.more && !this.stages.length) {
      return true;
    }

    if (!this.stages.length) {
      return false;
    }

    return this.stages.every(stage =>
      ['FINISHED', 'NOT_BUILT'].includes(stage.status),
    );
  }

  async updateFullLogs() {
    if (!this.historyId || !this.pipelineId) {
      return true;
    }

    const existed = this.fullLogs || { text: '', next_start: 0 };

    const logsUpdate = await this.fetchFullLogs.run({
      history_id: this.historyId,
      pipeline_uuid: this.pipelineId,
      start: existed.next_start,
    });

    this.fullLogs = {
      ...existed,
      text: existed.text + logsUpdate.text,
      next_start: logsUpdate.next_start,
      more: logsUpdate.more,
    };

    return !this.fullLogs.more;
  }

  async updateSteps() {
    this.stagesInited = true;

    if (!this.historyId || !this.pipelineId || !this.selectedStageId) {
      return true;
    }

    this.steps = await this.fetchSteps.run({
      history_id: this.historyId,
      pipeline_uuid: this.pipelineId,
      stage_id: this.selectedStageId,
    });

    if (!this.stepUserSelected) {
      this.steps.forEach(step => {
        step.active = step.status === 'PAUSED' && step.id;
      });
    }

    return this.steps.every(step =>
      ['FINISHED', 'NOT_BUILT'].includes(step.status),
    );
  }

  async updateLogs() {
    if (!this.historyId || !this.pipelineId || !this.selectedStageId) {
      return true;
    }

    const paramsByStep = this.steps
      .filter(
        step =>
          step.status !== 'PAUSED' &&
          (!this.logs[step.id] || this.logs[step.id].more),
      )
      .map(step => ({
        history_id: this.historyId,
        pipeline_uuid: this.pipelineId,
        stage_id: this.selectedStageId,
        step_id: step.id,
        start: (this.logs[step.id] && this.logs[step.id].next_start) || 0,
      }));

    const logsUpdates = await this.fetchLogs.run(paramsByStep);

    logsUpdates.forEach(log => {
      const existed = this.logs[log.step_id] || { text: '', next_start: 0 };
      this.logs[log.step_id] = {
        ...existed,
        text: existed.text + log.text,
        next_start: log.next_start,
        more: log.more,
      };
    });

    return Object.keys(this.logs).every(key => !this.logs[key].more);
  }
}
