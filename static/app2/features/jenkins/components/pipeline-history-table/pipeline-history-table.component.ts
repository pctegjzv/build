import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  SimpleChanges,
} from '@angular/core';
import { Router } from '@angular/router';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import { get, pickBy } from 'lodash';

import { badgeHint } from '../../utils/badge-hint';
import { historyStatus } from '../../utils/history-helper';

@Component({
  selector: 'rc-jenkins-pipeline-history-table',
  styleUrls: ['pipeline-history-table.component.scss'],
  templateUrl: 'pipeline-history-table.component.html',
})
export class PipelineHistoryTableComponent implements OnChanges, OnDestroy {
  @Input()
  items: any[];
  @Input()
  page: number;
  @Input()
  pageSize: number;
  @Input()
  total: number;
  @Input()
  hidePipeline = false;
  @Input()
  loading: boolean;
  @Input()
  mutatingItems: { [id: string]: 'starting' | 'deleting' | 'canceling' };
  @Output()
  pageChange = new EventEmitter<number>();
  @Output()
  startHistory = new EventEmitter<any>();
  @Output()
  cancelHistory = new EventEmitter<any>();
  @Output()
  deleteHistory = new EventEmitter<any>();
  @Output()
  approveHistory = new EventEmitter<any>();
  rows: any[] = [];

  constructor(
    private roleUtil: RoleUtilitiesService,
    private modal: ModalService,
    private translate: TranslateService,
    private router: Router,
  ) {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.items && changes.items.currentValue) {
      this.getRowsByItems();
    }
  }

  ngOnDestroy() {}

  hasPermission(item: any, method: string) {
    return this.roleUtil.resourceHasPermission(
      item,
      'jenkins_pipeline_history',
      method,
    );
  }

  getRowsByItems() {
    this.rows = this.items.map(item => ({
      data: item,
      status: historyStatus(item),
      actions: this.actions(item),
    }));
  }

  actions(item: any) {
    const identity = historyIdentity(item.history_id, item.pipeline_uuid);

    const actions = [
      {
        text: 'view',
        action: (item: any) =>
          this.router.navigate([
            '/jenkins/histories',
            item.pipeline_uuid,
            item.history_id,
          ]),
        disabled: false,
        hide: !this.hasPermission(item, 'view'),
      },
      {
        text: 'jenkins_history_restart',
        action: (item: any) => this.start(item),
        disabled:
          !!this.mutatingItems[identity] ||
          item.status !== 'FINISHED' ||
          !get(item, 'actions.replay', {}),
        hide: !this.hasPermission(item, 'replay'),
      },
      {
        text: 'jenkins_history_cancel',
        action: (item: any) => this.cancel(item),
        disabled: !!this.mutatingItems[identity] || item.status === 'FINISHED',
        hide: !this.hasPermission(item, 'cancel'),
      },
      {
        method: 'delete',
        text: 'delete',
        action: (item: any) => this.delete(item),
        disabled: !!this.mutatingItems[identity] || item.status !== 'FINISHED',
        hide: !this.hasPermission(item, 'delete'),
      },
    ];

    const available = actions.filter(action => !action.hide);

    return available;
  }

  start(item: any) {
    this.startHistory.next(item);
  }

  async cancel(item: any) {
    try {
      await this.modal.confirm({
        title: this.translate.get('jenkins_history_cancel'),
        content: this.translate.get('jenkins_history_confirm_cancel', {
          pipeline_name: item.pipeline_display_name || item.pipeline_name,
          history_id: item.history_id,
        }),
      });

      this.cancelHistory.next(item);
    } catch (error) {}
  }

  async delete(item: any) {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('jenkins_history_confirm_delete', {
          pipeline_name: item.pipeline_display_name || item.display_name,
          history_id: item.history_id,
        }),
      });

      this.deleteHistory.next(item);
    } catch (error) {}
  }

  approve(item: any) {
    this.approveHistory.next(item);
  }

  triggerName(data: any) {
    const causes = get(data, 'actions.causes', {});
    return Object.keys(pickBy(causes, x => x))[0];
  }

  badgeHint(data: any) {
    return this.translate.get(badgeHint(data));
  }
}

function historyIdentity(id: string, pipeline_uuid: string) {
  return `${pipeline_uuid}#${id}`;
}
