import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  Component,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChange,
  SimpleChanges,
} from '@angular/core';

import { JenkinsService } from 'app2/shared/services/features/jenkins.service';
import { TranslateService } from 'app2/translate/translate.service';

import { JenkinsMessageService } from '../../services/jenkins-message.service';
import { CancelableTask } from '../../utils/cancelable-task';

const ORDERED_TEST_RESULT = [
  'REGRESSION',
  'FAILED',
  'SKIPPED',
  'FIXED',
  'PASSED',
];

@Component({
  selector: 'rc-jenkins-pipeline-history-test-result',
  styleUrls: ['pipeline-history-test-result.component.scss'],
  templateUrl: 'pipeline-history-test-result.component.html',
  animations: [
    trigger('slide', [
      state('active', style({ height: '*' })),
      state('*', style({ height: 0 })),
      transition('active => *', [
        style({ height: '*' }),
        animate(250, style({ height: 0 })),
      ]),
      transition('* => active', [
        style({ height: 0 }),
        animate(250, style({ height: '*' })),
      ]),
    ]),
  ],
})
export class PipelineHistoryTestResultComponent
  implements OnChanges, OnDestroy {
  @Input()
  historyId: string;
  @Input()
  pipelineId: string;

  testResults: any[];

  private fetchTestResult = new CancelableTask(params => {
    return this.jenkins.testResult
      .get(params)
      .then((res: any) => {
        const result = res.JUnit;
        this.testResults =
          ORDERED_TEST_RESULT.map(item => {
            return {
              status: item,
              results: (result[item] || []).map((itm: any) => {
                itm.active = false;
                return itm;
              }),
            };
          }) || [];
      })
      .catch(error => {
        this.message.error(error);
      });
  });

  private sleep = new CancelableTask(() => {
    return new Promise(resolve => setTimeout(() => resolve(), 5 * 1000));
  });

  constructor(
    private jenkins: JenkinsService,
    private message: JenkinsMessageService,
    private translate: TranslateService,
  ) {}

  private inputChanged(change: SimpleChange) {
    return change && change.currentValue !== change.previousValue;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (
      this.inputChanged(changes.historyId) ||
      this.inputChanged(changes.pipelineId)
    ) {
      this.refresh(true).catch(error => {
        this.message.error(error);
      });
    }
  }

  ngOnDestroy() {
    this.fetchTestResult.cancel();
    this.sleep.cancel();
  }

  async refresh(reset = false) {
    if (reset) {
      this.testResults = [];
    }
    const params = {
      history_id: this.historyId,
      pipeline_uuid: this.pipelineId,
    };
    const testResult = this.fetchTestResult.run(params);

    if (!testResult) {
      await this.sleep.run();
      this.refresh().catch(() => {});
    }
  }

  getDisplayStatusName(status: any) {
    return this.translate.get(`jenkins_test_result_${status.toLowerCase()}`);
  }

  onSelectedTestResultChange(result: any) {
    result.active = !result.active;
  }

  trackByIndex(index: number) {
    return index;
  }
}
