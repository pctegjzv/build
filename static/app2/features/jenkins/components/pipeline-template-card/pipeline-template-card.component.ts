import { Component, Inject, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { templateStagesConvert } from 'app2/features/jenkins/services/pipeline-template.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import { extend as __extend, get as __get } from 'lodash';
import { get } from 'lodash';

import { DefaultConfig, DiagramConfig } from '../pipeline-history-diagram';
import { PipelineTemplateInfoComponent } from '../pipeline-template-info';

const CardDiagramConfig: DiagramConfig = {};
Object.keys(DefaultConfig).forEach((key: string) => {
  CardDiagramConfig[key] = DefaultConfig[key] * 0.5;
});

export enum DisplayType {
  List,
  Dialog,
}

__extend(CardDiagramConfig, { NODE_DISTANCE_X: 80, TEXT_OFFSET: -16 });
@Component({
  selector: 'rc-pipeline-template-card',
  templateUrl: './pipeline-template-card.component.html',
  styleUrls: ['./pipeline-template-card.component.scss'],
})
export class PipelineTemplateCardComponent implements OnInit {
  BRIEF_DESC_NAME = `${this.env.label_base_domain}/readme`;
  VERSION_NAME = `${this.env.label_base_domain}/version`;
  diagramConfig: DiagramConfig = CardDiagramConfig;
  tags: any[] = [];
  language: string;
  @Input()
  canCreatePipeline: boolean;
  @Input()
  template: any;
  @Input()
  type: number;
  actions = {
    main: {
      name: 'jenkins_pipeline_create',
      fun: this.createPipeline.bind(this),
    },
    others: [
      { name: 'jenkins_template_detail_view', fun: this.showHelp.bind(this) },
      {
        name: 'jenkins_template_generator_pipeline_list_view',
        fun: this.showRelatedPipelines.bind(this),
      },
    ],
  };

  constructor(
    private modal: ModalService,
    private translate: TranslateService,
    private router: Router,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {
    this.language = this.translate.currentLang === 'en' ? 'en' : 'zh-CN';
  }

  ngOnInit() {
    this.template = mapperToPageModal(this.template);
    Object.keys(this.template.labels).forEach((key: string) => {
      this.tags.push({ key: key, value: this.template.labels[key] });
    });
    if (!this.canCreatePipeline) {
      this.actions.main = this.actions.others.shift();
    }
  }

  showHelp() {
    if (this.type === DisplayType.Dialog) {
      return;
    }
    this.modal.open(PipelineTemplateInfoComponent, {
      title: this.template.display_name[`${this.language}`],
      width: 820,
      data: {
        template: this.template,
      },
    });
  }
  async createPipeline() {
    const status = get(this.template, 'status.phase', false);
    if (status) {
      try {
        this.modal.closeAll();
        await this.modal
          .confirm({
            title: this.translate.get(
              'jenkins_template_create_pipeline_unstable_title',
            ),
            content: this.translate.get(
              'jenkins_template_create_pipeline_unstable_tips',
            ),
            confirmText: this.translate.get('jenkins_pipeline_create'),
          })
          .then(() => {});
      } catch (err) {
        return;
      }
    }
    this.router.navigate([
      '/jenkins/pipelines/templates/template_create_pipeline',
      this.template.uuid,
    ]);
    this.modal.closeAll();
  }

  showRelatedPipelines() {
    this.router.navigate(['/jenkins/pipelines'], {
      queryParams: {
        search: '',
        uuid: this.template.uuid,
      },
    });
  }

  get briefDes(): string {
    return this.template.annotations[
      `${this.BRIEF_DESC_NAME}.${this.language}`
    ];
  }

  get displayName(): string {
    return __get(this.template, `display_name.${this.language}`, '');
  }

  get version() {
    const an = __get(this.template, `annotations`, {});
    return an[`${this.VERSION_NAME}`];
  }
}

export function mapperToPageModal(template: any) {
  const {
    name,
    uuid,
    source,
    display_name,
    definition: {
      metadata: { annotations, labels },
      spec: { stages },
      status,
    },
  } = template;
  return {
    name,
    uuid,
    display_name,
    status,
    labels,
    source,
    stages: templateStagesConvert(stages),
    arguments: __get(template, 'definition.spec.arguments'),
    annotations,
  };
}
