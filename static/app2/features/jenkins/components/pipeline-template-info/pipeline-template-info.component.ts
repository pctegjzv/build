import { Component, Inject } from '@angular/core';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { Template } from 'app2/features/jenkins/services/pipeline-template.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './pipeline-template-info.component.html',
  styleUrls: ['./pipeline-template-info.component.scss'],
})
export class PipelineTemplateInfoComponent {
  DESCRIPTION_NAME = `${this.env.label_base_domain}/description`;
  BRIEF_DESC_NAME = `${this.env.label_base_domain}/readme`;
  template: Template;
  data: any;
  language: string;
  tags: any[] = [];
  diagramConfig = {};

  constructor(
    @Inject(ENVIRONMENTS) private env: Environments,
    @Inject(MODAL_DATA) public modalData: any,
    private translate: TranslateService,
  ) {
    this.template = modalData.template;
    Object.keys(this.template.labels || {}).forEach((key: string) => {
      this.tags.push({ key: key, value: this.template.labels[key] });
    });
    this.language = this.translate.currentLang === 'en' ? 'en' : 'zh-CN';
  }

  targetLanguageString(target: any) {
    return (target && target[this.language]) || '';
  }

  get description() {
    return this.template.annotations[
      `${this.DESCRIPTION_NAME}.${this.language}`
    ];
  }

  get briefDes() {
    return this.template.annotations[
      `${this.BRIEF_DESC_NAME}.${this.language}`
    ];
  }
}
