import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { get } from 'lodash';
import { Subject, Subscription } from 'rxjs';

@Component({
  templateUrl: './pipeline-template-sync-report.component.html',
  styleUrls: ['./pipeline-template-sync-report.component.scss'],
})
export class PipelineTemplateSyncReportComponent implements OnInit, OnDestroy {
  console = window.console;
  status$: Subject<any>;
  status: any;
  lastJob: any;
  task_counter = {
    success: 0,
    skip: 0,
    fail: 0,
  };
  errors: any[];
  private subscription: Subscription;

  constructor(@Inject(MODAL_DATA) private data: any) {
    this.status$ = this.data.status$;
  }

  ngOnInit() {
    this.subscription = this.data.status$.subscribe((status: any) => {
      this.handleStatus(status);
    });
  }

  private handleStatus(status: any) {
    this.status = status;
    this.lastJob = get(status, 'last_job');
    if (this.lastJob.status === 'SUCCESS') {
      const items: any[] = get(this.lastJob, 'report.items');
      items.sort((_a: any, b: any) => {
        if (b.status === 'FAIL') {
          return 1;
        }
      });
      items.forEach(item => {
        if (item.status === 'SUCCESS') {
          this.task_counter.success++;
        } else if (item.status === 'FAIL') {
          this.task_counter.fail++;
        }
        if (item.action === 'SKIP') {
          this.task_counter.skip++;
        }
      });
    } else {
      this.errors = get(this.lastJob, 'report.errors');
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getItemBriefPath(path: string) {
    const spArr = path.split('/');
    return `.../${spArr[spArr.length - 1]}`;
  }

  timeConsuming() {
    return Math.round(
      (new Date(this.lastJob.updated_at).getTime() -
        new Date(this.lastJob.created_at).getTime()) /
        1000,
    );
  }
}
