import { Component, EventEmitter, Inject, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { JenkinsService } from 'app2/shared/services/features/jenkins.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { extend } from 'lodash';

import { JenkinsMessageService } from '../../services/jenkins-message.service';

@Component({
  templateUrl: './pipeline-template-sync-settings.component.html',
  styleUrls: ['./pipeline-template-sync-settings.component.scss'],
})
export class PipelineTemplateSyncSettingsComponent {
  sourceList = [{ name: 'GIT', value: 'GIT' }, { name: 'SVN', value: 'SVN' }];
  model: any = { type: 'GIT' };
  submitting = false;
  finished = new EventEmitter<any>();
  @ViewChild('createForm')
  form: NgForm;
  constructor(
    @Inject(MODAL_DATA) private data: any,
    private modal: ModalService,
    private jenkins: JenkinsService,
    private message: JenkinsMessageService,
  ) {
    this.model = extend(this.model, this.data.model);
  }

  submit() {
    const data: any = {
      refresh: true,
      source: {
        ...this.model,
      },
    };
    if (this.model.scm === 'SVN') {
      delete data.source.branch;
    }
    this.submitting = true;
    if (this.data.model.uuid) {
      this.jenkins.templates
        .syncSettingsUpdate({
          ...data,
          template_source_uuid: this.data.model.uuid,
        })
        .catch((err: any) => {
          this.message.error(err);
        })
        .then(() => {
          this.final();
        });
    } else {
      this.jenkins.templates
        .syncSettingsCreate(data)
        .catch((err: any) => {
          this.message.error(err);
        })
        .then((res: any) => {
          this.final((res && res.uuid) || '');
        });
    }
  }

  final(id?: string) {
    this.submitting = false;
    this.finished.next(id);
    this.cancel();
  }

  cancel() {
    this.modal.closeAll();
  }
}
