import { Component, Inject } from '@angular/core';
import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';

@Component({
  selector: 'rc-repo-not-bind-hint',
  styleUrls: ['repo-not-bind-hint.component.scss'],
  templateUrl: 'repo-not-bind-hint.component.html',
})
export class RepoNotBindHintComponent {
  get isRootUser() {
    return !this.account.username;
  }

  constructor(@Inject(ACCOUNT) private account: RcAccount) {}
}
