import {
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { PipelineGlobalVarHelpComponent } from 'app2/features/jenkins/components/pipeline-global-var-help';
import {
  PipelineTemplateService,
  templateStagesConvert,
} from 'app2/features/jenkins/services/pipeline-template.service';
import { TooltipDirective } from 'app2/shared/directives/tooltip/tooltip.directive';
import { JenkinsService } from 'app2/shared/services/features/jenkins.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import { get } from 'lodash';
import { Subject, Subscription, combineLatest } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

import { CreateJenkinsCredentialComponent } from '../../components/create-jenkins-credential';
import { JenkinsfileContentExampleComponent } from '../../components/jenkinsfile-content-example';
import { PipelineFormTriggerComponent } from '../../components/pipeline-form-trigger';
import { mapperToPageModal } from '../../components/pipeline-template-card';
import { PipelineTemplateInfoComponent } from '../../components/pipeline-template-info';
import { JenkinsMessageService } from '../../services/jenkins-message.service';
import { PipelineFormService } from '../../services/pipeline-form.service';

enum STEPS {
  basic = 0,
  params = 1,
}

@Component({
  templateUrl: './pipeline-create-by-template.component.html',
  styleUrls: [
    './pipeline-create-by-template.component.scss',
    '../pipeline-create/pipeline-create.component.scss',
  ],
  providers: [PipelineFormService, PipelineTemplateService],
})
export class PipelineCreateByTemplateComponent implements OnInit, OnDestroy {
  @ViewChild('triggerSelect')
  triggerSelect: TooltipDirective;
  @ViewChild('tips')
  tipsElement: ElementRef;

  @ViewChildren('triggerComponent')
  triggers: QueryList<PipelineFormTriggerComponent>;

  onIntegrationChange$ = new Subject<string>();
  onCodeClientChange$ = new Subject<string>();
  onCodeOrgChange$ = new Subject<string>();
  private subs: Subscription[] = [];
  currentStep = STEPS.basic;
  fieldModels: any[] = [];
  relationship: any;
  template: any;
  paramsForm: any;
  stages: any[];
  basic_info_show = false;
  paramErrorStatus = true;
  withoutSCM = true;
  private _codeClients: any;

  get scriptTypes() {
    return this.formService.scriptTypes;
  }

  get spaces() {
    return this.formService.spaces;
  }

  get integrations() {
    return this.formService.integrations;
  }

  get codeClients() {
    if (!this._codeClients || !this._codeClients.length) {
      setTimeout(() => {
        this._codeClients =
          this.formService.codeClients &&
          this.formService.codeClients.slice(
            0,
            this.formService.codeClients.length - 1,
          );
      }, 100);
    } else {
      return this._codeClients;
    }
  }

  get codeOrgs() {
    return this.formService.codeOrgs;
  }

  get credentials() {
    return this.formService.credentials;
  }

  get createCredentialEnabled() {
    return this.formService.createCredentialEnabled;
  }

  get form() {
    return this.formService.form;
  }

  get initialValue() {
    return this.formService.initialValue;
  }

  get initialized() {
    return this.formService.initialized;
  }

  get saving() {
    return this.formService.saving;
  }

  get isSelectedCodeClientNotBind() {
    return this.formService.isSelectedCodeClientNotBind;
  }

  constructor(
    @Inject(ENVIRONMENTS) private env: Environments,
    private route: ActivatedRoute,
    private router: Router,
    private modalService: ModalService,
    private translate: TranslateService,
    private formService: PipelineFormService,
    private message: JenkinsMessageService,
    private jenkinsService: JenkinsService,
    private templateService: PipelineTemplateService,
  ) {
    this.subs.push(
      this.onIntegrationChange$
        .pipe(distinctUntilChanged())
        .subscribe(value => this.formService.onIntegrationChange(value)),
      this.onCodeClientChange$
        .pipe(distinctUntilChanged())
        .subscribe(value => this.formService.onCodeClientChange(value)),
      this.onCodeOrgChange$
        .pipe(distinctUntilChanged())
        .subscribe(value => this.formService.onCodeOrgChange(value)),
    );
  }

  ngOnInit() {
    combineLatest(this.route.paramMap, this.route.queryParamMap).subscribe(
      async ([params, queryParams]) => {
        const clone_uuid = queryParams.get('clone');
        const is_clone = !!clone_uuid;
        this.formService.onInit(clone_uuid, is_clone).catch(error => {
          this.formService.errorMessage(error);
          this.router.navigateByUrl('/jenkins/pipelines');
        });
        let upgrade_uuid;
        let originTemplate: any;
        if (clone_uuid) {
          originTemplate = await this.jenkinsService.pipelines.get(clone_uuid);
          upgrade_uuid = get(originTemplate, 'template.upgrade');
        }
        const id = upgrade_uuid ? upgrade_uuid : params.get('id');
        this.jenkinsService.templates
          .get(id)
          .then((template: any) => {
            this.template = template;
            this.stages = templateStagesConvert(
              get(this.template, 'definition.spec.stages'),
            );
            this.withoutSCM = !get(
              this.template,
              'definition.spec.withSCM',
              false,
            );
            this.templateService.template = template;
            this.fieldModels = this.templateService.fields;
            if (clone_uuid) {
              const arguments_values = get(
                originTemplate,
                'template.arguments_values',
              );
              this.fieldModels.forEach((model: any) => {
                model.value = get(arguments_values, model.name);
              });
            }
            this.relationship = this.templateService.relationship;
          })
          .catch(() => {
            this.router.navigate(['/jenkins/pipelines']);
          });
      },
    );
  }

  get briefDes() {
    const annotations = get(this.template, 'definition.metadata.annotations');
    return (
      annotations && annotations[`${this.env.label_base_domain}/readme.zh-CN`]
    );
  }

  get displayName() {
    return get(this.template, 'display_name.zh-CN');
  }

  showHelp() {
    this.modalService.open(PipelineTemplateInfoComponent, {
      title: this.template.display_name[
        `${this.translate.currentLang === 'en' ? 'en' : 'zh-CN'}`
      ],
      width: 850,
      data: {
        template: mapperToPageModal(this.template),
      },
    });
  }

  ngOnDestroy() {
    this.formService.release();
    this.subs.forEach(sub => sub.unsubscribe());
  }

  triggerIdentify(_index: number, trigger: FormGroup) {
    return trigger.value['@@id'];
  }

  addTrigger(type: 'cron' | 'image_change' | 'code_change_hook') {
    if (this.cronTriggerDisabled() && type === 'cron') {
      return;
    }

    if (this.codeChangeHookTriggerDisabled() && type === 'code_change_hook') {
      return;
    }

    this.formService.addTrigger(type);
    this.triggerSelect.hideTooltip();
  }

  removeTrigger(trigger: FormGroup) {
    this.formService.removeTrigger(trigger);
  }

  addCredential() {
    const jenkins_integration_id = this.form.value.jenkins_integration_id;

    if (!jenkins_integration_id) {
      return;
    }

    const modalRef = this.modalService.open(CreateJenkinsCredentialComponent, {
      title: this.translate.get('create'),
      data: { jenkins_integration_id },
    });

    modalRef.componentInstance.close.subscribe((created: any) => {
      if (created) {
        this.formService
          .getCredentials(jenkins_integration_id, created)
          .catch(error => this.message.error(error));
      }
      modalRef.close();
    });
  }

  codeChangeHookTriggerDisabled() {
    return (
      this.withoutSCM ||
      this.form.value.triggers.filter(
        (trigger: any) => trigger.type === 'code_change_hook',
      ).length > 0
    );
  }

  cronTriggerDisabled() {
    return (
      this.form.value.triggers.filter((trigger: any) => trigger.type === 'cron')
        .length > 0
    );
  }

  toggleSteps() {
    this.triggerSubmit(this.form);
    setTimeout(() => {
      if (!this.currentStep && this.form.status === 'INVALID') {
        return;
      }
      setTimeout(() => {
        this.currentStep = 1 - this.currentStep;
      }, 100);
    }, 100);
  }

  paramsChanged(value: any) {
    this.paramsForm = value;
  }

  onSubmit(isPreview: boolean) {
    this.triggers.forEach(trigger => trigger.setSubmited());
    if (this.saving || this.form.status !== 'VALID' || this.paramErrorStatus) {
      return;
    }
    const data = {};
    this.fieldModels.forEach((field: any) => {
      data[field.name] =
        this.paramsForm[field.name] === 'NOT_USE'
          ? ''
          : this.paramsForm[field.name];
    });

    const triggers = this.triggers.map(trigger => trigger.toJson());
    const template_param = {
      uuid: this.template.uuid,
      arguments_values: data,
    };
    this.formService
      .save(
        isPreview ? this.template.uuid : '',
        triggers,
        template_param,
        this.withoutSCM,
        isPreview,
      )
      .then(result => {
        if (isPreview) {
          this.showJenkinsfile(result);
        } else {
          if (result) {
            this.router.navigate(['/jenkins/pipelines', result]);
          }
        }
      })
      .catch(error => {
        this.message.error(error);
      });
  }

  showVarHelp() {
    const { pipeline_scm } = this.formService.toJson();
    const sourceInfo = {
      pipeline_scm,
      triggers: this.triggers.map(trigger => trigger.toJson()),
    };
    if (this.withoutSCM) {
      delete sourceInfo.pipeline_scm;
    }
    this.modalService.open(PipelineGlobalVarHelpComponent, {
      title: this.translate.get('jenkins_pipeline_use_vars'),
      width: 850,
      data: {
        sourceInfo: sourceInfo,
      },
    });
  }

  cancelCreate() {
    this.modalService
      .confirm({
        title: this.translate.get(
          'jenkins_template_create_pipeline_cancel_title',
        ),
        content: this.translate.get(
          'jenkins_template_create_pipeline_cancel_content',
        ),
      })
      .then(() => {
        this.router.navigate(['/jenkins/templates']);
      })
      .catch(() => {});
  }

  paramStatusChanged(value: boolean) {
    this.paramErrorStatus = value;
  }

  get version() {
    const an = get(this.template, 'definition.metadata.annotations', '');
    return an[`${this.env.label_base_domain}/version`];
  }

  previewJenkinsfile() {
    this.onSubmit(true);
  }

  private showJenkinsfile(file_data: { jenkinsfile: string }) {
    this.modalService.open(JenkinsfileContentExampleComponent, {
      width: 900,
      title: this.translate.get('jenkinsfile_preview'),
      data: {
        content: file_data.jenkinsfile,
      },
    });
  }

  triggerSubmit(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((key: string) => {
      const control = formGroup.controls[key];
      if (control instanceof FormGroup) {
        this.triggerSubmit(control);
      } else if (control instanceof FormControl) {
        control.markAsDirty();
        control.updateValueAndValidity();
      }
    });
  }
}
