import { Component, Input } from '@angular/core';

@Component({
  selector: 'rc-pipeline-create-by-template-steps',
  templateUrl: './pipeline-create-by-template-steps.component.html',
  styleUrls: ['./pipeline-create-by-template-steps.component.scss'],
})
export class PipelineCreateByTemplateStepsComponent {
  @Input()
  currentStep: number;
}
