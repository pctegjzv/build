import { Component } from '@angular/core';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';

import { PipelineTemplateListComponent } from '../pipeline-template-list';
@Component({
  templateUrl: './pipeline-create-mode.component.html',
  styleUrls: ['./pipeline-create-mode.component.scss'],
})
export class PipelineCreateModeComponent {
  constructor(
    private modal: ModalService,
    private translate: TranslateService,
  ) {}

  selected() {
    this.modal.closeAll();
  }

  templateList() {
    this.modal.closeAll();
    this.modal.open(PipelineTemplateListComponent, {
      title: this.translate.get('jenkins_template_create_pipeline'),
      width: 842,
    });
  }
}
