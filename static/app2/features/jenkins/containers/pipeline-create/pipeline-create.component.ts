import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TooltipDirective } from 'app2/shared/directives/tooltip/tooltip.directive';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import { Subject, Subscription } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

import * as actions from '../../actions/pipeline';
import { CreateJenkinsCredentialComponent } from '../../components/create-jenkins-credential';
import { JenkinsfileContentExampleComponent } from '../../components/jenkinsfile-content-example';
import { EXAMPLE } from '../../components/jenkinsfile-content-example/jenkinsfile-content-example';
import { PipelineFormTriggerComponent } from '../../components/pipeline-form-trigger';
import { State } from '../../reducers';
import { JenkinsMessageService } from '../../services/jenkins-message.service';
import { PipelineFormService } from '../../services/pipeline-form.service';

const HEADER_HEIGHT = 76;
const MENU_PADDING_TOP = 20;

@Component({
  styleUrls: ['pipeline-create.component.scss'],
  templateUrl: 'pipeline-create.component.html',
  providers: [PipelineFormService],
})
export class PipelineCreateComponent implements OnInit, OnDestroy {
  @ViewChild('triggerSelect')
  triggerSelect: TooltipDirective;
  @ViewChild('tips')
  tipsElement: ElementRef;

  @ViewChildren('triggerComponent')
  triggers: QueryList<PipelineFormTriggerComponent>;

  onIntegrationChange$ = new Subject<string>();
  onCodeClientChange$ = new Subject<string>();
  onCodeOrgChange$ = new Subject<string>();
  private subs: Subscription[] = [];

  get scriptTypes() {
    return this.formService.scriptTypes;
  }

  get spaces() {
    return this.formService.spaces;
  }

  get integrations() {
    return this.formService.integrations;
  }

  get codeClients() {
    return this.formService.codeClients;
  }

  get codeOrgs() {
    return this.formService.codeOrgs;
  }

  get credentials() {
    return this.formService.credentials;
  }

  get createCredentialEnabled() {
    return this.formService.createCredentialEnabled;
  }

  get form() {
    return this.formService.form;
  }

  get initialValue() {
    return this.formService.initialValue;
  }

  get initialized() {
    return this.formService.initialized;
  }

  get saving() {
    return this.formService.saving;
  }

  get isSelectedCodeClientNotBind() {
    return this.formService.isSelectedCodeClientNotBind;
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private modalService: ModalService,
    private translate: TranslateService,
    private formService: PipelineFormService,
    private store: Store<State>,
    private message: JenkinsMessageService,
  ) {
    this.subs.push(
      this.onIntegrationChange$
        .pipe(distinctUntilChanged())
        .subscribe(value => this.formService.onIntegrationChange(value)),
      this.onCodeClientChange$
        .pipe(distinctUntilChanged())
        .subscribe(value => this.formService.onCodeClientChange(value)),
      this.onCodeOrgChange$
        .pipe(distinctUntilChanged())
        .subscribe(value => this.formService.onCodeOrgChange(value)),
    );
  }

  ngOnInit() {
    this.route.queryParamMap.subscribe(queryParams => {
      const clone_uuid = queryParams.get('clone');
      const is_clone = !!clone_uuid;
      this.formService.onInit(clone_uuid, is_clone).catch(error => {
        this.formService.errorMessage(error);
        this.router.navigateByUrl('/jenkins/pipelines');
      });
    });
  }

  ngOnDestroy() {
    this.formService.release();
    this.subs.forEach(sub => sub.unsubscribe());
  }

  triggerIdentify(_index: number, trigger: FormGroup) {
    return trigger.value['@@id'];
  }

  addTrigger(type: 'cron' | 'image_change' | 'code_change_hook') {
    if (this.cronTriggerDisabled() && type === 'cron') {
      return;
    }

    if (this.codeChangeHookTriggerDisabled() && type === 'code_change_hook') {
      return;
    }

    this.formService.addTrigger(type);
    this.triggerSelect.hideTooltip();
  }

  removeTrigger(trigger: FormGroup) {
    this.formService.removeTrigger(trigger);
  }

  addCredential() {
    const jenkins_integration_id = this.form.value.jenkins_integration_id;

    if (!jenkins_integration_id) {
      return;
    }

    const modalRef = this.modalService.open(CreateJenkinsCredentialComponent, {
      title: this.translate.get('create'),
      data: { jenkins_integration_id },
    });

    modalRef.componentInstance.close.subscribe((created: any) => {
      if (created) {
        this.formService
          .getCredentials(jenkins_integration_id, created)
          .catch(error => this.message.error(error));
      }
      modalRef.close();
    });
  }

  codeChangeHookTriggerDisabled() {
    return (
      this.form.value.source === 'NOT_USE' ||
      this.form.value.triggers.filter(
        (trigger: any) => trigger.type === 'code_change_hook',
      ).length > 0
    );
  }

  cronTriggerDisabled() {
    return (
      this.form.value.triggers.filter((trigger: any) => trigger.type === 'cron')
        .length > 0
    );
  }

  getMenuTop(scrollTop: number) {
    let tipsHeight =
      (this.tipsElement &&
        this.tipsElement.nativeElement &&
        this.tipsElement.nativeElement.offsetHeight) ||
      HEADER_HEIGHT;
    tipsHeight = tipsHeight > 100 ? 86 : tipsHeight;
    return `${Math.max(
      tipsHeight + MENU_PADDING_TOP * 2 - scrollTop,
      MENU_PADDING_TOP,
    )}px`;
  }

  jenkinsfileExample() {
    this.modalService.open(JenkinsfileContentExampleComponent, {
      width: 900,
      title: this.translate.get('jenkinsfile_content_example'),
      data: {
        content: EXAMPLE,
      },
    });
  }

  onSubmit() {
    this.triggers.forEach(trigger => trigger.setSubmited());

    if (this.saving || this.form.status !== 'VALID') {
      return;
    }
    const triggers = this.triggers.map(trigger => trigger.toJson());

    this.formService.save('', triggers).then(result => {
      if (result) {
        this.store.dispatch(new actions.Created(result));
        this.router.navigate(['/jenkins/pipelines', result]);
      }
    });
  }
}
