import {
  Component,
  EventEmitter,
  Inject,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subject, Subscription } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

import { TooltipDirective } from 'app2/shared/directives/tooltip/tooltip.directive';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';

import * as actions from '../../actions/pipeline';
import { CreateJenkinsCredentialComponent } from '../../components/create-jenkins-credential';
import { JenkinsfileContentExampleComponent } from '../../components/jenkinsfile-content-example';
import { EXAMPLE } from '../../components/jenkinsfile-content-example/jenkinsfile-content-example';
import { PipelineFormTriggerComponent } from '../../components/pipeline-form-trigger';
import { State } from '../../reducers';
import { JenkinsMessageService } from '../../services/jenkins-message.service';
import { PipelineFormService } from '../../services/pipeline-form.service';

@Component({
  selector: 'rc-jenkins-pipeline-edit',
  templateUrl: './pipeline-edit.component.html',
  styleUrls: ['./pipeline-edit.component.scss'],
  providers: [PipelineFormService],
})
export class PipelineEditComponent
  implements OnInit, OnDestroy, OnInit, OnDestroy {
  @ViewChild('triggerSelect')
  triggerSelect: TooltipDirective;

  @ViewChildren('triggerComponent')
  triggers: QueryList<PipelineFormTriggerComponent>;

  close = new EventEmitter<void>();
  onIntegrationChange$ = new Subject<string>();
  onCodeClientChange$ = new Subject<string>();
  onCodeOrgChange$ = new Subject<string>();
  subs: Subscription[] = [];

  get scriptTypes() {
    return this.formService.scriptTypes;
  }

  get spaces() {
    return this.formService.spaces;
  }

  get integrations() {
    return this.formService.integrations;
  }

  get codeClients() {
    return this.formService.codeClients;
  }

  get codeOrgs() {
    return this.formService.codeOrgs;
  }

  get credentials() {
    return this.formService.credentials;
  }

  get createCredentialEnabled() {
    return this.formService.createCredentialEnabled;
  }

  get form() {
    return this.formService.form;
  }

  get initialValue() {
    return this.formService.initialValue;
  }

  get initialized() {
    return this.formService.initialized;
  }

  get saving() {
    return this.formService.saving;
  }

  get isSelectedCodeClientNotBind() {
    return this.formService.isSelectedCodeClientNotBind;
  }

  constructor(
    @Inject(MODAL_DATA) public data: any,
    private modalService: ModalService,
    private translate: TranslateService,
    private formService: PipelineFormService,
    private store: Store<State>,
    private message: JenkinsMessageService,
  ) {
    this.subs.push(
      this.onIntegrationChange$
        .pipe(distinctUntilChanged())
        .subscribe(value => this.formService.onIntegrationChange(value)),
      this.onCodeClientChange$
        .pipe(distinctUntilChanged())
        .subscribe(value => this.formService.onCodeClientChange(value)),
      this.onCodeOrgChange$
        .pipe(distinctUntilChanged())
        .subscribe(value => this.formService.onCodeOrgChange(value)),
    );
  }

  ngOnInit() {
    if (!this.data.id) {
      this.close.next();
      return;
    }

    this.formService.onInit(this.data.id).catch(error => {
      this.formService.errorMessage(error);
      this.close.next();
    });
  }

  ngOnDestroy() {
    this.formService.release();
  }

  triggerIdentify(_index: number, trigger: FormGroup) {
    return trigger.value['@@id'];
  }

  addTrigger(type: 'cron' | 'image_change' | 'code_change_hook') {
    if (this.cronTriggerDisabled() && type === 'cron') {
      return;
    }

    if (this.codeChangeHookTriggerDisabled() && type === 'code_change_hook') {
      return;
    }

    this.formService.addTrigger(type);
    this.triggerSelect.hideTooltip();
  }

  removeTrigger(trigger: FormGroup) {
    this.formService.removeTrigger(trigger);
  }

  addCredential() {
    const jenkins_integration_id = this.form.value.jenkins_integration_id;

    if (!jenkins_integration_id) {
      return;
    }

    const modalRef = this.modalService.open(CreateJenkinsCredentialComponent, {
      title: this.translate.get('create'),
      data: { jenkins_integration_id },
    });

    modalRef.componentInstance.close.subscribe((created: any) => {
      if (created) {
        this.formService
          .getCredentials(jenkins_integration_id, created)
          .catch(error => this.message.error(error));
      }
      modalRef.close();
    });
  }

  codeChangeHookTriggerDisabled() {
    return (
      this.form.value.source === 'NOT_USE' ||
      this.form.value.triggers.filter(
        (trigger: any) => trigger.type === 'code_change_hook',
      ).length > 0
    );
  }

  cronTriggerDisabled() {
    return (
      this.form.value.triggers.filter((trigger: any) => trigger.type === 'cron')
        .length > 0
    );
  }

  jenkinsfileExample() {
    this.modalService.open(JenkinsfileContentExampleComponent, {
      width: 900,
      title: this.translate.get('jenkinsfile_content_example'),
      data: {
        content: EXAMPLE,
      },
    });
  }

  onSubmit() {
    if (this.saving || this.form.status !== 'VALID') {
      return;
    }
    const triggers = this.triggers.map(trigger => trigger.toJson());

    if (this.data.id) {
      this.formService.save(this.data.id, triggers).then(result => {
        if (result) {
          this.store.dispatch(new actions.Updated(this.data.id));
          this.close.emit();
        }
      });
    }
  }
}
