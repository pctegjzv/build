import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute /*Router*/, Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { shallowEqual } from 'app2/state-container/core/utils/shallow-equal';
import { TranslateService } from 'app2/translate/translate.service';
import { get, pickBy } from 'lodash';
import { Subject, interval } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  shareReplay,
  switchMap,
  takeUntil,
  takeWhile,
  tap,
  withLatestFrom,
} from 'rxjs/operators';

import * as actions from '../../actions/history';
import {
  State,
  selectHistoryDetailData,
  selectHistoryDetailId,
  selectHistoryDetailLoading,
  selectHistoryDetailMutating,
} from '../../reducers';
import { badgeHint } from '../../utils/badge-hint';
import { ComponentEffect } from '../../utils/component-effect';
import { historyStatus } from '../../utils/history-helper';

const AUTO_REFRESH_DELAY = 5;

@Component({
  templateUrl: './pipeline-history-detail.component.html',
  styleUrls: ['./pipeline-history-detail.component.scss'],
})
export class PipelineHistoryDetailComponent implements OnDestroy {
  id$ = this.store.select(selectHistoryDetailId);
  data$ = this.store.select(selectHistoryDetailData);
  loading$ = this.store.select(selectHistoryDetailLoading);
  mutating$ = this.store.select(selectHistoryDetailMutating);
  status$ = this.data$.pipe(
    map(data => historyStatus(data || {})),
    shareReplay(1),
  );
  private onDestroy$ = new Subject<void>();

  @ComponentEffect()
  identityChange$ = this.route.paramMap.pipe(
    takeUntil(this.onDestroy$),
    map(paramMap => ({
      id: paramMap.get('id') || '',
      pipeline_uuid: paramMap.get('pipeline_uuid') || '',
    })),
    distinctUntilChanged(shallowEqual),
    tap(identity =>
      this.store.dispatch(new actions.Get(identity.id, identity.pipeline_uuid)),
    ),
  );

  @ComponentEffect()
  refresh$ = this.data$.pipe(
    filter(data => !!data && data.status !== 'FINISHED'),
    switchMap(() =>
      interval(1000).pipe(
        takeWhile(value => value <= AUTO_REFRESH_DELAY),
        map(value => AUTO_REFRESH_DELAY - value),
      ),
    ),
    takeUntil(this.onDestroy$),
    filter(value => value === 0),
    withLatestFrom(this.id$),
    tap(([, identity]) =>
      this.store.dispatch(new actions.Get(identity.id, identity.pipeline_uuid)),
    ),
  );

  @ComponentEffect()
  started$ = this.actions$.ofType(actions.types.Started).pipe(
    takeUntil(this.onDestroy$),
    map(action => action as actions.Started),
    withLatestFrom(this.id$),
    filter(
      ([action, identity]) =>
        action.from.id === identity.id &&
        action.from.pipeline_uuid === identity.pipeline_uuid,
    ),
    map(([action]) => action),
    tap(action =>
      this.router.navigate([
        '/jenkins/histories',
        action.pipeline_uuid,
        action.id,
      ]),
    ),
  );

  @ComponentEffect()
  deleted$ = this.actions$.ofType(actions.types.Deleted).pipe(
    takeUntil(this.onDestroy$),
    map(action => action as actions.Deleted),
    withLatestFrom(this.id$),
    filter(
      ([action, identity]) =>
        action.id === identity.id &&
        action.pipeline_uuid === identity.pipeline_uuid,
    ),
    tap(() => this.router.navigate(['/jenkins/histories'])),
  );

  @ComponentEffect()
  cancel$ = this.actions$.ofType(actions.types.Canceled).pipe(
    takeUntil(this.onDestroy$),
    map(action => action as actions.Canceled),
    withLatestFrom(this.id$),
    filter(
      ([action, identity]) =>
        action.id === identity.id &&
        action.pipeline_uuid === identity.pipeline_uuid,
    ),
    map(([action]) => action),
    tap((action: actions.Canceled) =>
      this.store.dispatch(new actions.Get(action.id, action.pipeline_uuid)),
    ),
  );

  @ComponentEffect()
  getError$ = this.actions$.ofType(actions.types.GetError).pipe(
    takeUntil(this.onDestroy$),
    map(action => action as actions.GetError),
    withLatestFrom(this.id$),
    filter(
      ([action, identity]) =>
        action.id === identity.id &&
        action.pipeline_uuid === identity.pipeline_uuid,
    ),
    tap(() => this.router.navigate(['/jenkins/histories'])),
  );

  constructor(
    private store: Store<State>,
    private route: ActivatedRoute,
    private roleUtil: RoleUtilitiesService,
    private modal: ModalService,
    private translate: TranslateService,
    private actions$: Actions,
    private router: Router,
  ) {}

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  hasPermission(item: any, action: string) {
    return this.roleUtil.resourceHasPermission(
      item,
      'jenkins_pipeline_history',
      action,
    );
  }

  canRestart(item: any) {
    return this.hasPermission(item, 'replay');
  }

  canCancel(item: any) {
    return this.hasPermission(item, 'cancel');
  }

  canDelete(item: any) {
    return this.hasPermission(item, 'delete');
  }

  canApprove(item: any) {
    return this.hasPermission(item, 'approve');
  }

  start(item: any) {
    this.store.dispatch(new actions.Start(item.history_id, item.pipeline_uuid));
  }

  async cancel(item: any) {
    try {
      await this.modal.confirm({
        title: this.translate.get('jenkins_history_cancel'),
        content: this.translate.get('jenkins_history_confirm_cancel', {
          pipeline_name: item.pipeline_display_name || item.pipeline_name,
          history_id: item.history_id,
        }),
      });

      this.store.dispatch(
        new actions.Cancel(item.history_id, item.pipeline_uuid),
      );
    } catch (error) {}
  }

  triggerName(data: any) {
    const causes = get(data, 'actions.causes', {});
    return Object.keys(pickBy(causes, x => x))[0];
  }

  async delete(item: any) {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('jenkins_history_confirm_delete', {
          pipeline_name: item.pipeline_display_name || item.display_name,
          history_id: item.history_id,
        }),
      });

      this.store.dispatch(
        new actions.Delete(item.history_id, item.pipeline_uuid),
      );
    } catch (error) {}
  }

  badgeHint(data: any) {
    return this.translate.get(badgeHint(data));
  }
}
