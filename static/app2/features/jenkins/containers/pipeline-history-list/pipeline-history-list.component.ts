import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { head, toInteger } from 'lodash';
import { Observable, Subject, interval } from 'rxjs';

import {
  filter,
  map,
  shareReplay,
  switchMap,
  takeUntil,
  takeWhile,
  tap,
} from 'rxjs/operators';

import { JenkinsMessageService } from 'app2/features/jenkins/services/jenkins-message.service';
import { IntegrationService } from 'app2/shared/services/features/integration.service';

import * as actions from '../../actions/history';
import {
  State,
  selectHistoryListItems,
  selectHistoryListLoading,
  selectHistoryListMutatingItems,
  selectHistoryListPage,
  selectHistoryListParams,
  selectHistoryListTotal,
} from '../../reducers';
import { CancelableTask } from '../../utils/cancelable-task';
import { ComponentEffect } from '../../utils/component-effect';

const AUTO_REFRESH_DELAY = 10;
const PAGE_SIZE = 10;

@Component({
  templateUrl: './pipeline-history-list.component.html',
  styleUrls: ['./pipeline-history-list.component.scss'],
})
export class PipelineHistoryListComponent implements OnInit, OnDestroy {
  params$ = this.store.select(selectHistoryListParams);
  loading$ = this.store.select(selectHistoryListLoading);
  total$ = this.store.select(selectHistoryListTotal);
  items$ = this.store.select(selectHistoryListItems);
  page$ = this.store.select(selectHistoryListPage);
  mutatingItems$ = this.store.select(selectHistoryListMutatingItems);
  autoRefreshIn$: Observable<string>;
  inputValue: string;
  jenkinsIntegrationId: string;
  integrations: any[];
  pageSize = PAGE_SIZE;

  private fetchIntegrations = new CancelableTask(() => {
    return this.integrationService
      .getIntegrations({
        types: 'Jenkins',
        page: 1,
        page_size: 200,
      })
      .then(results => results.filter(item => item.enabled));
  });
  private onDestroy$ = new Subject<void>();

  @ComponentEffect()
  paramsChange$ = this.route.queryParamMap.pipe(
    takeUntil(this.onDestroy$),
    map(queryParamMap => getParams(queryParamMap)),
    filter(params => !!params.jenkins_integration_id),
    tap(params => {
      this.inputValue = params.search;
      this.jenkinsIntegrationId = params.jenkins_integration_id;
      this.store.dispatch(new actions.Search(params));
    }),
  );

  @ComponentEffect()
  started$ = this.actions$.ofType(actions.types.Started).pipe(
    takeUntil(this.onDestroy$),
    tap(() => this.refresh()),
    tap((action: actions.Started) => {
      this.router.navigate([
        '/jenkins/histories',
        action.pipeline_uuid,
        action.id,
      ]);
    }),
  );

  @ComponentEffect()
  deleted$ = this.actions$.ofType(actions.types.Deleted).pipe(
    takeUntil(this.onDestroy$),
    tap(() => this.refresh()),
  );

  @ComponentEffect()
  canceled$ = this.actions$.ofType(actions.types.Canceled).pipe(
    takeUntil(this.onDestroy$),
    tap(() => this.refresh()),
  );

  constructor(
    private store: Store<State>,
    private route: ActivatedRoute,
    private router: Router,
    private integrationService: IntegrationService,
    private message: JenkinsMessageService,
    private actions$: Actions,
  ) {
    this.autoRefreshIn$ = this.autoRefreshIn();
  }

  autoRefreshIn() {
    return this.items$.pipe(
      switchMap(() =>
        interval(1000).pipe(
          takeWhile(value => value <= AUTO_REFRESH_DELAY),
          map(value => AUTO_REFRESH_DELAY - value),
        ),
      ),
      takeUntil(this.onDestroy$),
      tap(value => {
        if (value === 0) {
          this.refresh(true);
        }
      }),
      map(value => value.toString()),
      shareReplay(1),
    );
  }

  ngOnInit() {
    this.initIntegrations().catch(error => this.message.error(error));
  }

  async initIntegrations() {
    this.integrations = await this.fetchIntegrations.run();
    const jenkins_integration_id =
      this.route.snapshot.queryParamMap.get('jenkins_integration_id') || '';

    const matched = this.integrations.find(
      item => item.id === jenkins_integration_id,
    );

    if ((!jenkins_integration_id || !matched) && this.integrations.length) {
      this.onJenkinsIntegrationIdChange(head(this.integrations).id);
    }
  }

  ngOnDestroy() {
    this.fetchIntegrations.cancel();
    this.onDestroy$.next();
  }

  onJenkinsIntegrationIdChange(jenkins_integration_id: string) {
    const existedParams = getParams(this.route.snapshot.queryParamMap);
    this.router.navigate(['/jenkins/histories'], {
      queryParams: {
        ...existedParams,
        jenkins_integration_id,
      },
    });
  }

  search() {
    const existedParams = getParams(this.route.snapshot.queryParamMap);
    this.router.navigate(['/jenkins/histories'], {
      queryParams: {
        ...existedParams,
        search: (this.inputValue || '').trim(),
        page: 1,
      },
    });
  }

  onPageChange(page: number) {
    const existedParams = getParams(this.route.snapshot.queryParamMap);
    this.router.navigate(['/jenkins/histories'], {
      queryParams: {
        ...existedParams,
        page,
      },
    });
  }

  refresh(ignoreError = false) {
    const existedParams = getParams(this.route.snapshot.queryParamMap);
    if (existedParams.jenkins_integration_id) {
      this.store.dispatch(new actions.Search(existedParams, ignoreError));
    }
  }

  startHistory(item: any) {
    this.store.dispatch(new actions.Start(item.history_id, item.pipeline_uuid));
  }

  cancelHistory(item: any) {
    this.store.dispatch(
      new actions.Cancel(item.history_id, item.pipeline_uuid),
    );
  }

  deleteHistory(item: any) {
    this.store.dispatch(
      new actions.Delete(item.history_id, item.pipeline_uuid),
    );
  }
}

function getParams(queryParamMap: ParamMap) {
  return {
    search: queryParamMap.get('search') || '',
    page: toInteger(queryParamMap.get('page')) || 1,
    jenkins_integration_id: queryParamMap.get('jenkins_integration_id') || '',
    page_size: PAGE_SIZE,
  };
}
