import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { find, get, isEmpty } from 'lodash';
import { extend, toInteger } from 'lodash';
import { Observable, Subject, interval } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  shareReplay,
  switchMap,
  takeUntil,
  takeWhile,
  tap,
  withLatestFrom,
} from 'rxjs/operators';

import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';
import { PipelineCreateModeComponent } from 'app2/features/jenkins/containers/pipeline-create-mode';
import { PipelineEditComponent } from 'app2/features/jenkins/containers/pipeline-edit/pipeline-edit.component';
import { PipelineUpdateByTemplateComponent } from 'app2/features/jenkins/containers/pipeline-update-by-template';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ModalMode, ModalRef } from 'app2/shared/services/modal/modal.types';
import { shallowEqual } from 'app2/state-container/core/utils/shallow-equal';
import { TranslateService } from 'app2/translate/translate.service';

import * as historyActions from '../../actions/history';
import * as actions from '../../actions/pipeline';
import {
  State,
  selectPipelineListItems,
  selectPipelineListLoading,
  selectPipelineListMutatingItems,
  selectPipelineListParams,
  selectPipelineListTotal,
} from '../../reducers';
import { ComponentEffect } from '../../utils/component-effect';
import { historyStatus } from '../../utils/history-helper';

const PAGE_SIZE = 10;
const AUTO_REFRESH_DELAY = 60;

@Component({
  templateUrl: './pipeline-list.component.html',
  styleUrls: ['./pipeline-list.component.scss'],
})
export class PipelineListComponent implements OnInit, OnDestroy {
  editModalRef:
    | ModalRef<PipelineEditComponent>
    | ModalRef<PipelineUpdateByTemplateComponent>;
  creatable = false;
  params$ = this.store.select(selectPipelineListParams);
  loading$ = this.store.select(selectPipelineListLoading);
  total$ = this.store.select(selectPipelineListTotal);
  mutatingItems$ = this.store.select(selectPipelineListMutatingItems);
  items$ = this.store.select(selectPipelineListItems).pipe(
    distinctUntilChanged(),
    withLatestFrom(this.mutatingItems$),
    map(([items, mutatingItems]) => this.addActions(items, mutatingItems)),
    map((items: any[]) => this.triggerPolymer(items)),
    shareReplay(1),
  );
  autoRefreshIn$: Observable<string>;
  inputValue: string;
  toggledId: string;
  private onDestroy$ = new Subject<void>();

  @ComponentEffect()
  paramsChange$ = this.route.queryParamMap.pipe(
    takeUntil(this.onDestroy$),
    map(queryParamMap =>
      extend(
        {
          search: queryParamMap.get('search') || '',
          page: toInteger(queryParamMap.get('page')) || 1,
        },
        {
          template_uuids: queryParamMap.get('uuid')
            ? queryParamMap.get('uuid')
            : undefined,
        },
      ),
    ),
    distinctUntilChanged(shallowEqual),
    tap(params => {
      this.inputValue = params.search;
      this.store.dispatch(
        new actions.Search({
          ...params,
          page_size: PAGE_SIZE,
        }),
      );
    }),
  );

  @ComponentEffect()
  itemEffects$ = this.actions$
    .ofType(actions.types.Deleted, actions.types.Created, actions.types.Updated)
    .pipe(
      takeUntil(this.onDestroy$),
      tap(() => this.refresh(true)),
    );

  @ComponentEffect()
  started$ = this.actions$.ofType(historyActions.types.Started).pipe(
    takeUntil(this.onDestroy$),
    tap(() => this.refresh()),
    tap((action: historyActions.Started) => {
      this.router.navigate([
        '/jenkins/histories',
        action.pipeline_uuid,
        action.id,
      ]);
    }),
  );

  constructor(
    private store: Store<State>,
    private route: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private modalService: ModalService,
    private roleUtil: RoleUtilitiesService,
    private actions$: Actions,
    @Inject(WEBLABS) public weblabs: Weblabs,
  ) {
    this.autoRefreshIn$ = this.autoRefreshIn();
  }

  async ngOnInit() {
    this.creatable = await this.roleUtil.resourceTypeSupportPermissions(
      'jenkins_pipeline',
      null,
      'create',
    );
    this.store.dispatch(new actions.Renew()); // TODO: clone action depends on create permission, force render for create permission take effect.
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    if (this.editModalRef) {
      this.editModalRef.close();
    }
  }

  autoRefreshIn() {
    return this.items$.pipe(
      switchMap(() =>
        interval(1000).pipe(
          takeWhile(value => value <= AUTO_REFRESH_DELAY),
          map(value => AUTO_REFRESH_DELAY - value),
        ),
      ),
      takeUntil(this.onDestroy$),
      tap(value => {
        if (value === 0) {
          this.refresh(true);
        }
      }),
      map(value => value.toString()),
      shareReplay(1),
    );
  }

  refresh(ignoreError = false) {
    this.store.dispatch(
      new actions.Search(
        {
          search: this.inputValue,
          page: toInteger(this.route.snapshot.queryParamMap.get('page')) || 1,
          page_size: PAGE_SIZE,
        },
        ignoreError,
      ),
    );
  }

  pageChange(page: number) {
    this.router.navigate(['/jenkins/pipelines'], {
      queryParams: {
        search: this.inputValue,
        page,
      },
    });
  }

  search(keyword: string) {
    this.inputValue = keyword;
    this.router.navigate(['/jenkins/pipelines/'], {
      queryParams: {
        search: (this.inputValue || '').trim(),
        page: 1,
      },
    });
  }

  create() {
    this.modalService.open(PipelineCreateModeComponent, {
      title: this.translate.get(`please_select_create_mode`),
    });
  }

  edit(item: any, upgrade = false) {
    const title = upgrade
      ? `${this.translate.get('upgrde')} ${item.name}`
      : `${this.translate.get('update')} ${item.name}`;

    const upgradeTo = upgrade ? get(item, 'template.upgrade.uuid') : '';
    console.log(upgradeTo);
    this.editModalRef = item.template
      ? this.modalService.open(PipelineUpdateByTemplateComponent, {
          title: title,
          mode: ModalMode.RIGHT_SLIDER,
          width: 900,
          data: {
            id: item.uuid,
            template: item,
            upgradeTo,
          },
        })
      : this.modalService.open(PipelineEditComponent, {
          title: this.translate.get('update'),
          mode: ModalMode.RIGHT_SLIDER,
          width: 900,
          data: {
            id: item.uuid,
          },
        });

    this.editModalRef.componentInstance.close.subscribe(() => {
      this.editModalRef.close();
    });

    this.editModalRef.afterClosed.subscribe(() => {
      this.editModalRef = null;
    });
  }

  async delete(item: any) {
    try {
      await this.modalService.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('jenkins_pipeline_confirm_delete', {
          pipeline_name: item.display_name || item.name,
        }),
      });

      this.store.dispatch(new actions.Delete(item.uuid));
    } catch (error) {}
  }

  start(id: string) {
    this.store.dispatch(new actions.Start(id));
  }

  historyStatus(history: any) {
    return historyStatus(history);
  }

  addActions(items: any[], mutatingItems: any) {
    return items.map(item => ({
      item,
      actions: this.actions(item, mutatingItems[item.uuid]),
    }));
  }

  actions(item: any, mutating: any) {
    const actions = [
      {
        text: 'jenkins_pipeline_start',
        action: (item: any) => this.start(item.uuid),
        disabled: !!mutating,
        hide: !this.hasPermission(item, 'trigger'),
      },
      {
        text: 'view',
        action: (item: any) =>
          this.router.navigate(['/jenkins/pipelines', item.uuid]),
        disabled: false,
        hide: !this.hasPermission(item, 'view'),
      },
      {
        text: 'jenkins_pipeline_clone',
        action: (item: any) => {
          const target = item.template
            ? [
                '/jenkins/pipelines/templates/template_create_pipeline',
                item.template.uuid,
              ]
            : ['/jenkins/pipelines/create'];
          this.router.navigate(target, {
            queryParams: { clone: item.uuid },
          });
        },
        disabled: !!mutating,
        hide: !this.creatable,
      },
      {
        text: 'update',
        action: (item: any) => this.edit(item),
        disabled: !!mutating,
        hide: !this.hasPermission(item, 'update'),
      },
      {
        text: 'jenkins_pipeline_upgrade',
        action: (item: any) => this.edit(item, true),
        disabled: !!mutating,
        hide:
          !this.hasPermission(item, 'update') || !get(item, 'template.upgrade'),
      },
      {
        text: 'delete',
        action: (item: any) => this.delete(item),
        disabled: !!mutating,
        hide: !this.hasPermission(item, 'delete'),
      },
    ];

    return actions.filter(action => !action.hide);
  }

  hasPermission(item: any, action: string) {
    return this.roleUtil.resourceHasPermission(
      item,
      'jenkins_pipeline',
      action,
    );
  }

  getTemplateName(target: any) {
    if (!target) {
      return '';
    }
    const lang = this.translate.currentLang === 'en' ? 'en' : 'zh-CN';
    return target[lang];
  }

  triggerPolymer(items: any[]) {
    items.map(({ item }) => {
      const trg = item.triggers;
      if (!trg) {
        item['triggers_polymer'] = [];
        return;
      }
      const ply = [
        { type: 'cron', values: find(trg, { type: 'cron' }) },
        {
          type: 'image_change',
          values: trg.filter((t: any) => t.type === 'image_change'),
        },
        {
          type: 'code_change_hook',
          values: find(trg, { type: 'code_change_hook' }),
        },
      ];
      item['triggers_polymer'] = ply.filter(t => !isEmpty(t.values));
    });
    return items;
  }
}
