import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Template } from 'app2/features/jenkins/services/pipeline-template.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { TranslateService } from 'app2/translate/translate.service';
import { Subject, Subscription } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

import * as actions from '../../actions/template';
import * as sourceActions from '../../actions/template_sources';
import { DisplayType } from '../../components/pipeline-template-card';
import {
  State,
  selectTemplateListItems,
  selectTemplateSourcesListItems,
} from '../../reducers';

@Component({
  selector: 'rc-pipeline-template-list',
  templateUrl: './pipeline-template-list.component.html',
  styleUrls: ['./pipeline-template-list.component.scss'],
})
export class PipelineTemplateListComponent implements OnInit, OnDestroy {
  templates: Template[] = [];
  templateSub: Subscription;
  sourcesSub: Subscription;
  sources: any[] = [];
  private _type = 'all';
  private keyword = '';
  items$ = this.store
    .select(selectTemplateListItems)
    .pipe(distinctUntilChanged());
  sources$ = this.store
    .select(selectTemplateSourcesListItems)
    .pipe(distinctUntilChanged());
  private onDestroy$: Subject<void> = new Subject<void>();
  canCreatePipeline = false;

  templateTypeList = [
    { name: this.translate.get('jenkins_template_type_all'), value: 'all' },
    {
      name: this.translate.get('jenkins_template_type_system'),
      value: 'system',
    },
    {
      name: this.translate.get('jenkins_template_type_custom'),
      value: 'custom',
    },
  ];
  loading = false;
  @Input()
  listType = DisplayType.Dialog;

  constructor(
    private store: Store<State>,
    private translate: TranslateService,
    private roleUtil: RoleUtilitiesService,
  ) {}

  get type() {
    return this._type;
  }

  set type(value: string) {
    this._type = value;
    this.setQuery();
  }

  setQuery() {
    this.loading = true;
    this.store.dispatch(
      new actions.Find(
        this.keyword,
        this.type,
        this.type === 'all' && this.sources
          ? this.sources.map((s: any) => s.uuid)
          : this.type === 'custom'
            ? this.sources.filter(s => !s.is_public).map((s: any) => s.uuid)
            : [],
        this.type === 'all' || this.type === 'system',
      ),
    );
  }

  async ngOnInit() {
    this.store.dispatch(new sourceActions.Find());
    this.templateSub = this.items$.subscribe((templates: any) => {
      this.loading = false;
      this.templates = templates;
    });
    this.sourcesSub = this.sources$.subscribe((sources: any[]) => {
      this.sources = sources;
      if (this.type === 'all' && sources) {
        const ids = sources.map(source => source.uuid);
        this.loading = true;
        this.store.dispatch(
          new actions.Find(this.keyword, this.type, ids, true),
        );
      }
    });
    const pipelinePermissions = await this.roleUtil.resourceTypesPermissions([
      'jenkins_pipeline',
    ]);
    this.canCreatePipeline = pipelinePermissions['jenkins_pipeline'].includes(
      'jenkins_pipeline:create',
    );
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.templateSub.unsubscribe();
  }

  search(searchKey: string) {
    this.keyword = searchKey;
    this.setQuery();
  }
}
