import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { JenkinsService } from 'app2/shared/services/features/jenkins.service';
import { get } from 'lodash';
import { Observable, from as fromPromise, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import * as actions from '../actions/history';
import * as pipelineActions from '../actions/pipeline';
import { JenkinsMessageService } from '../services/jenkins-message.service';

@Injectable()
export class HistoryEffects {
  @Effect()
  search$: Observable<Action> = this.actions$.ofType(actions.types.Search).pipe(
    switchMap((action: actions.Search) =>
      fromPromise(
        this.jenkins.histories.find({ ...action.params, pipeline_uuid: null }),
      )
        .pipe(
          map(response => {
            return new actions.SearchComplete(action.params, {
              total: response.count,
              items: response.results,
            });
          }),
        )
        .pipe(
          catchError(error => {
            if (!action.ignoreError) {
              this.message.error(error);
            }
            return of(new actions.SearchError(action.params, error));
          }),
        ),
    ),
  );

  @Effect()
  get$: Observable<Action> = this.actions$.ofType(actions.types.Get).pipe(
    switchMap((action: actions.Get) =>
      fromPromise(
        this.jenkins.histories.get(action.id, {
          pipeline_uuid: action.pipeline_uuid,
        }),
      )
        .pipe(
          map(
            response =>
              new actions.GetComplete(
                action.id,
                action.pipeline_uuid,
                response,
              ),
          ),
        )
        .pipe(
          catchError(error => {
            this.message.error(error);
            return of(
              new actions.GetError(action.id, action.pipeline_uuid, error),
            );
          }),
        ),
    ),
  );

  @Effect()
  startFromPipeline$: Observable<Action> = this.actions$
    .ofType(pipelineActions.types.Start)
    .pipe(
      switchMap((action: pipelineActions.Start) =>
        fromPromise(this.jenkins.histories.start(action.id, ''))
          .pipe(
            map(
              (response: any) =>
                new actions.Started(response.uuid, response.pipeline_uuid, {
                  pipeline_uuid: action.id,
                }),
            ),
          )
          .pipe(
            catchError(error => {
              const code = get(error, 'errors[0].code');
              if (
                code === 'JenkinsIntegrationInActive' ||
                code === 'JenkinsServiceUnavailable'
              ) {
                this.message.warning(get(error, 'errors[0]'));
              } else {
                this.message.error(error);
              }
              return of(new pipelineActions.StartError(action.id, error));
            }),
          ),
      ),
    );

  @Effect()
  start$: Observable<Action> = this.actions$.ofType(actions.types.Start).pipe(
    switchMap((action: actions.Start) =>
      fromPromise(this.jenkins.histories.start(action.pipeline_uuid, action.id))
        .pipe(
          map(
            (response: any) =>
              new actions.Started(response.uuid, response.pipeline_uuid, {
                pipeline_uuid: action.pipeline_uuid,
                id: action.id,
              }),
          ),
        )
        .pipe(
          catchError(error => {
            this.message.error(error);
            return of(
              new actions.StartError(action.id, action.pipeline_uuid, error),
            );
          }),
        ),
    ),
  );

  @Effect()
  cancel$: Observable<Action> = this.actions$.ofType(actions.types.Cancel).pipe(
    switchMap((action: actions.Cancel) =>
      fromPromise(
        this.jenkins.histories.cancel(action.pipeline_uuid, action.id),
      )
        .pipe(map(() => new actions.Canceled(action.id, action.pipeline_uuid)))
        .pipe(
          catchError(error => {
            this.message.error(error);
            return of(
              new actions.CancelError(action.id, action.pipeline_uuid, error),
            );
          }),
        ),
    ),
  );

  @Effect()
  delete$: Observable<Action> = this.actions$.ofType(actions.types.Delete).pipe(
    switchMap((action: actions.Delete) =>
      fromPromise(
        this.jenkins.histories.delete(action.pipeline_uuid, action.id),
      )
        .pipe(map(() => new actions.Deleted(action.id, action.pipeline_uuid)))
        .pipe(
          catchError(error => {
            this.message.error(error);
            return of(
              new actions.DeleteError(action.id, action.pipeline_uuid, error),
            );
          }),
        ),
    ),
  );

  constructor(
    private jenkins: JenkinsService,
    private message: JenkinsMessageService,
    private actions$: Actions,
  ) {}
}
