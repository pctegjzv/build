import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { JenkinsService } from 'app2/shared/services/features/jenkins.service';
import { Observable, from as fromPromise } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import * as actions from '../actions/template';

@Injectable()
export class TemplateEffects {
  @Effect()
  find$: Observable<Action> = this.actions$.ofType(actions.types.Find).pipe(
    switchMap((action: actions.Find) =>
      fromPromise(
        this.jenkins.templates.find({
          search: action.search,
          official: action.official,
          source_uuids: action.source_uuids.join(','),
        }),
      ).pipe(
        map((result: any[]) => {
          return new actions.FindComplete(result);
        }),
      ),
    ),
  );

  constructor(private actions$: Actions, private jenkins: JenkinsService) {}
}
