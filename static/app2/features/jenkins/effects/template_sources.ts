import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { JenkinsService } from 'app2/shared/services/features/jenkins.service';
import { Observable, from as fromPromise } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import * as actions from '../actions/template_sources';

@Injectable()
export class TemplateSourcesEffects {
  @Effect()
  find$: Observable<Action> = this.actions$.ofType(actions.types.Find).pipe(
    switchMap((action: actions.Find) =>
      fromPromise(
        this.jenkins.templates.sources({ with_public: action.with_public }),
      ).pipe(
        map((result: any[]) => {
          return new actions.FindComplete(result);
        }),
      ),
    ),
  );

  constructor(private actions$: Actions, private jenkins: JenkinsService) {}
}
