import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PipelineCreateComponent } from './containers/pipeline-create';
import { PipelineCreateByTemplateComponent } from './containers/pipeline-create-by-template';
import { PipelineDetailComponent } from './containers/pipeline-detail';
import { PipelineHistoryDetailComponent } from './containers/pipeline-history-detail';
import { PipelineHistoryListComponent } from './containers/pipeline-history-list';
import { PipelineListComponent } from './containers/pipeline-list';
import { PipelineTemplateListComponent } from './containers/pipeline-template-list';

const routes: Routes = [
  {
    path: 'jenkins',
    children: [
      {
        path: '',
        redirectTo: 'pipelines',
        pathMatch: 'full',
      },
      {
        path: 'pipelines', // querystring: ?keywords=:keywords&page=:page&edit=:id
        component: PipelineListComponent,
      },
      {
        path: 'pipelines/create',
        component: PipelineCreateComponent,
      },
      {
        path: 'pipelines/templates/template_create_pipeline/:id',
        component: PipelineCreateByTemplateComponent,
      },
      {
        path: 'pipelines/templates',
        component: PipelineTemplateListComponent,
      },
      {
        path: 'pipelines/:id', // querystring: ?edit
        component: PipelineDetailComponent,
      },
      {
        path: 'histories', // querystring: ?keywords=:keywords&page=:page
        component: PipelineHistoryListComponent,
      },
      {
        path: 'histories/:pipeline_uuid/:id',
        component: PipelineHistoryDetailComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class JenkinsRoutingModule {}
