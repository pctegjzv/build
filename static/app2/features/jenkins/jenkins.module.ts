import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { PipelineGlobalVarHelpComponent } from 'app2/features/jenkins/components/pipeline-global-var-help';
import { PipelineTemplateSyncSettingsComponent } from 'app2/features/jenkins/components/pipeline-template-sync-settings';
import { PipelineFormService } from 'app2/features/jenkins/services/pipeline-form.service';

import { SharedModule } from '../../shared/shared.module';

import { CodeRepositorySelectComponent } from './components/code-repository-select';
import { CodeRepositorySelectModalComponent } from './components/code-repository-select-modal';
import { CreateJenkinsCredentialComponent } from './components/create-jenkins-credential';
import { JenkinsfileContentExampleComponent } from './components/jenkinsfile-content-example';
import { PipelineCustomTemplateManComponent } from './components/pipeline-custom-template-man';
import { PipelineFormTriggerComponent } from './components/pipeline-form-trigger';
import {
  PipelineHistoryDiagramComponent,
  PipelineHistoryDiagramLinkComponent,
  PipelineHistoryDiagramNodeComponent,
} from './components/pipeline-history-diagram';
import { PipelineHistoryLogsComponent } from './components/pipeline-history-logs';
import { PipelineHistoryTableComponent } from './components/pipeline-history-table';
import { PipelineHistoryTestResultComponent } from './components/pipeline-history-test-result';
import { PipelineTemplateCardComponent } from './components/pipeline-template-card';
import { PipelineTemplateParamsFormComponent } from './components/pipeline-template-form';
import { PipelineTemplateInfoComponent } from './components/pipeline-template-info';
import { PipelineTemplateSyncReportComponent } from './components/pipeline-template-sync-report';
import { RecentHistoriesComponent } from './components/recent-histories';
import { RepoNotBindHintComponent } from './components/repo-not-bind-hint';
import { PipelineCreateComponent } from './containers/pipeline-create';
import {
  PipelineCreateByTemplateComponent,
  PipelineCreateByTemplateStepsComponent,
} from './containers/pipeline-create-by-template';
import { PipelineCreateModeComponent } from './containers/pipeline-create-mode';
import { PipelineDetailComponent } from './containers/pipeline-detail';
import { PipelineEditComponent } from './containers/pipeline-edit';
import { PipelineHistoryDetailComponent } from './containers/pipeline-history-detail';
import { PipelineHistoryListComponent } from './containers/pipeline-history-list';
import { PipelineListComponent } from './containers/pipeline-list';
import {
  PipelineTemplateListComponent,
  PipelineTemplateListContainerComponent,
} from './containers/pipeline-template-list';
import { PipelineUpdateByTemplateComponent } from './containers/pipeline-update-by-template';
import { HistoryEffects } from './effects/histories';
import { PipelineEffects } from './effects/pipeline';
import { TemplateEffects } from './effects/template';
import { TemplateSourcesEffects } from './effects/template_sources';
import { JenkinsRoutingModule } from './jenkins.routing.module';
import { reducers } from './reducers';
import { JenkinsMessageService } from './services/jenkins-message.service';
@NgModule({
  imports: [
    SharedModule,
    JenkinsRoutingModule,
    StoreModule.forFeature('jenkins', reducers),
    EffectsModule.forFeature([
      PipelineEffects,
      HistoryEffects,
      TemplateEffects,
      TemplateSourcesEffects,
    ]),
  ],
  declarations: [
    PipelineListComponent,
    PipelineDetailComponent,
    PipelineCreateComponent,
    PipelineEditComponent,
    PipelineHistoryListComponent,
    PipelineHistoryDetailComponent,
    CreateJenkinsCredentialComponent,
    PipelineFormTriggerComponent,
    CodeRepositorySelectComponent,
    CodeRepositorySelectModalComponent,
    PipelineHistoryLogsComponent,
    PipelineHistoryTestResultComponent,
    PipelineHistoryDiagramComponent,
    PipelineHistoryDiagramLinkComponent,
    PipelineHistoryDiagramNodeComponent,
    PipelineHistoryTableComponent,
    RecentHistoriesComponent,
    RepoNotBindHintComponent,
    PipelineCreateModeComponent,
    PipelineTemplateListComponent,
    PipelineTemplateListContainerComponent,
    PipelineTemplateCardComponent,
    PipelineTemplateInfoComponent,
    PipelineUpdateByTemplateComponent,
    PipelineCreateByTemplateComponent,
    PipelineTemplateParamsFormComponent,
    PipelineCreateByTemplateStepsComponent,
    PipelineGlobalVarHelpComponent,
    PipelineTemplateSyncSettingsComponent,
    PipelineTemplateSyncReportComponent,
    PipelineCustomTemplateManComponent,
    JenkinsfileContentExampleComponent,
  ],
  providers: [JenkinsMessageService, PipelineFormService],
  entryComponents: [
    PipelineEditComponent,
    CreateJenkinsCredentialComponent,
    CodeRepositorySelectModalComponent,
    PipelineCreateModeComponent,
    PipelineTemplateInfoComponent,
    PipelineGlobalVarHelpComponent,
    PipelineUpdateByTemplateComponent,
    PipelineTemplateListComponent,
    PipelineTemplateSyncSettingsComponent,
    PipelineTemplateSyncReportComponent,
    PipelineCustomTemplateManComponent,
    JenkinsfileContentExampleComponent,
  ],
})
export class JenkinsModule {}
