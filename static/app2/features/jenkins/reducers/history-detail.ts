import * as actions from '../actions/history';

export interface State {
  mutating: 'starting' | 'deleting' | 'canceling';
  loading: boolean;
  error: Error;
  data: any;
  id: string;
  pipeline_uuid: string;
}

export const initialState: State = {
  mutating: null,
  loading: false,
  error: null,
  data: null,
  id: null,
  pipeline_uuid: null,
};

export function reducer(state = initialState, action: actions.All) {
  switch (action.type) {
    case actions.types.Delete:
      return flagMutating(state, action, 'deleting');
    case actions.types.Start:
      return flagMutating(state, action, 'starting');
    case actions.types.Cancel:
      return flagMutating(state, action, 'canceling');
    case actions.types.Deleted:
      return clearWhenDeleted(state, action);
    case actions.types.DeleteError:
    case actions.types.StartError:
    case actions.types.CancelError:
    case actions.types.Started:
    case actions.types.Canceled:
      return unflagMutating(state, action);
    case actions.types.Get:
      return get(state, action);
    case actions.types.GetComplete:
      return getComplete(state, action);
    case actions.types.GetError:
      return getError(state, action);
    default:
      return state;
  }
}

export const selectActivatedId = (state: State) => ({
  id: state.id,
  pipeline_uuid: state.pipeline_uuid,
});
export const selectData = (state: State) => state.data;
export const selectMutating = (state: State) => state.mutating;
export const selectLoading = (state: State) => state.loading;

function flagMutating(
  state: State,
  action: actions.Start | actions.Delete | actions.Cancel,
  mutating: 'starting' | 'deleting' | 'canceling',
) {
  if (action.id !== state.id || action.pipeline_uuid !== state.pipeline_uuid) {
    return state;
  }

  return {
    ...state,
    mutating,
  };
}

function unflagMutating(
  state: State,
  action:
    | actions.StartError
    | actions.DeleteError
    | actions.CancelError
    | actions.Started
    | actions.Canceled,
) {
  const trigger =
    action instanceof actions.Started
      ? (action as actions.Started).from
      : action;

  if (
    trigger.id !== state.id &&
    trigger.pipeline_uuid !== state.pipeline_uuid
  ) {
    return state;
  }

  return {
    ...state,
    mutating: null,
  };
}

function getError(state: State, action: actions.GetError): State {
  if (action.id !== state.id || action.pipeline_uuid !== state.pipeline_uuid) {
    return state;
  }

  return {
    ...state,
    loading: false,
    error: action.error,
  };
}

function getComplete(state: State, action: actions.GetComplete): State {
  if (action.id !== state.id || action.pipeline_uuid !== state.pipeline_uuid) {
    return state;
  }

  return {
    ...state,
    loading: false,
    data: action.result,
  };
}

function get(state: State, action: actions.Get): State {
  if (action.id === state.id && action.pipeline_uuid === state.pipeline_uuid) {
    return {
      ...state,
      error: null,
      loading: true,
    };
  }

  return {
    ...initialState,
    id: action.id,
    pipeline_uuid: action.pipeline_uuid,
    loading: true,
    error: null,
  };
}

function clearWhenDeleted(state: State, action: actions.Deleted): State {
  if (action.id === state.id && action.pipeline_uuid === state.pipeline_uuid) {
    return {
      ...initialState,
      id: action.id,
      pipeline_uuid: action.pipeline_uuid,
    };
  }
  return state;
}
