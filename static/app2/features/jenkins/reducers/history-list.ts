import { createSelector } from '@ngrx/store';
import { omit } from 'lodash';

import * as actions from '../actions/history';

export interface State {
  total: number;
  loading: boolean;
  error: any;
  params: {
    search: string;
    jenkins_integration_id: string;
    page: number;
    page_size: number;
  };
  queries: {
    [params: string]: any[];
  };
  mutatingItems: {
    [id: string]: 'deleting' | 'starting' | 'canceling';
  };
}

export const initialState: State = {
  total: 0,
  loading: false,
  error: null,
  params: {
    search: '',
    jenkins_integration_id: '',
    page: 1,
    page_size: 10,
  },
  queries: {},
  mutatingItems: {},
};

export function reducer(state = initialState, action: actions.All) {
  switch (action.type) {
    case actions.types.Search:
      return search(state, action);
    case actions.types.SearchComplete:
      return searchComplete(state, action);
    case actions.types.SearchError:
      return searchError(state, action);
    case actions.types.Delete:
      return flagMutatingItem(state, action, 'deleting');
    case actions.types.Deleted:
      return removeDeletedItem(state, action);
    case actions.types.Cancel:
      return flagMutatingItem(state, action, 'canceling');
    case actions.types.Canceled:
    case actions.types.CancelError:
    case actions.types.DeleteError:
      return unflagMutatingItem(state, action);
    default:
      return state;
  }
}

export const selectActivatedParams = (state: State) => state.params;
export const selectActivatedPage = (state: State) => state.params.page;
export const selectAllQueries = (state: State) => state.queries;
export const selectActivatedQuery = createSelector(
  selectActivatedParams,
  selectAllQueries,
  (params, queries) => queries[queryIdentity(params)] || [],
);
export const selectLoading = (state: State) => state.loading;
export const selectTotal = (state: State) => state.total;
export const selectMutatingItems = (state: State) => state.mutatingItems;

function queryIdentity(params: { [name: string]: any }) {
  return Object.keys(params)
    .sort()
    .map(key => `${key}=${params[key]}`)
    .join('&');
}

function historyIdentity(id: string, pipeline_uuid: string) {
  return `${pipeline_uuid}#${id}`;
}

function search(state: State, action: actions.Search) {
  const newState: State = {
    ...state,
    loading: true,
    error: null,
    params: action.params,
  };

  if (
    state.params.search !== action.params.search ||
    state.params.page_size !== action.params.page_size ||
    state.params.jenkins_integration_id !== action.params.jenkins_integration_id
  ) {
    return {
      ...newState,
      total: 0,
    };
  }

  return newState;
}

function searchComplete(state: State, action: actions.SearchComplete) {
  const queryId = queryIdentity(action.params);
  if (queryId !== queryIdentity(state.params)) {
    return state;
  }

  return {
    ...state,
    loading: false,
    total: action.result.total,
    queries: {
      ...state.queries,
      [queryId]: action.result.items,
    },
  };
}

function searchError(state: State, action: actions.SearchError) {
  if (queryIdentity(action.params) !== queryIdentity(state.params)) {
    return state;
  }

  return {
    ...state,
    loading: false,
    error: action.error,
  };
}

function flagMutatingItem(
  state: State,
  action: actions.Delete | actions.Start | actions.Cancel,
  mutating: 'deleting' | 'starting' | 'canceling',
) {
  const { id, pipeline_uuid } = action;
  const identity = historyIdentity(id, pipeline_uuid);

  return {
    ...state,
    mutatingItems: {
      ...state.mutatingItems,
      [identity]: mutating,
    },
  };
}

function unflagMutatingItem(
  state: State,
  action:
    | actions.DeleteError
    | actions.StartError
    | actions.CancelError
    | actions.Started
    | actions.Canceled,
) {
  const { id, pipeline_uuid } = action;
  const identity = historyIdentity(id, pipeline_uuid);

  if (state.mutatingItems[identity]) {
    return {
      ...state,
      mutatingItems: omit(state.mutatingItems, [identity]),
    };
  }

  return state;
}

function removeDeletedItem(state: State, action: actions.Deleted) {
  const affected = Object.keys(state.queries || {}).filter(queryId =>
    state.queries[queryId].some(
      item =>
        item.pipeline_uuid === action.pipeline_uuid &&
        item.history_id === action.id,
    ),
  );

  const { id, pipeline_uuid } = action;
  const identity = historyIdentity(id, pipeline_uuid);

  const mutatingItems = state.mutatingItems[identity]
    ? omit(state.mutatingItems, [identity])
    : state.mutatingItems;

  if (affected.length || mutatingItems !== state.mutatingItems) {
    return {
      ...state,
      mutatingItems,
      queries: {
        ...state.queries,
        ...affected.reduce((result, queryId) => {
          return {
            ...result,
            [queryId]: state.queries[queryId].filter(
              item =>
                item.pipeline_uuid !== action.pipeline_uuid ||
                item.history_id !== action.id,
            ),
          };
        }, {}),
      },
    };
  }

  return state;
}
