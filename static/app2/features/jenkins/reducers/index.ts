import { Action, createFeatureSelector, createSelector } from '@ngrx/store';
import * as accountActions from 'app2/state-container/core/actions/account';
import * as projectActions from 'app2/state-container/core/actions/project';
import * as fromRoot from 'app2/state-container/core/reducers';

import * as fromHistoryDetail from './history-detail';
import * as fromHistoryList from './history-list';
import * as fromPipelineDetail from './pipeline-detail';
import * as fromPipelineList from './pipeline-list';
import * as fromTemplateList from './template-list';
import * as fromTemplateSourcesList from './template-sources';

export interface JenkinsState {
  pipelineList: fromPipelineList.State;
  pipelineDetail: fromPipelineDetail.State;
  historyList: fromHistoryList.State;
  historyDetail: fromHistoryDetail.State;
  templateList: fromTemplateList.State;
  templateSourcesList: fromTemplateSourcesList.State;
}

export interface State extends fromRoot.State {
  jenkins: JenkinsState;
}

export const reducers = {
  pipelineList: clearWhenGlobalChange(fromPipelineList.reducer),
  pipelineDetail: clearWhenGlobalChange(fromPipelineDetail.reducer),
  historyList: clearWhenGlobalChange(fromHistoryList.reducer),
  historyDetail: clearWhenGlobalChange(fromHistoryDetail.reducer),
  templateList: clearWhenGlobalChange(fromTemplateList.reducer),
  templateSourcesList: clearWhenGlobalChange(fromTemplateSourcesList.reducer),
};

function clearWhenGlobalChange<TState, TAction extends Action>(
  reducer: (state: TState, action: TAction) => TState,
) {
  return function(state: TState, action: TAction) {
    if (
      action.type === accountActions.SET_ACCOUNT ||
      action.type === projectActions.SET_PROJECT
    ) {
      return reducer(undefined, action);
    }
    return reducer(state, action);
  };
}

const selectJenkinsFeature = createFeatureSelector<JenkinsState>('jenkins');

export const selectPipelineList = createSelector(
  selectJenkinsFeature,
  (state: JenkinsState) => state.pipelineList,
);

export const selectPipelineDetail = createSelector(
  selectJenkinsFeature,
  (state: JenkinsState) => state.pipelineDetail,
);

export const selectHistoryList = createSelector(
  selectJenkinsFeature,
  (state: JenkinsState) => state.historyList,
);

export const selectHistoryDetail = createSelector(
  selectJenkinsFeature,
  (state: JenkinsState) => state.historyDetail,
);

export const selectTemplateList = createSelector(
  selectJenkinsFeature,
  (state: JenkinsState) => state.templateList,
);

export const selectTemplateSourcesList = createSelector(
  selectJenkinsFeature,
  (state: JenkinsState) => state.templateSourcesList,
);

// pipeline-list
export const selectPipelineListLoading = createSelector(
  selectPipelineList,
  fromPipelineList.selectLoading,
);
export const selectPipelineListTotal = createSelector(
  selectPipelineList,
  fromPipelineList.selectTotal,
);
export const selectPipelineListParams = createSelector(
  selectPipelineList,
  fromPipelineList.selectActivatedParams,
);
export const selectPipelineListMutatingItems = createSelector(
  selectPipelineList,
  fromPipelineList.selectMutatingItems,
);
export const selectPipelineListItems = createSelector(
  selectPipelineList,
  fromPipelineList.selectActivatedQuery,
);

// pipeline-detail
export const selectPipelineDetailData = createSelector(
  selectPipelineDetail,
  fromPipelineDetail.selectData,
);

export const selectPipelineDetailId = createSelector(
  selectPipelineDetail,
  fromPipelineDetail.selectActivatedId,
);

export const selectPipelineDetailLoading = createSelector(
  selectPipelineDetail,
  fromPipelineDetail.selectLoading,
);

export const selectPipelineDetailMutating = createSelector(
  selectPipelineDetail,
  fromPipelineDetail.selectMutating,
);

export const selectPipelineDetailHistoriesLoading = createSelector(
  selectPipelineDetail,
  fromPipelineDetail.selectHistoriesLoading,
);

export const selectPipelineDetailHistoriesPage = createSelector(
  selectPipelineDetail,
  fromPipelineDetail.selectActivatedPage,
);

export const selectPipelineDetailHistoriesTotal = createSelector(
  selectPipelineDetail,
  fromPipelineDetail.selectHistoriesTotal,
);

export const selectPipelineDetailHistories = createSelector(
  selectPipelineDetail,
  fromPipelineDetail.selectHistories,
);

export const selectPipelineDetailMutatingHistories = createSelector(
  selectPipelineDetail,
  fromPipelineDetail.selectMutatingHistories,
);

// history-list
export const selectHistoryListLoading = createSelector(
  selectHistoryList,
  fromHistoryList.selectLoading,
);

export const selectHistoryListPage = createSelector(
  selectHistoryList,
  fromHistoryList.selectActivatedPage,
);

export const selectHistoryListTotal = createSelector(
  selectHistoryList,
  fromHistoryList.selectTotal,
);
export const selectHistoryListParams = createSelector(
  selectHistoryList,
  fromHistoryList.selectActivatedParams,
);
export const selectHistoryListMutatingItems = createSelector(
  selectHistoryList,
  fromHistoryList.selectMutatingItems,
);
export const selectHistoryListItems = createSelector(
  selectHistoryList,
  fromHistoryList.selectActivatedQuery,
);

// history-detail
export const selectHistoryDetailData = createSelector(
  selectHistoryDetail,
  fromHistoryDetail.selectData,
);

export const selectHistoryDetailId = createSelector(
  selectHistoryDetail,
  fromHistoryDetail.selectActivatedId,
);

export const selectHistoryDetailMutating = createSelector(
  selectHistoryDetail,
  fromHistoryDetail.selectMutating,
);

export const selectHistoryDetailLoading = createSelector(
  selectHistoryDetail,
  fromHistoryDetail.selectLoading,
);

// template list
export const selectTemplateListItems = createSelector(
  selectTemplateList,
  fromTemplateList.selectItems,
);

// template sources
export const selectTemplateSourcesListItems = createSelector(
  selectTemplateSourcesList,
  fromTemplateSourcesList.selectItems,
);
