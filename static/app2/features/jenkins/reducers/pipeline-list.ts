import { createSelector } from '@ngrx/store';
import { omit } from 'lodash';

import * as actions from '../actions/pipeline';

export interface State {
  total: number;
  loading: boolean;
  error: any;
  params: {
    search: string;
    page: number;
    page_size: number;
  };
  queries: {
    [params: string]: any[];
  };
  mutatingItems: {
    [id: string]: 'deleting';
  };
}

export const initialState: State = {
  total: 0,
  loading: false,
  error: null,
  params: {
    search: '',
    page: 1,
    page_size: 10,
  },
  queries: {},
  mutatingItems: {},
};

export function reducer(state = initialState, action: actions.All) {
  switch (action.type) {
    case actions.types.Search:
      return search(state, action);
    case actions.types.SearchComplete:
      return searchComplete(state, action);
    case actions.types.SearchError:
      return searchError(state, action);
    case actions.types.Delete:
      return flagDeletingItem(state, action);
    case actions.types.Deleted:
      return removeDeletedItem(state, action);
    case actions.types.DeleteError:
      return unflagDeletingItem(state, action);
    case actions.types.Renew: {
      const identity = queryIdentity(state.params);
      return {
        ...state,
        queries: {
          ...state.queries,
          [identity]: [...(state.queries[identity] || [])],
        },
      };
    }
    default:
      return state;
  }
}

export const selectActivatedParams = (state: State) => state.params;
export const selectAllQueries = (state: State) => state.queries;
export const selectActivatedQuery = createSelector(
  selectActivatedParams,
  selectAllQueries,
  (params, queries) => queries[queryIdentity(params)] || [],
);
export const selectLoading = (state: State) => state.loading;
export const selectTotal = (state: State) => state.total;
export const selectMutatingItems = (state: State) => state.mutatingItems;

function queryIdentity(params: { [name: string]: any }) {
  return Object.keys(params)
    .sort()
    .map(key => `${key}=${params[key]}`)
    .join('&');
}

function search(state: State, action: actions.Search) {
  const newState: State = {
    ...state,
    loading: true,
    error: null,
    params: action.params,
  };

  if (
    state.params.search !== action.params.search ||
    state.params.page_size !== action.params.page_size
  ) {
    return {
      ...newState,
      total: 0,
    };
  }

  return newState;
}

function searchComplete(state: State, action: actions.SearchComplete) {
  const queryId = queryIdentity(action.params);
  if (queryId !== queryIdentity(state.params)) {
    return state;
  }

  return {
    ...state,
    loading: false,
    total: action.result.total,
    queries: {
      ...state.queries,
      [queryId]: action.result.items,
    },
  };
}

function searchError(state: State, action: actions.SearchError) {
  if (queryIdentity(action.params) !== queryIdentity(state.params)) {
    return state;
  }

  return {
    ...state,
    loading: false,
    error: action.error,
  };
}

function flagDeletingItem(state: State, action: actions.Delete) {
  return {
    ...state,
    mutatingItems: {
      ...state.mutatingItems,
      [action.id]: 'deleting',
    },
  };
}

function unflagDeletingItem(state: State, action: actions.DeleteError) {
  if (state.mutatingItems[action.id]) {
    return {
      ...state,
      mutatingItems: omit(state.mutatingItems, [action.id]),
    };
  }

  return state;
}

function removeDeletedItem(state: State, action: actions.Deleted) {
  const affected = Object.keys(state.queries || {}).filter(queryId =>
    state.queries[queryId].some(item => item.uuid === action.id),
  );

  const mutatingItems = state.mutatingItems[action.id]
    ? omit(state.mutatingItems, [action.id])
    : state.mutatingItems;

  if (affected.length || mutatingItems !== state.mutatingItems) {
    return {
      ...state,
      mutatingItems,
      queries: {
        ...state.queries,
        ...affected.reduce((result, queryId) => {
          return {
            ...result,
            [queryId]: state.queries[queryId].filter(
              item => item.uuid !== action.id,
            ),
          };
        }, {}),
      },
    };
  }

  return state;
}
