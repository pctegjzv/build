import * as actions from '../actions/template';
//import { createSelector } from '@ngrx/store';

export interface State {
  search: string;
  items: any[];
}

export const initialState: State = {
  search: '',
  items: [],
};

export function reducer(state = initialState, action: actions.All) {
  switch (action.type) {
    case actions.types.Delete:
      return removeItems(state, action);
    case actions.types.Get:
      return items(state, action);
    case actions.types.Find:
      return find(state, action);
    case actions.types.FindComplete:
      return FindComplete(state, action);
    default:
      return state;
  }
}

export const selectItems = (state: State) => state.items;

function items(_state: State, _action: actions.Get) {}

function removeItems(state: State, _action: actions.Delete) {
  return { ...state };
}

function find(state: State, action: actions.Find) {
  return {
    ...state,
    search: action.search,
  };
}

function FindComplete(state: State, action: actions.FindComplete) {
  return {
    ...state,
    items: action.items,
  };
}
