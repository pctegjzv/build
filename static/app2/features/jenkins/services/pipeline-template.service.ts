import { Injectable } from '@angular/core';
import { DynamicFormFieldDefinition } from 'app2/shared/form-field-control';
import { TranslateService } from 'app2/translate/translate.service';
import {
  extend as __extend,
  // find as __find,
  findIndex as __findIndex,
  flatten as __flatten,
  get as __get,
} from 'lodash';

import { ConfigmapService } from 'app2/shared/services/features/configmap.service';
import {
  ImageRegistry,
  ImageRegistryService,
} from 'app2/shared/services/features/image-registry.service';
import { ImageRepositoryService } from 'app2/shared/services/features/image-repository.service';
import { IntegrationService } from 'app2/shared/services/features/integration.service';
import { SonarQubeService } from 'app2/shared/services/features/integration.sonar-qube.service';
import { NamespaceService } from 'app2/shared/services/features/namespace.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';

export interface Template {
  stages: any;
  name: string;
  uuid: string;
  display_name: any;
  brief_description: string;
  description: string;
  arguments: any;
  annotations: any;
  labels: any;
}
interface RelationRule {
  action: string;
  when: { value: any; name: string } & {
    all?: { value: any; name: string }[];
    any?: { value: any; name: string }[];
  };
}

interface ValidationFields {
  maxLength?: number;
  minLength?: number;
  max?: number;
  min?: number;
  pattern: string;
}

interface I18nValue {
  'en-ZH': string;
  cn: string;
}

interface PipelineTemplateDynamicFormFieldDefinition {
  name: string;
  schema: { ['type']: string };
  display: {
    ['type']: string;
    ['name']: I18nValue;
    description: I18nValue;
    related: string;
    args?: {
      [name: string]: any;
    };
  };
  default: string;
  required: boolean;
  relation?: RelationRule[];
  validation: ValidationFields;
}

// constraint with backend
interface PipelineTemplateFiledSegmentDefinition {
  displayName: I18nValue;
  items: PipelineTemplateDynamicFormFieldDefinition[];
}

const TemplateFieldsType = {
  string: 'rc-input',
  multistring: 'rc-tagsinput',
  shellscripts: 'rc-code-edit',
  int: 'rc-input',
  boolean: 'rc-checkbox',
  'alauda.io/jenkinscredentials': 'rc-pipeline-dyform-credential',
  'alauda.io/newk8scontainermix': 'rc-pipeline-template-service-container',
  'alauda.io/imagetag': 'rc-single-selection-dropdown',
  'alauda.io/imagerepositorymix': 'rc-pipeline-dyform-repository',
  'alauda.io/k8senv': 'rc-pipeline-dyform-env',
  'alauda.io/k8snamespace': 'rc-single-selection-dropdown',
  'alauda.io/newk8sclustername': 'rc-single-selection-dropdown',
  'alauda.io/k8sconfigmap': 'rc-pipeline-dyform-configmap',
  'alauda.io/integration': 'rc-single-selection-dropdown',
  'alauda.io/sonarqube/qualitygates': 'rc-single-selection-dropdown',
  'alauda.io/sonarqube/lang': 'rc-single-selection-dropdown',
};

const CHINESE_FLAG = 'zh-CN';
const ENGLISH_FLAG = 'en';

export interface RelationPolymer {
  name: string;
  type: RELATION_TYPE;
  displayType?: string;
  action?: string;
  condition?: Function;
  cb?: Function;
}

export enum RELATION_TYPE {
  SIMPLE,
  BOOLEAN,
  LOGIC_OPERATION,
}

export function templateStagesConvert(stages: any[]): any[] {
  let cid = 1;
  const r: any[] = [];
  stages.map((stage: any) => {
    stage.id = cid;
    cid++;
    stage.tasks.map((ta: any) => {
      ta.id = cid;
      cid++;
    });
  });
  stages.map((stage: any, idx: number) => {
    if (stage.tasks.length > 1) {
      r.push({
        id: stage.id,
        name: stage.name,
        edges: stage.tasks.map((t: any) => ({
          id: t.id,
        })),
      });
    }
    stage.tasks.map((ta: any) => {
      const st = stages[idx + 1];
      r.push({
        id: ta.id,
        name: ta.name,
        edges: !st
          ? []
          : st.tasks.length > 1
            ? [{ id: stages[idx + 1].id }]
            : [{ id: st.tasks[0].id }],
      });
    });
  });
  return r;
}

const DROPDOWN_FIELDS = ['clusterName', 'namespace', 'envFrom'];
const DROPDOWN_TYPES = [
  'alauda.io/integration',
  'alauda.io/sonarqube/qualitygates',
  'alauda.io/sonarqube/lang',
  'alauda.io/imagetag',
  'alauda.io/k8sconfigmap',
];

@Injectable()
export class PipelineTemplateService {
  private _language: string;
  private _template: string;
  private _fieldConfigStore: any;
  private _init = false;

  private _arguments: PipelineTemplateFiledSegmentDefinition[];
  private _items: PipelineTemplateDynamicFormFieldDefinition[];
  private _configs: DynamicFormFieldDefinition[];

  private _relationship: any;

  constructor(
    private translate: TranslateService,
    private regionService: RegionService,
    private regionUtilitiesService: RegionUtilitiesService,
    private namespaceService: NamespaceService,
    private configMapService: ConfigmapService,
    private integrationService: IntegrationService,
    private sonarQubeService: SonarQubeService,
    private imageRegistryService: ImageRegistryService,
    private imageRepositoryService: ImageRepositoryService,
  ) {
    this._language =
      this.translate.currentLang === 'en' ? ENGLISH_FLAG : CHINESE_FLAG;
  }

  set template(value: string) {
    this._template = value;
    this.arguments = __get(value, 'definition.spec.arguments');
  }

  get template() {
    return this._template;
  }

  get language() {
    return this._language;
  }

  get fields(): DynamicFormFieldDefinition[] {
    if (!this._init) {
      this._fieldConfigStore = __extend([], this._configs);
      this._init = true;
    }
    return this._configs;
  }

  set arguments(value: any) {
    this._arguments = value;
    this.items = __flatten(value && value.map((seg: any) => seg.items));
  }

  get arguments() {
    return this._arguments;
  }

  set items(items) {
    this._items = items;
    this.handler();
    this.buildRelationship();
  }

  get items() {
    return this._items;
  }

  get relationship() {
    return this.bindRelationship(this._relationship);
  }

  get fieldsConfig() {
    return this._configs;
  }

  private buildRelationship() {
    const relation: any = {};
    this.items.forEach((item: PipelineTemplateDynamicFormFieldDefinition) => {
      if (item.display.related) {
        relation[item.display.related] = relation[item.display.related] || [];
        relation[item.display.related].push(
          this.getRelation(item, RELATION_TYPE.SIMPLE),
        );
      }
      if (item.relation) {
        const reln = __get(item, 'relation[0]');
        let target: RelationPolymer[];
        let type: RELATION_TYPE;
        if (reln.when.any || reln.when.all) {
          target = reln.when.any || reln.when.all;
          type = RELATION_TYPE.LOGIC_OPERATION;
        } else {
          target = [reln.when];
          type = RELATION_TYPE.BOOLEAN;
        }
        target.forEach((re: any) => {
          relation[re.name] = relation[re.name] || [];
          relation[re.name].push(this.getRelation(item, type));
        });
      }
    });
    this._relationship = relation;
  }

  private getRelation(item: any, type: RELATION_TYPE): RelationPolymer {
    let r: RelationRule;
    if ([RELATION_TYPE.BOOLEAN, RELATION_TYPE.LOGIC_OPERATION].includes(type)) {
      r = __get(item, 'relation[0]');
    }

    function simple() {
      return {
        name: item.name,
        type: RELATION_TYPE.SIMPLE,
        displayType: item.display.type,
      };
    }

    function bool() {
      return {
        name: item.name,
        action: r.action,
        type: RELATION_TYPE.BOOLEAN,
        condition: function(currentValue: any) {
          return !!currentValue === r.when.value
            ? r.action
            : oppositeAction(r.action);
        },
      };
    }

    function logic() {
      return {
        name: item.name,
        action: r.action,
        type: RELATION_TYPE.LOGIC_OPERATION,
        condition: function(values: any) {
          let iterationResult = r.when.all ? true : false;
          r.when.all.forEach((condition: { value: boolean; name: string }) => {
            const valueCondition = !!values[condition.name] === condition.value;
            iterationResult = r.when.all
              ? iterationResult && valueCondition
              : iterationResult || valueCondition;
          });
          return iterationResult ? r.action : oppositeAction(r.action);
        },
      };
    }

    switch (type) {
      case RELATION_TYPE.SIMPLE:
        return simple();
      case RELATION_TYPE.BOOLEAN:
        return bool();
      case RELATION_TYPE.LOGIC_OPERATION:
        return logic();
    }
  }

  private tagFields(index: number, flag: number) {
    if (index === -1) {
      return;
    }
    if (flag) {
      this._fieldConfigStore[index]['status'] = 'show';
    } else {
      this._fieldConfigStore[index]['status'] = 'hidden';
    }
  }

  private bindRelationship(relation: any) {
    const allFields = this._fieldConfigStore;
    const self = this;

    const boolEffect = (currentRelation: RelationPolymer) => {
      return async function(value: any) {
        const action = currentRelation.condition(value);
        if (action === 'show') {
          const index = __findIndex(allFields, {
            name: currentRelation.name,
          });
          self.tagFields(index, 1);
        } else {
          const index = __findIndex(allFields, {
            name: currentRelation.name,
          });
          self.tagFields(index, 0);
        }
        return self._fieldConfigStore.filter(
          (m: { status: string }) => m.status !== 'hidden',
        );
      };
    };

    const effecter = {
      [RELATION_TYPE.BOOLEAN]: boolEffect,
      [RELATION_TYPE.LOGIC_OPERATION]: boolEffect,
      [RELATION_TYPE.SIMPLE]: (
        currentRelation: RelationPolymer,
        _sourceField?: string,
      ) => {
        return async function(currentValue: any, models?: any) {
          let index: number;
          if (currentRelation.name === 'env') {
            index = __findIndex(models, { name: currentRelation.name });
            if (index > -1 && currentValue && currentValue.namespace) {
              const targetModel = models[index];
              targetModel['namespace'] = currentValue.namespace;
              targetModel['clusterName'] = currentValue.clusterName;
            }
          }

          if (
            DROPDOWN_FIELDS.includes(currentRelation.name) ||
            DROPDOWN_TYPES.includes(currentRelation.displayType)
          ) {
            index = __findIndex(models, { name: currentRelation.name });
            if (index > -1) {
              const targetModel = models[index];
              targetModel['options'] = await targetModel['options_getter'](
                currentValue,
              );
            }
          }
          return models;
        };
      },
    };

    Object.keys(relation).forEach((key: any) => {
      const targetShip: RelationPolymer[] = relation[key];
      targetShip.forEach((relation: RelationPolymer) => {
        relation.cb = effecter[relation.type](relation);
      });
    });
    return relation;
  }

  private handleValidation(item: any) {
    const va = __get(item, 'validation');
    const na = {};
    if (va) {
      Object.keys(va).forEach((k: string) => {
        na[k.toLowerCase()] = va[k];
      });
    }
    return na;
  }

  private handler() {
    this._configs = this._items.map(
      (item: PipelineTemplateDynamicFormFieldDefinition) => {
        const field = {
          name: item.name,
          controlType: TemplateFieldsType[item.display.type] || 'rc-input',
          label:
            this.targetLanguageString(__get(item, 'display.name')) || 'unknown',
          placeholder: '',
          description: this.targetLanguageString(
            __get(item, 'display.description'),
          ),
          ...__extend(this.handleValidation(item), {
            required: !!item.required,
          }),
          displayField: 'name',
          valueField: 'name',
        };

        if (item.default) {
          field['value'] = item.default;
        } else if (item.display.type === 'boolean') {
          field['value'] = false;
        }

        if (item.name === 'args') {
          field['maxlength'] = '200';
        }

        if (DROPDOWN_FIELDS.includes(item.name)) {
          field['options_getter'] = this.relatedResources(item.name);
        }

        if (DROPDOWN_TYPES.includes(item.display.type)) {
          field['options_getter'] = this.relatedResources(
            item.name,
            item.display.type,
            item.display.args || {},
          );
        }

        switch (item.display.type) {
          case 'alauda.io/integration':
          case 'alauda.io/sonarqube/qualitygates':
            field.valueField = 'id';
            break;
          case 'alauda.io/sonarqube/lang':
            field.valueField = 'key';
            break;
        }

        return field;
      },
    );
  }

  private targetLanguageString(target: any) {
    return (target && target[this._language]) || '';
  }

  private relatedResources(name: string, type = '', args: any = {}) {
    const self = this;
    async function clusters() {
      return await self.regionService
        .getRegions()
        .then((regions: Region[]) => {
          return regions.filter(region =>
            self.regionUtilitiesService.isNewK8sRegion(region),
          );
        })
        .catch(() => {});
    }

    async function namespaces(value: string) {
      if (!value) {
        return;
      }
      const region = await self.regionService.getRegionByName(value);
      return await self.namespaceService.getNamespaceOptions(region.id);
    }

    async function envForm(value: {
      clusterName: string;
      namespace: string;
      containerName: string;
    }) {
      if (!value || !value.clusterName || !value.namespace) {
        return;
      }
      const region = await self.regionService.getRegionByName(
        value.clusterName,
      );
      return await self.configMapService
        .getK8sConfigmaps({
          clusterId: region.id,
          namespace: value.namespace,
        })
        .then((configMaps: any[]) => {
          return configMaps.map((configMap: any) => {
            return {
              name: __get(configMap, 'kubernetes.metadata.name'),
            };
          });
        });
    }

    async function integrations(args: any) {
      const { types } = args;

      return await self.integrationService
        .getIntegrations({ types, page: 1, page_size: 200 })
        .catch(() => []);
    }

    async function sonarQubeQualityGates(integration: string) {
      if (!integration) {
        return [];
      }

      return await self.sonarQubeService
        .getQualityGates(integration)
        .catch(() => []);
    }

    async function sonarQubeLanguages(integration: string) {
      if (!integration) {
        return [];
      }

      return await self.sonarQubeService
        .getLanguages(integration)
        .catch(() => []);
    }

    async function imageTags(image: { registry: string; repository: string }) {
      if (!image || !image.registry || !image.repository) {
        return [];
      }

      const registries = await self.imageRegistryService
        .find()
        .catch(() => [] as ImageRegistry[]);
      const registry = registries.find(
        item => item.endpoint === image.registry,
      );

      if (!registry) {
        return [];
      }

      const repositoryParts = image.repository.split('/');

      if (!repositoryParts.length) {
        return [];
      }

      const [project_name, repository_name] =
        repositoryParts.length > 1
          ? repositoryParts
          : [undefined, ...repositoryParts];

      const tags = await self.imageRepositoryService
        .getRepositoryTags({
          registry_name: registry.name,

          project_name: registry.is_public ? undefined : project_name,
          repository_name,
        })
        .catch(() => []);

      return tags;
    }

    switch (type) {
      case 'alauda.io/integration':
        return integrations.bind(null, args);
      case 'alauda.io/sonarqube/qualitygates':
        return sonarQubeQualityGates;
      case 'alauda.io/sonarqube/lang':
        return sonarQubeLanguages;
      case 'alauda.io/imagetag':
        return imageTags;
    }

    switch (name) {
      case 'clusterName':
        return clusters;
      case 'namespace':
        return namespaces;
      case 'envFrom':
        return envForm;
    }
  }
}

function oppositeAction(action: string) {
  return action === 'show' ? 'hidden' : 'show';
}
