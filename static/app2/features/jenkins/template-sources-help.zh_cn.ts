export const ZH_HELP = `

参照下面步骤创建并管理自定义流水线模板

1. 参照下方“流水线与任务模板创建说明”，编写 Yaml 文件

2. 模板验证：可下载最新版本”模板验证工具“在本地进行模板验证，[下载地址](https://github.com/alauda/jenkinsfilext/releases)

3. 参照下方“仓库结构”，将编写好的 Yaml 文件存放到代码仓库的相应位置

4. 在流水线模板页点击"同步模板仓库"下拉按钮，点击“模板同步设置”

5. 正确填写模板仓库信息，点击“保存并同步”

6. 同步成功后，在流水线模板页可看到导入的自定义模板，并可以用其创建流水线

仓库结构

\`\`\`
|-- definition
|   |--pipelines               # 模板定义文件
|   |   |--golangBuild      
|   |      |--pipeline.yaml    # 描述文件
|   |--tasks                   # task 模板
|      |--golangBuildImages
|         |--task.yaml         # 描述文件
\`\`\`


流水线与任务模板创建说明
\`\`\`
apiVersion (1)
kind (2)
metadata (3)
	name (4)
	annotations (5)
		alauda.io/displayName.zh-CN (6)
		alauda.io/displayName.en
		alauda.io/description.zh-CN (7)
		alauda.io/description.en
		alauda.io/readme.zh-CN (8)
		alauda.io/readme.en
		alauda.io/version (9)
	labels (10)
		phase: incubator (11)
spec (12)
	engine (13)
	withSCM (14)
	stages (15)
		name (16)
			tasks (17)
				name (18)
				type (19)
				options (20)
					timeout (21)
				approve (22)
					message (23)
					timeout (24)
				environments (25)
					name (26)
					value (27)
	arguments (28)
		displayName (29)
			zh-CN
			en
		items (30)
			name (31)
			schema (32)
				type (33)
			binding (34)
			display (35)
				type (36)
				name (37)
					zh-CN
					en
				description (38)
		  			zh-CN
					en
				related (39)
			required (40)
			default (41)
			relation (42)
				action (43)
				when (44)
					name (45)
					value
			validation (46)
				pattern (47)
				maxLength (48)
			
\`\`\`


1. api版本：固定文字“devops.alauda.io/v1alpha1”

2. 模板类型：模板可分为“流水线模板PipelineTemplate”和“任务模板PipelineTaskTemplate”两种类型

   > 流水线模板（PipelineTemplate）：流水线模板创建成功后，可用其创建流水线
   >
   > 任务模板（PipelineTaskTemplate）：任务模板创建成功后，创建流水线模板时可以引用任务模板

3. 元数据：包含模板名称、注释组、标签组等参数

4. 模板名称

5. 注释组：包含模板的显示名称、描述、说明、版本等参数

6. 中英文显示名称

7. 中英文描述

8. 中英文说明

9. 模板版本：版本号必须以“v”开头；同步模板时，只有模板版本变更时，才会更新模板

10. 模板标签：用户可自定义任何名称的 label，会展示到模板卡片以及模板详情中

11. 官方提供 label ：phase，当 phase 的值为 incubator 或 test 时，用户使用该模板创建流水线时，会提示其此版本为开发中的版本，并不稳定

12. 格式定义：定义该模板的渲染方式、是否需要代码仓库、阶段组以及表单区域组等具体内容

13. 渲染方式：决定模板的书写格式

    > 流水线模板（PipelineTemplate）：现仅支持 graph
    >
    > 任务模板（PipelineTaskTemplate）：现仅支持 gotpl；gotpl 类型的脚本语法请参照：
    >
    > go template：https://golang.org/pkg/text/template/
    >
    > jenkinsfile ：https://jenkins.io/doc/book/pipeline/syntax/

14. 是否需要代码仓库：若用此模板创建的流水线需要使用代码仓库，则值为true，否则为false；只有模板类型为流水线模板（PipelineTemplate）时此参数生效

15. 阶段组：一个流水线模板可包含多个阶段

16. 阶段名称：一个阶段的名称

17. 任务组：一个阶段中，可以有多个任务，每个任务分别引用任务模板（PipelineTaskTemplate）。任务组中，包含任务名称、任务类型、任务选项组、任务审核、环境变量组等参数

18. 任务名称：任务组中，一个任务的名称

19. 任务类型：格式为“账号名称/任务模板名称”

20. 任务选项组：包含“超时时长”等信息

21. 超时时长：单位为秒；任务执行耗时超过此处设定的超时时长时，任务中止，流水线状态变为“取消”

22. 审核：添加审核后，执行到该任务时，用户需选择“继续（proceed）”或“取消（abort）”，选择“继续”则继续执行流水线，选择“取消”则中止流水线

23. 审核消息：流水线执行到审核任务时，用户可看到此提示消息内容

24. 审核超时时长：单位为秒；等待用户审核耗时若超过审核超时时长，则流水线中止执行，状态变为“取消” 

25. 环境变量组：可添加多组环境变量，每组由环境变量名称与环境变量值组成

26. 环境变量名称

27. 环境变量值

28. 表单区域组：表单参数可分别放到不同的表单区域中

29. 区域显示名称（中英文）：每一个表单区块卡片的抬头内容

30. 表单参数组：在一个表单区域下，可以有一个或多个表单参数

31. 表单参数名称：表单参数组中的一个表单参数的名称

32. 表单参数概要：包含表单参数类型（type）等参数

33. 表单参数类型包括以下数据类型：
    > boolean、int、map、array、string、alauda.io/newk8scontainermix、alauda.io/imagerepositorymix、alauda.io/k8senv

34. 表单参数绑定：表单中的参数需要与其引用的任务模板中的参数相关联，模板参数方可生效
    > 例：“BuildImage.args.dockerfilePath”，表示此表单参数（item）与任务模板（task）“BuildImage”中的表单参数（item）dockerfilePath 关联

35. 表单参数显示：包含显示类型、显示名称、显示描述以及关联参数等参数

36. 表单参数显示类型：前端交互类型
    > 目前支持类型：int, boolean, string,alauda.io/newk8scontainermix,alauda.io/imagerepositorymix,alauda.io/k8senv, shellscripts

37. 表单参数显示名称：页面展示的参数名称

38. 表单参数显示描述：流水线详情页展示的参数描述

39. 关联参数：当参数类型为 alauda.io/jenkinscredentials 或 alauda.io/imagetag 时，可为其设置此关联参数,该参数值需设置为其他参数的名称，设置后，参数的选择分支内容会随设置的 related 值变化

40. 表单参数必填选项：此表单参数是否必填：true为必填参数，false为选填参数

41. 默认值：该参数默认值

42. 依赖关系组：当依赖关系组中，执行条件（when）中的参数名（name）等于参数值（value）时，会触发动作（action）执行

43. 关联的动作

44. 执行条件

45. 条件执行依赖的参数名和参数值

46. 验证：可使用正则验证、或最大长度验证

47. 正则验证：参数类型为 string 时有效

48. 最大长度验证：默认 0 表示不进行校验， 参数类型为 string 时有效
`;
