const badgeTexts = {
  sonarqube: 'jenkins_history_badge_sonarqube_hint',
  defaultText: 'jenkins_history_badge_hint',
};
export function badgeHint(data: any) {
  let hintText = '';
  if (data.text) {
    const type = data.text.toLowerCase();
    if (!['sonarqube'].includes(type)) {
      hintText = badgeTexts['defaultText'];
    } else {
      hintText = badgeTexts[type];
    }
  }
  return hintText;
}
