import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { K8sAppListComponent } from 'app2/features/app/list/k8s-app-list.component';
import { K8sAppServiceListComponent } from 'app2/features/app/service-list/k8s-app-service-list.component';
import { SharedModule } from 'app2/shared/shared.module';

const components = [K8sAppListComponent, K8sAppServiceListComponent];

@NgModule({
  imports: [SharedModule, RouterModule],
  declarations: components,
  exports: components,
  entryComponents: [],
})
export class AppAppCatalogSharedModule {}
