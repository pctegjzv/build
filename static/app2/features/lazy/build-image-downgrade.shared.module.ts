import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BuildCreateSelectComponent } from 'app2/features/build/create/build-create-select.component';
import { BuildHistoryComponent } from 'app2/features/build/history/build-history.component';
import { BuildTriggerComponent } from 'app2/features/build/trigger/build.trigger.component';
import { ArtifactsTableComponent } from 'app2/features/image/repository/artifacts-table/artifacts-table.component';
import { SharedModule } from 'app2/shared/shared.module';

const components = [
  ArtifactsTableComponent,
  BuildCreateSelectComponent,
  BuildHistoryComponent,
  BuildTriggerComponent,
];

@NgModule({
  imports: [RouterModule, SharedModule],
  declarations: components,
  exports: components,
  entryComponents: components,
})
export class BuildImageDowngradeSharedModule {}
