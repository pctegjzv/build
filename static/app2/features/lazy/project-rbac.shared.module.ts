import { NgModule } from '@angular/core';

import { RoleListComponent } from 'app2/features/rbac/roles/list/role-list.component';
import { SharedModule } from 'app2/shared/shared.module';

const components = [RoleListComponent];

@NgModule({
  imports: [SharedModule],
  declarations: [RoleListComponent],
  exports: components,
})
export class ProjectRbacSharedModule {}
