import { NgModule } from '@angular/core';

import { SharedModule } from 'app2/shared/shared.module';

import { AccountUpdatePwdComponent } from 'app2/features/rbac/account/dialog/account-update-pwd/account-update-pwd.component';
import { UpdateCompanyComponent } from 'app2/features/rbac/detail/dialog/update-company/update-company.component';

@NgModule({
  imports: [SharedModule],
  declarations: [AccountUpdatePwdComponent, UpdateCompanyComponent],
  entryComponents: [AccountUpdatePwdComponent, UpdateCompanyComponent],
})
export class RbacUserSharedModule {}
