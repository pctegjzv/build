import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ServiceConfigAffinityComponent } from 'app2/features/service/config/service-config-affinity.component';
import { ServiceConfigKubeServiceComponent } from 'app2/features/service/config/service-config-kube-service.component';
import { ServiceConfigLabelsComponent } from 'app2/features/service/config/service-config-labels.component';
import { ServiceConfigNodeTagsComponent } from 'app2/features/service/config/service-config-node-tags.component';
import { ServiceDetailConfigComponent } from 'app2/features/service/config/service-detail-config.component';
import { ContainerConfigVolumeComponent } from 'app2/features/service/container-config/service-container-config-volume.component';
import { ContainerConfigComponent } from 'app2/features/service/container-config/service-container-config.component';
import { ContainerConfigConfigmapComponent } from 'app2/features/service/container-config/service-container-configmap.component';
import { ContainerConfigEnvvarsComponent } from 'app2/features/service/container-config/service-container-envvars.component';
import { ContainerConfigHealthcheckComponent } from 'app2/features/service/container-config/service-container-healthcheck.component';
import { ContainerConfigLogFileComponent } from 'app2/features/service/container-config/service-container-log-file.component';
import {
  ContainerFieldsEnvvarsComponent,
  ContainerFieldsEnvvarsValidatorDirective,
} from 'app2/features/service/create-fields/container-envvars.component';
import { HealthcheckDialogComponent } from 'app2/features/service/create-fields/container-healthcheck-dialog.component';
import { HealthcheckComponent } from 'app2/features/service/create-fields/container-healthcheck.component';
import { ContainerFieldsLogFileComponent } from 'app2/features/service/create-fields/container-log-file.component';
import { ContainerFieldsResourceSizeComponent } from 'app2/features/service/create-fields/container-resource-size.component';
import { ContainerVolumeDialogComponent } from 'app2/features/service/create-fields/container-volume-dialog.component';
import { ContainerFieldsVolumeComponent } from 'app2/features/service/create-fields/container-volume.component';
import { ServiceFieldsKubeServiceDialogComponent } from 'app2/features/service/create-fields/kube-service-dialog.component';
import { ServiceFieldsKubeServiceComponent } from 'app2/features/service/create-fields/kube-service.component';
import { ServiceFieldsAffinityComponent } from 'app2/features/service/create-fields/service-affinity.component';
import { ServiceContainerFieldsComponent } from 'app2/features/service/create-fields/service-container.component';
import { ServiceFieldsLabelsComponent } from 'app2/features/service/create-fields/service-labels.component';
import { ServiceFieldsNetworkModeComponent } from 'app2/features/service/create-fields/service-network-mode.component';
import { ServiceFieldsNodeTagsComponent } from 'app2/features/service/create-fields/service-node-tags.component';
import { ServiceCreateMethodSelectComponent } from 'app2/features/service/create-method-select/service-create-method-select.component';
import { ServiceEndpointsComponent } from 'app2/features/service/endpoints/service-endpoints.component';
import { K8sServiceExecComponent } from 'app2/features/service/exec/k8s-service-exec.component';
import { ServiceInstancesComponent } from 'app2/features/service/instances/service-instances.component';
import { ServiceLogsPollingComponent } from 'app2/features/service/log/service-logs-polling.component';
import { ServiceLogsSearchComponent } from 'app2/features/service/log/service-logs-search.component';
import { ServiceMetricChartComponent } from 'app2/features/service/metric/service-metric-chart.component';
import { ServiceMetricsComponent } from 'app2/features/service/metric/service-metrics.component';
import { ServiceUpdateConfigmapComponent } from 'app2/features/service/update-configmap/service-update-configmap.component';
import { ServiceUpdateEndpointsComponent } from 'app2/features/service/update-endpoints/service-update-endpoints.component';
import { ServiceUpdateEnvvarsComponent } from 'app2/features/service/update-envvars/service-update-envvars.component';
import { K8sServiceHealthCheckConfigComponent } from 'app2/features/service/update-health-check/k8s-service-health-check-config.component';
import { K8sServiceUpdateHealthCheckComponent } from 'app2/features/service/update-health-check/k8s-service-update-health-check.component';
import { ServiceUpdateImageTagComponent } from 'app2/features/service/update-image-tag/service-update-image-tag.component';
import { ServiceUpdateReplicasComponent } from 'app2/features/service/update-replicas/service-update-replicas.component';
import { ServiceUpdateResourceSizeComponent } from 'app2/features/service/update-resource-size/service-update-resource-size.component';
import { ServiceUpdateScalingComponent } from 'app2/features/service/update-scaling/service-update-scaling.component';
import { ServiceNameValidatorDirective } from 'app2/features/service/validator/service-name-validator.directive';
import { ServiceVersionSelectComponent } from 'app2/features/service/version-select/service-version-select.component';
import { SharedModule } from 'app2/shared/shared.module';

const components = [
  ServiceFieldsKubeServiceComponent,
  ServiceContainerFieldsComponent,
  ContainerFieldsEnvvarsComponent,
  ContainerFieldsEnvvarsValidatorDirective,
  ContainerFieldsResourceSizeComponent,
  ContainerFieldsVolumeComponent,
  ServiceFieldsAffinityComponent,
  ServiceFieldsNodeTagsComponent,
  ServiceFieldsLabelsComponent,
  ServiceFieldsKubeServiceDialogComponent,
  HealthcheckComponent,
  HealthcheckDialogComponent,
  ServiceMetricsComponent,
  ServiceMetricChartComponent,
  ServiceCreateMethodSelectComponent,
  ServiceNameValidatorDirective,
  ContainerVolumeDialogComponent,
  ServiceFieldsNetworkModeComponent,
  ContainerFieldsLogFileComponent,
];

const serviceDetailComponents = [
  ServiceInstancesComponent,
  ServiceLogsPollingComponent,
  ServiceEndpointsComponent,
  ServiceLogsSearchComponent,
  ServiceDetailConfigComponent,
  ServiceConfigLabelsComponent,
  ServiceConfigNodeTagsComponent,
  ServiceConfigKubeServiceComponent,
  ServiceConfigAffinityComponent,
  ContainerConfigComponent,
  ContainerConfigVolumeComponent,
  ContainerConfigConfigmapComponent,
  ContainerConfigEnvvarsComponent,
  ContainerConfigHealthcheckComponent,
  ServiceUpdateReplicasComponent,
  ServiceUpdateConfigmapComponent,
  ServiceUpdateEndpointsComponent,
  ServiceUpdateEnvvarsComponent,
  K8sServiceHealthCheckConfigComponent,
  ServiceUpdateImageTagComponent,
  ServiceUpdateResourceSizeComponent,
  ServiceUpdateScalingComponent,
  K8sServiceUpdateHealthCheckComponent,
  K8sServiceExecComponent,
  ServiceVersionSelectComponent,
  ContainerConfigLogFileComponent,
];

@NgModule({
  imports: [RouterModule, SharedModule],
  declarations: [...components, ...serviceDetailComponents],
  exports: [...components, ...serviceDetailComponents],
  entryComponents: [
    ServiceFieldsKubeServiceDialogComponent,
    HealthcheckDialogComponent,
    ServiceCreateMethodSelectComponent,
    ContainerVolumeDialogComponent,
    ServiceUpdateReplicasComponent,
    ServiceUpdateConfigmapComponent,
    ServiceUpdateEndpointsComponent,
    ServiceUpdateEnvvarsComponent,
    K8sServiceHealthCheckConfigComponent,
    ServiceUpdateImageTagComponent,
    ServiceUpdateResourceSizeComponent,
    ServiceUpdateScalingComponent,
    K8sServiceUpdateHealthCheckComponent,
    K8sServiceExecComponent,
    ServiceVersionSelectComponent,
  ],
})
export class ServiceAppSharedModule {}
