import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ServiceInstanceBindingAppListComponent } from 'app2/features/service-catalog/service-instance/binding-app-list/binding-app-list.component';
import { ServiceInstanceBindingInstanceDialogComponent } from 'app2/features/service-catalog/service-instance/binding-instance-dailog/binding-instance-dialog.component';
import { ServiceInstanceViewInfoDialogComponent } from 'app2/features/service-catalog/service-instance/view-info-dialog/view-info-dialog.component';
import { SharedModule } from 'app2/shared/shared.module';

const components = [
  ServiceInstanceBindingAppListComponent,
  ServiceInstanceBindingInstanceDialogComponent,
  ServiceInstanceViewInfoDialogComponent,
];

@NgModule({
  imports: [RouterModule, SharedModule],
  declarations: components,
  exports: components,
  entryComponents: [
    ServiceInstanceBindingInstanceDialogComponent,
    ServiceInstanceViewInfoDialogComponent,
  ],
})
export class ServiceCatalogServiceSharedModule {}
