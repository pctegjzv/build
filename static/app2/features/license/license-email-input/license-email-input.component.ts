import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';

import { TranslateService } from 'app2/translate/translate.service';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'rc-license-email-input',
  templateUrl: './license-email-input.component.html',
  styleUrls: ['./license-email-input.component.scss'],
})
export class LicenseEmailInputComponent implements OnInit, AfterViewInit {
  @Input()
  email: string;
  @Input()
  index: number;
  @Output()
  confirm: EventEmitter<any> = new EventEmitter();
  @Output()
  delete: EventEmitter<any> = new EventEmitter();

  @ViewChild('input')
  private inputRef: ElementRef;

  inputState: 'edit' | 'done';
  emailDisplay: string;
  emailCache: string;
  errorsMap: any;

  constructor(private translate: TranslateService) {}

  ngOnInit() {
    this.emailCache = this.emailDisplay = this.email;
    this.inputState = !!this.email.trim() ? 'done' : 'edit';
    this.errorsMap = {
      email: {
        pattern: this.translate.get('email_format_limit'),
      },
    };
  }

  ngAfterViewInit() {
    if (this.inputState === 'edit') {
      this.inputRef.nativeElement.focus();
    }
  }

  edit(input: HTMLInputElement) {
    input.focus();
    this.inputState = 'edit';
  }

  deleteEmail() {
    this.delete.emit({
      index: this.index,
      email: this.emailCache,
    });
  }

  close() {
    if (!!this.emailCache.trim()) {
      this.emailDisplay = this.emailCache;
      this.inputState = 'done';
    } else {
      this.deleteEmail();
    }
  }

  complete() {
    if (!this.inputIsValid) {
      return;
    }
    if (this.emailDisplay.trim() === this.emailCache.trim()) {
      return (this.inputState = 'done');
    }
    this.confirm.emit({
      index: this.index,
      email: this.emailDisplay,
      callback: () => {
        this.inputState = 'done';
        this.emailCache = this.emailDisplay;
      },
    });
  }

  get inputIsValid() {
    return this.inputRef.nativeElement.className.includes('ng-valid');
  }

  get inputIsInvalidState() {
    return this.inputState === 'edit' && !this.inputIsValid;
  }
}
