import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { cloneDeep, findKey, remove } from 'lodash';
import moment from 'moment';
import { Observable } from 'rxjs';

import { MessageService, NotificationService } from 'alauda-ui';
import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount, RcProfile, RcRegisterInfo } from 'app2/core/types';
import {
  EmailsCallbackOptions,
  LicenseInfoModal,
} from 'app2/features/license/license.type';
import { AccountService } from 'app2/shared/services/features/account.service';
import { LicenseService } from 'app2/shared/services/features/license.service';
import { HttpService } from 'app2/shared/services/http/http.service';
import * as licenseActions from 'app2/state-store/license/actions';
import * as licenseReducers from 'app2/state-store/license/reducers';
import { AppState } from 'app2/state-store/reducers';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './license.component.html',
  styleUrls: ['./license.component.scss'],
})
export class LicenseComponent implements OnInit {
  username: String;
  loading = false;
  emailLoading = false;
  profile: RcProfile;
  licenseInfo: LicenseInfoModal;
  licenseCode: string;
  remindType = 2;
  registerInfo: RcRegisterInfo;
  expiredDayClass: string;
  licenseActivatedOrder: string;
  tipShow = false;
  textareaWarning: boolean;
  licenseState$: Observable<licenseReducers.State>;
  remindTypeOptions: any = [
    {
      name: this.translateService.get('all_member'),
      type: 1,
    },
    {
      name: this.translateService.get('only_super_user'),
      type: 2,
    },
  ];
  licenseEditState: 'edit' | 'done';
  submitting: boolean;

  @ViewChild('ngForm')
  form: NgForm;

  expireDayRuler: any = {
    beta: {
      warning: (expirationDay: number): boolean => {
        return expirationDay > 3 && expirationDay <= 7;
      },
      error: (expirationDay: number): boolean => {
        return expirationDay <= 3;
      },
    },
    release: {
      warning: (expirationDay: number): boolean => {
        return expirationDay > 7 && expirationDay <= 30;
      },
      error: (expirationDay: number): boolean => {
        return expirationDay <= 7;
      },
    },
  };

  get notificationContent() {
    return `${this.translateService.get('license_info_5')}${
      this.profile.email
    }`;
  }

  get formatExpireAt() {
    if (this.licenseInfo) {
      return moment(this.licenseInfo.expire_at).format('YYYY-MM-DD');
    }
    return '';
  }

  constructor(
    private accountService: AccountService,
    private httpService: HttpService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
    private licenseService: LicenseService,
    private store: Store<AppState>,
    @Inject(ACCOUNT) private account: RcAccount,
  ) {}

  async ngOnInit() {
    //licenseActivatedOrder ： 1 first activate ，2：not first
    this.licenseActivatedOrder = localStorage.getItem('licenseActivatedOrder');
    if (this.licenseActivatedOrder === '1') {
      this.tipShow = true;
    } else if (this.licenseActivatedOrder === '2') {
      this.auiMessageService.success({
        content: this.translateService.get('license_has_activated'),
      });
    }
    localStorage.setItem('licenseActivatedOrder', '0');

    await this.loadProfile();
    this.cancleEditLiceseCode();
  }

  async loadProfile() {
    this.loading = true;

    try {
      this.profile = await this.accountService.getAuthProfile();

      const [profile, registerInfo, licenseInfo] = await Promise.all([
        this.accountService.getAuthProfile(),
        this.licenseService.getRegisterInfo(),
        this.licenseService.getLicenseInfo(),
      ]);

      //license store operation
      this.store.dispatch(new licenseActions.UpdateByKeys(licenseInfo));

      this.profile = profile;
      this.registerInfo = registerInfo;
      this.licenseInfo = licenseInfo;

      this.remindType = this.licenseInfo.notify_target;

      this.expiredDayClass = findKey(
        this.expireDayRuler[this.licenseInfo['kind']],
        (rulerFun: Function) =>
          rulerFun(this.licenseInfo['days_until_expiration']),
      );
    } catch (error) {
      if (error.errors) {
        this.auiNotificationService.error({
          content: this.translateService.get(error.errors[0].message),
        });
      } else {
        throw error;
      }
    } finally {
      this.loading = false;
    }
  }

  closeHeaderTip() {
    this.tipShow = false;
  }

  addEmail() {
    this.licenseInfo.emails.push('');
  }

  async deleteEmail(callbackData: EmailsCallbackOptions) {
    if (!this.licenseInfo.emails[callbackData.index]) {
      return remove(
        this.licenseInfo.emails,
        (_email, i) => i === callbackData.index,
      );
    }
    const emailsCopy = cloneDeep(this.licenseInfo.emails);
    remove(emailsCopy, (_email, i) => i === callbackData.index);
    this.emailLoading = true;
    try {
      await this.httpService.request(
        `/ajax/license-auth/${this.account.namespace}/notify/emails`,
        {
          method: 'PUT',
          body: {
            emails: emailsCopy,
          },
        },
      );
      remove(this.licenseInfo.emails, (_email, i) => i === callbackData.index);
      this.emailLoading = false;
    } catch ({ errors }) {
      this.emailLoading = false;
      if (errors) {
        this.auiNotificationService.error({
          content: this.translateService.get(errors[0].message),
        });
      }
    }
  }

  async confirmEmail(callbackData: EmailsCallbackOptions) {
    const emailsCopy = cloneDeep(this.licenseInfo.emails);
    emailsCopy[callbackData.index] = callbackData.email;

    this.emailLoading = true;
    try {
      await this.httpService.request(
        `/ajax/license-auth/${this.account.namespace}/notify/emails`,
        {
          method: 'PUT',
          body: {
            emails: emailsCopy,
          },
        },
      );
      this.licenseInfo.emails[callbackData.index] = callbackData.email;
      callbackData.callback();
    } catch ({ errors }) {
      if (errors) {
        this.auiNotificationService.error({
          content: this.translateService.get(errors[0].message),
        });
      }
    } finally {
      this.emailLoading = false;
    }
  }

  async changeRemindType(val: any) {
    const _originType = this.remindType;
    try {
      await this.httpService.request(
        `/ajax/license-auth/${this.account.namespace}/notify/target`,
        {
          method: 'PUT',
          body: {
            notify_target: val,
          },
        },
      );
      this.store.dispatch(
        new licenseActions.UpdateByKeys({ notify_target: val }),
      );
    } catch ({ errors }) {
      if (errors) {
        this.auiNotificationService.error({
          content: this.translateService.get(errors[0].message),
        });
      }
      this.remindType = _originType;
    }
  }

  async updateLicenseCode(textarea: HTMLInputElement) {
    if (this.licenseEditState === 'done') {
      textarea.select();
      this.licenseEditState = 'edit';
      return textarea.select();
    }
    if (!this.form.valid) {
      return;
    }
    if (this.licenseCode.trim() === this.licenseInfo['license_code']) {
      return (this.licenseEditState = 'done');
    }
    this.submitting = true;

    try {
      const licenseInfo = await this.httpService.request<LicenseInfoModal>(
        '/ajax/landing/license-activation',
        {
          method: 'POST',
          body: {
            license_code: this.licenseCode,
          },
        },
      );

      this.auiMessageService.success({
        content: this.translateService.get('license_has_activated'),
      });

      this.store.dispatch(new licenseActions.UpdateByKeys(licenseInfo));

      this.licenseEditState = 'done';
      this.licenseCode = this.licenseInfo['license_code'] =
        licenseInfo['license_code'];
      this.licenseInfo.expire_at = licenseInfo.expire_at;
      this.licenseInfo.enterprise_name = licenseInfo.enterprise_name;
      this.licenseInfo.days_until_expiration =
        licenseInfo.days_until_expiration;
      this.licenseInfo.kind = licenseInfo.kind;
      this.expiredDayClass = findKey(
        this.expireDayRuler[licenseInfo.kind],
        (rulerFun: Function) => rulerFun(licenseInfo.days_until_expiration),
      );
    } catch ({ errors }) {
      if (errors) {
        this.textareaWarning = true;
        const errMsg =
          this.translateService.get(`license_${errors[0].code}_msg`) ===
          `license_${errors[0].code}_msg`
            ? errors[0].message
            : `license_${errors[0].code}_msg`;
        this.auiNotificationService.error({
          title: this.translateService.get(`license_${errors[0].code}`),
          content: this.translateService.get(errMsg),
        });
      }
    } finally {
      this.submitting = false;
    }
  }

  cancleEditLiceseCode() {
    this.licenseEditState = 'done';
    this.textareaWarning = false;
    this.licenseCode = this.licenseInfo['license_code'];
  }
}
