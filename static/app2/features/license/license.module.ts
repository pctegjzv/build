import { NgModule } from '@angular/core';

import { SharedModule } from 'app2/shared/shared.module';

import { LicenseEmailInputComponent } from './license-email-input/license-email-input.component';
import { LicenseRoutingModule } from './license-routing.module';
import { LicenseComponent } from './license.component';

@NgModule({
  imports: [LicenseRoutingModule, SharedModule],
  declarations: [LicenseComponent, LicenseEmailInputComponent],
})
export class LicenseModule {}
