export interface EmailsCallbackOptions {
  index: number;
  callback: Function;
  email: string;
}

export interface LicenseInfoModal {
  product_name?: string;
  enterprise_name?: string;
  kind?: 'beta' | 'release';
  license_code?: string;
  expire_at?: Date;
  days_until_expiration?: number;
  emails?: string[];
  notify_target?: 1 | 2;
}
