import { Component, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { ACCOUNT, WEBLABS } from 'app2/core/tokens';
import { RcAccount, Weblabs } from 'app2/core/types';
import {
  SerieChartData,
  VerticalBarChartConfig,
} from 'app2/features/dashboard/charts/ngx-charts.types';
import { IntegrationService } from 'app2/shared/services/features/integration.service';
import {
  LogChartParam,
  LogService,
  QueryCondition,
  QueryConditionService,
  Types,
} from 'app2/shared/services/features/log.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { LoggerUtilitiesService } from 'app2/shared/services/utility/logger.service';
import { TranslateService } from 'app2/translate/translate.service';
import * as _ from 'lodash';
import moment, { Moment } from 'moment';
import { first } from 'rxjs/operators';

import { TIME_STAMP_OPTIONS } from '../log-constant';
import { LogQueryUpdateDialogComponent } from '../query-condition-update/query-condition-update.component';

@Component({
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  @Input()
  service: any;
  @Input()
  source: any;
  @ViewChild('manualTooltip')
  manualTooltip: any;
  loading = false;
  initialized = false;
  loadingTypes = false;
  timeRangeOutOfDate = false;
  hasQueryString = false;
  loadingLogs = false;
  loadingSeries = false;
  showChart = false;
  logTypes: Types;
  conditions: Array<any> = [];
  filteredConditions: Array<any> = [];
  labeledConditions: Array<any> = [];
  queryConditionUUID: string;
  activeCondition: QueryCondition;
  tags: Array<any> = [];
  tagRegex = /\w+: .+/;
  selectedDataSource: any;
  tagsTypes = ['search', 'instance', 'service', 'node', 'path', 'cluster'];
  _bucketsInterval = 0; // interval of two buckets
  /*const _timestampMap = TIME_STAMP_OPTIONS.map((option) => ({ [option.type]: option.offset }));*/

  suggesstions: Array<any>;
  trackFn = (val: any) => {
    if (val.uuid) {
      return val.uuid;
    } else if (val.type) {
      return `${val.type}: ${val.name}`;
    } else if (this.tagRegex.test(val)) {
      return val;
    } else {
      return `search: ${val}`;
    }
  };
  filterFn = (filter: string, option: any) =>
    (option.value.name || option.value).includes(filter);
  tagsInputSearchHint = this.translateService.get('logs_dashboard_search_hint');
  logSearchHint = this.translateService.get('logs_search_hint');
  tagsInputPlaceholder = this.translateService.get(
    'logs_dashboard_tags_input_placeholder',
  );
  current_time = moment();
  start_time: Moment;
  logs: Array<any> = [];
  logDataSources: Array<any> = [];
  logDataSource: any;
  logCache = {};
  logCollapseMap = {};
  timestampOptions = _.map(TIME_STAMP_OPTIONS, option =>
    _.merge(option, { name: this.translateService.get(option.type) }),
  );
  timestampOption = 'last_30_minites';
  queryDates: {
    start_time: number;
    end_time: number;
  };
  queryDatesShown: {
    start_time: string;
    end_time: string;
  };
  dateTimeOptions: {
    maxDate: number;
    minDate: number;
    enableTime: true;
  };
  pagination = {
    total_items: 0,
    size: 50,
  };
  currentPage = 1;
  logCount: string;
  logFilterCreateEnabled: boolean;
  chartData: SerieChartData[];
  barChartData: any;
  chartConfig: VerticalBarChartConfig = {
    scheme: { domain: ['#8BD6FA'] },
    xAxis: true,
    yAxis: true,
    showGridLines: true,
    roundDomains: false,
    roundEdges: false,
    barPadding: 8,
    xAxisTickFormatting: (val: Date) => {
      return moment(val).format('YYYY-MM-DD HH:mm');
    },
    yAxisTickFormatting: (val: number) => {
      if (val >= 1000) {
        return val / 1000 + 'k';
      } else {
        return val;
      }
    },
  };
  constructor(
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(WEBLABS) public weblabs: Weblabs,
    private route: ActivatedRoute,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private applicationService: ServiceService,
    private logService: LogService,
    private modalService: ModalService,
    private integrationService: IntegrationService,
    private roleUtilities: RoleUtilitiesService,
    private logger: LoggerUtilitiesService,
    private router: Router,
  ) {}

  async ngOnInit() {
    this.start_time = this.current_time
      .clone()
      .startOf('day')
      .subtract(6, 'days');
    this.queryDates = {
      start_time: this.start_time.valueOf(),
      end_time: this.current_time.valueOf(),
    };
    this.queryDatesShown = {
      start_time: this.logService.dateNumToStr(this.queryDates.start_time),
      end_time: this.logService.dateNumToStr(this.queryDates.end_time),
    };
    this.dateTimeOptions = {
      maxDate: this.current_time
        .clone()
        .endOf('day')
        .valueOf(),
      minDate: this.start_time.valueOf(),
      enableTime: true,
    };
    this.logFilterCreateEnabled = await this.roleUtilities.resourceTypeSupportPermissions(
      'log_filter',
    );
    this.route.params.pipe(first()).subscribe(async params => {
      this.queryConditionUUID = params['uuid'];
      await Promise.all([
        this.initLogDataSources(),
        this.initQueryConditions(),
      ]);
      this.search();
      this.initialized = true;
    });
  }

  private async initLogDataSources() {
    try {
      this.logDataSources = (await this.integrationService.getIntegrations({
        families: 'LogSource',
        page: 1,
        page_size: 200,
      })).filter((item: any) => item.enabled && !!item.fields.query_address);
    } catch (err) {
      this.logDataSources = [];
    }
    this.logDataSources.unshift({ id: 'default', name: 'default' });
    this.logDataSource = this.logDataSources[0].name;
    await this.initSuggestions(this.logDataSources[0].name);
  }

  private async initSuggestions(logSource = '') {
    this.loadingTypes = true;
    try {
      this.logTypes = await this.logService.getTypes(logSource);
    } catch (e) {
      this.logger.error('Cant Load Log Types URL');
    }
    this.suggesstions = _.chain(this.logTypes)
      .mapValues((items: Array<QueryConditionService>, type: string) => {
        if (type === 'services') {
          return items.map(item => ({
            type: 'service',
            name: item.name,
            uuid: item.uuid,
          }));
        }
        return items.map(item => ({
          type: type.slice(0, -1),
          name: item,
        }));
      })
      .values()
      .flattenDeep()
      .uniq() // TODO: temporarily solution when duplicated_resource_names is true
      .value();
    this.loadingTypes = false;
  }

  private async initQueryConditions() {
    const { result } = await this.logService.getLogQueryConditions({
      all: true,
    });
    this.conditions = result;
    this.listFilter('');
    this.labeledConditions = this.conditions.slice(0, 5);
    if (!this.initialized && this.queryConditionUUID) {
      for (let i = 0; i < result.length; i++) {
        if (result[i].uuid === this.queryConditionUUID) {
          this.activeCondition = result[i];
        }
      }
      this.onQueryConditionSelect(this.activeCondition, false);
    }
  }

  private getQueryConditionPayload() {
    const [
      conditions,
      range,
    ] = this.logService.generateQueryConditionFromTagsAndTimeRange({
      tags: this.tags,
      timeRangeType: this.timestampOption,
      timeRange: this.queryDates,
    });
    const payload = Object.assign(
      {},
      {
        query_conditions: conditions,
      },
      range,
    );
    return payload;
  }

  conditionSave() {
    if (!this.activeCondition) {
      this.conditionSaveAs();
    } else {
      if (!this.checkQueryDates()) {
        return;
      }
      const payload = this.getQueryConditionPayload();
      const modelRef = this.modalService.open(LogQueryUpdateDialogComponent, {
        title: this.translateService.get('save'),
        width: 400,
      });

      modelRef.componentInstance.setModel(this.activeCondition, payload);
      modelRef.componentInstance.finished.pipe(first()).subscribe(() => {
        this.initQueryConditions();
        modelRef.close();
      });
    }
  }

  conditionSaveAs() {
    if (!this.checkQueryDates()) {
      return;
    }
    const payload = this.getQueryConditionPayload();
    const modelRef = this.modalService.open(LogQueryUpdateDialogComponent, {
      title: this.translateService.get('save_as'),
      width: 400,
    });

    modelRef.componentInstance.setModel(
      {
        space_name: '',
        name: '',
        display_name: '',
        uuid: '',
        end_time: 0,
      },
      payload,
    );
    modelRef.componentInstance.finished.pipe(first()).subscribe(() => {
      this.initQueryConditions();
      modelRef.close();
    });
  }

  onLogDataSourceChanged(source: any) {
    this.selectedDataSource = source;
    if (this.initialized) {
      this.initSuggestions(source.name);
    }
  }

  private checkQueryDates() {
    if (!this.queryDates.start_time || !this.queryDates.end_time) {
      this.auiNotificationService.warning(
        this.translateService.get('log_query_timerange_required'),
      );
      return false;
    }
    if (
      this.timestampOption === 'custom_time_range' &&
      this.queryDates.start_time >= this.queryDates.end_time
    ) {
      this.auiNotificationService.warning(
        this.translateService.get('log_query_timerange_warning'),
      );
      return false;
    }
    return true;
  }

  conditionManage() {
    this.router.navigateByUrl('log/query_list');
  }

  shouldDisalbeSaveButton() {
    return (
      (!this.activeCondition && !this.logFilterCreateEnabled) ||
      (this.activeCondition &&
        !this.roleUtilities.resourceHasPermission(
          this.activeCondition,
          'log_filter',
          'update',
        ))
    );
  }

  onQueryConditionSelect(option: any, toggleFlag?: boolean) {
    this.activeCondition = option;
    this.logService.updateLogConditionUsedTime(option.uuid);
    const {
      tags,
      timeRangeType,
      timeRange,
    } = this.logService.parseQueryConditionToTagsAndTimeRange(option);
    this.tags = tags;
    this.timestampOption = timeRangeType;
    if (timeRangeType === 'custom_time_range') {
      // validate custom time range
      if (moment(timeRange.start_time).isBefore(this.dateTimeOptions.minDate)) {
        this.timeRangeOutOfDate = true;
        timeRange.start_time = null;
      }
      if (moment(timeRange.end_time).isBefore(this.dateTimeOptions.minDate)) {
        this.timeRangeOutOfDate = true;
        timeRange.end_time = null;
      }
      this.queryDates.start_time = timeRange.start_time;
      this.queryDates.end_time = timeRange.end_time;
      this.fillCalendar(timeRange.start_time, timeRange.end_time);
    } else {
      this.resetTimeRange();
    }
    if (toggleFlag === true) {
      this.manualTooltip.toggleTooltip();
    }
  }

  isConditionItemActive(item: any) {
    return this.activeCondition && item.uuid === this.activeCondition.uuid;
  }

  listFilter(keyword: string) {
    const conditions = this.conditions.slice(5, this.conditions.length);
    this.filteredConditions = conditions.filter((el: any) => {
      return el.display_name.includes(keyword);
    });
  }

  search(pageno = 1, size = 50) {
    this.currentPage = pageno;
    if (this.timestampOption !== 'custom_time_range') {
      this.resetTimeRange();
    }
    if (!this.checkQueryDates()) {
      return;
    }
    this.timeRangeOutOfDate = false;

    let payload = _.merge(
      { pageno, size },
      this.queryDates,
      this.logService.generateQuerysFromTagObjects(this.tags),
      {
        read_log_source_uuid: this.selectedDataSource.id,
      },
    );

    payload = this.paramsCompletion(payload);

    this.getLogs(payload);
    this.getLogsEvents(payload);
  }

  showContext(log: any) {
    return (
      this.hasQueryString &&
      log.paths &&
      log.service_name &&
      (log.instance_id_full || log.instance_id)
    );
  }

  onTimestampChanged(option: any) {
    this.timeRangeOutOfDate = false;
    this.timestampOption = option.type;
    if (this.timestampOption !== 'custom_time_range') {
      this.resetTimeRange();
    }
  }

  addKeywordByClick(type: string, value: string) {
    if (
      !this.tags.find((element: any) => {
        return element.name === value;
      })
    ) {
      this.tags = this.tags.concat([
        {
          type: type,
          name: value,
        },
      ]);
    }
  }

  tagClassFn(_label: string, tag: any) {
    // init rc-tags-input tagClass
    if (tag.type) {
      return `tag-${tag.type}`;
    } else if (typeof tag === 'string' && /\w+: .+/.test(tag)) {
      return `tag-${/(\w+): .+/.exec(tag)[1]}`;
    }
  }

  logInsight(log = { time: '' }, mode = '') {
    if (!mode) {
      this.logCollapseMap[log.time] = !this.logCollapseMap[log.time];
      if (this.logCache[log.time]) {
        return;
      }
    }

    const payload = this.createContextPayload(log, mode);
    this.loadingLogs = true;
    this.applicationService
      .serviceLogContext(payload)
      .then(data => {
        if (!_.isEmpty(data.logs)) {
          this.showContextByMode(log, data.logs, mode);
        } else {
          this.auiNotificationService.warning(
            this.translateService.get('no_more_logs'),
          );
        }
      })
      .catch(() => {})
      .then(() => {
        this.loadingLogs = false;
      });
  }

  private getLogs(payload: any) {
    this.loadingLogs = true;
    return this.logService
      .logsSearch(payload)
      .then(data => {
        if (!data.logs) {
          return;
        }
        this.pagination.total_items =
          data.total_items > 10000 ? 10000 : data.total_items;
        this.logCache = {};
        this.logs = data.logs;
      })
      .catch((e: any) => {
        this.logger.error('Can not load logs', e);
        // clear previously loaded logs if server error exists
        this.logs = [];
      })
      .then(() => {
        this.loadingLogs = false;
      });
  }

  private getLogsEvents(payload: any) {
    this.loadingSeries = true;
    this.showChart = false;
    return this.logService
      .getAggregations(payload)
      .then(data => {
        if (!_.get(data, 'buckets.length')) {
          // the buckets length can't equal 0 except time offset is 0
          return;
        }
        this.showChart = true;
        const buckets = data.buckets;
        buckets.forEach((bucket: any) => {
          const _date = new Date(bucket.time * 1000);
          bucket.time = _date.getTime();
        });
        if (buckets.length > 2) {
          this._bucketsInterval = buckets[1].time - buckets[0].time;
        }
        const totalCount = buckets.reduce(
          (result: any, bucket: any) => result + bucket.count,
          0,
        );
        this.renderLogEventsChart({ totalCount, buckets });
      })
      .catch((e: any) => {
        this.logger.error('Can not load log aggregation', e);
        this.chartData = [];
        this.pagination.total_items = 0;
      })
      .then(() => {
        this.loadingSeries = false;
      });
  }

  private paramsCompletion(payload: any) {
    // 补充 paths
    if (!payload.paths) {
      payload.paths = this.tags.filter((tag: any) => tag.type === 'path');
    }
    // 补充 query_string
    this.hasQueryString = false;
    if (payload.search) {
      this.hasQueryString = true;
      payload.query_string = payload.search;
      delete payload.search;
    }
    payload.start_time /= 1000;
    payload.end_time /= 1000;
    return payload;
  }

  private resetTimeRange() {
    this.queryDates = this.logService.getTimeRangeByRangeType(
      this.timestampOption,
    );
    this.fillCalendar(this.queryDates.start_time, this.queryDates.end_time);
  }

  fillCalendar(start_time: number, end_time: number) {
    this.queryDatesShown.start_time = this.logService.dateNumToStr(start_time);
    this.queryDatesShown.end_time = this.logService.dateNumToStr(end_time);
  }

  onStartTimeSelect(event: any) {
    this.queryDates.start_time = this.logService.dateStrToNum(event);
  }

  onEndTimeSelect(event: any) {
    this.queryDates.end_time = this.logService.dateStrToNum(event);
  }

  private renderLogEventsChart({
    totalCount = 0,
    buckets = [],
  }: LogChartParam) {
    this.logCount = totalCount + ' Logs';
    this.chartData = buckets.map(bucket => ({
      name: bucket.time,
      // name: moment(bucket.time).format('YYYY-MM-DD HH:mm:ss'),
      // name: new Date(bucket.time),
      value: bucket.count,
    }));
    this.barChartData = this.chartData;
  }

  onChartSelect(event: any) {
    this.timestampOption = 'custom_time_range';
    // this.queryDates.start_time = moment(
    //   event.name,
    //   'YYYY-MM-DD HH:mm:ss',
    // ).valueOf();
    this.queryDates.start_time = event.name;
    this.queryDates.end_time =
      this.queryDates.start_time + this._bucketsInterval;
    this.fillCalendar(this.queryDates.start_time, this.queryDates.end_time);
    this.search();
  }

  getTip(val: Date) {
    return moment(val).format('YYYY-MM-DD HH:mm:ss');
  }

  private createContextPayload(log: any = {}, mode = '') {
    const match = log.service_name.match(/(.+)\.(.+)/);
    let service_name, application;
    if (match) {
      application = match[1];
      service_name = match[2];
    }
    const params = {
      log_time: log.time,
      log_source: log.paths,
      service_name: service_name || log.service_name,
      instance_id: log.instance_id_full || log.instance_id,
    };
    if (application) {
      params['application'] = application;
    }
    if (mode === 'before') {
      params['direction'] = 'before';
      // start time is time of first log
      params['log_time'] = this.logCache[log.time]['before'][0]['time'];
    } else if (mode === 'after') {
      params['direction'] = 'later';
      // start time is time of last log
      const len = this.logCache[log.time]['after'].length;
      params['log_time'] = this.logCache[log.time]['after'][len - 1]['time'];
    }
    return params;
  }

  private showContextByMode(log: any, logs: Array<any>, mode = '') {
    const index = _.findIndex(logs, item => {
      return item.time === log.time;
    });
    switch (mode) {
      case '':
        this.logCache[log.time] = {
          before: _.slice(logs, 0, index),
          after: _.slice(logs, index + 1),
        };
        break;
      case 'before':
        // insert before
        this.logCache[log.time]['before'] = logs.concat(
          this.logCache[log.time]['before'],
          logs,
        );
        break;
      case 'after':
        // insert after
        this.logCache[log.time]['after'] = this.logCache[log.time][
          'after'
        ].concat(logs);
    }
  }
}
