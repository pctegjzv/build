import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { LogQueryListComponent } from './query_condition/list.component';

const logRoutes: Routes = [
  {
    path: '',
    component: DashboardComponent,
  },
  {
    path: 'saved_query/:uuid',
    component: DashboardComponent,
  },
  {
    path: 'query_list',
    component: LogQueryListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(logRoutes)],
  exports: [RouterModule],
  declarations: [],
})
export class LogRoutingModule {}
