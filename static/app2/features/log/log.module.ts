import { NgModule } from '@angular/core';
import { SharedModule } from 'app2/shared/shared.module';

import { DashboardComponent } from './dashboard/dashboard.component';
import { LogRoutingModule } from './log-routing.module';
import { LogQueryUpdateDialogComponent } from './query-condition-update/query-condition-update.component';
import { LogQueryListComponent } from './query_condition/list.component';

@NgModule({
  imports: [SharedModule, LogRoutingModule],
  declarations: [
    DashboardComponent,
    LogQueryUpdateDialogComponent,
    LogQueryListComponent,
  ],
  entryComponents: [DashboardComponent, LogQueryUpdateDialogComponent],
})
export class LogModule {}
