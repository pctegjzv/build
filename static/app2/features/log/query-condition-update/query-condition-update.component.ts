import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ACCOUNT, WEBLABS } from 'app2/core/tokens';
import { RcAccount, Weblabs } from 'app2/core/types';
import { SingleSelectionDropdownComponent } from 'app2/shared/components/select/single-selection-dropdown.component';
import {
  LogQueryCondition,
  LogService,
} from 'app2/shared/services/features/log.service';
import {
  QuotaSpacesResult,
  ResourceService,
} from 'app2/shared/services/features/resource.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import * as _ from 'lodash';

import { RESOURCE_NAME_BASE } from '../../../../app/components/common/config/common-pattern';

@Component({
  selector: 'rc-log-query-save-dialog',
  templateUrl: './query-condition-update.component.html',
})
export class LogQueryUpdateDialogComponent implements OnInit {
  @Output()
  finished = new EventEmitter<any>();
  @ViewChild(NgForm)
  queryUpdateForm: NgForm;
  @ViewChild(SingleSelectionDropdownComponent)
  singleDropdown: SingleSelectionDropdownComponent;

  submitting = false;
  loading = false;
  create_alarm = false;
  quotaEnabled: boolean;
  disableAlarmCreate: boolean;
  payload: {
    end_time: number;
  } = {
    end_time: 0,
  };
  resourceNameBaseRegExp: any;
  model: LogQueryCondition = {
    display_name: '',
    end_time: 0,
    name: '',
    namespace: '',
    query_conditions: {
      cluster: '',
      nodes: '',
      services: '',
      paths: '',
      query_string: '',
      instances: '',
    },
    space_name: '',
    start_time: 0,
    uuid: '',
  };
  isItemInactive = true;
  activeItem: {
    space_name: string;
    name: string;
    display_name: string;
    uuid?: string;
  };
  spaces: QuotaSpacesResult;

  constructor(
    @Inject(ACCOUNT) private account: RcAccount,
    @Inject(WEBLABS) public weblabs: Weblabs,
    private resourceService: ResourceService,
    private translateService: TranslateService,
    private logService: LogService,
    private errorsToastService: ErrorsToastService,
    private router: Router,
  ) {}

  async ngOnInit() {
    this.loading = true;
    this.quotaEnabled = this.weblabs.QUOTA_ENABLED;
    this.disableAlarmCreate = this.payload.end_time !== 0;
    this.isItemInactive = this.activeItem && this.activeItem.name === '';

    this.resourceNameBaseRegExp = RESOURCE_NAME_BASE;
    if (!this.isItemInactive) {
      this.model.space_name = this.activeItem.space_name;
      this.model.name = this.activeItem.name;
      this.model.display_name = this.activeItem.display_name;
    }
    if (this.quotaEnabled && this.isItemInactive) {
      this.spaces = await this.resourceService.getConsumableSpaces();
    }
    this.loading = false;
    if (this.spaces) {
      setTimeout(() => {
        this.singleDropdown.setSelectedValue(this.spaces[0].name);
      }, 100);
    }
  }

  onSpaceChanged(option: any) {
    this.model.space_name = option.name;
  }

  async submit() {
    if (this.queryUpdateForm.valid && !this.submitting) {
      this.submitting = true;
      if (this.quotaEnabled && !this.model.space_name) {
        return false;
      }
      this.model = Object.assign(this.model, this.payload);
      if (!this.model.display_name) {
        this.model.display_name = this.model.name;
      }
      this.submitting = true;
      try {
        let res;
        if (!this.isItemInactive) {
          res = await this.logService.updateLogQueryCondition(
            this.activeItem.uuid,
            this.model,
          );
        } else {
          res = await this.logService.createLogQueryCondition(this.model);
        }
        this.complete(res);
        if (this.create_alarm) {
          this.router.navigateByUrl(`alarm/log_alarm_create?quuid=${res.uuid}`);
        }
      } catch (err) {
        this.errorsToastService.handleGenericAjaxError({
          errors: err.errors,
          handleNonGenericCodes: true,
          fallbackMessage: this.translateService.get(
            'query_condition_update_error',
          ),
        });
      }
      this.submitting = false;
    }
  }

  get namespace() {
    return this.account.namespace;
  }

  setModel(activeItem: any, payload: any) {
    this.activeItem = _.cloneDeep(activeItem);
    this.payload = _.cloneDeep(payload);
  }

  cancel() {
    this.complete();
  }

  private complete(res?: any) {
    this.finished.next(res);
    this.finished.complete();
  }
}
