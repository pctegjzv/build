import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { Router } from '@angular/router';
import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';
import {
  // ResourceService,
  // Space,
  // QuotaConfigMaped,
  // QuotaTypes,
  LogService,
} from 'app2/shared/services/features/log.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { LoggerUtilitiesService } from 'app2/shared/services/utility/logger.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-log-query-condition-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class LogQueryListComponent implements OnInit {
  @Input()
  isNew = false;
  @Output()
  finished = new EventEmitter<any>();
  @ViewChild(NgForm)
  queryUpdateForm: NgForm;

  initialized = false;
  loading = false;
  queryString = '';
  conditions: Array<any> = [];
  pagination = {
    page_size: 20,
    page: 1,
    count: 0,
  };
  deletingItems: any = {};
  hasCreateLogAlarmPermission = false;

  constructor(
    @Inject(WEBLABS) public weblabs: Weblabs,
    private logger: LoggerUtilitiesService,
    private translate: TranslateService,
    private logService: LogService,
    private router: Router,
    private roleUtilities: RoleUtilitiesService,
    private modalService: ModalService,
  ) {}

  async ngOnInit() {
    await this.initQueryConditions();
    this.hasCreateLogAlarmPermission = await this.roleUtilities.resourceTypeSupportPermissions(
      'log_alarm',
    );
    this.initialized = true;
  }

  async initQueryConditions() {
    this.loading = true;
    const data = await this.logService.getLogQueryConditions({
      page: this.pagination.page,
      page_size: this.pagination.page_size,
      display_name: this.queryString,
    });
    this.conditions = this.formatQueryConditionsDisplay(data.results);
    this.conditions.forEach((item: any) => {
      item.canDelete = this.buttonDisplayExpr(item, 'delete');
    });
    this.pagination.count = data.count;
    this.loading = false;
  }

  private formatQueryConditionsDisplay(conditions: any) {
    return conditions.map((item: any) => {
      item.timeRangeDisplay = this.logService.getQueryConditionTimeRangeDisplay(
        item,
        ' ',
      );
      item.conditionsDisplay = this.logService.getQueryConditionValuesDisplay(
        item,
      );
      return item;
    });
  }

  redirectToLogDashboard(item: any) {
    this.logService.updateLogConditionUsedTime(item.uuid);
    this.router.navigateByUrl(`log/saved_query/${item.uuid}`);
  }

  async actionDelete(item: any) {
    try {
      await this.modalService.confirm({
        title: this.translate.get('query_condition_delete_title'),
        content: this.translate.get('query_condition_delete_condent', {
          name: item.name,
        }),
      });
      await this.actionDeleteAction(item.uuid);
    } catch (e) {
      this.logger.error('query condition delete error: ', e);
    }
  }

  actionAlarmCreate(item: any) {
    this.router.navigateByUrl(`alarm/log_alarm_create?quuid=${item.uuid}`);
  }

  private async actionDeleteAction(uuid: string) {
    try {
      this.deletingItems[uuid] = true;
      await this.logService.deleteLogQueryCondition(uuid);
      this.removeItem(uuid);
    } catch (e) {
      this.logger.error('query condition delete error: ', e);
    }
    this.deletingItems[uuid] = false;
  }

  private removeItem(uuid: string) {
    delete this.deletingItems[uuid];
    this.conditions = this.conditions.filter(item => {
      return item.uuid !== uuid;
    });
    if (!this.conditions.length && this.pagination.page > 1) {
      this.pagination.page--;
      this.initQueryConditions();
    }
  }

  shouldDisableDeleteButton(uuid: string) {
    if (!this.deletingItems) {
      return false;
    }
    return !!this.deletingItems[uuid];
  }

  shouldDisableAlarmCreateButton(item: any) {
    return item.end_time !== 0;
  }

  getAlarmCreateTooltip(item: any) {
    if (item.end_time === 0) {
      return this.translate.get('alarm_create');
    } else {
      return this.translate.get('log_query_condition_alarm_create_disable_tip');
    }
  }

  search(value: string) {
    this.queryString = value;
    this.initQueryConditions();
  }

  pageNoChange(page: number) {
    this.pagination.page = page;
    this.initQueryConditions();
  }

  buttonDisplayExpr(item: any, action: any) {
    return this.roleUtilities.resourceHasPermission(item, 'log_filter', action);
  }
}
