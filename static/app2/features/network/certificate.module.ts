import { NgModule } from '@angular/core';

import { CertificateRoutingModule } from 'app2/features/network/certificate.routing.module';
import { CertificateCreateComponent } from 'app2/features/network/certificate/certificate-create.component';
import { CertificateListComponent } from 'app2/features/network/certificate/certificate-list.component';
import { SharedModule } from 'app2/shared/shared.module';

@NgModule({
  imports: [SharedModule, CertificateRoutingModule],
  declarations: [CertificateListComponent, CertificateCreateComponent],
  entryComponents: [],
})
export class CertificateModule {}
