import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CertificateCreateComponent } from 'app2/features/network/certificate/certificate-create.component';
import { CertificateListComponent } from 'app2/features/network/certificate/certificate-list.component';

const routes: Routes = [
  {
    path: '',
    component: CertificateListComponent,
    pathMatch: 'full',
  },
  {
    path: 'create',
    component: CertificateCreateComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class CertificateRoutingModule {}
