import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { NetworkService } from 'app2/shared/services/features/network.service';
import { QuotaSpaceService } from 'app2/shared/services/features/quota-space.service';
@Component({
  templateUrl: './certificate-create.component.html',
  styleUrls: [
    './certificate-create.component.scss',
    './certificate-list.component.scss',
  ],
})
export class CertificateCreateComponent implements OnInit {
  @ViewChild('createForm')
  form: NgForm;
  model: any;
  spaces: any[];
  submitting = false;

  constructor(
    private quotaSpaceService: QuotaSpaceService,
    private networkService: NetworkService,
    private auiNotificationService: NotificationService,
    private router: Router,
  ) {
    this.model = {
      name: '',
      space_name: '',
      description: '',
      public_cert: '',
      private_key: '',
    };
  }

  ngOnInit() {
    this.quotaSpaceService.getConsumableSpaces().then(sp => {
      this.spaces = sp;
    });
  }

  back() {
    this.router.navigateByUrl('certificate');
  }

  async submit() {
    if (this.form.valid && !this.submitting) {
      this.networkService
        .createCertificate(this.model)
        .then(() => {
          this.back();
        })
        .catch(({ errors }) => {
          if (errors) {
            this.auiNotificationService.error(errors[0].message);
          }
        });
    }
  }
}
