import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { publishReplay, refCount } from 'rxjs/operators';

import {
  AluCertificate,
  NetworkService,
} from 'app2/shared/services/features/network.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import {
  Pagination,
  PaginationDataWrapper,
} from 'app2/shared/services/utility/pagination-data';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './certificate-list.component.html',
  styleUrls: ['./certificate-list.component.scss'],
})
export class CertificateListComponent implements OnInit {
  paginationWapper: PaginationDataWrapper<AluCertificate>;
  result$: Observable<Pagination<AluCertificate>>;

  constructor(
    private networkService: NetworkService,
    private modalService: ModalService,
    private errorsToast: ErrorsToastService,
    private translateService: TranslateService,
  ) {
    const fetchData = (pageNo: number, pageSize: number, params: any) => {
      return this.networkService.getPageCertificates<AluCertificate>({
        page: pageNo,
        page_size: pageSize,
        ...params,
      });
    };

    this.paginationWapper = new PaginationDataWrapper(fetchData);
    this.result$ = this.paginationWapper.pagination.pipe(
      publishReplay(1),
      refCount(),
    );
  }

  get loading() {
    return this.paginationWapper.loading;
  }

  async delete({ name, uuid }: AluCertificate) {
    try {
      await this.modalService.confirm({
        title: this.translateService.get('delete'),
        content: this.translateService.get('delete_certificate_confirm', {
          name,
        }),
      });
      await this.networkService.deleteCertificate(uuid);
    } catch (e) {
      return this.errorsToast.error(e);
    }

    this.paginationWapper.refetch();
  }

  ngOnInit() {
    this.paginationWapper.refetch();
  }
}
