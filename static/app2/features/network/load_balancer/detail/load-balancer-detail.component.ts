import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { uniq } from 'lodash';
import { Subscription } from 'rxjs';
import { first, switchMap } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { LoadBalancerDomainSuffixUpdateComponent } from 'app2/features/network/load_balancer/detail/load-balancer-domain-suffix-update.component';
import { LoadBalancerPortAddRuleComponent } from 'app2/features/network/load_balancer/port/load-balancer-port-add-rule.component';
import { LoadBalancerPortAddServiceComponent } from 'app2/features/network/load_balancer/port/load-balancer-port-add-service.component';
import { TableComponent } from 'app2/shared/components/table/table.component';
import {
  FrontendDetail,
  LB_TYPE,
  LoadBalancer,
  NetworkService,
  Service,
} from 'app2/shared/services/features/network.service';
import { NetworkUtilitiesService } from 'app2/shared/services/features/network.utilities.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ModalMode } from 'app2/shared/services/modal/modal.types';
import {
  Pagination,
  PaginationDataWrapper,
} from 'app2/shared/services/utility/pagination-data';
import { TranslateService } from 'app2/translate/translate.service';

interface FrontendService extends FrontendDetail {
  services?: Service[];
}

@Component({
  templateUrl: './load-balancer-detail.component.html',
  styleUrls: ['./load-balancer-detail.component.scss'],
  providers: [],
})
export class LoadBalancerDetailComponent implements OnInit, OnDestroy {
  @ViewChild('lbPortTable')
  table: TableComponent;
  loadBalancerDetail: LoadBalancer;
  paginationDataWrapper: PaginationDataWrapper<any>;
  paginationDataSubscription: Subscription;
  loadBalancerDataSubscription: Subscription;

  customDomainsSuffix: any[] = [];
  customDomainsSuffixString: string;
  defaultDomainSuffix = '';

  tableSelectIndex = 1;
  pageSize = 20;
  count = 0;
  frontends: FrontendDetail[] = [];
  showMore = false;
  basicLoading = false;

  constructor(
    private networkService: NetworkService,
    private route: ActivatedRoute,
    private router: Router,
    private util: NetworkUtilitiesService,
    private modalService: ModalService,
    private auiNotificationService: NotificationService,
    private regionService: RegionService,
    private translateService: TranslateService,
  ) {}

  ngOnInit(): void {
    const fetchFrontends = (pageNo: number, pageSize: number, params: any) => {
      return (this.networkService.getFrontends({
        pageNo,
        pageSize,
        params,
      }) as Promise<any>).then(res => {
        return {
          ...res,
          results: res.data,
        };
      });
    };

    this.paginationDataWrapper = new PaginationDataWrapper(
      fetchFrontends,
      this.pageSize,
    );
    this.paginationDataSubscription = this.paginationDataWrapper.pagination.subscribe(
      (paginationData: Pagination<FrontendService>) => {
        if (!paginationData.loadError) {
          this.count = paginationData.count;
          this.pageSize = paginationData.pageSize;
          this.frontends = paginationData.results.map(r => {
            let allServices: Service[] = [];
            if (r.rules) {
              r.rules.forEach((rule: any) => {
                allServices = uniq(allServices.concat(rule.services));
              });
            } else {
              r.rules = [];
            }
            r.services = allServices;
            return r;
          });
        } else {
        }
      },
    );
    this.loadBalancerDataSubscription = this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) => {
          this.basicLoading = true;
          return this.networkService.getDetail({
            uuid: params.get('name'),
            frontend: true,
          });
        }),
      )
      .subscribe(
        (detail: LoadBalancer) => {
          this.basicLoading = false;
          this.regionService.setRegionByName(detail.region_name);
          this.loadBalancerDetail = detail;
          this._formatDomains();
          this.paginationDataWrapper.setParams({
            lb_id: detail.load_balancer_id,
          });
        },
        () => {
          this.router.navigate(['/load_balancer']);
          this.basicLoading = false;
        },
      );
  }

  ngOnDestroy() {
    this.paginationDataSubscription.unsubscribe();
    this.loadBalancerDataSubscription.unsubscribe();
  }

  searchChanged(queryString: string) {
    this.paginationDataWrapper.setParams({
      search: queryString,
    });
  }

  pageChanged(page: number) {
    this.paginationDataWrapper.setPageNo(page);
  }

  viewFrontend(row: FrontendDetail) {
    this.router.navigate([
      '/load_balancer/detail',
      this.loadBalancerDetail.name,
      'port',
      row.port,
    ]);
  }

  deleteFrontend(row: FrontendDetail) {
    this.modalService
      .confirm({
        title: this.translateService.get('delete'),
        content: this.translateService.get(
          'load_balancer_delete_frontend_confirm',
          { port: row.port },
        ),
      })
      .then(() => {
        this.networkService
          .deleteFrontend(this.loadBalancerDetail.load_balancer_id, row.port, {
            protocol: row.protocol,
          })
          .then(() => {
            this.refetchFrontends();
          })
          .catch(({ errors }) => {
            if (errors) {
              this.auiNotificationService.error(errors[0].message);
            }
          });
      });
  }

  addPort() {
    let elb_services;
    if (this.loadBalancerDetail.type === LB_TYPE.ELB) {
      elb_services = this.frontends.map(frontend => ({
        service_name: frontend.service_name,
        uuid: frontend.service_id,
        ports: frontend.container_port,
      }));
    }
    const modalRef = this.modalService.open(LoadBalancerPortAddRuleComponent, {
      title: this.translateService.get('load_balancer_add_ports'),
      mode: ModalMode.RIGHT_SLIDER,
      width: 900,
      data: {
        loadBalancer: this.loadBalancerDetail,
        elb_service:
          elb_services && elb_services.length ? elb_services[0] : undefined,
        type: 'create',
      },
    });

    modalRef.componentInstance.confirmed.subscribe(
      async (isConfirm: boolean) => {
        if (isConfirm) {
          await this.refetchFrontends();
        }
        modalRef.close(isConfirm);
      },
    );
  }

  addService(row: FrontendDetail) {
    const parseRules = row.rules.map(r => {
      return this.util.getPartialIndicators(r.dsl);
    });
    const modalRef = this.modalService.open(
      LoadBalancerPortAddServiceComponent,
      {
        title: this.translateService.get('add_service'),
        mode: ModalMode.RIGHT_SLIDER,
        width: 900,
        data: {
          lbId: this.loadBalancerDetail.load_balancer_id,
          lbName: this.loadBalancerDetail.name,
          protocol: row.protocol,
          port: row.port,
          parsedRules: parseRules,
          ruleList: row.rules,
          type: 'create',
        },
      },
    );

    modalRef.componentInstance.confirmed.subscribe(
      async (isConfirm: boolean) => {
        if (isConfirm) {
          this.refetchFrontends();
        }
        modalRef.close(isConfirm);
      },
    );
  }

  updateDomainSuffix() {
    let defaultDomainDisabled = false;
    if (this.loadBalancerDetail.domain_info) {
      this.loadBalancerDetail.domain_info
        .filter(r => r.type === 'default-domain')
        .map(r => {
          defaultDomainDisabled = r.disabled;
        });
    }
    const updateDomainSuffixModelRef = this.modalService.open(
      LoadBalancerDomainSuffixUpdateComponent,
      {
        title: this.translateService.get('custom_domain_suffix'),
        data: {
          defaultDomainDisabled: defaultDomainDisabled,
          customDomains: this.customDomainsSuffix || [],
          lbName: this.loadBalancerDetail.name,
        },
      },
    );
    updateDomainSuffixModelRef.componentInstance.finished
      .pipe(first())
      .subscribe(res => {
        updateDomainSuffixModelRef.close();
        if (res) {
          this.refetchLB();
        }
      });
  }

  deleteLB() {
    try {
      this.modalService
        .confirm({
          title: this.translateService.get('delete'),
          content: this.translateService.get('load_balancer_delete_confirm', {
            lb_name: this.loadBalancerDetail.name,
          }),
        })
        .then(() => {
          this.networkService
            .delete(this.loadBalancerDetail.name)
            .then(() => {
              this.router.navigate(['/load_balancer']);
            })
            .catch(({ errors }) => {
              if (errors) {
                this.auiNotificationService.error(errors[0].message);
              }
            });
        })
        .catch(() => {});
    } catch (err) {
      throw err;
    }
  }

  buttonDisplay(action: string) {
    return this.networkService.canAction(
      this.loadBalancerDetail,
      action || 'update',
    );
  }

  private _formatDomains() {
    const domain_info = this.loadBalancerDetail.domain_info;
    this.customDomainsSuffix = [];
    if (domain_info && domain_info.length) {
      domain_info.forEach((item: any) => {
        if (item.type === 'default-domain') {
          this.defaultDomainSuffix = '*.' + item.domain;
        } else if (item.type === 'loadbalancer-domain') {
          if (this.customDomainsSuffix.indexOf(item.domain) < 0) {
            this.customDomainsSuffix.push(item.domain);
          }
        }
      });
    }
  }

  getDomainString(): string {
    return (
      (this.customDomainsSuffix && this.customDomainsSuffix.join(',')) || ''
    );
  }

  get empty() {
    return !this.frontends.length;
  }

  get loadError() {
    return this.paginationDataWrapper.loadError;
  }

  get loading() {
    return this.paginationDataWrapper.loading;
  }

  get searching() {
    // TODO
    return false;
    // return this.paginationDataWrapper.searching;
  }

  refetchFrontends(): any {
    this.paginationDataWrapper.refetch();
  }

  refetchLB(): any {
    this.basicLoading = true;
    this.networkService
      .getDetail({
        uuid: this.loadBalancerDetail.name,
        frontend: true,
      })
      .then(detail => {
        this.loadBalancerDetail = detail;
        this._formatDomains();
      })
      .catch(() => {})
      .then(() => {
        this.basicLoading = false;
      });
    this.refetchFrontends();
  }

  toggleShowMore() {
    this.showMore = !this.showMore;
  }
}
