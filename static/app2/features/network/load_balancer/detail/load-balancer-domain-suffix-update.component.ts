import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { NetworkService } from 'app2/shared/services/features/network.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';

@Component({
  templateUrl: './load-balancer-domain-suffix-update.component.html',
  styleUrls: ['./load-balancer-domain-suffix-update.component.scss'],
  providers: [NetworkService],
})
export class LoadBalancerDomainSuffixUpdateComponent implements OnInit {
  @Output()
  finished = new EventEmitter<any>();
  @ViewChild('domainSuffixForm')
  form: NgForm;

  customDomains: string[];
  defaultDomainDisabled: boolean;
  lbName: string;
  model: any;
  submitting = false;

  constructor(
    @Inject(MODAL_DATA)
    modalData: {
      defaultDomainDisabled: boolean;
      customDomains: string[];
      lbName: string;
    },
    private networkService: NetworkService,
  ) {
    this.model = {
      defaultDomainsEnabled: false,
      domains: [],
    };
    this.defaultDomainDisabled = modalData.defaultDomainDisabled;
    this.customDomains = modalData.customDomains;
    this.lbName = modalData.lbName;
  }

  ngOnInit(): void {
    this.model.defaultDomainsEnabled = !this.defaultDomainDisabled;
    this.model.domains = this.customDomains.map((t: string) => {
      return t.charAt(0) === '*' ? t : `*.${t}`;
    });
  }

  async submit() {
    if (this.form.valid && !this.submitting) {
      this.submitting = true;
      const data = {
        action: 'set_domain_suffix',
        disable_default_suffix: !this.model.defaultDomainsEnabled,
        domain_info: this.model.domains.map((t: string) => {
          return { domain: t };
        }),
      };
      this.networkService
        .update(this.lbName, data)
        .then(() => this.done(true))
        .catch(() => {
          this.submitting = false;
        });
    }
  }

  private done(res?: any) {
    this.finished.next(res);
    this.finished.complete();
  }

  cancel() {
    this.done();
  }
}
