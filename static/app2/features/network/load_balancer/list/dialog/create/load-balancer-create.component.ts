import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { NotificationService } from 'alauda-ui';
import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';
import { SingleSelectionDropdownComponent } from 'app2/shared/components/select/single-selection-dropdown.component';
import { NetworkService } from 'app2/shared/services/features/network.service';
import { NetworkUtilitiesService } from 'app2/shared/services/features/network.utilities.service';
import {
  QuotaSpace,
  QuotaSpaceService,
} from 'app2/shared/services/features/quota-space.service';
import { Region } from 'app2/shared/services/features/region.service';

@Component({
  templateUrl: './load-balancer-create.component.html',
  styleUrls: ['./load-balancer-create.component.scss'],
  providers: [NetworkService, QuotaSpaceService, NetworkUtilitiesService],
})
export class LoadBalancerCreateComponent implements OnInit {
  @ViewChild(SingleSelectionDropdownComponent)
  @ViewChild('createForm')
  form: NgForm;
  @Input()
  region: Region;
  @Output()
  finished = new EventEmitter<any>();

  singleDropdown: SingleSelectionDropdownComponent;
  submitting: boolean;
  model: any;
  quotaSpaceOptions: QuotaSpace[];
  lbTypeOptions: any[];
  addressTypeOptions: any[];
  quota_enabled: boolean;

  constructor(
    private networkService: NetworkService,
    @Inject(WEBLABS) private weblabs: Weblabs,
    private networkUtilities: NetworkUtilitiesService,
    private auiNotificationService: NotificationService,
    private quotaSpaceService: QuotaSpaceService,
  ) {
    this.model = {
      name: '',
      type: '',
      address_type: '',
      region_name: '',
      create_type: 'manual',
    };
  }

  ngOnInit() {
    this.quota_enabled = this.weblabs.QUOTA_ENABLED;
    this.model.region_name = this.region.name;
    this._initOptions();
  }

  private _initOptions() {
    this.addressTypeOptions = this.networkUtilities.getAddressTypes();
    this.lbTypeOptions = this.networkUtilities.getRegionSupportedCreateTypes(
      this.region,
    );
    if (this.quota_enabled) {
      this.quotaSpaceService
        .getConsumableSpaces()
        .then(spaces => {
          this.quotaSpaceOptions = spaces;
        })
        .catch(() => {});
    }
  }

  cancel() {
    this.done();
  }

  async submit() {
    if (this.form.valid && !this.submitting) {
      this.submitting = true;
      this.networkService
        .create(this.model)
        .then(() => {
          this.done(true);
        })
        .catch(({ errors }) => {
          if (errors) {
            this.auiNotificationService.error(errors[0].message);
          }
        })
        .then(() => {
          this.submitting = false;
        });
    }
  }

  private done(res?: any) {
    this.finished.next(res);
    this.finished.complete();
  }
}
