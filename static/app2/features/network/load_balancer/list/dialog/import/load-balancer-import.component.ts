import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { NotificationService } from 'alauda-ui';
import { ACCOUNT } from 'app2/core/tokens';
import { WEBLABS } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { Weblabs } from 'app2/core/types';
import { SingleSelectionDropdownComponent } from 'app2/shared/components/select/single-selection-dropdown.component';
import { NetworkService } from 'app2/shared/services/features/network.service';
import { NetworkUtilitiesService } from 'app2/shared/services/features/network.utilities.service';
import {
  QuotaSpace,
  QuotaSpaceService,
} from 'app2/shared/services/features/quota-space.service';
import { Region } from 'app2/shared/services/features/region.service';

@Component({
  templateUrl: './load-balancer-import.component.html',
  styleUrls: ['../create/load-balancer-create.component.scss'],
  providers: [NetworkService, QuotaSpaceService, NetworkUtilitiesService],
})
export class LoadBalancerImportComponent implements OnInit {
  @ViewChild(SingleSelectionDropdownComponent)
  @ViewChild('importForm')
  form: NgForm;
  @Input()
  region: Region;
  @Output()
  finished = new EventEmitter<any>();

  quotaSpaceOptions: QuotaSpace[];
  lbTypeOptions: any[];
  addressTypeOptions: any[];
  quota_enabled: boolean;

  singleDropdown: SingleSelectionDropdownComponent;
  submitting: boolean;
  model: any;
  namespace: string;

  constructor(
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(WEBLABS) private weblabs: Weblabs,
    private networkService: NetworkService,
    private auiNotificationService: NotificationService,
    private networkUtilities: NetworkUtilitiesService,
    private quotaSpaceService: QuotaSpaceService,
  ) {
    this.submitting = false;
    this.model = {
      name: '',
      type: '',
      address_type: '',
      adress: '',
      create_type: 'import',
      region_name: '',
      iaas_id: '',
    };
  }

  ngOnInit() {
    this.model.region_name = this.region.name;
    this.namespace = this.account.namespace;
    this.quota_enabled = this.weblabs.QUOTA_ENABLED;
    this.addressTypeOptions = this.networkUtilities.getAddressTypes();
    this.lbTypeOptions = this.networkUtilities.getRegionSupportedImportTypes(
      this.region,
    );
    if (this.quota_enabled) {
      this.quotaSpaceService
        .getConsumableSpaces()
        .then(spaces => {
          this.quotaSpaceOptions = spaces;
        })
        .catch(() => {});
    }
  }

  cancel() {
    this.done();
  }

  async submit() {
    if (this.form.valid && !this.submitting) {
      this.submitting = true;
      this.networkService
        .create(this.model)
        .then(() => {
          this.done(true);
        })
        .catch(({ errors }) => {
          if (errors) {
            this.auiNotificationService.error(errors[0].message);
          }
        })
        .then(() => {
          this.submitting = false;
        });
    }
  }

  fieldRequired() {
    return ['haproxy', 'nginx'].includes(this.model.type);
  }

  private done(res?: any) {
    this.finished.next(res);
    this.finished.complete();
  }
}
