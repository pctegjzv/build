import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { TableComponent } from 'app2/shared/components/table/table.component';
import {
  LoadBalancer,
  NetworkService,
} from 'app2/shared/services/features/network.service';
import { NetworkUtilitiesService } from 'app2/shared/services/features/network.utilities.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';

import { LoadBalancerCreateComponent } from './dialog/create/load-balancer-create.component';
import { LoadBalancerImportComponent } from './dialog/import/load-balancer-import.component';

@Component({
  templateUrl: './load-balancer-list.component.html',
  styleUrls: ['./load-balancer-list.component.scss'],
  providers: [NetworkService],
})
export class LoadBalancerListComponent implements OnInit {
  @ViewChild('lbTable')
  table: TableComponent;
  loadBalancerList: LoadBalancer[];
  regionSubscription: Subscription;
  empty: boolean;
  loading: boolean;
  loadError: boolean;
  region: Region;
  filterKey = '';
  lbCreateEnabled = false;

  constructor(
    private regionService: RegionService,
    private modalService: ModalService,
    private router: Router,
    private networkUtilities: NetworkUtilitiesService,
    private auiNotificationService: NotificationService,
    private roleUtil: RoleUtilitiesService,
    private translateService: TranslateService,
    private networkService: NetworkService,
  ) {
    this.empty = false;
    this.loading = this.loadError = false;
    this.loadBalancerList = [];
  }

  async ngOnInit() {
    this.regionSubscription = this.regionService.region$
      .pipe(filter(region => !!region))
      .subscribe(region => {
        this.region = region;
        this.fetch(region.name);
      });
    this.lbCreateEnabled = await this.roleUtil.resourceTypeSupportPermissions(
      'load_balancer',
      {
        cluster_name: this.region.name,
      },
    );
  }

  ngOnDestory() {
    this.regionSubscription.unsubscribe();
  }

  fetch(region_name: string) {
    this.empty = true;
    this.loading = true;
    this.networkService
      .getList({
        region_name,
      })
      .then(res => {
        this.empty = !res.length;
        this.loadBalancerList = res;
      })
      .catch(() => {
        this.loadError = true;
      })
      .then(() => {
        this.loading = false;
      });
  }

  refetch() {
    this.fetch(this.region.name);
  }

  searchChanged(event: string) {
    this.filterKey = event;
  }

  import() {
    const modelRef = this.modalService.open(LoadBalancerImportComponent, {
      title: this.translateService.get('import'),
    });
    modelRef.componentInstance.region = this.region;
    modelRef.componentInstance.finished.pipe(first()).subscribe(res => {
      modelRef.close();
      if (res) {
        this.refetch();
      }
    });
  }

  create() {
    const modelRef = this.modalService.open(LoadBalancerCreateComponent, {
      title: this.translateService.get('create'),
    });
    modelRef.componentInstance.region = this.region;
    modelRef.componentInstance.finished.pipe(first()).subscribe(res => {
      modelRef.close();
      if (res) {
        this.refetch();
      }
    });
  }

  getLoadBalancerList() {
    return this.loadBalancerList.filter(
      lb => lb.name.indexOf(this.filterKey) !== -1,
    );
  }

  viewLoadBalancer(lb: LoadBalancer) {
    this.router.navigate(['/load_balancer/detail', lb.name]);
  }

  deleteLoadBalancer(lb: LoadBalancer) {
    this.modalService
      .confirm({
        title: this.translateService.get('delete'),
        content: this.translateService.get('load_balancer_delete_confirm', {
          lb_name: lb.name,
        }),
      })
      .then(() => {
        this.networkService
          .delete(lb.load_balancer_id)
          .then(() => {
            this.refetch();
          })
          .catch(({ errors }) => {
            if (errors.length) {
              this.auiNotificationService.error(errors[0].message);
            }
          });
      })
      .catch(() => {});
  }

  canImport(): boolean {
    return !!this.networkUtilities.getRegionSupportedImportTypes(this.region)
      .length;
  }

  canCreate(): boolean {
    return !!this.networkUtilities.getRegionSupportedCreateTypes(this.region)
      .length;
  }
}
