import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AppService } from 'app2/shared/services/features/app.service';
import { cloneDeep, find, flatten } from 'lodash';

import { NotificationService } from 'alauda-ui';
import { K8sComponentInfo } from 'app2/features/service/service.type';
import { TagsInputComponent } from 'app2/shared/components/tags-input/tags-input.component';
import {
  AluCertificate,
  FrontendDetail,
  LB_TYPE,
  LoadBalancer,
  MATCH_TYPE,
  NetworkService,
  RULE_TYPE,
  Rule as BaseRule,
  RuleIndicator,
  Service as RuleService,
} from 'app2/shared/services/features/network.service';
import { NetworkUtilitiesService } from 'app2/shared/services/features/network.utilities.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

interface Rule extends BaseRule {
  expanded: boolean;
  ruleIndicators: RuleIndicator[];
  sessionAffinity: boolean;
}

const PATTERNS = {
  SRC_IP: /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/,
  NUMBER: /^\d+$/,
};

interface RuleService2 extends RuleService {
  container_ports: string[];
}

@Component({
  templateUrl: 'load-balancer-port-add-rule.component.html',
  styleUrls: ['load-balancer-port-add-rule.component.scss'],
})
export class LoadBalancerPortAddRuleComponent implements OnInit {
  @Output()
  confirmed = new EventEmitter<boolean>();

  PATTERNS = PATTERNS;

  ruleTypes: { display: string; value: RULE_TYPE }[];
  matchTypesMap: Map<
    RULE_TYPE,
    {
      key: boolean;
      matchTypes: MATCH_TYPE[];
    }
  > = new Map();
  sessionAffinities: { display: string; value: string }[];

  rules: Rule[] = [];
  services: K8sComponentInfo[];
  portsMap: Map<string, number[]> = new Map();

  loadBalancer: LoadBalancer;
  frontendDetail: FrontendDetail;
  type: string;
  certificatesOptions: AluCertificate[];
  frontendBasicModel: any;
  PROTOCOLS: any[] = [
    {
      name: 'HTTPS',
      value: 'https',
    },
    {
      name: 'HTTP',
      value: 'http',
    },
    {
      name: 'TCP',
      value: 'tcp',
    },
  ];
  created: boolean;

  confirming: boolean;

  cloneDeep = cloneDeep;

  constructor(
    @Inject(MODAL_DATA)
    private modalData: {
      loadBalancer: LoadBalancer;
      frontendDetail: FrontendDetail;
      rule?: Rule;
      elb_service?: K8sComponentInfo;
      type: string;
    },
    private regionService: RegionService,
    private regionUtilitiesService: RegionUtilitiesService,
    private serviceService: ServiceService,
    private networkService: NetworkService,
    private translate: TranslateService,
    private appService: AppService,
    private utils: NetworkUtilitiesService,
    private auiNotificationService: NotificationService,
    private errorsToastService: ErrorsToastService,
  ) {
    this.loadBalancer = this.modalData.loadBalancer;
    this.frontendDetail = this.modalData.frontendDetail;
    this.type = modalData.type || 'add';

    const ruleTypes = [];

    const lbType = this.loadBalancer.type;
    this.frontendBasicModel = {
      certificate_name: '',
      certificate_id: '',
      container_port: 0,
      container_ports: [],
      service_id: '',
      protocol: '',
      port: '',
      load_balancer_id: '',
    };

    if (lbType === LB_TYPE.CLB) {
      this.PROTOCOLS = this.PROTOCOLS.filter(p => p.name !== 'HTTPS');
    }

    switch (lbType) {
      case LB_TYPE.HAPROXY:
      case LB_TYPE.SLB:
      case LB_TYPE.CLB:
        ruleTypes.push(RULE_TYPE.HOST, RULE_TYPE.URL);
        break;
      default:
        ruleTypes.push(
          RULE_TYPE.HOST,
          RULE_TYPE.URL,
          RULE_TYPE.SRC_IP,
          RULE_TYPE.HEADER,
          RULE_TYPE.COOKIE,
          RULE_TYPE.PARAM,
        );
        break;
    }

    this.ruleTypes = ruleTypes.map(value => ({
      display: this.translate.get('rule_' + value.toLocaleLowerCase()),
      value,
    }));
    this.matchTypesMap
      .set(RULE_TYPE.HOST, {
        key: false,
        matchTypes: [MATCH_TYPE.IN],
      })
      .set(RULE_TYPE.URL, {
        key: false,
        matchTypes:
          lbType === 'slb'
            ? [MATCH_TYPE.STARTS_WITH]
            : [MATCH_TYPE.REGEX, MATCH_TYPE.STARTS_WITH],
      })
      .set(RULE_TYPE.SRC_IP, {
        key: false,
        matchTypes: [MATCH_TYPE.EQ, MATCH_TYPE.RANGE],
      })
      .set(RULE_TYPE.HEADER, {
        key: true,
        matchTypes: [MATCH_TYPE.EQ, MATCH_TYPE.RANGE, MATCH_TYPE.REGEX],
      })
      .set(RULE_TYPE.COOKIE, {
        key: true,
        matchTypes: [MATCH_TYPE.EQ],
      })
      .set(RULE_TYPE.PARAM, {
        key: true,
        matchTypes: [MATCH_TYPE.EQ, MATCH_TYPE.RANGE],
      });

    this.sessionAffinities = ['sip-hash', 'cookie'].map(value => ({
      value,
      display: this.translate.get('session_affinity_' + value),
    }));
  }

  isSpecialLb(lbType: LB_TYPE) {
    return [LB_TYPE.HAPROXY, LB_TYPE.SLB, LB_TYPE.CLB].includes(lbType);
  }

  getWeightsPercent(rule: Rule, weight: number = 0) {
    const total = rule.services.reduce(
      (total: number, curr: RuleService) => total + (curr.weight || 0),
      0,
    );

    return total ? ((weight / total) * 100).toFixed(2) + '%' : 'N/A';
  }

  async ngOnInit() {
    let { rule } = this.modalData;

    if (rule) {
      rule = cloneDeep(rule);
      rule.expanded = true;
      rule.sessionAffinity = !!rule.session_affinity_policy;

      rule.ruleIndicators = cloneDeep(this.utils.parseDSL(rule));

      rule.services.forEach(service => {
        const port = service.container_port;
        if (port) {
          (service as RuleService2).container_ports = [port + ''];
        }
      });
    }

    // 大部分时候创建端口是添加规则是独立操作
    if (this.type !== 'create') {
      this.addRule(rule);
    }

    const region = await this.regionService.getCurrentRegion();

    let services = await this.serviceService.getRegionServices({
      region_name: region.name,
      region_platform_version: region.platform_version,
    });

    services.forEach((s: K8sComponentInfo) => {
      s.service_name = `${s.service_name}(${s.service_namespace})`;
    });

    if (!this.regionUtilitiesService.isNewK8sRegion(region)) {
      await this.appService
        .getApps({
          regionName: region.name,
        })
        .then((data: any) => {
          const appServices = flatten(
            data.map((app: any) => {
              const newServiceList = app.services.filter(
                (service: any) =>
                  !service.pod_controller ||
                  !['StatefulSet', 'DaemonSet'].includes(
                    service.pod_controller,
                  ),
              );
              newServiceList.forEach((s: K8sComponentInfo) => {
                s.service_name = `${app.app_name}.${s.service_name}(${
                  s.service_namespace
                })`;
              });
              return newServiceList;
            }),
          );
          services = services.concat(appServices as K8sComponentInfo[]);
        })
        .catch(() => {});
    }

    // this.services = services.filter(({ ports }) => ports.length);
    this.services = services;

    this.services.forEach(s => this.portsMap.set(s.uuid, s.ports));

    if (
      this.loadBalancer.type === LB_TYPE.ELB &&
      this.modalData.elb_service &&
      this.type === 'create'
    ) {
      this.services = [this.modalData.elb_service];
    }

    if (this.type === 'create') {
      // add service clear option when create
      this.services.unshift({
        service_name: this.translate.get('none'),
        uuid: '',
      } as K8sComponentInfo);

      this.networkService
        .getCertificates<AluCertificate>({
          page_size: 0,
          order_by: '-created_at',
          load_balancer_id: this.loadBalancer.load_balancer_id,
        })
        .then(certs => {
          this.certificatesOptions = certs.filter(c => !c.is_used);
        });
    }
  }

  getPartialRuleIndicators(ruleIndicators: RuleIndicator[]) {
    return this.utils.getPartialIndicators(ruleIndicators).indicators;
  }

  changePort(ruleService: RuleService2) {
    ruleService.container_port = +ruleService.container_ports[0];
  }

  setTagsInput(serviceId: string, tagsInput: TagsInputComponent) {
    const ports = this.portsMap.get(serviceId) || [];
    tagsInput.setSuggestions(ports.map(port => port + ''));
  }

  addRule(rule?: Rule) {
    if (
      this.frontendBasicModel.protocol === 'tcp' ||
      this.loadBalancer.type === LB_TYPE.ELB
    ) {
      return;
    }
    this.rules.push(
      rule ||
        ({
          expanded: true,
          ruleIndicators: [{ values: [] } as RuleIndicator],
          services: [{ weight: 100 } as RuleService],
        } as Rule),
    );
  }

  moveRule(from: number, to: number) {
    const { rules } = this;
    const temp = rules[to];
    rules[to] = rules[from];
    rules[from] = temp;
  }

  addRuleIndicator(rule: Rule) {
    rule.ruleIndicators.push({ values: [] } as RuleIndicator);
  }

  addService(rule: Rule) {
    rule.services.push({ weight: 100 } as RuleService);
  }

  onIndicatorTypeChange(indicator: RuleIndicator) {
    indicator.values = [[]];
    if (!indicator.type || !this.matchTypesMap.get(indicator.type).key) {
      delete indicator.key;
    }
  }

  portProtocolChanged() {
    if (
      this.frontendBasicModel.protocol === 'tcp' ||
      this.loadBalancer.type === LB_TYPE.ELB
    ) {
      this.rules = [];
    }
  }

  async submitForm(formRef: NgForm) {
    if (formRef.form.invalid || this.confirming) {
      return;
    }

    this.confirming = true;

    const isCreate = this.type === 'create';

    let certificateName = '',
      certificateId = '';
    if (isCreate) {
      if (this.frontendBasicModel.protocol === 'https') {
        if (!this.frontendBasicModel.certificate_id) {
          this.confirming = false;
          return this.auiNotificationService.error(
            this.translate.get('load_balancer_not_set_certificate'),
          );
        }
        const found = find<AluCertificate>(this.certificatesOptions, {
          uuid: this.frontendBasicModel.certificate_id,
        });
        this.frontendBasicModel.certificate_name = (found && found.name) || '';
        certificateName = this.frontendBasicModel.certificate_name;
        certificateId = this.frontendBasicModel.certificate_id;
      }
      if (
        !this.frontendBasicModel.container_ports.length &&
        this.frontendBasicModel.service_id
      ) {
        this.confirming = false;
        return this.auiNotificationService.error(
          this.translate.get('load_balancer_not_set_container_port'),
        );
      } else if (this.frontendBasicModel.container_ports.length) {
        this.frontendBasicModel.container_port = +this.frontendBasicModel
          .container_ports[0];
      }
    }

    let generatedRules: any[];

    try {
      generatedRules = this.rules.map(rule => {
        if (!rule.ruleIndicators.length) {
          throw new Error(this.translate.get('no_rule_indicators'));
        }

        const { services } = rule;

        const caches = {};

        services.forEach(({ service_id, container_port }) => {
          let cache = caches[service_id];

          if (!caches[service_id]) {
            cache = caches[service_id] = [];
          }

          if (cache.includes(container_port)) {
            throw new Error(this.translate.get('duplicate_ports'));
          }

          cache.push(container_port);
        });

        rule.ruleIndicators.forEach(indicator => {
          const { type, values } = indicator;
          values.forEach(v => {
            const [matchType, value] = v;
            if (
              type === RULE_TYPE.URL &&
              matchType === MATCH_TYPE.STARTS_WITH &&
              !/^\//.test(value)
            ) {
              v[1] = '/' + value;
            }
          });
        });

        let domain, url;

        if (this.isSpecialLb(this.loadBalancer.type)) {
          const rules = new Set<RULE_TYPE>();

          rule.ruleIndicators.forEach((indicator, index) => {
            const { type, values } = indicator;

            rules.add(type);

            if (rules.size !== index + 1) {
              throw new Error(this.translate.get('duplicate_indicator_type'));
            }

            const [matchType, value] = values[0];

            switch (type) {
              case RULE_TYPE.HOST:
                domain = value;
                break;
              case RULE_TYPE.URL:
                let prefix = '';

                if (matchType === MATCH_TYPE.REGEX && !/^[.*]/.test(value)) {
                  prefix = '^';
                }

                url = prefix + value;

                break;
            }
          });

          if (this.loadBalancer.type === 'clb') {
            // reference: https://cloud.tencent.com/document/product/214/6744
            if (!domain) {
              throw new Error(this.translate.get('domain_required_in_clb'));
            } else {
              // url可以为空，以/开头的url地址，满足一定条件的正则
              const domainVaild =
                (/[a-zA-Z0-9_.-]/.test(domain) || /^\*\.|\.\*$/.test(domain)) &&
                !/["{};\` ']/.test(domain);
              const urlVaild =
                (url &&
                  (/^\/[a-zA-Z0-9_.-]/.test(url) ||
                    (!/["{};\` ']/.test(url) && /^\^/.test(url)))) ||
                !url;

              if (!domainVaild || !urlVaild) {
                throw new Error(
                  this.translate.get('domain_or_url_invaild_in_clb'),
                );
              }
            }
          }
        }

        return {
          rule_id: rule.rule_id,
          domain,
          url,
          certificate_id: isCreate
            ? certificateId
            : this.frontendDetail.certificate_id,
          certificate_name: isCreate
            ? certificateName
            : this.frontendDetail.certificate_name,
          description: rule.description,
          session_affinity_attribute: rule.sessionAffinity
            ? rule.session_affinity_attribute
            : '',
          session_affinity_policy: rule.sessionAffinity
            ? rule.session_affinity_policy
            : '',
          dsl: this.utils.stringifyDSL(rule.ruleIndicators),
          services,
        };
      });
    } catch (e) {
      this.confirming = false;
      return this.auiNotificationService.error(e.message);
    }

    try {
      if (isCreate) {
        await this.networkService.createFrontend(
          this.loadBalancer.load_balancer_id,
          this.frontendBasicModel,
        );
        this.created = true;
      }

      await Promise.all(
        generatedRules.map((rule, i) =>
          this.networkService[
            this.type === 'update' ? 'updateRule' : 'createRule'
          ](
            this.loadBalancer.load_balancer_id,
            this.frontendBasicModel.port || this.frontendDetail.port,
            rule,
          ).catch(e => {
            e.index = i;
            throw e;
          }),
        ),
      ).then(async (ruleList: any[]) => {
        if (this.type === 'update') {
          ruleList = generatedRules;
        }
        await Promise.all(
          ruleList.map((rule: any, i) => {
            return this.networkService
              .updateRuleServices({
                lb_id: this.loadBalancer.load_balancer_id,
                port: this.frontendBasicModel.port || this.frontendDetail.port,
                rule_id: rule.rule_id,
                body: generatedRules[i].services,
              })
              .catch(e => {
                e.index = i;
                throw e;
              });
          }),
        );
      });
      this.confirmed.next(true);
    } catch (e) {
      if (this.created) {
        this.type = 'add';
        this.frontendDetail = cloneDeep(this.frontendBasicModel);
      } else if (isCreate || this.type === 'update') {
        return this.errorsToastService.handleGenericAjaxError({
          errors: e.errors,
          handleNonGenericCodes: true,
        });
      }
      this.rules.splice(0, e.index);

      return this.auiNotificationService.error(
        `No.${e.index} Error - ${e.errors ? e.errors[0].message : e.message}`,
      );
    } finally {
      this.confirming = false;
    }
  }
}
