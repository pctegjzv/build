import { cloneDeep, flatten } from 'lodash';

import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

import { NotificationService } from 'alauda-ui';
import { K8sComponentInfo } from 'app2/features/service/service.type';
import { TagsInputComponent } from 'app2/shared/components/tags-input/tags-input.component';
import { AppService } from 'app2/shared/services/features/app.service';
import {
  NetworkService,
  Rule,
  RuleIndicator,
  Service as _RuleService,
} from 'app2/shared/services/features/network.service';
import { NetworkUtilitiesService } from 'app2/shared/services/features/network.utilities.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';

interface ModalData {
  lbId: string;
  lbName: string;
  protocol: string;
  port: number;
  rule: Rule;
  ruleList: Rule[];
  parsedRules: any[];
  type: string;
}

const PATTERNS = {
  NUMBER: /^\d+$/,
};

interface RuleService extends _RuleService {
  container_ports?: string[];
}

@Component({
  templateUrl: 'load-balancer-port-add-service.component.html',
  styleUrls: ['load-balancer-port-add-service.component.scss'],
})
export class LoadBalancerPortAddServiceComponent implements OnInit {
  @Output()
  confirmed = new EventEmitter<boolean>();

  PATTERNS = PATTERNS;

  ruleServices: RuleService[];
  ruleIndicators: RuleIndicator[];
  services: K8sComponentInfo[];
  portsMap: Map<string, number[]> = new Map();
  type: string;
  selectedRuleIndex = -1;

  confirming: boolean;

  parseDSL = this.utils.parseDSL;

  constructor(
    @Inject(MODAL_DATA) public modalData: ModalData,
    private serviceService: ServiceService,
    private regionService: RegionService,
    private regionUtilitiesService: RegionUtilitiesService,
    private networkService: NetworkService,
    private utils: NetworkUtilitiesService,
    private appService: AppService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
  ) {
    this.type = this.modalData.type || 'add';

    if (this.type === 'create') {
      return;
    }

    const { rule } = this.modalData;
    this.ruleServices = cloneDeep(rule.services);

    if (this.ruleServices.length) {
      this.ruleServices.forEach(service => {
        const port = service.container_port;
        if (port) {
          service.container_ports = [port + ''];
        }
      });
    } else {
      this.addService();
    }

    this.ruleIndicators = this.utils.parseDSL(rule);
  }

  async ngOnInit() {
    const region = await this.regionService.getCurrentRegion();

    let services = await this.serviceService.getRegionServices({
      region_name: region.name,
      region_platform_version: region.platform_version,
    });

    if (!this.regionUtilitiesService.isNewK8sRegion(region)) {
      await this.appService
        .getApps({
          regionName: region.name,
        })
        .then((data: any) => {
          const appServices = flatten(
            data.map((app: any) => {
              const newServiceList = app.services.filter(
                (service: any) =>
                  !service.pod_controller ||
                  !['StatefulSet', 'DaemonSet'].includes(
                    service.pod_controller,
                  ),
              );
              newServiceList.forEach((s: K8sComponentInfo) => {
                s.service_name = `${app.app_name}.${s.service_name}`;
              });
              return newServiceList;
            }),
          );
          services = services.concat(appServices as K8sComponentInfo[]);
        })
        .catch(() => {});
    }

    // this.services = services.filter(({ ports }) => ports.length);
    this.services = services;

    this.services.forEach(s => this.portsMap.set(s.uuid, s.ports));
  }

  getWeightsPercent(weight: number = 0) {
    const total = this.ruleServices.reduce(
      (total: number, curr: RuleService) => total + (curr.weight || 0),
      0,
    );

    return total ? ((weight / total) * 100).toFixed(2) + '%' : 'N/A';
  }

  ruleSelectedChanged(va: boolean, index: number) {
    if (va) {
      this.selectedRuleIndex = index;
      this.ruleServices =
        cloneDeep(this.modalData.ruleList[index]).services || [];
      if (this.ruleServices.length) {
        this.ruleServices.forEach(service => {
          const port = service.container_port;
          if (port) {
            service.container_ports = [port + ''];
          }
        });
      } else {
        this.addService();
      }
    } else if (this.selectedRuleIndex === index) {
      this.selectedRuleIndex = -1;
    }
  }

  setTagsInput(serviceId: string, tagsInput: TagsInputComponent) {
    tagsInput.setSuggestions(
      this.portsMap.get(serviceId).map(port => port + ''),
    );
  }

  changePort(ruleService: RuleService) {
    ruleService.container_port = +ruleService.container_ports[0];
  }

  addService() {
    if (this.type === 'create' && this.selectedRuleIndex < 0) {
      return;
    }
    this.ruleServices.push({} as RuleService);
  }

  removeService(i: number) {
    this.ruleServices.splice(i, 1);
    this.ruleServices = [...this.ruleServices];
  }

  async submitForm(formRef: NgForm) {
    if (formRef.form.invalid || this.confirming) {
      return;
    }

    this.confirming = true;

    try {
      const { ruleServices } = this;

      const caches = {};

      ruleServices.forEach(({ service_id, container_port }) => {
        let cache = caches[service_id];

        if (!caches[service_id]) {
          cache = caches[service_id] = [];
        }

        if (cache.includes(container_port)) {
          throw new Error(this.translate.get('duplicate_ports'));
        }

        cache.push(container_port);
      });
      await this.networkService.updateRuleServices({
        lb_id: this.modalData.lbId,
        port: this.modalData.port,
        rule_id:
          (this.type === 'create' &&
            this.selectedRuleIndex >= 0 &&
            this.modalData.ruleList[this.selectedRuleIndex].rule_id) ||
          this.modalData.rule.rule_id,
        body: ruleServices,
      });
      this.confirmed.next(true);
    } catch (e) {
      this.auiNotificationService.error(
        e.errors ? e.errors[0].message : e.message,
      );
    } finally {
      this.confirming = false;
    }
  }
}
