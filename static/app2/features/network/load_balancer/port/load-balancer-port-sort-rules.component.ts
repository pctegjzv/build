import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';

import { NotificationService } from 'alauda-ui';
import {
  NetworkService,
  Rule,
} from 'app2/shared/services/features/network.service';
import { NetworkUtilitiesService } from 'app2/shared/services/features/network.utilities.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';

@Component({
  templateUrl: 'load-balancer-port-sort-rules.component.html',
  styleUrls: ['load-balancer-port-sort-rules.component.scss'],
})
export class LoadBalancerPortSortRulesComponent implements OnInit {
  @Output()
  confirmed: EventEmitter<boolean> = new EventEmitter();

  confirming: boolean;
  rules: Rule[];

  draggingIndex: number;
  previewingIndex: number;

  constructor(
    @Inject(MODAL_DATA)
    private modalData: {
      lbId: string;
      port: number;
    },
    private networkService: NetworkService,
    private utils: NetworkUtilitiesService,
    private auiNotificationService: NotificationService,
  ) {}

  async ngOnInit() {
    const { rules } = await this.networkService.getFrontendDetail(
      this.modalData.lbId,
      this.modalData.port,
    );
    this.rules = rules.filter(rule => rule.type !== 'system');
  }

  parseDSL(rule: Rule) {
    return this.utils.parseDSL(rule);
  }

  getPartialRuleIndicators(rule: Rule) {
    if (rule.dsl) {
      return this.utils.getPartialIndicators(rule.dsl);
    }

    return this.utils.getPartialIndicators(this.parseDSL(rule));
  }

  onDragStart(index: number, e: DragEvent) {
    e.dataTransfer.setData('text/html', 'dragging');
    this.draggingIndex = this.previewingIndex = index;
  }

  onDragEnter(index: number) {
    this.previewingIndex = index;
  }

  onDragEnd() {
    this.draggingIndex = this.previewingIndex = null;
  }

  onDrop(to: number) {
    const { draggingIndex: from } = this;

    if (from === to) {
      return;
    }

    if (from > to) {
      to++;
    }

    const [fromEl] = this.rules.splice(from, 1);
    this.rules.splice(to, 0, fromEl);
  }

  async sortRules() {
    if (this.confirming) {
      return;
    }
    this.confirming = true;
    try {
      await this.networkService.setRuleOrder(
        this.modalData.lbId,
        this.modalData.port,
        this.rules.map(({ rule_id }) => rule_id),
      );
      this.confirmed.next(true);
    } catch (e) {
      this.auiNotificationService.error(
        e.errors ? e.errors[0].message : e.messagee,
      );
    } finally {
      this.confirming = false;
    }
  }
}
