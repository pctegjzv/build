import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

import { NotificationService } from 'alauda-ui';
import {
  Certificate,
  CertificateService,
} from 'app2/shared/services/features/certificate.service';
import {
  FrontendDetail,
  NetworkService,
} from 'app2/shared/services/features/network.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';

@Component({
  templateUrl: 'load-balancer-port-update-certificate.component.html',
  styleUrls: ['load-balancer-port-update-certificate.component.scss'],
})
export class LoadBalancerPortUpdateCertificateComponent implements OnInit {
  @Output()
  confirmed: EventEmitter<{
    certificate_id: string;
    certificate_name: string;
  }> = new EventEmitter();

  certificates: Certificate[];
  certificateId: string;
  certificateName: string;

  confirming: boolean;

  constructor(
    private certificateService: CertificateService,
    private networkService: NetworkService,
    private auiNotificationService: NotificationService,
    @Inject(MODAL_DATA)
    private modalData: {
      lbId: string;
      port: number;
      frontendDetail: FrontendDetail;
    },
  ) {
    const { frontendDetail } = this.modalData;
    this.certificateId = frontendDetail.certificate_id;
    this.certificateName = frontendDetail.certificate_name;
  }

  async ngOnInit() {
    const {
      results: certificates,
    } = await this.certificateService.getCertificates({
      load_balancer_id: this.modalData.lbId,
    });

    this.certificates = certificates;
  }

  async submitForm(formRef: NgForm) {
    if (formRef.form.invalid || this.confirming) {
      return;
    }

    this.confirming = true;

    const newParams = {
      certificate_id: this.certificateId,
      certificate_name: this.certificateName,
    };

    try {
      await this.networkService.updateFrontend(
        this.modalData.lbId,
        this.modalData.port,
        {
          ...this.modalData.frontendDetail,
          ...newParams,
        },
      );

      this.confirmed.next(newParams);
    } catch (e) {
      this.auiNotificationService.error(
        e.errors ? e.errors[0].message : e.message,
      );
    } finally {
      this.confirming = false;
    }
  }
}
