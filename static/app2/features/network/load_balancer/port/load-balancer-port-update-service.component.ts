import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NotificationService } from 'alauda-ui';
import { K8sComponentInfo } from 'app2/features/service/service.type';
import { TagsInputComponent } from 'app2/shared/components/tags-input/tags-input.component';
import { AppService } from 'app2/shared/services/features/app.service';
import {
  FrontendDetail,
  NetworkService,
} from 'app2/shared/services/features/network.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';
import { flatten } from 'lodash';

const PATTERNS = {
  NUMBER: /^\d+$/,
};

@Component({
  templateUrl: 'load-balancer-port-update-service.component.html',
  styleUrls: ['load-balancer-port-update-service.component.scss'],
})
export class LoadBalancerPortUpdateServiceComponent implements OnInit {
  @Output()
  confirmed: EventEmitter<{
    container_port: number;
    service_id: string;
    service_name: string;
  }> = new EventEmitter();

  PATTERNS = PATTERNS;

  services: K8sComponentInfo[];
  inputPorts: string[];

  appName: string;
  serviceId: string;
  serviceName: string;
  port: number;

  confirming: boolean;

  constructor(
    private regionService: RegionService,
    private regionUtilities: RegionUtilitiesService,
    private serviceService: ServiceService,
    private networkService: NetworkService,
    private auiNotificationService: NotificationService,
    private appService: AppService,
    private translate: TranslateService,
    @Inject(MODAL_DATA)
    private modalData: {
      lbId: string;
      frontendDetail: FrontendDetail;
    },
  ) {
    const { frontendDetail } = this.modalData;
    this.serviceId = frontendDetail.service_id;
    this.port = frontendDetail.container_port;
    this.inputPorts = this.port ? [this.port + ''] : [];
  }

  async ngOnInit() {
    const region = await this.regionService.getCurrentRegion();

    let services = await this.serviceService.getRegionServices({
      region_name: region.name,
      region_platform_version: region.platform_version,
    });
    if (!this.regionUtilities.isNewK8sRegion(region)) {
      await this.appService
        .getApps({
          regionName: region.name,
        })
        .then((data: any) => {
          const appServices = flatten(
            data.map((app: any) => {
              const newServiceList = app.services.filter(
                (service: any) =>
                  !service.pod_controller ||
                  !['StatefulSet', 'DaemonSet'].includes(
                    service.pod_controller,
                  ),
              );
              newServiceList.forEach((s: K8sComponentInfo) => {
                s.service_name = `${app.app_name}.${s.service_name}`;
              });
              return newServiceList;
            }),
          );
          services = services.concat(appServices as K8sComponentInfo[]);
        })
        .catch(() => {});
    }

    // this.services = services.filter(({ ports }) => ports.length);

    // add none option in service select for unbinding default service
    services.unshift({
      service_name: this.translate.get('none'),
      uuid: '',
    } as K8sComponentInfo);
    this.services = services;
  }

  setTagsInput(tagsInput: TagsInputComponent) {
    if (!this.serviceId) {
      tagsInput.setSuggestions([]);
      this.port = undefined;
      this.serviceName = '';
      this.inputPorts = [];
      return;
    }

    const service = this.services.find(({ uuid }) => uuid === this.serviceId);

    this.appName = service.app_name;
    this.serviceName = service.service_name;
    this.inputPorts = [];

    tagsInput.setSuggestions(
      service.ports ? service.ports.map(port => port + '') : [],
    );
  }

  async submitForm(formRef: NgForm) {
    if (formRef.form.invalid || this.confirming) {
      return;
    }

    this.confirming = true;

    const newParams = {
      service_id: this.serviceId,
      app_name: this.appName,
      service_name: this.serviceName,
      container_port: +this.port,
    };

    try {
      const { frontendDetail } = this.modalData;

      await this.networkService.updateFrontend(
        this.modalData.lbId,
        frontendDetail.port,
        {
          ...frontendDetail,
          service_id: this.serviceId,
          container_port: +this.port,
        },
      );

      this.confirmed.next(newParams);
    } catch (e) {
      this.auiNotificationService.error(
        e.errors ? e.errors[0].message : e.message,
      );
    } finally {
      this.confirming = false;
    }
  }
}
