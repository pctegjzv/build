import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { LoadBalancerPortAddRuleComponent } from 'app2/features/network/load_balancer/port/load-balancer-port-add-rule.component';
import { LoadBalancerPortSortRulesComponent } from 'app2/features/network/load_balancer/port/load-balancer-port-sort-rules.component';
import {
  FrontendDetail,
  LoadBalancer,
  NetworkService,
  Rule,
  Service,
} from 'app2/shared/services/features/network.service';
import { NetworkUtilitiesService } from 'app2/shared/services/features/network.utilities.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ModalMode } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';
import { memoize } from 'lodash';

import { LoadBalancerPortAddServiceComponent } from './load-balancer-port-add-service.component';
import { LoadBalancerPortUpdateCertificateComponent } from './load-balancer-port-update-certificate.component';
import { LoadBalancerPortUpdateServiceComponent } from './load-balancer-port-update-service.component';

interface RulePage {
  count: number;
  page: number;
  page_size: number;
  data: Rule[];
}

@Component({
  templateUrl: './load-balancer-port.component.html',
  styleUrls: ['./load-balancer-port.component.scss'],
})
export class LoadBalancerPortComponent implements OnInit {
  lbId: string;
  port: number;
  region: Region;
  loadBalancer: LoadBalancer = {} as LoadBalancer;
  frontendDetail: FrontendDetail = {} as FrontendDetail;

  isK8sService: boolean;
  loading = true;
  searching = true;
  search = '';
  count: number;
  page = 1;
  pageSize = 20;
  rules: Rule[];

  getTotalWeights = memoize((services: Service[]) =>
    services.reduce((total, curr) => total + curr.weight, 0),
  );

  constructor(
    private networkService: NetworkService,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private modal: ModalService,
    private auiNotificationService: NotificationService,
    private networkUtils: NetworkUtilitiesService,
    private router: Router,
    private regionService: RegionService,
    private regionUtilitiesService: RegionUtilitiesService,
  ) {}

  async fetchFrontendDetail() {
    this.loading = true;

    try {
      this.frontendDetail = await this.networkService.getFrontendDetail(
        this.lbId,
        this.port,
      );
    } catch (e) {
      return this.router.navigate([
        '/load_balancer/detail',
        this.loadBalancer.name,
      ]);
    } finally {
      this.loading = false;
    }
  }

  async getPageRules() {
    this.searching = true;

    try {
      const { count, data: rules, page } = await this.networkService.getRules<
        RulePage
      >(this.lbId, this.port, {
        page: this.page,
        search: this.search,
      });

      this.count = count;
      this.rules = rules;
      this.page = count && page;
    } catch (e) {
      this.handleError(e);
    } finally {
      this.searching = false;
    }
  }

  fetchData() {
    return Promise.all([this.fetchFrontendDetail(), this.getPageRules()]);
  }

  async ngOnInit() {
    this.route.paramMap.subscribe(async map => {
      this.lbId = map.get('name');
      this.port = +map.get('port');

      this.region = await this.regionService.getCurrentRegion();

      this.isK8sService = this.regionUtilitiesService.isNewK8sRegion(
        this.region,
      );

      try {
        this.loadBalancer = await this.networkService.getDetail({
          uuid: this.lbId,
        });
      } catch (e) {
        return this.router.navigate(['/load_balancer']);
      }

      this.lbId = this.loadBalancer.load_balancer_id;

      await this.fetchData();
    });
  }

  canUpdate() {
    return this.networkService.canAction(this.loadBalancer, 'update');
  }

  addRule() {
    const modalRef = this.modal.open(LoadBalancerPortAddRuleComponent, {
      title: this.translate.get('add_rule'),
      width: 1000,
      mode: ModalMode.RIGHT_SLIDER,
      data: {
        loadBalancer: this.loadBalancer,
        frontendDetail: this.frontendDetail,
      },
    });

    modalRef.componentInstance.confirmed.subscribe(
      async (isConfirm: boolean) => {
        modalRef.close(isConfirm);
        if (isConfirm) {
          await this.fetchData();
        }
      },
    );
  }

  parseDSL(rawDSL: string) {
    return this.networkUtils.parseDSL(rawDSL);
  }

  getPartiaIndicators(rawDSL: string) {
    return this.networkUtils.getPartialIndicators(rawDSL);
  }

  getWeightsPercent(services: Service[], weight: number) {
    return ((weight / this.getTotalWeights(services)) * 100).toFixed(2) + '%';
  }

  pageChanged(page: number) {
    this.page = page;
    this.getPageRules();
  }

  addService(rule: Rule) {
    const modalRef = this.modal.open(LoadBalancerPortAddServiceComponent, {
      title: this.translate.get('add_service'),
      width: 1000,
      mode: ModalMode.RIGHT_SLIDER,
      data: {
        lbId: this.lbId,
        lbName: this.loadBalancer.name,
        protocol: this.frontendDetail.protocol,
        port: this.port,
        rule,
      },
    });

    modalRef.componentInstance.confirmed.subscribe(
      async (isConfirm: boolean) => {
        modalRef.close(isConfirm);
        if (isConfirm) {
          await this.fetchData();
        }
      },
    );
  }

  updateRule(rule: Rule) {
    const modalRef = this.modal.open(LoadBalancerPortAddRuleComponent, {
      title: this.translate.get('update_rule'),
      width: 1000,
      mode: ModalMode.RIGHT_SLIDER,
      data: {
        loadBalancer: this.loadBalancer,
        frontendDetail: this.frontendDetail,
        rule,
        type: 'update',
      },
    });

    modalRef.componentInstance.confirmed.subscribe(
      async (isConfirm: boolean) => {
        modalRef.close(isConfirm);
        if (isConfirm) {
          await this.fetchData();
        }
      },
    );
  }

  updateCertificate() {
    const modalRef = this.modal.open(
      LoadBalancerPortUpdateCertificateComponent,
      {
        title: this.translate.get('update_default_certificate'),
        data: {
          lbId: this.lbId,
          port: this.port,
          frontendDetail: this.frontendDetail,
          type: 'update',
        },
      },
    );

    modalRef.componentInstance.confirmed.subscribe(
      (certificate?: { certificate_id: string; certificate_name: string }) => {
        if (certificate) {
          Object.assign(this.frontendDetail, certificate);
        }
        modalRef.close();
      },
    );
  }

  updateService() {
    const modalRef = this.modal.open(LoadBalancerPortUpdateServiceComponent, {
      title: this.translate.get('update_default_service'),
      data: {
        lbId: this.lbId,
        frontendDetail: this.frontendDetail,
      },
    });

    modalRef.componentInstance.confirmed.subscribe((service?: {}) => {
      if (service) {
        Object.assign(this.frontendDetail, service);
      }
      modalRef.close();
    });
  }

  async sortRules() {
    const modalRef = this.modal.open(LoadBalancerPortSortRulesComponent, {
      title: this.translate.get('set_rule_sorting'),
      width: 800,
      mode: ModalMode.RIGHT_SLIDER,
      data: {
        lbId: this.lbId,
        port: this.port,
      },
    });

    modalRef.componentInstance.confirmed.subscribe(
      async (isConfirm: boolean) => {
        modalRef.close(isConfirm);
        if (isConfirm) {
          this.page = 1;
          await this.getPageRules();
        }
      },
    );
  }

  async deleteFrontendDetail() {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete_port'),
        content: this.translate.get('confirm_delete_port'),
      });
    } catch (e) {
      return;
    }

    try {
      await this.networkService.deleteFrontend(this.lbId, this.port);
      this.router.navigate(['/load_balancer/detail', this.loadBalancer.name]);
    } catch (e) {
      this.handleError(e);
    }
  }

  async deleteRule(rule: Rule) {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete_rule'),
        content: this.translate.get('confirm_delete_rule'),
      });
    } catch (e) {
      return;
    }

    try {
      await this.networkService.deleteRule(this.lbId, this.port, rule.rule_id);
      await this.fetchData();
    } catch (e) {
      this.handleError(e);
    }
  }

  handleError(e: any) {
    this.auiNotificationService.error(
      e.errors ? e.errors[0].message : e.message,
    );
  }
}
