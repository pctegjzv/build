import { NgModule } from '@angular/core';
import { LoadbalancerRoutingModule } from 'app2/features/network/loadbalancer.routing.module';
import { LoadBalancerDomainSuffixUpdateComponent } from 'app2/features/network/load_balancer/detail/load-balancer-domain-suffix-update.component';
import { SharedModule } from 'app2/shared/shared.module';

import { LoadBalancerDetailComponent } from './load_balancer/detail/load-balancer-detail.component';
import { LoadBalancerCreateComponent } from './load_balancer/list/dialog/create/load-balancer-create.component';
import { LoadBalancerImportComponent } from './load_balancer/list/dialog/import/load-balancer-import.component';
import { LoadBalancerListComponent } from './load_balancer/list/load-balancer-list.component';
import { LoadBalancerPortAddRuleComponent } from './load_balancer/port/load-balancer-port-add-rule.component';
import { LoadBalancerPortAddServiceComponent } from './load_balancer/port/load-balancer-port-add-service.component';
import { LoadBalancerPortSortRulesComponent } from './load_balancer/port/load-balancer-port-sort-rules.component';
import { LoadBalancerPortUpdateCertificateComponent } from './load_balancer/port/load-balancer-port-update-certificate.component';
import { LoadBalancerPortUpdateServiceComponent } from './load_balancer/port/load-balancer-port-update-service.component';
import { LoadBalancerPortComponent } from './load_balancer/port/load-balancer-port.component';

@NgModule({
  imports: [SharedModule, LoadbalancerRoutingModule],
  declarations: [
    LoadBalancerListComponent,
    LoadBalancerDetailComponent,
    LoadBalancerPortComponent,
    LoadBalancerPortAddRuleComponent,
    LoadBalancerPortAddServiceComponent,
    LoadBalancerPortUpdateCertificateComponent,
    LoadBalancerPortUpdateServiceComponent,
    LoadBalancerPortSortRulesComponent,
    LoadBalancerImportComponent,
    LoadBalancerCreateComponent,
    LoadBalancerDomainSuffixUpdateComponent,
  ],
  entryComponents: [
    LoadBalancerCreateComponent,
    LoadBalancerImportComponent,
    LoadBalancerPortAddServiceComponent,
    LoadBalancerPortAddRuleComponent,
    LoadBalancerPortUpdateCertificateComponent,
    LoadBalancerPortUpdateServiceComponent,
    LoadBalancerPortSortRulesComponent,
    LoadBalancerDomainSuffixUpdateComponent,
  ],
})
export class LoadbalancerModule {}
