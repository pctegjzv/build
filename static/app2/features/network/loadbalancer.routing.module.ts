import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoadBalancerDetailComponent } from './load_balancer/detail/load-balancer-detail.component';
import { LoadBalancerListComponent } from './load_balancer/list/load-balancer-list.component';
import { LoadBalancerPortComponent } from './load_balancer/port/load-balancer-port.component';

const routes: Routes = [
  {
    path: '',
    component: LoadBalancerListComponent,
    pathMatch: 'full',
  },
  {
    path: 'detail/:name',
    component: LoadBalancerDetailComponent,
  },
  {
    path: 'detail/:name/port/:port',
    component: LoadBalancerPortComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoadbalancerRoutingModule {}
