import { NgModule } from '@angular/core';

import { NetworkPolicyRoutingModule } from 'app2/features/network/network-policy.routing.module';
import { NetworkPolicyCreateDialogComponent } from 'app2/features/network/network_policy/create/network-policy-create-dialog.component';
import { NetworkPolicyCreateComponent } from 'app2/features/network/network_policy/create/network-policy-create.component';
import { NetworkPolicyDetailComponent } from 'app2/features/network/network_policy/detail/network-policy-detail.component';
import { NetworkPolicyListComponent } from 'app2/features/network/network_policy/list/network-policy-list.component';
import { SharedModule } from 'app2/shared/shared.module';

@NgModule({
  imports: [SharedModule, NetworkPolicyRoutingModule],
  declarations: [
    NetworkPolicyListComponent,
    NetworkPolicyCreateComponent,
    NetworkPolicyCreateDialogComponent,
    NetworkPolicyDetailComponent,
  ],
  entryComponents: [NetworkPolicyCreateDialogComponent],
})
export class NetworkPolicyModule {}
