import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NetworkPolicyCreateComponent } from 'app2/features/network/network_policy/create/network-policy-create.component';
import { NetworkPolicyDetailComponent } from 'app2/features/network/network_policy/detail/network-policy-detail.component';
import { NetworkPolicyListComponent } from 'app2/features/network/network_policy/list/network-policy-list.component';

const routes: Routes = [
  {
    path: '',
    component: NetworkPolicyListComponent,
    pathMatch: 'full',
  },
  {
    path: 'create',
    component: NetworkPolicyCreateComponent,
    pathMatch: 'full',
  },
  {
    path: 'detail/:cluster_id/:namespace/:name',
    component: NetworkPolicyDetailComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class NetworkPolicyRoutingModule {}
