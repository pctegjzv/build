import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService, NotificationService } from 'alauda-ui';
import { NetworkService } from 'app2/shared/services/features/network.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import * as jsyaml from 'js-yaml';

@Component({
  selector: 'rc-network-policy-create-dialog',
  templateUrl: './network-policy-create-dialog.component.html',
  styleUrls: ['./network-policy-create-dialog.component.scss'],
})
export class NetworkPolicyCreateDialogComponent implements OnInit, OnDestroy {
  @Output()
  finish = new EventEmitter<any>();
  @ViewChild('form')
  form: NgForm;
  readOnly = false;
  isUpdate = false;
  submitting = false;
  submitted = false;
  loadingYaml = false;
  model = {
    name: '',
    namespace: '',
    networkPolicyYaml: '',
    cluster_id: '',
  };
  networkPolicyYaml = '';
  uuid = '';

  constructor(
    private networkService: NetworkService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
    private modalService: ModalService,
    private router: Router,
  ) {}

  ngOnInit() {}
  ngOnDestroy() {}

  async initValue(initVal: any, readOnly: boolean, isUpdate: boolean) {
    this.readOnly = readOnly;
    this.isUpdate = isUpdate;
    this.loadingYaml = true;
    if (initVal) {
      this.model.name = initVal.name;
      this.model.namespace = initVal.namespace;
      this.model.cluster_id = initVal.cluster_id;
      let curYaml: any;
      if (!isUpdate) {
        curYaml = jsyaml.safeLoad(initVal.networkPolicyYaml);
        // rewrite name and namespace in yaml
        curYaml.metadata = {
          name: this.model.name,
          namespace: this.model.namespace,
        };
      } else {
        await this.networkService
          .getNetworkPolicy(initVal.cluster_id, initVal.namespace, initVal.name)
          .then((res: any) => {
            this.networkPolicyYaml = jsyaml.safeDump(res.kubernetes);
            this.uuid = res.kubernetes.metadata.uid;
          });
        curYaml = jsyaml.safeLoad(this.networkPolicyYaml);
      }
      this.model.networkPolicyYaml = jsyaml.safeDump(curYaml);
      this.loadingYaml = false;
    }
  }

  onSubmit() {
    this.submitted = true;
  }

  private complete(res?: any) {
    this.finish.next(res);
    this.finish.complete();
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    try {
      jsyaml.safeLoad(this.model.networkPolicyYaml);
    } catch (error) {
      // yaml parse error
      this.auiNotificationService.error({
        title: this.translateService.get('yaml_error'),
        content: error.message,
      });
      return;
    }
    if (this.isUpdate) {
      this.updateNetWorkPolicy();
    } else {
      this.createNetWorkPolicy();
    }
  }

  createNetWorkPolicy() {
    const data = jsyaml.safeLoad(this.model.networkPolicyYaml);
    this.submitting = true;
    this.networkService
      .createNetworkPolicy(this.model.cluster_id, data)
      .then(() => {
        this.auiMessageService.success({
          content: this.translateService.get('network_policy_create_success'),
        });
        this.submitting = false;
        this.complete();
        this.router.navigateByUrl(
          `network_policy/detail/${this.model.cluster_id}/${
            this.model.namespace
          }/${this.model.name}`,
        );
      })
      .catch(({ errors }) => {
        this.auiNotificationService.error({
          title: this.translateService.get('network_policy_create_failed'),
          content: errors[0].message,
        });
        this.submitting = false;
      });
  }

  updateNetWorkPolicy() {
    const data: any = jsyaml.safeLoad(this.model.networkPolicyYaml);
    // rewrite name and namespace before submit
    data.metadata = {
      name: this.model.name,
      namespace: this.model.namespace,
    };
    this.submitting = true;
    this.networkService
      .updateNetworkPolicy(
        this.model.cluster_id,
        this.model.namespace,
        this.model.name,
        data,
      )
      .then(() => {
        this.auiMessageService.success({
          content: this.translateService.get('network_policy_update_success'),
        });
        this.submitting = false;
        this.complete(true);
        this.router.navigateByUrl(
          `network_policy/detail/${this.model.cluster_id}/${
            this.model.namespace
          }/${this.model.name}`,
        );
      })
      .catch(({ errors }) => {
        this.auiNotificationService.error({
          title: this.translateService.get('network_policy_update_failed'),
          content: errors[0].message,
        });
        this.submitting = false;
        this.complete(true);
      });
  }
  cancel() {
    if (this.isUpdate && this.form.touched) {
      this.modalService
        .confirm({
          title: this.translateService.get(
            'network_policy_confirm_cancel_update',
            {
              name: this.model.name,
            },
          ),
        })
        .then(() => {
          this.complete();
        })
        .catch(() => {});
    } else {
      this.complete();
    }
  }

  trackByIndex(index: number) {
    return index;
  }
}
