import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import {
  NamespaceOption,
  NamespaceService,
} from 'app2/shared/services/features/namespace.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import * as jsyaml from 'js-yaml';
import { Subscription } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import { NetworkPolicyCreateDialogComponent } from './network-policy-create-dialog.component';

@Component({
  templateUrl: './network-policy-create.component.html',
  styleUrls: ['./network-policy-create.component.scss'],
})
export class NetworkPolicyCreateComponent implements OnInit, OnDestroy {
  @ViewChild('createForm')
  form: NgForm;
  model: any;
  namespaceOptions: NamespaceOption[] = [];
  submitting = false;
  private regionSubscription: Subscription;

  constructor(
    private namespaceService: NamespaceService,
    private regionService: RegionService,
    private modalService: ModalService,
    private translate: TranslateService,
    private router: Router,
    private auiNotificationService: NotificationService,
  ) {
    this.model = {
      name: '',
      namespace: '',
      networkPolicyYaml: '',
      cluster_id: '',
    };
  }

  ngOnInit() {
    this.regionSubscription = this.regionService.region$
      .pipe(filter(r => !!r))
      .subscribe(async region => {
        this.model.cluster_id = region.id;
        this.model.namespace = '';
        this.namespaceOptions = await this.namespaceService.getNamespaceOptions(
          region.id,
        );
      });
  }

  ngOnDestroy() {
    this.regionSubscription.unsubscribe();
  }

  cancel() {
    if (this.model.networkPolicyYaml) {
      try {
        this.modalService
          .confirm({
            title: this.translate.get('network_policy_confirm_cancel_create', {
              name: this.model.name,
            }),
          })
          .then(() => {
            this.router.navigateByUrl('network_policy');
          })
          .catch(() => {});
      } catch (e) {}
    } else {
      this.router.navigateByUrl('network_policy');
    }
  }

  async confirm() {
    if (this.form.valid && this.model.namespace && !this.submitting) {
      try {
        jsyaml.safeLoad(this.model.networkPolicyYaml);
      } catch (error) {
        // yaml parse error
        this.auiNotificationService.error({
          title: this.translate.get('yaml_error'),
          content: error.message,
        });
        return;
      }
      this.openPreviewModal(this.model);
    }
  }

  async openPreviewModal(initVal: any) {
    try {
      const modalRef = await this.modalService.open(
        NetworkPolicyCreateDialogComponent,
        {
          title: this.translate.get(`network_policy_create`),
        },
      );
      modalRef.componentInstance.initValue(initVal, true, false);
      modalRef.componentInstance.finish.pipe(first()).subscribe(() => {
        modalRef.close();
      });
    } catch (error) {}
  }
}
