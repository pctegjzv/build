import { ChangeDetectorRef } from '@angular/core';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as jsyaml from 'js-yaml';
import { Subscription } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import { MessageService, NotificationService } from 'alauda-ui';
import { NetworkService } from 'app2/shared/services/features/network.service';
import { NetworkUtilitiesService } from 'app2/shared/services/features/network.utilities.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ModalMode } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';

import { NetworkPolicyCreateDialogComponent } from '../create/network-policy-create-dialog.component';

@Component({
  templateUrl: './network-policy-detail.component.html',
  styleUrls: ['./network-policy-detail.component.scss'],
})
export class NetworkPolicyDetailComponent implements OnInit, OnDestroy {
  private routeParamsSubscription: Subscription;
  private pollingTimer: any;
  private _loading = false;
  private _loadError: any;
  private namespace: string;
  private name: string;
  private cluster_id: string;

  networkPolicyData: any;
  networkPolicyYaml: string;
  uuid: string;
  networkPolicyTipClosed = false;
  regionSubscription: Subscription;
  region: Region;

  destroyed = false;
  initialized = false;

  constructor(
    private route: ActivatedRoute,
    private networkService: NetworkService,
    public networkUtilities: NetworkUtilitiesService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
    private modalService: ModalService,
    private regionService: RegionService,
    private regionUtilities: RegionUtilitiesService,
    private router: Router,
  ) {}

  ngOnInit() {
    this.routeParamsSubscription = this.route.params
      .pipe(first())
      .subscribe(async params => {
        this.cluster_id = params['cluster_id'];
        this.namespace = params['namespace'];
        this.name = params['name'];
        await this.refetch();
        this.initialized = true;
      });
    this.regionSubscription = this.regionService.region$
      .pipe(filter(region => !!region))
      .subscribe(region => {
        this.region = region;
        if (!this.regionUtilities.isNetworkPolicyRegion(this.region)) {
          this.router.navigateByUrl('home');
        }
      });
  }

  ngOnDestroy() {
    this.routeParamsSubscription.unsubscribe();
    this.regionSubscription.unsubscribe();
    clearTimeout(this.pollingTimer);
    this.destroyed = true;
  }

  async refetch() {
    if (this.destroyed) {
      return;
    }
    this._loading = true;
    try {
      await this.networkService
        .getNetworkPolicy(this.cluster_id, this.namespace, this.name)
        .then((res: any) => {
          this.networkPolicyData = res;
          this.networkPolicyYaml = jsyaml.safeDump(res.kubernetes);
          this.uuid = `${this.cluster_id}_networkpolicies_${this.namespace}_${
            this.name
          }`;
        })
        .catch(({ status, errors }) => {
          this._loadError = errors;
          if (status === 403) {
            this.auiNotificationService.error({
              title: this.translate.get('permission_denied'),
            });
            this.router.navigateByUrl('network_policy');
          } else if (status === 404) {
            this.auiNotificationService.error({
              title: this.translate.get('network_policy_not_exist'),
            });
            this.router.navigateByUrl('network_policy');
          }
        });
      this._loadError = null;
    } catch (e) {}

    this._loading = false;
    if (!this.destroyed) {
      this.cdr.detectChanges();
    }
  }

  get empty() {
    return !this.networkPolicyData;
  }

  get loading() {
    return this._loading;
  }

  get loadError() {
    return this._loadError;
  }

  async updateNetworkPolicy(init: any) {
    const initVal = {
      name: init.kubernetes.metadata.name,
      namespace: init.kubernetes.metadata.namespace,
      networkPolicyYaml: jsyaml.safeDump(init.kubernetes),
      cluster_id: this.cluster_id,
    };
    try {
      const modalRef = await this.modalService.open(
        NetworkPolicyCreateDialogComponent,
        {
          title: this.translate.get(`network_policy_update`),
          mode: ModalMode.RIGHT_SLIDER,
          width: 900,
        },
      );
      modalRef.componentInstance.initValue(initVal, false, true);
      modalRef.componentInstance.finish.pipe(first()).subscribe((res?: any) => {
        if (res) {
          this.refetch();
        }
        modalRef.close();
      });
    } catch (e) {}
  }

  async deleteNetworkPolicy(np: any) {
    this.modalService
      .confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('network_policy_delete_confirm', {
          name: np.kubernetes.metadata.name,
        }),
      })
      .then(() => {
        this.networkService
          .deleteNetworkPolicy(
            this.region.id,
            np.kubernetes.metadata.namespace,
            np.kubernetes.metadata.name,
          )
          .then(() => {
            this.auiMessageService.success({
              content: this.translate.get('network_policy_delete_success'),
            });
            this.router.navigateByUrl('network_policy');
          })
          .catch(({ errors }) => {
            if (errors.length) {
              this.auiNotificationService.error({
                title: this.translate.get('network_policy_delete_failed'),
                content: errors[0].message,
              });
            }
          });
      })
      .catch(() => {});
  }

  openNetworkPolicyTip() {
    this.networkPolicyTipClosed = false;
  }
  onNetworkPolicyTipClosed() {
    this.networkPolicyTipClosed = true;
  }
}
