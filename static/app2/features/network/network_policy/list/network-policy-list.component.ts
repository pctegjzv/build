import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService, NotificationService } from 'alauda-ui';
import { TableComponent } from 'app2/shared/components/table/table.component';
import {
  NetworkPolicy,
  NetworkService,
} from 'app2/shared/services/features/network.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ModalMode } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';
import * as jsyaml from 'js-yaml';
import { first } from 'rxjs/operators';

import { NetworkPolicyCreateDialogComponent } from '../create/network-policy-create-dialog.component';

@Component({
  templateUrl: './network-policy-list.component.html',
  styleUrls: ['./network-policy-list.component.scss'],
  providers: [NetworkService],
})
export class NetworkPolicyListComponent implements OnInit {
  @ViewChild('npTable')
  table: TableComponent;
  networkPolicyList: NetworkPolicy[];
  empty: boolean;
  loading: boolean;
  loadError: boolean;
  region: Region;
  filterKey = '';
  networkPolicyCreateEnabled = false;
  networkPolicyTipClosed = false;

  constructor(
    private regionService: RegionService,
    private modalService: ModalService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private roleUtil: RoleUtilitiesService,
    private translateService: TranslateService,
    public networkService: NetworkService,
    private regionUtilities: RegionUtilitiesService,
    private router: Router,
  ) {
    this.empty = false;
    this.loading = this.loadError = false;
    this.networkPolicyList = [];
  }

  async ngOnInit() {
    this.region = await this.regionService.getCurrentRegion();
    if (this.regionUtilities.isNetworkPolicyRegion(this.region)) {
      this.fetch(this.region.id);
      if (this.region) {
        this.roleUtil
          .resourceTypeSupportPermissions(
            'k8s_networkpolicies',
            { cluster_name: this.region.id },
            'create',
          )
          .then((res: boolean) => {
            this.networkPolicyCreateEnabled = res;
          });
      } else {
        this.networkPolicyCreateEnabled = false;
      }
    } else {
      this.router.navigateByUrl('home');
    }
  }

  ngOnDestory() {}

  fetch(region_id: string) {
    this.empty = true;
    this.loading = true;
    this.networkService
      .getNetworkPolicies(region_id)
      .then((res: any) => {
        this.empty = !res.result.length;
        this.networkPolicyList = res.result ? res.result : [];
      })
      .catch(() => {
        this.loadError = true;
      })
      .then(() => {
        this.loading = false;
      });
  }

  refetch() {
    this.fetch(this.region.id);
  }

  searchChanged(event: string) {
    this.filterKey = event;
  }

  create() {
    this.router.navigateByUrl('network_policy/create');
  }

  getNetworkPolicyList() {
    return this.networkPolicyList.filter(
      item => item.kubernetes.metadata.name.indexOf(this.filterKey) !== -1,
    );
  }

  viewNetworkPolicy(np: NetworkPolicy) {
    this.router.navigateByUrl(
      `network_policy/detail/${this.region.id}/${
        np.kubernetes.metadata.namespace
      }/${np.kubernetes.metadata.name}`,
    );
  }

  deleteNetworkPolicy(np: NetworkPolicy) {
    this.modalService
      .confirm({
        title: this.translateService.get('delete'),
        content: this.translateService.get('network_policy_delete_confirm', {
          name: np.kubernetes.metadata.name,
        }),
      })
      .then(() => {
        this.networkService
          .deleteNetworkPolicy(
            this.region.id,
            np.kubernetes.metadata.namespace,
            np.kubernetes.metadata.name,
          )
          .then(() => {
            this.auiMessageService.success({
              content: this.translateService.get(
                'network_policy_delete_success',
              ),
            });
            this.refetch();
          })
          .catch(({ errors }) => {
            if (errors.length) {
              this.auiNotificationService.error({
                title: this.translateService.get(
                  'network_policy_delete_failed',
                ),
                content: errors[0].message,
              });
            }
          });
      })
      .catch(() => {});
  }

  async updateNetworkPolicy(init: NetworkPolicy) {
    const initVal = {
      name: init.kubernetes.metadata.name,
      namespace: init.kubernetes.metadata.namespace,
      networkPolicyYaml: jsyaml.safeDump(init.kubernetes),
      cluster_id: this.region.id,
    };
    try {
      const modalRef = await this.modalService.open(
        NetworkPolicyCreateDialogComponent,
        {
          title: this.translateService.get(`network_policy_update`),
          mode: ModalMode.RIGHT_SLIDER,
          width: 900,
        },
      );
      modalRef.componentInstance.initValue(initVal, false, true);
      modalRef.componentInstance.finish
        .pipe(first())
        .subscribe(() => modalRef.close());
    } catch (e) {}
  }

  openNetworkPolicyTip() {
    this.networkPolicyTipClosed = false;
    // 滚动到顶部
  }
  onNetworkPolicyTipClosed() {
    this.networkPolicyTipClosed = true;
  }
}
