import { NgModule } from '@angular/core';

import { SubnetRoutingModule } from 'app2/features/network/subnet.routing.module';
import { SubnetDailogComponent } from 'app2/features/network/subnet/subnet-dailog.component';
import { SubnetDetailComponent } from 'app2/features/network/subnet/subnet-detail.component';
import { SubnetImportIpComponent } from 'app2/features/network/subnet/subnet-import-ip.component';
import { SubnetListComponent } from 'app2/features/network/subnet/subnet-list.component';
import { SubnetUpdateComponent } from 'app2/features/network/subnet/subnet-update.component';
import { SharedModule } from 'app2/shared/shared.module';

@NgModule({
  imports: [SharedModule, SubnetRoutingModule],
  declarations: [
    SubnetDailogComponent,
    SubnetDetailComponent,
    SubnetListComponent,
    SubnetImportIpComponent,
    SubnetUpdateComponent,
  ],
  entryComponents: [
    SubnetDailogComponent,
    SubnetImportIpComponent,
    SubnetUpdateComponent,
  ],
})
export class SubnetModule {}
