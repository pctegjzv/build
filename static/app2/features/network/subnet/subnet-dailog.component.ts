import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { NotificationService } from 'alauda-ui';
import {
  NetworkService,
  SubnetRaw,
} from 'app2/shared/services/features/network.service';
import { QuotaSpaceService } from 'app2/shared/services/features/quota-space.service';
import { Region } from 'app2/shared/services/features/region.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';

@Component({
  templateUrl: './subnet-dailog.component.html',
  styleUrls: ['./subnet-dailog.component.scss'],
})
export class SubnetDailogComponent implements OnInit {
  confirmed: EventEmitter<any> = new EventEmitter();
  model: SubnetRaw;
  spaces: any[];
  submitting = false;
  newSubnetApiEnabled = false;
  @Input()
  region: Region;
  @ViewChild('createForm')
  form: NgForm;

  constructor(
    private networkService: NetworkService,
    private auiNotificationService: NotificationService,
    private quotaSpaceService: QuotaSpaceService,
    private modal: ModalService,
  ) {
    this.model = {
      region_name: '',
      subnet_name: '',
      gateway: '',
      cidr_block: '',
      space_name: '',
      is_default: true,
    };
  }

  async ngOnInit() {
    this.newSubnetApiEnabled = await this.networkService.subnetNewApiEnabled(
      this.region.id,
    );
    this.model.region_name = this.region.name;
    this.quotaSpaceService.getConsumableSpaces().then(sp => {
      this.spaces = sp;
    });
  }

  async submit() {
    if (this.form.valid && !this.submitting) {
      this.submitting = true;
      if (this.newSubnetApiEnabled) {
        try {
          await this.networkService.createK8sSubnet(this.region.id, {
            name: this.model.subnet_name,
            cidr: this.model.cidr_block,
            gateway: this.model.gateway,
          });
          this.done(true);
        } catch ({ errors }) {
          if (errors) {
            this.auiNotificationService.error(errors[0].message);
          }
        }
      } else {
        await this.networkService
          .createSubnet(this.model)
          .then(() => {
            this.done(true);
          })
          .catch(({ errors }) => {
            if (errors) {
              this.auiNotificationService.error(errors[0].message);
            }
          })
          .then(() => {
            this.submitting = false;
          });
      }
      this.confirmed.next(true);
    }
  }

  cancel() {
    this.modal.closeAll();
  }

  private done(res?: any) {
    this.confirmed.next(res);
    this.confirmed.complete();
  }
}
