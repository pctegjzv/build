import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, Subject, Subscription, from, merge } from 'rxjs';
import {
  filter,
  map,
  publishReplay,
  refCount,
  switchMap,
  tap,
} from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { SubnetImportIpComponent } from 'app2/features/network/subnet/subnet-import-ip.component';
import { SubnetUpdateComponent } from 'app2/features/network/subnet/subnet-update.component';
import {
  K8sSubnetIp,
  NetworkService,
  Subnet,
} from 'app2/shared/services/features/network.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './subnet-detail.component.html',
  styleUrls: ['./subnet-detail.component.scss'],
})
export class SubnetDetailComponent implements OnInit, OnDestroy {
  detail$: Observable<any>;
  detail: any;
  region: Region;
  update$: Subject<any> = new Subject();
  deleteIp$: Subject<any> = new Subject();
  import$: Subject<any> = new Subject();
  regionSubscription: Subscription;

  private _subnet_name: string;
  newSubnetApiEnabled = false;
  loading = false;
  ips: K8sSubnetIp[];
  ips_unused: K8sSubnetIp[];

  constructor(
    private route$: ActivatedRoute,
    private networkService: NetworkService,
    private modalService: ModalService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private router: Router,
    private regionService: RegionService,
  ) {}

  async ngOnInit() {
    this.regionSubscription = this.regionService.region$
      .pipe(filter(r => !!r))
      .subscribe((r: Region) => {
        this.region = r;
      });
    this.newSubnetApiEnabled = await this.networkService.subnetNewApiEnabled(
      this.region.id,
    );
    const updateObservable = this.update$.pipe(
      switchMap(() => {
        const modelRef = this.modalService.open(SubnetUpdateComponent, {
          title: this.translate.get('update'),
        });
        modelRef.componentInstance.subnet = this.detail;
        modelRef.componentInstance.region = this.region;
        return modelRef.componentInstance.finished;
      }),
      map((res: any) => {
        if (res) {
          this.modalService.closeAll();
        }
      }),
    );

    const importObservable = this.import$.pipe(
      switchMap(() => {
        const modelRef = this.modalService.open(SubnetImportIpComponent, {
          title: this.translate.get('import'),
        });
        modelRef.componentInstance.subnet_name = this._subnet_name;
        modelRef.componentInstance.region = this.region;
        return modelRef.componentInstance.finished;
      }),
      map((res: any) => {
        if (res) {
          this.modalService.closeAll();
        }
      }),
    );

    const deleteIPObservable = this.deleteIp$.pipe(
      switchMap((ip_name: string) => {
        return from(
          this.modalService
            .confirm({
              title: this.translate.get('delete'),
              content: this.translate.get('subnet_ip_delete', {
                address: ip_name,
              }),
            })
            .then(() => {
              this.networkService
                .deleteSubnetIp(this._subnet_name, ip_name)
                .catch(({ errors }) => {
                  if (errors) {
                    this.auiNotificationService.error(errors[0].message);
                  }
                });
            })
            .catch(() => {}),
        );
      }),
    );

    const deleteK8sIPObservable = this.deleteIp$.pipe(
      switchMap((ip_name: string) => {
        return from(
          this.modalService
            .confirm({
              title: this.translate.get('delete'),
              content: this.translate.get('subnet_ip_delete', {
                address: ip_name,
              }),
            })
            .then(() => {
              this.networkService
                .deleteK8sSubnetIp(this.region.id, this._subnet_name, ip_name)
                .catch(({ errors }) => {
                  if (errors) {
                    this.auiNotificationService.error(errors[0].message);
                  }
                });
            })
            .catch(() => {}),
        );
      }),
    );

    const routeObservable = this.route$.paramMap.pipe(
      map((param: ParamMap) => {
        this._subnet_name = param.get('name');
        return this._subnet_name;
      }),
    );

    if (this.newSubnetApiEnabled) {
      this.detail$ = merge(
        routeObservable,
        updateObservable,
        importObservable,
        deleteK8sIPObservable,
      ).pipe(
        switchMap(() => {
          this.loading = true;
          return from(
            Promise.all([
              this.networkService.getK8sSubnet(
                this.region.id,
                this._subnet_name,
              ),
              this.networkService.getK8sSubnetIps(
                this.region.id,
                this._subnet_name,
              ),
            ])
              .then(([detail, ips]) => {
                this.loading = false;
                this.ips = ips;
                this.ips_unused = ips.filter(ip => !ip.kubernetes.spec.used);
                return detail;
              })
              .catch(() => {
                this.loading = false;
              }),
          );
        }),
        tap(detail => {
          this.detail = detail;
        }),
        filter(e => !!e),
        publishReplay(1),
        refCount(),
      );
    } else {
      this.detail$ = merge(
        routeObservable,
        updateObservable,
        importObservable,
        deleteIPObservable,
      ).pipe(
        switchMap(() => {
          this.loading = true;
          return from(
            Promise.all([
              this.networkService.getSubnet(this._subnet_name),
              this.networkService.getSubnetIps(this._subnet_name),
            ])
              .then(([detail, ips]) => {
                this.loading = false;
                detail['ips'] = ips;
                detail['ip_unused'] = ips.filter(ip => !ip.used);
                return detail;
              })
              .catch(() => {
                this.loading = false;
              }),
          );
        }),
        tap(detail => {
          this.detail = detail;
        }),
        filter(e => !!e),
        publishReplay(1),
        refCount(),
      );
    }
  }

  deleteSubnet() {
    this.modalService
      .confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('subnet_delete', {
          name: this._subnet_name,
        }),
      })
      .then(() => {
        if (this.newSubnetApiEnabled) {
          this.networkService
            .deleteK8sSubnet(this.region.id, this._subnet_name)
            .then(() => {
              this.router.navigateByUrl('subnet');
            })
            .catch(({ errors }) => {
              if (errors) {
                this.auiNotificationService.error(errors[0].message);
              }
            });
        } else {
          this.networkService
            .deleteSubnet(this._subnet_name)
            .then(() => {
              this.router.navigateByUrl('subnet');
            })
            .catch(({ errors }) => {
              if (errors) {
                this.auiNotificationService.error(errors[0].message);
              }
            });
        }
      })
      .catch(() => {});
  }

  buttonDisplay(data: Subnet, action: string) {
    return this.networkService.canAction(data, action || 'view', 'subnet');
  }

  ngOnDestroy() {
    this.regionSubscription.unsubscribe();
  }
}
