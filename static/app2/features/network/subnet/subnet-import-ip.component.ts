import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { NotificationService } from 'alauda-ui';
import { NetworkService } from 'app2/shared/services/features/network.service';
import { Region } from 'app2/shared/services/features/region.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './subnet-import-ip.component.html',
  styleUrls: ['./subnet-update.component.scss'],
})
export class SubnetImportIpComponent implements OnInit {
  @Output()
  finished = new EventEmitter<any>();
  @ViewChild('importForm')
  form: NgForm;
  @Input()
  subnet_name: string;
  @Input()
  region: Region;

  importMethodOptions: any[];
  importMethod = 'range';
  range: object;
  ipList: string[];
  submitting = false;
  newSubnetApiEnabled = false;
  constructor(
    private auiNotificationService: NotificationService,
    private networkService: NetworkService,
    private modalService: ModalService,
    private translate: TranslateService,
  ) {
    this.importMethodOptions = [
      { name: this.translate.get('range'), value: 'range' },
      { name: this.translate.get('list'), value: 'list' },
    ];
    this.range = {
      address_start: '',
      address_end: '',
    };
  }

  async ngOnInit() {
    this.newSubnetApiEnabled = await this.networkService.subnetNewApiEnabled(
      this.region.id,
    );
  }

  cancel() {
    this.modalService.closeAll();
  }

  async submit() {
    if (this.form.valid && !this.submitting) {
      this.submitting = true;
      const data = {};
      if (this.importMethod === 'range') {
        data['address_range'] = this.range;
      } else if (this.importMethod === 'list') {
        data['address_list'] = this.ipList;
      }
      if (this.newSubnetApiEnabled) {
        await this.networkService
          .importK8sSubnetIp(this.region.id, this.subnet_name, data)
          .then(() => {
            this.done(true);
          })
          .catch(({ errors }) => {
            if (errors) {
              this.auiNotificationService.error(errors[0].message);
            }
          })
          .then(() => {
            this.submitting = false;
          });
      } else {
        await this.networkService
          .importSubnetIp(this.subnet_name, data)
          .then(() => {
            this.done(true);
          })
          .catch(({ errors }) => {
            if (errors) {
              this.auiNotificationService.error(errors[0].message);
            }
          })
          .then(() => {
            this.submitting = false;
          });
      }
      this.finished.next(true);
    }
  }

  private done(res?: any) {
    this.finished.next(res);
    this.finished.complete();
  }
}
