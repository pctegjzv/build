import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { get } from 'lodash';
import {
  BehaviorSubject,
  Observable,
  Subject,
  Subscription,
  combineLatest,
  from,
  merge,
} from 'rxjs';
import { filter, map, mergeMap, share, switchMap } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';
import { SubnetDailogComponent } from 'app2/features/network/subnet/subnet-dailog.component';
import { SubnetUpdateComponent } from 'app2/features/network/subnet/subnet-update.component';
import {
  K8sSubnet,
  NetworkService,
  Subnet,
} from 'app2/shared/services/features/network.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './subnet-list.component.html',
  styleUrls: ['./subnet-list.component.scss'],
})
export class SubnetListComponent implements OnInit {
  items$: Observable<any>;
  delete$: Subject<any> = new Subject();
  create$: Subject<any> = new Subject();
  update$: Subject<any> = new Subject();
  itemsSubscription: Subscription;

  private _region: Region;
  regionSubscription: Subscription;
  subnetCanCreate = false;
  regionSubnetEnabled = false;
  newSubnetApiEnabled = false;
  filterKey$: BehaviorSubject<string> = new BehaviorSubject('');
  subnets: Subnet[] | K8sSubnet[];

  loading = false;
  initialized = false;

  constructor(
    private regionService: RegionService,
    private networkService: NetworkService,
    private auiNotificationService: NotificationService,
    private modalService: ModalService,
    private translate: TranslateService,
    private role: RoleUtilitiesService,
    private router: Router,
    @Inject(WEBLABS) public weblabs: Weblabs,
  ) {}

  async ngOnInit() {
    this.regionSubscription = this.regionService.region$
      .pipe(filter(region => !!region))
      .subscribe(async region => {
        this._region = region;
        this.regionSubnetEnabled = get(
          region,
          'features.service.features',
          [],
        ).includes('subnet');
        this.subnetCanCreate = await this.role.resourceTypeSupportPermissions(
          'subnet',
          { cluster_name: this._region.name },
        );
        this.newSubnetApiEnabled = await this.networkService.subnetNewApiEnabled(
          this._region.id,
        );
        this.loading = true;
        this.newSubnetApiEnabled
          ? this.initK8sSubnetData()
          : this.initSubnetData();
      });
  }

  private initK8sSubnetData() {
    const items = merge(
      this.regionService.region$,
      this.delete$.pipe(
        switchMap((sb: K8sSubnet) => {
          return from(
            this.modalService
              .confirm({
                title: this.translate.get('delete'),
                content: this.translate.get('subnet_delete', {
                  name: sb.kubernetes.metadata.name,
                }),
              })
              .then(() => sb),
          );
        }),
        switchMap((sb: K8sSubnet) => {
          return from(
            this.networkService
              .deleteK8sSubnet(this._region.id, sb.kubernetes.metadata.name)
              .catch(({ errors }) => {
                if (errors) {
                  this.auiNotificationService.error(errors[0].message);
                }
              }),
          );
        }),
      ),
      this.update$.pipe(
        mergeMap((rowData: K8sSubnet) => {
          const ref = this.modalService.open(SubnetUpdateComponent, {
            title: this.translate.get('update_subnet'),
          });
          ref.componentInstance.subnet = rowData;
          ref.componentInstance.region = this._region;
          return ref.componentInstance.finished;
        }),
        map(() => this.modalService.closeAll()),
      ),
      this.create$.pipe(
        mergeMap(() => {
          const ref = this.modalService.open(SubnetDailogComponent, {
            title: this.translate.get('add_subnet'),
          });
          ref.componentInstance.region = this._region;
          return ref.componentInstance.confirmed;
        }),
        map(() => this.modalService.closeAll()),
      ),
    ).pipe(
      switchMap(() => this.getK8sItemsObservable()),
      filter(e => !!e),
      share(),
    );

    this.itemsSubscription = combineLatest(items, this.filterKey$)
      .pipe(
        map(([item1, filterkey]: [K8sSubnet[], string]) =>
          item1.filter((s: K8sSubnet) =>
            s.kubernetes.metadata.name.includes(filterkey),
          ),
        ),
      )
      .subscribe((items: K8sSubnet[]) => {
        this.subnets = items;
      });
  }

  private initSubnetData() {
    const items = merge(
      this.regionService.region$,
      this.delete$.pipe(
        switchMap((sb: Subnet) =>
          from(
            this.modalService
              .confirm({
                title: this.translate.get('delete'),
                content: this.translate.get('subnet_delete', {
                  name: sb.subnet_name,
                }),
              })
              .then(() => sb),
          ),
        ),
        switchMap((sb: Subnet) =>
          from(
            this.networkService
              .deleteSubnet(sb.subnet_name)
              .catch(({ errors }) => {
                if (errors) {
                  this.auiNotificationService.error(errors[0].message);
                }
              }),
          ),
        ),
      ),
      this.update$.pipe(
        mergeMap((rowData: Subnet) => {
          const ref = this.modalService.open(SubnetUpdateComponent, {
            title: this.translate.get('update_subnet'),
          });
          ref.componentInstance.subnet = rowData;
          ref.componentInstance.region = this._region;
          return ref.componentInstance.finished;
        }),
        map(() => this.modalService.closeAll()),
      ),
      this.create$.pipe(
        mergeMap(() => {
          const ref = this.modalService.open(SubnetDailogComponent, {
            title: this.translate.get('add_subnet'),
          });
          ref.componentInstance.region = this._region;
          return ref.componentInstance.confirmed;
        }),
        map(() => this.modalService.closeAll()),
      ),
    ).pipe(
      switchMap(() => this.getItemsObservable()),
      filter(e => !!e),
      share(),
    );

    this.itemsSubscription = combineLatest(items, this.filterKey$)
      .pipe(
        map(([item1, filterkey]: [Subnet[], string]) =>
          item1.filter((s: Subnet) => s.subnet_name.includes(filterkey)),
        ),
      )
      .subscribe((items: Subnet[]) => {
        this.subnets = items;
      });
  }

  getItemsObservable() {
    return from(
      this.networkService
        .getSubnets(this._region.name || '')
        .then(res => {
          this.loading = false;
          this.initialized = true;
          return res;
        })
        .catch(() => {
          this.loading = false;
          this.initialized = true;
          return [];
        }),
    );
  }

  getK8sItemsObservable() {
    return from(
      this.networkService
        .getK8sSubnets(this._region.id)
        .then(res => {
          this.loading = false;
          this.initialized = true;
          return res;
        })
        .catch(() => {
          this.initialized = true;
          this.loading = false;
          return [];
        }),
    );
  }

  view(sb: Subnet) {
    this.router.navigateByUrl(`subnet/detail/${sb.subnet_name}`);
  }

  viewK8sSubnet(sb: K8sSubnet) {
    this.router.navigateByUrl(`subnet/detail/${sb.kubernetes.metadata.name}`);
  }

  buttonDisplay(data: Subnet | K8sSubnet, action: string) {
    return this.networkService.canAction(data, action || 'view', 'subnet');
  }
}
