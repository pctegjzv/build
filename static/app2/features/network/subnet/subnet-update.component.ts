import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { NotificationService } from 'alauda-ui';
import { NetworkService } from 'app2/shared/services/features/network.service';
import { Region } from 'app2/shared/services/features/region.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';

@Component({
  templateUrl: './subnet-update.component.html',
  styleUrls: ['./subnet-update.component.scss'],
})
export class SubnetUpdateComponent implements OnInit {
  @Output()
  finished = new EventEmitter<any>();
  @ViewChild('updateForm')
  form: NgForm;
  @Input()
  subnet: any = {};
  @Input()
  region: Region;
  submitting = false;
  model: any = { cidr_block: '', gateway: '' };
  newSubnetApiEnabled = false;
  constructor(
    private networkService: NetworkService,
    private modalService: ModalService,
    private auiNotificationService: NotificationService,
  ) {}

  async ngOnInit() {
    this.newSubnetApiEnabled = await this.networkService.subnetNewApiEnabled(
      this.region.id,
    );
    if (this.newSubnetApiEnabled) {
      this.model = {
        cidr_block: this.subnet.kubernetes.spec.cidr_block || '',
        gateway: this.subnet.kubernetes.spec.gateway || '',
      };
    } else {
      this.model = {
        cidr_block: this.subnet.cidr_block || '',
        gateway: this.subnet.gateway || '',
      };
    }
  }

  cancel() {
    this.modalService.closeAll();
  }

  async submit() {
    if (this.form.valid && !this.submitting) {
      this.submitting = true;
      if (this.newSubnetApiEnabled) {
        await this.networkService
          .updateK8sSubnet(
            this.region.id,
            this.subnet.kubernetes.metadata.name,
            {
              cidr: this.model.cidr_block,
              gateway: this.model.gateway,
            },
          )
          .then(() => {
            this.done(true);
          })
          .catch(({ errors }) => {
            if (errors) {
              this.auiNotificationService.error(errors[0].message);
            }
          })
          .then(() => {
            this.submitting = false;
          });
      } else {
        await this.networkService
          .updateSubnet(this.subnet.subnet_name, this.model)
          .then(() => {
            this.done(true);
          })
          .catch(({ errors }) => {
            if (errors) {
              this.auiNotificationService.error(errors[0].message);
            }
          })
          .then(() => {
            this.submitting = false;
          });
      }
      this.finished.next(true);
    }
  }

  private done(res?: any) {
    this.finished.next(res);
    this.finished.complete();
  }
}
