import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { NotificationService as AuiNotificationService } from 'alauda-ui';
import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';
import {
  NotificationPayload,
  NotificationService,
  NotificationSubscription,
} from 'app2/shared/services/features/notification.service';
import { QuotaSpaceService } from 'app2/shared/services/features/quota-space.service';
import { TranslateService } from 'app2/translate/translate.service';
import { uniqBy } from 'lodash';

@Component({
  selector: 'rc-create-notification',
  templateUrl: './create-notification.component.html',
  styleUrls: ['./create-notification.component.scss'],
})
export class CreateNotificationComponent implements OnInit {
  @Output()
  finish = new EventEmitter<any>();
  form: FormGroup;
  initialized = false;
  submitting = false;
  updating = false;
  spaces: any[] = [];
  subMethods: any[];
  defaultSub: any = {
    method: '',
    recipient: '',
    remark: '',
  };
  initialSubs: FormGroup[];
  notification: any;
  errorMappers: any = {};
  patternMap = {
    name: { reg: /^[a-z][a-z0-9\-\_]*$/, hint: 'notification_name_hint' },
    email: { reg: /^.+@.+\..+$/, hint: 'email_format_limit' },
    sms: { reg: /^\+{0,1}[0-9]{7,15}$/, hint: 'phone_format_limit' },
    call: { reg: /^\+{0,1}[0-9]{7,15}$/, hint: 'phone_format_limit' },
    webhook: { reg: /^.*$/, hint: '' },
    dingtalk: { reg: /^[a-z0-9]{64}$/, hint: 'dingtalk_format_hint' },
  };
  constructor(
    private fb: FormBuilder,
    private auiNotificationService: AuiNotificationService,
    private translate: TranslateService,
    private quotaSpace: QuotaSpaceService,
    private notificationService: NotificationService,
    @Inject(WEBLABS) public weblabs: Weblabs,
  ) {
    this.subMethods = [
      {
        name: this.translate.get('email'),
        value: 'email',
      },
      {
        name: this.translate.get('sms'),
        value: 'sms',
      },
      {
        name: this.translate.get('notification_type_webhook'),
        value: 'webhook',
      },
      {
        name: this.translate.get('dingtalk'),
        value: 'dingtalk',
      },
    ];
    this.initForm();
  }

  async ngOnInit() {
    await this.initSpaces();
    this.initialized = true;
  }

  // Invoked when updating
  initNotification(notification: any) {
    this.notification = notification;
    this.initialSubs = notification.subscriptions.map(
      (sub: NotificationSubscription) => {
        return this.createSub(sub);
      },
    );
    this.form = this.fb.group({
      subs: this.fb.array(this.initialSubs),
    });
    this.updating = true;
  }

  initForm() {
    this.errorMappers = {
      name: { pattern: this.translate.get(this.patternMap.name.hint) },
      email: { pattern: this.translate.get(this.patternMap.email.hint) },
      sms: { pattern: this.translate.get(this.patternMap.sms.hint) },
      call: { pattern: this.translate.get(this.patternMap.sms.hint) },
      webhook: { pattern: '' },
      dingtalk: { pattern: this.translate.get(this.patternMap.dingtalk.hint) },
    };
    this.form = this.fb.group({
      name: '',
      space: '',
      subs: this.fb.array([this.createSub()]),
    });
  }

  createSub(sub = this.defaultSub): FormGroup {
    const subFormGroup: FormGroup = this.fb.group({
      method: sub.method,
      recipient: sub.recipient,
      // Validators.pattern(this.patternMap[sub.method].reg),
      // secret: sub.secret,
      remark: sub.remark,
    });
    if (sub.method.length) {
      subFormGroup
        .get('recipient')
        .setValidators([
          Validators.pattern(this.patternMap[sub.method].reg),
          Validators.required,
        ]);

      if (sub.method === 'webhook') {
        subFormGroup.addControl(
          'secret',
          new FormControl(sub.secret ? sub.secret : ''),
        );
      }
    }
    return subFormGroup;
  }

  onMethodChange(option: any, index: number) {
    if (typeof option === 'symbol') {
      return;
    }
    const subFormGroup = this.subFormArray.at(index) as FormGroup;
    if (option.value === 'webhook') {
      subFormGroup.addControl('secret', new FormControl(''));
    }

    // Recipient pattern
    subFormGroup
      .get('recipient')
      .setValidators([
        Validators.pattern(this.patternMap[option.value].reg),
        Validators.required,
      ]);
  }

  addSub() {
    this.subFormArray.push(this.createSub());
  }

  removeSub(index: number) {
    this.subFormArray.removeAt(index);
  }

  get subFormArray(): FormArray {
    return <FormArray>this.form.get('subs');
  }

  async initSpaces() {
    try {
      const spaces = await this.quotaSpace.getConsumableSpaces();
      this.spaces = spaces.map((space: any) => {
        return space.name;
      });
      this.form.get('space').setValue(this.spaces[0]);
    } catch ({ errors }) {}
  }

  complete(result?: boolean) {
    this.finish.next(result);
    this.finish.complete();
  }

  cancel() {
    this.complete();
  }

  async submit() {
    if (this.form.invalid || this.submitting) {
      return;
    }
    const value = this.form.value;
    // Check subscription key
    const uniqed = uniqBy(value.subs, 'recipient');
    if (uniqed.length < value.subs.length) {
      this.auiNotificationService.error(
        this.translate.get('duplicate_subscription_key'),
      );
      return;
    }
    const filteredSubs = value.subs.filter((sub: any) => !!sub.method.length);
    if (!filteredSubs.length) {
      this.auiNotificationService.error(
        this.translate.get('recipient_required'),
      );
      return;
    }
    this.submitting = true;
    const payload: NotificationPayload = {
      name: value.name,
      subscriptions: filteredSubs,
    };
    if (this.weblabs.QUOTA_ENABLED && value.space) {
      payload.space_name = value.space;
    }
    try {
      if (this.updating) {
        await this.notificationService.updateNotification({
          name: this.notification.name,
          uuid: this.notification.uuid,
          subscriptions: filteredSubs,
        });
      } else {
        await this.notificationService.createNotification(payload);
      }
      this.complete(true);
    } catch ({ errors }) {
      if (errors) {
        this.auiNotificationService.error(errors[0].message);
      }
    }
    this.submitting = false;
  }
}
