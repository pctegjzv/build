import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService as AuiNotificationService } from 'alauda-ui';
import { NotificationService } from 'app2/shared/services/features/notification.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ModalMode } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { CreateNotificationComponent } from '../create/create-notification.component';

@Component({
  templateUrl: './notification-detail.component.html',
})
export class NotificationDetailComponent implements OnInit {
  loading = false;
  loadError = false;
  paramsSubscription: Subscription;
  name = '';
  notification: any;
  canDelete = false;
  canUpdate = false;
  isDeleting = false;
  constructor(
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private roleUtil: RoleUtilitiesService,
    private modal: ModalService,
    private auiNotificationService: AuiNotificationService,
    private translate: TranslateService,
    private router: Router,
  ) {}

  async ngOnInit() {
    this.paramsSubscription = this.route.params
      .pipe(first())
      .subscribe(async params => {
        this.name = params['uuid'];
        await this.getNotification();
      });
  }

  async update() {
    try {
      const modalRef = this.modal.open(CreateNotificationComponent, {
        width: 860,
        title: this.translate.get('notification_update'),
        mode: ModalMode.RIGHT_SLIDER,
      });
      modalRef.componentInstance.initNotification(this.notification);
      modalRef.componentInstance.finish
        .pipe(first())
        .subscribe((result: boolean) => {
          modalRef.close();
          if (result) {
            this.auiNotificationService.success(
              this.translate.get('notification_update_success'),
            );
            this.getNotification();
          }
        });
    } catch (error) {}
  }

  async getNotification() {
    this.loading = true;
    try {
      const res = await this.notificationService.getNotificationDetail(
        this.name,
      );
      this.notification = res;
      this.canDelete = this.roleUtil.resourceHasPermission(
        this.notification,
        'notification',
        'delete',
      );
      this.canUpdate = this.roleUtil.resourceHasPermission(
        this.notification,
        'notification',
        'update',
      );
    } catch ({ errors }) {
      this.loadError = true;
      if (errors) {
        if (errors[0].code === 'resource_not_exist') {
          this.auiNotificationService.error(
            this.translate.get('notification_not_exist'),
          );
          this.router.navigate(['/notification']);
          return;
        }
        this.auiNotificationService.error(
          this.translate.get(errors[0].message),
        );
      }
    }
    this.loading = false;
  }

  async delete() {
    await this.modal.confirm({
      title: this.translate.get('delete_notification_title'),
      content: this.translate.get('delete_notification_content', {
        notification_name: this.notification.name,
      }),
    });
    this.isDeleting = true;
    try {
      await this.notificationService.deleteNotification(this.notification.uuid);
      this.auiNotificationService.success(this.translate.get('delete_success'));
      this.router.navigate(['/notification']);
    } catch ({ errors }) {}
  }
}
