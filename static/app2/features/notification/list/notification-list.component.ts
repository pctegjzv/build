import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService as AuiNotificationService } from 'alauda-ui';

import { NotificationService } from 'app2/shared/services/features/notification.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ModalMode } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';
import { cloneDeep, includes } from 'lodash';
import moment from 'moment';
import { first } from 'rxjs/operators';

import { CreateNotificationComponent } from '../create/create-notification.component';

@Component({
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.scss'],
})
export class NotificationListComponent implements OnInit {
  loading = false;
  initialized = false;
  notifications: any[];
  notificationCache: any[];
  hasCreatePermission = false;
  keyword = '';
  constructor(
    private notificationService: NotificationService,
    private roleUtil: RoleUtilitiesService,
    private modal: ModalService,
    private translate: TranslateService,
    private auiNotificationService: AuiNotificationService,
    private router: Router,
  ) {}

  async ngOnInit() {
    this.hasCreatePermission = await this.roleUtil.resourceTypeSupportPermissions(
      'notification',
    );
    await this.load();
    this.initialized = true;
  }

  filter(keyword: string) {
    this.keyword = keyword;
    if (!this.keyword.length) {
      this.notifications = this.notificationCache;
      return;
    }
    this.notifications = this.notificationCache.filter((item: any) => {
      return includes(item.name, this.keyword);
    });
  }

  async load() {
    this.loading = true;
    try {
      const { result } = await this.notificationService.getNotifications();
      result.forEach((item: any) => {
        item.isDeleting = false;
      });
      this.notifications = result.sort((prev: any, next: any) => {
        return moment(prev.created_at).isAfter(next.created_at) ? -1 : 1;
      });
      this.notificationCache = cloneDeep(this.notifications);
    } catch ({ errors }) {}
    this.loading = false;
  }

  async create() {
    try {
      const modalRef = this.modal.open(CreateNotificationComponent, {
        width: 860,
        title: this.translate.get('notification_create'),
        mode: ModalMode.RIGHT_SLIDER,
      });
      modalRef.componentInstance.finish
        .pipe(first())
        .subscribe((result: boolean) => {
          modalRef.close();
          if (result) {
            this.auiNotificationService.success(
              this.translate.get('notification_create_success'),
            );
            this.load();
          }
        });
    } catch (error) {}
  }

  async delete(notification: any) {
    await this.modal.confirm({
      title: this.translate.get('delete_notification_title'),
      content: this.translate.get('delete_notification_content', {
        notification_name: notification.name,
      }),
    });
    notification.isDeleting = true;
    try {
      await this.notificationService.deleteNotification(notification.uuid);
      this.auiNotificationService.success(this.translate.get('delete_success'));
      this.load();
    } catch ({ errors }) {}
  }

  canDelete(notification: any) {
    return this.roleUtil.resourceHasPermission(
      notification,
      'notification',
      'delete',
    );
  }

  viewDetail(notification: any) {
    this.router.navigate(['/notification/detail', notification.uuid]);
  }
}
