import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NotificationDetailComponent } from './detail/notification-detail.component';
import { NotificationListComponent } from './list/notification-list.component';

const notificationRoutes: Routes = [
  {
    path: '',
    component: NotificationListComponent,
  },
  {
    path: 'detail/:uuid',
    component: NotificationDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(notificationRoutes)],
  exports: [RouterModule],
})
export class NotificationRoutingModule {}
