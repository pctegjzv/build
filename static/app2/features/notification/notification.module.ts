import { NgModule } from '@angular/core';
import { NotificationListComponent } from 'app2/features/notification/list/notification-list.component';
import { NotificationRoutingModule } from 'app2/features/notification/notification-routing.module';
import { SharedModule } from 'app2/shared/shared.module';

import { CreateNotificationComponent } from './create/create-notification.component';
import { NotificationDetailComponent } from './detail/notification-detail.component';

@NgModule({
  imports: [SharedModule, NotificationRoutingModule],
  declarations: [
    NotificationListComponent,
    NotificationDetailComponent,
    CreateNotificationComponent,
  ],
  entryComponents: [CreateNotificationComponent],
})
export class NotificationModule {}
