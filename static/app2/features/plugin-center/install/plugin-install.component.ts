import {
  Component,
  Inject,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import {
  CONTROL_TYPE_MAP,
  DATA_TYPE_MAP,
} from 'app2/features/plugin-center/plugin-constant';
import { DynamicFormComponent } from 'app2/shared/components/dynamic-form/dynamic-form.component';
import { DynamicFormFieldDefinition } from 'app2/shared/form-field-control';
import {
  NamespaceOption,
  NamespaceService,
} from 'app2/shared/services/features/namespace.service';
import { PluginApiService } from 'app2/shared/services/features/plugin.service';
import { QuotaSpace } from 'app2/shared/services/features/quota-space.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { HttpService } from 'app2/shared/services/http/http.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './plugin-install.component.html',
  styleUrls: ['./plugin-install.component.scss'],
})
export class PluginInstallComponent implements OnInit, OnDestroy {
  loading: boolean;
  loadingErr: boolean;
  initialized: boolean;
  submitting: boolean;
  basicInfoCollapse: boolean;
  lang: string;
  originalData: object = { avatar: '' };
  regionSubscription: Subscription;
  region: Region;
  namespaces: NamespaceOption[];
  spaces: QuotaSpace[];

  defaultSettings: DynamicFormFieldDefinition[] = [];
  basicSettings: DynamicFormFieldDefinition[] = [];
  advancedSettings: DynamicFormFieldDefinition[] = [];
  integrationSettings: DynamicFormFieldDefinition[] = [];
  lbs: any[] = [];
  regionMap = {};
  apiUrls = {
    api: '',
    db: '',
  };
  submitForm = {
    name: '',
    region_name: '',
    space_name: '',
    region_alb_version: '',
    network_mode: '',
  };

  @ViewChildren(DynamicFormComponent)
  dynamicForms: QueryList<DynamicFormComponent>;

  constructor(
    private pluginApiService: PluginApiService,
    private route: ActivatedRoute,
    private translateService: TranslateService,
    private httpService: HttpService,
    private router: Router,
    private auiNotificationService: NotificationService,
    private regionService: RegionService,
    private regionUtil: RegionUtilitiesService,
    private namespaceService: NamespaceService,
    @Inject(ACCOUNT) public account: RcAccount,
  ) {}

  ngOnInit() {
    this.loading = true;
    // get language
    this.lang = this.translateService.currentLang.match('en') ? 'en' : 'cn';

    this.regionSubscription = this.regionService.region$
      .pipe(filter(region => !!region))
      .subscribe(region => {
        this.region = region;
        this.refetch();
      });
  }

  ngOnDestroy(): void {
    this.regionSubscription.unsubscribe();
  }

  async refetch() {
    this.loadingErr = false;
    this.loading = true;
    this.initialized = false;
    this.defaultSettings = [];
    this.basicSettings = [];
    this.advancedSettings = [];
    this.integrationSettings = [];
    this.spaces = null;
    this.namespaces = null;
    const pluginType = await this._getPluginType();
    if (this.region.platform_version === 'v4') {
      this.loadingErr = true;
      this.loading = false;
      return;
    }
    // get form
    this.pluginApiService
      .getForm(pluginType)
      .then(res => {
        this.originalData = Object.assign({}, res);
        this._dataFormat(res);
        this.initialized = true;
      })
      .catch(() => {
        this.loadingErr = true;
      });
  }

  get formError() {
    return this.dynamicForms.some(dynamicForm => dynamicForm.errorState);
  }

  cancel() {
    this.router.navigateByUrl('plugin');
  }

  async submit() {
    this.dynamicForms.forEach(dynamicForm => dynamicForm.onSubmit(null));
    if (this.formError) {
      return;
    }
    this.submitting = true;
    this.submitForm['integration_config'].name =
      this.submitForm.name + '-' + this.submitForm['integration_config'].name;
    const pluginType = await this._getPluginType();
    const submitForm = _.cloneDeep(this.submitForm);
    this.pluginApiService
      .createInstance(pluginType, submitForm)
      .then(() => {
        this.auiNotificationService.success(
          this.translateService.get('plugin_center_installing'),
        );
        this.router.navigateByUrl('plugin');
      })
      .catch(({ errors }) => {
        this.auiNotificationService.error(
          this.translateService.get('plugin_center_create_fail') +
            ': ' +
            errors[0].message,
        );
      })
      .then(() => {
        this.submitting = false;
      });
  }

  async defaultSettingsChange(_dyForm: DynamicFormComponent, $event: any) {
    if (!this.region || !(this.spaces || this.namespaces)) {
      return;
    }

    Object.assign(this.submitForm, {
      region_name: this.region.name,
      network_mode: this.regionMap['network_mode'],
      region_alb_version: this.regionMap['region_alb_version'],
      space_name: $event.space_name || '',
      region_lb_name: $event.region_lb_name,
      name: $event.name,
    });

    if (this.regionUtil.isNewK8sRegion(this.region)) {
      let namespace_name = '';
      this.namespaces.forEach(namespace => {
        if (namespace.uuid === $event.namespace) {
          namespace_name = namespace.name;
        }
      });
      Object.assign(this.submitForm, {
        k8snamespace: {
          uuid: $event.namespace,
          name: namespace_name,
        },
        is_new_k8s: true,
      });
    } else {
      Object.assign(this.submitForm, {
        is_new_k8s: false,
      });
    }

    // api url & db url
    const pluginNameTrs =
      '<' + this.translateService.get('plugin_center_instance_name') + '>';
    const haslb = this.lbs.some(lb => {
      if (lb.name === $event.region_lb_name) {
        if (!lb.domain_info) {
          this.apiUrls.api = this.translateService.get(
            'plugin_center_url_null',
          );
          this.apiUrls.db = this.translateService.get('plugin_center_url_null');
        } else {
          this.apiUrls.api = `${$event.name ? $event.name : pluginNameTrs}.${
            $event.space_name ? $event.space_name : $event.namespace
          }.${lb.domain_info[0].domain}`;
          this.apiUrls.db = `${$event.name ? $event.name : pluginNameTrs}db.${
            $event.space_name ? $event.space_name : $event.namespace
          }.${lb.domain_info[0].domain}`;
        }
        return true;
      }
    });
    if (!haslb) {
      this.apiUrls.api = `${$event.name ? $event.name : pluginNameTrs}.${
        $event.space_name ? $event.space_name : $event.namespace
      }.<${this.translateService.get('lb')}>`;
      this.apiUrls.db = `${$event.name ? $event.name : pluginNameTrs}db.${
        $event.space_name ? $event.space_name : $event.namespace
      }.<${this.translateService.get('lb')}>`;
    }

    // prefix for integration-name
    if (this.integrationSettings.length) {
      this.integrationSettings[0].prefix = $event.name
        ? $event.name
        : pluginNameTrs;
    }
  }

  settingsChange($event: object, type: string) {
    switch (type) {
      case 'basic':
        this.submitForm = Object.assign(this.submitForm, {
          basic_config: $event,
        });
        break;
      case 'advance':
        this.submitForm = Object.assign(this.submitForm, {
          advanced_config: $event,
        });
        break;
      case 'integration':
        this.submitForm = Object.assign(this.submitForm, {
          integration_config: $event,
        });
        break;
      default:
        break;
    }
  }

  private _getSpace() {
    return this.httpService
      .request('/ajax/v1/spaces/' + this.account.namespace, {
        method: 'GET',
        params: { action: 'consume' },
        addNamespace: false,
      })
      .then(({ result }) => result);
  }

  private _getLoadBalancersSetting(region_name: string) {
    return this.httpService
      .request('/ajax/v1/load_balancers/' + this.account.namespace, {
        method: 'GET',
        params: { region_name },
        addNamespace: false,
      })
      .then(({ result }) => {
        this.lbs = result;
        const region_lb_name = {
          name: 'region_lb_name',
          controlType: 'rc-single-selection-dropdown',
          label: this.translateService.get('lb'),
          placeholder: this.translateService.get('lb'),
          required: true,
          options: this.lbs,
          displayField: 'name',
          valueField: 'name',
        };
        const has_region_lb_name = this.defaultSettings.some((item, i) => {
          if (item.name === 'region_lb_name') {
            this.defaultSettings[i] = region_lb_name;
            return true;
          }
        });
        if (!has_region_lb_name) {
          this.defaultSettings = [...this.defaultSettings, region_lb_name];
        }
      })
      .catch(() => {
        this.loadingErr = true;
        throw 0;
      });
  }

  private _getPluginType() {
    return this.route.queryParams
      .pipe(first())
      .toPromise()
      .then(({ pluginType }) => !!pluginType && pluginType);
  }

  /**
   * format data for dynamicFormComponent
   * @param data data to generate form [from jakiro]
   */
  private async _dataFormat(data: object) {
    // default settings
    try {
      if (this.regionUtil.isNewK8sRegion(this.region)) {
        this.namespaces = await this.namespaceService.getNamespaceOptions(
          this.region.id,
        );
      } else {
        this.spaces = await this._getSpace();
      }
    } catch (err) {
      this.loadingErr = true;
      throw err;
    }
    const defaultSettings: any = this._getDefaultSettings(
      this.spaces || this.namespaces,
    );
    this.defaultSettings = [...defaultSettings];
    await this._getLoadBalancersSetting(this.region.name);
    this.loading = false;

    // others
    this.basicSettings = [
      ...this.basicSettings,
      ...this._getDynamicSettings(data, 'basic_config'),
    ];
    this.advancedSettings = [
      ...this.advancedSettings,
      ...this._getDynamicSettings(data, 'advanced_config'),
    ];
    this.integrationSettings = [
      ...this.integrationSettings,
      ...this._getDynamicSettings(data, 'integration_config'),
    ];
  }

  /**
   * get default settings
   * @param regionData regionsList
   * @param spaces spacesList
   */
  private _getDefaultSettings(options: any[] = []) {
    // set region dropdown
    if (this.region.platform_version === 'v4') {
      return;
    }
    const features: any[] = _.get(this.region, 'features.service.features', []);
    const region_alb_version = features.includes('alb') ? 'v2' : 'v1';
    if (this.region.container_manager !== 'NONE') {
      this.regionMap = {
        network_mode: _.intersection(features, [
          'bridge',
          'macvlan',
          'flannel',
        ])[0],
        region_alb_version,
      };
    }

    let arr: DynamicFormFieldDefinition[] = [];
    if (this.regionUtil.isNewK8sRegion(this.region)) {
      arr = [
        {
          name: 'namespace',
          controlType: 'rc-single-selection-dropdown',
          label: this.translateService.get('namespace'),
          required: true,
          displayField: 'name',
          valueField: 'uuid',
          options: options,
          value: options.length && options[0].name,
        },
        {
          name: 'name',
          controlType: 'rc-input',
          label: this.translateService.get('plugin_center_instance_name'),
          placeholder: this.translateService.get('plugin_center_instance_name'),
          required: true,
          minlength: 2,
          maxlength: 30,
          pattern: '^[A-Za-z0-9\\-_]+$',
        },
      ];
    } else {
      arr = [
        {
          name: 'space_name',
          controlType: 'rc-single-selection-dropdown',
          label: this.translateService.get('space'),
          placeholder:
            !options.length &&
            this.translateService.get('plugin_center_spaces_none'),
          required: true,
          displayField: 'name',
          valueField: 'name',
          options: options,
          value: options.length && options[0].name,
        },
        {
          name: 'name',
          controlType: 'rc-input',
          label: this.translateService.get('plugin_center_instance_name'),
          placeholder: this.translateService.get('plugin_center_instance_name'),
          required: true,
          minlength: 2,
          maxlength: 30,
          pattern: '^[A-Za-z0-9\\-_]+$',
        },
      ];
    }
    return arr;
  }

  /**
   * get settings not default
   * @param data data to generate form [from jakiro]
   * @param type basic_config | advanced_config | integration_config ...
   */
  private _getDynamicSettings(data: any, type: string) {
    const lang = this.lang === 'cn' ? 'zh' : 'en';
    const arr: DynamicFormFieldDefinition[] = [];

    data[type].forEach((config: any) => {
      // format options
      const options = config.option;
      let displayField: string;
      let valueField: string;
      if (options && options.length > 0) {
        if (options[0] instanceof Object) {
          const keys = Object.keys(options[0]);
          // displayField always should be the first
          displayField = keys[0];
          // valueField always should be the second
          if (keys.length > 1) {
            valueField = keys[1];
          } else {
            valueField = keys[0];
          }
        }
      }

      arr.push({
        name: config.attribute_name,
        controlType: CONTROL_TYPE_MAP[config.type],
        label: config.display_name[lang],
        placeholder: config.placeholder
          ? config.placeholder[lang]
          : config.display_name[lang],
        description: config.description && config.description[lang],
        required: config.required === false ? false : true,
        type: DATA_TYPE_MAP[config.type],
        pattern: config.pattern && config.pattern,
        min: config.min_value,
        max: config.max_value,
        minlength: config.min_length,
        maxlength: config.max_length,
        options,
        displayField,
        valueField,
        value: config.default_value,
        prefix:
          type === 'integration_config' &&
          config.attribute_name === 'name' &&
          this.translateService.get('plugin_center_instance_name'),
      });
    });
    return arr;
  }
}
