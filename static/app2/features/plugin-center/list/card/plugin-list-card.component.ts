import { Component, Inject, Input, OnChanges, Optional } from '@angular/core';
import { Router } from '@angular/router';
import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { PluginApiService } from 'app2/shared/services/features/plugin.service';
import { Region } from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { TranslateService } from 'app2/translate/translate.service';

import { PluginListComponent } from '../plugin-list.component';

@Component({
  selector: 'rc-plugin-list-card',
  templateUrl: './plugin-list-card.component.html',
  styleUrls: ['./plugin-list-card.component.scss'],
})
export class PluginListCardComponent implements OnChanges {
  @Input()
  cardItem: any;
  @Input()
  region: Region;
  @Input()
  installButtonEnabled: boolean;
  loading = true;
  flipOver = false;
  instanceList: any;
  instanceListLoadingErr: boolean;
  statusList: any[];

  constructor(
    @Optional() public pluginList: PluginListComponent,
    @Inject(ACCOUNT) public account: RcAccount,
    private translateService: TranslateService,
    private router: Router,
    private pluginApiService: PluginApiService,
    private regionUtil: RegionUtilitiesService,
  ) {}

  ngOnChanges() {
    if (Object.keys(this.cardItem.latest_plugin).length) {
      this.statusList = this.getLatestPluginStatus(this.cardItem.latest_plugin);
    }
  }

  flip() {
    this.flipOver = !this.flipOver;
    if (this.flipOver) {
      this.getInstanceList();
    }
  }

  install() {
    this.router.navigateByUrl(
      `plugin/install?pluginType=${this.cardItem.plugin_type}`,
    );
  }

  getInstanceList() {
    this.loading = true;
    this.instanceListLoadingErr = false;

    this.pluginApiService
      .getInstances(this.cardItem.plugin_type, this.region.id)
      .then(res => {
        this.instanceList = res;
      })
      .catch(() => (this.instanceListLoadingErr = true))
      .then(() => {
        this.loading = false;
      });
  }

  getLatestPluginStatus(latestPlugin: any) {
    const statusList = [
      {
        status: 'INACTIVE',
        txt: this.translateService.get('plugin_center_app_not_create'),
      },
      {
        status: 'INACTIVE',
        txt: this.translateService.get('plugin_center_integration_not_create'),
      },
      {
        status: 'INACTIVE',
        txt: this.translateService.get('plugin_center_plugin_not_create'),
      },
    ];

    const latestStatus = latestPlugin.status;
    // app
    const appStatus = latestPlugin.pluginapplication.status;
    if (['CreateError', 'StartError', 'Deleted'].includes(appStatus)) {
      statusList[0] = {
        status: 'ERROR',
        txt: this.translateService.get('plugin_center_app_created_failed'),
      };
      return statusList;
    } else if (['Running'].includes(appStatus)) {
      statusList[0] = {
        status: 'SUCCESS',
        txt: this.translateService.get('plugin_center_app_created'),
      };
    } else if (
      ['Failed'].includes(latestStatus) &&
      ['NotCreated'].includes(appStatus)
    ) {
      statusList[0] = {
        status: 'ERROR',
        txt: this.translateService.get('plugin_center_app_created_failed'),
      };
      return statusList;
    } else {
      return statusList;
    }

    // integration
    const integrationStatus = latestPlugin.pluginintegration.status;
    if (['Failed', 'Deleted'].includes(latestPlugin.pluginintegration.status)) {
      statusList[1] = {
        status: 'ERROR',
        txt: this.translateService.get(
          'plugin_center_integration_created_failed',
        ),
      };
      return statusList;
    } else if (['Succeed'].includes(integrationStatus)) {
      statusList[1] = {
        status: 'SUCCESS',
        txt: this.translateService.get('plugin_center_integration_created'),
      };
    } else {
      return statusList;
    }

    // latest plugin
    if (
      ['Failed'].includes(latestStatus) &&
      !['NotCreated'].includes(integrationStatus)
    ) {
      statusList[2] = {
        status: 'ERROR',
        txt: this.translateService.get('plugin_center_plugin_created_failed'),
      };
    } else if (['Succeed'].includes(latestStatus)) {
      statusList[2] = {
        status: 'SUCCESS',
        txt: this.translateService.get('plugin_center_plugin_created'),
      };
    }

    return statusList;
  }

  viewApp(plugin: any) {
    if (this.regionUtil.isNewK8sRegion(this.region)) {
      this.router.navigateByUrl(`k8s_app/detail/${plugin.app_uuid}`);
    } else {
      this.router.navigateByUrl(
        `app_service/app/app_detail/${plugin.app_uuid}`,
      );
    }
  }

  get namespace() {
    return this.account.namespace;
  }

  get lang() {
    return this.translateService.currentLang;
  }
}
