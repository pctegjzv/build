import { filter, map } from 'rxjs/operators';

import { Component, OnDestroy, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { Observable, Subscription } from 'rxjs';

import { PluginApiService } from 'app2/shared/services/features/plugin.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { delay } from 'app2/shared/services/utility/delay';
import { PaginationDataWrapper } from 'app2/shared/services/utility/pagination-data';

@Component({
  templateUrl: './plugin-list.component.html',
  styleUrls: ['./plugin-list.component.scss'],
})
export class PluginListComponent implements OnInit, OnDestroy {
  static PAGE_SIZE = 100;

  initialized: boolean;
  pluginPaginationServiceWrapper: PaginationDataWrapper<any>;
  private _empty: Observable<boolean>;
  private _listItems: Observable<any>;
  private destroyed: boolean;
  regionSubscription: Subscription;
  region: Region;
  cardInstallButtonEnabled: boolean;

  constructor(
    private pluginApiService: PluginApiService,
    private regionService: RegionService,
    private roleUtil: RoleUtilitiesService,
  ) {}

  ngOnInit() {
    this.regionSubscription = this.regionService.region$
      .pipe(filter(region => !!region))
      .subscribe(region => {
        this.region = region;
        this.refetch();
      });
    // get install permission
    this.roleUtil
      .resourceTypeSupportPermissions('application', {
        cluster_name: this.region.name,
      })
      .then(res => (this.cardInstallButtonEnabled = res));
  }

  ngOnDestroy(): void {
    this.destroyed = true;
  }

  refetch() {
    this.initPagination();
    this.startPolling();
  }

  async refetchList() {
    return await this.pluginPaginationServiceWrapper.setPageNo(1);
  }

  get loading() {
    return this.pluginPaginationServiceWrapper.loading;
  }

  get loadError() {
    return this.pluginPaginationServiceWrapper.loadError;
  }

  get empty() {
    return this._empty;
  }

  get listItems() {
    return this._listItems;
  }

  trackByFn(_: number, item: any) {
    return item.plugin_type;
  }

  // install(plugin_type: string) {
  //   $state.go()
  // }

  private initPagination() {
    // 暂无分页
    const fetchRequest = (/* pageNo: number, pageSize: number */) => {
      return this.pluginApiService
        .getList(this.region.id, this.region.platform_version !== 'v2')
        .then(results => ({
          count: results.length,
          results,
        }));
    };

    this.pluginPaginationServiceWrapper = new PaginationDataWrapper(
      fetchRequest,
      PluginListComponent.PAGE_SIZE,
    );
    this._empty = this.results.pipe(
      map(results => !results || results.length === 0),
    );
    this._listItems = this.results.pipe(
      map(results => {
        return _.orderBy(results, 'plugin_type', 'asc');
      }),
    );
  }

  private get results() {
    return this.pluginPaginationServiceWrapper.data;
  }

  private async startPolling() {
    while (!this.destroyed) {
      await this.refetchList();
      this.initialized = true;
      await delay(30000);
    }
  }
}
