export const CONTROL_TYPE_MAP = {
  option: 'rc-single-selection-dropdown',
  int: 'rc-input',
  string: 'rc-input',
  cluster_size: 'rc-customize-size',
};

export const DATA_TYPE_MAP = {
  int: 'number',
};
