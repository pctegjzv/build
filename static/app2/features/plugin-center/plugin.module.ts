import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';

import { PluginInstallComponent } from './install/plugin-install.component';
import { PluginListCardComponent } from './list/card/plugin-list-card.component';
import { PluginListComponent } from './list/plugin-list.component';
import './plugin.global.scss';
import { PluginRoutingModule } from './plugin.routing.module';

@NgModule({
  imports: [SharedModule, PluginRoutingModule],
  declarations: [
    PluginListComponent,
    PluginListCardComponent,
    PluginInstallComponent,
  ],
})
export class PluginModule {}
