import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PluginInstallComponent } from './install/plugin-install.component';
import { PluginListComponent } from './list/plugin-list.component';

const routes: Routes = [
  {
    path: '',
    component: PluginListComponent,
  },
  {
    path: 'install',
    component: PluginInstallComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class PluginRoutingModule {}
