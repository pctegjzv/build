import { ChangeDetectorRef, Inject } from '@angular/core';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { get, some } from 'lodash';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { TableComponent } from 'app2/shared/components/table/table.component';
import { Pod, PodService } from 'app2/shared/services/features/pod.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import {
  Pagination,
  PaginationDataWrapper,
} from 'app2/shared/services/utility/pagination-data';

@Component({
  templateUrl: './k8s-pod-list.component.html',
  styleUrls: ['./k8s-pod-list.component.scss'],
})
export class K8sPodListComponent implements OnInit, OnDestroy {
  @ViewChild('podTable')
  table: TableComponent;

  paginationDataWrapper: PaginationDataWrapper<any>;

  regionSubscription: Subscription;

  paginationDataSubscription: Subscription;

  pageSize = 20;

  count = 0;

  podItems: any[] = [];

  initialized = false;

  private pollingTimer: any = null;

  private destroyed = false;

  constructor(
    @Inject(ENVIRONMENTS) private env: Environments,
    private podService: PodService,
    private regionService: RegionService,
    private regionUtilities: RegionUtilitiesService,
    private router: Router,
    private cdr: ChangeDetectorRef,
  ) {}

  async ngOnInit() {
    const fetchRequest = (pageNo: number, pageSize: number, params: any) => {
      return this.podService.getK8sPods({
        pageNo,
        pageSize,
        params,
      });
    };
    this.paginationDataWrapper = new PaginationDataWrapper(
      fetchRequest,
      this.pageSize,
    );

    this.paginationDataSubscription = this.paginationDataWrapper.pagination.subscribe(
      (paginationData: Pagination<Pod>) => {
        if (!paginationData.loadError) {
          this.count = paginationData.count;
          this.podItems = paginationData.results || [];
          this.podItems.forEach((item: any) => {
            item.imageDisplayParam = item.spec.containers.map(
              (container: any) => {
                return {
                  image: container.image,
                  size: {
                    cpu: get(container, 'resources.requests.cpu'),
                    memory: get(container, 'resources.requests.memory'),
                  },
                };
              },
            );
          });
          this.cdr.detectChanges();
        }
        this.resetPollingTimer();
      },
    );

    this.regionSubscription = this.regionService.region$
      .pipe(filter(region => !!region))
      .subscribe(region => {
        if (this.regionUtilities.isNewK8sRegion(region)) {
          this.onRegionChanged(region.name);
        } else {
          this.router.navigateByUrl('/home');
        }
      });
  }

  ngOnDestroy() {
    this.destroyed = true;
    this.paginationDataSubscription.unsubscribe();
    this.regionSubscription.unsubscribe();
    clearTimeout(this.pollingTimer);
  }

  async onRegionChanged(regionName: string) {
    this.podItems = [];
    await this.paginationDataWrapper.setParams({
      cluster: regionName,
    });
    this.initialized = true;
  }

  get loading() {
    return this.paginationDataWrapper.loading;
  }

  get loadError() {
    return this.paginationDataWrapper.loadError;
  }

  get empty() {
    return !this.podItems || !this.podItems.length;
  }

  get currentPage() {
    return this.paginationDataWrapper.pageNo;
  }

  get serviceUuidKey() {
    return `service.${this.env.label_base_domain}/uuid`;
  }

  get appUuidKey() {
    return `app.${this.env.label_base_domain}/uuid`;
  }

  get serviceNameKey() {
    return `service.${this.env.label_base_domain}/name`;
  }

  get appNameKey() {
    return `app.${this.env.label_base_domain}/name`;
  }

  refetch() {
    this.paginationDataWrapper.refetch();
  }

  pageChanged(page: number) {
    this.paginationDataWrapper.setPageNo(page);
  }

  searchChanged(queryString: string) {
    this.paginationDataWrapper.setParams({
      name: queryString,
    });
  }

  getBelongingService(pod: object) {
    const labels = get(pod, 'metadata.labels', {});
    return labels[this.serviceUuidKey]
      ? {
          uuid: labels[this.serviceUuidKey] ? labels[this.serviceUuidKey] : '',
          name: labels[this.serviceNameKey] ? labels[this.serviceNameKey] : '',
        }
      : null;
  }

  getBelongingApp(pod: object) {
    const labels = get(pod, 'metadata.labels', {});
    return labels[this.appUuidKey]
      ? {
          uuid: labels[this.appUuidKey] ? labels[this.appUuidKey] : '',
          name: labels[this.appNameKey] ? labels[this.appNameKey] : '',
        }
      : null;
  }

  /**
   * paginationData 返回后重置下次轮询时间 若有部署中的应用，等待时间为3s
   */
  private resetPollingTimer() {
    clearTimeout(this.pollingTimer);
    if (this.destroyed) {
      return;
    }
    const hasDeployingPods = some(this.podItems, (pod: any) => {
      return pod.status.phase.toLowerCase() === 'deploying';
    });
    const waitTime = hasDeployingPods ? 3000 : 10000;
    this.pollingTimer = setTimeout(() => {
      this.refetch();
    }, waitTime);
  }
}
