import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { K8sPodListComponent } from 'app2/features/pod/list/k8s-pod-list.component';

const k8sPodRoutes: Routes = [
  {
    path: '',
    component: K8sPodListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(k8sPodRoutes)],
  exports: [RouterModule],
  declarations: [],
})
export class K8sPodRoutingModule {}
