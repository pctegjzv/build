import { NgModule } from '@angular/core';
import { K8sPodListComponent } from 'app2/features/pod/list/k8s-pod-list.component';
import { K8sPodRoutingModule } from 'app2/features/pod/pod-routing.module';

import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [SharedModule, K8sPodRoutingModule],
  declarations: [K8sPodListComponent],
})
export class K8sPodModule {}
