import { Component, Inject, Input, OnInit } from '@angular/core';
import { AccountService } from 'app2/shared/services/features/account.service';

import { Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { VendorCustomer } from 'app2/core/customization/vendor-customer.types';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { AccountType, Environments, RcAccount } from 'app2/core/types';
import {
  AccountPage,
  OrgService,
} from 'app2/shared/services/features/org.service';
import { RBACService } from 'app2/shared/services/features/rbac.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { cloneDeep, get, uniqBy } from 'lodash';

@Component({
  selector: 'rc-accounts-list',
  templateUrl: './accounts-list.component.html',
  styleUrls: ['./accounts-list.component.scss'],
})
export class ProjectAccountsListComponent implements OnInit {
  @Input()
  isCreate: boolean;

  @Input()
  projectName: string;

  @Input()
  k8sNamespaceName: string;

  @Input()
  k8sNamespaceUuid: string;

  @Input()
  clusterNames: string;

  isUserView: boolean;
  isCMB = true;

  loading = true;
  loadingError = false;
  roleAdding: boolean;
  users: RcAccount[];
  currentUserPage = 1;
  userCount = 0;

  accountTypes: AccountType[] = [];
  selectedUser = '';
  selectedRole: AccountType;

  templateMap = {};

  search = '';

  columnDefs: any[] = [
    {
      name: 'username',
      label: 'name',
    },
    {
      name: 'roles',
      label: 'roles',
    },
    {
      name: 'type',
      label: 'user_type',
    },
    {
      name: 'sub_type',
      label: 'user_source',
    },
    {
      name: 'is_valid',
      label: 'status',
    },
    {
      name: 'assigned_at',
      label: 'assigned_at',
    },
    {
      name: 'action',
      label: '',
    },
  ];
  columns = this.columnDefs.map(columnDef => columnDef.name);

  rolesViewPermission: boolean;
  rolesAssignPermission: boolean;
  rolesRevokePermission: boolean;

  constructor(
    private rbacService: RBACService,
    private orgService: OrgService,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
    private modalService: ModalService,
    private roleUtil: RoleUtilitiesService,
    private router: Router,
    private errorToast: ErrorsToastService,
    private accountService: AccountService,
    @Inject(ENVIRONMENTS) private environments: Environments,
  ) {}

  async ngOnInit() {
    this.isCMB = this.checkCustomer('CMB');
    this.isUserView = this.accountService.isUserView();
    /* TO DO: blocked by search dropdown */
    // await this.orgService
    //   .getAccounts({
    //     page_size: 0,
    //     ignoreProject: true,
    //   })
    //   .then(({ results }) => (this.allAccounts = results))
    //   .catch(() => {});
    [
      this.rolesViewPermission,
      this.rolesAssignPermission,
      this.rolesRevokePermission,
    ] = await this.roleUtil.resourceTypeSupportPermissions('role', {}, [
      'view',
      'assign',
      'revoke',
    ]);
    this.fetchRoleTypes();
    this.fetchAccounts();
  }

  checkCustomer(user: string) {
    return this.environments.vendor_customer === VendorCustomer[user];
  }

  async refetch() {
    try {
      this.loading = true;
      this.loadingError = false;
      this.accountTypes = await this.rbacService
        .getValidRoleTypes({
          project_name: this.projectName || '',
          k8s_namespace_name: this.k8sNamespaceName || '',
          cluster_names: this.clusterNames || '',
          level: this.k8sNamespaceName ? 'namespace' : 'project',
        })
        .then(res =>
          res.map((role: AccountType) => ({
            role_uuid: role.role_uuid,
            role_name: role.role_name,
            template_display_name: role.template_display_name
              ? this.isCMB
                ? role.template_display_name
                : `${role.role_name}(${role.template_display_name})`
              : role.role_name,
            template_name: role.template_name,
            template_uuid: role.template_uuid,
          })),
        );
      if (this.isCreate) {
        this.accountTypes.forEach((role: AccountType) => {
          if (!this.templateMap[role.template_uuid]) {
            this.templateMap[role.template_uuid] = [{ name: role.role_name }];
          } else {
            this.templateMap[role.template_uuid].push({ name: role.role_name });
          }
        });
        this.accountTypes = uniqBy(this.accountTypes, 'template_uuid');
      }

      await this.orgService
        .getAccounts({
          page: this.currentUserPage,
          page_size: 20,
          project_name: this.projectName || '',
          k8s_namespace_uuid: this.k8sNamespaceUuid || '',
          search: this.search,
          ignoreProject: true,
        })
        .then((res: AccountPage) => {
          this.userCount = res.count;
          this.users = res.results;
        });
    } catch (error) {
      this.loadingError = true;
      this.loading = false;
    }
    this.loading = false;
  }

  async fetchRoleTypes() {
    try {
      this.loading = true;
      this.loadingError = false;
      this.accountTypes = await this.rbacService
        .getValidRoleTypes({
          project_name: this.projectName || '',
          k8s_namespace_name: this.k8sNamespaceName || '',
          cluster_names: this.clusterNames || '',
          level: this.k8sNamespaceName ? 'namespace' : 'project',
        })
        .then(res =>
          res.map((role: AccountType) => ({
            role_uuid: role.role_uuid,
            role_name: role.role_name,
            template_display_name: role.template_display_name
              ? this.isCMB
                ? role.template_display_name
                : `${role.role_name}(${role.template_display_name})`
              : role.role_name,
            template_name: role.template_name,
            template_uuid: role.template_uuid,
          })),
        );
      this.selectedRole = this.accountTypes[0];
      if (this.isCreate) {
        this.accountTypes.forEach((role: AccountType) => {
          if (!this.templateMap[role.template_uuid]) {
            this.templateMap[role.template_uuid] = [{ name: role.role_name }];
          } else {
            this.templateMap[role.template_uuid].push({ name: role.role_name });
          }
        });
        this.accountTypes = uniqBy(this.accountTypes, 'template_uuid');
      }
    } catch (error) {
      this.loadingError = true;
      this.loading = false;
    }
    this.loading = false;
  }

  fetchAccounts() {
    this.loading = true;
    this.loadingError = false;
    this.orgService
      .getAccounts({
        page: this.currentUserPage,
        page_size: 20,
        project_name: this.projectName || '',
        k8s_namespace_uuid: this.k8sNamespaceUuid || '',
        search: this.search,
        ignoreProject: true,
      })
      .then((res: AccountPage) => {
        this.userCount = res.count;
        this.users = res.results;
      })
      .catch(error => {
        this.loadingError = true;
        this.errorToast.error(error);
      })
      .then(() => {
        this.loading = false;
      });
  }

  onSearch($event: string) {
    this.search = $event;
    this.currentUserPage = 1;
    this.fetchAccounts();
  }

  pageChanged(page: number) {
    this.currentUserPage = page;
    this.fetchAccounts();
  }

  addAccount() {
    this.roleAdding = true;
    let roles = [{ name: this.selectedRole.role_name }];
    if (this.isCreate) {
      roles = cloneDeep(this.templateMap[this.selectedRole.template_uuid]);
    }
    this.orgService
      .addAccountRoles(this.selectedUser, roles)
      .then(() => {
        this.auiNotificationService.success(
          this.translateService.get('user_add_success'),
        );
        this.fetchAccounts();
      })
      .catch(error => {
        if (get(error, 'errors[0].code') === 'user_have_role') {
          this.auiNotificationService.error(
            this.translateService.get('user_have_role', {
              user: this.selectedUser,
              type: this.selectedRole.template_display_name
                ? this.isCMB
                  ? this.selectedRole.template_display_name
                  : `${this.selectedRole.role_name}(${
                      this.selectedRole.template_display_name
                    })`
                : this.selectedRole.role_name,
            }),
          );
          return;
        }
        this.errorToast.error(error);
      })
      .then(() => {
        this.selectedUser = '';
        this.roleAdding = false;
      });
  }

  removeUser(user: RcAccount) {
    const roles = user.roles.map((role: AccountType) => ({
      name: role.role_name,
    }));
    this.modalService
      .confirm({
        title: this.translateService.get('delete_account_confirm', {
          name: user.username,
        }),
      })
      .then(() => {
        this.orgService
          .removeAccountRoles(user.username, roles)
          .then(() => {
            this.auiNotificationService.success(
              this.translateService.get('delete_success'),
            );
            this.pageChanged(1);
          })
          .catch(error => {
            this.errorToast.error(error);
          });
      })
      .catch(() => {});
  }

  removeRole(account: RcAccount, role_name: string) {
    this.rbacService
      .deleteUser(role_name, [{ user: account.username }])
      .then(() => {
        this.auiNotificationService.success(
          this.translateService.get('delete_success'),
        );
        account.roles = account.roles.filter(
          (role: AccountType) => role.role_name !== role_name,
        );
      })
      .catch(error => {
        this.errorToast.error(error);
      });
  }

  showRoleDetail(role_name: string) {
    this.router.navigateByUrl(`/rbac/roles/detail?role_name=${role_name}`);
  }

  isInvalidAccount(account: RcAccount) {
    return account.type === 'organizations.LDAPAccount' && !account.is_valid;
  }

  get zeroState() {
    return this.users && !this.users.length && !this.loadingError;
  }
}
