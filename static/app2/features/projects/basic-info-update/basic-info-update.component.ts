import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { NotificationService } from 'alauda-ui';
import { VendorCustomer } from 'app2/core/customization/vendor-customer.types';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { ProjectService } from 'app2/shared/services/features/project.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-project-basic-info-update',
  templateUrl: './basic-info-update.component.html',
  styleUrls: ['./basic-info-update.component.scss'],
})
export class ProjectBasicInfoUpdateComponent implements OnInit {
  @Output()
  finished = new EventEmitter<any>();

  isCMB = true;
  data = {
    display_name: '',
    description: '',
  };
  name = '';
  uuid = '';

  constructor(
    private projectService: ProjectService,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
    @Inject(ENVIRONMENTS) private environments: Environments,
  ) {}

  ngOnInit(): void {
    this.isCMB = this.environments.vendor_customer === VendorCustomer.CMB;
  }

  submit() {
    this.projectService
      .updateProjectBasicInfo(this.uuid, this.data)
      .then(() => {
        this.auiNotificationService.success(
          this.translateService.get('update_success'),
        );
        this.finished.next(this.data);
      })
      .catch(({ errors }) => {
        this.auiNotificationService.error(
          this.translateService.get(errors[0].code),
        );
      });
  }

  cancel() {
    this.finished.next(null);
  }
}
