import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { TranslateService } from 'app2/translate/translate.service';
import { cloneDeep, findIndex, get, remove, union, uniqBy } from 'lodash';

import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { VendorCustomer } from 'app2/core/customization/vendor-customer.types';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { EnvironmentService } from 'app2/shared/services/features/environment.service';
import {
  Project,
  ProjectService,
} from 'app2/shared/services/features/project.service';
import {
  Mirror,
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { State } from 'app2/state-container/core';
import * as projectActions from 'app2/state-container/core/actions/project';

export interface Quota {
  cpu?: number;
  memory?: number;
  pvc_num?: number;
  pods?: number;
  storage?: number;
}

interface PreAddRegionData {
  name?: string;
  display_name?: string;
  uuid?: string;
  service_type?: string;
  quota?: Quota;
}

interface PreSubmitData {
  name: string;
  display_name: string;
  description: string;
  clusters: PreAddRegionData[];
}

enum CREATE_STEPS {
  BASIC = 1,
  QUOTA = 2,
  MEMBER = 3,
}

@Component({
  selector: 'rc-project-create',
  templateUrl: './project-create.component.html',
  styleUrls: ['./project-create.component.scss'],
})
export class ProjectCreateComponent implements OnInit {
  @ViewChild('basicForm')
  basicForm: NgForm;
  @ViewChild('quotaForm')
  quotaForm: NgForm;
  isCMB = true;
  minQuota: number;
  submitting: boolean;
  data: PreSubmitData = {
    name: '',
    display_name: '',
    description: '',
    clusters: [],
  };
  project_uuid: string;
  regions: Region[];
  selectedRegions: Region[] = [];
  normalRegions: PreAddRegionData[] = [];
  mirrorRegions: Mirror[] = [];
  curStep = CREATE_STEPS['BASIC'];
  stepsEnum = CREATE_STEPS;
  steps: string[];
  kubernetes = true;

  setSameQuotaFlags: boolean[] = [];
  mirrorHiddenFlags: boolean[] = [];
  normalHiddenFlags: boolean[] = [];

  constructor(
    private projectService: ProjectService,
    private auiNotificationService: NotificationService,
    private errorsToast: ErrorsToastService,
    private translateService: TranslateService,
    private router: Router,
    private regionService: RegionService,
    private environmentService: EnvironmentService,
    private store: Store<State>,
    @Inject(ENVIRONMENTS) private environments: Environments,
  ) {}

  ngOnInit(): void {
    this.isCMB = this.environments.vendor_customer === VendorCustomer.CMB;
    if (this.isCMB) {
      this.steps = ['basic_info', 'set_region_quotas', 'add_member'];
    } else {
      this.steps = ['basic_info', 'add_member'];
    }
    this.minQuota = this.isCMB ? 1 : 0;
    const defaultQuota = this.environmentService.getDefaultQuota('project');
    this.regionService
      .getRegions()
      .then((regions: Region[]) => {
        const normalRegions = remove(
          regions,
          (region: Region) => !region.mirror.regions,
        );
        const UniqRegions = union(
          uniqBy(
            regions.map((region: Region) => {
              region.display_name =
                region.mirror.display_name || region.mirror.name;
              region.name = region.mirror.name;
              region.mirror.uuid = region.mirror.id;
              region.mirror.regions = region.mirror.regions.map(
                (cluster: Region) => ({
                  name: cluster.name,
                  display_name: cluster.display_name,
                  uuid: cluster.id,
                  service_type: 'kubernetes',
                  quota: defaultQuota || {
                    cpu: 0,
                    memory: 0,
                    pvc_num: 0,
                    pods: 0,
                    storage: 0,
                  },
                }),
              );
              return region;
            }),
            'mirror.name',
          ),
          normalRegions,
        );
        this.regions = cloneDeep(UniqRegions);
      })
      .catch(() => {});
  }

  stepQuota() {
    this.basicForm.onSubmit(null);
    if (this.basicForm.valid) {
      this.curStep = this.stepsEnum['QUOTA'];
    }
  }

  create() {
    if (this.basicForm) {
      this.basicForm.onSubmit(null);
      if (!this.basicForm.valid) {
        return;
      }
    }
    if (this.quotaForm) {
      this.quotaForm.onSubmit(null);
      if (!this.quotaForm.valid) {
        return;
      }
    }
    this.submitting = true;
    if (this.isCMB) {
      this.data.name = this.data.display_name.toLowerCase();
    }
    const data: PreSubmitData = cloneDeep(this.data);
    let preAddMirrorRegions: PreAddRegionData[] = [];
    this.mirrorRegions.forEach((region: Mirror, i) => {
      if (this.setSameQuotaFlags[i]) {
        region.regions.forEach(
          (item: PreAddRegionData) =>
            (item.quota = cloneDeep(region.regions[0].quota)),
        );
      }
      const regions: PreAddRegionData[] = get(region, 'regions');
      preAddMirrorRegions = [...preAddMirrorRegions, ...regions];
    });
    const preAddregions = union(this.normalRegions, preAddMirrorRegions);
    data.clusters = preAddregions;

    this.projectService
      .createProject(data)
      .then((res: Project) => {
        this.store.dispatch(new projectActions.CreateProjectSuccess(res));
        this.auiNotificationService.success(
          this.translateService.get('create_success'),
        );
        this.project_uuid = res.uuid;
        this.curStep = this.stepsEnum['MEMBER'];
      })
      .catch(error => this.errorsToast.error(error))
      .then(() => (this.submitting = false));
  }

  regionAdd($event: Region) {
    const defaultQuota = this.environmentService.getDefaultQuota('project');
    if ($event.mirror.regions) {
      this.mirrorRegions.push($event.mirror);
      this.setSameQuotaFlags[
        findIndex(this.mirrorRegions, ['id', $event.mirror.id])
      ] = true;
    } else {
      this.normalRegions.push({
        name: $event.name,
        display_name: $event.display_name,
        uuid: $event.id,
        service_type: 'kubernetes',
        quota: defaultQuota || {
          cpu: 0,
          memory: 0,
          pvc_num: 0,
          pods: 0,
          storage: 0,
        },
      });
    }
  }

  regionRemove($event: Region) {
    if ($event.mirror.regions) {
      remove(this.mirrorRegions, item => item.uuid === $event.mirror.id);
      this.setSameQuotaFlags[
        findIndex(this.mirrorRegions, ['id', $event.mirror.id])
      ] = true;
      this.mirrorHiddenFlags[
        findIndex(this.mirrorRegions, ['id', $event.mirror.id])
      ] = false;
    } else {
      remove(this.normalRegions, item => item.uuid === $event.id);
      this.normalHiddenFlags[
        findIndex(this.mirrorRegions, ['id', $event.mirror.id])
      ] = false;
    }
  }

  goToDetail() {
    this.router.navigateByUrl(
      `projects/detail/${this.data.name};uuid=${this.project_uuid}`,
    );
  }

  cancel() {
    this.router.navigateByUrl('/projects');
  }

  isNaN(number: number) {
    return isNaN(number);
  }
}
