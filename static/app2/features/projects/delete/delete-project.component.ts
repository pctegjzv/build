import { Component, EventEmitter } from '@angular/core';

@Component({
  templateUrl: './delete-project.component.html',
  styleUrls: ['./delete-project.component.scss'],
})
export class DeleteProjectComponent {
  deleteLoading: boolean;
  projectName: string;
  displayName: string;
  inputName = '';
  finished = new EventEmitter();
  constructor() {}

  get inputCorrect() {
    return this.inputName && this.inputName === this.projectName;
  }

  delete() {
    this.finished.next(true);
  }

  cancel() {
    this.finished.next(false);
  }
}
