import { Component, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { NotificationService } from 'alauda-ui';
import { VendorCustomer } from 'app2/core/customization/vendor-customer.types';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { SerieChartData } from 'app2/features/dashboard/charts/ngx-charts.types';
import { ProjectBasicInfoUpdateComponent } from 'app2/features/projects/basic-info-update/basic-info-update.component';
import { DeleteProjectComponent } from 'app2/features/projects/delete/delete-project.component';
import { QuotaUpdateComponent } from 'app2/features/projects/quota-update/quota-update.component';
import { TabGroupComponent } from 'app2/shared/components/tabs/tab-group.component';
import { AccountService } from 'app2/shared/services/features/account.service';
import {
  Project,
  ProjectService,
} from 'app2/shared/services/features/project.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { State } from 'app2/state-container/core';
import * as projectActions from 'app2/state-container/core/actions/project';
import { TranslateService } from 'app2/translate/translate.service';
import { Subscription } from 'rxjs';

interface QuotaItem {
  cpu?: {
    max: number;
    used: number;
  };
  memory?: {
    max: number;
    used: number;
  };
  storage?: {
    max: number;
    used: number;
  };
  pvc_num?: {
    max: number;
    used: number;
  };
  pods?: {
    max: number;
    used: number;
  };
}

interface PieData {
  cpu: SerieChartData[];
  memory: SerieChartData[];
  storage: SerieChartData[];
  pvc_num: SerieChartData[];
  pods: SerieChartData[];
}

interface PreAddRegionData {
  name?: string;
  display_name?: string;
  uuid?: string;
  service_type?: string;
  quota?: {
    cpu: number;
    memory: number;
    pvc_num: number;
    pods: number;
    storage: number;
  };
}

interface PreSubmitData {
  uuid: string;
  name: string;
  display_name: string;
  description: string;
  clusters?: PreAddRegionData[];
  updated_at?: string;
}

@Component({
  selector: 'rc-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.scss'],
})
export class ProjectDetailComponent implements OnInit {
  @Input()
  project: Project;
  @ViewChild('tabGroup')
  tabGroup: TabGroupComponent;
  isUserView = true;
  isCMB = true;
  updatePermission: boolean;
  deletePermission: boolean;
  data: PreSubmitData = {
    name: '',
    uuid: '',
    description: '',
    display_name: '',
    clusters: [],
  };
  unitMap = {
    cpu: this.translateService.get('unit_core'),
    memory: this.translateService.get('unit_gb'),
    pvc_num: this.translateService.get('ge'),
    pods: this.translateService.get('ge'),
    storage: this.translateService.get('unit_gb'),
  };
  clustersMap: any = {
    kubernetes: [],
    PCF: [],
  };
  shouldHiddenQuotas = false;
  curQuota: QuotaItem = {
    cpu: {
      max: 0,
      used: 0,
    },
    memory: {
      max: 0,
      used: 0,
    },
    storage: {
      max: 0,
      used: 0,
    },
    pvc_num: {
      max: 0,
      used: 0,
    },
    pods: {
      max: 0,
      used: 0,
    },
  };
  curPieData: PieData = {
    cpu: this.getPieData(0, 0),
    memory: this.getPieData(0, 0),
    storage: this.getPieData(0, 0),
    pvc_num: this.getPieData(0, 0),
    pods: this.getPieData(0, 0),
  };
  quotaSortQuery = ['cpu', 'memory', 'pods', 'pvc_num', 'storage'];
  pieDataMap: any = {};
  curCluster = { uuid: '', name: '' };
  serviceType: 'Kubernetes' | 'PCF' = 'Kubernetes';
  paramsSubscription: Subscription;
  clustersTitle: string;

  constructor(
    private projectService: ProjectService,
    private route: ActivatedRoute,
    private router: Router,
    private auiNotificationService: NotificationService,
    private modalService: ModalService,
    private translateService: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private errorToast: ErrorsToastService,
    private accountService: AccountService,
    private store: Store<State>,
    @Inject(ENVIRONMENTS) private environments: Environments,
  ) {}

  async ngOnInit() {
    this.isUserView = this.accountService.isUserView();
    this.isCMB = this.checkCustomer('CMB');
    [
      this.updatePermission,
      this.deletePermission,
    ] = await this.roleUtil.resourceTypeSupportPermissions('project', {}, [
      'update',
      'delete',
    ]);
    this.route.params.subscribe(async params => {
      this.data.uuid = params['uuid'];
      this.data.name = params['name'];
    });

    this.refetch();
  }

  async refetch() {
    try {
      if (this.project) {
        this.data = this.project;
      } else {
        this.data = await this.projectService.getProject(
          this.data.uuid || this.data.name,
        );
      }
      this.data.clusters = await this.projectService
        .getProjectQuota(this.data.name)
        .then(({ clusters }) => clusters);

      this.clustersMap = {
        kubernetes: [],
        PCF: [],
      };

      if (!this.data.clusters.length) {
        return;
      }

      this.tabGroup.selectedIndex = 0;
      this.tabGroup.onSelectedIndexChange(0);

      this.clustersTitle = this.data.clusters
        .map(region => region.display_name || region.name)
        .join(', ');
      this.data.clusters.forEach((region: PreAddRegionData) => {
        switch (region.service_type) {
          case 'kubernetes':
            this.clustersMap.kubernetes.push(region);
            break;
          case 'PCF':
            this.clustersMap.PCF.push(region);
            break;

          default:
            // 更新如果不传service_type该字段会被置空, 后端暂时不好解决
            this.clustersMap.kubernetes.push(region);
            break;
        }

        const regionPie: any = {};
        Object.keys(this.curQuota).forEach(key => {
          regionPie[key] = this.getPieData(
            region.quota[key].used,
            region.quota[key].max,
          );
        });
        this.pieDataMap[region.uuid] = regionPie;
      });
      this.curCluster = this.clustersMap.kubernetes[0];
      this.curQuota = this.clustersMap.kubernetes[0].quota;
      this.curPieData = this.pieDataMap[this.curCluster.uuid];
    } catch (error) {
      this.errorToast.error(error);
      if (!this.isUserView) {
        this.router.navigateByUrl('/projects');
      }
    }
  }

  regionChange($event: any) {
    this.curQuota = $event.quota;
    this.curPieData = this.pieDataMap[$event.uuid];
  }

  keys(obj: object) {
    return Object.keys(obj);
  }

  getPieData(used: number, max: number): SerieChartData[] {
    return [
      {
        name: this.translateService.get('chart_status_USED'),
        value: used,
      },
      {
        name: this.translateService.get('chart_status_UNUSED'),
        value: max - used,
      },
    ];
  }

  showUpdateQuotaDialog(): void {
    const modelRef = this.modalService.open(QuotaUpdateComponent, {
      title: this.translateService.get('update_quota'),
      width: 800,
    });
    modelRef.componentInstance.data.project_uuid = this.data.uuid;
    modelRef.componentInstance.project_name = this.data.name;
    modelRef.componentInstance.curCluster = this.curCluster.uuid;
    modelRef.componentInstance.clusters = this.data.clusters;
    modelRef.componentInstance.finished.subscribe((res: boolean | null) => {
      modelRef.close();
      if (res) {
        this.refetch();
      }
    });
  }

  showDeleteDialog() {
    const modelRef = this.modalService.open(DeleteProjectComponent, {
      title: this.translateService.get('delete_project'),
      width: 600,
    });
    modelRef.componentInstance.projectName = this.data.name;
    modelRef.componentInstance.displayName = this.data.display_name;
    modelRef.componentInstance.finished.subscribe((res: boolean) => {
      if (res) {
        modelRef.componentInstance.deleteLoading = true;
        this.projectService
          .deleteProject(this.data.uuid)
          .then(() => {
            this.store.dispatch(
              new projectActions.DeleteProjectSuccess(this.data.name),
            );
            this.auiNotificationService.success(
              this.translateService.get('delete_success'),
            );
            modelRef.close();
            this.router.navigateByUrl('/projects');
            // this.store.dispatch(new projectActions.SetProject(''));
          })
          .catch(error => {
            this.errorToast.error(error);
          })
          .then(() => (modelRef.componentInstance.deleteLoading = false));
      } else if (res === false) {
        modelRef.close();
      }
    });
  }

  showUpdateBasicInfoDialog(): void {
    const modelRef = this.modalService.open(ProjectBasicInfoUpdateComponent, {
      title: this.translateService.get('update_basic_info'),
      width: 600,
    });
    modelRef.componentInstance.uuid = this.data.uuid;
    modelRef.componentInstance.name = this.data.name;
    modelRef.componentInstance.data = {
      display_name: this.data.display_name,
      description: this.data.description,
    };
    modelRef.componentInstance.finished.subscribe(
      (res: { display_name: string; description: string }) => {
        modelRef.close();
        if (res) {
          this.projectService
            .getProject(this.data.uuid)
            .then((res: Project) => {
              this.data.display_name = res.display_name;
              this.data.description = res.description;
              this.data.updated_at = res.updated_at;
            });
        }
      },
    );
  }

  checkCustomer(user: string) {
    return this.environments.vendor_customer === VendorCustomer[user];
  }
}
