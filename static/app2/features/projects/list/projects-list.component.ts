import { Component, Inject, OnInit } from '@angular/core';
import { VendorCustomer } from 'app2/core/customization/vendor-customer.types';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import {
  Project,
  ProjectService,
} from 'app2/shared/services/features/project.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { BehaviorSubject, Observable, combineLatest, from } from 'rxjs';
import { first, map, publishReplay, refCount } from 'rxjs/operators';

@Component({
  selector: 'rc-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.scss'],
})
export class ProjectsListComponent implements OnInit {
  isCMB = true;
  loading = true;
  createPermission: boolean;
  viewPermission: boolean;
  list$: Observable<Project[]>;
  filterString = '';
  filterString$: BehaviorSubject<string> = new BehaviorSubject('');
  columns: string[];

  constructor(
    private projectService: ProjectService,
    private roleUtil: RoleUtilitiesService,
    @Inject(ENVIRONMENTS) private environments: Environments,
  ) {}

  async ngOnInit() {
    this.isCMB = this.environments.vendor_customer === VendorCustomer.CMB;
    if (this.isCMB) {
      this.columns = ['name', 'service_type', 'admin_list', 'created_at'];
    } else {
      this.columns = ['name', 'clusters', 'admin_list', 'created_at'];
    }
    [
      this.createPermission,
      this.viewPermission,
    ] = await this.roleUtil.resourceTypeSupportPermissions('project', {}, [
      'create',
      'view',
    ]);
    const projectList$ = from(this.projectService.getProjects()).pipe(
      publishReplay(1),
      refCount(),
    );
    projectList$.pipe(first()).subscribe(() => {
      this.loading = false;
    });
    this.list$ = combineLatest(projectList$, this.filterString$).pipe(
      map(([projects, filterString]) =>
        projects.filter(
          (project: Project) =>
            project.name.includes(filterString) ||
            project.display_name.includes(filterString),
        ),
      ),
    );
  }

  getClusters(row: { clusters: { display_name: string; name: string }[] }) {
    const clusters = row.clusters || [];
    return clusters
      .map((item: { display_name: string; name: string }) => {
        return item.display_name || item.name;
      })
      .filter(name => !!name)
      .join(', ');
  }

  getServiceType(row: { service_type: string[] }) {
    return row.service_type && row.service_type.length
      ? row.service_type.join(', ')
      : '-';
  }

  getAdminList(row: { admin_list: string[] }) {
    return row.admin_list && row.admin_list.length
      ? row.admin_list.join(', ')
      : '-';
  }
}
