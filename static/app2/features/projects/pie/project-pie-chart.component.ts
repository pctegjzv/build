import { Component, Input, OnInit } from '@angular/core';
import {
  PieChartConfig,
  SerieChartData,
} from 'app2/features/dashboard/charts/ngx-charts.types';

@Component({
  selector: 'rc-project-pie-chart',
  templateUrl: './project-pie-chart.component.html',
  styleUrls: ['./project-pie-chart.component.scss'],
})
export class ProjectPieChartComponent implements OnInit {
  @Input()
  data: SerieChartData[] = [];
  @Input()
  unit: string;
  @Input()
  title: string;
  chartConfig: PieChartConfig = {
    scheme: { domain: ['#36b37e', '#ededed'] },
    doughnut: true,
  };

  constructor() {}

  ngOnInit(): void {}

  getPercent() {
    const data: number = Math.round(
      (this.data[0].value / (this.data[0].value + this.data[1].value)) * 100,
    );
    return data ? data + '%' : 0 + '%';
  }
}
