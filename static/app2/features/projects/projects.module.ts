import { NgModule } from '@angular/core';
import { ProjectBasicInfoUpdateComponent } from 'app2/features/projects/basic-info-update/basic-info-update.component';
import { DeleteProjectComponent } from 'app2/features/projects/delete/delete-project.component';
import { ProjectPieChartComponent } from 'app2/features/projects/pie/project-pie-chart.component';
import { QuotaUpdateComponent } from 'app2/features/projects/quota-update/quota-update.component';
import { ProjectRoleListComponent } from 'app2/features/projects/role-list/project-role-list.component';
import { SharedModule } from 'app2/shared/shared.module';
import { StepsProcessComponent } from 'app_user/shared/components/steps-process/steps-process.component';

import { ProjectAccountsListComponent } from './accounts-list/accounts-list.component';
import { ProjectCreateComponent } from './create/project-create.component';
import { ProjectDetailComponent } from './detail/project-detail.component';
import { ProjectsListComponent } from './list/projects-list.component';
import { ProjectsRoutingModule } from './projects.routing.module';

@NgModule({
  imports: [SharedModule, ProjectsRoutingModule],
  declarations: [
    ProjectsListComponent,
    ProjectDetailComponent,
    ProjectCreateComponent,
    ProjectAccountsListComponent,
    ProjectPieChartComponent,
    QuotaUpdateComponent,
    ProjectBasicInfoUpdateComponent,
    StepsProcessComponent,
    ProjectRoleListComponent,
    DeleteProjectComponent,
  ],
  entryComponents: [
    QuotaUpdateComponent,
    ProjectBasicInfoUpdateComponent,
    DeleteProjectComponent,
  ],
})
export class ProjectsModule {}
