import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProjectCreateComponent } from './create/project-create.component';
import { ProjectDetailComponent } from './detail/project-detail.component';
import { ProjectsListComponent } from './list/projects-list.component';

const routes: Routes = [
  {
    path: '',
    component: ProjectsListComponent,
  },
  {
    path: 'detail/:name',
    component: ProjectDetailComponent,
  },
  {
    path: 'create',
    component: ProjectCreateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class ProjectsRoutingModule {}
