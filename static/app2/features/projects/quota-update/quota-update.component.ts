import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NotificationService } from 'alauda-ui';
import { ProjectService } from 'app2/shared/services/features/project.service';
import { TranslateService } from 'app2/translate/translate.service';
import { cloneDeep } from 'lodash';

interface PreSubmitData {
  project_uuid: string;
  service_type: string;
  clusters: Cluster[];
}

interface Cluster {
  uuid: string;
  service_type?: string;
  quota: {
    cpu: number;
    memory: number;
    pvc_num: number;
    pods: number;
    storage: number;
  };
}

interface ClustersDisplayField {
  uuid: string;
  display_name: string;
  service_type: string;
}

@Component({
  selector: 'rc-quota-update',
  templateUrl: './quota-update.component.html',
  styleUrls: ['./quota-update.component.scss'],
})
export class QuotaUpdateComponent implements OnInit {
  @Output()
  finished = new EventEmitter<any>();
  data: PreSubmitData = {
    project_uuid: '',
    service_type: 'kubernetes',
    clusters: [
      {
        uuid: '',
        quota: {
          cpu: 0,
          memory: 0,
          storage: 0,
          pvc_num: 0,
          pods: 0,
        },
      },
    ],
  };
  project_name = '';
  curCluster = '';
  clusters: any[] = [];

  clustersDisplayField: ClustersDisplayField[] = [];

  constructor(
    private projectService: ProjectService,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
  ) {}

  ngOnInit(): void {
    this.clustersDisplayField = this.clusters.map(cluster => ({
      display_name: `${cluster.service_type} ${cluster.display_name} (${
        cluster.name
      })`,
      uuid: cluster.uuid,
      service_type: cluster.service_type,
    }));
    this.clusters = this.clusters.map(cluster => ({
      uuid: cluster.uuid,
      service_type: cluster.service_type,
      quota: {
        cpu: cluster.quota.cpu.max,
        memory: cluster.quota.memory.max,
        storage: cluster.quota.storage.max,
        pvc_num: cluster.quota.pvc_num.max,
        pods: cluster.quota.pods.max,
      },
    }));
    this.clusterSelect({ uuid: this.curCluster });
  }

  clusterSelect($event: { uuid: string }) {
    this.clusters.some((cluster: Cluster) => {
      if (cluster.uuid === $event.uuid) {
        this.data.clusters[0] = {
          uuid: cluster.uuid,
          service_type: cluster.service_type,
          quota: cloneDeep(cluster.quota),
        };
        return true;
      }
    });
  }

  submit() {
    this.projectService
      .updateProjectQuota(this.project_name, this.data)
      .then(() => {
        this.auiNotificationService.success(
          this.translateService.get('update_success'),
        );
        this.finished.next(true);
      })
      .catch(({ errors }) => {
        this.auiNotificationService.error(
          this.translateService.get(errors[0].code),
        );
      });
  }

  cancel() {
    this.finished.next(null);
  }
}
