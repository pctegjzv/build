import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ACCOUNT, ENVIRONMENTS, WEBLABS } from 'app2/core/tokens';
import {
  Environments,
  RcAccount,
  RcProfile,
  RcRole,
  Weblabs,
} from 'app2/core/types';
import { TagsInputComponent } from 'app2/shared/components/tags-input/tags-input.component';
import { AccountService } from 'app2/shared/services/features/account.service';
import { OrgService } from 'app2/shared/services/features/org.service';
import {
  PatternHelperService,
  PwdPattern,
} from 'app2/shared/services/features/pattern-helper.service';
import { RoleService } from 'app2/shared/services/features/role.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { pick, remove } from 'lodash';

@Component({
  selector: 'rc-app-account-create',
  templateUrl: './account-create.component.html',
  styleUrls: ['./account-create.component.scss'],
})
export class AccountCreateComponent implements OnInit {
  selectedRoles: Array<RcRole> = [];
  accountNames: Array<string> = [];
  privateDeploy: boolean;
  profile: RcProfile;
  username: string;
  passwordPatternObj: PwdPattern;
  changePasswordErrorMapper: any;
  password = '';
  randomPassword = false;
  showMoreInfo = false;
  passwordView = false;
  helpText: string;
  initialized: boolean;
  createSuccess: boolean;
  submitting: boolean;
  office_address: string;
  position: string;
  department: string;
  users: Array<RcRole> = [];
  allRoles: Array<RcRole> = [];
  @ViewChild('accountCreateForm')
  form: NgForm;
  @ViewChild('tagsInput')
  tagsInput: TagsInputComponent;
  @Output()
  afterConfirmed: EventEmitter<any> = new EventEmitter();

  constructor(
    private modalService: ModalService,
    private orgService: OrgService,
    private roleService: RoleService,
    private translate: TranslateService,
    private accountService: AccountService,
    private errorHandle: ErrorsToastService,
    private patternHelperService: PatternHelperService,
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(ENVIRONMENTS) public environments: Environments,
  ) {}

  async ngOnInit() {
    this.accountNames = [];
    this.privateDeploy = this.environments.is_private_deploy_enabled;
    this.profile = await this.accountService.getAuthProfile();
    this.passwordPatternObj = this.patternHelperService.getPasswordPattern();
    // this.rbChangePasswordErrorMapper = rbChangePasswordErrorMapper;
    this.changePasswordErrorMapper = this.patternHelperService.getChangePasswordErrorMapper();
    this.allRoles = await this.roleService
      .getRoleList({ page_size: 0, ignoreProject: true })
      .then(({ results }) => results);
    this.tagsInput.registerBeforeAdd((value: string) => {
      const reg = /^[A-Za-z0-9]{3,30}$/;
      return reg.test(value);
    });

    this.helpText = this.translate.get('org_account_create_help', {
      email: this.profile.email,
    });
    this.initialized = true;
  }

  responseItemFilter(option: RcRole) {
    return !this.selectedRoles.find(
      (item: RcRole) => item.uuid === option.uuid,
    );
  }

  onRoleAdd(option: any) {
    this.selectedRoles.push(option);
  }

  onRoleRemove(option: RcRole) {
    remove(this.selectedRoles, (item: RcRole) => item.uuid === option.uuid);
  }

  confirmButtonDisabled() {
    // console.log(this.initialized, this.accountNames, this.form.invalid)
    return !this.initialized || !this.accountNames.length || this.form.invalid;
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;
    try {
      const data: any = await this.orgService.createRoleBasedAccounts({
        usernames: this.accountNames,
        roles: this.selectedRoles.map((item: RcRole) => {
          return {
            name: item.name,
            uuid: item.uuid,
          };
        }),
        password: this.randomPassword ? '' : this.password,
        ...pick(this, 'department', 'position', 'office_address'),
      });
      this.afterConfirmed.emit();

      if (this.passwordView) {
        this.users = data.result;
        this.createSuccess = true;
      } else {
        this.ok();
      }
    } catch (e) {
      this.createSuccess = false;
      this.errorHandle.error(e);
    } finally {
      this.submitting = false;
    }
  }

  cancel() {
    this.modalService.closeAll();
  }

  ok() {
    this.modalService.closeAll();
  }
}
