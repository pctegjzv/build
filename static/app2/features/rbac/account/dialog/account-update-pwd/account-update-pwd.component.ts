import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NotificationService } from 'alauda-ui';
import { VendorCustomer } from 'app2/core/customization/vendor-customer.types';
import { ACCOUNT, ENVIRONMENTS, WEBLABS } from 'app2/core/tokens';
import { Environments, RcAccount, Weblabs } from 'app2/core/types';
import { AccountService } from 'app2/shared/services/features/account.service';
import {
  PasswordType,
  PatternHelperService,
  PwdPattern,
} from 'app2/shared/services/features/pattern-helper.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-app-account-update-pwd',
  templateUrl: './account-update-pwd.component.html',
  styleUrls: ['./account-update-pwd.component.scss'],
})
export class AccountUpdatePwdComponent implements OnInit {
  isSubAccount: boolean;
  passwordNotSame: boolean;
  passwordPatternObj: PwdPattern;
  getPasswordStrength: Function;
  currentUserName: String;
  confirm_new_password: string;
  new_password: string;
  submitting: boolean;
  old_password: string;
  changePasswordErrorMapper: any;
  username: string;
  accountSubType: string;
  isCmb = false;
  @ViewChild('updatePasswordForm')
  form: NgForm;

  constructor(
    private translate: TranslateService,
    private accountService: AccountService,
    private modalService: ModalService,
    private auiNotificationService: NotificationService,
    private patternHelperService: PatternHelperService,
    private errorHandle: ErrorsToastService,
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(MODAL_DATA) private modalData: any,
    @Inject(ENVIRONMENTS) public environments: Environments,
  ) {}

  async ngOnInit() {
    this.isSubAccount = this.accountService.isSubAccount();
    this.isCmb = this.environments.vendor_customer === VendorCustomer.CMB;
    this.passwordNotSame = false;
    this.passwordPatternObj = this.patternHelperService.getPasswordPattern(
      this.isCmb ? PasswordType.UAA : '',
    );
    this.getPasswordStrength = this.patternHelperService.getPasswordStrength;
    this.changePasswordErrorMapper = this.patternHelperService.getChangePasswordErrorMapper(
      this.isCmb ? PasswordType.UAA : '',
    );
    this.currentUserName = this.account.username;
    this.username = this.modalData.username || '';
  }

  checkPasswordStrength = (value: string) =>
    !value ||
    this.getPasswordStrength(value) >= this.passwordPatternObj.strength;

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    const params: any = {
      username: this.username || undefined,
      password: this.new_password,
    };
    if ((!this.isSubAccount && !this.username) || this.isSubAccount) {
      params.old_password = this.old_password;
    }
    this.submitting = true;

    try {
      if (this.username) {
        this.isCmb
          ? await this.accountService.updateUaaAccountPassword(params)
          : await this.accountService.updateSubAccountPassword(params);
      } else {
        await this.accountService.updateProfile(params as any);
      }
      this.auiNotificationService.success({
        content: this.translate.get('update_password_success'),
      });
      this.modalService.closeAll();
    } catch (e) {
      this.errorHandle.error(e);
    } finally {
      this.submitting = false;
    }
  }

  cancel = () => {
    this.modalService.closeAll();
  };

  showOldPasswordField() {
    return this.isSubAccount
      ? this.currentUserName === this.username
      : !this.username;
  }

  get cPwdPattern() {
    return this.new_password === this.confirm_new_password ? /\.*/ : /\.{,0}/;
  }
}
