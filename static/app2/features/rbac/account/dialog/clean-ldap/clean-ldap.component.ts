import {
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  OnInit,
  Output,
} from '@angular/core';
import { NotificationService } from 'alauda-ui';
import { ACCOUNT, WEBLABS } from 'app2/core/tokens';
import { RcAccount, Weblabs } from 'app2/core/types';
import { ConfirmBoxComponent } from 'app2/shared/components/confirm-box/confirm-box.component';
import { OrgService } from 'app2/shared/services/features/org.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';
import $ from 'jquery';
import { debounce } from 'lodash';

@Component({
  selector: 'rc-app-clean-ldap',
  templateUrl: './clean-ldap.component.html',
  styleUrls: ['./clean-ldap.component.scss'],
})
export class CleanLdapComponent implements OnInit {
  searchAccounts: Function;
  invalidAccounts: number;
  search: string;
  accounts: Array<RcAccount>;
  count: number;
  accountsLoading: boolean;
  @Output()
  afterConfirmed: EventEmitter<any> = new EventEmitter();

  constructor(
    private translate: TranslateService,
    private modalService: ModalService,
    private auiNotificationService: NotificationService,
    private orgService: OrgService,
    private elementRef: ElementRef,
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(MODAL_DATA) private modalData: any,
  ) {}

  ngOnInit() {
    this.searchAccounts = debounce(this.fetchAccountList, 300);
    this.invalidAccounts = this.modalData.invalidAccounts || 0;
    this.search = this.modalData.searchText;
    this.fetchAccountList();
  }

  async fetchAccountList() {
    this.accountsLoading = true;
    try {
      const ret: any = await this.orgService.listOrgFilteredAccounts({
        search: this.search,
        page_size: 0,
        invalid_ldap: 'true',
      });

      this.accounts = ret.results;
      this.count = ret.count;
    } catch (e) {
      this.accounts = [];
    } finally {
      this.accountsLoading = false;
    }
  }

  async deleteUsers(username: string) {
    const $modal = $(this.elementRef.nativeElement).parents('.rc-modal');
    $modal.css('cssText', 'display:none !important');
    const modalRef = this.modalService.open(ConfirmBoxComponent, {
      title: this.translate.get('delete_invalid_accounts'),
    });

    modalRef.componentInstance.content = `<h4>${this.translate.get(
      username
        ? 'confrim_delete_invalid_user_tip'
        : 'confrim_delete_all_invalid_users_tip',
      {
        username,
        invalid_accounts: this.invalidAccounts,
      },
    )}</h4><div style="color:#a1a1a1">${this.translate.get(
      'confirm_delete_invalid_users_tip',
    )}</div>`;

    modalRef.componentInstance.confirmed.subscribe(async (res: boolean) => {
      if (res) {
        $modal.css('cssText', 'display:none !important');
        await this.orgService.deleteLdapAccounts({
          delete_all: !username,
          usernames: username,
        });

        this.auiNotificationService.success({
          content: this.translate.get(
            username ? 'delete_success' : 'delete_invalid_accounts_success',
            {
              invalid_accounts: this.invalidAccounts,
            },
          ),
        });

        this.afterConfirmed.emit({
          reopen: username && this.invalidAccounts > 1,
          search: this.search,
        });
      } else {
        $modal.css('cssText', 'display:block !important');
      }
      modalRef.close();
    });
  }

  cancel() {
    this.modalService.closeAll();
  }
}
