import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ACCOUNT, WEBLABS } from 'app2/core/tokens';
import { RcAccount, RcRole, Weblabs } from 'app2/core/types';
import { OrgService } from 'app2/shared/services/features/org.service';
import { RoleService } from 'app2/shared/services/features/role.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { filter, find, findIndex, forEach } from 'lodash';

@Component({
  selector: 'rc-app-roles-add',
  templateUrl: './account-roles-add-dialog.component.html',
  styleUrls: ['./account-roles-add-dialog.component.scss'],
})
export class RolesAddComponent implements OnInit {
  selectedRoles: Array<RcRole> = [];
  accountRoles: Array<RcRole> = [];
  avaliableRoles: Array<RcRole> = [];
  initialized: boolean;
  roleBackendUrl: string;
  submitting: boolean;
  username: string;
  filterRoles: Array<RcRole> = [];
  @ViewChild('roleAddForm')
  roleAddForm: NgForm;

  constructor(
    private modalService: ModalService,
    private orgService: OrgService,
    private roleService: RoleService,
    private errorHandle: ErrorsToastService,
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(MODAL_DATA) private modalData: any,
  ) {}

  async ngOnInit() {
    this.username = this.modalData.username || '';
    this.initialized = false;
    this.avaliableRoles = [];

    const allRoles = await this.roleService
      .getRoleList({
        page_size: 0,
        ignoreProject: true,
      })
      .then(({ results }) => results);
    await this.fetchAccountRoles();
    this.filterRoles = this.filterNonExistRoles(allRoles);
    this.initialized = true;
  }

  fetchAccountRoles() {
    return this.orgService
      .getAccountRoles(this.username)
      .then((data: any) => {
        this.accountRoles = data.result;
      })
      .catch(() => {
        this.accountRoles = [];
      });
  }

  onRoleAdd(value: RcRole) {
    this.selectedRoles.push(value);
  }

  onRoleRemove(value: RcRole) {
    const index = findIndex(this.selectedRoles, item => {
      return item.uuid === value.uuid;
    });
    this.selectedRoles.splice(index, 1);
  }

  rolesFilter(item: RcRole) {
    return !find(this.accountRoles, (accountRole: RcRole) => {
      return item.uuid === accountRole.role_uuid;
    });
  }

  filterNonExistRoles(allRoles: Array<RcRole>): Array<RcRole> {
    const newRoles: Array<RcRole> = [];
    forEach(allRoles, (role: RcRole) => {
      if (!filter(this.accountRoles, { role_uuid: role.uuid }).length) {
        newRoles.push(role);
      }
    });
    return newRoles;
  }

  confirm() {
    this.roleAddForm.onSubmit(null);
    if (!this.selectedRoles.length) {
      return;
    }
    this._addRoles();
  }

  cancel() {
    this.modalService.closeAll();
  }

  _addRoles() {
    this.submitting = true;
    const _roles = this.selectedRoles.map(item => {
      return {
        name: item.name,
      };
    });
    return this.orgService
      .addAccountRoles(this.username, _roles)
      .then(() => {
        this.modalService.closeAll();
      })
      .catch(e => this.errorHandle.error(e))
      .then(() => {
        this.submitting = false;
      });
  }
}
