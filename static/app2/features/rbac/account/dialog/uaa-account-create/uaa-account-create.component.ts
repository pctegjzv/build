import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DialogRef } from 'alauda-ui';
import {
  AccountService,
  UaaAccount,
} from 'app2/shared/services/features/account.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

interface ValidateDictionary {
  type: string;
  name: string;
  displayName: string;
  minLength?: number;
  maxLength?: number;
  pattern?: RegExp | string;
  errorPattern?: string;
}

@Component({
  templateUrl: './uaa-account-create.component.html',
  styleUrls: ['./uaa-account-create.component.scss'],
})
export class UaaAccountCreateComponent implements OnInit {
  user: UaaAccount = {};
  patternDictionary: ValidateDictionary[] = [];
  loading: boolean;
  @ViewChild('form')
  form: NgForm;

  constructor(
    private accountService: AccountService,
    private errorToast: ErrorsToastService,
    private dialogRef: DialogRef<any>,
    private translate: TranslateService,
  ) {}

  ngOnInit() {
    this.patternDictionary = [
      {
        type: 'text',
        name: 'username',
        displayName: this.translate.get('username'),
        minLength: 4,
        maxLength: 32,
        pattern: /^[A-Za-z0-9]{4,32}$/,
        errorPattern: this.translate.get('username_pattern_error'),
      },
      {
        type: 'email',
        name: 'email',
        displayName: this.translate.get('email'),
        minLength: 4,
        pattern: /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
      },
    ];
  }

  async submit() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    try {
      await this.accountService.createUaaAccount({
        usernames: [this.user.username],
        email: this.user.email,
      });
      this.dialogRef.close('close');
    } catch (e) {
      this.errorToast.error(e);
    }
    this.loading = false;
  }
}
