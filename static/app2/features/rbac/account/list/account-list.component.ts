import { Component, Inject, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DialogService, NotificationService } from 'alauda-ui';
import { VendorCustomer } from 'app2/core/customization/vendor-customer.types';
import { ACCOUNT, ENVIRONMENTS, WEBLABS } from 'app2/core/tokens';
import { Environments, RcAccount, RcProfile, Weblabs } from 'app2/core/types';
import { AccountCreateComponent } from 'app2/features/rbac/account/dialog/account-create/account-create.component';
import { AccountUpdatePwdComponent } from 'app2/features/rbac/account/dialog/account-update-pwd/account-update-pwd.component';
import { CleanLdapComponent } from 'app2/features/rbac/account/dialog/clean-ldap//clean-ldap.component';
import { RolesAddComponent } from 'app2/features/rbac/account/dialog/roles-add/account-roles-add-dialog.component';
import { UaaAccountCreateComponent } from 'app2/features/rbac/account/dialog/uaa-account-create/uaa-account-create.component';
import { UserAddComponent } from 'app2/features/rbac/roles/dialog/user-add//user-add.component';
import { ConfirmBoxComponent } from 'app2/shared/components/confirm-box/confirm-box.component';
import { TooltipDirective } from 'app2/shared/directives/tooltip/tooltip.directive';
import { AccountService } from 'app2/shared/services/features/account.service';
import { OrgService } from 'app2/shared/services/features/org.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { RoleService } from 'app2/shared/services/features/role.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { getCookie } from 'app2/utils/cookie';
import { forEach } from 'lodash';

@Component({
  selector: 'rc-app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.scss'],
})
export class AccountListComponent implements OnInit {
  @Input()
  project: string;
  isSubAccount: boolean;
  initialized: boolean;
  accounts: Array<RcAccount>;
  ldapConfigTrue: boolean;
  pagination: {
    page: number;
    count: number;
    page_size: number;
  };
  popoverConfig: any;
  user: any;
  accountsLoading: boolean;
  createAccountEnabled: boolean;
  roleAssignEnabled: boolean;
  ldapSyncEnabled: boolean;
  addUserEnabled: boolean;
  search: string;
  searching: boolean;
  invalid_accounts: number;
  userRolesCache: any = {};
  submitting: boolean;
  triggerLdapSyncIng: boolean;
  profile: RcProfile;
  isAdmin: boolean;
  isCmb = false;

  constructor(
    private translate: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private router: Router,
    private roleService: RoleService,
    private orgService: OrgService,
    private accountService: AccountService,
    private modalService: ModalService,
    private errorHandle: ErrorsToastService,
    private notificationService: NotificationService,
    private dialogService: DialogService,
    @Inject(ENVIRONMENTS) public environments: Environments,
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(WEBLABS) public weblabs: Weblabs,
  ) {}

  async ngOnInit() {
    this.isSubAccount = this.accountService.isSubAccount();
    this.initialized = false;
    this.isCmb = this.environments.vendor_customer === VendorCustomer.CMB;
    this.accounts = [];
    let ldapConfig: any;
    try {
      if (!this.isSubAccount) {
        ldapConfig = await this.orgService.getOrgLdap();
      } else {
        ldapConfig = await this.orgService.getOrgLdapSync();
      }
    } catch (e) {
      ldapConfig = null;
    }
    this.ldapConfigTrue = ldapConfig && !ldapConfig.empty;
    this.pagination = {
      page: 1,
      count: 0,
      page_size: 20,
    };
    this.popoverConfig = {
      list: [],
      loading: false,
      username: '',
      noDataText: this.translate.get('account_has_no_role'),
    };
    this.user = await this.orgService.getOrgAccount({
      username: this.account.username,
    });

    await this.fetchBasicInfo();
    this.profile = await this.accountService.getAuthProfile();
    this.isAdmin = this.profile.is_admin;
    this.initialized = true;
    //TPAccount ，Third party account type
    this.createAccountEnabled =
      (await this.roleUtil.resourceTypeSupportPermissions('subaccount')) &&
      (this.user.type !== 'organizations.TPAccount' || this.isCmb);
    this.roleAssignEnabled = await this.roleUtil.resourceTypeSupportPermissions(
      'role',
      {},
      'assign',
    );
    this.ldapSyncEnabled = await this.roleUtil.resourceTypeSupportPermissions(
      'organization',
      {},
      'sync_users',
    );

    // rbSafeApply();
  }

  async fetchAccountList() {
    this.accountsLoading = true;
    try {
      const { results, count } = (await this.orgService.listOrgFilteredAccounts(
        {
          search: this.search,
          page: this.pagination.page,
          page_size: this.pagination.page_size,
          project_name: this.project || '',
          ignoreProject: true,
        },
      )) as { results?: Array<RcAccount>; count?: number };

      this.accounts = results;
      this.pagination.count = count;
    } catch (e) {
      this.accounts = [];
    } finally {
      this.accountsLoading = false;
    }
  }

  async getOrgLdapInfo() {
    const data = (await this.orgService.getOrgLdapInfo()) as {
      result?: { invalid_accounts: number };
    };
    this.invalid_accounts = data.result.invalid_accounts;
  }

  fetchBasicInfo() {
    let promises: Promise<any> | Array<Promise<any>> = this.fetchAccountList();
    if (!this.isSubAccount) {
      promises = Promise.all([promises, this.getOrgLdapInfo()]);
    }
    return promises;
  }

  onSearchChanged(value: string) {
    this.search = value;
    this.pagination.page = 1;
    this.searching = true;
    this.fetchAccountList().then(() => {
      this.searching = false;
    });
  }

  async deleteInvalidAccounts(searchText: string) {
    const modalRef = this.modalService.open(CleanLdapComponent, {
      width: 800,
      title: this.translate.get('clean_invalid_ldap_users'),
      data: {
        invalidAccounts: this.invalid_accounts,
        searchText,
      },
    });
    modalRef.componentInstance.afterConfirmed.subscribe(async (ret: any) => {
      await this.fetchBasicInfo();
      this.modalService.closeAll();
      if (ret.reopen) {
        this.deleteInvalidAccounts(ret.search);
      }
    });
  }

  pageNoChange(page: number) {
    this.pagination.page = page;
    this.fetchAccountList();
  }

  async updatePassword(item: RcAccount) {
    this.modalService.open(AccountUpdatePwdComponent, {
      title: this.translate.get('update_password'),
      data: {
        username: item.username,
      },
    });
  }

  async addRole(account: RcAccount) {
    this.modalService.open(RolesAddComponent, {
      width: 600,
      title: this.translate.get('add_role'),
      data: {
        username: account.username,
      },
    });
  }

  async deleteAccount(item: RcAccount) {
    try {
      const modalRef = this.modalService.open(ConfirmBoxComponent, {
        title: this.translate.get('delete'),
      });

      modalRef.componentInstance.content = this.translate.get(
        'confirm_remove_account_from_org',
        {
          username: item.username,
        },
      );
      modalRef.componentInstance.confirmed.subscribe((res: boolean) => {
        modalRef.close(res);
        if (res) {
          this._deleteAccount(item);
        }
      });
    } catch (e) {
      this.errorHandle.error(e);
    }
  }

  async _deleteAccount(item: RcAccount) {
    this.submitting = true;
    try {
      await (this.isInvalidAccount(item)
        ? this.orgService.deleteLdapAccounts({
            usernames: item.username,
          })
        : this.orgService.removeOrgAccount({
            username: item.username,
          }));

      await this.fetchBasicInfo();
    } finally {
      this.submitting = false;
    }
  }

  fetchAccountRoles(username: string, tooltip: TooltipDirective) {
    this.popoverConfig.username = username;
    this.popoverConfig.list = [];
    this.popoverConfig.loading = true;
    //$timeout
    setTimeout(() => {
      this.orgService
        .getAccountRoles(username)
        .then((data: { result?: any }) => {
          this.userRolesCache[username] = data.result;
          this.popoverConfig.list = this.userRolesCache[username];
          tooltip.updatePosition();
        })
        .catch(e => this.errorHandle.error(e))
        .then(() => {
          this.popoverConfig.loading = false;
        });
    }, 200);
  }

  revokeRole(role_name: string) {
    const users = [
      {
        user: this.popoverConfig.username,
      },
    ];
    const list = this.userRolesCache[this.popoverConfig.username];
    this.popoverConfig.loading = true;
    this.roleService
      .deleteRoleUsers(role_name, users)
      .then(() => {
        forEach(list, (item, index) => {
          if (item.role_name === role_name) {
            list.splice(index, 1);
            return false;
          }
        });
      })
      .catch(e => this.errorHandle.error(e))
      .then(() => {
        this.popoverConfig.loading = false;
      });
  }

  async createAccounts() {
    if (this.isCmb) {
      const uaaModalRef = this.dialogService.open(UaaAccountCreateComponent);
      uaaModalRef.afterClosed().subscribe(r => {
        if (r === 'close') {
          this.fetchAccountList();
        }
      });
      return;
    }
    const modalRef = this.modalService.open(AccountCreateComponent, {
      width: 600,
      title: this.translate.get('create_account'),
    });
    modalRef.componentInstance.afterConfirmed.subscribe(() => {
      this.fetchAccountList();
    });
  }

  async addUsers() {
    const modalRef = this.modalService.open(UserAddComponent, {
      width: 600,
      title: this.translate.get('add_user'),
      data: {
        users: this.accounts,
      },
    });
    modalRef.componentInstance.afterConfirmed.subscribe(() => {
      this.fetchAccountList();
    });
  }

  buttonDisplayExpr(item: any, action: string) {
    const accountTypeTest =
      item.type !== 'organizations.LDAPAccount' ||
      (item.is_valid
        ? !['delete', 'update_password'].includes(action)
        : action === 'delete' && !this.isSubAccount);
    return (
      accountTypeTest &&
      this.roleUtil.resourceHasPermission(item, 'subaccount', action)
    );
  }

  isInvalidAccount(account: any) {
    return account.type === 'organizations.LDAPAccount' && !account.is_valid;
  }

  checkRoleResourceAction(item: any, action: string) {
    return this.roleUtil.resourceHasPermission(item, 'role', action);
  }

  async triggerLdapSync() {
    this.triggerLdapSyncIng = true;
    try {
      await this.orgService.triggerOrgLdapSync({
        orgName: this.account.namespace,
      });
      await this.fetchBasicInfo();
    } catch (e) {
      this.errorHandle.error(e);
    } finally {
      this.triggerLdapSyncIng = false;
    }
  }

  getAccountRoleNameDisplay(item: any) {
    return item.project_name
      ? `${item.project_name}/${item.role_name}`
      : item.role_name;
  }

  viewAccountRoleDetail(item: any, $event: Event) {
    $event.preventDefault();
    const currentProjectName = getCookie('project');
    if (
      this.weblabs.PROJECTS_ENABLED &&
      (item.project_name && item.project_name === currentProjectName)
    ) {
      this.router.navigate(['rbac/roles/detail'], {
        queryParams: {
          role_name: item.role_name,
        },
      });
    } else {
      this.router.navigateByUrl(
        `/rbac/roles/detail?role_name=${item.role_name}`,
      );
    }
  }

  shouldShowRoleLink(item: any) {
    if (this.weblabs.PROJECTS_ENABLED) {
      const currentProjectName = getCookie('project');
      return (
        !item.project_name ||
        (currentProjectName && currentProjectName === item.project_name)
      );
    }
    return true;
  }

  gotoUserDetail(username: string) {
    this.router.navigateByUrl(`/user/detail?username=${username}`);
  }

  shouldShowPwdUpBtn(account: RcAccount) {
    return (
      this.buttonDisplayExpr(account, 'update_password') &&
      (account.type !== 'organizations.TPAccount' ||
        (this.isCmb && account.sub_type !== 'ldap'))
    );
  }

  shouldShowDelBtn(account: RcAccount) {
    return (
      this.account.username !== account.username &&
      this.buttonDisplayExpr(account, 'delete') &&
      (account.type !== 'organizations.TPAccount' ||
        (this.isCmb && account.sub_type !== 'ldap'))
    );
  }

  shouldShowPwdResetBtn(account: RcAccount) {
    return (
      this.account.username !== account.username &&
      this.buttonDisplayExpr(account, 'reset_password') &&
      (account.type !== 'organizations.TPAccount' ||
        (this.isCmb && account.sub_type !== 'ldap'))
    );
  }

  async ResetPassword(item: RcAccount) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('reset_password'),
        content: this.translate.get('reset_password_desc', {
          username: item.username,
        }),
        confirmText: this.translate.get('reset'),
        cancelText: this.translate.get('cancel'),
      });
      await this.accountService.resetAccountPassword(item);
      this.notificationService.success(
        this.translate.get('reset_password_success'),
      );
    } catch (e) {
      this.errorHandle.error(e);
    }
  }
}
