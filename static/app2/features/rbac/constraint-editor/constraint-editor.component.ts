import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
} from '@angular/core';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-constraint-editor',
  templateUrl: './constraint-editor.component.html',
  styleUrls: ['./constraint-editor.component.scss'],
})
export class ConstraintEditorComponent implements OnInit, OnChanges {
  @Input()
  type: string;
  @Input()
  options: any;
  @Input()
  constraints: Array<any>;
  @Output()
  onChange = new EventEmitter<any>();
  newOptions: any[] = [];

  constructor(private translateService: TranslateService) {}

  // tslint:disable-next-line:member-ordering
  key: any[] = [];
  // tslint:disable-next-line:member-ordering
  val: any[] = [];

  ngOnInit() {
    this.newOptions = this.options.map((item: string) => ({
      name: this.translateService.get(item.split(':')[1]),
      value: item,
    }));
  }

  ngOnChanges() {
    if (!this.constraints || !this.constraints.length) {
      this.constraints = [{}];
    }
  }

  add(key: string, val: string, index: number) {
    if (!key || !val) {
      return;
    }
    const constraint = {};
    constraint[key] = val;
    this.constraints[index] = Object.assign(
      {},
      this.constraints[index],
      constraint,
    );
    this.onChange.next(this.filteredConstraints);
    this.val[index] = '';
    this.key[index] = '';
  }

  addGroup() {
    this.constraints.push({});
  }

  deleteItem(key: string, index: number) {
    delete this.constraints[index][key];
    this.constraints = this.constraints.filter(
      item => !!Object.keys(item).length,
    );
    this.onChange.next(this.filteredConstraints);
  }

  get filteredConstraints() {
    return this.constraints.filter(item => !!Object.keys(item).length);
  }

  keys(object: object) {
    return Object.keys(object);
  }
}
