import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { MessageService, NotificationService } from 'alauda-ui';
import { ACCOUNT, WEBLABS } from 'app2/core/tokens';
import { RcAccount, Weblabs } from 'app2/core/types';
import { OrgService } from 'app2/shared/services/features/org.service';
import { DefaultErrorMapperService } from 'app2/shared/services/form/default-error-mapper.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-app-update-company',
  templateUrl: './update-company.component.html',
  styleUrls: ['./update-company.component.scss'],
})
export class UpdateCompanyComponent implements OnInit {
  formerCompanyName: string;
  orgName: string;
  submitting: boolean;
  errorMapper: any;
  companyName: string;

  @ViewChild('updateCompanyForm')
  updateCompanyForm: NgForm;
  @Output()
  afterConfirmed: EventEmitter<any> = new EventEmitter();

  constructor(
    private modalService: ModalService,
    private orgService: OrgService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private defaultErrorMapperService: DefaultErrorMapperService,
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(MODAL_DATA) private modalData: any,
  ) {}

  ngOnInit() {
    this.formerCompanyName = this.modalData.formerCompanyName;
    this.orgName = this.modalData.orgName;
    this.errorMapper = {
      map: (key: string, error: any) => {
        if (key === 'pattern') {
          return this.translate.get('company_name_invalid');
        } else {
          return this.defaultErrorMapperService.map(key, error);
        }
      },
    };
  }

  confirm() {
    this.updateCompanyForm.onSubmit(null);
    if (this.updateCompanyForm.invalid) {
      return;
    }
    this._updateOrg();
  }

  cancel() {
    this.modalService.closeAll();
  }

  _updateOrg() {
    this.submitting = true;
    this.orgService
      .updateOrg(this.orgName, this.companyName)
      .then(data => {
        this.auiMessageService.success({
          content: this.translate.get('update_company_success'),
        });
        this.afterConfirmed.emit(data);
        this.modalService.closeAll();
      })
      .catch(e => {
        if (e.errors) {
          this.auiNotificationService.error({
            content: this.translate.get(e.errors[0].code),
          });
        }
      })
      .then(() => {
        this.submitting = false;
      });
  }
}
