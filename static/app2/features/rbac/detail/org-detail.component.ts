import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'alauda-ui';
import { ACCOUNT, WEBLABS } from 'app2/core/tokens';
import { RcAccount, Weblabs } from 'app2/core/types';
import { UpdateCompanyComponent } from 'app2/features/rbac/detail/dialog/update-company/update-company.component';
import { AccountService } from 'app2/shared/services/features/account.service';
import { OrgService } from 'app2/shared/services/features/org.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-app-org-detail',
  templateUrl: './org-detail.component.html',
  styleUrls: ['./org-detail.component.scss'],
})
export class OrgDetailComponent implements OnInit {
  orgName: string;
  accountInfo: string;
  editingOrgCompany: boolean;
  editingOrgLogo: boolean;
  isSubAccount: boolean;
  initialized: boolean;
  org: any;
  orgCompany: string;
  avatarPath: string;
  avatarSubmitting: boolean;
  avatarPreviewing: boolean;

  constructor(
    private router: Router,
    private orgService: OrgService,
    private accountService: AccountService,
    private auiMessageService: MessageService,
    private translate: TranslateService,
    private modalService: ModalService,
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(WEBLABS) public weblabs: Weblabs,
  ) {}

  async ngOnInit() {
    this.orgName = this.account.namespace;
    const userName = this.account.username;
    this.accountInfo = userName ? `${userName}@${this.orgName}` : this.orgName;

    this.editingOrgCompany = false;
    this.editingOrgLogo = false;
    this.isSubAccount = this.accountService.isSubAccount();
    this.initialized = false;
    await this.getOrgDetail();
    this.initialized = true;
    this.avatarPath = this.org.logo_file;
  }

  updateCompany() {
    const modalRef = this.modalService.open(UpdateCompanyComponent, {
      title: this.translate.get('update_company_name'),
      data: {
        formerCompanyName: this.org.company,
        orgName: this.org.name,
      },
    });
    modalRef.componentInstance.afterConfirmed.subscribe((data: any) => {
      this.org = data;
    });
  }

  async getOrgDetail() {
    try {
      const org = await this.orgService.getOrg();
      this.org = org;
      this.orgCompany = org.company;
    } catch (e) {}
  }

  gotoUserDetail() {
    this.router.navigateByUrl('/user/detail');
  }

  previewAvatar(avatarFile: HTMLInputElement) {
    const file = avatarFile.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.avatarPath = reader.result as string;
      this.avatarPreviewing = true;
    };
  }

  cancelPreview(avatarFile: HTMLInputElement) {
    this.avatarPath = this.org.logo_file;
    avatarFile.value = '';
    this.avatarPreviewing = false;
  }

  async updateAvatar(avatarFile: HTMLInputElement) {
    this.avatarSubmitting = true;
    try {
      await this.accountService.updateNamespaceLogo(
        this.account.namespace,
        avatarFile.files[0],
      );
      this.auiMessageService.success(
        this.translate.get('org_logo_update_success'),
      );
      this.avatarPreviewing = false;
    } catch (e) {
    } finally {
      this.avatarSubmitting = false;
    }
  }
}
