import { Component, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NotificationService } from 'alauda-ui';
import { ACCOUNT, WEBLABS } from 'app2/core/tokens';
import { RcAccount, Weblabs } from 'app2/core/types';
import { ORG_CONSTANTS } from 'app2/features/rbac/rbac-constant';
import { OrgService } from 'app2/shared/services/features/org.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import marked from 'marked';

import { DESCRIPTION_CN, DESCRIPTION_ZH_CN } from './help-text.constant';

@Component({
  selector: 'rc-app-org-ldap',
  templateUrl: './org-ldap.component.html',
  styleUrls: ['./org-ldap.component.scss'],
})
export class OrgLdapComponent implements OnInit {
  HELP_TEXTS: string;
  AzureCountrys: Array<{
    name: string;
    value: 'CN' | 'US';
  }>;
  submitting: boolean;
  ldapConfigLoading: boolean;
  orgLdapPageMap: any;
  ldapConfig: any;
  ldapMode: string;

  @Input()
  orgName: string;
  @ViewChild('ldapConfigForm')
  ldapConfigForm: NgForm;

  constructor(
    private translate: TranslateService,
    private orgService: OrgService,
    private modalService: ModalService,
    private auiNotificationService: NotificationService,
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(WEBLABS) public weblabs: Weblabs,
  ) {}

  async ngOnInit() {
    this.HELP_TEXTS =
      this.translate.currentLang === 'zh_cn'
        ? DESCRIPTION_CN
        : DESCRIPTION_ZH_CN;
    this.AzureCountrys = [
      {
        name: this.translate.get('org_ldap_account_country_cn'),
        value: 'CN',
      },
      {
        name: this.translate.get('org_ldap_account_country_us'),
        value: 'US',
      },
    ];
    this.submitting = false;
    this.ldapConfigLoading = true;
    this.orgLdapPageMap = ORG_CONSTANTS.ORG_LDAP_PAGE_MAP;
    try {
      this.ldapConfig = Object.assign(
        {
          test: {},
          schema: {},
          config: {},
        },
        await this.orgService.getOrgLdap(),
      );
    } catch (e) {
      this.ldapConfig = {};
    }
    this.ldapConfigLoading = false;
    this.ldapMode =
      this.ldapConfig && !this.ldapConfig.empty
        ? ORG_CONSTANTS.ORG_LDAP_PAGE_MAP.DISPLAY
        : ORG_CONSTANTS.ORG_LDAP_PAGE_MAP.CREATE;
    if (this.ldapMode === ORG_CONSTANTS.ORG_LDAP_PAGE_MAP.CREATE) {
      this.selectLdapType('OpenLDAP');
    }
  }

  selectLdapType(type = 'OpenLDAP') {
    if (!this.ldapConfig || this.ldapConfig.type !== type) {
      this.ldapConfig = ORG_CONSTANTS.ORG_LDAP_TYPE_MAP[type];
    }
  }

  editLdapConfig() {
    this.ldapMode = ORG_CONSTANTS.ORG_LDAP_PAGE_MAP.UPDATE;
  }

  getHelpText() {
    return this.HELP_TEXTS;
  }

  getTaskHelpTextPreview() {
    const helpText = this.getHelpText();
    return helpText.replace(/\s+/g, ' ');
  }

  helpLinkClicked() {
    this.modalService.alert({
      title: this.translate.get('ldap_filter_condition_demo'),
      confirmText: this.translate.get('ok'),
      content: marked(this.getHelpText()),
    });
  }

  save(ldapConfigForm: any) {
    ldapConfigForm.onSubmit();
    if (ldapConfigForm.invalid) {
      return;
    }
    this.submitting = true;
    if (this.ldapMode === ORG_CONSTANTS.ORG_LDAP_PAGE_MAP.CREATE) {
      this.orgService
        .createOrgLdap({ config: this.ldapConfig, orgName: this.orgName })
        .then(async () => {
          await this.ngOnInit();
        })
        .catch(e => {
          if (e.errors) {
            this.auiNotificationService.error({
              content: this.translate.get(e.errors[0].code),
            });
          }
        })
        .then(() => {
          this.submitting = false;
        });
    } else if (this.ldapMode === ORG_CONSTANTS.ORG_LDAP_PAGE_MAP.UPDATE) {
      this.orgService
        .updateOrgLdap({ config: this.ldapConfig, orgName: this.orgName })
        .then(async () => {
          await this.ngOnInit();
        })
        .catch(e => {
          if (e.errors) {
            this.auiNotificationService.error({
              content: this.translate.get(e.errors[0].code),
            });
          }
        })
        .then(() => {
          this.submitting = false;
        });
    }
  }

  cancel() {
    this.ldapMode = ORG_CONSTANTS.ORG_LDAP_PAGE_MAP.DISPLAY;
  }
}
