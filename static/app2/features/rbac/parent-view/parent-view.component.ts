import { Component, Inject, OnInit } from '@angular/core';
import { RBACService } from 'app2/shared/services/features/rbac.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';

@Component({
  selector: 'rc-parent-view',
  templateUrl: './parent-view.component.html',
  styleUrls: ['./parent-view.component.scss'],
})
export class ParentViewComponent implements OnInit {
  role: any = {
    permissions: [],
  };
  initialized = false;

  constructor(
    private rbacService: RBACService,
    private modalService: ModalService,
    @Inject(MODAL_DATA) private modalData: { role_name: string },
  ) {}

  ngOnInit() {
    this.rbacService
      .getRole(this.modalData.role_name)
      .then(res => {
        this.role = res;
        this.initialized = true;
      })
      .catch(() => {});
  }

  cancel() {
    this.modalService.closeAll();
  }

  keys(obj: object) {
    return Object.keys(obj);
  }
}
