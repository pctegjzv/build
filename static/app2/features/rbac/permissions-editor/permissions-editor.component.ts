import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { MultiSelectionDropdownComponent } from 'app2/shared/components/select/multi-selection-dropdown.component';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';
import * as _ from 'lodash';

@Component({
  selector: 'rc-permissions-editor',
  templateUrl: './permissions-editor.component.html',
  styleUrls: ['./permissions-editor.component.scss'],
})
export class PermissionsEditorComponent implements OnInit {
  @Output()
  finished = new EventEmitter<any>();
  @ViewChild('form')
  form: any;
  @ViewChildren('actions')
  actionsInputs: QueryList<MultiSelectionDropdownComponent>;
  action = {};
  newMap = _.cloneDeep(this.modalData.permissionsMap);

  resource: any[];

  constructor(
    @Inject(MODAL_DATA)
    private modalData: {
      type: string;
      permissionsMap: any;
      schemaMap: any;
      actionsMap: any;
    },
    private modalService: ModalService,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    delete this.modalData.schemaMap['*'];
    this.resource = Object.keys(this.modalData.schemaMap).map(key => ({
      name: this.translateService.get(key),
      key,
    }));
    this.resource.forEach(({ key }) => {
      this.action[key] = [];
      if (this.newMap[key]) {
        this.newMap[key].forEach((permission: any) => {
          this.action[key].push(
            permission.actions.map((act: string) => act.split(':')[1]),
          );
        });
      }
    });
  }

  constraintChange($event: any, permission: any) {
    permission.constraints = $event;
  }

  actionsChange($event: any, permission: any) {
    permission.actions = $event.map(
      (act: string) => permission.resource_type + ':' + act,
    );
  }

  resourceChange($event: any) {
    if (
      !this.newMap[this.modalData.type] ||
      !this.newMap[this.modalData.type].length
    ) {
      this.newMap[this.modalData.type] = [
        {
          resource_type: this.modalData.type,
          constraints: [],
          actions: [],
          resource: [],
        },
      ];
    }
    this.modalData.type = $event.key;
    setTimeout(() => {
      this.actionsInputs.forEach((input, i) => {
        const actions = this.newMap[this.modalData.type][i].actions;
        if (actions.length && actions[0].match(/\*/)) {
          input.setSelectedValue(
            this.modalData.actionsMap[this.modalData.type].map(
              (item: any) => item.value,
            ),
          );
        } else if (actions.length) {
          input.setSelectedValue(
            this.newMap[this.modalData.type][i].actions.map(
              (act: string) => act.split(':')[1],
            ),
          );
        }
      });
    }, 100);
  }

  deletePermission(i: number) {
    this.newMap[this.modalData.type].splice(i, 1);
  }

  addPermission() {
    this.action[this.modalData.type].push([]);
    this.newMap[this.modalData.type].push({
      resource_type: this.modalData.type,
      constraints: [],
      actions: [],
      resource: ['*'],
    });
  }

  save() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    Object.keys(this.newMap).forEach(type => {
      this.newMap[type] = this.newMap[type].filter(
        (permission: any) => permission.actions.length,
      );
      if (!this.newMap[type].length) {
        delete this.newMap[type];
      }
      if (this.newMap[type]) {
        this.newMap[type].forEach((permission: any) => {
          if (!permission.resource.length) {
            permission.resource = ['*'];
          }
        });
      }
    });
    this.finished.next(this.newMap);
  }

  async cancel() {
    if (this.form.dirty) {
      try {
        await this.modalService.confirm({
          title: this.translateService.get('cancel'),
          content: this.translateService.get('rbac_give_up'),
        });
        this.modalService.closeAll();
      } catch (rejection) {}
    } else {
      this.modalService.closeAll();
    }
  }

  keys(obj: object) {
    return Object.keys(obj);
  }
}
