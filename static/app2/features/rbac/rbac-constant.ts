// 一级导航
export const PERMISSIONS_CATALOG = [
  { type: 'container' },
  { type: 'image' },
  { type: 'build' },
  { type: 'pipeline' },
  { type: 'cluster' },
  { type: 'storage' },
  { type: 'network' },
  { type: 'appplatform' },
  { type: 'appcatalog' },
  { type: 'servicecatalog' },
  { type: 'integration' },
  { type: 'jenkins' },
  { type: 'resource' },
  { type: 'monitoring' },
  { type: 'event' },
  { type: 'notification' },
  { type: 'alarm' },
  { type: 'project' },
  { type: 'account' },
  { type: 'others' },
];
// 二级，三级导航
export const PERMISSIONS_CATALOG_MAP = {
  container: {
    application: ['application', 'application_template'],
    service: ['service'],
    job_config: ['job_config'],
    job_history: ['job_history'],
    env_file: ['env_file'],
    configuration_file: ['configuration_file'],
    configmap: ['configmap'],
  },
  image: {
    repository: ['repository', 'registry', 'registry_project'],
    sync_config: ['sync_config'],
    sync_history: ['sync_history'],
  },
  build: { build_config: ['build_config'], build_history: ['build_history'] },
  pipeline: {
    pipeline_config: ['pipeline_config', 'pipeline_task'],
    pipeline_history: ['pipeline_history'],
  },
  cluster: {
    cluster: [
      'cluster',
      'cluster_node',
      'namespace',
      'k8s_resourcequotas',
      'k8s_others',
    ],
  },
  storage: {
    volume: ['volume'],
    snapshot: ['snapshot'],
    persistentvolumeclaim: ['persistentvolumeclaim'],
    persistentvolume: ['persistentvolume'],
    k8s_storageclasses: ['k8s_storageclasses'],
  },
  network: {
    domain: ['domain'],
    load_balancer: ['load_balancer'],
    ip: ['ip'],
    subnet: ['subnet', 'private_ip'],
    certificate: ['certificate'],
    k8s_networkpolicies: ['k8s_networkpolicies'],
  },
  appplatform: {
    public_helm_template: ['public_helm_template'],
  },
  appcatalog: {
    helm_template_repo: ['helm_template_repo'],
    helm_template: ['helm_template'],
  },
  servicecatalog: {
    clusterservicebroker: ['clusterservicebroker'],
    serviceinstance: ['serviceinstance'],
  },
  integration: {
    integration: ['integration'],
  },
  jenkins: {
    jenkins: [
      'jenkins_credential',
      'jenkins_pipeline',
      'jenkins_pipeline_history',
      'jenkins_pipeline_template',
      'jenkins_pipeline_template_repository',
    ],
  },
  resource: { space: ['space'] },
  monitoring: { monitoring: ['dashboard', 'dashboard_panel'] },
  event: { event: ['event'] },
  notification: { notification: ['notification'] },
  alarm: {
    alarm: ['alarm'],
    log_alarm: ['log_alarm'],
    log_filter: ['log_filter'],
  },
  project: { project: ['project', 'project_template'] },
  account: {
    account: ['role', 'subaccount', 'organization', 'role_template'],
    rbac_view: ['operation_view'],
  },
  others: { '*': ['*'] },
};

export const TRANSLATE_MAP = {
  '*': 'rbac_all',
  update: 'update',
  create: 'create',
  delete: 'delete',
  view: 'view',
};

const checkedMap = {};

Object.keys(PERMISSIONS_CATALOG_MAP).forEach(first => {
  Object.keys(PERMISSIONS_CATALOG_MAP[first]).forEach(sec => {
    PERMISSIONS_CATALOG_MAP[first][sec].forEach((type: string) => {
      checkedMap[type] = false;
    });
  });
});

export const CHECKED_MAP = checkedMap;

export const contentTip = {
  zh_cn: `
  资源类型：平台上的各种功能；如：应用、集群、权限管理等<br/>
  操作：可对资源类型的操作；如：创建、更新、触发等<br/>
  约束条件：对资源类型所关联资源的名称进行约束，可使用通配符“*”<br/>
  资源名称：根据资源类型所创建的实例名称，可使用通配符“*”<br/><br/>

  例如：若一个角色拥有如下权限：<br/>
  资源类型：应用<br/>
  操作：查看、更新<br/>
  约束条件：[集群：devcluster] <br/>
  资源名称：dev<br/>
  则该角色拥有如下权限：在名为devcluster的集群上，可查看和更新名为dev的应用`,
  en: `
  Resource type: E.g. application, cluster, permission management, etc.<br/>
  Action: Actions enabled of resource type; e.g. create, update, trigger, etc.<br/>
  Constraints: Constraints of resource type, limited by name, '*' is enabled<br/>
  Resource name: Instance name according to resource type, '*' is enabled<br/><br/>

  E.g. One role like this: <br/>
  Resource type: Application<br/>
  Action: view, update<br/>
  Constraints: [cluster: devcluster] <br/>
  Resource name: dev<br/><br/>
  then this role has permissions: cluster named devcluster，view and update is enabled to application dev`,
};

// 页面状态：已配置，创建，更新
const ORG_LDAP_PAGE_MAP = {
  DISPLAY: 'initial',
  CREATE: 'create',
  UPDATE: 'update',
};

const ORG_LDAP_TYPE_MAP = {
  OpenLDAP: {
    type: 'OpenLDAP',
    config: {
      host: '',
      port: '',
      tls: false,
      service_account: '',
      service_password: '',
      search_base: 'dc=example,dc=org',
      server_cert: '',
      client_key: '',
      domain: '',
    },
    schema: {
      users: {
        class_type: 'inetOrgPerson',
        login_field: 'uid',
        search: '',
      },
    },
    test: {
      username: '',
      password: '',
    },
  },
  ActiveDirectory: {
    type: 'ActiveDirectory',
    config: {
      host: '',
      port: '',
      tls: false,
      service_account: '',
      service_password: '',
      search_base: 'dc=example,dc=org',
      domain: '',
    },
    schema: {
      users: {
        class_type: 'person',
        login_field: 'sAMAccountName',
        search: '',
      },
    },
    test: {
      username: '',
      password: '',
    },
  },
  AzureAD: {
    type: 'AzureAD',
    config: {
      service_account: '',
      service_password: '',
      tenant_id: '',
      country: '',
      domain: '',
    },
    schema: {},
    test: {
      username: '',
      password: '',
    },
  },
};

const ORG_LDAP_STATUS_MAP = {
  bare: 'bare',
  synced: 'synced',
  syncing: 'syncing',
  failure: 'failure',
};

//const ORG_LDAP_

export const ORG_CONSTANTS = {
  ORG_LDAP_PAGE_MAP,
  ORG_LDAP_TYPE_MAP,
  ORG_LDAP_STATUS_MAP,
};
