import { NgModule } from '@angular/core';
import { RbacUserSharedModule } from 'app2/features/lazy/rbac-user.shared.module';
import { AccountCreateComponent } from 'app2/features/rbac/account/dialog/account-create/account-create.component';
import { CleanLdapComponent } from 'app2/features/rbac/account/dialog/clean-ldap//clean-ldap.component';
import { RolesAddComponent } from 'app2/features/rbac/account/dialog/roles-add/account-roles-add-dialog.component';
import { UaaAccountCreateComponent } from 'app2/features/rbac/account/dialog/uaa-account-create/uaa-account-create.component';
import { AccountListComponent } from 'app2/features/rbac/account/list/account-list.component';
import { ConstraintEditorComponent } from 'app2/features/rbac/constraint-editor/constraint-editor.component';
import { OrgLdapComponent } from 'app2/features/rbac/detail/org-ldap/org-ldap.component';
import { ParentViewComponent } from 'app2/features/rbac/parent-view/parent-view.component';
import { PermissionsEditorComponent } from 'app2/features/rbac/permissions-editor/permissions-editor.component';
import { RoleCreateComponent } from 'app2/features/rbac/roles/create/role-create.component';
import { RoleImportComponent } from 'app2/features/rbac/roles/create/role-template-import.component';
import { AddParentComponent } from 'app2/features/rbac/roles/detail/add-parent/add-parent.component';
import { AddUserComponent } from 'app2/features/rbac/roles/detail/add-user/add-user.component';
import { RoleDetailComponent } from 'app2/features/rbac/roles/detail/role-detail.component';
import { UserAddComponent } from 'app2/features/rbac/roles/dialog/user-add//user-add.component';
import { RoleListComponent } from 'app2/features/rbac/roles/list/role-list.component';

import { SharedModule } from '../../shared/shared.module';
import { OrgDetailComponent } from '../rbac/detail/org-detail.component';

import { RBACRoutingModule } from './rbac.routing.module';

@NgModule({
  imports: [SharedModule, RbacUserSharedModule, RBACRoutingModule],
  declarations: [
    ConstraintEditorComponent,
    RoleCreateComponent,
    RoleDetailComponent,
    RoleImportComponent,
    ParentViewComponent,
    OrgDetailComponent,
    AccountListComponent,
    OrgLdapComponent,
    RolesAddComponent,
    AccountCreateComponent,
    UserAddComponent,
    CleanLdapComponent,
    PermissionsEditorComponent,
    AddUserComponent,
    AddParentComponent,
    RoleListComponent,
    UaaAccountCreateComponent,
  ],
  entryComponents: [
    RoleImportComponent,
    ParentViewComponent,
    PermissionsEditorComponent,
    OrgDetailComponent,
    AccountListComponent,
    OrgLdapComponent,
    RolesAddComponent,
    AccountCreateComponent,
    UserAddComponent,
    CleanLdapComponent,
    AddUserComponent,
    AddParentComponent,
    UaaAccountCreateComponent,
  ],
})
export class RBACModule {}
