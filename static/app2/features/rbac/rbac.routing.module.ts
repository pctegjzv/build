import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoleCreateComponent } from 'app2/features/rbac/roles/create/role-create.component';
import { RoleDetailComponent } from 'app2/features/rbac/roles/detail/role-detail.component';

import { OrgDetailComponent } from '../rbac/detail/org-detail.component';

const routes: Routes = [
  {
    path: 'roles',
    children: [
      {
        path: 'create',
        component: RoleCreateComponent,
      },
      {
        path: 'detail',
        component: RoleDetailComponent,
      },
    ],
  },
  {
    path: '',
    component: OrgDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class RBACRoutingModule {}
