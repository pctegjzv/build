import { NgModule } from '@angular/core';
import { RoleTemplateRoutingModule } from 'app2/features/rbac/role-template.routing.module';
import { RoleTemplateCreateComponent } from 'app2/features/rbac/role-template/create/role-template-create.component';
import { RoleTemplateDetailComponent } from 'app2/features/rbac/role-template/detail/role-template-detail.component';
import { RoleTemplateListComponent } from 'app2/features/rbac/role-template/list/role-template-list.component';
import { RoleTemplateUpdateComponent } from 'app2/features/rbac/role-template/update/role-template-update.component';

import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [SharedModule, RoleTemplateRoutingModule],
  declarations: [
    RoleTemplateListComponent,
    RoleTemplateDetailComponent,
    RoleTemplateCreateComponent,
    RoleTemplateUpdateComponent,
  ],
})
export class RoleTemplateModule {}
