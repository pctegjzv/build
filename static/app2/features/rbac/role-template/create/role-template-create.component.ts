import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import {
  CHECKED_MAP,
  PERMISSIONS_CATALOG,
  PERMISSIONS_CATALOG_MAP,
  TRANSLATE_MAP,
  contentTip,
} from 'app2/features/rbac/rbac-constant';
import {
  Permission,
  RBACService,
  RoleTemplateItem,
} from 'app2/shared/services/features/rbac.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import * as _ from 'lodash';

@Component({
  selector: 'rc-role-template-create',
  templateUrl: './role-template-create.component.html',
  styleUrls: ['./role-template-create.component.scss'],
})
export class RoleTemplateCreateComponent implements OnInit {
  @ViewChild('permissionsTable')
  table: any;
  @ViewChild('form')
  form: any;

  contentTip = contentTip;

  translateMap = TRANSLATE_MAP;
  permissionsCatalog = [...PERMISSIONS_CATALOG];
  permissionsCatalogMap = PERMISSIONS_CATALOG_MAP;
  checkedMap = _.cloneDeep(CHECKED_MAP);
  permissionsMap: object = {};
  actionsMap = {};
  schemaMap = {};
  multiplyOptions: any[] = [];
  defaultVal: any[] = [];

  data: RoleTemplateItem = {
    name: '',
    display_name: '',
    description: '',
    scope: {},
    permissions: [{}],
    created_by: '',
  };

  constructor(
    private rbacService: RBACService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: ModalService,
    @Inject(ACCOUNT) public account: RcAccount,
  ) {}

  async ngOnInit() {
    this.permissionsCatalog.splice(PERMISSIONS_CATALOG.length - 1, 1);

    await this.rbacService
      .getTemplateScopes()
      .then(res => {
        this.multiplyOptions = res.map((item: any) => ({
          name: this.translateService.get(item.resource_type),
          value: item.resource_type,
        }));
      })
      .catch(() => {});

    this.rbacService
      .getRoleSchema()
      .then(res => {
        res.forEach((item: any) => {
          this.schemaMap[item.resource] = item.constraints;
          this.actionsMap[item.resource] = item.actions.map((act: string) => ({
            name: this.translateService.get(act.split(':')[1]),
            value: act.split(':')[1],
          }));
        });
      })
      .catch(() => {});

    this.route.queryParams.subscribe(({ uuid }) => {
      if (!uuid) {
        return;
      }
      this.rbacService
        .getTemplateDetail(uuid)
        .then((res: any) => {
          res['created_by'] = '';
          delete res.uuid;
          this.data = res;
          if (res.scope && Object.keys(res.scope).length) {
            this.defaultVal = Object.keys(res.scope);
          }
          res.permissions.forEach((item: any) => {
            this.checkedMap[item.resource_type] = true;
            this.permissionsMap[item.resource_type] = item;
          });
        })
        .catch(() => {});
    });
  }

  toggleExpandRow(row: any, rowIndex: number) {
    this.table.toggleExpandRow(row, rowIndex);
  }

  keys(object: object) {
    return Object.keys(object);
  }

  onCheck(checked: boolean, type: string) {
    if (checked) {
      this.permissionsMap[type] = {
        resource_type: type,
        resource: ['*'],
        actions: [type + ':*'],
        constraints: [],
      };
    } else {
      delete this.permissionsMap[type];
    }
  }

  translateTypes(type: string) {
    return `(${Object.keys(this.permissionsCatalogMap[type])
      .map(key => this.translateService.get(key))
      .join(', ')})`;
  }

  onScopesChange($event: any) {
    const scope = {};
    $event.forEach((key: string) => {
      scope[key] = true;
    });
    this.data.scope = scope;
  }

  onActionsChange($event: { name: string; value: string }[], type: string) {
    if (!$event.length) {
      this.permissionsMap[type].actions = [type + ':*'];
    } else {
      this.permissionsMap[type].actions = $event.map(
        (act: { name: string; value: string }) => type + ':' + act.value,
      );
    }
  }

  onResourceChange($event: any, type: string) {
    if ($event.length > 0) {
      this.permissionsMap[type].resource = $event.filter(
        (item: string) => item !== '*',
      );
    } else {
      this.permissionsMap[type].resource = ['*'];
    }
  }

  constraintChange($event: any, type: string) {
    this.permissionsMap[type].constraints = [...$event];
  }

  submit() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    if (!Object.values(this.checkedMap).some(checked => !!checked)) {
      this.auiNotificationService.error(
        this.translateService.get('rbac_template_permission_required'),
      );
      return;
    }
    const permissions: Permission[] = [];
    Object.keys(this.permissionsMap).forEach(type => {
      permissions.push(this.permissionsMap[type]);
    });
    this.data.permissions = permissions;
    this.data.created_by = this.account.username;
    delete this.data.created_at;
    this.rbacService
      .createTemplate(this.data)
      .then(() => {
        this.auiNotificationService.success(
          this.translateService.get('create_success'),
        );
        this.router.navigateByUrl('role-template');
      })
      .catch(({ errors }) =>
        this.auiNotificationService.error(
          this.translateService.get(errors[0].code),
        ),
      );
  }

  async cancel() {
    try {
      await this.modalService.confirm({
        title: this.translateService.get('cancel'),
        content: this.translateService.get('rbac_give_up'),
      });
      this.router.navigateByUrl('role-template');
    } catch (rejection) {}
  }

  get lang() {
    return this.translateService.currentLang;
  }
}
