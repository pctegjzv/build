import { Component, OnInit, ViewChild } from '@angular/core';
import { NotificationService } from 'alauda-ui';
import { ModalService } from 'app2/shared/services/modal/modal.service';

import { ActivatedRoute, Router } from '@angular/router';
import {
  PERMISSIONS_CATALOG,
  PERMISSIONS_CATALOG_MAP,
  TRANSLATE_MAP,
  contentTip,
} from 'app2/features/rbac/rbac-constant';
import {
  RBACService,
  RoleTemplateItem,
} from 'app2/shared/services/features/rbac.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { TranslateService } from 'app2/translate/translate.service';
import * as _ from 'lodash';

@Component({
  selector: 'rc-role-template-detail',
  templateUrl: './role-template-detail.component.html',
  styleUrls: ['./role-template-detail.component.scss'],
})
export class RoleTemplateDetailComponent implements OnInit {
  @ViewChild('permissionsTable')
  table: any;
  createEnabled = false;
  createRoleEnabled = false;
  updateEnabled = false;
  deleteEnabled = false;
  loading = true;
  expanded: any = {};
  translateMap = TRANSLATE_MAP;
  permissionsCatalog = [...PERMISSIONS_CATALOG];
  permissionsCatalogMap = PERMISSIONS_CATALOG_MAP;
  scope: string;
  actionsMap = {};
  permissionsMap = {};
  _ = _;

  contentTip = contentTip;

  data: RoleTemplateItem;

  constructor(
    private translateService: TranslateService,
    private route: ActivatedRoute,
    private rbacService: RBACService,
    private auiNotificationService: NotificationService,
    private modal: ModalService,
    private router: Router,
    private roleUtil: RoleUtilitiesService,
  ) {}

  async ngOnInit() {
    await this.rbacService
      .getRoleSchema()
      .then(res => {
        res.forEach((item: any) => {
          this.actionsMap[item.resource] = item.actions.map((act: string) => ({
            name: this.translateService.get(act.split(':')[1]),
            value: act.split(':')[1],
          }));
        });
      })
      .catch(() => {});
    this.data = await this.refetch();
    this.getScope();
    this.permissionsMap = await this.rbacService.sortPermissions(
      this.data.permissions,
    );
    this.permissionsCatalog = this.catalogFilter(
      this.permissionsCatalog,
      this.permissionsMap,
      this.permissionsCatalogMap,
    );
    this.roleUtil
      .resourceTypeSupportPermissions('role_template')
      .then(res => (this.createEnabled = res));
    this.roleUtil
      .resourceTypeSupportPermissions('role_template', {}, 'update')
      .then(res => (this.updateEnabled = res && !this.data.official));
    this.roleUtil
      .resourceTypeSupportPermissions('role_template', {}, 'delete')
      .then(res => (this.deleteEnabled = res && !this.data.official));
    this.roleUtil
      .resourceTypeSupportPermissions('role')
      .then(res => (this.createRoleEnabled = res));
    // 默认展开
    setTimeout(() => {
      for (let i = 0; i < this.permissionsCatalog.length; i++) {
        this.toggleExpandRow(this.permissionsCatalog[i], i);
      }
    }, 100);
  }

  async refetch() {
    let id: string;
    this.route.queryParams.subscribe(({ uuid }) => {
      id = uuid;
    });
    return this.rbacService.getTemplateDetail(id);
  }

  get lang() {
    return this.translateService.currentLang;
  }

  translateTypes(type: string) {
    return `(${Object.keys(this.permissionsCatalogMap[type])
      .map(key => this.translateService.get(key))
      .join(', ')})`;
  }

  getScope() {
    const scopeArr: any[] = [];
    for (const key in this.data.scope) {
      if (this.data.scope[key]) {
        const str = this.translateService.get(key);
        scopeArr.push(str);
      }
    }
    this.scope = scopeArr.join(', ');
  }

  private catalogFilter(
    permissionsCatalog: Array<any>,
    permissionsMap: object,
    permissionsCatalogMap: object,
  ) {
    return permissionsCatalog.filter(item => {
      let flag = false;
      for (const key in permissionsCatalogMap[item.type]) {
        if (!flag) {
          flag = permissionsCatalogMap[item.type][key].some(
            (type: string) => !!permissionsMap[type],
          );
        }
      }
      return flag;
    });
  }

  toggleExpandRow(row: any, rowIndex: number) {
    this.table.toggleExpandRow(row, rowIndex);
  }

  keys(obj: object) {
    return Object.keys(obj);
  }

  delete(uuid: string, template_name: string) {
    this.modal
      .confirm({
        title: this.translateService.get('delete'),
        content: this.translateService.get('rbac_delete_template_confirm', {
          template_name,
        }),
      })
      .then(() => {
        this.rbacService
          .deleteTemplate(uuid)
          .then(() => {
            this.auiNotificationService.success(
              this.translateService.get('delete_success'),
            );
          })
          .catch(() => {
            this.auiNotificationService.error(
              this.translateService.get('delete_failed'),
            );
          })
          .then(() => {
            this.router.navigateByUrl('role-template');
          });
      })
      .catch(() => {});
  }

  update(uuid: string) {
    this.router.navigateByUrl(`role-template/update?uuid=${uuid}`);
  }

  duplicate(uuid: string) {
    this.router.navigateByUrl(`role-template/create?uuid=${uuid}`);
  }

  createRole(uuid: string) {
    this.router.navigateByUrl(`rbac/roles/create?uuid=${uuid}`);
  }
}
