import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import {
  RBACService,
  RoleTemplateListItem,
} from 'app2/shared/services/features/rbac.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-role-template-list',
  templateUrl: './role-template-list.component.html',
  styleUrls: ['./role-template-list.component.scss'],
})
export class RoleTemplateListComponent implements OnInit {
  loading = true;
  loadError = false;
  createEnabled = false;
  createRoleEnabled = false;
  updateEnabled = false;
  deleteEnabled = false;
  list: RoleTemplateListItem[];
  constructor(
    private translateService: TranslateService,
    private router: Router,
    private rbacService: RBACService,
    private auiNotificationService: NotificationService,
    private modal: ModalService,
    private roleUtil: RoleUtilitiesService,
  ) {}

  ngOnInit() {
    this.refetch();
    // get permissions
    this.roleUtil
      .resourceTypeSupportPermissions('role_template')
      .then(res => (this.createEnabled = res));
    this.roleUtil
      .resourceTypeSupportPermissions('role_template', {}, 'update')
      .then(res => (this.updateEnabled = res));
    this.roleUtil
      .resourceTypeSupportPermissions('role_template', {}, 'delete')
      .then(res => (this.deleteEnabled = res));
    this.roleUtil
      .resourceTypeSupportPermissions('role')
      .then(res => (this.createRoleEnabled = res));
  }

  refetch() {
    this.rbacService
      .getTemplateList()
      .then(res => {
        this.list = res;
      })
      .catch(() => {
        this.loadError = true;
      })
      .then(() => {
        this.loading = false;
      });
  }

  showDetail(uuid: string) {
    this.router.navigateByUrl(`role-template/detail?uuid=${uuid}`);
  }

  create() {
    this.router.navigateByUrl('role-template/create');
  }

  update(uuid: string) {
    this.router.navigateByUrl(`role-template/update?uuid=${uuid}`);
  }

  delete(uuid: string, template_name: string) {
    this.modal
      .confirm({
        title: this.translateService.get('delete'),
        content: this.translateService.get('rbac_delete_template_confirm', {
          template_name,
        }),
      })
      .then(() => {
        this.rbacService
          .deleteTemplate(uuid)
          .then(() => {
            this.auiNotificationService.success(
              this.translateService.get('delete_success'),
            );
          })
          .catch(() => {
            this.auiNotificationService.error(
              this.translateService.get('delete_failed'),
            );
          })
          .then(() => {
            this.refetch();
          });
      })
      .catch(() => {});
  }

  duplicate(uuid: string) {
    this.router.navigateByUrl(`role-template/create?uuid=${uuid}`);
  }

  createRole(uuid: string) {
    this.router.navigateByUrl(`rbac/roles/create?uuid=${uuid}`);
  }

  // async buttonDisplayExpr(type: any = 'role_template', action: any = 'create') {
  //   let hasPermission = false;
  //   await this.roleUtil
  //     .resourceTypeSupportPermissions(type, {}, action)
  //     .then(res => (hasPermission = res));
  //   console.log(hasPermission);
  //   return hasPermission;
  // }

  get lang() {
    return this.translateService.currentLang;
  }
}
