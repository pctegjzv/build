import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import {
  CHECKED_MAP,
  PERMISSIONS_CATALOG,
  PERMISSIONS_CATALOG_MAP,
  TRANSLATE_MAP,
  contentTip,
} from 'app2/features/rbac/rbac-constant';
import {
  Permission,
  RBACService,
} from 'app2/shared/services/features/rbac.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import * as _ from 'lodash';
import { first } from 'rxjs/operators';

@Component({
  selector: 'rc-role-template-update',
  templateUrl: './role-template-update.component.html',
  styleUrls: ['./role-template-update.component.scss'],
})
export class RoleTemplateUpdateComponent implements OnInit {
  @ViewChild('permissionsTable')
  table: any;
  @ViewChild('templateScopes')
  scopes: any;
  @ViewChild('form')
  form: any;

  contentTip = contentTip;

  translateMap = TRANSLATE_MAP;
  permissionsCatalog = [...PERMISSIONS_CATALOG];
  permissionsCatalogMap = PERMISSIONS_CATALOG_MAP;
  checkedMap = _.cloneDeep(CHECKED_MAP);
  permissionsMap: object = {};
  actionsMap = {};
  schemaMap = {};
  multiplyOptions: any[] = [];

  data = {
    name: '',
    display_name: '',
    description: '',
    scope: {},
    permissions: [{}],
  };

  constructor(
    private rbacService: RBACService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private route: ActivatedRoute,
    private router: Router,
    private modalService: ModalService,
  ) {}

  async ngOnInit() {
    this.permissionsCatalog.splice(PERMISSIONS_CATALOG.length - 1, 1);
    await this.refetch();
    await this.rbacService
      .getTemplateScopes()
      .then(res => {
        this.multiplyOptions = res.map((item: any) => ({
          name: this.translateService.get(item.resource_type),
          value: item.resource_type,
        }));
        setTimeout(() => {
          this.scopes.setSelectedValue(Object.keys(this.data.scope));
        }, 0);
      })
      .catch(() => {});

    await this.rbacService
      .getRoleSchema()
      .then(res => {
        res.forEach((item: any) => {
          this.schemaMap[item.resource] = item.constraints;
          this.actionsMap[item.resource] = item.actions.map((act: string) => ({
            name: this.translateService.get(act.split(':')[1]),
            value: act.split(':')[1],
          }));
        });
      })
      .catch(() => {});
    // 展开
    setTimeout(() => {
      const types = Object.keys(this.permissionsMap);
      for (let i = 0; i < this.permissionsCatalog.length; i++) {
        const line = this.permissionsCatalog[i].type;
        const expand = Object.keys(this.permissionsCatalogMap[line]).some(
          key => {
            return !!_.intersection(
              this.permissionsCatalogMap[line][key],
              types,
            ).length;
          },
        );
        if (expand) {
          this.toggleExpandRow(this.permissionsCatalog[i], i);
        }
      }
    }, 100);
  }

  toggleExpandRow(row: any, rowIndex: number) {
    this.table.toggleExpandRow(row, rowIndex);
  }

  keys(object: object) {
    return Object.keys(object);
  }

  onCheck(checked: boolean, type: string) {
    if (checked) {
      this.permissionsMap[type] = {
        resource_type: type,
        resource: ['*'],
        actions: [type + ':*'],
        constraints: [],
      };
    } else {
      delete this.permissionsMap[type];
    }
  }

  onScopesChange($event: any) {
    const scope = {};
    $event.forEach((key: string) => {
      scope[key] = true;
    });
    this.data.scope = scope;
  }

  onActionsChange($event: { name: string; value: string }[], type: string) {
    if (!$event.length) {
      this.permissionsMap[type].actions = [type + ':*'];
    } else {
      this.permissionsMap[type].actions = $event.map(
        (act: { name: string; value: string }) => type + ':' + act.value,
      );
    }
  }

  onResourceChange($event: any, type: string) {
    if ($event.length > 0) {
      this.permissionsMap[type].resource = $event.filter(
        (item: string) => item !== '*',
      );
    } else {
      this.permissionsMap[type].resource = ['*'];
    }
  }

  constraintChange($event: any, type: string) {
    this.permissionsMap[type].constraints = [...$event];
  }

  async refetch() {
    const uuid = await this.route.queryParams
      .pipe(first())
      .toPromise()
      .then(({ uuid }) => uuid);
    return this.rbacService
      .getTemplateDetail(uuid)
      .then(res => {
        this.data = res;
        res.permissions.forEach(item => {
          this.checkedMap[item.resource_type] = true;
          this.permissionsMap[item.resource_type] = item;
        });
      })
      .catch(() => {
        this.auiNotificationService.error(
          this.translateService.get('loading_error'),
        );
        this.router.navigateByUrl('role-template');
      });
  }

  translateTypes(type: string) {
    return `(${Object.keys(this.permissionsCatalogMap[type])
      .map(key => this.translateService.get(key))
      .join(', ')})`;
  }

  submit() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    if (!Object.values(this.checkedMap).some(checked => !!checked)) {
      this.auiNotificationService.error(
        this.translateService.get('rbac_template_permission_required'),
      );
      return;
    }
    const permissions: Permission[] = [];
    Object.keys(this.permissionsMap).forEach(type => {
      permissions.push(this.permissionsMap[type]);
    });
    this.data.permissions = permissions;
    this.rbacService
      .updateTemplate(this.data)
      .then(() => {
        this.auiNotificationService.success(
          this.translateService.get('update_success'),
        );
        this.router.navigateByUrl('role-template');
      })
      .catch(({ errors }) =>
        this.auiNotificationService.error(
          this.translateService.get(errors[0].code),
        ),
      );
  }

  async cancel() {
    try {
      await this.modalService.confirm({
        title: this.translateService.get('cancel'),
        content: this.translateService.get('rbac_give_up'),
      });
      this.router.navigateByUrl('role-template');
    } catch (rejection) {}
  }

  get lang() {
    return this.translateService.currentLang;
  }
}
