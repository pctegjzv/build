import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { ParentViewComponent } from 'app2/features/rbac/parent-view/parent-view.component';
import { PermissionsEditorComponent } from 'app2/features/rbac/permissions-editor/permissions-editor.component';
import {
  CHECKED_MAP,
  PERMISSIONS_CATALOG,
  PERMISSIONS_CATALOG_MAP,
  TRANSLATE_MAP,
  contentTip,
} from 'app2/features/rbac/rbac-constant';
import {
  Project,
  ProjectService,
} from 'app2/shared/services/features/project.service';
import {
  Permission,
  RBACService,
} from 'app2/shared/services/features/rbac.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ModalMode } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';
import { getCookie } from 'app2/utils/cookie';
import * as _ from 'lodash';
import { first } from 'rxjs/operators';

import { RoleImportComponent } from './role-template-import.component';

interface Data {
  parents: any[];
  name: string;
  permissions: any[];
  template_uuid?: string;
}

@Component({
  selector: 'rc-role-create',
  templateUrl: './role-create.component.html',
  styleUrls: ['./role-create.component.scss'],
})
export class RoleCreateComponent implements OnInit {
  @ViewChild('permissionsTable')
  table: any;

  @ViewChild('form')
  form: NgForm;

  contentTip = contentTip;

  translateMap = TRANSLATE_MAP;
  permissionsCatalog = [...PERMISSIONS_CATALOG];
  permissionsCatalogMap = PERMISSIONS_CATALOG_MAP;
  checkedMap = _.cloneDeep(CHECKED_MAP);
  permissionsMap: object = {};
  actionsMap = {};
  schemaMap = {};
  parentOptions: any[] = [];
  initialized = false;

  data: Data = {
    name: '',
    parents: [{ name: '', uuid: '' }],
    permissions: [],
    template_uuid: '',
  };

  select_none: string = this.translateService.get('select_none');
  project: string = this.select_none;
  projectNames: string[];

  constructor(
    private rbacService: RBACService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private router: Router,
    private projectService: ProjectService,
    private modalService: ModalService,
    private route: ActivatedRoute,
  ) {}

  async ngOnInit() {
    this.permissionsCatalog.splice(PERMISSIONS_CATALOG.length - 1, 1);
    this.parentOptions = await this.rbacService
      .getRoles(0)
      .then(({ results }) => results);
    await this.rbacService
      .getRoleSchema()
      .then(res => {
        res.forEach((item: any) => {
          this.schemaMap[item.resource] = item.constraints;
          this.actionsMap[item.resource] = item.actions.map((act: string) => ({
            name: this.translateService.get(act.split(':')[1]),
            value: act.split(':')[1],
          }));
        });
      })
      .catch(() => {});
    this.projectNames = await this.projectService
      .getProjects()
      .then((projects: Project[]) => [
        this.select_none,
        ...projects.map(project => project.name),
      ]);
    this.initialized = true;

    this.route.queryParams.subscribe(({ uuid }) => {
      if (!uuid) {
        return;
      }
      this.import(uuid);
    });
  }

  onParentsChange($event: any, i: number) {
    this.data.parents[i].name = $event.name;
    this.data.parents[i].uuid = $event.uuid;
    this.data.parents = _.uniqBy(this.data.parents, 'uuid');
  }

  addParent() {
    this.data.parents.push({ name: '', uuid: '' });
  }

  deleteParent(i: number) {
    this.data.parents.splice(i, 1);
  }

  toggleExpandRow(row: any, rowIndex: number) {
    this.table.toggleExpandRow(row, rowIndex);
  }

  keys(object: object) {
    return Object.keys(object);
  }

  async onCheck(checked: boolean, type: string) {
    if (checked) {
      this.permissionsMap[type] = [
        {
          resource_type: type,
          resource: ['*'],
          actions: [type + ':*'],
          constraints: [],
        },
      ];
    } else {
      if (this.permissionsMap[type].length > 1) {
        try {
          await this.modalService.confirm({
            title: this.translateService.get('rbac_uncheck'),
            content: this.translateService.get('rbac_uncheck_confirm'),
          });
          delete this.permissionsMap[type];
        } catch (rejection) {
          this.checkedMap[type] = true;
        }
      } else {
        delete this.permissionsMap[type];
      }
    }
  }

  translateTypes(type: string) {
    return `(${Object.keys(this.permissionsCatalogMap[type])
      .map(key => this.translateService.get(key))
      .join(', ')})`;
  }

  onActionsChange($event: { name: string; value: string }[], permission: any) {
    if (!$event.length) {
      permission.actions = [permission.resource_type + ':*'];
    } else {
      permission.actions = $event.map(
        (act: { name: string; value: string }) =>
          permission.resource_type + ':' + act.value,
      );
    }
  }

  onResourceChange($event: any, permission: any) {
    if ($event.length > 0) {
      permission.resource = $event.filter((item: string) => item !== '*');
    } else {
      permission.resource = ['*'];
    }
  }

  constraintChange($event: any, permission: any) {
    permission.constraints = _.cloneDeep($event);
  }

  addPermission(type: string) {
    const modelRef = this.modalService.open(PermissionsEditorComponent, {
      title: this.translateService.get('add_permission'),
      data: {
        type,
        permissionsMap: this.permissionsMap,
        schemaMap: this.schemaMap,
        actionsMap: this.actionsMap,
        resourceChangeEnabled: !type,
      },
    });
    modelRef.componentInstance.finished.pipe(first()).subscribe(res => {
      modelRef.close();
      if (res) {
        // 折叠全部
        this.permissionsCatalog = [...this.permissionsCatalog];

        this.permissionsMap = Object.assign({}, this.permissionsMap, res);
        Object.keys(res).forEach(type => {
          this.checkedMap[type] = true;
        });
        this.auiNotificationService.success(
          this.translateService.get('rbac_add_permission_success'),
        );
        // 展开有值的
        this.expandHasVal();
      }
    });
  }

  async removePermission(type: string, i: number) {
    try {
      await this.modalService.confirm({
        title: this.translateService.get('remove'),
        content: this.translateService.get('rbac_confirm_remove_permission'),
      });
      this.permissionsMap[type].splice(i, 1);
      if (!this.permissionsMap[type].length) {
        this.checkedMap[type] = false;
      }
    } catch (rejection) {}
  }

  import(uuid: string) {
    const modelRef = this.modalService.open(RoleImportComponent, {
      title: this.translateService.get('select_role_template'),
      width: 1280,
      mode: ModalMode.RIGHT_SLIDER,
      data: { uuid },
    });
    modelRef.componentInstance.finished.pipe(first()).subscribe(res => {
      modelRef.close();
      if (res) {
        this.data.template_uuid = res.template_uuid;
        const permissionsMap = res.map;
        // 折叠全部
        this.permissionsCatalog = [...this.permissionsCatalog];

        // 筛选可用约束条件
        Object.keys(permissionsMap).forEach(type => {
          const validConstraints = Object.keys(this.schemaMap[type]);
          const importConstraints = Object.keys(
            permissionsMap[type][0].constraints[0],
          );
          importConstraints.forEach(key => {
            if (!validConstraints.includes(key)) {
              delete permissionsMap[type][0].constraints[0][key];
            }
          });
          if (!importConstraints.length) {
            permissionsMap[type][0].constraints = [];
          }
        });
        // 合并constraints
        Object.keys(this.permissionsMap).forEach(type => {
          if (permissionsMap[type]) {
            permissionsMap[type][0].constraints = [
              ...permissionsMap[type][0].constraints,
              ...this.permissionsMap[type][0].constraints,
            ];
          }
        });

        this.permissionsMap = Object.assign(
          {},
          this.permissionsMap,
          permissionsMap,
        );

        Object.keys(permissionsMap).forEach(type => {
          this.checkedMap[type] = true;
        });
        this.auiNotificationService.success(
          this.translateService.get('rbac_import_success'),
        );
        // 展开
        this.expandHasVal();
      } else {
        this.data.template_uuid = '';
      }
    });
  }

  private expandHasVal() {
    setTimeout(() => {
      const types = Object.keys(this.permissionsMap);
      for (let i = 0; i < this.permissionsCatalog.length; i++) {
        const line = this.permissionsCatalog[i].type;
        const expand = Object.keys(this.permissionsCatalogMap[line]).some(
          key => {
            return !!_.intersection(
              this.permissionsCatalogMap[line][key],
              types,
            ).length;
          },
        );
        if (expand) {
          this.toggleExpandRow(this.permissionsCatalog[i], i);
        }
      }
    }, 100);
  }

  submit() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    const data = _.cloneDeep(this.data);
    data.parents = data.parents.filter(item => !!item.uuid);
    if (
      !Object.values(this.checkedMap).some(checked => !!checked) &&
      !data.parents.length
    ) {
      this.auiNotificationService.error(
        this.translateService.get('rbac_at_least_a_permission'),
      );
      return;
    }
    let permissions: Permission[] = [];
    Object.keys(this.permissionsMap).forEach(type => {
      permissions = [...permissions, ...this.permissionsMap[type]];
    });
    data.permissions = permissions;
    const project_name = this.project === this.select_none ? '' : this.project;
    this.rbacService
      .createRole(data, { project_name })
      .then(() => {
        this.auiNotificationService.success(
          this.translateService.get('create_success'),
        );
        const project = getCookie('project');
        if (project) {
          this.router.navigateByUrl(`projects/detail/${project}`);
        } else {
          this.router.navigateByUrl('rbac');
        }
      })
      .catch(({ errors }) => {
        this.auiNotificationService.error(
          this.translateService.get(errors[0].code),
        );
      });
  }

  showParent(role_name: string) {
    this.modalService.open(ParentViewComponent, {
      title: this.translateService.get('rbac_parent_roles'),
      data: { role_name },
    });
  }

  async cancel() {
    try {
      await this.modalService.confirm({
        title: this.translateService.get('cancel'),
        content: this.translateService.get('rbac_give_up'),
      });
      this.router.navigateByUrl('rbac');
    } catch (rejection) {}
  }

  get lang() {
    return this.translateService.currentLang;
  }
}
