import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';

import { NotificationService } from 'alauda-ui';
import {
  PERMISSIONS_CATALOG,
  PERMISSIONS_CATALOG_MAP,
  TRANSLATE_MAP,
} from 'app2/features/rbac/rbac-constant';
import { RBACService } from 'app2/shared/services/features/rbac.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';
import * as _ from 'lodash';
import moment from 'moment';

@Component({
  templateUrl: './role-template-import.component.html',
  styleUrls: ['./role-template-import.component.scss'],
})
export class RoleImportComponent implements OnInit {
  @ViewChild('permissionsTable')
  table: any;
  @ViewChild('form')
  form: any;
  @Output()
  finished = new EventEmitter<any>();

  loading = true;
  title = '';
  description = '';
  translateMap = TRANSLATE_MAP;
  permissionsCatalog: any[];
  permissionsCatalogMap = PERMISSIONS_CATALOG_MAP;
  scope: any[];
  actionsMap = {};
  permissionsMap = {};
  _ = _;

  tabIndex: number;
  tabList: any[];
  curUuid = '';

  constructor(
    private rbacService: RBACService,
    private modalService: ModalService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    @Inject(MODAL_DATA) private modalData: { uuid: string },
  ) {}

  async ngOnInit() {
    await this.rbacService
      .getRoleSchema()
      .then(res => {
        res.forEach((item: any) => {
          this.actionsMap[item.resource] = item.actions.map((act: string) => ({
            name: this.translateService.get(act.split(':')[1]),
            value: act.split(':')[1],
          }));
        });
      })
      .catch(() => {});

    await this.rbacService
      .getTemplateList()
      .then(res => {
        this.tabList = _.orderBy(
          res,
          [item => moment.utc(item.updated_at).valueOf()],
          'desc',
        );
      })
      .catch(() => {})
      .then(() => {
        this.loading = false;
      });
    if (this.modalData.uuid) {
      const index = _.findIndex(this.tabList, tab => {
        return tab.uuid === this.modalData.uuid;
      });
      setTimeout(() => {
        this.tabChanged(index);
      }, 100);
    }
  }

  async tabChanged(index: number) {
    if (index === undefined || this.tabIndex === index) {
      return;
    }
    const dirty = Object.keys(this.form.value).some(
      key => !!this.form.value[key],
    );
    if (dirty) {
      try {
        await this.modalService.confirm({
          title: this.translateService.get('rbac_switch'),
          content: this.translateService.get('rbac_switch_confirm'),
        });
      } catch (rejection) {
        return;
      }
    }
    this.loading = true;
    this.tabIndex = index;
    if (this.tabList[index].uuid) {
      this.curUuid = this.tabList[index].uuid;
      this.permissionsMap = {};
      await this.rbacService
        .getTemplateDetail(this.tabList[index].uuid)
        .then(res => {
          res.permissions.forEach(item => {
            delete item.uuid;
            delete item.template_uuid;
            this.permissionsMap[item.resource_type] = item;
          });
          // set title & description
          this.title =
            (this.lang.match('en') &&
              (res.display_name_en ? res.display_name_en : res.display_name)) ||
            res.name;
          this.description = res.description;
          // set scope
          if (res.scope && Object.keys(res.scope).length) {
            this.scope = Object.keys(res.scope).map(scope => ({
              key: scope,
              val: '',
            }));
          } else {
            this.scope = [];
          }
        })
        .catch(() => {});
    }
    // 删掉空行
    this.permissionsCatalog = this.catalogFilter(
      PERMISSIONS_CATALOG,
      this.permissionsMap,
      this.permissionsCatalogMap,
    );
    setTimeout(() => {
      for (let i = 0; i < this.permissionsCatalog.length; i++) {
        this.toggleExpandRow(this.permissionsCatalog[i], i);
      }
      this.loading = false;
    }, 100);
  }

  private catalogFilter(
    permissionsCatalog: Array<any>,
    permissionsMap: object,
    permissionsCatalogMap: object,
  ) {
    return permissionsCatalog.filter(item => {
      let flag = false;
      for (const key in permissionsCatalogMap[item.type]) {
        if (!flag) {
          flag = permissionsCatalogMap[item.type][key].some(
            (type: string) => !!permissionsMap[type],
          );
        }
      }
      return flag;
    });
  }

  translateTypes(type: string) {
    return `(${Object.keys(this.permissionsCatalogMap[type])
      .map(key => this.translateService.get(key))
      .join(', ')})`;
  }

  get lang() {
    return this.translateService.currentLang;
  }

  toggleExpandRow(row: any, rowIndex: number) {
    this.table.toggleExpandRow(row, rowIndex);
  }

  keys(obj: object) {
    return Object.keys(obj);
  }

  async confirm() {
    const newMap = {};
    const constraints: any[] = [{}];
    let project_name = '';
    let namespace_name = '';
    this.form.onSubmit(null);
    if (this.form.invalid && this.scope.length > 0) {
      return;
    }
    if (this.scope.length > 0) {
      this.scope.forEach(item => {
        if (item.key === 'project') {
          project_name = item.val;
        } else if (item.key === 'namespace') {
          namespace_name = item.val;
        }
        constraints[0]['res:' + item.key] = item.val;
      });
    }
    Object.keys(this.permissionsMap).forEach(type => {
      newMap[type] = _.cloneDeep(this.permissionsMap[type]);
      newMap[type].constraints = _.cloneDeep(constraints);
      newMap[type].resource[0] = newMap[type].resource[0].replace(
        '{{project_name}}',
        project_name,
      );
      newMap[type].resource[0] = newMap[type].resource[0].replace(
        '{{namespace_name}}',
        namespace_name,
      );
      newMap[type] = [newMap[type]];
    });
    // dry-run
    const error = await this.rbacService
      .temmplateGenerate(this.tabList[this.tabIndex].uuid)
      .then(() => null)
      .catch(({ errors }) => errors[0]);
    if (error && error.code === 'permissions_partially_accepted') {
      try {
        await this.modalService.confirm({
          title: this.translateService.get('rbac_confirm_continue_import'),
          content: this.translateService.get(
            'rbac_permission_partially_denied',
          ),
        });
        error.permissions.forEach((permission: any) => {
          if (permission !== null) {
            delete newMap[permission.resource_type];
          }
        });
      } catch (rejection) {
        return;
      }
    } else if (error) {
      this.auiNotificationService.error(
        this.translateService.get('rbac_import_failed'),
      );
      return;
    }
    this.finished.next({ template_uuid: this.curUuid, map: newMap });
  }

  async cancel() {
    if (this.tabIndex !== undefined || this.form.dirty) {
      try {
        await this.modalService.confirm({
          title: this.translateService.get('cancel'),
          content: this.translateService.get('rbac_give_up'),
        });
        this.modalService.closeAll();
      } catch (rejection) {}
    } else {
      this.modalService.closeAll();
    }
  }
}
