import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { NotificationService } from 'alauda-ui';
import { CheckboxComponent } from 'app2/shared/components/checkbox/checkbox.component';
import { RBACService } from 'app2/shared/services/features/rbac.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-add-parent',
  templateUrl: './add-parent.component.html',
  styleUrls: ['./add-parent.component.scss'],
})
export class AddParentComponent implements OnInit {
  @Output()
  afterConfirmed: EventEmitter<any> = new EventEmitter();
  @ViewChildren(CheckboxComponent)
  checkBoxGroup: QueryList<CheckboxComponent>;
  roles: any[] = [];
  name = '';
  uuid = '';
  loading = true;
  initialized = false;
  disabled = false;

  constructor(
    private rbacService: RBACService,
    private modalService: ModalService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private errorsToast: ErrorsToastService,
    @Inject(MODAL_DATA) private modalData: { role_name: string },
  ) {}

  ngOnInit() {
    this.searched();
  }

  searched(value: string = '') {
    this.loading = true;
    this.rbacService
      .getRoles(20, 1, value)
      .then(({ results }) => {
        this.roles = results;
        this.name = null;
        this.uuid = null;
        this.initialized = true;
      })
      .catch(err => this.errorsToast.error(err))
      .then(() => {
        this.loading = false;
      });
  }

  onParentChange($event: any) {
    if ($event) {
      this.disabled = false;
      this.name = $event.name;
    } else {
      this.disabled = true;
    }
  }

  confirm() {
    if (!this.uuid) {
      this.disabled = true;
      this.auiNotificationService.error(
        this.translateService.get('rbac_parent_required'),
      );
      this.loading = false;
      return;
    }
    this.loading = true;
    this.rbacService
      .addParent(this.modalData.role_name, { name: this.name, uuid: this.uuid })
      .then(() => {
        this.afterConfirmed.next(true);
        this.modalService.closeAll();
      })
      .catch(err => this.errorsToast.error(err))
      .then(() => {
        this.loading = false;
      });
  }

  cancel() {
    this.modalService.closeAll();
  }
}
