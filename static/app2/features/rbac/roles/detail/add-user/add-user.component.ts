import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { remove, uniqBy } from 'lodash';

import { CheckboxComponent } from 'app2/shared/components/checkbox/checkbox.component';
import { OrgService } from 'app2/shared/services/features/org.service';
import { RBACService } from 'app2/shared/services/features/rbac.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';

interface PostDataItem {
  user: string;
}

@Component({
  selector: 'rc-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
})
export class AddUserComponent implements OnInit {
  @Output()
  afterConfirmed: EventEmitter<any> = new EventEmitter();
  @ViewChildren(CheckboxComponent)
  checkBoxGroup: QueryList<CheckboxComponent>;
  users: any[] = [];
  data: PostDataItem[] = [];
  loading = true;
  initialized = false;

  constructor(
    private orgService: OrgService,
    private rbacService: RBACService,
    private modalService: ModalService,
    private errorsToast: ErrorsToastService,
    @Inject(MODAL_DATA) private modalData: { role_name: string },
  ) {}

  ngOnInit() {
    this.searched();
  }

  searched(value: string = '') {
    this.loading = true;
    this.orgService
      .listOrgAccounts({ search: value, page_size: 10 })
      .then(({ results }) => {
        this.users = results;
        this.data = [];
        this.checkBoxGroup.forEach(checkBox => {
          checkBox.value = false;
        });
        this.initialized = true;
      })
      .catch(err => this.errorsToast.error(err))
      .then(() => {
        this.loading = false;
      });
  }

  isInvalidAccount(account: any) {
    return account.type === 'organizations.LDAPAccount' && !account.is_valid;
  }

  onCheck($event: any, user: string) {
    if ($event) {
      this.data.push({ user });
    } else {
      remove(this.data, item => item.user === user);
    }
    this.data = uniqBy(this.data, 'user');
  }

  confirm() {
    this.loading = true;
    this.rbacService
      .assignUser(this.modalData.role_name, this.data)
      .then(() => {
        this.afterConfirmed.next(true);
        this.modalService.closeAll();
      })
      .catch(err => this.errorsToast.error(err))
      .then(() => {
        this.loading = false;
      });
  }

  cancel() {
    this.modalService.closeAll();
  }
}
