import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { ParentViewComponent } from 'app2/features/rbac/parent-view/parent-view.component';
import { PermissionsEditorComponent } from 'app2/features/rbac/permissions-editor/permissions-editor.component';
import {
  PERMISSIONS_CATALOG,
  PERMISSIONS_CATALOG_MAP,
} from 'app2/features/rbac/rbac-constant';
import { AddParentComponent } from 'app2/features/rbac/roles/detail/add-parent/add-parent.component';
import { AddUserComponent } from 'app2/features/rbac/roles/detail/add-user/add-user.component';
import { RBACService } from 'app2/shared/services/features/rbac.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { delay } from 'app2/shared/services/utility/delay';
import { TranslateService } from 'app2/translate/translate.service';
import { cloneDeep, intersection } from 'lodash';
import { first } from 'rxjs/operators';

interface Data {
  parents: any[];
  uuid: string;
  name: string;
  permissions: any[];
}

@Component({
  selector: 'rc-role-detail',
  templateUrl: './role-detail.component.html',
  styleUrls: ['./role-detail.component.scss'],
})
export class RoleDetailComponent implements OnInit {
  @ViewChild('permissionsTable')
  table: any;

  roleUpdatePermission: boolean;
  roleAssignPermission: boolean;
  roleRevokePermission: boolean;

  permissionsCatalog = [...PERMISSIONS_CATALOG];
  permissionsCatalogMap = PERMISSIONS_CATALOG_MAP;
  translateTabs = {
    permission: this.translateService.get('permission'),
    parent: this.translateService.get('parent'),
  };
  schemaMap = {};
  permissionsMap = {};
  actionsMap = {};
  initialized = false;
  loading = true;
  currentUserPage = 1;
  userCount = 0;
  users: any = [];
  basicInfo = {
    created_by: '',
    created_at: '',
    updated_at: '',
  };

  data: Data = {
    name: '',
    uuid: '',
    parents: [],
    permissions: [],
  };

  constructor(
    private translateService: TranslateService,
    private rbacService: RBACService,
    private route: ActivatedRoute,
    private modalService: ModalService,
    private auiNotificationService: NotificationService,
    private router: Router,
    private roleUtil: RoleUtilitiesService,
  ) {}

  async ngOnInit() {
    [
      this.roleUpdatePermission,
      this.roleAssignPermission,
      this.roleRevokePermission,
    ] = await this.roleUtil.resourceTypeSupportPermissions('role', {}, [
      'update',
      'assign',
      'revoke',
    ]);
    this.route.queryParams.subscribe(({ role_name }) => {
      this.data.name = role_name;
    });
    await this.rbacService
      .getRole(this.data.name)
      .then((res: any) => {
        this.data.parents = res.parents;
        this.data.name = res.name;
        this.data.uuid = res.uuid;
        this.basicInfo.created_by = res.created_by;
        this.basicInfo.created_at = res.created_at;
        this.basicInfo.updated_at = res.updated_at;
        if (res.permissions) {
          res.permissions.forEach((permission: any) => {
            delete permission.uuid;
            if (this.permissionsMap[permission.resource_type]) {
              this.permissionsMap[permission.resource_type].push(permission);
            } else {
              this.permissionsMap[permission.resource_type] = [permission];
            }
          });
        }
      })
      .catch(({ errors }) => {
        this.auiNotificationService.error(
          this.translateService.get(errors[0].code),
        );
        this.router.navigateByUrl('rbac');
      });
    await this.rbacService
      .getRoleSchema()
      .then(res => {
        res.forEach((item: any) => {
          this.schemaMap[item.resource] = item.constraints;
          this.actionsMap[item.resource] = item.actions.map((act: string) => ({
            name: this.translateService.get(act.split(':')[1]),
            value: act.split(':')[1],
          }));
        });
      })
      .catch(() => {});
    await this.pageChanged(1);
    this.expandHasVal();
    this.initialized = true;
  }

  toggleExpandRow(row: any, rowIndex: number) {
    this.table.toggleExpandRow(row, rowIndex);
  }

  translateTypes(type: string) {
    return `(${Object.keys(this.permissionsCatalogMap[type])
      .map(key => this.translateService.get(key))
      .join(', ')})`;
  }

  keys(object: object) {
    return Object.keys(object);
  }

  addPermission(type: string) {
    const modelRef = this.modalService.open(PermissionsEditorComponent, {
      title: this.translateService.get('add_permission'),
      data: {
        type,
        permissionsMap: this.permissionsMap,
        schemaMap: this.schemaMap,
        actionsMap: this.actionsMap,
        resourceChangeEnabled: !type,
      },
    });
    modelRef.componentInstance.finished.pipe(first()).subscribe(res => {
      modelRef.close();
      if (res) {
        const oldPermissionsMap = cloneDeep(this.permissionsMap);
        // 折叠全部
        this.permissionsCatalog = [...this.permissionsCatalog];
        this.permissionsMap = Object.assign({}, this.permissionsMap, res);
        if (!this.submit()) {
          this.permissionsMap = oldPermissionsMap;
        }
        // 展开有值的
        this.expandHasVal();
      }
    });
  }

  private expandHasVal() {
    setTimeout(() => {
      const types = Object.keys(this.permissionsMap);
      for (let i = 0; i < this.permissionsCatalog.length; i++) {
        const line = this.permissionsCatalog[i].type;
        const expand = Object.keys(this.permissionsCatalogMap[line]).some(
          key => {
            return !!intersection(this.permissionsCatalogMap[line][key], types)
              .length;
          },
        );
        if (expand) {
          this.toggleExpandRow(this.permissionsCatalog[i], i);
        }
      }
    }, 100);
  }

  async removePermission(type: string, i: number) {
    try {
      await this.modalService.confirm({
        title: this.translateService.get('remove'),
        content: this.translateService.get('rbac_confirm_remove_permission'),
      });
      const oldPermissionsMap = cloneDeep(this.permissionsMap);
      this.permissionsMap[type].splice(i, 1);
      if (!this.submit()) {
        this.permissionsMap = oldPermissionsMap;
      }
    } catch (rejection) {}
  }

  showParent(role_name: string) {
    this.modalService.open(ParentViewComponent, {
      title: this.translateService.get('rbac_parent_roles'),
      data: { role_name },
    });
  }

  removeParent(role_name: string, uuid: string) {
    this.modalService
      .confirm({
        title: this.translateService.get('confirm'),
        content: this.translateService.get('rbac_delete_parent_confirm', {
          role_name,
        }),
      })
      .then(() => {
        this.rbacService
          .deleteParent(this.data.name, uuid)
          .then(() => {
            this.data.parents = this.data.parents.filter(
              parent => parent.name !== role_name,
            );
            this.auiNotificationService.success(
              this.translateService.get('delete_success'),
            );
          })
          .catch(({ errors }) => {
            this.auiNotificationService.error(
              this.translateService.get(errors[0].code),
            );
          });
      })
      .catch(() => {});
  }

  addParent() {
    const modalRef = this.modalService.open(AddParentComponent, {
      width: 600,
      title: this.translateService.get('rbac_add_parent_role'),
      data: { role_name: this.data.name },
    });
    modalRef.componentInstance.afterConfirmed.subscribe((res: boolean) => {
      if (res) {
        this.rbacService.getRole(this.data.name).then((res: any) => {
          this.data.parents = res.parents;
        });
      }
    });
  }

  pageChanged($event: any, search: string = '') {
    this.loading = true;
    this.currentUserPage = $event;
    this.rbacService
      .getUsers(this.data.name, search, $event)
      .then((res: any) => {
        this.userCount = res.count;
        this.users = res.results || [];
      })
      .catch(({ errors }) => {
        this.auiNotificationService.error(
          this.translateService.get(errors[0].code),
        );
      })
      .then(() => {
        this.loading = false;
      });
  }

  removeUser(user: string) {
    this.modalService
      .confirm({
        title: this.translateService.get('remove'),
        content: this.translateService.get('delete_user_role_confirm', {
          name: user,
        }),
      })
      .then(() => {
        this.rbacService
          .deleteUser(this.data.name, [{ user }])
          .then(() => {
            this.auiNotificationService.success(
              this.translateService.get('delete_success'),
            );
            this.pageChanged(1);
          })
          .catch(({ errors }) => {
            this.auiNotificationService.error(
              this.translateService.get(errors[0].code),
            );
          });
      })
      .catch(() => {});
  }

  async addUsers() {
    const modalRef = this.modalService.open(AddUserComponent, {
      width: 600,
      title: this.translateService.get('add_user'),
      data: { role_name: this.data.name },
    });
    modalRef.componentInstance.afterConfirmed.subscribe((res: boolean) => {
      if (res) {
        this.pageChanged(1);
      }
    });
  }

  search($event: any) {
    this.pageChanged(1, $event);
  }

  async showDetail(role_name: string) {
    this.router.navigateByUrl('empty-route');
    await delay();
    this.router.navigateByUrl(`/rbac/roles/detail?role_name=${role_name}`);
  }

  async submit() {
    let succeed = false;
    let permissions: any[] = [];
    Object.keys(this.permissionsMap).forEach(type => {
      permissions = [...permissions, ...this.permissionsMap[type]];
    });
    this.data.permissions = permissions;
    await this.rbacService
      .updateRole(this.data, this.data.name)
      .then(() => {
        succeed = true;
        this.auiNotificationService.success(
          this.translateService.get('update_success'),
        );
      })
      .catch(({ errors }) =>
        this.auiNotificationService.error(
          this.translateService.get(errors[0].code),
        ),
      );
    return succeed;
  }
}
