import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ACCOUNT, WEBLABS } from 'app2/core/tokens';
import { RcAccount, RcRole, Weblabs } from 'app2/core/types';
import { OrgService } from 'app2/shared/services/features/org.service';
import { RoleService } from 'app2/shared/services/features/role.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';

@Component({
  selector: 'rc-app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss'],
})
export class UserAddComponent implements OnInit {
  roles: Array<RcRole> = [];
  users: Array<any> = [];
  filterRoles: Array<RcRole> = [];
  filterUsers: Array<any> = [];
  initialized: boolean;
  user: RcRole;
  selectedRoleName: string;
  roleName: string;
  submitting: boolean;
  @ViewChild('UserAddForm')
  form: NgForm;
  @Output()
  afterConfirmed: EventEmitter<any> = new EventEmitter();

  constructor(
    private modalService: ModalService,
    private orgService: OrgService,
    private errorHandle: ErrorsToastService,
    private roleService: RoleService,
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(MODAL_DATA) private modalData: any,
  ) {}

  async ngOnInit() {
    this.roleName = this.modalData.roleName;
    this.users = this.modalData.users;
    [this.filterRoles, this.filterUsers] = await Promise.all([
      this.roleService
        .getRoleList({ ignoreProject: true, page_size: 0 })
        .then(({ results }) => results),
      this.orgService.getAllRolesOrUsers('user'),
    ]);
    this.initialized = true;
  }

  responseItemFilter(options: any) {
    return !this.users.find((user: any) => user === options.username);
  }

  onRoleChanged(value: string) {
    this.selectedRoleName = value;
  }

  onUserAdd(value: RcRole) {
    this.users.push(value);
  }

  onUserRemove(value: RcRole) {
    this.users.splice(this.users.findIndex(item => item === value), 1);
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.users.length || (!this.selectedRoleName && !this.roleName)) {
      return;
    }
    this._addUsers();
  }

  cancel() {
    this.modalService.closeAll();
  }

  _addUsers() {
    this.submitting = true;
    const _users = this.users.map(item => {
      return {
        user: item,
      };
    });
    return this.roleService
      .addRoleUsers(this.selectedRoleName || this.roleName, _users)
      .then(() => {
        this.modalService.closeAll();
        this.afterConfirmed.emit();
      })
      .catch(e => this.errorHandle.error(e))
      .then(() => {
        this.submitting = false;
      });
  }
}
