import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { ConfirmBoxComponent } from 'app2/shared/components/confirm-box/confirm-box.component';
import { TooltipDirective } from 'app2/shared/directives/tooltip/tooltip.directive';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { RoleService } from 'app2/shared/services/features/role.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { forEach } from 'lodash';

interface Role {
  parents: any[];
  name: string;
  permissions: any[];
}

interface ResourceType {
  privilege?: string;
  resource_actions?: string[];
}

@Component({
  selector: 'rc-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.scss'],
})
export class RoleListComponent implements OnInit {
  initialized: boolean;
  roles: Array<any>;
  pagination: {
    page: number;
    count: number;
    page_size: number;
  };
  popoverConfig: any;
  createRoleEnabled: boolean;
  hasTemplatePermission: boolean;
  rolesLoading: boolean;
  search: string;
  searching: boolean;
  roleUsersCache: any = {};

  constructor(
    private router: Router,
    private modalService: ModalService,
    private translate: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private roleService: RoleService,
    private errorHandle: ErrorsToastService,
    @Inject(ACCOUNT) public account: RcAccount,
  ) {}

  searched() {}

  async ngOnInit() {
    this.initialized = false;
    this.roles = [];
    this.pagination = {
      page: 1,
      count: 0,
      page_size: 20,
    };
    this.popoverConfig = {
      list: [],
      loading: false,
      role_name: '',
      role: null, // used for resource_actions check
      noDataText: this.translate.get('role_has_no_user'),
    };
    this.roleUtil
      .resourceTypeSupportPermissions('role')
      .then(res => (this.createRoleEnabled = res));
    this.roleUtil
      .resourceTypeSupportPermissions('role_template', {}, 'view')
      .then(res => (this.hasTemplatePermission = res));

    this.fetchRoleList().then(() => {
      // rbSafeApply();
      this.initialized = true;
    });
  }

  roleTemplateList() {
    this.router.navigate(['/role-template']);
  }

  fetchRoleList() {
    this.rolesLoading = true;
    return this.roleService
      .getRoleList({
        search: this.search,
        page: this.pagination.page,
        page_size: this.pagination.page_size,
        ignoreProject: true,
      })
      .then((data: any) => {
        this.roles = data.results;
        this.pagination.count = data.count;
      })
      .catch(e => this.errorHandle.error(e))
      .then(() => {
        this.rolesLoading = false;
      });
  }

  onSearchChanged(value: string) {
    this.search = value;
    this.pagination.page = 1;
    this.searching = true;
    this.fetchRoleList().then(() => {
      this.searching = false;
    });
  }

  pageNoChange(page: number) {
    this.pagination.page = page;
    this.fetchRoleList();
  }

  createRole() {
    this.router.navigateByUrl('/rbac/roles/create');
  }

  showRoleDetail(role: Role) {
    this.router.navigateByUrl(`/rbac/roles/detail?role_name=${role.name}`);
  }

  deleteRole(role: Role) {
    const title = this.translate.get('delete_role');
    const content = this.translate.get('delete_role_confirm', {
      name: role.name,
    });

    const modalRef = this.modalService.open(ConfirmBoxComponent, {
      title,
    });

    modalRef.componentInstance.content = content;
    modalRef.componentInstance.confirmed.subscribe((res: boolean) => {
      modalRef.close(res);
      if (res) {
        this._deleteRole(role);
      }
    });
  }

  _deleteRole(role: Role) {
    this.roleService
      .deleteRole(role.name)
      .then(() => {
        if (this.roles.length === 1) {
          this.pagination.page = Math.max(this.pagination.page - 1, 1);
        }
        this.fetchRoleList();
      })
      .catch(e => this.errorHandle.error(e));
  }

  fetchRoleUsers(role: Role, tooltip: TooltipDirective) {
    const role_name = role.name;
    this.popoverConfig.role = role;
    this.popoverConfig.role_name = role_name;
    this.popoverConfig.list = [];
    this.popoverConfig.loading = true;
    // rbDelay(200);
    this.roleService
      .listRoleUsers({
        page_size: 50,
        role_name,
      })
      .then((data: any) => {
        this.roleUsersCache[role_name] = data.results || [];
        this.popoverConfig.list = this.roleUsersCache[role_name];
        tooltip.updatePosition();
      })
      .catch(e => this.errorHandle.error(e))
      .then(() => {
        this.popoverConfig.loading = false;
      });
  }

  deleteUserOfRole(username: string) {
    const users = [
      {
        user: username,
      },
    ];
    const list = this.roleUsersCache[this.popoverConfig.role_name];
    this.popoverConfig.loading = true;
    this.roleService
      .deleteRoleUsers(this.popoverConfig.role_name, users)
      .then(() => {
        forEach(list, (item, index) => {
          if (item.user === username) {
            list.splice(index, 1);
            return false;
          }
        });
      })
      .catch(e => this.errorHandle.error(e))
      .then(() => {
        this.popoverConfig.loading = false;
      });
  }

  buttonDisplayExpr(item: ResourceType, action: string) {
    return this.roleUtil.resourceHasPermission(item, 'role', action);
  }
}
