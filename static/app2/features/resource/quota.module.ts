import { NgModule } from '@angular/core';
import { QuotaRoutingModule } from 'app2/features/resource/quota.routing.module';
import { SharedModule } from 'app2/shared/shared.module';

import { QuotaChargeListComponent } from './quota/charge-list/quota-charge-list.component';
import { QuotaDetailComponent } from './quota/detail/quota-detail.component';
import { QuotaUsageCardComponent } from './quota/detail/quota-usage-card.component';
import { QuotaListCardComponent } from './quota/list-card/quota-list-card.component';
import { QuotaListComponent } from './quota/list/quota-list.component';
import { QuotaResourceListComponent } from './quota/resource-list/quota-resource-list.component';
import { QuotaUpdateDialogComponent } from './quota/update/quota-update-dialog.component';

@NgModule({
  imports: [SharedModule, QuotaRoutingModule],
  declarations: [
    QuotaListComponent,
    QuotaListCardComponent,
    QuotaUpdateDialogComponent,
    QuotaUsageCardComponent,
    QuotaResourceListComponent,
    QuotaChargeListComponent,
    QuotaDetailComponent,
  ],
  entryComponents: [QuotaListComponent, QuotaUpdateDialogComponent],
})
export class QuotaModule {}
