import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { QuotaDetailComponent } from './quota/detail/quota-detail.component';
import { QuotaListComponent } from './quota/list/quota-list.component';

const routes: Routes = [
  {
    path: '',
    component: QuotaListComponent,
  },
  {
    path: 'detail/:name',
    component: QuotaDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class QuotaRoutingModule {}
