import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
} from '@angular/core';

import { Router } from '@angular/router';
import { ACCOUNT, WEBLABS } from 'app2/core/tokens';
import { RcAccount, Weblabs } from 'app2/core/types';
import {
  QuotaCharges,
  ResourceService,
} from 'app2/shared/services/features/resource.service';
import { LoggerUtilitiesService } from 'app2/shared/services/utility/logger.service';
import { max, orderBy } from 'lodash';

@Component({
  selector: 'rc-quota-charge-list',
  templateUrl: './quota-charge-list.component.html',
  styleUrls: ['./quota-charge-list.component.scss'],
})
export class QuotaChargeListComponent implements OnInit {
  @Input()
  spaceName: string;
  @Input()
  quotaName: string;
  @Input()
  percent: string;
  @Output()
  close = new EventEmitter<void>();
  unit: string;
  initailized = false;
  loading = true;
  loadError = false;
  quotaLabel: string;
  list: QuotaCharges[];
  max: number;
  QUOTA_TYPE_LABELS = {
    'service.mem': 'quota_service_mem',
    'service.cpu': 'quota_service_cpu',
  };
  constructor(
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(WEBLABS) public weblabs: Weblabs,
    private resourceService: ResourceService,
    private logger: LoggerUtilitiesService,
    private router: Router,
  ) {}

  async ngOnInit() {
    this.list = [];
    this.unit =
      'unit_' +
      (await this.resourceService.getQuotaConfig()).find(
        config => config.name === this.quotaName,
      ).unit;

    await this.refetch();
    this.initailized = true;
  }

  async refetch() {
    this.quotaLabel = this.QUOTA_TYPE_LABELS[this.quotaName];

    try {
      this.list = await this.resourceService.getQuotaCharges(
        this.spaceName,
        this.quotaName,
      );
      this.max = max(this.list.map((usage: QuotaCharges) => usage.request));
      this.list.forEach((usage: QuotaCharges) => {
        usage.normalizedWidth =
          ((usage.request / this.max) * 100).toFixed(2) + '%';
        usage.displayName = usage.app_name
          ? usage.app_name + '.' + usage.name
          : usage.name;
      });
      this.list = orderBy(this.list, ['request'], ['desc']);
    } catch (err) {
      this.loadError = true;
      this.logger.error('Failed to load charge list data');
    }

    this.loading = false;
  }

  viewServiceDetail(charge: QuotaCharges) {
    this.router.navigateByUrl(
      `/app_service/service/service_detail/${charge.uuid}`,
    );
  }
}
