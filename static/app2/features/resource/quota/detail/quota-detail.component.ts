import {
  Component,
  Inject,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';
import { first, switchMap } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { SidePanelService } from 'app2/layout/side-panel.service';
import { ResourceService } from 'app2/shared/services/features/resource.service';
import {
  QuotaTypes,
  Space,
} from 'app2/shared/services/features/resource.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';

import { QuotaUpdateDialogComponent } from '../update/quota-update-dialog.component';

@Component({
  templateUrl: './quota-detail.component.html',
  styleUrls: ['./quota-detail.component.scss'],
})
export class QuotaDetailComponent implements OnInit, OnDestroy {
  initialized = false;
  showMore = false;
  deleting = false;
  spaceName: string;
  quotaName: string;
  percent: string;
  space: Space;
  namespace: string;
  currentTab: string;
  activeQuotaCard: string;
  paramSubscription$: Subscription;
  QUOTA_SPACE_STATUS_TO_CSS_STATUS = {
    running: 'normal',
    updating: 'deploying',
    failed: 'overflow',
  };

  @ViewChild('chargeTemplate', { read: TemplateRef })
  private chargeTemplateRef: TemplateRef<any>;

  constructor(
    @Inject(ACCOUNT) public account: RcAccount,
    private resourceService: ResourceService,
    private route: ActivatedRoute,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private roleUtilities: RoleUtilitiesService,
    private sidePanel: SidePanelService,
    private modalService: ModalService,
    private router: Router,
  ) {}

  async ngOnInit() {
    this.namespace = this.account.namespace;
    this.space = {
      quotas: [],
    };
    this.currentTab = '';
    this.refetch();
  }

  ngOnDestroy() {
    this.sidePanel.close();
    this.paramSubscription$.unsubscribe();
  }

  async refetch() {
    this.paramSubscription$ = this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) => {
          this.spaceName = params.get('name');
          return this.resourceService.getSpace(params.get('name'));
        }),
      )
      .subscribe(
        (space: Space) => (this.space = space),
        ({ status }) => {
          if (status === 403) {
            this.auiNotificationService.warning(
              this.translate.get('permission_denied'),
            );
          } else if (status === 404) {
            this.auiNotificationService.warning(
              this.translate.get('quota_space_not_exist'),
            );
          }
          return this.router.navigateByUrl('quota');
        },
      );
    this.initialized = true;
  }

  async refetchAgain() {
    this.resourceService.getSpace(this.spaceName).then(space => {
      this.space = space;
    });
  }

  buttonDisplayExpr(action: string) {
    return this.roleUtilities.resourceHasPermission(
      this.space,
      'space',
      action,
    );
  }

  showUpdateDialog(): void {
    const modelRef = this.modalService.open(QuotaUpdateDialogComponent, {
      title: this.translate.get('update'),
      width: 900,
    });
    modelRef.componentInstance.isNew = false;
    modelRef.componentInstance.setModel(this.space);
    modelRef.componentInstance.finished.pipe(first()).subscribe(() => {
      modelRef.close();
      this.refetchAgain();
      if (this.activeQuotaCard) {
        this.onQuotaCardClicked(
          this.space.quotas.find(quota => quota.name === this.activeQuotaCard),
        );
      }
    });
  }

  async deleteClicked() {
    if (this.deleting) {
      return;
    }
    await this.modalService.confirm({
      title: this.translate.get('delete'),
      content: this.translate.get('quota_delete_confirm_info', {
        name: this.space.name,
      }),
    });
    this.deleting = true;
    try {
      await this.resourceService.deleteSpace(this.space.name);
      this.router.navigateByUrl('quota');
    } catch (err) {
      const error = err.errors[0];
      if ('child_resources_exist' === error.code) {
        // code 'child_resources_exist' used for resource delete (when sub resources exist)
        let messageFields = JSON.parse(error.message);
        messageFields = _.sortBy(messageFields, (item: any) => {
          return item.type;
        });
        const message = messageFields
          .map((field: any) => {
            return `${this.translate.get(field.type.toLowerCase())}: ${
              field.name
            }`;
          })
          .join('<br>');
        this.auiNotificationService.error(
          this.translate.get(error.code) + '<br>' + message,
        );
      }
      this.deleting = false;
    }
  }

  onQuotaCardClicked(quota: QuotaTypes) {
    this.quotaName = quota.name;
    this.percent = ((quota.used / quota.hard) * 100).toFixed(2);
    this.sidePanel.open({
      position: 'bottom',
      size: 200,
      resizable: true,
      floating: true,
      templateRef: this.chargeTemplateRef,
      backdrop: true,
      customStyle: {
        background: '#fff',
        padding: 0,
      },
    });
    this.activeQuotaCard = quota.name;
    this.refetch();
  }

  getCssStatus = () =>
    this.space && this.QUOTA_SPACE_STATUS_TO_CSS_STATUS[this.space.status];
}
