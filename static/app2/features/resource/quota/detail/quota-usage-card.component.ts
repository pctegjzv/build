import { Component, Inject, Input, OnInit } from '@angular/core';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { GaugeChartConfig } from 'app2/features/dashboard/charts/ngx-charts.types';
import { ResourceService } from 'app2/shared/services/features/resource.service';
import { QuotaTypes } from 'app2/shared/services/features/resource.service';

@Component({
  selector: 'rc-quota-usage-card',
  templateUrl: './quota-usage-card.component.html',
  styleUrls: ['./quota-usage-card.component.scss'],
})
export class QuotaUsageCardComponent implements OnInit {
  @Input()
  usage: QuotaTypes;
  @Input()
  active = false;
  unit: string;
  percentage: any;
  titleLabel: string;
  usedDescLabel: string;
  cssState: string;
  data: [
    {
      name: string;
      value: number;
    }
  ];
  chartConfig: GaugeChartConfig = {
    startAngle: 0,
    angleSpan: 360,
    scheme: { domain: ['#83E39F'] },
    showAxis: false,
  };

  QUOTA_TITLE_LABELS = {
    'service.mem': 'quota_service_mem',
    'service.cpu': 'quota_service_cpu',
  };

  QUOTA_USED_DESC_LABELS = {
    'service.mem': 'quota_mem_used',
    'service.cpu': 'quota_cpu_used',
  };

  constructor(
    @Inject(ACCOUNT) public account: RcAccount,
    private resourceService: ResourceService,
  ) {}

  async ngOnInit() {
    this.unit =
      'unit_' +
      (await this.resourceService.getQuotaConfig()).find(
        config => config.name === this.usage.name,
      ).unit;
    this.updateState();
  }

  onChanges(usage: QuotaTypes) {
    if (usage && usage.currentValue) {
      this.updateState();
    }
  }

  updateState() {
    if (!this.usage) {
      return;
    }
    this.percentage = this.usage.hard
      ? ((this.usage.used / this.usage.hard) * 100).toFixed(2)
      : 0;
    this.titleLabel = this.QUOTA_TITLE_LABELS[this.usage.name];
    this.usedDescLabel = this.QUOTA_USED_DESC_LABELS[this.usage.name];
    this.cssState = this.usage.used >= this.usage.hard ? 'error' : 'success';
    this.data = [
      {
        name: this.titleLabel,
        value: parseFloat(this.percentage),
      },
    ];
  }
}
