import { Component, Input, OnInit, Optional } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { ResourceService } from 'app2/shared/services/features/resource.service';
import { Space } from 'app2/shared/services/features/resource.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import * as _ from 'lodash';
import { first } from 'rxjs/operators';

import {
  QUOTA_SPACE_STATUS_TO_CSS_STATUS,
  QUOTA_SPACE_STATUS_TO_TRANSLATION_KEYS,
} from '../../rb-quota-space-constant';
import { QuotaListComponent } from '../list/quota-list.component';
import { QuotaUpdateDialogComponent } from '../update/quota-update-dialog.component';

@Component({
  selector: 'rc-quota-list-card',
  templateUrl: './quota-list-card.component.html',
  styleUrls: ['./quota-list-card.component.scss'],
})
export class QuotaListCardComponent implements OnInit {
  @Input()
  space: Space;
  createInstanceEnabled: boolean;
  loading = false;
  deleting = false;
  getStatusCss = () =>
    this.space && QUOTA_SPACE_STATUS_TO_CSS_STATUS[this.space.status];
  getStatusText = () =>
    this.space && QUOTA_SPACE_STATUS_TO_TRANSLATION_KEYS[this.space.status];

  constructor(
    @Optional() public quotaList: QuotaListComponent,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
    private resourceService: ResourceService,
    private roleUtilities: RoleUtilitiesService,
    private modalService: ModalService,
    private router: Router,
  ) {}

  ngOnInit() {}

  async editClicked() {
    if (this.deleting) {
      return;
    }

    const modelRef = this.modalService.open(QuotaUpdateDialogComponent, {
      title: this.translateService.get('update'),
      width: 900,
    });

    modelRef.componentInstance.isNew = false;
    modelRef.componentInstance.setModel(this.space);
    modelRef.componentInstance.finished.pipe(first()).subscribe(() => {
      modelRef.close();
    });
  }

  async deleteClicked() {
    if (this.deleting) {
      return;
    }
    await this.modalService.confirm({
      title: this.translateService.get('delete'),
      content: this.translateService.get('quota_delete_confirm_info', {
        name: this.space.name,
      }),
    });
    this.deleting = true;
    try {
      await this.resourceService.deleteSpace(this.space.name);
      this.quotaList.loadQuotaSpaces();
    } catch (err) {
      const error = err.errors[0];
      if ('child_resources_exist' === error.code) {
        // code 'child_resources_exist' used for resource delete (when sub resources exist)
        let messageFields = JSON.parse(error.message);
        messageFields = _.sortBy(messageFields, (item: any) => {
          return item.type;
        });
        const message = messageFields
          .map((field: any) => {
            return `${this.translateService.get(field.type.toLowerCase())}: ${
              field.name
            }`;
          })
          .join('<br>');
        this.auiNotificationService.error(
          this.translateService.get(error.code) + '<br>' + message,
        );
      }
      this.deleting = false;
    }
  }

  buttonDisplayExpr(action: string) {
    return this.roleUtilities.resourceHasPermission(
      this.space,
      'space',
      action,
    );
  }

  gotoDetail() {
    this.router.navigateByUrl(`quota/detail/${this.space.name}`);
  }
}
