import { Component, OnInit } from '@angular/core';
import { ResourceService } from 'app2/shared/services/features/resource.service';
import { Space } from 'app2/shared/services/features/resource.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import * as _ from 'lodash';
import { first } from 'rxjs/operators';

import { QuotaUpdateDialogComponent } from '../update/quota-update-dialog.component';

@Component({
  templateUrl: './quota-list.component.html',
  styleUrls: ['./quota-list.component.scss'],
})
export class QuotaListComponent implements OnInit {
  initialized = false;
  filterKey = '';
  createEnabled = false;
  loading = false;
  list: Array<any> = [];
  async ngOnInit() {
    this.loadQuotaSpaces();
    this.initialized = true;
    this.createEnabled = await this.roleUtilities.resourceTypeSupportPermissions(
      'space',
    );
  }

  constructor(
    private resourceService: ResourceService,
    private translateService: TranslateService,
    private roleUtilities: RoleUtilitiesService,
    private modalService: ModalService,
  ) {}

  /**
   * Load quota spaces
   */
  loadQuotaSpaces(): void {
    // init query
    if (this.loading) {
      return;
    }
    this.loading = true;

    // start query quota spaces
    this.resourceService
      .getSpaces('view')
      .then(data => {
        this.loading = false;
        const unsortedList: Array<any> = data.result || [];
        this.list = _.orderBy(unsortedList, ['created_at'], ['desc']);
      })
      .catch(() => {
        this.loading = false;
      });
  }

  showCreateDialog(): void {
    const modelRef = this.modalService.open(QuotaUpdateDialogComponent, {
      title: this.translateService.get('quota_space_create'),
      width: 900,
    });
    modelRef.componentInstance.isNew = true;
    modelRef.componentInstance.finished.pipe(first()).subscribe(res => {
      modelRef.close();
      if (res) {
        this.loadQuotaSpaces();
      }
    });
  }

  shouldHideSpace(space: Space) {
    return !space.name.includes(this.filterKey);
  }
}
