import { Component, Inject, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { RouterUtilService } from 'app2/core/services/router-util.service';
import { ACCOUNT, WEBLABS } from 'app2/core/tokens';
import { RcAccount, Weblabs } from 'app2/core/types';
import {
  QuotaResourceType,
  ResourceService,
  ResourceType,
} from 'app2/shared/services/features/resource.service';
import { delay } from 'app2/shared/services/utility/delay';
import { LoggerUtilitiesService } from 'app2/shared/services/utility/logger.service';
import { TranslateService } from 'app2/translate/translate.service';
import { groupBy } from 'lodash';

import {
  ORDERED_RESOURCES,
  RESOURCE_TYPE_STATE_MAP,
} from '../../rb-quota-space-constant';

@Component({
  selector: 'rc-quota-resource-list',
  templateUrl: './quota-resource-list.component.html',
  styleUrls: ['./quota-resource-list.component.scss'],
})
export class QuotaResourceListComponent implements OnInit, OnDestroy {
  @Input()
  name: string;
  initialized = false;
  destroyed = false;
  filter = '';
  selectedResources: QuotaResourceType[];
  displayedResources: QuotaResourceType[];
  resources: object;
  resourceTypes: ResourceType[];
  selectedType: string;
  constructor(
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(WEBLABS) public weblabs: Weblabs,
    private resourceService: ResourceService,
    private logger: LoggerUtilitiesService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private router: Router,
    private routerUtil: RouterUtilService,
  ) {}

  async ngOnInit() {
    this.startPolling();
    this.resourceTypes = [];
  }

  async ngOnDestroy() {
    this.destroyed = true;
  }

  _destroy() {
    this.destroyed = true;
  }

  async startPolling() {
    try {
      await this.refetch();
    } catch (err) {
      this.logger.error('Failed to poll detail');
    }

    await delay(300 * 1000);
    if (this._shouldPoll()) {
      this.startPolling();
    }
  }

  _shouldPoll() {
    return !this.destroyed;
  }

  async refetch() {
    try {
      const resources = await this.resourceService.getSpaceResources(this.name);
      this.groupResourceByType(resources);
    } catch ({ data }) {
      if (data && data.errors && data.errors[0].code === 'resource_not_exist') {
        this.auiNotificationService.error(
          this.translate.get('quota_space_not_exist'),
        );
        this.router.navigateByUrl('quota');
      }
    }

    this.initialized = true;
  }

  groupResourceByType(resources: QuotaResourceType[]) {
    this.resources = groupBy(resources, item => {
      return item.type;
    });
    this.resourceTypes = Object.entries(this.resources)
      .map(([type, values]) => ({
        name: type,
        count: values.length,
      }))
      .sort(
        (a, b) =>
          ORDERED_RESOURCES.indexOf(a.name) - ORDERED_RESOURCES.indexOf(b.name),
      );
    if (this.resourceTypes.length) {
      this.selectedType = this.resourceTypes[0]['name'];
      this.displayedResources = this.selectedResources = this.resources[
        this.selectedType
      ];
    }
  }

  onResourceTypeSelect(type: string) {
    let clearFlag = false;
    if (this.selectedType !== type) {
      clearFlag = true;
    }
    this.selectedType = type;
    this.selectedResources = this.resources[type];
    if (clearFlag) {
      this.filter = '';
      this.displayedResources = this.selectedResources;
    }
  }

  shouldShowResourceLink(item: QuotaResourceType) {
    return Object.keys(RESOURCE_TYPE_STATE_MAP).includes(item.type);
  }

  getResourceUrl(item: QuotaResourceType) {
    const param = {};
    if (item.type === 'APPLICATION_TEMP') {
      this.router.navigateByUrl(`/app_service/template/detail/${item.uuid}`);
    } else if (item.type === 'ENVFILE') {
      this.router.navigateByUrl(`/app_service/envfile/detail/${item.uuid}`);
    } else if (item.type === 'DASHBOARD') {
      this.router.navigateByUrl(
        `/monitor/dashboard/detail/${item.uuid}?display_name=${
          item.name
        }&crumbParams=display_name`,
      );
    } else if (item.type === 'ALARM') {
      this.router.navigate(['/alarm/alarm_detail', item.uuid]);
    } else if (item.type === 'CONFIG') {
      this.router.navigateByUrl(
        `/app_service/configuration/detail?name=${item.uuid}`,
      );
    } else if (item.type === 'PIPELINE') {
      this.router.navigateByUrl(`/pipeline/config/detail?name=${item.uuid}`);
    } else if (item.type === 'BUILDS_CONFIG') {
      this.router.navigateByUrl('/build/config/detail', {
        queryParams: { name: item.uuid },
      });
    } else if (item.type === 'JOB_CONFIG') {
      this.router.navigateByUrl(`/job/config/detail/${item.uuid}`);
    } else if (item.type === 'LOG_ALARM') {
      this.router.navigate(['/alarm/log_alarm_detail', item.uuid]);
    } else if (item.type === 'SUBNET') {
      this.router.navigateByUrl(`subnet/detail/${item.name}`);
    } else if (item.type === 'JENKINS_PIPELINE') {
      this.router.navigate(['/jenkins/pipelines', item.uuid]);
    } else if (item.type === 'SYNC_REGIS_CONF') {
      this.router.navigateByUrl('/image/sync-center/detail/' + item.uuid);
    } else if (item.type === 'INTEGRATION') {
      this.router.navigate(['/integration_center/detail', item.uuid]);
    } else if (item.type === 'NOTIFICATION') {
      this.router.navigate(['/notification/detail', item.uuid]);
    } else {
      const paramName = RESOURCE_TYPE_STATE_MAP[item.type]['paramName'];
      param[paramName] = this.isParamUseUuid(paramName) ? item.uuid : item.name;
      this.routerUtil.go(RESOURCE_TYPE_STATE_MAP[item.type]['state'], param);
    }
  }

  // 临时解决方法，duplicated_names_enabled当前只对service/application有效
  private isParamUseUuid(paramName: string) {
    return (
      paramName === 'uuid' ||
      [
        'envfileName',
        'service_name',
        'app_name',
        'notificationName',
        'templateName',
      ].includes(paramName)
    );
  }

  listFilter() {
    this.displayedResources = this.selectedResources.filter(
      (el: QuotaResourceType) => {
        return el.name.includes(this.filter);
      },
    );
  }
}
