import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import {
  QuotaConfigMaped,
  QuotaTypes,
  ResourceService,
  Space,
  UnitLabels,
} from 'app2/shared/services/features/resource.service';
import { DefaultErrorMapperService } from 'app2/shared/services/form/default-error-mapper.service';
import { ErrorResponse } from 'app2/shared/services/http/http.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { ErrorMapper } from 'app2/shared/types';
import { TranslateService } from 'app2/translate/translate.service';
import * as _ from 'lodash';

import { RESOURCE_NAME_BASE } from '../../../../../app/components/common/config/common-pattern';
import { QUOTA_TYPE_LABELS } from '../../rb-quota-space-constant';

@Component({
  selector: 'rc-quota-update-dialog',
  templateUrl: './quota-update-dialog.component.html',
  styleUrls: ['./quota-update-dialog.component.scss'],
})
export class QuotaUpdateDialogComponent implements OnInit {
  @Input()
  isNew = false;
  @Output()
  finished = new EventEmitter<any>();
  @ViewChild(NgForm)
  quotaUpdateForm: NgForm;

  submitting = false;
  model: Space;
  errorMapper: ErrorMapper;
  unitLabels: UnitLabels;
  quotaTypes: QuotaConfigMaped[];
  resourceNameBaseRegExp: any;

  constructor(
    @Inject(ACCOUNT) private account: RcAccount,
    private auiNotificationService: NotificationService,
    private errorsToastService: ErrorsToastService,
    private translate: TranslateService,
    private defaultErrorMapper: DefaultErrorMapperService,
    private resourceService: ResourceService,
    private router: Router,
  ) {}

  async ngOnInit() {
    this.model = this.model || {
      name: '',
      description: '',
    };
    if (!this.model.quotas) {
      this.model.quotas = [];
    }
    this.quotaTypes = [];
    this.resourceNameBaseRegExp = RESOURCE_NAME_BASE;
    if (
      this.model &&
      this.model.name !== '' &&
      this.model.quotas.length === 0
    ) {
      try {
        this.model = await this.resourceService.getSpace(this.model.name);
        if (!this.model.quotas) {
          this.model.quotas = [];
        }
      } catch (e) {
        this.cancel();
        this.router.navigateByUrl('quota');
        this.auiNotificationService.success(
          this.translate.get('quota_space_not_exist'),
        );
        return;
      }
    }
    this.unitLabels = {};

    await this.resourceService
      .getQuotaConfig()
      .then(res => {
        this.quotaTypes = res.map(config => ({
          name: config.name,
          label: this.translate.get(QUOTA_TYPE_LABELS[config.name]),
          unitLabel: this.translate.get('unit_' + config.unit),
        }));
        this.quotaTypes.forEach(type => {
          this.unitLabels[type.name] = type.unitLabel;
        });
      })
      .catch(() => {});

    this.errorMapper = {
      map: this.errorMap.bind(this),
    };
  }

  get namespace() {
    return this.account.namespace;
  }

  setModel(model: Space) {
    this.model = _.cloneDeep(model);
  }

  cancel() {
    this.complete();
  }

  formValid() {
    return this.quotaUpdateForm.valid && !this.hasDuplicatedQuotaTypes();
  }

  async submit() {
    if (this.quotaUpdateForm.valid && !this.submitting) {
      this.submitting = true;
      try {
        const res = await (this.isNew
          ? this.resourceService.createSpace(this.model)
          : this.resourceService.updateSpace(this.model));
        this.complete(res);
      } catch (e) {
        if (e instanceof ErrorResponse) {
          this.errorsToastService.handleGenericAjaxError({
            errors: e.errors,
            handleNonGenericCodes: true,
          });
        }
      }
      this.submitting = false;
    }
  }

  private complete(res?: any) {
    this.finished.next(res);
    this.finished.complete();
  }

  private errorMap(key: string, error: any): string {
    switch (key) {
      case 'pattern':
        return this.translate.get('resource_name_pattern_invalid');
      default:
        return this.defaultErrorMapper.map(key, error);
    }
  }

  hasDuplicatedQuotaTypes() {
    return (
      this.model &&
      this.model.quotas &&
      new Set(this.model.quotas.map(quota => quota.name)).size <
        this.model.quotas.length
    );
  }

  addRow() {
    this.model.quotas.push({
      name: 'service.mem',
    });
  }

  deleteRow(index: number) {
    this.model.quotas.splice(index, 1);
  }

  canAdd() {
    return this.model.quotas.length < this.quotaTypes.length;
  }

  quotaConfigOptionChanged(option: QuotaConfigMaped, row: QuotaTypes) {
    row.name = option.name;
  }
}
