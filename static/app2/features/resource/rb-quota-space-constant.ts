// To decorate the status bar in the list view
export const QUOTA_SPACE_STATUS_TO_CSS_STATUS = {
  running: 'success',
  updating: 'in-progress',
  failed: 'error',
};

export const QUOTA_SPACE_STATUS_TO_TRANSLATION_KEYS = {
  running: 'quota_space_running',
  updating: 'quota_space_updating',
  failed: 'quota_space_failed',
};

export const QUOTA_TYPE_LABELS = {
  'service.mem': 'quota_service_mem',
  'service.cpu': 'quota_service_cpu',
};

export const RESOURCE_TYPE_STATE_MAP = {
  ALARM: {
    state: 'monitor.alarm.detail',
    paramName: 'alarmName',
  },
  APPLICATION: {
    state: 'app_service.app.app_detail',
    paramName: 'app_name',
  },
  APPLICATION_TEMP: {
    state: 'app_service.template.detail',
    paramName: 'templateName',
  },
  BUILDS_CONFIG: {
    state: 'build.config.detail',
    paramName: 'name',
  },
  CONFIG: {
    state: 'app_service.configuration.detail',
    paramName: 'name',
  },
  DASHBOARD: {
    state: 'monitor.dashboard.state_dashboard_detail',
    paramName: 'uuid', // uuid
  },
  ENVFILE: {
    state: 'app_service.envfile.detail',
    paramName: 'envfileName',
  },
  // 'IP': {}
  JOB_CONFIG: {
    state: 'job.config.detail',
    paramName: 'name',
  },
  LOG_ALARM: {
    state: 'monitor/alarmv2/log_alarm_detail',
    paramName: 'uuid',
  },
  LOAD_BALANCER: {
    state: 'network.load_balancer.detail',
    paramName: 'name',
  },
  NOTIFICATION: {
    state: 'notification.detail',
    paramName: 'uuid',
  },
  PIPELINE: {
    state: 'pipeline.config.detail',
    paramName: 'name',
  },
  JENKINS_PIPELINE: {
    // state: 'jenkins.pipelines',
    // paramName: 'id',
  },
  // 'PRIVATE_IP': {}, //private_ip has no detail page
  SERVICE: {
    state: 'app_service.service.service_detail',
    paramName: 'service_name',
  },
  SUBNET: {
    state: 'network.subnet.detail',
    paramName: 'subnet_name', // TODO: region_name ??
  },
  // 'SNAPSHOT': {}, // snapshot has no detail page
  SYNC_REGIS_CONF: {
    state: 'image.sync-center.detail',
    paramName: 'name',
  },
  VOLUME: {
    state: 'storage.volume.detail',
    paramName: 'uuid', // uuid
  },
  INTEGRATION: {
    state: 'integration_center.detail',
    paramName: 'uuid',
  },
};

export const ORDERED_RESOURCES = [
  'APPLICATION',
  'SERVICE',
  'APPLICATION_TEMP',
  'ENVFILE',
  'CONFIG',
  'JOB_CONFIG',
  'SYNC_REGIS_CONF',
  'BUILDS_CONFIG',
  'PIPELINE',
  'JENKINS_PIPELINE',

  'VOLUME',
  'LOAD_BALANCER',
  'IP',
  'PRIVATE_IP',
  'SUBNET',

  'DASHBOARD',
  'LOG_FILTER',
  'NOTIFICATION',
  'ALARM',
  'LOG_ALARM',
  'INTEGRATION',

  'CERTIFICATE',
];

export const RESOURCE_CONSTANT = {
  RESOURCE_TYPE_STATE_MAP,
};
