import { NgModule } from '@angular/core';
import { ServiceUsageRoutingModule } from 'app2/features/resource/service-usage.routing.module';
import { SharedModule } from 'app2/shared/shared.module';

import { ServiceUsageListComponent } from './service_usage/service-usage-list.component';

@NgModule({
  imports: [SharedModule, ServiceUsageRoutingModule],
  declarations: [ServiceUsageListComponent],
})
export class ServiceUsageModule {}
