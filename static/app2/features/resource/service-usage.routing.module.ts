import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ServiceUsageListComponent } from './service_usage/service-usage-list.component';

const routes: Routes = [
  {
    path: '',
    component: ServiceUsageListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class ServiceUsageRoutingModule {}
