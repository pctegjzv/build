import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import * as _ from 'lodash';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { SingleSelectionDropdownComponent } from 'app2/shared/components/select/single-selection-dropdown.component';
import {
  ResourceService,
  ServiceData,
} from 'app2/shared/services/features/resource.service';
import { LoggerUtilitiesService } from 'app2/shared/services/utility/logger.service';
import * as FileSaver from 'file-saver';

@Component({
  templateUrl: './service-usage-list.component.html',
  styleUrls: ['./service-usage-list.component.scss'],
})
export class ServiceUsageListComponent implements OnInit {
  exporting = false;
  loading = false;
  namespace: string;
  usage = [
    'service_region_name',
    'service_name',
    'service_id',
    'instance_size',
    'running_time_in_hours',
  ];
  singleOptions: any = [];
  keyword = '';
  month = '';
  selDate = '';
  initialList: Array<ServiceData> = [];
  filteredList: Array<ServiceData> = [];
  @ViewChild(SingleSelectionDropdownComponent)
  singleDropdown: SingleSelectionDropdownComponent;

  constructor(
    @Inject(ACCOUNT) public account: RcAccount,
    private resourceService: ResourceService,
    private logger: LoggerUtilitiesService,
  ) {}

  async ngOnInit() {
    this.namespace = this.account.namespace;
    this.initDatePicker();
  }

  private initDatePicker() {
    let date = new Date();
    let dateTemplate = null;
    const dateArray = [];
    for (let i = 0; i < 6; i++) {
      const mon = date.getMonth() + 1;
      if (mon < 10) {
        dateTemplate = `${date.getFullYear()}-0${mon}`;
      } else {
        dateTemplate = `${date.getFullYear()}-${mon}`;
      }
      dateArray.push(dateTemplate);
      date = preMonth(date);
    }
    this.singleOptions = dateArray;
    setTimeout(
      () => this.singleDropdown.setSelectedValue(this.singleOptions[0]),
      100,
    );
    function preMonth(date = new Date()) {
      return new Date(date.setMonth(date.getMonth() - 1));
    }
  }

  onSingleOptionChange(value: any) {
    this.getData(value);
  }

  private async getData(date: string) {
    this.month = date;
    if (this.selDate === date) {
      return;
    }
    try {
      this.loading = true;
      const data = await this.resourceService.getServiceUsageData(this.month);
      this.selDate = _.get(data, 'usage.month');
      const usage = data;
      const services = _.get(usage, 'resources.service', []);

      // Service usage data is grouped by region. We need to flatten them into a single array
      this.initialList = _.flatMap(services, service => {
        const usageListByRegion: any[] = _.get(service, 'data', []);
        const regionName = _.get(service, 'region.name');
        return usageListByRegion.map(usage => ({
          ...usage,
          region_name: regionName,
        }));
      });
      this.listFilter();
    } catch (e) {
      this.initialList = [];
      this.filteredList = [];
      this.logger.error('Failed to load service usage list', e);
    }
    this.loading = false;
  }

  listFilter() {
    this.filteredList = this.initialList.filter((el: ServiceData) => {
      return [el.region_name, el.service_name].find(it =>
        it.includes(this.keyword),
      );
    });
  }

  exportServiceUsage() {
    this.exporting = true;
    this.resourceService
      .getServiceUsageExportData(this.month)
      .then(data => {
        this.exporting = false;
        FileSaver.saveAs(data, this.namespace + '-' + this.month + '.xls');
      })
      .catch(() => {
        this.exporting = false;
      });
  }
}
