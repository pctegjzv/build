import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmType, DialogService, NotificationService } from 'alauda-ui';
import {
  GenericRSRC,
  RSRCManagementService,
  ResourceType,
} from 'app2/shared/services/features/resrc-manage.service';
import { TranslateService } from 'app2/translate/translate.service';
import { createActions, updateActions } from 'app2/utils/code-editor-config';
import { safeDump, safeLoad, safeLoadAll } from 'js-yaml';
import { get } from 'lodash';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'rc-resource-management-list',
  templateUrl: './create-page.component.html',
  styleUrls: ['./create-page.component.scss'],
})
export class ResourceManagementCreatePageComponent
  implements OnInit, OnDestroy {
  edtiorActions = createActions;
  editorOptions = {
    language: 'yaml',
    readOnly: false,
    renderLineHighlight: 'none',
  };
  yamlInputValue = '';
  originalYaml = '';
  resourceData: GenericRSRC;
  update: boolean;
  resourceType: ResourceType;
  updatePayload: GenericRSRC['kubernetes'];
  createPayload: any[];

  @ViewChild('form')
  form: NgForm;

  constructor(
    private router: Router,
    private dialogService: DialogService,
    private translate: TranslateService,
    private RSRCService: RSRCManagementService,
    private auiNotificationService: NotificationService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.RSRCService.currentResouceData$
      .pipe(filter(data => !!data))
      .subscribe(data => this.onViewChange(data));
    this.RSRCService.currentResourceType$
      .pipe(filter(type => !!type))
      .subscribe(type => (this.resourceType = type));
  }

  ngOnDestroy(): void {
    this.RSRCService.setCurrentResourceData(null);
  }

  private onViewChange(data: GenericRSRC) {
    this.update = true;
    this.resourceData = data;
    this.yamlInputValue = safeDump(data.kubernetes, { lineWidth: 9999 });
    this.originalYaml = this.yamlInputValue;
    this.edtiorActions = Object.assign({}, updateActions);
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }

    try {
      if (!this.update) {
        this.createPayload = safeLoadAll(this.yamlInputValue);
      } else {
        this.updatePayload = safeLoad(this.yamlInputValue);
      }
    } catch (error) {
      this.auiNotificationService.error(
        error.message || this.translate.get('yaml_format_hint'),
      );
      return;
    }

    this.dialogService
      .confirm({
        title: this.translate.get(
          this.update
            ? 'confirm_update_resource_content'
            : 'confirm_create_resource_content',
        ),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        confirmType: ConfirmType.Primary,
        beforeConfirm: this.update
          ? this.updateResource()
          : this.createResource(),
      })
      .then(() => {
        this.router.navigate(['../list'], { relativeTo: this.route });
      })
      .catch(() => null);
  }

  cancel() {
    if (this.yamlInputValue) {
      this.dialogService
        .confirm({
          title: this.translate.get(
            this.update
              ? 'cancel_resource_update_content'
              : 'cancel_resource_create_content',
          ),
          confirmText: this.translate.get('confirm'),
          cancelText: this.translate.get('cancel'),
          confirmType: ConfirmType.Warning,
        })
        .then(() => {
          this.router.navigate(['../list'], { relativeTo: this.route });
        })
        .catch(() => null);
    } else {
      this.router.navigate(['../list'], { relativeTo: this.route });
    }
  }

  private createResource() {
    return (resolve: () => void, reject: () => void) => {
      this.RSRCService.createResource({
        data: this.createPayload,
      })
        .then(() => resolve())
        .catch(err => {
          reject();
          this.auiNotificationService.error(
            get(err, 'errors[0].message', this.translate.get('create_failed')),
          );
        });
    };
  }

  private updateResource() {
    return (resolve: () => void, reject: () => void) => {
      this.RSRCService.updateResource({
        resourceTypeName: this.resourceType.name,
        resourceName: this.resourceData.kubernetes.metadata.name,
        data: this.updatePayload,
        namespace: this.resourceData.kubernetes.metadata.namespace || '',
      })
        .then(() => resolve())
        .catch(err => {
          reject();
          this.auiNotificationService.error(
            get(err, 'errors[0].message', this.translate.get('update_failed')),
          );
        });
    };
  }
}
