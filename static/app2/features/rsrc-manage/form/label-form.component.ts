import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Directive,
  Inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  NgForm,
  Validator,
} from '@angular/forms';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { DefaultErrorMapperService } from 'app2/shared/services/form/default-error-mapper.service';
import { TranslateService } from 'app2/translate/translate.service';
import { cloneDeep, includes } from 'lodash';
import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'rc-resource-label-form',
  templateUrl: './label-form.component.html',
  styleUrls: ['./label-form.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: ResourceLabelFormComponent,
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: ResourceLabelFormComponent,
      multi: true,
    },
  ],
})
export class ResourceLabelFormComponent
  implements OnInit, OnDestroy, AfterViewInit, ControlValueAccessor {
  private _value: any;
  @ViewChild('form')
  form: NgForm;
  private formSubscription: Subscription;
  omitedValue: Array<{ key: string; value: string }>;
  onChange: (_: any) => void;
  onTouch: (_: any) => void;

  labelsMap: Array<{ key: string; value: string }>;
  keyDuplicated: boolean;

  valueReg = {
    pattern: /^(([A-Za-z0-9][-A-Za-z0-9_.]*)?[A-Za-z0-9])?$/,
    tip: 'regexp_tip_resource_label_key',
  };

  errorMappers = {
    key: {
      map: (key: string, error: any) => {
        switch (key) {
          case 'pattern':
            return this.translateService.get('invalid_pattern');
          case 'namePattern':
            return this.translateService.get(
              'regexp_tip_resource_label_key_name',
            );
          case 'domainPattern':
            return this.translateService.get(
              'regexp_tip_resource_label_key_domain',
            );
          case 'duplicated':
            return this.translateService.get('node_tags_key_unique');
          default:
            return this.defaultErrorMapperService.map(key, error);
        }
      },
    },
    value: {
      map: (key: string, error: any) => {
        switch (key) {
          case 'pattern':
            return this.translateService.get('regexp_tip_resource_label_value');
          default:
            return this.defaultErrorMapperService.map(key, error);
        }
      },
    },
  };

  constructor(
    @Inject(ENVIRONMENTS) private env: Environments,
    private parentForm: NgForm,
    private translateService: TranslateService,
    private cdr: ChangeDetectorRef,
    private defaultErrorMapperService: DefaultErrorMapperService,
  ) {
    this.labelsMap = [];
    this.omitedValue = [];
  }

  ngOnInit(): void {}

  ngAfterViewInit() {
    this.parentForm.ngSubmit.subscribe((e: any) => this.form.onSubmit(e));
    this.formSubscription = this.form.valueChanges
      .pipe(debounceTime(50))
      .subscribe(() => {
        const _value = cloneDeep(this.labelsMap).concat(this.omitedValue);
        this._value = _value.reduce((accum, curr) => {
          accum[curr.key] = curr.value;
          return accum;
        }, {});
        this.emitToModel();
        this.cdr.markForCheck();
      });
  }

  ngOnDestroy(): void {
    this.formSubscription.unsubscribe();
  }

  validate(_control: AbstractControl): { [key: string]: any } | null {
    return this.form.invalid ? { 'resource-label-form': true } : null;
  }

  trackByFn(index: number) {
    return index;
  }

  onKeyChange(currentkey: string) {
    this.keyDuplicated =
      this.labelsMap.filter(item => item.key === currentkey).length > 1;
  }

  /**
   * Native <=> _Value <=> Model
   */
  private applyToNative() {
    if (this._value === null || this._value === undefined) {
      return;
    }
    this.omitedValue = Object.keys(this._value)
      .filter(key => includes(key, this.env.label_base_domain))
      .map(key => {
        return {
          key,
          value: this._value[key],
        };
      });
    this.labelsMap = Object.keys(this._value)
      .filter(key => !includes(key, this.env.label_base_domain))
      .map(key => {
        return {
          key,
          value: this._value[key],
        };
      });
    this.cdr.markForCheck();
  }

  private emitToModel() {
    this.onChange(this._value);
  }

  /* implement `ControlValueAccessor` interface */
  writeValue(value: any) {
    this._value = value;
    this.applyToNative();
  }

  registerOnChange(fn: (_: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: (_: any) => void): void {
    this.onTouch = fn;
  }
}

@Directive({
  selector: 'input[rcResourceLabelValid]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: ResourceLabelUniqueValidatorDirective,
      multi: true,
    },
  ],
})
export class ResourceLabelUniqueValidatorDirective
  implements Validator, OnChanges {
  @Input()
  keyDuplicated: boolean;
  private _onChange: () => void;

  prefixRe = /^[a-z0-9][-a-z0-9]{0,62}(\.[a-z0-9][-a-z0-9]{0,62})+$/;
  nameRe = /^(([A-Za-z0-9][-A-Za-z0-9_.]*)?[A-Za-z0-9])?$/;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.keyDuplicated && this._onChange) {
      this._onChange();
    }
  }

  registerOnValidatorChange(fn: () => void): void {
    this._onChange = fn;
  }

  validate(control: AbstractControl): { [key: string]: any } {
    return this.uniqueValidator(control);
  }

  uniqueValidator(control: AbstractControl): { [key: string]: any } {
    if (!control.value) {
      return null;
    }

    const keyPartition = control.value.split('/');
    if (keyPartition.length > 2) {
      return { pattern: true };
    }

    const prefixTest =
      keyPartition.length === 2 ? this.prefixRe.test(keyPartition[0]) : true;
    const nameTest =
      keyPartition.length === 1
        ? this.nameRe.test(keyPartition[0])
        : this.nameRe.test(keyPartition[1]);

    if (!prefixTest) {
      return { domainPattern: true };
    }

    if (!nameTest) {
      return { namePattern: true };
    }

    return this.keyDuplicated ? { duplicated: { value: control.value } } : null;
  }
}
