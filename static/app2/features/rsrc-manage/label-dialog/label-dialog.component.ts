import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DialogRef, NotificationService } from 'alauda-ui';
import {
  GenericRSRC,
  RSRCManagementService,
  ResourceType,
} from 'app2/shared/services/features/resrc-manage.service';
import { TranslateService } from 'app2/translate/translate.service';
import { get } from 'lodash';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'rc-resource-label-dialog',
  templateUrl: './label-dialog.component.html',
  styleUrls: ['./label-dialog.component.scss'],
})
export class ResourceLabelDialogComponent implements OnInit {
  @Input()
  kubernetes: any = {};
  @Input()
  title: string;

  @ViewChild('form')
  form: NgForm;
  _loading: boolean;
  resourceData: GenericRSRC;
  resourceType: ResourceType;
  requestData: GenericRSRC['kubernetes'];

  get loading() {
    return this._loading;
  }

  constructor(
    private dialogRef: DialogRef,
    private rsrcManagementService: RSRCManagementService,
    private auiNotification: NotificationService,
    private translate: TranslateService,
  ) {}

  ngOnInit(): void {
    this.rsrcManagementService.currentResourceType$
      .pipe(filter(type => !!type))
      .subscribe(type => (this.resourceType = type));
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    try {
      this._loading = true;
      await this.rsrcManagementService.updateResource({
        resourceTypeName: this.resourceType.name,
        resourceName: this.kubernetes.metadata.name,
        namespace: this.kubernetes.metadata.namespace || '',
        data: this.kubernetes,
      });
      this.auiNotification.success(this.translate.get('update_success'));
      this._loading = false;
      this.dialogRef.close(true);
    } catch (err) {
      this._loading = false;
      this.auiNotification.error(
        get(err, 'errors[0].message', this.translate.get('create_failed')),
      );
    }
  }
}
