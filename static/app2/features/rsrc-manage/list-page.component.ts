import { HttpClient } from '@angular/common/http';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  Optional,
  TemplateRef,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ConfirmType,
  DialogService,
  DialogSize,
  NotificationService,
} from 'alauda-ui';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { ResourceLabelDialogComponent } from 'app2/features/rsrc-manage/label-dialog/label-dialog.component';
import { RegionService } from 'app2/shared/services/features/region.service';
import {
  Category,
  GenericRSRC,
  RSRCManagementService,
  ResourceManagementListFetchParams,
  ResourceType,
} from 'app2/shared/services/features/resrc-manage.service';
import { TranslateService } from 'app2/translate/translate.service';
import { viewActions } from 'app2/utils/code-editor-config';
import { PageParams, ResourceList } from 'app_user/core/types';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { BaseResourceListComponent } from 'app_user/shared/abstract/base-resource-list.component';
import { safeDump } from 'js-yaml';
import { find, get, includes, omitBy } from 'lodash';
import { BehaviorSubject, Observable, Subject, Subscription, from } from 'rxjs';
import { filter, map, switchMap, takeUntil } from 'rxjs/operators';

export interface ColumnDef {
  name: string;
  label?: string;
  sortable?: boolean;
  sortStart?: 'asc' | 'desc';
}

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
  },
  {
    name: 'label',
    label: 'label',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
  },
  {
    name: 'namespace',
    label: 'namespace',
  },
  {
    name: 'action',
  },
];

@Component({
  selector: 'rc-resource-management-list',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceManagementListPageComponent
  extends BaseResourceListComponent<GenericRSRC>
  implements OnInit, AfterViewInit, OnDestroy {
  private onDestroy$ = new Subject<void>();
  fetchParams$: Observable<ResourceManagementListFetchParams>;
  regionContext$: Subject<boolean> = new BehaviorSubject<boolean>(false);
  columnDefs: ColumnDef[];
  columns$: Observable<string[]>;
  initialized = false;
  listSubscription: Subscription;
  regionName: string;

  expandedMap = {
    namespaced: false,
    region: false,
  };

  activePath = '';
  yamlInputValue = '';

  namespacedKinds: string[] = [];
  regionKinds: string[] = [];

  category: Category = {
    namespaced: [],
    region: [],
  };

  edtiorActions = viewActions;
  editorOptions = {
    language: 'yaml',
    readOnly: true,
    renderLineHighlight: 'none',
  };

  currentResourceType: ResourceType;

  userView: boolean;

  /* implements methods of base class */
  map(value: ResourceList) {
    return value.results;
  }

  fetchResources(
    params: ResourceManagementListFetchParams,
  ): Observable<ResourceList> {
    return from(this.RSRCService.getResourceList(params));
  }

  get navZeroState() {
    return !this.category.namespaced.length && !this.category.region.length;
  }

  get navLoading() {
    return this.RSRCService.navLoading;
  }

  constructor(
    @Inject(ENVIRONMENTS) private env: Environments,
    http: HttpClient,
    router: Router,
    activatedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    private dialogService: DialogService,
    private translateService: TranslateService,
    private RSRCService: RSRCManagementService,
    private auiNotificationService: NotificationService,
    private regionService: RegionService,
    @Optional() private workspaceComponent: WorkspaceComponent,
  ) {
    super(http, router, activatedRoute, cdr);
  }

  ngOnInit(): void {
    if (this.RSRCService.isUserView()) {
      this.userView = true;
      this.workspaceComponent.baseParams
        .pipe(takeUntil(this.onDestroy$))
        .subscribe(params => {
          this.RSRCService.initScope(params);
          this.RSRCService.initResourceTypeList$();
        });
    } else {
      this.regionService.region$
        .pipe(
          takeUntil(this.onDestroy$),
          filter(cluster => !!cluster),
        )
        .subscribe(cluster => {
          this.RSRCService.initScope(cluster);
          this.RSRCService.initResourceTypeList$();
        });
    }

    this.RSRCService.currentResourceType$.subscribe((type: ResourceType) => {
      this.currentResourceType = type;
    });

    this.RSRCService.category$.subscribe((category: Category) => {
      this.category = category;
      this.initNav();
      this.initColumns$();
      this.refetch();
    });

    super.ngOnInit();

    this.list$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(list => !!list),
      )
      .subscribe((_list: GenericRSRC[]) => {
        this.initialized = true;
      });
  }

  initNav() {
    if (!this.currentResourceType.kind) {
      this.expandedMap['namespaced'] = true;
      this.activePath = 'namespaced';
    } else {
      const isNamespacedKind = !!find(this.category.namespaced, [
        'kind',
        this.currentResourceType.kind,
      ]);
      this.expandedMap = {
        namespaced: isNamespacedKind,
        region: !isNamespacedKind,
      };
      this.activePath = isNamespacedKind ? 'namespaced' : 'region';
      this.regionContext$.next(isNamespacedKind ? false : true);
    }
  }

  initColumns$() {
    this.columnDefs = this.getColumnDefs();
    const allColumns = this.columnDefs.map(colDef => colDef.name);
    const columnsWithoutNamespace = allColumns.filter(
      column => column !== 'namespace',
    );
    this.columns$ = this.regionContext$.pipe(
      map(
        regionContext =>
          regionContext || this.RSRCService.isUserView()
            ? columnsWithoutNamespace
            : allColumns,
      ),
    );
  }

  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }

  ngAfterViewInit() {}

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  /**
   * Init fetching params to be used when fetching list result.
   */
  initFetchParams$() {
    this.fetchParams$ = this.RSRCService.currentResourceType$.pipe(
      filter(type => !!type.name),
      switchMap(currentResourceType => {
        return this.getDefaultFetchParams$().pipe(
          map(fetchParams => ({
            ...fetchParams,
            currentResourceType,
          })),
        );
      }),
    );
    return this.fetchParams$;
  }

  getLabelValue(resource: any) {
    return omitBy(get(resource, 'kubernetes.metadata.labels', {}), (_v, k) =>
      includes(k, this.env.label_base_domain),
    );
  }

  viewResource(resource: any, templateRef: TemplateRef<any>) {
    this.yamlInputValue = safeDump(resource.kubernetes, { lineWidth: 9999 });
    this.dialogService.open(templateRef, {
      size: DialogSize.Large,
      data: {
        name: resource.kubernetes.metadata.name,
      },
    });
  }

  /**
   * Actions
   */
  updateAction(item: GenericRSRC) {
    this.RSRCService.setCurrentResourceData(item);
    this.router.navigate(['../update'], { relativeTo: this.activedRoute });
  }

  updateLabelAction(item: { kubernetes: any; resource_actions: any }) {
    const dialogRef = this.dialogService.open(ResourceLabelDialogComponent);
    dialogRef.componentInstance.title = this.translateService.get(
      'labels_update',
    );
    dialogRef.componentInstance.kubernetes = item.kubernetes;
    dialogRef.afterClosed().subscribe(result => !!result && this.refetch());
  }

  deleteAction(item: GenericRSRC) {
    this.dialogService
      .confirm({
        title: this.translateService.get('confirm_delete_resource_content', {
          kind: this.currentResourceType.kind,
          name: item.kubernetes.metadata.name,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
        confirmType: ConfirmType.Danger,
        beforeConfirm: (resolve, reject) => {
          this.RSRCService.deleteResouce({
            resourceTypeName: this.currentResourceType.name,
            resourceName: item.kubernetes.metadata.name,
            namespace: item.kubernetes.metadata.namespace || '',
          })
            .then(() => {
              resolve();
              this.auiNotificationService.success(
                this.translateService.get('delete_success'),
              );
            })
            .catch(err => {
              reject();
              this.auiNotificationService.error(
                get(
                  err,
                  'errors[0].message',
                  this.translateService.get('delete_failed'),
                ),
              );
            });
        },
      })
      .then(() => {
        this.refetch();
      })
      .catch(() => null);
  }

  refetch() {
    this.onUpdate(null);
    this.cdr.markForCheck();
  }

  pageChanged(page: PageParams) {
    this.onPageEvent({
      pageIndex: ++page.pageIndex,
      pageSize: page.pageSize,
    });
  }

  /* Nav list */
  handleClick(path: string) {
    this.expandedMap[path] = !this.expandedMap[path];
    this.activePath = path;
  }

  onKindsClick(resourceType: ResourceType, regionContext: boolean) {
    this.initialized = false;
    this.RSRCService.setCurrentResourceType(resourceType);
    this.regionContext$.next(regionContext);
  }

  isPathActive(path: string) {
    return this.activePath.startsWith(path);
  }

  isPathExpanded(path: string) {
    return this.expandedMap[path];
  }
}
