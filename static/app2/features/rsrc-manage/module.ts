import { NgModule } from '@angular/core';
import { ResourceManagementCreatePageComponent } from 'app2/features/rsrc-manage/create-page.component';
import {
  ResourceLabelFormComponent,
  ResourceLabelUniqueValidatorDirective,
} from 'app2/features/rsrc-manage/form/label-form.component';
import { ResourceLabelDialogComponent } from 'app2/features/rsrc-manage/label-dialog/label-dialog.component';
import { ResourceManagementListPageComponent } from 'app2/features/rsrc-manage/list-page.component';
import { RSRCManagementRoutingModule } from 'app2/features/rsrc-manage/routing.module';
import { SharedModule } from 'app2/shared/shared.module';

@NgModule({
  imports: [SharedModule, RSRCManagementRoutingModule],
  declarations: [
    ResourceManagementListPageComponent,
    ResourceManagementCreatePageComponent,
    ResourceLabelDialogComponent,
    ResourceLabelFormComponent,
    ResourceLabelUniqueValidatorDirective,
  ],
  entryComponents: [ResourceLabelDialogComponent],
})
export class RSRCManagementModule {}
