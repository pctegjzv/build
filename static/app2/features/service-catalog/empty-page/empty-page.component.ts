import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Region } from 'app2/shared/services/features/region.service';
import { RoleService } from 'app2/shared/services/features/role.service';
import { get } from 'lodash';

@Component({
  selector: 'rc-service-catalog-empty-page',
  templateUrl: './empty-page.component.html',
  styleUrls: ['./empty-page.component.scss', '../service-catalog.common.scss'],
})
export class ServiceCatalogEmptyComponent implements OnInit {
  @Output()
  createBroker = new EventEmitter<void>();
  @Input()
  type: string;
  @Input()
  cluster: Region;
  showNotification = true;
  serviceCatalogCluster = false;
  permission: Array<string> = [];
  constructor(private roleService: RoleService) {}

  ngOnInit() {
    this.roleService
      .getPluralContextPermissions(['clusterservicebroker'])
      .then(res => {
        this.permission = res.clusterservicebroker;
      })
      .catch(() => {});
    this.serviceCatalogCluster = this.checkServiceFeatures(this.cluster);
  }

  refreshAction() {
    this.createBroker.next();
  }

  notiClose() {
    this.showNotification = false;
  }

  showNoti() {
    this.showNotification = true;
  }

  checkServiceFeatures(cluster: Region) {
    const featureTypes = get(cluster, 'features.service.features');
    if (!featureTypes) {
      return false;
    }
    return featureTypes.indexOf('service-catalog') !== -1;
  }
}
