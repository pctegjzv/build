import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'rc-loading-error-page',
  templateUrl: './loading-error-page.component.html',
  styleUrls: ['./loading-error-page.component.scss'],
})
export class ServiceCatalogLoadingErrorComponent {
  @Output()
  refresh = new EventEmitter<void>();
  constructor() {}

  refreshAction() {
    this.refresh.next();
  }
}
