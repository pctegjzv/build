import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'alauda-ui';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import {
  ServiceBrokerPayload,
  ServiceCatalogService,
} from 'app2/shared/services/features/service-catalog.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

import { K8S_RESOURCE_NAME_BASE } from '../../../../../app/components/common/config/common-pattern';

@Component({
  templateUrl: './service-broker-create.component.html',
  styleUrls: ['./service-broker-create.component.scss'],
})
export class ServiceBrokerCreateComponent implements OnInit {
  @ViewChild('serviceBrokerForm')
  form: NgForm;
  serviceBrokerPayload: ServiceBrokerPayload;
  submitting = false;
  regionLoading = false;
  cluster: Region;
  broker_display_name = '';

  resourceNameReg = K8S_RESOURCE_NAME_BASE;

  constructor(
    @Inject(ENVIRONMENTS) private environments: Environments,
    private regionService: RegionService,
    private router: Router,
    private catalogService: ServiceCatalogService,
    private auiMessageService: MessageService,
    private translateService: TranslateService,
    private modalService: ModalService,
    private errorsToastService: ErrorsToastService,
  ) {
    this.serviceBrokerPayload = {
      spec: {
        CABundle: '',
        authInfo: {},
        url: '',
      },
      metadata: {
        name: '',
        annotations: {
          [`clusterservicebroker.${
            this.environments.label_base_domain
          }/display_name`]: '',
        },
      },
    };
  }

  ngOnInit() {
    this.regionLoading = true;
    this.regionService
      .getCurrentRegion()
      .then(region => {
        this.cluster = region;
      })
      .catch();
  }

  async createBroker() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return false;
    }
    this.serviceBrokerPayload.metadata.annotations[
      `clusterservicebroker.${this.environments.label_base_domain}/display_name`
    ] = this.broker_display_name;
    this.submitting = true;
    try {
      await this.catalogService.createBroker(
        this.cluster.id,
        this.serviceBrokerPayload,
      );

      this.auiMessageService.success({
        content: this.translateService.get(
          'service_catalog_broker_create_success_title',
        ),
      });
      this.router.navigateByUrl(
        `service_catalog/service_broker/broker_detail/${
          this.serviceBrokerPayload.metadata.name
        }`,
      );
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
    this.submitting = false;
  }

  async cancelCreateBroker() {
    if (this.form.dirty) {
      try {
        await this.modalService.confirm({
          title: this.translateService.get(
            'service_catalog_broker_cancle_create_broker',
          ),
        });
      } catch (rejection) {
        return false;
      }
    }
    this.router.navigateByUrl('service_catalog/service_broker');
  }
}
