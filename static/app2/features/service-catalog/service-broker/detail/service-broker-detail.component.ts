import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { Subscription } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import { MessageService, NotificationService } from 'alauda-ui';
import { ServiceBrokerUpdateDialogComponent } from 'app2/features/service-catalog/service-broker/update-broker-dialog/service-broker-update-dialog.component';
import { ServiceCatalogUtilitiesService } from 'app2/features/service-catalog/service-catalog.utilities.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import {
  ServiceBroker,
  ServiceCatalogService,
} from 'app2/shared/services/features/service-catalog.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { get } from 'lodash';

@Component({
  templateUrl: './service-broker-detail.component.html',
  styleUrls: [
    './service-broker-detail.component.scss',
    '../../service-catalog.common.scss',
  ],
})
export class ServiceBrokerDetailComponent implements OnInit, OnDestroy {
  private routeParamsSubscription: Subscription;
  private name: string;
  private cluster: Region;
  private lastBrokerStatus: boolean;
  private regionSubscription: Subscription;

  brokerData: ServiceBroker;
  brokerClassList: any[] = [];
  _loading = false;
  _loadError: any;
  pageSize = 999;
  initialized = false;
  initializedData = false;
  destroyed = false;
  pollingTimer: any;

  constructor(
    @Inject(ENVIRONMENTS) private environments: Environments,
    private router: Router,
    private serviceCatalogUtilities: ServiceCatalogUtilitiesService,
    private modal: ModalService,
    private translateService: TranslateService,
    private route: ActivatedRoute,
    private catalogService: ServiceCatalogService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private regionService: RegionService,
    private errorsToastService: ErrorsToastService,
  ) {}

  ngOnInit() {
    this.routeParamsSubscription = this.route.params
      .pipe(first())
      .subscribe(async params => {
        this.name = params['name'];
        await this.refetch();
      });

    this.regionSubscription = this.regionService.region$
      .pipe(filter(region => !!region))
      .subscribe(() => {
        if (this.initialized) {
          this.router.navigateByUrl('service_catalog/service_broker');
        } else {
          this.initialized = true;
        }
      });
  }

  ngOnDestroy() {
    this.destroyed = true;
    this.routeParamsSubscription.unsubscribe();
    this.regionSubscription.unsubscribe();
  }

  async refetch() {
    this._loading = true;
    try {
      this.cluster = await this.regionService.getCurrentRegion();
      this.brokerData = await this.catalogService.getBroker(
        this.cluster.id,
        this.name,
      );
      if (this.brokerStatus !== this.lastBrokerStatus) {
        this.lastBrokerStatus = this.brokerStatus;
        const classes: any = await this.catalogService.getBrokerClasses({
          pageSize: this.pageSize,
          params: {
            fieldSelector: `spec.clusterServiceBrokerName=${
              this.brokerData.kubernetes.metadata.name
            }`,
            cluster_id: this.cluster.id,
          },
        });
        this.brokerClassList = classes.results;
      }
      this._loadError = false;
    } catch (errors) {
      if (errors.status === 403) {
        this.auiNotificationService.warning(
          this.translateService.get('permission_denied'),
        );
        return this.router.navigateByUrl('service_catalog/service_broker');
      } else if (errors.status === 404) {
        this.auiNotificationService.warning(
          this.translateService.get('service_catalog_broker_not_exist'),
        );
        return this.router.navigateByUrl('service_catalog/service_broker');
      }
      this._loadError = true;
      this.errorsToastService.error(errors);
    }
    this._loading = false;
    this.initializedData = true;
    this.resetPollingTimer();
  }

  get empty() {
    return !this.brokerData;
  }

  get loading() {
    return this._loading;
  }

  get loadError() {
    return this._loadError;
  }

  get brokerStatus() {
    if (
      this.brokerData &&
      this.brokerData.kubernetes.status.conditions.length !== 0
    ) {
      if (
        this.brokerData.kubernetes.status.conditions[
          this.brokerData.kubernetes.status.conditions.length - 1
        ].status === 'True'
      ) {
        return true;
      }
    }
    return false;
  }

  get display_name() {
    return (
      this.brokerData.kubernetes.metadata.annotations[
        `clusterservicebroker.${
          this.environments.label_base_domain
        }/display_name`
      ] || this.brokerData.kubernetes.metadata.name
    );
  }

  updateBroker() {
    const modelRef = this.modal.open(ServiceBrokerUpdateDialogComponent, {
      title: this.translateService.get(
        'service_catalog_template_upadte_service_broker',
      ),
      width: 700,
    });
    modelRef.componentInstance.brokerData = this.brokerData;
    modelRef.componentInstance.cluster = this.cluster;
    modelRef.componentInstance.finished.pipe(first()).subscribe(res => {
      if (res) {
        this.auiMessageService.success({
          content: this.translateService.get(
            'service_catalog_broker_update_success_title',
          ),
        });
        this.refetch();
      }
      modelRef.close();
    });
  }

  async deleteBroker() {
    try {
      const response = await this.serviceCatalogUtilities.deleteBroker(
        this.brokerData,
        this.cluster.id,
        this.display_name,
      );
      if (response) {
        this.router.navigateByUrl('service_catalog/service_broker');
      }
    } catch (err) {}
  }

  private resetPollingTimer() {
    clearTimeout(this.pollingTimer);
    if (this.destroyed) {
      return;
    }
    const hasDeployingBroker =
      get(this.brokerData, 'kubernetes.status.conditions', []).length === 0;
    const waitTime = hasDeployingBroker ? 5000 : 15000;
    this.pollingTimer = setTimeout(() => {
      this.refetch();
    }, waitTime);
  }
}
