import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { Subscription } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import { MessageService } from 'alauda-ui';
import { ServiceBrokerUpdateDialogComponent } from 'app2/features/service-catalog/service-broker/update-broker-dialog/service-broker-update-dialog.component';
import { ServiceCatalogUtilitiesService } from 'app2/features/service-catalog/service-catalog.utilities.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RoleService } from 'app2/shared/services/features/role.service';
import {
  ServiceBroker,
  ServiceCatalogService,
} from 'app2/shared/services/features/service-catalog.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import {
  Pagination,
  PaginationDataWrapper,
} from 'app2/shared/services/utility/pagination-data';
import { TranslateService } from 'app2/translate/translate.service';
import { get, some } from 'lodash';

@Component({
  templateUrl: './service-broker-list.component.html',
  styleUrls: [
    './service-broker-list.component.scss',
    '../../service-catalog.common.scss',
  ],
})
export class ServiceBrokerListComponent implements OnInit, OnDestroy {
  pageSize = 20;
  count = 0;
  paginationDataWrapper: PaginationDataWrapper<any>;
  paginationDataSubscription: Subscription;
  regionSubscription: Subscription;
  cluster: Region;
  brokerItems: ServiceBroker[] = [];
  permission: Array<string> = [];
  queryString = '';
  initialized = false;
  destroyed = false;
  pollingTimer: any;

  constructor(
    @Inject(ENVIRONMENTS) private environments: Environments,
    private router: Router,
    private serviceCatalogUtilities: ServiceCatalogUtilitiesService,
    private translateService: TranslateService,
    private modal: ModalService,
    private catalogService: ServiceCatalogService,
    private regionService: RegionService,
    private roleService: RoleService,
    private auiMessageService: MessageService,
  ) {
    const fetchRequest = (pageNo: number, pageSize: number, params: any) => {
      return this.catalogService.getBrokers({
        pageNo,
        pageSize,
        params,
      });
    };
    this.paginationDataWrapper = new PaginationDataWrapper(
      fetchRequest,
      this.pageSize,
    );
  }

  ngOnInit() {
    this.roleService
      .getPluralContextPermissions(['clusterservicebroker'])
      .then(res => {
        this.permission = res.clusterservicebroker;
      })
      .catch(() => {});
    this.paginationDataSubscription = this.paginationDataWrapper.pagination.subscribe(
      (paginationData: Pagination<any>) => {
        if (!paginationData.loadError) {
          this.count = paginationData.count;
          this.brokerItems = paginationData.results;
          this.brokerItems.map((item: any) => {
            const annotations = get(
              item,
              'kubernetes.metadata.annotations',
              '',
            );
            item.display_name = annotations
              ? annotations[
                  `clusterservicebroker.${
                    this.environments.label_base_domain
                  }/display_name`
                ] || ''
              : '';
            item.classes_num = annotations
              ? annotations[
                  `clusterservicebroker.${
                    this.environments.label_base_domain
                  }/classes_num`
                ] || 0
              : 0;
          });
          this.initialized = true;
        }
        this.resetPollingTimer();
      },
    );

    this.regionSubscription = this.regionService.region$
      .pipe(filter(region => !!region))
      .subscribe(region => {
        this.cluster = region;
        this.onRegionChanged(region.id);
      });
  }

  ngOnDestroy() {
    this.destroyed = true;
    this.paginationDataSubscription.unsubscribe();
    clearTimeout(this.pollingTimer);
  }

  refetch() {
    this.paginationDataWrapper.refetch();
  }

  get empty() {
    return !this.brokerItems || !this.brokerItems.length;
  }

  get loading() {
    return this.paginationDataWrapper.loading;
  }

  get loadError() {
    return this.paginationDataWrapper.loadError;
  }

  get currentPage() {
    return this.paginationDataWrapper.pageNo;
  }

  getBrokerStatus(rowData: ServiceBroker) {
    if (rowData.kubernetes.status.conditions.length === 0) {
      return 'BrokerCreating';
    } else if (
      rowData.kubernetes.status.conditions[
        rowData.kubernetes.status.conditions.length - 1
      ].status !== 'True'
    ) {
      return 'BrokerInvalid';
    } else {
      return 'BrokerNormal';
    }
  }

  searchChanged(queryString: string) {
    this.queryString = queryString;
    this.paginationDataWrapper.setParams({
      name: queryString,
    });
  }

  async onRegionChanged(cluster_id: string) {
    this.brokerItems = [];
    await this.paginationDataWrapper.setParams({
      cluster_id,
    });
  }

  createBroker() {
    this.router.navigateByUrl('service_catalog/service_broker/create_broker');
  }

  viewBroker(name: string) {
    this.router.navigateByUrl(
      `service_catalog/service_broker/broker_detail/${name}`,
    );
  }

  updateBroker(broker_name: string) {
    const modelRef = this.modal.open(ServiceBrokerUpdateDialogComponent, {
      title: this.translateService.get(
        'service_catalog_template_upadte_service_broker',
      ),
      width: 700,
    });
    modelRef.componentInstance.brokerName = broker_name;
    modelRef.componentInstance.cluster = this.cluster;
    modelRef.componentInstance.finished.pipe(first()).subscribe(res => {
      if (res) {
        this.auiMessageService.success({
          content: this.translateService.get(
            'service_catalog_broker_update_success_title',
          ),
        });
        this.refetch();
      }
      modelRef.close();
    });
  }

  async deleteBroker(broker: any) {
    let name = '';
    if (
      broker.kubernetes.metadata.annotations[
        `clusterservicebroker.${
          this.environments.label_base_domain
        }/display_name`
      ]
    ) {
      name =
        broker.kubernetes.metadata.annotations[
          `clusterservicebroker.${
            this.environments.label_base_domain
          }/display_name`
        ];
    } else {
      name = broker.kubernetes.metadata.name;
    }
    try {
      const response = await this.serviceCatalogUtilities.deleteBroker(
        broker,
        this.cluster.id,
        name,
      );
      if (response) {
        this.auiMessageService.success({
          content: this.translateService.get(
            'service_catalog_broker_delete_success_title',
          ),
        });
        this.refetch();
      }
    } catch (err) {}
  }

  pageChanged(page: number) {
    this.paginationDataWrapper.setPageNo(page);
  }

  trackByFn(index: number) {
    return index;
  }

  private resetPollingTimer() {
    clearTimeout(this.pollingTimer);
    if (this.destroyed) {
      return;
    }
    const hasDeployingBroker = some(
      this.brokerItems,
      (broker: ServiceBroker) => {
        return get(broker, 'kubernetes.status.conditions', []).length === 0;
      },
    );
    const waitTime = hasDeployingBroker ? 3000 : 10000;
    this.pollingTimer = setTimeout(() => {
      this.refetch();
    }, waitTime);
  }
}
