import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { Region } from 'app2/shared/services/features/region.service';
import {
  ServiceBroker,
  ServiceBrokerPayload,
  ServiceCatalogService,
} from 'app2/shared/services/features/service-catalog.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { cloneDeep } from 'lodash';

@Component({
  templateUrl: './service-broker-update-dialog.component.html',
  styleUrls: ['./service-broker-update-dialog.component.scss'],
})
export class ServiceBrokerUpdateDialogComponent implements OnInit {
  @ViewChild('serviceBrokerUpdateForm')
  form: NgForm;
  @Input()
  brokerData: ServiceBroker;
  @Input()
  brokerName = '';
  @Input()
  cluster: Region;
  @Output()
  finished = new EventEmitter<any>();
  updateBrokerPayload: ServiceBroker;
  loading = false;
  submitting = false;
  broker_display_name = '';

  constructor(
    @Inject(ENVIRONMENTS) private environments: Environments,
    private catalogService: ServiceCatalogService,
    private translateService: TranslateService,
    private errorsToastService: ErrorsToastService,
    private modalService: ModalService,
  ) {}

  async ngOnInit() {
    if (this.brokerName === '') {
      this.updateBrokerPayload = cloneDeep(this.brokerData);
      this.brokerName = this.brokerData.kubernetes.metadata.name;
    } else {
      this.loading = true;
      try {
        this.updateBrokerPayload = await this.catalogService.getBroker(
          this.cluster.id,
          this.brokerName,
        );
        this.broker_display_name =
          this.updateBrokerPayload.kubernetes.metadata.annotations[
            `clusterservicebroker.${
              this.environments.label_base_domain
            }/display_name`
          ] || '';
      } catch (errors) {
        this.errorsToastService.error(errors);
      }
      this.loading = false;
    }
    if (!this.updateBrokerPayload.kubernetes.spec.authInfo) {
      this.updateBrokerPayload.kubernetes.spec.authInfo = {
        basicAuthUsernameKey: '',
        basicAuthPasswordKey: '',
      };
    }
  }

  async updateBroker() {
    this.submitting = true;
    try {
      const body: ServiceBrokerPayload = {
        spec: {
          CABundle: this.updateBrokerPayload.kubernetes.spec.CABundle || '',
          authInfo: {
            basicAuthUsernameKey: this.updateBrokerPayload.kubernetes.spec
              .authInfo.basicAuthUsernameKey,
            basicAuthPasswordKey: this.updateBrokerPayload.kubernetes.spec
              .authInfo.basicAuthPasswordKey,
          },
          url: this.updateBrokerPayload.kubernetes.spec.url,
        },
        metadata: {
          name: this.updateBrokerPayload.kubernetes.metadata.name,
          resourceVersion: this.updateBrokerPayload.kubernetes.metadata
            .resourceVersion,
          annotations: {
            [`clusterservicebroker.${
              this.environments.label_base_domain
            }/display_name`]: this.broker_display_name,
          },
        },
      };
      await this.catalogService.updateBroker(
        body,
        this.cluster.id,
        this.brokerName,
      );
      this.complete(true);
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
    this.submitting = false;
  }

  async cancelUpdateBroker() {
    if (this.form.dirty) {
      try {
        await this.modalService.confirm({
          title: this.translateService.get(
            'service_catalog_broker_cancle_update_broker',
          ),
        });
      } catch (rejection) {
        return false;
      }
    }
    this.complete();
  }

  private complete(res?: any) {
    this.finished.next(res);
    this.finished.complete();
  }
}
