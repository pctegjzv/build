import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ServiceBrokerCreateComponent } from './service-broker/create/service-broker-create.component';
import { ServiceBrokerDetailComponent } from './service-broker/detail/service-broker-detail.component';
import { ServiceBrokerListComponent } from './service-broker/list/service-broker-list.component';
import { ServiceClassDetailComponent } from './service-class/detail/service-class-detail.component';
import { ServiceClassListComponent } from './service-class/list/service-class-list.component';
import { ServiceInstanceDetailComponent } from './service-instance/detail/service-instance-detail.component';
import { ServiceInstanceListComponent } from './service-instance/list/service-instance-list.component';

const routes: Routes = [
  {
    path: 'service_broker',
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        component: ServiceBrokerListComponent,
      },
      {
        path: 'create_broker',
        component: ServiceBrokerCreateComponent,
      },
      {
        path: 'broker_detail/:name',
        component: ServiceBrokerDetailComponent,
      },
    ],
  },
  {
    path: 'service_class',
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        component: ServiceClassListComponent,
      },
      {
        path: 'class_detail/:name',
        component: ServiceClassDetailComponent,
      },
    ],
  },
  {
    path: 'service_instance',
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        component: ServiceInstanceListComponent,
      },
      {
        path: 'instance_detail/:namespace/:name',
        component: ServiceInstanceDetailComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ServiceCatalogRoutingModule {}
