import { NgModule } from '@angular/core';
import { ServiceCatalogServiceSharedModule } from 'app2/features/lazy/service_catalog-service.shared.module';

import { SharedModule } from '../../shared/shared.module';

import { ServiceCatalogEmptyComponent } from './empty-page/empty-page.component';
import { ServiceCatalogLoadingErrorComponent } from './loading-error-page/loading-error-page.component';
import { ServiceBrokerCreateComponent } from './service-broker/create/service-broker-create.component';
import { ServiceBrokerDetailComponent } from './service-broker/detail/service-broker-detail.component';
import { ServiceBrokerListComponent } from './service-broker/list/service-broker-list.component';
import { ServiceBrokerUpdateDialogComponent } from './service-broker/update-broker-dialog/service-broker-update-dialog.component';
import { ServiceCatalogRoutingModule } from './service-catalog-routing.module';
import { ServiceCatalogUtilitiesService } from './service-catalog.utilities.service';
import { ServiceCatalogTemplateCardComponent } from './service-class/card/service-class-card.component';
import { ServiceClassDetailComponent } from './service-class/detail/service-class-detail.component';
import { ServiceClassListComponent } from './service-class/list/service-class-list.component';
import { ServiceInstanceBindingAppDialogComponent } from './service-instance/binding-app-dialog/binding-app-dialog.component';
import { ServiceInstanceDetailComponent } from './service-instance/detail/service-instance-detail.component';
import { ServiceInstanceListComponent } from './service-instance/list/service-instance-list.component';
import { ServiceInstanceUpdateParamsDialogComponent } from './service-instance/update-params-dialog/update-params-dialog.component';

@NgModule({
  imports: [
    SharedModule,
    ServiceCatalogServiceSharedModule,
    ServiceCatalogRoutingModule,
  ],
  declarations: [
    ServiceBrokerListComponent,
    ServiceBrokerCreateComponent,
    ServiceBrokerDetailComponent,
    ServiceBrokerUpdateDialogComponent,
    ServiceCatalogEmptyComponent,
    ServiceClassListComponent,
    ServiceClassDetailComponent,
    ServiceCatalogTemplateCardComponent,
    ServiceInstanceListComponent,
    ServiceInstanceDetailComponent,
    ServiceInstanceUpdateParamsDialogComponent,
    ServiceInstanceBindingAppDialogComponent,
    ServiceCatalogLoadingErrorComponent,
  ],
  entryComponents: [
    ServiceBrokerUpdateDialogComponent,
    ServiceInstanceUpdateParamsDialogComponent,
    ServiceInstanceBindingAppDialogComponent,
  ],
  providers: [ServiceCatalogUtilitiesService],
})
export class ServiceCatalogModule {}
