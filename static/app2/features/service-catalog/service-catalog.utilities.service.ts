import { Injectable } from '@angular/core';

import { NotificationService } from 'alauda-ui';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ServiceCatalogService } from 'app2/shared/services/features/service-catalog.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';

@Injectable()
export class ServiceCatalogUtilitiesService {
  constructor(
    private modalService: ModalService,
    private serviceCatalogService: ServiceCatalogService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private roleUtilities: RoleUtilitiesService,
  ) {}
  showBrokerUpdate(serviceCatalog: any) {
    return this.roleUtilities.resourceHasPermission(
      serviceCatalog,
      'clusterservicebroker',
      'update',
    );
  }

  showBrokerDelete(serviceCatalog: any) {
    return this.roleUtilities.resourceHasPermission(
      serviceCatalog,
      'clusterservicebroker',
      'delete',
    );
  }

  showInstanceUpdate(serviceCatalog: any) {
    return this.roleUtilities.resourceHasPermission(
      serviceCatalog,
      'serviceinstance',
      'update',
    );
  }

  showInstanceDelete(serviceCatalog: any) {
    return this.roleUtilities.resourceHasPermission(
      serviceCatalog,
      'serviceinstance',
      'delete',
    );
  }

  async deleteBroker(broker: any, cluster_id: string, name: string) {
    await this.modalService.confirm({
      title: this.translate.get('service_catalog_broker_delete_confirm_title', {
        name: name,
      }),
      content: this.translate.get(
        'service_catalog_broker_delete_confirm_content',
        {
          name: name,
        },
      ),
    });
    try {
      await this.serviceCatalogService.deleteBroker(
        cluster_id,
        broker.kubernetes.metadata.name,
      );
      return true;
    } catch (rejection) {
      if (rejection && rejection.errors) {
        this.auiNotificationService.error({
          title: this.translate.get('service_catalog_execution_failed'),
          content: rejection.errors[0].message,
        });
      } else if (rejection && rejection.message) {
        this.auiNotificationService.error({
          title: this.translate.get('service_catalog_execution_failed'),
          content: rejection.message,
        });
        return false;
      }
    }
  }

  async deleteInstance(cluster_id: string, instance: any, listNum: number) {
    const name = instance.kubernetes.metadata.name;
    if (listNum === 0) {
      try {
        await this.modalService.confirm({
          title: this.translate.get(
            'service_catalog_broker_delete_confirm_title',
            {
              name: name,
            },
          ),
        });
      } catch (errors) {
        return false;
      }
      try {
        await this.serviceCatalogService.deleteInstance(
          cluster_id,
          name,
          instance.kubernetes.metadata.namespace,
        );
        return true;
      } catch (rejection) {
        if (rejection && rejection.errors) {
          this.auiNotificationService.error({
            title: this.translate.get('service_catalog_execution_failed'),
            content: rejection.errors[0].message,
          });
        } else if (rejection && rejection.message) {
          this.auiNotificationService.error({
            title: this.translate.get('service_catalog_execution_failed'),
            content: rejection.message,
          });
          return false;
        }
      }
    } else {
      await this.modalService.alert({
        title: this.translate.get(
          'service_catalog_instance_cant_delete_title',
          {
            name: name,
          },
        ),
        content: this.translate.get(
          'service_catalog_instance_cant_delete_content',
          {
            name: name,
          },
        ),
        confirmText: 'service_catalog_konw',
      });
    }
  }
}
