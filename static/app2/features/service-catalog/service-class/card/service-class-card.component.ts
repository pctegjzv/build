import { Component, Inject, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { ServiceClass } from 'app2/shared/services/features/service-catalog.service';

@Component({
  selector: 'rc-service-catalog-class-card',
  templateUrl: 'service-class-card.component.html',
  styleUrls: [
    'service-class-card.component.scss',
    '../../service-catalog.common.scss',
  ],
})
export class ServiceCatalogTemplateCardComponent implements OnInit {
  @Input()
  class: ServiceClass;

  warm_tips = '';

  constructor(
    @Inject(ENVIRONMENTS) private environments: Environments,
    private router: Router,
  ) {}

  ngOnInit() {
    if (!this.class.kubernetes.metadata.annotations) {
      this.class.kubernetes.metadata.annotations = {};
    }
    this.class.status = this.class.kubernetes.metadata.annotations[
      `clusterservicebroker.${this.environments.label_base_domain}/status`
    ];
  }

  seeMore() {
    this.router.navigateByUrl(
      `service_catalog/service_class/class_detail/${
        this.class.kubernetes.metadata.name
      }`,
    );
  }
}
