import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { Subscription } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import { MessageService, NotificationService } from 'alauda-ui';
import {
  NamespaceOption,
  NamespaceService,
} from 'app2/shared/services/features/namespace.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RoleService } from 'app2/shared/services/features/role.service';
import {
  ServiceBindingPayload,
  ServiceCatalogService,
  ServiceClass,
  ServiceInstancePayload,
  ServicePlan,
} from 'app2/shared/services/features/service-catalog.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './service-class-detail.component.html',
  styleUrls: [
    './service-class-detail.component.scss',
    '../../service-catalog.common.scss',
  ],
})
export class ServiceClassDetailComponent implements OnInit, OnDestroy {
  parseInt = parseInt;
  @ViewChild('serviceInstanceForm')
  form: NgForm;
  private regionSubscription: Subscription;
  private routeParamsSubscription: Subscription;

  pageSize = 999;
  _loading = false;
  _loadError: any;
  bingdingAppLoading = false;
  isCreateInstance = false;
  submitting = false;
  initialized = false;

  namespaceOptions: NamespaceOption[];
  bingdingAppOptions: any[];
  instanceCreateParameters: any[];
  instanceBindingParameters: any[];
  instanceCreateParametersRequiredList: string[] = [];
  instanceBindingParametersRequiredList: string[] = [];
  permission: Array<string> = [];
  selectedPlan: ServicePlan;
  classData: ServiceClass;
  name: string;
  serviceInstancePayload: ServiceInstancePayload;
  bindingPayload: ServiceBindingPayload;
  binding_app_uuid = '';
  cluster: Region;

  constructor(
    @Inject(ENVIRONMENTS) private environments: Environments,
    private regionService: RegionService,
    private namespaceService: NamespaceService,
    private route: ActivatedRoute,
    private router: Router,
    private catalogService: ServiceCatalogService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
    private serviceService: ServiceService,
    private modalService: ModalService,
    private roleService: RoleService,
    private errorsToastService: ErrorsToastService,
  ) {
    this.serviceInstancePayload = {
      metadata: {
        name: '',
        namespace: '',
      },
      spec: {
        clusterServiceClassExternalName: '',
        clusterServicePlanExternalName: '',
        parameters: {},
      },
    };
    this.bindingPayload = {
      metadata: {
        namespace: '',
        labels: {
          [`service.${this.environments.label_base_domain}/uuid`]: '',
        },
      },
      spec: {
        parameters: {},
        instanceRef: {
          name: '',
        },
      },
    };
  }

  async ngOnInit() {
    this.routeParamsSubscription = this.route.params
      .pipe(first())
      .subscribe(async params => {
        this.name = params['name'];
        await this.refetch();
      });

    this.regionSubscription = this.regionService.region$
      .pipe(filter(region => !!region))
      .subscribe(() => {
        if (this.initialized) {
          this.router.navigateByUrl('service_catalog/service_class');
        } else {
          this.initialized = true;
        }
      });
  }

  ngOnDestroy() {
    this.regionSubscription.unsubscribe();
    this.routeParamsSubscription.unsubscribe();
  }

  async refetch() {
    this._loading = true;
    try {
      this.regionSubscription = await this.regionService.region$
        .pipe(filter(r => !!r))
        .subscribe(async region => {
          this.cluster = region;
          this.namespaceOptions = await this.namespaceService.getNamespaceOptions(
            region.id,
          );
        });
      this.classData = await this.catalogService.getClass(
        this.cluster.id,
        this.name,
      );
      this.selectedPlan = this.classData.clusterserviceplans[0];
      this.roleService
        .getPluralContextPermissions(['serviceinstance'])
        .then(res => {
          this.permission = res.serviceinstance;
        })
        .catch(() => {
          this.permission = [];
        });
      this._loadError = false;
    } catch (errors) {
      this._loadError = true;
      if (errors.status === 403) {
        this.auiNotificationService.warning(
          this.translateService.get('permission_denied'),
        );
        return this.router.navigateByUrl('service_catalog/service_class');
      } else if (errors.status === 404) {
        this.auiNotificationService.warning(
          this.translateService.get('service_catalog_class_not_exist'),
        );
        return this.router.navigateByUrl('service_catalog/service_class');
      }
      this.errorsToastService.error(errors);
    }
    this._loading = false;
  }

  get empty() {
    return !this.classData;
  }

  get loading() {
    return this._loading;
  }

  get loadError() {
    return this._loadError;
  }

  get brokerStatus() {
    if (this.classData && this.classData.kubernetes.metadata.annotations) {
      if (
        this.classData.kubernetes.metadata.annotations[
          `clusterservicebroker.${this.environments.label_base_domain}/status`
        ] === 'True'
      ) {
        return true;
      }
    }
    return false;
  }

  selectPlan(index: number) {
    this.selectedPlan = this.classData.clusterserviceplans[index];
  }

  getParams() {
    if (this.selectedPlan.spec.instanceCreateParameterSchema) {
      this.instanceCreateParameters = [];
      if (this.selectedPlan.spec.instanceCreateParameterSchema.properties) {
        this.instanceCreateParameters = Object.entries(
          this.selectedPlan.spec.instanceCreateParameterSchema.properties,
        );
      }
      if (this.selectedPlan.spec.instanceCreateParameterSchema.required) {
        this.instanceCreateParametersRequiredList = this.selectedPlan.spec.instanceCreateParameterSchema.required;
      } else {
        this.instanceCreateParametersRequiredList = [];
      }
    }
    if (this.selectedPlan.spec.serviceBindingCreateParameterSchema) {
      this.instanceBindingParameters = [];
      if (
        this.selectedPlan.spec.serviceBindingCreateParameterSchema.properties
      ) {
        this.instanceBindingParameters = Object.entries(
          this.selectedPlan.spec.serviceBindingCreateParameterSchema.properties,
        );
      }
      if (this.selectedPlan.spec.serviceBindingCreateParameterSchema.required) {
        this.instanceBindingParametersRequiredList = this.selectedPlan.spec.serviceBindingCreateParameterSchema.required;
      }
    }
  }

  handleType(type: string) {
    return this.catalogService.handleJSONSchemaType(type);
  }

  instanceParamRequired(paramName: string) {
    return this.instanceCreateParametersRequiredList.indexOf(paramName) !== -1;
  }

  bindingParamRequired(paramName: string) {
    return this.instanceBindingParametersRequiredList.indexOf(paramName) !== -1;
  }

  confirmServicePlan() {
    this.getParams();
    this.isCreateInstance = true;
  }

  async createInstance() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return false;
    }
    this.submitting = true;
    this.serviceInstancePayload.spec.clusterServiceClassExternalName = this.classData.kubernetes.spec.externalName;
    this.serviceInstancePayload.spec.clusterServicePlanExternalName = this.selectedPlan.spec.externalName;
    try {
      await this.catalogService.createInstance(
        this.serviceInstancePayload,
        this.cluster.id,
      );
      if (this.binding_app_uuid !== '') {
        this.createBinding();
      }
      this.auiMessageService.success({
        content: this.translateService.get(
          'service_catalog_instance_create_success_title',
        ),
      });
      this.router.navigateByUrl(
        `service_catalog/service_instance/instance_detail/${
          this.serviceInstancePayload.metadata.namespace
        }/${this.serviceInstancePayload.metadata.name}`,
      );
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
    this.submitting = false;
  }

  async createBinding() {
    this.bindingPayload.metadata.labels[
      `service.${this.environments.label_base_domain}/uuid`
    ] = this.binding_app_uuid;
    this.bindingPayload.spec.instanceRef.name = this.serviceInstancePayload.metadata.name;
    this.bindingPayload.metadata.namespace = this.serviceInstancePayload.metadata.namespace;
    try {
      await this.catalogService.createBinding(
        this.bindingPayload,
        this.cluster.id,
      );
      this.auiMessageService.success({
        content: this.translateService.get(
          'service_catalog_binding_success_title',
        ),
      });
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
  }

  async cancelCreateInstance() {
    if (this.form.dirty) {
      try {
        await this.modalService.confirm({
          title: this.translateService.get(
            'service_catalog_confirm_cancel_create_instance',
          ),
        });
      } catch (rejection) {
        return false;
      }
    }
    this.reset();
  }

  reset() {
    this.isCreateInstance = false;
    this.binding_app_uuid = '';
    this.serviceInstancePayload = {
      metadata: {
        name: '',
        namespace: '',
      },
      spec: {
        clusterServiceClassExternalName: '',
        clusterServicePlanExternalName: '',
        parameters: {},
      },
    };
    this.bindingPayload = {
      metadata: {
        namespace: '',
        labels: {
          [`service.${this.environments.label_base_domain}/uuid`]: '',
        },
      },
      spec: {
        parameters: {},
        instanceRef: {
          name: '',
        },
      },
    };
  }

  onNamespaceOptionChange(option: any) {
    this.serviceInstancePayload.metadata.namespace = option.name;
    this.getBindingApps(option);
  }

  onBindingAppOptionChange(option: any) {
    this.binding_app_uuid = option.uuid;
  }

  async getBindingApps(namespace: any) {
    this.bingdingAppOptions = [];
    this.bingdingAppLoading = true;
    try {
      const res = await this.serviceService.getK8sServices({
        pageSize: this.pageSize,
        params: {
          cluster: this.cluster.name,
          namespace: namespace.name,
        },
      });
      this.bingdingAppOptions = res.results.map((result: any) => {
        return {
          name: result.resource.name,
          uuid: result.resource.uuid,
        };
      });
      this.bingdingAppOptions.unshift({
        name: this.translateService.get('service_catalog_not_bind_app'),
        uuid: '',
      });
    } catch (err) {}
    this.bingdingAppLoading = false;
  }
}
