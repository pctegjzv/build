import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import {
  ServiceBrokerListItem,
  ServiceCatalogService,
  ServiceClass,
} from 'app2/shared/services/features/service-catalog.service';
import {
  Pagination,
  PaginationDataWrapper,
} from 'app2/shared/services/utility/pagination-data';

@Component({
  templateUrl: './service-class-list.component.html',
  styleUrls: [
    './service-class-list.component.scss',
    '../../service-catalog.common.scss',
  ],
})
export class ServiceClassListComponent implements OnInit {
  pageSize = 999;
  count = 0;
  paginationDataWrapper: PaginationDataWrapper<ServiceClass>;
  paginationDataSubscription: Subscription;
  regionSubscription: Subscription;
  classItems: ServiceClass[] = [];
  display_classItems: ServiceClass[] = [];
  brokerList: ServiceBrokerListItem[] = [];
  cluster: Region;

  constructor(
    @Inject(ENVIRONMENTS) private environments: Environments,
    private catalogService: ServiceCatalogService,
    private regionService: RegionService,
    private router: Router,
  ) {
    const fetchRequest = (pageNo: number, pageSize: number, params: any) => {
      return this.catalogService.getClasses({
        pageNo,
        pageSize,
        params,
      });
    };
    this.paginationDataWrapper = new PaginationDataWrapper(
      fetchRequest,
      this.pageSize,
    );
  }

  ngOnInit() {
    this.paginationDataSubscription = this.paginationDataWrapper.pagination.subscribe(
      (paginationData: Pagination<ServiceClass>) => {
        if (!paginationData.loadError) {
          this.count = paginationData.count;
          this.classItems = paginationData.results;
          const broker_name_list: string[] = [];
          this.brokerList = [];
          this.classItems.forEach(item => {
            if (
              broker_name_list.indexOf(
                item.kubernetes.spec.clusterServiceBrokerName,
              ) === -1
            ) {
              broker_name_list.push(
                item.kubernetes.spec.clusterServiceBrokerName,
              );
              this.brokerList.push({
                name: item.kubernetes.spec.clusterServiceBrokerName,
                display_name:
                  item.kubernetes.metadata.annotations[
                    `clusterservicebroker.${
                      this.environments.label_base_domain
                    }/display_name`
                  ] || '',
                status:
                  item.kubernetes.metadata.annotations[
                    `clusterservicebroker.${
                      this.environments.label_base_domain
                    }/status`
                  ] || '',
              });
            }
          });
          this.display_classItems = this.classItems.slice(0);
        }
      },
    );

    this.regionSubscription = this.regionService.region$
      .pipe(filter(region => !!region))
      .subscribe(region => {
        this.cluster = region;
        this.onRegionChanged(region.id);
      });
  }

  refetch() {
    this.paginationDataWrapper.refetch();
  }

  get empty() {
    return !this.classItems || !this.classItems.length;
  }

  get loading() {
    return this.paginationDataWrapper.loading;
  }

  get loadError() {
    return this.paginationDataWrapper.loadError;
  }

  searchChanged(queryString: string) {
    this.display_classItems = this.classItems.filter(item => {
      return item.kubernetes.spec.externalName.indexOf(queryString) !== -1;
    });
  }

  async onRegionChanged(cluster_id: string) {
    this.classItems = [];
    await this.paginationDataWrapper.setParams({
      cluster_id,
    });
  }

  createBroker() {
    this.router.navigateByUrl('service_catalog/service_broker/create_broker');
  }
}
