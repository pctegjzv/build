import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { MessageService } from 'alauda-ui';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { Region } from 'app2/shared/services/features/region.service';
import {
  ServiceBindingPayload,
  ServiceCatalogService,
} from 'app2/shared/services/features/service-catalog.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './binding-app-dialog.component.html',
  styleUrls: [
    './binding-app-dialog.component.scss',
    '../../service-catalog.common.scss',
  ],
})
export class ServiceInstanceBindingAppDialogComponent implements OnInit {
  parseInt = parseInt;
  @ViewChild('instanceCreateBindingForm')
  form: NgForm;
  @Input()
  instanceDetailData: any;
  @Input()
  name = '';
  @Input()
  namespace = '';
  @Input()
  cluster: Region;
  @Output()
  finished = new EventEmitter<any>();
  pageSize = 999;
  loading = false;
  submitting = false;
  bingdingAppLoading = false;
  bindingPayload: ServiceBindingPayload;
  binding_app_uuid: string;
  bingdingAppOptions: any;
  bindingCreateParameters: any;
  bindingPlaceholder: string;

  constructor(
    @Inject(ENVIRONMENTS) private environments: Environments,
    private catalogService: ServiceCatalogService,
    private translateService: TranslateService,
    private auiMessageService: MessageService,
    private serviceService: ServiceService,
    private errorsToastService: ErrorsToastService,
  ) {
    this.bindingPayload = {
      metadata: {
        namespace: '',
        labels: {
          [`service.${this.environments.label_base_domain}/uuid`]: '',
        },
      },
      spec: {
        parameters: {},
        instanceRef: {
          name: '',
        },
      },
    };
  }

  async ngOnInit() {
    if (!this.namespace) {
      this.namespace = this.instanceDetailData.kubernetes.metadata.namespace;
    }
    if (this.name !== '') {
      this.loading = true;
      try {
        this.instanceDetailData = await this.catalogService.getInstance(
          this.cluster.id,
          this.name,
          this.namespace,
        );
      } catch (errors) {
        this.errorsToastService.error(errors);
      }
      this.loading = false;
    }
    if (!this.instanceDetailData) {
      return false;
    }
    this.bindingPayload.spec.instanceRef.name = this.instanceDetailData.kubernetes.metadata.name;
    this.getParams();
    this.getBindingApps();
  }

  cancel() {
    this.complete();
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return false;
    }
    this.bindingPayload.metadata.labels[
      `service.${this.environments.label_base_domain}/uuid`
    ] = this.binding_app_uuid;
    this.bindingPayload.metadata.namespace = this.namespace;
    this.createBinding();
  }

  async createBinding() {
    this.submitting = true;
    try {
      const res = await this.catalogService.createBinding(
        this.bindingPayload,
        this.cluster.id,
      );
      this.auiMessageService.success({
        content: this.translateService.get(
          'service_catalog_binding_success_title',
        ),
      });
      this.complete(res);
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
    this.submitting = false;
  }

  async getBindingApps() {
    this.bingdingAppOptions = [];
    this.bingdingAppLoading = true;
    try {
      const res = await this.serviceService.getK8sServices({
        pageSize: this.pageSize,
        params: {
          cluster: this.cluster.name,
          namespace: this.namespace,
        },
      });
      this.bingdingAppOptions = res.results.map((result: any) => {
        return {
          name: result.resource.name,
          uuid: result.resource.uuid,
        };
      });
      if (this.bingdingAppOptions.length === 0) {
        this.bindingPlaceholder = this.translateService.get(
          'service_catalog_binding_service_empty',
        );
      } else {
        this.bindingPlaceholder = this.translateService.get('please_select');
      }
    } catch (err) {}
    this.bingdingAppLoading = false;
  }

  getParams() {
    if (
      this.instanceDetailData.clusterserviceplans[0].spec
        .serviceBindingCreateParameterSchema &&
      this.instanceDetailData.clusterserviceplans[0].spec
        .serviceBindingCreateParameterSchema.properties
    ) {
      this.bindingCreateParameters = Object.entries(
        this.instanceDetailData.clusterserviceplans[0].spec
          .serviceBindingCreateParameterSchema.properties,
      );
    } else {
      this.bindingCreateParameters = [];
    }
  }

  handleType(type: string) {
    return this.catalogService.handleJSONSchemaType(type);
  }

  private complete(res?: any) {
    this.finished.next(res);
    this.finished.complete();
  }
}
