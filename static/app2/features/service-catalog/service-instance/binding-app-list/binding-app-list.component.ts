import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'alauda-ui';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { RoleService } from 'app2/shared/services/features/role.service';
import { ServiceCatalogService } from 'app2/shared/services/features/service-catalog.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import {
  Pagination,
  PaginationDataWrapper,
} from 'app2/shared/services/utility/pagination-data';
import { TranslateService } from 'app2/translate/translate.service';
import { get, last, some } from 'lodash';
import { Subscription } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import { ServiceInstanceBindingInstanceDialogComponent } from '../binding-instance-dailog/binding-instance-dialog.component';
import { ServiceInstanceViewInfoDialogComponent } from '../view-info-dialog/view-info-dialog.component';

@Component({
  selector: 'rc-service-catalog-binding-app-list',
  templateUrl: './binding-app-list.component.html',
  styleUrls: [
    './binding-app-list.component.scss',
    '../../service-catalog.common.scss',
  ],
})
export class ServiceInstanceBindingAppListComponent
  implements OnInit, OnDestroy {
  @Input()
  instanceName = '';
  @Input()
  serviceId = '';
  @Input()
  serviceNamespace: any;
  @Output()
  updateListNumber = new EventEmitter<number>();

  pageSize = 20;
  count = 0;
  paginationDataWrapper: PaginationDataWrapper<any>;
  paginationDataSubscription: Subscription;
  regionSubscription: Subscription;
  bindingItems: any[] = [];
  permission: Array<any> = [];
  cluster: Region;
  with_instance = false;
  initialized = false;
  destroyed = false;
  pollingTimer: any;

  constructor(
    @Inject(ENVIRONMENTS) private environments: Environments,
    private modal: ModalService,
    private translateService: TranslateService,
    private catalogService: ServiceCatalogService,
    private regionService: RegionService,
    private regionUtilities: RegionUtilitiesService,
    private router: Router,
    private errorsToastService: ErrorsToastService,
    private roleService: RoleService,
    private auiMessageService: MessageService,
  ) {
    const fetchRequest = (pageNo: number, pageSize: number, params: any) => {
      return this.catalogService.getBindings({
        pageNo,
        pageSize,
        params,
      });
    };
    this.paginationDataWrapper = new PaginationDataWrapper(
      fetchRequest,
      this.pageSize,
    );
  }

  ngOnInit() {
    if (this.serviceId) {
      this.with_instance = true;
    }

    this.paginationDataSubscription = this.paginationDataWrapper.pagination.subscribe(
      (paginationData: Pagination<any>) => {
        if (!paginationData.loadError) {
          this.count = paginationData.count;
          this.bindingItems = paginationData.results;
          this.bindingItems.map(item => {
            item.serviceinstance = {
              name:
                item.kubernetes.metadata.annotations[
                  `serviceinstance.${this.environments.label_base_domain}/name`
                ],
              clusterserviceclass_name:
                item.kubernetes.metadata.annotations[
                  `clusterserviceclass.${
                    this.environments.label_base_domain
                  }/name`
                ],
              clusterserviceplan_name:
                item.kubernetes.metadata.annotations[
                  `clusterserviceplan.${
                    this.environments.label_base_domain
                  }/name`
                ],
            };
          });
          this.updateListNumberAction();
          this.initialized = true;
        }
        this.resetPollingTimer();
      },
    );

    this.regionSubscription = this.regionService.region$
      .pipe(filter(region => !!region))
      .subscribe(region => {
        if (this.regionUtilities.isNewK8sRegion(region)) {
          this.onRegionChanged(region);
        } else {
          this.router.navigateByUrl('home');
        }
      });

    this.roleService
      .getPluralContextPermissions(['serviceinstance'])
      .then(res => {
        this.permission = res.serviceinstance;
      })
      .catch(() => {
        this.permission = [];
      });
  }

  ngOnDestroy() {
    this.destroyed = true;
    this.paginationDataSubscription.unsubscribe();
  }

  get bindingServiceNamespace() {
    return this.serviceNamespace.name || this.serviceNamespace;
  }

  get empty() {
    return !this.bindingItems || !this.bindingItems.length;
  }

  get loading() {
    return this.paginationDataWrapper.loading;
  }

  get loadError() {
    return this.paginationDataWrapper.loadError;
  }

  refetch() {
    this.paginationDataWrapper.refetch();
    this.updateListNumberAction();
  }

  async onRegionChanged(region: any) {
    this.bindingItems = [];
    this.cluster = region;
    await this.paginationDataWrapper.setParams({
      cluster: region.id,
      instance_name: this.instanceName,
      service_uuid: this.serviceId,
      namespace: this.bindingServiceNamespace,
    });
  }

  updateListNumberAction() {
    this.updateListNumber.emit(this.bindingItems.length);
  }

  viewCredentials(item: any) {
    const infoArr = Object.entries(item.credential);
    const modelRef = this.modal.open(ServiceInstanceViewInfoDialogComponent, {
      title: this.translateService.get('service_catalog_credentials'),
    });
    modelRef.componentInstance.instanceInfo = infoArr;
    modelRef.componentInstance.finished.pipe(first()).subscribe(res => {
      if (res) {
        this.refetch();
      }
      modelRef.close();
    });
  }

  viewParams(params: any) {
    const infoArr = Object.entries(params);
    const modelRef = this.modal.open(ServiceInstanceViewInfoDialogComponent, {
      title: this.translateService.get('service_catalog_binding_params'),
    });
    modelRef.componentInstance.instanceInfo = infoArr;
    modelRef.componentInstance.finished.pipe(first()).subscribe(res => {
      if (res) {
        this.refetch();
      }
      modelRef.close();
    });
  }

  bindInstance() {
    const modelRef = this.modal.open(
      ServiceInstanceBindingInstanceDialogComponent,
      {
        title: this.translateService.get(
          'service_catalog_bind_service_instance',
        ),
      },
    );
    modelRef.componentInstance.cluster = this.cluster;
    modelRef.componentInstance.service_uuid = this.serviceId;
    modelRef.componentInstance.service_namespace = this.bindingServiceNamespace;
    modelRef.componentInstance.finished.pipe(first()).subscribe(res => {
      if (res) {
        this.refetch();
      }
      modelRef.close();
    });
  }

  async unbind(bindingApp: any) {
    try {
      await this.catalogService.unbind(
        this.cluster.id,
        bindingApp.kubernetes.metadata.name,
        bindingApp.kubernetes.metadata.namespace,
      );
      this.auiMessageService.success({
        content: this.translateService.get(
          'service_catalog_unbinding_success_title',
        ),
      });
      this.refetch();
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
  }

  pageChanged(page: number) {
    this.paginationDataWrapper.setPageNo(page);
  }

  searchChanged(queryString: string) {
    this.paginationDataWrapper.setParams({
      name: queryString,
    });
  }

  getBindingStatus(binding: any) {
    if (binding.kubernetes.status.conditions.length !== 0) {
      if (last(binding.kubernetes.status.conditions)['status'] === 'True') {
        return 'Running';
      }
    }
    return 'Error';
  }

  trackByFn(index: number) {
    return index;
  }

  private resetPollingTimer() {
    clearTimeout(this.pollingTimer);
    if (this.destroyed) {
      return;
    }
    const hasDeployingBroker = some(this.bindingItems, (bindingItem: any) => {
      return get(bindingItem, 'kubernetes.status.conditions', []).length === 0;
    });
    const waitTime = hasDeployingBroker ? 3000 : 10000;
    this.pollingTimer = setTimeout(() => {
      this.refetch();
    }, waitTime);
  }
}
