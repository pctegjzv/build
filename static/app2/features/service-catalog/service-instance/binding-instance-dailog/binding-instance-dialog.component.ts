import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { MessageService } from 'alauda-ui';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import {
  ServiceBindingPayload,
  ServiceCatalogService,
} from 'app2/shared/services/features/service-catalog.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './binding-instance-dialog.component.html',
  styleUrls: [
    './binding-instance-dialog.component.scss',
    '../../service-catalog.common.scss',
  ],
})
export class ServiceInstanceBindingInstanceDialogComponent implements OnInit {
  parseInt = parseInt;
  @ViewChild('serviceCreateBindingForm')
  form: NgForm;
  @Input()
  cluster: any;
  @Input()
  service_uuid: string;
  @Input()
  service_namespace: string;
  @Output()
  finished = new EventEmitter<any>();
  pageSize = 999;
  instanceName = '';
  bindingCreateParameters: any[];
  bingdingInstanceOptions: any[];
  bingdingInstanceLoading = false;
  submitting = false;
  bindingPayload: ServiceBindingPayload;
  bindingPlaceholder: string;
  instanceNamespace: string;

  constructor(
    @Inject(ENVIRONMENTS) private environments: Environments,
    private catalogService: ServiceCatalogService,
    private auiMessageService: MessageService,
    private translateService: TranslateService,
    private errorsToastService: ErrorsToastService,
  ) {
    this.bindingPayload = {
      metadata: {
        namespace: '',
        labels: {
          [`service.${this.environments.label_base_domain}/uuid`]: '',
        },
      },
      spec: {
        parameters: {},
        instanceRef: {
          name: '',
        },
      },
    };
  }

  ngOnInit() {
    this.getBindingInstances();
  }

  cancel() {
    this.complete();
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return false;
    }
    this.bindingPayload.spec.instanceRef.name = this.instanceName;
    this.bindingPayload.metadata.labels[
      `service.${this.environments.label_base_domain}/uuid`
    ] = this.service_uuid;
    this.bindingPayload.metadata.namespace = this.instanceNamespace;
    this.bindInstance();
  }

  async bindInstance() {
    this.submitting = true;
    try {
      const res = await this.catalogService.createBinding(
        this.bindingPayload,
        this.cluster.id,
      );
      this.auiMessageService.success({
        content: this.translateService.get(
          'service_catalog_binding_success_title',
        ),
      });
      this.complete(res);
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
    this.submitting = false;
  }

  async getBindingInstances() {
    this.bingdingInstanceOptions = [];
    this.bingdingInstanceLoading = true;
    try {
      const res = await this.catalogService.getInstances({
        pageSize: this.pageSize,
        params: {
          cluster_id: this.cluster.id,
          namespace: this.service_namespace,
        },
      });
      this.bingdingInstanceOptions = res.results.map((result: any) => {
        return {
          name: result.kubernetes.metadata.name,
          namespace: result.kubernetes.metadata.namespace,
        };
      });
      if (this.bingdingInstanceOptions.length === 0) {
        this.bindingPlaceholder = this.translateService.get(
          'service_catalog_binding_instance_empty',
        );
      } else {
        this.bindingPlaceholder = this.translateService.get('please_select');
      }
    } catch (err) {}
    this.bingdingInstanceLoading = false;
  }

  handleType(type: string) {
    return this.catalogService.handleJSONSchemaType(type);
  }

  async onInstanceOptionChange(option: any) {
    this.instanceName = option.name;
    this.bindingCreateParameters = [];
    this.bingdingInstanceLoading = true;
    this.instanceNamespace = option.namespace;
    try {
      const res: any = await this.catalogService.getInstance(
        this.cluster.id,
        option.name,
        option.namespace,
      );
      if (res.clusterserviceplans[0].spec.serviceBindingCreateParameterSchema) {
        this.bindingCreateParameters = Object.entries(
          res.clusterserviceplans[0].spec.serviceBindingCreateParameterSchema
            .parameters.properties,
        );
      } else {
        this.bindingCreateParameters = [];
      }
    } catch (errors) {}
    this.bingdingInstanceLoading = false;
  }

  private complete(res?: any) {
    this.finished.next(res);
    this.finished.complete();
  }
}
