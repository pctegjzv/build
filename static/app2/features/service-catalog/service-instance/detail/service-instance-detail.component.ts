import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { Subscription } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { ServiceCatalogUtilitiesService } from 'app2/features/service-catalog/service-catalog.utilities.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { ServiceCatalogService } from 'app2/shared/services/features/service-catalog.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { get } from 'lodash';

import { ServiceInstanceBindingAppDialogComponent } from '../binding-app-dialog/binding-app-dialog.component';
import { ServiceInstanceBindingAppListComponent } from '../binding-app-list/binding-app-list.component';
import { ServiceInstanceUpdateParamsDialogComponent } from '../update-params-dialog/update-params-dialog.component';

@Component({
  templateUrl: './service-instance-detail.component.html',
  styleUrls: [
    './service-instance-detail.component.scss',
    '../../service-catalog.common.scss',
  ],
})
export class ServiceInstanceDetailComponent implements OnInit, OnDestroy {
  private routeParamsSubscription: Subscription;

  @ViewChild(ServiceInstanceBindingAppListComponent)
  private bindingAppList: ServiceInstanceBindingAppListComponent;
  private regionSubscription: Subscription;

  instanceParameters: any[] = [];
  bindList_num = 0;
  _loading = false;
  _loadError: any;
  instanceData: any;
  name: string;
  namespace: string;
  cluster: Region;
  initialized = false;
  destroyed = false;
  pollingTimer: any;

  constructor(
    @Inject(ENVIRONMENTS) private environments: Environments,
    private regionService: RegionService,
    private serviceCatalogUtilities: ServiceCatalogUtilitiesService,
    private modal: ModalService,
    private translate: TranslateService,
    private catalogService: ServiceCatalogService,
    private auiNotificationService: NotificationService,
    private route: ActivatedRoute,
    private router: Router,
    private translateService: TranslateService,
    private errorsToastService: ErrorsToastService,
  ) {}

  ngOnInit() {
    this.routeParamsSubscription = this.route.params
      .pipe(first())
      .subscribe(async params => {
        this.name = params['name'];
        this.namespace = params['namespace'];
        await this.refetch();
      });

    this.regionSubscription = this.regionService.region$
      .pipe(filter(region => !!region))
      .subscribe(() => {
        if (this.initialized) {
          this.router.navigateByUrl('service_catalog/service_instance');
        } else {
          this.initialized = true;
        }
      });
  }

  ngOnDestroy() {
    this.destroyed = true;
    this.routeParamsSubscription.unsubscribe();
    this.regionSubscription.unsubscribe();
  }

  async refetch() {
    this._loading = true;
    try {
      this.cluster = await this.regionService.getCurrentRegion();
      this.instanceData = await this.catalogService.getInstance(
        this.cluster.id,
        this.name,
        this.namespace,
      );
      this.instanceParameters = Object.entries(
        this.instanceData.kubernetes.spec.parameters,
      );
      this._loadError = false;
    } catch (errors) {
      this._loadError = true;
      if (errors.status === 403) {
        this.auiNotificationService.warning(
          this.translateService.get('permission_denied'),
        );
        return this.router.navigateByUrl('service_catalog/service_instance');
      } else if (errors.status === 404) {
        this.auiNotificationService.warning(
          this.translateService.get('service_catalog_instance_not_exist'),
        );
        return this.router.navigateByUrl('service_catalog/service_instance');
      }
      this.errorsToastService.error(errors);
    }
    this._loading = false;
    this.resetPollingTimer();
  }

  get empty() {
    return !this.instanceData;
  }

  get loading() {
    return this._loading;
  }

  get loadError() {
    return this._loadError;
  }

  get status() {
    if (
      this.instanceData.clusterserviceplans &&
      this.instanceData.kubernetes.status.conditions.length !== 0 &&
      this.instanceData.kubernetes.status.conditions[
        this.instanceData.kubernetes.status.conditions.length - 1
      ].status === 'True' &&
      this.instanceData.kubernetes.metadata.annotations[
        `clusterservicebroker.${this.environments.label_base_domain}/status`
      ] === 'True'
    ) {
      return true;
    }
    return false;
  }

  get brokerStatus() {
    if (
      this.instanceData.clusterserviceplans &&
      this.instanceData.kubernetes.metadata.annotations[
        `clusterservicebroker.${this.environments.label_base_domain}/status`
      ] === 'True'
    ) {
      return true;
    }
    return false;
  }

  get updateEnable() {
    return (
      this.instanceData.kubernetes.metadata.annotations[
        `clusterserviceclass.${
          this.environments.label_base_domain
        }/plan_updatable`
      ] &&
      this.instanceData.kubernetes.metadata.annotations[
        `clusterserviceclass.${
          this.environments.label_base_domain
        }/plan_updatable`
      ] !== 'false'
    );
  }

  bindingListNumber(num: number) {
    this.bindList_num = num;
  }

  redirctClusterDetail() {
    this.router.navigateByUrl(
      `cluster/detail/${this.instanceData.cluster.name}`,
    );
  }

  async deleteInstance() {
    try {
      const response = await this.serviceCatalogUtilities.deleteInstance(
        this.cluster.id,
        this.instanceData,
        this.bindList_num,
      );
      if (response) {
        this.router.navigateByUrl('service_catalog/service_instance');
      }
    } catch (err) {}
  }

  bindApp() {
    const modelRef = this.modal.open(ServiceInstanceBindingAppDialogComponent, {
      title: this.translate.get('service_catalog_binding_application'),
    });
    modelRef.componentInstance.instanceDetailData = this.instanceData;
    modelRef.componentInstance.cluster = this.cluster;
    modelRef.componentInstance.finished.pipe(first()).subscribe(res => {
      if (res && this.bindingAppList) {
        this.bindingAppList.refetch();
      }
      modelRef.close();
    });
  }

  updateParams() {
    const modelRef = this.modal.open(
      ServiceInstanceUpdateParamsDialogComponent,
      {
        title: this.translate.get('service_catalog_update_params'),
      },
    );
    modelRef.componentInstance.instanceDetailData = this.instanceData;
    modelRef.componentInstance.cluster_id = this.cluster.id;
    modelRef.componentInstance.finished.pipe(first()).subscribe(res => {
      if (res) {
        this.refetch();
      }
      modelRef.close();
    });
  }

  private resetPollingTimer() {
    clearTimeout(this.pollingTimer);
    if (this.destroyed) {
      return;
    }
    const hasDeployingInstance =
      get(this.instanceData, 'kubernetes.status.conditions', []).length === 0;
    const waitTime = hasDeployingInstance ? 5000 : 10000;
    this.pollingTimer = setTimeout(() => {
      this.refetch();
    }, waitTime);
  }
}
