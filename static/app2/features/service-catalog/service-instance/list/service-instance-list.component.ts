import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'alauda-ui';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { ServiceCatalogUtilitiesService } from 'app2/features/service-catalog/service-catalog.utilities.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { ServiceCatalogService } from 'app2/shared/services/features/service-catalog.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import {
  Pagination,
  PaginationDataWrapper,
} from 'app2/shared/services/utility/pagination-data';
import { TranslateService } from 'app2/translate/translate.service';
import { get, some } from 'lodash';
import { Subscription } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import { ServiceInstanceBindingAppDialogComponent } from '../binding-app-dialog/binding-app-dialog.component';
import { ServiceInstanceUpdateParamsDialogComponent } from '../update-params-dialog/update-params-dialog.component';

@Component({
  selector: 'rc-service-catalog-service-instance-list',
  templateUrl: './service-instance-list.component.html',
  styleUrls: [
    './service-instance-list.component.scss',
    '../../service-catalog.common.scss',
  ],
})
export class ServiceInstanceListComponent implements OnInit, OnDestroy {
  pageSize = 20;
  count = 0;
  cluster: Region;
  paginationDataWrapper: PaginationDataWrapper<any>;
  paginationDataSubscription: Subscription;
  regionSubscription: Subscription;
  instanceItems: any[] = [];
  initialized = false;
  destroyed = false;
  pollingTimer: any;

  constructor(
    @Inject(ENVIRONMENTS) private environments: Environments,
    private router: Router,
    private catalogService: ServiceCatalogService,
    private regionService: RegionService,
    private modal: ModalService,
    private translateService: TranslateService,
    private serviceCatalogUtilities: ServiceCatalogUtilitiesService,
    private errorsToastService: ErrorsToastService,
    private auiMessageService: MessageService,
  ) {
    const fetchRequest = (pageNo: number, pageSize: number, params: any) => {
      return this.catalogService.getInstances({
        pageNo,
        pageSize,
        params,
      });
    };
    this.paginationDataWrapper = new PaginationDataWrapper(
      fetchRequest,
      this.pageSize,
    );
  }

  ngOnInit() {
    this.paginationDataSubscription = this.paginationDataWrapper.pagination.subscribe(
      (paginationData: Pagination<any>) => {
        if (!paginationData.loadError) {
          this.count = paginationData.count;
          this.instanceItems = paginationData.results;
          this.initialized = true;
        }
        this.resetPollingTimer();
      },
    );

    this.regionSubscription = this.regionService.region$
      .pipe(filter(region => !!region))
      .subscribe(region => {
        this.cluster = region;
        this.onRegionChanged(region.id);
      });
  }

  ngOnDestroy() {
    this.destroyed = true;
    this.paginationDataSubscription.unsubscribe();
  }

  refetch() {
    this.paginationDataWrapper.refetch();
  }

  get empty() {
    return !this.instanceItems || !this.instanceItems.length;
  }

  get loading() {
    return this.paginationDataWrapper.loading;
  }

  get loadError() {
    return this.paginationDataWrapper.loadError;
  }

  getAppNum(item: any) {
    return (
      (item.kubernetes.metadata.annotations &&
        item.kubernetes.metadata.annotations[
          `serviceinstance.${this.environments.label_base_domain}/app_numbers`
        ]) ||
      0
    );
  }

  async onRegionChanged(cluster_id: string) {
    this.instanceItems = [];
    await this.paginationDataWrapper.setParams({
      cluster_id,
    });
  }

  status(rowData: any) {
    if (!rowData.kubernetes.metadata.annotations) {
      rowData.kubernetes.metadata.annotations = {};
    }
    if (
      rowData.kubernetes.metadata.annotations[
        `clusterservicebroker.${this.environments.label_base_domain}/status`
      ] !== 'True'
    ) {
      return 'brokerBreakdown';
    }
    if (rowData.kubernetes.status.conditions.length === 0) {
      return 'instanceCreating';
    }
    if (
      rowData.kubernetes.status.conditions[
        rowData.kubernetes.status.conditions.length - 1
      ].status !== 'True'
    ) {
      return 'instanceBreakdown';
    }
    return 'running';
  }

  updateEnable(rowData: any) {
    return (
      rowData.kubernetes.metadata.annotations[
        `clusterserviceclass.${
          this.environments.label_base_domain
        }/plan_updatable`
      ] &&
      rowData.kubernetes.metadata.annotations[
        `clusterserviceclass.${
          this.environments.label_base_domain
        }/plan_updatable`
      ] !== 'false'
    );
  }

  viewInstance(rowData: any) {
    this.router.navigateByUrl(
      `service_catalog/service_instance/instance_detail/${
        rowData.kubernetes.metadata.namespace
      }/${rowData.kubernetes.metadata.name}`,
    );
  }

  bindApp(item: any) {
    const modelRef = this.modal.open(ServiceInstanceBindingAppDialogComponent, {
      title: this.translateService.get('service_catalog_binding_application'),
    });
    modelRef.componentInstance.name = item.kubernetes.metadata.name;
    modelRef.componentInstance.namespace = item.kubernetes.metadata.namespace;
    modelRef.componentInstance.cluster = this.cluster;
    modelRef.componentInstance.finished.pipe(first()).subscribe(res => {
      if (res) {
        this.refetch();
      }
      modelRef.close();
    });
  }

  updateParams(item: any) {
    const modelRef = this.modal.open(
      ServiceInstanceUpdateParamsDialogComponent,
      {
        title: this.translateService.get('service_catalog_update_params'),
      },
    );
    modelRef.componentInstance.name = item.kubernetes.metadata.name;
    modelRef.componentInstance.namespace = item.kubernetes.metadata.namespace;
    modelRef.componentInstance.cluster_id = this.cluster.id;
    modelRef.componentInstance.finished.pipe(first()).subscribe(res => {
      if (res) {
        this.refetch();
      }
      modelRef.close();
    });
  }

  async deleteInstance(instance: any) {
    try {
      const response = await this.serviceCatalogUtilities.deleteInstance(
        this.cluster.id,
        instance,
        (instance.kubernetes.metadata.annotations &&
          parseInt(
            instance.kubernetes.metadata.annotations[
              `serviceinstance.${
                this.environments.label_base_domain
              }/app_numbers`
            ],
            10,
          )) ||
          0,
      );
      if (response) {
        this.auiMessageService.success({
          content: this.translateService.get(
            'service_catalog_instance_delate_success_title',
          ),
        });
        this.refetch();
      }
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
  }

  searchChanged(queryString: string) {
    this.paginationDataWrapper.setParams({
      name: queryString,
    });
  }

  pageChanged(page: number) {
    this.paginationDataWrapper.setPageNo(page);
  }

  trackByFn(index: number) {
    return index;
  }

  private resetPollingTimer() {
    clearTimeout(this.pollingTimer);
    if (this.destroyed) {
      return;
    }
    const hasDeployingBroker = some(this.instanceItems, (instance: any) => {
      return get(instance, 'kubernetes.status.conditions', []).length === 0;
    });
    const waitTime = hasDeployingBroker ? 3000 : 10000;
    this.pollingTimer = setTimeout(() => {
      this.refetch();
    }, waitTime);
  }
}
