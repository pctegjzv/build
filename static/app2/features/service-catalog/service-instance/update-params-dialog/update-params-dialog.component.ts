import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { MessageService } from 'alauda-ui';
import {
  ServiceCatalogService,
  ServiceInstancePayload,
} from 'app2/shared/services/features/service-catalog.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './update-params-dialog.component.html',
  styleUrls: [
    './update-params-dialog.component.scss',
    '../../service-catalog.common.scss',
  ],
})
export class ServiceInstanceUpdateParamsDialogComponent implements OnInit {
  parseInt = parseInt;
  @ViewChild('instanceUpdateParamsForm')
  form: NgForm;
  @Input()
  instanceDetailData: any;
  @Input()
  name = '';
  @Input()
  namespace = '';
  @Input()
  cluster_id = '';
  @Output()
  finished = new EventEmitter<any>();
  instanceUpdateParameters: any[] = [];
  updateParamsPayload: ServiceInstancePayload;
  submitting = false;
  loading = false;

  constructor(
    private catalogService: ServiceCatalogService,
    private translateService: TranslateService,
    private auiMessageService: MessageService,
    private errorsToastService: ErrorsToastService,
  ) {
    this.updateParamsPayload = {
      metadata: {
        name: '',
        namespace: '',
        resourceVersion: '',
      },
      spec: {
        clusterServiceClassExternalName: '',
        clusterServicePlanExternalName: '',
        externalID: '',
        parameters: {},
      },
    };
  }

  async ngOnInit() {
    if (this.name !== '') {
      this.loading = true;
      try {
        this.instanceDetailData = await this.catalogService.getInstance(
          this.cluster_id,
          this.name,
          this.namespace,
        );
      } catch (errors) {
        this.errorsToastService.error(errors);
      }
      this.loading = false;
    }
    if (!this.instanceDetailData) {
      return false;
    }
    this.getParams();
  }

  getParams() {
    this.instanceUpdateParameters = [];
    if (
      this.instanceDetailData.clusterserviceplans[0].spec
        .serviceInstanceUpdateParameterSchema
    ) {
      this.instanceUpdateParameters = Object.entries(
        this.instanceDetailData.clusterserviceplans[0].spec
          .serviceInstanceUpdateParameterSchema.properties,
      );
      this.instanceUpdateParameters.forEach(item => {
        if (this.instanceDetailData.kubernetes.spec.parameters[item[0]]) {
          item[1].value = this.instanceDetailData.kubernetes.spec.parameters[
            item[0]
          ];
        }
      });
      this.instanceUpdateParameters.forEach(param => {
        this.updateParamsPayload.spec.parameters[param[0]] = param[1].value;
      });
    } else {
      Object.entries(
        this.instanceDetailData.kubernetes.spec.parameters,
      ).forEach(param => {
        this.instanceUpdateParameters.push([param[0], { value: param[1] }]);
        this.updateParamsPayload.spec.parameters[param[0]] = param[1];
      });
    }
  }

  async updateParams() {
    this.submitting = true;
    this.updateParamsPayload.spec.clusterServiceClassExternalName = this.instanceDetailData.kubernetes.spec.clusterServiceClassExternalName;
    this.updateParamsPayload.spec.clusterServicePlanExternalName = this.instanceDetailData.kubernetes.spec.clusterServicePlanExternalName;
    this.updateParamsPayload.metadata.name = this.instanceDetailData.kubernetes.metadata.name;
    this.updateParamsPayload.metadata.namespace = this.instanceDetailData.kubernetes.metadata.namespace;
    this.updateParamsPayload.metadata.resourceVersion = this.instanceDetailData.kubernetes.metadata.resourceVersion;
    this.updateParamsPayload.spec.externalID = this.instanceDetailData.kubernetes.spec.externalID;
    try {
      const res = await this.catalogService.updateInstance(
        this.updateParamsPayload,
        this.cluster_id,
        this.instanceDetailData.kubernetes.metadata.name,
        this.instanceDetailData.kubernetes.metadata.namespace,
      );
      this.auiMessageService.success({
        content: this.translateService.get(
          'service_catalog_instance_update_params_success_title',
        ),
      });
      this.complete(res);
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
    this.submitting = false;
  }

  handleType(type: string) {
    return this.catalogService.handleJSONSchemaType(type);
  }

  cancel() {
    this.complete();
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return false;
    }
    this.updateParams();
  }

  private complete(res?: any) {
    this.finished.next(res);
    this.finished.complete();
  }
}
