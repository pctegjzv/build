import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  templateUrl: './view-info-dialog.component.html',
  styleUrls: ['./view-info-dialog.component.scss'],
})
export class ServiceInstanceViewInfoDialogComponent {
  @Input()
  instanceInfo: any[];
  @Output()
  finished = new EventEmitter<any>();
  constructor() {}

  cancel() {
    this.complete();
  }

  private complete(res?: any) {
    this.finished.next(res);
    this.finished.complete();
  }
}
