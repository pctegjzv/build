import { Component, Input, OnInit } from '@angular/core';

import { ServiceService } from 'app2/shared/services/features/service.service';

@Component({
  selector: 'rc-service-config-kube-service',
  templateUrl: './service-config-kube-service.component.html',
  styleUrls: ['../service-tab.common.scss'],
})
export class ServiceConfigKubeServiceComponent implements OnInit {
  private _kubernetes: any[] = []; // 'kubernetes' field from service api response
  @Input()
  set kubernetes(kubernetes: any[]) {
    this._kubernetes = kubernetes;
    this.getkubeServices();
  }

  get kubernetes(): any[] {
    return this._kubernetes;
  }
  kubeServices: any[];

  constructor(private serviceService: ServiceService) {}

  ngOnInit() {}

  private getkubeServices() {
    this.kubeServices = this.kubernetes
      .filter((item: any) => {
        return item.kind === 'Service';
      })
      .map((item: any) => {
        return this.serviceService.getKubeServiceViewModel(item);
      });
  }
}
