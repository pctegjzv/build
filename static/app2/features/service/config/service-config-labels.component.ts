import { Component, Input, OnInit } from '@angular/core';

import { ServiceService } from 'app2/shared/services/features/service.service';

@Component({
  selector: 'rc-service-config-labels',
  templateUrl: './service-config-labels.component.html',
  styleUrls: ['../service-tab.common.scss'],
})
export class ServiceConfigLabelsComponent implements OnInit {
  private _kubernetes: any[] = []; // 'kubernetes' field from service api response
  @Input()
  set kubernetes(kubernetes: any[]) {
    this._kubernetes = kubernetes;
    this.getServiceAffinity();
  }

  get kubernetes(): any[] {
    return this._kubernetes;
  }
  serviceLabels: any[] = [];

  constructor(private serviceService: ServiceService) {}

  ngOnInit() {}

  private getServiceAffinity() {
    const kindOptions: string[] = ['Deployment', 'DaemonSet', 'StatefulSet'];
    const origin = this.kubernetes.find((item: any) => {
      return kindOptions.includes(item.kind);
    });

    this.serviceLabels = this.serviceService.getServiceLabelsViewModel(origin);
  }
}
