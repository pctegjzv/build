import { Component, Input, OnInit } from '@angular/core';

import { ServiceService } from 'app2/shared/services/features/service.service';

@Component({
  selector: 'rc-service-config-node-tags',
  templateUrl: './service-config-node-tags.component.html',
  styleUrls: ['../service-tab.common.scss'],
})
export class ServiceConfigNodeTagsComponent implements OnInit {
  private _kubernetes: any[] = []; // 'kubernetes' field from service api response
  @Input()
  set kubernetes(kubernetes: any[]) {
    this._kubernetes = kubernetes;
    this.getServiceAffinity();
  }

  get kubernetes(): any[] {
    return this._kubernetes;
  }
  nodeTags: any[] = [];

  constructor(private serviceService: ServiceService) {}

  ngOnInit() {}

  private getServiceAffinity() {
    const kindOptions: string[] = ['Deployment', 'DaemonSet', 'StatefulSet'];
    const origin = this.kubernetes.find((item: any) => {
      return kindOptions.includes(item.kind);
    });

    this.nodeTags = this.serviceService.getServiceNodeTagsViewModel(
      origin,
      true,
    );
  }
}
