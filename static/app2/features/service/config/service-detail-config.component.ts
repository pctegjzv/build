import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'rc-service-detail-config',
  templateUrl: './service-detail-config.component.html',
  styleUrls: ['../service-tab.common.scss'],
})
export class ServiceDetailConfigComponent implements OnInit {
  private _kubernetes: any[] = []; // 'kubernetes' field from service api response
  @Input()
  set kubernetes(kubernetes: any[]) {
    this._kubernetes = kubernetes;
  }

  get kubernetes(): any[] {
    return this._kubernetes;
  }
  currentTab = 'service-label';

  constructor() {}

  ngOnInit() {}

  public setCurrentTab(tab: string) {
    this.currentTab = tab;
  }
}
