import { Component, Input, OnInit } from '@angular/core';
import _ from 'lodash';

import { ContainerVolumeViewModel } from 'app2/features/service/service.type';
import { ServiceService } from 'app2/shared/services/features/service.service';

@Component({
  selector: 'rc-service-container-config-volume',
  templateUrl: './service-container-config-volume.component.html',
  styleUrls: ['../service-tab.common.scss'],
})
export class ContainerConfigVolumeComponent implements OnInit {
  private _kubernetes: any[] = []; // 'kubernetes' field from service api response
  @Input()
  set kubernetes(kubernetes: any[]) {
    this._kubernetes = kubernetes;
    this.getContainerVolume();
  }

  get kubernetes(): any[] {
    return this._kubernetes;
  }
  containers: any[];

  constructor(private serviceService: ServiceService) {}

  ngOnInit(): void {}

  private getContainerVolume() {
    const kindOptions: string[] = ['Deployment', 'DaemonSet', 'StatefulSet'];
    const origin = this.kubernetes.find((item: any) => {
      return kindOptions.includes(item.kind);
    });
    const _containers = _.get(origin, 'spec.template.spec.containers', []);
    const volumes = _.get(origin, 'spec.template.spec.volumes', []).map(
      (item: any) => {
        return this.serviceService.getContainerVolumeViewModel(item);
      },
    );

    this.containers = _containers
      .filter((item: any) => {
        return item.volumeMounts && item.volumeMounts.length;
      })
      .map((item: any) => {
        return this.getContainerVolumeViewModel(item, volumes);
      });
  }

  private getContainerVolumeViewModel(container: any, volumes: any[]) {
    const instance: any = {
      name: container.name,
      volumes: [],
    };
    instance.volumes = _.get<any[]>(container, 'volumeMounts', [])
      .map(item => {
        const volume = volumes.find(v => v.name === item.name);
        let duplicateFlag = false;
        if (volume) {
          if (volume.type !== 'configmap') {
            volume.containerPath = item.mountPath;
            if (item.subPath) {
              volume.subPath = item.subPath;
            }
          } else {
            if (item.subPath) {
              volume.containerPath = '';
              volume.configMapKeyRef = true;
              if (!volume.configMapKeyMap) {
                volume.configMapKeyMap = [];
              } else {
                duplicateFlag = true;
              }
              volume.configMapKeyMap.push({
                key: item.subPath,
                path: item.mountPath,
              });
            } else {
              volume.containerPath = item.mountPath;
            }
          }
          return duplicateFlag ? null : volume;
        }
        return null;
      })
      .filter(v => !!v);
    return instance;
  }

  getItemNameOrPathDisplay(item: ContainerVolumeViewModel) {
    switch (item.type) {
      case 'host-path':
        return item.hostPath;
      case 'volume':
        return item.volumeName;
      case 'pvc':
        return item.pvcName;
      case 'configmap':
        return item.configMapName;
    }
  }
}
