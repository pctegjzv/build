import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'rc-service-container-config',
  templateUrl: './service-container-config.component.html',
  styleUrls: ['../service-tab.common.scss'],
})
export class ContainerConfigComponent implements OnInit {
  private _kubernetes: any[] = []; // 'kubernetes' field from service api response
  @Input()
  set kubernetes(kubernetes: any[]) {
    this._kubernetes = kubernetes;
  }

  get kubernetes(): any[] {
    return this._kubernetes;
  }
  currentTab = 'volume';

  constructor() {}

  ngOnInit() {}

  public setCurrentTab(tab: string) {
    this.currentTab = tab;
  }
}
