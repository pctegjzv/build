import { Component, Input, OnInit } from '@angular/core';

import { ServiceService } from 'app2/shared/services/features/service.service';

@Component({
  selector: 'rc-container-config-configmap',
  templateUrl: './service-container-configmap.component.html',
  styleUrls: ['../service-tab.common.scss'],
})
export class ContainerConfigConfigmapComponent implements OnInit {
  private _kubernetes: any[] = []; // 'kubernetes' field from service api response
  @Input()
  set kubernetes(kubernetes: any[]) {
    this._kubernetes = kubernetes;
    this.getContainerConfigmaps();
  }

  containers: any[];

  get kubernetes(): any[] {
    return this._kubernetes;
  }
  constructor(private serviceService: ServiceService) {}

  ngOnInit(): void {}

  private getContainerConfigmaps() {
    const _containers = this.serviceService.getContainersFromKubernetes(
      this.kubernetes,
    );
    this.containers = _containers
      .filter((item: any) => {
        return item.envFrom;
      })
      .map((item: any) => {
        return this.getContainerConfigmapViewModelFromYaml(item);
      });
  }

  private getContainerConfigmapViewModelFromYaml(container: any) {
    const instance: any = {
      name: container.name,
      configMaps: [], // name: '',
    };
    const configMaps = container.envFrom;
    instance.configMaps = configMaps.map((item: any) => {
      return {
        name: item.configMapRef.name,
      };
    });
    return instance;
  }
}
