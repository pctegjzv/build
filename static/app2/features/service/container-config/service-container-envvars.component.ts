import { Component, Input, OnInit } from '@angular/core';
import _ from 'lodash';

import { ServiceService } from 'app2/shared/services/features/service.service';

@Component({
  selector: 'rc-service-container-envvars',
  templateUrl: './service-container-envvars.component.html',
  styleUrls: ['../service-tab.common.scss'],
})
export class ContainerConfigEnvvarsComponent implements OnInit {
  private _kubernetes: any[] = []; // 'kubernetes' field from service api response
  @Input()
  set kubernetes(kubernetes: any[]) {
    this._kubernetes = kubernetes;
    this.getContainerEnvvars();
  }

  get kubernetes(): any[] {
    return this._kubernetes;
  }
  containers: any[];

  constructor(private serviceService: ServiceService) {}

  ngOnInit(): void {}

  private getContainerEnvvars() {
    const _containers = this.serviceService.getContainersFromKubernetes(
      this.kubernetes,
    );
    this.containers = _containers.map((item: any) => {
      return this.getContainerEnvvarViewModelFromYaml(item);
    });
  }

  private getContainerEnvvarViewModelFromYaml(container: any) {
    const instance: any = {
      name: container.name,
      envVars: [], // name: '', value: '', reference: false, configmap: '', configmapKey: '',
    };
    const env = container.env;
    instance.envVars = env
      .filter((item: any) => {
        return !_.has(item, 'valueFrom.secretKeyRef');
      })
      .filter((item: any) => {
        return ![
          '__ALAUDA_FILE_LOG_PATH__',
          '__ALAUDA_EXCLUDE_LOG_PATH__',
          '__ALAUDA_SERVICE_ID__',
          '__ALAUDA_SERVICE_NAME__',
          '__ALAUDA_APP_NAME__',
        ].includes(item.name);
      })
      .map((item: any) => {
        const data = {
          name: item.name,
          value: item.value || '',
          reference: !!item.valueFrom,
          configmap: _.get(item, 'valueFrom.configMapKeyRef.name', ''),
          configmapKey: _.get(item, 'valueFrom.configMapKeyRef.key', ''),
        };
        return data;
      });
    return instance;
  }
}
