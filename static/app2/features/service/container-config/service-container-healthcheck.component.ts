import { Component, Input, OnInit } from '@angular/core';
import { get } from 'lodash';

import { ServiceService } from 'app2/shared/services/features/service.service';

@Component({
  selector: 'rc-container-config-healthcheck',
  templateUrl: './service-container-healthcheck.component.html',
  styleUrls: [
    './service-container-healthcheck.component.scss',
    '../service-tab.common.scss',
  ],
})
export class ContainerConfigHealthcheckComponent implements OnInit {
  private _kubernetes: any[] = []; // 'kubernetes' field from service api response
  @Input()
  set kubernetes(kubernetes: any[]) {
    this._kubernetes = kubernetes;
    this.getContainerHealthcheck();
  }

  get kubernetes(): any[] {
    return this._kubernetes;
  }

  containers: any[];

  constructor(private serviceService: ServiceService) {}

  ngOnInit(): void {}

  getContainerHealthcheck() {
    const _containers = this.serviceService.getContainersFromKubernetes(
      this.kubernetes,
    );
    this.containers = _containers
      .filter((item: any) => {
        return item.livenessProbe || item.readinessProbe;
      })
      .map((item: any) => {
        const data = {
          name: item.name,
          healthCheck: {
            liveness: this.serviceService.getContainerHealthCheckItemViewModel(
              get(item, 'livenessProbe', null),
            ),
            readiness: this.serviceService.getContainerHealthCheckItemViewModel(
              get(item, 'readinessProbe', null),
            ),
          },
        };
        return data;
      });
  }
}
