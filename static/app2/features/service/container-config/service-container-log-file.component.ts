import { Component, Input, OnInit } from '@angular/core';

import { LogFileDetailViewModel } from 'app2/features/service/service.type';
import { ServiceService } from 'app2/shared/services/features/service.service';

@Component({
  selector: 'rc-service-container-log-file',
  templateUrl: './service-container-log-file.component.html',
  styleUrls: ['../service-tab.common.scss'],
})
export class ContainerConfigLogFileComponent implements OnInit {
  private _kubernetes: any[] = []; // 'kubernetes' field from service api response
  @Input()
  set kubernetes(kubernetes: any[]) {
    this._kubernetes = kubernetes;
    this.getContainerLogFiles();
  }

  get kubernetes(): any[] {
    return this._kubernetes;
  }
  containers: any[];

  constructor(private serviceService: ServiceService) {}

  ngOnInit(): void {}

  private getContainerLogFiles() {
    const _containers = this.serviceService.getContainersFromKubernetes(
      this.kubernetes,
    );
    this.containers = _containers.map((item: any) => {
      return this.getContainerLogFileViewModelFromYaml(item);
    });
  }

  private getContainerLogFileViewModelFromYaml(container: any) {
    const instance: { name: string; logFileDetail: LogFileDetailViewModel } = {
      name: container.name,
      logFileDetail: {
        logFiles: [],
        excludeLogFiles: [],
      },
    };
    const env = container.env || [];
    const logFiles = env.find((item: { name: string; value: string }) => {
      return item.name === '__ALAUDA_FILE_LOG_PATH__';
    });
    instance.logFileDetail.logFiles = logFiles ? logFiles.value.split(',') : [];
    const excludeLogFiles = env.find(
      (item: { name: string; value: string }) => {
        return item.name === '__ALAUDA_EXCLUDE_LOG_PATH__';
      },
    );
    instance.logFileDetail.excludeLogFiles = excludeLogFiles
      ? excludeLogFiles.value.split(',')
      : [];
    return instance;
  }
}
