import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';

import { ServiceService } from 'app2/shared/services/features/service.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';

@Component({
  templateUrl: './k8s-service-container-select.component.html',
  styleUrls: ['./k8s-service-container-select.component.scss'],
})
export class K8sServiceContainerSelectComponent implements OnInit {
  @Output()
  close = new EventEmitter();

  activeIndex: number;
  kubernetes: any;
  containers: any;

  constructor(
    @Inject(MODAL_DATA)
    private data: {
      serviceId: any;
      containers: any;
      kubernetes?: any;
      nextStep: () => void;
    },
    private serviceService: ServiceService,
  ) {}

  async ngOnInit() {
    if (this.data.kubernetes) {
      this.containers = this.data.containers;
      this.kubernetes = this.data.kubernetes;
      return;
    }

    const { kubernetes } = await this.serviceService.getK8sService(
      this.data.serviceId,
    );

    const { containers } = this.serviceService.getServiceViewModel(kubernetes);

    this.kubernetes = kubernetes;
    this.containers = containers;
  }

  nextStep() {
    this.data.nextStep();
  }
}
