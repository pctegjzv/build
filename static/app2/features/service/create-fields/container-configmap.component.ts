import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  Component,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
  Optional,
  Self,
  SimpleChange,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';
import { Subject } from 'rxjs';

import { FormFieldControl } from 'app2/shared/form-field-control';
import {
  ConfigMapOption,
  ConfigmapService,
} from 'app2/shared/services/features/configmap.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';

let nextUniqueId = 0;

/**
 * value:
 * ['configmap1', 'configmap2']
 */

@Component({
  selector: 'rc-container-fields-configmap',
  templateUrl: './container-configmap.component.html',
  styleUrls: ['./container-configmap.component.scss'],
  providers: [
    {
      provide: FormFieldControl,
      useExisting: ContainerFieldsConfigmapComponent,
    },
  ],
})
export class ContainerFieldsConfigmapComponent
  implements FormFieldControl, ControlValueAccessor, OnInit, OnChanges {
  /***** FormFieldControl start *****/

  private _uniqueId = `rc-container-fields-configmap-${++nextUniqueId}`;
  private _value: any;
  private _required = false;
  protected _disabled = false;

  stateChanges = new Subject<void>();

  @Input()
  id = this._uniqueId;

  @Input()
  get value(): any {
    return this.ngControl ? this.ngControl.value : this._value;
  }

  set value(value: any) {
    this._value = value;
    this.emitToModel();
    this.applyToNative();
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }
    this._disabled = disabled;
    this.stateChanges.next();
  }

  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);

    return !!(isInvalid && (isDirty || isSubmitted));
  }

  controlType = 'rc-container-fields-configmap';

  /***** FormFieldControl end *****/
  @Input()
  namespace: string;
  configmapOptions: ConfigMapOption[] = [];
  configmaps: string[] = [];
  region: Region;

  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() private parentForm: NgForm,
    @Optional() private parentFormGroup: FormGroupDirective,
    private configmapService: ConfigmapService,
    private regionService: RegionService,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }
  async ngOnInit() {
    this.region = await this.regionService.getCurrentRegion();
    await this.getK8sConfigmapOptions();
  }

  ngOnChanges({ namespace }: { namespace: SimpleChange }): void {
    if (!this.region) {
      return;
    }
    if (namespace && namespace.currentValue) {
      this.namespace = namespace.currentValue;
      this.getK8sConfigmapOptions();
    }
  }

  async getK8sConfigmapOptions() {
    this.configmapOptions = await this.configmapService.getConfigMapOptions({
      clusterId: this.region.id,
      namespace: this.namespace,
    });
  }

  trackByIndex(index: number) {
    return index;
  }

  // set ngModel value when native view changes
  updateModelValue() {
    this._value = this.configmaps.map((item: string) => {
      return item;
    });
    this.emitToModel();
  }

  private emitToModel() {
    this.onTouchedCallback();
    this.onChangeCallback(this._value);
  }

  private applyToNative() {
    if (this._value === null || this._value === undefined) {
      return;
    }
    try {
      this.configmaps = this._value;
    } catch (e) {
      // placeholder
    }
  }

  /** Following Methods Implements ControlValueAccessor **/

  writeValue(value: any) {
    this._value = value;
    this.applyToNative();
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}
