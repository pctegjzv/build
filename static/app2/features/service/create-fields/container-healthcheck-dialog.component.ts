import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import * as _ from 'lodash';

@Component({
  selector: 'rc-container-fields-healthcheck-dialog',
  templateUrl: './container-healthcheck-dialog.component.html',
  styleUrls: ['./container-healthcheck-dialog.component.scss'],
})

/*
* init value
* {
*   protocol: 'HTTP',  // 'HTTP', 'TCP', 'EXEC'
    initialDelaySeconds: 100,
    periodSeconds: 60,
    timeoutSeconds: 20,
    successThreshold: 0,
    failureThreshold: 0,
    scheme: 'test-scheme',
    port: 80,
    path: '/',
    headers: [{name: 'test-name', value: 'test-value'}],
    commands: [command: 'test-commands'],
* }
* output value format is same with init value
*
* */
export class HealthcheckDialogComponent implements OnInit, OnDestroy {
  @Output()
  finish = new EventEmitter<any>();
  @ViewChild('form')
  form: NgForm;
  submitting = false;
  submitted = false;
  isUpdate = false;
  // errorMappers: any = {};
  protocolOptions: any[];
  schemeOptions: string[];
  initHeaders: any[] = [];
  model = {
    protocol: 'HTTP',
    initialDelaySeconds: 300,
    periodSeconds: 60,
    timeoutSeconds: 30,
    successThreshold: 0,
    failureThreshold: 5,
    scheme: 'HTTP',
    port: 80,
    path: '/',
    headers: this.initHeaders,
    commands: [
      {
        command: '',
      },
    ],
  };

  constructor() {}

  ngOnInit() {
    this.protocolOptions = ['HTTP', 'TCP', 'EXEC'];
    this.schemeOptions = ['HTTP', 'HTTPS'];
  }
  ngOnDestroy() {}

  initValue(healthcheck: any) {
    if (healthcheck) {
      Object.assign(this.model, healthcheck);
      this.isUpdate = true;
    }
  }

  onSubmit() {
    this.submitted = true;
  }

  addHeader() {
    this.model.headers.push({
      name: '',
      value: '',
    });
  }

  removeHeader(i: number) {
    this.model.headers.splice(i, 1);
  }

  addCommand() {
    this.model.commands.push({
      command: '',
    });
  }

  removeCommand(i: number) {
    if (this.model.commands.length === 1) {
      return;
    }
    this.model.commands.splice(i, 1);
  }

  private complete(res?: any) {
    this.finish.next(res);
    this.finish.complete();
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    /*
    * click comfirm button to output the formatted value ( variable 'data' here ) to the componenet 'pod-healthcheck.component'
    * */
    this.complete({
      result: true,
      data: _.clone(this.model),
    });
  }
  cancel() {
    this.complete();
  }

  trackByIndex(index: number) {
    return index;
  }
}
