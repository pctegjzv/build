import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  AfterViewInit,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  Self,
  SimpleChanges,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';
import * as _ from 'lodash';
import { Subject } from 'rxjs';
import { first } from 'rxjs/operators';

import { FormFieldControl } from 'app2/shared/form-field-control';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';

import { HealthcheckDialogComponent } from './container-healthcheck-dialog.component';

let nextUniqueId = 0;

/**
 * value:
 * 协议类型 protocol 为 HTTP TCP EXEC 其中之一
 * 下表中为 'Y' 表示在此协议类型下此字段值为必填
 * '-' 表示此协议类型下无此字段
 *
 {
    liveness: {
      protocol: 'HTTP',              //协议类型           HTTP       TCP        EXEC

      initialDelaySeconds: 30,       //启动时间           Y          Y          Y
      periodSeconds: 10,             //间隔              Y          Y          Y
      timeoutSeconds: 5,             //超时时长           Y          Y          Y
      successThreshold: 1,           //正常阈值           Y          Y          Y
      failureThreshold: 5,           //不正常阈值         Y          Y          Y

      port: 8000,                    //端口              Y          Y          -

      scheme:'HTTP',                 //scheme协议        Y          -          -
      path: '/test',                 //路径              Y          -          -
      headers: [                     //请求标题           Y          -          -
        {
          name: X-Custom-Header,
          value: 'awesome',
        },
      ],

      commands: [                    //执行命令           -          -          Y
        'test.sh'
      ]

    },
    readiness: {
      //同上
    }
 }
 */

@Component({
  selector: 'rc-container-fields-healthcheck',
  templateUrl: 'container-healthcheck.component.html',
  styleUrls: ['container-healthcheck.component.scss'],
  providers: [
    {
      provide: FormFieldControl,
      useExisting: HealthcheckComponent,
    },
  ],
})
export class HealthcheckComponent
  implements
    FormFieldControl,
    ControlValueAccessor,
    OnInit,
    OnDestroy,
    OnChanges,
    AfterViewInit {
  /***** FormFieldControl start *****/

  private _uniqueId = `rc-healthcheck-${++nextUniqueId}`;
  private _value: any;
  private _required = false;
  protected _disabled = false;

  stateChanges = new Subject<void>();

  @Input()
  id = this._uniqueId;

  @Input()
  get value(): any {
    return this.ngControl ? this.ngControl.value : this._value;
  }

  set value(value: any) {
    this._value = value;
    this.emitToModel();
    this.applyToNative();
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }
    this._disabled = disabled;
    this.stateChanges.next();
  }

  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);

    return !!(isInvalid && (isDirty || isSubmitted));
  }

  controlType = 'rc-container-fields-healthcheck';

  /***** FormFieldControl end *****/
  liveness: any;
  readiness: any;
  healthcheckValue = {
    liveness: this.liveness,
    readiness: this.readiness,
  };
  @Output()
  valueChanges = new EventEmitter<Array<any>>();

  async openTemplateModal(
    type: string,
    updating: boolean = false,
    healthcheck: any = null,
  ) {
    try {
      const modalRef = await this.modalService.open(
        HealthcheckDialogComponent,
        {
          title: this.translate.get(
            `k8s_service_add_${type}_health_check_items`,
          ),
        },
      );
      if (updating) {
        modalRef.componentInstance.initValue(healthcheck);
      }
      modalRef.componentInstance.finish.pipe(first()).subscribe((res: any) => {
        if (!res) {
          modalRef.close();
          return;
        }
        // get formatted value from modal component 'HealthcheckDialogComponent'
        if (res.data) {
          this.healthcheckValue[type] = _.clone(res.data);
          this.updateModelValue();
          modalRef.close();
        } else {
          modalRef.close();
        }
      });
    } catch (error) {
      //
    }
  }

  trackByIndex(index: number) {
    return index;
  }

  deleteHealthcheck(type: string) {
    this.healthcheckValue[type] = null;
    this.updateModelValue();
  }

  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() private parentForm: NgForm,
    @Optional() private parentFormGroup: FormGroupDirective,
    private modalService: ModalService,
    private translate: TranslateService,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }
  ngOnInit() {}
  ngOnChanges(_changes: SimpleChanges): void {}
  ngAfterViewInit() {}
  ngOnDestroy() {}

  // set ngModel value when native view changes
  private updateModelValue() {
    this._value = _.clone(this.healthcheckValue);
    this.emitToModel();
  }

  // emit _value to ngModel
  private emitToModel() {
    this.onTouchedCallback();
    this.onChangeCallback(this._value);
  }

  // apply _value to native view
  private applyToNative() {
    if (this._value === null || this._value === undefined) {
      return;
    }
    this.healthcheckValue = this._value;
  }

  /** Following Methods Implements ControlValueAccessor **/

  writeValue(value: any) {
    this._value = value;
    this.applyToNative();
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}
