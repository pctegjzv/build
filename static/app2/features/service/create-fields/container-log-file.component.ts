import { Component, OnInit, Optional, Self, ViewChild } from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';
import { TagsInputComponent } from 'app2/shared/components/tags-input/tags-input.component';

import { K8S_LOG_FILE_PATTERN } from '../../../../app/components/common/config/common-pattern';

/**
 * value: TODO
 * ['logfile1', 'logfile2']
 */

@Component({
  selector: 'rc-container-fields-log-file',
  templateUrl: './container-log-file.component.html',
  styleUrls: ['./container-log-file.component.scss'],
})
export class ContainerFieldsLogFileComponent
  implements ControlValueAccessor, OnInit {
  logFiles: string[];
  logFileReg = K8S_LOG_FILE_PATTERN;
  @ViewChild('tagsInput')
  tagsInput: TagsInputComponent;
  _value: string[];

  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }

  ngOnInit() {
    this.tagsInput.registerBeforeAdd((value: string) => {
      const reg = new RegExp(this.logFileReg.pattern);
      return reg.test(value);
    });
  }

  onModelChange() {
    this.updateModelValue();
  }

  // set ngModel value when native view changes
  updateModelValue() {
    this._value = this.logFiles.map((item: string) => {
      return item;
    });
    this.emitToModel();
  }

  private emitToModel() {
    this.onTouchedCallback();
    this.onChangeCallback(this._value);
  }

  private applyToNative() {
    if (this._value === null || this._value === undefined) {
      return;
    }
    try {
      this.logFiles = this._value;
    } catch (e) {
      // placeholder
    }
  }

  /** Following Methods Implements ControlValueAccessor **/

  writeValue(value: any) {
    if (!value) {
      return;
    }
    this._value = value;
    this.applyToNative();
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}
