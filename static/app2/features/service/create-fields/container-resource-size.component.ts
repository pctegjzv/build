import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  AfterViewInit,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  Self,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';
import * as _ from 'lodash';
import { Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { FormFieldControl } from 'app2/shared/form-field-control';

import { INT_PATTERN } from '../../../../app/components/common/config/common-pattern';

let nextUniqueId = 0;
/*
* value:
* {
*   "requests": {
*     "memory": "3M",
*     "cpu": "3000m"                         // cpu 单位为m时 值为string
*   },
*   "limits": {
*     "memory": "",                          // 无值时 值为 "" 空串
*     "cpu": 3                               // cpu 单位为c时 省略单位c 1c=1000m 值为number
*   }
* }
*
*
* control errors
* {
* 	"container_resource_size_invalid": {
* 		"cpu": {                             // cpu 和 memory 如果没有错误 则不包含此字段
* 			"request": "3000000000m",
* 			"limit": "3"                       // cpu 单位为c时 省略单位c 1c=1000m
* 		},
* 		"memory": {
* 			"request": "300000M",
* 			"limit": "3G"
* 		}
* 	}
* }
*
* */

@Component({
  selector: 'rc-container-fields-resource-size',
  templateUrl: './container-resource-size.component.html',
  styleUrls: ['./container-resource-size.component.scss'],
  providers: [
    {
      provide: FormFieldControl,
      useExisting: ContainerFieldsResourceSizeComponent,
    },
  ],
})
export class ContainerFieldsResourceSizeComponent
  implements
    FormFieldControl,
    ControlValueAccessor,
    OnInit,
    OnDestroy,
    AfterViewInit {
  /***** FormFieldControl start *****/
  private _value: any;
  private _uniqueId = `rc-container-fields-resource-size-${++nextUniqueId}`;
  private _required = false;
  protected _disabled = false;

  @ViewChild('form')
  form: NgForm;

  @Input()
  id = this._uniqueId;

  stateChanges = new Subject<void>();

  @Input()
  get value(): Array<any> {
    return this.ngControl ? this.ngControl.value : this._value;
  }

  set value(value: Array<any>) {
    this._value = value;
    this.emitToModel();
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }
    this._disabled = disabled;
    this.stateChanges.next();
  }

  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);

    return !!(isInvalid && (isDirty || isSubmitted));
  }

  get errorInfo(): any {
    if (this.ngControl && this.ngControl.invalid) {
      return this.ngControl.errors;
    } else {
      return {};
    }
  }

  controlType = 'rc-container-fields-resource-size';

  /***** FormFieldControl end *****/
  numberReg = INT_PATTERN;
  private formSubscription: Subscription;
  memUnits = ['K', 'M', 'G', 'T', 'P', 'E'];
  cpuUnits = ['m', 'c'];
  defaultMemUnit = 'G';
  defaultCpuUnit = 'c';
  defaultMemVal: number;
  defaultCpuVal: number;

  resourceSize = {
    requests: {
      memory: {
        value: this.defaultMemVal,
        unit: this.defaultMemUnit,
      },
      cpu: {
        value: this.defaultCpuVal,
        unit: this.defaultCpuUnit,
      },
    },
    limits: {
      memory: {
        value: this.defaultMemVal,
        unit: this.defaultMemUnit,
      },
      cpu: {
        value: this.defaultCpuVal,
        unit: this.defaultCpuUnit,
      },
    },
  };

  @Output()
  valueChanges = new EventEmitter<Array<any>>();

  parseValueString(str: string, type: string) {
    if (str === '' || str === null || str === undefined) {
      let defaultUnit = '';
      switch (type) {
        case 'memory':
          defaultUnit = this.defaultMemUnit;
          break;
        case 'cpu':
          defaultUnit = this.defaultCpuUnit;
          break;
      }
      return {
        value: null,
        unit: defaultUnit,
      };
    } else {
      let result = str;
      if (type === 'cpu') {
        result = this.cpuUnitPadding(str);
      }
      return {
        value: parseInt(result, 10),
        unit: result.slice(-1),
      };
    }
  }

  cpuUnitPadding(str: string) {
    const unitIndex = this.cpuUnits.findIndex((item: any) => {
      return str.slice(-1) === item;
    });
    return unitIndex === -1 ? str + 'c' : str;
  }

  parseResourceSize(cs: any) {
    return {
      requests: {
        memory: this.parseValueString(
          _.get(cs, 'requests.memory', ''),
          'memory',
        ),
        cpu: this.parseValueString(_.get(cs, 'requests.cpu', '') + '', 'cpu'),
      },
      limits: {
        memory: this.parseValueString(_.get(cs, 'limits.memory', ''), 'memory'),
        cpu: this.parseValueString(_.get(cs, 'limits.cpu', '') + '', 'cpu'),
      },
    };
  }

  stringifyValue(obj: any) {
    return obj.value
      ? obj.unit === 'c'
        ? parseInt(obj.value, 10)
        : obj.value + obj.unit
      : '';
  }
  stringifyResourceSize(cs: any) {
    return {
      requests: {
        memory: this.stringifyValue(cs.requests.memory),
        cpu: this.stringifyValue(cs.requests.cpu),
      },
      limits: {
        memory: this.stringifyValue(cs.limits.memory),
        cpu: this.stringifyValue(cs.limits.cpu),
      },
    };
  }

  isRequestLessThanLimit(type: string, req: string, lim: string) {
    // 根据 单位units 和 单位换算倍数times 比较不同单位的值大小
    // 如果单位相同 直接比较值
    // 如果单位不同 较大单位的值*倍数 与 较小单位的值进行比较(如 1G*1024 与 100M 比较计算结果的数值)
    let request = req + '',
      limit = lim + '';
    if (!request || !limit) {
      return;
    }
    let times: number, units: any[];
    if (type === 'memory') {
      times = 1024;
      units = this.memUnits;
    } else if (type === 'cpu') {
      times = 1000;
      units = this.cpuUnits;
      request = this.cpuUnitPadding(request);
      limit = this.cpuUnitPadding(limit);
    }
    const requestVal = parseInt(request, 10),
      requestUnit = request.slice(-1),
      limitVal = parseInt(limit, 10),
      limitUnit = limit.slice(-1);

    if (!(requestVal && limitVal)) {
      return false;
    }
    const requestIndex = units.findIndex((itm: any) => {
      return itm === requestUnit;
    });
    const limitIndex = units.findIndex((itm: any) => {
      return itm === limitUnit;
    });
    if (requestIndex === limitIndex) {
      return requestVal <= limitVal;
    }
    if (requestIndex > limitIndex) {
      return requestVal * times <= limitVal;
    } else {
      return requestVal <= limitVal * times;
    }
  }
  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() private parentForm: NgForm,
    @Optional() private parentFormGroup: FormGroupDirective,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }
  async ngOnInit() {
    this.ngControl.control.setValidators((control: AbstractControl) => {
      const value = control.value;
      if (!value) {
        return;
      }
      // const error = {
      //   cpu: {},
      //   memory: {},
      // };
      const error = [];
      let memNotValid = false,
        cpuNotValid = false;
      if (value.requests.memory && value.limits.memory) {
        memNotValid = !this.isRequestLessThanLimit(
          'memory',
          value.requests.memory,
          value.limits.memory,
        );
        if (memNotValid) {
          // error.memory = {
          //   request: value.requests.memory,
          //   limit: value.limits.memory,
          // };
          error.push('memory');
        }
      }
      if (value.requests.cpu && value.limits.cpu) {
        cpuNotValid = !this.isRequestLessThanLimit(
          'cpu',
          value.requests.cpu,
          value.limits.cpu,
        );
        if (cpuNotValid) {
          // error.cpu = {
          //   request: value.requests.cpu,
          //   limit: value.limits.cpu,
          // };
          error.push('cpu');
        }
      }
      if (memNotValid || cpuNotValid) {
        // if (!memNotValid) {
        //   delete error.memory;
        // }
        // if (!cpuNotValid) {
        //   delete error.cpu;
        // }
        return {
          container_resource_size_invalid: error,
        };
      }
      return null;
    });
  }

  ngAfterViewInit() {
    this.formSubscription = this.form.valueChanges
      .pipe(debounceTime(300))
      .subscribe(() => {
        const value = _.clone(this.resourceSize);
        this._value = this.stringifyResourceSize(value);
        this.emitToModel();
      });
  }

  ngOnDestroy() {
    this.formSubscription.unsubscribe();
  }

  // emit _value to ngModel
  private emitToModel() {
    this.onTouchedCallback();
    this.onChangeCallback(this._value);
  }

  // apply _value to native view
  private applyToNative() {
    if (this._value === null || this._value === undefined) {
      return;
    }
    this.resourceSize = this.parseResourceSize(this._value);
  }

  /** Following Methods Implements ControlValueAccessor **/

  writeValue(value: any) {
    this._value = value;
    this.applyToNative();
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}
