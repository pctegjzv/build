import {
  Component,
  EventEmitter,
  Inject,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { get, remove } from 'lodash';

import { ContainerVolumeViewModel } from 'app2/features/service/service.type';
import {
  ConfigMapOption,
  ConfigmapService,
} from 'app2/shared/services/features/configmap.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { StorageService } from 'app2/shared/services/features/storage.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './container-volume-dialog.component.html',
  styleUrls: ['./container-volume.component.scss'],
})
export class ContainerVolumeDialogComponent implements OnInit, OnDestroy {
  @ViewChild('form')
  form: NgForm;
  @Output()
  close = new EventEmitter<any>();
  initialized: boolean;
  volumeOptions: any[];
  pvcOptions: any[];
  configMapOptions: ConfigMapOption[];
  configMapKeyOptions: string[];
  typeOptions: { name: string; value: string }[];
  viewModel: ContainerVolumeViewModel;

  private currentRegion: Region;
  private configMapKeyIndex = 0;
  private configMapKeyMap: {
    [key: string]: any;
  } = {};

  constructor(
    @Inject(MODAL_DATA)
    private modalData: {
      serviceUuid: string;
      namespaceName: string;
      data: ContainerVolumeViewModel;
      selectedPVC: string[];
    },
    private regionService: RegionService,
    private storageService: StorageService,
    private configMapService: ConfigmapService,
    private translateService: TranslateService,
  ) {
    this.typeOptions = [
      {
        name: this.translateService.get('host-path'),
        value: 'host-path',
      },
      {
        name: this.translateService.get('volume'),
        value: 'volume',
      },
      {
        name: this.translateService.get('pvc'),
        value: 'pvc',
      },
      {
        name: this.translateService.get('configmap'),
        value: 'configmap',
      },
    ];
    this.configMapKeyOptions = [];
    if (this.modalData.data) {
      const data = this.modalData.data;
      if (data.configMapKeyMap) {
        data.configMapKeyMap.forEach((item: any) => {
          item.index = this.configMapKeyIndex++;
        });
      } else {
        data.configMapKeyMap = [
          {
            index: this.configMapKeyIndex++,
            key: '',
            path: '',
          },
        ];
      }
      this.viewModel = data;
    } else {
      this.viewModel = {
        type: 'host-path',
        containerPath: '',
      };
    }
  }

  async ngOnInit() {
    this.currentRegion = await this.regionService.getCurrentRegion();
    await Promise.all([
      this.getVolumeOptions(),
      this.getPvcOptions(),
      this.getConfigMapOptions(),
    ]);
    this.initialized = true;
  }

  ngOnDestroy() {}

  typeChange() {
    this.resetViewModel();
    if (this.viewModel.type === 'configmap') {
      this.resetConfigMapKeyMap();
    }
  }

  volumeOptionChange(option: any) {
    this.viewModel.driverName = option.driverName;
    this.viewModel.driverVolumeId = option.driverVolumeId;
  }

  trackByKeyIndex(_index: number, key: any) {
    return key.index;
  }

  addKey() {
    this.viewModel.configMapKeyMap.push({
      index: this.configMapKeyIndex++,
      key: '',
      path: '',
    });
  }

  removeKey(index: number) {
    remove(this.viewModel.configMapKeyMap, (item: any) => {
      return item.index === index;
    });
  }

  async configMapChange(option: { name: string; uuid: string }) {
    if (!option || !option.uuid) {
      return;
    }
    if (option.uuid !== this.viewModel.configMapUuid) {
      this.resetConfigMapKeyMap();
    }
    if (this.configMapKeyMap[option.name]) {
      this.configMapKeyOptions = this.configMapKeyMap[option.name];
    } else {
      this.configMapKeyOptions = await this.getConfigMapKeyOptions(option.name);
    }
    this.viewModel.configMapUuid = option.uuid;
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    if (this.viewModel.configMapKeyRef) {
      this.viewModel.containerPath = '';
    }
    this.close.emit(this.viewModel);
  }

  cancel() {
    this.close.emit(null);
  }

  private resetViewModel() {
    Object.keys(this.viewModel).forEach((key: string) => {
      if (!['index', 'containerPath', 'type'].includes(key)) {
        this.viewModel[key] = null;
      }
    });
  }

  private resetConfigMapKeyMap() {
    this.viewModel.configMapKeyMap = [
      {
        index: this.configMapKeyIndex++,
        key: '',
        path: '',
      },
    ];
  }

  private async getVolumeOptions() {
    this.volumeOptions = await this.storageService
      .getVolumes({
        region_id: this.currentRegion.id,
        pageno: 1,
        size: 100,
        list_all: true,
      })
      .then((res: any) => {
        return res.volumes.map((item: any) => {
          const result = {
            name: item.name,
            uuid: item.id,
            driverName: item.driver_name,
            driverVolumeId: '',
          };
          if (item.driver_volume_id) {
            result.driverVolumeId = item.driver_volume_id;
          } else {
            delete result.driverVolumeId;
          }
          return result;
        });
      })
      .catch(() => []);
  }

  // TODO: @FIX - blocked by documention -> add filter()
  private async getPvcOptions() {
    this.pvcOptions = await this.storageService
      .getPvcs({
        cluster_id: this.currentRegion.id,
        page_size: 100,
      })
      .then((res: any) => {
        return res.results
          .filter((item: any) => {
            const status: string = get(
              item,
              'kubernetes.status.phase',
              '',
            ).toLowerCase();
            // const accessModes: any = item.kubernetes.spec.accessModes;
            // if (
            //   accessModes.includes('ReadWriteOnce') &&
            //   accessModes.length === 1 &&
            //   status === 'bound'
            // ) {
            //   const referencedByUuid = get(item, 'referenced_by[0].uuid', '');
            //   if (
            //     this.modalData.serviceUuid &&
            //     this.modalData.serviceUuid === referencedByUuid
            //   ) {
            //     return true;
            //   }
            //   return false;
            // }
            if (status === 'lost') {
              return false;
            }
            return true;
          })
          .map((item: any) => {
            return {
              name: item.kubernetes.metadata.name,
              uuid: item.kubernetes.metadata.uid,
              namespace_name: item.kubernetes.metadata.namespace,
            };
          })
          .filter((item: any) => {
            if (
              this.modalData.selectedPVC.includes(item.name) &&
              item.name !== this.viewModel.pvcName
            ) {
              return false;
            }
            return item.namespace_name === this.modalData.namespaceName;
          });
      })
      .catch(() => []);
  }

  private async getConfigMapOptions() {
    this.configMapOptions = await this.configMapService.getConfigMapOptions({
      clusterId: this.currentRegion.id,
      namespace: this.modalData.namespaceName,
    });
    if (this.viewModel && this.viewModel.type === 'configmap') {
      const item: {
        name: string;
        uuid: string;
      } = this.configMapOptions.find((item: any) => {
        return item.name === this.viewModel.configMapName;
      });
      if (item) {
        this.viewModel.configMapUuid = item.uuid;
      }
    }
  }

  private async getConfigMapKeyOptions(name: string) {
    const res = await this.configMapService.getK8sConfigmap({
      clusterId: this.currentRegion.id,
      namespace: this.modalData.namespaceName,
      name,
    });
    const data = get(res, 'kubernetes.data');
    const keys = data ? Object.keys(data) : [];
    this.configMapKeyMap[name] = keys;
    return keys;
  }
}
