import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  AfterViewInit,
  Component,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Self,
  ViewChild,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';
import { Subject } from 'rxjs';
import { first } from 'rxjs/operators';

import { ContainerVolumeDialogComponent } from 'app2/features/service/create-fields/container-volume-dialog.component';
import { ContainerVolumeViewModel } from 'app2/features/service/service.type';
import { FormFieldControl } from 'app2/shared/form-field-control';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import { cloneDeep, remove } from 'lodash';

let nextUniqueId = 0;
/*
* value:
* [
*   {
*     index: number,
*     type: 'host-path',     // 'host-path', 'volume', 'pvc'
*
*     hostPath: '',          // type为host-path时有
*
*     volumeName: '',        // type为volume时有以下3字段
*     driverName: '',
*     driverVolumeId: '',    // 此值不存在时 不保留此字段
*
*     pvcName: '',           // type为pvc时有
*
*     configMapName?: string;
*     configMapUuid?: string;
*     configMapKeyRef?: boolean;
      configMapKeyMap?: {
        index: number;
        key: string;
        path: string;
      }[];
*
*     containerPath: '',     // 3种类型都有
*   }
* ]
*
* */

@Component({
  selector: 'rc-container-fields-volume',
  templateUrl: './container-volume.component.html',
  styleUrls: ['./container-volume.component.scss'],
  providers: [
    {
      provide: FormFieldControl,
      useExisting: ContainerFieldsVolumeComponent,
    },
  ],
})
export class ContainerFieldsVolumeComponent
  implements
    FormFieldControl,
    ControlValueAccessor,
    OnInit,
    OnDestroy,
    AfterViewInit {
  /***** FormFieldControl start *****/
  private _value: any;
  private _uniqueId = `rc-container-fields-volume-${++nextUniqueId}`;
  private _required = false;
  protected _disabled = false;

  @ViewChild('form')
  form: NgForm;

  @Input()
  id = this._uniqueId;

  stateChanges = new Subject<void>();

  @Input()
  get value(): Array<any> {
    return this.ngControl ? this.ngControl.value : this._value;
  }

  set value(value: Array<any>) {
    this._value = value;
    this.emitToModel();
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }
    this._disabled = disabled;
    this.stateChanges.next();
  }

  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);

    return !!(isInvalid && (isDirty || isSubmitted));
  }

  controlType = 'rc-container-fields-volume';

  /***** FormFieldControl end *****/

  @Input()
  namespaceName: string;
  @Input()
  serviceUuid: string;
  private volumeIndex = 0;
  defaultType = 'host-path';
  volumeList: any[] = [
    {
      type: this.defaultType,
      path: '',
      containerPath: '',
    },
  ];

  selectedPVC: string[] = [];

  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() private parentForm: NgForm,
    @Optional() private parentFormGroup: FormGroupDirective,
    private translateService: TranslateService,
    private modalService: ModalService,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }
  async ngOnInit() {}

  ngAfterViewInit() {}

  ngOnDestroy() {}

  trackByIndex(_index: number, item: any) {
    return item.index;
  }

  getItemNameOrPathDisplay(item: ContainerVolumeViewModel) {
    switch (item.type) {
      case 'host-path':
        return item.hostPath;
      case 'volume':
        return item.volumeName;
      case 'pvc':
        return item.pvcName;
      case 'configmap':
        return item.configMapName;
    }
  }

  addItem() {
    const modalRef = this.modalService.open(ContainerVolumeDialogComponent, {
      width: 600,
      title: this.translateService.get('service_add_volume'),
      data: {
        serviceUuid: this.serviceUuid,
        namespaceName: this.namespaceName,
        selectedPVC: this.selectedPVC,
      },
    });
    modalRef.componentInstance.close
      .pipe(first())
      .subscribe((data: ContainerVolumeViewModel) => {
        if (data) {
          if (data.type === 'pvc' && !this.selectedPVC.includes(data.pvcName)) {
            this.selectedPVC.push(data.pvcName);
          }
          this.volumeList.push({
            index: this.volumeIndex++,
            ...data,
          });
        }
        modalRef.close();
        this.updateModelValue();
      });
  }

  updateItem(item: any) {
    const modalRef = this.modalService.open(ContainerVolumeDialogComponent, {
      width: 600,
      title: this.translateService.get('service_update_volume'),
      data: {
        serviceUuid: this.serviceUuid,
        namespaceName: this.namespaceName,
        data: cloneDeep(item),
        selectedPVC: this.selectedPVC,
      },
    });
    modalRef.componentInstance.close
      .pipe(first())
      .subscribe((data: ContainerVolumeViewModel) => {
        if (data) {
          if (data.type === 'pvc' && !this.selectedPVC.includes(data.pvcName)) {
            this.selectedPVC.push(data.pvcName);
          }
          const item = this.volumeList.find((_item: any) => {
            return _item.index === data.index;
          });
          Object.assign(item, data);
        }
        modalRef.close();
        this.updateModelValue();
      });
  }

  removeItem(index: number) {
    const removeItem = this.volumeList.find(
      (item: ContainerVolumeViewModel) => {
        return item.index === index;
      },
    );
    if (removeItem && removeItem.pvcName) {
      remove(this.selectedPVC, item => {
        return item === removeItem.pvcName;
      });
    }
    remove(this.volumeList, (item: ContainerVolumeViewModel) => {
      return item.index === index;
    });
    this.updateModelValue();
  }

  // set ngModel value when native view changes
  private updateModelValue() {
    // this._value = this.transferToSpecialFormat(this.volumeList);
    this._value = cloneDeep(this.volumeList);
    this.emitToModel();
  }

  // emit _value to ngModel
  private emitToModel() {
    this.onTouchedCallback();
    this.onChangeCallback(this._value);
  }

  // apply _value to native view
  private applyToNative() {
    if (this._value === null || this._value === undefined) {
      return;
    }
    this.volumeList = cloneDeep(this._value);
    this.volumeList.forEach((item: ContainerVolumeViewModel) => {
      item.index = this.volumeIndex++;
      if (item.pvcName && !this.selectedPVC.includes(item.pvcName)) {
        this.selectedPVC.push(item.pvcName);
      }
    });
  }

  /** Following Methods Implements ControlValueAccessor **/

  writeValue(value: any) {
    this._value = value;
    this.applyToNative();
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}
