import {
  AfterViewInit,
  Component,
  EventEmitter,
  Inject,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ServicePort } from 'app2/features/service/service.type';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import * as _ from 'lodash';

import {
  INT_PATTERN,
  K8S_RESOURCE_NAME_BASE,
} from '../../../../app/components/common/config/common-pattern';

@Component({
  templateUrl: './kube-service-dialog.component.html',
  styleUrls: ['./kube-service-dialog.component.scss'],
})
export class ServiceFieldsKubeServiceDialogComponent
  implements OnInit, OnDestroy, AfterViewInit {
  @Output()
  close = new EventEmitter<any>();
  @ViewChild('form')
  form: NgForm;

  portNumberReg = INT_PATTERN;
  resourceNameReg = K8S_RESOURCE_NAME_BASE;

  showRouteView = false;

  loadBalancerOptions: string[];
  containerPortsOptions: string[];
  data: {
    name: string;
    type: string;
    ports: ServicePort[];
  };

  types = ['ClusterIP', 'Headless', 'NodePort'];
  protocols = ['HTTP', 'TCP'];

  constructor(@Inject(MODAL_DATA) private modalData: any) {}

  async ngOnInit() {
    if (!this.modalData.data) {
      this.data = {
        name: '',
        type: 'ClusterIP',
        ports: [],
      };
    } else {
      this.data = this.formatInputData(_.cloneDeep(this.modalData.data));
    }
    this.containerPortsOptions = this.modalData.containerPortsOptions || [];
    this.loadBalancerOptions = this.modalData.loadBalancerOptions || [];
  }

  ngAfterViewInit() {}

  ngOnDestroy() {}

  trackByIndex(index: number) {
    return index;
  }

  addPort() {
    this.data.ports.push({
      containerPort: null,
      nodePort: null,
      loadBalancerName: '$$none',
      listenerPort: null,
      protocol: 'HTTP',
    });
  }

  removePort(index: number) {
    this.data.ports.splice(index, 1);
  }

  typeChange(type: string) {
    if (type !== 'NodePort') {
      this.data.ports.forEach((item: any) => {
        item.nodePort = '';
      });
    }
  }

  private formatInputData(data: any) {
    const _ports = data.ports.map((item: any) => {
      item.loadBalancerName = !item.loadBalancerName
        ? '$$none'
        : item.loadBalancerName;
      return item;
    });
    data.ports = _ports;
    return data;
  }

  private formatOutputData(data: any) {
    const _ports = data.ports.map((item: any) => {
      item.loadBalancerName =
        item.loadBalancerName === '$$none' ? '' : item.loadBalancerName;
      return item;
    });
    data.ports = _ports;
    return data;
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.close.next(this.formatOutputData(this.data));
  }

  cancel() {
    this.close.next(null);
  }
}
