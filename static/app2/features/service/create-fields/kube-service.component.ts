import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  HostBinding,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Optional,
  Self,
  SimpleChange,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';
import * as _ from 'lodash';
import { Subject } from 'rxjs';
import { first } from 'rxjs/operators';

import { ServiceFieldsKubeServiceDialogComponent } from 'app2/features/service/create-fields/kube-service-dialog.component';
import { ServiceViewModel } from 'app2/features/service/service.type';
import { FormFieldControl } from 'app2/shared/form-field-control';
import {
  LoadBalancer,
  NetworkService,
} from 'app2/shared/services/features/network.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';

// Increasing integer for generating unique ids for checkbox components.
let nextUniqueId = 0;

/**
 * value: [
 * {
 *  name: 'yyy',
 *  type: 'Headless'
 *  ports: [],
 * }]
 */
@Component({
  selector: 'rc-service-fields-kube-service',
  templateUrl: './kube-service.component.html',
  styleUrls: ['./kube-service.component.scss'],
  providers: [
    {
      provide: FormFieldControl,
      useExisting: ServiceFieldsKubeServiceComponent,
    },
  ],
})
export class ServiceFieldsKubeServiceComponent
  implements
    OnInit,
    OnDestroy,
    OnChanges,
    AfterViewInit,
    ControlValueAccessor,
    FormFieldControl {
  /***** FormFieldControl start *****/
  private _uniqueId = `rc-service-kube-service-${++nextUniqueId}`;
  private _value: any;
  private _required = false;
  protected _disabled = false;

  stateChanges = new Subject<void>();

  @Input()
  id = this._uniqueId;

  @Input()
  get value(): any {
    return this.ngControl ? this.ngControl.value : this._value;
  }

  set value(value: any) {
    this._value = value;
    this.emitToModel();
    this.applyToNative();
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }
    this._disabled = disabled;
    this.stateChanges.next();
  }

  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);

    return !!(isInvalid && (isDirty || isSubmitted));
  }

  controlType = 'rc-service-kube-service';
  /***** FormFieldControl end *****/
  @Input()
  serviceName: string;
  @Input()
  containerPortsOptions: string[];
  private loadBalancers: string[];
  private defaultServiceName = 'default-service';
  private touched = false;
  loadBalancerOptions: Array<{ name: string; value: string }>;
  kubeServices: ServiceViewModel[];

  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() protected parentForm: NgForm,
    @Optional() protected parentFormGroup: FormGroupDirective,
    @Optional() protected cdr: ChangeDetectorRef,
    private regionService: RegionService,
    private networkService: NetworkService,
    private modalService: ModalService,
    private translate: TranslateService,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
    this._value = null;
    this.loadBalancerOptions = [];
    this.containerPortsOptions = [];
    this.loadBalancers = [];
    this.kubeServices = [];
  }

  async ngOnInit() {
    const currentRegion: Region = await this.regionService.getCurrentRegion();
    const response: LoadBalancer[] = await this.networkService.getList({
      region_name: currentRegion.name,
    });
    this.loadBalancers = response.map((item: LoadBalancer) => {
      return item.name;
    });
    const loadBalancerOptions = response.map((item: LoadBalancer) => {
      return {
        name: item.name,
        value: item.name,
      };
    });
    loadBalancerOptions.unshift({
      name: this.translate.get('select_none'),
      value: '$$none',
    });
    this.loadBalancerOptions = loadBalancerOptions;
    if (!this.kubeServices.length) {
      this.updateDefaultService();
    }
  }

  ngOnChanges({
    containerPortsOptions,
    serviceName,
  }: {
    containerPortsOptions: SimpleChange;
    serviceName: SimpleChange;
  }) {
    if (
      containerPortsOptions &&
      containerPortsOptions.currentValue &&
      containerPortsOptions.currentValue.length
    ) {
      this.updateDefaultService();
    }
    if (serviceName && serviceName.currentValue) {
      this.updateDefaultService();
    }
  }

  ngAfterViewInit() {}

  ngOnDestroy() {}

  async addKubeService() {
    const modalRef = await this.modalService.open(
      ServiceFieldsKubeServiceDialogComponent,
      {
        width: 850,
        title: this.translate.get('add_kube_service'),
        data: {
          containerPortsOptions: this.containerPortsOptions,
          loadBalancerOptions: this.loadBalancerOptions,
        },
      },
    );
    modalRef.componentInstance.close.pipe(first()).subscribe(res => {
      if (res) {
        this._addKubeService(res);
      }
      modalRef.close();
    });
  }

  removeKubeService(index: number) {
    this.touched = true;
    this.kubeServices.splice(index, 1);
    this.updateModelValue();
  }

  async updateKubeService(index: number, service: any) {
    const modalRef = await this.modalService.open(
      ServiceFieldsKubeServiceDialogComponent,
      {
        width: 850,
        title: this.translate.get('update'),
        data: {
          containerPortsOptions: this.containerPortsOptions,
          loadBalancerOptions: this.loadBalancerOptions,
          data: service,
        },
      },
    );
    modalRef.componentInstance.close.pipe(first()).subscribe(res => {
      if (res) {
        this._updateKubeService(index, res);
      }
      modalRef.close();
    });
  }

  private updateDefaultService() {
    if (
      this.touched ||
      !this.containerPortsOptions.length ||
      !this.loadBalancers.length
    ) {
      return;
    }
    const defaultKubeService = this.generateDefaultService();
    if (!this.kubeServices.length) {
      this.kubeServices.push(defaultKubeService);
    } else if (this.kubeServices.length === 1) {
      this.kubeServices[0] = defaultKubeService;
    }
    setTimeout(() => {
      this.updateModelValue();
    });
  }

  private generateDefaultService() {
    const name = this.serviceName ? this.serviceName : this.defaultServiceName;
    const defaultService: any = {
      name,
      type: 'ClusterIP',
      ports: [
        {
          containerPort: this.containerPortsOptions[0],
          nodePort: '',
          loadBalancerName: this.loadBalancers[0],
          listenerPort: 80,
          protocol: 'HTTP',
        },
      ],
    };

    return defaultService;
  }

  private _addKubeService(item: any) {
    this.touched = true;
    this.kubeServices.push(item);
    this.updateModelValue();
  }

  private _updateKubeService(index: number, data: any) {
    this.touched = true;
    this.kubeServices[index] = data;
    this.updateModelValue();
  }

  // set ngModel value when native view changes
  private updateModelValue() {
    this._value = _.cloneDeep(this.kubeServices);
    this.emitToModel();
  }

  // emit _value to ngModel
  private emitToModel() {
    this.onTouchedCallback();
    this.onChangeCallback(this._value);
  }

  // apply _value to native view
  private applyToNative() {
    if (this._value) {
      this.kubeServices = _.cloneDeep(this._value);
    }
  }

  /** Following Methods Implements ControlValueAccessor **/

  writeValue(value: any) {
    this._value = value;
    this.applyToNative();
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}
