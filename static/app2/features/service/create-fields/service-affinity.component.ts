import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  ChangeDetectorRef,
  Component,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Self,
  ViewChild,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';
import * as _ from 'lodash';
import { Subject } from 'rxjs';

import { MultiSelectionDropdownComponent } from 'app2/shared/components/select/multi-selection-dropdown.component';
import { FormFieldControl } from 'app2/shared/form-field-control';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { delay } from 'app2/testing/helpers';
import { TranslateService } from 'app2/translate/translate.service';

// Increasing integer for generating unique ids for checkbox components.
let nextUniqueId = 0;

/**
 * value: {
 *  affinityServices: [{name: "feae354f-0cb6-4b09-bf67-d7dfe0d273d9", namespace: "default"}],
 *  antiAffinityServices: [{name: "feae354f-0cb6-4b09-bf67-d7dfe0d273d9", namespace: "default"}]
 * }
 */
@Component({
  selector: 'rc-service-fields-affinity',
  templateUrl: './service-affinity.component.html',
  styleUrls: ['./service-affinity.component.scss'],
  providers: [
    {
      provide: FormFieldControl,
      useExisting: ServiceFieldsAffinityComponent,
    },
  ],
})
export class ServiceFieldsAffinityComponent
  implements OnInit, OnDestroy, ControlValueAccessor, FormFieldControl {
  /***** FormFieldControl *****/
  private _uniqueId = `rc-service-affinity-${++nextUniqueId}`;
  private _value: any;
  private _required = false;
  protected _disabled = false;

  stateChanges = new Subject<void>();

  @Input()
  id = this._uniqueId;

  @Input()
  get value(): any {
    return this.ngControl ? this.ngControl.value : this._value;
  }

  set value(value: any) {
    this._value = value;
    this.emitToModel();
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }
    this._disabled = disabled;
    this.stateChanges.next();
  }

  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);

    return !!(isInvalid && (isDirty || isSubmitted));
  }

  controlType = 'rc-service-affinity';
  /***** FormFieldControl *****/
  @Input()
  self: any; // self service
  @ViewChild('affinitySelect')
  affinitySelect: MultiSelectionDropdownComponent;
  @ViewChild('antiAffinitySelect')
  antiAffinitySelect: MultiSelectionDropdownComponent;

  private affinityServices: any[] = [];
  private antiAffinityServices: any[] = [];

  affinityServiceOptions: any[] = [];
  antiAffinityServiceOptions: any[] = [];

  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() protected parentForm: NgForm,
    @Optional() protected parentFormGroup: FormGroupDirective,
    @Optional() protected cdr: ChangeDetectorRef,
    private regionService: RegionService,
    private serviceService: ServiceService,
    private translateService: TranslateService,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
    this._value = null;
  }

  async ngOnInit() {
    const currentRegion: Region = await this.regionService.getCurrentRegion();
    const response = await this.serviceService.getK8sServices({
      pageSize: 999,
      params: {
        cluster: currentRegion.name,
      },
    });
    this.initServiceOptions(response.results);
  }

  ngOnDestroy() {}

  async initServiceOptions(services: any[]) {
    const serviceOptions = services.map((service: any) => {
      return {
        name: service.resource.name,
        uuid: service.resource.uuid,
        namespace: service.namespace.name,
      };
    });

    if (!this.self) {
      // 创建服务
      this.affinityServiceOptions = _.cloneDeep(serviceOptions);
      this.antiAffinityServiceOptions = [
        {
          uuid: '$$self',
          name: this.translateService.get('affinity_service_self'),
        },
      ].concat(_.cloneDeep(serviceOptions));
    } else {
      // 更新服务，需要初始化dropdown选项
    }
    if (this._value) {
      await delay(0);
      const affinityServices: string[] = _.get(
        this._value,
        'affinityServices',
        [],
      );
      const antiAffinityServices: string[] = _.get(
        this._value,
        'antiAffinityServices',
        [],
      );
      this.affinitySelect.setSelectedValue(
        affinityServices.map((item: any) => {
          return item.name;
        }),
      );
      this.antiAffinitySelect.setSelectedValue(
        antiAffinityServices.map((item: any) => {
          return item.name;
        }),
      );
    }
  }

  addAffinityService(option: any) {
    this.affinityServices = _.chain(this.affinityServices)
      .concat(option)
      .uniqBy('name')
      .value();
    this.antiAffinityServiceOptions.forEach((item: any) => {
      if (item.uuid === option.uuid) {
        item['$$rcItemDisabled'] = true;
      }
    });
    this.updateModelValue();
  }

  removeAffinityService(option: any) {
    _.remove(this.affinityServices, (item: any) => {
      return item.name === option.name;
    });
    this.antiAffinityServiceOptions.forEach((item: any) => {
      if (item.name === option.name) {
        item['$$rcItemDisabled'] = false;
      }
    });
    this.updateModelValue();
  }

  addAntiAffinityService(option: any) {
    this.antiAffinityServices = _.chain(this.antiAffinityServices)
      .concat(option)
      .uniqBy('name')
      .value();
    this.affinityServiceOptions.forEach((item: any) => {
      if (item.name === option.name) {
        item['$$rcItemDisabled'] = true;
      }
    });
    this.updateModelValue();
  }

  removeAntiAffinityService(option: any) {
    _.remove(this.antiAffinityServices, (item: any) => {
      return item.name === option.name;
    });
    this.affinityServiceOptions.forEach((item: any) => {
      if (item.name === option.name) {
        item['$$rcItemDisabled'] = false;
      }
    });
    this.updateModelValue();
  }

  // set ngModel value when native view changes
  private updateModelValue() {
    this._value = {
      affinityServices: this.affinityServices.map((item: any) => {
        return {
          name: item.name,
          uuid: item.uuid,
          namespace: item.namespace,
        };
      }),
      antiAffinityServices: this.antiAffinityServices.map((item: any) => {
        return {
          name: item.name,
          uuid: item.uuid,
          namespace: item.namespace,
        };
      }),
    };
    this.emitToModel();
  }

  // emit _value to ngModel
  private emitToModel() {
    this.onTouchedCallback();
    this.onChangeCallback(this._value);
  }

  // apply _value to native view
  private applyToNative() {}

  /** Following Methods Implements ControlValueAccessor **/

  writeValue(value: any) {
    this._value = value;
    this.applyToNative();
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}
