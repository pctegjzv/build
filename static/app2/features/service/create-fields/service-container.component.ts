import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  HostBinding,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  Self,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';
import { MessageService } from 'alauda-ui';
import { ACCOUNT, WEBLABS } from 'app2/core/tokens';
import { RcAccount, Weblabs } from 'app2/core/types';
import { ImageSelectComponent } from 'app2/features/service/image-select/image-select.component';
import {
  ContainerViewModel,
  RcImageSelection,
} from 'app2/features/service/service.type';
import { FormFieldControl } from 'app2/shared/form-field-control';
import {
  ImageRepositoryService,
  ImageRepositoryTag,
} from 'app2/shared/services/features/image-repository.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import * as _ from 'lodash';
import { Subject, Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { K8S_RESOURCE_NAME_BASE } from '../../../../app/components/common/config/common-pattern';

// Increasing integer for generating unique ids for checkbox components.
let nextUniqueId = 0;

/**
 * value: [{name: 'yyy', value: 'xxx'}]
 */
@Component({
  selector: 'rc-service-container-fields',
  templateUrl: './service-container.component.html',
  styleUrls: [
    '../create-ui/service-create-ui.common.scss',
    './service-container.component.scss',
  ],
  providers: [
    {
      provide: FormFieldControl,
      useExisting: ServiceContainerFieldsComponent,
    },
  ],
})
export class ServiceContainerFieldsComponent
  implements
    OnInit,
    OnDestroy,
    AfterViewInit,
    ControlValueAccessor,
    FormFieldControl {
  /***** FormFieldControl start *****/
  private _uniqueId = `rc-service-container-${++nextUniqueId}`;
  private _value: any;
  private _required = false;
  protected _disabled = false;

  stateChanges = new Subject<void>();

  @Input()
  id = this._uniqueId;

  @Input()
  get value(): any {
    return this.ngControl ? this.ngControl.value : this._value;
  }

  set value(value: any) {
    this._value = value;
    this.emitToModel();
    this.applyToNative();
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }
    this._disabled = disabled;
    this.stateChanges.next();
  }

  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);

    return !!(isInvalid && (isDirty || isSubmitted));
  }

  controlType = 'rc-service-container';
  /***** FormFieldControl end *****/
  resourceNameReg = K8S_RESOURCE_NAME_BASE;
  @Input()
  namespace = ''; // selected namespace in service basic form
  @Input()
  containerIndex = 0;
  @Input()
  serviceUuid: string;
  @Output()
  portsChange = new EventEmitter<string[]>();
  @Output()
  nameChange = new EventEmitter<string>();

  @ViewChild('containerForm')
  containerForm: NgForm;
  @ViewChild('resourceSize')
  resourceSize: NgForm;
  showAdvanceOptions = false;
  private imageSelection: RcImageSelection;

  container: ContainerViewModel;
  imageTags: any[] = [];
  imageTagsLoading: boolean;

  formSubscription: Subscription;
  errorMapper: any;

  constructor(
    // private fb: FormBuilder,
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() protected parentForm: NgForm,
    @Optional() protected parentFormGroup: FormGroupDirective,
    @Optional() protected cdr: ChangeDetectorRef,
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(ACCOUNT) public account: RcAccount,
    private translateService: TranslateService,
    private serviceService: ServiceService,
    private repositoryService: ImageRepositoryService,
    private modal: ModalService,
    private auiMessageService: MessageService,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }

    this._value = this.container = {
      name: '',
      image: '',
      imageTag: '',
      resources: null, // 容器大小，包括最大值和参考值
      volumes: [],
      healthCheck: {},
      entrypoint: '',
      cmd: [],
      envvars: [],
      configmaps: [],
      logFiles: [],
      excludeLogFiles: [],
    };
    // custom error mapper
    this.errorMapper = {
      map: this.errorMap.bind(this),
    };
  }

  private errorMap(key: string, error: any): string {
    switch (key) {
      case 'container_resource_size_invalid':
        const types = error.join(',');
        return `${types} ${this.translateService.get(
          'requests_should_less_than_limits',
        )}`;
    }
  }

  async ngOnInit() {
    this.ngControl.control.setValidators((control: AbstractControl) => {
      const value = control.value;
      if (!value) {
        return null;
      }
      if (!value.name) {
        return {
          container_filds_missing: {
            name: 'name',
          },
        };
      }
      if (!value.image) {
        return {
          container_filds_missing: {
            name: 'image',
          },
        };
      }
      if (!value.imageTag) {
        return {
          container_filds_missing: {
            name: 'imageTag',
          },
        };
      }
      return this.resourceSize.form.invalid
        ? { 'service-container': true }
        : null;
    });
  }

  ngAfterViewInit() {
    this.formSubscription = this.containerForm.valueChanges.subscribe(() => {
      setTimeout(() => {
        this._value = this.container;
        this.emitToModel();
      });
    });
  }

  ngOnDestroy() {
    if (this.formSubscription) {
      this.formSubscription.unsubscribe();
    }
  }

  trackByFn(index: number) {
    return index;
  }

  containerNameChange(name: string) {
    this.setContainerName(name);
  }

  async openImageSelectDialog() {
    try {
      const modalRef = await this.modal.open(ImageSelectComponent, {
        title: this.translateService.get('nav_select_image'),
        width: 800,
      });
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe((res: RcImageSelection) => {
          modalRef.close();
          if (res) {
            this.imageChange(res);
          }
        });
    } catch (error) {
      //
    }
  }

  imageTagChange(tag: string) {
    if (!tag) {
      return;
    }
    this.getImageTagDetail(tag);
  }

  private imageChange(params: RcImageSelection) {
    this.imageTags = [];
    this.container.image = params.full_image_name;
    this.container.imageTag = '';
    this.container.cmd = [];
    this.container.entrypoint = '';
    this.getImageTags(params);
  }

  private async getImageTags(params: RcImageSelection) {
    this.imageSelection = params;
    this.imageTagsLoading = true;
    let tags: any;
    if (params.registry_name) {
      tags = await this.repositoryService
        .getRepositoryTags({
          registry_name: params.registry_name,
          project_name: params.project_name,
          repository_name: params.repository_name,
          view_type: 'security',
          page_size: 0,
        })
        .then(({ results }: { results: ImageRepositoryTag[] }) => {
          if (!results) {
            results = [];
          }
          return results.map((tag: ImageRepositoryTag) => {
            const level = tag.level || 'unscanned';
            const className = 'rb-tag-level-' + level.toLowerCase();
            const levelName = this.translateService.get(
              'image_repository_scan_status_' + level.toLowerCase(),
            );
            return {
              tag_name: tag.tag_name,
              tag_display: `${
                tag.tag_name
              }<span class="${className} margin-left-1">(${levelName})</span>`,
            };
          });
        });
    } else {
      tags = await this.repositoryService
        .getPublicRepositoryTags(params.full_image_name)
        .then((tags: string[]) => {
          return tags.map((tag: string) => {
            return {
              tag_name: tag,
              tag_display: `<span>${tag}</span>`,
            };
          });
        });
    }
    if (!tags || !tags.length) {
      this.auiMessageService.warning({
        content: this.translateService.get('get_image_tags_failed'),
      });
    }
    this.imageTags = tags;
    this.imageTagsLoading = false;
    this.setDefaultImageTag();
  }

  private setDefaultImageTag() {
    if (!this.container.imageTag && this.imageTags && this.imageTags.length) {
      const imageTag = _.get(this.imageTags, '[0].tag_name', '');
      this.container.imageTag = imageTag;
      this.getImageTagDetail(imageTag);
    }
  }

  private async getImageTagDetail(tag: string) {
    if (!tag) {
      return;
    }
    let data: any;
    if (this.imageSelection.registry_name) {
      data = await this.repositoryService.getRepositoryTagDetail({
        registry_name: this.imageSelection.registry_name,
        project_name: this.imageSelection.project_name,
        repository_name: this.imageSelection.repository_name,
        tag_name: tag,
      });
    } else {
      data = await this.repositoryService.getPublicRepositoryTagDetail({
        image_name: this.imageSelection.full_image_name,
        tag_name: tag,
      });
    }
    if (data) {
      if (data.instance_ports) {
        this.portsChange.emit(data.instance_ports);
      }
    }
  }

  private async getImageTagsFromRawImage(image: string) {
    const params: RcImageSelection = await this.serviceService.getRepositoryParamsFromRawImageName(
      image,
    );
    this.getImageTags(params);
  }

  private setContainerName(name: string) {
    this.container.name = name;
    this.nameChange.emit(name);
  }

  // emit _value to ngModel
  private emitToModel() {
    this.onTouchedCallback();
    this.onChangeCallback(this._value);
  }

  // apply _value to native view
  private applyToNative() {
    if (this._value) {
      this.container = this._value;
      if (this.container.image) {
        this.getImageTagsFromRawImage(this.container.image);
      }
    }
  }

  triggerSubmit() {
    this.containerForm.onSubmit(null);
  }

  /** Following Methods Implements ControlValueAccessor **/

  writeValue(value: any) {
    if (!value) {
      return;
    }
    if (this._value) {
      Object.assign(this._value, value);
    } else {
      this._value = value;
    }
    this.applyToNative();
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}
