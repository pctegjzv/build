import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  ChangeDetectorRef,
  Component,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Self,
  ViewChild,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';
import { FormFieldControl } from 'app2/shared/form-field-control';
import * as _ from 'lodash';
import { Subject } from 'rxjs';

// Increasing integer for generating unique ids for checkbox components.
let nextUniqueId = 0;

/**
 * value: [{name: 'yyy', value: 'xxx'}]
 */
@Component({
  selector: 'rc-service-fields-labels',
  templateUrl: './service-labels.component.html',
  styleUrls: ['./service-labels.component.scss'],
  providers: [
    {
      provide: FormFieldControl,
      useExisting: ServiceFieldsLabelsComponent,
    },
  ],
})
export class ServiceFieldsLabelsComponent
  implements OnInit, OnDestroy, ControlValueAccessor, FormFieldControl {
  /***** FormFieldControl start *****/
  private _uniqueId = `rc-service-labels-${++nextUniqueId}`;
  private _value: any;
  private _required = false;
  protected _disabled = false;

  stateChanges = new Subject<void>();

  @Input()
  id = this._uniqueId;

  @Input()
  get value(): any {
    return this.ngControl ? this.ngControl.value : this._value;
  }

  set value(value: any) {
    this._value = value;
    this.emitToModel();
    this.applyToNative();
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }
    this._disabled = disabled;
    this.stateChanges.next();
  }

  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);

    return !!(isInvalid && (isDirty || isSubmitted));
  }

  controlType = 'rc-service-labels';
  /***** FormFieldControl end *****/

  @ViewChild('form')
  form: NgForm;
  label: any;
  labels: any[] = [];

  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() protected parentForm: NgForm,
    @Optional() protected parentFormGroup: FormGroupDirective,
    @Optional() protected cdr: ChangeDetectorRef,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
    this._value = null;
  }

  async ngOnInit() {
    this.label = {
      name: '',
      value: '',
    };
  }

  ngOnDestroy() {}

  addLabel() {
    this.form.onSubmit(null);
    if (!this.label.name || !this.label.value) {
      return;
    }
    if (this.form.form.invalid) {
      return;
    }
    const label = _.find(this.labels, item => {
      return item.name === this.label.name;
    });
    if (label) {
      label.value = this.label.value;
    } else {
      this.labels.push({
        name: this.label.name,
        value: this.label.value,
      });
    }
    this.form.resetForm();
    this.updateModelValue();
  }

  removeLabel(index: number) {
    this.labels.splice(index, 1);
    this.updateModelValue();
  }

  // set ngModel value when native view changes
  private updateModelValue() {
    this._value = _.cloneDeep(this.labels);
    this.emitToModel();
  }

  // emit _value to ngModel
  private emitToModel() {
    this.onTouchedCallback();
    this.onChangeCallback(this._value);
  }

  // apply _value to native view
  private applyToNative() {
    if (this._value) {
      this.labels = _.cloneDeep(this._value);
    }
  }

  /** Following Methods Implements ControlValueAccessor **/

  writeValue(value: any) {
    this._value = value;
    this.applyToNative();
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}
