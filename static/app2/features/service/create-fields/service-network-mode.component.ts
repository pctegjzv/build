import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  NgForm,
} from '@angular/forms';
import { clone, get } from 'lodash';
import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { NetworkModeViewModel } from 'app2/features/service/service.type';
import {
  K8sSubnet,
  K8sSubnetIp,
  NetworkService,
  Subnet,
  SubnetIp,
} from 'app2/shared/services/features/network.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';

/**
 * value:
 * {
 *    hostNetwork: false | true,
 *    subnet: { name: '', ipAddress: [] } | null,
 * }
 */

@Component({
  selector: 'rc-service-fields-network-mode',
  templateUrl: './service-network-mode.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: ServiceFieldsNetworkModeComponent,
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: ServiceFieldsNetworkModeComponent,
      multi: true,
    },
  ],
})
export class ServiceFieldsNetworkModeComponent
  implements ControlValueAccessor, OnInit, OnDestroy, AfterViewInit {
  private _value: any;
  @ViewChild('form')
  form: NgForm;
  private formSubscription: Subscription;
  onChange: (_: any) => void;
  onTouch: (_: any) => void;

  networkMode: NetworkModeViewModel = {
    hostNetwork: false,
    subnet: null,
  };

  showMacvlanOptions: boolean;
  ipAssigned = false;
  currentNetworkMode = ''; // 'MacVlan' | 'Host'
  networkModeOptions = ['MacVlan', 'Host'];
  subnetOptions: { subnet_name: string }[] = [];
  subnetIpOptions: { address: string }[] = [];
  subnetIpIsLoading: boolean;
  newSubnetApiEnabled = false;
  region: Region;

  constructor(
    private regionService: RegionService,
    private networkService: NetworkService,
    private cdr: ChangeDetectorRef,
    private parentForm: NgForm,
  ) {}

  async ngOnInit() {
    await this.checkIfMacvlanRegion();
    if (this.showMacvlanOptions) {
      if (this.networkMode.hostNetwork) {
        this.currentNetworkMode = 'Host';
      } else {
        this.currentNetworkMode = 'MacVlan';
        this.setNetworkOptions();
      }
    }
  }

  ngAfterViewInit() {
    this.parentForm.ngSubmit.subscribe((e: any) => this.form.onSubmit(e));
    this.formSubscription = this.form.valueChanges
      .pipe(debounceTime(300))
      .subscribe(() => {
        const value = clone(this.networkMode);
        this._value = this.formatOutputValue(value);
        this.emitToModel();
      });
    this.cdr.markForCheck();
  }

  ngOnDestroy(): void {
    this.formSubscription.unsubscribe();
  }

  validate(_control: AbstractControl): { [key: string]: any } | null {
    return this.form.invalid ? { 'service-network-mode': true } : null;
  }

  async checkIfMacvlanRegion() {
    this.region = await this.regionService.getCurrentRegion();
    const regionType: string = get(
      this.region,
      'features.kubernetes.cni.type',
      '',
    );
    this.showMacvlanOptions = regionType === 'macvlan';
  }

  private setNetworkOptions() {
    if (this.networkMode.subnet && this.networkMode.subnet.name) {
      this.ipAssigned = true;
      this.setSubnetOptions();
      this.setSubnetIpsOptions(this.networkMode.subnet.name);
    }
  }

  private async setSubnetOptions() {
    this.newSubnetApiEnabled = await this.networkService.subnetNewApiEnabled(
      this.region.id,
    );
    try {
      if (this.newSubnetApiEnabled) {
        const res = await this.networkService.getK8sSubnets(this.region.id);
        this.subnetOptions =
          res &&
          res.map((subnet: K8sSubnet) => {
            return { subnet_name: subnet.kubernetes.metadata.name };
          });
      } else {
        const res = await this.networkService.getSubnets(
          this.region.name || '',
        );
        this.subnetOptions =
          res &&
          res.map((subnet: Subnet) => {
            return { subnet_name: subnet.subnet_name };
          });
      }
    } catch (err) {
      // ..
    }
  }

  private async setSubnetIpsOptions(subnetName: string) {
    this.subnetIpIsLoading = true;
    try {
      if (this.newSubnetApiEnabled) {
        const res = await this.networkService.getK8sSubnetIps(
          this.region.id,
          subnetName,
        );
        this.subnetIpOptions =
          res &&
          res.map((ip: K8sSubnetIp) => {
            return { address: ip.kubernetes.metadata.name };
          });
      } else {
        const res = await this.networkService.getSubnetIps(subnetName);
        this.subnetIpOptions =
          res &&
          res.map((ip: SubnetIp) => {
            return { address: ip.address };
          });
      }
    } catch (err) {
      // ..
    }
    this.subnetIpIsLoading = false;
  }

  onSubnetOptionChange(option: Subnet) {
    this.setSubnetIpsOptions((option && option.subnet_name) || '');
  }

  onNetworkModeChange(mode: string) {
    if (mode === 'Host') {
      // reset
      this.networkMode.hostNetwork = true;
      this.networkMode.subnet = null;
      this.ipAssigned = false;
    } else {
      this.networkMode.hostNetwork = false;
      if (!this.networkMode.subnet) {
        this.networkMode.subnet = {
          name: '',
          ipAddress: [],
        };
        this.setSubnetOptions();
      }
    }
  }

  private formatOutputValue(value: any) {
    if (!this.ipAssigned || this.currentNetworkMode !== 'MacVlan') {
      value.subnet = null;
    }
    return value;
  }

  private emitToModel() {
    this.onChange(this._value);
  }

  private applyToNative() {
    if (this._value === null || this._value === undefined) {
      return;
    }
    this.networkMode = this._value;
  }

  /* implement `ControlValueAccessor` interface */
  writeValue(value: any) {
    this._value = value;
    this.applyToNative();
  }

  registerOnChange(fn: (_: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: (_: any) => void): void {
    this.onTouch = fn;
  }
}
