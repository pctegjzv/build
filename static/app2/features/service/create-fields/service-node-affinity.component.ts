import { Component, OnInit, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { reduce } from 'lodash';

import { NamespaceService } from 'app2/shared/services/features/namespace.service';
import { RegionService } from 'app2/shared/services/features/region.service';

/**
 * value: ['ip:1.1.1.1', 'global:rubick', 'es:xxx']
 */
@Component({
  selector: 'rc-service-fields-node-affinitys',
  templateUrl: './service-node-affinity.component.html',
  styleUrls: ['./service-node-affinity.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ServiceFieldsNodeAffinityComponent),
      multi: true,
    },
  ],
})
export class ServiceFieldsNodeAffinityComponent
  implements OnInit, ControlValueAccessor {
  private value: any;

  nodeAffinityOption: any[] = [];
  loading: boolean;
  validNodeAffinitysLength: number;
  nodeAffinitysCheckPass: boolean;
  nodeAffinitysCheckError: boolean;

  constructor(
    private regionService: RegionService,
    private namespaceService: NamespaceService,
  ) {}

  async ngOnInit() {
    this.loading = true;
    const userNamespace = await this.namespaceService.getCurrentUserNamespace();
    const result = await this.regionService.getRegionLabelsV2(
      userNamespace.cluster_name,
    );
    const nodeAffinityOption: any = [];
    for (const key in result) {
      if (result.hasOwnProperty(key)) {
        result[key].map((label: any) => {
          const existAffinity = nodeAffinityOption.findIndex((_node: any) => {
            return _node.affinity === `${label.key}:${label.value}`;
          });
          if (existAffinity < 0) {
            nodeAffinityOption.push(
              Object.assign(label, {
                display: `${label.key}: ${label.value}`,
                affinity: `${label.key}:${label.value}`,
                host: [key],
              }),
            );
          } else {
            nodeAffinityOption[existAffinity].host.push(key);
          }
        });
      }
    }
    this.nodeAffinityOption = nodeAffinityOption;
    this.loading = false;
  }

  onNodeAffinitySelect() {
    this.resetCheckStatus();
    this.emitToModel();
  }

  checkNodeAffinitysDisabled() {
    return !this.value || !this.value.length;
  }

  private nodeFilter(result: any, value: any) {
    const _result = new Set(result);
    const _value = new Set(value);
    return [..._value].filter(x => _result.has(x));
  }

  checkNodeAffinitys() {
    const affinityKeyMap = {};
    let hostList: any = [];
    try {
      this.nodeAffinityOption.map((item: any) => {
        if (this.value.includes(item.affinity)) {
          if (affinityKeyMap.hasOwnProperty(item.key)) {
            affinityKeyMap[item.key] = affinityKeyMap[item.key].concat(
              item.host,
            );
          } else {
            affinityKeyMap[item.key] = item.host;
          }
          hostList = hostList.concat(item.host);
        }
      });
      const resultHost = reduce(affinityKeyMap, this.nodeFilter, hostList);
      this.nodeAffinitysCheckPass = true;
      this.validNodeAffinitysLength = resultHost.length;
    } catch (err) {
      this.nodeAffinitysCheckError = true;
    }
  }

  private resetCheckStatus() {
    this.validNodeAffinitysLength = 0;
    this.nodeAffinitysCheckPass = false;
    this.nodeAffinitysCheckError = false;
  }

  // emit _value to ngModel
  private emitToModel() {
    this.onTouchedCallback();
    this.onChangeCallback(this.value);
  }

  /** Following Methods Implements ControlValueAccessor **/

  writeValue(value: any) {
    this.value = value;
    this.emitToModel();
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}
