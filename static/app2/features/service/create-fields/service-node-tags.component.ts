import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  ChangeDetectorRef,
  Component,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Self,
  ViewChild,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';
import { remove } from 'lodash';
import { Subject } from 'rxjs';

import { MultiSelectionDropdownComponent } from 'app2/shared/components/select/multi-selection-dropdown.component';
import { FormFieldControl } from 'app2/shared/form-field-control';
import { RegionService } from 'app2/shared/services/features/region.service';
import { delay } from 'app2/testing/helpers';

// Increasing integer for generating unique ids for checkbox components.
let nextUniqueId = 0;

/**
 * value: ['ip:1.1.1.1', 'global:rubick', 'es:xxx']
 */
@Component({
  selector: 'rc-service-fields-node-tags',
  templateUrl: './service-node-tags.component.html',
  styleUrls: ['./service-node-tags.component.scss'],
  providers: [
    {
      provide: FormFieldControl,
      useExisting: ServiceFieldsNodeTagsComponent,
    },
  ],
})
export class ServiceFieldsNodeTagsComponent
  implements OnInit, OnDestroy, ControlValueAccessor, FormFieldControl {
  /***** FormFieldControl start *****/
  private _uniqueId = `rc-service-node-tags-${++nextUniqueId}`;
  private _value: any;
  private _required = false;
  protected _disabled = false;

  stateChanges = new Subject<void>();

  @Input()
  id = this._uniqueId;

  @Input()
  get value(): any {
    return this.ngControl ? this.ngControl.value : this._value;
  }

  set value(value: any) {
    this._value = value;
    this.emitToModel();
    this.applyToNative();
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }
    this._disabled = disabled;
    this.stateChanges.next();
  }

  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);

    return !!(isInvalid && (isDirty || isSubmitted));
  }

  controlType = 'rc-service-node-tags';
  /***** FormFieldControl end *****/

  private regionName: string;
  nodeTagOptions: any[];
  nodeTags: any[] = [];
  validNodeTagsLength: number;
  nodeTagsCheckPass: boolean;
  nodeTagsCheckError: boolean;
  nodeTagsCheckLoading: boolean;

  @ViewChild('tagsSelect')
  tagsSelect: MultiSelectionDropdownComponent;

  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() protected parentForm: NgForm,
    @Optional() protected parentFormGroup: FormGroupDirective,
    @Optional() protected cdr: ChangeDetectorRef,
    private regionService: RegionService,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
    this._value = null;
  }

  async ngOnInit() {
    const currentRegion = await this.regionService.getCurrentRegion();
    this.regionName = currentRegion.name;
    const labels = await this.regionService.getRegionLabels(this.regionName);
    this.nodeTagOptions = labels.map((label: any) => {
      return Object.assign(label, {
        display: `${label.key}: ${label.value}`,
        tag: `${label.key}:${label.value}`,
      });
    });

    if (this._value) {
      await delay(0);
      this.tagsSelect.setSelectedValue(this._value);
    }
  }

  ngOnDestroy() {}

  addNodeTag(option: any) {
    this.nodeTags.push(option);
    this.nodeTagOptions.forEach((item: any) => {
      if (item.key === option.key && item.tag !== option.tag) {
        item.$$rcItemDisabled = true;
      }
    });
    this.resetCheckStatus();
    this.updateModelValue();
  }

  removeNodeTag(option: any) {
    remove(this.nodeTags, (item: any) => {
      return item.display === option.display;
    });
    this.nodeTagOptions.forEach((item: any) => {
      if (item.key === option.key) {
        item.$$rcItemDisabled = false;
      }
    });
    this.resetCheckStatus();
    this.updateModelValue();
  }

  checkNodeTagsDisabled() {
    return !this.nodeTags || !this.nodeTags.length;
  }

  async checkNodeTags() {
    const kvString = this.nodeTags
      .map((item: any) => {
        return `${item.key}:${item.value}`;
      })
      .join(',');
    try {
      this.nodeTagsCheckLoading = true;
      const data: any = await this.regionService.checkAvailableNodes(
        kvString,
        this.regionName,
      );
      this.validNodeTagsLength = data.nodes && data.nodes.length;
      this.nodeTagsCheckPass = true;
    } catch (err) {
      this.nodeTagsCheckError = true;
    } finally {
      this.nodeTagsCheckLoading = false;
    }
  }

  private resetCheckStatus() {
    this.validNodeTagsLength = 0;
    this.nodeTagsCheckPass = false;
    this.nodeTagsCheckError = false;
  }

  // set ngModel value when native view changes
  private updateModelValue() {
    this._value = this.nodeTags.map((item: any) => {
      return item.tag;
    });
    this.emitToModel();
  }

  // emit _value to ngModel
  private emitToModel() {
    this.onTouchedCallback();
    this.onChangeCallback(this._value);
  }

  // apply _value to native view
  private applyToNative() {
    if (this.tagsSelect) {
      this.tagsSelect.setSelectedValue(this._value);
    }
  }

  /** Following Methods Implements ControlValueAccessor **/

  writeValue(value: any) {
    this._value = value;
    this.applyToNative();
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}
