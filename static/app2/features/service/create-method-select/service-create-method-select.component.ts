import { Component, EventEmitter, Output } from '@angular/core';

import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-service-create-method-select',
  templateUrl: './service-create-method-select.component.html',
  styleUrls: ['./service-create-method-select.component.scss'],
})
export class ServiceCreateMethodSelectComponent {
  @Output()
  finish = new EventEmitter<any>();
  resourceType: string;
  keyByRepo: string;
  keyByYaml: string;

  constructor(private translate: TranslateService) {}
  initResourceType(type: string) {
    this.resourceType = type;
    switch (type) {
      case 'app':
        this.keyByRepo = this.translate.get('create_k8s_app_by_repository');
        this.keyByYaml = this.translate.get('create_k8s_app_by_compose_file');
        break;
      case 'service':
        this.keyByRepo = this.translate.get('create_k8s_service_by_repository');
        this.keyByYaml = this.translate.get(
          'create_k8s_service_by_compose_file',
        );
        break;
    }
  }
  createByRepo() {
    this.finish.next('repo');
  }
  createByYaml() {
    this.finish.next('yaml');
  }
}
