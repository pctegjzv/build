import {
  Component,
  Input,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { ServiceContainerFieldsComponent } from 'app2/features/service/create-fields/service-container.component';
import { ServiceFieldsNetworkModeComponent } from 'app2/features/service/create-fields/service-network-mode.component';
import { ImageSelectComponent } from 'app2/features/service/image-select/image-select.component';
import {
  ContainerViewModel,
  K8sComponent,
  RcImageSelection,
} from 'app2/features/service/service.type';
import {
  NamespaceOption,
  NamespaceService,
} from 'app2/shared/services/features/namespace.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import * as jsyaml from 'js-yaml';
import * as _ from 'lodash';
import { first } from 'rxjs/operators';

import {
  INT_PATTERN,
  K8S_RESOURCE_NAME_BASE,
} from '../../../../app/components/common/config/common-pattern';

@Component({
  selector: 'rc-service-create-ui',
  templateUrl: './service-create-ui.component.html',
  styleUrls: [
    './service-create-ui.common.scss',
    './service-create-ui.component.scss',
  ],
})
export class ServiceCreateUiComponent implements OnInit, OnDestroy {
  @Input()
  serviceData: K8sComponent; // data of service to update
  numberReg = INT_PATTERN;
  resourceNameReg = K8S_RESOURCE_NAME_BASE;

  @ViewChild('serviceForm')
  serviceForm: NgForm;
  @ViewChildren(ServiceContainerFieldsComponent)
  private containerFields: QueryList<ServiceContainerFieldsComponent>;
  @ViewChild(ServiceFieldsNetworkModeComponent)
  private networkModeFields: ServiceFieldsNetworkModeComponent;
  private containerIndex = 0;
  update = false;

  initialized = false;
  submitting = false;
  yamlPreview: boolean;
  yamlReadonly = true;

  showAdvanceOptions = false;

  service: any;
  serviceYaml: string;
  servicePayload: any;

  containerPorts: any[] = [];
  containerNames: any = [];
  containerPortsOptions: any[] = [];

  serviceKindOptions: any[] = [];
  namespaceOptions: NamespaceOption[] = [];

  constructor(
    private regionService: RegionService,
    private namespaceService: NamespaceService,
    private modalService: ModalService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private serviceService: ServiceService,
    private errorsToastService: ErrorsToastService,
  ) {}

  async ngOnInit() {
    if (this.serviceData) {
      this.update = true;
      this.servicePayload = {
        kubernetes: [],
      };
    } else {
      this.servicePayload = {
        cluster: {
          uuid: '',
          name: '',
        },
        namespace: {
          uuid: '',
          name: '',
        },
        kubernetes: [],
      };
      const region = await this.regionService.getCurrentRegion();
      this.servicePayload.cluster.uuid = region.id;
      this.servicePayload.cluster.name = region.name;
      this.namespaceOptions = await this.namespaceService.getNamespaceOptions(
        region.id,
      );
    }

    this.serviceKindOptions = ['Deployment', 'DaemonSet', 'StatefulSet'];
    this.initServcie();
    this.initialized = true;
  }

  ngOnDestroy() {}

  private async initServcie() {
    if (!this.update) {
      this.service = {
        namespace: '',
        name: '',
        // from kubernetes
        kind: '', // 部署模式
        replicas: 1, // 实例数
        labels: [],
        minReplicas: 1,
        maxReplicas: 20,
        nodeTags: [],
        affinity: null, // 亲和性，包括container亲和反亲和，key为 `service.${label_base_doamin}/name`
        maxSurge: '',
        maxUnavailable: '',
        kubeServices: [],
        containers: [],
        networkMode: {
          hostNetwork: false,
          subnet: null,
        },
      };
      if (this.namespaceOptions.length) {
        this.service.namespace = this.namespaceOptions[0]['name'];
      }
      this.service.kind = this.serviceKindOptions[0];
      this.addContainer(true);
    } else {
      const service = {
        namespace: this.serviceData.namespace.name,
        name: this.serviceData.resource.name,
      };
      this.service = Object.assign(
        service,
        this.serviceService.getServiceViewModel(this.serviceData.kubernetes),
      );
      this.service.containers.forEach(
        (
          container: { config: ContainerViewModel; index?: number },
          index: number,
        ) => {
          container.index = this.containerIndex++;
          this.containerNames[index] = container.config.name;
          this.containerPorts.push([]);
        },
      );
    }
  }

  onNamespaceOptionChange(option: any) {
    this.service.namespace = option.name;
    this.servicePayload.namespace.name = option.name;
    this.servicePayload.namespace.uuid = option.uuid;
  }

  async addContainer(isDefault = false) {
    const config: any = {};
    if (isDefault) {
      config.image =
        this.activatedRoute.snapshot.queryParamMap.get('full_image_name') || '';
      config.name =
        this.activatedRoute.snapshot.queryParamMap.get('repository_name') || '';
      this.containerNames[this.containerIndex] = config.name;
      this.service.containers.push({
        config,
        index: this.containerIndex++,
      });
      this.addContainerPorts();
    } else {
      try {
        const modalRef = await this.modalService.open(ImageSelectComponent, {
          title: this.translateService.get('nav_select_image'),
          width: 800,
        });
        modalRef.componentInstance.close
          .pipe(first())
          .subscribe((res: RcImageSelection) => {
            modalRef.close();
            if (res) {
              config.image = res.full_image_name;
              config.name = res.repository_name || '';
              this.containerNames[this.containerIndex] = config.name;
              this.service.containers.push({
                config,
                index: this.containerIndex++,
              });
              this.addContainerPorts();
            }
          });
      } catch (error) {
        //
      }
    }
  }

  removeContainer(index: number) {
    _.remove(this.service.containers, (container: any) => {
      return index === container.index;
    });
    this.containerNames[index] = undefined;
    this.removeContainerPorts(index);
  }

  containerNameChange(name: string, index: number) {
    setTimeout(() => {
      this.containerNames[index] = name;
    });
  }

  containerPortsChange(ports: number[], index: number) {
    this.containerPorts[index] = ports;
    this.refreshContainerPortsOptions();
  }

  private addContainerPorts() {
    if (!this.containerPorts) {
      this.containerPorts = [];
    }
    this.containerPorts.push([]);
  }

  private removeContainerPorts(index: number) {
    this.containerPorts[index] = [];
    this.refreshContainerPortsOptions();
  }

  private refreshContainerPortsOptions() {
    if (!this.containerPorts) {
      return;
    }
    this.containerPortsOptions = _.chain(this.containerPorts)
      .flatten()
      .uniq()
      .value();
  }

  // 实例数
  shouldShowReplicas() {
    return this.service.kind !== 'DaemonSet';
  }

  // 最大可超出
  shouldShowMaxSurge() {
    return this.service.kind === 'Deployment';
  }

  // 最多不可用
  shouldShowMaxUnavliable() {
    return ['Deployment', 'DaemonSet'].includes(this.service.kind);
  }

  // 亲和性
  shouldShowAffinity() {
    return this.service.kind !== 'DaemonSet';
  }

  trackByContainerIndex(_index: number, container: any) {
    return container.index;
  }

  generateYaml() {
    this.triggerSubmit();
    if (
      this.serviceForm.invalid ||
      (this.networkModeFields && this.networkModeFields.form.invalid)
    ) {
      return;
    }
    if (!this.serviceService.checkServicePayload(this.service)) {
      return;
    }
    this.serviceYaml = this.serviceService.generateServiceYamlPayload(
      this.service,
    );
    this.yamlPreview = true;
  }

  async confirm() {
    this.triggerSubmit();
    if (
      this.serviceForm.invalid ||
      (this.networkModeFields && this.networkModeFields.form.invalid)
    ) {
      return;
    }
    if (!this.serviceYaml) {
      if (!this.serviceService.checkServicePayload(this.service)) {
        return;
      }
      try {
        this.serviceYaml = this.serviceService.generateServiceYamlPayload(
          this.service,
        );
      } catch (error) {
        this.auiNotificationService.error(error);
        return;
      }
    }
    this.servicePayload.resource = {
      create_method: this.yamlReadonly ? 'UI' : 'YAML',
    };
    this.servicePayload.kubernetes = jsyaml.safeLoadAll(this.serviceYaml);
    if (this.update) {
      try {
        await this.modalService.confirm({
          title: this.translateService.get('update'),
          content: this.translateService.get(
            'app_service_update_service_confirm',
            {
              service_name: this.serviceData.resource.name,
            },
          ),
        });
        this.updateService();
      } catch (rejection) {}
    } else {
      try {
        await this.modalService.confirm({
          title: this.translateService.get('create'),
          content: this.translateService.get(
            'app_service_create_service_confirm',
            {
              service_name: this.service.name,
            },
          ),
        });
        this.createService();
      } catch (rejection) {}
    }
  }

  private async createService() {
    this.submitting = true;
    this.servicePayload.resource.name = this.service.name;
    try {
      const response: any = await this.serviceService.createK8sService(
        this.servicePayload,
      );
      return this.router.navigateByUrl(
        `k8s_service/detail/${response.resource.uuid}`,
      );
    } catch (err) {
      this.errorsToastService.handleGenericAjaxError({
        errors: err.errors,
        handleNonGenericCodes: true,
        fallbackMessage: this.translateService.get('create_failed'),
      });
    }
    this.submitting = false;
  }

  private async updateService() {
    this.submitting = true;
    try {
      const response: any = await this.serviceService.patchK8sService(
        this.serviceData.resource.uuid,
        this.servicePayload,
      );
      this.auiNotificationService.success(
        this.translateService.get('update_success'),
      );
      return this.router.navigateByUrl(
        `k8s_service/detail/${response.resource.uuid}`,
      );
    } catch (err) {
      this.errorsToastService.handleGenericAjaxError({
        errors: err.errors,
        handleNonGenericCodes: true,
        fallbackMessage: this.translateService.get('create_failed'),
      });
    }
    this.submitting = false;
  }

  cancel() {
    return this.router.navigateByUrl('k8s_service');
  }

  backToUi() {
    this.yamlPreview = false;
    this.serviceYaml = '';
  }

  async toggleYamlReadonly() {
    try {
      await this.modalService.confirm({
        title: this.translateService.get('confirm'),
        content: this.translateService.get(
          this.translateService.get('service_toggle_yaml_preview_confirm'),
        ),
      });
      this.yamlReadonly = false;
    } catch (e) {
      return false;
    }
  }

  private triggerSubmit() {
    this.containerFields.forEach(
      (container: ServiceContainerFieldsComponent) => {
        container.triggerSubmit();
      },
    );
    if (this.networkModeFields) {
      this.networkModeFields.form.onSubmit(null);
    }
    this.serviceForm.onSubmit(null);
  }
}
