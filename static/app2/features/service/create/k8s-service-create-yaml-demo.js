const yaml_template = `apiVersion: apps/v1beta1
kind: Deployment
metadata:
  name: nginx-app
  labels:
    aaa: nginx-app
spec:
  replicas: 1
  template:
    metadata:
      labels:
        run: nginx-app

    spec:
      containers:
      - name: nginx-app
        image: nginx:1.9.0
        ports:
        - containerPort: 80
          name: web
---
apiVersion: v1
kind: Service
metadata:
  labels:
    run: nginx-app
  name: nginx-app
  namespace: default
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    run: nginx-app
  sessionAffinity: None
  type: ClusterIP`;

export default yaml_template;
