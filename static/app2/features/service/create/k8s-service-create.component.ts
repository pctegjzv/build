import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import * as jsyaml from 'js-yaml';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import {
  NamespaceOption,
  NamespaceService,
} from 'app2/shared/services/features/namespace.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

import { K8S_RESOURCE_NAME_BASE } from '../../../../app/components/common/config/common-pattern';

import yaml_template from './k8s-service-create-yaml-demo';

@Component({
  selector: 'rc-service-create',
  templateUrl: './k8s-service-create.component.html',
  styleUrls: ['./k8s-service-create.component.scss'],
})
export class K8sServiceCreateComponent implements OnInit, OnDestroy {
  private regionSubscription: Subscription;

  namespaceOptions: NamespaceOption[];
  servicePayload: any;
  serviceYaml: string;
  submitting = false;

  resourceNameReg = K8S_RESOURCE_NAME_BASE;

  @ViewChild('serviceCreateForm')
  form: NgForm;

  constructor(
    private namespaceService: NamespaceService,
    private regionService: RegionService,
    private serviceService: ServiceService,
    private modalService: ModalService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private errorsToastService: ErrorsToastService,
    private router: Router,
  ) {
    /**
     * @type {{resource: {}; namespace: {}; cluster: {}; kubernetes: Array}}
     * {
        "resource": {
          "name": "service-name"
        },
        "namespace": {
          "uuid": "4928E1EE-ED87-4BD1-99F4-1227E1192DBC",
          "name": "default"
        },
        "cluster": {
          "uuid": "EDC70F4A-EE71-40B1-9CC7-CD5569BEAD2D",
          "name": "cluster-1"
        },
        "kubernetes": [
          "<example-service-yaml-for-kubernetes>"
        ]
      }
     */
    this.servicePayload = {
      resource: {},
      namespace: {},
      cluster: {},
      kubernetes: [],
    };
  }

  ngOnInit() {
    this.regionSubscription = this.regionService.region$
      .pipe(filter(region => !!region))
      .subscribe(async region => {
        this.servicePayload.cluster.uuid = region.id;
        this.servicePayload.cluster.name = region.name;
        this.namespaceOptions = await this.namespaceService.getNamespaceOptions(
          region.id,
        );
      });
  }

  ngOnDestroy() {
    this.regionSubscription.unsubscribe();
  }

  onNamespaceOptionChange(option: any) {
    this.servicePayload.namespace.name = option.name;
  }

  async createService() {
    this.submitting = true;
    try {
      const response: any = await this.serviceService.createK8sService(
        this.servicePayload,
      );
      return this.router.navigateByUrl(
        `k8s_service/detail/${response.resource.uuid}`,
      );
    } catch (err) {
      this.errorsToastService.handleGenericAjaxError({
        errors: err.errors,
        handleNonGenericCodes: true,
        fallbackMessage: this.translateService.get('create_failed'),
      });
    }
    this.submitting = false;
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.valid) {
      try {
        this.servicePayload.kubernetes = jsyaml.safeLoadAll(this.serviceYaml);
      } catch (error) {
        // yaml parse error
        this.auiNotificationService.error(error);
        return;
      }
      // this.logger.log('create service data: ', this.servicePayload);
      try {
        await this.modalService.confirm({
          title: this.translateService.get('create'),
          content: this.translateService.get(
            'app_service_create_service_confirm',
            {
              service_name: this.servicePayload.resource.name,
            },
          ),
        });
        this.createService();
      } catch (rejection) {}
    }
  }

  cancel() {
    return this.router.navigateByUrl('k8s_service');
  }

  useDemoTemplate() {
    this.serviceYaml = yaml_template;
  }
}
