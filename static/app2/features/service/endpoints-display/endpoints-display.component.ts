import { Component, Input } from '@angular/core';

@Component({
  selector: 'rc-endpoints-display',
  templateUrl: './endpoints-display.component.html',
  styleUrls: ['./endpoints-display.component.scss'],
})
export class EndpointsDisplayComponent {
  @Input()
  urlList: Array<String> = [];

  constructor() {}
}
