import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';

import { NetworkService } from 'app2/shared/services/features/network.service';
import { NetworkUtilitiesService } from 'app2/shared/services/features/network.utilities.service';
import { RegionService } from 'app2/shared/services/features/region.service';

@Component({
  selector: 'rc-service-endpoints',
  styleUrls: ['./service-endpoints.component.scss'],
  templateUrl: './service-endpoints.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceEndpointsComponent implements OnInit, OnDestroy {
  @Input()
  uuid: string;
  endpoints: any[];
  initialized: boolean;

  constructor(
    private networkService: NetworkService,
    private regionService: RegionService,
    private networkUtils: NetworkUtilitiesService,
    private cdr: ChangeDetectorRef,
  ) {
    this.endpoints = [];
  }

  async ngOnInit() {
    await this.getEndpoints();
    this.initialized = true;
    this.cdr.markForCheck();
  }

  ngOnDestroy() {}

  async refetch() {
    await this.getEndpoints();
    this.cdr.markForCheck();
  }

  private getEndpointAddress(item: any) {
    let url = '';
    if (item.rule_domain) {
      url = `${item.protocol}://${item.rule_domain}`;
    } else {
      // default domain
      url = `${item.protocol}://${item.lb_address}`;
    }
    if (![80, 443].includes(item.port)) {
      url += `:${item.port}`;
    }
    return url;
  }

  private async getEndpoints() {
    try {
      const currentRegion = await this.regionService.getCurrentRegion();
      const res = await this.networkService.getList({
        region_name: currentRegion.name,
        service_id: this.uuid,
        frontend: true,
      });
      this.resolveServiceEndpoints(res);
    } catch (err) {
      this.endpoints = [];
    }
  }

  private resolveServiceEndpoints(data: any) {
    const endpoints: any[] = [];
    if (data && data.length) {
      data.forEach((lb: any) => {
        if (!lb.frontends || !lb.frontends.length) {
          return;
        }
        lb.frontends.forEach((frontend: any) => {
          if (frontend.service_id === this.uuid) {
            // default domain
            const item: any = {
              load_balancer_id: lb.load_balancer_id,
              load_balancer_name: lb.name,
              container_port:
                frontend.container_port === 0 ? null : frontend.container_port,
              port: frontend.port,
              protocol: frontend.protocol,
              lb_address: lb.address,
              rules: null,
            };
            item.endpoint_address = this.getEndpointAddress(item);
            endpoints.push(item);
          }
          if (frontend.rules && frontend.rules.length) {
            frontend.rules.forEach((rule: any) => {
              if (!rule.services) {
                return;
              }
              const service = rule.services.find((item: any) => {
                return item.service_id === this.uuid;
              });
              if (!service) {
                return;
              }
              if (rule.domain && !rule.url) {
                const item: any = {
                  load_balancer_id: lb.load_balancer_id,
                  load_balancer_name: lb.name,
                  container_port: service.container_port,
                  port: frontend.port,
                  protocol: frontend.protocol,
                  rule_domain: rule.domain,
                  rule_indicators: this.networkUtils.parseDSL(rule),
                };
                item.endpoint_address = this.getEndpointAddress(item);
                endpoints.push(item);
              } else {
                const item: any = {
                  load_balancer_id: lb.load_balancer_id,
                  load_balancer_name: lb.name,
                  container_port: service.container_port,
                  port: frontend.port,
                  protocol: frontend.protocol,
                  rule_indicators: this.networkUtils.parseDSL(rule),
                };
                endpoints.push(item);
              }
            });
          }
        });
      });
    }
    this.endpoints = endpoints;
  }
}
