import {
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { ACCOUNT, ENVIRONMENTS, WEBLABS } from 'app2/core/tokens';
import { Environments, RcAccount, Weblabs } from 'app2/core/types';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { getCookie } from 'app2/utils/cookie';

@Component({
  templateUrl: './k8s-service-exec.component.html',
  styleUrls: ['./k8s-service-exec.component.scss'],
})
export class K8sServiceExecComponent implements OnInit {
  @Output()
  close = new EventEmitter();
  @ViewChild(NgForm)
  ngForm: NgForm;
  @ViewChild('form')
  formRef: ElementRef;

  serviceId: string;
  serviceApplication?: string;

  loading = true;
  projectsEnabled: boolean;
  projectName: string;
  projectPrefix: string;
  lycanUrl: string;
  user_name: string;
  serviceExecEndpoint: string;

  instanceName = '';
  command = '/bin/sh';
  copySuccess = false;

  serviceInstances: Array<{
    uuid: string;
    name: string;
    namespace: string;
    containers: any[];
  }>;

  serviceContainerGroup: string;
  serviceContainers: any[];

  serviceContainerGroupId: string;
  serviceContainerId: string;

  constructor(
    @Inject(MODAL_DATA)
    private data: {
      serviceId: string;
      serviceApplication?: string;
    },
    @Inject(WEBLABS) private weblabs: Weblabs,
    @Inject(ENVIRONMENTS) private environments: Environments,
    @Inject(ACCOUNT) public account: RcAccount,
    private regionService: RegionService,
    private serviceService: ServiceService,
  ) {
    this.serviceId = this.data.serviceId;
    this.serviceApplication = this.data.serviceApplication;

    this.projectsEnabled = this.weblabs.PROJECTS_ENABLED;

    this.projectName = getCookie('project');
    this.projectPrefix =
      this.projectsEnabled && this.projectName
        ? ` project/${this.projectName}`
        : '';

    this.lycanUrl = this.environments.lycan_url;

    this.user_name = this.account.username
      ? `${this.account.namespace}/${this.account.username}`
      : this.account.namespace;
  }

  get execCommand() {
    return `ssh -p 4022 -t ${this.user_name}@${this.serviceExecEndpoint} ${
      this.account.namespace
    }/${this.instanceName} ${this.command}`;
  }

  async ngOnInit() {
    const region = await this.regionService.getCurrentRegion();
    this.serviceExecEndpoint = region.features.service.exec.endpoint;

    const { result } = await this.serviceService.getK8sServiceInstances<{
      metadata: {
        name: string;
        namespace: string;
        uid: string;
      };
      spec: {
        containers: any;
      };
    }>(this.serviceId);

    this.serviceInstances = result.map(
      ({ metadata: { name, namespace, uid }, spec: { containers } }) => ({
        uuid: uid,
        name,
        namespace,
        containers,
      }),
    );

    if (this.serviceInstances.length) {
      const firstContainerGroup = this.serviceInstances[0];
      this.serviceContainerGroupId = firstContainerGroup.uuid;
      const { containers } = firstContainerGroup;

      if (containers.length) {
        this.serviceContainerId = containers[0].name;
      }
    }

    this.loading = false;
  }

  onInstanceGroupChange(group: { name: string; containers: any[] }) {
    this.serviceContainerGroup = group.name;
    this.serviceContainers = group.containers;
  }

  onContainerChange(container: any) {
    this.instanceName = `${this.serviceId}/${this.serviceContainerGroup}/${
      container.name
    }`;
  }

  submit() {
    if (this.ngForm.invalid) {
      return;
    }

    this.close.emit();
    this.formRef.nativeElement.submit();
  }
}
