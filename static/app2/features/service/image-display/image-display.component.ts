import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

import { TranslateService } from 'app2/translate/translate.service';

export interface ImageDisplayParams {
  image: string;
  size: {
    cpu: string;
    memory: string;
  };
}

export interface ImageDisplayItem {
  image: string;
  size: string;
  copyStatus?: boolean;
}

@Component({
  selector: 'rc-service-image-display',
  templateUrl: './image-display.component.html',
  styleUrls: ['./image-display.component.scss', './image-display.common.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceImageDisplayComponent implements OnInit, OnChanges {
  @Input()
  imageDisplayParams: Array<ImageDisplayParams> = [];
  list: Array<ImageDisplayItem> = [];
  firstCopyTip: string;
  firstCopySuccessTip: string;
  firstCopyFailTip: string;
  secondCopyTip: string;
  secondCopySuccessTip: string;
  secondCopyFailTip: string;

  constructor(
    private translateService: TranslateService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnChanges({ imageDisplayParams }: SimpleChanges) {
    if (
      imageDisplayParams &&
      !imageDisplayParams.firstChange &&
      imageDisplayParams.currentValue
    ) {
      this.imageDisplayParams = imageDisplayParams.currentValue;
      this.list = this.getList();
      this.cdr.markForCheck();
    }
  }

  ngOnInit(): void {
    this.upadte();
  }

  private upadte() {
    this.list = this.getList();
    this.getCopyTip();
    this.cdr.markForCheck();
  }

  private getList() {
    if (!this.imageDisplayParams || !this.imageDisplayParams.length) {
      return [];
    }
    return this.imageDisplayParams.map(param => {
      return {
        image: param.image,
        size: this.formatSize(param.size),
      };
    });
  }

  private formatSize(size: { cpu?: string; memory?: string }) {
    size.cpu =
      size.cpu && size.cpu.endsWith('m')
        ? size.cpu
        : size.cpu
          ? `${size.cpu}${this.translateService.get('unit_core')}`
          : '0';
    const cpu = size.cpu.startsWith('0') ? '-' : `${size.cpu}`;
    const memory = size.memory && size.memory !== '0' ? `${size.memory}` : '-';
    const memTranslation = this.translateService.get('memory');
    return cpu === '-' && memory === '-'
      ? `CPU - ; ${memTranslation} -`
      : `CPU ${cpu} ; ${memTranslation} ${memory}`;
  }

  private getCopyTip() {
    if (!this.list.length) {
      return;
    }
    this.firstCopyTip = `
      [${this.translateService.get('click_to_copy')}]
      ${this.list[0].image}
    `;
    this.firstCopySuccessTip = this.translateService.get(
      'copy_clipboard_success',
    );
    this.firstCopyFailTip = this.translateService.get('copy_clipboard_fail');

    if (this.list.length === 2) {
      this.secondCopyTip = `
        [${this.translateService.get('click_to_copy')}]
        ${this.list[1].image}
      `;
      this.secondCopySuccessTip = this.translateService.get(
        'copy_clipboard_success',
      );
      this.secondCopyFailTip = this.translateService.get('copy_clipboard_fail');
    }
  }
}
