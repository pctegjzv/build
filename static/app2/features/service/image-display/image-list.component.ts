import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import {
  ImageDisplayItem,
  ImageDisplayParams,
} from 'app2/features/service/image-display/image-display.component';
import { TranslateService } from 'app2/translate/translate.service';
import clipboard from 'clipboard-polyfill';

@Component({
  selector: 'rc-service-image-list',
  templateUrl: './image-list.component.html',
  styleUrls: ['./image-list.component.scss', './image-display.common.scss'],
})
export class ServiceImageListComponent implements OnInit {
  @Input()
  imageDisplayParams: Array<ImageDisplayParams> = [];
  list: Array<ImageDisplayItem> = [];

  constructor(
    private translateService: TranslateService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit(): void {
    this.list = this.getList();
  }

  private getList() {
    if (!this.imageDisplayParams.length) {
      return [];
    }
    return this.imageDisplayParams.map(param => {
      return {
        image: param.image,
        size: this.formatSize(param.size),
        copyStatus: false,
      };
    });
  }

  private formatSize(size: { cpu?: string; memory?: string }) {
    const cpu = size.cpu && size.cpu.startsWith('0') ? '-' : `${size.cpu}`;
    const memory = size.memory && size.memory !== '0' ? `${size.memory}` : '-';
    const memTranslation = this.translateService.get('memory');
    return cpu === '-' && memory === '-'
      ? `CPU - ; ${memTranslation} -`
      : `CPU ${cpu} ; ${memTranslation} ${memory}`;
  }

  copyToClipboard(item: ImageDisplayItem) {
    clipboard.writeText(item.image).then(() => {
      item.copyStatus = true;
      this.cdr.markForCheck();
    });
  }

  resetStatus(item: ImageDisplayItem) {
    item.copyStatus = false;
  }
}
