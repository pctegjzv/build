export const GALLERY_IMAGES = {
  'SSH Servers': [
    'tutum/ubuntu',
    'tutum/centos',
    'tutum/debian',
    'tutum/fedora',
  ],
  'Web Servers': [
    'tutum/tomcat',
    'tutum/lamp',
    'library/nginx',
    'library/httpd',
    'tutum/glassfish',
  ],
  Databases: ['tutum/mysql', 'tutum/mongodb', 'tutum/postgresql'],
  'Cache Servers': ['tutum/memcached', 'tutum/redis'],
  Others: [
    'alauda/hello-world',
    'alauda/sftp',
    'tutum/wordpress',
    'tutum/rabbitmq',
    'ibosyang/ibos',
  ],
};

export const OUTSOURCING_IMAGES: string[] = ['library/centos'];
