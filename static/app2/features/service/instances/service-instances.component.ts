import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { get } from 'lodash';

import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { PodService } from 'app2/shared/services/features/pod.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { TerminalService } from 'app2/shared/services/features/terminal.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-service-instances',
  templateUrl: './service-instances.component.html',
  styleUrls: ['./service-instances.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceInstancesComponent implements OnInit, OnDestroy {
  @Input()
  serviceId: string;
  @Input()
  clusterId: string;
  @Input()
  canUpdate: boolean; // can update service ?
  @Output()
  shouldRefresh: EventEmitter<any>;
  private _loading = false;
  private submittingMap: any = {};
  instanceItems: any[];
  destroyed = false;

  constructor(
    @Inject(ENVIRONMENTS) private env: Environments,
    private cdr: ChangeDetectorRef,
    private serviceService: ServiceService,
    private podService: PodService,
    private modal: ModalService,
    private errorsToastService: ErrorsToastService,
    private translateService: TranslateService,
    private terminalService: TerminalService,
  ) {
    this.shouldRefresh = new EventEmitter<any>();
  }

  ngOnInit() {
    if (this.serviceId) {
      this.refetch();
    }
  }

  ngOnDestroy() {
    this.destroyed = true;
  }

  get empty() {
    return !this.instanceItems || !this.instanceItems.length;
  }

  get loading() {
    return this._loading;
  }

  async refetch() {
    if (this.destroyed || this.loading) {
      return;
    }
    this._loading = true;
    try {
      const response = await this.serviceService.getK8sServiceInstances(
        this.serviceId,
      );
      this.instanceItems = response['result'];
    } catch (err) {
      this.instanceItems = [];
    }
    this._loading = false;
    if (!this.destroyed) {
      this.cdr.detectChanges();
    }
  }

  getInstanceVersion(instance: any) {
    const annotations: any = get(instance, 'metadata.annotations', {});
    return (
      annotations[`service.${this.env.label_base_domain}/version`] || 'current'
    ); // current/previous
  }

  getInstanceVersionText(instance: any) {
    const version = this.getInstanceVersion(instance);
    return this.translateService.get(`instance_version_${version}`);
  }

  async deleteInstance(instance: any) {
    try {
      await this.modal.confirm({
        title: this.translateService.get(
          'service_instance_restart_confirm_title',
        ),
        content: this.translateService.get('service_instance_restart_confirm'),
      });
    } catch (error) {
      return;
    }
    const { name, namespace } = instance.metadata;
    try {
      this.submittingMap[name] = true;
      await this.podService.deletePod({
        cluster_id: this.clusterId,
        namespace,
        pod_name: name,
      });
      this.shouldRefresh.next(null);
    } catch (err) {
      this.errorsToastService.error(err);
    }
    this.submittingMap[name] = false;
  }

  deleteInstanceSubmitting(instance: any) {
    const { name } = instance.metadata;
    return !!this.submittingMap[name];
  }

  // EXEC
  onContainerSelect(pod: any, container: any) {
    this.terminalService.openTerminal({
      pod: pod.metadata.name,
      container: container.name,
      namespace: pod.metadata.namespace,
      clusterId: this.clusterId,
    });
  }

  canEXEC(containerName: string, pod: any): boolean {
    if (!pod.status.containerStatuses) {
      return false;
    }
    const containerStatus = pod.status.containerStatuses.find(
      (co: any) => co.name === containerName,
    );
    return !!containerStatus.state.running;
  }
}
