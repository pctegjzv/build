import {
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { ServiceUtilitiesService } from 'app2/features/service/service-utilities.service';
import { TableComponent } from 'app2/shared/components/table/table.component';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { ErrorResponse } from 'app2/shared/services/http/http.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import {
  Pagination,
  PaginationDataWrapper,
} from 'app2/shared/services/utility/pagination-data';

@Component({
  selector: 'rc-k8s-service-list',
  templateUrl: './k8s-service-list.component.html',
  styleUrls: ['./k8s-service-list.component.scss'],
})
export class K8sServiceListComponent implements OnInit, OnDestroy, OnChanges {
  @Input()
  searchQuery: string;
  @Input()
  regionName: string;

  @ViewChild('serviceTable')
  table: TableComponent;

  paginationDataWrapper: PaginationDataWrapper<any>;

  paginationDataSubscription: Subscription;

  serviceCreateEnabled: boolean;

  pageSize = 20;

  count = 0;

  serviceItems: any[] = [];

  imageDisplayParamsMap: any = {};

  searching = false;

  queryString = '';

  private pollingTimer: any = null;

  private destroyed = false;

  private _onRequestParamsChange = _.debounce(
    this.onRequestParamsChange.bind(this),
    100,
  );

  constructor(
    private serviceService: ServiceService,
    public serviceUtilities: ServiceUtilitiesService,
    private errorsToastService: ErrorsToastService,
    private cdr: ChangeDetectorRef,
    private router: Router,
  ) {
    const fetchRequest = (pageNo: number, pageSize: number, params: any) => {
      return this.serviceService.getK8sServices({
        pageNo,
        pageSize,
        params,
      });
    };
    this.paginationDataWrapper = new PaginationDataWrapper(
      fetchRequest,
      this.pageSize,
    );
  }

  async ngOnInit() {
    this.paginationDataSubscription = this.paginationDataWrapper.pagination.subscribe(
      (paginationData: Pagination<any>) => {
        if (!paginationData.loadError) {
          this.count = paginationData.count;
          this.serviceItems = paginationData.results;
          this.serviceItems.forEach((item: any) => {
            this.imageDisplayParamsMap[
              item.resource.uuid
            ] = this.serviceUtilities.getServiceImageDisplayParams(item, 'api');
          });
          if (!this.destroyed) {
            this.cdr.detectChanges();
          }
        }
        this.resetPollingTimer();
      },
    );
  }

  ngOnChanges({ searchQuery, regionName }: SimpleChanges): void {
    if (searchQuery || regionName) {
      if (regionName) {
        this.serviceItems = [];
      }
      this._onRequestParamsChange({
        name: this.searchQuery,
        cluster: this.regionName,
      });
    }
  }

  ngOnDestroy() {
    this.destroyed = true;
    this.paginationDataSubscription.unsubscribe();
    clearTimeout(this.pollingTimer);
  }

  get loading() {
    return this.paginationDataWrapper.loading;
  }

  get loadError() {
    return this.paginationDataWrapper.loadError;
  }

  get empty() {
    return !this.serviceItems || !this.serviceItems.length;
  }

  get currentPage() {
    return this.paginationDataWrapper.pageNo;
  }

  refetch() {
    return this.paginationDataWrapper.refetch();
  }

  pageChanged(page: number) {
    this.paginationDataWrapper.setPageNo(page);
  }

  viewService(service: any) {
    return this.router.navigateByUrl(
      `k8s_service/detail/${service.resource.uuid}`,
    );
  }

  async startService(service: any) {
    try {
      const response = await this.serviceUtilities.startService(
        service.resource.name,
        service.resource.uuid,
      );
      this.refetch();
      return response;
    } catch (rejection) {
      if (rejection instanceof ErrorResponse) {
        this.errorsToastService.handleGenericAjaxError({
          errors: rejection.errors,
          handleNonGenericCodes: true,
        });
      }
    }
  }

  async stopService(service: any) {
    try {
      const response = await this.serviceUtilities.stopService(
        service.resource.name,
        service.resource.uuid,
      );
      this.refetch();
      return response;
    } catch (rejection) {
      if (rejection instanceof ErrorResponse) {
        this.errorsToastService.handleGenericAjaxError({
          errors: rejection.errors,
          handleNonGenericCodes: true,
        });
      }
    }
  }

  updateService(service: any) {
    const type = service.resource.create_method === 'UI' ? 'repo' : 'yaml';
    this.router.navigateByUrl(
      `/k8s_service/update/${service.resource.uuid}?type=${type}`,
    );
  }

  updateReplicas(service: any) {
    this.serviceUtilities
      .updateReplicas(service.resource.uuid)
      .pipe(first())
      .subscribe((res: boolean) => {
        if (res) {
          this.refetch();
        }
      });
  }

  updateImageTag(resource: any) {
    const { containers, uuid } = resource;
    this.serviceUtilities
      .updateImageTag({
        uuid,
        containers,
      })
      .pipe(first())
      .subscribe((finish: boolean) => {
        if (finish) {
          this.refetch();
        }
      });
  }

  updateEndpoints(uuid: string) {
    this.serviceUtilities.updateEndpoints(uuid);
  }

  async deleteService(service: any) {
    try {
      const response = await this.serviceUtilities.deleteService(
        service.resource.name,
        service.resource.uuid,
      );
      this.refetch();
      return response;
    } catch (rejection) {
      if (rejection instanceof ErrorResponse) {
        this.errorsToastService.handleGenericAjaxError({
          errors: rejection.errors,
          handleNonGenericCodes: true,
        });
      }
    }
  }

  private onRequestParamsChange({
    name,
    cluster,
  }: {
    name: string;
    cluster: string;
  }) {
    this.paginationDataWrapper.setParams({ name, cluster });
  }

  /**
   * paginationData 返回后重置下次轮询时间 若有部署中的应用，等待时间为3s
   */
  private resetPollingTimer() {
    clearTimeout(this.pollingTimer);
    if (this.destroyed) {
      return;
    }
    const hasDeployingServices = _.some(this.serviceItems, (app: any) => {
      return app.resource.status.toLowerCase() === 'deploying';
    });
    const waitTime = hasDeployingServices ? 3000 : 10000;
    this.pollingTimer = setTimeout(() => {
      this.refetch();
    }, waitTime);
  }

  serviceExec(serviceData: any) {
    this.serviceUtilities.serviceExec({
      serviceId: serviceData.resource.uuid,
      serviceApplication: serviceData.parent.uuid,
    });
  }
}
