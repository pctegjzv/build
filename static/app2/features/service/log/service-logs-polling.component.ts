import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { LogService } from 'app2/shared/services/features/log.service';
import * as moment from 'moment';

@Component({
  selector: 'rc-service-logs-polling',
  templateUrl: './service-logs-polling.component.html',
  styleUrls: ['./service-logs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceLogsPollingComponent
  implements OnInit, OnChanges, OnDestroy {
  @Input()
  uuid: string;
  @Input()
  set path(value: string) {
    this._path = value;
    this.refetch();
  }
  get path(): string {
    return this._path;
  }
  @Input()
  logSourceId: string;
  @ViewChild('logContainer')
  logContainer: ElementRef;
  private _path: string;
  private timeRange: {
    start_time: number;
    end_time: number;
  };
  private timer: any;
  private shouldScrollToBottom: boolean;
  private destroyed: boolean;
  logs: any[];
  initialized: boolean;

  constructor(private cdr: ChangeDetectorRef, private logService: LogService) {
    this.logs = [];
  }
  ngOnChanges() {}

  ngOnInit() {
    this.refetch();
  }

  ngOnDestroy() {
    clearTimeout(this.timer);
    this.destroyed = true;
  }

  trackByFn(_index: number, item: any) {
    return item.time;
  }

  private async refetch() {
    if (this.destroyed) {
      return;
    }
    clearTimeout(this.timer);
    await this.getLogs();
    this.timer = setTimeout(() => {
      this.refetch();
    }, 5000);
  }

  private async getLogs() {
    this.resetTimeRange();
    try {
      const res = await this.logService.logsSearch({
        start_time: this.timeRange.start_time,
        end_time: this.timeRange.end_time,
        pageno: 1,
        size: 500,
        paths: this.path || 'stdout',
        read_log_source_uuid: this.logSourceId,
        services: this.uuid,
        mode: 'polling',
      });
      this.resetShouldScrollToBottom();
      this.logs = res.logs;
      this.initialized = true;
      this.cdr.markForCheck();
      setTimeout(() => {
        this.scrollToBottom();
      });
    } catch (err) {
      // placeholder
    }
  }

  private resetTimeRange() {
    const now = moment().valueOf();
    this.timeRange = {
      start_time: (now - 3600 * 24 * 7 * 1000) / 1000,
      end_time: now / 1000,
    };
  }

  private resetShouldScrollToBottom() {
    const $el = $(this.logContainer.nativeElement);
    // console.log(
    //   this.logContainer.nativeElement,
    //   $el.innerHeight(),
    //   $el.scrollTop(),
    //   this.logContainer.nativeElement.scrollHeight,
    // );
    this.shouldScrollToBottom =
      $el.innerHeight() + $el.scrollTop() + 5 >=
      this.logContainer.nativeElement.scrollHeight;
  }

  private scrollToBottom() {
    const $el = $(this.logContainer.nativeElement);
    if (this.shouldScrollToBottom) {
      $el.scrollTop(this.logContainer.nativeElement.scrollHeight);
    }
  }
}
