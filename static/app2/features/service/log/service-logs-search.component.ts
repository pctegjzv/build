import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import * as moment from 'moment';

import { TagsInputComponent } from 'app2/shared/components/tags-input/tags-input.component';
import { LogService } from 'app2/shared/services/features/log.service';
import { LoggerUtilitiesService } from 'app2/shared/services/utility/logger.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-service-logs-search',
  templateUrl: './service-logs-search.component.html',
  styleUrls: ['./service-logs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceLogsSearchComponent
  implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  @Input()
  uuid: string;
  @Input()
  set path(value: string) {
    this._path = value;
  }
  get path(): string {
    return this._path;
  }
  @Input()
  logSourceId: string;
  @ViewChild('tagsInput')
  tagsInput: TagsInputComponent;
  timeRangeOptions: any[];
  timeRange: any;
  timeRangeDisplay: any;
  queryTags: string[];
  selectedTimeRange: number;
  pagination: any;
  logs: any[];
  loading: boolean;
  aggregationResults: any;
  chartConfig: any;
  hasQueryString: boolean;
  private aggregationInterval: number;
  private _path: string;

  constructor(
    private translateService: TranslateService,
    private cdr: ChangeDetectorRef,
    private logService: LogService,
    private logger: LoggerUtilitiesService,
  ) {
    this.timeRangeDisplay = {
      start_time: '',
      end_time: '',
    };
    this.timeRange = {
      start_time: '',
      end_time: '',
    };
    this.pagination = {
      size: 50,
      total_page: 0,
      total_items: 0,
    };
    this.logs = [];
    this.chartConfig = {
      xAxisTickFormatting: (val: Date) => {
        return moment(val).format('YYYY-MM-DD HH:mm');
      },
      yAxisTickFormatting: (val: number) => {
        if (val >= 1000) {
          return val / 1000 + 'k';
        } else {
          return val;
        }
      },
    };
  }
  ngOnChanges() {}

  ngOnInit() {
    this.timeRangeOptions = [
      {
        name: this.translateService.get('last_10_minites'),
        offset: 10 * 60 * 1000,
      },
      {
        name: this.translateService.get('last_30_minites'),
        offset: 30 * 60 * 1000,
      },
      {
        name: this.translateService.get('last_hour'),
        offset: 60 * 60 * 1000,
      },
      {
        name: this.translateService.get('last_12_hour'),
        offset: 12 * 3600 * 1000,
      },
      {
        name: this.translateService.get('last_day'),
        offset: 24 * 3600 * 1000,
      },
      {
        name: this.translateService.get('last_3_days'),
        offset: 3 * 24 * 3600 * 1000,
      },
      {
        name: this.translateService.get('last_5_days'),
        offset: 5 * 24 * 3600 * 1000,
      },
      {
        name: this.translateService.get('last_7_days'),
        offset: 7 * 24 * 3600 * 1000,
      },
      {
        name: this.translateService.get('custom_time_range'),
        offset: -1,
      },
    ];
    this.selectedTimeRange = this.timeRangeOptions[0]['offset'];
  }

  ngAfterViewInit() {
    this.tagsInput.registerBeforeAdd((tag: string) => {
      const keywordMatches = tag.match(/^(\w+):\s(.+)/);
      if (keywordMatches && keywordMatches[1] === 'instance') {
        return tag;
      } else {
        return 'search: ' + tag;
      }
    });
  }

  ngOnDestroy() {}

  timeRangeChanged() {
    if (!this.isCustomTimeRange()) {
      this.calculateTimeRange();
      this.search();
    }
  }

  tagClassFn(item: any) {
    const match = item.match(/(\w+):/);
    if (!match) {
      return '';
    } else if (match[1]) {
      return 'label tag-' + match[1];
    }
  }

  addKeywordByClick(type: string, value: string) {
    this.tagsInput.addTag(`${type}: ${value}`);
  }

  onStartTimeChange(e: any) {
    this.timeRange.start_time = moment(e.dates[0]).valueOf();
  }

  onEndTimeChange(e: any) {
    this.timeRange.end_time = moment(e.dates[0]).valueOf();
  }

  isCustomTimeRange() {
    return this.selectedTimeRange === -1;
  }

  async search(pageno = 1, size = 50) {
    try {
      this.loading = true;
      const params = Object.assign(this.getSearchParams(), {
        pageno,
        size,
      });
      await Promise.all([
        this.loadSearchResults(params),
        this.loadSearchAggregations(params),
      ]);
    } catch (err) {
      // placeholder
      this.logger.log(err);
    }
    this.loading = false;
    this.cdr.detectChanges();
  }

  trackByFn(_index: number, item: any) {
    return item.time;
  }

  getAggregationTooltip(val: Date) {
    return moment(val).format('YYYY-MM-DD HH:mm:ss');
  }

  onChartSelect(event: any) {
    this.selectedTimeRange = this.timeRangeOptions[
      this.timeRangeOptions.length - 1
    ]['offset'];
    this.timeRange.start_time = event.name;
    this.timeRange.end_time = event.name + this.aggregationInterval;
    this.updateTimeRangeDisplay();
    this.search();
  }

  private calculateTimeRange() {
    const end_time = moment().valueOf();
    this.timeRange = {
      start_time: end_time - this.selectedTimeRange,
      end_time,
    };
    this.updateTimeRangeDisplay();
  }

  private updateTimeRangeDisplay() {
    this.timeRangeDisplay = {
      start_time: moment(this.timeRange.start_time).format('YYYY-MM-DD HH:mm'),
      end_time: moment(this.timeRange.end_time).format('YYYY-MM-DD HH:mm'),
    };
  }

  private getSearchParams() {
    const params: any = {
      start_time: this.timeRange.start_time / 1000,
      end_time: this.timeRange.end_time / 1000,
      pageno: 1,
      size: this.pagination.size,
      paths: this.path || 'stdout',
      read_log_source_uuid: this.logSourceId,
      services: this.uuid,
    };
    const queryParams: any = this.logService.generateQuerysFromTags(
      this.queryTags,
    );
    if (queryParams && queryParams.search) {
      queryParams.query_string = queryParams.search;
      delete queryParams.search;
      this.hasQueryString = true;
    } else {
      this.hasQueryString = false;
    }
    Object.assign(params, queryParams);
    return params;
  }

  private async loadSearchResults(params: any) {
    try {
      const res = await this.logService.logsSearch(params);
      Object.assign(this.pagination, {
        total_items: res.total_items,
        total_page: res.total_page,
      });
      this.logs = res.logs;
    } catch (err) {
      this.logs = [];
    }
  }

  private async loadSearchAggregations(params: any) {
    try {
      const res = await this.logService.getAggregations(params);
      if (!res.buckets || !res.buckets.length) {
        return;
      }
      const buckets = res.buckets;
      if (buckets.length) {
        const total = buckets.reduce(
          (acc: any, cur: any) => acc + cur.count,
          0,
        );
        if (total === 0) {
          this.aggregationResults = null;
        } else {
          buckets.forEach((bucket: any) => {
            bucket.time = new Date(bucket.time * 1000).getTime();
          });
          if (buckets.length > 2) {
            this.aggregationInterval = buckets[1].time - buckets[0].time;
          }
          const series = buckets.map((bucket: any) => ({
            name: bucket.time,
            value: bucket.count,
          }));
          this.aggregationResults = series;
        }
      } else {
        this.aggregationResults = null;
      }
    } catch (err) {
      this.aggregationResults = null;
    }
  }
}
