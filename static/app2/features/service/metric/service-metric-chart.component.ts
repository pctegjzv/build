import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChange,
  TemplateRef,
} from '@angular/core';
import * as _ from 'lodash';
import * as moment from 'moment';

import { BytesFormatterPipe } from 'app2/shared/pipes/bytes-formatter.pipe';
import {
  Metric,
  MetricQuery,
  MonitorService,
} from 'app2/shared/services/features/monitor.service';

export interface MetricChartResult {
  name: string;
  series: {
    name: any;
    value: any;
  }[];
  tags: any;
}

@Component({
  selector: 'rc-service-metric-chart',
  templateUrl: './service-metric-chart.component.html',
  styleUrls: ['./service-metric-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceMetricChartComponent
  implements OnInit, OnChanges, OnDestroy {
  @Input()
  metricDisplayName: string;
  @Input()
  metricQuery: MetricQuery;
  @Input()
  showLegend: boolean;
  @Input()
  groupNameFormatting: Function; // 参数为tags, index
  @Input()
  tooltipTemplate: TemplateRef<any>;
  private timeRange: {
    start: number;
    end: number;
  };
  private unit: string;

  view: number[];
  colorScheme: any;
  xAxisTickFormatting: Function;
  yAxisTickFormatting: Function;

  loading: boolean;
  results: MetricChartResult[];

  constructor(
    private cdr: ChangeDetectorRef,
    private monitorService: MonitorService,
  ) {
    this.view = [undefined, 240];
    this.showLegend = true;
  }

  ngOnInit() {
    this.colorScheme = {
      domain: [
        '#7cb5ec',
        '#61D44A',
        '#434348',
        '#f7a35c',
        '#8085e9',
        '#f15c80',
        '#e4d354',
        '#2b908f',
        '#f45b5b',
        '#08DCCB',
      ],
    };
    this.xAxisTickFormatting = function(value: number) {
      return moment(value * 1000).format('MM-DD HH:mm');
    };
  }

  ngOnChanges({ metricQuery }: { metricQuery: SimpleChange }) {
    if (metricQuery && metricQuery.currentValue) {
      this.refetch();
    }
  }

  ngOnDestroy() {}

  get empty() {
    return !this.results || !this.results.length;
  }

  setTimeRange(start: number, end: number) {
    this.timeRange = {
      start,
      end,
    };
    this.refetch();
  }

  getPointValueDisplay(value: any, withUnit = true) {
    const unit = ['Percent', '-'].includes(this.unit) ? '' : this.unit;
    return withUnit
      ? `${this.pointValueFormatter(this.unit, value)} ${unit}`
      : this.pointValueFormatter(this.unit, value);
  }

  async refetch() {
    if (!this.timeRange) {
      return;
    }
    this.loading = true;
    this.cdr.markForCheck();
    try {
      const result = await this.monitorService.queryMetrics({
        qs: [this.metricQuery],
        start: this.timeRange.start,
        end: this.timeRange.end,
      });
      this.generateYAxisTickFormattingFn(result);
      this.results = this.getTransformedResults(result);
      // console.log('results: ', this.results);
    } catch (e) {
      // placeholder
      // console.log(e);
    }
    this.loading = false;
    this.cdr.markForCheck();
  }

  // TODO
  // actionRefresh() {}

  private generateYAxisTickFormattingFn(result: Metric[]) {
    if (!result.length) {
      return;
    }
    const unit = result[0]['unit'] || '-';
    if (this.unit === unit) {
      return;
    }
    this.unit = unit;
    const pointValueFormatter = this.pointValueFormatter;
    this.yAxisTickFormatting = (value: number) => {
      return pointValueFormatter(unit, value, 2);
    };
  }

  private pointValueFormatter(unit: string, value: any, precision = 4) {
    if (value === undefined || value === null) {
      return '-';
    }
    let data: any = value;
    if (unit === 'Percent') {
      data = data === 0 ? 0 : (value * 100).toPrecision(precision) + '%';
    } else if (['Bytes', 'Bytes/Second'].includes(unit)) {
      const bytesFormatter = new BytesFormatterPipe();
      data = bytesFormatter.transform(value, precision);
    } else {
      data = _.isInteger(value)
        ? value.toLocaleString()
        : value.toFixed(precision);
    }
    return data;
  }

  private getTransformedResults(result: Metric[]): MetricChartResult[] {
    return result.map((item: Metric, index: number) => {
      const series = Object.entries(item.dps).map(([key, value]) => {
        return {
          name: parseInt(key, 10).toString(),
          value: value,
        };
      });
      return {
        name: this.groupNameFormatting.call(null, item.tags, index),
        series,
        tags: item.tags,
      };
    });
  }
}
