import {
  AfterViewInit,
  Input,
  OnDestroy,
  QueryList,
  ViewChildren,
} from '@angular/core';
import * as moment from 'moment';

import { MessageService } from 'alauda-ui';
import { ServiceMetricChartComponent } from 'app2/features/service/metric/service-metric-chart.component';
import {
  MetricQuery,
  MonitorService,
} from 'app2/shared/services/features/monitor.service';
import { TranslateService } from 'app2/translate/translate.service';

export class ServiceMetricsBase implements AfterViewInit, OnDestroy {
  @Input()
  uuid: string;
  @ViewChildren(ServiceMetricChartComponent)
  protected charts: QueryList<ServiceMetricChartComponent>;
  protected timerId: number;
  protected timerInterval: number;

  initialized: boolean;
  timeRange: any;
  timeRangeDisplay: any;
  metrics: {
    metricDisplayName: string;
    metricQuery: MetricQuery;
  }[];

  periodOptions: any[];
  selectedPeriod: number;
  groupNameFormatting: Function;

  constructor(
    protected auiMessageService: MessageService,
    protected translateService: TranslateService,
    protected monitorService: MonitorService,
  ) {
    this.timerInterval = 60 * 1000;
    this.periodOptions = this.monitorService.getPeriodOptions();
    this.timeRangeDisplay = {
      start_time: '',
      end_time: '',
    };
    this.timeRange = {
      start_time: '',
      end_time: '',
    };
    this.selectedPeriod = this.periodOptions[0]['period'];
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.initialized = true;
      this.refetch();
    }, 200);
  }

  ngOnDestroy() {
    clearTimeout(this.timerId);
  }

  periodChanged(option: any) {
    if (option && !this.isCustomTimeRange()) {
      this.refetch();
    }
  }

  onStartTimeChange(e: any) {
    this.timeRange.start_time = moment(e.dates[0]).valueOf();
  }

  onEndTimeChange(e: any) {
    this.timeRange.end_time = moment(e.dates[0]).valueOf();
  }

  isCustomTimeRange() {
    return this.selectedPeriod === -1;
  }

  refetch() {
    if (!this.initialized) {
      return;
    }
    if (this.isCustomTimeRange()) {
      if (this.timeRange.end_time <= this.timeRange.start_time) {
        this.auiMessageService.warning({
          id: 'timeRange',
          content: this.translateService.get('metric_timerange_invalid'),
        });
        return;
      }
      if (
        this.timeRange.end_time - this.timeRange.start_time <
        30 * 60 * 1000
      ) {
        this.auiMessageService.warning({
          id: 'timeRange',
          content: this.translateService.get('metric_timerange_should_less_30'),
        });
        return;
      }
    } else {
      this.calculateTimeRange();
    }
    this.charts.forEach((chart: ServiceMetricChartComponent) => {
      chart.setTimeRange(this.timeRange.start_time, this.timeRange.end_time);
    });
    if (!this.isCustomTimeRange()) {
      this.resetTimer();
    }
  }

  protected calculateTimeRange() {
    const end_time = moment().valueOf();
    this.timeRange = {
      start_time: end_time - this.selectedPeriod,
      end_time,
    };
    this.timeRangeDisplay = {
      start_time: moment(this.timeRange.start_time).format('YYYY-MM-DD HH:mm'),
      end_time: moment(this.timeRange.end_time).format('YYYY-MM-DD HH:mm'),
    };
  }

  protected resetTimer() {
    clearTimeout(this.timerId);
    this.timerId = window.setTimeout(() => {
      if (!this.isCustomTimeRange()) {
        this.refetch();
      }
    }, this.timerInterval);
  }
}
