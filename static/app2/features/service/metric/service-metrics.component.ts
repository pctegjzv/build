import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MessageService } from 'alauda-ui';
import { ServiceMetricsBase } from 'app2/features/service/metric/service-metrics-base';
import { MonitorService } from 'app2/shared/services/features/monitor.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-service-metrics',
  templateUrl: './service-metrics.component.html',
  styleUrls: ['./service-metrics.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceMetricsComponent extends ServiceMetricsBase
  implements OnInit {
  dimensionOptions: any[];
  selectedDimension: string;

  constructor(
    protected messageService: MessageService,
    protected translateService: TranslateService,
    protected monitorService: MonitorService,
  ) {
    super(messageService, translateService, monitorService);

    this.dimensionOptions = [
      { name: this.translateService.get('instance'), value: 'INSTANCE' },
      { name: this.translateService.get('component'), value: 'SERVICE' },
    ];
    this.selectedDimension = this.dimensionOptions[0]['value'];

    this.groupNameFormatting = function(tags: any, index: number) {
      if (tags.instance_id) {
        return tags.instance_id.substr(0, 8);
      } else if (tags.service_name) {
        return tags.service_name;
      } else {
        return `Series${index}`;
      }
    };
  }

  ngOnInit() {
    this.metrics = [
      {
        metricDisplayName: this.translateService.get('cpu_utilization'),
        metricQuery: {
          name: 'service.cpu.utilization',
          agg: 'avg',
          where: {
            service_id: this.uuid,
          },
          by: ['instance_id'],
        },
      },
      {
        metricDisplayName: this.translateService.get('memory_utilization'),
        metricQuery: {
          name: 'service.mem.utilization',
          agg: 'avg',
          where: {
            service_id: this.uuid,
          },
          by: ['instance_id'],
        },
      },
      {
        metricDisplayName: this.translateService.get('sent_bytes'),
        metricQuery: {
          name: 'service.net.bytes_sent',
          agg: 'avg',
          where: {
            service_id: this.uuid,
          },
          by: ['instance_id'],
        },
      },
      {
        metricDisplayName: this.translateService.get('received_bytes'),
        metricQuery: {
          name: 'service.net.bytes_rcvd',
          agg: 'avg',
          where: {
            service_id: this.uuid,
          },
          by: ['instance_id'],
        },
      },
    ];
  }

  dimensionChanged(option: any) {
    if (!option) {
      return;
    }
    const groupBy =
      this.selectedDimension === 'INSTANCE' ? ['instance_id'] : [];
    this.metrics.forEach((item: any) => {
      item.metricQuery = {
        ...item.metricQuery,
        by: groupBy,
      };
    });
  }
}
