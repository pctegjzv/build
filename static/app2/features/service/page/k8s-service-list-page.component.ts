import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import { DialogService } from 'alauda-ui';
import { ServiceCreateMethodSelectComponent } from 'app2/features/service/create-method-select/service-create-method-select.component';
import { ImageSelectComponent } from 'app2/features/service/image-select/image-select.component';
import { K8sServiceListComponent } from 'app2/features/service/list/k8s-service-list.component';
import { RcImageSelection } from 'app2/features/service/service.type';
import { RegionService } from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: 'k8s-service-list-page.component.html',
  styleUrls: ['k8s-service-list-page.component.scss'],
})
export class K8sServiceListPageComponent implements OnInit, OnDestroy {
  serviceCreateEnabled = false;

  loading = false;
  searching = false;
  searchQuery = '';
  regionName: Observable<string>;
  private regionSubscrption: Subscription;
  @ViewChild(K8sServiceListComponent)
  private k8sServiceListComponent: K8sServiceListComponent;

  constructor(
    private regionService: RegionService,
    private modal: ModalService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private router: Router,
  ) {}
  ngOnInit(): void {
    this.regionName = this.regionService.regionName$;

    this.regionSubscrption = this.regionService.region$
      .pipe(filter(region => !!region))
      .subscribe(async region => {
        if (region) {
          this.serviceCreateEnabled = await this.roleUtil.resourceTypeSupportPermissions(
            'service',
            { cluster_name: region.name },
            'create',
          );
        } else {
          this.serviceCreateEnabled = false;
        }
      });
  }

  createService() {
    this.openCreateTypeSelectionDialog();
  }

  async openCreateTypeSelectionDialog() {
    try {
      const modalRef = await this.dialogService.open(
        ServiceCreateMethodSelectComponent,
      );
      modalRef.componentInstance.initResourceType('service');
      modalRef.componentInstance.finish.pipe(first()).subscribe((res: any) => {
        modalRef.close();
        if (res === 'repo') {
          setTimeout(() => {
            this.openImageSelectionDialog();
          }, 200);
        } else if (res === 'yaml') {
          this.router.navigateByUrl('k8s_service/create?type=yaml');
        }
      });
    } catch (error) {
      //
    }
  }

  private async openImageSelectionDialog() {
    try {
      const modalRef = await this.modal.open(ImageSelectComponent, {
        title: this.translate.get('nav_select_image'),
        width: 800,
      });
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe((res: RcImageSelection) => {
          modalRef.close();
          if (res) {
            const queryParams = Object.assign(
              {
                type: 'repo',
              },
              res,
            );
            this.router.navigate(['k8s_service/create'], { queryParams });
          }
        });
    } catch (error) {
      //
    }
  }

  async searchChanged(searchQuery: string) {
    this.searching = true;
    this.searchQuery = searchQuery;

    await this.k8sServiceListComponent.paginationDataWrapper.pagination
      .pipe(first())
      .toPromise();
    if (this.searchQuery === searchQuery) {
      this.searching = false;
    }
  }

  async refetch() {
    this.loading = true;
    await this.k8sServiceListComponent.refetch();
    this.loading = false;
  }

  ngOnDestroy(): void {
    this.regionSubscrption.unsubscribe();
  }
}
