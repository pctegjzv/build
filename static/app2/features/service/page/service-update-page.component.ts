import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { K8sComponent } from 'app2/features/service/service.type';
import { ServiceService } from 'app2/shared/services/features/service.service';

@Component({
  templateUrl: './service-update-page.component.html',
})
export class ServiceUpdatePageComponent implements OnInit, OnDestroy {
  type: string;
  uuid: string;

  serviceData: K8sComponent;
  serviceYaml: string;

  initialized = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private serviceService: ServiceService,
  ) {}

  async ngOnInit() {
    this.type = this.route.snapshot.queryParamMap.get('type'); // repo, yaml
    if (!['repo', 'yaml'].includes(this.type)) {
      setTimeout(() => {
        this.router.navigateByUrl('/k8s_service');
      });
      return;
    }
    this.uuid = this.route.snapshot.paramMap.get('uuid');
    // console.log('update service page: ', this.uuid, this.type, !['repo', 'yaml'].includes(this.type));
    await this.initService();
    this.initialized = true;
  }

  ngOnDestroy() {}

  private async initService() {
    [this.serviceData, this.serviceYaml] = await Promise.all([
      this.serviceService.getK8sService(this.uuid),
      this.serviceService.getK8sServiceYaml(this.uuid),
    ]);
  }
}
