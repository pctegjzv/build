import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { flatMap } from 'lodash';

import { ServiceUtilitiesService } from 'app2/features/service/service-utilities.service';
import { TableComponent } from 'app2/shared/components/table/table.component';
import { AppService } from 'app2/shared/services/features/app.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ServiceService } from 'app2/shared/services/features/service.service';

/**
 * Referenced by other resources, do not use alone.
 *
 * Usage:
 * <rc-referenced-service-list [params]="{KEY: VALUE}"></rc-referenced-service-list>
 */
@Component({
  selector: 'rc-referenced-service-list',
  templateUrl: './referenced-service-list.component.html',
  styleUrls: ['./referenced-service-list.component.scss'],
})
export class ReferencedServiceListComponent implements OnInit, OnDestroy {
  @Input()
  apiVersion: 'v1' | 'v2'; // default is v2
  @Input()
  params: any;
  @ViewChild('serviceTable')
  table: TableComponent;
  loading = false;
  loadError = false;
  serviceItems: any[] = [];
  region: any;

  private pollingTimer: any = null;

  private destroyed = false;

  constructor(
    public serviceUtilities: ServiceUtilitiesService,
    private serviceService: ServiceService,
    private appService: AppService,
    private regionService: RegionService,
  ) {}

  async ngOnInit() {
    this.region = await this.regionService.getCurrentRegion();
    this.loading = true;
    await this.refetch();
    this.loading = false;
  }

  /**
   * uuid, service_name, current_status, target_num_instances, healthy_num_instances, image_name, image_tag
   * @returns {Promise<void>}
   */
  async getV1Services() {
    try {
      const [serviceItems, appItems] = await Promise.all([
        this.serviceService
          .getV1Services({
            ...this.params,
            instance_ip: this.params.host_ip,
            pageSize: 100,
            region_name: this.region.name,
            basic: false,
          })
          .then(({ results }) => results)
          .catch(() => []),
        this.appService.getApps({
          regionName: this.region.name,
          instance_ip: this.params.host_ip,
          basic: false,
          page_size: 100,
        }),
      ]);
      // const serviceItems = res.results;
      const appServiceItems = flatMap(appItems, (app: any) => {
        return app.services.map((service: any) => {
          return {
            ...service,
            service_name: `${service.app_name}.${service.service_name}`,
          };
        });
      });
      this.serviceItems = serviceItems.concat(appServiceItems);
    } catch (err) {
      this.loadError = true;
    }
  }

  async getV2Services() {
    if (this.destroyed) {
      return;
    }
    try {
      const res = await this.serviceService.getK8sServices({
        pageNo: 1,
        pageSize: 100,
        params: {
          cluster: this.region.name,
          ...this.params,
        },
      });
      this.serviceItems = res.results;
      this.serviceItems.forEach((item: any) => {
        item.imageDisplayParams = this.serviceUtilities.getServiceImageDisplayParams(
          item,
          'api',
        );
      });
    } catch ({ errors }) {
      this.loadError = true;
    }
  }

  ngOnDestroy() {
    this.destroyed = true;
    clearTimeout(this.pollingTimer);
  }

  async refetch() {
    this.loadError = false;
    if (this.apiVersion === 'v1') {
      await this.getV1Services();
    } else {
      await this.getV2Services();
    }
  }

  get empty() {
    return !this.serviceItems || !this.serviceItems.length;
  }
}
