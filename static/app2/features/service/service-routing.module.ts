import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { K8sServiceDetailComponent } from 'app2/features/service/detail/k8s-service-detail.component';
import { K8sServiceListPageComponent } from 'app2/features/service/page/k8s-service-list-page.component';
import { ServiceCreatePageComponent } from 'app2/features/service/page/service-create-page.component';
import { ServiceUpdatePageComponent } from 'app2/features/service/page/service-update-page.component';

const k8sServiceRoutes: Routes = [
  {
    path: '',
    component: K8sServiceListPageComponent,
  },
  {
    path: 'create',
    component: ServiceCreatePageComponent,
  },
  {
    path: 'update/:uuid', // queryString: type=yaml/repo
    component: ServiceUpdatePageComponent,
  },
  {
    path: 'detail/:uuid',
    component: K8sServiceDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(k8sServiceRoutes)],
  exports: [RouterModule],
  declarations: [],
})
export class ServiceRoutingModule {}
