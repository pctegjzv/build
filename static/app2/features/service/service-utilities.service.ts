import { EventEmitter, Inject, Injectable } from '@angular/core';
import { cloneDeep, get, toString } from 'lodash';
import { first, take } from 'rxjs/operators';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { K8sServiceContainerSelectComponent } from 'app2/features/service/container-select/k8s-service-container-select.component';
import { K8sServiceExecComponent } from 'app2/features/service/exec/k8s-service-exec.component';
import { ImageDisplayParams } from 'app2/features/service/image-display/image-display.component';
import { K8sComponent } from 'app2/features/service/service.type';
import { ServiceUpdateConfigmapComponent } from 'app2/features/service/update-configmap/service-update-configmap.component';
import { ServiceUpdateEndpointsComponent } from 'app2/features/service/update-endpoints/service-update-endpoints.component';
import { ServiceUpdateEnvvarsComponent } from 'app2/features/service/update-envvars/service-update-envvars.component';
import { K8sServiceUpdateHealthCheckComponent } from 'app2/features/service/update-health-check/k8s-service-update-health-check.component';
import { ServiceUpdateImageTagComponent } from 'app2/features/service/update-image-tag/service-update-image-tag.component';
import { ServiceUpdateReplicasComponent } from 'app2/features/service/update-replicas/service-update-replicas.component';
import { ServiceUpdateResourceSizeComponent } from 'app2/features/service/update-resource-size/service-update-resource-size.component';
import { ServiceUpdateScalingComponent } from 'app2/features/service/update-scaling/service-update-scaling.component';
import { ServiceVersionSelectComponent } from 'app2/features/service/version-select/service-version-select.component';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ModalMode } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';

@Injectable()
export class ServiceUtilitiesService {
  constructor(
    @Inject(ACCOUNT) public account: RcAccount,
    private modalService: ModalService,
    private serviceService: ServiceService,
    private roleUtilities: RoleUtilitiesService,
    private translateService: TranslateService,
  ) {}

  getServiceImageDisplayParams(
    service: any,
    origin: 'api' | 'viewModel',
  ): ImageDisplayParams[] {
    let containers: any = [];
    switch (origin) {
      case 'api':
        containers = service.resource.containers || [];
        return containers.map((item: any) => {
          return {
            image: item.image,
            size: {
              cpu: get(item, 'size.cpu', ''),
              memory: get(item, 'size.memory', '') || get(item, 'size.mem', ''),
            },
          };
        });
        break;
      case 'viewModel':
        containers = service.data.containers || [];
        return containers.map((item: any) => {
          return {
            image: `${item.config.image}:${item.config.imageTag}`,
            size: {
              cpu: toString(item.config.resources.requests.cpu),
              memory: toString(item.config.resources.requests.memory),
            },
          };
        });
        break;
      default:
        break;
    }
  }

  /**
   * Service status: Running/Warning/Error/Stopped/Deploying
   * enable / disable
   * @param app
   */
  canStart(service: any) {
    return ['stopped'].includes(service.resource.status.toLowerCase());
  }

  canStop(service: any) {
    return (
      ['running', 'warning', 'error'].includes(
        service.resource.status.toLowerCase(),
      ) || !!get(service, 'resource.actions.stop', false)
    );
  }

  canDelete(service: any) {
    return ['running', 'stopped', 'error', 'warning', 'deploying'].includes(
      service.resource.status.toLowerCase(),
    );
  }

  canUpdate(service: any) {
    return !['deploying'].includes(service.resource.status.toLowerCase());
  }

  canRollback(service: any) {
    return !!get(service, 'resource.actions.rollback', false);
  }

  canRetry(service: any) {
    return !!get(service, 'resource.actions.retry', false);
  }

  /**
   * Service permission: service:start
   * show / hide
   * @param service
   */
  showStart(service: any) {
    return this.roleUtilities.resourceHasPermission(
      service,
      'service',
      'start',
    );
  }

  showStop(service: any) {
    return this.roleUtilities.resourceHasPermission(service, 'service', 'stop');
  }

  showUpdate(service: any) {
    return this.roleUtilities.resourceHasPermission(
      service,
      'service',
      'update',
    );
  }

  showRevision(service: K8sComponent) {
    return this.showUpdate(service) && service.resource.kind === 'Deployment';
  }

  showDelete(service: any) {
    return this.roleUtilities.resourceHasPermission(
      service,
      'service',
      'delete',
    );
  }

  showRetry(service: any) {
    return this.roleUtilities.resourceHasPermission(
      service,
      'service',
      'update',
    );
  }

  async startService(name: string, uuid: string) {
    await this.modalService.confirm({
      title: this.translateService.get('start'),
      content: this.translateService.get('app_service_start_service_confirm', {
        service_name: name,
      }),
    });
    return this.serviceService.startK8sService(uuid);
  }

  async stopService(name: string, uuid: string) {
    await this.modalService.confirm({
      title: this.translateService.get('stop'),
      content: this.translateService.get('app_service_stop_service_confirm', {
        service_name: name,
      }),
    });
    return this.serviceService.stopK8sService(uuid);
  }

  async deleteService(name: string, uuid: string) {
    await this.modalService.confirm({
      title: this.translateService.get('delete'),
      content: this.translateService.get('app_service_delete_service_confirm', {
        service_name: name,
      }),
    });
    return this.serviceService.deleteK8sService(uuid);
  }

  async rollbackService(name: string, uuid: string) {
    await this.modalService.confirm({
      title: this.translateService.get('rollback'),
      content: this.translateService.get(
        'app_service_rollback_service_confirm',
        {
          service_name: name,
        },
      ),
    });
    return this.serviceService.rollbackK8sService(uuid);
  }

  async revisionService(uuid: string) {
    const modalRef = this.modalService.open(ServiceVersionSelectComponent, {
      title: this.translateService.get('rollback'),
      width: 650,
      mode: ModalMode.RIGHT_SLIDER,
      data: {
        uuid,
      },
    });
    modalRef.componentInstance.finish.pipe(first()).subscribe(() => {
      modalRef.close();
    });
    return modalRef.componentInstance.finish;
  }

  async retryUpdateService(name: string, uuid: string) {
    await this.modalService.confirm({
      title: this.translateService.get('retry'),
      content: this.translateService.get('app_service_retry_update_confirm', {
        service_name: name,
      }),
    });
    return this.serviceService.retryUpdateK8sService(uuid);
  }

  updateReplicas(uuid: string): EventEmitter<boolean> {
    const modalRef = this.modalService.open(ServiceUpdateReplicasComponent, {
      title: this.translateService.get('scaling'),
      data: {
        uuid,
      },
    });
    modalRef.componentInstance.close.pipe(first()).subscribe(() => {
      modalRef.close();
    });
    return modalRef.componentInstance.close;
  }

  updateScalingConfig(uuid: string, name: string) {
    this.modalService.open(ServiceUpdateScalingComponent, {
      title: this.translateService.get('auto_scaling'),
      data: {
        uuid,
        name,
      },
      width: 976,
    });
  }

  updateEndpoints(uuid: string) {
    const modalRef = this.modalService.open(ServiceUpdateEndpointsComponent, {
      title: this.translateService.get('update_default_domain'),
      data: {
        uuid,
      },
      width: 650,
    });
    return modalRef.componentInstance.refresh;
  }

  async updateHealthCheck({
    uuid,
    containers,
    kubernetes,
  }: {
    uuid: string;
    containers: any;
    kubernetes?: any;
  }) {
    const serviceId = uuid;

    if (containers.length > 1) {
      let selectContainerInstance: K8sServiceContainerSelectComponent;

      const selectContainerRef = this.modalService.open(
        K8sServiceContainerSelectComponent,
        {
          title: this.translateService.get('k8s_service_select_container'),
          data: {
            serviceId,
            containers,
            kubernetes,
            nextStep: () => {
              const modalRef = this.updateHealthCheckModal({
                activeIndex: selectContainerInstance.activeIndex,
                kubernetes: selectContainerInstance.kubernetes,
                serviceId,
              });

              modalRef.componentInstance.close.subscribe(
                (prevStep: boolean) => {
                  if (!prevStep) {
                    selectContainerInstance.close.emit();
                  }
                  modalRef.close();
                },
              );
            },
          },
        },
      );

      selectContainerInstance = selectContainerRef.componentInstance;

      selectContainerInstance.close
        .pipe(take(1))
        .subscribe(() => selectContainerRef.close());

      return;
    }

    const modalRef = this.updateHealthCheckModal({
      activeIndex: 0,
      serviceId,
      kubernetes,
    });

    modalRef.componentInstance.close.subscribe(() => modalRef.close());
  }

  private updateHealthCheckModal({
    activeIndex,
    kubernetes,
    serviceId,
  }: {
    activeIndex: number;
    kubernetes?: any;
    serviceId: string;
  }) {
    return this.modalService.open(K8sServiceUpdateHealthCheckComponent, {
      title: this.translateService.get('k8s_service_update_health_check'),
      width: 1000,
      data: {
        activeIndex: activeIndex,
        kubernetes: kubernetes && cloneDeep(kubernetes),
        serviceId,
      },
    });
  }

  serviceExec(data: { serviceId: string; serviceApplication?: string }) {
    const modalRef = this.modalService.open(K8sServiceExecComponent, {
      title: 'EXEC',
      data,
    });

    modalRef.componentInstance.close.pipe(take(1)).subscribe(() => {
      modalRef.close();
    });
  }

  updateImageTag({
    uuid,
    containers,
    kubernetes,
  }: {
    uuid: string;
    containers: any;
    kubernetes?: any; // 从service API获取的response中kubernetes字段，若为空需要手动调用API获取
  }): EventEmitter<boolean> {
    const finish$ = new EventEmitter<boolean>();
    const serviceId = uuid;

    if (containers.length > 1) {
      let selectContainerInstance: K8sServiceContainerSelectComponent;

      const selectContainerRef = this.modalService.open(
        K8sServiceContainerSelectComponent,
        {
          title: this.translateService.get('k8s_service_select_container'),
          data: {
            serviceId,
            containers,
            kubernetes,
            nextStep: () => {
              const modalRef = this.updateImageTagModal({
                activeIndex: selectContainerInstance.activeIndex,
                kubernetes: selectContainerInstance.kubernetes,
                serviceId,
              });
              modalRef.componentInstance.close
                .pipe(first())
                .subscribe(
                  ({
                    prevStep,
                    finish,
                  }: {
                    prevStep: boolean;
                    finish: boolean;
                  }) => {
                    modalRef.close();
                    if (!prevStep) {
                      selectContainerInstance.close.emit();
                    }
                    if (finish) {
                      finish$.emit(true);
                    }
                  },
                );
            },
          },
        },
      );
      selectContainerInstance = selectContainerRef.componentInstance;
      selectContainerInstance.close
        .pipe(take(1))
        .subscribe(() => selectContainerRef.close());
    } else {
      const modalRef = this.updateImageTagModal({
        activeIndex: 0,
        serviceId,
        kubernetes,
      });
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe(({ finish }: { finish: boolean }) => {
          modalRef.close();
          if (finish) {
            finish$.emit(true);
          }
        });
    }
    return finish$;
  }

  updateResourceSize({
    uuid,
    containers,
    kubernetes,
  }: {
    uuid: string;
    containers: any;
    kubernetes?: any;
  }): EventEmitter<boolean> {
    const finish$ = new EventEmitter<boolean>();
    const serviceId = uuid;

    if (containers.length > 1) {
      let selectContainerInstance: K8sServiceContainerSelectComponent;

      const selectContainerRef = this.modalService.open(
        K8sServiceContainerSelectComponent,
        {
          title: this.translateService.get('k8s_service_select_container'),
          data: {
            serviceId,
            containers,
            kubernetes,
            nextStep: () => {
              const modalRef = this.updateResourceSizeModal({
                activeIndex: selectContainerInstance.activeIndex,
                kubernetes: selectContainerInstance.kubernetes,
                serviceId,
              });
              modalRef.componentInstance.close
                .pipe(first())
                .subscribe(
                  ({
                    prevStep,
                    finish,
                  }: {
                    prevStep: boolean;
                    finish: boolean;
                  }) => {
                    modalRef.close();
                    if (!prevStep) {
                      selectContainerInstance.close.emit();
                    }
                    if (finish) {
                      finish$.emit(true);
                    }
                  },
                );
            },
          },
        },
      );
      selectContainerInstance = selectContainerRef.componentInstance;
      selectContainerInstance.close
        .pipe(take(1))
        .subscribe(() => selectContainerRef.close());
    } else {
      const modalRef = this.updateResourceSizeModal({
        activeIndex: 0,
        serviceId,
        kubernetes,
      });
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe(({ finish }: { finish: boolean }) => {
          modalRef.close();
          if (finish) {
            finish$.emit(true);
          }
        });
    }
    return finish$;
  }

  updateConfigmap({
    uuid,
    containers,
    kubernetes,
    namespace,
  }: {
    uuid: string;
    containers: any;
    kubernetes?: any;
    namespace: string;
  }): EventEmitter<boolean> {
    const finish$ = new EventEmitter<boolean>();
    const serviceId = uuid;

    if (containers.length > 1) {
      let selectContainerInstance: K8sServiceContainerSelectComponent;

      const selectContainerRef = this.modalService.open(
        K8sServiceContainerSelectComponent,
        {
          title: this.translateService.get('k8s_service_select_container'),
          data: {
            serviceId,
            containers,
            kubernetes,
            nextStep: () => {
              const modalRef = this.updateConfigmapModal({
                activeIndex: selectContainerInstance.activeIndex,
                kubernetes: selectContainerInstance.kubernetes,
                serviceId,
                namespace,
              });
              modalRef.componentInstance.close
                .pipe(first())
                .subscribe(
                  ({
                    prevStep,
                    finish,
                  }: {
                    prevStep: boolean;
                    finish: boolean;
                  }) => {
                    modalRef.close();
                    if (!prevStep) {
                      selectContainerInstance.close.emit();
                    }
                    if (finish) {
                      finish$.emit(true);
                    }
                  },
                );
            },
          },
        },
      );
      selectContainerInstance = selectContainerRef.componentInstance;
      selectContainerInstance.close
        .pipe(take(1))
        .subscribe(() => selectContainerRef.close());
    } else {
      const modalRef = this.updateConfigmapModal({
        activeIndex: 0,
        serviceId,
        kubernetes,
        namespace,
      });
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe(({ finish }: { finish: boolean }) => {
          modalRef.close();
          if (finish) {
            finish$.emit(true);
          }
        });
    }
    return finish$;
  }

  updateEnvvars({
    uuid,
    containers,
    kubernetes,
    namespace,
  }: {
    uuid: string;
    containers: any;
    kubernetes?: any;
    namespace: string;
  }): EventEmitter<boolean> {
    const finish$ = new EventEmitter<boolean>();
    const serviceId = uuid;

    if (containers.length > 1) {
      let selectContainerInstance: K8sServiceContainerSelectComponent;

      const selectContainerRef = this.modalService.open(
        K8sServiceContainerSelectComponent,
        {
          title: this.translateService.get('k8s_service_select_container'),
          data: {
            serviceId,
            containers,
            kubernetes,
            nextStep: () => {
              const modalRef = this.updateEnvvarsModal({
                activeIndex: selectContainerInstance.activeIndex,
                kubernetes: selectContainerInstance.kubernetes,
                serviceId,
                namespace,
              });
              modalRef.componentInstance.close
                .pipe(first())
                .subscribe(
                  ({
                    prevStep,
                    finish,
                  }: {
                    prevStep: boolean;
                    finish: boolean;
                  }) => {
                    modalRef.close();
                    if (!prevStep) {
                      selectContainerInstance.close.emit();
                    }
                    if (finish) {
                      finish$.emit(true);
                    }
                  },
                );
            },
          },
        },
      );
      selectContainerInstance = selectContainerRef.componentInstance;
      selectContainerInstance.close
        .pipe(take(1))
        .subscribe(() => selectContainerRef.close());
    } else {
      const modalRef = this.updateEnvvarsModal({
        activeIndex: 0,
        serviceId,
        kubernetes,
        namespace,
      });
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe(({ finish }: { finish: boolean }) => {
          modalRef.close();
          if (finish) {
            finish$.emit(true);
          }
        });
    }
    return finish$;
  }

  private updateImageTagModal({
    activeIndex,
    kubernetes,
    serviceId,
  }: {
    activeIndex: number;
    kubernetes?: any;
    serviceId: string;
  }) {
    return this.modalService.open(ServiceUpdateImageTagComponent, {
      title: this.translateService.get('update_image_tag'),
      width: 650,
      data: {
        activeIndex: activeIndex,
        kubernetes: kubernetes && cloneDeep(kubernetes),
        serviceId,
      },
    });
  }

  private updateResourceSizeModal({
    activeIndex,
    kubernetes,
    serviceId,
  }: {
    activeIndex: number;
    kubernetes?: any;
    serviceId: string;
  }) {
    return this.modalService.open(ServiceUpdateResourceSizeComponent, {
      title: this.translateService.get('k8s_service_update_resource_size'),
      width: 800,
      data: {
        activeIndex,
        kubernetes: kubernetes && cloneDeep(kubernetes),
        serviceId,
      },
    });
  }

  private updateConfigmapModal({
    activeIndex,
    kubernetes,
    serviceId,
    namespace,
  }: {
    activeIndex: number;
    kubernetes?: any;
    serviceId: string;
    namespace: string;
  }) {
    return this.modalService.open(ServiceUpdateConfigmapComponent, {
      title: this.translateService.get('k8s_service_update_configmap'),
      width: 600,
      data: {
        activeIndex,
        kubernetes: kubernetes && cloneDeep(kubernetes),
        serviceId,
        namespace,
      },
    });
  }

  private updateEnvvarsModal({
    activeIndex,
    kubernetes,
    serviceId,
    namespace,
  }: {
    activeIndex: number;
    kubernetes?: any;
    serviceId: string;
    namespace: string;
  }) {
    return this.modalService.open(ServiceUpdateEnvvarsComponent, {
      title: this.translateService.get('k8s_service_update_envvars'),
      width: 800,
      data: {
        activeIndex,
        kubernetes: kubernetes && cloneDeep(kubernetes),
        serviceId,
        namespace,
      },
    });
  }
}
