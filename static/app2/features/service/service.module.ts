import { NgModule } from '@angular/core';
import { ServiceAppSharedModule } from 'app2/features/lazy/service-app.shared.module';
import { ServiceCatalogServiceSharedModule } from 'app2/features/lazy/service_catalog-service.shared.module';
import { ServiceUtilitiesService } from 'app2/features/service/service-utilities.service';
import { SharedModule } from 'app2/shared/shared.module';

import { K8sServiceContainerSelectComponent } from './container-select/k8s-service-container-select.component';
import { ServiceCreateUiComponent } from './create-ui/service-create-ui.component';
import { K8sServiceCreateComponent } from './create/k8s-service-create.component';
import { K8sServiceDetailComponent } from './detail/k8s-service-detail.component';
import { K8sServiceListComponent } from './list/k8s-service-list.component';
import { K8sServiceListPageComponent } from './page/k8s-service-list-page.component';
import { ServiceCreatePageComponent } from './page/service-create-page.component';
import { ServiceUpdatePageComponent } from './page/service-update-page.component';
import { ServiceRoutingModule } from './service-routing.module';
import { K8sServiceUpdateComponent } from './update/k8s-service-update.component';

@NgModule({
  imports: [
    SharedModule,
    ServiceAppSharedModule,
    ServiceCatalogServiceSharedModule,
    ServiceRoutingModule,
  ],
  declarations: [
    K8sServiceListPageComponent,
    K8sServiceListComponent,
    K8sServiceCreateComponent,
    K8sServiceUpdateComponent,
    K8sServiceDetailComponent,
    ServiceCreateUiComponent,
    ServiceCreatePageComponent,
    ServiceUpdatePageComponent,
    K8sServiceContainerSelectComponent,
  ],
  exports: [],
  entryComponents: [K8sServiceContainerSelectComponent],
  providers: [ServiceUtilitiesService],
})
export class K8sServiceModule {}
