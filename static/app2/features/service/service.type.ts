/**
 * K8S component定义
 */
export interface K8sComponent {
  // 更新策略
  strategy?: any;
  // 一级属性
  resource_actions?: string[];
  resource: {
    uuid: string;
    name: string;
    kind: 'Deployment' | 'StatefulSet' | 'DaemonSet';
    created_at: string;
    updated_at: string;
    create_method: string;
    status: string;
    actions: {
      rollback: boolean;
      retry: boolean;
    };
    instances: {
      desired: number;
      current: number;
    };
    containers?: {
      image: string;
      size?: {
        cpu: string;
        mem: string;
      };
    }[];
  };
  namespace: {
    uuid: string;
    name: string;
  };
  cluster: {
    uuid: string;
    name: string;
  };
  parent?: {
    uuid: string;
    name: string;
    type: 'App';
  };
  referenced?: any;
  kubernetes?: any[];
}

/**
 * legacy Service 和 K8s service通用类型
 */
export interface K8sComponentInfo {
  app_name: string;
  service_name: string;
  uuid: string;
  service_namespace: string;
  ports: number[];
}

export interface ContainerEnvvar {
  name: string;
  value?: string;
  valueFrom?: {
    configMapKeyRef: {
      key: string;
      name: string;
    };
  };
}

/**
 * @ViewModel
 */
export interface ContainerViewModel {
  name: string;
  image: string;
  imageTag: string;
  resources?: ContainerResourcesViewModel;
  volumes?: any[];
  healthCheck?: any;
  entrypoint?: string;
  cmd?: string[];
  envvars?: ContainerEnvvar[];
  configmaps?: string[];
  logFiles?: string[];
  excludeLogFiles?: string[];
}

export interface ComponentViewModel {
  name: string;
  namespace: string;
  owners?: any[];
  kind: string;
  replicas: number;
  labels: {
    name: string;
    value: string;
  }[];
  minReplicas: number;
  maxReplicas: number;
  nodeTags: string[];
  nodeAffinity?: string[];
  affinity: {
    affinityServices?: {
      name: string;
      uuid: string;
      namespace: string;
    };
    antiAffinityServices: {
      name: string;
      uuid: string;
      namespace: string;
    };
  };
  maxSurge: string;
  maxUnavailable: string;
  kubeServices: any[];
  containers: {
    config: ContainerViewModel;
    index: number;
  }[];
  networkMode: {
    hostNetwork: boolean;
    subnet: {
      id: string;
      ipAddress: string[];
    };
  };
}

export interface NetworkModeViewModel {
  hostNetwork: boolean;
  subnet: { name: string; ipAddress: string[] } | null;
}

export interface ContainerResourcesViewModel {
  requests?: {
    memory?: string;
    cpu?: string;
  };
  limits?: {
    memory?: string;
    cpu?: string;
  };
}

export interface EnvvarViewModel {
  index: number;
  name: string;
  value: string;
  reference: boolean;
  configmap: string;
  configmapKey: string;
}

export interface LogFileDetailViewModel {
  logFiles: string[];
  excludeLogFiles: string[];
}

export interface ServiceViewModel {
  name: string;
  type: 'ClusterIP' | 'Headless' | 'NodePort';
  ports: ServicePort[];
  route?: ServiceRoute;
}

export interface ServicePort {
  port?: number; // 服务端口
  containerPort: number; // 容器端口 yaml里的targetPort
  nodePort?: number; // 节点端口
  loadBalancerName?: string; // 负载均衡名称
  protocol?: string; // 负载均衡protocol, Service ports里的protocol 目前默认是TCP
  listenerPort?: number; // 负载均衡监听端口
}

export interface ServiceRoute {
  host: string;
  path?: string;
}

export declare interface RcImageSelection {
  is_public_registry?: boolean;
  registry_name?: string;
  registry_endpoint?: string;
  project_name?: string;
  repository_name?: string;
  full_image_name?: string;
  tag?: string;
}

export interface ContainerVolumeViewModel {
  _uniqueName?: string;
  index?: number;
  name?: string;
  type?: 'host-path' | 'volume' | 'pvc' | 'configmap';
  containerPath: string;
  subPath?: string;
  // host-path
  hostPath?: string;
  // pvc
  pvcName?: string;
  // volume
  volumeName?: string;
  driverName?: string;
  driverVolumeId?: string;
  // configmap
  configMapName?: string;
  configMapUuid?: string;
  configMapKeyRef?: boolean;
  configMapKeyMap?: {
    index: number;
    key: string;
    path: string;
  }[];
}
