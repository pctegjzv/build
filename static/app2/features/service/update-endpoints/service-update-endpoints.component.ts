import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { MessageService } from 'alauda-ui';
import {
  LoadBalancer,
  NetworkService,
} from 'app2/shared/services/features/network.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

import { INT_PATTERN } from '../../../../app/components/common/config/common-pattern';

interface LoadbalancerListener {
  loadbalancer_name?: string;
  loadbalancer_id: string;
  service_id: string;
  container_port: string;
  listener_port: string;
  protocol: string;
}

@Component({
  templateUrl: './service-update-endpoints.component.html',
})
export class ServiceUpdateEndpointsComponent implements OnInit {
  @Output()
  refresh = new EventEmitter<boolean>();
  @ViewChild('form')
  form: NgForm;
  portNumberReg = INT_PATTERN;
  initialized: boolean;
  submitting: boolean;
  unbindSubmitting: boolean;
  loadbalancerOptions: any[] = [];
  protocolOptions = [
    {
      name: 'HTTP',
      value: 'http',
    },
    {
      name: 'TCP',
      value: 'tcp',
    },
  ];
  listeners: LoadbalancerListener[] = [];
  newListener: LoadbalancerListener;
  private serviceId: string;
  private regionName: string;
  private selectedLoadbalancerName: string;
  constructor(
    @Inject(MODAL_DATA)
    private modalData: {
      uuid: string;
    },
    private networkService: NetworkService,
    private regionService: RegionService,
    private errorsToastService: ErrorsToastService,
    private auiMessageService: MessageService,
    private translateService: TranslateService,
  ) {}

  async ngOnInit() {
    this.serviceId = this.modalData.uuid;
    this.newListener = {
      service_id: this.serviceId,
      protocol: 'http',
      container_port: null,
      listener_port: null,
      loadbalancer_id: '',
    };
    const region: Region = await this.regionService.getCurrentRegion();
    this.regionName = region.name;
    await Promise.all([this.getListeners(), this.getLoadbalancerOptions()]);
    this.initialized = true;
    // console.log(this.listeners);
  }

  async unbind(listener: LoadbalancerListener) {
    if (this.unbindSubmitting) {
      return;
    }
    const data = {
      action: 'unbind',
      listeners: [
        {
          service_id: this.serviceId,
          protocol: listener.protocol,
          listener_port: parseInt(listener.listener_port, 10),
          container_port: parseInt(listener.container_port, 10),
        },
      ],
    };
    this.unbindSubmitting = true;
    try {
      await this.networkService.bind(listener.loadbalancer_id, data);
      this.auiMessageService.success({
        content: this.translateService.get('service_unbind_listener_success'),
      });
      this.listeners = this.listeners.filter((item: LoadbalancerListener) => {
        return !(
          item.loadbalancer_id === listener.loadbalancer_id &&
          item.service_id === listener.service_id &&
          item.container_port === listener.container_port &&
          item.listener_port === listener.listener_port
        );
      });
      this.refresh.emit();
    } catch (err) {
      this.errorsToastService.handleGenericAjaxError({
        errors: err.errors,
        handleNonGenericCodes: true,
      });
    }
    this.unbindSubmitting = false;
  }

  async bind() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    await this.bindListener();
  }

  onLoadbalancerSelect(option: any) {
    if (option) {
      this.selectedLoadbalancerName = option.name;
    }
  }

  private resetNewListener() {
    this.selectedLoadbalancerName = '';
    this.form.resetForm({
      containerPort: null,
      listenerPort: null,
      loadbalancer: null,
      protocol: 'http',
    });
  }

  private async getLoadbalancerOptions() {
    try {
      const res = await this.networkService.getList({
        region_name: this.regionName,
      });
      this.loadbalancerOptions = res.map((item: LoadBalancer) => {
        return {
          name: item.name,
          uuid: item.load_balancer_id,
        };
      });
    } catch (err) {
      this.loadbalancerOptions = [];
    }
  }

  private async getListeners() {
    try {
      const res = await this.networkService.getList({
        region_name: this.regionName,
        service_id: this.serviceId,
        frontend: true,
      });
      res.forEach((lb: LoadBalancer) => {
        if (lb.listeners && lb.listeners.length) {
          lb.listeners.forEach(
            (listener: {
              service_id: string;
              container_port: string;
              listener_port: string;
              protocol: string;
            }) => {
              // 虽然请求里指定了service_id，但默认服务的listener仍然会返回，API暂时没有处理这种情况
              if (listener.service_id === this.serviceId) {
                this.listeners.push({
                  ...listener,
                  loadbalancer_name: lb.name,
                  loadbalancer_id: lb.load_balancer_id,
                });
              }
            },
          );
        }
      });
    } catch (err) {
      this.listeners = [];
    }
  }

  private async bindListener() {
    const data = {
      action: 'bind',
      listeners: [
        {
          service_id: this.serviceId,
          protocol: this.newListener.protocol,
          listener_port: parseInt(this.newListener.listener_port, 10),
          container_port: parseInt(this.newListener.container_port, 10),
        },
      ],
    };
    this.submitting = true;
    try {
      await this.networkService.bind(this.newListener.loadbalancer_id, data);
      this.auiMessageService.success({
        content: this.translateService.get('service_bind_listener_success'),
      });
      this.listeners.push({
        loadbalancer_name: this.selectedLoadbalancerName,
        loadbalancer_id: this.newListener.loadbalancer_id,
        service_id: this.serviceId,
        protocol: this.newListener.protocol,
        listener_port: this.newListener.listener_port,
        container_port: this.newListener.container_port,
      });
      this.refresh.emit();
      this.resetNewListener();
    } catch (err) {
      this.errorsToastService.handleGenericAjaxError({
        errors: err.errors,
        handleNonGenericCodes: true,
      });
    }
    this.submitting = false;
  }
}
