import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { cloneDeep, compact, unset } from 'lodash';

import { MessageService } from 'alauda-ui';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './service-update-envvars.component.html',
})
export class ServiceUpdateEnvvarsComponent implements OnInit {
  @Output()
  close = new EventEmitter<{ prevStep?: boolean; finish?: boolean }>();
  @ViewChild('form')
  form: NgForm;
  private activeIndex = 0;
  private kubernetes: any;
  private containers: any;
  containerEnvvars: any;
  initialized: boolean;
  submitting: boolean;
  namespace: string;

  constructor(
    @Inject(MODAL_DATA)
    private data: {
      activeIndex: number;
      kubernetes: any;
      serviceId: string;
      namespace: string;
    },
    private serviceService: ServiceService,
    private auiMessageService: MessageService,
    private translateService: TranslateService,
    private errorsToastService: ErrorsToastService,
  ) {}

  async ngOnInit() {
    this.namespace = this.data.namespace;
    this.activeIndex = this.data.activeIndex;
    let { kubernetes } = this.data;
    if (!kubernetes) {
      const service = await this.serviceService.getK8sService(
        this.data.serviceId,
      );
      kubernetes = service.kubernetes;
    }
    this.kubernetes = kubernetes;
    await this.getContainerEnvvars();
    this.initialized = true;
  }

  private async getContainerEnvvars() {
    this.containers = this.serviceService.getServiceViewModel(this.kubernetes)[
      'containers'
    ];
    this.containerEnvvars = this.containers[this.activeIndex].config.envvars;
  }

  shouldShowPrevStep() {
    return this.containers && this.containers.length > 1;
  }

  cancel() {
    this.close.emit({});
  }

  prevStep() {
    this.close.emit({ prevStep: true });
  }

  async confirm() {
    if (this.form.invalid) {
      return;
    }
    const INCLUDED_KINDS = ['Deployment', 'DaemonSet', 'StatefulSet'];
    const kubernetes = cloneDeep(this.kubernetes);
    const service = kubernetes.find(({ kind }: any) =>
      INCLUDED_KINDS.includes(kind),
    );
    const container = service.spec.template.spec.containers[this.activeIndex];
    const env = container.env || [];
    const logFiles = env.find((item: { name: string; value: string }) => {
      return item.name === '__ALAUDA_FILE_LOG_PATH__';
    });
    const excludeLogFiles = env.find(
      (item: { name: string; value: string }) => {
        return item.name === '__ALAUDA_EXCLUDE_LOG_PATH__';
      },
    );

    if (this.containerEnvvars.length) {
      container.env = compact([
        logFiles,
        excludeLogFiles,
        ...this.containerEnvvars,
      ]);
    } else if (!logFiles && !excludeLogFiles) {
      unset(container, 'env');
    }
    this.submitting = true;
    try {
      await this.serviceService.patchK8sService(this.data.serviceId, {
        kubernetes,
      });
      this.auiMessageService.success({
        content: this.translateService.get('update_success'),
      });
      this.close.emit({ finish: true });
    } catch (err) {
      this.errorsToastService.handleGenericAjaxError({
        errors: err.errors,
        handleNonGenericCodes: true,
        fallbackMessage: this.translateService.get('update_failed'),
      });
    }
    this.submitting = false;
  }
}
