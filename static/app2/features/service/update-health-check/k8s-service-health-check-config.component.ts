import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { cloneDeep } from 'lodash';

interface Header {
  name: string;
  value: string;
}

export interface HealthCheckConfigItem {
  protocol: 'HTTP' | 'TCP';
  initialDelaySeconds: number;
  periodSeconds: number;
  timeoutSeconds: number;
  successThreshold: number;
  failureThreshold: number;
  scheme: 'HTTP' | 'HTTPS';
  port: number;
  path: string;
  headers: Header[];
  commands: Array<{
    command: string;
  }>;
}

export interface HealthCheckConfig {
  liveness: HealthCheckConfigItem;
  readiness: HealthCheckConfigItem;
}

export type HealthCheckConfigType = 'liveness' | 'readiness';

export enum ModalAction {
  CANCEL = 'cancel',
  PREV_STEP = 'prevSetp',
  CONFRIM = 'confrim',
}

const DEFAULT_CONFIG: HealthCheckConfigItem = {
  protocol: 'HTTP',
  initialDelaySeconds: 300,
  periodSeconds: 60,
  timeoutSeconds: 30,
  successThreshold: 0,
  failureThreshold: 5,
  scheme: 'HTTP',
  port: 80,
  path: '/',
  headers: [],
  commands: [
    {
      command: '',
    },
  ],
};

@Component({
  selector: 'rc-k8s-service-health-check-config',
  templateUrl: './k8s-service-health-check-config.component.html',
  styleUrls: ['./k8s-service-health-check-config.component.scss'],
})
export class K8sServiceHealthCheckConfigComponent implements OnInit {
  @Input()
  healthCheckConfig: HealthCheckConfig;
  @Input()
  prevStep: boolean;
  @Output()
  formChanged = new EventEmitter<NgForm>();
  @Output()
  close = new EventEmitter<ModalAction>();
  @ViewChild(NgForm)
  form: NgForm;

  protocolOptions = ['HTTP', 'TCP', 'EXEC'];
  schemeOptions = ['HTTP', 'HTTPS'];

  configTypes = ['liveness', 'readiness'];

  ngOnInit() {
    this.formChanged.emit(this.form);
  }

  trackByIndex(index: number) {
    return index;
  }

  addConfigItem(type: HealthCheckConfigType) {
    this.healthCheckConfig[type] = cloneDeep(DEFAULT_CONFIG);
  }

  removeConfigItem(type: HealthCheckConfigType) {
    this.healthCheckConfig[type] = null;
  }

  addHeader(type: HealthCheckConfigType) {
    const config = this.healthCheckConfig[type];
    const headers = config.headers || [];
    config.headers = [...headers, {} as Header];
  }

  addCommand(type: HealthCheckConfigType) {
    const config = this.healthCheckConfig[type];
    if (!config.commands) {
      config.commands = [];
    }
    config.commands.push({
      command: null,
    });
  }

  removeHeader(type: HealthCheckConfigType, index: number) {
    const config = this.healthCheckConfig[type];
    config.headers = config.headers.filter((_h, i) => i !== index);
  }

  removeCommand(type: HealthCheckConfigType, index: number) {
    const config = this.healthCheckConfig[type];
    config.commands = config.commands.filter((_c, i) => i !== index);
  }
}
