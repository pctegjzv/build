import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ComponentViewModel } from 'app2/features/service/service.type';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { cloneDeep } from 'lodash';

import { ModalAction } from './k8s-service-health-check-config.component';

const INCLUDED_KINDS = ['Deployment', 'DaemonSet', 'StatefulSet'];

@Component({
  templateUrl: './k8s-service-update-health-check.component.html',
  styleUrls: ['./k8s-service-update-health-check.component.scss'],
})
export class K8sServiceUpdateHealthCheckComponent implements OnInit {
  @Output()
  close = new EventEmitter<boolean>();

  form: NgForm;
  activeIndex = 0;
  kubernetes: any;
  serviceVm: ComponentViewModel;

  constructor(
    @Inject(MODAL_DATA)
    private data: {
      activeIndex: number;
      kubernetes: any;
      serviceId: string;
    },
    private serviceService: ServiceService,
  ) {}

  async ngOnInit() {
    this.activeIndex = this.data.activeIndex;

    let { kubernetes } = this.data;

    if (!kubernetes) {
      const service = await this.serviceService.getK8sService(
        this.data.serviceId,
      );

      kubernetes = service.kubernetes;
    }

    this.kubernetes = kubernetes;
    this.serviceVm = this.serviceService.getServiceViewModel(kubernetes);
  }

  async closeModal(modalAction: ModalAction) {
    if (modalAction === ModalAction.CANCEL) {
      return this.close.emit();
    }

    if (modalAction === ModalAction.PREV_STEP) {
      return this.close.emit(true);
    }

    if (this.form.invalid) {
      return;
    }

    const kubernetes = cloneDeep(this.kubernetes);

    const service = kubernetes.find(({ kind }: any) =>
      INCLUDED_KINDS.includes(kind),
    );

    const { containers } = this.serviceService
      .getServiceKubernetesDefinition(this.serviceVm)
      .find(({ kind }) => INCLUDED_KINDS.includes(kind)).spec.template.spec;

    service.spec.template.spec.containers.forEach(
      (container: any, index: number) => {
        container.livenessProbe = containers[index].livenessProbe;
        container.readinessProbe = containers[index].readinessProbe;
      },
    );

    try {
      await this.serviceService.patchK8sService(this.data.serviceId, {
        kubernetes,
      });
    } catch (e) {
      return;
    }

    this.close.emit();
  }
}
