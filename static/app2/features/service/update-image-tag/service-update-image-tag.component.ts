import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { cloneDeep } from 'lodash';

import { MessageService } from 'alauda-ui';
import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';
import { RcImageSelection } from 'app2/features/service/service.type';
import {
  ImageRepositoryService,
  ImageRepositoryTag,
} from 'app2/shared/services/features/image-repository.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './service-update-image-tag.component.html',
})
export class ServiceUpdateImageTagComponent implements OnInit {
  @Output()
  close = new EventEmitter<{ prevStep?: boolean; finish?: boolean }>();
  @ViewChild('form')
  form: NgForm;
  private activeIndex = 0;
  private kubernetes: any;
  private containers: any;
  initialized: boolean;
  submitting: boolean;
  containerName: string;
  containerImage: string;
  containerImageTag: string;
  imageTagOptions: any[];

  constructor(
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(MODAL_DATA)
    private data: {
      activeIndex: number;
      kubernetes: any;
      serviceId: string;
    },
    private serviceService: ServiceService,
    private repositoryService: ImageRepositoryService,
    private errorsToastService: ErrorsToastService,
    private translateService: TranslateService,
    private auiMessageService: MessageService,
  ) {
    this.imageTagOptions = [];
  }

  async ngOnInit() {
    this.activeIndex = this.data.activeIndex;
    let { kubernetes } = this.data;
    if (!kubernetes) {
      const service = await this.serviceService.getK8sService(
        this.data.serviceId,
      );
      kubernetes = service.kubernetes;
    }
    this.kubernetes = kubernetes;
    await this.resolveContainerImage();
    this.initialized = true;
  }

  shouldShowPrevStep() {
    return this.containers && this.containers.length > 1;
  }

  private async resolveContainerImage() {
    this.containers = this.serviceService.getServiceViewModel(this.kubernetes)[
      'containers'
    ];
    this.containerName = this.containers[this.activeIndex].config.name;
    this.containerImage = this.containers[this.activeIndex].config.image;
    this.containerImageTag = this.containers[this.activeIndex].config.imageTag;
    const params: RcImageSelection = await this.serviceService.getRepositoryParamsFromRawImageName(
      this.containerImage,
    );
    this.imageTagOptions = await this.getImageTagOptions(params);
  }

  private async getImageTagOptions(params: RcImageSelection) {
    let tags;
    if (params.registry_name) {
      tags = await this.repositoryService
        .getRepositoryTags({
          registry_name: params.registry_name,
          project_name: params.project_name,
          repository_name: params.repository_name,
          view_type: 'security',
          page_size: 0,
        })
        .then(({ results }: { results: ImageRepositoryTag[] }) => {
          if (!results) {
            results = [];
          }
          return results.map((tag: ImageRepositoryTag) => {
            const level = tag.level || 'unscanned';
            const className = 'rb-tag-level-' + level.toLowerCase();
            const levelName = this.translateService.get(
              'image_repository_scan_status_' + level.toLowerCase(),
            );
            return {
              tag_name: tag.tag_name,
              tag_display: `${
                tag.tag_name
              }<span class="${className} margin-left-1">(${levelName})</span>`,
            };
          });
        });
    } else {
      tags = await this.repositoryService
        .getPublicRepositoryTags(params.full_image_name)
        .then((tags: string[]) => {
          return tags.map((tag: string) => {
            return {
              tag_name: tag,
              tag_display: `<span>${tag}</span>`,
            };
          });
        });
    }
    return tags;
  }

  cancel() {
    this.close.emit({});
  }

  prevStep() {
    this.close.emit({ prevStep: true });
  }

  async confirm() {
    if (this.form.invalid) {
      return;
    }
    const INCLUDED_KINDS = ['Deployment', 'DaemonSet', 'StatefulSet'];
    const kubernetes = cloneDeep(this.kubernetes);
    const service = kubernetes.find(({ kind }: any) =>
      INCLUDED_KINDS.includes(kind),
    );
    service.spec.template.spec.containers[this.activeIndex]['image'] = `${
      this.containerImage
    }:${this.containerImageTag}`;
    // console.log(service);
    this.submitting = true;
    try {
      await this.serviceService.patchK8sService(this.data.serviceId, {
        kubernetes,
      });
      this.auiMessageService.success({
        content: this.translateService.get('update_success'),
      });
      this.close.emit({ finish: true });
    } catch (err) {
      this.errorsToastService.handleGenericAjaxError({
        errors: err.errors,
        handleNonGenericCodes: true,
        fallbackMessage: this.translateService.get('update_failed'),
      });
    }
    this.submitting = false;
  }
}
