import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import * as _ from 'lodash';

import { MessageService } from 'alauda-ui';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

import { INT_PATTERN } from '../../../../app/components/common/config/common-pattern';

@Component({
  templateUrl: './service-update-replicas.component.html',
})
export class ServiceUpdateReplicasComponent implements OnInit {
  @Output()
  close = new EventEmitter<boolean>();
  @ViewChild('form')
  form: NgForm;
  numberReg = INT_PATTERN;
  currentReplicas: number;
  replicas: string;
  submitting: boolean;
  initialized: boolean;
  private kubernetes: any;
  private service: any;
  constructor(
    @Inject(MODAL_DATA) private modalData: any,
    private errorsToastService: ErrorsToastService,
    private serviceService: ServiceService,
    private translateService: TranslateService,
    private auiMessageService: MessageService,
  ) {}

  async ngOnInit() {
    await this.resolveCurrentReplicas();
    this.initialized = true;
  }

  private async resolveCurrentReplicas() {
    const res = await this.serviceService.getK8sService(this.modalData.uuid);
    this.kubernetes = res.kubernetes;
    this.service = this.kubernetes.find((item: { kind: string }) => {
      return ['Deployment', 'StatefulSet', 'DaemonSet'].includes(item.kind);
    });

    this.currentReplicas = _.get(this.service, 'spec.replicas', 1);
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.service.spec.replicas = parseInt(this.replicas, 10);
    this.submitting = true;
    try {
      await this.serviceService.patchK8sService(this.modalData.uuid, {
        kubernetes: this.kubernetes,
      });
      this.close.next(true);
      this.auiMessageService.success({
        content: this.translateService.get('update_success'),
      });
    } catch (err) {
      this.errorsToastService.handleGenericAjaxError({
        errors: err.errors,
        handleNonGenericCodes: true,
        fallbackMessage: this.translateService.get('update_failed'),
      });
    }
    this.submitting = false;
  }

  cancel() {
    this.close.next();
  }
}
