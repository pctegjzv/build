import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { cloneDeep, unset } from 'lodash';

import { MessageService } from 'alauda-ui';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './service-update-resource-size.component.html',
})
export class ServiceUpdateResourceSizeComponent implements OnInit {
  @Output()
  close = new EventEmitter<{ prevStep?: boolean; finish?: boolean }>();
  @ViewChild('resourceSize')
  resourceSize: NgForm;
  private activeIndex = 0;
  private kubernetes: any;
  private containers: any;
  initialized: boolean;
  submitting: boolean;
  containerResources: any;
  errorMapper: any;

  constructor(
    @Inject(MODAL_DATA)
    private data: {
      activeIndex: number;
      kubernetes: any;
      serviceId: string;
    },
    private serviceService: ServiceService,
    private translateService: TranslateService,
    private auiMessageService: MessageService,
    private errorsToastService: ErrorsToastService,
  ) {
    // custom error mapper
    this.errorMapper = {
      map: (key: string, error: any): string => {
        switch (key) {
          case 'container_resource_size_invalid':
            const types = error.join(',');
            return `${types} ${this.translateService.get(
              'requests_should_less_than_limits',
            )}`;
        }
      },
    };
  }

  async ngOnInit() {
    this.activeIndex = this.data.activeIndex;
    let { kubernetes } = this.data;
    if (!kubernetes) {
      const service = await this.serviceService.getK8sService(
        this.data.serviceId,
      );
      kubernetes = service.kubernetes;
    }
    this.kubernetes = kubernetes;
    await this.resolveContainerImage();
    this.initialized = true;
  }

  shouldShowPrevStep() {
    return this.containers && this.containers.length > 1;
  }

  private async resolveContainerImage() {
    this.containers = this.serviceService.getServiceViewModel(this.kubernetes)[
      'containers'
    ];
    this.containerResources = this.containers[
      this.activeIndex
    ].config.resources;
  }

  private getResourcesYaml(resources: any) {
    let resourceData: any;
    if (!resources.limits.memory) {
      resources.limits.memory = resources.requests.memory;
    }

    if (!resources.limits.cpu) {
      resources.limits.cpu = resources.requests.cpu;
    }

    resourceData = {
      requests: {
        cpu: resources.requests.cpu,
        memory: resources.requests.memory,
      },
      limits: {
        cpu: resources.limits.cpu,
        memory: resources.limits.memory,
      },
    };

    return resourceData;
  }

  cancel() {
    this.close.emit({});
  }

  prevStep() {
    this.close.emit({ prevStep: true });
  }

  async confirm() {
    if (this.resourceSize.form.invalid) {
      return;
    }
    const INCLUDED_KINDS = ['Deployment', 'DaemonSet', 'StatefulSet'];
    const kubernetes = cloneDeep(this.kubernetes);
    const service = kubernetes.find(({ kind }: any) =>
      INCLUDED_KINDS.includes(kind),
    );
    const resourcesYaml = this.getResourcesYaml(this.containerResources);
    const container = service.spec.template.spec.containers[this.activeIndex];
    if (resourcesYaml) {
      container.resources = resourcesYaml;
    } else {
      unset(container, 'resources');
    }
    this.submitting = true;
    try {
      await this.serviceService.patchK8sService(this.data.serviceId, {
        kubernetes,
      });
      this.auiMessageService.success({
        content: this.translateService.get('update_success'),
      });
      this.close.emit({ finish: true });
    } catch (err) {
      this.errorsToastService.handleGenericAjaxError({
        errors: err.errors,
        handleNonGenericCodes: true,
        fallbackMessage: this.translateService.get('update_failed'),
      });
    }
    this.submitting = false;
  }
}
