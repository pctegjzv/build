import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgModel } from '@angular/forms';
import { Router } from '@angular/router';
import { get, remove, sortBy } from 'lodash';

import { MessageService } from 'alauda-ui';
import { AlarmService } from 'app2/shared/services/features/alarm.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { MODAL_DATA, ModalRef } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

import { INT_PATTERN } from '../../../../app/components/common/config/common-pattern';

interface ScaleAlarm {
  name: string;
  uuid: string;
  action_type: 'scaling_in' | 'scaling_out';
  alarm_type: 'metric_alarm' | 'log_alarm';
  severity_level: 'OK' | 'ALARM' | 'INSUFFICIENT_DATA';
}

@Component({
  styleUrls: ['./service-update-scaling.component.scss'],
  templateUrl: './service-update-scaling.component.html',
})
export class ServiceUpdateScalingComponent implements OnInit {
  @Output()
  close = new EventEmitter<boolean>();
  numberReg = INT_PATTERN;
  initialized: boolean;

  scaleOutAlarms: ScaleAlarm[]; // 扩容告警
  scaleInAlarms: ScaleAlarm[]; // 缩容告警
  scaleInAlarmOptions: any[];
  scaleOutAlarmOptions: any[];
  scaleOutAlarmTypeOptions: {
    name: string;
    value: string;
  }[];
  scaleInAlarmTypeOptions: {
    name: string;
    value: string;
  }[];
  scaleInAlarmType: string;
  scaleOutAlarmType: string;

  maxReplicas: number;
  minReplicas: number;
  submitting: boolean;
  maxReplicasEditing: boolean;
  minReplicasEditing: boolean;
  maxReplicasSubmitting: boolean;
  minReplicasSubmitting: boolean;

  scaleOutAlarmId: string;
  scaleInAlarmId: string;

  private service_id: string;
  private service_name: string;
  private serviceLogAlarms: ScaleAlarm[]; // 服务关联的扩缩容日志告警
  private serviceMetricAlarms: ScaleAlarm[]; // 服务关联的扩缩容指标告警
  private metricAlarms: any[] = [];
  private logAlarms: any[] = [];
  private selectedScaleOutAlarm: any;
  private selectedScaleInAlarm: any;
  private kubernetes: any; // service 'kubernetes' data from service detail API

  @ViewChild('maxReplicasInput')
  maxReplicasInput: NgModel;
  @ViewChild('minReplicasInput')
  minReplicasInput: NgModel;
  @ViewChild('scaleOutAlarmSelect')
  scaleOutAlarmSelect: NgModel;
  @ViewChild('scaleInAlarmSelect')
  scaleInAlarmSelect: NgModel;
  constructor(
    @Inject(MODAL_DATA)
    private modalData: {
      uuid: string;
      name: string;
    },
    @Inject(ModalRef) private modalRef: ModalRef,
    private errorsToastService: ErrorsToastService,
    private serviceService: ServiceService,
    private alarmService: AlarmService,
    private translateService: TranslateService,
    private auiMessageService: MessageService,
    private router: Router,
  ) {
    this.scaleOutAlarms = [];
    this.scaleInAlarms = [];
    this.scaleOutAlarmTypeOptions = this.scaleInAlarmTypeOptions = [
      {
        name: this.translateService.get('metric_alarm'),
        value: 'metric_alarm',
      },
      {
        name: this.translateService.get('log_alarm'),
        value: 'log_alarm',
      },
    ];
    this.scaleInAlarmType = this.scaleOutAlarmType = 'metric_alarm';
  }

  async ngOnInit() {
    this.service_id = this.modalData.uuid;
    this.service_name = this.modalData.name;
    await Promise.all([
      this.getServiceMetricAlarms(),
      this.getServiceLogAlarms(),
      this.getAlarmOptions(),
      this.getServiceDetail(),
    ]);
    this.refreshScaleInAlarmOptions();
    this.refreshScaleOutAlarmOptions();
    this.initialized = true;
  }

  trackByIndex(index: number) {
    return index;
  }

  refreshScaleInAlarmOptions() {
    let alarms = [];
    if (this.scaleInAlarmType === 'metric_alarm') {
      alarms = this.metricAlarms.filter((item: any) => {
        return !this.serviceMetricAlarms.find((_item: ScaleAlarm) => {
          return item.uuid === _item.uuid;
        });
      });
    } else if (this.scaleInAlarmType === 'log_alarm') {
      alarms = this.logAlarms.filter((item: any) => {
        return !this.serviceLogAlarms.find((_item: ScaleAlarm) => {
          return item.uuid === _item.uuid;
        });
      });
    }
    this.scaleInAlarmSelect.control.reset();
    this.scaleInAlarmOptions = sortBy(alarms, 'name');
  }

  refreshScaleOutAlarmOptions() {
    let alarms = [];
    if (this.scaleOutAlarmType === 'metric_alarm') {
      alarms = this.metricAlarms.filter((item: any) => {
        return !this.serviceMetricAlarms.find((_item: ScaleAlarm) => {
          return item.uuid === _item.uuid;
        });
      });
    } else if (this.scaleOutAlarmType === 'log_alarm') {
      alarms = this.logAlarms.filter((item: any) => {
        return !this.serviceLogAlarms.find((_item: ScaleAlarm) => {
          return item.uuid === _item.uuid;
        });
      });
    }
    this.scaleOutAlarmSelect.control.reset('');
    this.scaleOutAlarmOptions = sortBy(alarms, 'name');
  }

  onScaleOutAlarmTypeChange() {
    if (!this.initialized) {
      return;
    }
    this.scaleOutAlarmId = null;
    this.refreshScaleOutAlarmOptions();
  }

  onScaleInAlarmTypeChange() {
    if (!this.initialized) {
      return;
    }
    this.scaleInAlarmId = null;
    this.refreshScaleInAlarmOptions();
  }

  onScaleOutAlarmSelected(alarm: any) {
    this.selectedScaleOutAlarm = alarm;
  }

  onScaleInAlarmSelected(alarm: any) {
    this.selectedScaleInAlarm = alarm;
  }

  async saveMaxReplicas() {
    // console.log(this.maxReplicas);
    this.maxReplicasInput.control.markAsDirty();
    this.maxReplicasInput.control.updateValueAndValidity();
    if (this.maxReplicasInput.control.invalid) {
      return;
    }
    this.maxReplicasSubmitting = true;
    await this.saveAutoScalingConfig({
      maxReplicas: this.maxReplicas,
    });
    this.maxReplicasEditing = false;
    this.maxReplicasSubmitting = false;
  }

  async saveMinReplicas() {
    // console.log(this.minReplicas);
    this.minReplicasInput.control.markAsDirty();
    this.minReplicasInput.control.updateValueAndValidity();
    if (this.minReplicasInput.control.invalid) {
      return;
    }
    this.minReplicasSubmitting = true;
    this.saveAutoScalingConfig({
      minReplicas: this.minReplicas,
    });
    this.minReplicasEditing = false;
    this.minReplicasSubmitting = false;
  }

  async addScaleOutAlarm() {
    // console.log(this.selectedScaleOutAlarm);
    if (!this.scaleOutAlarmId) {
      return;
    }
    await this.addAlarmActions({
      alarm: this.selectedScaleOutAlarm,
      type: 'scaling_out',
      alarm_type: this.scaleOutAlarmType,
    });
    if (this.scaleOutAlarmType === 'log_alarm') {
      await this.getServiceLogAlarms();
    } else {
      await this.getServiceMetricAlarms();
    }
    this.refreshScaleOutAlarmOptions();
  }

  async addScaleInAlarm() {
    // console.log(this.selectedScaleInAlarm);
    if (!this.scaleInAlarmId) {
      return;
    }
    await this.addAlarmActions({
      alarm: this.selectedScaleInAlarm,
      type: 'scaling_in',
      alarm_type: this.scaleInAlarmType,
    });
    if (this.scaleInAlarmType === 'log_alarm') {
      await this.getServiceLogAlarms();
    } else {
      await this.getServiceMetricAlarms();
    }
    this.refreshScaleInAlarmOptions();
  }

  async removeAlarm(
    alarm: ScaleAlarm,
    action_type: 'scaling_in' | 'scaling_out',
  ) {
    console.log('remove actions', alarm);
    await this.removeAlarmActions({
      alarm,
      action_type,
    });
    if (alarm.alarm_type === 'log_alarm') {
      await this.getServiceLogAlarms();
    } else {
      await this.getServiceMetricAlarms();
    }
    if (action_type === 'scaling_in') {
      this.refreshScaleInAlarmOptions();
    } else {
      this.refreshScaleOutAlarmOptions();
    }
  }

  createAlarm(
    action_type: 'scaling_in' | 'scaling_out',
    alarm_type: 'log_alarm' | 'metric_alarm',
  ) {
    const service = JSON.stringify({
      action_type,
      uuid: this.service_id,
      display_name: this.service_name,
    });
    let url =
      alarm_type === 'metric_alarm'
        ? 'alarm/alarm_create'
        : 'alarm/log_alarm_create';
    url += `?service=${service}`;
    this.router.navigateByUrl(url);
    this.modalRef.close();
  }

  closeModal() {
    this.modalRef.close();
  }

  private async getServiceDetail() {
    const res = await this.serviceService.getK8sService(this.modalData.uuid);
    this.kubernetes = res.kubernetes;
    this.initAutoScalingConfig();
  }

  private initAutoScalingConfig() {
    const scalingService = this.kubernetes.find((item: any) => {
      return item.kind === 'HorizontalPodAutoscaler';
    });
    if (!scalingService) {
      return;
    }
    this.minReplicas = get(scalingService, 'spec.minReplicas', null);
    this.maxReplicas = get(scalingService, 'spec.maxReplicas', null);
  }

  private async saveAutoScalingConfig(
    params: { maxReplicas?: number; minReplicas?: number } = {
      maxReplicas: null,
      minReplicas: null,
    },
  ) {
    // console.log(this.kubernetes);
    let scalingService = this.kubernetes.find((item: any) => {
      return item.kind === 'HorizontalPodAutoscaler';
    });
    if (!scalingService) {
      const kindOptions: string[] = ['Deployment', 'StatefulSet'];
      const service = this.kubernetes.find((item: any) => {
        return kindOptions.includes(item.kind);
      });
      scalingService = this.serviceService.getScalingServiceDefinition({
        maxReplicas: +params.maxReplicas,
        minReplicas: +params.minReplicas,
        kind: service.kind,
        name: service.metadata.name,
        namespace: service.metadata.namespace,
      });
      this.kubernetes.push(scalingService);
    } else {
      try {
        if (params.maxReplicas) {
          scalingService.spec.maxReplicas = +params.maxReplicas;
        }
        if (params.minReplicas) {
          scalingService.spec.minReplicas = +params.minReplicas;
        }
      } catch (err) {
        return;
      }
    }
    try {
      await this.serviceService.patchK8sService(this.service_id, {
        kubernetes: this.kubernetes,
      });
      this.auiMessageService.success({
        content: this.translateService.get('service_scaling_config_saved'),
      });
    } catch (err) {
      this.errorsToastService.error(err);
    }
  }

  private async addAlarmActions({
    alarm,
    type,
    alarm_type,
  }: {
    alarm: any;
    type: string;
    alarm_type: string;
  }) {
    const services: any[] = get(alarm, 'alarm_actions.services', []);
    const item = {
      uuid: this.service_id,
      type,
    };
    if (!services.length) {
      alarm.alarm_actions.services = [item];
    } else {
      services.push(item);
    }
    try {
      this.submitting = true;
      await this.saveAlarm(alarm, alarm_type);
      this.auiMessageService.success({
        content: this.translateService.get('service_add_alarm_success'),
      });
    } catch (err) {
      this.errorsToastService.error(err);
    }
    this.submitting = false;
  }

  private async removeAlarmActions({
    alarm,
    action_type,
  }: {
    alarm: ScaleAlarm;
    action_type: 'scaling_out' | 'scaling_in';
  }) {
    const alarm_type = alarm.alarm_type;
    let actions_key;
    switch (alarm.severity_level) {
      case 'ALARM':
        actions_key = 'alarm_actions';
        break;
      case 'INSUFFICIENT_DATA':
        actions_key = 'insufficient_actions';
        break;
      default:
        actions_key = 'insufficient_actions';
        break;
    }
    if (alarm_type === 'log_alarm') {
      alarm = this.logAlarms.find((item: any) => {
        return item.uuid === alarm.uuid;
      });
    } else if (alarm_type === 'metric_alarm') {
      alarm = this.metricAlarms.find((item: any) => {
        return item.uuid === alarm.uuid;
      });
    }
    if (!alarm) {
      return;
    }
    const services = get(alarm, `${actions_key}.services`, []);
    remove(services, (item: any) => {
      return item.uuid === this.service_id && item.type === action_type;
    });
    try {
      this.submitting = true;
      await this.saveAlarm(alarm, alarm_type);
      this.auiMessageService.success({
        content: this.translateService.get('service_remove_alarm_success'),
      });
    } catch (err) {
      this.errorsToastService.error(err);
    }
    this.submitting = false;
  }

  private async saveAlarm(alarm: any, alarm_type: string) {
    if (alarm_type === 'log_alarm') {
      await this.alarmService.updateLogAlarm(alarm.uuid, alarm);
    } else if (alarm_type === 'metric_alarm') {
      await this.alarmService.updateAlarm(alarm.uuid, alarm);
    }
  }

  private async getServiceMetricAlarms() {
    const res = await this.alarmService.getAlarmsByService(this.service_id);
    res.forEach((item: ScaleAlarm) => {
      item.alarm_type = 'metric_alarm';
    });
    this.serviceMetricAlarms = res;
    let scaleInAlarms = res.filter((item: ScaleAlarm) => {
      return item.action_type === 'scaling_in';
    });
    let scaleOutAlarms = res.filter((item: ScaleAlarm) => {
      return item.action_type === 'scaling_out';
    });
    scaleInAlarms = this.scaleInAlarms
      .filter((item: ScaleAlarm) => {
        return item.alarm_type !== 'metric_alarm';
      })
      .concat(scaleInAlarms);
    this.scaleInAlarms = sortBy(scaleInAlarms, 'name');
    scaleOutAlarms = this.scaleOutAlarms
      .filter((item: ScaleAlarm) => {
        return item.alarm_type !== 'metric_alarm';
      })
      .concat(scaleOutAlarms);
    this.scaleOutAlarms = sortBy(scaleOutAlarms, 'name');
  }

  private async getServiceLogAlarms() {
    const res = await this.alarmService.getLogAlarmsByService(this.service_id);
    res.forEach((item: ScaleAlarm) => {
      item.alarm_type = 'log_alarm';
    });
    this.serviceLogAlarms = res;
    let scaleInAlarms = res.filter((item: ScaleAlarm) => {
      return item.action_type === 'scaling_in';
    });
    let scaleOutAlarms = res.filter((item: ScaleAlarm) => {
      return item.action_type === 'scaling_out';
    });
    scaleInAlarms = this.scaleInAlarms
      .filter((item: ScaleAlarm) => {
        return item.alarm_type !== 'log_alarm';
      })
      .concat(scaleInAlarms);
    this.scaleInAlarms = sortBy(scaleInAlarms, 'name');
    scaleOutAlarms = this.scaleOutAlarms
      .filter((item: ScaleAlarm) => {
        return item.alarm_type !== 'log_alarm';
      })
      .concat(scaleOutAlarms);
    this.scaleOutAlarms = sortBy(scaleOutAlarms, 'name');
  }

  private async getAlarmOptions() {
    const [metricAlarms, logAlarms] = await Promise.all([
      this.alarmService.getAlarms(),
      this.alarmService
        .getLogAlarms({
          all: true,
        })
        .then(({ result }) => result)
        .catch(() => []),
    ]);
    this.metricAlarms = metricAlarms;
    this.logAlarms = logAlarms;
  }
}
