import {
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { K8sComponent } from 'app2/features/service/service.type';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import * as jsyaml from 'js-yaml';

@Component({
  selector: 'rc-service-update',
  templateUrl: './k8s-service-update.component.html',
  styleUrls: ['./k8s-service-update.component.scss'],
})
export class K8sServiceUpdateComponent implements OnInit, OnDestroy, OnChanges {
  @Input()
  serviceData: K8sComponent;
  @Input()
  serviceYaml: string;
  servicePayload: any;
  submitting = false;
  initialized = false;
  loadingYaml = false;

  @ViewChild('serviceUpdateForm')
  form: NgForm;

  constructor(
    private serviceService: ServiceService,
    private modalService: ModalService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private router: Router,
    private errorsToastService: ErrorsToastService,
  ) {
    this.servicePayload = {
      kubernetes: [],
    };
  }

  ngOnInit() {}

  ngOnDestroy() {}

  ngOnChanges() {
    if (this.serviceData && this.serviceYaml) {
      this.initialized = true;
      this.loadingYaml = false;
    }
  }

  async updateService() {
    this.submitting = true;
    try {
      const response: any = await this.serviceService.patchK8sService(
        this.serviceData.resource.uuid,
        this.servicePayload,
      );
      this.auiNotificationService.success(
        this.translateService.get('update_success'),
      );
      return this.router.navigateByUrl(
        `k8s_service/detail/${response.resource.uuid}`,
      );
    } catch (err) {
      this.errorsToastService.handleGenericAjaxError({
        errors: err.errors,
        handleNonGenericCodes: true,
        fallbackMessage: this.translateService.get('create_failed'),
      });
    }
    this.submitting = false;
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.valid) {
      try {
        this.servicePayload.kubernetes = jsyaml.safeLoadAll(this.serviceYaml);
      } catch (error) {
        // yaml parse error
        this.auiNotificationService.error(error);
        return;
      }
      try {
        await this.modalService.confirm({
          title: this.translateService.get('update'),
          content: this.translateService.get(
            'app_service_update_service_confirm',
            {
              service_name: this.serviceData.resource.name,
            },
          ),
        });
        this.updateService();
      } catch (rejection) {}
    }
  }

  cancel() {
    return this.router.navigateByUrl('k8s_service');
  }
}
