import { Directive, Input } from '@angular/core';
import {
  AbstractControl,
  AsyncValidator,
  NG_ASYNC_VALIDATORS,
  ValidationErrors,
} from '@angular/forms';
import { Observable, timer } from 'rxjs';
import { first, switchMap } from 'rxjs/operators';

import { ServiceService } from 'app2/shared/services/features/service.service';

@Directive({
  selector:
    '[rcServiceNameValidator][formControlName], [rcServiceNameValidator][ngModel]',
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: ServiceNameValidatorDirective,
      multi: true,
    },
  ],
})
export class ServiceNameValidatorDirective implements AsyncValidator {
  @Input()
  rcNamespaceUuid: string;
  @Input()
  rcNamespace: string;
  @Input()
  rcCluster: string;
  constructor(private serviceService: ServiceService) {}
  validate(control: AbstractControl): Observable<ValidationErrors | null> {
    return timer(500).pipe(
      switchMap(() => {
        if (this.rcNamespaceUuid) {
          return this._validate(control.value, this.rcNamespaceUuid);
        }
        return this._validate(
          control.value,
          this.rcNamespaceUuid,
          this.rcNamespace,
          this.rcCluster,
        );
      }),
      first(),
    );
  }

  private _validate(
    serviceName: string,
    namespaceUuid: string,
    namespace?: string,
    cluster?: string,
  ): Observable<ValidationErrors | null> {
    return new Observable((observer: any) => {
      if (!((namespaceUuid || namespace) && serviceName)) {
        observer.next(null);
      } else {
        this.serviceService
          .checkK8sServiceName(cluster, namespaceUuid, namespace, serviceName)
          .then(() => {
            observer.next(null);
          })
          .catch(() => {
            observer.next({
              service_name_existed: true,
            });
          });
      }
    });
  }
}
