import {
  Component,
  EventEmitter,
  Inject,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';

import { MessageService } from 'alauda-ui';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './service-version-select.component.html',
  styleUrls: ['./service-version-select.component.scss'],
})
export class ServiceVersionSelectComponent implements OnInit, OnDestroy {
  @Output()
  finish = new EventEmitter<boolean>();

  initialized: boolean;
  submitting: boolean;
  versions: {
    revision: string;
    created_at: string;
    changeCause: string;
  }[];

  constructor(
    private serviceService: ServiceService,
    private errorsToastService: ErrorsToastService,
    private translateService: TranslateService,
    private auiMessageService: MessageService,
    @Inject(MODAL_DATA)
    private data: {
      uuid: string;
    },
    private modalService: ModalService,
  ) {
    this.versions = [];
  }

  async ngOnInit() {
    this.versions = await this.serviceService.getServiceRevisions(
      this.data.uuid,
    );
    this.initialized = true;
  }

  ngOnDestroy() {}

  async selectVersion(version: string) {
    try {
      await this.modalService.confirm({
        title: this.translateService.get('rollback'),
        content: this.translateService.get('service_revision_confirm'),
      });
      await this.serviceService.rollbackK8sService(this.data.uuid, version);
      this.auiMessageService.success({
        content: this.translateService.get('action_success'),
      });
      this.finish.next(true);
    } catch (err) {
      this.errorsToastService.error(err);
    }
  }

  cancel() {
    this.finish.next();
  }
}
