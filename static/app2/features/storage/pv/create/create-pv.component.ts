import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotificationService } from 'alauda-ui';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { ACCESS_MODES } from 'app2/features/storage/storage.constant';
import {
  ACCESS_MODE_LIMIT,
  PV_RECYCLE_STRATEGIES,
  RECYCLE_STRATEGY_LIMIT,
} from 'app2/features/storage/storage.constant';
import {
  KubernetesPv,
  StorageService,
} from 'app2/shared/services/features/storage.service';
import { TranslateService } from 'app2/translate/translate.service';
import * as _ from 'lodash';

@Component({
  selector: 'rc-create-pvc',
  templateUrl: './create-pv.component.html',
  styleUrls: ['../../pvc/create/create-pvc.component.scss'],
})
export class CreatePvComponent implements OnInit {
  @Output()
  finish = new EventEmitter<any>();
  form: FormGroup;
  submitting = false;
  region: any;
  pv: any;
  accessModes: any = ACCESS_MODES;
  recycleStrategies: any = PV_RECYCLE_STRATEGIES;
  volumeOptions: any[];
  accessModeOptions: any[];
  recycleStrategyOptions: any[];
  errorMappers: any = {};
  selectedVolume: any;

  constructor(
    @Inject(ENVIRONMENTS) private env: Environments,
    private translate: TranslateService,
    private fb: FormBuilder,
    private storageService: StorageService,
    private auiNotificationService: NotificationService,
  ) {
    this.form = this.fb.group({
      namespace: [],
      volume: [],
      name: ['', Validators.required],
      accessMode: [],
      recycleStrategy: [],
    });
  }

  async ngOnInit() {
    // custom ErrorMapper
    this.errorMappers = {
      pvName: {
        pattern: this.translate.get('storage_name_hint'),
      },
    };
    this.initRadioGroup();
  }

  volumeChanged(volume: any) {
    this.selectedVolume = volume;
    if (volume.driver_name) {
      // omit limit access modes
      this.accessModes = _.omit(
        ACCESS_MODES,
        ACCESS_MODE_LIMIT[volume.driver_name],
      );

      // omit limit recycle strategies
      this.recycleStrategies = _.omit(
        PV_RECYCLE_STRATEGIES,
        RECYCLE_STRATEGY_LIMIT[volume.driver_name],
      );
    }
    this.initRadioGroup();
  }

  initRadioGroup() {
    // access mode
    this.accessModeOptions = this.generateRadioGroupOptions(this.accessModes);

    // recycle strategy
    this.recycleStrategyOptions = this.generateRadioGroupOptions(
      this.recycleStrategies,
    );

    let accessMode = this.accessModeOptions[0].value;
    let recycleStrategy = this.recycleStrategyOptions[0].value;
    if (this.updating) {
      accessMode = this.pv.kubernetes
        ? this.pv.kubernetes.spec.accessModes[0]
        : this.pv.resource.accessModes[0];
      recycleStrategy = this.pv.kubernetes
        ? this.pv.kubernetes.spec.persistentVolumeReclaimPolicy
        : this.pv.resource.persistentVolumeReclaimPolicy;
    }
    setTimeout(() => {
      this.accessMode.setValue(accessMode);
      this.recycleStrategy.setValue(recycleStrategy);
    }, 0);
  }

  generateRadioGroupOptions(
    sourceData: any,
  ): { name: string; value: string }[] {
    return _.reduce(
      sourceData,
      (res: any[], value: string, key: string) => {
        res.push({
          value: key,
          name: this.translate.get(value),
        });
        return res;
      },
      [],
    );
  }

  // Invoked by parent component
  initRegion(region: any) {
    this.region = region;
  }

  initVolumes(volumes: any[]) {
    if (!this.updating) {
      volumes = volumes.filter(volume => {
        return volume.state !== 'in-use';
      });
    }
    this.volumeOptions = _.map(volumes, volume => {
      return {
        name: volume.name,
        uuid: volume.id,
        driver_name: volume.driver_name,
        size: volume.size,
      };
    });
    if (!this.volumeOptions.length) {
      this.accessMode.disable();
      this.recycleStrategy.disable();
    }
    const uuid = this.updating
      ? this.pv.kubernetes.metadata.annotations[
          `pv.${this.env.label_base_domain}/volume_uuid`
        ]
      : this.volumeOptions.length
        ? this.volumeOptions[0].uuid
        : '';
    this.volume.setValue(uuid);
  }

  // Invoked when updating
  initValue(pv: any) {
    this.pv = pv;
    // name
    const nameCtrl = this.form.get('name');
    nameCtrl.setValue(this.pv.kubernetes.metadata.name);
    nameCtrl.disable();

    // volume
    this.volume.disable();
  }

  get name() {
    return this.form.get('name');
  }

  get volume() {
    return this.form.get('volume');
  }

  get accessMode() {
    return this.form.get('accessMode');
  }

  get recycleStrategy() {
    return this.form.get('recycleStrategy');
  }

  get updating() {
    return !!this.pv;
  }

  async submit() {
    if (this.form.invalid || this.submitting) {
      return;
    }
    this.submitting = true;
    const values = this.form.value;
    // prepare data
    const kubernetes: KubernetesPv = {
      kind: 'PersistentVolume',
      spec: {
        accessModes: [values.accessMode],
        capacity: {
          storage: this.selectedVolume.size + 'G',
        },
        persistentVolumeReclaimPolicy: values.recycleStrategy,
      },
      apiVersion: 'v1',
      metadata: {
        name: this.name.value,
        annotations: {
          [`pv.${this.env.label_base_domain}/volume_uuid`]: this.selectedVolume
            .uuid,
          [`pv.${this.env.label_base_domain}/volume_driver`]: this
            .selectedVolume.driver_name,
          [`cluster.${this.env.label_base_domain}/name`]: this.region.name,
        },
      },
    };
    try {
      if (this.updating) {
        await this.storageService.updatePv(
          this.name.value,
          this.region.id,
          kubernetes,
        );
      } else {
        await this.storageService.createPv(this.region.id, kubernetes);
      }
      this.complete(true);
    } catch ({ errors }) {
      if (errors) {
        this.auiNotificationService.error(errors[0].message);
      }
    }
    this.submitting = false;
  }

  private complete(res?: any) {
    this.finish.next(res);
    this.finish.complete();
  }

  cancel() {
    this.complete();
  }
}
