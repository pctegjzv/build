import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { CreateSnapshotComponent } from 'app2/features/storage/snapshot/create/create-snapshot.component';
import {
  PV_RECYCLE_STRATEGIES,
  PV_STATUS_MAP,
} from 'app2/features/storage/storage.constant';
import { ACCESS_MODES } from 'app2/features/storage/storage.constant';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import {
  Pv,
  StorageService,
  Volume,
} from 'app2/shared/services/features/storage.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ModalRef } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';
import { cloneDeep } from 'lodash';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { CreatePvComponent } from '../create/create-pv.component';

@Component({
  templateUrl: './pv-detail.component.html',
})
export class PvDetailComponent implements OnInit, OnDestroy {
  loading = false;
  loadError = false;
  paramsSubscription: Subscription;
  name: string;
  pv: Pv;
  region: Region;
  regionSubscription: Subscription;
  statusMap = PV_STATUS_MAP;
  accessModes = ACCESS_MODES;
  recycleStrategies = PV_RECYCLE_STRATEGIES;
  hasCreateSnapshotPermission = false;
  createSnapshotModalRef: ModalRef;
  snapshotVolume: Volume | null;
  isDeleting = false;
  canDelete = false;
  canUpdate = false;
  collapsed = true;

  get nameKey() {
    return `pv.${this.env.label_base_domain}/volume_name`;
  }

  get uuidKey() {
    return `pv.${this.env.label_base_domain}/volume_uuid`;
  }

  constructor(
    @Inject(ENVIRONMENTS) private env: Environments,
    private route: ActivatedRoute,
    private modal: ModalService,
    private translate: TranslateService,
    private storageService: StorageService,
    private auiNotificationService: NotificationService,
    private roleUtil: RoleUtilitiesService,
    private regionService: RegionService,
    private router: Router,
  ) {}

  async ngOnInit() {
    this.region = await this.regionService.getCurrentRegion();
    this.regionSubscription = this.regionService.region$.subscribe(region => {
      this.region = region;
    });
    this.paramsSubscription = this.route.params
      .pipe(first())
      .subscribe(async params => {
        this.name = params['name'];
        await this.loadPv();
        await this.initVolume();
        this.setDefaultRegion();
      });
    // this.hasCreateSnapshotPermission = await this.roleUtil.resourceTypeSupportPermissions(
    //   'snapshot',
    // );
    this.hasCreateSnapshotPermission = false;
  }

  ngOnDestroy() {
    this.regionSubscription.unsubscribe();
    this.paramsSubscription.unsubscribe();
  }

  private setDefaultRegion() {
    const name = this.pv.kubernetes.metadata.annotations[
      `cluster.${this.env.label_base_domain}/name`
    ];
    if (name) {
      this.regionService.setRegionByName(name);
    }
  }

  async initVolume() {
    if (!this.pv.kubernetes.metadata.annotations[this.uuidKey]) {
      this.snapshotVolume = null;
      return;
    }
    try {
      this.snapshotVolume = await this.storageService.getVolumeDetail(
        this.pv.kubernetes.metadata.annotations[this.uuidKey],
      );
    } catch ({ errors }) {
      this.snapshotVolume = null;
    }
  }

  async loadPv() {
    if (this.loading) {
      return;
    }
    this.loading = true;
    try {
      this.pv = await this.storageService.getPvDetail(
        this.region.id,
        this.name,
      );
      this.canDelete = this.hasPermission(this.pv, 'delete');
      this.canUpdate = this.hasPermission(this.pv, 'update');
    } catch ({ status, errors }) {
      this.loadError = true;
      if (status === 403) {
        this.auiNotificationService.warning(
          this.translate.get('permission_denied'),
        );
        return this.router.navigate(['/storage/pv']);
      } else if (status === 404) {
        this.auiNotificationService.warning(this.translate.get('pv_not_exist'));
        return this.router.navigate(['/storage/pv']);
      } else {
        this.auiNotificationService.error(
          errors ? errors[0].message : 'unknown_issue',
        );
      }
    }
    this.loading = false;
  }

  async createSnapshot() {
    try {
      const modalRef = await this.modal.open(CreateSnapshotComponent, {
        title: this.translate.get('storage_create_snapshot'),
      });
      modalRef.componentInstance.initVolume(this.snapshotVolume);
      modalRef.componentInstance.finish.pipe(first()).subscribe(result => {
        modalRef.close();
        if (result) {
          this.auiNotificationService.success(
            this.translate.get('storage_snapshot_create_success'),
          );
        }
      });
    } catch (error) {}
  }

  async delete() {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete_pv_title'),
        content: this.translate.get('delete_pv_content', {
          name: this.pv.kubernetes.metadata.name,
        }),
      });
      this.isDeleting = true;
      await this.storageService.deletePv(
        this.region.id,
        this.pv.kubernetes.metadata.name,
      );
      this.auiNotificationService.success(
        this.translate.get('pv_delete_success'),
      );
      this.router.navigate(['/storage/pv']);
    } catch (e) {}
    this.isDeleting = false;
  }

  hasPermission(item: Pv, action: string) {
    return this.roleUtil.resourceHasPermission(
      item,
      'persistentvolume',
      action,
    );
  }

  async update() {
    try {
      const modalRef = await this.modal.open(CreatePvComponent, {
        title: this.translate.get('update_pv'),
      });
      // load volume
      modalRef.componentInstance.initRegion(cloneDeep(this.region));
      modalRef.componentInstance.initValue(this.pv);
      modalRef.componentInstance.initVolumes([this.snapshotVolume]);
      modalRef.componentInstance.finish
        .pipe(first())
        .subscribe((result: boolean) => {
          modalRef.close();
          if (result) {
            modalRef.close();
            this.auiNotificationService.success(
              this.translate.get('pv_update_success'),
            );
            this.loadPv();
          }
        });
    } catch (error) {
      //
    }
  }

  collapse() {
    this.collapsed = !this.collapsed;
  }

  get status() {
    return this.pv.kubernetes.status.phase;
  }
}
