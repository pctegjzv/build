import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { PV_STATUS_MAP } from 'app2/features/storage/storage.constant';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import {
  Pv,
  StorageService,
  Volume,
} from 'app2/shared/services/features/storage.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { cloneDeep } from 'lodash';
import { Subscription } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import { CreatePvComponent } from '../create/create-pv.component';

@Component({
  templateUrl: './pv-list.component.html',
  styleUrls: [
    '../../storage.common.scss',
    '../../pvc/list/pvc-list.component.scss',
  ],
})
export class PvListComponent implements OnInit, OnDestroy {
  loading = false;
  initialized = false;
  destroyed = false;
  loadError = false;
  searching = false;
  pvs: Pv[] = [];
  pvCache: Pv[] = [];
  statusMap = PV_STATUS_MAP;
  regionSubscription: Subscription;
  region: Region;
  regionVolumes: Volume[];
  pagination = {
    currentPage: 1,
    count: 0,
    size: 20,
  };
  keyword = '';
  pollingTimer: any = null;
  hasCreatePvPermission = false;
  emptyMessage = 'no_data';
  notificationVisible = true;

  get currentPageIndex(): number {
    return this.pagination.currentPage - 1;
  }

  get nameKey() {
    return `pv.${this.env.label_base_domain}/volume_name`;
  }

  get uuidKey() {
    return `pv.${this.env.label_base_domain}/volume_uuid`;
  }

  constructor(
    @Inject(ENVIRONMENTS) private env: Environments,
    private modal: ModalService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private regionService: RegionService,
    private storageService: StorageService,
    private roleUtil: RoleUtilitiesService,
    private router: Router,
    private errorToast: ErrorsToastService,
  ) {}

  showNotification() {
    this.notificationVisible = true;
  }

  closeNotification() {
    this.notificationVisible = false;
  }

  // async searchChanged(keyword: string) {
  //   this.keyword = keyword;
  //   this.emptyMessage = keyword.length ? 'no_results' : 'no_data';
  //   this.searching = true;
  //   await this.load();
  //   this.searching = false;
  // }

  filter(keyword: string) {
    this.keyword = keyword;
    this.pvs = keyword.length
      ? this.pvCache.filter((item: Pv) => {
          return item.kubernetes.metadata.name.includes(keyword);
        })
      : this.pvCache;
  }

  async ngOnInit() {
    // init region
    this.region = await this.regionService.getCurrentRegion();
    this.regionSubscription = this.regionService.region$
      .pipe(filter(region => !!region))
      .subscribe((region: Region) => {
        this.region = region;
        this.onRegionChanged();
      });
    this.hasCreatePvPermission = await this.roleUtil.resourceTypeSupportPermissions(
      'persistentvolume',
      {
        cluster_name: this.region.name,
      },
    );
    await this.load();
    this.initialized = true;
  }

  ngOnDestroy() {
    clearTimeout(this.pollingTimer);
    this.regionSubscription.unsubscribe();
    this.destroyed = true;
  }

  onRegionChanged() {
    this.load();
    this.loadVolumes();
  }

  async loadVolumes() {
    try {
      const { volumes } = await this.storageService.getVolumes({
        region_id: this.region.id,
        pageno: 1,
        size: 1000,
        list_all: true,
      });
      this.regionVolumes = volumes;
    } catch ({ errors }) {}
  }

  async load() {
    if (this.destroyed) {
      return;
    }
    this.loading = true;
    try {
      const result = await this.storageService.getPvs({
        cluster_id: this.region.id,
        page: this.pagination.currentPage,
        page_size: this.pagination.size,
        name: this.keyword,
      });
      this.pvs = (result.results || []).filter((item: Pv) => {
        const annotations = item.kubernetes.metadata.annotations;
        if (annotations) {
          return annotations.hasOwnProperty(this.nameKey);
        }
      });
      if (!this.pvs.length && this.pagination.currentPage > 1) {
        --this.pagination.currentPage;
        this.load();
        return;
      }
      this.pagination.count = result.count;
      this.pvHandler();
      this.pvCache = cloneDeep(this.pvs);
      this.filter(this.keyword);
    } catch (error) {}
    this.loading = false;
    this.initialized = true;
  }

  pvHandler() {
    this.pvs.forEach((pv: any) => {
      pv.isDeleting = false;
      pv.canDelete = this.hasPermission(pv, 'delete');
      pv.canUpdate = this.hasPermission(pv, 'update');
    });
  }

  async createPv(updating: boolean = false, pv: any = null) {
    try {
      const modalRef = await this.modal.open(CreatePvComponent, {
        title: this.translate.get(updating ? 'update_pv' : 'create_pv'),
      });
      modalRef.componentInstance.initRegion(cloneDeep(this.region));
      if (updating) {
        modalRef.componentInstance.initValue(pv);
      }
      modalRef.componentInstance.initVolumes(this.regionVolumes);
      modalRef.componentInstance.finish
        .pipe(first())
        .subscribe((result: boolean) => {
          modalRef.close();
          if (result) {
            this.auiNotificationService.success(
              this.translate.get(
                updating ? 'pv_update_success' : 'pv_create_success',
              ),
            );
            this.refetch();
          }
        });
    } catch (error) {}
  }

  refetch() {
    this.loadVolumes();
    this.load();
  }

  async delete(pv: Pv) {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete_pv_title'),
        content: this.translate.get('delete_pv_content', {
          name: pv.kubernetes.metadata.name,
        }),
      });
    } catch (e) {
      return false;
    }
    // pv.isDeleting = true;
    try {
      await this.storageService.deletePv(
        this.region.id,
        pv.kubernetes.metadata.name,
      );
      this.auiNotificationService.success(
        this.translate.get('pv_delete_success'),
      );
      this.refetch();
    } catch (error) {
      this.errorToast.error(error);
    }
    // pv.isDeleting = false;
  }

  viewDetail(name: string) {
    this.router.navigate(['/storage/pv/detail', name]);
  }

  async update(item: any) {
    await this.createPv(true, item);
  }

  pageChanged(pageEvent: { pageIndex: number; pageSize: number }) {
    this.pagination.currentPage = ++pageEvent.pageIndex;
    this.pagination.size = pageEvent.pageSize;
    this.load();
  }

  hasPermission(item: any, action: string) {
    return this.roleUtil.resourceHasPermission(
      item,
      'persistentvolume',
      action,
    );
  }

  polling() {
    clearTimeout(this.pollingTimer);
    if (this.destroyed || this.pagination.currentPage !== 1) {
      return;
    }
    this.pollingTimer = setTimeout(() => {
      this.load();
    }, 30 * 1000);
  }
}
