import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VendorCustomerService } from 'app2/core/customization/vendor-customer.service';
import { CustomModuleName } from 'app2/core/customization/vendor-customer.types';
import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';
import { ACCESS_MODES } from 'app2/features/storage/storage.constant';
import {
  NamespaceOption,
  NamespaceService,
} from 'app2/shared/services/features/namespace.service';
import {
  Pvc,
  PvcPayload,
  StorageService,
} from 'app2/shared/services/features/storage.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { reduce } from 'lodash';

import { INT_PATTERN } from '../../../../../app/components/common/config/common-pattern';

@Component({
  selector: 'rc-create-pvc',
  templateUrl: './create-pvc.component.html',
  styleUrls: ['./create-pvc.component.scss'],
})
export class CreatePvcComponent implements OnInit {
  @Output()
  finish = new EventEmitter<any>();
  numberReg = INT_PATTERN;
  form: FormGroup;
  submitting = false;
  region: any;
  pvc: Pvc;
  storageClassEnabled = false;
  appUserPvc = true;

  namespaceOptions: string[];
  accessModeOptions: any[];
  pvLabelOptions: string[];
  storageClassOptions: any[] = [];
  errorMappers: any = {};
  maxPvcSize: any;
  mirrorClusters: {
    name: string;
    display_name: string;
  }[] = [];

  defaultSCOptions = [
    {
      name: 'storageclass_nonuse',
      value: 'nonuse',
    },
    {
      name: 'storageclass_use_default',
      value: 'default',
    },
  ];

  constructor(
    private namespaceService: NamespaceService,
    private translate: TranslateService,
    private storageService: StorageService,
    private fb: FormBuilder,
    private vendorCustomerService: VendorCustomerService,
    private errorToast: ErrorsToastService,
    @Inject(WEBLABS) public weblabs: Weblabs,
  ) {
    this.form = this.fb.group({
      namespace: [],
      clusters: [],
      name: ['', Validators.required],
      accessMode: [],
      storageClass: '',
      size: ['', Validators.required],
    });
  }

  async ngOnInit() {
    this.appUserPvc = this.vendorCustomerService.support(
      CustomModuleName.APP_USER_PVC,
    );
    this.storageClassEnabled = !this.appUserPvc;
    // custom ErrorMapper
    this.errorMappers = {
      pvcName: {
        pattern: this.translate.get('storage_name_hint'),
      },
    };
    this.storageClassOptions = this.storageClassOptions.concat(
      this.defaultSCOptions.map(option => {
        return {
          name: this.translate.get(option.name),
          value: option.value,
        };
      }),
    );

    if (this.storageClassEnabled) {
      await this.initStorageClass();
      // storageclass
      this.stroageClass.setValue(this.storageClassOptions[0].value);
    }

    this.initAccessMode();
    this.getNamespaceQuota();
    // init namespace
    if (!this.updating && !this.appUserPvc) {
      await this.initNamespace();
    }
  }

  async initStorageClass() {
    try {
      const ret = await this.storageService.getStorageClasses({
        cluster_id: this.region.id,
        page: 1,
        page_size: 1000,
      });
      this.storageClassOptions = this.storageClassOptions.concat(
        ret.results.map((item: any) => item.kubernetes.metadata.name),
      );
    } catch (error) {
      return;
    }
  }

  initAccessMode() {
    this.accessModeOptions = reduce(
      ACCESS_MODES,
      (res, value: string, key) => {
        res.push({
          value: key,
          name: this.translate.get(value),
        });
        return res;
      },
      [],
    );
    let accessMode = this.accessModeOptions[0].value;
    if (this.updating) {
      accessMode = this.pvc.kubernetes.spec.accessModes[0];
    }
    setTimeout(() => {
      this.accessMode.setValue(accessMode);
    }, 0);
  }

  async getNamespaceQuota() {
    try {
      const namespaceQuota = await this.namespaceService.getNamespaceQuota(
        this.region.id,
        this.region.namespace,
      );
      this.maxPvcSize =
        namespaceQuota.kubernetes.status.hard['requests.storage'];
    } catch (err) {
      return;
    }
  }

  async initNamespace() {
    const namespaces = await this.namespaceService.getNamespaceOptions(
      this.region.id,
    );
    this.namespaceOptions = namespaces.map((namespace: NamespaceOption) => {
      return namespace.name;
    });
    this.namespace.setValue(
      this.namespaceOptions &&
        this.namespaceOptions.length &&
        this.namespaceOptions[0],
    );
  }

  // Called by parent component
  initRegion(region: any, mirrorClusters?: any) {
    this.region = region;
    if (mirrorClusters && mirrorClusters.length) {
      this.mirrorClusters = mirrorClusters;
      this.clusters = mirrorClusters.map((item: { name: string }) => {
        return item.name;
      });
    }
  }

  initValue(pvc: Pvc) {
    this.pvc = pvc;

    // name
    const nameCtrl = this.form.get('name');
    nameCtrl.setValue(this.pvc.kubernetes.metadata.name);
    nameCtrl.disable();

    // namespace
    this.namespaceOptions = [pvc.kubernetes.metadata.namespace];
    this.namespace.setValue(pvc.kubernetes.metadata.namespace);
    this.namespace.disable();

    // storageclass
    this.stroageClass.setValue(this.storageClassOptions[0].value);
  }

  get name() {
    return this.form.get('name');
  }

  get namespace() {
    return this.form.get('namespace');
  }

  get accessMode() {
    return this.form.get('accessMode');
  }

  get stroageClass() {
    return this.form.get('storageClass');
  }

  set accessMode(value: any) {
    this.accessMode.setValue(value.value);
  }

  get size() {
    return this.form.get('size');
  }

  get clusters() {
    return this.form.get('clusters');
  }

  set clusters(value: any) {
    this.clusters.setValue(value);
  }

  get updating() {
    return !!this.pvc;
  }

  async submit() {
    if (this.form.invalid || this.submitting) {
      return;
    }
    this.submitting = true;
    const values = this.form.value;
    // prepare data
    const payload: PvcPayload = {
      kind: 'PersistentVolumeClaim',
      spec: {
        accessModes: [values.accessMode],
        resources: {
          requests: {
            storage: values.size + 'G',
          },
        },
      },
      apiVersion: 'v1',
      metadata: {
        name: this.name.value,
        namespace: values.namespace,
      },
    };
    if (this.storageClassEnabled && values.storageClass !== 'default') {
      payload.spec.storageClassName =
        values.storageClass !== 'nonuse' ? values.storageClass : '';
    }

    if (this.appUserPvc) {
      payload.metadata.namespace = this.region.namespace;
    }

    try {
      this.updating
        ? await this.storageService.updatePvc(
            this.region.id,
            this.pvc.kubernetes.metadata.name,
            this.pvc.kubernetes.metadata.namespace,
            payload,
          )
        : await this.storageService.createPvc(
            this.region.id,
            payload,
            this.clusters.value,
          );
      this.complete(true);
    } catch (error) {
      this.errorToast.error(error);
    }

    this.submitting = false;
  }

  private complete(res?: any) {
    this.finish.next(res);
    this.finish.complete();
  }

  cancel() {
    this.complete();
  }
}
