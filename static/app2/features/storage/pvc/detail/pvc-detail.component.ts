import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import {
  ACCESS_MODES,
  PVC_STATUS_MAP,
} from 'app2/features/storage/storage.constant';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import {
  Pvc,
  StorageService,
} from 'app2/shared/services/features/storage.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { groupBy } from 'lodash';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { CreatePvcComponent } from '../create/create-pvc.component';

@Component({
  templateUrl: './pvc-detail.component.html',
})
export class PvcDetailComponent implements OnInit, OnDestroy {
  loading = false;
  loadError = false;
  paramsSubscription: Subscription;
  name: string;
  pvc: Pvc;
  clusterId: string;
  namespace: string;
  statusMap = PVC_STATUS_MAP;
  accessModes = ACCESS_MODES;
  isDeleting = false;
  canUpdate = false;
  canDelete = false;
  collapsed = true;
  refServices: { name: string; uuid: string }[] = [];

  constructor(
    private route: ActivatedRoute,
    private storageService: StorageService,
    private modal: ModalService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private roleUtil: RoleUtilitiesService,
    private router: Router,
    private errorToast: ErrorsToastService,
  ) {}

  async ngOnInit() {
    this.paramsSubscription = this.route.params
      .pipe(first())
      .subscribe(async params => {
        this.name = params['name'];
        this.clusterId = params['clusterId'];
        this.namespace = params['namespace'];
        await this.loadPvc();
      });
  }

  ngOnDestroy() {
    this.paramsSubscription.unsubscribe();
  }

  async loadPvc() {
    if (this.loading) {
      return;
    }
    this.loading = true;
    try {
      const pvc = await this.storageService.getPvcDetail(
        this.clusterId,
        this.namespace,
        this.name,
      );
      this.pvc = pvc;
    } catch ({ status, errors }) {
      this.loadError = true;
      if (status === 403) {
        this.auiNotificationService.warning(
          this.translate.get('permission_denied'),
        );
        this.router.navigate(['/storage/pvc']);
      } else if (status === 404) {
        this.auiNotificationService.warning(
          this.translate.get('pvc_not_exist'),
        );
        this.router.navigate(['/storage/pvc']);
      } else {
        this.auiNotificationService.error(
          errors ? errors[0].message : 'unknown_issue',
        );
      }
      this.loading = false;
      return;
    }
    const refs = groupBy(this.pvc.kubernetes.referenced_by, 'type');
    if (refs.AlaudaService) {
      this.refServices = refs.AlaudaService;
    }
    this.canDelete = this.hasPermission(this.pvc, 'delete');
    this.canUpdate = this.hasPermission(this.pvc, 'update');
    this.loading = false;
  }

  async update() {
    try {
      const modalRef = await this.modal.open(CreatePvcComponent, {
        title: this.translate.get('update_pvc'),
      });
      modalRef.componentInstance.initRegion({
        id: this.clusterId,
      });
      modalRef.componentInstance.initValue(this.pvc);
      modalRef.componentInstance.finish
        .pipe(first())
        .subscribe((result: boolean) => {
          modalRef.close();
          if (result) {
            modalRef.close();
            this.auiNotificationService.success(
              this.translate.get('pvc_update_success'),
            );
            this.loadPvc();
          }
        });
    } catch (error) {
      //
    }
  }

  async delete() {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete_pvc_title'),
        content: this.translate.get('delete_pvc_content', {
          name: this.pvc.kubernetes.metadata.name,
        }),
      });
    } catch (e) {
      return false;
    }
    this.isDeleting = true;
    try {
      await this.storageService.deletePvc(
        this.clusterId,
        this.pvc.kubernetes.metadata.namespace,
        this.name,
      );
      this.auiNotificationService.success(
        this.translate.get('pvc_delete_success'),
      );
      this.router.navigate(['/storage/pvc']);
    } catch (error) {
      this.errorToast.error(error);
    }
    this.isDeleting = false;
  }

  hasPermission(item: any, action: string) {
    return this.roleUtil.resourceHasPermission(
      item,
      'persistentvolumeclaim',
      action,
    );
  }

  get status() {
    return this.pvc.kubernetes.status.phase;
  }

  collapse() {
    this.collapsed = !this.collapsed;
  }
}
