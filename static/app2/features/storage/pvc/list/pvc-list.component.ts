import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { PVC_STATUS_MAP } from 'app2/features/storage/storage.constant';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import {
  Pvc,
  StorageService,
} from 'app2/shared/services/features/storage.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { cloneDeep, groupBy } from 'lodash';
import { Subscription } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import { CreatePvcComponent } from '../create/create-pvc.component';

@Component({
  templateUrl: './pvc-list.component.html',
  styleUrls: ['../../storage.common.scss', './pvc-list.component.scss'],
})
export class PvcListComponent implements OnInit, OnDestroy {
  loading = false;
  initialized = false;
  pvcs: Pvc[] = [];
  pvcCache: Pvc[] = [];
  statusMap = PVC_STATUS_MAP;
  regionSubscription: Subscription;
  region: Region;
  pagination: any = {
    currentPage: 1,
    count: 0,
    size: 20,
  };
  serviceLinks: { name: string; uuid: string }[];
  keyword = '';
  pollingTimer: any = null;
  destroyed = false;
  hasCreatePvcPermission = false;
  emptyMessage = 'no_data';
  notificationVisible = true;
  refService = {};

  get currentPageIndex(): number {
    return this.pagination.currentPage - 1;
  }

  constructor(
    private storageService: StorageService,
    private modal: ModalService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private regionService: RegionService,
    private roleUtil: RoleUtilitiesService,
    private router: Router,
    private errorToast: ErrorsToastService,
  ) {}

  showNotification() {
    this.notificationVisible = true;
  }

  closeNotification() {
    this.notificationVisible = false;
  }

  // search(keyword: string) {
  //   this.keyword = keyword;
  //   this.emptyMessage = keyword.length ? 'no_results' : 'no_data';
  //   this.load();
  // }

  filter(keyword: string) {
    this.keyword = keyword;
    this.pvcs = keyword.length
      ? this.pvcCache.filter((item: Pvc) => {
          return item.kubernetes.metadata.name.includes(keyword);
        })
      : this.pvcCache;
  }

  async ngOnInit() {
    // init region
    this.region = await this.regionService.getCurrentRegion();
    this.regionSubscription = this.regionService.region$
      .pipe(filter(region => !!region))
      .subscribe((region: Region) => {
        this.region = region;
        this.load();
      });
    this.hasCreatePvcPermission = await this.roleUtil.resourceTypeSupportPermissions(
      'persistentvolumeclaim',
      {
        cluster_name: this.region.name,
      },
    );
    await this.load();
    this.initialized = true;
  }

  ngOnDestroy() {
    clearTimeout(this.pollingTimer);
    this.regionSubscription.unsubscribe();
    this.destroyed = true;
  }

  async load() {
    if (this.destroyed) {
      return;
    }
    this.loading = true;
    try {
      const result = await this.storageService.getPvcs({
        cluster_id: this.region.id,
        page: this.pagination.currentPage,
        page_size: this.pagination.size,
        name: this.keyword,
      });
      this.pvcs = result.results || [];
      if (!this.pvcs.length && this.pagination.currentPage > 1) {
        --this.pagination.currentPage;
        this.load();
        return;
      }
      this.pagination.count = result.count;
      this.pvcHandler();
      this.pvcCache = cloneDeep(this.pvcs);
      this.filter(this.keyword);
      this.polling();
    } catch ({ errors }) {}
    this.loading = false;
  }

  pvcHandler() {
    this.pvcs.forEach((pvc: Pvc) => {
      const refs = groupBy(pvc.kubernetes.referenced_by, 'type');
      if (refs.AlaudaService) {
        this.refService[pvc.kubernetes.metadata.uid] = refs.AlaudaService;
      }
    });
  }

  async createPvc(updating: boolean = false, pvc: Pvc = null) {
    try {
      const modalRef = await this.modal.open(CreatePvcComponent, {
        title: this.translate.get(updating ? 'update_pvc' : 'create_pvc'),
      });
      modalRef.componentInstance.initRegion(cloneDeep(this.region));
      if (updating) {
        modalRef.componentInstance.initValue(pvc);
      }
      modalRef.componentInstance.finish
        .pipe(first())
        .subscribe((result: boolean) => {
          modalRef.close();
          if (result) {
            this.auiNotificationService.success(
              this.translate.get(
                updating ? 'pvc_update_success' : 'pvc_create_success',
              ),
            );
            this.load();
          }
        });
    } catch (error) {
      //
    }
  }
  async delete(pvc: Pvc) {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete_pvc_title'),
        content: this.translate.get('delete_pvc_content', {
          name: pvc.kubernetes.metadata.name,
        }),
      });
    } catch (e) {
      return false;
    }

    try {
      await this.storageService.deletePvc(
        this.region.id,
        pvc.kubernetes.metadata.namespace,
        pvc.kubernetes.metadata.name,
      );
      this.auiNotificationService.success(
        this.translate.get('pvc_delete_success'),
      );
      await this.load();
    } catch (error) {
      this.errorToast.error(error);
    }
  }

  async update(item: any) {
    await this.createPvc(true, item);
  }

  viewDetail(pvc: Pvc) {
    this.router.navigate([
      '/storage/pvc/detail',
      this.region.id,
      pvc.kubernetes.metadata.namespace,
      pvc.kubernetes.metadata.name,
    ]);
  }

  pageChanged(pageEvent: { pageIndex: number; pageSize: number }) {
    this.pagination.currentPage = ++pageEvent.pageIndex;
    this.pagination.size = pageEvent.pageSize;
    this.load();
  }

  hasPermission(item: Pvc, action: string) {
    return this.roleUtil.resourceHasPermission(
      item,
      'persistentvolumeclaim',
      action,
    );
  }

  polling() {
    clearTimeout(this.pollingTimer);
    if (this.destroyed || this.pagination.currentPage !== 1) {
      return;
    }
    this.pollingTimer = setTimeout(() => {
      this.load();
    }, 30 * 1000);
  }
}
