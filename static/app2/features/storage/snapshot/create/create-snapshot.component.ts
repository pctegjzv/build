import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { NotificationService } from 'alauda-ui';
import { SingleSelectionDropdownComponent } from 'app2/shared/components/select/single-selection-dropdown.component';
import { StorageService } from 'app2/shared/services/features/storage.service';
import { ErrorMapper } from 'app2/shared/types';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-create-snapshot',
  templateUrl: './create-snapshot.component.html',
  styleUrls: ['../../storage.common.scss', './create-snapshot.component.scss'],
})
export class CreateSnapshotComponent implements OnInit {
  @Output()
  finish = new EventEmitter<any>();
  @ViewChild('quotaSpaceDropdown')
  @ViewChild('snapshotForm')
  form: NgForm;
  quotaSpaceDropdown: SingleSelectionDropdownComponent;
  quotaEnabled = false;
  spaces: any[];
  submitting = false;
  initialized = false;
  data: any;
  errorMapper: ErrorMapper;
  volume: any;

  constructor(
    private storageService: StorageService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
  ) {}

  async ngOnInit() {
    this.data = {
      name: '',
      volume_name: this.volume.name,
      volume_id: this.volume.id,
      region_id: this.volume.region_id,
    };
    this.initialized = true;

    this.errorMapper = {
      pattern: this.translate.get('storage_name_hint'),
    };
  }

  initVolume(volume: any) {
    this.volume = volume;
  }

  async submit() {
    this.form.onSubmit(null);
    if (this.form.valid && !this.submitting) {
      this.submitting = true;
      try {
        await this.storageService.createSnapshot(this.data);
        this.complete({
          result: true,
        });
      } catch ({ errors }) {
        if (errors) {
          this.auiNotificationService.error(errors[0].message);
        }
      }
      this.submitting = false;
    }
  }

  private complete(res?: any) {
    this.finish.next(res);
    this.finish.complete();
  }

  cancel() {
    this.complete();
  }
}
