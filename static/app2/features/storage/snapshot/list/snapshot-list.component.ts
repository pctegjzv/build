import { Component, OnDestroy, OnInit } from '@angular/core';
import { cloneDeep, debounce, includes } from 'lodash';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { CreateVolumeComponent } from 'app2/features/storage/volume/create/create-volume.component';
import { RegionService } from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { StorageService } from 'app2/shared/services/features/storage.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ModalRef } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';

import { SNAPSHOT_STATES } from '../../storage.constant';

@Component({
  templateUrl: './snapshot-list.component.html',
  styleUrls: ['../../storage.common.scss'],
})
export class SnapshotListComponent implements OnInit, OnDestroy {
  loading = false;
  loadError = false;
  initialized = false;
  region: any;
  regionSubscription: Subscription;
  pagination: any = {
    count: 0,
    pageSize: 20,
    currentPage: 1,
    disabled: false,
  };
  keyword = '';
  snapshotList: any[] = [];
  currentPageCache: any[] = [];
  snapshotStateMap: any = SNAPSHOT_STATES;
  hasVolumeCreatePermission = false;
  createVolumeModalRef: ModalRef;
  createVolumeSnapshot: any;
  polling = debounce(this.loadSnapshots, 30000);
  emptyMessage = 'no_data';

  constructor(
    private translate: TranslateService,
    private storageService: StorageService,
    private regionService: RegionService,
    private roleUtilityService: RoleUtilitiesService,
    private modalService: ModalService,
    private auiNotificationService: NotificationService,
  ) {}

  async ngOnInit() {
    const region = await this.regionService.getCurrentRegion();
    this.regionSubscription = this.regionService.region$.subscribe(region => {
      this.onReginChanged(region);
    });
    this.onReginChanged(region);
  }

  ngOnDestroy() {
    this.polling.cancel();
  }

  async onReginChanged(region: any) {
    if (!region) {
      return;
    }
    this.region = region;
    this.hasVolumeCreatePermission = await this.roleUtilityService.resourceTypeSupportPermissions(
      'volume',
      {
        cluster_name: this.region.name,
      },
    );
    this.loadSnapshots();
  }

  async loadSnapshots() {
    if (this.loading) {
      return;
    }
    this.loading = true;
    const query = {
      pageno: this.pagination.currentPage,
      size: this.pagination.pageSize,
      list_all: false,
      region_id: this.region.id,
    };
    try {
      const result = await this.storageService.getSnapshots(query);
      this.loadError = false;
      this.snapshotList = this.snapshotsHandler(result.snapshots);
      this.pagination.count = this.snapshotList.length;
      // cache current page data
      this.currentPageCache = cloneDeep(this.snapshotList);
      this.search(this.keyword);
      // polling
      if (this.pagination.currentPage === 1) {
        this.polling();
      }
    } catch ({ errors }) {
      this.loadError = true;
    }
    this.loading = false;
    this.initialized = true;
  }

  snapshotsHandler(snapshots: any) {
    snapshots.forEach(async (item: any) => {
      item.canDelete =
        this.roleUtilityService.resourceHasPermission(
          item,
          'snapshot',
          'delete',
        ) && item.state !== 'pending';
      item.isDeleting = false;
    });
    return snapshots;
  }

  async createVolume(snapshot: any) {
    try {
      const modalRef = await await this.modalService.open(
        CreateVolumeComponent,
        {
          title: this.translate.get('storage_create_volume'),
        },
      );
      modalRef.componentInstance.initValue({
        region: this.region,
        snapshot: snapshot,
      });
      modalRef.componentInstance.finish
        .pipe(first())
        .subscribe((result: boolean) => {
          modalRef.close();
          if (result) {
            this.auiNotificationService.success(
              this.translate.get('storage_volume_create_success'),
            );
          }
        });
    } catch (e) {}
  }

  async deleteSnapshot(snapshot: any) {
    try {
      await this.modalService.confirm({
        title: this.translate.get('storage_delete_snapshot_title'),
        content: this.translate.get('storage_delete_snapshot_content', {
          name: snapshot.name,
        }),
      });
      snapshot.isDeleting = true;
      await this.storageService.deleteSnapshot(snapshot.id);
      this.auiNotificationService.success(
        this.translate.get('storage_snapshot_delete_success'),
      );
      this.loadSnapshots();
    } catch ({ errors }) {
      snapshot.isDeleting = false;
      this.auiNotificationService.error(this.translate.get(errors[0].code));
    }
  }

  search(keyword: string) {
    if (!keyword) {
      this.emptyMessage = this.translate.get('no_data');
      this.snapshotList = cloneDeep(this.currentPageCache);
      return;
    }
    this.emptyMessage = this.translate.get('no_results');
    this.keyword = keyword;
    this.snapshotList = this.currentPageCache.filter((snapshot: any) => {
      return includes(snapshot.name, this.keyword);
    });
  }

  pageChanged(pageNumber: number) {
    this.pagination.currentPage = pageNumber;
    this.loadSnapshots();
  }
}
