import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PvDetailComponent } from './pv/detail/pv-detail.component';
import { PvListComponent } from './pv/list/pv-list.component';
import { PvcDetailComponent } from './pvc/detail/pvc-detail.component';
import { PvcListComponent } from './pvc/list/pvc-list.component';
import { SnapshotListComponent } from './snapshot/list/snapshot-list.component';
import { CreateStorageClassComponent } from './storageclass/create/create-storageclass.component';
import { StorageClassDetailComponent } from './storageclass/detail/storageclass-detail.component';
import { StorageClassListComponent } from './storageclass/list/storageclass-list.component';
import { VolumeDetailComponent } from './volume/detail/volume-detail.component';
import { VolumeListComponent } from './volume/list/volume-list.component';

const storageRoutes: Routes = [
  {
    path: '',
    redirectTo: 'volume',
    pathMatch: 'full',
  },
  {
    path: 'volume',
    component: VolumeListComponent,
  },
  {
    path: 'volume/detail/:uuid',
    component: VolumeDetailComponent,
  },
  {
    path: 'snapshot',
    component: SnapshotListComponent,
  },
  {
    path: 'pvc',
    component: PvcListComponent,
  },
  {
    path: 'pvc/detail/:clusterId/:namespace/:name',
    component: PvcDetailComponent,
  },
  {
    path: 'pv',
    component: PvListComponent,
  },
  {
    path: 'pv/detail/:name',
    component: PvDetailComponent,
  },
  {
    path: 'storageclass',
    component: StorageClassListComponent,
  },
  {
    path: 'storageclass/create',
    component: CreateStorageClassComponent,
  },
  {
    path: 'storageclass/detail/:name',
    component: StorageClassDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(storageRoutes)],
  exports: [RouterModule],
})
export class StorageRoutingModule {}
