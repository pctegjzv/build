import { NgModule } from '@angular/core';
import { EventModule } from 'app2/features/event/event.module';
import { StorageRoutingModule } from 'app2/features/storage/storage-routing.module';
import { SharedModule } from 'app2/shared/shared.module';

import { CreatePvComponent } from './pv/create/create-pv.component';
import { PvDetailComponent } from './pv/detail/pv-detail.component';
import { PvListComponent } from './pv/list/pv-list.component';
import { CreatePvcComponent } from './pvc/create/create-pvc.component';
import { PvcDetailComponent } from './pvc/detail/pvc-detail.component';
import { PvcListComponent } from './pvc/list/pvc-list.component';
import { CreateSnapshotComponent } from './snapshot/create/create-snapshot.component';
import { SnapshotListComponent } from './snapshot/list/snapshot-list.component';
import { CreateStorageClassComponent } from './storageclass/create/create-storageclass.component';
import { StorageClassDetailComponent } from './storageclass/detail/storageclass-detail.component';
import { StorageClassListComponent } from './storageclass/list/storageclass-list.component';
import { CreateVolumeComponent } from './volume/create/create-volume.component';
import { VolumeDetailComponent } from './volume/detail/volume-detail.component';
import { VolumeListComponent } from './volume/list/volume-list.component';

@NgModule({
  imports: [SharedModule, EventModule, StorageRoutingModule],
  declarations: [
    VolumeListComponent,
    VolumeDetailComponent,
    CreateVolumeComponent,
    SnapshotListComponent,
    PvcListComponent,
    PvcDetailComponent,
    CreatePvcComponent,
    PvListComponent,
    PvDetailComponent,
    CreatePvComponent,
    CreateSnapshotComponent,
    StorageClassListComponent,
    CreateStorageClassComponent,
    StorageClassDetailComponent,
  ],
  entryComponents: [
    CreateVolumeComponent,
    CreateSnapshotComponent,
    CreatePvcComponent,
    CreatePvComponent,
  ],
})
export class StorageModule {}
