export interface VolumesQuery {
  region_id: string;
  pageno: number;
  size: number;
  list_all: boolean;
  volume_names?: string;
}

export interface VolumeBody {
  region_id: string;
  driver_name: string;
  name: string;
  space_name?: string;
  size: number;
  snapshot_id?: string;
  bricks?: string;
}

export interface SnapshotsQuery {
  region_id: string;
  pageno?: number;
  size?: number;
  list_all: boolean;
}

export interface SnapshotBody {
  name: string;
  volume_name: string;
  volume_id: string;
  region_id: string;
  space_name?: string;
}
