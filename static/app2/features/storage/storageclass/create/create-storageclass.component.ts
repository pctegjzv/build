import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import * as jsyaml from 'js-yaml';
import { Subscription } from 'rxjs';

import { NotificationService } from 'alauda-ui';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { StorageService } from 'app2/shared/services/features/storage.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './create-storageclass.component.html',
  styleUrls: ['./create-storageclass.component.scss'],
})
export class CreateStorageClassComponent implements OnInit, OnDestroy {
  loading = false;
  initialized = false;
  updating = false;
  submitting = false;
  region: Region;
  regionSubscription$: Subscription;
  // errorsMapper: any;
  yamlContent = '';
  payload: any;
  @ViewChild('form')
  form: NgForm;
  constructor(
    private regionService: RegionService,
    private translate: TranslateService,
    private router: Router,
    private auiNotificationService: NotificationService,
    private modal: ModalService,
    private storageService: StorageService,
    private errorService: ErrorsToastService,
  ) {}

  async ngOnInit() {
    this.region = await this.regionService.getCurrentRegion();
    this.regionSubscription$ = this.regionService.region$.subscribe(
      (region: Region) => (this.region = region),
    );
    // this.errorsMapper = {
    //   name: {
    //     pattern: this.translate.get('storageclass_name_hint'),
    //   },
    // };
    this.initialized = true;
  }

  async cancel() {
    try {
      await this.modal.confirm({
        title: this.translate.get('cancel'),
        content: this.translate.get('cancel_create_storageclass_content'),
      });
    } catch (error) {
      return false;
    }
    this.router.navigate(['/storage/storageclass']);
  }

  async submit() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    try {
      this.payload = jsyaml.safeLoadAll(this.yamlContent);
    } catch (error) {
      this.auiNotificationService.error(this.translate.get('yaml_format_hint'));
      return;
    }
    const payload = this.payload[0];
    // Reserved name or not
    const name = payload.metadata && payload.metadata.name;
    if (['alauda-system-gfs', 'alauda-system-ebs'].includes(name)) {
      this.auiNotificationService.error(
        this.translate.get('storageclass_reserved_name_hint', {
          name,
        }),
      );
      return;
    }
    try {
      await this.modal.confirm({
        title: this.translate.get('alarm'),
        content: this.translate.get('storageclass_create_confirm'),
      });
    } catch (error) {
      return;
    }
    this.submitting = true;
    try {
      const ret = await this.storageService.createStorageClass(
        this.region.id,
        payload,
      );
      this.auiNotificationService.success(
        this.translate.get('storageclass_create_success'),
      );
      this.router.navigate([
        '/storage/storageclass/detail',
        ret.kubernetes.metadata.name,
      ]);
    } catch (error) {
      this.errorService.error(error);
    }
    this.submitting = false;
  }

  ngOnDestroy() {}
}
