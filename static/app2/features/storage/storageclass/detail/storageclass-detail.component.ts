import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as jsyaml from 'js-yaml';
import { cloneDeep } from 'lodash';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import { RegionService } from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { StorageService } from 'app2/shared/services/features/storage.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './storageclass-detail.component.html',
  styleUrls: [
    '../../storage.common.scss',
    './storageclass-detail.component.scss',
  ],
})
export class StorageClassDetailComponent implements OnInit, OnDestroy {
  loading = false;
  initialized = false;
  loadError = false;
  inAction = false;
  paramsSubscription$: Subscription;
  regionSubscription$: Subscription;
  hasCreatePermission = false;
  name: string;
  storageClass: any;
  region: any;
  storageClassType = '';
  storageClassYaml = '';
  isSystem = false;
  isDefault = false;
  canUpdate = false;
  canDelete = false;
  lang: string;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private storageService: StorageService,
    private regionService: RegionService,
    private roleUtil: RoleUtilitiesService,
    private errorService: ErrorsToastService,
    private modal: ModalService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
  ) {
    this.lang = this.translate.currentLang;
  }

  async ngOnInit() {
    this.region = await this.regionService.getCurrentRegion();
    this.paramsSubscription$ = this.route.params
      .pipe(first())
      .subscribe(params => {
        this.name = params['name'];
      });
    this.hasCreatePermission = await this.roleUtil.resourceTypeSupportPermissions(
      'k8s_storageclasses',
    );
    await this.load();
    this.initialized = true;
  }

  hasPermission(item: any, action: string) {
    return this.roleUtil.resourceHasPermission(
      item,
      'k8s_storageclasses',
      action,
    );
  }

  async setDefault() {
    const payload = cloneDeep(this.storageClass.kubernetes);
    if (!payload.metadata.annotations) {
      payload.metadata.annotations = {};
    }
    payload.metadata.annotations[
      'storageclass.kubernetes.io/is-default-class'
    ] = this.isDefault ? 'false' : 'true';
    this.inAction = true;
    try {
      await this.storageService.updateStorageClass(
        this.region.id,
        this.storageClass.kubernetes.metadata.name,
        payload,
      );
      this.load();
    } catch (error) {
      this.errorService.error(error);
    }
    this.inAction = false;
  }

  async delete() {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete_storageclass_title'),
        content: this.translate.get('delete_storageclass_content', {
          name: this.storageClass.kubernetes.metadata.name,
        }),
      });
    } catch (error) {
      this.errorService.error(error);
      return;
    }
    this.inAction = true;
    try {
      await this.storageService.deleteStorageClass(
        this.region.id,
        this.storageClass.kubernetes.metadata.name,
      );
      this.auiNotificationService.success(
        this.translate.get('storageclass_delete_success'),
      );
      this.load();
    } catch (error) {
      this.errorService.error(error);
    }
    this.inAction = false;
  }

  async load() {
    this.loading = true;
    try {
      this.storageClass = await this.storageService.getStorageClassDetail(
        this.region.id,
        this.name,
      );
      this.canUpdate = this.hasPermission(this.storageClass, 'update');
      this.canDelete = this.hasPermission(this.storageClass, 'delete');
    } catch (error) {
      this.router.navigate(['/storage/storageclass']);
      return;
    }
    this.loading = false;
    this.storageClassType = this.storageClass.kubernetes.provisioner.split(
      '/',
    )[1];
    const kube = this.storageClass.kubernetes;
    this.storageClassYaml = jsyaml.safeDump(kube);
    this.isSystem = ['alauda-system-gfs', 'alauda-system-ebs'].includes(
      kube.metadata.name,
    );
    this.isDefault =
      kube.metadata.annotations &&
      kube.metadata.annotations[
        'storageclass.kubernetes.io/is-default-class'
      ] === 'true';
  }

  ngOnDestroy() {}
}
