import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { get } from 'lodash';
import { Subscription } from 'rxjs';
import { skip } from 'rxjs/operators';

import { NotificationService } from 'alauda-ui';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { StorageService } from 'app2/shared/services/features/storage.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './storageclass-list.component.html',
  styleUrls: [
    '../../storage.common.scss',
    './storageclass-list.component.scss',
  ],
})
export class StorageClassListComponent implements OnInit, OnDestroy {
  loading = false;
  initialized = false;
  searching = false;
  hasCreatePermission = false;
  loadError = false;
  data: any[] = [];
  regionSubscription$: Subscription;
  region: Region;
  pagination = {
    count: 0,
    pageSize: 100,
    currentPage: 1,
  };
  keyword = '';
  inAction = false;
  emptyMessage = 'no_data';
  lang: string;
  notificationVisible = true;
  constructor(
    private router: Router,
    private storageService: StorageService,
    private regionService: RegionService,
    private roleUtil: RoleUtilitiesService,
    private errorService: ErrorsToastService,
    private modal: ModalService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
  ) {
    this.lang = this.translate.currentLang;
  }

  async ngOnInit() {
    this.region = await this.regionService.getCurrentRegion();
    this.regionSubscription$ = this.regionService.region$
      .pipe(skip(1))
      .subscribe((region: Region) => {
        this.region = region;
        this.load();
      });
    this.hasCreatePermission = await this.roleUtil.resourceTypeSupportPermissions(
      'k8s_storageclasses',
      {
        cluster_name: this.region.name,
      },
    );
    await this.load();
    this.initialized = true;
  }

  showNotification() {
    this.notificationVisible = true;
  }

  closeNotification() {
    this.notificationVisible = false;
  }

  async load() {
    this.loading = true;
    let ret: any;

    try {
      ret = await this.storageService.getStorageClasses({
        cluster_id: this.region.id,
        page: this.pagination.currentPage,
        page_size: this.pagination.pageSize,
        name: this.keyword,
      });
    } catch (e) {
      this.errorService.error(e);
      this.loading = false;
      return;
    }

    this.pagination.count = ret.count;
    ret.results.forEach((item: any) => {
      const anno = get(item, 'kubernetes.metadata.annotations');
      item.isDefault =
        anno && anno['storageclass.kubernetes.io/is-default-class'] === 'true';
      item.isSystem = ['alauda-system-gfs', 'alauda-system-ebs'].includes(
        item.kubernetes.metadata.name,
      );
      item.canUpdate = this.hasPermission(item, 'update');
      item.canDelete = this.hasPermission(item, 'delete');
    });
    this.data = ret.results;
    this.loading = false;
  }

  pageChanged(page: number) {
    this.pagination.currentPage = page;
    this.load();
  }

  async setDefault(sc: any) {
    const payload = sc.kubernetes;
    payload.metadata.annotations[
      'storageclass.kubernetes.io/is-default-class'
    ] = sc.isDefault ? 'false' : 'true';
    sc.inAction = true;
    try {
      await this.storageService.updateStorageClass(
        this.region.id,
        sc.kubernetes.metadata.name,
        payload,
      );
      this.load();
    } catch (error) {
      this.errorService.error(error);
    }
    sc.inAction = false;
  }

  async delete(sc: any) {
    try {
      await this.modal.confirm({
        title: this.translate.get('delete_storageclass_title'),
        content: this.translate.get('delete_storageclass_content', {
          name: sc.kubernetes.metadata.name,
        }),
      });
    } catch (error) {
      this.errorService.error(error);
      return;
    }
    sc.inAction = true;
    try {
      await this.storageService.deleteStorageClass(
        this.region.id,
        sc.kubernetes.metadata.name,
      );
      this.auiNotificationService.success(
        this.translate.get('storageclass_delete_success'),
      );
      this.load();
    } catch (error) {
      this.errorService.error(error);
    }
    sc.inAction = false;
  }

  viewDetail(storageClass: any) {
    this.router.navigate([
      '/storage/storageclass/detail',
      storageClass.kubernetes.metadata.name,
    ]);
  }

  hasPermission(item: any, action: string) {
    return this.roleUtil.resourceHasPermission(
      item,
      'k8s_storageclasses',
      action,
    );
  }

  createStorageClass() {
    this.router.navigate(['/storage/storageclass/create']);
  }

  extractStorageClassName(storageClass: any) {
    return storageClass.kubernetes.provisioner.split('/')[1];
  }

  searchChanged(keyword: string) {
    this.keyword = keyword;
    this.emptyMessage = keyword.length ? 'no_results' : 'no_data';
    this.load();
  }

  ngOnDestroy() {}
}
