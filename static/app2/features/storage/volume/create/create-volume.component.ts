import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { NotificationService } from 'alauda-ui';
import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';
import { SingleSelectionDropdownComponent } from 'app2/shared/components/select/single-selection-dropdown.component';
import { StorageService } from 'app2/shared/services/features/storage.service';
import { TranslateService } from 'app2/translate/translate.service';
import * as _ from 'lodash';

import { DRIVERS, VOLUME_TYPES } from '../../storage.constant';

@Component({
  selector: 'rc-create-volume',
  templateUrl: './create-volume.component.html',
  styleUrls: ['../../storage.common.scss', './create-volume.component.scss'],
})
export class CreateVolumeComponent implements OnInit {
  @Output()
  finish = new EventEmitter<any>();
  @ViewChild('volumeForm')
  volumeForm: NgForm;
  @ViewChild('snapshotDropdown')
  snapshotDropdown: SingleSelectionDropdownComponent;
  region: any;
  drivers: any[];
  snapshot: any;
  initialized = false;
  volumeTypes: string[] = VOLUME_TYPES;
  volumeSizeRange: any = {
    min: 10,
    max: 1024,
  };
  driverType: any;
  quotaEnabled = false;
  submitting = false;
  dataModel: any;
  errorMappers: any;
  snapshotOptions: any[];
  quotaSpaceOptions: string[];
  driverTypeOptions: any[];
  shouldShowVolumeSize = false;
  shouldShowVolumeType = false;
  shouldShowBricks = false;
  dropdowns: any = {
    snapshot: {},
    quotaSpace: {},
    driverType: {},
  };
  constructor(
    private translate: TranslateService,
    private storageService: StorageService,
    private auiNotificationService: NotificationService,
    @Inject(WEBLABS) public weblabs: Weblabs,
  ) {}

  async ngOnInit() {
    this.dataModel = {
      regionId: this.region.id,
      snapshotId: '',
      driverName: '',
      name: '',
      size: '',
      bricksNode: '',
      bricksPath: '',
    };

    // custom error mappers
    this.errorMappers = {
      name: {
        pattern: this.translate.get('storage_name_hint'),
      },
      size: {
        pattern: this.translate.get('integer_pattern'),
        outOfRange: '10 - 1024',
      },
    };

    // await this.initFieldSnapshot();
    this.initFieldDriverType();
    this.initialized = true;
  }

  initValue(value: any) {
    this.region = value.region;
    this.drivers = value.drivers;
    this.snapshot = value.snapshot;
  }

  async initFieldSnapshot() {
    try {
      const { snapshots } = await this.storageService.getSnapshots({
        region_id: this.region.id,
        list_all: true,
      });
      this.dropdowns.snapshot.options = snapshots.filter((snapshot: any) => {
        return snapshot.state === 'completed';
      });
    } catch ({ errors }) {
      this.dropdowns.snapshot.options = [];
    }
    this.dropdowns.snapshot.options.unshift({
      id: 'undefined',
      name: this.translate.get('storage_snapshot_select_none'),
    });
    // default
    this.dataModel.snapshotId = 'undefined';
  }

  async initFieldDriverType() {
    if (!this.drivers) {
      try {
        const { result } = await this.storageService.getVolumeDrivers(
          this.region.name,
        );
        this.drivers = result;
      } catch (error) {
        // error handle
        this.drivers = [];
      }
    }
    const driverTypes = this.drivers.map(({ driver }) => {
      return this.generateDriverType(driver);
    });
    const driverTypesGroupedByDriverTypes = _.groupBy(driverTypes, 'driver');
    this.dropdowns.driverType.options = ((): any[] => {
      return this.snapshot && this.snapshot.id
        ? driverTypesGroupedByDriverTypes[this.snapshot.driver_name]
        : driverTypes;
    })();
    this.setDriverTypeValue();
  }

  setDriverTypeValue(driverName?: string) {
    if (driverName) {
      this.dataModel.driverName = driverName;
    } else if (this.dropdowns.driverType.options.length) {
      // default
      this.dataModel.driverName = this.dropdowns.driverType.options[0].driver;
    }
    this.driverTypeChanged();
  }

  onSnapshotChanged(option: any) {
    if (this.initialized) {
      this.setDriverTypeValue(option.driver_name);
    }
  }

  driverTypeChanged(_driverType?: any) {
    this.driverType = this.generateDriverType(this.dataModel.driverName);
    this.initFieldVolumeSize();
    this.initFieldVolumeType();
    this.initFieldsBricks();
  }

  initFieldVolumeSize() {
    this.volumeSizeRange = this.getVolumeSizeRange();
    if (
      this.snapshot &&
      this.snapshot.id &&
      this.driverType.driver === 'glusterfs'
    ) {
      this.shouldShowVolumeSize = false;
    } else {
      this.shouldShowVolumeSize = true;
    }
  }

  initFieldVolumeType() {
    if (this.driverType && this.driverType.fields.includes('volume_type')) {
      this.shouldShowVolumeType = true;
      this.dataModel.volumeType = this.volumeTypes[0];
    } else {
      this.shouldShowVolumeType = false;
    }
  }

  initFieldsBricks() {
    if (this.driverType && this.driverType.fields.includes('bricks')) {
      this.shouldShowBricks = true;
    } else {
      this.shouldShowBricks = false;
    }
  }

  generateDriverType(driverName: string) {
    return {
      driver: driverName,
      displayName: this.translate.get(
        'storage_volume_driver_type_' + driverName,
      ),
      fields: DRIVERS[driverName] ? DRIVERS[driverName] : [],
    };
  }

  getVolumeSizeRange() {
    const min = () => {
      const regionVolumeMinSize = _.get(
        this.region,
        'features.volume.size.min',
        10,
      );
      const snapshotSize = _.get(this.snapshot, 'size', 0);
      return Math.max(snapshotSize, regionVolumeMinSize);
    };
    const max = () => {
      const regionVolumeMaxSize = _.get(
        this.region,
        'features.volume.size.max',
        1024,
      );
      return Math.max(min(), regionVolumeMaxSize);
    };
    return {
      min: min(),
      max: max(),
    };
  }

  submit() {
    this.volumeForm.onSubmit(null);
    if (!this.volumeForm.valid || this.submitting) {
      return;
    }
    // init params
    const volume: any = {
      region_id: this.dataModel.regionId,
      driver_name: this.dataModel.driverName,
      name: this.dataModel.name,
    };
    if (this.snapshot && this.snapshot.id) {
      volume.snapshot_id = this.snapshot.id;
    } else if (this.dataModel.snapshotId !== 'undefined') {
      volume.snapshot_id = this.dataModel.snapshotId;
    }
    if (this.dataModel.size) {
      volume.size = parseInt(this.dataModel.size, 10);
    }
    if (this.dataModel.volumeType) {
      volume.volume_type = this.dataModel.volumeType;
    }
    if (this.dataModel.bricksNode) {
      volume.bricks = `${this.dataModel.bricksNode}:${
        this.dataModel.bricksPath
      }`;
    }
    this.submitting = true;
    this.storageService
      .createVolume(volume)
      .then(() => {
        this.submitting = false;
        this.complete(true);
      })
      .catch(({ errors }) => {
        this.submitting = false;
        if (errors) {
          this.auiNotificationService.error(
            this.translate.get(errors[0].message),
          );
        }
      });
  }

  private complete(res?: any) {
    this.finish.next(res);
    this.finish.complete();
  }

  cancel() {
    this.complete();
  }
}
