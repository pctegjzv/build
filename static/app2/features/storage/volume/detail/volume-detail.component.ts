import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { RouterUtilService } from 'app2/core/services/router-util.service';
import { CreateSnapshotComponent } from 'app2/features/storage/snapshot/create/create-snapshot.component';
import { RegionService } from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { StorageService } from 'app2/shared/services/features/storage.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import {
  SNAPSHOT_SUPPORTED_DRIVERS,
  VOLUME_STATES,
} from '../../storage.constant';

@Component({
  templateUrl: './volume-detail.component.html',
  styleUrls: ['../../storage.common.scss', './volume-detail.component.scss'],
})
export class VolumeDetailComponent implements OnInit {
  volumeStateMap: any = VOLUME_STATES;
  snapshotSupportedDrivers: string[] = SNAPSHOT_SUPPORTED_DRIVERS;
  routeParamsSubscription: Subscription;
  pollingTimer: any;
  uuid: string;
  loading: boolean;
  loadError: any;
  volume: any;
  region: any;
  isSnapshotSupported = false;
  hasDeletePermission = false;
  isDeleting = false;
  isNewK8s: boolean;
  collapsed = true;
  constructor(
    private route: ActivatedRoute,
    private storageService: StorageService,
    private roleUtilities: RoleUtilitiesService,
    private routerUtil: RouterUtilService,
    private regionService: RegionService,
    private modalService: ModalService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private regionUtil: RegionUtilitiesService,
    private router: Router,
    private errorToast: ErrorsToastService,
  ) {}

  ngOnInit() {
    this.routeParamsSubscription = this.route.params
      .pipe(first())
      .subscribe(async params => {
        this.uuid = params['uuid'];
        await this.loadVolume();
      });
  }

  async loadVolume() {
    if (this.loading) {
      return;
    }
    this.loading = true;
    try {
      this.volume = await this.storageService.getVolumeDetail(this.uuid);
      this.loading = false;
      // this.isSnapshotSupported =
      //   this.snapshotSupportedDrivers.includes(this.volume.driver_name) &&
      //   (await this.roleUtilities.resourceTypeSupportPermissions('snapshot'));
      this.isSnapshotSupported = false;
      this.hasDeletePermission = await this.roleUtilities.resourceHasPermission(
        this.volume,
        'volume',
        'delete',
      );

      this.volume.stats.forEach(async (stat: any) => {
        stat.href = await this.getServiceUrl(stat);
      });
      const regions = await this.regionService.regions$
        .pipe(first())
        .toPromise();
      this.region = regions.find(region => {
        return region.id === this.volume.region_id;
      });
      this.isNewK8s = this.regionUtil.isNewK8sRegion(this.region);
    } catch ({ status, errors }) {
      if (status === 403) {
        this.auiNotificationService.warning(
          this.translate.get('permission_denied'),
        );
      } else if (status === 404) {
        this.auiNotificationService.warning(
          this.translate.get('volume_not_exist'),
        );
      }
      return this.router.navigateByUrl('storage/volume');
    }
  }

  async getServiceUrl(stat: any) {
    const stateName = 'app_service.service.service_detail';
    const params = {
      service_name: stat.service_id,
    };
    return await this.routerUtil.href(stateName, params);
  }

  async deleteVolume() {
    try {
      await this.modalService.confirm({
        title: this.translate.get('storage_delete_volume_title'),
        content: this.translate.get('storage_delete_volume_content', {
          name: this.volume.name,
        }),
      });
    } catch (e) {
      return false;
    }

    this.isDeleting = true;
    try {
      await this.storageService.deleteVolume(this.volume.id);
      this.router.navigate(['/storage/volume']);
    } catch (error) {
      this.errorToast.error(error);
    }
    this.isDeleting = false;
  }

  async createSnapshot() {
    try {
      const modalRef = await this.modalService.open(CreateSnapshotComponent, {
        title: this.translate.get('storage_create_snapshot'),
      });
      modalRef.componentInstance.initVolume(this.volume);
      modalRef.componentInstance.finish.pipe(first()).subscribe(result => {
        modalRef.close();
        if (result) {
          this.auiNotificationService.success(
            this.translate.get('storage_snapshot_create_success'),
          );
        }
      });
    } catch (error) {}
  }

  collapse() {
    this.collapsed = !this.collapsed;
  }
}
