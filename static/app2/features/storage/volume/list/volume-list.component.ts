import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { RouterUtilService } from 'app2/core/services/router-util.service';
import { CreateSnapshotComponent } from 'app2/features/storage/snapshot/create/create-snapshot.component';
import { CreateVolumeComponent } from 'app2/features/storage/volume/create/create-volume.component';
import { RegionService } from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { StorageService } from 'app2/shared/services/features/storage.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import {
  SNAPSHOT_SUPPORTED_DRIVERS,
  VOLUME_STATES,
} from '../../storage.constant';

@Component({
  templateUrl: './volume-list.component.html',
  styleUrls: ['../../storage.common.scss'],
})
export class VolumeListComponent implements OnInit {
  // data from service page
  @Input()
  serviceDetail: any;
  loading = false;
  loadError = false;
  initialized = false;
  isNewK8s: boolean;
  region: any;
  regionSubscription: Subscription;
  pagination: any = {
    count: 0,
    pageSize: 20,
    currentPage: 1,
    disabled: false,
  };
  volumeList: any[];
  currentPageCache: any = {};
  volumeStateMap = VOLUME_STATES;
  snapshotSupportedDrivers = SNAPSHOT_SUPPORTED_DRIVERS;
  hasSnapshotCreatePermission = false;
  hasVolumeCreatePermission = false;
  drivesByRegionId: any = {};
  snapshotVolume: any;
  emptyMessage = 'no_data';

  constructor(
    private translate: TranslateService,
    private storageService: StorageService,
    private regionService: RegionService,
    private roleUtilityService: RoleUtilitiesService,
    private routerUtilService: RouterUtilService,
    private regionUtil: RegionUtilitiesService,
    private modalService: ModalService,
    private auiNotificationService: NotificationService,
    private router: Router,
    private errorToast: ErrorsToastService,
  ) {}

  async ngOnInit() {
    this.region = await this.regionService.getCurrentRegion();
    this.regionSubscription = this.regionService.region$.subscribe(region =>
      this.onRegionChanged(region),
    );
    this.hasSnapshotCreatePermission = await this.roleUtilityService.resourceTypeSupportPermissions(
      'snapshot',
    );
    await this.onRegionChanged(this.region);
  }

  async onRegionChanged(region: any) {
    if (!region) {
      return;
    }
    this.region = region;
    this.hasVolumeCreatePermission = await this.roleUtilityService.resourceTypeSupportPermissions(
      'volume',
      {
        cluster_name: this.region.name,
      },
    );
    this.loadVolumes();
    this.loadDrives();
    this.isNewK8s = this.regionUtil.isNewK8sRegion(region);
  }

  async loadVolumes() {
    if (this.loading) {
      return;
    }
    this.loading = true;
    const query = {
      pageno: this.pagination.currentPage,
      size: this.pagination.pageSize,
      list_all: false,
      region_id: this.region.id,
    };
    // service page
    if (this.serviceDetail) {
      query['volume_names'] = _.uniq(
        this.serviceDetail.volumes.map((volume: any) => volume.volume_name),
      );
      // set size with 100 and disable pagination for service detail page
      query['size'] = 100;
    }
    try {
      const result = await this.storageService.getVolumes(query);
      this.loadError = false;
      this.volumeList = await this.volumesHandler(result.volumes);
      // cache current page data
      this.currentPageCache = _.cloneDeep(this.volumeList);
      this.pagination.count = result.total_items;
    } catch ({ errors }) {
      this.loadError = true;
      this.volumeList = [];
    }
    this.initialized = true;
    this.loading = false;
  }

  async loadDrives() {
    try {
      const drives = await this.storageService.getVolumeDrivers(
        this.region.name,
      );
      this.drivesByRegionId[this.region.id] = drives.result;
    } catch ({ errors }) {
      // error handle
      this.drivesByRegionId[this.region.id] = [];
    }
  }

  search(keyword: string) {
    if (!keyword.length) {
      this.emptyMessage = 'no_data';
      this.volumeList = _.cloneDeep(this.currentPageCache);
      return;
    }
    this.emptyMessage = 'no_results';
    this.volumeList = this.currentPageCache.filter((volume: any) => {
      return _.includes(volume.name, keyword);
    });
  }

  async volumesHandler(volumes: any) {
    // for service detail page
    let serviceVolumesMap: any;
    if (this.serviceDetail && this.serviceDetail.volumes) {
      serviceVolumesMap = _.groupBy(this.serviceDetail.volumes, 'volume_name');
    }
    volumes.forEach(async (volume: any) => {
      volume.canDelete =
        volume.state !== 'in-use' && this.hasPermission(volume, 'delete');
      volume.isDeleting = false;
      // volume.canAddSnapshot = this.isSnapshotSupported(volume);
      // Disable for 1.13
      volume.canAddSnapshot = false;

      // for service detail page and no stats
      if (!!serviceVolumesMap && !volume.stats) {
        volume.stats = serviceVolumesMap[volume.name].map(
          (serviceVolume: any) => ({
            app_volume_dir: serviceVolume.app_volume_dir,
          }),
        );
        // Set date to null for 'hostPath'
        volume.created_at = null;
      }

      volume.stats.forEach(async (stat: any) => {
        stat.href = await this.getServiceUrl(stat);
      });
    });
    return volumes;
  }

  async deleteVolume(volume: any) {
    try {
      await this.modalService.confirm({
        title: this.translate.get('storage_delete_volume_title'),
        content: this.translate.get('storage_delete_volume_content', {
          name: volume.name,
        }),
      });
    } catch (error) {
      return false;
    }
    volume.isDeleting = true;
    try {
      await this.storageService.deleteVolume(volume.id);
      this.auiNotificationService.success(
        this.translate.get('storage_volume_delete_success'),
      );
      this.loadVolumes();
    } catch (error) {
      this.errorToast.error(error);
    }
    volume.isDeleting = false;
  }

  async createVolume() {
    try {
      const modalRef = await this.modalService.open(CreateVolumeComponent, {
        title: this.translate.get('storage_create_volume'),
      });
      modalRef.componentInstance.initValue({
        region: this.region,
        drivers: this.drivesByRegionId[this.region.id],
      });
      modalRef.componentInstance.finish
        .pipe(first())
        .subscribe((result: boolean) => {
          modalRef.close();
          if (result) {
            this.auiNotificationService.success(
              this.translate.get('storage_volume_create_success'),
            );
            this.loadVolumes();
          }
        });
    } catch (e) {}
  }

  async createSnapshot(volume: any) {
    try {
      const modalRef = await this.modalService.open(CreateSnapshotComponent, {
        title: this.translate.get('storage_create_snapshot'),
      });
      modalRef.componentInstance.initVolume(volume);
      modalRef.componentInstance.finish.pipe(first()).subscribe(result => {
        modalRef.close();
        if (result) {
          this.auiNotificationService.success(
            this.translate.get('storage_snapshot_create_success'),
          );
        }
      });
    } catch (error) {}
  }

  viewDetail(volume: any) {
    this.router.navigate(['/storage/volume/detail', volume.id]);
  }

  hasPermission(item: any, action: string) {
    return this.roleUtilityService.resourceHasPermission(
      item,
      'volume',
      action,
    );
  }

  isSnapshotSupported(volume: any) {
    return (
      this.snapshotSupportedDrivers.includes(volume.driver_name) &&
      this.hasSnapshotCreatePermission
    );
  }

  async getServiceUrl(stat: any) {
    const stateName = 'app_service.service.service_detail';
    const params = {
      service_name: stat.service_id,
    };
    return await this.routerUtilService.href(stateName, params);
  }

  pageChanged(pageNumber: number) {
    this.pagination.currentPage = pageNumber;
    this.loadVolumes();
  }
}
