import {
  Component,
  EventEmitter,
  Inject,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { NotificationService } from 'alauda-ui';
import { BuildService } from 'app2/shared/services/features/build.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './account-info-os-china-link.component.html',
})
export class AccountInfoOsChinaLinkComponent {
  @Output()
  close = new EventEmitter<boolean>();

  @ViewChild(NgForm)
  ngForm: NgForm;

  infoName = '';
  infoPass = '';

  submitting: boolean;

  constructor(
    @Inject(MODAL_DATA)
    private data: {
      codeClientName: string;
      beforeLink: () => void;
    },
    private build: BuildService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private errorToast: ErrorsToastService,
  ) {}

  async confirm() {
    if (this.ngForm.invalid) {
      return;
    }

    this.data.beforeLink();

    this.submitting = true;

    try {
      await this.build.linkPrivateBuildCodeClient({
        codeClientName: this.data.codeClientName,
        oschinaUserEmail: this.infoName,
        oschinaUserPwd: this.infoPass,
      });

      this.close.emit(true);

      this.auiNotificationService.success(
        this.translate.get('oschina_link_success'),
      );
    } catch (e) {
      this.errorToast.error(e);
    }

    this.submitting = false;
  }
}
