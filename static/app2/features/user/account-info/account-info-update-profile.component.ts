import {
  Component,
  EventEmitter,
  Inject,
  Output,
  ViewChild,
} from '@angular/core';

import { NgForm } from '@angular/forms';
import { NotificationService } from 'alauda-ui';
import { RcProfile } from 'app2/core/types';
import { AccountService } from 'app2/shared/services/features/account.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './account-info-update-profile.component.html',
})
export class AccountInfoUpdateProfileComponent {
  @Output()
  close = new EventEmitter<{
    email: string;
    mobile: string;
  }>();

  @ViewChild(NgForm)
  ngForm: NgForm;

  field: 'email' | 'mobile';
  email: string;
  mobile: string;

  submitting: boolean;

  constructor(
    @Inject(MODAL_DATA)
    private data: {
      field: 'email' | 'mobile';
      profile: RcProfile;
    },
    private account: AccountService,
    private auiNotificationService: NotificationService,
    private errorToast: ErrorsToastService,
    private translate: TranslateService,
  ) {
    const { field, profile } = this.data;
    this.field = field;
    this.email = profile.email;
    this.mobile = profile.mobile;
  }

  async confirm() {
    if (this.ngForm.invalid) {
      return;
    }

    this.submitting = true;

    const params = {
      email: this.email,
      mobile: this.mobile,
    };

    try {
      await this.account.updateProfile(params as RcProfile);

      this.close.emit(params);

      this.auiNotificationService.success(this.translate.get('update_success'));
    } catch (e) {
      this.errorToast.error(e);
    } finally {
      this.submitting = false;
    }
  }
}
