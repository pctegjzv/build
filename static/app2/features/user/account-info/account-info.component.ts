import { Component, Inject, Input, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';

import { ActivatedRoute } from '@angular/router';
import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount, RcProfile } from 'app2/core/types';
import { AccountUpdatePwdComponent } from 'app2/features/rbac/account/dialog/account-update-pwd/account-update-pwd.component';
import { AccountInfoOsChinaLinkComponent } from 'app2/features/user/account-info/account-info-os-china-link.component';
import { AccountInfoUpdateProfileComponent } from 'app2/features/user/account-info/account-info-update-profile.component';
import {
  BuildService,
  CodeClient,
} from 'app2/shared/services/features/build.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-account-info',
  templateUrl: './account-info.component.html',
  styleUrls: ['./account-info.component.scss'],
})
export class AccountInfoComponent implements OnInit {
  @Input()
  profile: RcProfile;
  @Input()
  token: string;
  linking: boolean;
  codeClients: CodeClient[];

  constructor(
    @Inject(ACCOUNT) public account: RcAccount,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private modal: ModalService,
    private build: BuildService,
  ) {}

  ngOnInit() {
    if (!this.account.username) {
      this.getCodeClients();
    }
  }

  updateRootPassword() {
    this.modal.open(AccountUpdatePwdComponent, {
      title: this.translate.get('update_password'),
      data: {},
    });
  }

  updateProfile(field: string) {
    const modalRef = this.modal.open(AccountInfoUpdateProfileComponent, {
      title: this.translate.get('update'),
      data: {
        field,
        profile: this.profile,
      },
    });

    modalRef.componentInstance.close
      .pipe(take(1))
      .subscribe((profile: RcProfile) => {
        Object.assign(this.profile, profile);
        modalRef.close();
      });
  }

  async getCodeClients() {
    this.codeClients = await this.build.getPrivateBuildCodeClients();
  }

  oschinaLinkClient(codeClient: CodeClient) {
    const modalRef = this.modal.open(AccountInfoOsChinaLinkComponent, {
      title: this.translate.get('oschina_link'),
      data: {
        codeClientName: codeClient.name,
        beforeLink: () => (codeClient.is_authed = 'linking'),
      },
    });

    modalRef.componentInstance.close
      .pipe(take(1))
      .subscribe((isConfirm: boolean) => {
        if (isConfirm) {
          this.getCodeClients();
        } else {
          codeClient.is_authed = false;
        }
        modalRef.close();
      });
  }

  async linkClient(codeClient: CodeClient) {
    if (codeClient.name === 'OSCHINA') {
      return this.oschinaLinkClient(codeClient);
    }

    this.linking = true;

    try {
      location.href = await this.build.getPrivateBuildCodeClientAuthUrl({
        codeClientName: codeClient.name,
        redirectParams: {
          nextStateName: 'user.detail',
          nextStateParams: {
            username: this.route.snapshot.queryParamMap.get('username'),
            currentTab: 'ACCOUNT_INFO',
          },
        },
      });
    } catch (e) {
      this.linking = false;
    }
  }

  async unlinkClient(codeClient: CodeClient) {
    try {
      await this.modal.confirm({
        title: this.translate.get('unlink_oauth_code_client_confirm_title'),
        content: this.translate.get(
          'unlink_oauth_code_client_confirm_content',
          {
            clientName: codeClient.name,
          },
        ),
      });
    } catch (e) {
      return;
    }

    codeClient.is_authed = 'unlinking';

    try {
      await this.build.unlinkPrivateBuildCodeClient({
        codeClientName: codeClient.name,
      });
    } catch (e) {
      codeClient.is_authed = true;
      return;
    }

    await this.getCodeClients();
  }
}
