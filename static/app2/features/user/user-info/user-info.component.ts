import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { NotificationService } from 'alauda-ui';
import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { AccountUpdatePwdComponent } from 'app2/features/rbac/account/dialog/account-update-pwd/account-update-pwd.component';
import { ACCOUNT_TYPE } from 'app2/shared/services/features/account.service';
import { OrgService } from 'app2/shared/services/features/org.service';
import { RoleService } from 'app2/shared/services/features/role.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { User } from 'app2/shared/types';
import { TranslateService } from 'app2/translate/translate.service';

const INPUT_FIELDS = [
  'realname',
  'department',
  'position',
  'mobile',
  'email',
  'landline_phone',
];
const TEXT_FIELDS = ['office_address', 'extra_info'];

@Component({
  selector: 'rc-user-info',
  templateUrl: './user-info.component.html',
})
export class UserInfoComponent implements OnInit {
  @Input()
  username: string;
  @Input()
  user: User;
  @Output()
  update: EventEmitter<User> = new EventEmitter();

  fields: string[] = [...INPUT_FIELDS, ...TEXT_FIELDS];
  editing: boolean;
  submitting: boolean;
  showPassword: boolean;
  subAccountPermissions: string[] = [];
  isTPAUser: boolean;
  isLDAPAccount: boolean;

  userModel: User = { ...this.user };

  patterns: object = {
    mobile: /^\d{11}$/,
    // tslint:disable-next-line max-line-length
    email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  };

  constructor(
    @Inject(ACCOUNT) public account: RcAccount,
    private roleService: RoleService,
    private orgService: OrgService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private modal: ModalService,
  ) {}

  async ngOnInit() {
    this.userModel = { ...this.user };

    this.subAccountPermissions = await this.roleService.getContextPermissions(
      'subaccount',
    );
    this.isLDAPAccount = this.isLDAPType(this.userModel.type);
    this.isTPAUser = this.isTPAType(this.userModel.type);
    //organizations.TPAccount: Three party users
    if (
      (this.account.username === this.username ||
        this.hasPermission('update_password')) &&
      !this.isTPAUser
    ) {
      this.showPassword = true;
    }
  }

  hasPermission(type: string) {
    return this.subAccountPermissions.includes(`subaccount:${type}`);
  }

  isTPAType(type: string) {
    return type === ACCOUNT_TYPE.TPA_ACCOUNT;
  }

  isLDAPType(type: string) {
    return type === ACCOUNT_TYPE.LDAP_ACCOUNT;
  }

  async updateUser(form: NgForm) {
    if (form.invalid) {
      return;
    }

    const user = {
      ...this.user,
      ...this.userModel,
    } as User;

    this.submitting = true;

    try {
      await this.orgService.updateUser(user);

      this.userModel = { ...user };
      this.update.emit(user);
    } catch (e) {
      this.auiNotificationService.error(this.translate.get(e.errors[0].code));
    } finally {
      this.submitting = this.editing = false;
    }
  }

  cancelUpdate() {
    this.userModel = { ...this.user } as User;
    this.editing = false;
  }

  changePassword() {
    this.modal.open(AccountUpdatePwdComponent, {
      title: this.translate.get('update_password'),
      data: {
        username: this.username,
      },
    });
  }
}
