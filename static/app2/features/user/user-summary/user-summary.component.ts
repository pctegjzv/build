import { Component, Inject, Input, OnInit } from '@angular/core';
import { NotificationService } from 'alauda-ui';
import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount, RcProfile } from 'app2/core/types';
import { UpdateCompanyComponent } from 'app2/features/rbac/detail/dialog/update-company/update-company.component';
import { AccountService } from 'app2/shared/services/features/account.service';
import { Org, OrgService } from 'app2/shared/services/features/org.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { User } from 'app2/shared/types';
import { TranslateService } from 'app2/translate/translate.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'rc-user-summary',
  templateUrl: './user-summary.component.html',
  styleUrls: ['./user-summary.component.scss'],
})
export class UserSummaryComponent implements OnInit {
  @Input()
  username: string;
  @Input()
  user: User;
  @Input()
  profile: RcProfile;
  @Input()
  token: string;

  avatarPath: string;
  avatarSubmitting: boolean;
  avatarPreviewing: boolean;
  org: Org;
  tokenCopied = false;

  constructor(
    @Inject(ACCOUNT) public account: RcAccount,
    private orgService: OrgService,
    private accountService: AccountService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private modal: ModalService,
  ) {}

  async ngOnInit() {
    this.org = await this.orgService.getOrg();
    this.avatarPath = this.org.logo_file;
  }

  previewAvatar(avatarFile: HTMLInputElement) {
    const file = avatarFile.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.avatarPath = reader.result as string;
      this.avatarPreviewing = true;
    };
  }

  cancelPreview(avatarFile: HTMLInputElement) {
    this.avatarPath = this.org.logo_file;
    avatarFile.value = '';
    this.avatarPreviewing = false;
  }

  async updateAvatar(avatarFile: HTMLInputElement) {
    this.avatarSubmitting = true;
    try {
      await this.accountService.updateNamespaceLogo(
        this.account.namespace,
        avatarFile.files[0],
      );
      this.auiNotificationService.success(
        this.translate.get('org_logo_update_success'),
      );
      this.avatarPreviewing = false;
    } catch (e) {
    } finally {
      this.avatarSubmitting = false;
    }
  }

  async changeAccountName() {
    const modalRef = this.modal.open(UpdateCompanyComponent, {
      title: this.translate.get('update_company_name'),
      data: {
        formerCompanyName: this.org.company,
        orgName: this.org.name,
      },
    });

    modalRef.componentInstance.afterConfirmed
      .pipe(take(1))
      .subscribe((org: Org) => {
        this.org = org;
      });
  }

  copyText(input: HTMLInputElement) {
    input.select();
    document.execCommand('Copy');
    setTimeout(() => (this.tokenCopied = true));
  }

  get accountName() {
    if (this.user && this.user.realname) {
      return this.user.realname;
    } else if (this.username) {
      return this.username + '@' + this.account.namespace;
    } else {
      return this.account.namespace;
    }
  }
}
