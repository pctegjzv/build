import { Component, Inject, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount, RcProfile } from 'app2/core/types';
import { AccountService } from 'app2/shared/services/features/account.service';
import { OrgService } from 'app2/shared/services/features/org.service';
import { User } from 'app2/shared/types';

@Component({
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  loading: boolean;
  username: string;
  user: User;
  profile: RcProfile;
  token: string;

  constructor(
    @Inject(ACCOUNT) public account: RcAccount,
    private route: ActivatedRoute,
    private orgService: OrgService,
    private accountService: AccountService,
  ) {}

  ngOnInit() {
    this.route.queryParamMap.subscribe(queryParams => {
      const username = queryParams.get('username');
      this.username = username || this.account.username;
      this.loadUserInfo();
    });
  }

  async loadUserInfo() {
    this.loading = true;

    const promises = [
      this.accountService.getAuthProfile(),
      this.accountService.getUserToken(),
    ];

    if (this.username) {
      promises.push(this.orgService.getUser(this.username));
    }

    const data = await Promise.all(promises);

    this.loading = false;

    this.profile = data[0];
    this.token = data[1];

    if (!this.username) {
      return;
    }

    this.user = data[2];
  }
}
