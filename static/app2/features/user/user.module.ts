import { NgModule } from '@angular/core';
import { RbacUserSharedModule } from 'app2/features/lazy/rbac-user.shared.module';
import { UserSummaryComponent } from 'app2/features/user/user-summary/user-summary.component';
import { SharedModule } from 'app2/shared/shared.module';

import { AccountInfoOsChinaLinkComponent } from './account-info/account-info-os-china-link.component';
import { AccountInfoUpdateProfileComponent } from './account-info/account-info-update-profile.component';
import { AccountInfoComponent } from './account-info/account-info.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';

@NgModule({
  declarations: [
    UserComponent,
    UserSummaryComponent,
    UserInfoComponent,
    AccountInfoComponent,
    AccountInfoUpdateProfileComponent,
    AccountInfoOsChinaLinkComponent,
  ],
  imports: [SharedModule, RbacUserSharedModule, UserRoutingModule],
  entryComponents: [
    AccountInfoUpdateProfileComponent,
    AccountInfoOsChinaLinkComponent,
  ],
})
export class UserModule {}
