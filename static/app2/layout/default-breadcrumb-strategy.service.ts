import { Injectable } from '@angular/core';

import { RcRouterStateSnapshot } from '../core/services/router-util.service';

import {
  BreadcrumbPartial,
  LayoutBreadcrumbStrategy,
} from './layout-breadcrumb-strategy';
import { NavStateService } from './nav-list/nav-state.service';
import {
  NavItemViewModel,
  NavSubItemViewModel,
} from './nav-list/nav-view-model';

@Injectable()
export class DefaultBreadcrumbStrategy implements LayoutBreadcrumbStrategy {
  constructor(private navState: NavStateService) {}

  async renderBreadcrumbPartials(
    state: RcRouterStateSnapshot,
  ): Promise<BreadcrumbPartial[]> {
    await this.navState.ready;
    const navItem = this.navState.getItemByUrl(state.url);
    const partials = this.renderByActiveNameItem(navItem);

    return partials.filter(({ name }, index, arr) => {
      return !(
        index === arr.length - 1 && ['list', 'dashboard'].includes(name)
      );
    });
  }

  private renderByActiveNameItem(
    navItem: NavItemViewModel | NavSubItemViewModel,
  ): BreadcrumbPartial[] {
    if (!navItem.primaryItem) {
      return [];
    }
    const partials: BreadcrumbPartial[] = [
      {
        label: { name: navItem.primaryItem.displayName },
        name: navItem.primaryItem.name,
        href: navItem.primaryItem.href,
      },
    ];
    if (!navItem.isPrimary) {
      partials.push({
        label: { name: navItem.displayName },
        href: navItem.href,
        name: navItem.name,
      });
    }
    return partials;
  }
}
