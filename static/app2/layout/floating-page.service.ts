import { Injectable, TemplateRef } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

export interface FloatingPageOption {
  templateRef: TemplateRef<any>;

  title: string;

  /**
   * Whether or not to show a backdrop. Default false.
   */
  backdrop?: boolean;

  /**
   * Custom style applied to the floating page container
   */
  customStyle?: any;
}

@Injectable()
export class FloatingPageService {
  private optionSubject = new BehaviorSubject<FloatingPageOption>(undefined);
  private resultSubject = new Subject();

  constructor() {}

  get option$() {
    return this.optionSubject.asObservable();
  }

  get result$() {
    return this.resultSubject.asObservable();
  }

  getOption() {
    return this.optionSubject.getValue();
  }

  isShowing() {
    return !!this.optionSubject.getValue();
  }

  open(option: FloatingPageOption) {
    this.optionSubject.next(option);
  }

  close(val: any = {}) {
    this.optionSubject.next(null);
    this.resultSubject.next(val);
  }
}
