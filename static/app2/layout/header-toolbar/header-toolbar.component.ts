import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { WEBLABS } from 'app2/core/tokens';
import { Weblabs } from 'app2/core/types';
import { RegionBadgeOptionService } from 'app2/shared/directives/region-badge-option/region-badge-option.service';
import { debounceTime } from 'rxjs/operators';

import { LayoutStateService } from '../layout-state.service';

@Component({
  selector: 'rc-header-toolbar',
  templateUrl: 'header-toolbar.component.html',
  styleUrls: ['header-toolbar.component.scss'],
  animations: [
    trigger('scaleInOut', [
      state(
        '0',
        style({ transform: 'scale(0.8)', opacity: '0', display: 'none' }),
      ),
      state('1', style({ transform: 'scale(1)', opacity: '1' })),
      transition('0 => 1', [
        animate('.1s', style({ transform: 'scale(1.1)', opacity: '1' })),
        animate('.1s', style({ transform: 'scale(1)' })),
      ]),
      transition('1 => 0', animate('.1s')),
    ]),
  ],
})
export class HeaderToolbarComponent implements OnInit {
  projectsEnabled: boolean;
  userViewEnabled: boolean;
  shouldShowRegionBadge: boolean;

  constructor(
    public layoutState: LayoutStateService,
    private cdr: ChangeDetectorRef,
    private regionBadgeOption: RegionBadgeOptionService,
    @Inject(WEBLABS) private weblabs: Weblabs,
  ) {}

  ngOnInit() {
    this.projectsEnabled = this.weblabs.PROJECTS_ENABLED;
    this.userViewEnabled = this.weblabs.USER_VIEW_ENABLED;
    this.regionBadgeOption.regionBadgeOption$
      .pipe(debounceTime(100))
      .subscribe(option => {
        this.shouldShowRegionBadge = option && !option.hidden;
        this.cdr.detectChanges();
      });
  }
}
