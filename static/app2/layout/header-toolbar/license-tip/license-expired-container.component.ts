import { Component, Inject } from '@angular/core';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';

@Component({
  templateUrl: './license-expired-container.component.html',
  styleUrls: ['./license-expired-container.component.scss'],
})
export class LicenseExpiredModalComponent {
  productName: string;

  constructor(@Inject(MODAL_DATA) modalData: { productName: string }) {
    this.productName = modalData.productName;
  }

  normalConfirm() {
    location.href = '/ap/logout';
  }
}
