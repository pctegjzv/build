import { Component, Inject, OnInit } from '@angular/core';

import { ModalService } from 'app2/shared/services/modal/modal.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';

@Component({
  templateUrl: './license-tip-modal.component.html',
  styleUrls: ['./license-tip-modal.component.scss'],
})
export class LicenseTipModalComponent implements OnInit {
  productName: string;
  isAdmin = false;
  constructor(
    private modalService: ModalService,
    @Inject(MODAL_DATA) private modalData: any,
  ) {}

  ngOnInit() {
    this.isAdmin = this.modalData.isAdmin;
    this.productName = this.modalData.productName;
  }

  lgOut() {
    this.modalService.closeAll();
    location.href = '/ap/logout';
  }

  upLicense() {
    this.modalService.closeAll();
    location.replace(`/landing/license-activation`);
  }
}
