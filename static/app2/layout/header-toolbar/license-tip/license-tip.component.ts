import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { findKey } from 'lodash';
import { Observable } from 'rxjs';

import { NotificationService } from 'alauda-ui';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments, RcRegisterInfo } from 'app2/core/types';
import { LicenseInfoModal } from 'app2/features/license/license.type';
import { ConfirmBoxComponent } from 'app2/shared/components/confirm-box/confirm-box.component';
import { AccountService } from 'app2/shared/services/features/account.service';
import { LicenseService } from 'app2/shared/services/features/license.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import * as licenseActions from 'app2/state-store/license/actions';
import * as licenseReducers from 'app2/state-store/license/reducers';
import { AppState } from 'app2/state-store/reducers';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-license-tip',
  templateUrl: './license-tip.component.html',
  styleUrls: ['./license-tip.component.scss'],
})
export class LicenseTipComponent implements OnInit {
  tipClass: string;
  expirationDay: number;
  templateHtml: string;
  accountType: string;
  registeInfo: RcRegisterInfo;
  licenseState$: Observable<licenseReducers.State>;

  tipShowRuler: any = {
    beta: {
      warning: (expirationDay: number): boolean => {
        return expirationDay > 3 && expirationDay <= 7;
      },
      error: (expirationDay: number): boolean => {
        return expirationDay <= 3;
      },
    },
    release: {
      superUser: {
        warning: (expirationDay: number): boolean => {
          return expirationDay > 7 && expirationDay <= 30;
        },
        error: (expirationDay: number): boolean => {
          return expirationDay <= 7;
        },
      },
      normal: {
        warning: (expirationDay: number): boolean => {
          return expirationDay > 3 && expirationDay <= 7;
        },
        error: (expirationDay: number): boolean => {
          return expirationDay <= 3;
        },
      },
    },
  };

  initModalRuler() {
    const self = this;
    return {
      beta: {
        superUser: {
          matchRule(expirationDay: number) {
            return expirationDay <= 3;
          },
          title(expirationDay: number) {
            return expirationDay > 0
              ? `${self.translateService.get(
                  'beta_day_expire',
                )} ${expirationDay} ${self.translateService.get('day')}`
              : `${self.translateService.get('license_is_outdata')}`;
          },
          content(expirationDay: number) {
            return expirationDay > 0
              ? `<div class='content'>
              ${self.translateService.get('license_info_6_1')}${
                  self.registeInfo.product_name
                }
              ${self.translateService.get('license_info_6_2')}</div>`
              : `<div class='content'>
            ${self.registeInfo.product_name}${self.translateService.get(
                  'license_info_7_2',
                )}
            </div>`;
          },
          cancelText(expirationDay: number) {
            return expirationDay > 0 && expirationDay <= 3
              ? self.translateService.get('cancel')
              : self.translateService.get('logout');
          },
          confirmText() {
            return self.translateService.get('to_update_license');
          },
          confirmeFun: (expirationDay: number) => {
            expirationDay > 0
              ? self.router.navigate(['/license'])
              : location.replace(`/landing/license-activation`);
          },
          afterClosedFun(expirationDay: number) {
            if (expirationDay <= 0) {
              location.href = '/ap/logout';
            }
          },
        },
      },
      release: {
        superUser: {
          matchRule(expirationDay: number) {
            return expirationDay <= 7;
          },
          title(expirationDay: number) {
            return expirationDay > 0
              ? `${self.translateService.get('license_expire')}${expirationDay}
            ${self.translateService.get('day')}`
              : `${self.translateService.get('license_is_outdata')}`;
          },
          content(expirationDay: number) {
            return expirationDay > 0
              ? `<div class='body'>
            ${self.translateService.get('license_info_6_3')}${
                  self.registeInfo.product_name
                }${self.translateService.get('license_info_6_2')}
            </div>`
              : `<div class='content'>
            ${self.registeInfo.product_name}${self.translateService.get(
                  'license_info_7_2',
                )}
            </div>`;
          },
          cancelText(expirationDay: number) {
            return expirationDay > 0 && expirationDay <= 7
              ? self.translateService.get('cancel')
              : self.translateService.get('logout');
          },
          confirmText() {
            return self.translateService.get('to_update_license');
          },
          confirmeFun() {
            self.expirationDay > 0
              ? self.router.navigate(['/license'])
              : location.replace(`/landing/license-activation`);
          },
          afterClosedFun(expirationDay: number) {
            if (expirationDay <= 0) {
              location.href = '/ap/logout';
            }
          },
        },
      },
    };
  }

  constructor(
    private router: Router,
    private auiNotificationService: NotificationService,
    private licenseService: LicenseService,
    private accountService: AccountService,
    private modalService: ModalService,
    private translateService: TranslateService,
    private store: Store<AppState>,
    @Inject(ENVIRONMENTS) public environments: Environments,
  ) {}

  async ngOnInit() {
    try {
      //licnese only appear in private deploy
      if (
        !(
          this.environments.is_private_deploy_enabled &&
          this.environments.license_required
        )
      ) {
        return;
      }

      const [licenseInfo, profile, registeInfo] = await Promise.all([
        this.licenseService.getLicenseInfo(),
        this.accountService.getAuthProfile(true),
        this.licenseService.getRegisterInfo(),
      ]);
      this.registeInfo = registeInfo;
      this.accountType = ['superUser', 'normal'][+!profile['is_admin']];
      this.expirationDay = licenseInfo.days_until_expiration;
      this.updateNewestTipClass(licenseInfo);

      //license store operation
      this.store.dispatch(new licenseActions.UpdateByKeys(licenseInfo));
      this.licenseState$ = this.store.select(licenseReducers.selectFeature);
      this.licenseState$.subscribe(license => {
        this.updateNewestTipClass(license);
      });

      //fromeLogin === 'true' ,only form login can tip modal
      if (localStorage.getItem('fromLogin') !== 'true') {
        return;
      }
      localStorage.setItem('fromLogin', 'false');
      const modalRuler = this.initModalRuler()[licenseInfo.kind][
        this.accountType
      ];

      if (modalRuler.matchRule(this.expirationDay)) {
        await this.openConfirmBox(modalRuler);
      }
    } catch ({ errors }) {
      if (errors) {
        this.auiNotificationService.error({
          content: this.translateService.get(errors[0].message),
        });
      }
    }
  }

  updateNewestTipClass(license: LicenseInfoModal) {
    const tipSubRuler = this.tipShowRuler[license.kind];
    if (tipSubRuler) {
      const tipRuler = tipSubRuler.hasOwnProperty(this.accountType)
        ? tipSubRuler[this.accountType]
        : tipSubRuler;
      this.tipClass = findKey(tipRuler, (rulerFun: Function) =>
        rulerFun(license.days_until_expiration),
      );
    }
  }

  async openConfirmBox(ruler: any) {
    const modalRef = this.modalService.open(ConfirmBoxComponent, {
      closable: true,
      title: `<div class="license-modal-box-header">
        ${this.translateService.get(ruler.title(this.expirationDay))}
        </div>`,
    });

    const commonFoot = `<div class='footer'>
    <div>
    ${this.translateService.get('license_info_1_1')} ${
      this.registeInfo.product_name
    } ${this.translateService.get('license_info_1_2')}
    </div>
    <div>${this.translateService.get(
      'email',
    )}：<a class="mail-link" href="mailto:marketing@alauda.io">marketing@alauda.io</a></div>
    <div>${this.translateService.get('call')}：4006-252-832</div>
    </div>`;

    modalRef.componentInstance = Object.assign(modalRef.componentInstance, {
      content: `
        <div class='license-modal-box' id="license-expired-modal-mask">
          ${ruler.content(this.expirationDay)}${commonFoot}
        </div>
      `,
      confirmText: ruler.confirmText && ruler.confirmText(this.expirationDay),
      cancelText: ruler.cancelText && ruler.cancelText(this.expirationDay),
    });

    modalRef.componentInstance.confirmed.subscribe((res: boolean) => {
      if (res && ruler.confirmeFun) {
        ruler.confirmeFun(this.expirationDay);
      }
      modalRef.close(res);
    });
    return modalRef.afterClosed.toPromise().then(() => {
      if (ruler.afterClosedFun) {
        ruler.afterClosedFun(this.expirationDay);
      }
    });
  }
}
