import { Component, Inject, OnInit } from '@angular/core';
import { DialogService } from 'alauda-ui';

import { ACCOUNT, WEBLABS } from 'app2/core/tokens';
import { RcAccount, RcProfile, Weblabs } from 'app2/core/types';
import { AccountService } from 'app2/shared/services/features/account.service';
import { OrgService } from 'app2/shared/services/features/org.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { User } from 'app2/shared/types';
import { TranslateService } from 'app2/translate/translate.service';
import { setCookie } from 'app2/utils/cookie';

@Component({
  selector: 'rc-profile-dropdown',
  templateUrl: 'profile-dropdown.component.html',
  styleUrls: ['profile-dropdown.component.scss'],
})
export class ProfileDropdownComponent implements OnInit {
  public profile: RcProfile;
  public initialized: boolean;
  public user: User;

  constructor(
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(WEBLABS) public weblabs: Weblabs,
    public translate: TranslateService,
    private modal: ModalService,
    private accountService: AccountService,
    private orgService: OrgService,
    private dialogService: DialogService,
    private errorsToast: ErrorsToastService,
  ) {}

  async ngOnInit() {
    try {
      [this.user, this.profile] = await Promise.all([
        this.orgService.getUser(),
        this.accountService.getAuthProfile(),
      ]);
    } catch (e) {
      this.errorsToast.error(e);
    }

    this.initialized = true;
  }

  async switchToUserView() {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('change_to_user_view_confirm'),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
      location.href = '/user';
    } catch (e) {}
  }

  async logout() {
    try {
      await this.modal.confirm({
        title: this.translate.get('logout_confirm'),
      });
      if (this.weblabs.USER_VIEW_ENABLED) {
        setCookie('project', '');
      }
      location.href = '/ap/logout';
    } catch (e) {}
  }
}
