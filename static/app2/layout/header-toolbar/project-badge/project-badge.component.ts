import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Project } from 'app2/shared/services/features/project.service';
import { sortBy } from 'lodash';
import {
  BehaviorSubject,
  Observable,
  Subject,
  combineLatest,
  interval,
} from 'rxjs';
import {
  debounceTime,
  filter,
  map,
  publishReplay,
  refCount,
  startWith,
  takeUntil,
} from 'rxjs/operators';

import { Store } from '@ngrx/store';
import { TooltipDirective } from 'alauda-ui';
import * as projectActions from 'app2/state-container/core/actions/project';
import { State } from 'app2/state-container/core/reducers';
import * as projectSelectors from 'app2/state-container/core/selectors/project.ts';

@Component({
  selector: 'rc-project-badge',
  templateUrl: 'project-badge.component.html',
  styleUrls: ['project-badge.component.scss'],
})
export class ProjectBadgeComponent implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<void>();
  protected projects$: Observable<Project[]>;
  protected filteredProjects$: Observable<Project[]>;
  protected projectsLength$: Observable<number>;
  protected selectedProject: Project;

  @ViewChild('projectListTooltipEl', { read: TooltipDirective })
  projectListTooltipInstance: TooltipDirective;
  active: boolean;
  filterName = '';
  filterName$: BehaviorSubject<string>;

  constructor(private router: Router, private store: Store<State>) {}

  ngOnInit() {
    this.filterName$ = new BehaviorSubject<string>(this.filterName);
    interval(30000)
      .pipe(
        startWith(null),
        takeUntil(this.onDestroy$),
      )
      .subscribe(() => {
        this.store.dispatch(new projectActions.LoadProjects());
      });

    this.projects$ = this.store.select(projectSelectors.getAllProjects).pipe(
      map((projects: Project[]) => {
        return sortBy(projects, project => project.name);
      }),
    );
    this.projectsLength$ = this.projects$.pipe(
      map(projects => projects.length),
      publishReplay(1),
      refCount(),
    );

    this.filteredProjects$ = combineLatest(
      this.filterName$,
      this.projects$,
    ).pipe(
      debounceTime(100),
      map(([name, projects]) => {
        name = name.trim();
        return name
          ? projects.filter(project => {
              const projectName = project.display_name || project.name;
              return projectName.includes(name);
            })
          : projects;
      }),
      publishReplay(1),
      refCount(),
    );

    combineLatest(
      this.projects$.pipe(filter(projects => !!projects.length)),
      this.store.select(projectSelectors.getSelectedProjectName),
    )
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(([projects, name]) => {
        let project: Project;
        project = projects.find(project => name === project.name);
        if (!project) {
          project = projects[0];
        }
        this.selectedProject = project;
        if (name !== project.name) {
          this.store.dispatch(new projectActions.SetProject(project.name));
        }
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  selectProject(project: Project) {
    this.projectListTooltipInstance.disposeTooltip();
    this.store.dispatch(new projectActions.SetProject(project.name));
    this.router.navigate(['/']);
  }

  projectDetail() {
    if (this.selectedProject) {
      this.router.navigate(['/projects/detail/', this.selectedProject.name]);
    }
  }

  trackByProjectId(_index: number, project: Project) {
    return project.uuid;
  }
}
