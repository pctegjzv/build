import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription, combineLatest } from 'rxjs';
import { debounceTime, filter, map, pluck, switchMap } from 'rxjs/operators';

import { NavStateService } from 'app2/layout/nav-list/nav-state.service';
import {
  NavItemViewModel,
  NavSubItemViewModel,
} from 'app2/layout/nav-list/nav-view-model';
import {
  RegionBadgeOption,
  RegionBadgeOptionService,
} from 'app2/shared/directives/region-badge-option/region-badge-option.service';
import { TooltipDirective } from 'app2/shared/directives/tooltip/tooltip.directive';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { LoggerUtilitiesService } from 'app2/shared/services/utility/logger.service';

@Component({
  selector: 'rc-region-badge',
  templateUrl: 'region-badge.component.html',
  styleUrls: ['region-badge.component.scss'],
  animations: [
    trigger('scaleInOut', [
      state(
        'void',
        style({
          transform: 'scale(0.8)',
          opacity: '0',
          display: 'none',
        }),
      ),
      state(
        '*',
        style({
          transform: 'scale(1)',
          opacity: '1',
        }),
      ),
      transition(':enter', [
        animate('.1s', style({ transform: 'scale(1.1)', opacity: '1' })),
        animate('.1s', style({ transform: 'scale(1)' })),
      ]),
      transition(':leave', animate('.1s')),
    ]),
  ],
})
export class RegionBadgeComponent implements OnInit, AfterViewInit, OnDestroy {
  protected regionDisplayName$: Observable<string>;
  protected selectedRegionName: string;
  protected regions$: Observable<Region[]>;
  protected regionLength$: Observable<number>;
  private subscription: Subscription;
  private navSubscription: Subscription;
  private option: RegionBadgeOption;
  noOnChangeHint = false;

  @ViewChild('regionListTooltipEl', { read: TooltipDirective })
  regionListTooltipInstance: TooltipDirective;

  constructor(
    protected cdr: ChangeDetectorRef,
    protected regionService: RegionService,
    protected regionBadgeOption: RegionBadgeOptionService,
    protected logger: LoggerUtilitiesService,
    protected router: Router,
    private navState: NavStateService,
  ) {}

  async ngOnInit() {
    this.regionService.refetch();

    const regionBadgeOption$ = this.regionBadgeOption.regionBadgeOption$.pipe(
      debounceTime(100),
    );

    this.regionDisplayName$ = this.regionService.region$.pipe(
      filter(region => !!region),
      pluck('display_name'),
    );

    this.regions$ = regionBadgeOption$.pipe(
      switchMap(option =>
        this.regionService.regions$.pipe(
          filter(regions => !!regions),
          map(regions =>
            regions.filter(region => option.filterFunction(region)),
          ),
        ),
      ),
    );

    this.subscription = regionBadgeOption$.subscribe(option => {
      this.option = option;

      // If no onChange action is provided,
      // we believe the current page is not region related
      this.noOnChangeHint = !option.onChange;
      this.cdr.detectChanges();
    });

    this.regionLength$ = this.regions$.pipe(map(regions => regions.length));

    await this.navState.ready;
    this.navSubscription = this.navState.activeNavItem$.subscribe(
      (item: NavItemViewModel | NavSubItemViewModel) => {
        // will be called before setRegionBadgeOptionByDirective
        this.regionBadgeOption.setRegionBadgeOptionByK8sFlag(item);
      },
    );
  }

  ngAfterViewInit() {
    this.regionService.region$.subscribe(
      region => {
        this.selectedRegionName = region ? region.name : '';
        this.cdr.detectChanges();
      },
      err => {
        this.logger.error(
          'Error thrown when setting to a new region, but how ???',
          err,
        );
      },
    );

    combineLatest(this.regionService.regionName$, this.regions$)
      .pipe(
        debounceTime(200),
        filter(
          ([regionName, regions]) =>
            regions &&
            regions.length > 0 &&
            !regions.some(_region => regionName === _region.name),
        ),
        pluck('1'),
      )
      .subscribe(
        (regions: { name: string }[]) => {
          const newRegionName = regions[0].name;
          this.regionService.setRegionByName(newRegionName);
          this.cdr.detectChanges();
        },
        err => {
          this.logger.error(
            'Error thrown when switching to a new region automatically, but how ???',
            err,
          );
        },
      );

    // Poll region data
    setInterval(() => {
      this.regionService.refetch();
    }, 30000);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.navSubscription.unsubscribe();
  }

  async onRegionClicked(region: Region) {
    this.regionListTooltipInstance.hideTooltip();
    const defaultOnChange = () => true;
    const onChange = this.option.onChange || defaultOnChange;
    try {
      if (await onChange(region)) {
        this.regionService.setRegionByName(region.name);
      }
    } catch (err) {
      this.logger.error(
        'Error thrown when switching to a new region, but how ???',
        err,
      );
    }
  }

  goToRegionDetail() {
    if (this.selectedRegionName) {
      this.router.navigate(['/cluster/detail', this.selectedRegionName]);
    }
  }

  trackByRegionName(_: number, region: Region) {
    return region.name;
  }
}
