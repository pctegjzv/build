import { RcRouterStateSnapshot } from 'app2/core/services/router-util.service';

export interface BreadcrumbPartial {
  href?: string;
  label: { name: string; params?: { [key: string]: string } };
  name: string;
}

export interface LayoutBreadcrumbStrategy {
  /**
   * For a given state, returns the default breadcrumb partials to be rendered.
   * @param state
   */
  renderBreadcrumbPartials(
    state: RcRouterStateSnapshot,
  ): Promise<BreadcrumbPartial[]>;
}
