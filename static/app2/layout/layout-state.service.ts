import { Inject, Injectable } from '@angular/core';
import { Observable, Subject, Subscription, fromEvent } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
  startWith,
} from 'rxjs/operators';

import { ENVIRONMENTS } from '../core/tokens';
import { Environments } from '../core/types';

/**
 * State service for fundamental layout.
 */
@Injectable()
export class LayoutStateService {
  static COLLAPSED_THRESHOLD = 1160;

  // Indicates the collapsed state stream of layout's left nav
  navCollapsed: Observable<boolean>;

  // Current state state of layout's left nav
  navCollapseState = false;

  // Window resize events
  windowResized: Observable<number>;

  // Default logo src. Can be overridden by overridden_logo_sources
  logoMainSrc = '/static/images/logo/logo-main.svg';
  logoMainMiniSrc = '/static/images/logo/logo-main-mini.svg';

  private toggleNavCollapseSubject: Subject<any> = new Subject();
  private windowResizeSub: Subscription;

  constructor(@Inject(ENVIRONMENTS) private environments: Environments) {
    this.windowResized = fromEvent(window, 'resize').pipe(
      debounceTime(30),
      startWith(null),
      map(() => document.body.offsetWidth),
    );

    // Nav collapse state should be calculated based on two events:
    //  - user toggle the nav actively
    //  - when the window size changed
    this.navCollapsed = this.toggleNavCollapseSubject.asObservable().pipe(
      publishReplay(1),
      refCount(),
    );

    this.windowResizeSub = this.windowResized
      .pipe(distinctUntilChanged())
      .subscribe(size =>
        this.setToggleNavCollapse(
          size < LayoutStateService.COLLAPSED_THRESHOLD,
        ),
      );

    // Override logo src if there is an environment variable been set.
    const logoSrcMap =
      this.environments.overridden_logo_sources &&
      this.environments.overridden_logo_sources
        .split(',')
        .reduce((accum, logoSrcEnv) => {
          const splitIndex = logoSrcEnv.indexOf(':');
          const key = logoSrcEnv.substr(0, splitIndex);
          accum[key] = logoSrcEnv.substr(splitIndex + 1);
          return accum;
        }, {});

    if (logoSrcMap) {
      this.logoMainSrc = logoSrcMap['logo-main'] || this.logoMainSrc;
      this.logoMainMiniSrc =
        logoSrcMap['logo-main-mini'] || this.logoMainMiniSrc;
    }
  }

  toggleNavCollapse(): boolean {
    this.windowResizeSub.unsubscribe();
    return this.setToggleNavCollapse(!this.navCollapseState);
  }

  setToggleNavCollapse(navCollapseState: boolean): boolean {
    this.navCollapseState = navCollapseState;
    this.toggleNavCollapseSubject.next(this.navCollapseState);
    return this.navCollapseState;
  }
}
