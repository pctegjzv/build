import { Location } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { RouterUtilService } from 'app2/core/services/router-util.service';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { NavStateService } from 'app2/layout/nav-list/nav-state.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { TranslateService } from 'app2/translate/translate.service';
import { debounce, get } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { LayoutStateService } from './layout-state.service';

@Component({
  selector: 'rc-layout',
  templateUrl: 'layout.component.html',
  styleUrls: ['layout.component.scss'],
})
export class LayoutComponent implements OnInit, OnDestroy {
  routerStateName: Observable<string>;
  titleText = '';
  mouseMoveHandler: any;
  timer: number;

  constructor(
    private location: Location,
    private router: Router,
    public layoutState: LayoutStateService,
    private translate: TranslateService,
    private navState: NavStateService,
    private title: Title,
    private regionService: RegionService,
    private auiNotificationService: NotificationService,
    routerUtil: RouterUtilService,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {
    this.routerStateName = routerUtil.routerState.pipe(
      map(state => state.name),
    );

    routerUtil.routerState.subscribe(async state => {
      await this.navState.ready;
      const navItem = this.navState.getItemByUrl(state.url);
      if (navItem) {
        this.titleText = navItem.primaryItem.displayName;
      } else {
        this.titleText = state.name.split('.')[0];
      }
      this.title.setTitle(this.translate.get(this.titleText));
    });

    this.translate.currentLang$.subscribe(() => {
      if (this.titleText) {
        this.title.setTitle(this.translate.get(this.titleText));
      }
    });
  }

  private linkHandler = async (e: MouseEvent) => {
    const el = this.getTargetLinkElement(e.target as Node);
    if (el) {
      const href = el.getAttribute('href');
      if (href === 'javascript:openGrafanaTab()') {
        const region = await this.regionService.getCurrentRegion();
        const grafanaUrl = get(region, 'features.customized.metric.grafana');
        if (grafanaUrl) {
          window.open(grafanaUrl);
        } else {
          this.auiNotificationService.error(
            this.translate.get('grafana_not_found'),
          );
        }
      }
      if (!href || href === '#' || href.startsWith('javascript:')) {
        e.preventDefault();
        return false;
      }
      const { pathname } = el;
      if (pathname && pathname.startsWith('/console/')) {
        e.preventDefault();
        this.router.navigateByUrl(
          this.location.normalize(pathname) + el.search,
        );
        return false;
      }
    }
  };

  private _mouseMoveHandler() {
    this.resetTimer();
  }

  private logout() {
    location.href = '/ap/logout';
  }

  private resetTimer() {
    window.clearTimeout(this.timer);
    const latencyMinutes = this.env.auto_logout_latency;
    this.timer = window.setTimeout(this.logout, latencyMinutes * 60 * 1000);
  }

  private getTargetLinkElement(el: Node): HTMLAnchorElement {
    while (el) {
      if (el.nodeName === 'A') {
        return el as HTMLAnchorElement;
      }
      el = el.parentNode;
    }
    return null;
  }

  ngOnInit() {
    this.mouseMoveHandler = debounce(() => {
      this._mouseMoveHandler();
    }, 200);
    document.addEventListener('click', this.linkHandler);
    document.addEventListener('mousemove', this.mouseMoveHandler);
    this.resetTimer();
  }

  ngOnDestroy() {
    document.removeEventListener('click', this.linkHandler);
    document.removeEventListener('mousemove', this.mouseMoveHandler);
  }
}
