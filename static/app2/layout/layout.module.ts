import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FloatingPageService } from 'app2/layout/floating-page.service';
import { LicenseExpiredModalComponent } from 'app2/layout/header-toolbar/license-tip/license-expired-container.component';
import { LicenseTipModalComponent } from 'app2/layout/header-toolbar/license-tip/license-tip-modal.component';
import { LicenseTipComponent } from 'app2/layout/header-toolbar/license-tip/license-tip.component';
import { RegionBadgeComponent } from 'app2/layout/header-toolbar/region-badge/region-badge.component';
import { FloatingPageComponent } from 'app2/layout/page-container/floating-page.component';
import { SidePanelComponent } from 'app2/layout/page-container/side-panel.component';
import { SidePanelService } from 'app2/layout/side-panel.service';
import { PageHeaderContentService } from 'app2/shared/directives/page-header-content/page-header-content.service';
import { RegionBadgeOptionService } from 'app2/shared/directives/region-badge-option/region-badge-option.service';

import { SharedModule } from '../shared/shared.module';

import { DefaultBreadcrumbStrategy } from './default-breadcrumb-strategy.service';
import { HeaderToolbarComponent } from './header-toolbar/header-toolbar.component';
import { ProfileDropdownComponent } from './header-toolbar/profile-dropdown/profile-dropdown.component';
import { ProjectBadgeComponent } from './header-toolbar/project-badge/project-badge.component';
import { LayoutStateService } from './layout-state.service';
import { LayoutComponent } from './layout.component';
import { NavListCategoryComponent } from './nav-list/category/nav-list-category.component';
import { NavListItemComponent } from './nav-list/item/nav-list-item.component';
import { NavListComponent } from './nav-list/nav-list.component';
import { NavStateService } from './nav-list/nav-state.service';
import { PageContainerComponent } from './page-container/page-container.component';

/**
 * For fundamental page Layout.
 */
@NgModule({
  providers: [
    NavStateService,
    LayoutStateService,
    SidePanelService,
    FloatingPageService,
    DefaultBreadcrumbStrategy,
    PageHeaderContentService,
    RegionBadgeOptionService,
  ],
  imports: [RouterModule, ScrollDispatchModule, SharedModule],
  exports: [LayoutComponent, LicenseExpiredModalComponent],
  declarations: [
    LayoutComponent,
    HeaderToolbarComponent,
    NavListComponent,
    NavListCategoryComponent,
    NavListItemComponent,
    PageContainerComponent,
    SidePanelComponent,
    FloatingPageComponent,
    ProfileDropdownComponent,
    ProjectBadgeComponent,
    RegionBadgeComponent,
    LicenseTipComponent,
    LicenseExpiredModalComponent,
    LicenseTipModalComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [LicenseExpiredModalComponent, LicenseTipModalComponent],
})
export class LayoutModule {}
