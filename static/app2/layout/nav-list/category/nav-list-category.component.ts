import { animate, state, style, transition } from '@angular/animations';
import { trigger } from '@angular/animations';
import {
  Component,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';

import { NavCategoryViewModel, NavItemViewModel } from '../nav-view-model';

@Component({
  selector: 'rc-nav-list-category',
  templateUrl: 'nav-list-category.component.html',
  styleUrls: ['nav-list-category.component.scss'],
  animations: [
    trigger('item', [
      state('0', style({ display: 'none', opacity: '0' })),
      state('1', style({ opacity: '1' })),
      transition('0 => 1', [animate('1s ease')]),
      transition('1 => 0', [animate('1s ease')]),
    ]),
  ],
})
export class NavListCategoryComponent implements OnInit, OnDestroy {
  @Input()
  navCategory: NavCategoryViewModel;
  /**
   * Should hide myself
   * @type {boolean}
   */
  @HostBinding('hidden')
  hidden = false;

  /**
   * Hidden flags for child items
   */
  hiddenFlags: { [navItemName: string]: boolean };
  // inputCategory: NavCategoryViewModel;

  ngOnInit() {
    // Hide all by default
    this.hiddenFlags = this.navCategory.children.reduce(
      (accum, item) => ({ ...accum, [item.name]: false }),
      {},
    );

    // this.inputCategory = Object.assign({}, this.navCategory);
  }

  ngOnDestroy(): void {}

  constructor() {}

  trackByIndex(_index: number, item: NavItemViewModel) {
    return item.index;
  }

  /**
   * We should hide the category if no items is listed in the category.
   * @param {boolean} hidden
   * @param {NavItemViewModel} navItem
   */
  onItemHide(hidden: boolean, navItem: NavItemViewModel) {
    this.hiddenFlags[navItem.name] = hidden;
    this.hidden =
      Object.keys(this.hiddenFlags).length ===
        this.navCategory.children.length &&
      Object.values(this.hiddenFlags).every(flag => !!flag);
  }
}
