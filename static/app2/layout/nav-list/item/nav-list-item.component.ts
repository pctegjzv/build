import {
  Component,
  EventEmitter,
  HostBinding,
  Inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChange,
} from '@angular/core';
import { Subscription } from 'rxjs';

import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { TranslateService } from 'app2/translate/translate.service';

import { NavStateService } from '../nav-state.service';
import {
  BaseNavItemViewModel,
  NavItemViewModel,
  NavSubItemViewModel,
} from '../nav-view-model';

/**
 * NavListItemComponent has two main section, one for the primary item and one
 * for the secondary item list.
 *
 * The primary item will be used as a expand toggle when there is a secondary item list;
 * if there is no secondary item list, it MUST has an associated HREF (or language picker).
 */
@Component({
  selector: 'rc-nav-list-item',
  templateUrl: 'nav-list-item.component.html',
  styleUrls: ['nav-list-item.component.scss'],
})
export class NavListItemComponent implements OnInit, OnDestroy, OnChanges {
  @Input()
  navItem: NavItemViewModel;

  @Output()
  hide = new EventEmitter();

  /**
   * To show active indicator
   * @returns {boolean}
   */
  @HostBinding('class.active')
  get active() {
    return this.navItem.active;
  }

  /**
   * Should expand the sub items
   * @type {boolean}
   */
  @HostBinding('class.expanded')
  get expanded() {
    return this.navItem.expanded;
  }

  /**
   * Side effect to show native browser tooltip.
   * @returns {string}
   */
  @HostBinding('title')
  get title() {
    return this.translate.get(this.navItem.displayName);
  }

  /**
   * Give a name for testing friendly.
   * @returns {string}
   */
  @HostBinding('attr.name')
  get name() {
    return this.navItem.name;
  }

  /**
   * The expandable flag.
   * @returns {boolean}
   */
  @HostBinding('class.expandable')
  get expandable(): boolean {
    return this.navItem.expandable && this.children.length > 0;
  }

  children: NavSubItemViewModel[] = [];

  private expandableSubscriber: Subscription;
  private activeNavItemSubscriber: Subscription;
  private destroyed = false;

  constructor(
    private navState: NavStateService,
    private translate: TranslateService,
    @Inject(ENVIRONMENTS) public environments: Environments,
  ) {}

  async ngOnInit() {
    this.initExpandSubscriptions();
    if (this.navItem.isLanguagePicker) {
      this.navItem.tooltip = 'switch_lang_hint';
    }
  }

  async ngOnChanges({ navItem }: { navItem: SimpleChange }) {
    if (navItem && navItem.currentValue) {
      await this.navState.ready;
      if (this.destroyed) {
        return;
      }
      this.children = this.navItem.children;
    }
  }

  ngOnDestroy(): void {
    this.destroyed = true;
    if (this.expandableSubscriber) {
      this.expandableSubscriber.unsubscribe();
    }

    if (this.activeNavItemSubscriber) {
      this.activeNavItemSubscriber.unsubscribe();
    }
  }

  /**
   * Handle primary item click event.
   * @param {Event} event
   */
  primaryClicked(event: Event) {
    if (!this.navItem.href || this.navItem.disabled) {
      // disable a[href]
      event.preventDefault();
      event.stopPropagation();
    }

    if (this.expandable) {
      // Event will be handled later
      this.navState.navItemExpanded$.next(this.navItem);
    }

    if (this.navItem.isLanguagePicker) {
      this.translate.changeLanguage();
    }
  }

  subClicked(event: Event, subItem: NavSubItemViewModel) {
    if (subItem.disabled) {
      event.preventDefault();
      event.stopPropagation();
    }
  }

  get expandIcon() {
    return this.expanded ? 'fa fa-minus' : 'fa fa-plus';
  }

  getLinkElementTarget(item: BaseNavItemViewModel) {
    return item.isExternalHref ? '_blank' : undefined;
  }

  trackByItem(_index: number, item: BaseNavItemViewModel) {
    return item.index;
  }

  private get expandMode() {
    return this.navState.navViewModel.expandMode;
  }

  private initExpandSubscriptions() {
    this.expandableSubscriber = this.navState.navItemExpanded$.subscribe(
      item => {
        if (this.expandable) {
          if (item.index === this.navItem.index) {
            // Same item, toggle expanded
            this.navItem.expanded = !this.navItem.expanded;
          } else if (!this.active && this.expandMode === 'SINGLE') {
            // For SINGLE mode, the item should
            this.navItem.expanded = false;
          }
        } else {
          if (this.expandMode === 'NO_COLLAPSE') {
            // If NO_COLLAPSE, expand the item when
            // - it is not disabled
            // - has children
            this.navItem.expanded =
              !this.navItem.disabled && this.navItem.hasChildren;
          }
        }
      },
    );

    // We should collapse the item when some other item becomes active for SINGLE mode
    this.activeNavItemSubscriber = this.navState.activeNavItem$.subscribe(
      activeNavItem => {
        if (this.expandMode === 'SINGLE' && this.expandable) {
          const activePrimaryItem = activeNavItem && activeNavItem.primaryItem;
          if (
            !activePrimaryItem ||
            activePrimaryItem.index !== this.navItem.index
          ) {
            this.navItem.expanded = false;
          }
        }
      },
    );
  }
}
