import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NavListItemComponent } from 'app2/layout/nav-list/item/nav-list-item.component';
import { BehaviorSubject, of } from 'rxjs';

import { APP_BASE_HREF } from '@angular/common';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { setGlobal } from 'app2/app-global';
import { RouterUtilService } from 'app2/core/services/router-util.service';
import { ACCOUNT, ENVIRONMENTS, NAV_CONFIG, WEBLABS } from 'app2/core/tokens';
import { LayoutStateService } from 'app2/layout/layout-state.service';
import { NavListCategoryComponent } from 'app2/layout/nav-list/category/nav-list-category.component';
import { NavListComponent } from 'app2/layout/nav-list/nav-list.component';
import { NavStateService } from 'app2/layout/nav-list/nav-state.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { SharedModule } from 'app2/shared/shared.module';
import { translateService } from 'app2/testing/fake-translate.service';
import { delay, registerCleanup } from 'app2/testing/helpers';
import DEFAULT_NAV_CONFIG from 'app2/testing/testing-nav-config';
import { TranslateService } from 'app2/translate/translate.service';

const hrefFunction = (stateName: string) => stateName.split('.').join('/');

const mocked$Injector = {
  get($token: string) {
    return 'FETCHED_' + $token;
  },
};

const fake$routerState = {
  href: (stateName: string) => Promise.resolve(hrefFunction(stateName)),
  routerState: new BehaviorSubject({
    name: 'pipeline.config.list',
    params: {},
    url: 'pipeline/config',
    config: {},
    data: {},
  }),
};

const TEST_BASE = '/TEST_BASE/';

describe('NavListComponent', () => {
  let fixture: ComponentFixture<NavListComponent>;
  const environments: any = {};
  const weblabs: any = {};
  setGlobal('NGJS_READY_PROMISE', Promise.resolve());

  function getNavItemByName(name: string) {
    return fixture.debugElement.query(
      By.css(`rc-nav-list-item[name='${name}']`),
    );
  }

  function getPrimaryNavItemByName(name: string) {
    return fixture.debugElement.query(
      By.css(`.primary-nav-item[name='${name}']`),
    );
  }

  function getSubNavItemByName(name: string) {
    return fixture.debugElement.query(By.css(`.sub-nav-item[name='${name}']`));
  }

  function checkDebugItemWithClass(element: DebugElement, className: string) {
    return element.nativeElement.classList.contains(className);
  }

  beforeEach(async done => {
    const testBed = TestBed.configureTestingModule({
      declarations: [
        NavListComponent,
        NavListItemComponent,
        NavListCategoryComponent,
      ],

      imports: [SharedModule, RouterModule.forRoot([])],

      providers: [
        // Mock dependencies:
        {
          provide: APP_BASE_HREF,
          useValue: TEST_BASE,
        },
        NavStateService,
        LayoutStateService,
        {
          provide: RouterUtilService,
          useValue: fake$routerState,
        },
        {
          provide: TranslateService,
          useValue: translateService,
        },
        {
          provide: NAV_CONFIG,
          useValue: DEFAULT_NAV_CONFIG,
        },
        {
          provide: WEBLABS,
          useFactory: () => weblabs,
        },
        {
          provide: ENVIRONMENTS,
          useFactory: () => environments,
        },
        {
          provide: '$injector',
          useValue: mocked$Injector,
        },
        {
          provide: ACCOUNT,
          useValue: {},
        },
      ],
    }).overrideComponent(NavListItemComponent, {
      set: {
        providers: [
          {
            provide: RegionService,
            useValue: {
              region$: of({
                platform_version: 'v3',
                container_manager: true,
              }),
            },
          },
        ],
      },
    });
    await testBed.compileComponents();

    fixture = TestBed.createComponent(NavListComponent);
    fixture.detectChanges();
    await delay(200);

    fixture.detectChanges();

    done();
  });

  it('can init NavListComponent correctly', () => {
    const pipelineItem = getNavItemByName('pipeline');
    expect(checkDebugItemWithClass(pipelineItem, 'active')).toBeTruthy();
    expect(checkDebugItemWithClass(pipelineItem, 'expanded')).toBeTruthy();

    const pipelineConfigItem = getSubNavItemByName('pipeline_config');
    expect(checkDebugItemWithClass(pipelineConfigItem, 'active')).toBeTruthy();

    const containerItem = getNavItemByName('container');
    expect(checkDebugItemWithClass(containerItem, 'active')).toBeFalsy();
    expect(checkDebugItemWithClass(containerItem, 'expanded')).toBeFalsy();

    // Weblab for log is false:
    const logsItem = getPrimaryNavItemByName('monitor_logs');
    expect(checkDebugItemWithClass(logsItem, 'disabled')).toBeTruthy();

    // User docs should be hidden
    const docsItem = getNavItemByName('docs');
    expect(docsItem.nativeElement.style.display).toBe('none');
  });

  it('can expand item when primary item is clicked', () => {
    const containerPrimaryItem = getPrimaryNavItemByName('container');
    containerPrimaryItem.nativeElement.click();
    fixture.detectChanges();
    expect(
      checkDebugItemWithClass(getNavItemByName('container'), 'expanded'),
    ).toBeTruthy();

    // Since pipeline is active, it should still be expanded
    expect(
      checkDebugItemWithClass(getNavItemByName('pipeline'), 'expanded'),
    ).toBeTruthy();
  });

  it('can collapse active item when other nav is active', () => {
    // Fire another router state.
    fake$routerState.routerState.next({
      name: 'image.repository',
      params: {},
      url: 'image/repository',
      config: {},
      data: {},
    });
    fixture.detectChanges();
    expect(
      checkDebugItemWithClass(getNavItemByName('image'), 'expanded'),
    ).toBeTruthy();
    expect(
      checkDebugItemWithClass(
        getSubNavItemByName('image_repository'),
        'active',
      ),
    ).toBeTruthy();

    const pipelineItem = getNavItemByName('pipeline');
    expect(checkDebugItemWithClass(pipelineItem, 'active')).toBeFalsy();
    expect(checkDebugItemWithClass(pipelineItem, 'expanded')).toBeFalsy();

    const pipelineConfigItem = getSubNavItemByName('pipeline_config');
    expect(checkDebugItemWithClass(pipelineConfigItem, 'active')).toBeFalsy();
  });

  it('can change language when language picker is clicked', () => {
    fixture.detectChanges();
    spyOn(
      fixture.debugElement.injector.get(TranslateService),
      'changeLanguage',
    );

    const languagePicker = getPrimaryNavItemByName('language_picker');
    languagePicker.nativeElement.click();
    expect(
      fixture.debugElement.injector.get(TranslateService).changeLanguage,
    ).toHaveBeenCalled();
  });
  registerCleanup();
});
