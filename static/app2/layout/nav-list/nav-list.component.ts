import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { cloneDeep, flatten } from 'lodash';
import { Subscription, combineLatest } from 'rxjs';

import { RouterUtilService } from 'app2/core/services/router-util.service';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments, RcProfile, ResourceType } from 'app2/core/types';
import { AccountService } from 'app2/shared/services/features/account.service';
import {
  Region,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';

import { LayoutStateService } from '../layout-state.service';

import { NavStateService } from './nav-state.service';
import {
  BaseNavItemViewModel,
  NavCategoryViewModel,
  NavItemViewModel,
  NavSubItemViewModel,
  NavViewModel,
} from './nav-view-model';

@Component({
  selector: 'rc-nav-list',
  templateUrl: 'nav-list.component.html',
  styleUrls: ['nav-list.component.scss'],
})
export class NavListComponent implements OnInit, OnDestroy {
  nav: NavViewModel;
  private initialNav: NavViewModel; // the initial navViewModel
  private activeItemSubscription: Subscription;
  private permissionAndRegionSubscriber: Subscription;
  private profile: RcProfile;
  private currentActiveNavItem: BaseNavItemViewModel;

  async ngOnInit() {
    await this.navState.setupNav(this.routerUtil);
    this.profile = await this.accountService.getAuthProfile(true);
    this.initialNav = cloneDeep(this.navState.navViewModel);
    this.nav = this.navState.navViewModel;
    this.activeItemSubscription = this.navState.activeNavItem$.subscribe(
      activeNavItem => {
        this.currentActiveNavItem = activeNavItem;
        this.nav.setActiveItem(activeNavItem);
        const shouldExpand =
          activeNavItem &&
          activeNavItem.primaryItem.expandable &&
          !activeNavItem.primaryItem.active;
        if (shouldExpand) {
          activeNavItem.primaryItem.expanded = true;
        }
      },
    );

    this.permissionAndRegionSubscriber = combineLatest(
      this.regionService.region$,
      this.navState.resourcePermissions$,
    ).subscribe(([region, permissionsByResourceTypes]) => {
      // Hide child items
      const nav = cloneDeep(this.initialNav);
      nav.children.forEach((category: NavCategoryViewModel) => {
        // filter out primary item
        category.children = category.children.filter(
          (navItem: NavItemViewModel) => {
            return (
              this.checkNavItemByRegionPlatform(navItem, region) &&
              this.checkNavItemByPermissions(
                navItem,
                permissionsByResourceTypes,
              ) &&
              this.checkNavItemWithBizCondition(navItem)
            );
          },
        );
        // filter out secondary item
        category.children.forEach((navItem: NavItemViewModel) => {
          navItem.children = navItem.children.filter(
            (subItem: NavSubItemViewModel) => {
              return (
                this.checkNavItemByRegionPlatform(subItem, region) &&
                this.checkNavItemByPermissions(
                  subItem,
                  permissionsByResourceTypes,
                )
              );
            },
          );
        });
        // If there is no children, we should also hide the primary item.
        category.children = category.children.filter(
          (navItem: NavItemViewModel) => {
            if (
              !navItem.href &&
              !navItem.isLanguagePicker &&
              (navItem.disabled || !navItem.children.length)
            ) {
              return false;
            }
            return true;
          },
        );
      });

      nav.children = nav.children.filter((category: NavCategoryViewModel) => {
        return !!category.children.length;
      });

      // Extract all primary nav items
      const currentNavItems = nav.children.reduce((accu, category) => {
        return [
          ...accu,
          ...category.children.reduce((_accu, item) => {
            return [..._accu, item, ...flatten(item.children || [])];
          }, []),
        ];
      }, []);
      // If current active item or sub item is active and is not visible, we should go to the home page.
      if (this.currentActiveNavItem) {
        const current = currentNavItems.find((item: BaseNavItemViewModel) => {
          return (
            this.currentActiveNavItem &&
            this.currentActiveNavItem.index === item.index
          );
        });
        if (!current && Object.keys(permissionsByResourceTypes).length) {
          this.currentActiveNavItem = null;
          this.router.navigateByUrl('home');
        } else if (current) {
          nav.setActiveItem(current);
        }
      } else {
        // if current url does not match any of nav items, do nothing
      }
      this.nav = nav;
    });
  }

  ngOnDestroy(): void {
    if (this.activeItemSubscription) {
      this.activeItemSubscription.unsubscribe();
    }
    if (this.permissionAndRegionSubscriber) {
      this.permissionAndRegionSubscriber.unsubscribe();
    }
  }

  trackByCategory(_index: number, category: NavCategoryViewModel) {
    return category.index;
  }

  /**
   * 根据当前用户的所有资源权限，判断某个NavItem是否合法
   * @param {BaseNavItemViewModel} item
   * @param permissionsByResourceTypes
   * @returns {boolean}
   */
  private checkNavItemByPermissions(
    item: BaseNavItemViewModel,
    permissionsByResourceTypes: any,
  ) {
    const checkResourcePermission = (resourceType: ResourceType) =>
      permissionsByResourceTypes[resourceType] &&
      permissionsByResourceTypes[resourceType].length > 0;

    if (item.resourceTypes.length) {
      return item.resourceTypes.some(checkResourcePermission);
    }
    return true;
  }

  /**
   * 当前集群的信息，判断某个NavItem是否合法
   * @param {BaseNavItemViewModel} item
   * @param {Region} region
   * @returns {boolean}
   */
  private checkNavItemByRegionPlatform(
    item: BaseNavItemViewModel,
    region: Region,
  ) {
    return (
      this.checkRegionK8sFlagCondition(region, item) &&
      this.checkRegionNetworkPolicyCondition(region, item)
    );
  }

  /**
   * k8s集群显示k8sFlag为'SHOW'相关的导航项，非k8s集群显示k8sFlag为'HIDE'的导航项
   * @param {Region} region
   * @param {BaseNavItemViewModel} item
   * @returns {boolean}
   */
  private checkRegionK8sFlagCondition(
    region: Region,
    item: BaseNavItemViewModel,
  ) {
    if (!region) {
      // 1.若用户无集群权限，默认显示和未设置k8sFlag的导航项目
      // 2.处理regionsSubject和regionNameSubject初始化的undefined值
      // 3.若region为空，且设置了k8sFlag，隐藏该导航项
      return !item.k8sFlag;
    }
    return (
      (this.regionUtilities.isNewK8sRegion(region) &&
        item.k8sFlag === 'SHOW') ||
      (!this.regionUtilities.isNewK8sRegion(region) &&
        item.k8sFlag === 'HIDE') ||
      !item.k8sFlag
    );
  }

  /**
   * network_policy 导航项需要集群支持对应的feature
   * @param {Region} region
   * @param {BaseNavItemViewModel} item
   * @returns {boolean}
   */
  private checkRegionNetworkPolicyCondition(
    region: Region,
    item: BaseNavItemViewModel,
  ) {
    if (!region) {
      return item.name !== 'network_policy';
    }
    return (
      (item.name === 'network_policy' &&
        this.regionUtilities.isNetworkPolicyRegion(region)) ||
      item.name !== 'network_policy'
    );
  }

  /**
   * 特殊业务需求，判断是否需要隐藏当前NavItem，如 license
   * Note: 目前只对 Primary Item 应用了此规则进行过滤
   * @returns {boolean}
   */
  private checkNavItemWithBizCondition(item: BaseNavItemViewModel) {
    let flag = true;
    const licenseCondition =
      this.environments.is_private_deploy_enabled &&
      this.environments.license_required &&
      this.profile.is_admin;
    if (item.isLicenseRequired && !licenseCondition) {
      flag = false;
    }
    return flag;
  }

  constructor(
    public layoutState: LayoutStateService,
    private routerUtil: RouterUtilService,
    private router: Router,
    private navState: NavStateService,
    private regionService: RegionService,
    private accountService: AccountService,
    private regionUtilities: RegionUtilitiesService,
    @Inject(ENVIRONMENTS) public environments: Environments,
  ) {}
}
