import { TestBed, inject } from '@angular/core/testing';
import { Subject, of } from 'rxjs';

import { RouterUtilService } from 'app2/core/services/router-util.service';
import { ENVIRONMENTS, NAV_CONFIG, WEBLABS } from 'app2/core/tokens';
import { NavStateService } from 'app2/layout/nav-list/nav-state.service';
import { ProjectService } from 'app2/shared/services/features/project.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { CookieService } from 'app2/shared/services/utility/cookie.service';
import { translateService } from 'app2/testing/fake-translate.service';
import DEFAULT_NAV_CONFIG from 'app2/testing/testing-nav-config';
import { TranslateService } from 'app2/translate/translate.service';

const hrefFunction = (stateName: string) => stateName.split('.').join('/');

const fakeRouterUtil = {
  routerState: of({
    url: 'pipeline/config',
  }),
  href: (stateName: string) => Promise.resolve(hrefFunction(stateName)),
};

describe('NavStateService', () => {
  let navState: NavStateService;
  const environments: any = {};
  const weblabs: any = {
    REPO_SYNC_ENABLED: true,
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        NavStateService,
        ProjectService,
        CookieService,
        // Mock dependencies:
        {
          provide: TranslateService,
          useValue: translateService,
        },
        {
          provide: RoleUtilitiesService,
          useValue: {
            resourceTypesPermissions: () => true,
          },
        },
        {
          provide: NAV_CONFIG,
          useValue: DEFAULT_NAV_CONFIG,
        },
        {
          provide: WEBLABS,
          useFactory: () => weblabs,
        },
        {
          provide: ENVIRONMENTS,
          useFactory: () => environments,
        },
      ],
    });
  });

  beforeEach(inject([NavStateService], (ns: NavStateService) => {
    navState = ns;
  }));

  it('should get correct display name', () => {
    expect(navState.getDisplayName('app_service')).toBe(
      'translated_nav_app_service',
    );
    expect(navState.getDisplayName('language_picker')).toBe('translated_en');
    expect(navState.getDisplayName('')).toBe('');
  });

  it('should have nave view model instance ready', () => {
    expect(navState.navViewModel).toBeDefined();
    expect(navState.navItemExpanded$ instanceof Subject).toBeTruthy();
  });

  it('can correctly setup nav', async done => {
    let isReady = false;
    navState.ready.then(() => (isReady = true));
    expect(isReady).toBe(false);
    await navState.setupNav(<RouterUtilService>fakeRouterUtil);
    expect(isReady).toBe(true);
    const activeNavItem = await navState.activeNavItem$.toPromise();
    expect(activeNavItem.stateName).toBe('pipeline.config');

    // Should have display name rendered
    expect(
      navState.navViewModel.children.every(category => {
        return (
          (!category.name || category.displayName) &&
          category.children.every(item => {
            return (
              item.displayName &&
              item.children.every(subItem => {
                return !!subItem.displayName;
              })
            );
          })
        );
      }),
    ).toBeTruthy();

    // App catelog should should beta icon by default
    expect(navState.getItemByUrl('monitor/logs').iconClass).toBe(
      'rc-beta-icon',
    );

    // Image sync center should be enabled since its weblab is enabled
    expect(navState.getItemByUrl('image/sync-center').disabled).toBeFalsy();

    // Job config should be disabled since its weblab is disabled
    expect(
      navState.getItemByUrl('network/load_balancer').disabled,
    ).toBeTruthy();
    done();
  });

  it('private deploy should hide beta icon', async done => {
    environments.is_private_deploy_enabled = true;
    await navState.setupNav(<RouterUtilService>fakeRouterUtil);
    expect(
      navState.getItemByUrl('app_catalog').iconClass === 'rc-beta-icon',
    ).toBeFalsy();
    done();
  });
});
