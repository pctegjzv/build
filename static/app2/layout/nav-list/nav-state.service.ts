import { Inject, Injectable } from '@angular/core';
import { flatMap, maxBy } from 'lodash';
import { Observable, Subject, defer } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
  retry,
  startWith,
  switchMap,
} from 'rxjs/operators';

import { Store } from '@ngrx/store';
import { RouterUtilService } from 'app2/core/services/router-util.service';
import { ENVIRONMENTS, NAV_CONFIG, WEBLABS } from 'app2/core/tokens';
import {
  Environments,
  NavConfig,
  ResourceType,
  Weblabs,
} from 'app2/core/types';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { State } from 'app2/state-container/core/reducers';
import * as projectSelectors from 'app2/state-container/core/selectors/project';

import {
  BaseNavItemViewModel,
  NavItemViewModel,
  NavSubItemViewModel,
  NavViewModel,
} from './nav-view-model';

/**
 * View Model state for Nav.
 * - holding the state
 * - events
 * - more utilities
 */
@Injectable()
export class NavStateService {
  private hrefItems: Array<NavItemViewModel | NavSubItemViewModel> = [];
  private _navViewModel: NavViewModel;
  /**
   * Will be resolved when nav state is ready.
   */
  ready = new Promise(resolve => {
    this.readyResolve = resolve;
  });

  /**
   * Events for active NavItemViewModel.
   * Including both primary and secondary nav item.
   *
   * Will be available after ready.
   */
  activeNavItem$: Observable<NavItemViewModel | NavSubItemViewModel>;

  /**
   * Events for expanded NavItemViewModel.
   * Primary nav item only.
   *
   * Will be available after ready.
   */
  navItemExpanded$ = new Subject<NavItemViewModel>();

  resourcePermissions$: Observable<{ [resourceType: string]: string[] }>;

  private routerUtil: RouterUtilService;
  private readyResolve: Function;
  private setup = false;

  constructor(
    @Inject(NAV_CONFIG) private navConfig: NavConfig,
    @Inject(WEBLABS) private weblabs: Weblabs,
    @Inject(ENVIRONMENTS) private environments: Environments,
    private roleUtilities: RoleUtilitiesService,
    private store: Store<State>,
  ) {
    this._navViewModel = new NavViewModel(this.navConfig);
    this._navViewModel.applyWeblabs(this.weblabs);
    this._navViewModel.applyEnvironments(this.environments);
    this.setupNavResourcePermissions();
  }

  /**
   * Get the global NavViewModel instance.
   * @returns {NavViewModel}
   */
  get navViewModel() {
    return this._navViewModel;
  }

  getDisplayName(name: string) {
    return 'nav_' + name;
  }

  getItemByUrl(url: string): NavItemViewModel | NavSubItemViewModel {
    if (!this.setup) {
      throw new Error('NavViewModel is not configured yet');
    }
    // Filter out items which has similar urls:
    const closeItems = this.hrefItems.filter(item => {
      return item.href && url.startsWith(item.href);
    });
    return !!closeItems.length && maxBy(closeItems, item => item.href.length);
  }

  async setupNav(routerUtil: RouterUtilService) {
    if (this.setup) {
      throw new Error('NavViewModel is already configured');
    }
    this.setup = true;
    this.routerUtil = routerUtil;

    this.navViewModel.children.forEach(category => {
      category.displayName = this.getDisplayName(category.name);
      category.children.forEach(item =>
        item.traverse(_item => {
          _item.displayName = this.getDisplayName(_item.name);
          // Private deployed app does not need to show beta icon
          if (!this.environments.is_private_deploy_enabled && _item.beta) {
            _item.iconClass = 'rc-beta-icon';
          }
        }),
      );
    });

    await Promise.all(
      this.navViewModel.children.map(category =>
        Promise.all(
          category.children.map(navItem => this.setupNavItemHref(navItem)),
        ),
      ),
    );

    this.activeNavItem$ = this.routerUtil.routerState.pipe(
      map(state => state.url),
      distinctUntilChanged(),
      map(url => this.getItemByUrl(url)),
      publishReplay(1),
      refCount(),
    );

    this.readyResolve();
  }

  /**
   * Nav list items display is dependent on resource permissions.
   * This method will pre-flight the permissions which is subject to project changes.
   */
  setupNavResourcePermissions() {
    // Flatten all resource types listed in nav config
    let allResourceTypes: ResourceType[] = this._navViewModel.children
      .reduce(
        (accum, category) => [
          ...accum,
          ...category.children.reduce(
            (_accum, item) => [
              ..._accum,
              ...item.resourceTypes,
              ...flatMap(item.children || [], item => item.resourceTypes),
            ],
            [],
          ),
        ],
        [],
      )
      .filter(type => !!type);

    allResourceTypes = Array.from(new Set(allResourceTypes));
    // An observable factory to fetch permissions for a list of resource types
    const resourceTypesObFactory = (projectName: string) => {
      if (
        !projectName &&
        this.weblabs.PROJECTS_ENABLED &&
        this.environments.vendor_customer !== 'CMB' // TODO: 后续使用视图拆分WEBLAB，若视图拆分未打开，且无项目选中状态，则这里返回空权限。
      ) {
        return defer(() => Promise.resolve({}));
      } else {
        return defer(() =>
          this.roleUtilities.resourceTypesPermissions(allResourceTypes),
        ).pipe(
          // Immediately emit empty result to show skeleton nav list without any permissions.
          startWith({}),
          // Since permission API sometimes returns 500, we should make it fail safe by retrying a couple of times.
          retry(3),
        );
      }
    };

    this.resourcePermissions$ = this.store
      .select(projectSelectors.getSelectedProjectName)
      .pipe(
        debounceTime(100),
        switchMap(resourceTypesObFactory),
        publishReplay(1),
        refCount(),
      );
  }

  private setupNavItemHref(
    navItem: NavItemViewModel | NavSubItemViewModel,
  ): Promise<any> {
    const promises: Promise<any>[] = [];
    promises.push(
      this.getNavItemHref(navItem).then(href => {
        navItem.href = href;
        navItem.isExternalHref = !!href && !navItem.stateName;
        if (href) {
          this.hrefItems.push(navItem);
        }
      }),
    );

    if (navItem instanceof NavItemViewModel) {
      promises.push(
        Promise.all(
          navItem.children.map(subItem => this.setupNavItemHref(subItem)),
        ),
      );
    }
    return Promise.all(promises);
  }

  private async getNavItemHref(item: BaseNavItemViewModel): Promise<string> {
    let href = item.href;
    if (item.stateName) {
      href = await this.routerUtil.href(item.stateName);
    } else if (href && href.startsWith('env:')) {
      const [, env] = href.split('env:');
      href = this.environments[env] || '';
    }
    return href;
  }
}
