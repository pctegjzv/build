import {
  Environments,
  NavCategoryConfig,
  NavConfig,
  NavItemConfig,
  ResourceType,
  Weblabs,
} from 'app2/core/types';

interface ActiveItemSetter {
  setActiveItem(navItem: NavItemViewModel | NavSubItemViewModel): void;
}

interface WeblabsSetter {
  applyWeblabs(weblabs: Weblabs): void;
}

interface EnvironmentsSetter {
  applyEnvironments(environments: Environments): void;
}

let navIndexCounter = 0;

export class BaseNavItemViewModel {
  private _index: string;
  active: boolean;
  beta: boolean;
  disabled: boolean;
  hide: boolean;
  isExternalHref: boolean;
  href: string;
  iconClass: string;
  stateName: string;
  displayName: string;
  tooltip: string;
  resourceTypes: ResourceType[];
  k8sFlag?: 'SHOW' | 'HIDE';
  weblab?: string;
  environment?: string;

  constructor(public config: NavItemConfig) {
    this._index = `${this.config.name}${++navIndexCounter}`;
    this.href = this.config.href;
    this.stateName = this.config.stateName;
    this.iconClass = this.config.icon;
    this.beta = this.config.beta;
    this.weblab = this.config.weblab;
    this.environment = this.config.environment;

    if (this.config.resourceType instanceof Array) {
      this.resourceTypes = this.config.resourceType;
    } else {
      this.resourceTypes = this.config.resourceType
        ? [this.config.resourceType]
        : [];
    }
    this.isExternalHref = false;
    this.k8sFlag = this.config.k8sFlag;
  }

  get name() {
    return this.config.name;
  }

  get index() {
    return this._index;
  }

  get isPrimary() {
    return isNavItemPrimary(this);
  }

  get primaryItem(): NavItemViewModel {
    if (isNavItemPrimary(this)) {
      return this;
    } else if (isNavItemSecondary(this)) {
      return this.parent;
    } else {
      throw new Error('Unknown NavItem type???');
    }
  }

  get category() {
    return this.primaryItem.parent;
  }

  get nav() {
    return this.category.parent;
  }

  get isLicenseRequired() {
    return this.name === 'license';
  }
}

export class NavSubItemViewModel extends BaseNavItemViewModel
  implements ActiveItemSetter {
  constructor(config: NavItemConfig, public parent: NavItemViewModel) {
    super(config);
  }

  setActiveItem(navItem: NavItemViewModel | NavSubItemViewModel) {
    this.active = navItem.index === this.index;
    if (this.index === navItem.index && this.parent && this.parent.expandable) {
      this.parent.expanded = true;
    }
  }
}

export class NavItemViewModel extends BaseNavItemViewModel
  implements ActiveItemSetter, WeblabsSetter, EnvironmentsSetter {
  children: NavSubItemViewModel[];
  expanded: boolean;

  constructor(config: NavItemConfig, public parent: NavCategoryViewModel) {
    super(config);
    this.children = this.config.children
      ? this.config.children.map(
          subItem => new NavSubItemViewModel(subItem, this),
        )
      : [];
  }

  get isLanguagePicker() {
    return this.name === 'language_picker';
  }

  get hasChildren() {
    return this.config.children && !!this.config.children.length;
  }

  get expandable() {
    return (
      this.nav.expandMode !== 'NO_COLLAPSE' &&
      !this.disabled &&
      this.hasChildren
    );
  }

  setActiveItem(navItem: NavItemViewModel | NavSubItemViewModel) {
    if (navItem instanceof NavSubItemViewModel) {
      this.active = navItem.parent.index === this.index;
    } else {
      this.active = navItem.index === this.index;
    }
    this.children.forEach(subItem => {
      subItem.setActiveItem(navItem);
    });
  }

  applyWeblabs(weblabs: Weblabs) {
    if (this.config.weblab) {
      this.disabled = !weblabs[this.config.weblab];
    }

    this.children = this.children.filter((item: NavSubItemViewModel) => {
      return !item.config.weblab || !!weblabs[item.config.weblab];
    });
  }

  applyEnvironments(environments: Environments) {
    this.children = this.children.filter((item: NavSubItemViewModel) => {
      return !item.environment || !!environments[item.config.environment];
    });
  }

  traverse(fn: (navItemViewModel: BaseNavItemViewModel) => void) {
    fn(this);
    this.children.forEach(subItem => fn(subItem));
  }
}

export class NavCategoryViewModel
  implements ActiveItemSetter, WeblabsSetter, EnvironmentsSetter {
  private _index: string;
  children: NavItemViewModel[] = [];
  displayName: string;
  active: boolean;

  get name() {
    return this.config.name;
  }

  constructor(private config: NavCategoryConfig, public parent: NavViewModel) {
    this._index = `${this.config.name}${++navIndexCounter}`;
    this.children = this.config.children.map(
      item => new NavItemViewModel(item, this),
    );
  }

  setActiveItem(navItem: NavItemViewModel | NavSubItemViewModel) {
    this.active = navItem && navItem.category.index === this.index;
    this.children.forEach(_navItem => {
      _navItem.setActiveItem(navItem);
    });
  }

  applyWeblabs(weblabs: Weblabs) {
    this.children = this.children.filter((item: NavItemViewModel) => {
      return !item.config.weblab || !!weblabs[item.config.weblab];
    });
    this.children.forEach(item => {
      item.applyWeblabs(weblabs);
    });
  }

  applyEnvironments(environments: Environments) {
    this.children = this.children.filter((item: NavItemViewModel) => {
      // TODO: should filter by environments value equality
      return !item.environment || !!environments[item.config.environment];
    });
    this.children.forEach(item => {
      item.applyEnvironments(environments);
    });
  }

  get index() {
    return this._index;
  }
}

function isNavItemPrimary(
  navItem: BaseNavItemViewModel,
): navItem is NavItemViewModel {
  return navItem instanceof NavItemViewModel;
}

function isNavItemSecondary(
  navItem: BaseNavItemViewModel,
): navItem is NavSubItemViewModel {
  return navItem instanceof NavSubItemViewModel;
}

/**
 * View Model classes containing the mutable states for the displaying item.
 */
export class NavViewModel implements ActiveItemSetter {
  children: NavCategoryViewModel[] = [];

  get expandMode() {
    return this.config.expandMode;
  }

  constructor(private config: NavConfig) {
    this.children = this.config.categories.map(
      category => new NavCategoryViewModel(category, this),
    );
  }

  setActiveItem(navItem: NavItemViewModel | NavSubItemViewModel) {
    this.children.forEach(category => {
      category.setActiveItem(navItem);
    });
  }

  applyWeblabs(weblabs: Weblabs) {
    this.children.forEach(category => {
      category.applyWeblabs(weblabs);
    });
  }

  applyEnvironments(environments: Environments) {
    this.children.forEach(category => {
      category.applyEnvironments(environments);
    });
  }
}
