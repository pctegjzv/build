import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FloatingPageOption } from 'app2/layout/floating-page.service';

@Component({
  selector: 'rc-floating-page',
  templateUrl: 'floating-page.component.html',
  styleUrls: ['floating-page.component.scss'],
})
export class FloatingPageComponent implements OnInit {
  @Input()
  option: FloatingPageOption;
  @Output()
  close = new EventEmitter();

  ngOnInit(): void {}
}
