import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { TemplatePortal } from '@angular/cdk/portal';
import {
  Component,
  ElementRef,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { Observable, combineLatest, merge } from 'rxjs';
import { AsapScheduler } from 'rxjs/internal/scheduler/AsapScheduler';
import { debounceTime, map } from 'rxjs/operators';

import { RouterUtilService } from 'app2/core/services/router-util.service';
import {
  FloatingPageOption,
  FloatingPageService,
} from 'app2/layout/floating-page.service';
import {
  SidePanelOption,
  SidePanelService,
} from 'app2/layout/side-panel.service';
import { PageHeaderContentService } from 'app2/shared/directives/page-header-content/page-header-content.service';

import { LayoutStateService } from '../layout-state.service';

const backdropAnimation = trigger('backdrop', [
  transition(':enter', [style({ opacity: 0 }), animate(200)]),
  transition(':leave', [animate(200, style({ opacity: 0 }))]),
]);

const easeTiming = '0.3s ease-in-out';
const hSlideAnimation = trigger('hSlide', [
  transition(':enter', [style({ width: 0 }), animate(easeTiming)]),
  transition(':leave', [animate(easeTiming, style({ width: 0 }))]),
]);

const vSlideAnimation = trigger('vSlide', [
  transition(':enter', [style({ height: 0 }), animate(easeTiming)]),
  transition(':leave', [animate(easeTiming, style({ height: 0 }))]),
]);

const floatingPageAnimation = trigger('floatingPage', [
  state(
    '*',
    style({
      transform: 'translateY(0) scale(1)',
      opacity: 1,
    }),
  ),
  transition(':enter', [
    style({
      transform: 'translateY(50px) scale(0.9)',
      opacity: 0,
    }),
    animate(easeTiming),
  ]),
  transition(':leave', [
    animate(
      easeTiming,
      style({
        transform: 'translateY(50px) scale(0.9)',
        opacity: 0,
      }),
    ),
  ]),
]);

@Component({
  selector: 'rc-page-container',
  templateUrl: 'page-container.component.html',
  styleUrls: ['page-container.component.scss'],
  animations: [
    backdropAnimation,
    hSlideAnimation,
    vSlideAnimation,
    floatingPageAnimation,
  ],
})
export class PageContainerComponent implements OnInit {
  @ViewChild('pageContainer')
  pageContainer: ElementRef<HTMLDivElement>;

  scrollContainerWidth = '100%';
  scrollContainerHeight = '100%';
  pageNoMinWidth = false;
  showBackdrop: Observable<boolean>;

  sidePanelPosition: SidePanelOption['position'] | undefined;
  sidePanelTemplateRef: TemplateRef<any>;
  sidePanelTemplateContext: any;
  sidePanelSize: number | string;
  sidePanelResizable: boolean;
  sidePanelCustomStyle: any;

  floatingPageOption: Observable<FloatingPageOption>;

  get pageHeaderTemplate$(): Observable<TemplatePortal<any>> {
    return this._pageHeaderTemplate$;
  }

  private _pageHeaderTemplate$: Observable<TemplatePortal<any>>;

  constructor(
    protected layoutState: LayoutStateService,
    protected pageHeaderContent: PageHeaderContentService,
    private sidePanel: SidePanelService,
    private floatingPage: FloatingPageService,
    private routerUtil: RouterUtilService,
  ) {}

  ngOnInit() {
    this.configureSidePanel();

    this.floatingPageOption = this.floatingPage.option$;
    this.showBackdrop = combineLatest(
      this.floatingPage.option$,
      this.sidePanel.option$,
      (fpOption, spOption) =>
        (fpOption && fpOption.backdrop) || (spOption && spOption.backdrop),
      AsapScheduler,
    );

    this._pageHeaderTemplate$ = this.pageHeaderContent.templatePortal$.pipe(
      debounceTime(0),
    );
  }

  closeFloatingPage() {
    this.floatingPage.close();
  }

  /**
   * Adjust container attributes when side panel changes.
   */
  private configureSidePanel() {
    const handleContainerChange = (option: SidePanelOption) => {
      let componentContainerWidth = '100%';
      if (!option) {
        this.scrollContainerWidth = '100%';
        this.scrollContainerHeight = '100%';
      } else {
        const mainAxis = `calc(100% - ${option.size}px)`;
        const crossAxis = '100%';
        if (option.position === 'bottom') {
          this.scrollContainerWidth = crossAxis;
          this.scrollContainerHeight = mainAxis;
        } else {
          this.scrollContainerWidth = mainAxis;
          this.scrollContainerHeight = crossAxis;

          if (option.floating) {
            componentContainerWidth = `calc(100% + ${option.size}px)`;
          }
        }
      }
      // Style attribute won't work on ui-view:
      this.pageContainer.nativeElement.style.width = componentContainerWidth;
    };

    const handleSidePanelChange = (option: SidePanelOption) => {
      if (option) {
        this.sidePanelPosition = option.position;
        this.sidePanelSize =
          option.size + (typeof option.size === 'string' ? '' : 'px');
        this.sidePanelTemplateRef = option.templateRef;
        this.sidePanelTemplateContext = option.context || {};
        this.sidePanelResizable = option.resizable;
        this.sidePanelCustomStyle = option.customStyle;
        this.pageNoMinWidth = option.pageNoMinWidth;
      } else {
        this.sidePanelPosition = undefined;
        this.pageNoMinWidth = false;
      }
    };

    merge(this.routerUtil.routerState, this.sidePanel.option$)
      .pipe(
        debounceTime(0),
        map(() => this.sidePanel.getOption()),
      )
      .subscribe(option => {
        setTimeout(() => handleContainerChange(option), 200);
        handleSidePanelChange(option);
      });
  }
}
