import {
  Component,
  ElementRef,
  HostBinding,
  Input,
  OnInit,
  Renderer2,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import * as _ from 'lodash';

import { SidePanelService } from 'app2/layout/side-panel.service';

@Component({
  selector: 'rc-side-panel',
  templateUrl: 'side-panel.component.html',
  styleUrls: ['side-panel.component.scss'],
})
export class SidePanelComponent implements OnInit {
  @Input()
  @HostBinding('class.resizable')
  resizable: boolean;

  @Input()
  @HostBinding('attr.position')
  position: 'bottom' | 'right';

  @Input()
  templateRef: TemplateRef<any>;
  @Input()
  templateContext: any;
  @Input()
  customStyle: any;
  @Input()
  computedSize: string;

  @HostBinding('class.dragging')
  private dragging: boolean;
  @ViewChild('resizeHandle')
  private resizeHandleRef: ElementRef;
  @ViewChild('sidePanelContainer')
  private sidePanelContainer: ElementRef;
  private debouncedOnResize: Function;

  constructor(
    private sidePanel: SidePanelService,
    private renderer: Renderer2,
  ) {
    this.debouncedOnResize = _.debounce((newSize: number) =>
      this.sidePanel.resize(newSize),
    );
  }

  ngOnInit(): void {
    this.renderer.listen(this.resizeHandleRef.nativeElement, 'drag', event =>
      this.onDragEvent(event),
    );

    this.renderer.listen(
      this.resizeHandleRef.nativeElement,
      'dragstart',
      () => (this.dragging = true),
    );

    this.renderer.listen(
      this.resizeHandleRef.nativeElement,
      'dragend',
      () => (this.dragging = false),
    );
  }

  private onDragEvent(event: DragEvent) {
    if (event.screenX + event.screenY === 0) {
      return;
    }

    let newSize: number;
    if (this.position === 'bottom') {
      newSize = Math.max(window.innerHeight - Math.max(event.pageY, 50), 15);
    } else {
      newSize = Math.max(window.innerWidth - Math.max(event.pageX, 50), 15);
    }

    // Skip if the offset is too large
    const originSize = this.computedSize.endsWith('px')
      ? parseInt(this.computedSize, 10)
      : this.getComputedSize();
    if (Math.abs(newSize - originSize) < 500) {
      this.debouncedOnResize(newSize);
    }
  }

  private getComputedSize() {
    return this.position === 'bottom'
      ? this.sidePanelContainer.nativeElement.offsetHeight
      : this.sidePanelContainer.nativeElement.offsetWidth;
  }
}
