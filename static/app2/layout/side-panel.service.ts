import { Injectable, TemplateRef } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface SidePanelOption {
  position: 'bottom' | 'right';
  templateRef: TemplateRef<any>;
  context?: any;

  /**
   * Refers to height when position is bottom, width when position is right.
   * If size is a string, then it is a computed value.
   */
  size: number | string;

  /**
   * Whether or not to show the resize bar. Default false.
   */
  resizable?: boolean;

  /**
   * Whether or not to show a backdrop. Default false.
   */
  backdrop?: boolean;

  /**
   * If true, the main view will not have its width shrinked with
   * respected to the size of the side panel.
   *
   * Only applicable for 'right' position.
   */
  floating?: boolean;

  /**
   * If true, the main view will not have a min width applied.
   */
  pageNoMinWidth?: boolean;

  /**
   * Custom style applied to the side panel container
   */
  customStyle?: any;
}

@Injectable()
export class SidePanelService {
  private optionSubject = new BehaviorSubject<SidePanelOption>(undefined);

  constructor() {}

  get option$() {
    return this.optionSubject.asObservable();
  }

  getOption() {
    return this.optionSubject.getValue();
  }

  isShowing() {
    return !!this.optionSubject.getValue();
  }

  open(option: SidePanelOption) {
    this.optionSubject.next(option);
  }

  resize(size: number) {
    this.optionSubject.next({
      ...this.optionSubject.getValue(),
      size,
    });
  }

  close() {
    this.optionSubject.next(null);
  }
}
