import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  templateUrl: './alert-box.component.html',
  styleUrls: ['./alert-box.component.scss'],
})
export class AlertBoxComponent {
  @Input()
  title: string;
  @Input()
  content: string;
  @Output()
  confirmed = new EventEmitter<boolean>();
  @Input()
  confirmText = 'confirm';

  confirm() {
    this.confirmed.next(true);
  }
}
