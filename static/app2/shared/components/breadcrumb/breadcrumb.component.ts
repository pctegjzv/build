import {
  Component,
  ContentChildren,
  Directive,
  QueryList,
  TemplateRef,
} from '@angular/core';

@Directive({
  selector: '[rcBreadcrumbItem]',
})
export class BreadcrumbItemDirective {}

@Component({
  selector: 'rc-breadcrumb',
  templateUrl: 'breadcrumb.component.html',
  styleUrls: ['breadcrumb.component.scss'],
})
export class BreadcrumbComponent {
  @ContentChildren(BreadcrumbItemDirective, { read: TemplateRef })
  itemTemplateRefs: QueryList<TemplateRef<any>>;
}
