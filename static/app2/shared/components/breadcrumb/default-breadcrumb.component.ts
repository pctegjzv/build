import { Component, OnInit } from '@angular/core';
import { Observable, from } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { RouterUtilService } from 'app2/core/services/router-util.service';
import { DefaultBreadcrumbStrategy } from 'app2/layout/default-breadcrumb-strategy.service';
import { BreadcrumbPartial } from 'app2/layout/layout-breadcrumb-strategy';

@Component({
  selector: 'rc-default-breadcrumb',
  template: `
    <rc-breadcrumb>
      <ng-container *ngFor="let partial of partials$ | async; last as isLast">
        <ng-container *rcBreadcrumbItem>
          <a *ngIf="partial.href && !isLast; else notHrefItem"
             class="rb-btn-link" [href]="partial.href">
            {{ partial.label.name | translate : partial.label.params }}
          </a>
          <ng-template #notHrefItem>
            <span>{{ partial.label.name | translate }}</span>
          </ng-template>
          <ng-container *ngIf="isLast"><ng-content></ng-content></ng-container>
        </ng-container>
      </ng-container>
    </rc-breadcrumb>
  `,
})
export class DefaultBreadcrumbComponent implements OnInit {
  partials$: Observable<BreadcrumbPartial[]>;

  constructor(
    private routerUtil: RouterUtilService,
    private breadcrumbStrategy: DefaultBreadcrumbStrategy,
  ) {}

  ngOnInit() {
    this.partials$ = this.routerUtil.routerState.pipe(
      switchMap(state =>
        from(this.breadcrumbStrategy.renderBreadcrumbPartials(state)),
      ),
    );
  }
}
