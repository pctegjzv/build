import {
  AfterViewInit,
  Component,
  ContentChildren,
  Directive,
  ElementRef,
  HostBinding,
  Input,
  QueryList,
  ViewChild,
} from '@angular/core';

import { ButtonSize } from 'app2/shared/components/button/button.component';

@Directive({
  selector: '[rcIconTemplate]',
})
export class IconTemplateDirective {}

@Directive({
  selector: 'button[rcMenuButton], a[rcMenuButton]',
})
export class MenuButtonCssStylerDirective {
  @HostBinding()
  type = 'button';
  @HostBinding('class.rc-menu-button')
  className = true;
}

@Component({
  selector: 'rc-button-menu',
  templateUrl: 'button-menu.component.html',
  styleUrls: ['button-menu.component.scss'],
})
export class ButtonMenuComponent implements AfterViewInit {
  @ViewChild('dropdownAnchorEl')
  protected dropdownAnchorEl: ElementRef;
  @ContentChildren(MenuButtonCssStylerDirective)
  protected menuButtons: QueryList<MenuButtonCssStylerDirective>;

  @Input()
  text: string;
  @Input()
  textSize: ButtonSize;

  constructor() {}

  ngAfterViewInit() {}

  get tooltipDisabled() {
    return !this.menuButtons.length;
  }
}
