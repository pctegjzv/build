import {
  Component,
  Directive,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';

// tslint:disable:use-host-property-decorator
// tslint:disable:component-selector

@Directive({
  selector: 'button[rcButton], a[rcButton]',
  host: { class: 'rc-button' },
})
export class ButtonCssStylerDirective {}

@Directive({
  selector: 'button[rcIconButton], a[rcIconButton]',
  host: { class: 'rc-icon-button' },
})
export class IconButtonCssStylerDirective {}

export type ButtonColor = 'default' | 'primary' | 'primary-lite' | 'secondary';

export type ButtonSize = 'tiny' | 'small' | 'medium' | 'large';

@Component({
  selector: `button[rcButton], a[rcButton], button[rcIconButton], a[rcIconButton]`,
  // Automatically applies Semantic-UI classes
  host: { class: 'ui button' },
  templateUrl: 'button.component.html',
  styleUrls: ['button.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ButtonComponent implements OnChanges, OnInit {
  @Input()
  @HostBinding('disabled')
  disabled: boolean;
  @Input()
  @HostBinding('class.loading')
  loading: boolean;
  @Input()
  rcButton: ButtonColor;
  @Input()
  rcIconButton: ButtonColor;
  @HostBinding('attr.color')
  color: ButtonColor;
  @HostBinding('class.recently-enabled')
  recentlyEnabled = false;
  /**
   * Default HTML5 button has submit type and it will trigger submit events.
   * However in our app we will always trigger submit manually.
   * @type {string}
   */
  @Input()
  @HostBinding()
  type: 'button' | 'reset' | 'submit' = 'button';
  private recentlyEnabledTimer: number;

  @Input()
  rcButtonSize: ButtonSize;
  @HostBinding('class.tiny')
  get tiny() {
    return this.rcButtonSize === 'tiny';
  }

  @HostBinding('class.small')
  get small() {
    return this.rcButtonSize === 'small';
  }

  @HostBinding('class.medium')
  get medium() {
    return this.rcButtonSize === 'medium';
  }

  @HostBinding('class.large')
  get large() {
    return this.rcButtonSize === 'large';
  }

  ngOnInit(): void {
    this.color = this.rcButton || this.rcIconButton || 'default';
  }

  ngOnChanges(changes: SimpleChanges): void {
    // Applies .recently-enabled class when you need to apply custom animation to the button
    // TODO: make this as a utility directive?
    if (
      !this.recentlyEnabledTimer &&
      changes['disabled'] &&
      !changes['disabled'].currentValue &&
      changes['disabled'].previousValue
    ) {
      this.recentlyEnabled = true;
      this.recentlyEnabledTimer = window.setTimeout(() => {
        this.recentlyEnabled = false;
        this.recentlyEnabledTimer = undefined;
      }, 3000);
    } else {
      if (this.recentlyEnabledTimer) {
        clearTimeout(this.recentlyEnabledTimer);
        this.recentlyEnabledTimer = undefined;
      }
      this.recentlyEnabled = false;
    }
  }
}
