import { Component, ContentChild, Directive, Input } from '@angular/core';

@Directive({
  selector: '[rcCardHeader]',
})
export class CardHeaderDirective {}

@Component({
  selector: 'rc-card',
  templateUrl: 'card.component.html',
  styleUrls: ['card.component.scss'],
})
export class CardComponent {
  @Input()
  title: string;
  @Input()
  footnote: string;
  @Input()
  fitParent: boolean;
  @ContentChild(CardHeaderDirective)
  headerRef: CardHeaderDirective;
}
