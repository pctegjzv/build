import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { XAxisTicksComponent } from '@swimlane/ngx-charts/release/common/axes/x-axis-ticks.component';

@Component({
  /* tslint:disable-next-line */
  selector: 'g[rc-charts-x-axis-ticks]',
  template: `
    <svg:g #ticksel>
      <svg:g *ngFor="let tick of ticks" class="tick"
        [attr.transform]="tickTransform(tick)">
        <title>{{tickFormat(tick)}}</title>
        <svg:text
          stroke-width="0.01"
          [attr.text-anchor]="textAnchor"
          [attr.transform]="textTransform"
          [style.font-size]="'12px'">
          {{trimLabel(tickFormat(tick))}}
        </svg:text>
      </svg:g>
    </svg:g>

    <svg:g *ngFor="let tick of ticks"
      [attr.transform]="tickTransform(tick)">
      <svg:g *ngIf="showGridLines"
        [attr.transform]="gridLineTransform()">
        <svg:line
          class="gridline-path gridline-path-vertical"
          [attr.y1]="-gridLineHeight"
          y2="0" />
      </svg:g>
    </svg:g>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomXAxisTicksComponent extends XAxisTicksComponent {
  @Input()
  tickWidth: number;

  getMaxTicks(tickWidth: number): number {
    const width = this.tickWidth || tickWidth;
    return Math.floor(this.width / width);
  }

  getRotationAngle(): number {
    return 0;
  }
}
