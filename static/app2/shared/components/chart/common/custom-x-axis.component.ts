import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { XAxisComponent } from '@swimlane/ngx-charts/release/common/axes/x-axis.component';

@Component({
  /* tslint:disable-next-line */
  selector: 'g[rc-charts-x-axis]',
  template: `
    <svg:g
      [attr.class]="xAxisClassName"
      [attr.transform]="transform">
      <svg:g rc-charts-x-axis-ticks
        *ngIf="xScale"
        [tickFormatting]="tickFormatting"
        [tickArguments]="tickArguments"
        [tickWidth]="tickWidth"
        [tickStroke]="tickStroke"
        [scale]="xScale"
        [orient]="xOrient"
        [showGridLines]="showGridLines"
        [gridLineHeight]="dims.height"
        [width]="dims.width"
        (dimensionsChanged)="emitTicksHeight($event)"
      />
      <svg:g ngx-charts-axis-label
        *ngIf="showLabel"
        [label]="labelText"
        [offset]="labelOffset"
        [orient]="'bottom'"
        [height]="dims.height"
        [width]="dims.width">
      </svg:g>
    </svg:g>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomXAxisComponent extends XAxisComponent {
  @Input()
  tickWidth: number;
}
