import { animate, style, transition, trigger } from '@angular/animations';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';
import { BarVerticalComponent } from '@swimlane/ngx-charts';
import { scaleLinear, scalePoint, scaleTime } from 'd3-scale';

@Component({
  selector: 'rc-charts-bar-vertical',
  template: `
    <ngx-charts-chart
    [view]="[width, height]"
    [showLegend]="legend"
    [legendOptions]="legendOptions"
    [activeEntries]="activeEntries"
    [animations]="animations"
    (legendLabelClick)="onClick($event)"
    (legendLabelActivate)="onActivate($event)"
    (legendLabelDeactivate)="onDeactivate($event)">
        <svg:g [attr.transform]="transform" class="bar-chart chart">
          <svg:g rc-charts-x-axis
                *ngIf="xAxis"
                [xScale]="customXScale"
                [dims]="dims"
                [showLabel]="showXAxisLabel"
                [labelText]="xAxisLabel"
                [tickFormatting]="xAxisTickFormatting"
                [tickWidth]="xAxisTickWidth"
                (dimensionsChanged)="updateXAxisHeight($event)">
          </svg:g>
          <svg:g ngx-charts-y-axis
            *ngIf="yAxis"
            [yScale]="yScale"
            [dims]="dims"
            [showGridLines]="showGridLines"
            [showLabel]="showYAxisLabel"
            [labelText]="yAxisLabel"
            [tickFormatting]="yAxisTickFormatting"
            (dimensionsChanged)="updateYAxisWidth($event)">
          </svg:g>
          <svg:g ngx-charts-series-vertical
              [xScale]="xScale"
              [yScale]="yScale"
              [colors]="colors"
              [series]="results"
              [dims]="dims"
              [gradient]="gradient"
              [tooltipDisabled]="tooltipDisabled"
              [tooltipTemplate]="tooltipTemplate"
              [activeEntries]="activeEntries"
              [roundEdges]="roundEdges"
              [animations]="animations"
              (activate)="onActivate($event)"
              (deactivate)="onDeactivate($event)"
              (select)="onClick($event)">
            </svg:g>
        </svg:g>
      </ngx-charts-chart>
    `,
  encapsulation: ViewEncapsulation.Emulated,
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('animationState', [
      transition(':leave', [
        style({
          opacity: 1,
          transform: '*',
        }),
        animate(500, style({ opacity: 0, transform: 'scale(0)' })),
      ]),
    ]),
  ],
})
export class CustomBarVerticalComponent extends BarVerticalComponent {
  @Input()
  xScaleMin: any;
  @Input()
  xScaleMax: any;
  @Input()
  xAxisTickWidth: number;

  xSet: any;
  customXDomain: any[];
  customXScale: any;
  scaleType: string;

  update(): void {
    super.update();
    this.customXDomain = this.getCustomXDomain();
    this.customXScale = this.getCustomXScale(
      this.customXDomain,
      this.dims.width,
    );
  }

  private getCustomXScale(domain: any, width: any): any {
    let scale;

    if (this.scaleType === 'time') {
      scale = scaleTime()
        .range([0, width])
        .domain(domain);
    } else if (this.scaleType === 'linear') {
      scale = scaleLinear()
        .range([0, width])
        .domain(domain);

      if (this.roundDomains) {
        scale = scale.nice();
      }
    } else if (this.scaleType === 'ordinal') {
      scale = scalePoint()
        .range([0, width])
        .padding(0.1)
        .domain(domain);
    }

    return scale;
  }

  private getCustomXDomain(): any[] {
    let values: any[] = this.xDomain;

    this.scaleType = this.getScaleType(values);
    let domain = [];

    if (this.scaleType === 'linear') {
      values = values.map(v => Number(v));
    }

    let min;
    let max;
    if (this.scaleType === 'time' || this.scaleType === 'linear') {
      min = this.xScaleMin ? this.xScaleMin : Math.min(...values);

      max = this.xScaleMax ? this.xScaleMax : Math.max(...values);
    }

    if (this.scaleType === 'time') {
      domain = [new Date(min), new Date(max)];
      this.xSet = [...values].sort((a, b) => {
        const aDate = a.getTime();
        const bDate = b.getTime();
        if (aDate > bDate) {
          return 1;
        }
        if (bDate > aDate) {
          return -1;
        }
        return 0;
      });
    } else if (this.scaleType === 'linear') {
      domain = [min, max];
      // Use compare function to sort numbers numerically
      this.xSet = [...values].sort((a, b) => a - b);
    } else {
      domain = values;
      this.xSet = values;
    }

    return domain;
  }

  private getScaleType(values: any): string {
    let date = true;
    let num = true;

    for (const value of values) {
      if (!this.isDate(value)) {
        date = false;
      }

      if (typeof value !== 'number') {
        num = false;
      }
    }

    if (date) {
      return 'time';
    }
    if (num) {
      return 'linear';
    }
    return 'ordinal';
  }

  private isDate(value: any): boolean {
    return value instanceof Date;
  }
}
