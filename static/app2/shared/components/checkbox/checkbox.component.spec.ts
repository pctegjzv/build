import { Component, DebugElement } from '@angular/core';
import {
  ComponentFixture,
  TestBed,
  fakeAsync,
  flushMicrotasks,
} from '@angular/core/testing';
import { FormsModule, NgModel, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { CheckboxComponent } from 'app2/shared/components/checkbox/checkbox.component';

import { registerCleanup } from '../../../testing/helpers';

@Component({
  template: `<rc-checkbox [(ngModel)]="checked" name="cb"></rc-checkbox>`,
  styles: [],
})
class CheckboxWithFormDirectivesComponent {
  checked = false;
}

describe('CheckboxComponent', () => {
  let fixture: ComponentFixture<any>;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [CheckboxComponent, CheckboxWithFormDirectivesComponent],
      providers: [],
    });
  });

  describe('with ngModel', () => {
    let checkboxDebugElement: DebugElement;
    let checkboxNativeElement: HTMLElement;
    let checkboxInstance: CheckboxComponent;
    let inputElement: HTMLInputElement;
    let ngModel: NgModel;

    beforeEach(fakeAsync(() => {
      fixture = TestBed.createComponent(CheckboxWithFormDirectivesComponent);
      fixture.detectChanges();

      checkboxDebugElement = fixture.debugElement.query(
        By.directive(CheckboxComponent),
      );
      checkboxNativeElement = checkboxDebugElement.nativeElement;
      checkboxInstance = checkboxDebugElement.componentInstance;
      inputElement = <HTMLInputElement>(
        checkboxNativeElement.querySelector('input')
      );
      ngModel = checkboxDebugElement.injector.get<NgModel>(NgModel);

      flushMicrotasks();
    }));

    it('should be in pristine, untouched, and valid states initially', fakeAsync(() => {
      expect(ngModel.valid).toBe(true);
      expect(ngModel.pristine).toBe(true);
      expect(ngModel.touched).toBe(false);
    }));

    it('should toggle checked state on click', () => {
      expect(checkboxInstance.checked).toBe(false);
      expect(ngModel.value).toBe(false);

      inputElement.click();
      fixture.detectChanges();

      expect(checkboxInstance.checked).toBe(true);
      expect(ngModel.value).toBe(true);

      inputElement.click();
      fixture.detectChanges();

      expect(checkboxInstance.checked).toBe(false);
      expect(ngModel.value).toBe(false);
    });
  });

  registerCleanup();
});
