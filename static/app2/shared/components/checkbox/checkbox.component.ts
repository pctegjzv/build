import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  Component,
  EventEmitter,
  HostBinding,
  Input,
  Optional,
  Output,
  Self,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';
import { Subject } from 'rxjs';

import { FormFieldControl } from 'app2/shared/form-field-control';

// Increasing integer for generating unique ids for checkbox components.
let nextUniqueId = 0;

@Component({
  selector: 'rc-checkbox',
  templateUrl: 'checkbox.component.html',
  styleUrls: ['checkbox.component.scss'],
  providers: [{ provide: FormFieldControl, useExisting: CheckboxComponent }],
})
export class CheckboxComponent
  implements FormFieldControl, ControlValueAccessor {
  private _uniqueId = `rc-checkbox-${++nextUniqueId}`;
  protected _disabled = false;
  protected _value = false;
  private _required = false;
  stateChanges = new Subject<void>();
  controlType = 'rc-checkbox';

  @Input()
  id = this._uniqueId;

  /** Name value will be applied to the input element if present */
  @Input()
  name: string | null = null;

  @Input()
  mode: 'default' | 'toggle' = 'default';

  /** Alias name for mode */
  @Input()
  set type(type: 'default' | 'toggle') {
    this.mode = type;
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  /**
   * Underlining value. Truthy value represents 'checked'.
   */
  @Input()
  get value(): boolean {
    return this.ngControl ? this.ngControl.value : this._value;
  }

  set value(value: boolean) {
    if (value === this.value) {
      return;
    }

    this._value = value;
    this.stateChanges.next();
    this.change.emit(value);
  }

  @Input()
  get checked() {
    return this.value;
  }

  set checked(checked: boolean) {
    this.value = checked;
  }

  /**
   * Disabled state.
   */
  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }

    this._disabled = disabled;
    this.stateChanges.next();
  }

  /** Whether the control is in an error state. */
  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);

    return !!(isInvalid && (isDirty || isSubmitted));
  }

  /** Event emitted when the checkbox's `checked` value changes. */
  @Output()
  change = new EventEmitter<boolean>();

  /**
   * Sets the model value. Implemented as part of ControlValueAccessor.
   * @param value Value to be set to the model.
   */
  writeValue(value: any): void {
    this.value = !!value;
  }

  onChange = (value: any) => {
    this.checked = value;
  };

  onTouched = () => {};

  registerOnChange(fn: (_: any) => {}): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => {}): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() private parentForm: NgForm,
    @Optional() private parentFormGroup: FormGroupDirective,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }
}
