import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  Renderer,
  Self,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';
import * as CodeMirror from 'codemirror';
import * as FileSaver from 'file-saver';
import { Subject } from 'rxjs';

import { FloatingPageService } from 'app2/layout/floating-page.service';
import { FormFieldControl } from 'app2/shared/form-field-control';

import 'codemirror/addon/fold/foldgutter.css';
import 'codemirror/addon/lint/lint.css';
import 'codemirror/addon/scroll/simplescrollbars.css';
import 'codemirror/lib/codemirror.css';
import 'codemirror/theme/material.css';

// code extensions
import 'codemirror/mode/powershell/powershell.js';
import 'codemirror/mode/yaml/yaml.js';

// add-ons
import 'codemirror/addon/display/autorefresh';
import 'codemirror/addon/fold/foldcode.js';
import 'codemirror/addon/fold/foldgutter.js';
import 'codemirror/addon/fold/indent-fold.js';
import 'codemirror/addon/lint/lint.js';
import 'codemirror/addon/scroll/simplescrollbars';
import 'codemirror/addon/search/jump-to-line.js';
import 'codemirror/addon/search/search.js';
import 'codemirror/addon/search/searchcursor.js';

// Increasing integer for generating unique ids for checkbox components.
let nextUniqueId = 0;

@Component({
  selector: 'rc-code-editor',
  templateUrl: 'code-editor.component.html',
  styleUrls: ['code-editor.component.scss'],
  providers: [{ provide: FormFieldControl, useExisting: CodeEditorComponent }],
})
export class CodeEditorComponent
  implements
    FormFieldControl,
    ControlValueAccessor,
    OnInit,
    AfterViewInit,
    OnDestroy {
  private _uniqueId = `rc-code-editor-${++nextUniqueId}`;
  private _value = '';
  private _required = false;
  private _readonly = false;
  protected _disabled = false;

  stateChanges = new Subject<void>();
  controlType = 'rc-code-editor';

  @Input()
  id = this._uniqueId;

  @Input()
  config: any = {};
  @Input()
  theme = 'material';
  // @Input() readOnly: any = false;
  @Input()
  get readOnly(): boolean {
    return this._readonly;
  }
  set readOnly(value: boolean) {
    this._readonly = value;
    if (this.instance) {
      this.instance.setOption('readOnly', !!this._readonly);
    }
  }
  @Input()
  mode: any;
  @Input()
  autofocus = false;
  @Input()
  lint: any;
  @Input()
  allowDropFileTypes: any[] = [];
  @Input()
  allowMaximize = true;
  @Input()
  lineNumbers = true;
  @Input()
  gutters: any[] = [];
  @Input()
  allowExport = false;
  @Input()
  allowImport = false;
  @Input()
  exportFileName = 'file';
  @Input()
  exportFileExt = 'yaml';
  /**
   * Whether or not to show editor header
   */
  @Input()
  header = true;
  @Input()
  @HostBinding('class.rc-code-editor-fullscreen')
  fullscreenMode = false;

  @Output()
  change = new EventEmitter();
  @Output()
  blur = new EventEmitter();

  @ViewChild('editor')
  editor: ElementRef;
  @ViewChild('host')
  host: ElementRef;
  @ViewChild('content')
  content: ElementRef;
  @ViewChild('fullscreenEditor')
  fullscreenEditorTemplate: TemplateRef<any>;

  instance: CodeMirror.EditorFromTextArea;

  @Input()
  get value(): string {
    return this.ngControl ? this.ngControl.value : this._value;
  }

  set value(value: string) {
    if (value === this._value) {
      return;
    }
    this._value = value;
    this.onChangeCallback(value);
    this.setCodeMirrorInstanceValue(value);
    this.stateChanges.next();
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }
    this._disabled = disabled;
    this.stateChanges.next();
  }

  /** Whether the control is in an error state. */
  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);

    return !!(isInvalid && (isDirty || isSubmitted));
  }

  constructor(
    private renderer: Renderer,
    private floatingPageService: FloatingPageService,
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() private parentForm: NgForm,
    @Optional() private parentFormGroup: FormGroupDirective,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }

  ngOnInit() {
    this.config = {
      theme: this.theme,
      readOnly: this.readOnly,
      mode: this.mode,
      autofocus: this.autofocus,
      lint: this.lint,
      allowDropFileTypes: this.allowDropFileTypes,
      lineNumbers: this.lineNumbers,
      gutters: this.gutters,
      scrollbarStyle: 'simple',
      ...this.config,
    };
  }

  ngAfterViewInit() {
    if (typeof this.value !== 'string') {
      const elm = this.content.nativeElement;
      const code = elm.innerHTML;
      this.renderer.detachView([].slice.call(elm.childNodes));
      this.host.nativeElement.value = this.cleanCode(code);
    }

    this.instance = CodeMirror.fromTextArea(
      this.host.nativeElement,
      this.config,
    );
    this.instance.on('change', () => {
      this.updateModelValue(this.instance.getValue());
    });

    this.instance.on('blur', () => {
      this.blur.emit(this.instance.getValue());
    });

    if (this.value) {
      this.instance.setValue(this.value);
    }
  }

  ngOnDestroy() {
    this.instance.off('change', null);
    this.instance.off('blur', null);
    if (this.floatingPageService.isShowing()) {
      this.floatingPageService.close();
    }
  }

  onVisible(): void {
    // hidden on init will cause incorrect sizing
    setTimeout(() => {
      this.instance.refresh();
    }, 500);
  }

  toggleFullScreen() {
    if (this.fullscreenMode) {
      this.floatingPageService.close(this.value);
    } else {
      // open another full screen rc-code-editor
      this.floatingPageService.open({
        title: '',
        backdrop: false,
        templateRef: this.fullscreenEditorTemplate,
      });
    }
  }

  import(result: any) {
    this.value = result;
  }

  export() {
    const blob = new Blob([this.value]);
    FileSaver.saveAs(blob, `${this.exportFileName}.${this.exportFileExt}`);
  }

  updateModelValue(value: string): void {
    if (this.value !== value) {
      this._value = value;
      this.onTouchedCallback();
      this.onChangeCallback(value);
      this.change.emit(value);
    }
  }

  cleanCode(code: string): string {
    let lines = code.split('\n');

    // Remove empty lines
    lines = lines.filter(function(line) {
      return line.trim().length > 0;
    });

    // don't mess w/ empties
    if (!lines.length) {
      return '';
    }

    // Make it so each line starts at 0 whitespace
    const firstLineWhitespace = lines[0].match(/^\s*/)[0];
    const startingWhitespaceRegex = new RegExp('^' + firstLineWhitespace);
    lines = lines.map(function(line) {
      return line
        .replace('=""', '') // remove empty values
        .replace(startingWhitespaceRegex, '')
        .replace(/\s+$/, '');
    });

    const codeToParse = lines
      .join('\n')
      .replace(/\{ \{/gi, '{{')
      .replace(/\} \}/gi, '}}')
      // replace with < and > to render HTML in angular 2
      .replace(/&lt;/gi, '<')
      .replace(/&gt;/gi, '>');

    return codeToParse;
  }

  writeValue(value: string) {
    this._value = value;
    this.setCodeMirrorInstanceValue(value);
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  setDisabledState() {}

  private setCodeMirrorInstanceValue(value: string) {
    if (!this.instance) {
      return;
    }
    const currentValue = this.instance.getValue();
    if (value !== undefined && value !== null && value !== currentValue) {
      this.instance.setValue(this.value);
    }
  }

  private onTouchedCallback: () => void = () => {
    // placeholder
  };

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };
}
