import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component, Input } from '@angular/core';

const animation = trigger('activeState', [
  state(
    'active',
    style({
      height: '*',
    }),
  ),
  state(
    'inactive',
    style({
      height: '{{ initHeight }}',
    }),
    { params: { initHeight: '0' } },
  ),
  transition('active<=>inactive', animate('0.5s ease-in-out')),
]);

const animationLiner = trigger('linerState', [
  state(
    'active',
    style({
      height: '0',
    }),
  ),
  state(
    'inactive',
    style({
      height: '20px',
    }),
  ),
  transition('active<=>inactive', animate('0.5s ease-in-out')),
]);

@Component({
  selector: 'rc-collapse-pane',
  styleUrls: ['collapse.component.scss'],
  templateUrl: 'collapse.component.html',
  animations: [animation, animationLiner],
})
export class CollapsePaneComponent {
  private _state = 'inactive';
  private _initHeight: string;

  // true/false 底部垫层
  @Input()
  liner: boolean;

  /**
   * collapse pane container init height in inactive state, default 0.
   * string or number
   */
  @Input()
  get initHeight() {
    return this.hidden ? 'auto' : this._initHeight;
  }

  set initHeight(initHeight: any) {
    this._initHeight = parseInt(initHeight || '0', 10) + 'px';
  }

  @Input()
  hidden = false;
  @Input()
  disabled = false;

  constructor() {}

  toggleState() {
    this._state = this._state === 'active' ? 'inactive' : 'active';
  }
}
