import { PortalModule } from '@angular/cdk/portal';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMenuModule, MatTabsModule } from '@angular/material';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import {
  ButtonModule,
  CardModule,
  CheckboxModule,
  CodeEditorModule,
  DialogModule,
  DropdownModule,
  FormFieldModule,
  FormModule,
  IconModule,
  InputModule,
  RadioModule,
  SelectModule,
  SortModule,
  TableModule as AuiTableModule,
  TagModule,
  TooltipModule,
} from 'alauda-ui';
import {
  PipelineDyformCredentialComponent,
  PipelineDyformEnvComponent,
  PipelineDyformrRepositoryComponent,
  PipelineDyfromConfigMapComponent,
  PipelineServiceContainerComponent,
} from 'app2/features/jenkins/components/pipeline-dynamic-form';
import { ContainerFieldsConfigmapComponent } from 'app2/features/service/create-fields/container-configmap.component';
import {
  BreadcrumbComponent,
  BreadcrumbItemDirective,
} from 'app2/shared/components/breadcrumb/breadcrumb.component';
import { DefaultBreadcrumbComponent } from 'app2/shared/components/breadcrumb/default-breadcrumb.component';
import { IconTemplateDirective } from 'app2/shared/components/button-menu/button-menu.component';
import { ButtonMenuComponent } from 'app2/shared/components/button-menu/button-menu.component';
import { MenuButtonCssStylerDirective } from 'app2/shared/components/button-menu/button-menu.component';
import {
  ButtonComponent,
  ButtonCssStylerDirective,
  IconButtonCssStylerDirective,
} from 'app2/shared/components/button/button.component';
import {
  CardComponent,
  CardHeaderDirective,
} from 'app2/shared/components/card/card.component';
import { CustomXAxisTicksComponent } from 'app2/shared/components/chart/common/custom-x-axis-ticks.component';
import { CustomXAxisComponent } from 'app2/shared/components/chart/common/custom-x-axis.component';
import { CustomAreaChartComponent } from 'app2/shared/components/chart/custom-area-chart.component';
import { CustomBarVerticalComponent } from 'app2/shared/components/chart/custom-bar-vertical.component';
import { CustomLineChartComponent } from 'app2/shared/components/chart/custom-line-chart.component';
import { CheckboxComponent } from 'app2/shared/components/checkbox/checkbox.component';
import { CodeEditorComponent } from 'app2/shared/components/code-editor/code-editor.component';
import { CollapsePaneComponent } from 'app2/shared/components/collapse/collapse.component';
import { CustomizeSizeComponent } from 'app2/shared/components/customize-size/customize-size.component';
import { DynamicFormFieldComponent } from 'app2/shared/components/dynamic-form/dynamic-form-field.component';
import { DynamicFormComponent } from 'app2/shared/components/dynamic-form/dynamic-form.component';
import { FlexCellComponent } from 'app2/shared/components/flex-list/flex-cell/flex-cell.component';
import { FlexListComponent } from 'app2/shared/components/flex-list/flex-list.component';
import { FormContainerComponent } from 'app2/shared/components/form-container/form-container.component';
import { FooterContentDirective } from 'app2/shared/components/form-container/form-footer-content.directive';
import { FormAddonDirective } from 'app2/shared/components/form-field/form-addon.directive';
import { FormFieldColumnComponent } from 'app2/shared/components/form-field/form-field-column.component';
import { FormFieldComponent } from 'app2/shared/components/form-field/form-field.component';
import { FormPrefixDirective } from 'app2/shared/components/form-field/form-prefix.directive';
import { FormSuffixDirective } from 'app2/shared/components/form-field/form-suffix.directive';
import { ColComponent } from 'app2/shared/components/layout/col/col.component';
import { RowComponent } from 'app2/shared/components/layout/row/row.component';
import { ModalContainerComponent } from 'app2/shared/components/modal-container/modal-container.component';
import { PaginationComponent } from 'app2/shared/components/pagination/pagination.component';
import { Pagination2Component } from 'app2/shared/components/pagination2/pagination.component';
import { PasswordStrengthLabelComponent } from 'app2/shared/components/password-strength-label/password-strength-label.component';
import { PasswordStrengthComponent } from 'app2/shared/components/password-strength/password-strength.component';
import { SearchInputComponent } from 'app2/shared/components/search-input/search-input.component';
import {
  DropdownMaxLengthValidatorDirective,
  DropdownMinLengthValidatorDirective,
  MultiSelectionDropdownComponent,
} from 'app2/shared/components/select/multi-selection-dropdown.component';
import { OptionNotFoundIconComponent } from 'app2/shared/components/select/option-not-found-icon.component';
import { RadioGroupComponent } from 'app2/shared/components/select/radio-group.component';
import { SingleSelectionDropdownComponent } from 'app2/shared/components/select/single-selection-dropdown.component';
import { ServiceStatusBadgeComponent } from 'app2/shared/components/status-badge/service-status-badge.component';
import { StatusPopoverComponent } from 'app2/shared/components/status-popover/status-popover.component';
import { SuggestionInputComponent } from 'app2/shared/components/suggestion-input/suggestion-input.component';
import { TableModule } from 'app2/shared/components/table/table.module';
import {
  TabDirective,
  TabGroupComponent,
} from 'app2/shared/components/tabs/tab-group.component';
import {
  TabNavDirective,
  VerticalTabComponent,
  VerticalTabsComponent,
} from 'app2/shared/components/tabs/vertical-tabs.component';
import { TagComponent } from 'app2/shared/components/tag/tag.component';
import { TagsInputComponent } from 'app2/shared/components/tags-input/tags-input.component';
import { TagsLabelComponent } from 'app2/shared/components/tags-label/tags-label.ocmponent';
import { InlineNotificationComponent } from 'app2/shared/components/toast-container/inline-notification.component';
import { ViewPagerItemComponent } from 'app2/shared/components/view-pager/view-pager-item/view-pager-item.component';
import { ViewPagerComponent } from 'app2/shared/components/view-pager/view-pager.component';
import { YamlCommentFormComponent } from 'app2/shared/components/yaml-comment-form/yaml-comment-form.component';
import { DirectivesModule } from 'app2/shared/directives/directives.module';
import { PipesModule } from 'app2/shared/pipes/pipes.module';
import { TranslateModule } from 'app2/translate/translate.module';
import { ZeroStateComponent } from 'app_user/shared/components/zero-state/zero-state.component';

import { AlertBoxComponent } from './alert-box/alert-box.component';
import { ConfirmBoxComponent } from './confirm-box/confirm-box.component';
import { LoadingMaskComponent } from './loading-mask/loading-mask.component';
import { StatusBadgeComponent } from './status-badge/status-badge.component';

//import { ContainerFieldsEnvvarsComponent } from 'app2/shared/features/service-create-fields/container-envvars.component';

const THIRDY_PARTY_MODULES = [MatTabsModule, MatMenuModule, NgxChartsModule];

const COMPONENTS = [
  LoadingMaskComponent,
  StatusBadgeComponent,
  ServiceStatusBadgeComponent,
  ConfirmBoxComponent,
  AlertBoxComponent,
  FormContainerComponent,
  FooterContentDirective,
  SingleSelectionDropdownComponent,
  MultiSelectionDropdownComponent,
  DropdownMinLengthValidatorDirective,
  DropdownMaxLengthValidatorDirective,
  RadioGroupComponent,
  TagsInputComponent,
  FormFieldComponent,
  FormFieldColumnComponent,
  DynamicFormComponent,
  FormAddonDirective,
  FormPrefixDirective,
  FormSuffixDirective,
  CheckboxComponent,
  ButtonCssStylerDirective,
  IconButtonCssStylerDirective,
  ButtonComponent,
  StatusPopoverComponent,
  SuggestionInputComponent,
  BreadcrumbItemDirective,
  BreadcrumbComponent,
  DefaultBreadcrumbComponent,
  PaginationComponent,
  Pagination2Component,
  ButtonMenuComponent,
  MenuButtonCssStylerDirective,
  IconTemplateDirective,
  CodeEditorComponent,
  CustomizeSizeComponent,
  CardComponent,
  CardHeaderDirective,
  ModalContainerComponent,
  SearchInputComponent,
  YamlCommentFormComponent,
  RowComponent,
  ColComponent,
  FlexListComponent,
  FlexCellComponent,
  ViewPagerComponent,
  ViewPagerItemComponent,
  TabGroupComponent,
  TabDirective,
  CollapsePaneComponent,
  TagComponent,
  VerticalTabsComponent,
  VerticalTabComponent,
  TabNavDirective,
  InlineNotificationComponent,
  CustomBarVerticalComponent,
  CustomLineChartComponent,
  CustomAreaChartComponent,
  PasswordStrengthComponent,
  PasswordStrengthLabelComponent,
  CustomXAxisComponent,
  CustomXAxisTicksComponent,
  PipelineServiceContainerComponent,
  PipelineDyformCredentialComponent,
  PipelineDyformrRepositoryComponent,
  PipelineDyformEnvComponent,
  PipelineDyfromConfigMapComponent,
  ContainerFieldsConfigmapComponent,
  TagsLabelComponent,
  ZeroStateComponent,
];

const AuiModule = [
  IconModule,
  ButtonModule,
  SortModule,
  AuiTableModule,
  CodeEditorModule,
  CardModule,
  TooltipModule,
  DialogModule,
  FormFieldModule,
  InputModule,
  CheckboxModule,
  RadioModule,
  SelectModule,
  FormModule,
  DropdownModule,
  TagModule,
];

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    TranslateModule,
    TableModule,
    DirectivesModule,
    PipesModule,
    FormsModule,
    ReactiveFormsModule,
    PortalModule,
    ...THIRDY_PARTY_MODULES,
    ...AuiModule,
  ],
  declarations: [
    ...COMPONENTS,
    DynamicFormFieldComponent,
    OptionNotFoundIconComponent,
  ],
  exports: [...COMPONENTS, ...THIRDY_PARTY_MODULES, TableModule],
  providers: [],
  entryComponents: [
    LoadingMaskComponent,
    ConfirmBoxComponent,
    AlertBoxComponent,
    ModalContainerComponent,
  ],
})
export class ComponentsModule {}
