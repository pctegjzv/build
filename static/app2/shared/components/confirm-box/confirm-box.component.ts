import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  templateUrl: './confirm-box.component.html',
  styleUrls: ['./confirm-box.component.scss'],
})
export class ConfirmBoxComponent {
  @Input()
  title: string;
  @Input()
  content: string;
  @Output()
  confirmed = new EventEmitter<boolean>();
  @Input()
  confirmText = 'confirm';
  @Input()
  cancelText = 'cancel';

  confirm(yes: boolean) {
    this.confirmed.next(yes);
  }
}
