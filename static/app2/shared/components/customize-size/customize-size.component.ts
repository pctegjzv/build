/**
 * 需要注意的是，在普通的表单中，请不要使用[value]接口绑定数据，而是用[(ngModel)]
 */
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnInit,
  Optional,
  Output,
  Self,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';
import { Subject } from 'rxjs';

import { FormFieldControl } from 'app2/shared/form-field-control';

// import { INSTANCE_SIZE_CONFIG } from 'app2/core/tokens.ts';

let nextUniqueId = 0;
interface Size {
  size: 'CUSTOMIZED';
  cpu: number;
  mem: number;
}

@Component({
  selector: 'rc-customize-size',
  templateUrl: './customize-size.component.html',
  styleUrls: ['./customize-size.component.scss'],
  providers: [
    { provide: FormFieldControl, useExisting: CustomizeSizeComponent },
  ],
})
export class CustomizeSizeComponent
  implements FormFieldControl, ControlValueAccessor, OnInit {
  @Input()
  config: any;
  @Output()
  change = new EventEmitter<Size>();

  private _uniqueId = `rc-customize-size-${++nextUniqueId}`;
  protected _value: Size = {
    size: 'CUSTOMIZED',
    cpu: 0,
    mem: 0,
  };
  protected _disabled = false;
  private _required = true;
  unitOptions = [
    {
      name: 'MB',
      value: 1,
    },
    {
      name: 'GB',
      value: 1024,
    },
  ];
  baseUnit = 1;
  _memDisplay = 0;

  stateChanges = new Subject<void>();
  controlType = 'rc-customize-size';

  @Input()
  id = this._uniqueId;

  /** Name value will be applied to the input element if present */
  @Input()
  name: string | null = null;

  /**
   * 为了以后可以升级到卡片选择实例大小先留着
   */
  // @Input() presetSize: 'XXS' | 'XS' | 'S' | 'M' | 'L' | 'XL';

  // /** Alias name for presetSize */
  // @Input() set size(preset: 'XXS' | 'XS' | 'S' | 'M' | 'L' | 'XL') {
  //   this.presetSize = preset;
  // }

  ngOnInit() {
    if (this.value && this.value.mem % 1024 === 0) {
      this._memDisplay = this.value.mem / 1024;
      this.baseUnit = 1024;
    }
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  /**
   * Disabled state.
   */
  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }

    this._disabled = disabled;
    this.stateChanges.next();
  }

  /**
   * Underlining value. Truthy value represents 'Size'.
   */
  @Input()
  get value(): Size {
    return this.ngControl ? this.ngControl.value : this._value;
  }

  set value(value: Size) {
    if (value === this.value) {
      return;
    }
    this._value = value;
    this.stateChanges.next();
    this.onChange(value);
    this.change.emit(value);
  }

  @Input()
  get cpu() {
    return this.value ? this.value.cpu : 0;
  }
  set cpu(cpu: number) {
    this.value = { ...this.value, cpu };
  }

  @Input()
  get memDisplay() {
    return this._memDisplay;
  }
  set memDisplay(mem: any) {
    this._memDisplay = mem;
    this.value = { ...this.value, mem: Math.ceil(mem * this.baseUnit) };
  }

  /** Whether the control is in an error state. */
  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);

    return !!(isInvalid && (isDirty || isSubmitted));
  }

  /**
   * Sets the model value. Implemented as part of ControlValueAccessor.
   * @param value Value to be set to the model.
   */
  writeValue(value: any): void {
    this._value = value;
  }

  onChange = (value: any) => {
    this.value = value;
  };

  onTouched = () => {};

  registerOnChange(fn: (_: any) => {}): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => {}): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onUnitChange($event: any) {
    this.baseUnit = $event.value;
    this.value = {
      ...this.value,
      mem: Math.ceil(this.memDisplay * this.baseUnit),
    };
  }

  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() private parentForm: NgForm,
    @Optional() private parentFormGroup: FormGroupDirective,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }
}
