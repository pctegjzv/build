import {
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { Subscription, merge } from 'rxjs';

import { DynamicFormComponent } from 'app2/shared/components/dynamic-form/dynamic-form.component';
import { DynamicFormFieldDefinition } from 'app2/shared/form-field-control';

@Component({
  selector: 'rc-dynamic-form-field',
  templateUrl: 'dynamic-form-field.component.html',
  styleUrls: ['dynamic-form-field.component.scss'],
})
export class DynamicFormFieldComponent implements OnInit, OnDestroy {
  @Input()
  config: DynamicFormFieldDefinition;
  @Input()
  form: FormGroup;
  control: AbstractControl;

  private sub: Subscription;
  constructor(
    private cdr: ChangeDetectorRef,
    private dynamicForm: DynamicFormComponent,
  ) {}

  ngOnInit() {
    this.control = this.form.get(this.config.name);
    this.sub = merge(
      this.dynamicForm.valueChanges,
      this.dynamicForm.statusChanges,
    ).subscribe(() => this.cdr.detectChanges());
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
