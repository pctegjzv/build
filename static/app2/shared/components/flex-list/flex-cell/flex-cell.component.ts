import {
  Component,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { MessagerService } from 'app2/shared/components/flex-list/messager.service';

@Component({
  // tslint:disable-next-line
  selector: '[rcFlexCell]',
  template: '<ng-content></ng-content>',
  styleUrls: ['flex-cell.component.scss'],
})
export class FlexCellComponent implements OnInit, OnDestroy {
  @Input()
  rcFlexCellHolder: boolean;

  @HostBinding('class.rc-flex-cell')
  readonly bindClass = true;
  @HostBinding('style.margin-left')
  @HostBinding('style.margin-right')
  marginHor: string;
  @HostBinding('style.margin-bottom')
  marginVer: string;
  @HostBinding('style.width')
  width: string;
  @HostBinding('style.height')
  height: string;

  subscriptions: Subscription[] = [];

  constructor(private messagerService: MessagerService) {}

  ngOnInit(): void {
    const marginSub = this.messagerService.margin$
      .pipe(filter(data => !!data))
      .subscribe(margin => {
        this.marginHor = margin[0] / 2 + 'px';
        if (!this.rcFlexCellHolder) {
          this.marginVer = margin[1] + 'px';
        }
      });
    const widthSub = this.messagerService.width$
      .pipe(filter(data => !!data))
      .subscribe(width => {
        this.width = width;
      });
    const heightSub = this.messagerService.height$
      .pipe(filter(data => !!data))
      .subscribe(height => {
        this.height = !this.rcFlexCellHolder ? height : '0';
      });
    this.subscriptions.push(marginSub, widthSub, heightSub);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }
}
