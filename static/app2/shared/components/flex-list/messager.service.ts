import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';

@Injectable()
export class MessagerService {
  private marginSource$ = new ReplaySubject<[number, number]>(1);
  private widthSource$ = new ReplaySubject<string>(1);
  private heightSource$ = new ReplaySubject<string>(1);

  margin$ = this.marginSource$.asObservable();
  width$ = this.widthSource$.asObservable();
  height$ = this.heightSource$.asObservable();

  updateMargin(margin: [number, number]): void {
    this.marginSource$.next(margin);
  }

  updateWidth(width: string): void {
    this.widthSource$.next(width);
  }

  updateHeight(height: string): void {
    this.heightSource$.next(height);
  }
}
