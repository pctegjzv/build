import { CommonModule } from '@angular/common';
import { Component, NgModule } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormContainerComponent } from 'app2/shared/components/form-container/form-container.component';

import { registerCleanup } from 'app2/testing/helpers';

@Component({
  template: `
    <rc-form-container>
      <form>
        THIS IS THE FORM CONTENT
      </form>
      <ng-container *rcFooterContent>
        <button class="ui button rb-btn-primary">
          {{ 'ok' }}
        </button>
        <button class="ui button rb-btn-default">
          {{ 'cancel' }}
        </button>
      </ng-container>
    </rc-form-container>
  `,
  styles: [],
})
class TestComponent {}

@NgModule({
  imports: [CommonModule],
  declarations: [TestComponent, FormContainerComponent],
  entryComponents: [TestComponent],
  providers: [],
})
class TestModule {}

describe('LoadingMaskComponent', () => {
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestModule],
      providers: [],
    });

    fixture = TestBed.createComponent(TestComponent);
  });

  it('can correctly render form container', () => {
    fixture.detectChanges();

    // No real tests yet
  });

  registerCleanup();
});
