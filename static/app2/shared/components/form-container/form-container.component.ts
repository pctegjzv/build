import {
  Component,
  ContentChild,
  ElementRef,
  OnInit,
  TemplateRef,
} from '@angular/core';
import $ from 'jquery';

import { FooterContentDirective } from './form-footer-content.directive';

@Component({
  selector: 'rc-form-container',
  templateUrl: 'form-container.component.html',
  styleUrls: ['form-container.component.scss'],
})
export class FormContainerComponent implements OnInit {
  /* Is the form residing within a modal? */
  isInModal: boolean;

  @ContentChild(FooterContentDirective, { read: TemplateRef })
  footerContentTemplate: FooterContentDirective;

  constructor(private el: ElementRef) {}

  ngOnInit(): void {
    this.isInModal = !!$(this.el.nativeElement).parents('.rb-modal, .rc-modal')
      .length;
  }
}
