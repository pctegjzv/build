import { Directive, TemplateRef } from '@angular/core';

@Directive({
  selector: '[rcFooterContent]',
})
export class FooterContentDirective {
  constructor(public template: TemplateRef<any>) {}
}
