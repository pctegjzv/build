import { Directive } from '@angular/core';

@Directive({
  selector: '[rcFormAddon]',
})
export class FormAddonDirective {}
