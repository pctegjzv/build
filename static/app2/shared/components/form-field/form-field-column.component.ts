import { ChangeDetectionStrategy, Component } from '@angular/core';

import { FormFieldComponent } from 'app2/shared/components/form-field/form-field.component';

@Component({
  selector: 'rc-form-field-column',
  templateUrl: 'form-field-column.component.html',
  styleUrls: ['form-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormFieldColumnComponent extends FormFieldComponent {}
