import {
  AfterContentInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  HostBinding,
  Input,
  OnInit,
  Optional,
  SkipSelf,
} from '@angular/core';

import { FormAddonDirective } from 'app2/shared/components/form-field/form-addon.directive';
import { FormPrefixDirective } from 'app2/shared/components/form-field/form-prefix.directive';
import { FormSuffixDirective } from 'app2/shared/components/form-field/form-suffix.directive';
import { FormFieldControl } from 'app2/shared/form-field-control';
import { ErrorMapper } from 'app2/shared/types';

/**
 * Container for form controls that applies Rubick styling and behavior.
 * Interface design & implementation inspired by NgMaterial.
 *
 * https://material.angular.io/components/input
 *
 * 特性：
 * - 样式封装
 * - 规范化addon/prefix/suffix显示
 * - 自动添加required标记
 * - 添加输入hint标签
 * - 支持多层FormFieldComponent嵌套
 */
@Component({
  selector: 'rc-form-field',
  templateUrl: 'form-field.component.html',
  styleUrls: ['form-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormFieldComponent implements OnInit, AfterContentInit {
  /** Text for the form field label. */
  @Input()
  label = '';
  /** Text for the form field hint. */
  @Input()
  hintLabel = '';

  /** Hide the default errors tooltip */
  @Input()
  hideErrors = false;

  /** Errors mapper for error tooltip */
  @Input()
  errorsMapper: ErrorMapper;
  // tslint:disable-next-line:no-input-rename
  @Input('class')
  className: string;

  @HostBinding('class')
  additionalCssClass: any = '';

  @ContentChild(FormFieldControl)
  formFieldControl: FormFieldControl;
  @ContentChild(FormAddonDirective)
  formAddon: FormAddonDirective;
  @ContentChild(FormPrefixDirective)
  formPrefix: FormPrefixDirective;
  @ContentChild(FormSuffixDirective)
  formSuffix: FormSuffixDirective;

  // Whether the row should take full row space.
  @Input()
  shouldFlexRow: boolean;

  constructor(
    @SkipSelf()
    @Optional()
    public parentField: FormFieldComponent,
    private changeDetectorRef: ChangeDetectorRef,
  ) {}

  ngOnInit(): void {}

  ngAfterContentInit(): void {
    if (this.formFieldControl) {
      this.additionalCssClass = `rc-form-field-type-${
        this.formFieldControl.controlType
      }`;

      if (this.className) {
        this.additionalCssClass += ' ' + this.className;
      }

      this.formFieldControl.stateChanges.subscribe(() => {
        this.changeDetectorRef.detectChanges();
      });
      if (this.shouldFlexRow === undefined) {
        this.shouldFlexRow =
          !this.formFieldControl.controlType ||
          this.formFieldControl.controlType === 'rc-input';
      }
    } else {
      throw new Error('Form field not properly configured!');
    }
  }
}
