import { Directive } from '@angular/core';

@Directive({
  selector: '[rcFormPrefix]',
})
export class FormPrefixDirective {}
