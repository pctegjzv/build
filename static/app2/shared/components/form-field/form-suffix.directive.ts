import { Directive } from '@angular/core';

@Directive({
  selector: '[rcFormSuffix]',
})
export class FormSuffixDirective {}
