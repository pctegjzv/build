import { Component, HostBinding, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { MessagerService } from 'app2/shared/components/layout/messager.service';

@Component({
  // tslint:disable-next-line
  selector: '[rcCol]',
  template: '<ng-content></ng-content>',
  styleUrls: ['col.component.scss'],
})
export class ColComponent implements OnDestroy {
  // number of column the grid spans
  @Input()
  rcColSpan = 1;

  @HostBinding('style.padding-left')
  @HostBinding('style.padding-right')
  padding = '0';
  @HostBinding('class.rc-col')
  readonly bindClass = true;
  base = 24;

  subscriptions: Subscription[] = [];

  constructor(private messager: MessagerService) {
    const gutterSub = this.messager.gutter$.subscribe(gutter => {
      this.padding = gutter / 2 + 'px';
    });
    const baseSub = this.messager.base$.subscribe(base => {
      this.base = base;
    });
    this.subscriptions.push(gutterSub, baseSub);
  }

  @HostBinding('style.width')
  get colWidth(): string {
    return (this.rcColSpan / this.base) * 100 + '%';
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }
}
