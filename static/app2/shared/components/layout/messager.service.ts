import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';

@Injectable()
export class MessagerService {
  private baseSource$ = new ReplaySubject<number>(1);
  private gutterSource$ = new ReplaySubject<number>(1);

  base$ = this.baseSource$.asObservable();
  gutter$ = this.gutterSource$.asObservable();

  updateBase(base: number): void {
    this.baseSource$.next(base);
  }

  updateGutter(gutter: number): void {
    this.gutterSource$.next(gutter);
  }
}
