import {
  Component,
  HostBinding,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';

import { MessagerService } from 'app2/shared/components/layout/messager.service';

/*
* 栅格式布局，结合 rcCol 使用
* */

@Component({
  // tslint:disable-next-line
  selector: '[rcRow]',
  providers: [MessagerService],
  template: '<ng-content></ng-content>',
  styleUrls: ['row.component.scss'],
})
export class RowComponent implements OnChanges {
  // basic columns
  @Input()
  rcRowBase = 24;
  // grid spacing
  @Input()
  rcRowGutter = 0;

  @HostBinding('style.margin-left')
  @HostBinding('style.margin-right')
  margin = '0';
  @HostBinding('class.rc-row')
  readonly bindClass = true;

  constructor(private messager: MessagerService) {
    this.messager.updateGutter(this.rcRowGutter);
    this.messager.updateBase(this.rcRowBase);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.rcRowGutter) {
      this.margin = -changes.rcRowGutter.currentValue / 2 + 'px';
      this.messager.updateGutter(changes.rcRowGutter.currentValue);
    }
    if (changes.rcRowBase) {
      this.messager.updateBase(changes.rcRowBase.currentValue);
    }
  }
}
