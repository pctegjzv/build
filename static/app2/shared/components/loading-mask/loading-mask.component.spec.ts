import { CommonModule } from '@angular/common';
import { Component, Input, NgModule } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { registerCleanup } from '../../../testing/helpers';

import { LoadingMaskComponent } from './loading-mask.component';

const WIDTH = 500;
const HEIGHT = 600;

@Component({
  template: `
    <rc-loading-mask [loading]="loading"></rc-loading-mask>`,
  styles: [
    `
      :host {
        width: ${WIDTH}px;
        height: ${HEIGHT}px;
      }
    `,
  ],
})
class TestComponent {
  @Input()
  loading: boolean;
}

@NgModule({
  imports: [CommonModule],
  declarations: [TestComponent, LoadingMaskComponent],
  entryComponents: [TestComponent],
  providers: [],
})
class TestModule {}

describe('LoadingMaskComponent', () => {
  let comp: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestModule],
      providers: [],
    });

    fixture = TestBed.createComponent(TestComponent);

    comp = fixture.componentInstance;
  });

  it('should show spinner and visible when loading=true and vice verse', () => {
    const el: HTMLElement = fixture.nativeElement;
    comp.loading = true;
    fixture.detectChanges();

    // Check visibility and its size
    expect(el.querySelector('rc-loading-mask').clientWidth).toBe(WIDTH);
    expect(el.querySelector('rc-loading-mask').clientHeight).toBe(HEIGHT);

    comp.loading = false;
    fixture.detectChanges();
    // Check visibility and its size
    expect(el.querySelector('rc-loading-mask').clientWidth).toBe(0);
    expect(el.querySelector('rc-loading-mask').clientHeight).toBe(0);
  });

  registerCleanup();
});
