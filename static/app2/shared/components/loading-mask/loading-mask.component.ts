import {
  AfterContentInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostBinding,
  Input,
  OnChanges,
  OnDestroy,
} from '@angular/core';
import ResizeSensor from 'css-element-queries/src/ResizeSensor';
import { debounce } from 'lodash';

/**
 * A simple loading mask component which will show a spinner and greyed background
 * when loading attribute is true.
 */
@Component({
  selector: 'rc-loading-mask',
  template: `
    <div class="ui loader active" [ngClass]="spinnerSize"></div>`,
  styleUrls: ['loading-mask.component.scss'],
})
export class LoadingMaskComponent
  implements AfterContentInit, OnDestroy, OnChanges {
  @Input()
  loading: any;

  @HostBinding('hidden')
  get hidden() {
    return !this.loading || this.loading === 'false';
  }

  spinnerSize: string;

  private resizeSensor: any;

  private updateSpinnerSize = debounce(() => {
    this.spinnerSize = this.getSpinnerSize();
    this.cdr.markForCheck();
  }, 50);

  constructor(private el: ElementRef, private cdr: ChangeDetectorRef) {}

  ngOnChanges(): void {
    this.updateSpinnerSize();
  }

  ngAfterContentInit(): void {
    this.spinnerSize = this.getSpinnerSize();
    this.resizeSensor = new ResizeSensor(this.el.nativeElement, () => {
      this.updateSpinnerSize();
    });
  }

  ngOnDestroy(): void {
    if (this.resizeSensor) {
      this.resizeSensor.detach();
    }
  }

  private getSpinnerSize(): string {
    const height = (this.el.nativeElement as HTMLElement).clientHeight;
    let size = 'large';
    if (height <= 128) {
      size = 'medium';
    }
    if (height <= 64) {
      size = 'small';
    }
    return size;
  }
}
