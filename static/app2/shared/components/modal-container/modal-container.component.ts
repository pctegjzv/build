import {
  ComponentPortal,
  PortalHostDirective,
  TemplatePortal,
} from '@angular/cdk/portal';
import {
  Component,
  ComponentRef,
  EmbeddedViewRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';

/**
 * @see ModalService
 */
@Component({
  templateUrl: 'modal-container.component.html',
  styleUrls: ['modal-container.component.scss'],
})
export class ModalContainerComponent {
  @Input()
  title = '';
  @Output()
  close = new EventEmitter();

  @ViewChild(PortalHostDirective)
  private portalHost: PortalHostDirective;

  /**
   * Attach a ComponentPortal as content to this modal container.
   * @param portal Portal to be attached as the modal content.
   */
  attachComponentPortal<T>(portal: ComponentPortal<T>): ComponentRef<T> {
    return this.portalHost.attachComponentPortal(portal);
  }

  /**
   * Attach a TemplatePortal as content to this modal container.
   * @param portal Portal to be attached as the modal content.
   */
  attachTemplatePortal<C>(portal: TemplatePortal<C>): EmbeddedViewRef<C> {
    return this.portalHost.attachTemplatePortal(portal);
  }
}
