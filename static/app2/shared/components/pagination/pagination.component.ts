import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChange,
} from '@angular/core';

@Component({
  selector: 'rc-pagination',
  templateUrl: 'pagination.component.html',
  styleUrls: ['pagination.component.scss'],
})
export class PaginationComponent implements OnInit, OnChanges {
  @Input()
  count: number;
  @Input()
  currentPage = 1;
  @Input()
  pageSize: number;
  @Input()
  disabled: boolean;

  @Output()
  pageChanged = new EventEmitter<number>();

  totalPage = 0;
  initialized = false;

  constructor() {}

  ngOnInit() {}

  ngOnChanges({
    count,
    pageSize,
  }: {
    count: SimpleChange;
    pageSize: SimpleChange;
  }) {
    if (!this.initialized) {
      this.initPages();
    } else if (
      (count && count.currentValue !== undefined) ||
      (pageSize && pageSize.currentValue)
    ) {
      this.refreshPages();
    }
  }

  initPages() {
    if (!this.pageSize || !this.count) {
      return;
    }
    this.totalPage = Math.ceil(this.count / this.pageSize);
    this.initialized = true;
  }

  refreshPages() {
    if (this.count === 0) {
      this.currentPage = 0;
      this.totalPage = 0;
    } else {
      this.totalPage = Math.ceil(this.count / this.pageSize);
      if (this.currentPage > this.totalPage || this.currentPage < 1) {
        this.currentPage = 1;
        this.pageChanged.emit(this.currentPage);
      }
    }
  }

  get prevDisabled() {
    return this.disabled || this.currentPage <= 1;
  }

  get nextDisabled() {
    return this.disabled || this.currentPage >= this.totalPage;
  }

  prev() {
    this.pageChanged.emit(--this.currentPage);
  }

  next() {
    this.pageChanged.emit(++this.currentPage);
  }

  keydown(inputElement: HTMLInputElement, event: KeyboardEvent) {
    if (this.disabled) {
      return;
    }
    switch (event.key.toLowerCase()) {
      case 'enter':
        const page: number = +inputElement.value;
        if (page > this.totalPage || page <= 0) {
          inputElement.value = this.currentPage.toString();
          return;
        }
        this.currentPage = page;
        this.pageChanged.emit(this.currentPage);
        event.preventDefault();
        event.stopImmediatePropagation();
        break;
    }
  }
}
