import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { clamp, isFinite, mapValues, range } from 'lodash';
import {
  BehaviorSubject,
  Observable,
  Subject,
  Subscription,
  merge,
} from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  scan,
  shareReplay,
  startWith,
  switchMap,
  withLatestFrom,
} from 'rxjs/operators';

interface State {
  show: boolean;
  pages: number[];
  currentPage: number;
  totalPage: number;
  forwardEnabled: boolean;
  backwardEnabled: boolean;
}

const MAX_PAGE = 5;

@Component({
  selector: 'rc-pagination2',
  templateUrl: 'pagination.component.html',
  styleUrls: ['pagination.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class Pagination2Component implements OnInit, OnChanges {
  @Input()
  count: number;
  @Input()
  pageSize: number;
  @Input()
  page: number;
  @Input()
  disabled: boolean;
  @Output()
  pageChanged = new EventEmitter<number>();

  state$: Observable<State>;
  inputValue$: Observable<number>;

  // behaviors
  inputChange$ = new Subject<number>();
  prev$ = new Subject<void>();
  next$ = new Subject<void>();
  last$ = new Subject<void>();
  first$ = new Subject<void>();

  private _subs: Subscription[] = [];
  private _changes$ = new BehaviorSubject<SimpleChanges>(null);

  constructor() {
    this.state$ = this._changes$.pipe(
      map(changes => mapValues(changes, change => change.currentValue)),
      scan((prev = {}, changes) => Object.assign({}, prev, changes)),
      map(changes => {
        const { count, pageSize, page } = changes;

        if (!isFinite(count) || !isFinite(pageSize)) {
          return {
            show: false,
            pages: [],
            currentPage: null,
            totalPage: 0,
            forwardEnabled: false,
            backwardEnabled: false,
            inputValue: null,
          };
        }

        const totalPage = Math.ceil(count / pageSize);
        const currentPage = page || 1;
        const start = Math.max(
          Math.min(
            currentPage - Math.floor(MAX_PAGE / 2),
            totalPage - MAX_PAGE + 1,
          ),
          1,
        );
        const end = Math.min(start + MAX_PAGE, totalPage + 1);
        const pages = range(start, end, 1);

        return {
          show: totalPage > 1 && page >= 1 && page <= totalPage,
          pages,
          currentPage,
          totalPage,
          forwardEnabled: currentPage < totalPage,
          backwardEnabled: currentPage > 1,
        };
      }),
      shareReplay(1),
    );
  }

  ngOnInit() {
    const redirect$ = this.state$.pipe(
      filter(state => state.totalPage > 1),
      filter(
        state => state.currentPage < 1 || state.currentPage > state.totalPage,
      ),
      map(state => clamp(state.currentPage, 1, state.totalPage)),
      distinctUntilChanged(),
      shareReplay(1),
    );

    const inputFallback$ = this.state$.pipe(
      switchMap(state =>
        this.inputChange$.pipe(
          map(value => clamp(value, 1, state.totalPage)),
          startWith(null),
        ),
      ),
      debounceTime(0),
    );

    this.inputValue$ = merge(this.inputChange$, inputFallback$).pipe(
      shareReplay(1),
    );

    const changePage$ = merge(
      this.prev$.pipe(map(() => (state: State) => state.currentPage - 1)),
      this.next$.pipe(map(() => (state: State) => state.currentPage + 1)),
      this.first$.pipe(map(() => () => 1)),
      this.last$.pipe(map(() => (state: State) => state.totalPage)),
    ).pipe(withLatestFrom(this.state$, (fn, state) => fn(state)));

    this._subs.push(
      merge(
        redirect$.pipe(debounceTime(0)),
        this.inputValue$.pipe(
          filter(value => !!value),
          debounceTime(500),
        ),
        changePage$,
      ).subscribe(this.pageChanged),
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    this._changes$.next(changes);
  }
}
