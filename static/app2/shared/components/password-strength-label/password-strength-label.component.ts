import { Component, Input, OnInit } from '@angular/core';

import { PatternHelperService } from 'app2/shared/services/features/pattern-helper.service';

@Component({
  selector: 'rc-app-password-strength-label',
  templateUrl: './password-strength-label.component.html',
  styleUrls: ['./password-strength-label.component.scss'],
})
export class PasswordStrengthLabelComponent implements OnInit {
  levels: Array<any> = [];
  @Input()
  password: string;

  get level(): number {
    let level = 0;
    if (this.password) {
      const strength = this.patternHelper.getPasswordStrength(this.password);
      level = 1;
      if (strength > 60) {
        level = 3;
      } else if (strength >= 45) {
        level = 2;
      }
    }
    return level;
  }

  constructor(private patternHelper: PatternHelperService) {}

  ngOnInit() {
    this.levels = new Array(3);
  }

  isLevelActive(index: number) {
    const levelIndex = 3 - index;
    return this.level >= levelIndex;
  }
}
