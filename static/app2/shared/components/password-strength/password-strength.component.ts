import { Component, Input } from '@angular/core';

import { PatternHelperService } from 'app2/shared/services/features/pattern-helper.service';

@Component({
  selector: 'rc-app-password-strength',
  templateUrl: './password-strength.component.html',
})
export class PasswordStrengthComponent {
  @Input()
  password: string;

  constructor(private patternHelper: PatternHelperService) {}

  getStrengthLabelAndColor() {
    const strength = this.patternHelper.getPasswordStrength(this.password);

    let label = 'weak';
    let color = '#f00';
    if (strength > 60) {
      label = 'strong';
      color = '#158900';
    } else if (strength >= 45) {
      label = 'medium';
      color = '#e8cb00';
    }
    return { color, label };
  }
}
