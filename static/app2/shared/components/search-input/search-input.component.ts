/**
 * Simplest usage:
 *
 * <rc-search-input (onSearch)="handleFunc($event)"
 *                  [loading]="searchState"></rc-search-input>
 *
 * You can click the search button or press the enter key to trigger the search action.
 * A handling function is required, so you can receive the search keyword.
 * You should also set the loading property with your search state in order to change the component's state.
 *
 *
 * You can also set other properties, below is a full usage:
 *
 * <rc-search-input (onSearch)="handleFunc($event)"
 *                  [loading]="searchState"
 *                  [required]="false"
 *                  [disabled]="false"
 *                  [searchOnChange]="false"
 *                  [delay]="1000"
 *                  [placeholder]="placeholder"></rc-search-input>
 *
 */
import {
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { debounce } from 'lodash';

@Component({
  selector: 'rc-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss'],
})
export class SearchInputComponent implements OnInit {
  @Input()
  placeholder: string;
  @Input()
  required = false;
  @Input()
  disabled = false;
  @Input()
  loading = false;
  @Input()
  searchOnChange = false;
  @Input()
  delay = 1000;
  @Input()
  keyword = '';
  @Input()
  mode = 'search';
  @Input()
  size: string;
  @Output()
  onSearch = new EventEmitter<string>();

  @HostBinding('class.rc-search-small')
  get sizeClass() {
    return this.size === 'small';
  }

  onChange = debounce((instantValue: string) => {
    if (this.searchOnChange || this.isFilter) {
      this.emit(instantValue);
    }
  }, this.delay);

  constructor() {}

  ngOnInit() {
    if (this.isFilter && this.keyword.length) {
      this.search();
    }
  }

  search(): void {
    this.emit(this.keyword);
  }

  emit(value: string): void {
    if (!this.loading) {
      this.onSearch.emit(value);
    }
  }

  get isFilter() {
    return this.mode === 'filter';
  }
}
