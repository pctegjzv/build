import { Input, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

export type SelectOptionType = Object | string | number;
export const OPTION_NOT_FOUND = Symbol('OPTION_NOT_FOUND');

export abstract class BaseSelectComponent implements OnInit, OnDestroy {
  protected optionsSubject$ = new BehaviorSubject<Array<SelectOptionType>>(
    undefined,
  );
  private optionsSubscription: Subscription = Subscription.EMPTY;

  /**
   * Options for dropdown
   * @param {Array<SelectOptionType>} options
   */
  @Input()
  set options(options: Array<SelectOptionType>) {
    this.optionsSubject$.next(options);
  }

  get options() {
    return this.optionsSubject$.getValue();
  }

  /**
   * Attribute name for option display, can be omitted if option is primitive value
   */
  @Input()
  displayField: string;

  /**
   * Attribute name for option value, can be omitted if option is primitive value
   */
  @Input()
  valueField: string;

  /**
   * Set selected option by option value.
   * The selected option will be returned.
   */
  abstract setSelectedValue(value: any): any;

  /**
   * Will be called when options changed.
   * @param {Array<SelectOptionType>} _
   */
  protected abstract onOptionsChange(_: Array<SelectOptionType>): void;

  ngOnInit() {
    this.optionsSubscription = this.optionsSubject$
      .pipe(filter(value => !!value))
      .subscribe(options => {
        this.onOptionsChange(options);
      });
  }

  ngOnDestroy() {
    this.optionsSubscription.unsubscribe();
  }

  protected getOptionDisplay(option: SelectOptionType) {
    if (option && typeof option === 'object') {
      return this.displayField ? option[this.displayField] : '';
    } else {
      return option;
    }
  }

  protected getOptionValue(option: SelectOptionType) {
    if (option && typeof option === 'object') {
      return this.valueField ? option[this.valueField] : '';
    } else {
      return option;
    }
  }
}
