import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  ChangeDetectorRef,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  OnInit,
  Optional,
  Output,
  Self,
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';
import { Subject } from 'rxjs';

import {
  BaseSelectComponent,
  OPTION_NOT_FOUND,
  SelectOptionType,
} from 'app2/shared/components/select/base-select';
import { FormFieldControl } from 'app2/shared/form-field-control';
import { delay } from 'app2/shared/services/utility/delay';

export class BaseSingleSelectComponent extends BaseSelectComponent
  implements FormFieldControl, ControlValueAccessor, OnInit {
  protected _required = false;
  protected _disabled = false;
  protected _value: any = null;
  protected _selectedOption: SelectOptionType;
  protected _optionNotFound = false;
  id: string;
  stateChanges = new Subject<void>();

  /**
   * Underlining value. This indicates the value property of the given option.
   * For example, if option is `{ displayName: 'One Two Three', val: '123'  }`, and
   * `this.value` is 'val', the value is '123'.
   */
  @Input()
  get value(): any {
    return this.ngControl ? this.ngControl.value : this._value;
  }

  set value(value: any) {
    this.setSelectedValue(value);
  }

  /**
   * Disabled state.
   */
  @Input()
  @HostBinding('class.disabled')
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }

    this._disabled = disabled;
    this.stateChanges.next();
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  get optionNotFound(): boolean {
    return this._optionNotFound;
  }

  /** Whether the control is in an error state. */
  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);

    return !!(isInvalid && (isDirty || isSubmitted)) || this.optionNotFound;
  }

  /** Event emitted when value changes. */
  @Output()
  change = new EventEmitter<SelectOptionType>();

  constructor(
    @Optional() protected el: ElementRef<Element>, // 暂时不知道为什么要加 Optional()，否则会报错 `ElementRef not defined`
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() protected parentForm: NgForm,
    @Optional() protected parentFormGroup: FormGroupDirective,
    @Optional() protected cdr: ChangeDetectorRef,
  ) {
    super();
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }

  ngOnInit() {
    super.ngOnInit();
    // Hack how validator is augmented:
    if (!this.ngControl || !this.ngControl.control) {
      return;
    }
    // const originalValidator = this.ngControl.control.asyncValidator;
    this.ngControl.control.asyncValidator = async (
      _control: AbstractControl,
    ) => {
      // let errors = originalValidator ? await originalValidator(control) : null;
      await delay(0);
      if (this.optionNotFound) {
        return {
          option_not_found: {
            value: this.value,
          },
        };
      }
      return undefined;
    };
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  protected onOptionsChange(_: Array<SelectOptionType>) {
    if (this._selectedOption) {
      const valueToRecover = this.value;
      // defer selection in next task
      this.setSelectedValue(valueToRecover);
    }
  }

  /**
   * Set selected option by option value.
   * The selected option will be returned.
   */
  setSelectedValue(
    value: string | number,
    emitToModel = true,
  ): SelectOptionType {
    const newOption = this.findOptionByValue(value);

    this.setSelectedOption(newOption, emitToModel);

    return newOption;
  }

  private setSelectedOption(
    option: SelectOptionType,
    emitToModel = true,
  ): void {
    const optionNotFound = OPTION_NOT_FOUND === option;
    if (this._selectedOption !== option) {
      if (!optionNotFound && option && !this.options.includes(option)) {
        throw new Error(
          `No such option: ${JSON.stringify(
            option,
          )}, available options: ${JSON.stringify(this.options)}`,
        );
      }

      this._selectedOption = option;

      if (optionNotFound) {
        // What shall we do here?
      } else {
        const value = option ? this.getOptionValue(option) : undefined;
        this._value = value;
        if (this.ngControl) {
          this.ngControl.control.updateValueAndValidity();
        }

        // Force emit if option not found.
        if (emitToModel || optionNotFound) {
          // onChange (NgModel) emits value
          this.onChange(value);
        }
        if (this.ngControl) {
          this.ngControl.control.updateValueAndValidity();
        }
      }

      this.stateChanges.next();
      // change event emits option
      this.change.emit(option);
    }

    this._optionNotFound =
      !!this.options && OPTION_NOT_FOUND === this._selectedOption;
  }

  private findOptionByValue(value: any) {
    const option =
      this.options &&
      this.options.find(
        (_option: SelectOptionType) =>
          // tslint:disable-next-line triple-equals
          this.getOptionValue(_option) == value,
      );

    return !option && value ? OPTION_NOT_FOUND : option;
  }

  /**
   * Sets the model value. Implemented as part of ControlValueAccessor.
   * @param value Value to be set to the model.
   */
  writeValue(value: any): void {
    this.setSelectedValue(value, false);
  }

  onChange = (value: any) => {
    this.value = value;
  };

  onTouched = () => {};

  registerOnChange(fn: (_: any) => {}): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => {}): void {
    this.onTouched = fn;
  }
}
