import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Directive,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  OnInit,
  Optional,
  Output,
  Self,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormGroupDirective,
  NG_VALIDATORS,
  NgControl,
  NgForm,
  Validator,
} from '@angular/forms';
import $ from 'jquery';
import { debounce, intersection } from 'lodash';
import { Subject } from 'rxjs';
import { first } from 'rxjs/operators';

import { FormFieldControl } from 'app2/shared/form-field-control';

import { BaseSelectComponent, SelectOptionType } from './base-select';

// Increasing integer for generating unique ids for checkbox components.
let nextUniqueId = 0;

/**
 * ngModel 支持validator： rcDropdownMinLength，rcDropdownMaxLength
 */
@Component({
  selector: 'rc-multi-selection-dropdown',
  templateUrl: 'multi-selection-dropdown.component.html',
  styleUrls: ['base-dropdown.scss', 'multi-selection-dropdown.component.scss'],
  providers: [
    {
      provide: FormFieldControl,
      useExisting: MultiSelectionDropdownComponent,
    },
  ],
})
export class MultiSelectionDropdownComponent extends BaseSelectComponent
  implements OnInit, AfterViewInit, ControlValueAccessor, FormFieldControl {
  private _uniqueId = `rc-multi-dropdown-${++nextUniqueId}`;
  private _value: Array<string | number>;
  private _required = false;
  protected _disabled = false;

  stateChanges = new Subject<void>();
  controlType = 'rc-multi-dropdown';

  @Input()
  id = this._uniqueId;

  /**
   * JQuery Object of dropdown instance
   */
  private $dropdown: JQuery;
  /**
   * indicate whether dropdown has been initialized
   */
  private initialized = false;

  @ViewChild('dropdown')
  private dropdown: ElementRef;

  /**
   * Placeholder
   */
  @Input()
  placeholder: string;

  /**
   *  force the dropdown direction, default 'auto'
   */
  @Input()
  direction: 'auto' | 'upward' | 'downward';

  /**
   * Emit array of current selected option values, eg: [au, sk, em, ai]
   * @type {EventEmitter<Array<SelectOptionType>>}
   */
  @Output()
  valueChange = new EventEmitter<Array<string | number>>();

  /**
   * Emit added option
   * @type {EventEmitter<SelectOptionType>}
   */
  @Output()
  optionAdd = new EventEmitter<SelectOptionType>();

  /**
   * Emit removed option
   * @type {EventEmitter<SelectOptionType>}
   */
  @Output()
  optionRemove = new EventEmitter<SelectOptionType>();

  /**
   * Implements FormFieldControl
   * @returns {Array<string>}
   */
  @Input()
  get value(): Array<string | number> {
    return this.ngControl ? this.ngControl.value : this._value;
  }

  set value(value: Array<string | number>) {
    this.setSelectedValue(value);
  }

  /**
   * Implements FormFieldControl
   * @returns {Array<string>}
   */
  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  /**
   * set true to disable input element and hide remove icon for tags
   */
  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }
    this._disabled = disabled;
    this.stateChanges.next();
  }

  /** Whether the control is in an error state. */
  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);

    return !!(isInvalid && (isDirty || isSubmitted));
  }

  constructor(
    private el: ElementRef<Element>,
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() protected parentForm: NgForm,
    @Optional() protected parentFormGroup: FormGroupDirective,
    @Optional() protected cdr: ChangeDetectorRef,
  ) {
    super();
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
    this._value = [];
  }

  ngOnInit() {
    super.ngOnInit();
    this.$dropdown = $(this.dropdown.nativeElement);
  }

  ngAfterViewInit() {
    this.optionsSubject$
      .pipe(
        first((options: Array<SelectOptionType>) => {
          return options && options.length > 0;
        }),
      )
      .toPromise()
      .then(() => {
        this.dropdownInitialize();
      });
  }

  /**
   * set selected options by option value array
   * @param {Array<string | number>} values
   */
  setSelectedValue(
    value: Array<string | number>,
    emitToModel = true,
    applyToNative = true,
  ) {
    if (value === null || value === undefined) {
      return;
    }
    this._value = value;
    this.valueChange.emit(value);

    if (emitToModel !== false) {
      this.onTouchedCallback();
      this.onChangeCallback(value);
    }

    if (applyToNative !== false) {
      this.setNativeSelectedValue(value);
    }

    this.stateChanges.next();
  }

  private setNativeSelectedValue(value: Array<string | number>) {
    if (!this.initialized) {
      return;
    }
    if (value && value.length) {
      this.$dropdown.dropdown('clear');
      this.$dropdown.dropdown('set selected', value);
    } else {
      // If value does not present, reset to default.
      this.$dropdown.dropdown('clear');
      this.$dropdown.dropdown('restore placeholder text');
    }
  }

  private dropdownInitialize() {
    this.$dropdown.dropdown({
      debug: false,
      showOnFocus: false,
      selectOnKeydown: false,
      fullTextSearch: 'exact',
      match: 'text',
      placeholder: 'value',
      direction: this.direction || 'auto',
      onChange: debounce((value: string) => {
        if (value !== (this.value || []).join(',')) {
          // semantic multi dropdown, onchange 参数为 "a,b,c,d" 格式
          const newValue = value ? value.split(',') : [];
          this.setSelectedValue(newValue, true, false);
        }
      }, 50),
      onAdd: (value: string | number) => {
        const option = this.findOption(value);
        this.optionAdd.emit(option);
      },
      onRemove: (value: string | number) => {
        const option = this.findOption(value);
        this.optionRemove.emit(option);
      },
      onShow: () => this.el.nativeElement.classList.add('rc-active'),
      onHide: () => this.el.nativeElement.classList.remove('rc-active'),
    });
    this.initialized = true;
    if (this.ngControl) {
      this.setSelectedValue(this.value, false);
    }
  }

  private findOption(value: string | number) {
    return this.options.find((option: SelectOptionType) => {
      if (typeof option === 'object') {
        return this.getOptionValue(option).toString() === value.toString();
      } else {
        return option.toString() === value.toString();
      }
    });
  }

  protected onOptionsChange(options: Array<SelectOptionType>) {
    if (options && options.length && this.initialized) {
      if (this.ngControl && this.ngControl.value) {
        const newValues = options.map((item: SelectOptionType) => {
          return this.getOptionValue(item);
        });
        this.setSelectedValue(intersection(newValues, this.ngControl.value));
      } else {
        this.$dropdown.dropdown('restore placeholder text');
        this.$dropdown.dropdown('clear');
      }
    }
    // TODO: actions when multi-dropdown options changes ??
  }

  /** Following Methods Implements ControlValueAccessor **/

  writeValue(value: Array<string | number>) {
    this.setSelectedValue(value, false, true);
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}

/**
 * min selection length validator for multi dropdown
 */
@Directive({
  selector: 'rc-multi-selection-dropdown[rcDropdownMinLength][ngModel]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: DropdownMinLengthValidatorDirective,
      multi: true,
    },
  ],
})
export class DropdownMinLengthValidatorDirective implements Validator {
  @Input()
  rcDropdownMinLength: number;

  validate(control: AbstractControl): { [key: string]: any } {
    return this.rcDropdownMinLength
      ? this.dropdownMinLengthValidator(control)
      : null;
  }

  dropdownMinLengthValidator(control: AbstractControl): { [key: string]: any } {
    const minLength = this.rcDropdownMinLength;
    if (minLength) {
      return ((control.value && control.value.length) || 0) < minLength
        ? {
            dropdown_min_length: {
              minLength,
            },
          }
        : null;
    } else {
      return null;
    }
  }
}

/**
 * max selection length validator for multi dropdown
 */
@Directive({
  selector: 'rc-multi-selection-dropdown[rcDropdownMaxLength][ngModel]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: DropdownMaxLengthValidatorDirective,
      multi: true,
    },
  ],
})
export class DropdownMaxLengthValidatorDirective implements Validator {
  @Input()
  rcDropdownMaxLength: number;

  validate(control: AbstractControl): { [key: string]: any } {
    return this.rcDropdownMaxLength
      ? this.dropdownMaxLengthValidator(control)
      : null;
  }

  dropdownMaxLengthValidator(control: AbstractControl): { [key: string]: any } {
    const maxLength = this.rcDropdownMaxLength;
    if (control.value && control.value.length && maxLength) {
      return control.value.length > maxLength
        ? {
            dropdown_max_length: {
              maxLength,
            },
          }
        : null;
    } else {
      return null;
    }
  }
}
