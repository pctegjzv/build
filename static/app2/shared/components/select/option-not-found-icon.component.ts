import { Component, Input } from '@angular/core';

@Component({
  selector: 'rc-option-not-found-icon',
  template: `
    <div [rcTooltip]="optionNotFoundTooltip"
         rcTooltipPosition="top center"
         rcTooltipMode="error"
         class="fa fa-exclamation-circle option-not-found-icon">
    </div>

    <ng-template #optionNotFoundTooltip>
      <div style="white-space: nowrap">
        {{ 'select_option_not_found' | translate }}:
        <br>
        < <strong>{{ value }}</strong> >
      </div>
    </ng-template>
  `,
  styles: [
    `
      :host {
        display: inline-block;
        font-size: 18px;
        margin: 0 8px;
        cursor: pointer;
        color: #fc5d68;
      }
    `,
  ],
})
export class OptionNotFoundIconComponent {
  @Input()
  value: any | any[];
}
