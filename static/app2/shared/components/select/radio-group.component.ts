import { animate, style, transition, trigger } from '@angular/animations';
import { AfterViewInit, Component, HostBinding, Input } from '@angular/core';

import { BaseSingleSelectComponent } from 'app2/shared/components/select/base-single-select';
import { FormFieldControl } from 'app2/shared/form-field-control';

// Increasing integer for generating unique ids for radio group components.
let nextUniqueId = 0;

@Component({
  selector: 'rc-radio-group',
  templateUrl: 'radio-group.component.html',
  styleUrls: ['radio-group.component.scss'],
  animations: [
    trigger('activeIcon', [
      transition(':enter', [
        style({ transform: 'scale(0)', width: '0' }),
        animate('0.1s ease', style({ width: '*' })),
        animate('.2s ease', style({ transform: 'scale(1)' })),
      ]),
      transition(':leave', [
        animate('.2s ease', style({ transform: 'scale(0)' })),
        animate('0.2s ease', style({ width: '0' })),
      ]),
    ]),
  ],
  providers: [
    {
      provide: FormFieldControl,
      useExisting: RadioGroupComponent,
    },
    {
      provide: BaseSingleSelectComponent,
      useExisting: RadioGroupComponent,
    },
  ],
})
export class RadioGroupComponent extends BaseSingleSelectComponent
  implements AfterViewInit {
  private _uniqueId = `rc-radio-group-${++nextUniqueId}`;
  controlType = 'rc-radio-group';
  @Input()
  id = this._uniqueId;
  @Input()
  readOnly: boolean;

  @HostBinding('class.disabled')
  get disabled() {
    return super.disabled || this.readOnly;
  }

  set disabled(isDisabled: boolean) {
    super.disabled = isDisabled;
  }

  ngAfterViewInit(): void {
    this.setSelectedValue(this.value, false);
  }
}
