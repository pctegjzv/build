import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { first } from 'rxjs/operators';

import { BaseSingleSelectComponent } from 'app2/shared/components/select/base-single-select';
import { FormFieldControl } from 'app2/shared/form-field-control';

import { SelectOptionType } from './base-select';

// Increasing integer for generating unique ids
let nextUniqueId = 0;

@Component({
  selector: 'rc-single-selection-dropdown',
  templateUrl: 'single-selection-dropdown.component.html',
  styleUrls: ['base-dropdown.scss', 'single-selection-dropdown.component.scss'],
  providers: [
    {
      provide: FormFieldControl,
      useExisting: SingleSelectionDropdownComponent,
    },
    {
      provide: BaseSingleSelectComponent,
      useExisting: SingleSelectionDropdownComponent,
    },
  ],
})
export class SingleSelectionDropdownComponent extends BaseSingleSelectComponent
  implements OnInit, AfterViewInit {
  /**
   * indicate whether dropdown has been initialized
   */
  private initialized = false;
  private _uniqueId = `rc-single-selection-dropdown-${++nextUniqueId}`;
  controlType = 'rc-single-selection-dropdown';

  @Input()
  id = this._uniqueId;

  /**
   * JQuery Object of dropdown instance
   */
  private $dropdown: JQuery;
  @ViewChild('dropdown')
  private dropdown: ElementRef;

  /**
   * Placeholder
   */
  @Input()
  placeholder: string;

  /**
   * allow filter input to display
   */
  @Input()
  allowFilter = true;

  /**
   *  force the dropdown direction, default 'auto'
   */
  @Input()
  direction: 'auto' | 'upward' | 'downward';

  ngOnInit() {
    super.ngOnInit();
    this.$dropdown = $(this.dropdown.nativeElement);
  }

  ngAfterViewInit() {
    this.optionsSubject$
      .pipe(
        first(
          (options: Array<SelectOptionType>) => options && options.length > 0,
        ),
      )
      .toPromise()
      .then(() => this.dropdownInitialize());
  }

  /**
   * Set selected option by option value
   */
  setSelectedValue(
    value: string | number,
    emitToModel = true,
    applyToNative = true,
  ) {
    const option = super.setSelectedValue(value, emitToModel);

    if (applyToNative !== false) {
      setTimeout(() => {
        this.setNativeSelectedValue(option ? value : undefined);
      });
    }

    return option;
  }

  private setNativeSelectedValue(value: string | number) {
    if (!this.initialized) {
      return;
    }
    if (value) {
      this.$dropdown.dropdown('set selected', value);
    } else {
      // If value does not present, reset to default.
      this.$dropdown.dropdown('restore placeholder text');
      this.$dropdown.dropdown('clear');
    }
  }

  /**
   * Will be invoked after first `options` is ready
   * used to setup dropdown using semantic jquery-plugin
   */
  protected dropdownInitialize() {
    this.$dropdown.dropdown({
      debug: false,
      showOnFocus: false,
      selectOnKeydown: false,
      fullTextSearch: 'exact',
      match: 'text',
      placeholder: 'value',
      direction: this.direction || 'auto',
      onChange: (value: string | number) => {
        this.setSelectedValue(value, true, false);
      },
      onShow: () => {
        this.el.nativeElement.classList.add('rc-active');
      },
      onHide: () => {
        this.el.nativeElement.classList.remove('rc-active');
      },
    });
    this.initialized = true;
    this.setSelectedValue(this.value, false);
  }
}
