import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

import { getUnifiedStatus } from 'app2/shared/components/status-badge/status-mapper';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-service-status-badge',
  templateUrl: 'service-status-badge.component.html',
  styleUrls: ['service-status-badge.component.scss'],
})
export class ServiceStatusBadgeComponent implements OnInit, OnChanges {
  @Input()
  currentInstances = 0;

  @Input()
  desiredInstances = 0;

  @Input()
  status: string; // Running/Warning/Error/Stopped/Deploying/Starting/Updating/Stopping/Deleting

  statusText: string;
  type: string;
  size = 'mini';
  solid = true;
  closeable = false;
  round = true;

  statusTypeMap = {
    Running: 'success',
    Warning: 'warning',
    Error: 'error',
    Stopped: 'info',
    Deploying: 'primary',
    Starting: 'primary',
    Updating: 'primary',
    Stopping: 'warning',
    Deleting: 'warning',
    Unknown: 'info',
  };

  constructor(private translateService: TranslateService) {}

  ngOnInit() {}

  ngOnChanges({ status }: SimpleChanges) {
    if (status && status.currentValue) {
      this.statusText = this.translateService.get(
        `status_${status.currentValue.toLowerCase()}`,
      );
      this.statusMapToType(status.currentValue);
    }
  }

  private statusMapToType(status: string) {
    this.type = this.statusTypeMap[status] || 'info';
  }

  get statusAttr() {
    return getUnifiedStatus(this.status);
  }
}
