import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

import { getUnifiedStatus } from 'app2/shared/components/status-badge/status-mapper';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  selector: 'rc-status-badge',
  templateUrl: 'status-badge.component.html',
  styleUrls: ['status-badge.component.scss'],
})
export class StatusBadgeComponent implements OnInit, OnChanges {
  statusText: string;

  @Input()
  status: string;
  @Input()
  showPrimitiveStatus: boolean; // if true, status text is not translated
  @Input()
  mode: 'default' | 'indicator' | 'text' | 'rect' = 'default';

  constructor(private translateService: TranslateService) {}

  ngOnInit() {}

  ngOnChanges({ status }: SimpleChanges) {
    if (status.currentValue) {
      this.statusText = this.showPrimitiveStatus
        ? this.getCapitalizedStatus()
        : this.getTranslatedStatus();
    }
  }

  private getTranslatedStatus() {
    return this.translateService.get(`status_${this.status.toLowerCase()}`);
  }

  private getCapitalizedStatus() {
    return this.status.charAt(0).toUpperCase() + this.status.slice(1);
  }

  get statusAttr() {
    return getUnifiedStatus(this.status);
  }
}
