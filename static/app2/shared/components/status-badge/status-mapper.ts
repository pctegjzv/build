import { UnifiedStatus } from 'app2/core/types';

const statusMaps = [
  {
    status: UnifiedStatus.SUCCESS,
    values: [
      'running',
      'used',
      'completed',
      'bound',
      'normal',
      'ok',
      'switch_on',
    ],
  },
  {
    status: UnifiedStatus.ERROR,
    values: [
      'error',
      'matching_failed',
      'recycling_failed',
      'critical',
      'overflow',
      'alarm',
      'alert',
    ],
  },
  {
    status: UnifiedStatus.PENDING,
    values: ['warning', 'pending', 'waiting_recycle'],
  },
  {
    status: UnifiedStatus.IN_PROGRESS,
    values: [
      'starting',
      'deploying',
      'free',
      'matching',
      'destroying',
      'stopping',
      'shutting_down',
      'insufficient_data',
      'insufficient_res',
      'updating',
      'deleting',
    ],
  },
  {
    status: UnifiedStatus.INACTIVE,
    values: ['stopped', 'unknown', 'invalid', 'switch_off'],
  },
];

export function getUnifiedStatus(status = '') {
  const res: any = statusMaps.find((item: any) =>
    item.values.includes(status.toLowerCase()),
  );
  return res ? res.status : UnifiedStatus.INACTIVE;
}
