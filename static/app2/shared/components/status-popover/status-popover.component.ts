import { Component, Input, OnInit } from '@angular/core';
// import { UnifiedStatus } from 'app2/core/types.ts';

@Component({
  selector: 'rc-status-popover',
  templateUrl: './status-popover.component.html',
  styleUrls: ['./status-popover.component.scss'],
})
export class StatusPopoverComponent implements OnInit {
  @Input()
  statusList: any[];
  @Input()
  position: string;
  @Input()
  mode: string;
  @Input()
  trigger: string;
  @Input()
  target: HTMLElement;

  ngOnInit() {}

  getClassByStatus(status: any) {
    return {
      success: status === 'SUCCESS',
      error: status === 'ERROR',
      pending: status === 'PENDING',
      in_progress: status === 'IN_PROGRESS',
      inactive: status === 'INACTIVE',
    };
  }
  getIconClassByStatus(status: any) {
    return {
      'fa-check-circle': status === 'SUCCESS',
      'fa-times-circle': status === 'ERROR',
      'fa-minus-circle': status === 'INACTIVE',
    };
  }
}
