import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  AfterViewInit,
  Component,
  HostBinding,
  Input,
  OnChanges,
  Optional,
  Self,
  SimpleChanges,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';
import { debounce } from 'lodash';
import { Subject } from 'rxjs';

import { FormFieldControl } from 'app2/shared/form-field-control';

let nextUniqueId = 0;

class SuggestionList {
  private suggestions: Array<string>;
  loadWithDebounce: Function;
  items: Array<string>;
  visible: boolean;
  selectedIndex: number;
  selectedItem: string;

  constructor() {
    this.items = [];
    this.suggestions = [];
    this.selectedIndex = -1;
    this.loadWithDebounce = debounce((query: string) => {
      this.load(query);
    }, 50);
  }

  setSuggestions(suggestions: Array<string>): void {
    this.suggestions = suggestions;
  }

  reset() {
    this.visible = false;
    this.items = [];
    this.selectedIndex = -1;
    this.selectedItem = '';
  }

  async load(query = '') {
    this.items = this.suggestions.filter(item => {
      return item.includes(query);
    });
    if (this.items.length) {
      if (this.selectedIndex > this.items.length) {
        this.selectedIndex = this.items.length - 1;
      }
      this.selectedItem = this.items[this.selectedIndex];
      this.show();
    } else {
      this.reset();
    }
  }

  show() {
    this.visible = true;
  }

  selectNext() {
    this.select(++this.selectedIndex);
  }

  selectPrior() {
    this.select(--this.selectedIndex);
  }

  select(index: number, _shouldScroll = true) {
    if (index < 0) {
      index = this.items.length - 1;
    } else if (index >= this.items.length) {
      index = 0;
    }
    this.selectedIndex = index;
    this.selectedItem = this.items[index];
  }
}

@Component({
  selector: 'rc-suggestion-input',
  templateUrl: './suggestion-input.component.html',
  styleUrls: ['./suggestion-input.component.scss'],
})
export class SuggestionInputComponent
  implements AfterViewInit, OnChanges, FormFieldControl, ControlValueAccessor {
  private _uniqueId = `rc-suggestion-input-${++nextUniqueId}`;
  private _required = false;
  protected _disabled = false;

  stateChanges = new Subject<void>();
  controlType = 'rc-suggestion-input';

  @Input()
  id = this._uniqueId;

  @Input()
  suggestions: any[];
  @Input()
  placeholder: string;
  inputValue: string;
  private suggestionList: SuggestionList;
  focus: boolean;

  @Input()
  get value(): string {
    return this.ngControl ? this.ngControl.value : this.inputValue;
  }

  set value(value: string) {
    this.inputValue = value || '';
    this.stateChanges.next();
    this.updateModelValue();
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  /**
   * set true to disable input element and hide remove icon for tags
   */
  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }
    this._disabled = disabled;
    this.stateChanges.next();
  }

  /** Whether the control is in an error state. */
  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);

    return !!(isInvalid && (isDirty || isSubmitted));
  }

  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() private parentForm: NgForm,
    @Optional() private parentFormGroup: FormGroupDirective,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
    this.inputValue = '';
  }

  ngAfterViewInit() {}

  ngOnChanges(changes: SimpleChanges) {
    const suggestions = changes.suggestions;
    if (suggestions && suggestions.currentValue.length) {
      this.setSuggestions(suggestions.currentValue);
    } else {
      this.setSuggestions([]);
    }
  }

  setSuggestions(suggestions: Array<string>): void {
    this.suggestionList = new SuggestionList();
    this.suggestionList.setSuggestions(suggestions);
  }

  confirm() {
    if (this.suggestionList && this.suggestionList.selectedItem) {
      this.inputValue = this.suggestionList.selectedItem;
      this.updateModelValue();
    }
    this.suggestionList.visible = false;
  }
  onInputFocus() {
    this.focus = true;
    if (this.suggestionList) {
      this.suggestionList.load(this.inputValue);
    }
  }

  onInputBlur() {
    this.focus = false;
    this.suggestionList.visible = false;
  }
  onInputChange() {
    this.updateModelValue();
    if (this.suggestionList) {
      this.suggestionList.loadWithDebounce(this.inputValue);
    }
  }

  onKeydown(event: KeyboardEvent): void {
    switch (event.key.toLowerCase()) {
      case 'enter':
        this.confirm();
        event.preventDefault();
        event.stopImmediatePropagation();
        break;

      case 'arrowdown':
        if (this.suggestionList) {
          this.suggestionList.selectNext();
          event.preventDefault();
          event.stopImmediatePropagation();
        }
        break;

      case 'arrowup':
        if (this.suggestionList) {
          this.suggestionList.selectPrior();
          event.preventDefault();
          event.stopImmediatePropagation();
        }
        break;
    }
  }

  suggestionsVisible() {
    return this.suggestionList && this.suggestionList.visible;
  }

  get suggesstionItems(): string[] {
    return this.suggestionList ? this.suggestionList.items : [];
  }

  // set ngModel value when native view changes
  private updateModelValue() {
    this.emitToModel();
  }

  // emit _value to ngModel
  private emitToModel() {
    this.onTouchedCallback();
    this.onChangeCallback(this.inputValue);
  }

  // apply _value to native view
  // private applyToNative() {}

  writeValue(value: string) {
    this.inputValue = value || '';
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onTouchedCallback: () => void = () => {
    // placeholder
  };

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };
}
