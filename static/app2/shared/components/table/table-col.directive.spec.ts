import { Component } from '@angular/core';
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import {
  TableColCellDirective,
  TableColDirective,
  TableColHeaderDirective,
} from 'app2/shared/components/table/table-col.directive';

@Component({
  selector: 'rc-test-fixture-component',
  template: `
    <rc-table-col id="t1"></rc-table-col>
    <rc-table-col id="t2">
      <div *rcTableColHeader></div>
      <div *rcTableColCell></div>
    </rc-table-col>
  `,
})
class TestFixtureComponent {}

describe('DataTableColumnDirective', () => {
  let fixture: ComponentFixture<TestFixtureComponent>;
  let component: TestFixtureComponent;

  // provide our implementations or mocks to the dependency injector
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        TableColHeaderDirective,
        TableColCellDirective,
        TableColDirective,
        TestFixtureComponent,
      ],
    });
  });

  beforeEach(async(() => {
    TestBed.compileComponents().then(() => {
      fixture = TestBed.createComponent(TestFixtureComponent);
      component = fixture.componentInstance;
    });
  }));

  describe('fixture', () => {
    let directive: TableColDirective;

    beforeEach(() => {
      directive = fixture.debugElement
        .query(By.directive(TableColDirective))
        .injector.get(TableColDirective);
    });

    it('should have a component instance', () => {
      expect(component).toBeTruthy();
    });

    it('should have at least one DataTableColumnDirective directive', () => {
      expect(directive).toBeTruthy();
    });
  });

  describe('directive #1', () => {
    let directive: TableColDirective;

    beforeEach(() => {
      directive = fixture.debugElement
        .query(By.css('#t1'))
        .injector.get(TableColDirective);
    });

    it('should be found', () => {
      expect(directive).toBeTruthy();
    });

    it('should have undefined inputs by default', () => {
      fixture.detectChanges();
      expect(directive.name).toBeUndefined();
      expect(directive.prop).toBeUndefined();
      expect(directive.frozenRight).toBeUndefined();
      expect(directive.frozenLeft).toBeUndefined();
      expect(directive.flexGrow).toBeUndefined();
      expect(directive.resizeable).toBeUndefined();
      expect(directive.comparator).toBeUndefined();
      expect(directive.pipe).toBeUndefined();
      expect(directive.sortable).toBeUndefined();
      expect(directive.draggable).toBeUndefined();
      expect(directive.canAutoResize).toBeUndefined();
      expect(directive.minWidth).toBeUndefined();
      expect(directive.width).toBeUndefined();
      expect(directive.maxWidth).toBeUndefined();
    });
  });

  describe('directive #2', () => {
    let directive: TableColDirective;

    beforeEach(() => {
      directive = fixture.debugElement
        .query(By.css('#t2'))
        .injector.get(TableColDirective);
    });

    it('should be found', () => {
      expect(directive).toBeTruthy();
    });
  });
});
