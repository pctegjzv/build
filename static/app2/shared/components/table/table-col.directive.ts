/* tslint:disable:directive-selector */
import { ContentChild, Directive, Input, TemplateRef } from '@angular/core';
import {
  DataTableColumnCellDirective,
  DataTableColumnDirective,
  DataTableColumnHeaderDirective,
} from '@swimlane/ngx-datatable';

@Directive({
  selector: '[rcTableColHeader]',
})
export class TableColHeaderDirective extends DataTableColumnHeaderDirective {}

@Directive({
  selector: '[rcTableColCell]',
})
export class TableColCellDirective extends DataTableColumnCellDirective {}

@Directive({
  selector: 'rc-table-col',
})
export class TableColDirective extends DataTableColumnDirective {
  @Input()
  @ContentChild(TableColHeaderDirective, { read: TemplateRef })
  headerTemplate: TemplateRef<any>;

  @Input()
  @ContentChild(TableColCellDirective, { read: TemplateRef })
  cellTemplate: TemplateRef<any>;
}
