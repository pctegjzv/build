/* tslint:disable:directive-selector */
import { ContentChild, Directive, Input, TemplateRef } from '@angular/core';
import {
  DatatableRowDetailDirective,
  DatatableRowDetailTemplateDirective,
} from '@swimlane/ngx-datatable';

@Directive({
  selector: '[rcTableRowDetailTemplate]',
})
export class TableRowDetailTemplateDirective extends DatatableRowDetailTemplateDirective {}

@Directive({
  selector: 'rc-table-row-detail',
})
export class TableRowDetailDirective extends DatatableRowDetailDirective {
  @Input()
  @ContentChild(TableRowDetailTemplateDirective, { read: TemplateRef })
  template: TemplateRef<any>;
}
