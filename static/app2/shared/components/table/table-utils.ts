/**
 * 对数组A和B进行merge操作，要求保持A引用不变，且 1. B中新增数据在A中保持相同index，2. A中已有的数据，更新为B中对应的索引
 * @param {any[]} newList
 * @param {any[]} oldList
 * @param {string} propKey
 * @returns {any[]}
 */
import { clone, get } from 'lodash';
export function generateMergedListData(
  newList: any[],
  oldList: any[],
  propKey: string,
) {
  if (!oldList || !oldList.length) {
    return newList;
  }
  const clonedOldList = clone(oldList);
  oldList.length = newList.length;
  for (let i = 0; i < newList.length; i++) {
    const newItem = newList[i];
    const newIdx = get(newItem, propKey, '');
    const oldItem = clonedOldList.find(old => {
      const oldIdx = get(old, propKey, '');
      return oldIdx === newIdx;
    });
    if (oldItem) {
      oldList[i] = Object.assign(oldItem, newItem);
    } else {
      oldList[i] = newItem;
    }
  }
  return oldList;
}
