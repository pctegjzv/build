import { Component } from '@angular/core';
import {
  ComponentFixture,
  ComponentFixtureAutoDetect,
  TestBed,
} from '@angular/core/testing';
import { TableModule } from 'app2/shared/components/table/table.module';
import { registerCleanup } from 'app2/testing/helpers';

@Component({
  selector: 'rc-test-fixture-component',
  template: `
    <h1>Simple case</h1>
    <rc-table [rows]="rows"
              [columns]="columns"
              [headerHeight]="50"
              [footerHeight]="0"
              [rowHeight]="100">
    </rc-table>

    <br>

    <h1>Simple case with template control</h1>
    <rc-table [rows]="rows">
      <rc-table-col>
        <div *rcTableColHeader>酷炫的名字</div>
        <div *rcTableColCell="let rowData = row">{{ rowData.name }}</div>
      </rc-table-col>
      <rc-table-col name="性别" prop="gender"></rc-table-col>
      <rc-table-col name="公司" prop="company"></rc-table-col>
    </rc-table>
  `,
})
class TestFixtureComponent {
  columns = [{ prop: 'name' }, { name: 'Gender' }, { name: 'Company' }];

  rows = [
    { name: 'Larry', gender: 'Male', company: 'Cisco' },
    { name: 'Lauren', gender: 'Female', company: 'HP' },
  ];
}

describe('TableComponent', () => {
  let fixture: ComponentFixture<TestFixtureComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TestFixtureComponent],
      imports: [TableModule],
      providers: [{ provide: ComponentFixtureAutoDetect, useValue: true }],
    });

    fixture = TestBed.createComponent(TestFixtureComponent);
  });

  it('can render a table correctly', () => {
    fixture.detectChanges();
  });

  registerCleanup();
});
