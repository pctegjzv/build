import {
  Component,
  ContentChild,
  ContentChildren,
  Input,
  OnChanges,
  OnInit,
  QueryList,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import $ from 'jquery';

import { TableColDirective } from 'app2/shared/components/table/table-col.directive';
import { TableRowDetailDirective } from 'app2/shared/components/table/table-row-detail.directive';
import { TranslateService } from 'app2/translate/translate.service';

/**
 * Data table component. Wraps ngx-datatable.
 *
 * 目前功能还十分不充足。将根据需求引入更多功能。
 */
@Component({
  selector: 'rc-table',
  templateUrl: 'table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit, OnChanges {
  @ViewChild(DatatableComponent)
  private ngxDataTable: DatatableComponent;

  /**
   * The minimum header height in pixels.
   * Pass a falsey for no header
   */
  @Input()
  headerHeight: any = 30;

  /**
   * The row height; which is necessary
   * to calculate the height for the lazy rendering.
   */
  @Input()
  rowHeight = 30;

  /**
   * The minimum footer height in pixels.
   * Pass falsey for no footer
   */
  @Input()
  footerHeight = 0;

  /**
   * Enable vertical scrollbars
   */
  @Input()
  scrollbarV = false;

  /**
   * Enable horz scrollbars
   */
  @Input()
  scrollbarH = false;

  @Input()
  trackByProp: string;

  @Input()
  emptyMessage: string;

  @Input()
  columnMode: string;

  @ContentChild(TableRowDetailDirective)
  set rowDetail(val: TableRowDetailDirective) {
    this.ngxDataTable.rowDetail = val;
  }

  /**
   * Column templates gathered from `ContentChildren`
   * if described in your markup.
   */
  @ContentChildren(TableColDirective)
  set columnTemplates(val: QueryList<TableColDirective>) {
    this.ngxDataTable.columnTemplates = val;
  }

  @Input()
  set rows(val: any) {
    this.ngxDataTable.rows = val;
  }

  /**
   * Columns defined via simple input binding.
   * Note: columns and columnTemplates should not be used together.
   */
  @Input()
  set columns(val: any) {
    this.ngxDataTable.columns = val;
  }

  constructor(public translateService: TranslateService) {}

  /**
   * 这里需要在Table模板中传入rowIndex, 利用rowIndex设置展开的rowWrapper class， 使用方法参考demo
   * @param row
   * @param {number} rowIndex
   */
  toggleExpandRow(row: any, rowIndex: number) {
    const $rowWrapper = $(this.ngxDataTable.element)
      .find('.datatable-scroll')
      .eq(0)
      .find('> .datatable-row-wrapper');
    this.ngxDataTable.rowDetail.toggleExpandRow(row);
    $rowWrapper.eq(rowIndex).toggleClass('rc-row-wrapper-expanded');
  }

  ngOnInit(): void {
    this.ngxDataTable.columnMode =
      ColumnMode[this.columnMode] || ColumnMode.force;
    this.ngxDataTable.headerHeight = this.headerHeight;
    this.ngxDataTable.rowHeight = this.rowHeight;
    this.ngxDataTable.footerHeight = this.footerHeight;
    this.ngxDataTable.scrollbarV = this.scrollbarV;
    this.ngxDataTable.scrollbarH = this.scrollbarH;
    this.ngxDataTable.trackByProp = this.trackByProp;
    this.ngxDataTable.messages = {
      emptyMessage: this.emptyMessage || this.translateService.get('no_data'),
      totalMessage: this.translateService.get('total'),
    };
  }

  ngOnChanges(changes: SimpleChanges) {
    const emptyMessage = changes['emptyMessage'];
    if (emptyMessage) {
      this.ngxDataTable.messages.emptyMessage = emptyMessage.currentValue;
    }
  }
}
