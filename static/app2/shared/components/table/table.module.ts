import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import {
  TableColCellDirective,
  TableColDirective,
  TableColHeaderDirective,
} from 'app2/shared/components/table/table-col.directive';
import {
  TableRowDetailDirective,
  TableRowDetailTemplateDirective,
} from 'app2/shared/components/table/table-row-detail.directive';
import { TableComponent } from 'app2/shared/components/table/table.component';
import { TranslateModule } from 'app2/translate/translate.module';

@NgModule({
  imports: [CommonModule, TranslateModule, NgxDatatableModule],
  declarations: [
    TableComponent,
    TableColDirective,
    TableColCellDirective,
    TableColHeaderDirective,
    TableRowDetailDirective,
    TableRowDetailTemplateDirective,
  ],
  exports: [
    TableComponent,
    TableColDirective,
    TableColCellDirective,
    TableColHeaderDirective,
    TableRowDetailDirective,
    TableRowDetailTemplateDirective,
  ],
  providers: [],
  entryComponents: [],
})
export class TableModule {}
