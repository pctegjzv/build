import { TemplatePortal } from '@angular/cdk/portal';
import {
  AfterContentInit,
  Component,
  ContentChildren,
  Directive,
  EventEmitter,
  Inject,
  Input,
  OnDestroy,
  Output,
  QueryList,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
  forwardRef,
} from '@angular/core';
import { MatTabGroup } from '@angular/material';
import { Subscription } from 'rxjs';

let nextUniqueId = 0;

interface RcTab {
  id: string;
  label: string | TemplateRef<any>;
  template: TemplateRef<any>;
  disabled: boolean;
}

/* tslint:disable:no-use-before-declare */

/* tslint:disable:no-input-rename */
@Directive({
  selector: '[rcTab]',
})
export class TabDirective {
  private _disabled = false;
  uniqueId = `rc-tab-${++nextUniqueId}`;
  /**
   * tab label
   */
  @Input('rcTab')
  label: string | TemplateRef<any>;

  /**
   * disabled flag
   */
  @Input('rcTabDisabled')
  get disabled() {
    return this._disabled;
  }

  set disabled(value: boolean) {
    if (this._disabled !== value) {
      this._disabled = value;
      this.group.tabDisabledChanged(this.uniqueId, this._disabled);
    }
  }

  constructor(
    public template: TemplateRef<any>,
    @Inject(forwardRef(() => TabGroupComponent))
    private group: TabGroupComponent,
  ) {}
}

@Component({
  selector: 'rc-tab-group',
  templateUrl: './tab-group.component.html',
  styleUrls: ['./tab-group.component.scss'],
})
export class TabGroupComponent implements AfterContentInit, OnDestroy {
  private currentTabId: string;
  tabsSubscription: Subscription;
  portalInstance: TemplatePortal<any>;
  tabs: Array<RcTab> = [];
  @Input()
  selectedIndex: number;
  @Output()
  selectedIndexChange: EventEmitter<number> = new EventEmitter();
  @ContentChildren(TabDirective)
  tabItems: QueryList<TabDirective>;
  @ViewChild(MatTabGroup)
  tabGroup: MatTabGroup;

  constructor(private viewContainerRef: ViewContainerRef) {}

  isString(label: string) {
    return typeof label === 'string';
  }

  tabDisabledChanged(id: string, targetDisabledState: boolean) {
    if (!this.tabs.length) {
      return;
    }
    const index = this.tabs.findIndex(tab => {
      return tab.id === id;
    });
    if (index < 0) {
      return;
    }
    this.tabs[index].disabled = targetDisabledState;
    if (
      targetDisabledState === true &&
      this.tabs[index].id === this.currentTabId
    ) {
      this.renderDeafultPortal();
    }
  }

  ngAfterContentInit() {
    this.initializeMatTabs();
    this.renderDeafultPortal();

    this.tabsSubscription = this.tabItems.changes.subscribe(() => {
      // const newTabItems = changes;
      this.initializeMatTabs();
      const currentTab = this.tabs.find(tab => {
        return tab.id === this.currentTabId && !tab.disabled;
      });
      // 当 上一次active的tab 在当前QueryList中不存在或 为disabled时，渲染默认的Portal
      // 当 上一次active的tab 在当前QueryList中存在时，更新mat-tab-group的selectedIndex
      if (!currentTab) {
        this.renderDeafultPortal();
      } else {
        const selectedIndex = this.tabs.findIndex(tab => {
          return tab.id === this.currentTabId;
        });
        this.tabGroup.selectedIndex = selectedIndex;
      }
    });
  }

  ngOnDestroy() {
    if (this.tabsSubscription) {
      this.tabsSubscription.unsubscribe();
    }
  }

  initializeMatTabs() {
    this.tabs = this.tabItems.map(item => {
      return {
        id: item.uniqueId,
        label: item.label,
        template: item.template,
        disabled: item.disabled,
      };
    });
  }

  onSelectedIndexChange(selectedIndex: number) {
    this.renderTabPortalByIndex(selectedIndex);
    this.selectedIndexChange.emit(selectedIndex);
  }

  renderDeafultPortal() {
    let portalIndex;
    if (this.selectedIndex >= 0) {
      portalIndex = this.selectedIndex;
    } else {
      portalIndex = this.tabs.findIndex(tab => {
        return !tab.disabled;
      });
    }
    if (portalIndex >= 0) {
      this.tabGroup.selectedIndex = portalIndex;
      this.renderTabPortalByIndex(portalIndex);
    }
  }

  renderTabPortalByIndex(index: number) {
    if (!this.tabs || this.tabs.length - 1 < index) {
      return;
    }
    this.currentTabId = this.tabs[index].id;

    const tempalte = this.tabs[index].template;
    const templatePortal = new TemplatePortal(tempalte, this.viewContainerRef);
    this.portalInstance = templatePortal;
  }
}
