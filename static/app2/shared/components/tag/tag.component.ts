import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'rc-tag',
  templateUrl: 'tag.component.html',
  styleUrls: ['tag.component.scss'],
})
export class TagComponent {
  @Input()
  mode: 'default' | 'success' | 'warning' | 'error' | 'info' = 'default';
  @Input()
  closable: false;

  @Output()
  close = new EventEmitter();
}
