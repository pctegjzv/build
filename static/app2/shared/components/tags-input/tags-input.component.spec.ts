import {
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick,
} from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TagsInputComponent } from 'app2/shared/components/tags-input/tags-input.component';

import { registerCleanup } from '../../../testing/helpers';

describe('TagsInputComponent', () => {
  let comp: TagsInputComponent;
  let fixture: ComponentFixture<TagsInputComponent>;
  let inputEl: HTMLInputElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, BrowserAnimationsModule],
      declarations: [TagsInputComponent],
    });

    fixture = TestBed.createComponent(TagsInputComponent);
    inputEl = fixture.debugElement.nativeElement.querySelector('input');
    comp = fixture.componentInstance;
  });

  it('should be focus state when focused and vice verse', fakeAsync(() => {
    inputEl.dispatchEvent(new FocusEvent('focus'));
    fixture.detectChanges();
    expect(comp.focus).toBe(true);
    inputEl.dispatchEvent(new FocusEvent('blur'));
    tick();
    fixture.detectChanges();
    expect(comp.focus).toBe(false);
  }));
  it('should change inputValue when input changes', fakeAsync(() => {
    fixture.detectChanges();
    tick();
    inputEl.dispatchEvent(new FocusEvent('focus'));
    inputEl.value = 'jakiro';
    inputEl.dispatchEvent(new Event('input'));
    expect(comp.inputValue).toBe('jakiro');
  }));
  it('should save and emit tags and clear input when click enter', fakeAsync(() => {
    tick();
    fixture.detectChanges();
    let savedTags: Array<string>;
    comp.onTagsChange.subscribe((tags: Array<string>) => {
      savedTags = tags;
    });
    inputEl.dispatchEvent(new FocusEvent('focus'));

    inputEl.value = 'rubick';
    inputEl.dispatchEvent(new Event('input'));
    inputEl.dispatchEvent(
      new KeyboardEvent('keydown', {
        key: 'enter',
      }),
    );
    inputEl.dispatchEvent(
      new KeyboardEvent('keydown', {
        key: 'enter',
      }),
    );

    inputEl.value = 'jakiro';
    inputEl.dispatchEvent(new Event('input'));
    inputEl.dispatchEvent(
      new KeyboardEvent('keydown', {
        key: 'enter',
      }),
    );

    expect(inputEl.value === '');
    expect(comp.tags.join('-')).toBe('rubick-jakiro');
    expect(savedTags.join(',')).toBe('rubick,jakiro');
  }));
  // it('should save and emit tags and clear input when click space when addOnSpace=true', fakeAsync(() => {
  //   tick();
  //   comp.addOnSpace = true;
  //   fixture.detectChanges();
  //   let savedTags: Array<string>;
  //   comp.onTagsChange.subscribe((tags: Array<string>) => {
  //     savedTags = tags;
  //   });
  //   inputEl.dispatchEvent(new FocusEvent('focus'));
  //   inputEl.value = 'rubick';
  //   inputEl.dispatchEvent(new KeyboardEvent('keydown', {
  //     key: ' '
  //   }));
  //   inputEl.value = 'jakiro';
  //   inputEl.dispatchEvent(new KeyboardEvent('keydown', {
  //     key: ' '
  //   }));
  //   expect(inputEl.value === '');
  //   expect(comp.tags.join(',')).toBe('rubick,jakiro');
  //   expect(savedTags.join(',')).toBe('rubick,jakiro');
  // }));
  // it('should not add more tags when tags length === max', fakeAsync(() => {
  //   tick();
  //   comp.max = 1;
  //   fixture.detectChanges();
  //   let savedTags: Array<string>;
  //   comp.onTagsChange.subscribe((tags: Array<string>) => {
  //     savedTags = tags;
  //   });
  //   inputEl.dispatchEvent(new FocusEvent('focus'));
  //   inputEl.value = 'rubick';
  //   inputEl.dispatchEvent(new KeyboardEvent('keydown', {
  //     key: 'enter'
  //   }));
  //   inputEl.value = 'jakiro';
  //   inputEl.dispatchEvent(new KeyboardEvent('keydown', {
  //     key: 'enter'
  //   }));
  //   expect(savedTags.join(',')).toBe('rubick');
  // }));
  // it('should delete tags when click backspace', fakeAsync(() => {
  //   tick();
  //   let savedTags: Array<string>;
  //   comp.onTagsChange.subscribe((tags: Array<string>) => {
  //     savedTags = tags;
  //   });
  //   inputEl.dispatchEvent(new FocusEvent('focus'));
  //   inputEl.value = 'rubick';
  //   inputEl.dispatchEvent(new KeyboardEvent('keydown', {
  //     key: 'enter'
  //   }));
  //   inputEl.value = 'jakiro';
  //   inputEl.dispatchEvent(new KeyboardEvent('keydown', {
  //     key: 'enter'
  //   }));
  //   expect(savedTags.join(',')).toBe('rubick,jakiro');
  //   inputEl.dispatchEvent(new KeyboardEvent('keydown', {
  //     key: 'backspace'
  //   }));
  //   expect(savedTags.join(',')).toBe('rubick');
  // }));
  // it('should delete tags when click remove button', fakeAsync(() => {
  //   tick();
  //   let savedTags: Array<string>;
  //   comp.onTagsChange.subscribe((tags: Array<string>) => {
  //     savedTags = tags;
  //   });
  //   inputEl.dispatchEvent(new FocusEvent('focus'));
  //   inputEl.value = 'rubick';
  //   inputEl.dispatchEvent(new KeyboardEvent('keydown', {
  //     key: 'enter'
  //   }));
  //   inputEl.value = 'jakiro';
  //   inputEl.dispatchEvent(new KeyboardEvent('keydown', {
  //     key: 'enter'
  //   }));
  //   expect(savedTags.join(',')).toBe('rubick,jakiro');
  //   fixture.detectChanges();
  //   const removeButton: Element = fixture.nativeElement.querySelectorAll('.rc-tag-item-icon')[0];
  //   removeButton.dispatchEvent(new MouseEvent('click'));
  //   expect(savedTags.join(',')).toBe('jakiro');
  // }));
  // it('should filter added tags according to registered beforeAdd function', fakeAsync(() => {
  //   tick();
  //   let savedTags: Array<string>;
  //   comp.registerBeforeAdd((value: string) => {
  //     const ipRegex = /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
  //     return ipRegex.test(value);
  //   });
  //   comp.onTagsChange.subscribe((tags: Array<string>) => {
  //     savedTags = tags;
  //   });
  //   inputEl.dispatchEvent(new FocusEvent('focus'));
  //   inputEl.value = 'rubick';
  //   inputEl.dispatchEvent(new KeyboardEvent('keydown', {
  //     key: 'enter'
  //   }));
  //   inputEl.value = '192.168.1.1';
  //   inputEl.dispatchEvent(new KeyboardEvent('keydown', {
  //     key: 'enter'
  //   }));
  //   expect(savedTags.join(',')).toBe('192.168.1.1');
  // }));

  registerCleanup();
});
