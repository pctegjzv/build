import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  AfterContentInit,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  Optional,
  Output,
  Self,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';
import $ from 'jquery';
import { debounce } from 'lodash';
import { Subject } from 'rxjs';

import { FormFieldControl } from 'app2/shared/form-field-control';

// Increasing integer for generating unique ids for checkbox components.
let nextUniqueId = 0;

class SuggestionList {
  private tagsInput: TagsInputComponent;

  private suggestions: Array<string>;

  private selectedValues: Array<string>;

  loadWithDebounce: Function;
  items: Array<string>;
  selectedIndex: number;
  selectedItem: string;
  visible: boolean;

  constructor(tagsInput: TagsInputComponent) {
    this.tagsInput = tagsInput;
    this.selectedValues = [];
    this.selectedIndex = -1;
    this.items = [];
    this.suggestions = null;
    this.loadWithDebounce = debounce((query: string) => {
      this.load(query);
    }, 50);
  }

  setSelectedValues(values: Array<string>) {
    this.selectedValues = values;
  }

  setSuggestions(suggestions: Array<string>): void {
    this.suggestions = suggestions;
  }

  // TODO: not finished
  // setSuggestionsSource(fn: Function): void {
  //   this.suggestionsSource = fn;
  // }

  reset() {
    this.visible = false;
    this.selectedIndex = -1;
    this.selectedItem = null;
    this.items = [];
  }

  async load(query = '', visible = true) {
    // TODO: suggestion source support promise
    // if (this.setSuggestionsSource) {
    //   try {
    //     const data = await this.sourceFn(query);
    //     this.items = data.length ? data : [];
    //   } catch (error) {
    //
    //   }
    // }
    this.items = this.suggestions
      .filter(item => {
        return item.includes(query);
      })
      .filter(item => {
        return !this.selectedValues.includes(item);
      });
    if (this.items.length) {
      if (this.selectedIndex > this.items.length) {
        this.selectedIndex = this.items.length - 1;
      }
      this.selectedItem = this.items[this.selectedIndex];
      if (visible) {
        this.show();
      }
    } else {
      this.reset();
    }
  }

  show() {
    this.visible = true;
    // this.cd.detectChanges();   // TODO: Promise模式下，这一行如果注释掉，template中的visible就不起作用 ???
  }

  selectNext() {
    this.select(++this.selectedIndex);
  }

  selectPrior() {
    this.select(--this.selectedIndex);
  }

  select(index: number, shouldScroll = true) {
    if (index < 0) {
      index = this.items.length - 1;
    } else if (index >= this.items.length) {
      index = 0;
    }
    this.selectedIndex = index;
    this.selectedItem = this.items[index];
    if (shouldScroll) {
      this.scrollToSelected(index);
    }
  }

  clear() {
    this.selectedIndex = -1;
    this.selectedItem = null;
  }

  selectAndConfirm(event: MouseEvent, index: number) {
    this.tagsInput.addTag(this.items[index]);
    this.tagsInput.inputValue = '';
    event.preventDefault();
    event.stopImmediatePropagation();
  }

  scrollToSelected(index: number) {
    const element = this.tagsInput.$suggestionsContainer.find('li').eq(index);
    const parent = element.parent();
    const elementTop = element.prop('offsetTop');
    const elementHeight = element.prop('offsetHeight');
    const parentHeight = parent.prop('clientHeight');
    const parentScrollTop = parent.prop('scrollTop');

    if (elementTop < parentScrollTop) {
      parent.prop('scrollTop', elementTop);
    } else if (elementTop + elementHeight > parentHeight + parentScrollTop) {
      parent.prop('scrollTop', elementTop + elementHeight - parentHeight);
    }
  }
}

@Component({
  selector: 'rc-tags-input',
  templateUrl: './tags-input.component.html',
  styleUrls: ['./tags-input.component.scss'],
  animations: [
    trigger('scaleIn', [
      state('in', style({ transform: 'scale(1)', opacity: 1 })),
      transition('void => *', [
        style({ transform: 'scale(.4)', opacity: 0.25 }),
        animate(100),
      ]),
    ]),
  ],
  providers: [{ provide: FormFieldControl, useExisting: TagsInputComponent }],
})
/**
 *  //参考: https://github.com/primefaces/primeng/blob/master/src/app/components/chips/chips.ts
 *
 *  // TODO: support suggestion tags
 */
export class TagsInputComponent
  implements AfterContentInit, FormFieldControl, ControlValueAccessor {
  private _uniqueId = `rc-tags-input-${++nextUniqueId}`;
  private _required = false;
  protected _disabled = false;

  stateChanges = new Subject<void>();
  controlType = 'rc-tags-input';

  @Input()
  id = this._uniqueId;

  /**
   * placeholder string for input element
   */
  @Input()
  placeholder: string;
  /**
   * set true to allow add new tag by clicking `space`
   */
  @Input()
  addOnSpace: boolean;
  /**
   * max number of tags allowed
   */
  @Input()
  maxTags: number;
  /**
   * foldable tags list
   */
  @Input()
  foldable: boolean;
  @Input()
  tagClassFn: Function;

  @Output()
  onTagsChange = new EventEmitter<Array<string>>();
  @ViewChild('wrapper')
  private wrapperEl: ElementRef;
  @ContentChild('rcTagTemplate')
  templateRef: TemplateRef<any>;
  focus: boolean;
  tags: Array<string>;
  tagTemplate: TemplateRef<any>;
  hiddenCount = 0;
  loadSuggestionList: Function;
  inputValue = '';

  private suggestionList: SuggestionList;
  private $wrapper: JQuery;
  private $menu: JQuery;
  $suggestionsContainer: JQuery;

  private beforeAdd: Function = () => {
    return true;
  };

  @Input()
  get value(): Array<string> {
    return this.ngControl ? this.ngControl.value : this.tags;
  }

  set value(value: Array<string>) {
    this.tags = value || [];
    this.stateChanges.next();
    this.emitNewTags();
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  /**
   * set true to disable input element and hide remove icon for tags
   */
  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }
    this._disabled = disabled;
    this.stateChanges.next();
  }

  /** Whether the control is in an error state. */
  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);

    return !!(isInvalid && (isDirty || isSubmitted));
  }

  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() private parentForm: NgForm,
    @Optional() private parentFormGroup: FormGroupDirective,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
    this.tags = [];
  }

  ngAfterContentInit() {
    this.$wrapper = $(this.wrapperEl.nativeElement);
    this.$menu = this.$wrapper.find('ul');
    this.$suggestionsContainer = this.$wrapper.find('.rc-suggestion-list');
  }

  /**
   * register a function which will be invoked before adding new tag,
   * if return false or a rejected promise, the tag will not be added
   * if return a string which is not 'false', the string will be added
   * @param {Function} fn
   */
  registerBeforeAdd(fn: Function): void {
    this.beforeAdd = fn;
  }

  /**
   * add new tag
   * @param {string} value
   */
  addTag(value: string): void {
    if (
      value &&
      !this.tags.includes(value) &&
      (!this.maxTags || this.maxTags > this.tags.length)
    ) {
      const res = this.beforeAdd.call(null, value);
      if (res && res.then) {
        res
          .then(() => {
            this.doAddTag(value);
          })
          .catch(() => {});
      } else if (res === true) {
        this.doAddTag(value);
      } else if (typeof res === 'string' && res !== 'false') {
        this.doAddTag(res);
      }
    }
  }

  private doAddTag(value: string): void {
    this.tags = [...this.tags, value];
    this.emitNewTags();
  }

  /**
   * remove a tag by index
   * @param {number} index
   */
  removeTag(index: number): void {
    if (this.disabled) {
      return;
    }
    this.tags.splice(index, 1);
    this.emitNewTags();
  }

  /**
   * suggestions
   */
  setSuggestions(suggestions: Array<string>): void {
    this.suggestionList = new SuggestionList(this);
    this.suggestionList.setSuggestions(suggestions);
  }

  /**
   * set new tags to replace current tags
   * @param {Array<string | number>} tags
   */
  setTags(tags: Array<string | number>): void {
    this.tags = tags.map(value => String(value));
    this.emitNewTags();
  }

  /**
   * remove all tags
   */
  clearTags(): void {
    this.tags = [];
    this.emitNewTags();
  }

  onInputFocus() {
    this.focus = true;
    if (this.suggestionList) {
      this.suggestionList.load();
    }
    if (this.foldable) {
      this.animateWrapperHeight();
    }
  }

  onInputBlur() {
    this.confirm(false);
    setTimeout(() => {
      this.focus = false;
      if (this.foldable) {
        this.calculateHiddenTags();
        this.animateWrapperHeight();
      } else {
        if (this.suggestionList) {
          this.suggestionList.reset();
        }
      }
    });
  }

  onInputChange() {
    if (this.suggestionList) {
      this.suggestionList.loadWithDebounce(this.inputValue);
    }
  }

  emitNewTags() {
    this.onChangeCallback(this.tags);
    this.onTouchedCallback();
    this.onTagsChange.emit(this.tags);
    this.refreshSuggestions();
  }

  private confirm(shouldRefreshSuggessions = true) {
    if (this.inputValue) {
      this.addTag(this.inputValue);
      this.inputValue = '';
    } else if (this.suggestionList && this.suggestionList.selectedItem) {
      this.addTag(this.suggestionList.selectedItem);
    }
    if (shouldRefreshSuggessions) {
      this.refreshSuggestions();
    }
  }

  onKeydown(event: KeyboardEvent): void {
    switch (event.key.toLowerCase()) {
      case 'backspace':
        if (this.inputValue.length === 0 && this.tags.length > 0) {
          this.tags = [...this.tags];
          this.tags.pop();
          this.emitNewTags();
        }
        break;

      case 'enter':
        this.confirm();
        event.preventDefault();
        event.stopImmediatePropagation();
        break;

      case ' ':
        if (this.addOnSpace) {
          this.confirm();
          event.preventDefault();
        }
        break;

      case 'arrowdown':
        if (this.suggestionList) {
          this.suggestionList.selectNext();
          event.preventDefault();
          event.stopImmediatePropagation();
        }
        break;

      case 'arrowup':
        if (this.suggestionList) {
          this.suggestionList.selectPrior();
          event.preventDefault();
          event.stopImmediatePropagation();
        }
        break;

      default:
        if (this.maxOut) {
          event.preventDefault();
        }
        break;
    }
  }

  suggestionsVisible() {
    return this.suggestionList && this.suggestionList.visible;
  }

  get suggesstionItems(): string[] {
    return this.suggestionList ? this.suggestionList.items : [];
  }

  get maxOut(): boolean {
    return this.maxTags && this.tags.length === this.maxTags;
  }

  get showHiddenCount(): boolean {
    return this.foldable && !this.focus && this.hiddenCount > 0;
  }

  private refreshSuggestions() {
    if (!this.suggestionList) {
      return;
    }
    this.suggestionList.setSelectedValues(this.tags);
    this.suggestionList.load(this.inputValue, false);
  }

  // TODO: add animation for suggestions and foldable
  private animateWrapperHeight() {
    if (this.focus) {
      if (this.hiddenCount > 0) {
        // this.$wrapper.animate({
        //   height: this.$menu.outerHeight() + this.$wrapper.outerHeight()
        // }, 200, () => {
        //   this.$wrapper.css('height', 'auto');
        // });
        this.$wrapper.css('height', 'auto');
      } else {
        this.$wrapper.css('height', 'auto');
      }
    } else {
      // this.$wrapper.animate({
      //   height: 32
      // }, 100);
      this.$wrapper.css('height', 32);
    }
  }

  private calculateHiddenTags() {
    let offsetTop: number;
    let endIndex: number;
    const items = this.$menu.find('li');
    for (let i = 0; i < items.length; i++) {
      const item: HTMLElement = items[i];
      if (!offsetTop) {
        offsetTop = item.offsetTop;
      } else if (offsetTop && offsetTop !== item.offsetTop) {
        endIndex = i;
        break;
      }
    }
    this.hiddenCount = this.tags.length - endIndex;
  }

  writeValue(value: Array<string>) {
    this.tags = value || [];
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onTouchedCallback: () => void = () => {
    // placeholder
  };

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };
}
