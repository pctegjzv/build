import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'rc-view-pager-item',
  templateUrl: 'view-pager-item.component.html',
  styleUrls: ['view-pager-item.component.scss'],
})
export class ViewPagerItemComponent {
  @HostBinding('style.transform')
  transform = '';
}
