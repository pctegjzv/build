import {
  Component,
  ContentChildren,
  ElementRef,
  Input,
  QueryList,
  ViewChild,
} from '@angular/core';

import { ViewPagerItemComponent } from 'app2/shared/components/view-pager/view-pager-item/view-pager-item.component';

@Component({
  selector: 'rc-view-pager',
  templateUrl: 'view-pager.component.html',
  styleUrls: ['view-pager.component.scss'],
})
export class ViewPagerComponent {
  @ContentChildren(ViewPagerItemComponent)
  items: QueryList<ViewPagerItemComponent>;
  @ViewChild('wrapper')
  wrapper: ElementRef;

  // 指示器颜色
  @Input()
  color: string;
  // 只有一页时是否隐藏指示器
  @Input()
  autoHide: boolean;

  activeIndex = 0;

  scrollItems(index: number) {
    const offset = -index;
    const distance = this.wrapper.nativeElement.offsetWidth;
    const transformX = offset * distance;
    this.items.forEach(item => {
      item.transform = `translate(${transformX}px, 0)`;
    });
  }
}
