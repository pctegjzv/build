import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm } from '@angular/forms';
import { safeDump, safeLoad } from 'js-yaml';
import { set as _set } from 'lodash';

import { NotificationService } from 'alauda-ui';
import { LoggerUtilitiesService } from 'app2/shared/services/utility/logger.service';
import {
  YamlCommentParserService,
  YamlEntry,
} from 'app2/shared/services/yaml/yaml-comment-parser.service';

type ExtendedYamlEntry = YamlEntry & { path?: string };

@Component({
  selector: 'rc-yaml-comment-form',
  templateUrl: 'yaml-comment-form.component.html',
  styleUrls: ['yaml-comment-form.component.scss'],
})
export class YamlCommentFormComponent implements OnChanges {
  @Input()
  yaml: string;
  @Input()
  showComment = false;
  @Output()
  ngSubmit = new EventEmitter();
  submitted = false;
  parseYamlError = false;

  yamlEntries: ExtendedYamlEntry[];
  formGroup: FormGroup;

  @ViewChild(NgForm)
  ngForm: NgForm;

  constructor(
    private ycps: YamlCommentParserService,
    private formBuilder: FormBuilder,
    private logger: LoggerUtilitiesService,
    private auiNotificationService: NotificationService,
  ) {}

  ngOnChanges({ yaml }: SimpleChanges): void {
    if (yaml) {
      this.rebuildYaml();
    }
  }

  onSubmit(event: Event): boolean {
    this.submitted = true;
    return this.ngForm.onSubmit(event);
  }

  onNgSubmit(): boolean {
    this.ngSubmit.emit(this.dump());
    return false;
  }

  dump() {
    const json = safeLoad(this.yaml);
    Object.entries(this.formGroup.value).forEach(
      ([path, value]: [string, string]) => {
        const entry = this.yamlEntries.find(entry => entry.path === path);
        _set(json, entry.paths, this.dumpValue(entry.type, value));
      },
    );
    return safeDump(json);
  }

  isEntryMultiline(entry: ExtendedYamlEntry) {
    return typeof entry.value === 'string' && entry.value.includes('\n');
  }

  entryTrackBy(_: number, entry: ExtendedYamlEntry) {
    return entry.paths;
  }

  getEntryPath(paths: string[]) {
    return paths.join('.');
  }

  private rebuildYaml() {
    try {
      this.yamlEntries = this.ycps.parse(this.yaml);
      // Enhance the path with display format
      this.yamlEntries.forEach(
        entry => (entry.path = this.getEntryPath(entry.paths)),
      );
      this.formGroup = this.formBuilder.group(
        this.yamlEntries.reduce((accum, entry) => {
          const control = new FormControl(entry.value);
          return { ...accum, [entry.path]: control };
        }, {}),
      );
    } catch (err) {
      this.parseYamlError = true;
      this.auiNotificationService.error('Failed to parse YAML');
      this.logger.error('Failed to parse YAML: ', err);
    }
  }

  private dumpValue(type: YamlEntry['type'], value: string) {
    if (type === 'string') {
      return value;
    } else {
      try {
        return JSON.parse(value);
      } catch (err) {
        return value;
      }
    }
  }
}
