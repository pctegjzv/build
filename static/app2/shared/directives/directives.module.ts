import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FileReaderDirective } from 'app2/shared/directives/file-reader/file-reader.directive';
import {
  FormDirective,
  RcFormGroupDirective,
} from 'app2/shared/directives/form/form.directive';
import { InputDirective } from 'app2/shared/directives/input/input.directive';
import { PageHeaderContentDirective } from 'app2/shared/directives/page-header-content/page-header-content.directive';
import { RegionBadgeOptionDirective } from 'app2/shared/directives/region-badge-option/region-badge-option.directive';
import { TooltipModule } from 'app2/shared/directives/tooltip/tooltip.module';
import { VisibilityDirective } from 'app2/shared/directives/utilities/visibility.directive';
import { CronValidatorDirective } from 'app2/shared/directives/validators/cron.directive';
import {
  MaxValidatorDirective,
  MinValidatorDirective,
} from 'app2/shared/directives/validators/min-max.directive';

import { CalendarDirective } from './calendar/calendar.directive';
import { LoadingMaskDirective } from './loading-mask/loading-mask.directive';
import {
  RouterStateLinkDirective,
  RouterStateLinkWithoutHrefDirective,
} from './router-state-link/router-state-link.directive';
import {
  TocContentDirective,
  TocContentsContainerDirective,
  TocLinkDirective,
} from './table-of-contents';
import { TrimDirective } from './trim/trim.directive';

const directives = [
  LoadingMaskDirective,
  RouterStateLinkDirective,
  RouterStateLinkWithoutHrefDirective,
  InputDirective,
  FormDirective,
  RcFormGroupDirective,
  VisibilityDirective,
  PageHeaderContentDirective,
  RegionBadgeOptionDirective,
  CalendarDirective,
  TocContentsContainerDirective,
  TocContentDirective,
  TocLinkDirective,
  MinValidatorDirective,
  MaxValidatorDirective,
  TrimDirective,
  CronValidatorDirective,
  FileReaderDirective,
];

@NgModule({
  imports: [CommonModule, TooltipModule],
  declarations: directives,
  exports: [TooltipModule, ...directives],
})
export class DirectivesModule {}
