/* tslint:disable */
import { Directive, Self } from '@angular/core';
import { FormGroupDirective, NgForm } from '@angular/forms';

/**
 * Angular form does not apply ng-submitted class when form is submitted.
 * This is a fix to mimic the AngularJS behavior
 */
@Directive({
  selector: 'form:not([ngNoForm]):not([formGroup]),ngForm,[ngForm]',
  host: {
    '[class.ng-submitted]': 'ngForm.submitted',
  },
})
export class FormDirective {
  constructor(@Self() public ngForm: NgForm) {}
}

/**
 * Angular form does not apply ng-submitted class when form is submitted.
 * This is a fix to mimic the AngularJS behavior
 */
@Directive({
  selector: '[formGroup]',
  host: {
    '[class.ng-submitted]': 'ngFormGroup.submitted',
  },
})
export class RcFormGroupDirective {
  constructor(@Self() public ngFormGroup: FormGroupDirective) {}
}
