/* tslint:disable:use-host-property-decorator */
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  Directive,
  ElementRef,
  HostBinding,
  Input,
  Optional,
  Self,
} from '@angular/core';
import { FormGroupDirective, NgControl, NgForm } from '@angular/forms';
import { Subject } from 'rxjs';

import { FormFieldControl } from 'app2/shared/form-field-control';

import './input.global.scss';

let nextUniqueId = 0;
export type InputSize = 'small' | 'medium';
@Directive({
  selector: 'input[rcInput], textarea[rcInput]',
  host: {
    '[disabled]': 'disabled',
    '[required]': 'required',
    '[attr.autocomplete]': 'autocomplete',
    '[attr.id]': 'id',
    class: 'rc-input-element',
  },
  providers: [{ provide: FormFieldControl, useExisting: InputDirective }],
})
export class InputDirective implements FormFieldControl {
  /** Variables used as cache for getters and setters. */
  protected _disabled = false;
  protected _required = false;
  protected _id: string;
  protected _uid = `rc-input-${nextUniqueId++}`;

  controlType = 'rc-input';
  stateChanges = new Subject<void>();

  @Input()
  rcInputSize: InputSize;

  @HostBinding('class.small')
  get small() {
    return this.rcInputSize === 'small';
  }

  // Native input properties that are overwritten by Angular inputs need to be synced with
  // the native input element. Otherwise property bindings for those don't work.
  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(value: any) {
    this._disabled = coerceBooleanProperty(value);
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  /** The input element's value. */
  @Input()
  get value() {
    return this._elementRef.nativeElement.value;
  }

  set value(value: string) {
    if (value !== this.value) {
      this._elementRef.nativeElement.value = value;
      this.stateChanges.next();
    }
  }

  @Input()
  autocomplete: 'off' | 'on' = 'off';

  /** Unique id of the element. */
  @Input()
  get id() {
    return this._id;
  }

  set id(value: string) {
    this._id = value || this._uid;
  }

  /** Whether the control is in an error state. */
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);

    return !!(isInvalid && (isDirty || isSubmitted));
  }

  constructor(
    protected _elementRef: ElementRef,
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() private parentForm: NgForm,
    @Optional() private parentFormGroup: FormGroupDirective,
  ) {
    // Force setter to be called in case id was not specified.
    this.id = this.id;
  }
}
