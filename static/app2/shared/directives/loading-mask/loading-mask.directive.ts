import { ComponentPortal, DomPortalHost } from '@angular/cdk/portal';
import {
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  ElementRef,
  Injector,
  Input,
  OnDestroy,
  ViewContainerRef,
} from '@angular/core';

import { LoadingMaskComponent } from 'app2/shared/components/loading-mask/loading-mask.component';

import './loading-mask.directive.global.scss';

/**
 * A directive to show loading mask for a component.
 *
 * Example:
 *
 * <your-element [rcLoadingMask]='expression'></your-element>
 */
@Directive({
  selector: '[rcLoadingMask]',
})
export class LoadingMaskDirective implements OnDestroy {
  static OVERLAY_CLASS = 'loading-mask-overlay';
  private overlayRef: ComponentRef<LoadingMaskComponent>;
  private portalHost: DomPortalHost;

  constructor(
    private elementRef: ElementRef<Element>,
    private injector: Injector,
    private viewContainer: ViewContainerRef,
    private componentFactoryResolver: ComponentFactoryResolver,
  ) {}

  @Input()
  set rcLoadingMask(condition: any) {
    if (condition && !this.overlayRef) {
      this.initializeComponent();
    }
    if (this.overlayRef) {
      this.overlayRef.instance.loading = condition;
      this.overlayRef.changeDetectorRef.detectChanges();
    }
  }

  ngOnDestroy(): void {
    if (this.portalHost) {
      this.portalHost.dispose();
      this.elementRef.nativeElement.classList.remove(
        LoadingMaskDirective.OVERLAY_CLASS,
      );
    }
  }

  /**
   * Let CDK portal package handles dynamic loading mask generation.
   */
  private initializeComponent() {
    this.elementRef.nativeElement.classList.add(
      LoadingMaskDirective.OVERLAY_CLASS,
    );
    this.portalHost = new DomPortalHost(
      this.elementRef.nativeElement,
      this.componentFactoryResolver,
      null,
      this.injector,
    );
    const portal = new ComponentPortal(
      LoadingMaskComponent,
      this.viewContainer,
      this.injector,
    );
    this.overlayRef = this.portalHost.attachComponentPortal(portal);
  }
}
