import { TemplatePortal } from '@angular/cdk/portal';
import {
  Directive,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';

import { PageHeaderContentService } from 'app2/shared/directives/page-header-content/page-header-content.service';

/**
 * Dynamically change page header content based on active template.
 *
 * Usage:
 * If you want to customize a page header, wrap a template with *rcPageHeaderContent.
 *
 * eg:
 * <ng-container *rcPageHeaderContent>
 *   ... YOUR TEMPLATE CONTENT
 * </ng-container>
 */
@Directive({
  selector: '[rcPageHeaderContent]',
})
export class PageHeaderContentDirective implements OnInit, OnDestroy {
  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainerRef: ViewContainerRef,
    private pageHeaderContentService: PageHeaderContentService,
  ) {}

  ngOnInit() {
    const portal = new TemplatePortal(this.templateRef, this.viewContainerRef);
    this.pageHeaderContentService.setTemplatePortal(portal);
  }

  ngOnDestroy(): void {
    this.pageHeaderContentService.setTemplatePortal(null);
  }
}
