import { TemplatePortal } from '@angular/cdk/portal';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

/**
 * Native message center for changing page header content dynamically.
 */
@Injectable()
export class PageHeaderContentService {
  private templatePortalSubject = new BehaviorSubject<TemplatePortal<any>>(
    undefined,
  );
  private _templatePortal$: Observable<TemplatePortal<any>>;

  constructor() {
    this._templatePortal$ = this.templatePortalSubject.asObservable();
  }

  get templatePortal$() {
    return this._templatePortal$;
  }

  setTemplatePortal(templatePortal: TemplatePortal<any>) {
    this.templatePortalSubject.next(templatePortal);
  }
}
