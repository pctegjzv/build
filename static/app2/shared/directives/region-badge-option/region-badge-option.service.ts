import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

import { RouterUtilService } from 'app2/core/services/router-util.service';
import {
  NavItemViewModel,
  NavSubItemViewModel,
} from 'app2/layout/nav-list/nav-view-model';
import { Region } from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { LoggerUtilitiesService } from 'app2/shared/services/utility/logger.service';
import { TranslateService } from 'app2/translate/translate.service';

/**
 * Display option for global region badge.
 */
export interface RegionBadgeOption {
  /**
   * Whether or not to show region badge.
   * Should be false most of the time.
   */
  hidden?: boolean;

  /**
   * Filter function for region list. By default, non-healthy regions will be excluded.
   */
  filterFunction?: (region: Region) => boolean;

  /**
   * Callback to be called when a region is about to change. If the result is false, then the
   * displayed region will stay as it is.
   */
  onChange?: (region: Region) => boolean | Promise<boolean>;
}

/**
 * RegionBadgeDirective指令的选项
 */
export interface RegionBadgeDirectiveOption {
  /**
   * 切换集群成功后，切换到的路由状态名 (如app_service.app)。通常来说是当前资源的列表页。
   * 设计上没有引入普通意义的路由参数。如果有这样的需求，请使用 option 字段。
   */
  onChangeState: string;

  /**
   * 切换集群时是否需要用户二次确认。
   */
  onChangeConfirm: boolean;

  /**
   * 过滤模式。
   * 默认情况下只包含健康集群。
   */
  filterMode: 'ALL' | 'DEFAULT';

  /**
   * 用户自定义的完整option，一般来说用不着，除非你想更细致的调整集群切换按钮的行为。
   */
  option: RegionBadgeOption;
}

/**
 * By default, region list will be filtered by healthy state.
 */
export const DEFAULT_REGION_LIST_FILTER = (region: Region) =>
  region.container_manager !== 'NONE' && region.state === 'RUNNING';

@Injectable()
export class RegionBadgeOptionService {
  private regionBadgeOptionSubject = new BehaviorSubject<RegionBadgeOption>(
    this.getDefaultOption(),
  );

  /**
   * BehaviorSubject of RegionBadgeOption changes.
   */
  regionBadgeOption$ = this.regionBadgeOptionSubject.asObservable();

  /**
   * Indicator to identify the option sequence.
   */
  optionCounter = 0;

  constructor(
    private translate: TranslateService,
    private routerUtil: RouterUtilService,
    private logger: LoggerUtilitiesService,
    private modal: ModalService,
    private regionUtilities: RegionUtilitiesService,
    private router: Router,
  ) {}

  getDefaultOption(): RegionBadgeOption {
    return {
      filterFunction: DEFAULT_REGION_LIST_FILTER,
      hidden: false,
    };
  }

  setRegionBadgeOptionByDirective(optionDirective: RegionBadgeDirectiveOption) {
    this.setRegionBadgeOption(this.getOptionByDirective(optionDirective));
  }

  setRegionBadgeOptionByK8sFlag(item: NavItemViewModel | NavSubItemViewModel) {
    if (item.k8sFlag === 'SHOW') {
      this.setRegionBadgeOption(this.getK8sReleatedOption(item));
    }
  }

  setRegionBadgeOption(option: RegionBadgeOption = this.getDefaultOption()) {
    this.optionCounter++;
    this.regionBadgeOptionSubject.next(option);
  }

  private getK8sReleatedOption(
    navItem: NavItemViewModel | NavSubItemViewModel,
  ) {
    const option = this.getDefaultOption();
    return {
      ...option,
      onChange: async (region: Region) => {
        // if target region is not new k8s region, should redirect to home page
        if (!this.regionUtilities.isNewK8sRegion(region)) {
          try {
            await this.modal.confirm({
              title: this.translate.get(
                'region_badge_change_region_confirm_title',
              ),
              content: this.translate.get(
                'region_badge_change_region_confirm_content',
                { name: region.display_name },
              ),
            });
            await this.router.navigateByUrl('home');
          } catch (e) {
            return false;
          }
        } else {
          // k8s region related resource should redirect to 'list page' when region changed
          await this.router.navigateByUrl(navItem.stateName.replace('.', '/'));
        }
        return true;
      },
    };
  }

  private getOptionByDirective(
    directive: RegionBadgeDirectiveOption,
  ): RegionBadgeOption {
    const option = this.getDefaultOption();

    if (directive.onChangeState || directive.onChangeConfirm) {
      option.onChange = async (region: Region) => {
        if (directive.onChangeConfirm) {
          try {
            await this.modal.confirm({
              title: this.translate.get(
                'region_badge_change_region_confirm_title',
              ),
              content: this.translate.get(
                'region_badge_change_region_confirm_content',
                { name: region.display_name },
              ),
            });
          } catch (e) {
            // 意味着不切换集群
            return false;
          }
        }

        if (directive.onChangeState) {
          const { name } = this.routerUtil.getRouterStateSnapshot();

          // 假如用户当前页面的状态跟设定状态一致，则不切换路由
          if (name !== directive.onChangeState) {
            try {
              await this.routerUtil.go(directive.onChangeState);
            } catch (err) {
              this.logger.error('Failed to transit route: ', err);
            }
          }
        }
        return true;
      };
    }
    return {
      ...option,
      filterFunction:
        directive.filterMode === 'ALL'
          ? () => true
          : DEFAULT_REGION_LIST_FILTER,
      // 以上默认值将会被用户提供的option对象覆盖
      ...directive.option,
    };
  }
}
