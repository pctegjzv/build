import {
  Directive,
  HostBinding,
  HostListener,
  Input,
  OnChanges,
} from '@angular/core';

import { RouterUtilService } from 'app2/core/services/router-util.service';

/**
 * Similar to Angular's routerLink directive.
 * Note, this directive is tested in router-util.service
 */
@Directive({
  selector: 'a[rcRouterStateLink]',
})
export class RouterStateLinkDirective implements OnChanges {
  // the url displayed on the anchor element.
  @HostBinding()
  href: string;

  @HostBinding('attr.target')
  @Input()
  target: string;

  /**
   * State name of the route.
   */
  @Input()
  rcRouterStateLink: string;

  /**
   * Params. Can be path parameter or query parameter
   */
  @Input()
  params: { [k: string]: any };

  @HostListener('click', [
    '$event.button',
    '$event.ctrlKey',
    '$event.metaKey',
    '$event.shiftKey',
  ])
  onClick(
    button: number,
    ctrlKey: boolean,
    metaKey: boolean,
    shiftKey: boolean,
  ): boolean {
    if (button !== 0 || ctrlKey || metaKey || shiftKey) {
      return true;
    }

    if (typeof this.target === 'string' && this.target !== '_self') {
      return true;
    }

    this.routerUtilService.go(this.rcRouterStateLink, this.params);
    return false;
  }

  constructor(private routerUtilService: RouterUtilService) {}

  async ngOnChanges() {
    // Update href
    this.href = await this.routerUtilService.href(
      this.rcRouterStateLink,
      this.params,
    );
  }
}

@Directive({
  selector: ':not(a)[rcRouterStateLink]',
})
export class RouterStateLinkWithoutHrefDirective {
  /**
   * State name of the route.
   */
  @Input()
  rcRouterStateLink: string;

  /**
   * Params. Can be path parameter or query parameter
   */
  @Input()
  params: { [k: string]: any };

  @HostListener('click')
  onClick(): boolean {
    this.routerUtilService.go(this.rcRouterStateLink, this.params);
    return false;
  }

  constructor(private routerUtilService: RouterUtilService) {}
}
