/**
 * Features:
 *   - Active link and content when content visible in scroll container.
 *   - Smoth scroll to content when click link.
 *   - Support programly scroll to content.
 *   - Support nested content.
 * Limit:
 *   - container must scrollable.
 *   - link active or fix position styles not included.
 *   - if scroll to then end, scrollHeight - scrollTop === clientHeight, set last content active.
 * Simplest usage:
 * <!-- links -->
 * <aside>
 *   <a rcTocLink="content1" [for]="toc">content1</a>
 *   <a rcTocLink="content2" [for]="toc">content2</a>
 *   <a rcTocLink="content3" [for]="toc">content3</a>
 * </aside>
 * <!-- scroll container -->
 * <article rcTocContentsContainer #toc=toc-contents-container>
 *   <section rcTocContent="content1">...</section>
 *   <section rcTocContent="content2">...</section>
 *   <section rcTocContent="content3">...</section>
 * </article>
 *
 * Directives:
 *   - rcTocContentsContainer
 *     @Output() activedChange: EventEmitter<string>;
 *       emit when actived change, use this for self define links active.
 *     scrollTo(content: string): void;
 *       programly scroll to content.
 *   - rcTocContent
 *     @Input('rcTocContent') rcTocContent: string;
 *       set content name, must unique in same container.
 *     @HostBinding('class.active') active: boolean;
 *       you need define content `active` class for content styling when active.
 *   - (Optional) rcTocLink
 *     @Input() for: TocContentsContainerDirective;
 *       scroll container template reference variable
 *     @Input('rcTocLink') rcTocLink: string;
 *       link target content name.
 *     @HostBinding('class.active') active: boolean;
 *       you need define link `active` class for link styling when active.
 */

export * from './toc-contents-container.directive';
export * from './toc-content.directive';
export * from './toc-link.directive';
