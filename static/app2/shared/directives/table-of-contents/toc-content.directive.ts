import {
  Directive,
  ElementRef,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Optional,
} from '@angular/core';

import { TocContentsContainerDirective } from './toc-contents-container.directive';

@Directive({
  selector: '[rcTocContent]',
})
export class TocContentDirective implements OnInit, OnDestroy {
  @HostBinding('class.active')
  active: boolean;

  @Input()
  rcTocContent: string;

  nativeElement: any;

  constructor(
    elemRef: ElementRef,
    @Optional() private containerDirective: TocContentsContainerDirective,
  ) {
    this.nativeElement = elemRef.nativeElement;
  }

  ngOnInit(): void {
    if (this.containerDirective) {
      this.containerDirective.registerContent(this);
    }
  }

  ngOnDestroy(): void {
    if (this.containerDirective) {
      this.containerDirective.deregisterContent(this);
    }
  }
}
