import {
  ChangeDetectorRef,
  Directive,
  HostBinding,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Subscription } from 'rxjs';

import { TocContentsContainerDirective } from './toc-contents-container.directive';

@Directive({
  selector: '[rcTocLink]',
})
export class TocLinkDirective implements OnInit, OnDestroy {
  @HostBinding('class.active')
  active: boolean;

  @Input()
  for: TocContentsContainerDirective;
  @Input()
  rcTocLink: string;

  private _subs: Subscription[] = [];

  @HostListener('click')
  onClick() {
    this.for.scrollTo(this.rcTocLink);
  }

  constructor(private cdr: ChangeDetectorRef) {}

  ngOnInit() {
    if (!this.for || !this.rcTocLink) {
      return;
    }
    this._subs.push(
      this.for.activedChange.subscribe((actived: string) => {
        if (this.rcTocLink.includes('.')) {
          this.active = this.rcTocLink.split('.').includes(actived);
        } else {
          this.active = actived === this.rcTocLink;
        }
        this.cdr.detectChanges();
      }),
    );
  }

  ngOnDestroy() {
    this._subs.forEach(sub => sub.unsubscribe());
  }
}
