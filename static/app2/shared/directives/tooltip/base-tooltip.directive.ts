import PopupSettings = SemanticUI.PopupSettings;
import { debounce } from 'lodash';

const transitionDuration = 200;

import './tooltip.global.scss';

/**
 * Base tooltip directive for rendering a semantic ui tooltip.
 *
 * FIXME: it is only for legacy rbErrorsTooltip
 */
export class BaseTooltipDirective {
  initialized: boolean;
  debouncedAction: Function;
  noArrow = false;

  // Will hide by default when page scroll
  // .page-scroll-container: the scrolling context for console
  // .md-sidenav-container > [cdk-scrollable]: scolling context for demo
  hideOnScrollContext: string | JQuery =
    '.page-scroll-container, mat-sidenav-container > [cdkscrollable]';

  /** Used to decorate the tooltip style */
  mode: 'default' | 'error' | 'info' = 'default';
  flowing = false;

  constructor(private nativeElement: any) {}

  /**
   * Tooltip should be initialized when host element is ready.
   */
  init(settings: PopupSettings, tooltipClass = '') {
    if (!this.initialized) {
      // Fix "Added non-passive event listener to 'touchstart' event" issue
      const manualHover = settings['on'] === 'hover';
      if (manualHover) {
        settings.on = 'manual';
        this.registerHoverEvent();
      }

      this.popoverHost.popup({
        ...settings,
        className: {
          popup: `ui popup rc-tooltip ${this.mode} ${
            this.flowing ? 'flowing' : ''
          } ${this.noArrow ? 'no-arrow' : ''} ${tooltipClass}`,
        },
        duration: transitionDuration,
        // Move a bit closer to the popup target if no arrow
        distanceAway: this.noArrow ? -8 : 0,
        scrollContext: $(this.hideOnScrollContext),
        // If tooltip is not inline, the we may always hide the tooltip on scroll
        hideOnScroll:
          !settings.inline && settings.on !== 'manual' && !manualHover,
      } as any);
    }
    this.initialized = true;

    this.debouncedAction = debounce((action: PopupSettings.Param) => {
      this.popoverHost.popup(action);
    }, transitionDuration);
  }

  destroy() {
    if (this.initialized) {
      if (this.popoverHost.popup('exists')) {
        this.popoverHost.popup('hide');
      }
      this.popoverHost.popup('destroy');
      this.initialized = false;
    }
  }

  updateTooltipContent(tooltip: string = '') {
    if (this.initialized) {
      if (this.popoverHost.popup('exists')) {
        if (tooltip === '') {
          this.destroy();
        } else {
          this.popoverHost.popup('change content', tooltip);
        }
      } else {
        this.popoverHost.popup('setting', { html: tooltip });
      }
    }
  }

  repositionTooltip() {
    if (this.initialized) {
      if (this.popoverHost.popup('exists')) {
        this.popoverHost.popup('reposition');
      }
    }
  }

  showTooltip() {
    if (this.initialized) {
      this.debouncedAction('show');
    }
  }

  hideTooltip() {
    if (this.initialized) {
      this.debouncedAction('hide');
    }
  }

  protected get popoverHost(): JQuery {
    return $(this.nativeElement);
  }

  /**
   * Semantic-ui will register blocking touchstart for popovers which is a performance issue.
   * In this case, we implemented the following custom event handlers.
   */
  private registerHoverEvent() {
    this.popoverHost.on('mouseover', () => {
      this.showTooltip();
    });

    this.popoverHost.on('mouseleave', () => {
      this.hideTooltip();
    });
  }
}
