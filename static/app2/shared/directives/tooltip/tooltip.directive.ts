import {
  ApplicationRef,
  ChangeDetectorRef,
  ComponentFactoryResolver,
  Directive,
  ElementRef,
  HostListener,
  Injector,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import { Modifiers } from 'popper.js';

import { BaseTooltip } from './base-tooltip';
import { TooltipMode } from './tooltip-content/tooltip-content.component';

@Directive({
  selector: '[rcTooltip]',
  exportAs: 'tooltip',
  providers: [TooltipDirective],
})
export class TooltipDirective extends BaseTooltip
  implements OnChanges, OnDestroy {
  static readonly DELAY_TIMES = 50;

  @Input()
  rcTooltip: number | string | TemplateRef<any>;
  @Input()
  rcTooltipPosition = 'right center';
  // 渲染tooltip的节点, true: 父元素, false: body
  @Input()
  rcTooltipInline = true;
  // 用于计算位置的元素
  @Input()
  rcTooltipTarget: HTMLElement;
  @Input()
  rcTooltipMode: TooltipMode = TooltipMode.Default;
  // manual: 外部调用showTooltip(),hideTooltip()
  @Input()
  rcTooltipOn: 'focus' | 'click' | 'hover' | 'manual' = 'hover';
  @Input()
  rcTooltipNoArrow = false;
  // true: 丢弃自带tooltip样式
  @Input()
  rcTooltipEmpty = false;
  // https://popper.js.org/popper-documentation.html#modifiers..offset
  @Input()
  rcTooltipOffset: number | string = '0,8';
  // https://popper.js.org/popper-documentation.html#modifiers
  @Input()
  rcTooltipModifiers: Modifiers;

  /**
   * usage:
   *    1. somePlainObject = {count: 123};
   *    2. [rcTooltip]="foo"  [rcTooltipContext]="somePlainObject"
   *    3. <ng-template #foo let-data> Counter: {{data.count}} </ng-template>
   * @param context
   */
  @Input()
  rcTooltipContext: any;

  private hostHovered = false;
  private tooltipHovered = false;

  @HostListener('focus')
  onFocus() {
    if (this.rcTooltipOn !== 'focus') {
      return;
    }
    this.showTooltip();
  }

  @HostListener('blur')
  onBlur() {
    if (this.rcTooltipOn !== 'focus') {
      return;
    }
    this.hideTooltip();
  }

  @HostListener('click')
  onClick() {
    if (this.rcTooltipOn !== 'click') {
      return;
    }
    if (!this.portalHost || !this.portalHost.hasAttached()) {
      setTimeout(() => {
        this.showTooltip();
        this.cdr.markForCheck();
      });
    }
  }

  @HostListener('document:click')
  onDocumentClick() {
    if (this.rcTooltipOn !== 'click') {
      return;
    }
    if (this.portalHost && this.portalHost.hasAttached()) {
      this.hideTooltip();
    }
  }

  @HostListener('mouseenter')
  onMouseEnter() {
    this.hostHovered = true;
    if (this.rcTooltipOn !== 'hover') {
      return;
    }
    setTimeout(() => {
      if (this.hostHovered) {
        this.showTooltip();
        this.cdr.markForCheck();
      }
    }, TooltipDirective.DELAY_TIMES);
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.hostHovered = false;
    if (this.rcTooltipOn !== 'hover') {
      return;
    }
    setTimeout(() => {
      if (!this.tooltipHovered && !this.hostHovered) {
        this.hideTooltip();
      }
    }, TooltipDirective.DELAY_TIMES);
  }

  constructor(
    private hostRef: ElementRef,
    injector: Injector,
    appRef: ApplicationRef,
    componentFactoryResolver: ComponentFactoryResolver,
    viewContainerRef: ViewContainerRef,
    private cdr: ChangeDetectorRef,
  ) {
    super(injector, appRef, componentFactoryResolver, viewContainerRef);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (
      (changes.rcTooltip || changes.rcTooltipContext) &&
      this.attachedInstance
    ) {
      this.attachedInstance.context = this.rcTooltipContext;
      this.updateContent(this.rcTooltip);
      this.updatePosition();
    }
  }

  ngOnDestroy() {
    this.hideTooltip();
  }

  showTooltip() {
    if (this.rcTooltip) {
      this.attachView();
    }
  }

  hideTooltip() {
    this.detachTooltip();
    this.hostHovered = false;
    this.tooltipHovered = false;
  }

  toggleTooltip() {
    if (this.showing) {
      this.hideTooltip();
    } else {
      this.showTooltip();
    }
  }

  /* Whether tooltip is showing in the page */
  get showing() {
    return this.portalHost && this.portalHost.hasAttached();
  }

  attachView() {
    this.attachTooltip(
      this.rcTooltipInline
        ? this.hostRef.nativeElement.parentElement
        : TooltipDirective.DEFAULT_HOST,
    );
    this.attachedInstance.context = this.rcTooltipContext;
    this.updateContent(this.rcTooltip);
    this.addTooltipListener(this.attachedInstance.tooltip.nativeElement);
    this.appendInstanceProperties();
  }

  private updateContent(content: number | string | TemplateRef<any>) {
    if (typeof content === 'string' || typeof content === 'number') {
      this.attachedInstance.title = <string>content;
    } else {
      this.attachedInstance.template = <TemplateRef<any>>content;
    }
  }

  private addTooltipListener(elm: HTMLElement) {
    elm.addEventListener('mouseenter', () => {
      this.tooltipHovered = true;
    });
    elm.addEventListener('mouseleave', () => {
      this.tooltipHovered = false;
      if (this.rcTooltipOn !== 'hover') {
        return;
      }
      setTimeout(() => {
        if (!this.hostHovered && !this.tooltipHovered) {
          this.hideTooltip();
        }
      }, TooltipDirective.DELAY_TIMES);
    });
  }

  private appendInstanceProperties() {
    this.setupConfigs({
      mode: this.rcTooltipMode,
      noArrow: this.rcTooltipNoArrow,
      empty: this.rcTooltipEmpty,
      reference: this.rcTooltipTarget || this.hostRef.nativeElement,
      placement: this.rcTooltipPosition,
      offset: this.rcTooltipOffset,
      modifiers: this.rcTooltipModifiers,
    });
  }
}
