import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ErrorsTooltipDirective } from './errors-tooltip.directive';
import { TooltipContentComponent } from './tooltip-content/tooltip-content.component';
import { TooltipDirective } from './tooltip.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [
    TooltipDirective,
    TooltipContentComponent,
    ErrorsTooltipDirective,
  ],
  entryComponents: [TooltipContentComponent],
  exports: [TooltipDirective, TooltipContentComponent, ErrorsTooltipDirective],
})
export class TooltipModule {}
