import { Directive, Input } from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
} from '@angular/forms';

@Directive({
  selector:
    '[rcResourceNameBlackListValidator][formControlName], [rcResourceNameBlackListValidator][ngModel]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: ResourceNameBlackListValidatorDirective,
      multi: true,
    },
  ],
})
export class ResourceNameBlackListValidatorDirective implements Validator {
  @Input()
  rcResourceNameBlackList: string[];
  constructor() {}
  validate(control: AbstractControl): ValidationErrors | null {
    const resourceNameExist = this.rcResourceNameBlackList.includes(
      control.value,
    );
    return resourceNameExist ? { resource_name_existed: true } : null;
  }
}
