import { NgControl, ValidatorFn } from '@angular/forms';
import { Observable } from 'rxjs';

/** An interface which allows a control to work inside of a `FormFieldComponent`. */
/**
 * 注意：这里使用abstract而不是interface是为了更方便的进行依赖注入。
 */
export abstract class FormFieldControl {
  /** The value of the control. */
  value: any;

  /** The element ID for this control. */
  id: string;

  /**
   * Stream that emits whenever the state of the control changes such that the parent `FormFieldComponent`
   * needs to run change detection.
   */
  readonly stateChanges: Observable<void>;

  /** Gets the NgControl for this control. */
  readonly ngControl: NgControl | null;

  /** Whether the control is required. */
  required: boolean;

  /** Whether the control is disabled. */
  disabled: boolean;

  /** Whether the control is in error state */
  readonly errorState: boolean;

  /**
   * An optional name for the control type that can be used to distinguish `rc-form-field` elements
   * based on their control type. The form field will add a class,
   * `rc-form-field-type-{{controlType}}` to its root element.
   */
  readonly controlType?: string;
}

/**
 * The normalized form field definition for constructing a dynamic form field
 */
export class DynamicFormFieldDefinition {
  /**
   * A type of form control in Rubick
   */
  controlType?: string;

  /**
   * Default value. You should not expect it to be updated.
   */
  value?: any;

  /**
   * Name of the form field
   */
  name?: string;

  /**
   * Label to be applied to the form
   */
  label?: string;

  /**
   * Description info (will be placed in the suffix area)
   */
  description?: string;

  /**
   * prefix
   */
  prefix?: string;

  suffix?: string;

  /**
   * Additional custom validators.
   */
  validators?: ValidatorFn[];

  /**
   * Additional fields. Note, not all fields are required by all form controls.
   */
  placeholder?: string;
  type?: string;
  required? = true;
  min?: number;
  max?: number;
  pattern?: string | RegExp;
  minlength? = -1;
  maxlength? = -1;
  readOnly? = false;

  // Dropdown fields
  options?: any[];
  valueField?: string;
  displayField?: string;
}
