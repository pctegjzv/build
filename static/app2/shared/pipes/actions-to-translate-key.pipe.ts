import { Pipe, PipeTransform } from '@angular/core';
import _ from 'lodash';

import { TranslateService } from 'app2/translate/translate.service';

/**
 * Actions to translated text
 */
@Pipe({ name: 'rcActionsToTranslateKeys' })
export class ActionsToTranslateKeys implements PipeTransform {
  constructor(private translateService: TranslateService) {}
  transform(actions: Array<any>, schema: any): string {
    let res: any[] = [];

    if (actions.length > 0) {
      res = _.uniq(
        actions.map(action => {
          return action.split(':')[1];
        }),
      );
      if (res.includes('*') || res.length === Object.keys(schema).length) {
        res = ['rbac_all'];
      }
      res = res.map(act => this.translateService.get(act));
    }

    return res.join(', ');
  }
}
