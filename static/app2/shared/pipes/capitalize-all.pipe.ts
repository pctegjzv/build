import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rcCapitalizeAll',
})
export class CaptalizeAllPipe implements PipeTransform {
  transform(field: string) {
    if (!field) {
      return '';
    }
    const tokens = field.split(' ');
    const formatField = tokens
      .map(
        token => token.charAt(0).toUpperCase() + token.slice(1).toLowerCase(),
      )
      .join(' ');
    return formatField;
  }
}
