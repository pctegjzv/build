import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment';

import { TranslateService } from 'app2/translate/translate.service';

/**
 * UTC string to consumed time.
 */
@Pipe({ name: 'rcDurationToConsumed' })
export class DurationToConsumedPipe implements PipeTransform {
  constructor(private translate: TranslateService) {}

  transform(ms: number): string {
    if (!ms) {
      return '-';
    }

    let message = '';
    if (ms >= 1000) {
      const duration = moment.duration(ms);

      const days = duration.days();
      const hours = duration.hours();
      const minutes = duration.minutes();
      const seconds = duration.seconds();

      message += days ? days + this.translate.get('day') : '';
      message += hours ? hours + this.translate.get('hour') : '';
      message += minutes ? minutes + this.translate.get('minute') : '';
      message += seconds ? seconds + this.translate.get('second') : '';
    } else if (ms > 0) {
      message = this.translate.get('less_than_a_second');
    } else {
      message = '-';
    }

    return message;
  }
}
