import { Pipe, PipeTransform } from '@angular/core';

/**
 * Pipe a title sentence to correct cases.
 */
@Pipe({ name: 'rcFieldNotAvailable' })
export class FieldNotAvailablePipe implements PipeTransform {
  constructor() {}

  transform(field: string): string {
    if (field === null || field === undefined || field === '') {
      return '-';
    }
    return field;
  }
}
