import { Pipe, PipeTransform } from '@angular/core';

export interface Mem {
  value: number;
  unit: string;
}

const MEM_UNITS = [
  'Bytes',
  'KB',
  'MB',
  'GB',
  'TB',
  'PB',
  'EB',
  'ZB',
  'YB',
  'BB',
  'NB',
  'DB',
  'CB',
  'XB',
];

export function fitMem(value: number, unit: string): Mem {
  const i = MEM_UNITS.findIndex(item => item === unit);
  if (value >= 1024 && i < MEM_UNITS.length - 1) {
    return fitMem(value / 1024, MEM_UNITS[i + 1]);
  } else if (value < 1 && i > 0) {
    return fitMem(value * 1024, MEM_UNITS[i - 1]);
  } else {
    return { value: value, unit };
  }
}

function calcMem(value: number, index: number): Mem {
  if (value >= 1024 && index < MEM_UNITS.length - 1) {
    return calcMem(value / 1024, index + 1);
  } else if (value < 1 && index > 0) {
    return calcMem(value * 1024, index - 1);
  } else {
    return { value: value, unit: MEM_UNITS[index] };
  }
}

/*
*
* 2048MB => 2GB
* 0.1GB => 102.4MB
*
* */

@Pipe({ name: 'rcFitMem' })
export class FitMemPipe implements PipeTransform {
  transform(
    value: number,
    unit: string = 'Bytes',
    precision: number = 0,
  ): number | string {
    if (isNaN(value) || !isFinite(value)) {
      return '??';
    }

    const index = MEM_UNITS.findIndex(item => item === unit);
    const mem = calcMem(value, index);
    return `${mem.value.toFixed(precision)} ${mem.unit}`;
  }
}
