import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment';

/**
 * UTC string to consumed time.
 */
@Pipe({ name: 'rcFormatMs' })
export class FormatMs implements PipeTransform {
  transform(ms: number): string {
    if (!ms) {
      return '-';
    }
    return moment(ms).format('YYYY-MM-DD HH:mm:ss');
  }
}
