import { Pipe, PipeTransform } from '@angular/core';

/**
 * Limit string length
 */
@Pipe({ name: 'rcLimitTo' })
export class LimitToPipe implements PipeTransform {
  transform(str: string, len: number): string {
    return str && str.length > len ? str.substr(0, len) : str;
  }
}
