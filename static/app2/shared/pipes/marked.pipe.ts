import { Pipe, PipeTransform } from '@angular/core';

import * as marked from 'marked';

@Pipe({ name: 'rcMarked' })
export class MarkedPipe implements PipeTransform {
  transform(content: string) {
    return marked(content);
  }
}
