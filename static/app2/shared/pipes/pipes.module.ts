import { NgModule } from '@angular/core';
import { CrontabParsePipe } from 'app2/shared/pipes/crontab.pipe';
import { UrlProxy } from 'app2/shared/pipes/url-proxy.pipe';

import { ActionsToTranslateKeys } from './actions-to-translate-key.pipe';
import { BytesFormatterPipe } from './bytes-formatter.pipe';
import { CaptalizeAllPipe } from './capitalize-all.pipe';
import { CrontabNextPipe } from './crontab-next.pipe';
import { DurationToConsumedPipe } from './duration-to-consumed.pipe';
import { FieldNotAvailablePipe } from './field-not-available.pipe';
import { FitMemPipe } from './fit-mem.pipe';
import { FormatMs } from './format-ms.pipe';
import { FormatUtcStrPipe } from './format-utc-str.pipe';
import { LimitToPipe } from './limit-to.pipe';
import { MarkdownToHtmlPipe } from './markdown-to-html.pipe';
import { MarkedPipe } from './marked.pipe';
import { MsToConsumedPipe } from './ms-to-consumed.pipe';
import { ObjectToMapPipe } from './object-to-map.pipe';
import { SvgTranslatePipe } from './svg-translate.pipe';
import { ThousandUnitPipe } from './thousand-unit.pipe';
import { TitleCasePipe } from './title-case.pipe';
import { TrustHtmlPipe } from './trust-html.pipe';
import { TrustStylePipe } from './trust-style.pipe';
import { TrustUrlPipe } from './trust-url.pipe';
import { UtcToConsumedPipe } from './utc-to-consumed.pipe';

const pipes = [
  TrustHtmlPipe,
  FormatUtcStrPipe,
  UtcToConsumedPipe,
  FormatMs,
  MsToConsumedPipe,
  DurationToConsumedPipe,
  ObjectToMapPipe,
  TrustUrlPipe,
  TrustStylePipe,
  TitleCasePipe,
  ThousandUnitPipe,
  FitMemPipe,
  SvgTranslatePipe,
  ActionsToTranslateKeys,
  FieldNotAvailablePipe,
  LimitToPipe,
  BytesFormatterPipe,
  MarkedPipe,
  CaptalizeAllPipe,
  MarkdownToHtmlPipe,
  CrontabNextPipe,
  CrontabParsePipe,
  UrlProxy,
];

@NgModule({
  declarations: pipes,
  exports: pipes,
})
export class PipesModule {}
