import { Pipe, PipeTransform } from '@angular/core';

/**
 * 1000 to 1k.
 */
@Pipe({ name: 'rcThousandUnit' })
export class ThousandUnitPipe implements PipeTransform {
  transform(value: number, precision: number = 2): number | string {
    if (value >= 1000) {
      return (value / 1000).toFixed(precision) + 'k';
    } else {
      return value;
    }
  }
}
