import { Pipe, PipeTransform } from '@angular/core';
import { TitleCaseService } from 'app2/shared/services/utility/title-case.service';

/**
 * Pipe a title sentence to correct cases.
 */
@Pipe({ name: 'rcTitleCase' })
export class TitleCasePipe implements PipeTransform {
  constructor(private titleCase: TitleCaseService) {}

  transform(title: string): string {
    return this.titleCase.capitalize(title);
  }
}
