import { Inject, Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { HttpService } from 'app2/shared/services/http/http.service';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';

/**
 * Format a UTC string.
 */
@Pipe({ name: 'rcUrlProxy' })
export class UrlProxy implements PipeTransform {
  constructor(
    private http: HttpService,
    private domSanitizationService: DomSanitizer,
    @Inject(ENVIRONMENTS) public environments: Environments,
  ) {}

  transform(url: string): Observable<SafeUrl> {
    if (!url || !this.environments.is_private_deploy_enabled) {
      return from([url]);
    }
    return from(
      this.http.request('/ajax/image-proxy', {
        params: {
          url,
        },
        method: 'GET',
        cache: true,
        addNamespace: false,
      }),
    ).pipe(
      map((res: any) => {
        return this.domSanitizationService.bypassSecurityTrustUrl(res.body);
      }),
    );
  }
}
