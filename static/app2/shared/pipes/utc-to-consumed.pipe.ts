import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment';

/**
 * UTC string to consumed time.
 */
@Pipe({ name: 'rcUtcToConsumed' })
export class UtcToConsumedPipe implements PipeTransform {
  transform(utcStr: string): string {
    if (!utcStr) {
      return '-';
    }
    return utcStr ? moment.utc(utcStr).fromNow() : '';
  }
}
