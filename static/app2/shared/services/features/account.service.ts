import { Inject, Injectable } from '@angular/core';

import { ACCOUNT } from 'app2/core/tokens';
import { ConsoleViewType, RcAccount, RcProfile } from 'app2/core/types';

import { HttpService } from '../http/http.service';

export interface UaaAccount {
  username?: string;
  password?: string;
  email?: string;
}

export enum ACCOUNT_TYPE {
  LDAP_ACCOUNT = 'organizations.LDAPAccount',
  ORG_ACCOUNT = 'organizations.Account',
  // 三方 登录进来的账户
  TPA_ACCOUNT = 'organizations.TPAccount',
}

@Injectable()
export class AccountService {
  constructor(
    @Inject(ACCOUNT) public account: RcAccount,
    private httpService: HttpService,
  ) {}

  getUserToken() {
    return this.httpService
      .request('/ajax-sp/account/token', {
        method: 'GET',
        cache: true,
      })
      .then(({ token }) => token);
  }

  getAuthProfile(cache: boolean = true): Promise<RcProfile> {
    return this.httpService.request<RcProfile>('/ajax/auth/profile', {
      method: 'get',
      cache,
    });
  }

  updateNamespaceLogo(namespace: string, file: File) {
    const body = new FormData();
    body.append('logo_file', file);
    return this.httpService.request(`/ajax/users/${namespace}/logo`, {
      method: 'POST',
      body,
    });
  }

  updateSubAccountPassword(data: { username: string }) {
    return this.httpService.request(
      `/ajax/orgs/${this.account.namespace}/accounts/${data.username}`,
      {
        method: 'PUT',
        body: data,
      },
    );
  }

  updateUaaAccountPassword(data: {
    username?: string;
    password: string;
    old_password?: string;
  }) {
    return this.httpService.request(
      `/ajax/orgs/${this.account.namespace}/accounts/${data.username}`,
      {
        method: 'PUT',
        body: data,
      },
    );
  }

  isSubAccount(): boolean {
    return !!this.account.username;
  }

  isUserView() {
    return this.account.view_type === ConsoleViewType.USER;
  }

  isAdminView() {
    return this.account.view_type === ConsoleViewType.ADMIN;
  }

  updateProfile(profile: RcProfile) {
    return this.httpService.request({
      url: '/ajax/auth/profile',
      method: 'PUT',
      body: profile,
      addNamespace: false,
    });
  }

  createUaaAccount(data: { usernames: string[]; email: string }) {
    return this.httpService.request(
      `/ajax/orgs/${this.account.namespace}/accounts`,
      {
        method: 'POST',
        body: data,
      },
    );
  }

  resetAccountPassword(data: RcAccount) {
    return this.httpService.request(
      `/ajax/orgs/${this.account.namespace}/accounts/${
        data.username
      }/reset_password`,
      {
        method: 'POST',
      },
    );
  }
}
