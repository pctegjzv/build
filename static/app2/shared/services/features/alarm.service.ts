import { Inject, Injectable } from '@angular/core';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { LogService } from 'app2/shared/services/features/log.service';
import { SourceStatsCount } from 'app2/shared/services/features/model.types';
import { HttpService } from 'app2/shared/services/http/http.service';

export interface MetricAlarmPayload {
  alarm_actions: {
    notifications: {
      name: string;
      uuid: string;
    }[];
    services: {
      type: string;
      uuid: string;
    }[];
  };
  comparison_operator: string;
  description?: string;
  exclude_tags: string[];
  group_by: string;
  insufficient_actions: {
    notifications: any[];
    services: any[];
  };
  metric_name: string;
  name: string;
  notify_interval: number;
  ok_actions: {
    notifications: any[];
    services: any[];
  };
  period: number;
  space_name: string;
  statistic: string;
  tags: string[];
  threshold: number;
}

@Injectable()
export class AlarmService {
  ALARM_V2_URL: string;
  LOG_ALARM_V2_URL: string;

  constructor(
    private httpService: HttpService,
    private logService: LogService,
    @Inject(ACCOUNT) account: RcAccount,
  ) {
    const namespace = account.namespace;
    this.ALARM_V2_URL = `/ajax/v2/alarms/${namespace}/`;
    this.LOG_ALARM_V2_URL = `/ajax/v2/log_alarms/${namespace}/`;
  }

  getAlarmsCount(): Promise<SourceStatsCount> {
    return this.httpService.request(this.ALARM_V2_URL, {
      method: 'GET',
      params: {
        display_count: 1,
      },
    });
  }

  getAlarms(): Promise<any> {
    return this.httpService
      .request(this.ALARM_V2_URL, {
        method: 'GET',
      })
      .then(({ result }) => result)
      .catch(() => []);
  }

  getAlarmsByService(service_uuid: string): Promise<any> {
    return this.httpService
      .request(this.ALARM_V2_URL, {
        method: 'GET',
        params: {
          service_uuid,
        },
      })
      .then((res: any) => {
        return res.alarms || [];
      })
      .catch(() => []);
  }

  getAlarmDetail(uuid: string): Promise<any> {
    return this.httpService.request(this.ALARM_V2_URL + uuid, {
      method: 'GET',
    });
  }

  createAlarm(payload: any): Promise<any> {
    return this.httpService.request(this.ALARM_V2_URL, {
      method: 'POST',
      body: payload,
    });
  }

  updateAlarm(uuid: string, payload: any): Promise<any> {
    return this.httpService.request(this.ALARM_V2_URL + uuid, {
      method: 'PUT',
      body: payload,
    });
  }

  deleteAlarm(uuid: string): Promise<any> {
    return this.httpService.request(this.ALARM_V2_URL + uuid, {
      method: 'DELETE',
    });
  }

  ackAlarm(uuid: string, alertKeys: string[]): Promise<any> {
    const actions = alertKeys.map((key: string) => {
      return {
        type: 'ack',
        alert_key: key,
      };
    });
    return this.httpService.request(this.ALARM_V2_URL + uuid + '/action', {
      method: 'POST',
      body: actions,
      addNamespace: false,
    });
  }

  deleteSubAlarm(uuid: string, alertKeys: string[]): Promise<any> {
    const actions = {
      type: 'delete',
      alert_key: alertKeys,
    };
    return this.httpService.request(this.ALARM_V2_URL + uuid + '/action', {
      method: 'POST',
      body: actions,
      addNamespace: false,
    });
  }

  getLogAlarms({
    page = 1,
    page_size = 20,
    name = '',
    all = false,
    service_uuid = '',
  }): Promise<any> {
    return this.httpService
      .request(this.LOG_ALARM_V2_URL, {
        method: 'GET',
        params: {
          page,
          page_size,
          name,
          all,
          service_uuid,
        },
      })
      .then((res: any) => {
        res.result = res.result.map((result: any) => {
          if (result.saved_search_detail) {
            result.saved_search_detail.query_conditions = this.logService.queryConditionFilter(
              result.saved_search_detail.query_conditions,
            );
          }
          return result;
        });
        return res;
      });
  }

  getLogAlarmsByService(service_uuid: string): Promise<any> {
    return this.httpService
      .request(this.LOG_ALARM_V2_URL, {
        method: 'GET',
        params: {
          service_uuid,
        },
      })
      .then((res: any) => {
        return res.alarms || [];
      })
      .catch(() => []);
  }

  getLogAlarmDetail(uuid: string): Promise<any> {
    return this.httpService
      .request(this.LOG_ALARM_V2_URL + uuid, {
        method: 'GET',
      })
      .then((result: any) => {
        if (result.saved_search_detail) {
          result.saved_search_detail.query_conditions = this.logService.queryConditionFilter(
            result.saved_search_detail.query_conditions,
          );
        }
        return result;
      });
  }

  createLogAlarm(payload: any): Promise<any> {
    return this.httpService.request(this.LOG_ALARM_V2_URL, {
      method: 'POST',
      body: payload,
    });
  }

  updateLogAlarm(uuid: string, payload: any): Promise<any> {
    return this.httpService.request(this.LOG_ALARM_V2_URL + uuid, {
      method: 'PUT',
      body: payload,
    });
  }

  deleteLogAlarm(uuid: string): Promise<any> {
    return this.httpService.request(this.LOG_ALARM_V2_URL + uuid, {
      method: 'DELETE',
    });
  }
}
