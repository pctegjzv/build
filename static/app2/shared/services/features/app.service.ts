import { Inject, Injectable } from '@angular/core';
import { keyBy, mapValues } from 'lodash';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { K8sComponent } from 'app2/features/service/service.type';
import { SourceStatsCount } from 'app2/shared/services/features/model.types';
import { HttpService } from 'app2/shared/services/http/http.service';
import { LoggerUtilitiesService } from 'app2/shared/services/utility/logger.service';
import { FetchDataResult } from 'app2/shared/services/utility/pagination-data';
import { BatchReponse } from 'app_user/core/types';

export interface Application {
  type: 'App';
  kubernetes?: any;
  resource: {
    uuid: string;
    name: string;
    description: string;
    source?: string;
    category: string;
    create_method: 'YAML' | 'UI';
    created_at: string;
    updated_at: string;
    status: string;
    actions: any;
    services: {
      total: number;
      running: number;
    };
  };
  namespace: {
    uuid: string;
    name: string;
  };
  cluster: {
    uuid: string;
    name: string;
  };
  // 只有在source:"CATALOG"才会有内容
  catalog_info?: any;
  services: K8sComponent[];
}

export interface MirrorApp {
  uuid: string;
  name: string;
  cluster_name: string;
  cluster_uuid: string;
  cluster_display_name: string;
  namespace_name: string;
  namespace_uuid: string;
}

@Injectable()
export class AppService {
  APPS_ENDPOINT: string;
  K8S_APPS_ENDPOINT: string;

  constructor(
    private httpService: HttpService,
    private logger: LoggerUtilitiesService,
    @Inject(ACCOUNT) account: RcAccount,
  ) {
    const namespace = account.namespace;
    this.APPS_ENDPOINT = `/ajax/v1/applications/${namespace}/`;
    this.K8S_APPS_ENDPOINT = '/ajax/v2/apps/';
  }

  // ********** Legacy App API (v1) ********** //

  getApps({
    regionName = '',
    allowMeta = false,
    instance_ip = '',
    page = 1,
    page_size = 20,
    label = '',
    name = '',
    type = '',
    basic = true,
  }): Promise<any> {
    return this.httpService
      .request(this.APPS_ENDPOINT, {
        method: 'GET',
        params: {
          region: regionName,
          meta: allowMeta,
          instance_ip: instance_ip,
          labels: 1,
          label,
          name,
          type,
          basic,
          page,
          page_size,
        },
        addNamespace: false,
      })
      .then(({ results }) => results)
      .catch(() => {
        return new Error('get apps failed');
      });
  }

  getAppsCount(region_name: string): Promise<SourceStatsCount> {
    return this.httpService
      .request(this.APPS_ENDPOINT, {
        method: 'GET',
        params: {
          display_count: 1,
          region: region_name,
        },
      })
      .then((res: SourceStatsCount) => {
        if (!res.count_result) {
          throw new Error('no applications');
        }
        return res;
      });
  }

  // ********** K8S App API (v2) ********** //

  async getK8sApps({
    pageNo = 1,
    pageSize = 20,
    params: {
      cluster = '',
      namespace = '',
      name = '',
      app_name = '', // 精确匹配，有此参数时可以忽略cluster，用户查找多个集群下相同名字的app
      label = '',
      repository_uuid = '',
      template_uuid = '',
    },
  }) {
    // console.log('fetch apps', 'pageNo: ', pageNo, 'pageSize: ', pageSize, 'search: ', search, 'label: ', label);
    return this.httpService
      .request(this.K8S_APPS_ENDPOINT, {
        method: 'GET',
        params: {
          cluster,
          namespace,
          name,
          app_name,
          label,
          repository_uuid,
          template_uuid: template_uuid || '',
          page: pageNo,
          page_size: pageSize,
        },
        addNamespace: false,
      })
      .then((res: FetchDataResult<any>) => res);
  }

  getK8sAppsCount(cluster_name: string) {
    return this.httpService
      .request(this.K8S_APPS_ENDPOINT, {
        method: 'GET',
        params: {
          display_count: 1,
          cluster: cluster_name,
        },
        addNamespace: false,
      })
      .then((res: SourceStatsCount) => {
        if (!res.count_result) {
          throw new Error('no applications');
        }
        return res;
      });
  }

  async getK8sApp(uuid: string): Promise<Application> {
    return this.httpService
      .request(this.K8S_APPS_ENDPOINT + uuid, {
        method: 'GET',
      })
      .then((res: Application) => res);
  }

  createK8sApp(body: any, cluster?: string): Promise<Application> {
    if (!cluster) {
      return this.httpService.request(this.K8S_APPS_ENDPOINT, {
        method: 'POST',
        body,
        addNamespace: false,
      });
    } else {
      // body.cluster = {
      //   name: cluster,
      // };
      return this.httpService.request(
        `${this.K8S_APPS_ENDPOINT}?cluster=${cluster}`,
        {
          method: 'POST',
          body,
          addNamespace: false,
        },
      );
    }
  }

  createK8sAppBatch(
    body: any,
    clusters: string[],
    project_name: string,
  ): Promise<BatchReponse> {
    const requests = clusters.map((cluster: string) => {
      return {
        cluster,
        url: `/v2/apps/?cluster=${cluster}&project_name=${project_name}`,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      addNamespace: false,
      body: {
        common: {
          method: 'POST',
          header: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(body),
        },
        requests: mapValues(
          keyBy(requests, (item: any) => {
            return item.cluster;
          }),
          (item: any) => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }

  patchK8sApp(uuid: string, body: any): Promise<Application> {
    return this.httpService.request(this.K8S_APPS_ENDPOINT + uuid, {
      method: 'PATCH',
      body,
    });
  }

  patchK8sAppBatch(
    uuids: string[],
    body: any,
    project_name: string,
  ): Promise<BatchReponse> {
    const requests = uuids.map((uuid: string) => {
      return {
        uuid,
        url: `/v2/apps/${uuid}?project_name=${project_name}`,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      addNamespace: false,
      body: {
        common: {
          method: 'PATCH',
          header: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(body),
        },
        requests: mapValues(
          keyBy(requests, (item: any) => {
            return item.uuid;
          }),
          (item: any) => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }

  startK8sApp(uuid: string) {
    return this.httpService.request(this.K8S_APPS_ENDPOINT + uuid + '/start', {
      method: 'PUT',
    });
  }

  stopK8sApp(uuid: string) {
    return this.httpService.request(this.K8S_APPS_ENDPOINT + uuid + '/stop', {
      method: 'PUT',
    });
  }

  rollbackK8sApp(uuid: string) {
    return this.httpService.request(
      this.K8S_APPS_ENDPOINT + uuid + '/rollback',
      {
        method: 'PUT',
      },
    );
  }

  deleteK8sAppBatch(
    uuids: string[],
    project_name: string,
  ): Promise<BatchReponse> {
    const requests = uuids.map((uuid: string) => {
      return {
        uuid,
        url: `/v2/apps/${uuid}?project_name=${project_name}`,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      addNamespace: false,
      body: {
        common: {
          method: 'DELETE',
          header: {
            'Content-Type': 'application/json',
          },
        },
        requests: mapValues(
          keyBy(requests, (item: any) => {
            return item.uuid;
          }),
          (item: any) => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }

  deleteK8sApp(uuid: string) {
    return this.httpService.request(this.K8S_APPS_ENDPOINT + uuid, {
      method: 'DELETE',
    });
  }

  getK8sAppYaml(uuid: string): Promise<string> {
    return this.httpService
      .request(this.K8S_APPS_ENDPOINT + uuid + '/yaml', {
        method: 'GET',
        responseType: 'text',
      })
      .then((data: string) => {
        return data;
      })
      .catch(err => {
        this.logger.error(err);
        return '';
      });
  }
}
