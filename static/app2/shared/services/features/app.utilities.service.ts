import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { get } from 'lodash';

import { MessageService, NotificationService } from 'alauda-ui';
import {
  AppService,
  Application,
  MirrorApp,
} from 'app2/shared/services/features/app.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { FetchDataResult } from 'app2/shared/services/utility/pagination-data';
import { TranslateService } from 'app2/translate/translate.service';
import { getCookie } from 'app2/utils/cookie';
import { BatchReponse } from 'app_user/core/types';

@Injectable()
export class AppUtilitiesService {
  constructor(
    private modalService: ModalService,
    private appService: AppService,
    private roleUtilities: RoleUtilitiesService,
    private translateService: TranslateService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private errorsToastService: ErrorsToastService,
    private router: Router,
    private regionService: RegionService,
  ) {}

  /**
   * App status: Running/Warning/Error/Stopped/Deploying
   * @param app
   */
  canStart(app: any) {
    return ['stopped'].includes(app.resource.status.toLowerCase());
  }

  canStop(app: any) {
    return ['running', 'warning', 'error'].includes(
      app.resource.status.toLowerCase(),
    );
  }

  canDelete(app: any) {
    return ['running', 'stopped', 'error', 'warning', 'deploying'].includes(
      app.resource.status.toLowerCase(),
    );
  }

  canUpdate(app: any) {
    return !['deploying'].includes(
      (app.status || app.resource.status).toLowerCase(),
    );
  }

  canRollback(app: any) {
    return !!get(app, 'resource.state.rollbackable');
  }

  /**
   * App permission: app:start
   * show / hide
   * @param service
   */
  showStart(app: any) {
    return this.roleUtilities.resourceHasPermission(
      app,
      'application',
      'start',
    );
  }

  showStop(app: any) {
    return this.roleUtilities.resourceHasPermission(app, 'application', 'stop');
  }

  showUpdate(app: any) {
    return this.roleUtilities.resourceHasPermission(
      app,
      'application',
      'update',
    );
  }

  showDelete(app: any) {
    return this.roleUtilities.resourceHasPermission(
      app,
      'application',
      'delete',
    );
  }

  showRollback(service: any) {
    return this.roleUtilities.resourceHasPermission(
      service,
      'application',
      'update',
    );
  }

  async startApp(app: any) {
    const name = app.resource.name;
    const uuid = app.resource.uuid;
    await this.modalService.confirm({
      title: this.translateService.get('start'),
      content: this.translateService.get('app_service_start_app_confirm', {
        app_name: name,
      }),
    });
    return this.appService.startK8sApp(uuid);
  }

  async stopApp(app: any) {
    const name = app.resource.name;
    const uuid = app.resource.uuid;
    await this.modalService.confirm({
      title: this.translateService.get('stop'),
      content: this.translateService.get('app_service_stop_app_confirm', {
        app_name: name,
      }),
    });
    return this.appService.stopK8sApp(uuid);
  }

  async rollbackApp(app: any) {
    const name = app.resource.name;
    const uuid = app.resource.uuid;
    await this.modalService.confirm({
      title: this.translateService.get('stop'),
      content: this.translateService.get('app_service_rollback_app_confirm', {
        app_name: name,
      }),
    });
    return this.appService.rollbackK8sApp(uuid);
  }

  async deleteApp(app: Application): Promise<any> {
    const name = app.resource.name;
    const uuid = app.resource.uuid;
    await this.modalService.confirm({
      title: this.translateService.get('delete'),
      content: this.translateService.get('app_service_delete_app_confirm', {
        app_name: name,
      }),
    });
    try {
      this.appService.deleteK8sApp(uuid);
      this.auiNotificationService.success({
        content: this.translateService.get('app_delete_success'),
      });
      return;
    } catch (err) {
      this.errorsToastService.error(err);
    }
  }

  async deleteMirrorApps(app: MirrorApp[] | Application): Promise<any> {
    const project_name = getCookie('project');
    if (Array.isArray(app)) {
      const uuids = app.map((_item: any) => {
        return _item.uuid;
      });
      const res: BatchReponse = await this.appService.deleteK8sAppBatch(
        uuids,
        project_name,
      );
      Object.entries(res.responses).forEach(([key, data]) => {
        const cluster = app.find(_item => {
          return _item.uuid === key;
        });
        if (data.code >= 300) {
          const body = JSON.parse(data.body);
          const error = this.errorsToastService.parseErrors({
            errors: body.errors,
          });
          const title = this.translateService.get('cluster_app_delete_error', {
            appName: app[0].name,
            clusterName: cluster.cluster_display_name,
          });
          this.auiNotificationService.error({
            content: `${title}:<br>${error.content}`,
          });
        } else {
          this.auiNotificationService.success({
            content: this.translateService.get('cluster_app_delete_success', {
              appName: app[0].name,
              clusterName: cluster.cluster_display_name,
            }),
          });
        }
      });
      return;
    }
  }

  /**
   * 用户视角创建app
   */
  async createApp({
    payload,
    clusterName,
    clusters,
    routeParams,
  }: {
    payload: {
      resource: { name: string };
      namespace: { name: string };
      kubernetes: any;
    };
    clusterName: string;
    clusters?: string[];
    routeParams: {
      project: string;
      cluster: string;
      namespace: string;
    };
  }) {
    try {
      if (clusters && clusters.length > 1) {
        // batch create
        const project_name = getCookie('project');
        const res: BatchReponse = await this.appService.createK8sAppBatch(
          payload,
          clusters,
          project_name,
        );
        let uuid: string;
        Object.entries(res.responses).forEach(([key, data]) => {
          if (data.code >= 300) {
            const body = JSON.parse(data.body);
            const error = this.errorsToastService.parseErrors({
              errors: body.errors,
            });
            const title = this.translateService.get(
              'cluster_app_create_error',
              {
                appName: payload.resource.name,
                clusterName: key,
              },
            );
            this.auiNotificationService.error({
              content: `${title}:<br>${error.content}`,
            });
          } else {
            this.auiNotificationService.success({
              content: this.translateService.get('cluster_app_create_success', {
                appName: payload.resource.name,
                clusterName: key,
              }),
            });
            if (key === clusterName) {
              const body = JSON.parse(data.body);
              uuid = body.resource.uuid;
            }
          }
        });
        if (uuid) {
          return this.router.navigate([
            '/workspace',
            routeParams,
            'app',
            'detail',
            uuid,
          ]);
        }
      } else {
        const response: Application = await this.appService.createK8sApp(
          payload,
          clusterName,
        );
        this.auiNotificationService.success({
          content: this.translateService.get('cluster_app_create_success', {
            appName: payload.resource.name,
            clusterName: clusterName,
          }),
        });
        return this.router.navigate([
          '/workspace',
          routeParams,
          'app',
          'detail',
          response.resource.uuid,
        ]);
      }
    } catch (err) {
      this.errorsToastService.handleGenericAjaxError({
        errors: err.errors,
        handleNonGenericCodes: true,
        fallbackMessage: this.translateService.get('create_failed'),
      });
    }
  }

  /**
   * 用户视角更新app
   */
  async updateApp({
    payload,
    uuid,
    mirrorApps,
    routeParams,
  }: {
    payload: {
      kubernetes: any;
    };
    uuid: string;
    mirrorApps?: MirrorApp[]; // mirror apps to update, including original app
    routeParams: {
      project: string;
      cluster: string;
      namespace: string;
    };
  }) {
    if (!mirrorApps || !mirrorApps.length) {
      try {
        const response: any = await this.appService.patchK8sApp(uuid, {
          kubernetes: payload.kubernetes,
        });
        this.auiMessageService.success({
          content: this.translateService.get('update_success'),
        });
        return this.router.navigate([
          '/workspace',
          routeParams,
          'app',
          'detail',
          response.resource.uuid,
        ]);
      } catch (err) {
        this.errorsToastService.handleGenericAjaxError({
          errors: err.errors,
          handleNonGenericCodes: true,
          fallbackMessage: this.translateService.get('update_failed'),
        });
      }
    } else if (mirrorApps && mirrorApps.length) {
      const project_name = getCookie('project');
      // batch update
      const appUuids: string[] = mirrorApps.map((app: MirrorApp) => {
        return app.uuid;
      });
      const res: BatchReponse = await this.appService.patchK8sAppBatch(
        appUuids,
        payload,
        project_name,
      );
      const [, data] = Object.entries(res.responses).find(
        ([id]) => id === uuid,
      );
      if (data.code >= 300) {
        this.auiNotificationService.error({
          content: this.translateService.get('update_failed'),
        });
      } else {
        const appName = mirrorApps[0].name;
        Object.entries(res.responses).forEach(([id, data]) => {
          const clusterName = mirrorApps.find((app: MirrorApp) => {
            return app.uuid === id;
          }).cluster_display_name;
          if (data.code >= 300) {
            const body = JSON.parse(data.body);
            const error = this.errorsToastService.parseErrors({
              errors: body.errors,
            });
            const title = this.translateService.get(
              'cluster_app_update_error',
              {
                appName,
                clusterName,
              },
            );
            this.auiNotificationService.error({
              content: `${title}:<br>${error.content}`,
            });
          } else {
            this.auiNotificationService.success({
              content: this.translateService.get('cluster_app_update_success', {
                appName,
                clusterName,
              }),
            });
          }
        });
        return this.router.navigate([
          '/workspace',
          routeParams,
          'app',
          'detail',
          uuid,
        ]);
      }
    }
  }

  async getMirrorApps(appData: Application): Promise<MirrorApp[]> {
    let mirrorApps: MirrorApp[] = [];
    const [cluster, apps] = await Promise.all([
      this.regionService.getCluster(appData.cluster.name),
      this.appService
        .getK8sApps({
          pageSize: 100,
          params: {
            namespace: appData.namespace.name,
            app_name: appData.resource.name,
          },
        })
        .then((res: FetchDataResult<Application>) => {
          return res.results;
        }),
    ]);
    if (cluster.mirror) {
      mirrorApps = (cluster.mirror.regions || [])
        .map((region: { name: string; id: string; display_name: string }) => {
          const app = apps.find((app: Application) => {
            return app.cluster.uuid === region.id;
          });
          return {
            uuid: app ? app.resource.uuid : '',
            name: app ? app.resource.name : '',
            namespace_name: app ? app.namespace.name : '',
            namespace_uuid: app ? app.namespace.uuid : '',
            cluster_name: region.name,
            cluster_uuid: region.id,
            cluster_display_name: region.display_name,
          };
        })
        .filter((app: MirrorApp) => {
          return !!app.uuid;
        });
    }
    return mirrorApps;
  }
}
