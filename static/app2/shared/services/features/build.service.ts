import { Inject, Injectable } from '@angular/core';
import moment from 'moment';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount, RcLog } from 'app2/core/types';
import {
  HistoryCountResult,
  HistoryStatsCount,
  Pagination,
  TimeSection,
} from 'app2/shared/services/features/model.types';
import { HttpService } from 'app2/shared/services/http/http.service';

export interface CodeClient {
  is_authed: boolean | string;
  name: string;
}

export interface PrivateBuildSonarqube {
  integration_instance_id: string;
  integration_instance: {
    name: string;
    id: string;
  };
  step_at: string;
  execution_status: string;
  integration_execution: {
    quality_gate_value: string;
    error: string;
    analysed_at: string;
    report_url: string;
    analysis_result: {
      measures: Array<{
        metric: string;
        value: string;
      }>;
    };
  };
  build_integration_config: {
    project: string;
    project_key: string;
    quality_gate: string;
    code_encoding: string;
    code_language: string;
    code_scan_path: string;
  };
  created_at: string;
  ended_at: string;
}

export interface PrivateBuildArtifact {
  key: string;
  size: number;
  last_modified: string;
}

export interface PrivateBuild {
  notification_id: string;
  status: string;
  config_id: string;
  build_cache_enabled: boolean;
  started_at: string;
  build_id: string;
  ci_config_file_location: string;
  endpoint_name: string;
  docker_repo_path: string;
  namespace: string;
  created_by: string;
  image_repo_id: string;
  ci_enabled: boolean;
  docker_repo_version_tag: string;
  notification_name: string;
  docker_repo_tag: string;
  docker_repo_label_tag: string;
  dockerfile_content: string;
  can_removed: boolean;
  auto_tag_type: string;
  build_image_enabled: boolean;
  is_auto_created: boolean;
  is_scheduled: boolean;
  step_at: number;
  image_repo: {
    project: string;
    registry: {
      display_name: string;
      name: string;
    };
    name: string;
  };
  code_repo: {
    created_at: string;
    dockerfile_location: string;
    code_head_commit: string;
    code_id: string;
    code_repo_client: string;
    code_repo_key_id: number;
    code_repo_type: string;
    code_repo_type_value: string;
    build: string;
    code_repo_username: string;
    build_context_path: string;
    id: number;
    code_repo_path: string;
  };
  build_config_name: string;
  registry_index: string;
  ended_at: string;
  image_id: string;
  config: string;
  created_at: string;
  endpoint_id: string;
  cpu: number;
  memory: number;
  sonarqube: PrivateBuildSonarqube;
  resource_actions?: string[];
  artifacts?: PrivateBuildArtifact[];
}

export interface PrivateBuildConfig {
  config_id: string;
  code_cache_enabled: boolean;
  artifact_upload_enabled: boolean;
  updated_at: string;
  ci_envs: {
    language: string;
    runtimes: Array<{
      name: string;
    }>;
  };
  ci_steps: string[];
  ci_config_file_location: string;
  uuid: string;
  endpoint_name: string;
  auto_build_enabled: string;
  notification: {
    name: string;
    uuid: string;
  };
  image_repo_id: string;
  namespace: string;
  created_by: string;
  ci_enabled: boolean;
  build_image_enabled: boolean;
  privilege: string;
  code_repo: {
    dockerfile_content: string;
    dockerfile_location: string;
    code_id: string;
    web_hook_url: string;
    code_repo_client: string;
    code_repo_webhook_id: string;
    code_repo_key_id: string;
    code_repo_type: string;
    updated_at: string;
    code_repo_public_key: null;
    code_repo_type_value: string;
    code_repo_webhook_code: string;
    code_repo_username: string;
    build_config: string;
    build_context_path: string;
    created_at: string;
    code_repo_path: string;
  };
  auto_tag_type: string;
  customize_tag_rule: string;
  image_repo: {
    project?: {
      project_name: string;
      project_id: string;
    };
    registry: {
      display_name: string;
      name: string;
    };
    name: string;
  };
  name: string;
  customize_tag: string;
  created_at: string;
  endpoint_id: string;
  image_cache_enabled: number;
  resource_actions?: string[];
}

export interface DockerfilerPreset {
  uuid: string;
  name: string;
  display_name: string;
  description: string;
}

@Injectable()
export class BuildService {
  BUILDS_URL = `/ajax/v1/private-builds/${this.account.namespace}/`;
  BUILD_CONFIGS_URL = `/ajax/v1/private-build-configs/${
    this.account.namespace
  }/`;
  CODE_CLIENTS_URL = `/ajax/private-build-code-clients/${
    this.account.namespace
  }/`;
  INTEGRATIONS_URL = `/ajax/private-build-integrations/${
    this.account.namespace
  }/`;
  DOCKERFILE_URL = `/ajax/dockerfile/${this.account.namespace}/`;

  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) private account: RcAccount,
  ) {}

  getBuildsCount({
    start_time,
    end_time,
  }: TimeSection): Promise<HistoryStatsCount> {
    return this.httpService
      .request(this.BUILDS_URL, {
        method: 'GET',
        params: {
          display_count: 1,
          start_time,
          end_time,
        },
      })
      .then(({ count, unit, count_result }) => {
        return {
          count,
          unit,
          count_result: count_result.map((item: HistoryCountResult) => {
            return {
              OK: item.OK,
              ERROR: item.ERROR,
              OTHER: item.OTHER,
              start: parseFloat(moment.utc(item.start).format('X')),
              end: parseFloat(moment.utc(item.end).format('X')),
            };
          }),
        };
      });
  }

  getPrivateBuilds({
    build_config_name,
    registry_name,
    repository_name,
    project_name,
    page,
  }: {
    build_config_name: string;
    registry_name?: string;
    repository_name?: string;
    project_name?: string;
    page?: number;
  }): Promise<Pagination<PrivateBuild>> {
    return this.httpService
      .request({
        method: 'GET',
        url: this.BUILDS_URL,
        params: {
          build_config_name,
          registry_name,
          image_repo_name: repository_name,
          repository_project_name: project_name,
          page,
        },
      })
      .then((result: any) =>
        Object.assign(result, {
          results: result.result_list || result.result,
        }),
      );
  }

  getPrivateBuild(build_id: string) {
    return this.httpService.request<PrivateBuild>({
      method: 'GET',
      url: this.BUILDS_URL + build_id,
    });
  }

  deletePrivateBuild(build_id: string) {
    return this.httpService.request({
      method: 'DELETE',
      url: this.BUILDS_URL + build_id,
    });
  }

  getPrivateBuildLogs({
    build_id,
    start_time,
    end_time,
    limit,
    type,
  }: {
    build_id: string;
    start_time?: number;
    end_time?: number;
    limit?: number;
    type?: string;
  }): Promise<RcLog[]> {
    return this.httpService
      .request({
        method: 'GET',
        url: this.BUILDS_URL + build_id + '/logs',
        params: {
          start_time,
          end_time,
          limit,
          type,
        },
      })
      .then(({ result }) => result);
  }

  getPrivateBuildCodeClients(): Promise<CodeClient[]> {
    return this.httpService
      .request({
        method: 'GET',
        url: this.CODE_CLIENTS_URL,
      })
      .then(({ result }) => result);
  }

  async getPrivateBuildCodeClientOrgs(
    code_client_name: string,
    page = 1,
  ): Promise<string[]> {
    const res = await this.httpService.request<{
      result?: Array<{
        name: string;
      }>;
      page: number;
      pagelen: number;
      size: number;
      values: Array<{
        name: string;
      }>;
    }>({
      method: 'GET',
      url: this.CODE_CLIENTS_URL + code_client_name,
      addNamespace: false,
      params: {
        page,
      },
    });

    if (res.result) {
      return res.result.map(({ name }) => name);
    }

    if (res.size / res.pagelen > res.page) {
      const values = await this.getPrivateBuildCodeClientOrgs(
        code_client_name,
        page + 1,
      );

      return res.values.map(({ name }) => name).concat(values);
    } else {
      return res.values.map(({ name }) => name);
    }
  }

  getPrivateBuildCodeClientAuthUrl({
    codeClientName,
    redirectParams,
  }: {
    codeClientName: string;
    redirectParams: {};
  }): Promise<string> {
    return this.httpService
      .request({
        url: this.CODE_CLIENTS_URL + codeClientName + '/auth-url',
        addNamespace: false,
        method: 'POST',
      })
      .then(({ auth_url }) => {
        const redirectUri = this.generateOauthRedirectUri({
          codeClientName,
          redirectParams,
        });

        // Both GitHub / BitBucket follow Oauth2 RFC. We may need to think about for other cases in the future.
        auth_url += '&redirect_uri=' + encodeURIComponent(redirectUri);

        // Need to confirm with backend about this parameter
        if (codeClientName === 'BITBUCKET') {
          auth_url += '&response_type=code';
        }

        return auth_url;
      });
  }

  private generateOauthRedirectUri({
    codeClientName,
    redirectParams,
  }: {
    codeClientName: string;
    redirectParams: {};
  }) {
    const origin = location.origin;
    const encodedRedirectParams = JSON.stringify(redirectParams);

    // We need to configure /auth-redirect in github application management page
    return `${origin}/console/auth-redirect/${codeClientName}?redirect_params=${encodedRedirectParams}`;
  }

  unlinkPrivateBuildCodeClient({ codeClientName }: { codeClientName: string }) {
    return this.httpService.request({
      method: 'DELETE',
      url: this.CODE_CLIENTS_URL + codeClientName,
      addNamespace: false,
    });
  }

  linkPrivateBuildCodeClient({
    codeClientName,
    code,
    redirectParams,
    oschinaUserEmail,
    oschinaUserPwd,
  }: {
    codeClientName: string;
    code?: string;
    redirectParams?: string;
    oschinaUserEmail: string;
    oschinaUserPwd: string;
  }) {
    const redirectUri = this.generateOauthRedirectUri({
      codeClientName,
      redirectParams,
    });

    return this.httpService.request({
      method: 'POST',
      url: this.CODE_CLIENTS_URL,
      body: {
        code_client_name: codeClientName,
        oschina_user_email: oschinaUserEmail,
        oschina_user_pwd: oschinaUserPwd,
        code,
        state: this.account.namespace,
        redirect_uri: redirectUri,
      },
      addNamespace: false,
    });
  }

  getPrivateBuildConfigs(params?: {
    image_repo_uuid?: string;
    project_name?: string;
    page?: number;
    page_size?: number;
    search?: string;
  }): Promise<Pagination<PrivateBuildConfig>> {
    return this.httpService
      .request({
        url: this.BUILD_CONFIGS_URL,
        method: 'GET',
        addNamespace: false,
        params,
      })
      .then(
        (res: any) =>
          res.results
            ? res
            : {
                results: res.result,
              },
      );
  }

  getPrivateBuildConfig(name: string) {
    return this.httpService.request<PrivateBuildConfig>({
      method: 'GET',
      url: this.BUILD_CONFIGS_URL + name,
    });
  }

  createPrivateBuild(build: {
    build_config_name: string;
    code_head_commit: string;
    code_repo_type_value: string;
  }): Promise<{
    build_id: string;
  }> {
    return this.httpService.request({
      method: 'POST',
      url: this.BUILDS_URL,
      body: build,
    });
  }

  getConfigName(config: PrivateBuildConfig) {
    return config.uuid || config.config_id;
  }

  deletePrivateBuildConfig(configOrName: string | PrivateBuildConfig) {
    return this.httpService.request({
      url:
        this.BUILD_CONFIGS_URL +
        (typeof configOrName === 'string'
          ? configOrName
          : this.getConfigName(configOrName)),
      method: 'DELETE',
      addNamespace: false,
    });
  }

  getSonarqubeLanguages(integration_id: string) {
    return this.httpService.request<{
      languages: Array<{
        name: string;
        key: string;
      }>;
    }>({
      method: 'GET',
      url: this.INTEGRATIONS_URL + `/sonarqube/${integration_id}/languages`,
    });
  }

  getSonarqubeQualityGates(integration_id: string) {
    return this.httpService.request<{
      default: number;
      qualitygates: Array<{
        id: string;
        name: string;
      }>;
    }>({
      method: 'GET',
      url: this.INTEGRATIONS_URL + `/sonarqube/${integration_id}/qualitygates`,
    });
  }

  getDockerfilePresets(): Promise<DockerfilerPreset[]> {
    return this.httpService
      .request({
        method: 'GET',
        url: this.DOCKERFILE_URL,
      })
      .then(({ result }) => result);
  }
}
