import { Injectable } from '@angular/core';

import {
  BUILD_NEW_STEP_MAP,
  BUILD_STATUS_MAP,
  BUILD_STEP_MAP,
} from 'app2/features/build/build.constant';
import { PrivateBuild } from 'app2/shared/services/features/build.service';

@Injectable()
export class BuildUtilitiesService {
  constructor() {}

  getConfigNameByHistory(build: PrivateBuild) {
    return build.config_id;
  }

  getOauthRepoCommitLink({
    clientName,
    repoPath,
    commitId,
  }: {
    clientName: string;
    repoPath: string;
    commitId: string;
  }) {
    if (!commitId) {
      return;
    }

    switch (clientName) {
      case 'GITHUB':
        return `https://github.com/${repoPath}/commit/${commitId}`;
      case 'BITBUCKET':
        return `https://bitbucket.org/${repoPath}/commits/${commitId}`;
      case 'OSCHINA':
        return `https://git.oschina.net/${repoPath}/commit/${commitId}`;
      default:
        return;
    }
  }

  getBuildIconStatus(status: string) {
    if (!status) {
      return 'pending';
    }
    switch (status) {
      case 'B':
      case 'W':
      case 'I':
        return 'deploying';
      case 'S':
        return 'ok';
      case 'D':
      case 'F':
        return 'error';
    }
  }

  getBuildStatusIconText({ status, step_at }: PrivateBuild) {
    if (!status) {
      return '';
    }
    if (status === 'I') {
      if (step_at > 1000) {
        return BUILD_NEW_STEP_MAP[step_at];
      } else {
        return BUILD_STEP_MAP[step_at];
      }
    } else {
      return 'build_status_' + BUILD_STATUS_MAP[status];
    }
  }
}
