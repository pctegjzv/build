import { Injectable } from '@angular/core';
import { keyBy, mapValues } from 'lodash';

import { K8sResourceService } from 'app2/shared/services/features/k8s-resource.service';
import { HttpService } from 'app2/shared/services/http/http.service';
import { FetchDataResult } from 'app2/shared/services/utility/pagination-data';
import { getCookie } from 'app2/utils/cookie';
import { BatchReponse } from 'app_user/core/types';

export interface K8sConfigMap {
  kubernetes?: {
    apiVersion: string;
    data: {
      [key: string]: string;
    };
    kind: 'ConfigMap';
    metadata: {
      name: string;
      uid?: string;
      namespace?: string;
      resourceVersion?: string;
      annotations?: {
        [key: string]: string;
      };
    };
  };
  resource_actions?: string[];
}

export interface ConfigMapOption {
  name: string;
  uuid: string;
  namespace: string;
  data: any;
}
export interface MirrorConfigmap {
  uuid: string;
  name: string;
  cluster_uuid: string;
  cluster_name: string;
  cluster_display_name: string;
  namespace_name: string;
}

@Injectable()
export class ConfigmapService {
  CLUSTERS_ENDPOINT = '/ajax/v2/kubernetes/clusters/';

  constructor(
    private httpService: HttpService,
    private k8sResourceService: K8sResourceService,
  ) {}

  // ********** K8S Configmap ********** //

  getK8sConfigmaps(parmas: {
    clusterId: string;
    namespace?: string;
    name?: string;
  }): Promise<K8sConfigMap[]>;
  getK8sConfigmaps(parmas: {
    clusterId: string;
    namespace?: string;
    page: number;
    page_size: number;
    name?: string;
  }): Promise<FetchDataResult<K8sConfigMap>>;
  getK8sConfigmaps({
    clusterId,
    namespace = '',
    page,
    page_size,
    name,
  }: {
    clusterId: string;
    namespace?: string;
    page?: number;
    page_size?: number;
    name?: string;
  }): Promise<K8sConfigMap[] | FetchDataResult<K8sConfigMap>> {
    return this.httpService
      .request(
        this.CLUSTERS_ENDPOINT +
          clusterId +
          '/configmaps/' +
          (namespace ? namespace + '/' : ''),
        {
          method: 'GET',
          params: {
            name,
            page,
            page_size,
          },
          addNamespace: false,
        },
      )
      .then((res: any) => (res.results ? res : res.result));
  }

  getConfigMapOptions({
    clusterId,
    namespace,
    name,
  }: {
    clusterId: string;
    namespace?: string;
    name?: string;
  }): Promise<ConfigMapOption[]> {
    return this.getK8sConfigmaps({
      clusterId,
      namespace,
      name,
    })
      .then(results =>
        results.map(result => ({
          name: result.kubernetes.metadata.name,
          uuid: result.kubernetes.metadata.uid,
          namespace: result.kubernetes.metadata.namespace,
          data: result.kubernetes.data,
        })),
      )
      .catch(() => []);
  }

  getK8sConfigmap({
    clusterId,
    namespace,
    name,
  }: {
    clusterId: string;
    namespace: string;
    name: string;
  }) {
    return this.httpService.request<K8sConfigMap>(
      this.CLUSTERS_ENDPOINT +
        clusterId +
        '/configmaps/' +
        namespace +
        '/' +
        name,
      {
        method: 'GET',
      },
    );
  }

  async createK8sConfigmap(
    clusterId: string,
    configMapInfo: K8sConfigMap['kubernetes'],
    clusters?: string[],
  ) {
    if (clusters && clusters.length) {
      const project_name = getCookie('project');
      const res: BatchReponse = await this.createK8sConfigmapBatch(
        configMapInfo,
        clusters,
        project_name,
      );
      this.k8sResourceService.reourceBatchNotification(
        'cluster_configMap_create',
        res,
        configMapInfo.metadata.name,
      );
      return;
    }
    return this.httpService.request<K8sConfigMap>(
      this.CLUSTERS_ENDPOINT + clusterId + '/configmaps/',
      {
        method: 'POST',
        body: configMapInfo,
        addNamespace: false,
      },
    );
  }

  createK8sConfigmapBatch(
    body: K8sConfigMap['kubernetes'],
    clusters: string[],
    project_name: string,
  ): Promise<BatchReponse> {
    const requests = clusters.map((cluster: string) => {
      return {
        cluster,
        url: `/v2/kubernetes/clusters/${cluster}/configmaps?project_name=${project_name}`,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      addNamespace: false,
      body: {
        common: {
          method: 'POST',
          header: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(body),
        },
        requests: mapValues(
          keyBy(requests, (item: any) => {
            return item.cluster;
          }),
          (item: any) => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }

  async updateK8sConfigmap(
    clusterId: string,
    configMapInfo: K8sConfigMap,
    clusters?: string[],
  ) {
    if (clusters && clusters.length) {
      const res: BatchReponse = await this.updateConfigmapBatch(
        clusters,
        configMapInfo,
      );
      this.k8sResourceService.reourceBatchNotification(
        'cluster_configMap_update',
        res,
        configMapInfo.kubernetes.metadata.name,
      );
      return;
    }
    return this.updateConfigmap(clusterId, configMapInfo);
  }

  updateConfigmap(clusterId: string, configMap: K8sConfigMap) {
    return this.httpService.request({
      method: 'PUT',
      url:
        this.CLUSTERS_ENDPOINT +
        clusterId +
        '/configmaps/' +
        configMap.kubernetes.metadata.namespace +
        '/' +
        configMap.kubernetes.metadata.name,
      body: configMap.kubernetes,
      addNamespace: false,
    });
  }

  updateConfigmapBatch(
    clusters: string[],
    configMap: K8sConfigMap,
  ): Promise<BatchReponse> {
    const project_name = getCookie('project');
    const requests = clusters.map((cluster: string) => {
      return {
        cluster,
        url: `/v2/kubernetes/clusters/${cluster}/configmaps/${
          configMap.kubernetes.metadata.namespace
        }/${configMap.kubernetes.metadata.name}?project_name=${project_name}`,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      addNamespace: false,
      body: {
        common: {
          method: 'PUT',
          header: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(configMap.kubernetes),
        },
        requests: mapValues(
          keyBy(requests, (item: any) => {
            return item.cluster;
          }),
          (item: any) => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }

  deleteK8sConfigmap(clusterId: string, configMap: K8sConfigMap) {
    return this.httpService.request(
      this.CLUSTERS_ENDPOINT +
        clusterId +
        '/configmaps/' +
        configMap.kubernetes.metadata.namespace +
        '/' +
        configMap.kubernetes.metadata.name,
      {
        method: 'DELETE',
      },
    );
  }

  deleteConfigmapBatch(
    mirrorConfigmap: MirrorConfigmap[],
  ): Promise<BatchReponse> {
    const project_name = getCookie('project');
    const requests = mirrorConfigmap.map((item: MirrorConfigmap) => {
      return {
        cluster: item.cluster_name,
        url: `/v2/kubernetes/clusters/${item.cluster_name}/configmaps/${
          item.namespace_name
        }/${item.name}?project_name=${project_name}`,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      addNamespace: false,
      body: {
        common: {
          method: 'DELETE',
          header: {
            'Content-Type': 'application/json',
          },
        },
        requests: mapValues(
          keyBy(requests, (item: any) => {
            return item.cluster;
          }),
          (item: any) => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }

  async getMirrorConfigmaps(
    clusterName: string,
    namespace: string,
    name: string,
  ): Promise<any> {
    const mirrorConfigmap: MirrorConfigmap[] = [];
    const mirrorCluster = await this.k8sResourceService.getMirrorCluster(
      clusterName,
    );
    if (mirrorCluster.length) {
      const clusters = mirrorCluster.map((item: any) => {
        return item.name;
      });
      const res: BatchReponse = await this.k8sResourceService.getMirrorResources(
        clusters,
        namespace,
        'configmaps',
        name,
      );
      Object.entries(res.responses).forEach(([id, data]) => {
        if (data.code < 300) {
          const body = JSON.parse(data.body);
          const cluster = mirrorCluster.find(cluster => {
            return cluster.name === id;
          });
          const metadata = body.kubernetes.metadata;
          mirrorConfigmap.push({
            uuid: metadata.uid,
            name: metadata.name,
            namespace_name: metadata.namespace,
            cluster_uuid: cluster ? cluster.id : '',
            cluster_name: cluster ? cluster.name : '',
            cluster_display_name: cluster ? cluster.display_name : '',
          });
        }
      });
    }
    return mirrorConfigmap;
  }
}
