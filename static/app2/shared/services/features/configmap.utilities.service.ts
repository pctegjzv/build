import { Injectable } from '@angular/core';
import { NotificationService } from 'alauda-ui';

import {
  ConfigmapService,
  K8sConfigMap,
  MirrorConfigmap,
} from 'app2/shared/services/features/configmap.service';
import { K8sResourceService } from 'app2/shared/services/features/k8s-resource.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import { BatchReponse } from 'app_user/core/types';

@Injectable()
export class ConfigmapUtilitiesService {
  constructor(
    private modalService: ModalService,
    private configmapService: ConfigmapService,
    private roleUtilities: RoleUtilitiesService,
    private translateService: TranslateService,
    private k8sResourceService: K8sResourceService,
    private notificationService: NotificationService,
  ) {}

  showUpdate(configmap: K8sConfigMap) {
    return this.roleUtilities.resourceHasPermission(
      configmap,
      'configmap',
      'update',
    );
  }

  showDelete(configmap: K8sConfigMap) {
    return this.roleUtilities.resourceHasPermission(
      configmap,
      'configmap',
      'delete',
    );
  }

  async updateConfigmap(
    clusterId: string,
    configmap: K8sConfigMap,
    clusters?: string[],
  ) {
    await this.modalService.confirm({
      title: this.translateService.get('update'),
      content: this.translateService.get(
        'configmap_service_update_configmap_confirm',
        {
          configmap_name: configmap.kubernetes.metadata.name,
        },
      ),
    });
    if (clusters && clusters.length) {
      const res: BatchReponse = await this.configmapService.updateConfigmapBatch(
        clusters,
        configmap,
      );
      this.k8sResourceService.reourceBatchNotification(
        'cluster_configMap_update',
        res,
        configmap.kubernetes.metadata.name,
      );
      return;
    }
    return this.configmapService.updateConfigmap(clusterId, configmap);
  }

  async updateConfigmapAddKey(
    clusterId: string,
    configmap: K8sConfigMap,
    key: string,
  ) {
    await this.modalService.confirm({
      title: this.translateService.get('add'),
      content: this.translateService.get(
        'configmap_service_add_key_configmap_confirm',
        {
          configmap_key: key,
        },
      ),
    });
    return this.configmapService.updateConfigmap(clusterId, configmap);
  }

  async updateConfigmapUpdateKey(
    clusterId: string,
    configmap: K8sConfigMap,
    key: string,
  ) {
    await this.modalService.confirm({
      title: this.translateService.get('update'),
      content: this.translateService.get(
        'configmap_service_update_key_configmap_confirm',
        {
          configmap_key: key,
        },
      ),
    });
    return this.configmapService.updateConfigmap(clusterId, configmap);
  }

  async deleteConfigmapKey(
    clusterId: string,
    configmap: K8sConfigMap,
    key: string,
  ) {
    await this.modalService.confirm({
      title: this.translateService.get('delete'),
      content: this.translateService.get(
        'configmap_service_delete_key_configmap_confirm',
        {
          configmap_key: key,
        },
      ),
    });
    return this.configmapService.updateConfigmap(clusterId, configmap);
  }

  async deleteConfigmap(clusterId: string, configmap: K8sConfigMap) {
    await this.modalService.confirm({
      title: this.translateService.get('delete'),
      content: this.translateService.get(
        'configmap_service_delete_configmap_confirm',
        {
          configmap_name: configmap.kubernetes.metadata.name,
        },
      ),
    });
    try {
      await this.configmapService.deleteK8sConfigmap(clusterId, configmap);
      this.notificationService.success({
        content: this.translateService.get('delete_configmap_success'),
      });
      return;
    } catch (err) {
      throw err;
    }
  }

  async deleteMirrorConfigmap(
    mirrorConfigmap: MirrorConfigmap[],
    configmap: K8sConfigMap,
  ) {
    if (mirrorConfigmap && mirrorConfigmap.length) {
      const res: BatchReponse = await this.configmapService.deleteConfigmapBatch(
        mirrorConfigmap,
      );
      this.k8sResourceService.reourceBatchNotification(
        'cluster_configMap_delete',
        res,
        configmap.kubernetes.metadata.name,
      );
      return;
    }
  }
}
