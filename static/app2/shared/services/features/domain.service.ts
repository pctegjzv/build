import { Inject, Injectable } from '@angular/core';
import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { HttpService } from 'app2/shared/services/http/http.service';

import { ResourceList } from 'app_user/core/types';

export interface Domain {
  domain_id?: string;
  domain: string;
  projects: string[];
  resource_actions?: string[];
}

@Injectable()
export class DomainService {
  CLUSTERS_ENDPOINT = '/ajax/v2/domains/';

  constructor(
    @Inject(ACCOUNT) public account: RcAccount,
    private httpService: HttpService,
  ) {}

  getDomains(
    ignoreProject: boolean,
    params?: {
      page: number;
      page_size: number;
      search: string;
    },
  ): Promise<ResourceList | any> {
    const urlQuery = params
      ? `?page=${params.page}&page_size=${params.page_size}&search=${
          params.search
        }`
      : '';

    return this.httpService.request(
      `${this.CLUSTERS_ENDPOINT}${this.account.namespace}${urlQuery}`,
      {
        method: 'GET',
        addNamespace: false,
        ignoreProject,
      },
    );
  }

  createDomain(payload: Domain) {
    return this.httpService.request(
      `${this.CLUSTERS_ENDPOINT}${this.account.namespace}`,
      {
        method: 'POST',
        body: payload,
        addNamespace: false,
      },
    );
  }

  updateDomainBindProject(domain_id: string, projects: string[]) {
    return this.httpService.request(
      `${this.CLUSTERS_ENDPOINT}${this.account.namespace}/${domain_id}`,
      {
        method: 'PUT',
        body: projects,
        addNamespace: false,
      },
    );
  }

  deleteDomain(domain_id: string) {
    return this.httpService.request(
      `${this.CLUSTERS_ENDPOINT}${this.account.namespace}/${domain_id}`,
      {
        method: 'DELETE',
        addNamespace: false,
      },
    );
  }
}
