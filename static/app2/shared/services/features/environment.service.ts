import { Inject, Injectable } from '@angular/core';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { get } from 'lodash';

export const QUOTA_TYPES = ['cpu', 'memory', 'storage', 'pvc_num', 'pods'];

@Injectable()
export class EnvironmentService {
  constructor(@Inject(ENVIRONMENTS) public environments: Environments) {}

  getDefaultQuota(type: 'namespace' | 'project') {
    let baseRate = 1;
    let transferType = type;
    try {
      const defaultQuota = JSON.parse(this.environments.default_quota);
      const newQuota = {};
      if (
        type === 'project' &&
        defaultQuota &&
        !defaultQuota['project'] &&
        defaultQuota['namespace']
      ) {
        transferType = 'namespace';
        baseRate = 2;
      }
      if (!defaultQuota || !defaultQuota[transferType]) {
        return null;
      }
      QUOTA_TYPES.forEach(quotaType => {
        newQuota[quotaType] =
          parseFloat(get(defaultQuota[transferType], quotaType)) * baseRate ||
          0;
      });
      return newQuota;
    } catch (e) {
      console.error(e);
      return null;
    }
  }
}
