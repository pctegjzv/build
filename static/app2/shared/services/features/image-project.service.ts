import { Inject, Injectable } from '@angular/core';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { HttpService } from 'app2/shared/services/http/http.service';
import { TranslateService } from 'app2/translate/translate.service';

export interface ImageProject {
  resource_actions: string[];
  project_name: string;
  display_name?: string;
  name: string;
  repo_count: number;
  project_id: string;
  namespace: string;
  created_by: string;
  registry: {
    uuid: string;
    name: string;
  };
}

@Injectable()
export class ImageProjectService {
  URL = `/ajax/registries/${this.account.namespace}`;

  constructor(
    private httpService: HttpService,
    private translate: TranslateService,
    @Inject(ACCOUNT) public account: RcAccount,
  ) {}

  find(registry_name: string): Promise<ImageProject[]> {
    return this.httpService
      .request({
        url: `${this.URL}/${registry_name}/projects`,
        method: 'GET',
        addNamespace: false,
      })
      .then(({ result }) => [
        {
          project_name: '@@shared',
          display_name: this.translate.get('default_project_name'),
        },
        ...result.map((project: ImageProject) => ({
          display_name: project.project_name,
          ...project,
        })),
      ]);
  }

  getProjects(registry_name: string): Promise<ImageProject[]> {
    return this.httpService
      .request({
        url: `${this.URL}/${registry_name}/projects`,
        method: 'GET',
        addNamespace: false,
      })
      .then(({ result }) => result);
  }

  deleteProject(registry_name: string, project_name: string): Promise<void> {
    return this.httpService.request({
      url: `${this.URL}/${registry_name}/projects/${project_name}`,
      method: 'DELETE',
      addNamespace: false,
    });
  }

  addProject(
    registry_name: string,
    project_name: string,
  ): Promise<ImageProject> {
    return this.httpService.request({
      url: `${this.URL}/${registry_name}/projects`,
      method: 'POST',
      addNamespace: false,
      body: {
        name: project_name,
      },
    });
  }
}
