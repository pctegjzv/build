import { Inject, Injectable } from '@angular/core';
import { pick } from 'lodash';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { HttpService } from 'app2/shared/services/http/http.service';

export interface ImageRegistry {
  tag_max_number: number;
  protocol: 'http' | 'https';
  region_id: string;
  updated_at: string;
  issuer: string;
  resource_actions: string[];
  display_name: string;
  uuid: string;
  namespace: string;
  created_by: string;
  schedule_config_secret_key: string;
  garbage_collect_schedule_rule: string;
  description: string;
  auto_garbage_collect: boolean;
  is_public: string;
  schedule_config_id: string;
  integration_id: string;
  endpoint: string;
  name: string;
  created_at: string;
  tag_protected_max_number: string;
  audience: string;
  channel: string;
  is_third: boolean;
}

export interface ThirdPartyRegistry {
  is_http: boolean;
  endpoint: string;
  dest_type: string;
  internal_id: string;
  username: string;
  password: string;
}

@Injectable()
export class ImageRegistryService {
  URL = `/ajax/registries/${this.account.namespace}`;

  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) public account: RcAccount,
  ) {}

  find(): Promise<ImageRegistry[]> {
    return this.httpService
      .request({
        url: this.URL,
        method: 'GET',
        addNamespace: false,
      })
      .then(({ result }) => result);
  }

  getRegistry(registry_name: string): Promise<ImageRegistry> {
    return this.find().then(registries =>
      registries.find(({ name }) => name === registry_name),
    );
  }

  updateRegistry(registry: ImageRegistry) {
    return this.httpService.request({
      url: this.URL + '/' + registry.uuid,
      method: 'PUT',
      addNamespace: false,
      body: pick(
        registry,
        'integration_id',
        'tag_max_number',
        'auto_garbage_collect',
        'garbage_collect_schedule_rule',
      ),
    });
  }
}
