import { Inject, Injectable } from '@angular/core';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { Pagination } from 'app2/shared/services/features/model.types';
import { HttpService } from 'app2/shared/services/http/http.service';

export interface ImageRepository {
  description: string;
  scan_enabled: boolean;
  tag_protected_count: number;
  updated_at: string;
  full_description: string;
  registry: {
    integration_id: string;
    endpoint: string;
    display_name: string;
    uuid: string;
    tag_protected_max_number: number;
    tag_max_number: number;
    is_public: boolean;
    name: string;
  };
  download: number;
  is_public: boolean;
  icon: string;
  resource_actions: string[];
  pushed_at: string;
  uuid: string;
  name: string;
  created_at: string;
  pulled_at: string;
  namespace: string;
  upload: string;
  created_by: string;
  project: {
    project_name: string;
    project_id: string;
  };
}

export interface ImageRepositoryTag {
  tag_name: string;
  level:
    | 'High'
    | 'Medium'
    | 'Low'
    | 'Negligible'
    | 'Unknown'
    | 'Failed'
    | 'Analyzing';
  summary?: {
    [key: string]: string;
  };
  created_at?: string;
  finished?: string;
  digest?: string;
  protected?: boolean;
}

export interface ImageRepositoryTagDetail {
  run_command?: string;
  entrypoint?: string;
  instance_ports?: number[];
  volumes?: string[];
  image_envvars?: {
    name: string;
    value: string;
  }[];
}

export interface ImageRepositoryTagReportSummary {
  total: number;
  fixable: number;
  detail: {
    High: number;
    Medium: number;
    Low: number;
    Negligible: number;
    Unknown: number;
    Security?: number;
  };
}

export interface ImageRepositoryTagReportDetail {
  cve_name?: string;
  level?: string;
  package_name: string;
  cur_version: string;
  secure_version?: string;
  layer_command: string;
  summary?: {
    count: number;
    level: string;
  };
  updated?: {
    count: number;
    level: string;
  };
}

export interface ImageRepositoryTagArtifacts {
  tag_name: string;
  build_id: string;
  build_at: string;
  artifacts_count: number;
  artifacts: ImageRepositoryTagArtifact[];
}

export interface ImageRepositoryTagArtifact {
  key: string;
  size: string;
  last_modified: string;
  path?: string;
}

@Injectable()
export class ImageRepositoryService {
  URL = `/ajax/registries/${this.account.namespace}`;

  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) public account: RcAccount,
  ) {}

  private getUrlPrefix(registry_name: string, projectNameOrNamespace?: string) {
    const prefix = `${this.URL}/${registry_name}/`;
    return projectNameOrNamespace
      ? `${prefix}projects/${projectNameOrNamespace}/repositories`
      : `${prefix}repositories`;
  }

  getRepositories(
    registryName: string,
    projectNameOrK8sNamespace?: string,
    isThird?: boolean,
  ): Promise<ImageRepository[]>;
  getRepositories(options: {
    registry_name: string;
    project_name?: string;
    k8s_namespace?: string;
    page?: number;
    page_size?: number;
    search?: string;
    is_third?: boolean;
  }): Promise<Pagination<ImageRepository>>;
  getRepositories(
    registryNameOrOptions:
      | string
      | {
          registry_name: string;
          project_name?: string;
          k8s_namespace?: string;
          page?: number;
          page_size?: number;
          search?: string;
          is_third?: boolean;
        },
    projectNameOrK8sNamespace?: string,
    is_third?: boolean,
  ) {
    let registry_name;
    let params;

    if (typeof registryNameOrOptions === 'string') {
      registry_name = registryNameOrOptions;
      params = {
        is_third,
      };
    } else {
      registry_name = registryNameOrOptions.registry_name;
      is_third = registryNameOrOptions.is_third || is_third;
      projectNameOrK8sNamespace =
        (is_third
          ? registryNameOrOptions.k8s_namespace ||
            registryNameOrOptions.project_name
          : registryNameOrOptions.project_name) || projectNameOrK8sNamespace;

      params = {
        page: registryNameOrOptions.page,
        page_size: registryNameOrOptions.page_size || 20,
        search: registryNameOrOptions.search,
        is_third,
      };
    }

    return this.httpService
      .request({
        url: this.getUrlPrefix(registry_name, projectNameOrK8sNamespace),
        method: 'GET',
        addNamespace: false,
        params,
      })
      .then((res: any) => res.result || res);
  }

  getRepository(
    registry_name: string,
    repository_name: string,
    project_name?: string,
  ): Promise<ImageRepository> {
    return this.httpService.request({
      url:
        this.getUrlPrefix(registry_name, project_name) + '/' + repository_name,
      method: 'GET',
      addNamespace: false,
    });
  }

  deleteRepository(
    registry_name: string,
    repository_name: string,
    project_name?: string,
  ): Promise<void> {
    return this.httpService.request({
      url:
        this.getUrlPrefix(registry_name, project_name) + '/' + repository_name,
      method: 'DELETE',
      addNamespace: false,
    });
  }

  addRepository(
    registry_name: string,
    repository: ImageRepository,
    project_name?: string,
  ): Promise<ImageRepository> {
    return this.httpService.request({
      url: this.getUrlPrefix(registry_name, project_name),
      method: 'POST',
      body: repository,
      addNamespace: false,
    });
  }

  updateRepository(
    registry_name: string,
    repository: ImageRepository,
    project_name?: string,
  ): Promise<void> {
    return this.httpService.request({
      url:
        this.getUrlPrefix(registry_name, project_name) + '/' + repository.name,
      method: 'PUT',
      body: {
        description: repository.description,
        full_description: repository.full_description,
        is_public: repository.is_public,
        scan_enabled: repository.scan_enabled,
      },
      addNamespace: false,
    });
  }

  getRepositoryTags(params: {
    registry_name: string;
    repository_name: string;
    project_name?: string;
    k8s_namespace?: string;
    page_size?: number;
    page?: number;
    is_third?: boolean;
  }): Promise<string[]>;
  getRepositoryTags(params: {
    registry_name: string;
    repository_name: string;
    project_name?: string;
    k8s_namespace?: string;
    view_type: 'security' | 'detail';
    page_size?: number;
    page?: number;
    is_third?: boolean;
  }): Promise<Pagination<ImageRepositoryTag>>;
  getRepositoryTags({
    registry_name,
    repository_name,
    project_name,
    k8s_namespace,
    view_type,
    page_size,
    page,
    is_third,
  }: {
    registry_name: string;
    repository_name: string;
    project_name?: string;
    k8s_namespace?: string;
    view_type?: 'security' | 'detail';
    page_size?: number;
    page?: number;
    is_third?: boolean;
  }): Promise<string[] | Pagination<ImageRepositoryTag>> {
    return this.httpService
      .request({
        url:
          this.getUrlPrefix(
            registry_name,
            is_third ? k8s_namespace || project_name : project_name,
          ) +
          '/' +
          repository_name +
          '/tags',
        method: 'GET',
        params: {
          view_type,
          page_size,
          page,
          is_third,
        },
        addNamespace: false,
      })
      .then((res: any) => res.result || res)
      .catch(() => []);
  }

  getRepositoryTagDetail({
    registry_name,
    repository_name,
    project_name,
    tag_name,
  }: {
    registry_name: string;
    repository_name: string;
    project_name?: string;
    tag_name: string;
  }): Promise<ImageRepositoryTagDetail> {
    return this.httpService
      .request({
        url:
          this.getUrlPrefix(registry_name, project_name) +
          `/${repository_name}/tags/${tag_name}`,
        method: 'GET',
        addNamespace: false,
      })
      .then(({ config }) => this.getFormatRepositoryTagDetail(config))
      .catch(() => null);
  }

  updateRepositoryTag({
    registry_name,
    repository_name,
    project_name,
    tag_name,
    tag_protected,
  }: {
    registry_name: string;
    repository_name: string;
    project_name?: string;
    tag_name: string;
    tag_protected: boolean;
  }): Promise<ImageRepositoryTag> {
    return this.httpService.request({
      url:
        this.getUrlPrefix(registry_name, project_name) +
        '/' +
        repository_name +
        '/tags/' +
        tag_name,
      method: 'PUT',
      body: {
        protected: tag_protected,
      },
      addNamespace: false,
    });
  }

  deleteRepositoryTag({
    registry_name,
    repository_name,
    project_name,
    tag_name,
  }: {
    registry_name: string;
    repository_name: string;
    project_name?: string;
    tag_name: string;
  }): Promise<void> {
    return this.httpService.request({
      url:
        this.getUrlPrefix(registry_name, project_name) +
        '/' +
        repository_name +
        '/tags/' +
        tag_name,
      method: 'DELETE',
      addNamespace: false,
    });
  }

  startRepositoryTagScan({
    registry_name,
    repository_name,
    project_name,
    tag_name,
  }: {
    registry_name: string;
    repository_name: string;
    project_name?: string;
    tag_name: string;
  }): Promise<void> {
    return this.httpService.request({
      url:
        this.getUrlPrefix(registry_name, project_name) +
        '/' +
        repository_name +
        '/tags/' +
        tag_name,
      method: 'POST',
      addNamespace: false,
    });
  }

  getRepositoryTagReport(params: {
    registry_name: string;
    repository_name: string;
    project_name?: string;
    tag_name: string;
    view_type: 'cve' | 'package' | 'pipeline';
    action_type: 'summary';
  }): Promise<ImageRepositoryTagReportSummary>;
  getRepositoryTagReport(params: {
    registry_name: string;
    repository_name: string;
    project_name?: string;
    tag_name: string;
    view_type: 'cve' | 'package' | 'pipeline';
    action_type: 'detail';
    page_size?: number;
    page?: number;
  }): Promise<Pagination<ImageRepositoryTagReportDetail>>;
  getRepositoryTagReport({
    registry_name,
    repository_name,
    project_name,
    tag_name,
    view_type,
    action_type,
    page_size,
    page,
  }: {
    registry_name: string;
    repository_name: string;
    project_name?: string;
    tag_name: string;
    view_type: 'cve' | 'package' | 'pipeline';
    action_type: 'summary' | 'detail';
    page_size?: number;
    page?: number;
  }): Promise<
    ImageRepositoryTagReportSummary | Pagination<ImageRepositoryTagReportDetail>
  > {
    return this.httpService.request({
      url:
        this.getUrlPrefix(registry_name, project_name) +
        '/' +
        repository_name +
        '/tags/' +
        tag_name +
        '/' +
        action_type,
      method: 'GET',
      addNamespace: false,
      params: {
        view_type,
        page_size,
        page,
      },
    });
  }

  // public repository

  getLegacyPublicRepositories(namespace: string) {
    return this.httpService
      .request({
        url: `/ajax/repositories/${namespace}`,
        method: 'GET',
        addNamespace: false,
      })
      .then(({ results }) => results)
      .catch(
        (): any[] => {
          return [];
        },
      );
  }

  getPublicRepositoryTags(image_name: string) {
    const [index, ...repo_names] = image_name.split('/');
    const repo_path = repo_names.join('/');
    if (!index || !repo_path) {
      return Promise.resolve([]);
    } else {
      return this.httpService
        .request({
          url: '/ajax/public_repository/tag/list',
          params: {
            index,
            repo_path,
          },
          method: 'GET',
          addNamespace: false,
        })
        .then(({ tags }: { tags: any }) => {
          return tags.map((item: any) => {
            return item.tag;
          });
        })
        .catch(() => []);
    }
  }

  getPublicRepositoryTagDetail({
    image_name,
    tag_name,
  }: {
    image_name: string;
    tag_name: string;
  }) {
    const [index, ...repo_names] = image_name.split('/');
    const repo_path = repo_names.join('/');
    if (!index || !repo_path) {
      return Promise.resolve(null);
    } else {
      return this.httpService
        .request({
          url: '/ajax/public_repository/tag',
          params: { index, repo_path, tag: tag_name },
          method: 'GET',
          addNamespace: false,
        })
        .then(({ tag: { config } }) =>
          this.getFormatRepositoryTagDetail(config),
        )
        .catch(() => null);
    }
  }

  private getFormatRepositoryTagDetail(
    tag_config: any,
  ): ImageRepositoryTagDetail {
    const config: ImageRepositoryTagDetail = {};
    if (tag_config.Cmd) {
      const run_commands: string[] = [];
      tag_config.Cmd.forEach((item: string) => {
        let cmd = item;
        if (cmd && cmd.split(' ').length > 1) {
          cmd = "'" + cmd + "'";
        }
        run_commands.push(cmd);
      });
      config.run_command = run_commands.join(' ');
    }
    if (tag_config.Entrypoint) {
      config.entrypoint = tag_config.Entrypoint.join(' ');
    }
    if (tag_config.ExposedPorts) {
      const ports = Object.keys(tag_config.ExposedPorts);
      config.instance_ports = ports.map((port: string) => {
        return parseInt(port, 10);
      });
    }
    if (tag_config.Volumes) {
      config.volumes = Object.keys(tag_config.Volumes);
    }
    config.image_envvars = [];
    if (tag_config.Env) {
      tag_config.Env.forEach((e: string) => {
        const arr = e.split('=');
        if (2 === arr.length) {
          config.image_envvars.push({
            name: arr[0],
            value: arr[1],
          });
        }
      });
    }
    return config;
  }

  getImageRepositoryTagArtifacts({
    registry_name,
    repository_name,
    project_name,
    tag_name,
  }: {
    registry_name: string;
    repository_name: string;
    project_name?: string;
    tag_name: string;
  }) {
    return this.httpService.request<ImageRepositoryTagArtifacts>({
      url:
        this.getUrlPrefix(registry_name, project_name) +
        '/' +
        repository_name +
        '/tags/' +
        tag_name +
        '/artifacts',
      method: 'GET',
      addNamespace: false,
    });
  }
}
