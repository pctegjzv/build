import { Inject, Injectable } from '@angular/core';

import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { ImageRepository } from 'app2/shared/services/features/image-repository.service';

@Injectable()
export class ImageRepositoryUtilitiesService {
  constructor(@Inject(ENVIRONMENTS) private environments: Environments) {}
  getImageAddress(repository: ImageRepository & { repo_path?: string }) {
    if (repository.registry) {
      const registry = repository.registry;
      const project = repository.project;
      if (registry.is_public) {
        return `${repository.registry.endpoint}/${repository.namespace}/${
          repository.name
        }`;
      } else {
        if (project) {
          return `${repository.registry.endpoint}/${project.project_name}/${
            repository.name
          }`;
        } else {
          return `${repository.registry.endpoint}/${repository.name}`;
        }
      }
    } else {
      return `${this.environments.alauda_image_index}/${repository.repo_path}`;
    }
  }
}
