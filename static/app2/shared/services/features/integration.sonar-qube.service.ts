import { Inject, Injectable } from '@angular/core';
import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { HttpService } from 'app2/shared/services/http/http.service';

@Injectable()
export class SonarQubeService {
  private BASE_URL = `/ajax/jenkins_pipelines_integrations/${
    this.account.namespace
  }/sonar_integration`;

  constructor(
    @Inject(ACCOUNT) private account: RcAccount,
    private http: HttpService,
  ) {}

  getQualityGates(integrationId: string) {
    return this.http
      .request({
        url: `${this.BASE_URL}/${integrationId}/qualitygates`,
        method: 'GET',
      })
      .then((response: any) =>
        (response.qualitygates || []).map((item: any) => ({
          name: item.name,
          id: `${item.id}`,
        })),
      );
  }

  getLanguages(integrationId: string) {
    return this.http
      .request({
        url: `${this.BASE_URL}/${integrationId}/languages`,
        method: 'GET',
      })
      .then((response: any) => response.languages);
  }
}
