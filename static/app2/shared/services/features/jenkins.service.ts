import { Inject, Injectable } from '@angular/core';
import { omitBy } from 'lodash';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { HttpService } from 'app2/shared/services/http/http.service';

class JenkinsPipelines {
  opt = { addNamespace: false };
  constructor(private http: HttpService, private baseUrl: string) {}

  get(id: string): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/pipelines/${id}`,
      method: 'GET',
      ...this.opt,
    });
  }

  find(params: any): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/pipelines`,
      method: 'GET',
      params,
      ...this.opt,
    });
  }

  create(data: any) {
    return this.http.request({
      url: `${this.baseUrl}/pipelines`,
      method: 'POST',
      body: data,
    });
  }

  update(id: string, data: any) {
    return this.http.request({
      url: `${this.baseUrl}/pipelines/${id}`,
      method: 'PUT',
      body: data,
    });
  }

  delete(id: string, force: boolean = false) {
    return this.http.request({
      url: `${this.baseUrl}/pipelines/${id}`,
      method: 'DELETE',
      params: { force: force },
    });
  }
}

class JenkinsHistories {
  opt = { addNamespace: false };

  constructor(private http: HttpService, private baseUrl: string) {}

  get(id: string, params: { pipeline_uuid: string }) {
    return this.http.request({
      url: `${this.baseUrl}/history/${id}`,
      method: 'GET',
      params: { ...params, ...this.opt },
      ...this.opt,
    });
  }

  find(params: {
    search?: string;
    pipeline_uuid: string;
    jenkins_integration_id: string;
    page: number;
    page_size: number;
  }): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/history`,
      method: 'GET',
      params: omitBy(params, value => !value),
      ...this.opt,
    });
  }

  start(pipeline_uuid: string, history_id?: string) {
    const body = history_id
      ? { pipeline_uuid, history_id, mode: 'replay' }
      : { pipeline_uuid };

    return this.http.request({
      url: `${this.baseUrl}/history`,
      method: 'POST',
      body,
      ...this.opt,
    });
  }

  cancel(pipeline_uuid: string, history_id: string) {
    return this.http.request({
      url: `${this.baseUrl}/history/${history_id}/cancel`,
      method: 'PUT',
      params: { pipeline_uuid },
      ...this.opt,
    });
  }

  delete(pipeline_uuid: string, history_id: string) {
    return this.http.request({
      url: `${this.baseUrl}/history/${history_id}`,
      method: 'DELETE',
      params: { pipeline_uuid },
      ...this.opt,
    });
  }
}
class JenkinsStages {
  opt = { addNamespace: false };

  constructor(private http: HttpService, private baseUrl: string) {}

  find(params: { history_id: string; pipeline_uuid: string }): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/history/${params.history_id}/stages`,
      method: 'GET',
      params: { pipeline_uuid: params.pipeline_uuid },
      ...this.opt,
    });
  }
}

class JenkinsSteps {
  opt = { addNamespace: false };

  constructor(private http: HttpService, private baseUrl: string) {}

  find(params: {
    history_id: string;
    pipeline_uuid: string;
    stage_id: string;
  }): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/history/${params.history_id}/stages/${
        params.stage_id
      }/steps`,
      method: 'GET',
      params: { pipeline_uuid: params.pipeline_uuid },
      ...this.opt,
    });
  }

  abort(params: {
    history_id: string;
    pipeline_uuid: string;
    stage_id: string;
    step_id: string;
    input_id: string;
  }): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/history/${params.history_id}/stages/${
        params.stage_id
      }/steps/${params.step_id}/abort`,
      method: 'PUT',
      params: { pipeline_uuid: params.pipeline_uuid },
      body: {
        input_id: params.input_id,
      },
      ...this.opt,
    });
  }

  proceed(params: {
    history_id: string;
    pipeline_uuid: string;
    stage_id: string;
    step_id: string;
    input_id: string;
  }): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/history/${params.history_id}/stages/${
        params.stage_id
      }/steps/${params.step_id}/proceed`,
      method: 'PUT',
      params: { pipeline_uuid: params.pipeline_uuid },
      body: {
        input_id: params.input_id,
      },
      ...this.opt,
    });
  }
}

class JenkinsLogs {
  opt = { addNamespace: false };

  constructor(private http: HttpService, private baseUrl: string) {}

  find(params: {
    history_id: string;
    pipeline_uuid: string;
    start: number;
  }): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/history/${params.history_id}/logs`,
      method: 'GET',
      params: { pipeline_uuid: params.pipeline_uuid, start: params.start },
      ...this.opt,
    });
  }
}

class JenkinsStepLogs {
  opt = { addNamespace: false };

  constructor(private http: HttpService, private baseUrl: string) {}

  find(params: {
    history_id: string;
    pipeline_uuid: string;
    stage_id: string;
    step_id: string;
    start: number;
  }): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/history/${params.history_id}/stages/${
        params.stage_id
      }/steps/${params.step_id}/logs`,
      method: 'GET',
      params: { pipeline_uuid: params.pipeline_uuid, start: params.start },
      ...this.opt,
    });
  }
}

class JenkinsTestResult {
  opt = { addNamespace: false };

  constructor(private http: HttpService, private baseUrl: string) {}

  get(params: { history_id: string; pipeline_uuid: string; status?: string }) {
    return this.http.request({
      url: `${this.baseUrl}/history/${params.history_id}/testresult`,
      method: 'GET',
      params: { ...params, ...this.opt },
      ...this.opt,
    });
  }
}

class JenkinsCredentials {
  opt = { addNamespace: false };

  constructor(private http: HttpService, private baseUrl: string) {}

  get(id: string, jenkins_integration_id: string) {
    return this.http.request({
      url: `${this.baseUrl}/credentials/${id}`,
      method: 'GET',
      params: { jenkins_integration_id },
    });
  }

  create(data: any) {
    return this.http.request({
      url: `${this.baseUrl}/credentials`,
      method: 'POST',
      body: data,
    });
  }

  find({
    jenkins_integration_id,
  }: {
    jenkins_integration_id: string;
  }): Promise<any[]> {
    return this.http
      .request({
        url: `${this.baseUrl}/credentials`,
        method: 'GET',
        params: { jenkins_integration_id },
      })
      .then((response: any) => response.result);
  }
}

class JenkinsTemplates {
  constructor(private http: HttpService, private baseUrl: string) {}

  find(params: any): Promise<any[]> {
    return this.http
      .request({
        url: `${this.baseUrl}/templates`,
        method: 'GET',
        params: params,
      })
      .then(({ results }: any) => results);
  }

  get(uuid: string): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/templates/${uuid}`,
      method: 'GET',
    });
  }

  create(): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/template`,
      method: 'POST',
    });
  }

  update(): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/template`,
      method: 'POST',
    });
  }

  vars(data: any): Promise<any[]> {
    return this.http
      .request({
        url: `${this.baseUrl}/global_vars`,
        method: 'POST',
        body: data,
      })
      .then(({ result }: any) => result);
  }

  sources(params: any): Promise<any[]> {
    return this.http
      .request({
        url: `${this.baseUrl}/template_sources`,
        method: 'GET',
        params: params,
      })
      .then(({ result }) => result);
  }

  syncSettingsCreate(data: any): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/template_sources`,
      method: 'POST',
      body: data,
      addNamespace: false,
    });
  }
  syncSettingsUpdate(data: any): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/template_sources/${data.template_source_uuid}`,
      method: 'PUT',
      body: data,
    });
  }

  syncInfo(template_source_uuid: string): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/template_sources/${template_source_uuid}`,
      method: 'GET',
    });
  }

  refresh(template_source_uuid: string): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/template_sources/${template_source_uuid}/refresh`,
      method: 'PUT',
    });
  }

  previewJenkinsfile(
    template_uuid: string,
    data: any,
  ): Promise<{ jenkinsfile: string }> {
    return this.http.request({
      url: `${this.baseUrl}/templates/${template_uuid}/preview_jenkinsfile`,
      method: 'POST',
      body: data,
    });
  }
}

@Injectable()
export class JenkinsService {
  private BASE_URL = `/ajax/jenkins_pipelines/${this.account.namespace}`;

  pipelines = new JenkinsPipelines(this.http, this.BASE_URL);
  histories = new JenkinsHistories(this.http, this.BASE_URL);
  stages = new JenkinsStages(this.http, this.BASE_URL);
  logs = new JenkinsLogs(this.http, this.BASE_URL);
  steps = new JenkinsSteps(this.http, this.BASE_URL);
  stepLogs = new JenkinsStepLogs(this.http, this.BASE_URL);
  testResult = new JenkinsTestResult(this.http, this.BASE_URL);
  credentials = new JenkinsCredentials(this.http, this.BASE_URL);
  templates = new JenkinsTemplates(this.http, this.BASE_URL);

  constructor(
    private http: HttpService,
    @Inject(ACCOUNT) private account: RcAccount,
  ) {}
}
