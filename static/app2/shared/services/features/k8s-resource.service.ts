import { Injectable } from '@angular/core';
import { keyBy, mapValues } from 'lodash';

import { NotificationService } from 'alauda-ui';
import { RegionService } from 'app2/shared/services/features/region.service';
import { HttpService } from 'app2/shared/services/http/http.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { getCookie } from 'app2/utils/cookie';
import { BatchReponse, ResourceList } from 'app_user/core/types';

interface Cluster {
  id: string;
  name: string;
  display_name: string;
}

export interface K8sReource<T> {
  apiVersion: string;
  kind: string;
  metadata: {
    name: string;
    namespace: string;
  };
  spec?: T;
}

export const K8sServiceSpecTypeMap = {
  ClusterIP: 'HTTP',
  NodePort: 'TCP',
};
export interface K8sService {
  kubernetes?: {
    apiVersion: string;
    kind: string | 'Service';
    metadata: {
      name: string;
      uid?: string;
      labels?: {};
      namespace?: string;
      resourceVersion?: string;
    };
    spec: {
      type?: string;
      clusterIP?: string;
      selector?: {
        [key: string]: string;
      };
      ports: {
        name: string;
        protocol: string;
        port: number | '';
        targetPort: number | '';
        nodePort?: number;
      }[];
    };
  };
  resource_actions?: string[];
}

export interface Route {
  kubernetes?: {
    apiVersion: string;
    kind: string | 'Route';
    metadata: {
      name: string;
      uid?: string;
      namespace?: string;
      resourceVersion?: string;
    };
    spec: {
      host: string;
      path: string;
      port: {
        targetPort: string;
      };
      to: {
        kind: string | 'Service';
        name: string;
        weight: number | 100;
      };
      wildcardPolicy: string | 'None';
    };
  };
  resource_actions?: string[];
}

@Injectable()
export class K8sResourceService {
  CLUSTERS_ENDPOINT = '/ajax/v2/kubernetes/clusters/';

  constructor(
    private httpService: HttpService,
    private regionService: RegionService,
    private notificationService: NotificationService,
    private errorsToastService: ErrorsToastService,
    private translateService: TranslateService,
  ) {}

  reourceBatchNotification(translate: string, res: BatchReponse, name: string) {
    Object.entries(res.responses).forEach(([key, data]) => {
      if (data.code >= 300) {
        const body = JSON.parse(data.body);
        const error = this.errorsToastService.parseErrors({
          errors: body.errors,
        });
        const title = this.translateService.get(`${translate}_error`, {
          name: name,
          clusterName: key,
        });
        this.notificationService.error({
          content: `${title}:<br>${error.content}`,
        });
      } else {
        this.notificationService.success({
          content: this.translateService.get(`${translate}_success`, {
            name: name,
            clusterName: key,
          }),
        });
      }
    });
  }

  async getMirrorCluster(clusterName: string): Promise<Cluster[]> {
    let mirrorCluster: Cluster[] = [];
    const clusters = await this.regionService.getCluster(clusterName);
    if (clusters.mirror && clusters.mirror.regions) {
      mirrorCluster = clusters.mirror.regions;
    }
    return mirrorCluster;
  }

  async getMirrorResources(
    clusters: string[],
    namespace: string,
    resourceType: string,
    resourceName: string,
  ): Promise<BatchReponse | any> {
    const project_name = getCookie('project');
    const requests = clusters.map((cluster: string) => {
      return {
        cluster,
        url: `/v2/kubernetes/clusters/${cluster}/${resourceType}/${namespace}/${resourceName}?project_name=${project_name}`,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      addNamespace: false,
      body: {
        common: {
          method: 'GET',
          header: {
            'Content-Type': 'application/json',
          },
        },
        requests: mapValues(
          keyBy(requests, (item: any) => {
            return item.cluster;
          }),
          (item: any) => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }

  getK8sReourceList(
    resourceType: string,
    parmas: {
      clusterName: string;
      namespace?: string;
      page?: number;
      page_size?: number;
      name?: string;
      labelSelector?: string;
    },
  ): Promise<ResourceList | any> {
    return this.httpService
      .request(
        this.CLUSTERS_ENDPOINT +
          parmas.clusterName +
          `/${resourceType}/` +
          (parmas.namespace ? parmas.namespace + '/' : ''),
        {
          method: 'GET',
          params: {
            name: parmas.name,
            page: parmas.page,
            page_size: parmas.page_size,
            labelSelector: parmas.labelSelector,
          },
          addNamespace: false,
        },
      )
      .then((res: any) => (res.results ? res : res.result));
  }

  getK8sReourceDetail(
    clusterName: string,
    resourceType: string,
    namespace: string,
    name: string,
  ): Promise<any> {
    return this.httpService.request(
      `${
        this.CLUSTERS_ENDPOINT
      }${clusterName}/${resourceType}/${namespace}/${name}`,
      {
        method: 'GET',
        addNamespace: false,
      },
    );
  }

  createK8sReource(
    clusterName: string,
    resourceType: string,
    payload: any,
  ): Promise<any> {
    return this.httpService.request(
      `${this.CLUSTERS_ENDPOINT}${clusterName}/${resourceType}/`,
      {
        method: 'POST',
        body: payload,
        addNamespace: false,
      },
    );
  }

  updateK8sReource(
    clusterName: string,
    resourceType: string,
    payload: any,
  ): Promise<any> {
    return this.httpService.request({
      method: 'PUT',
      url:
        this.CLUSTERS_ENDPOINT +
        clusterName +
        `/${resourceType}/` +
        payload.metadata.namespace +
        '/' +
        payload.metadata.name,
      body: payload,
      addNamespace: false,
    });
  }

  deleteK8sReource(
    clusterName: string,
    resourceType: string,
    payload: any,
  ): Promise<any> {
    return this.httpService.request(
      this.CLUSTERS_ENDPOINT +
        clusterName +
        `/${resourceType}/` +
        payload.metadata.namespace +
        '/' +
        payload.metadata.name,
      {
        method: 'DELETE',
      },
    );
  }

  getK8sTopology(params: {
    cluster: string;
    namespace: string;
    kind?: string;
    name?: string;
  }): Promise<any> {
    let url = `${this.CLUSTERS_ENDPOINT}${params.cluster}/topology/${
      params.namespace
    }`;
    if (params.kind) {
      url = `${url}?kind=${params.kind}&name=${params.name}`;
    }
    return this.httpService.request(url, {
      method: 'GET',
    });
  }

  createK8sReources(
    clusterName: string,
    clusters: string[],
    payload: K8sReource<any>[],
  ) {
    if (clusters && clusters.length) {
      return this.createK8sReourcesBatch(clusters, payload);
    }
    return this.httpService.request(
      `${this.CLUSTERS_ENDPOINT}${clusterName}/resources`,
      {
        method: 'POST',
        body: payload,
        addNamespace: false,
      },
    );
  }

  createK8sReourcesBatch(clusters: string[], payload: K8sReource<any>[]) {
    const project_name = getCookie('project');
    const requests = clusters.map((cluster: string) => {
      return {
        cluster,
        url: `/v2/kubernetes/clusters/${cluster}/resources?project_name=${project_name}`,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      addNamespace: false,
      body: {
        common: {
          method: 'POST',
          header: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(payload),
        },
        requests: mapValues(
          keyBy(requests, (item: any) => {
            return item.cluster;
          }),
          (item: any) => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }
}
