import { Inject, Injectable } from '@angular/core';
import { ACCOUNT } from 'app2/core/tokens';

import { RcAccount, RcRegisterInfo } from 'app2/core/types';
import { LicenseInfoModal } from 'app2/features/license/license.type';

import { HttpService } from '../http/http.service';

@Injectable()
export class LicenseService {
  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) private account: RcAccount,
  ) {}

  getLicenseInfo(): Promise<LicenseInfoModal> {
    return this.httpService.request(
      `/ajax/license-auth/${this.account.namespace}/license-code`,
      {
        method: 'GET',
      },
    );
  }

  getRegisterInfo(): Promise<RcRegisterInfo> {
    return this.httpService.request<RcRegisterInfo>(
      `/ajax/license-auth/${this.account.namespace}/registry-code`,
      {
        method: 'GET',
      },
    );
  }
}
