import { Inject, Injectable } from '@angular/core';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { SourceStatsCount } from 'app2/shared/services/features/model.types';
import { HttpService } from 'app2/shared/services/http/http.service';

@Injectable()
export class LogAlarmService {
  LOG_ALARM_V2_URL: string;

  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) account: RcAccount,
  ) {
    const namespace = account.namespace;
    this.LOG_ALARM_V2_URL = `/ajax/v2/log_alarms/${namespace}/`;
  }

  getLogAlarmsCount(): Promise<SourceStatsCount> {
    return this.httpService.request(this.LOG_ALARM_V2_URL, {
      method: 'GET',
      params: {
        display_count: 1,
      },
    });
  }
}
