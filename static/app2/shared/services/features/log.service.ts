import { Inject, Injectable } from '@angular/core';
import * as _ from 'lodash';
import moment from 'moment';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { TIME_STAMP_OPTIONS } from 'app2/features/log/log-constant';
import { HttpService } from 'app2/shared/services/http/http.service';
import { TranslateService } from 'app2/translate/translate.service';

export interface SearchParam {
  end_time: number;
  start_time: number;
  pageno: number;
  size: number;
  paths: string;
  read_log_source_uuid: string;
  clusters?: string;
  instances?: string;
  nodes?: string;
  query_string?: string;
  services?: string;
  allow_service_id?: boolean;
  mode?: string;
}

export interface Logs {
  logs: {
    cluster_name: string;
    instance_id: string;
    instance_id_full: string;
    message: string;
    nodes: string;
    paths: string;
    service_name: string;
    time: number;
  }[];
  total_items: number;
  total_page: number;
}

export interface Types {
  clusters: string[];
  services: string[];
  nodes: string[];
  paths: string[];
}

export interface AggregationsQueryParams {
  start_time: number;
  end_time: number;
  clusters?: string;
  instances?: string;
  nodes?: string;
  paths?: string;
  query_string?: string;
  services?: string;
}

export interface Aggregations {
  buckets: {
    count: number;
    time: number;
  }[];
}

export interface SavedSearch {
  created_at: number;
  created_by: string;
  display_name: string;
  end_time: number;
  name: string;
  project_name: string;
  project_uuid: string;
  query_conditions: {
    query_string: Array<string>;
  };
  resource_actions: Array<string>;
  space_name: string;
  space_uuid: string;
  start_time: number;
  updated_at: number;
  used_at: number;
  uuid: string;
}

export interface SavedSearchs {
  result: SavedSearch[];
}

export interface LogQueryCondition {
  display_name: string;
  end_time: number;
  name: string;
  namespace: string;
  query_conditions: {
    cluster: string;
    nodes: string;
    services: string;
    paths: string;
    query_string: string;
    instances: string;
  };
  space_name: string;
  start_time: number;
  uuid?: string;
}

export interface QueryConditionResult {
  num_pages?: number;
  page_size?: number;
  count?: number;
  result?: Array<QueryCondition>;
  results?: Array<QueryCondition>;
}

export interface QueryCondition {
  created_at: number;
  created_by: string;
  display_name: string;
  end_time: number;
  name: string;
  project_name: string;
  project_uuid: string;
  query_conditions: QueryConditionDetail;
  resource_actions: Array<string>;
  space_name: string;
  space_uuid: string;
  start_time: number;
  updated_at: number;
  used_at: number;
  uuid: string;
}

export interface QueryConditionService {
  app_name: string;
  kube_namespace: string;
  name: string;
  region_name: string;
  space_name: string;
  uuid: string;
}

export interface QueryConditionDetail {
  cluster?: Array<string>;
  nodes?: Array<string>;
  services?: Array<string | QueryConditionService>;
  paths?: Array<string>;
  query_string?: Array<string>;
  instances?: Array<string>;
}

export interface QueryConditions {
  results: QueryCondition[];
  count: number;
  num_pages: number;
  page_size: number;
}

export interface RangeDetail {
  tags: any[];
  timeRange: {
    start_time: number;
    end_time: number;
  };
  timeRangeType: string;
}

export interface Conditions {
  search: string;
  query_string: string;
}

export interface LogChartParam {
  totalCount: number;
  buckets: any[];
}

@Injectable()
export class LogService {
  LOG_V1_URL: string;
  LOG_V2_URL: string;

  constructor(
    private httpService: HttpService,
    private translateService: TranslateService,
    @Inject(ACCOUNT) account: RcAccount,
  ) {
    const namespace = account.namespace;
    this.LOG_V1_URL = `/ajax/v1/logs/${namespace}/`;
    this.LOG_V2_URL = `/ajax/v2/logs/${namespace}/`;
  }

  logsSearch(params: SearchParam): Promise<Logs> {
    return this.httpService.request(this.LOG_V2_URL + 'search', {
      method: 'GET',
      params,
    });
  }

  getTypes(logSource: string = 'default'): Promise<Types> {
    return this.httpService.request(this.LOG_V2_URL + 'types', {
      method: 'GET',
      params: {
        read_log_source_uuid: logSource,
      },
    });
  }

  getAggregations(params: AggregationsQueryParams): Promise<Aggregations> {
    return this.httpService.request(this.LOG_V2_URL + 'aggregations', {
      method: 'GET',
      params,
    });
  }

  getLogQueryConditions({
    page = 1,
    page_size = 20,
    all = false,
    display_name = '',
  }): Promise<any> {
    let params;
    if (all) {
      params = { all: true };
    } else {
      params = { page, page_size, display_name };
    }
    return this.httpService
      .request(this.LOG_V2_URL + 'saved_search', {
        method: 'GET',
        params,
      })
      .then((res: QueryConditionResult) => {
        if (res.result) {
          res.result = res.result.map(this.queryConditionMapper);
        }
        if (res.results) {
          res.results = res.results.map(this.queryConditionMapper);
        }
        return res;
      });
  }

  getLogQueryCondition(uuid: string): Promise<QueryCondition> {
    return this.httpService
      .request(this.LOG_V2_URL + 'saved_search/' + uuid, {
        method: 'GET',
      })
      .then((result: QueryCondition) => {
        result.query_conditions = this.queryConditionFilter(
          result.query_conditions,
        );
        return result;
      });
  }

  createLogQueryCondition(payload: LogQueryCondition): Promise<SavedSearch> {
    return this.httpService.request(this.LOG_V2_URL + 'saved_search', {
      method: 'POST',
      body: {
        ...payload,
      },
    });
  }

  updateLogQueryCondition(
    uuid: string,
    payload: LogQueryCondition,
  ): Promise<SavedSearch> {
    return this.httpService.request(this.LOG_V2_URL + 'saved_search/' + uuid, {
      method: 'PUT',
      body: {
        ...payload,
      },
    });
  }

  deleteLogQueryCondition(uuid: string) {
    return this.httpService.request(this.LOG_V2_URL + 'saved_search/' + uuid, {
      method: 'DELETE',
    });
  }

  queryConditionMapper = (result: QueryCondition) => {
    result.query_conditions = this.queryConditionFilter(
      result.query_conditions,
    );
    return result;
  };

  queryConditionFilter(queryCondition: QueryConditionDetail) {
    if (queryCondition.services) {
      queryCondition.services = queryCondition.services.filter(
        (service: QueryConditionService) => {
          return service.uuid;
        },
      );
      if (queryCondition.services.length === 0) {
        delete queryCondition.services;
      }
    }
    return queryCondition;
  }

  parseQueryConditionToTagsAndTimeRange(item: any) {
    const result: RangeDetail = {
      tags: [],
      timeRange: {
        start_time: 0,
        end_time: 0,
      },
      timeRangeType: '',
    };
    //$log.log(item);
    if (item.end_time === 0) {
      const offset = -item.start_time * 1000;
      const res = _.find(TIME_STAMP_OPTIONS, option => {
        return option.offset === offset;
      });
      result.timeRangeType = res.type;
      result.timeRange = this.getTimeRangeByRangeType(result.timeRangeType);
    } else {
      result.timeRangeType = 'custom_time_range';
      result.timeRange.start_time = item.start_time * 1000;
      result.timeRange.end_time = item.end_time * 1000;
    }
    const queryConditions = item.query_conditions;
    Object.keys(queryConditions).forEach(key => {
      let tags = [];
      if (key === 'query_string') {
        tags = queryConditions[key].map((value: any) => {
          return value;
        });
      } else if (key === 'services') {
        tags = queryConditions[key].map((value: any) => {
          // return `service: ${value.name}${value.uuid}`;
          return {
            type: 'service',
            name: value.name,
            uuid: value.uuid,
          };
        });
      } else {
        const _key = key.slice(0, -1);
        tags = queryConditions[key].map((value: any) => {
          // return `${_key}: ${value}`;
          return {
            type: _key,
            name: value,
          };
        });
      }
      result.tags = result.tags.concat(tags);
    });
    return result;
  }

  generateQueryConditionFromTagsAndTimeRange({
    tags = [],
    timeRangeType,
    timeRange,
  }: RangeDetail) {
    let conditions: any = this.generateQuerysFromTagObjects(tags);
    if (conditions === undefined) {
      conditions = {};
    }
    // replace 'search' keyword with 'query_string'
    if (conditions.search) {
      conditions.query_string = conditions.search;
      delete conditions.search;
    }
    const range = {
      start_time: 0,
      end_time: 0,
    };
    if (timeRangeType !== 'custom_time_range') {
      const item = _.find(TIME_STAMP_OPTIONS, _item => {
        return _item.type === timeRangeType;
      });
      range.start_time = -(item.offset / 1000);
      range.end_time = 0;
    } else {
      range.start_time = parseInt(timeRange.start_time / 1000 + '', 10);
      range.end_time = parseInt(timeRange.end_time / 1000 + '', 10);
    }
    return [conditions, range];
  }

  generateQuerysFromTagObjects(tags: any) {
    const querys = _.chain(tags)
      .map((tag: any) => {
        // from[{type:service,name:'a',uuid:'x'}, {type:node,name:'b'}, 'search string'] to [{services:a},{nodes:b},{search:c}]
        const param = {};
        if (typeof tag === 'string') {
          const regex = /(\w+): (.+)/;
          if (regex.test(tag)) {
            const regArr = regex.exec(tag);
            param[regArr[1] + 's'] = regArr[2];
          } else {
            param['search'] = tag;
          }
        } else if (tag.uuid) {
          param[tag.type + 's'] = tag.uuid;
        } else {
          param[tag.type + 's'] = tag.name;
        }
        return param;
      })
      .reduce((result, param) =>
        _.mergeWith(result, param, (pre, cur) => {
          // from [{services:a},{services:b},{clusters:c},{clusters:d},{search:e}] to {services:'a,b',clusters:'c,d',search:'e'}
          if (pre) {
            return [pre, cur].join(',');
          } else {
            return cur;
          }
        }),
      )
      .value();
    return querys;
  }

  generateQuerysFromTags(tags: any) {
    const querys = _.chain(tags)
      .map((tag: any) => {
        // from['service: a', 'node: b', 'path: c', 'cluster: d'] to [{services:a},{nodes:b},{paths:c},{clusters:d}]
        const param = {};
        const match = tag.match(/^(.+):\s(.+)$/);
        if (match && match[0]) {
          const value = (match[2] || '').trim();
          if (match[1] === 'search') {
            param[match[1]] = value;
          } else {
            param[match[1] + 's'] = value;
          }
        }
        return param;
      })
      .reduce((result, param) =>
        _.mergeWith(result, param, (pre, cur) => {
          // from [{services:a},{services:b},{clusters:c},{clusters:d},{search:e}] to {services:'a,b',clusters:'c,d',search:'e'}
          if (pre) {
            return [pre, cur].join(',');
          } else {
            return cur;
          }
        }),
      )
      .value();
    return querys;
  }

  getQueryConditionValuesDisplay(item: any) {
    if (!item) {
      return '-';
    }
    const conditions = item.query_conditions;
    if (!conditions || !Object.keys(conditions).length) {
      return '-';
    }
    return Object.keys(conditions)
      .map(key => {
        if (key === 'query_string') {
          const _key = 'search';
          const tags = conditions[key].join(',');
          return `<div class="rb-query-condition-line" title="${_key}:${tags}"><b>${_key}:</b>${tags}</div>`;
        } else if (key === 'services') {
          const tags = conditions[key]
            .map((service: QueryConditionService) => {
              return service.name;
            })
            .join(',');
          return `<div class="rb-query-condition-line" title="service:${tags}"><b>service:</b>${tags}</div>`;
        } else {
          const _key = key.slice(0, -1);
          const tags = conditions[key].join(',');
          return `<div class="rb-query-condition-line" title="${_key}:${tags}"><b>${_key}:</b>${tags}</div>`;
        }
      })
      .join('');
  }

  getQueryConditionStringDisplay(selectedSearchCondition: any) {
    if (!selectedSearchCondition) {
      return '-';
    }
    const conditions = selectedSearchCondition.query_conditions;
    if (!conditions || !Object.keys(conditions).length) {
      return '-';
    }
    return Object.keys(conditions)
      .map(key => {
        if (key === 'query_string') {
          const _key = 'search';
          const tags = conditions[key].join(',');
          return `${_key}: ${tags}`;
        } else if (key === 'services') {
          const tags = conditions[key]
            .map((service: QueryConditionService) => {
              return service.name;
            })
            .join(',');
          return `service: ${tags}`;
        } else {
          const _key = key.slice(0, -1);
          const tags = conditions[key].join(',');
          return `${_key}: ${tags}`;
        }
      })
      .join('， ');
  }

  getQueryConditionTimeRangeDisplay(item: any, delimeter = '<br/>') {
    if (!item) {
      return '-';
    }
    if (item.end_time === 0) {
      const offset = -item.start_time * 1000;
      const res = _.find(TIME_STAMP_OPTIONS, option => {
        return option.offset === offset;
      });
      return this.translateService.get(res.type);
    } else {
      return `${this.translateService.get('from')} ${moment(
        item.start_time * 1000,
      ).format('YYYY-MM-DD HH:mm:ss')}${delimeter}
        ${this.translateService.get('to')} ${moment(
        item.end_time * 1000,
      ).format('YYYY-MM-DD HH:mm:ss')}`;
      return '';
    }
  }

  updateLogConditionUsedTime(uuid: string) {
    return this.getLogQueryCondition(uuid);
  }

  getTimeRangeByRangeType(type: any) {
    let start_time;
    const end_time = new Date().getTime();
    const offset =
      TIME_STAMP_OPTIONS.find(option => option.type === type).offset ||
      TIME_STAMP_OPTIONS[0]['offset'];
    if (offset >= 7 * 24 * 3600 * 1000) {
      const endDate = new Date(end_time);
      start_time =
        new Date(
          endDate.getFullYear(),
          endDate.getMonth(),
          endDate.getDate(),
        ).getTime() -
        offset +
        24 * 3600 * 1000;
    } else {
      start_time = end_time - offset;
    }
    return {
      start_time,
      end_time,
    };
  }

  dateNumToStr(num: number) {
    return moment(num).format('YYYY-MM-DD HH:mm:ss');
  }

  dateStrToNum(str: string) {
    return moment(str, 'YYYY-MM-DD HH:mm:ss').valueOf();
  }
}
