export interface SourceStatsCount {
  count: number;
  count_result: {
    status: string;
    custom_status?: string;
    count: number;
  }[];
}

export interface HistoryCountResult {
  OK: {
    custom_status?: string;
    count: number;
  };
  ERROR: {
    custom_status?: string;
    count: number;
  };
  OTHER: {
    custom_status?: string;
    count: number;
  };
  start: number;
  end: number;
}

export interface HistoryStatsCount {
  count: number;
  unit: string;
  count_result: HistoryCountResult[];
}

export interface TimeSection {
  start_time: number;
  end_time: number;
}

export interface Pagination<T> {
  count: number;
  page_size: number;
  num_pages: number;
  page: number;
  results: T[];
}
