import { Inject, Injectable } from '@angular/core';
import { keyBy, mapValues } from 'lodash';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { Pagination } from 'app2/shared/services/features/model.types';
import { HttpService } from 'app2/shared/services/http/http.service';
import { CookieService } from 'app2/shared/services/utility/cookie.service';
import { BatchApi } from 'app_user/core/types';

export interface Namespace {
  kubernetes: {
    apiVersion: string;
    kind: 'Namespace';
    metadata: {
      creationTimestamp: string;
      name: string;
      resourceVersion: string;
      selfLink: string;
      uid: string;
    };
    spec: {
      finalizers: string[];
    };
    status: {
      phase: 'Active' | 'Terminating';
    };
  };
  resource_actions: string[];
}

export interface QuotaStatusModel {
  pods: string;
  'requests.cpu': string;
  'requests.storage': string;
  persistentvolumeclaims: string;
  'requests.memory': string;
}

export interface ResourceQuota {
  resource_actions: string[];
  kubernetes: {
    status: {
      hard: QuotaStatusModel;
      used: QuotaStatusModel;
    };
    kind: 'ResourceQuota';
    spec: {
      hard: QuotaStatusModel;
    };
    apiVersion: string;
    metadata: {
      creationTimestamp: string;
      name: string;
      resourceVersion: string;
      selfLink: string;
      uid: string;
    };
  };
}

export interface NamespaceOption {
  name: string;
  uuid: string;
}

export interface UserNamespace {
  name: string;
  uuid?: string;
  cluster_name: string;
  cluster_uuid?: string;
  cluster_display_name?: string;
}

@Injectable()
export class NamespaceService {
  CLUSTERS_URL = '/ajax/v2/kubernetes/clusters/';
  private namespaceSubject: BehaviorSubject<UserNamespace>;
  public namespace$: Observable<UserNamespace>;

  constructor(
    private httpService: HttpService,
    private cookieService: CookieService,
    @Inject(ACCOUNT) private account: RcAccount,
  ) {
    let userNamespace = null;
    try {
      userNamespace = JSON.parse(this.getFromCookie());
    } catch (err) {
      // placeholder
    }
    this.namespaceSubject = new BehaviorSubject(userNamespace);
    this.namespace$ = this.namespaceSubject.asObservable();
  }

  get(): UserNamespace {
    return this.namespaceSubject.getValue();
  }

  set(namespace: UserNamespace) {
    this.cookieService.setCookie('namespace', JSON.stringify(namespace));
    this.namespaceSubject.next(namespace);
  }

  getCurrentUserNamespace(): Promise<UserNamespace> {
    return this.namespace$
      .pipe(
        filter(namespace => !!namespace),
        first(),
      )
      .toPromise();
  }

  private getFromCookie(): string {
    return this.cookieService.getCookie('namespace');
  }

  // data services

  getNamespaces(params: {
    clusterId: string;
    name?: string;
    ignoreProject?: boolean;
  }): Promise<Namespace[]>;
  getNamespaces(params: {
    clusterId: string;
    name?: string;
    page: number;
    page_size: number;
    ignoreProject?: boolean;
  }): Promise<Pagination<Namespace>>;
  getNamespaces({
    clusterId,
    name,
    page,
    page_size,
    ignoreProject,
  }: {
    clusterId: string;
    name?: string;
    page: number;
    page_size: number;
    ignoreProject?: boolean;
  }): Promise<Namespace[] | Pagination<Namespace>> {
    return this.httpService
      .request({
        method: 'GET',
        url: this.CLUSTERS_URL + clusterId + '/namespaces',
        addNamespace: false,
        params: {
          page,
          page_size,
          name,
        },
        ignoreProject: !!ignoreProject,
      })
      .then((res: any) => (res.results ? res : res.result));
  }

  getNamespaceOptions(
    clusterId: string,
    name?: string,
    ignoreProject?: boolean,
  ): Promise<NamespaceOption[]> {
    return this.getNamespaces({
      clusterId,
      name,
      ignoreProject: !!ignoreProject,
    })
      .then(results =>
        results.map(({ kubernetes: { metadata } }) => ({
          name: metadata.name,
          uuid: metadata.uid,
        })),
      )
      .catch(() => []);
  }

  getBatchNamespaceOptions(
    clusters: {
      name: string;
      uuid: string;
      display_name: string;
    }[],
    project_name: string,
  ): Promise<UserNamespace[]> {
    const requests = clusters.map(
      (cluster: { name: string; uuid: string; display_name: string }) => {
        return {
          clusterId: cluster.uuid,
          url: `/v2/kubernetes/clusters/${
            cluster.uuid
          }/namespaces?project_name=${project_name || ''}`,
        };
      },
    );
    return this.httpService
      .request({
        method: 'POST',
        url: '/ajax/v2/batch/',
        addNamespace: false,
        ignoreProject: true,
        body: {
          common: {
            method: 'GET',
          },
          // cluster_uuid : request object
          requests: mapValues(
            keyBy(requests, (item: any) => {
              return item.clusterId;
            }),
            (item: any) => {
              return {
                url: item.url,
              };
            },
          ),
        },
      })
      .then(
        (res: {
          responses: {
            [key: string]: {
              body: string;
              code: number;
            };
          };
        }) => {
          let namespaces: {
            name: string;
            uuid: string;
            cluster_name: string;
            cluster_uuid: string;
            cluster_display_name: string;
          }[] = [];
          Object.keys(res.responses).forEach((clusterId: string) => {
            const response: {
              body: string;
              code: number;
            } = res.responses[clusterId];
            if (response.code === 200) {
              const data = JSON.parse(response.body);
              namespaces = namespaces.concat(
                data.map((namespace: Namespace) => {
                  const cluster = clusters.find((c: { uuid: string }) => {
                    return c.uuid === clusterId;
                  });
                  return {
                    name: namespace.kubernetes.metadata.name,
                    uuid: namespace.kubernetes.metadata.uid,
                    cluster_name: cluster.name,
                    cluster_uuid: cluster.uuid,
                    cluster_display_name: cluster.display_name,
                  };
                }),
              );
            }
          });
          return namespaces;
        },
      )
      .catch(() => []);
  }

  getNamespace(clusterId: string, namespace: string) {
    return this.httpService.request<Namespace>({
      method: 'GET',
      url: this.CLUSTERS_URL + clusterId + '/namespaces/' + namespace,
      addNamespace: false,
    });
  }

  getNamespaceQuota(clusterId: string, namespace: string) {
    return this.httpService.request<ResourceQuota>({
      method: 'GET',
      url:
        this.CLUSTERS_URL +
        clusterId +
        '/resourcequotas/' +
        namespace +
        '/default',
      addNamespace: false,
    });
  }

  /**
   * 获取集群下所有namespace的resourcequotas
   */
  getClusterNamespacesQuotas(clusterId: string) {
    return this.httpService
      .request<{ result: ResourceQuota[] }>({
        method: 'GET',
        url: `${this.CLUSTERS_URL}${clusterId}/resourcequotas/`,
        addNamespace: false,
      })
      .then(({ result }) => result)
      .catch(() => []);
  }

  createNamespace(clusterId: string, namespace: string) {
    return this.httpService.request<Namespace>({
      method: 'POST',
      url: this.CLUSTERS_URL + clusterId + '/namespaces',
      addNamespace: false,
      body: {
        apiVersion: 'v1',
        kind: 'Namespace',
        metadata: {
          name: namespace,
        },
      },
    });
  }

  deleteNamespace(clusterId: string, namespace: string) {
    return this.httpService.request<Pagination<Namespace>>({
      method: 'DELETE',
      url: this.CLUSTERS_URL + clusterId + '/namespaces/' + namespace,
      addNamespace: false,
    });
  }

  deleteNamespaceBatch(
    data: {
      clusterName: string;
      namespace: string;
      projectName: string;
      clusterDisplayName: string;
    }[],
  ): Promise<BatchApi> {
    const requests = {};
    data.forEach(v => {
      requests[v.clusterDisplayName] = {
        url: `/v2/custom/cmb/clusters/${v.clusterName}/namespaces/${
          v.namespace
        }?project_name=${v.projectName}`,
      };
    });

    return this.httpService.request('/ajax/v2/batch/', {
      method: 'POST',
      body: {
        common: {
          method: 'DELETE',
          header: {
            'Content-Type': 'application/json',
          },
        },
        requests,
      },
    });
  }

  createNamespaceBatch(
    arr: {
      clusterName: string;
      clusterDisplayName: string;
      namespace: string;
      projectName: string;
      quotaConfig: any;
    }[],
  ): Promise<BatchApi> {
    const requests = {};
    arr.forEach(c => {
      requests[c.clusterDisplayName] = {
        url: `/v2/custom/cmb/clusters/${
          c.clusterName
        }/namespaces/?project_name=${c.projectName}`,
        body: JSON.stringify({
          namespace: {
            apiVersion: 'v1',
            kind: 'Namespace',
            metadata: {
              name: c.namespace,
            },
          },
          resourcequota: {
            apiVersion: 'v1',
            kind: 'ResourceQuota',
            metadata: {
              name: 'default',
              namespace: c.namespace,
            },
            spec: {
              hard: c.quotaConfig,
            },
          },
          account: this.account.namespace,
        }),
      };
    });

    return this.httpService.request('/ajax/v2/batch/', {
      method: 'POST',
      addNamespace: false,
      body: {
        common: {
          method: 'POST',
          header: {
            'Content-Type': 'application/json',
          },
        },
        requests,
      },
    });
  }
}
