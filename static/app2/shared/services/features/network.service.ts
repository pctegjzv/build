import { Inject, Injectable } from '@angular/core';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount, ResourceType } from 'app2/core/types';
import { HttpService } from 'app2/shared/services/http/http.service';
import { FetchDataResult } from 'app2/shared/services/utility/pagination-data';

import { RoleUtilitiesService } from './role-utilities.service';

export enum LB_TYPE {
  HAPROXY = 'haproxy',
  SLB = 'slb',
  ELB = 'elb',
  CLB = 'clb',
}

export interface LoadBalancer {
  address: string;
  address_type: string;
  blocked_ports: Array<string>;
  create_type: string;
  create_at: string;
  domain_info: any[];
  frontends: Array<object>;
  iass_id: string;
  load_balancer_id: string;
  name: string;
  namespace: string;
  version: number;
  type: LB_TYPE;
  update_at: string;
  region_id: string;
  region_name: string;
  listeners: Array<object>;
}

export interface NetworkPolicy {
  resource_actions: Array<any>;
  kubernetes: any;
}
export interface FrontendDetail {
  protocol: 'http' | 'https';
  app_name: string;
  certificate_id: string;
  space_id: string;
  container_port: number;
  load_balancer_id: string;
  space_name: string;
  service_name: string;
  service_namespace: string;
  service_id: string;
  service_count: number;
  certificate_name: string;
  port: number;
  rules: Rule[];
}

export interface Service {
  container_port: number;
  service_id: string;
  service_name: string;
  weight: number;
}

export interface Rule {
  description: string;
  domain: string;
  url: string;
  dsl: string;
  rule_id: string;
  services: Service[];
  session_affinity_attribute: string;
  session_affinity_policy: string;
  type: string;
}

export enum MATCH_TYPE {
  AND = 'AND',
  EXIST = 'Exist',
  EQ = 'Equal',
  IN = 'In',
  OR = 'OR',
  RANGE = 'Range',
  REGEX = 'RegEx',
  STARTS_WITH = 'StartsWith',
}

export const MATCH_TYPES = {
  Exist: 'EXIST',
  Equal: 'EQ',
  In: 'IN',
  Range: 'RANGE',
  RegEx: 'REGEX',
  StartsWith: 'STARTS_WITH',
};

export enum RULE_TYPE {
  HOST = 'HOST',
  URL = 'URL',
  SRC_IP = 'SRC_IP',
  HEADER = 'HEADER',
  COOKIE = 'COOKIE',
  PARAM = 'PARAM',
}

export interface RuleIndicator {
  type: RULE_TYPE;
  values: string[][];
  key: string;
}

export enum CERT_STATUS {
  NORMAL = 'Normal',
}
export interface AluCertificate {
  uuid: string;
  name: string;
  space_name: string;
  space_uuid: string;
  is_used: boolean;
  project_name: string;
  project_uuid: string;
  description: string;
  private_key: string;
  public_cert: string;
  statsu: CERT_STATUS;
  service_count: number;
  created_by: string;
  created_at: string;
  updated_at: string;
}

export interface SubnetRaw {
  region_name: string;
  subnet_name: string;
  cidr_block: string;
  gateway: string;
  space_name: string;
  is_default: boolean;
}

export interface Subnet extends SubnetRaw {
  subent_id: string;
  created_at: string;
  updated_at: string;
}

export interface K8sSubnet {
  resource_actions: string[];
  kubernetes: {
    kind: 'Subnet';
    apiVersion: string;
    spec: {
      project_name: string;
      namesapce: string;
      subnet_name: string;
      create_at: string;
      update_at: string;
      create_by: string;
      cidr_block: string;
      gateway: string;
    };
    metadata: {
      name: string;
      clusterName: string;
      namespace: string;
      uid: string;
      creationTimestamp: string;
      selfLink: string;
      resourceVersion: string;
    };
  };
}

export interface SubnetIp {
  address_id: string;
  subnet_name: string;
  subnet_id: string;
  address: string;
  used: boolean;
  service_id: string;
  bind: boolean;
  created_at: string;
  updated_at: string;
  [propName: string]: any;
}

export interface K8sSubnetIp {
  resource_actions: string[];
  kubernetes: {
    kind: 'IP';
    apiVersion: string;
    spec: {
      subnet_name: string;
      fresh: boolean;
      used: boolean;
      service_name: string;
      service_id: string;
    };
    metadata: {
      name: string;
      clusterName: string;
      labels: {
        subnet: string;
        used: string;
      };
      namespace: string;
      resourceVersion: string;
      selfLink: string;
      uid: string;
    };
  };
}

export interface K8sSubnetPayload {
  name: string;
  cidr: string;
  gateway: string;
}

export interface IpImportPayload {
  address_list?: string[];
  address_range?: {
    address_start: string;
    address_end: string;
  };
}

@Injectable()
export class NetworkService {
  private ENDPOINT = `/ajax/load_balancers/${this.account.namespace}`;
  private CERT_ENDPOINT = `/ajax/certificates/${this.account.namespace}`;
  private SUBNET_ENPOINT = `/ajax/subnets/${this.account.namespace}`;
  private K8S_SUNNET_ENDPOINT = `/ajax/v2/subnet/clusters`;
  private NETWORK_POLICY_ENPOINT = `/ajax/v2/kubernetes/clusters`;
  private opt = { addNamespace: false };

  constructor(
    private httpService: HttpService,
    private roleService: RoleUtilitiesService,
    @Inject(ACCOUNT) public account: RcAccount,
  ) {}

  private request(url: string, option: any) {
    return this.httpService.request(url, Object.assign(this.opt, option));
  }

  // lb api
  getList({
    region_name,
    detail = false,
    service_id = '',
    frontend = false,
  }: {
    region_name: string;
    detail?: boolean;
    service_id?: string;
    frontend?: boolean;
  }): Promise<LoadBalancer[]> {
    return this.request(this.ENDPOINT, {
      method: 'GET',
      params: {
        region_name,
        detail,
        service_id,
        frontend,
      },
    })
      .then(({ result }) => result)
      .catch(() => {
        return [];
      });
  }

  create(data: object) {
    return this.httpService.request(this.ENDPOINT, {
      method: 'POST',
      body: data,
    });
  }

  update(lb_name: string, data: object) {
    return this.httpService.request(`${this.ENDPOINT}/${lb_name}`, {
      method: 'PUT',
      body: data,
      addNamespace: false,
    });
  }

  delete(name: string) {
    return this.httpService.request(`${this.ENDPOINT}/${name}`, {
      method: 'DELETE',
    });
  }

  bind(name: string, data: object) {
    return this.httpService.request(`${this.ENDPOINT}/${name}`, {
      method: 'PUT',
      body: data,
    });
  }

  getDetail({
    uuid,
    service_id = '',
    frontend = false,
  }: {
    uuid: string;
    service_id?: string;
    frontend?: boolean;
  }): Promise<LoadBalancer> {
    return this.httpService.request(`${this.ENDPOINT}/${uuid}`, {
      method: 'GET',
      params: {
        service_id,
        frontend,
      },
    });
  }

  canAction(
    resource: any,
    action: string,
    resourceType: ResourceType = 'load_balancer',
  ): boolean {
    return this.roleService.resourceHasPermission(
      resource,
      resourceType,
      action,
    );
  }

  // certificate api
  getCertificates<T>(params: any): Promise<T[]> {
    return this.httpService
      .request(`${this.CERT_ENDPOINT}`, {
        method: 'GET',
        params,
      })
      .then(({ results }) => results);
  }

  getPageCertificates<T>({
    page = 1,
    page_size = 20,
    search = '',
  }): Promise<FetchDataResult<T>> {
    return this.httpService.request(`${this.CERT_ENDPOINT}`, {
      method: 'GET',
      params: {
        page,
        page_size,
        search,
      },
    });
  }

  createCertificate(body: any) {
    return this.httpService.request(this.CERT_ENDPOINT, {
      method: 'POST',
      body,
    });
  }

  deleteCertificate(name: string) {
    return this.httpService.request(`${this.CERT_ENDPOINT}/${name}`, {
      method: 'DELETE',
    });
  }

  // frontend api
  getFrontends({
    pageNo = 1,
    pageSize = 20,
    params: { lb_id = '', search = '' },
  }): Promise<FrontendDetail[]> {
    return this.httpService.request(`${this.ENDPOINT}/${lb_id}/frontends`, {
      method: 'GET',
      params: {
        page: pageNo,
        page_size: pageSize,
        search: search,
      },
    });
  }

  createFrontend(lbId: string, data: any) {
    return this.httpService.request(`${this.ENDPOINT}/${lbId}/frontends`, {
      method: 'POST',
      body: data,
    });
  }

  updateFrontend(lbId: string, port: number, body: FrontendDetail) {
    return this.httpService.request(
      `${this.ENDPOINT}/${lbId}/frontends/${port}`,
      {
        method: 'put',
        body,
      },
    );
  }

  deleteFrontend(lb_id: string, port: number, params?: object): Promise<void> {
    return this.httpService.request({
      method: 'delete',
      params,
      url: `${this.ENDPOINT}/${lb_id}/frontends/${port}`,
    });
  }

  getFrontendDetail(lb_id: string, port: number): Promise<FrontendDetail> {
    return this.httpService
      .request(`${this.ENDPOINT}/${lb_id}/frontends/${port}`)
      .then(({ data }) => data);
  }

  // rules api
  getRules<T>(lb_id: string, port: number, params?: any): Promise<T> {
    return this.httpService.request({
      method: 'get',
      url: `${this.ENDPOINT}/${lb_id}/frontends/${port}/rules`,
      params,
    });
  }

  createRule(lb_id: string, port: number, rule: any): Promise<Rule> {
    return this.httpService
      .request({
        method: 'post',
        url: `${this.ENDPOINT}/${lb_id}/frontends/${port}/rules`,
        body: rule,
      })
      .then(({ data }) => data);
  }

  setRuleOrder(lb_id: string, port: number, rule_ids: string[]) {
    return this.httpService.request({
      method: 'put',
      url: `${this.ENDPOINT}/${lb_id}/frontends/${port}/rules`,
      body: rule_ids,
    });
  }

  updateRule(lb_id: string, port: number, rule: any): Promise<void> {
    return this.httpService.request({
      method: 'put',
      url: `${this.ENDPOINT}/${lb_id}/frontends/${port}/rules/${rule.rule_id}`,
      body: rule,
    });
  }

  deleteRule(lb_id: string, port: number, rule_id: string): Promise<void> {
    return this.httpService.request({
      method: 'delete',
      url: `${this.ENDPOINT}/${lb_id}/frontends/${port}/rules/${rule_id}`,
    });
  }

  updateRuleServices({
    lb_id,
    port,
    rule_id,
    body,
  }: {
    lb_id: string;
    port: number;
    rule_id: string;
    body: any;
  }): Promise<void> {
    return this.httpService.request({
      method: 'post',
      url: `${this.ENDPOINT}/${lb_id}/frontends/${port}/rules/${rule_id}`,
      body,
    });
  }

  // subnent new api enable or not
  subnetNewApiEnabled(cluster_id: string): Promise<boolean> {
    return this.httpService
      .request({
        method: 'GET',
        url: `${this.K8S_SUNNET_ENDPOINT}/${cluster_id}/is_new_macvlan`,
      })
      .then(({ new_macvlan }) => {
        return new_macvlan;
      })
      .catch(() => {
        return false;
      });
  }

  // subnet api
  getSubnets(region_name: string): Promise<Subnet[]> {
    return this.httpService
      .request({
        method: 'GET',
        url: `${this.SUBNET_ENPOINT}`,
        params: {
          region_name,
        },
        addNamespace: true,
      })
      .then(({ result }) => result);
  }

  getK8sSubnets(cluster_id: string): Promise<K8sSubnet[]> {
    return this.httpService
      .request({
        method: 'GET',
        url: `${this.K8S_SUNNET_ENDPOINT}/${cluster_id}`,
        addNamespace: false,
      })
      .then(({ result }) => result);
  }

  getSubnet(subnet_name: string): Promise<Subnet> {
    return this.httpService.request({
      method: 'GET',
      url: `${this.SUBNET_ENPOINT}/${subnet_name}`,
      addNamespace: true,
    });
  }

  getK8sSubnet(cluster_id: string, subnet_name: string): Promise<K8sSubnet> {
    return this.httpService.request({
      method: 'GET',
      url: `${this.K8S_SUNNET_ENDPOINT}/${cluster_id}/${subnet_name}`,
      addNamespace: false,
    });
  }

  getSubnetIps(subnet_name: string): Promise<SubnetIp[]> {
    return this.httpService
      .request({
        method: 'GET',
        url: `${this.SUBNET_ENPOINT}/${subnet_name}/private_ips`,
        addNamespace: true,
      })
      .then(({ result }) => result);
  }

  getK8sSubnetIps(
    cluster_id: string,
    subnet_name: string,
  ): Promise<K8sSubnetIp[]> {
    return this.httpService
      .request({
        method: 'GET',
        url: `${
          this.K8S_SUNNET_ENDPOINT
        }/${cluster_id}/${subnet_name}/private_ips`,
        addNamespace: false,
      })
      .then(({ result }) => result);
  }

  deleteSubnet(subnet_name: string) {
    return this.httpService.request({
      method: 'DELETE',
      url: `${this.SUBNET_ENPOINT}/${subnet_name}`,
    });
  }

  deleteK8sSubnet(cluster_id: string, subnet_name: string) {
    return this.httpService.request({
      method: 'DELETE',
      url: `${this.K8S_SUNNET_ENDPOINT}/${cluster_id}/${subnet_name}`,
      addNamespace: false,
    });
  }

  createSubnet(data: SubnetRaw) {
    return this.httpService.request({
      method: 'POST',
      url: `${this.SUBNET_ENPOINT}`,
      body: data,
    });
  }

  createK8sSubnet(cluster_id: string, data: K8sSubnetPayload) {
    return this.httpService.request({
      method: 'POST',
      url: `${this.K8S_SUNNET_ENDPOINT}/${cluster_id}`,
      body: data,
      addNamespace: false,
    });
  }

  importSubnetIp(subnet_name: string, data: IpImportPayload) {
    return this.httpService.request({
      method: 'POST',
      url: `${this.SUBNET_ENPOINT}/${subnet_name}/private_ips/`,
      body: data,
    });
  }

  importK8sSubnetIp(
    cluster_id: string,
    subnet_name: string,
    data: IpImportPayload,
  ) {
    return this.httpService.request({
      method: 'POST',
      url: `${
        this.K8S_SUNNET_ENDPOINT
      }/${cluster_id}/${subnet_name}/private_ips`,
      body: data,
      addNamespace: false,
    });
  }

  deleteSubnetIp(subnet_name: string, ip_name: string) {
    return this.httpService.request({
      method: 'DELETE',
      url: `${this.SUBNET_ENPOINT}/${subnet_name}/private_ips/${ip_name}`,
    });
  }

  deleteK8sSubnetIp(cluster_id: string, subnet_name: string, ip_name: string) {
    return this.httpService.request({
      method: 'DELETE',
      url: `${
        this.K8S_SUNNET_ENDPOINT
      }/${cluster_id}/${subnet_name}/private_ips/${ip_name}`,
      addNamespace: false,
    });
  }

  updateSubnet(subnet_name: string, body: any) {
    return this.httpService.request({
      method: 'PUT',
      url: `${this.SUBNET_ENPOINT}/${subnet_name}`,
      body,
    });
  }

  updateK8sSubnet(
    cluster_id: string,
    subnet_name: string,
    payload: { cidr: string; gateway: string },
  ) {
    return this.httpService.request({
      method: 'PUT',
      url: `${this.K8S_SUNNET_ENDPOINT}/${cluster_id}/${subnet_name}`,
      body: payload,
      addNamespace: false,
    });
  }

  // network policy
  createNetworkPolicy(cluster_id: string, data: any) {
    return this.httpService.request({
      method: 'POST',
      url: `${this.NETWORK_POLICY_ENPOINT}/${cluster_id}/networkpolicies`,
      body: data,
    });
  }

  updateNetworkPolicy(
    cluster_id: string,
    namespace: string,
    name: string,
    data: any,
  ) {
    return this.httpService.request({
      method: 'PUT',
      url: `${
        this.NETWORK_POLICY_ENPOINT
      }/${cluster_id}/networkpolicies/${namespace}/${name}`,
      body: data,
    });
  }

  getNetworkPolicy(cluster_id: string, namespace: string, name: string) {
    return this.httpService.request({
      method: 'GET',
      url: `${
        this.NETWORK_POLICY_ENPOINT
      }/${cluster_id}/networkpolicies/${namespace}/${name}`,
    });
  }

  getNetworkPolicies(cluster_id: string) {
    return this.httpService.request({
      method: 'GET',
      url: `${this.NETWORK_POLICY_ENPOINT}/${cluster_id}/networkpolicies`,
    });
  }

  deleteNetworkPolicy(cluster_id: string, namespace: string, name: string) {
    console.log('delete policy', namespace, name);
    return this.httpService.request({
      method: 'DELETE',
      url: `${
        this.NETWORK_POLICY_ENPOINT
      }/${cluster_id}/networkpolicies/${namespace}/${name}`,
    });
  }

  canNetworkPolicyAction(
    resource: any,
    action: string,
    resourceType: ResourceType = 'k8s_networkpolicies',
  ): boolean {
    return this.roleService.resourceHasPermission(
      resource,
      resourceType,
      action,
    );
  }
}
