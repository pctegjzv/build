import { Injectable } from '@angular/core';
import * as _ from 'lodash';

import {
  MATCH_TYPE,
  MATCH_TYPES,
  RULE_TYPE,
  Rule,
  RuleIndicator,
} from 'app2/shared/services/features/network.service.ts';
import { Region } from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { TranslateService } from 'app2/translate/translate.service';

const KEY_TYPES = [RULE_TYPE.COOKIE, RULE_TYPE.HEADER, RULE_TYPE.PARAM];

const RULE_INDICATORS_CACHE = {};
const PARTIAL_RULE_INDICATORS_CACHE = {};

@Injectable()
export class NetworkUtilitiesService {
  constructor(
    private roleUtilities: RoleUtilitiesService,
    private translateFilter: TranslateService,
  ) {}

  private isIaasLoadBalancer(str: string) {
    const name = str.toLowerCase();
    return name !== 'alb' && /^\w{1}lb$/.test(name);
  }

  getRegionSupportedCreateTypes(region: Region) {
    const types: any[] = [];
    const features: string[] = _.get(region, 'features.service.features', []);
    if (features.includes('alb')) {
      features.forEach(item => {
        if (this.isIaasLoadBalancer(item)) {
          types.push({
            name: item.toUpperCase(),
            value: item,
          });
        }
      });
    }
    return types;
  }

  getAddressTypes() {
    return [
      {
        name: this.translateFilter.get('lb_address_internal'),
        value: 'internal',
      },
      {
        name: this.translateFilter.get('lb_address_external'),
        value: 'external',
      },
    ];
  }

  getRegionSupportedImportTypes(region: Region) {
    let types: any[] = [];
    const features = _.get(region, 'features.service.features', []);
    if (features.includes('alb')) {
      if (features.includes('haproxy')) {
        types.push({
          name: 'HAProxy',
          value: 'haproxy',
        });
      }
      if (features.includes('nginx')) {
        types.push({
          name: 'Nginx',
          value: 'nginx',
        });
      }
      types = types.concat(this.getRegionSupportedCreateTypes(region));
    }
    return types;
  }

  tokenizer(rawDsl: string): string[] {
    const tokens: string[] = [];
    let nextTokenBeg = 0;
    rawDsl.split('').forEach((char, i) => {
      if (char === '(') {
        tokens.push('(');
        nextTokenBeg = i + 1;
      } else if (char === ' ') {
        const token = rawDsl.substring(nextTokenBeg, i);
        if (token && token !== ' ' && token !== ')') {
          tokens.push(token);
        }
        nextTokenBeg = i + 1;
      } else if (char === ')') {
        const token = rawDsl.substring(nextTokenBeg, i);
        if (token && token !== ' ' && token !== ')') {
          tokens.push(token);
        }
        tokens.push(')');
        nextTokenBeg = i + 1;
      }
    });
    return tokens;
  }

  parseTokens(tokens: string[]): string | any[] {
    if (!tokens.length) {
      return null;
    }

    const token = tokens.shift();

    if (token !== '(') {
      return token;
    }

    const exp = [];
    while (tokens[0] !== ')') {
      exp.push(this.parseTokens(tokens));
    }
    tokens.shift();
    return exp;
  }

  parseDSL(dslOrRule: string | Rule): RuleIndicator[] {
    let rawDSL;

    if (typeof dslOrRule === 'string') {
      rawDSL = dslOrRule;
    } else if (dslOrRule.dsl) {
      rawDSL = dslOrRule.dsl;
    } else {
      const indicators: RuleIndicator[] = [];

      if (dslOrRule.domain) {
        indicators.push({
          type: RULE_TYPE.HOST,
          key: null,
          values: [[MATCH_TYPE.IN, dslOrRule.domain]],
        });
      } else if (dslOrRule.url) {
        indicators.push({
          type: RULE_TYPE.URL,
          key: null,
          values: [
            [
              dslOrRule.url.startsWith('/')
                ? MATCH_TYPE.STARTS_WITH
                : MATCH_TYPE.REGEX,
              dslOrRule.url,
            ],
          ],
        });
      } else {
        return [];
      }

      rawDSL = this.stringifyDSL(indicators);
    }

    if (RULE_INDICATORS_CACHE[rawDSL]) {
      return RULE_INDICATORS_CACHE[rawDSL];
    }

    const tokens = this.parseTokens(this.tokenizer(rawDSL)) as any[];

    if (!tokens) {
      return [];
    }

    const first = tokens[0];

    const indicators = first === MATCH_TYPE.AND ? tokens.slice(1) : tokens;

    const ruleIndicators: RuleIndicator[] = [];

    indicators.forEach(indicator => {
      let matchType: MATCH_TYPE = indicator[0];
      const values: string[] | string[][] = indicator.slice(1);

      const ruleIndicator = { values: [] } as RuleIndicator;

      if (matchType === MATCH_TYPE.OR) {
        (values as string[][]).forEach(value => {
          matchType = value.shift() as MATCH_TYPE;
          const ruleType = value.shift();

          if (!ruleIndicator.type) {
            ruleIndicator.type = ruleType as RULE_TYPE;
          } else if (ruleIndicator.type !== ruleType) {
            throw new TypeError(
              'invalid dsl, rule type should be same in a single OR block',
            );
          }

          if (KEY_TYPES.includes(ruleIndicator.type)) {
            const key = value.shift() as string;

            if (!ruleIndicator.key) {
              ruleIndicator.key = key;
            } else if (ruleIndicator.key !== key) {
              throw new TypeError(
                'invalid dsl, rule key should be same in a single OR block',
              );
            }
          }

          ruleIndicator.values.push([MATCH_TYPE[matchType], ...value]);
        });
      } else {
        const ruleType = values.shift() as RULE_TYPE;

        ruleIndicator.type = ruleType;

        if (KEY_TYPES.includes(ruleIndicator.type)) {
          ruleIndicator.key = values.shift() as string;
        }

        if (ruleType === RULE_TYPE.HOST) {
          (values as string[]).forEach(value => {
            ruleIndicator.values.push([MATCH_TYPE[matchType], value]);
          });
        } else {
          ruleIndicator.values.push([MATCH_TYPE[matchType], ...values]);
        }
      }

      ruleIndicators.push(ruleIndicator);
    });

    return (RULE_INDICATORS_CACHE[rawDSL] = ruleIndicators);
  }

  stringifyDSL(ruleIndicators: RuleIndicator[]): string {
    let result = '';

    ruleIndicators.forEach(({ key, type, values }) => {
      if (!values.length) {
        return;
      }

      if (type === RULE_TYPE.HOST) {
        result += ` (IN HOST ${values.map(valueArr => valueArr[1]).join(' ')})`;
        return;
      }

      const temp = values.reduce((temp, [matchType, ...args]) => {
        matchType = MATCH_TYPES[matchType];

        if (key) {
          args.unshift(key);
        }

        args.unshift(matchType, type);

        temp += ` (${args.join(' ')})`;
        return temp;
      }, '');

      result += values.length > 1 ? ` (OR${temp})` : temp;
    });

    return result && `(AND${result})`;
  }

  getPartialIndicators(
    rawDslOrIndicators: string | RuleIndicator[],
  ): { more: boolean; indicators: RuleIndicator[] } {
    if (typeof rawDslOrIndicators !== 'string') {
      rawDslOrIndicators = this.stringifyDSL(rawDslOrIndicators);
    }

    if (PARTIAL_RULE_INDICATORS_CACHE[rawDslOrIndicators]) {
      return PARTIAL_RULE_INDICATORS_CACHE[rawDslOrIndicators];
    }

    const ruleIndicators =
      typeof rawDslOrIndicators === 'string'
        ? this.parseDSL(rawDslOrIndicators)
        : rawDslOrIndicators;

    const indicators: RuleIndicator[] = [];

    let i = 0;
    let more: boolean;

    ruleIndicators.some(({ key, type, values }) => {
      const newValues: string[][] = [];

      values.some(value => {
        more = i++ > 2;

        if (!more) {
          newValues.push(value);
        }

        return more;
      });

      if (newValues.length) {
        indicators.push({ key, type, values: newValues });
      }

      return more;
    });

    return (PARTIAL_RULE_INDICATORS_CACHE[rawDslOrIndicators] = {
      more,
      indicators,
    });
  }

  canNetworkPolicyDelete(np: any) {
    return this.roleUtilities.resourceHasPermission(
      np,
      'k8s_networkpolicies',
      'delete',
    );
  }

  canNetworkPolicyUpdate(np: any) {
    return this.roleUtilities.resourceHasPermission(
      np,
      'k8s_networkpolicies',
      'update',
    );
  }
}
