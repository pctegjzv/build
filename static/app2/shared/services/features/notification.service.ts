import { Inject, Injectable } from '@angular/core';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { HttpService } from 'app2/shared/services/http/http.service';

export interface NotificationSubscription {
  method: string;
  recipient: string;
  secret?: string;
  remark?: string;
}
export interface NotificationPayload {
  name: string;
  space_name?: string;
  subscriptions: NotificationSubscription[];
}

@Injectable()
export class NotificationService {
  NOTIFICATION_URL: string;
  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) account: RcAccount,
  ) {
    this.NOTIFICATION_URL = `/ajax/notifications/${account.namespace}/`;
  }

  getNotifications(): Promise<any> {
    return this.httpService.request(this.NOTIFICATION_URL, {
      method: 'GET',
    });
  }

  getNotificationDetail(name: string): Promise<any> {
    return this.httpService.request(this.NOTIFICATION_URL + name, {
      method: 'GET',
    });
  }

  createNotification(payload: any): Promise<any> {
    return this.httpService.request(this.NOTIFICATION_URL, {
      method: 'POST',
      body: payload,
    });
  }

  updateNotification(payload: any): Promise<any> {
    const notification = payload.uuid;
    return this.httpService.request(this.NOTIFICATION_URL + notification, {
      method: 'PUT',
      body: payload,
    });
  }

  deleteNotification(name: string): Promise<any> {
    return this.httpService.request(this.NOTIFICATION_URL + name, {
      method: 'DELETE',
    });
  }

  getNotificationSubscribers(): Promise<any> {
    return this.httpService.request(this.NOTIFICATION_URL + 'subscribers', {
      method: 'GET',
    });
  }
}
