import { Inject, Injectable } from '@angular/core';
import { NotificationService } from 'alauda-ui';
import { ACCOUNT, WEBLABS } from 'app2/core/tokens';
import { RcAccount, RcRole, Weblabs } from 'app2/core/types';
import { TranslateService } from 'app2/translate/translate.service';
import { getCookie } from 'app2/utils/cookie';

import { User } from '../../types';
import { HttpService } from '../http/http.service';

export interface AccountPage {
  count: number;
  results: RcAccount[];
}

export interface Org {
  company: string;
  name: string;
  logo_file: string;
}

@Injectable()
export class OrgService {
  private URL_PREFIX: string;

  constructor(
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(WEBLABS) public weblabs: Weblabs,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private http: HttpService,
  ) {
    this.URL_PREFIX = `/ajax/orgs/${account.namespace}`;
  }

  getOrg() {
    return this.http.request<Org>(this.URL_PREFIX);
  }

  /**
   * get user information
   */
  getUser(username?: string): Promise<User> {
    return this.http.request<User>(
      `${this.URL_PREFIX}/accounts/${username || this.account.username}`,
    );
  }

  updateUser(user: User) {
    return this.http.request(
      `${this.URL_PREFIX}/accounts/${user.username}/details`,
      {
        method: 'put',
        body: user,
      },
    );
  }

  updateOrg(name: string, company: string) {
    const url = `/ajax/orgs/${name}`;
    return this.http.request(url, {
      method: 'PUT',
      body: { name: name, company: company },
    });
  }

  //***  ldap start
  createOrgLdap(options: { config?: any; orgName?: string }) {
    return this.http
      .request(`/ajax/orgs/${options.orgName}/config/ldap`, {
        method: 'POST',
        body: options.config,
      })
      .then(res => {
        this.auiNotificationService.success({
          content: this.translate.get('org_ldap_create_success'),
        });
        return res;
      });
  }

  getOrgLdap({ cache = false } = {}) {
    return this.http.request(
      `/ajax/orgs/${this.account.namespace}/config/ldap`,
      {
        method: 'GET',
        cache,
      },
    );
  }

  getOrgLdapSync() {
    return this.http.request(
      `/ajax/orgs/${this.account.namespace}/config/ldap/sync`,
      {
        method: 'GET',
      },
    );
  }

  deleteOrgLdap(options: { orgName?: string }) {
    return this.http
      .request(`/ajax/orgs/${options.orgName}/config/ldap`, {
        method: 'DELETE',
      })
      .then(res => {
        this.auiNotificationService.success({
          content: this.translate.get('org_ldap_delete_success'),
        });
        return res;
      });
  }

  updateOrgLdap(options: { config?: any; orgName?: string }) {
    options.config.org_name = options.orgName;
    return this.http
      .request({
        method: 'PUT',
        url: `/ajax/orgs/${options.orgName}/config/ldap`,
        body: options.config,
      })
      .then(res => {
        this.auiNotificationService.success({
          content: this.translate.get('org_ldap_update_success'),
        });
        return res;
      });
  }

  triggerOrgLdapSync(options: { orgName?: string }) {
    return this.http
      .request({
        method: 'PUT',
        url: `/ajax/orgs/${options.orgName}/config/ldap/sync`,
      })
      .then(res => {
        this.auiNotificationService.success({
          content: this.translate.get('org_ldap_sync_trigger_success'),
        });
        return res;
      });
  }

  getOrgLdapInfo() {
    return this.http.request({
      method: 'GET',
      url: `/ajax/orgs/${this.account.namespace}/config/ldap/info`,
    });
  }

  deleteLdapAccounts(params: any) {
    return this.http.request(
      `/ajax/orgs/${this.account.namespace}/config/ldap/sync`,
      {
        method: 'DELETE',
        params,
      },
    );
  }

  // *** ldap end

  listOrgAccounts({
    page = 1,
    page_size = 20,
    order_by = 'username',
    search = '',
    team_name_filter = '',
    assign = '',
  }) {
    const url = `/ajax/orgs/${this.account.namespace}/accounts/`;
    return this.http.request(url, {
      method: 'GET',
      params: {
        page,
        page_size,
        order_by,
        search,
        team_name_filter,
        assign,
      },
      ignoreProject: true,
    });
  }

  listOrgFilteredAccounts({
    org_name = '',
    page = 1,
    page_size = 20,
    order_by = 'username',
    search = '',
    team_name_filter = '',
    assign = '',
    invalid_ldap = '',
    action = 'view',
    project_name = '',
    ignoreProject = false,
  }) {
    const url = '/ajax/org/account/filtered_list';
    return this.http.request(url, {
      method: 'GET',
      params: {
        org_name,
        page,
        page_size,
        order_by,
        search,
        team_name_filter,
        assign,
        invalid_ldap,
        action,
        project_name,
      },
      ignoreProject,
    });
  }

  getAccounts({
    page = 1,
    page_size = 20,
    order_by = 'username',
    search = '',
    team_name_filter = '',
    assign = '',
    invalid_ldap = '',
    action = 'view',
    project_name = '',
    k8s_namespace_uuid = '',
    customer = '',
    ignoreProject = false,
  }): Promise<AccountPage> {
    const url = `/ajax/orgs/${this.account.namespace}/accounts/`;
    return this.http.request({
      url: url,
      method: 'GET',
      params: {
        page,
        page_size,
        order_by,
        search,
        team_name_filter,
        assign,
        invalid_ldap,
        action,
        project_name,
        k8s_namespace_uuid,
        customer,
      },
      addNamespace: false,
      ignoreProject,
    });
  }

  getOrgAccount({ username = '', cache = false }) {
    const url = `/ajax/orgs/${this.account.namespace}/accounts/${username}`;
    return this.http.request(url, {
      method: 'GET',
      cache,
    });
  }

  removeOrgAccount(data: any) {
    const url = `/ajax/orgs/${this.account.namespace}/accounts/${
      data.username
    }`;
    return this.http.request(url, {
      method: 'DELETE',
      params: data,
    });
  }

  getAccountRoles(username: string) {
    return this.http.request({
      method: 'GET',
      url: `/ajax/orgs/${this.account.namespace}/accounts/${username}/roles/`,
      ignoreProject: true,
    });
  }

  addAccountRoles(username: string, roles: any) {
    return this.http.request(
      `/ajax/orgs/${this.account.namespace}/accounts/${username}/roles/`,
      {
        method: 'POST',
        body: roles,
        addNamespace: false,
      },
    );
  }

  removeAccountRoles(username: string, roles: any) {
    return this.http.request(
      `/ajax/orgs/${this.account.namespace}/accounts/${username}/roles/`,
      {
        method: 'DELETE',
        body: roles,
        addNamespace: false,
      },
    );
  }

  createRoleBasedAccounts(data: any) {
    return this.http.request(`/ajax/orgs/${this.account.namespace}/accounts`, {
      method: 'POST',
      body: data,
    });
  }

  //
  async getAllRolesOrUsers(type: 'role' | 'user'): Promise<Array<any>> {
    const pageSize = 50;
    let roles: Array<RcRole> = [];
    const reqUrlFun =
      type === 'role' ? this.getRoleBackUrl : this.getUerBackUrl;
    try {
      const firstPartData: {
        count?: number;
        results?: Array<RcRole>;
      } = await this.http.request(reqUrlFun.call(this, 1, pageSize));
      roles = roles.concat(firstPartData['results']);
      const total = Math.floor(firstPartData.count / pageSize);
      if (total) {
        const promiseArr = new Array(total).fill(0).map((_val, index) => {
          return this.http.request(reqUrlFun.call(this, index + 2, pageSize));
        });
        const retArr = await Promise.all(promiseArr);
        retArr.forEach(ret => {
          roles = roles.concat(ret['results']);
        });
      }
    } catch (e) {
      if (e.errors) {
        this.auiNotificationService.error({
          content: this.translate.get(e.errors[0].code),
        });
      } else {
        throw e;
      }
    } finally {
      return roles;
    }
  }

  getRoleBackUrl(page: number, pageSize: number = 50) {
    const currentProject = getCookie('project');
    let roleBackendUrl = `/ajax/roles/${
      this.account.namespace
    }/?page=${page}&page_size=${pageSize}&assign=true&namespace=${
      this.account.namespace
    }&order_by=created_at&action=view`;
    if (this.weblabs.PROJECTS_ENABLED && currentProject) {
      roleBackendUrl += `&project_name=${currentProject}`;
    }
    return roleBackendUrl;
  }

  getUerBackUrl(page: number = 1, pageSize: number = 50) {
    return `/ajax/org/account/filtered_list?page=${page}&page_size=${pageSize}&action=view&assign=true&order_by=username&namespace=${
      this.account.namespace
    }`;
  }
}
