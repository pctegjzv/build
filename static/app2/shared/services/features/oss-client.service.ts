import { Inject, Injectable } from '@angular/core';
import * as JsSha from 'jssha';

import { ACCOUNT, ENVIRONMENTS } from 'app2/core/tokens';
import { Environments, RcAccount } from 'app2/core/types';
import { AccountService } from 'app2/shared/services/features/account.service';

@Injectable()
export class OssClientService {
  constructor(
    @Inject(ENVIRONMENTS) private environments: Environments,
    @Inject(ACCOUNT) private account: RcAccount,
    private accountService: AccountService,
  ) {}

  async generateLink({
    skipMiddleLayer = false,
    bucket = this.account.namespace,
    path,
  }: {
    skipMiddleLayer?: boolean;
    bucket?: string;
    path: string;
  }) {
    const domain = skipMiddleLayer
      ? this.environments.oss_server_url
      : location.origin + '/oss';
    return (
      domain +
      `/${bucket}/${path}?` +
      $.param(await this.generateOssSignature())
    );
  }

  async generateOssSignature({ bucket = this.account.namespace } = {}) {
    const token = await this.accountService.getUserToken();
    const jssha = new JsSha('SHA-256', 'TEXT');
    jssha.setHMACKey(token, 'TEXT');
    const timestamp = Math.floor(new Date().valueOf() / 1000);
    let username = this.account.username;
    username = this.account.namespace + (username ? '/' + username : '');
    jssha.update(`${username}/${bucket}/${timestamp}`);

    return {
      username,
      signature: jssha.getHMAC('HEX'),
      timestamp,
    };
  }
}
