import { Inject, Injectable } from '@angular/core';

import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { TranslateService } from 'app2/translate/translate.service';

export interface PwdPattern {
  min?: number;
  errorMin?: string;
  max?: number;
  errorMax?: string;
  pattern?: RegExp;
  errorPattern?: string;
  strength?: number;
  errorStrength?: string;
}
export enum PasswordType {
  UAA = 'uaa',
}

@Injectable()
export class PatternHelperService {
  constructor(
    private translate: TranslateService,
    @Inject(ENVIRONMENTS) private environments: Environments,
  ) {}

  getPasswordPattern(type: PasswordType | '' = ''): PwdPattern {
    const isPrivateDeployEnabled = this.environments.is_private_deploy_enabled;
    const passwordPatternType =
      type ||
      (isPrivateDeployEnabled
        ? this.environments.private_deploy_password_pattern_type
        : '');
    let passwordPatternObj = {};
    switch (passwordPatternType) {
      case 'A':
        passwordPatternObj = {
          min: 8,
          errorMin: this.translate.get('warning_min_length', {
            length: 8,
          }),
          max: 64,
          errorMax: this.translate.get('warning_max_length', {
            length: 64,
          }),
          pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[`~!@#$%^&?,;:"'])[A-Za-z\d`~!@#$%^&?,;:"']{8,}$/,
          errorPattern: this.translate.get('passowrd_pattern_type_a_error'),
          strength: 0,
        };
        break;
      case PasswordType.UAA:
        passwordPatternObj = {
          min: 8,
          errorMin: this.translate.get('warning_min_length', {
            length: 8,
          }),
          max: 64,
          errorMax: this.translate.get('warning_max_length', {
            length: 64,
          }),
          pattern: /^(?=.*[a-z])(?=.*\d)(?=.*[`~!@#$%^&?,;:"'()_+-={}\[\]|<>./*])[a-z\d`~!@#$%^&?,;:"'()_+-={}\[\]|<>./*]{8,}$/i,
          errorPattern: this.translate.get('passowrd_pattern_type_a_error_2'),
          strength: 0,
        };
        break;
      default:
        passwordPatternObj = {
          min: 6,
          errorMin: this.translate.get('warning_min_length', {
            length: 6,
          }),
          max: 64,
          errorMax: this.translate.get('warning_max_length', {
            length: 64,
          }),
          pattern: '',
          errorPattern: '',
          errorStrength: this.translate.get('password_too_weak'),
          strength: 45,
        };
        break;
    }
    return passwordPatternObj;
  }

  getChangePasswordErrorMapper(type: PasswordType | '' = '') {
    const passwordPattern: PwdPattern = this.getPasswordPattern(type);
    return {
      map: (key: string, _error: any, control: any) => {
        let translateKey = '';
        if (key === 'required') {
          translateKey =
            control.name === 'old_password'
              ? 'old_password_not_empty'
              : 'password_not_empty';
        } else {
          switch (key) {
            case 'same_password':
              translateKey = 'password_not_same';
              break;
            case 'minlength':
              translateKey = passwordPattern.errorMin;
              break;
            case 'maxlength':
              translateKey = passwordPattern.errorMax;
              break;
            case 'pattern':
              translateKey = passwordPattern.errorPattern;
              break;
            case 'strength':
              translateKey = passwordPattern.errorStrength;
              break;
          }
        }
        return this.translate.get(translateKey);
      },
    };
  }

  getPasswordStrength(pass: string) {
    let score = 0;
    if (!pass) {
      return score;
    }
    // award every unique letter until 5 repetitions
    const letters = {};
    for (let i = 0; i < pass.length; i++) {
      letters[pass[i]] = (letters[pass[i]] || 0) + 1;
      score += 5.0 / letters[pass[i]];
    }

    // bonus points for mixing it up
    const variations = {
      digits: /\d/.test(pass),
      lower: /[a-z]/.test(pass),
      upper: /[A-Z]/.test(pass),
      nonWords: /\W/.test(pass),
    };

    let variationCount = 0;
    for (const check of Object.keys(variations)) {
      variationCount += variations[check] === true ? 1 : 0;
    }
    score += (variationCount - 1) * 10;
    return +score;
  }
}
