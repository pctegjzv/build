import { Inject, Injectable } from '@angular/core';
import * as moment from 'moment';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import {
  HistoryCountResult,
  HistoryStatsCount,
  TimeSection,
} from 'app2/shared/services/features/model.types';
import { HttpService } from 'app2/shared/services/http/http.service';

@Injectable()
export class PipelineService {
  PIPELINE_HISTORY_URL: string;

  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) account: RcAccount,
  ) {
    const namespace = account.namespace;
    this.PIPELINE_HISTORY_URL = `/ajax/v1/pipelines/${namespace}/history/`;
  }

  getPipelinesCount({
    start_time,
    end_time,
  }: TimeSection): Promise<HistoryStatsCount> {
    return this.httpService
      .request(this.PIPELINE_HISTORY_URL, {
        method: 'GET',
        params: {
          display_count: 1,
          start_time,
          end_time,
        },
      })
      .then(({ count, unit, count_result }) => {
        return {
          count,
          unit,
          count_result: count_result.map((item: HistoryCountResult) => {
            return {
              OK: item.OK,
              ERROR: item.ERROR,
              OTHER: item.OTHER,
              start: parseFloat(moment.utc(item.start).format('X')),
              end: parseFloat(moment.utc(item.end).format('X')),
            };
          }),
        };
      });
  }
}
