import { Inject, Injectable } from '@angular/core';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { HttpService } from 'app2/shared/services/http/http.service';

@Injectable()
export class PluginApiService {
  private PLUGIN_TYPES_ENDPOINT: string =
    '/ajax/v1/plugin-types/' + this.account.namespace + '/';

  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) public account: RcAccount,
  ) {}

  getList(region_uuid: string, is_new_k8s: boolean) {
    return this.httpService
      .request(this.PLUGIN_TYPES_ENDPOINT, {
        method: 'GET',
        params: {
          region_uuid,
          is_new_k8s,
        },
        addNamespace: false,
      })
      .then(({ results }) => results);
  }

  getForm(pluginType: string) {
    return this.httpService.request(this.PLUGIN_TYPES_ENDPOINT + pluginType, {
      method: 'GET',
      addNamespace: false,
    });
  }

  getInstances(pluginType: string, region_uuid: string) {
    return this.httpService
      .request(this.PLUGIN_TYPES_ENDPOINT + pluginType + '/plugins', {
        method: 'GET',
        params: {
          region_uuid,
        },
        addNamespace: false,
      })
      .then(({ results }) => results);
  }

  getInstanceInfo(pluginType: string, uuid: string) {
    return this.httpService.request(
      this.PLUGIN_TYPES_ENDPOINT + pluginType + '/plugins/' + uuid,
      {
        method: 'GET',
        addNamespace: false,
      },
    );
  }

  createInstance(pluginType: string, body: object) {
    return this.httpService.request(
      this.PLUGIN_TYPES_ENDPOINT + pluginType + '/plugins',
      {
        method: 'POST',
        body,
        addNamespace: false,
      },
    );
  }
}
