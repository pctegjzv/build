import { Inject, Injectable } from '@angular/core';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { HttpService } from 'app2/shared/services/http/http.service';
import { TranslateService } from 'app2/translate/translate.service';

const clients = [
  {
    name: 'GITHUB',
    displayName: 'jenkins_client_github',
    integrated: true,
    is_authed: false,
  },
  {
    name: 'BITBUCKET',
    displayName: 'jenkins_client_bitbucket',
    integrated: true,
    is_authed: false,
  },
  {
    name: 'GIT',
    displayName: 'jenkins_client_git',
    integrated: false,
    is_authed: false,
  },
  {
    name: 'SVN',
    displayName: 'jenkins_client_svn',
    integrated: false,
    is_authed: false,
  },
];

export interface FindParams {
  families: string;
  types: string;
  page: string;
  pageSize: string;
}

@Injectable()
export class PrivateBuildCodeClientService {
  URL = `/ajax/private-build-code-clients/${this.account.namespace}`;
  opt = { addNamespace: false };

  constructor(
    private httpService: HttpService,
    private translate: TranslateService,
    @Inject(ACCOUNT) public account: RcAccount,
  ) {}

  find(): Promise<any[]> {
    return this.httpService
      .request({
        url: this.URL,
        method: 'GET',
        ...this.opt,
      })
      .then((response: any) => response.result)
      .then(result => {
        return clients.map(client => {
          const status = result.find(
            (status: any) => status.name === client.name,
          );
          return {
            ...client,
            displayName: this.translate.get(client.displayName),
            is_authed: (status && status.is_authed) || false,
          };
        });
      });
  }

  findCodeOrgs(client: string): Promise<any[]> {
    const params = client === 'BITBUCKET' ? { page: 1 } : {};

    return this.httpService
      .request({
        url: `${this.URL}/${client}`,
        method: 'GET',
        params,
        ...this.opt,
      })
      .then(
        (response: any) =>
          client === 'BITBUCKET' ? response.values : response.result,
      );
  }

  findGithubRepos(org: string): Promise<any[]> {
    return this.httpService
      .request({
        url: `${this.URL}/GITHUB/orgs/${org}/repos`,
        method: 'GET',
        ...this.opt,
      })
      .then((response: any) => response.result);
  }

  findBitbucketRepos(
    org: string,
    page: number,
    pagelen: number = 8,
  ): Promise<{ results: any[]; total: number }> {
    return this.httpService
      .request({
        url: `${this.URL}/BITBUCKET/orgs/${org}/repos`,
        method: 'GET',
        params: {
          page,
          pagelen,
        },
        ...this.opt,
      })
      .then((response: any) => {
        const { size, values } = response;

        return {
          results: values,
          total: size,
        };
      });
  }
}
