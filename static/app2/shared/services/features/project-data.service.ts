import { Inject, Injectable } from '@angular/core';
import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { HttpService } from 'app2/shared/services/http/http.service';

@Injectable()
export class ProjectDataService {
  private BASE_URL: string;

  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) account: RcAccount,
  ) {
    this.BASE_URL = `/ajax/v1/projects/${account.namespace}/`;
  }

  getProjects() {
    return this.httpService
      .request({
        url: this.BASE_URL,
        method: 'GET',
        addNamespace: false,
      })
      .then(({ result }) => result)
      .catch(() => []);
  }

  getDetail(name: string) {
    return this.httpService.request({
      url: this.BASE_URL + name,
      method: 'GET',
      addNamespace: false,
    });
  }
}
