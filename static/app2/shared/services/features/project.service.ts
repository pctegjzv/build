import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { HttpService } from 'app2/shared/services/http/http.service';
import { QuotaRange } from 'app_user/core/types';

import { getCookie } from 'app2/utils/cookie';

export interface Project {
  name: string;
  uuid: string;
  display_name: string;
  status: string;
  description: string;
  template_uuid: string;
  template: string;
  created_at: string;
  updated_at: string;
  created_by: string;
  service_type?: string[];
  clusters?: {
    name: string;
    display_name: string;
    uuid: string;
  }[];
  admin_list?: string[];
}

export interface NamespaceSetting {
  quotas: {}[];
  name: string;
  cluster: NewCluster;
}

export interface NewCluster {
  name: string;
  display_name: string;
  origin_display_name?: string;
  uuid?: string;
  regions?: {
    name: string;
    display_name: string;
    id?: string;
  }[];
  quota?: QuotaRange;
}

export interface ProjectClustersQuotaApi {
  name: string;
  uuid: number;
  clusters: NewCluster[];
}

export interface NamespaceQuota {
  cluster_uuid: string;
  namespace_uuid: string;
  project_uuid: string;
  quota: {
    pvc_num: number;
    storage: number;
    memory: string;
    pods: string;
    cpu: string;
  };
}

@Injectable()
export class ProjectService {
  private BASE_URL: string;
  public project$: Observable<Project>;

  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) private account: RcAccount,
  ) {
    this.BASE_URL = `/ajax/v1/projects/${this.account.namespace}/`;
  }

  get() {
    return getCookie('project');
  }

  /**
   * verbose 为false时，不返回clusters和admin_list信息
   */
  getProjects(verbose = true) {
    return this.httpService
      .request({
        url: this.BASE_URL,
        method: 'GET',
        params: {
          verbose,
        },
        addNamespace: false,
        ignoreProject: true,
      })
      .then(({ result }) => result)
      .catch(() => []);
  }

  getProject(uuidOrName: string): Promise<Project> {
    return this.httpService
      .request({
        url: this.BASE_URL + uuidOrName,
        method: 'GET',
        addNamespace: false,
        ignoreProject: true,
      })
      .catch(() => null);
  }

  createProject(body: any) {
    // 占位
    if (!body.template) {
      body.template = 'empty-template';
    }
    return this.httpService.request(this.BASE_URL, {
      method: 'POST',
      body,
      addNamespace: false,
      ignoreProject: true,
    });
  }

  getProjectQuota(project_name: string, cluster_uuids: string = '') {
    return this.httpService.request({
      url: `${this.BASE_URL + project_name}/quota`,
      method: 'GET',
      params: {
        cluster_uuids,
      },
      addNamespace: false,
      ignoreProject: true,
    });
  }

  updateProjectQuota(project_name: string, body: any) {
    return this.httpService.request({
      url: `${this.BASE_URL + project_name}/quota`,
      method: 'PUT',
      body,
      addNamespace: false,
      ignoreProject: true,
    });
  }

  updateProjectBasicInfo(uuid: string, body: any) {
    return this.httpService.request({
      url: this.BASE_URL + uuid,
      method: 'PUT',
      body,
      addNamespace: false,
      ignoreProject: true,
    });
  }

  deleteProject(uuid: string) {
    return this.httpService.request({
      url: this.BASE_URL + uuid,
      method: 'DELETE',
      addNamespace: false,
      ignoreProject: true,
    });
  }
  getProjectClustersQuota(uuid: string, clusters: string[]) {
    return this.httpService.request<ProjectClustersQuotaApi>({
      url: `${this.BASE_URL}${uuid}/quota?cluster_uuids=${clusters.join(',')}`,
      method: 'GET',
      addNamespace: false,
      ignoreProject: true,
    });
  }

  getProjectNamespaceQuotas(
    project_name: string,
    cluster_uuid?: string,
  ): Promise<NamespaceQuota[]> {
    return this.httpService
      .request({
        url: `${
          this.BASE_URL
        }${project_name}/namespace?cluster_uuid=${cluster_uuid || ''}`,
        method: 'GET',
        addNamespace: false,
        ignoreProject: true,
      })
      .then((res: { result: NamespaceQuota[] }) => {
        return res.result ? res.result : [];
      })
      .catch(() => []);
  }

  getAdminRoles(params: {
    level?: 'platform' | 'project' | 'namespace';
    project_name?: string;
    cluster_name?: string;
    k8s_namespace_name?: string;
  }): Promise<string[]> {
    return this.httpService
      .request(`/ajax/v1/role-admin/${this.account.namespace}/users`, {
        params,
        method: 'GET',
        ignoreProject: true,
      })
      .then(({ result }) => result);
  }
}
