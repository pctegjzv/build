import { Inject, Injectable } from '@angular/core';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { HttpService } from 'app2/shared/services/http/http.service';

export interface QuotaSpace {
  uuid: string;
  name: string;
  description: string;
  status: string;
  created_by: string;
  created_at: string;
  resource_actions?: string[];
}

@Injectable()
export class QuotaSpaceService {
  SPACES_URL = `/ajax/spaces/${this.account.namespace}/`;
  opt = { addNamespace: false };

  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) public account: RcAccount,
    private roleUtil: RoleUtilitiesService,
  ) {}

  getConsumableSpaces(): Promise<QuotaSpace[]> {
    return this.getSpaces({ action: 'consume' }).then(list => {
      return list.filter(item => {
        return this.roleUtil.resourceHasPermission(item, 'space', 'consume');
      });
    });
  }

  getSpaces({ action = 'view' } = {}): Promise<QuotaSpace[]> {
    return this.httpService
      .request({
        url: this.SPACES_URL,
        method: 'GET',
        params: {
          action,
        },
      })
      .then(({ result }) => result);
  }
}
