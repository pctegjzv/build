import { Inject, Injectable } from '@angular/core';

import { ACCOUNT } from 'app2/core/tokens';
import { AccountType, RcAccount } from 'app2/core/types';
import { HttpService } from 'app2/shared/services/http/http.service';

@Injectable()
export class RBACService {
  private ROLE_TEMPLATE_ENDPOINT: string =
    '/ajax/v1/role-templates/' + this.account.namespace + '/';
  private ROLE_SCHEMA_ENDPOINT = '/ajax/v1/role-schema/';
  private ROLES_ENDPOINT = '/ajax/v1/roles/' + this.account.namespace + '/';

  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) public account: RcAccount,
  ) {}

  sortPermissions(permissions: Array<any>) {
    const map = {};
    permissions.forEach(item => {
      map[item.resource_type] = item;
    });
    return map;
  }

  getTemplateList(/* pageNo: number, pageSize: number */) {
    return this.httpService
      .request(this.ROLE_TEMPLATE_ENDPOINT, {
        method: 'GET',
        addNamespace: false,
      })
      .then(({ result }) => result);
  }

  getTemplateDetail(uuid: string): Promise<RoleTemplateItem> {
    return this.httpService.request(this.ROLE_TEMPLATE_ENDPOINT + uuid, {
      method: 'GET',
      addNamespace: false,
    });
  }

  getTemplateScopes() {
    return this.httpService
      .request('/ajax/v1/role-templates-scopes/', {
        method: 'GET',
        addNamespace: false,
      })
      .then(({ result }) => result);
  }

  createTemplate(body: any) {
    return this.httpService.request(this.ROLE_TEMPLATE_ENDPOINT, {
      method: 'POST',
      body,
      addNamespace: false,
    });
  }

  updateTemplate(body: any) {
    return this.httpService.request(this.ROLE_TEMPLATE_ENDPOINT + body.uuid, {
      method: 'PUT',
      body,
      addNamespace: false,
    });
  }

  deleteTemplate(uuid: string) {
    return this.httpService.request(this.ROLE_TEMPLATE_ENDPOINT + uuid, {
      method: 'DELETE',
      addNamespace: false,
    });
  }

  temmplateGenerate(uuid: string, params: any = { 'dry-run': true }) {
    return this.httpService.request(
      this.ROLE_TEMPLATE_ENDPOINT + uuid + '/generate/',
      {
        method: 'POST',
        params,
        addNamespace: false,
      },
    );
  }

  getRoleSchema() {
    return this.httpService
      .request(this.ROLE_SCHEMA_ENDPOINT, {
        method: 'GET',
        addNamespace: false,
      })
      .then(({ result }) => result);
  }

  getRoles(page_size: number = 20, page: number = 1, search: string = '') {
    return this.httpService.request(this.ROLES_ENDPOINT, {
      method: 'GET',
      params: { page_size, page, search },
      addNamespace: false,
      ignoreProject: true,
    });
  }

  getRole(role_name: string) {
    return this.httpService.request(this.ROLES_ENDPOINT + role_name, {
      method: 'GET',
      addNamespace: false,
    });
  }

  getUsers(
    role_name: string,
    search: string = '',
    page: number = 1,
    page_size: number = 20,
  ) {
    return this.httpService.request(
      this.ROLES_ENDPOINT + role_name + '/users',
      {
        method: 'GET',
        params: { page, page_size, search },
        addNamespace: false,
      },
    );
  }

  deleteUser(role_name: string, body: any) {
    return this.httpService.request(
      this.ROLES_ENDPOINT + role_name + '/users',
      {
        method: 'DELETE',
        body,
        addNamespace: false,
      },
    );
  }

  assignUser(role_name: string, body: any) {
    return this.httpService.request(
      this.ROLES_ENDPOINT + role_name + '/users',
      {
        method: 'POST',
        body,
        addNamespace: false,
        ignoreProject: true,
      },
    );
  }

  addParent(role_name: string, body: any) {
    return this.httpService.request(
      this.ROLES_ENDPOINT + role_name + '/parents',
      {
        method: 'POST',
        body,
        addNamespace: false,
      },
    );
  }

  deleteParent(role_name: string, uuid: string) {
    return this.httpService.request(
      this.ROLES_ENDPOINT + role_name + `/parents/${uuid}`,
      {
        method: 'DELETE',
        addNamespace: false,
      },
    );
  }

  createRole(body: any, params: any = {}) {
    return this.httpService.request(this.ROLES_ENDPOINT, {
      method: 'POST',
      body,
      params,
      addNamespace: false,
      ignoreProject: true,
    });
  }

  updateRole(body: any, role_name: string) {
    return this.httpService.request(this.ROLES_ENDPOINT + role_name, {
      method: 'PUT',
      body,
      addNamespace: false,
    });
  }

  // 获取添加用户时可以选择的角色类型
  getValidRoleTypes({
    project_name = '',
    cluster_names = '',
    k8s_namespace_name = '',
    level = 'project',
    ignoreProject = true,
  }): Promise<AccountType[]> {
    return this.httpService
      .request({
        url: `/ajax/v1/role-types/${this.account.namespace}`,
        method: 'GET',
        params: { project_name, cluster_names, k8s_namespace_name, level },
        addNamespace: false,
        ignoreProject,
      })
      .then(({ result }) => result);
  }
}

export interface RoleTemplateListItem {
  created_at: string;
  updated_at: string;
  description: string;
  display_name: string;
  display_name_en?: string;
  name: string;
  official: boolean;
  resource_actions: any[];
  uuid: string;
}

export interface RoleTemplateItem {
  created_at?: string;
  updated_at?: string;
  created_by?: string;
  description: string;
  display_name: string;
  display_name_en?: string;
  name: string;
  official?: boolean;
  resource_actions?: any[];
  uuid?: string;
  scope: object;
  permissions: any[];
}

export interface RoleItem {
  uuid: string;
  parents: [
    {
      uuid: string;
      name: string;
      assigned_at: string;
    }
  ];
  admin_role: false;
  name: string;
  permissions: any[];
  created_at: string;
  updated_at: string;
  resource_actions: any[];
}

export interface Permission {
  actions: string[];
  resource: string[];
  resource_type: string;
  uuid: string;
  template_uuid?: string;
  constraints?: any[];
}
