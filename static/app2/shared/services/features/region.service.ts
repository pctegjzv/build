import { Inject, Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as _ from 'lodash';
import { BehaviorSubject, Observable, combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  first,
  map,
  publishReplay,
  refCount,
  retry,
} from 'rxjs/operators';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { CloudNames } from 'app2/features/cluster/utils';
import { SourceStatsCount } from 'app2/shared/services/features/model.types';
import { HttpService } from 'app2/shared/services/http/http.service';
import { CookieService } from 'app2/shared/services/utility/cookie.service';
import { AppState } from 'app2/state-store/reducers';
import * as regionActions from 'app2/state-store/region/actions';
import * as fromRegion from 'app2/state-store/region/reducers';

export enum RegionStatus {
  Running = 'RUNNING',
  Deploying = 'DEPLOYING',
  Preparing = 'PREPARING',
  Warning = 'WARNING',
  Error = 'ERROR',
  Critical = 'CRITICAL',
  Stopped = 'STOPPED',
}

export enum ContainerManagers {
  None = 'NONE',
  Mesos = 'MESOS',
  Swarm = 'SWARM',
  Kubernetes = 'KUBERNETES',
}

export interface Region {
  name: string;
  id?: string;
  display_name?: string;
  platform_version?: string;
  attr?: any;
  features?: any;
  type?: 'CLAAS';
  container_manager?: ContainerManagers;
  created_at?: string;
  updated_at?: string;
  env_uuid?: string;
  state?: RegionStatus;
  namespace?: string;
  resource_actions?: string[];
  mirror?: Mirror;
}

export interface Mirror {
  name: string;
  id: string;
  uuid?: string;
  flag: string;
  display_name: string;
  regions: {
    name: string;
    display_name: string;
    id?: string;
    uuid?: string;
    quota?: any;
  }[];
}

export interface RegionStats {
  cpus_total: number;
  nodes_count: number;
  containers_count: number;
  pods_count?: number;
  cpus_allocated: number;
  mem_total: number;
  mem_allocated: number;
  gpus_total: number;
  gpus_allocated: number;
  services_count: number;
  mem_utilization: number;
  cpus_utilization: number;
  gpus_utilization: number;
}

export interface NodeStats {
  cpus_total: number;
  cpus_allocated: number;
  cpus_utilization: number;
  gpus_total: number;
  gpus_allocated: number;
  gpus_utilization: number;
  mem_total: number;
  mem_allocated: number;
  mem_utilization: number;
  uptime: number;
  max_disk_usage: number;
  containers_count: number;
  pods_count?: number;
  hostname: string;
  private_ip: string;
}

export interface RegionStatsHistory {
  [key: string]: {
    data: { timestamp: number; value: number }[];
    unit: string;
  };
}

export interface NodeLabel {
  editable: boolean;
  value: string;
  key: string;
}

export interface NodeResource {
  avail_disk: string;
  total_cpus: string;
  total_mem: string;
  containers_count: number;
  pods_count?: number;
}

export enum ComponentStatus {
  Running = 'RUNNING',
  Error = 'ERROR',
  Unknwon = 'UNKNOWN',
}

export interface NodeComponent {
  type: string;
  status: ComponentStatus;
}

export interface NodeAttr {
  comps_updated_at: string;
  components?: NodeComponent[];
  schedulable?: boolean;
  source: string;
}

export enum NodeState {
  Deploying = 'DEPLOYING',
  Running = 'RUNNING',
  Build = 'BUILD',
  Active = 'ACTIVE',
  Shutoff = 'SHUTOFF',
  ShuttingDown = 'SHUTTING_DOWN',
  Stopped = 'STOPPED',
  Stopping = 'STOPPING',
  Removed = 'REMOVED',
  Unknown = 'UNKNOWN',
  Offline = 'OFFLINE',
  Warning = 'WARNING',
  Error = 'ERROR',
  Critical = 'CRITICAL',
  Preparing = 'PREPARING',
  Draining = 'DRAINING',
}

export interface Node {
  id: string;
  instance_id: string;
  private_ip: string;
  state: NodeState;
  type: 'EMPTY' | 'SLAVE' | 'SYS' | 'SYSLAVE';
  labels: NodeLabel[];
  node_tag: string;
  resources: NodeResource;
  resource_actions: string[];
  attr: NodeAttr;
  updated_at: string;
  created_at: string;
}

export interface LogSource {
  id: string;
  name: string;
  type: string;
  namespace?: string;
  created_by?: string;
  version?: string;
  query_address?: string;
  integrated_address?: string;
}

export interface RegionLogSource {
  write_log_source: LogSource[];
  read_log_source: LogSource[];
}

export interface RegionComponent {
  status: ComponentStatus;
  type: string;
  hosts: string[];
}

export interface SupportedDockerVersion {
  default: string;
  versions: string[];
}

export interface RegionCreateData {
  name: string;
  display_name: string;
  over_commit: 1;
  container_manager: ContainerManagers;
  namespace: string;
  attr: {
    cloud: {
      name: CloudNames;
      [key: string]: any;
    };
    docker: {
      path: string;
      version: string;
    };
    nic: string;
  };
  features?: {
    service: {
      features: string[];
      [key: string]: any;
    };
    node: {
      features: string[];
      [key: string]: any;
    };
  };
}

export interface ClusterAccessData {
  display_name: string;
  name: string;
  namespace: string;
  attr: {
    cluster: {
      nic: string;
    };
    docker: {
      path: string;
      version: string;
    };
    cloud: {
      name: string;
    };
    kubernetes: {
      type: string;
      endpoint: string;
      token: string;
      version: string;
      cni: {
        type: string;
      };
    };
  };
  features: {
    logs: {
      type: 'third-party';
      storage: {
        read_log_source: string;
        write_log_source: string;
      };
    };
    'service-catalog': {
      type: 'official';
    };
  };
}

export interface Cluster {
  display_name: string;
  name: string;
  namespace: string;
  created_at: string;
  container_manager: string;
  updated_at: string;
  platform_version: string;
  state: RegionStatus;
  env_uuid: string;
  type: string;
  id: string;
  attr: {
    cluser: {
      nic: string;
    };
    docker: {
      path: string;
      version: string;
    };
    cloud: {
      name: 'PRIVATE' | 'VMWARE';
    };
    kubernetes: {
      type: 'openshift' | 'original';
      endpoint: string;
      token: string;
      version: string;
      cni: {
        type: string;
        cidr: string;
        network_policy: string;
      };
    };
  };
  features: {
    logs: {
      type: 'official';
      storage: {
        read_log_source: string;
        write_log_source: string;
      };
    };
    'service-catalog': {
      type: 'official';
    };
  };
  mirror?: {
    display_name: string;
    name: string;
    flag: string;
    regions: Array<{
      name: string;
      display_name: string;
      id: string;
    }>;
  };
  resource_actions?: string[];
}

export interface ClusterNode {
  status: {
    volumes_attached: boolean;
    daemon_endpoints: {
      kubelet_endpoint: {
        port: number;
      };
    };
    capacity: {
      'alpha.kubernetes.io/nvidia-gpu': string;
      pods: string;
      cpu: string;
      memory: string;
    };
    addresses: Array<{
      type: string;
      address: string;
    }>;
    allocatable: {
      'alpha.kubernetes.io/nvidia-gpu': string;
      pods: string;
      cpu: string;
      memory: string;
    };
    images: Array<{
      size_bytes: number;
      names: string[];
    }>;
    node_info: {
      kube_proxy_version: string;
      operating_system: string;
      kernel_version: string;
      system_uuid: string;
      container_runtime_version: string;
      os_image: string;
      architecture: string;
      boot_id: string;
      machine_id: string;
      kubelet_version: string;
    };
    volumes_in_use: boolean;
    phase: string;
    conditions: Array<{
      last_heartbeat_time: string;
      status: string;
      last_transition_time: string;
      reason:
        | 'KubeletHasSufficientDisk'
        | 'KubeletHasSufficientMemory'
        | 'KubeletHasNoDiskPressure'
        | 'KubeletReady';
      message: string;
      type: 'OutOfDisk' | 'MemoryPressure' | 'DiskPressure' | 'Ready';
    }>;
  };
  kind: string;
  spec: {
    provider_id: string;
    taints: Array<{
      key: string;
      time_added: boolean;
      effect: string;
      value: string;
    }>;
    config_source: string;
    unschedulable: boolean;
    pod_cidr: string;
    external_id: string;
  };
  api_version: string;
  metadata: {
    name: string;
    owner_references: string;
    generation: string;
    namespace: string;
    labels: {
      'node-role.kubernetes.io/master': string;
      'kubernetes.io/hostname': string;
      'beta.kubernetes.io/os': string;
      'beta.kubernetes.io/arch': string;
    };
    generate_name: null;
    deletion_timestamp: null;
    cluster_name: null;
    finalizers: null;
    deletion_grace_period_seconds: null;
    initializers: null;
    self_link: string;
    resource_version: string;
    creation_timestamp: string;
    annotations: {
      'node.alpha.kubernetes.io/ttl': string;
      'flannel.alpha.coreos.com/public-ip': string;
      'flannel.alpha.coreos.com/backend-data': string;
      'flannel.alpha.coreos.com/kube-subnet-manager': string;
      'flannel.alpha.coreos.com/backend-type': string;
      'volumes.kubernetes.io/controller-managed-attach-detach': string;
    };
    uid: string;
  };
}

export interface ClusterNodes {
  kind: 'NodeList';
  api_version: 'v1';
  metadata: {
    self_link: string;
    resource_version: string;
  };
  items: ClusterNode[];
}

export interface RegionFeature {
  config?: {
    type: 'official';
    application_uuid?: string;
    integration_uuid?: string;
  };
  new_version_avaliable: boolean;
  application_info?: {
    uuid: string;
    name: string;
    status: 'Running' | 'Warning' | 'Error' | 'Stopped' | 'Deploying';
  };
  integration_info?: {
    uuid: string;
    name: string;
    enabled: boolean;
  };
  template: {
    uuid: string;
    is_active: boolean;
    name: string;
    display_name: string;
    description: string;
    versions?: Array<{
      version_uuid: string;
      values_yaml_content: string;
    }>;
  };
}

@Injectable()
export class RegionService {
  private REGIONS_URL = '/ajax/v1/regions/' + this.account.namespace + '/';
  private REGIONS_V2_URL = '/ajax/v2/regions/' + this.account.namespace + '/';
  private regionNameSubject: BehaviorSubject<string>;
  region$: Observable<Region>;
  regions$: Observable<Region[]>;
  regionName$: Observable<string>;

  constructor(
    private store: Store<AppState>,
    private cookieService: CookieService,
    private httpService: HttpService,
    @Inject(ACCOUNT) private account: RcAccount,
  ) {
    this.regionNameSubject = new BehaviorSubject<string>(
      this.getRegionNameFromCookie(),
    );
    this.regions$ = this.store.select(fromRegion.selectList);
    this.regionName$ = this.regionNameSubject.asObservable();

    this.region$ = combineLatest(this.regionName$, this.regions$).pipe(
      filter(([_, regions]) => !!regions),
      map(([regionName, regions]) =>
        regions.find(region => region.name === regionName),
      ),
      distinctUntilChanged((region: Region, otherRegion: Region) =>
        _.isEqual(region, otherRegion),
      ),
      retry(10),
      publishReplay(1),
      refCount(),
    );
  }

  refetch() {
    this.store.dispatch(new regionActions.GetAll());
  }

  setRegionByName(regionName: string) {
    if (this.cookieService.getCookie('region') === regionName) {
      return;
    }
    this.cookieService.setCookie('region', regionName);
    this.regionNameSubject.next(regionName);
  }

  getCurrentRegion(): Promise<Region> {
    return this.region$
      .pipe(
        filter(region => !!region),
        first(),
      )
      .toPromise();
  }

  getRegionNameFromCookie(): string {
    return this.cookieService.getCookie('region');
  }

  getRegionStats(regionName: string): Promise<RegionStats> {
    return this.httpService.request(this.REGIONS_URL + regionName + '/stats/', {
      method: 'GET',
    });
  }

  getRegionNodesCount(regionName: string): Promise<SourceStatsCount> {
    return this.httpService.request(this.REGIONS_URL + regionName + '/nodes/', {
      method: 'GET',
      params: {
        display_count: 1,
      },
    });
  }

  getRegionNodes(regionName: string): Promise<Node[]> {
    return this.httpService
      .request(this.REGIONS_URL + regionName + '/nodes/', {
        method: 'GET',
      })
      .then(({ result }) => result);
  }

  getRegions(): Promise<Region[]> {
    return this.httpService
      .request(this.REGIONS_URL, {
        method: 'GET',
      })
      .then((resp: { result: Region[] }) => resp.result);
  }

  getRegionByName(name: string): Promise<Region> {
    return this.httpService.request(this.REGIONS_URL + name, {
      method: 'GET',
    });
  }

  getRegionLogSource(regionName: string): Promise<RegionLogSource> {
    return this.httpService.request(
      this.REGIONS_URL + regionName + '/log_source',
      {
        method: 'GET',
      },
    );
  }

  updateRegionLogSource(
    regionName: string,
    data: { write_log_source: string; read_log_source: string },
  ) {
    return this.httpService.request(
      this.REGIONS_URL + regionName + '/log_source',
      {
        method: 'PUT',
        body: data,
      },
    );
  }

  getRegionComponents(clusterName: string): Promise<RegionComponent[]> {
    return this.httpService
      .request(this.REGIONS_URL + clusterName + '/components', {
        method: 'GET',
      })
      .then(({ components }) => components);
  }

  getSupportedDockerVersions(): Promise<SupportedDockerVersion> {
    return this.httpService.request(
      this.REGIONS_URL + 'support-docker-version',
      {
        method: 'GET',
      },
    );
  }

  createRegion(data: RegionCreateData): Promise<Region> {
    return this.httpService.request(this.REGIONS_URL, {
      method: 'POST',
      body: data,
    });
  }

  updateRegion(region: string, data: any) {
    return this.httpService.request(this.REGIONS_URL + region, {
      method: 'PUT',
      body: data,
      addNamespace: false,
    });
  }

  deleteRegion(region: string) {
    return this.httpService.request(this.REGIONS_URL + region, {
      method: 'DELETE',
    });
  }

  getNodeStats(region: string, ip: string): Promise<NodeStats> {
    return this.httpService.request(
      this.REGIONS_URL + region + '/nodes/' + ip + '/stats',
      {
        method: 'GET',
      },
    );
  }

  getRegionNodeScripts(
    region: string,
    node_type: string = 'empty',
  ): Promise<{ commands: { install: string; uninstall: string } }> {
    return this.httpService.request(
      this.REGIONS_URL + region + '/node-scripts',
      {
        method: 'GET',
        params: {
          node_type,
        },
      },
    );
  }

  getRegionConfig(
    region: string,
  ): Promise<{
    default_scale_type: string;
    instance_types: string[];
    instanceTypes?: string[];
  }> {
    return this.httpService.request(this.REGIONS_URL + region + '/config', {
      method: 'GET',
    });
  }

  getRegionSSHKeyPairs(region: string): Promise<{ key_pairs: string[] }> {
    return this.httpService.request(
      this.REGIONS_URL + region + '/settings/key-pairs',
      {
        method: 'GET',
      },
    );
  }

  getRegionStatsHistory(
    region: string,
    params: {
      metrics_name?: string;
      start_time: number;
      end_time: number;
      period: number;
    },
  ): Promise<RegionStatsHistory> {
    return this.httpService.request(this.REGIONS_URL + region + '/stats', {
      method: 'GET',
      params: {
        mode: 'history',
        metrics_name: 'all',
        ...params,
      },
    });
  }

  getNodeStatsHistory(
    region: string,
    ip: string,
    params: {
      metrics_name?: string;
      node_type: string;
      start_time: number;
      end_time: number;
      period: number;
    },
  ) {
    return this.httpService.request(
      this.REGIONS_URL + region + '/nodes/' + ip + '/stats',
      {
        method: 'GET',
        params: {
          mode: 'history',
          metrics_name: 'all',
          ...params,
        },
      },
    );
  }

  getNode(region: string, ip: string): Promise<Node> {
    return this.httpService.request(
      this.REGIONS_URL + region + '/nodes/' + ip,
      {
        method: 'GET',
      },
    );
  }

  startNode(region: string, ip: string) {
    return this.httpService.request(
      this.REGIONS_URL + region + '/nodes/' + ip + '/start',
      {
        method: 'PUT',
      },
    );
  }

  stopNode(region: string, ip: string) {
    return this.httpService.request(
      this.REGIONS_URL + region + '/nodes/' + ip + '/stop',
      {
        method: 'PUT',
      },
    );
  }

  createNode(region: string, data: any) {
    return this.httpService.request(this.REGIONS_URL + region + '/nodes', {
      method: 'POST',
      body: data,
    });
  }

  deleteNode(region: string, privateIp: string) {
    return this.httpService.request(
      this.REGIONS_URL + region + '/nodes/' + privateIp,
      {
        method: 'DELETE',
      },
    );
  }

  actionNode(region: string, ip: string, action: string) {
    return this.httpService.request(
      this.REGIONS_URL + region + '/nodes/' + ip + '/actions',
      {
        method: 'PUT',
        body: {
          action,
        },
        addNamespace: false,
      },
    );
  }

  updateNodeType(region: string, ip: string, type: string) {
    return this.httpService.request(
      this.REGIONS_URL + region + '/nodes/' + ip + '/update_type',
      {
        method: 'PUT',
        body: { type },
      },
    );
  }

  updateNodeScaling(region: string, data: any) {
    return this.httpService.request(
      this.REGIONS_URL + region + '/settings/node-scaling',
      {
        method: 'PUT',
        body: data,
      },
    );
  }

  checkNodeLabel(region: string, data: { key: string; value: string }) {
    return this.httpService.request(
      this.REGIONS_URL + region + '/labels/check',
      {
        method: 'GET',
        params: data,
      },
    );
  }

  updateNodeLabel(region: string, ip: string, data: { [key: string]: string }) {
    return this.httpService.request(
      this.REGIONS_URL + region + '/nodes/' + ip + '/labels',
      {
        method: 'PUT',
        body: data,
        addNamespace: false,
      },
    );
  }

  getRegionLabels(regionName: string): Promise<any[]> {
    return this.httpService
      .request({
        method: 'GET',
        url: this.REGIONS_URL + regionName + '/labels',
        addNamespace: false,
      })
      .then(({ labels }) => labels)
      .catch(() => []);
  }

  getRegionLabelsV2(regionName: string): Promise<any> {
    return this.httpService
      .request({
        method: 'GET',
        url: this.REGIONS_V2_URL + regionName + '/labels',
        addNamespace: true,
      })
      .then(labels => labels)
      .catch(() => {});
  }

  checkAvailableNodes(labelsStr: string, region_name: string) {
    return this.httpService.request({
      method: 'GET',
      url: this.REGIONS_URL + region_name + '/node-selector',
      params: {
        labels: labelsStr,
      },
      addNamespace: false,
    });
  }

  regionCheck(params: { endpoint: string; token: string }) {
    return this.httpService.request<{
      version: string;
    }>({
      method: 'POST',
      url: this.REGIONS_V2_URL + 'version-check',
      body: params,
      addNamespace: false,
    });
  }

  accessCluster(params: ClusterAccessData) {
    return this.httpService.request<Cluster>({
      method: 'POST',
      url: this.REGIONS_V2_URL,
      body: params,
      addNamespace: false,
    });
  }

  getCluster(name: string) {
    return this.httpService.request<Cluster>({
      method: 'GET',
      url: this.REGIONS_V2_URL + name,
      addNamespace: false,
    });
  }

  getClusterNodes(name: string) {
    return this.httpService.request<ClusterNodes>({
      method: 'GET',
      url: this.REGIONS_V2_URL + name + '/nodes',
      addNamespace: false,
    });
  }

  deleteCluster(name: string) {
    return this.httpService.request(this.REGIONS_V2_URL + name, {
      method: 'DELETE',
    });
  }

  nodeScripts({
    regionName,
    nodeType = 'empty',
  }: {
    regionName: string;
    nodeType: string;
  }): Promise<{
    commands: {
      install: string;
    };
  }> {
    return this.httpService.request({
      method: 'GET',
      url: this.REGIONS_URL + regionName + '/node-scripts',
      params: {
        node_type: nodeType.toLowerCase(),
      },
    });
  }

  nodeCreate(body: {
    region_name: string;
    num: string;
    instance_type: string;
    ssh_key_name: string;
    node_type: string;
  }) {
    return this.httpService.request({
      method: 'POST',
      url: this.REGIONS_URL + body.region_name + '/nodes/',
      body,
    });
  }

  getRegionFeatures(regionName: string) {
    return this.httpService.request<{
      log: RegionFeature;
      metric: RegionFeature;
    }>({
      method: 'GET',
      url: this.REGIONS_V2_URL + regionName + '/features',
      addNamespace: false,
    });
  }

  editRegionFeature(
    regionName: string,
    featureName: 'log' | 'metric',
    feature: {
      config: {
        type: 'official' | 'prometheus';
        integration_uuid?: string;
        storage?: {
          read_log_source: 'default';
          write_log_source: 'default';
        };
      };
      values_yaml_content?: string;
    },
    isAddOrUpdate: boolean,
  ) {
    if (
      featureName === 'log' &&
      feature.config.type === 'official' &&
      !feature.config.storage
    ) {
      feature.config.storage = {
        read_log_source: 'default',
        write_log_source: 'default',
      };
    }
    return this.httpService.request<RegionFeature>({
      method: isAddOrUpdate ? 'POST' : 'PUT',
      url: this.REGIONS_V2_URL + regionName + '/features/' + featureName,
      body: feature,
      addNamespace: false,
    });
  }

  deleteRegionFeature(regionName: string, featureName: string) {
    return this.httpService.request({
      method: 'DELETE',
      url: this.REGIONS_V2_URL + regionName + '/features/' + featureName,
      addNamespace: false,
    });
  }
}
