import { Injectable } from '@angular/core';

import {
  ClusterNode,
  Region,
} from 'app2/shared/services/features/region.service';

export const ClusterStateColors = {
  Ready: 'green',
  NotReady: 'red',
  Warning: 'yellow',
};

@Injectable()
export class RegionUtilitiesService {
  isKubernetes(region: Region) {
    return region && region.container_manager === 'KUBERNETES';
  }

  isNewK8sRegion(region: Region) {
    return this.isKubernetes(region) && +region.platform_version.substr(1) > 2;
  }

  isNetworkPolicyRegion(region: Region) {
    // 新k8s集群且network_policy值不为空
    let networkPolicy;
    try {
      networkPolicy = region.features.kubernetes.cni.network_policy;
      if (networkPolicy === 'empty') {
        networkPolicy = '';
      }
    } catch (e) {
      networkPolicy = '';
    }
    return this.isNewK8sRegion(region) && !!networkPolicy;
  }

  getNodePrivateIp(node: ClusterNode) {
    return node.status.addresses.find(addr => addr.type === 'InternalIP')
      .address;
  }

  getNodeState(node: ClusterNode) {
    let ready: boolean;
    let warning: boolean;

    node.status.conditions.forEach(condition => {
      const status = condition.status.toLowerCase();
      if (condition.type === 'Ready') {
        ready = status === 'true';
      } else if (!warning) {
        warning = status === 'true';
      }
    });

    return warning ? 'Warning' : ready ? 'Ready' : 'NotReady';
  }

  getNodeStateColor(node: ClusterNode) {
    return ClusterStateColors[this.getNodeState(node)] || 'red';
  }
}
