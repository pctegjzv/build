import { Inject, Injectable } from '@angular/core';
import * as moment from 'moment';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { QuerySection } from 'app2/features/dashboard/charts/chart.types';
import {
  HistoryCountResult,
  HistoryStatsCount,
} from 'app2/shared/services/features/model.types';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { HttpService } from 'app2/shared/services/http/http.service';
import { TranslateService } from 'app2/translate/translate.service';

import { NotificationService } from 'alauda-ui';

export interface QuotaTypes {
  unitLabel?: string;
  created_at?: string;
  hard?: number;
  name?: string;
  updated_at?: string;
  used?: number;
  uuid?: string;
  _id?: number;
  currentValue?: string;
}

export interface UnitLabels {
  name?: string;
}

export interface QuotaConfig {
  name?: string;
  unit?: string;
  unitLabel?: string;
}

export interface QuotaConfigMaped {
  name?: string;
  label?: string;
  unitLabel?: string;
}

export interface Space {
  created_at?: string;
  created_by?: string;
  description?: string;
  name?: string;
  quotas?: QuotaTypes[];
  resource_actions?: string[];
  status?: string;
  updated_at?: string;
  uuid?: string;
}

export interface QuotaSpacesResult {
  total_items: number;
  total_pages: number;
  result: [Space];
}

export interface QuotaResourceType {
  created_at: string;
  name: string;
  type: string;
  uuid: string;
}

export interface ResourceType {
  name: string;
  count: number;
}

export interface QuotaCharges {
  app_name: string;
  app_uuid: string;
  created_at: string;
  name: string;
  request: number;
  updated_at: string;
  uuid: string;
  normalizedWidth: string;
  displayName: string;
}

export interface ServiceRegion {
  id: string;
  name: string;
}

export interface ServiceData {
  app_name: string;
  count: number;
  cpu: string;
  date: string;
  instance_size: string;
  mem: number;
  region_id: string;
  region_name: string;
  service_id: string;
  service_name: string;
}

export interface ServiceItem {
  data: ServiceData[];
  region: ServiceRegion;
}

export interface Service {
  service: ServiceItem[];
}

export interface ServiceUsage {
  month: string;
  resources: Service;
}

@Injectable()
export class ResourceService {
  SPACES_URL: string;
  SPACE_URL: string;
  USAGE_ENDPOINT: string;

  constructor(
    private translate: TranslateService,
    private httpService: HttpService,
    private auiNotificationService: NotificationService,
    private roleUtilities: RoleUtilitiesService,
    @Inject(ACCOUNT) account: RcAccount,
  ) {
    const namespace = account.namespace;
    this.SPACES_URL = `/ajax/spaces/${namespace}/`;
    this.SPACE_URL = `${this.SPACES_URL}space/`;
    this.USAGE_ENDPOINT = `/ajax/v1/usage/${namespace}`;
  }

  getEventsCount({
    start_time,
    end_time,
  }: QuerySection): Promise<HistoryStatsCount> {
    return this.httpService
      .request(this.SPACES_URL, {
        method: 'GET',
        params: {
          display_count: 1,
          start_time,
          end_time,
        },
      })
      .then(({ count, unit, count_result }) => {
        return {
          count,
          unit,
          count_result: count_result.map((item: HistoryCountResult) => {
            return {
              OK: item.OK,
              ERROR: item.ERROR,
              OTHER: item.OTHER,
              start: parseFloat(moment.utc(item.start).format('X')),
              end: parseFloat(moment.utc(item.end).format('X')),
            };
          }),
        };
      });
  }

  getSpaces(action: string): Promise<QuotaSpacesResult> {
    return this.httpService.request(this.SPACES_URL, {
      method: 'GET',
      params: {
        action,
      },
    });
  }

  getConsumableSpaces() {
    return this.getSpaces('consume').then((list: any) => {
      return list.result.filter((item: any) => {
        return this.roleUtilities.resourceHasPermission(
          item,
          'space',
          'consume',
        );
      });
    });
  }

  getSpace(name: string): Promise<Space> {
    return this.httpService.request(this.SPACE_URL + name);
  }

  getQuotaConfig(): Promise<QuotaConfig[]> {
    return this.httpService
      .request(this.SPACES_URL + 'config', {
        method: 'GET',
        cache: true,
      })
      .then(({ configs }) => configs);
  }

  createSpace(payload: Space) {
    return this.httpService
      .request({
        method: 'POST',
        url: this.SPACES_URL,
        body: payload,
      })
      .then(res => {
        this.auiNotificationService.success(
          this.translate.get('quota_space_create_success'),
        );
        return res;
      });
  }

  updateSpace(payload: Space) {
    return this.httpService
      .request({
        method: 'PUT',
        url: this.SPACE_URL + payload.name,
        body: payload,
      })
      .then(res => {
        this.auiNotificationService.success(
          this.translate.get('quota_space_update_success'),
        );
        return res;
      });
  }

  deleteSpace(name: String) {
    return this.httpService
      .request({
        method: 'DELETE',
        url: this.SPACE_URL + name,
      })
      .then(res => {
        this.auiNotificationService.success(
          this.translate.get('quota_space_delete_success'),
        );
        return res;
      });
  }

  getSpaceResources(name: String): Promise<QuotaResourceType[]> {
    return this.httpService
      .request({
        method: 'GET',
        url: this.SPACE_URL + name + '/resources',
      })
      .then(({ result }) => result);
  }

  getQuotaCharges(
    spaceName: string,
    quotaName: string,
  ): Promise<QuotaCharges[]> {
    return this.httpService
      .request({
        method: 'GET',
        url: this.SPACE_URL + spaceName + '/quota/' + quotaName,
      })
      .then(({ result }) => result);
  }

  getServiceUsageData(month: string): Promise<ServiceUsage[]> {
    return this.httpService
      .request({
        method: 'GET',
        addNamespace: false,
        url: this.USAGE_ENDPOINT,
        params: {
          month: month,
          resource: 'service', // hard-coded for now
        },
      })
      .then(({ usage }) => usage);
  }

  getServiceUsageExportData(month: string): Promise<Blob> {
    return this.httpService.request({
      method: 'GET',
      addNamespace: false,
      url: this.USAGE_ENDPOINT + '/file-data',
      params: {
        month: month,
        resource: 'service', // hard-coded for now
      },
      responseType: 'blob',
    });
  }
}
