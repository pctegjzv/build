import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { AccountService } from 'app2/shared/services/features/account.service';
import { Cluster } from 'app2/shared/services/features/region.service';
import { HttpService } from 'app2/shared/services/http/http.service';
import { Resource, ResourceList } from 'app_user/core/types';
import { FetchParams } from 'app_user/shared/abstract/base-resource-list.component';
import { concat, includes, sortBy, uniqBy } from 'lodash';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

export interface ResourceType {
  kind?: string;
  name?: string;
  namespaced?: boolean;
  verbs?: string[];
}

export interface ResourceManagementListFetchParams extends FetchParams {
  currentResourceType: ResourceType;
}

export interface Category {
  namespaced: ResourceType[];
  region: ResourceType[];
}

export interface GenericRSRC extends Resource {
  kubernetes?: {
    data?: any;
    kind?: string;
    metadata: GenericRSRCMetaData;
  };
  resource_actions?: string[];
}

export interface GenericRSRCMetaData {
  annotations?: {
    [key: string]: string;
  };
  creationTimestamp?: string;
  name?: string;
  namespace?: string;
  resourceVersion?: string;
  selfLink?: string;
  uid: string;
}

@Injectable()
export class RSRCManagementService {
  currentResourceType$: Subject<ResourceType> = new BehaviorSubject<
    ResourceType
  >({ kind: '', name: '' });
  fetchParams$: Observable<ResourceManagementListFetchParams>;
  category$: Subject<Category> = new BehaviorSubject<Category>({
    namespaced: [],
    region: [],
  });
  currentResouceData$: Subject<GenericRSRC> = new BehaviorSubject<GenericRSRC>(
    null,
  );

  clusterName: string;
  projectName: string;
  namespaceName: string;

  intialized = false;
  navLoading: boolean;

  constructor(
    private httpService: HttpService,
    private accountService: AccountService,
  ) {}

  public isUserView(): boolean {
    return this.accountService.isUserView();
  }

  public initScope(params: any) {
    return this.isUserView()
      ? this.initUserViewScope(params)
      : this.initAdminViewScope(params);
  }

  private initAdminViewScope(cluster: Cluster) {
    this.clusterName = cluster.name;
  }

  private initUserViewScope(scope: Params) {
    this.projectName = scope.project;
    this.clusterName = scope.cluster;
    this.namespaceName = scope.namespace;
  }

  public initResourceTypeList$() {
    this.navLoading = true;
    return this.httpService
      .request(`/ajax/v2/misc/clusters/${this.clusterName}/resourcetypes`, {
        method: 'GET',
      })
      .then((res: any) => {
        this.navLoading = false;
        const rawCategory = res.result.reduce(this.reducer, {
          namespaced: [],
          region: [],
        });
        const sortedCategory = this.sortCategory(rawCategory);
        if (!this.intialized) {
          this.setCurrentResourceType(sortedCategory.namespaced[0]);
          this.intialized = true;
        }
        this.category$.next(sortedCategory);
      });
  }

  private reducer(accum: { namespaced: any[]; region: any[] }, curr: any) {
    const allCategories = curr.resources.filter((item: any) => {
      return !includes(item.name, '/');
    });
    const namespacedKinds = allCategories
      .filter(
        (item: ResourceType) => item.namespaced && includes(item.verbs, 'get'),
      )
      .map((item: ResourceType) => {
        return {
          kind: item.kind,
          name: item.name,
          namespaced: item.namespaced,
          verbs: item.verbs,
        };
      });
    const regionKinds = allCategories
      .filter(
        (item: ResourceType) => !item.namespaced && includes(item.verbs, 'get'),
      )
      .map((item: ResourceType) => {
        return {
          kind: item.kind,
          name: item.name,
          namespaced: item.namespaced,
          verbs: item.verbs,
        };
      });
    return {
      namespaced: uniqBy(concat(accum.namespaced, namespacedKinds), 'kind'),
      region: uniqBy(concat(accum.region, regionKinds), 'kind'),
    };
  }

  /**
   * TODO: 资源类别排序，暂用首字母默认排序
   * @param rawCategory
   */
  private sortCategory(rawCategory: any) {
    return {
      namespaced: sortBy(rawCategory.namespaced, [
        (o: { kind: string; name: string }) => o.kind,
      ]),
      region: sortBy(rawCategory.region, [
        (o: { kind: string; name: string }) => o.kind,
      ]),
    };
  }

  public setCurrentResourceType(newType: ResourceType): void {
    this.currentResourceType$.next(newType);
  }

  public setCurrentResourceData(data: GenericRSRC | null) {
    this.currentResouceData$.next(data);
  }

  public getResourceList(
    params: ResourceManagementListFetchParams,
  ): Promise<ResourceList> {
    return this.httpService.request(
      `/ajax/v2/kubernetes/clusters/${this.clusterName}/${
        params.currentResourceType.name
      }${this.namespaceName ? `/${this.namespaceName}/` : ''}`,
      {
        method: 'GET',
        params: {
          name: params.search,
          page: params.pageParams.pageIndex,
          page_size: params.pageParams.pageSize,
        },
        addNamespace: false,
      },
    );
  }

  public updateResource(params: {
    resourceTypeName: string;
    resourceName: string;
    data: GenericRSRC['kubernetes'];
    namespace: string;
  }): Promise<any> {
    const customUrl = params.namespace
      ? `/${params.namespace}/${params.resourceName}`
      : `/${params.resourceName}`;
    return this.httpService.request(
      `/ajax/v2/kubernetes/clusters/${this.clusterName}/${
        params.resourceTypeName
      }${customUrl}`,
      {
        method: 'PUT',
        body: params.data,
        addNamespace: false,
      },
    );
  }

  public createResource(params: { data: any[] }): Promise<any> {
    return this.httpService.request(
      `/ajax/v2/kubernetes/clusters/${this.clusterName}/resources`,
      {
        method: 'POST',
        body: params.data,
        addNamespace: false,
      },
    );
  }

  public deleteResouce(params: {
    resourceTypeName: string;
    resourceName: string;
    namespace: string;
  }) {
    const customUrl = params.namespace
      ? `/${params.namespace}/${params.resourceName}`
      : `/${params.resourceName}`;
    return this.httpService.request(
      `/ajax/v2/kubernetes/clusters/${this.clusterName}/${
        params.resourceTypeName
      }${customUrl}`,
      {
        method: 'DELETE',
        addNamespace: false,
      },
    );
  }

  /**
   * Actions permission
   */
  hasPermission(resource_actions: string[], action: string): boolean {
    return resource_actions.some(permission =>
      includes(permission, `:${action}`),
    );
  }
}
