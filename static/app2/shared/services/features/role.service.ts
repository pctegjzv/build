import { Inject, Injectable } from '@angular/core';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount, ResourceType } from 'app2/core/types';

import { HttpService } from '../http/http.service';

@Injectable()
export class RoleService {
  ENDPOINT_ROLE = '/ajax/permissions/' + this.account.namespace + '/';
  ROLES_NAMESPACE_PREFIX = `/ajax/roles/${this.account.namespace}/`;

  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) public account: RcAccount,
  ) {}

  getContextPermissions(
    resource_type: ResourceType,
    context?: any,
  ): Promise<string[]> {
    return this.httpService
      .request<{ result: string[] }>(this.ENDPOINT_ROLE + resource_type, {
        method: 'GET',
        params: context,
        addNamespace: false,
        cache: false,
        ignoreProject: !!(context && context.project_name),
      })
      .then(data => data.result || [])
      .catch(() => []);
  }

  getPluralContextPermissions(
    resource_types: ResourceType[],
  ): Promise<{ [resourceType: string]: string[] }> {
    return this.httpService.request<{ [resourceType: string]: string[] }>(
      this.ENDPOINT_ROLE,
      {
        method: 'POST',
        body: resource_types,
        cache: false,
      },
    );
  }

  getRoleList({
    search = '',
    page = 1,
    page_size = 20,
    action = 'view',
    project_name = '',
    ignoreProject = false,
  }) {
    return this.httpService.request(this.ROLES_NAMESPACE_PREFIX, {
      method: 'GET',
      params: {
        search,
        page,
        page_size,
        order_by: 'created_at',
        action,
        project_name,
      },
      ignoreProject,
    });
  }

  createRole(body: any) {
    return this.httpService.request(this.ROLES_NAMESPACE_PREFIX, {
      method: 'POST',
      body,
    });
  }

  getRole(role_name: string) {
    return this.httpService.request(
      `${this.ROLES_NAMESPACE_PREFIX}${role_name}/`,
      {
        method: 'GET',
      },
    );
  }

  deleteRole(role_name: string) {
    return this.httpService.request(
      `${this.ROLES_NAMESPACE_PREFIX}${role_name}/`,
      {
        method: 'DELETE',
      },
    );
  }

  addPermissionOfRole(role_name: string, body: any) {
    return this.httpService.request(
      `${this.ROLES_NAMESPACE_PREFIX}${role_name}/permissions/`,
      {
        method: 'POST',
        body,
      },
    );
  }

  updatePermissionOfRole(role_name: string, permission_uuid: any, body: any) {
    return this.httpService.request(
      `${
        this.ROLES_NAMESPACE_PREFIX
      }${role_name}/permissions/${permission_uuid}`,
      {
        method: 'PUT',
        body,
      },
    );
  }

  deletePermissionOfRole(role_name: string, permission_uuid: any) {
    return this.httpService.request(
      `${
        this.ROLES_NAMESPACE_PREFIX
      }${role_name}/permissions/${permission_uuid}`,
      {
        method: 'DELETE',
      },
    );
  }

  addParent(role_name: string, body: any) {
    return this.httpService.request(
      `${this.ROLES_NAMESPACE_PREFIX}${role_name}/parents/`,
      {
        method: 'POST',
        body,
      },
    );
  }

  deleteParent(role_name: string, parent_uuid: any) {
    return this.httpService.request(
      `${this.ROLES_NAMESPACE_PREFIX}${role_name}/parents/${parent_uuid}`,
      {
        method: 'DELETE',
      },
    );
  }

  getRoleSchemaList() {
    return this.httpService
      .request<{ result: any[] }>('/ajax/role-schema/', {
        method: 'GET',
      })
      .then(data => data.result || [])
      .catch(() => {
        return [];
      });
  }

  getRoleSchema(resource_type = '') {
    return this.httpService.request({
      method: 'GET',
      url: `/ajax/role-schema/${resource_type}`,
    });
  }

  listRoleUsers({ role_name = '', page = 1, page_size = 20, search = '' }) {
    return this.httpService.request(
      `${this.ROLES_NAMESPACE_PREFIX}${role_name}/users/`,
      {
        method: 'GET',
        params: {
          page,
          page_size,
          search,
        },
      },
    );
  }

  addRoleUsers(role_name: string, users: any) {
    return this.httpService.request(
      `${this.ROLES_NAMESPACE_PREFIX}${role_name}/users/`,
      {
        method: 'POST',
        body: users,
        addNamespace: false,
      },
    );
  }

  deleteRoleUsers(role_name: string, users: any) {
    return this.httpService.request(
      `${this.ROLES_NAMESPACE_PREFIX}${role_name}/users/`,
      {
        method: 'DELETE',
        // headers: {
        //   'Content-Type': 'Application/json',
        // },
        body: users,
        addNamespace: false,
      },
    );
  }

  createRoleUser(role_name: string, body: any) {
    return this.httpService.request(
      `${this.ROLES_NAMESPACE_PREFIX}${role_name}/users/`,
      {
        // headers: {
        //   'Content-Type': 'Application/json',
        // },
        method: 'POST',
        body,
      },
    );
  }

  deleteRoleUser(role_name: string, body: any) {
    return this.httpService.request(
      `${this.ROLES_NAMESPACE_PREFIX}${role_name}/users/`,
      {
        method: 'DELETE',
        body,
      },
    );
  }
}
