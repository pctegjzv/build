import { Inject, Injectable } from '@angular/core';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';

import { HttpService } from 'app2/shared/services/http/http.service';
import { FetchDataResult } from 'app2/shared/services/utility/pagination-data';

export interface ServiceBroker {
  resource_actions?: string[];
  kubernetes: {
    kind?: string;
    spec: {
      CABundle?: string;
      authInfo?: {
        basicAuthUsernameKey: string;
        basicAuthPasswordKey: string;
      };
      url?: string;
    };
    apiVersion: string;
    metadata: {
      creationTimestamp: string;
      name: string;
      resourceVersion?: string;
      annotations: {
        [key: string]: string;
      };
    };
    description?: string;
    status: {
      asyncOpInProgress?: boolean;
      conditions: any[];
      deprovisionStatus?: string;
      externalProperties?: {
        clusterServicePlanExternalName?: string;
      };
      orphanMitigationInProgress?: boolean;
      reconciledGeneration?: number;
    };
  };
}

export interface ServiceBrokerListItem {
  name?: string;
  display_name?: string;
  status?: string;
}

export interface ServiceClass {
  status?: string;
  resource_actions?: string[];
  clusterserviceplans: ServicePlan[];
  kubernetes: {
    status?: {
      removedFromBrokerCatalog?: boolean;
    };
    kind?: string;
    spec?: {
      clusterServiceBrokerName?: string;
      description?: string;
      binding_retrievable?: boolean;
      tags?: string[];
      planUpdatable?: boolean;
      externalName?: string;
      externalID?: string;
      bindable?: boolean;
      externalMetadata?: {
        documentationUrl?: string;
        imageUrl?: string;
        shareable?: boolean;
        displayName?: string;
        providerDisplayName?: string;
      };
    };
    apiVersion?: string;
    metadata?: {
      name?: string;
      resourceVersion?: string;
      creationTimestamp?: string;
      annotations?: {
        [key: string]: string;
      };
      selfLink?: string;
      uid?: string;
    };
  };
}

export interface ServicePlan {
  status?: {
    removedFromBrokerCatalog: boolean;
  };
  spec: {
    clusterServiceBrokerName: string;
    instanceCreateParameterSchema: {
      $id?: string;
      required?: string[];
      definitions?: {};
      $schema?: string;
      type?: string;
      properties: {
        [propName: string]: any;
      };
    };
    description?: string;
    serviceBindingCreateParameterSchema: {
      $id?: string;
      required?: string[];
      definitions?: {};
      $schema?: string;
      type?: string;
      properties: {
        [propName: string]: any;
      };
    };
    free?: boolean;
    externalName?: string;
    clusterServiceClassRef?: {
      name?: string;
    };
    externalID?: string;
    externalMetadata?: {
      displayName?: string;
      bullets?: string[];
    };
  };
  metadata?: {
    resourceVersion?: string;
    creationTimestamp?: string;
    name?: string;
    selfLink?: string;
    uid?: string;
  };
}

export interface ServiceBinding {
  resource_actions: string[];
  kubernetes: {
    metadata: {
      annotations: {
        [propName: string]: any;
      };
      creationTimestamp: string;
      finalizers?: string[];
      generation?: number;
      name: string;
      namespace: string;
      resourceVersion?: string;
      selfLink?: string;
      uid?: string;
    };
    spec: {
      externalID?: string;
      instanceRef: {
        name: string;
      };
      parameters: {
        [propName: string]: any;
      };
      secretName?: string;
    };
    status: {
      asyncOpInProgress?: boolean;
      conditions: any[];
      externalProperties?: {
        [propName: string]: any;
      };
      orphanMitigationInProgress?: boolean;
      reconciledGeneration?: number;
      unbindStatus?: string;
    };
  };
  credential?: {
    name?: string;
    password?: string;
  };
  service: {
    name: string;
    uuid: string;
    status: string;
    namespace: string;
  };
}

export interface ServiceBrokerPayload {
  spec: {
    CABundle: string;
    authInfo: {
      basicAuthUsernameKey?: string;
      basicAuthPasswordKey?: string;
    };
    url: string;
  };
  metadata: {
    name: string;
    resourceVersion?: string;
    annotations: {
      [key: string]: string;
    };
  };
}

export interface ServiceInstancePayload {
  spec: {
    clusterServiceClassExternalName: string;
    clusterServicePlanExternalName: string;
    externalID?: string;
    parameters: {
      [propName: string]: any;
    };
  };
  metadata: {
    name: string;
    namespace: string;
    resourceVersion?: string;
  };
}

export interface ServiceBindingPayload {
  spec: {
    instanceRef: {
      name: string;
    };
    parameters: {
      [propName: string]: any;
    };
  };
  metadata: {
    namespace: string;
    labels: {
      [key: string]: string;
    };
  };
}

@Injectable()
export class ServiceCatalogService {
  constructor(
    @Inject(ENVIRONMENTS) private environments: Environments,
    private httpService: HttpService,
  ) {}

  getEndpointBase() {
    return `/ajax/v2/kubernetes/clusters/`;
  }

  getK8sServiceBrokerEndpointBase(cluster_id: string) {
    return `${this.getEndpointBase()}${cluster_id}/clusterservicebrokers/`;
  }

  getK8sServiceClassEndpointBase(cluster_id: string) {
    return `${this.getEndpointBase()}${cluster_id}/clusterserviceclasses/`;
  }

  getK8sServiceInstanceEndpointBase(cluster_id: string) {
    return `${this.getEndpointBase()}${cluster_id}/serviceinstances/`;
  }

  getK8sServiceBindingEndpointBase(cluster_id: string) {
    return `${this.getEndpointBase()}${cluster_id}/servicebindings/`;
  }

  createBroker(cluster_id: string, body: ServiceBrokerPayload) {
    return this.httpService.request(
      this.getK8sServiceBrokerEndpointBase(cluster_id),
      {
        method: 'POST',
        body,
        addNamespace: false,
      },
    );
  }

  getBrokers({
    pageNo = 1,
    pageSize = 20,
    params: { cluster_id = '', name = '' },
  }) {
    return this.httpService
      .request(this.getK8sServiceBrokerEndpointBase(cluster_id), {
        method: 'GET',
        params: {
          name,
          page: pageNo,
          page_size: pageSize,
        },
        addNamespace: false,
      })
      .then((res: FetchDataResult<any>) => res);
  }

  getBroker(cluster_id: string, name: string): Promise<ServiceBroker> {
    return this.httpService.request(
      this.getK8sServiceBrokerEndpointBase(cluster_id) + name,
      {
        method: 'GET',
      },
    );
  }

  deleteBroker(cluster_id: string, name: string) {
    return this.httpService.request(
      this.getK8sServiceBrokerEndpointBase(cluster_id) + name,
      {
        method: 'DELETE',
      },
    );
  }

  updateBroker(
    payload: ServiceBrokerPayload,
    cluster_id: string,
    name: string,
  ) {
    return this.httpService.request(
      this.getK8sServiceBrokerEndpointBase(cluster_id) + name,
      {
        method: 'PUT',
        body: payload,
        addNamespace: false,
      },
    );
  }

  getClasses({
    pageNo = 1,
    pageSize = 20,
    params: { cluster_id = '', broker_name = '', name = '' },
  }) {
    return this.httpService
      .request(this.getK8sServiceClassEndpointBase(cluster_id), {
        method: 'GET',
        params: {
          name,
          broker_name,
          page: pageNo,
          page_size: pageSize,
        },
        addNamespace: false,
      })
      .then((res: FetchDataResult<any>) => res);
  }

  getBrokerClasses({
    pageNo = 1,
    pageSize = 20,
    params: { cluster_id = '', fieldSelector = '', name = '' },
  }) {
    return this.httpService.request(
      this.getK8sServiceClassEndpointBase(cluster_id),
      {
        method: 'GET',
        params: {
          name,
          fieldSelector,
          page: pageNo,
          page_size: pageSize,
        },
        addNamespace: false,
      },
    );
  }

  getClass(cluster_id: string, name: string): Promise<ServiceClass> {
    return this.httpService.request(
      this.getK8sServiceClassEndpointBase(cluster_id) + name,
      {
        method: 'GET',
      },
    );
  }

  createInstance(body: any, cluster_id: string) {
    return this.httpService.request(
      this.getK8sServiceInstanceEndpointBase(cluster_id),
      {
        method: 'POST',
        body,
        addNamespace: false,
      },
    );
  }

  getInstances({
    pageNo = 1,
    pageSize = 20,
    params: { cluster_id = '', namespace = '', name = '' },
  }) {
    return this.httpService
      .request(
        namespace
          ? `${this.getK8sServiceInstanceEndpointBase(cluster_id)}${namespace}/`
          : this.getK8sServiceInstanceEndpointBase(cluster_id),
        {
          method: 'GET',
          params: {
            name,
            page: pageNo,
            page_size: pageSize,
          },
          addNamespace: false,
        },
      )
      .then((res: FetchDataResult<any>) => res);
  }

  getInstance(cluster_id: string, name: string, namespace: string) {
    return this.httpService.request(
      this.getK8sServiceInstanceEndpointBase(cluster_id) +
        `${namespace}/${name}`,
      {
        method: 'GET',
      },
    );
  }

  deleteInstance(cluster_id: string, name: string, namespace: string) {
    return this.httpService.request(
      this.getK8sServiceInstanceEndpointBase(cluster_id) +
        `${namespace}/${name}`,
      {
        method: 'DELETE',
      },
    );
  }

  updateInstance(
    payload: any,
    cluster_id: string,
    name: string,
    namespace: string,
  ) {
    return this.httpService.request(
      this.getK8sServiceInstanceEndpointBase(cluster_id) +
        `${namespace}/${name}`,
      {
        method: 'PUT',
        body: payload,
        addNamespace: false,
      },
    );
  }

  createBinding(body: any, cluster_id: string) {
    return this.httpService.request(
      this.getK8sServiceBindingEndpointBase(cluster_id),
      {
        method: 'POST',
        body,
        addNamespace: false,
      },
    );
  }

  getBindings({
    pageNo = 1,
    pageSize = 20,
    params: {
      name = '',
      cluster = '',
      namespace = '',
      instance_name = '',
      service_uuid = '',
    },
  }) {
    return this.httpService
      .request(
        `${this.getK8sServiceBindingEndpointBase(cluster)}${namespace}/`,
        {
          method: 'GET',
          params: {
            name,
            instance_name,
            labelSelector: service_uuid
              ? `service.${
                  this.environments.label_base_domain
                }/uuid=${service_uuid}`
              : '',
            fieldSelector: `metadata.namespace=${namespace}`,
            page: pageNo,
            page_size: pageSize,
          },
          addNamespace: false,
        },
      )
      .then((res: FetchDataResult<any>) => res);
  }

  unbind(cluster_id: string, name: string, namespace: string) {
    return this.httpService.request(
      this.getK8sServiceBindingEndpointBase(cluster_id) +
        `${namespace}/${name}`,
      {
        method: 'DELETE',
      },
    );
  }

  handleJSONSchemaType(type: string) {
    if (type === 'string') {
      return 'text';
    } else if (type === 'integer') {
      return 'number';
    }
    return type;
  }
}
