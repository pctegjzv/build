import { Inject, Injectable } from '@angular/core';
import * as jsyaml from 'js-yaml';
import { cloneDeep, find, flatten, get, has, map, set, unset } from 'lodash';

import { NotificationService } from 'alauda-ui';
import { ACCOUNT, ENVIRONMENTS } from 'app2/core/tokens';
import { Environments, RcAccount } from 'app2/core/types';
import {
  ContainerEnvvar,
  ContainerViewModel,
  ContainerVolumeViewModel,
  K8sComponent,
  K8sComponentInfo,
  NetworkModeViewModel,
  RcImageSelection,
  ServicePort,
  ServiceViewModel,
} from 'app2/features/service/service.type';
import { ImageRegistryService } from 'app2/shared/services/features/image-registry.service';
import { SourceStatsCount } from 'app2/shared/services/features/model.types';
import { HttpService } from 'app2/shared/services/http/http.service';
import { LoggerUtilitiesService } from 'app2/shared/services/utility/logger.service';
import { FetchDataResult } from 'app2/shared/services/utility/pagination-data';
import { uniqBy } from 'lodash';
@Injectable()
export class ServiceService {
  SERVICES_ENDPOINT: string;
  K8S_SERVICES_ENDPOINT: string;
  LABEL_BASE_DOMAIN: string;

  constructor(
    private httpService: HttpService,
    private auiNotificationService: NotificationService,
    private logger: LoggerUtilitiesService,
    private registryService: ImageRegistryService,
    @Inject(ACCOUNT) private account: RcAccount,
    @Inject(ENVIRONMENTS) private environments: Environments,
  ) {
    const namespace = this.account.namespace;
    this.SERVICES_ENDPOINT = `/ajax/v1/services/${namespace}/`;
    this.K8S_SERVICES_ENDPOINT = '/ajax/v2/services/';
    this.LABEL_BASE_DOMAIN = this.environments.label_base_domain;
  }

  getRegionServices(params: {
    region_name?: string;
    basic?: boolean;
    region_platform_version?: string; // region platform_version
  }): Promise<K8sComponentInfo[]> {
    return params.region_platform_version &&
      +params.region_platform_version.substr(1) > 2
      ? this.getK8sServices({
          pageSize: 100,
          params: { cluster: params.region_name },
        }).then(({ results }) =>
          results.map(({ resource: { name, uuid }, namespace }) => ({
            service_name: name,
            uuid,
            ports: [],
            service_namespace: namespace.name,
          })),
        )
      : this.getV1Services({
          pageSize: 100,
          region_name: params.region_name,
          basic: params.basic,
        }).then((res: FetchDataResult<any>) => res.results);
  }

  getServicesCount(region_name: string): Promise<SourceStatsCount> {
    return this.httpService
      .request(this.SERVICES_ENDPOINT, {
        method: 'GET',
        params: {
          display_count: 1,
          region_name,
        },
      })
      .then((res: SourceStatsCount) => {
        if (!res.count_result) {
          throw new Error('no services');
        }
        return res;
      });
  }

  /**
   * TODO: 这个方法需要废弃了，对k8s并不管用
   * @param {{service_name: string}} params
   */
  serviceLogContext(params: { service_name: string }): Promise<any> {
    return this.httpService.request(
      this.SERVICES_ENDPOINT + `/${params.service_name}/logs/context`,
      {
        method: 'GET',
        params,
        addNamespace: false,
      },
    );
  }

  getV1Services({
    pageNo = 1,
    pageSize = 20,
    basic = false,
    region_name = '',
    name = '',
    label = '',
    order_by = '-created_at',
    instance_ip = '',
  }) {
    return this.httpService
      .request(this.SERVICES_ENDPOINT, {
        method: 'GET',
        params: {
          basic,
          region_name,
          page: pageNo,
          page_size: pageSize,
          name,
          label,
          order_by,
          instance_ip,
        },
        addNamespace: false,
      })
      .then((res: FetchDataResult<any>) => res);
  }

  // ********** K8S Service ********** //

  checkK8sServiceName(
    cluster: string,
    namespace_uuid: string,
    namespace: string,
    name: string,
  ) {
    let params;
    if (cluster) {
      params = {
        namespace,
        cluster,
      };
    } else {
      params = {
        namespace_uuid,
      };
    }
    return this.httpService.request(
      this.K8S_SERVICES_ENDPOINT + name + '/check',
      {
        method: 'GET',
        params,
        addNamespace: false,
      },
    );
  }

  getK8sServices({
    pageNo = 1,
    pageSize = 20,
    params: {
      cluster = '',
      name = '',
      label = '',
      pvc = '',
      host_ip = '',
      namespace = '',
    },
  }) {
    return this.httpService
      .request(this.K8S_SERVICES_ENDPOINT, {
        method: 'GET',
        params: {
          cluster,
          name: name,
          label,
          page: pageNo,
          page_size: pageSize,
          pvc,
          host_ip,
          namespace,
        },
        addNamespace: false,
      })
      .then((res: FetchDataResult<any>) => res);
  }

  getK8sServicesCount(cluster_name: string) {
    return this.httpService
      .request(this.K8S_SERVICES_ENDPOINT, {
        method: 'GET',
        params: {
          display_count: 1,
          cluster: cluster_name,
        },
        addNamespace: false,
      })
      .then((res: SourceStatsCount) => {
        if (!res.count_result) {
          throw new Error('no services');
        }
        return res;
      });
  }

  getK8sService(uuid: string): Promise<K8sComponent> {
    return this.httpService
      .request(this.K8S_SERVICES_ENDPOINT + uuid, {
        method: 'GET',
      })
      .then((service: K8sComponent) => {
        const services = service.kubernetes || [];
        const _service = services.find((item: { kind: string }) => {
          return ['Deployment', 'StatefulSet', 'DaemonSet'].includes(item.kind);
        });
        // 参考：http://confluence.alaudatech.com/display/DEV/Kubernetes+Yaml+Explain
        if (_service) {
          service.resource.containers = _service.spec.template.spec.containers.map(
            (container: any) => {
              const size: any = {};
              if (container.resources && container.resources.requests) {
                if (container.resources.requests.cpu) {
                  size.cpu = container.resources.requests.cpu;
                }
                if (container.resources.requests.memory) {
                  size.memory = container.resources.requests.memory;
                }
              }
              return {
                image: container.image,
                size,
              };
            },
          );
          service.strategy = {};
          service.strategy.type =
            get(_service, 'spec.strategy.type') ||
            get(_service, 'spec.updateStrategy.type') ||
            '-';
          service.strategy.data =
            get(_service, 'spec.strategy.rollingUpdate') ||
            get(_service, 'spec.updateStrategy.rollingUpdate') ||
            null;
        }
        return service;
      });
  }

  createK8sService(body: any) {
    return this.httpService.request(this.K8S_SERVICES_ENDPOINT, {
      method: 'POST',
      body,
      addNamespace: false,
    });
  }

  patchK8sService(
    uuid: string,
    body: {
      kubernetes?: any;
      resources?: {
        create_method: 'UI' | 'YAML';
      };
    } = null,
  ) {
    return this.httpService.request(this.K8S_SERVICES_ENDPOINT + uuid, {
      method: 'PATCH',
      body,
      addNamespace: false,
    });
  }

  startK8sService(uuid: string) {
    return this.httpService.request(
      this.K8S_SERVICES_ENDPOINT + uuid + '/start',
      {
        method: 'PUT',
      },
    );
  }

  stopK8sService(uuid: string) {
    return this.httpService.request(
      this.K8S_SERVICES_ENDPOINT + uuid + '/stop',
      {
        method: 'PUT',
      },
    );
  }

  rollbackK8sService(uuid: string, rollback_to = '') {
    let url = this.K8S_SERVICES_ENDPOINT + uuid + '/rollback';
    if (rollback_to) {
      url += `?rollback_to=${rollback_to}`;
    }
    return this.httpService.request(url, {
      method: 'PUT',
    });
  }

  retryUpdateK8sService(uuid: string) {
    return this.httpService.request(
      this.K8S_SERVICES_ENDPOINT + uuid + '/retry',
      {
        method: 'PUT',
      },
    );
  }

  deleteK8sService(uuid: string) {
    return this.httpService.request(this.K8S_SERVICES_ENDPOINT + uuid, {
      method: 'DELETE',
    });
  }

  getK8sServiceYaml(uuid: string): Promise<string> {
    return this.httpService
      .request(this.K8S_SERVICES_ENDPOINT + uuid + '/yaml', {
        method: 'GET',
        responseType: 'text',
      })
      .then((data: string) => {
        return data;
      })
      .catch(err => {
        this.logger.error(err);
        return '';
      });
  }

  getK8sServiceInstances<T = any>(uuid: string) {
    return this.httpService.request<{
      result: T[];
    }>(this.K8S_SERVICES_ENDPOINT + uuid + '/instances', {
      method: 'GET',
    });
  }

  getK8sServiceLogSource(uuid: string): Promise<string[]> {
    return this.httpService
      .request(this.K8S_SERVICES_ENDPOINT + uuid + '/logs/sources', {
        method: 'GET',
      })
      .then((res: { result: string[] }) => res.result)
      .catch(err => {
        this.logger.error(err);
        return [];
      });
  }

  getServiceRevisions(uuid: string) {
    return this.httpService
      .request(this.K8S_SERVICES_ENDPOINT + uuid + '/revisions', {
        method: 'GET',
      })
      .then(
        (res: {
          result: {
            revision: string;
            created_at: string;
            changeCause: string;
          }[];
        }) => res.result,
      )
      .catch(err => {
        this.logger.error(err);
        return [];
      });
  }

  // Utilities function
  private getPercentageOrNumberFormat(value: any) {
    return +value === parseInt(value, 10) ? +value : value;
  }

  getContainersFromKubernetes(services: any) {
    const kindOptions: string[] = ['Deployment', 'DaemonSet', 'StatefulSet'];
    const origin = services.find((item: any) => {
      return kindOptions.includes(item.kind);
    });
    const containers: any[] = get(origin, 'spec.template.spec.containers', []);
    return containers;
  }

  checkServicePayload(service: any): boolean {
    service = cloneDeep(service);
    if (service.kind === 'StatefulSet') {
      const headlessService = find(service.kubeServices || [], (item: any) => {
        return item.type === 'Headless';
      });
      if (!headlessService) {
        this.auiNotificationService.warning(
          'StatefulSet类型的服务需要至少一个Headless类型的Service关联',
        );
        return false;
      }
    }
    return true;
  }

  getServiceKubernetesDefinition(service: any): any[] {
    service = cloneDeep(service);
    service.containers = service.containers.map((item: any) => {
      return item.config;
    });
    let scalingService;
    let kubeServices: any[] = [];
    let routes: any[] = [];
    const data: any = {
      apiVersion: 'extensions/v1beta1',
      kind: service.kind,
      metadata: {
        namespace: service.namespace,
        name: service.name,
      },
      spec: {
        selector: {
          matchLabels: {
            [`service.${this.LABEL_BASE_DOMAIN}/name`]: service.name,
          },
        },
        // strategy
        template: {
          metadata: {
            labels: {
              [`service.${this.LABEL_BASE_DOMAIN}/name`]: service.name,
            },
          },
          spec: {
            affinity: {},
            containers: [],
            hostNetwork: service.networkMode.hostNetwork,
            // nodeSelector
          },
        },
      },
    };
    if (service.kind === 'StatefulSet') {
      data.apiVersion = 'apps/v1beta1';
    }
    //app owner list
    if (service.owners && service.owners.length) {
      const ownerMeta = service.owners.map((item: any) => {
        return {
          name: item.name,
          phone: item.phone,
        };
      });
      set(
        data.spec.template.metadata,
        'annotations["owner.cmb.io/info"]',
        JSON.stringify(ownerMeta),
      );
    }
    // network mode
    if (service.networkMode.subnet && service.networkMode.subnet.name) {
      set(
        data.spec.template.metadata,
        `annotations["subnet.${this.LABEL_BASE_DOMAIN}/name"]`,
        service.networkMode.subnet.name,
      );
    }
    if (
      service.networkMode.subnet &&
      service.networkMode.subnet.ipAddress.length
    ) {
      set(
        data.spec.template.metadata,
        `annotations["subnet.${this.LABEL_BASE_DOMAIN}/ipAddrs"]`,
        service.networkMode.subnet.ipAddress.join(','),
      );
    }
    // replicas
    if (service.kind !== 'DaemonSet') {
      data.spec.replicas = +service.replicas;
    }

    // labels
    if (service.labels.length) {
      const labels = data.spec.template.metadata.labels;
      service.labels
        .filter(
          (item: any) => item.name !== `subnet.${this.LABEL_BASE_DOMAIN}/uuid`,
        )
        .forEach((item: any) => {
          labels[item.name] = item.value;
        });
    }
    // nodeTags -> nodeSelector
    if (service.nodeTags.length) {
      const nodeSelector = {};
      service.nodeTags.forEach((item: any) => {
        const [key, value] = item.split(':');
        nodeSelector[key] = value;
      });
      data.spec.template.spec.nodeSelector = nodeSelector;
    }
    // nodeAffinity
    if (Array.isArray(service.nodeAffinitys) && service.nodeAffinitys.length) {
      const matchExpressionObj = {};
      service.nodeAffinitys.forEach((item: any) => {
        const [key, value] = item.split(':');
        if (matchExpressionObj.hasOwnProperty(key)) {
          matchExpressionObj[key].values.push(value);
        } else {
          matchExpressionObj[key] = {
            operator: 'In',
            values: [value],
          };
        }
      });
      const nodeAffinity: any = {
        requiredDuringSchedulingIgnoredDuringExecution: {
          nodeSelectorTerms: [
            {
              matchExpressions: map(matchExpressionObj, (v, k) => {
                return Object.assign({ key: k }, v);
              }),
            },
          ],
        },
      };
      data.spec.template.spec.affinity['nodeAffinity'] = nodeAffinity;
    }
    // affinity
    if (service.kind !== 'DaemonSet') {
      const affinityServices = get(service, 'affinity.affinityServices', []);
      const antiAffinityServices = get(
        service,
        'affinity.antiAffinityServices',
        [],
      ).map((item: { name: string; uuid: string; namespace: string }) => {
        return item.uuid === '$$self'
          ? {
              name: service.name,
              namespace: service.namespace,
              uuid: '$$self',
            }
          : item;
      });
      const podAffinity = this.getServiceAffinityDefinition(affinityServices);
      const podAntiAffinity = this.getServiceAffinityDefinition(
        antiAffinityServices,
      );
      if (podAffinity || podAntiAffinity) {
        if (podAffinity) {
          data.spec.template.spec.affinity['podAffinity'] = podAffinity;
        }
        if (podAntiAffinity) {
          data.spec.template.spec.affinity['podAntiAffinity'] = podAntiAffinity;
        }
      }
    }
    // update strategy: maxSurge && maxUnavailable
    switch (service.kind) {
      case 'Deployment':
        data.spec.strategy = {
          type: 'RollingUpdate',
        };
        if (service.maxSurge || service.maxUnavailable) {
          data.spec.strategy.rollingUpdate = {};
          if (service.maxSurge) {
            data.spec.strategy.rollingUpdate.maxSurge = this.getPercentageOrNumberFormat(
              service.maxSurge,
            );
          }
          if (service.maxUnavailable) {
            data.spec.strategy.rollingUpdate.maxUnavailable = this.getPercentageOrNumberFormat(
              service.maxUnavailable,
            );
          }
        }
        break;
      case 'DaemonSet':
        data.spec.updateStrategy = {
          type: 'RollingUpdate',
        };
        if (service.maxUnavailable) {
          data.spec.updateStrategy.rollingUpdate = {
            maxUnavailable: this.getPercentageOrNumberFormat(
              service.maxUnavailable,
            ),
          };
        }
        break;
      case 'StatefulSet':
        data.spec.updateStrategy = {
          type: 'RollingUpdate',
        };
        break;
    }
    // kubernetes servcies
    if (service.kubeServices.length) {
      kubeServices = service.kubeServices.map((item: ServiceViewModel) => {
        return this.getKubeServiceDefinition(
          item,
          service.name,
          service.namespace,
        );
      });
      // StatefulSet有个serviceName字段，明确指定了与其关联的service name, 且必须是Headless service
      if (service.kind === 'StatefulSet') {
        const firstHeadlessService = find(service.kubeServices, (item: any) => {
          return item.type === 'Headless';
        });
        data.spec.serviceName = firstHeadlessService.name;
      }
      routes = service.kubeServices
        .map((item: ServiceViewModel) => {
          return this.getRouteDefinition(item, service.namespace);
        })
        .filter((item: ServiceViewModel) => !!item);
    }
    // scaling service
    if (service.kind !== 'DaemonSet') {
      scalingService = this.getScalingServiceDefinition({
        kind: service.kind,
        name: service.name,
        namespace: service.namespace,
        minReplicas: service.minReplicas,
        maxReplicas: service.maxReplicas,
      });
    }

    // containers
    service.containers.forEach((container: ContainerViewModel) => {
      if (container.volumes) {
        container.volumes.forEach((volume: ContainerVolumeViewModel) => {
          volume._uniqueName = this.getVolumeUniqeName(volume);
        });
      }
    });
    // volumes
    let allVolumes: ContainerVolumeViewModel[] = flatten(
      service.containers.map((container: any) => {
        return container.volumes || [];
      }),
    );
    if (allVolumes.length) {
      allVolumes = uniqBy(allVolumes, item => item._uniqueName);
      allVolumes.forEach((volume: ContainerVolumeViewModel, index: number) => {
        volume.name = `${volume.type}-${index}`;
      });
      data.spec.template.spec.volumes = allVolumes.map((item: any) => {
        return this.getServiceVolumeDefinition(item);
      });
    }

    const containers = service.containers.map((container: any) => {
      return this.getServiceContainerDefinition(container, allVolumes);
    });
    // containers[].volume中的 volume 和 pvc 类型
    data.spec.template.spec.containers = containers;
    // dump to yaml
    const services = [data];
    if (scalingService) {
      services.push(scalingService);
    }
    if (kubeServices.length) {
      services.push(...kubeServices);
    }
    if (routes.length) {
      services.push(...routes);
    }
    return services;
  }

  generateServiceYamlPayload(service: any): string {
    return this.getServiceKubernetesDefinition(service)
      .map(item => jsyaml.safeDump(item))
      .join('---\r\n');
  }

  private getServiceAffinityDefinition(
    services: {
      name: string;
      uuid: string;
      namespace: string;
    }[] = [],
  ) {
    let data;
    if (services.length) {
      const item = {
        topologyKey: 'kubernetes.io/hostname',
        labelSelector: {
          matchExpressions: [
            {
              key: `service.${this.LABEL_BASE_DOMAIN}/name`,
              operator: 'In',
              values: services.map((item: any) => {
                return item.name;
              }),
            },
          ],
        },
      };
      item['namespaces'] = services.map((item: any) => {
        return item.namespace || '';
      });
      data = {
        requiredDuringSchedulingIgnoredDuringExecution: [item],
      };
    } else {
      data = null;
    }
    return data;
  }

  private getKubeServiceDefinition(
    kubeService: ServiceViewModel,
    serviceName: string,
    namespace: string,
  ) {
    const data: any = {
      kind: 'Service',
      apiVersion: 'v1',
      metadata: {
        name: kubeService.name,
        namespace,
      },
    };
    const spec: any = {
      selector: {
        [`service.${this.LABEL_BASE_DOMAIN}/name`]: serviceName,
      },
    };
    // loadbalancer annotation
    const loadbalancers = kubeService.ports
      .filter(item => !!item.loadBalancerName)
      .map((item: ServicePort) => {
        return {
          container_port: +item.containerPort,
          protocol: item.protocol.toLowerCase(),
          name: item.loadBalancerName,
          port: item.listenerPort ? +item.listenerPort : 0,
        };
      });
    if (loadbalancers.length) {
      data.metadata.annotations = {
        [`loadbalancer.${this.LABEL_BASE_DOMAIN}/bind`]: JSON.stringify(
          loadbalancers,
        ),
      };
    }
    // service ports
    switch (kubeService.type) {
      case 'ClusterIP':
        spec.ports = kubeService.ports.map((item: ServicePort) => {
          const port = item.port ? +item.port : +item.containerPort;
          return {
            name: `${port}-${item.containerPort}`,
            protocol: 'TCP',
            port,
            targetPort: +item.containerPort,
          };
        });
        break;
      case 'Headless':
        spec.clusterIP = 'None';
        spec.ports = kubeService.ports.map((item: ServicePort) => {
          const port = item.port ? +item.port : +item.containerPort;
          return {
            name: `${port}-${item.containerPort}`,
            protocol: 'TCP',
            port,
            targetPort: +item.containerPort,
          };
        });
        break;
      case 'NodePort':
        spec.type = 'NodePort';
        spec.ports = kubeService.ports.map((item: ServicePort) => {
          const port = item.port ? +item.port : +item.containerPort;
          return {
            name: `${port}-${item.containerPort}`,
            protocol: 'TCP',
            port,
            targetPort: +item.containerPort,
            nodePort: +item.nodePort,
          };
        });
        break;
    }
    data.spec = spec;
    return data;
  }

  private getRouteDefinition(kubeService: ServiceViewModel, namespace: string) {
    if (kubeService.route && kubeService.route.host) {
      // 目前一个Service包含多组ports，但只支持添加一个Route，默认选择第一个containerPort
      const targetPort = kubeService.ports[0].containerPort;
      const routeData: any = {
        apiVersion: 'route.openshift.io/v1',
        kind: 'Route',
        metadata: {
          name: kubeService.name,
          namespace: namespace,
        },
        spec: {
          host: kubeService.route.host,
          path: '/' + kubeService.route.path,
          port: {
            targetPort: `tcp-${targetPort}`,
          },
          to: {
            kind: 'Service',
            name: kubeService.name,
            weight: 100,
          },
          wildcardPolicy: 'None',
        },
      };
      return routeData;
    } else {
      return null;
    }
  }

  private getServiceContainerDefinition(
    container: ContainerViewModel,
    allVolumes: ContainerVolumeViewModel[],
  ) {
    const data: any = {
      name: container.name,
      image: `${container.image}:${container.imageTag}`,
      imagePullPolicy: 'Always',
    };
    if (container.entrypoint) {
      data.command = [container.entrypoint];
    }
    if (container.cmd && container.cmd.length) {
      data.args = container.cmd;
    }
    if (container.resources) {
      let resourceData: any;
      const resources: any = Object.assign(
        {
          requests: {},
          limits: {},
        },
        container.resources,
      );
      /**
       * TODO: Delete Me <start>
       */
      if (resources.requests.memory || resources.requests.cpu) {
        resourceData = {
          requests: {},
        };
        if (resources.requests.memory) {
          resourceData.requests.memory = resources.requests.memory;
        }
        if (resources.requests.cpu) {
          resourceData.requests.cpu = resources.requests.cpu;
        }
      }
      if (resources.limits.memory || resources.limits.cpu) {
        if (!resourceData) {
          resourceData = {
            limits: {},
          };
        } else {
          resourceData.limits = {};
        }
        if (resources.limits.memory) {
          resourceData.limits.memory = resources.limits.memory;
        }
        if (resources.limits.cpu) {
          resourceData.limits.cpu = resources.limits.cpu;
        }
      }
      if (!resources.limits.memory && resources.requests.memory) {
        if (!resourceData.limits) {
          resourceData.limits = {};
        }
        resourceData.limits.memory = resources.requests.memory;
      }
      if (!resources.limits.cpu && resources.requests.cpu) {
        if (!resourceData.limits) {
          resourceData.limits = {};
        }
        resourceData.limits.cpu = resources.requests.cpu;
      }

      if (resourceData) {
        data.resources = resourceData;
      }
      /* <end> */

      /* TODO: Uncomment Me <start> */
      // if (!resources.limits.memory) {
      //   resources.limits.memory = resources.requests.memory;
      // }

      // if (!resources.limits.cpu) {
      //   resources.limits.cpu = resources.requests.cpu;
      // }

      // resourceData = {
      //   requests: {
      //     cpu: resources.requests.cpu,
      //     memory: resources.requests.memory,
      //   },
      //   limits: {
      //     cpu: resources.limits.cpu,
      //     memory: resources.limits.memory,
      //   },
      // };
      // data.resources = resourceData;
      /* <end> */
    }

    // envvars
    if (container.envvars.length) {
      const env: ContainerEnvvar[] = [];
      container.envvars.forEach((item: ContainerEnvvar) => {
        if (item.valueFrom) {
          if (
            !item.valueFrom.configMapKeyRef.name ||
            !item.valueFrom.configMapKeyRef.key
          ) {
            unset(item, 'valueFrom');
          }
        } else {
          if (!item.value) {
            unset(item, 'value');
          }
        }
        env.push(item);
      });
      data.env = env;
    }
    // log file
    if (container.logFiles && container.logFiles.length) {
      if (!data.env) {
        data.env = [];
      }
      const logFile = data.env.find(
        (item: any) => item.name === '__ALAUDA_FILE_LOG_PATH__',
      );
      if (logFile) {
        logFile.value = container.logFiles.join(',');
      } else {
        data.env.push({
          name: '__ALAUDA_FILE_LOG_PATH__',
          value: container.logFiles.join(','),
        });
      }
    }
    if (container.excludeLogFiles && container.excludeLogFiles.length) {
      if (!data.env) {
        data.env = [];
      }
      const excludeLogFile = data.env.find(
        (item: any) => item.name === '__ALAUDA_EXCLUDE_LOG_PATH__',
      );
      if (excludeLogFile) {
        excludeLogFile.value = container.excludeLogFiles.join(',');
      } else {
        data.env.push({
          name: '__ALAUDA_EXCLUDE_LOG_PATH__',
          value: container.excludeLogFiles.join(','),
        });
      }
    }
    // configmaps
    if (container.configmaps.length) {
      data.envFrom = container.configmaps.map((item: any) => {
        return {
          configMapRef: {
            name: item,
          },
        };
      });
    }
    // healthCheck
    if (container.healthCheck && container.healthCheck.liveness) {
      data.livenessProbe = this.getContainerHealthCheckItemDefinition(
        container.healthCheck.liveness,
      );
    }
    if (container.healthCheck && container.healthCheck.readiness) {
      data.readinessProbe = this.getContainerHealthCheckItemDefinition(
        container.healthCheck.readiness,
      );
    }
    // volumes
    if (container.volumes && container.volumes.length) {
      const volumeMounts: any[] = [];
      container.volumes.forEach((item: ContainerVolumeViewModel) => {
        const volume = allVolumes.find(v => v._uniqueName === item._uniqueName);
        if (item.configMapKeyRef) {
          item.configMapKeyMap.forEach((keyMap: any) => {
            volumeMounts.push({
              name: volume.name,
              mountPath: keyMap.path,
              subPath: keyMap.key,
            });
          });
        } else {
          volumeMounts.push({
            name: volume.name,
            mountPath: item.containerPath,
            ...(item.subPath ? { subPath: item.subPath } : {}),
          });
        }
      });
      data.volumeMounts = volumeMounts;
    }
    return data;
  }

  private getContainerHealthCheckItemDefinition(item: any) {
    const data: any = {
      initialDelaySeconds: item.initialDelaySeconds,
      periodSeconds: item.periodSeconds,
      timeoutSeconds: item.timeoutSeconds,
      successThreshold: item.successThreshold,
      failureThreshold: item.failureThreshold,
    };

    switch (item.protocol) {
      case 'HTTP':
        delete item.commands;
        // filter invalid header before output
        // headers: [{name: '', value: ''}, {name: '', value: 'no-name'}] --> headers: []
        item.headers =
          item.headers && item.headers.filter((item: any) => item.name);
        const httpGet = {
          path: item.path,
          scheme: item.scheme,
          port: item.port,
        };
        if (item.headers && item.headers.length) {
          httpGet['httpHeaders'] = item.headers;
        }
        data['httpGet'] = httpGet;
        break;
      case 'TCP':
        delete item.scheme;
        delete item.path;
        delete item.headers;
        delete item.commands;
        data['tcpSocket'] = {
          port: item.port,
        };
        break;
      case 'EXEC':
        delete item.scheme;
        delete item.path;
        delete item.port;
        delete item.headers;
        if (item.commands && item.commands.length) {
          data.exec = {
            command: item.commands.map(
              ({ command }: { command: string }) => command,
            ),
          };
        }
        break;
    }
    return data;
  }

  private getServiceVolumeDefinition(item: ContainerVolumeViewModel) {
    let data: any;
    switch (item.type) {
      case 'host-path':
        data = {
          name: item.name,
          hostPath: {
            path: item.hostPath,
          },
        };
        break;
      case 'pvc':
        data = {
          name: item.name,
          persistentVolumeClaim: {
            claimName: item.pvcName,
          },
        };
        break;
      case 'volume':
        switch (item.driverName) {
          case 'ebs':
            data = {
              name: item.name,
              awsElasticBlockStore: {
                volumeID: item.driverVolumeId,
                fsType: 'ext4',
              },
            };
            break;
          case 'glusterfs':
            data = {
              name: item.name,
              glusterfs: {
                endpoints: 'glusterfs-endpoints',
                path: item.volumeName,
              },
            };
            break;
          case 'ceph_rbd':
            data = {
              name: item.name,
              rbd: {
                image: item.volumeName,
              },
            };
            break;
          default:
            data = {};
            break;
        }
        break;
      case 'configmap':
        data = {
          name: item.name,
          configMap: {
            name: item.configMapName,
          },
        };
    }
    return data;
  }

  public getScalingServiceDefinition({
    minReplicas,
    maxReplicas,
    kind,
    name,
    namespace,
  }: {
    minReplicas: number;
    maxReplicas: number;
    kind: string;
    name: string;
    namespace: string;
  }) {
    if (!maxReplicas) {
      return null;
    }
    const data: any = {
      apiVersion: 'autoscaling/v1',
      kind: 'HorizontalPodAutoscaler',
      metadata: {
        name: name,
        namespace: namespace,
      },
      spec: {
        scaleTargetRef: {
          apiVersion: 'extensions/v1beta1',
          kind: kind,
          name: name,
        },
      },
    };
    if (minReplicas) {
      data.spec.minReplicas = +minReplicas;
    }
    if (maxReplicas) {
      data.spec.maxReplicas = +maxReplicas;
    }
    return data;
  }

  /**
   *
   * @param services 'kubernetes' field from service api response
   * @returns {any}
   */
  getServiceViewModel(services: any) {
    // placeholder

    const service: any = {
      kind: '', // 部署模式
      replicas: 1, // 实例数
      labels: [],
      minReplicas: null,
      maxReplicas: null,
      nodeTags: [],
      affinity: null, // 亲和性，包括pod亲和反亲和，key为 `service.${label_base_domain}/name`
      maxSurge: '',
      maxUnavailable: '',
      kubeServices: [],
      containers: [],
      networkMode: null,
    };

    const kindOptions: string[] = ['Deployment', 'DaemonSet', 'StatefulSet'];
    const origin = services.find((item: any) => {
      return kindOptions.includes(item.kind);
    });
    //networkMode
    service.networkMode = this.getServiceNetworkModeViewModel(origin);
    // kind, replicas
    service.kind = origin.kind;
    service.replicas = get(origin, 'spec.replicas', 1);
    // labels
    service.labels = this.getServiceLabelsViewModel(origin);
    // minReplicas && maxReplicas
    const scalingService = services.find((item: any) => {
      return item.kind === 'HorizontalPodAutoscaler';
    });
    if (scalingService) {
      service.minReplicas = get(scalingService, 'spec.minReplicas', null);
      service.maxReplicas = get(scalingService, 'spec.maxReplicas', null);
    }
    // nodeTags
    service.nodeTags = this.getServiceNodeTagsViewModel(origin);
    service.nodeAffinitys = this.getServiceNodeAffinitysViewModel(origin);
    // affinity
    service.affinity = this.getServiceAffinityViewModel(origin);
    // maxSurge && maxUnavailable
    switch (service.kind) {
      case 'Deployment':
        service.maxSurge = get(
          origin,
          'spec.strategy.rollingUpdate.maxSurge',
          null,
        );
        service.maxUnavailable = get(
          origin,
          'spec.strategy.rollingUpdate.maxUnavailable',
          null,
        );
        break;
      case 'DaemonSet':
        service.maxUnavailable = get(
          origin,
          'spec.updateStrategy.rollingUpdate.maxUnavailable',
          null,
        );
        break;
      case 'StatefulSet':
        break;
    }
    // kubeServices
    const routes: any[] = services.filter((item: any) => {
      return item.kind === 'Route';
    });
    service.kubeServices = services
      .filter((item: any) => {
        return item.kind === 'Service';
      })
      .map((item: any) => {
        return this.getKubeServiceViewModel(item, routes);
      });
    // volumes
    const volumes = get(origin, 'spec.template.spec.volumes', []).map(
      (item: any) => {
        return this.getContainerVolumeViewModel(item);
      },
    );
    // containers
    const containers: any[] = get(origin, 'spec.template.spec.containers', []);
    service.containers = containers.map((item: any) => {
      return {
        config: this.getContainerViewModel(item, volumes),
      };
    });
    return service;
  }

  getServiceLabelsViewModel(service: any) {
    const viewModel: any[] = [];
    const filterSet = [
      `service.${this.LABEL_BASE_DOMAIN}/uuid`,
      `service.${this.LABEL_BASE_DOMAIN}/name`,
      `service.${this.LABEL_BASE_DOMAIN}/createby`,
      `app.${this.LABEL_BASE_DOMAIN}/uuid`,
      `app.${this.LABEL_BASE_DOMAIN}/name`,
      `cluster.${this.LABEL_BASE_DOMAIN}/uuid`,
    ];
    const labels = get(service, 'spec.template.metadata.labels', {});
    Object.entries(labels).forEach(([name, value]) => {
      if (filterSet.includes(name)) {
        // do nothing
      } else {
        viewModel.push({
          name: name,
          value: value,
        });
      }
    });
    return viewModel;
  }

  getServiceNetworkModeViewModel(service: any) {
    const viewModel: NetworkModeViewModel = {
      hostNetwork: false,
      subnet: null,
    };
    const subnetName = get(
      service,
      `spec.template.metadata.annotations["subnet.${
        this.LABEL_BASE_DOMAIN
      }/name"]`,
      '',
    );
    const subnetIpAddressString: string = get(
      service,
      `spec.template.metadata.annotations["subnet.${
        this.LABEL_BASE_DOMAIN
      }/ipAddrs"]`,
      '',
    );
    const subnetIpAddress: string[] =
      (subnetIpAddressString && subnetIpAddressString.split(',')) || [];

    // hostNetwork
    viewModel.hostNetwork = get(
      service,
      'spec.template.spec.hostNetwork',
      false,
    );
    // subnet
    viewModel.subnet = subnetName
      ? { name: subnetName, ipAddress: subnetIpAddress }
      : null;

    return viewModel;
  }

  getServiceNodeTagsViewModel(service: any, isDetail: boolean = false) {
    const viewModel: any[] = [];
    const nodeSelector = get(service, 'spec.template.spec.nodeSelector', {});
    Object.entries(nodeSelector).forEach(([key, value]) => {
      const data = isDetail ? { name: key, value } : `${key}:${value}`;
      viewModel.push(data);
    });
    return viewModel;
  }

  getServiceNodeAffinitysViewModel(service: any) {
    const nodeAffinity = get(
      service,
      'spec.template.spec.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions',
      [],
    ).reduce((sum: any, item: any) => {
      return sum.concat(
        item.values.map((value: string) => {
          return `${item.key}:${value}`;
        }),
      );
    }, []);
    return nodeAffinity;
  }

  getServiceAffinityViewModel(service: any) {
    const affinityServiceNamespaces = get(
      service,
      'spec.template.spec.affinity.podAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].namespaces',
      [],
    );
    const affinityServices = get(
      service,
      'spec.template.spec.affinity.podAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].labelSelector.matchExpressions[0].values',
      [],
    ).map((item: string, index: number) => {
      return {
        name: item,
        namespace: affinityServiceNamespaces[index] || '',
      };
    });
    const antiAffinityServices = get(
      service,
      'spec.template.spec.affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].labelSelector.matchExpressions[0].values',
      [],
    ).map((item: string) => {
      return {
        name: item,
        namespace: '', // antiAffinity service does not need namespace
      };
    });
    return {
      affinityServices,
      antiAffinityServices,
    };
  }

  public getKubeServiceViewModel(kubeService: any, routes?: any[]) {
    const service: any = {
      name: kubeService.metadata.name,
      type: '',
      ports: [], //containerPort: '',nodePort: '',loadBalancerName: '',listenerPort: '',protocol: 'HTTP',
      route: null, // host: '', path: ''
    };
    if (get(kubeService, 'spec.type') === 'NodePort') {
      service.type = 'NodePort';
    } else if (get(kubeService, 'spec.clusterIP') === 'None') {
      service.type = 'Headless';
    } else {
      service.type = 'ClusterIP';
    }
    const ports = get(kubeService, 'spec.ports', []);
    const loadBalancers = JSON.parse(
      get(
        kubeService,
        `metadata.annotations["loadbalancer.${this.LABEL_BASE_DOMAIN}/bind"]`,
        '[]',
      ),
    );
    service.ports = ports.map(
      (item: {
        port: number;
        targetPort: number;
        protocol: string;
        nodePort?: number;
      }) => {
        const data = {
          port: item.port,
          containerPort: item.targetPort,
          nodePort: item.nodePort,
          loadBalancerName: '',
          listenerPort: '',
          protocol: 'HTTP',
        };
        const lb = loadBalancers.find((obj: any) => {
          return obj.container_port === item.port;
        });
        if (lb) {
          data.loadBalancerName = lb.name;
          data.listenerPort = lb.port === 0 ? null : lb.port;
          data.protocol = lb.protocol.toUpperCase();
        }
        return data;
      },
    );
    // route
    const route =
      routes &&
      routes.find((item: any) => {
        const routeRefTo = get(item, 'spec.to.name', '');
        return service.name === routeRefTo;
      });
    if (route) {
      service.route = {
        host: route.spec.host,
        path: route.spec.path.substr(1),
      };
    }
    return service;
  }

  private getContainerViewModel(data: any, volumes: any[]) {
    const container: ContainerViewModel = {
      name: data.name,
      image: '',
      imageTag: '',
      resources: null, // 容器大小，包括最大值和参考值
      volumes: [],
      healthCheck: {},
      entrypoint: get(data, 'command[0]'),
      cmd: get(data, 'args', []),
      envvars: [],
      configmaps: [],
      logFiles: [],
      excludeLogFiles: [],
    };
    const envvars = get(data, 'env', []);
    // log file
    const logFiles = envvars.find((item: { name: string; value: string }) => {
      return item.name === '__ALAUDA_FILE_LOG_PATH__';
    });
    container.logFiles = logFiles ? logFiles.value.split(',') : [];
    const excludeLogFiles = envvars.find(
      (item: { name: string; value: string }) => {
        return item.name === '__ALAUDA_EXCLUDE_LOG_PATH__';
      },
    );
    container.excludeLogFiles = excludeLogFiles
      ? excludeLogFiles.value.split(',')
      : [];
    // envvars
    container.envvars = envvars
      .filter((item: any) => {
        return !has(item, 'valueFrom.secretKeyRef');
      })
      .filter((item: { name: string; value: string }) => {
        return ![
          '__ALAUDA_FILE_LOG_PATH__',
          '__ALAUDA_EXCLUDE_LOG_PATH__',
          '__ALAUDA_SERVICE_ID__',
          '__ALAUDA_SERVICE_NAME__',
          '__ALAUDA_APP_NAME__',
        ].includes(item.name);
      });
    // image
    const image =
      data.image.indexOf('/') < 0
        ? `index.docker.io/library/${data.image}`
        : data.image;
    const [endpoint, ...imageRest] = image.split('/');
    const [imageName, imageTag] = imageRest.join('/').split(':');
    container.image = [endpoint, imageName].join('/');
    container.imageTag = imageTag;
    // resources
    const resources = {
      requests: {},
      limits: {},
    };
    container.resources = Object.assign(resources, data.resources);
    container.configmaps = get(data, 'envFrom', []).map((item: any) => {
      return get(item, 'configMapRef.name', '');
    });
    container.volumes = get<any[]>(data, 'volumeMounts', [])
      .map(item => {
        const volume = volumes.find(v => v.name === item.name);
        let duplicateFlag = false;
        if (volume) {
          if (volume.type !== 'configmap') {
            volume.containerPath = item.mountPath;
            if (item.subPath) {
              volume.subPath = item.subPath;
            }
          } else {
            if (item.subPath) {
              volume.containerPath = '';
              volume.configMapKeyRef = true;
              if (!volume.configMapKeyMap) {
                volume.configMapKeyMap = [];
              } else {
                duplicateFlag = true;
              }
              volume.configMapKeyMap.push({
                key: item.subPath,
                path: item.mountPath,
              });
            } else {
              volume.containerPath = item.mountPath;
            }
          }
          return duplicateFlag ? null : volume;
        }
        return null;
      })
      .filter(v => !!v);
    container.healthCheck = {
      liveness: this.getContainerHealthCheckItemViewModel(
        get(data, 'livenessProbe', null),
      ),
      readiness: this.getContainerHealthCheckItemViewModel(
        get(data, 'readinessProbe', null),
      ),
    };
    return container;
  }

  public getContainerHealthCheckItemViewModel(data: any) {
    if (!data) {
      return null;
    }
    const hc: any = {
      initialDelaySeconds: data.initialDelaySeconds,
      periodSeconds: data.periodSeconds,
      timeoutSeconds: data.timeoutSeconds,
      successThreshold: data.successThreshold || 0,
      failureThreshold: data.failureThreshold || 0,
    };
    if (data.tcpSocket) {
      hc.protocol = 'TCP';
      hc.port = data.tcpSocket.port;
    } else if (data.httpGet) {
      hc.protocol = 'HTTP';
      hc.port = data.httpGet.port;
      hc.scheme = data.httpGet.scheme;
      hc.path = data.httpGet.path;
      hc.headers = get(data, 'httpGet.httpHeaders', []);
    } else {
      hc.protocol = 'EXEC';
      hc.commands = (get(data, 'exec.command', []) as string[]).map(
        command => ({
          command,
        }),
      );
    }
    return hc;
  }

  getContainerVolumeViewModel(data: any) {
    const volume: ContainerVolumeViewModel = {
      name: data.name,
      containerPath: '', // containerPath定义在每个container的volumeMounts节点中
    };
    if (data.persistentVolumeClaim) {
      volume.type = 'pvc';
      volume.pvcName = get(data, 'persistentVolumeClaim.claimName', '');
    } else if (data.hostPath) {
      volume.type = 'host-path';
      volume.hostPath = get(data, 'hostPath.path', '');
    } else if (data.configMap) {
      volume.type = 'configmap';
      volume.configMapName = data.configMap.name;
    } else {
      const keys = Object.keys(data);
      if (keys.includes('awsElasticBlockStore')) {
        // TODO: 找个yaml字段存放volumeName?
        volume.volumeName = '';
        volume.driverVolumeId = get(data, 'awsElasticBlockStore.volumeID');
        volume.driverName = 'ebs';
      } else if (keys.includes('glusterfs')) {
        volume.volumeName = get(data, 'glusterfs.path', '');
        volume.driverName = 'glusterfs';
      } else if (keys.includes('rbd')) {
        volume.volumeName = get(data, 'rbd.image', '');
        volume.driverName = 'ceph_rbd';
      }
      volume.type = 'volume';
    }
    return volume;
  }

  async getRepositoryParamsFromRawImageName(
    image_name: string,
  ): Promise<RcImageSelection> {
    const result: RcImageSelection = {
      is_public_registry: false,
      registry_name: '',
      registry_endpoint: '',
      project_name: '',
      repository_name: '',
      full_image_name: image_name,
    };
    if (image_name.indexOf('/') < 0) {
      image_name = `index.docker.io/library/${image_name}`;
    }
    const registries = await this.registryService.find();
    const fields = image_name.split('/');
    const [endpoint, ...parts] = fields;
    result.registry_endpoint = endpoint;
    const registry = find(registries, item => {
      return item.endpoint === endpoint;
    });
    if (registry) {
      if (registry.is_public) {
        const [, namespace, repo_name] = fields;
        // repository is in public registry but not user namespace (eg: tutum, library), should get repository from thrid party API
        if (namespace !== this.account.namespace) {
          result.repository_name = parts.join('/');
          return result;
        }
        result.repository_name = repo_name;
      } else {
        if (fields.length === 2) {
          const [, repo_name] = fields;
          result.repository_name = repo_name;
        } else if (fields.length === 3) {
          const [, project_name, repo_name] = fields;
          result.project_name = project_name;
          result.repository_name = repo_name;
        }
      }
      result.is_public_registry = !!registry.is_public;
      result.registry_name = registry.name;
    } else {
      result.repository_name = parts.join('/');
      // result.full_image_name = `${result.registry_endpoint}/${
      //   result.repository_name
      // }`;
    }
    return result;
  }

  private getVolumeUniqeName(data: ContainerVolumeViewModel) {
    let uniqName;
    switch (data.type) {
      case 'host-path':
        uniqName = `${data.type}:${data.hostPath}`;
        break;
      case 'volume':
        uniqName = `${data.type}:${data.volumeName}`;
        break;
      case 'pvc':
        uniqName = `${data.type}:${data.pvcName}`;
        break;
      case 'configmap':
        uniqName = `${data.type}:${data.configMapName}`;
        break;
    }
    return uniqName;
  }
}
