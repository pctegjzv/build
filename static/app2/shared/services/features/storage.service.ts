import { Inject, Injectable } from '@angular/core';
import { keyBy, mapValues } from 'lodash';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import {
  SnapshotBody,
  SnapshotsQuery,
  VolumeBody,
  VolumesQuery,
} from 'app2/features/storage/storage.types';
import { Mem } from 'app2/shared/pipes/fit-mem.pipe';
import { K8sResourceService } from 'app2/shared/services/features/k8s-resource.service';
import { HttpService } from 'app2/shared/services/http/http.service';
import { getCookie } from 'app2/utils/cookie';
import { BatchReponse } from 'app_user/core/types';

export interface Volume {
  name: string;
  id: string;
  driver_name: string;
  driver_volume_id: string;
  namespace: string;
  pv_name: string;
  pv_uuid: string;
  size: number;
  snapshot_id: string;
  bricks: string;
  region_id: string;
  region_name: string;
  resource_actions: string[];
  stats: string;
  state: string;
}

export interface Pv {
  kubernetes: {
    apiVersion: string;
    kind: 'PersistentVolume';
    metadata: {
      creationTimestamp: string;
      name: string;
      uid: string;
      annotations?: {
        [key: string]: string;
      };
    };
    spec: {
      accessModes: ['ReadWriteOnce' | 'ReadOnlyMany' | 'ReadWriteMany'];
      capacity: {
        storage: string;
      };
      claimRef: {
        apiVersion: string;
        kind: 'PersistentVolumeClaim';
        name: string;
        namespace: string;
        uid: string;
      };
      persistentVolumeReclaimPolicy: 'Recycle' | 'Retain' | 'Delete';
    };
    status: {
      phase: string;
    };
  };
  resource_actions: string[];
}

export interface Pvc {
  kubernetes: {
    apiVersion: string;
    kind: 'PersistentVolumeClaim';
    referenced_by: [
      {
        uuid: string;
        name: string;
        type: string;
        detail: {
          location: string;
          value: string;
        };
      }
    ];
    metadata: {
      name: string;
      creationTimestamp: string;
      namespace: string;
      uid: string;
    };
    annotations?: any;
    spec: {
      accessModes: ['ReadWriteOnce' | 'ReadOnlyMany' | 'ReadWriteMany'];
      storageClassName?: string;
      volumeName?: string;
      resources: {
        requests: { storage: string };
      };
    };
    status: {
      phase: string;
    };
  };
  resource_actions: string[];
}

export interface StorageClass {
  kubernetes: {
    apiVersion: string;
    kind: 'StorageClass';
    metadata: {
      name: string;
      creationTimestamp: string;
      uid: string;
      annotations?: {
        'storageclass.kubernetes.io/is-default-class': 'true' | 'false';
      };
    };
    parameters: {
      resturl: string;
      volumetype: string;
    };
    provisioner: string;
    reclaimPolicy: string;
  };
  resource_actions: string[];
}

export interface KubernetesPv {
  kind: string;
  spec: {
    storageClassName?: string;
    accessModes: ['ReadWriteOnce' | 'ReadOnlyMany' | 'ReadWriteMany'];
    capacity: {
      storage: string;
    };
    persistentVolumeReclaimPolicy: 'Recycle' | 'Retain' | 'Delete';
  };
  apiVersion: string;
  metadata: {
    labels?: any;
    name: string;
    annotations: {
      [key: string]: string;
    };
  };
}

export interface PvcPayload {
  kind: string;
  spec: {
    storageClassName?: string;
    accessModes: ['ReadWriteOnce' | 'ReadOnlyMany' | 'ReadWriteMany'];
    resources: {
      requests: {
        storage: string;
      };
    };
  };
  apiVersion: string;
  metadata: {
    labels?: any;
    name: string;
    namespace: string;
  };
}

export interface MirrorPvc {
  uuid: string;
  name: string;
  cluster_uuid: string;
  cluster_name: string;
  cluster_display_name: string;
  namespace_name: string;
}

@Injectable()
export class StorageService {
  VOLUMES_URL: string;
  STORAGE_URL: string;
  SNAPSHOTS_URL: string;
  PV_URL: string;
  PVC_URL: string;
  K8S_URL: string;

  constructor(
    private httpService: HttpService,
    private k8sResourceService: K8sResourceService,
    @Inject(ACCOUNT) account: RcAccount,
  ) {
    const namespace = account.namespace;
    this.STORAGE_URL = `/ajax/v1/storage/${namespace}/`;
    this.VOLUMES_URL = `${this.STORAGE_URL}volumes/`;
    this.SNAPSHOTS_URL = `${this.STORAGE_URL}snapshots/`;
    this.PV_URL = '/ajax/v2/persistentvolumes/';
    this.PVC_URL = '/ajax/v2/persistentvolumeclaims/';
    this.K8S_URL = '/ajax/v2/kubernetes/clusters/';
  }

  getRegionVolumeSize(region_id: string): Promise<Mem> {
    return this.httpService.request(this.VOLUMES_URL, {
      method: 'GET',
      params: {
        display_count: 1,
        region_id,
      },
    });
  }

  getVolumes(query: VolumesQuery): Promise<any> {
    query['action'] = 'view';
    return this.httpService.request(this.VOLUMES_URL, {
      method: 'GET',
      params: query,
      ignoreProject: true,
    });
  }

  createVolume(volume: VolumeBody): Promise<any> {
    return this.httpService.request(this.VOLUMES_URL, {
      method: 'POST',
      body: volume,
      timeout: 60000,
      ignoreProject: true,
    });
  }

  deleteVolume(id: string): Promise<any> {
    return this.httpService.request(this.VOLUMES_URL + id, {
      method: 'DELETE',
      ignoreProject: true,
    });
  }

  getVolumeDrivers(region_name: string): Promise<any> {
    return this.httpService.request(
      `${this.STORAGE_URL}${region_name}/drivers`,
      {
        method: 'GET',
        cache: true,
      },
    );
  }
  getVolumeDetail(id: string): Promise<any> {
    return this.httpService.request(this.VOLUMES_URL + id, {
      method: 'GET',
      ignoreProject: true,
    });
  }

  // snapshot
  getSnapshots(query: SnapshotsQuery): Promise<any> {
    return this.httpService.request(this.SNAPSHOTS_URL, {
      method: 'GET',
      params: query,
      ignoreProject: true,
    });
  }

  createSnapshot(snapshot: SnapshotBody): Promise<any> {
    return this.httpService.request(this.SNAPSHOTS_URL, {
      method: 'POST',
      body: snapshot,
      ignoreProject: true,
    });
  }

  deleteSnapshot(id: string): Promise<any> {
    return this.httpService.request(this.SNAPSHOTS_URL + id, {
      method: 'DELETE',
      ignoreProject: true,
    });
  }

  // pv
  createPv(cluster_id: string, kubernetes: KubernetesPv): Promise<any> {
    return this.httpService.request(
      `${this.K8S_URL}${cluster_id}/persistentvolumes/`,
      {
        method: 'POST',
        body: kubernetes,
        addNamespace: false,
        ignoreProject: true,
      },
    );
  }

  getPvs({
    cluster_id,
    page_size = 20,
    page,
    name = '',
  }: {
    cluster_id: string;
    page_size: number;
    page: number;
    name?: string;
  }): Promise<any> {
    return this.httpService.request(
      `${this.K8S_URL}${cluster_id}/persistentvolumes/`,
      {
        method: 'GET',
        params: {
          page,
          page_size,
          name,
        },
        addNamespace: false,
        ignoreProject: true,
      },
    );
  }

  getPvDetail(cluster_id: string, name: string): Promise<any> {
    return this.httpService.request(
      `${this.K8S_URL}${cluster_id}/persistentvolumes/${name}`,
      {
        method: 'GET',
        ignoreProject: true,
      },
    );
  }

  updatePv(
    name: string,
    cluster_id: string,
    kubernetes: KubernetesPv,
  ): Promise<any> {
    return this.httpService.request(
      `${this.K8S_URL}${cluster_id}/persistentvolumes/${name}`,
      {
        method: 'PUT',
        body: kubernetes,
        addNamespace: false,
        ignoreProject: true,
      },
    );
  }

  deletePv(cluster_id: string, name: string) {
    return this.httpService.request(
      `${this.K8S_URL}${cluster_id}/persistentvolumes/${name}`,
      {
        method: 'DELETE',
        ignoreProject: true,
      },
    );
  }

  // pvc
  async createPvc(
    cluster_id: string,
    payload: PvcPayload,
    clusters?: string[],
  ): Promise<any> {
    if (clusters && clusters.length) {
      const res: BatchReponse = await this.createPvcBatch(payload, clusters);
      this.k8sResourceService.reourceBatchNotification(
        'cluster_pvc_create',
        res,
        payload.metadata.name,
      );
      return;
    }
    return this.httpService.request(
      `${this.K8S_URL}${cluster_id}/persistentvolumeclaims/`,
      {
        method: 'POST',
        body: payload,
        addNamespace: false,
      },
    );
  }

  createPvcBatch(
    payload: PvcPayload,
    clusters: string[],
  ): Promise<BatchReponse> {
    const project_name = getCookie('project');
    const requests = clusters.map((cluster: string) => {
      return {
        cluster,
        url: `/v2/kubernetes/clusters/${cluster}/persistentvolumeclaims?project_name=${project_name}`,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      addNamespace: false,
      body: {
        common: {
          method: 'POST',
          header: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(payload),
        },
        requests: mapValues(
          keyBy(requests, (item: any) => {
            return item.cluster;
          }),
          (item: any) => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }

  getPvcs({
    cluster_id,
    page = 1,
    page_size = 20,
    name = '',
    namespace = '',
    ignoreProject = false,
  }: {
    cluster_id: string;
    page?: number;
    page_size?: number;
    name?: string;
    namespace?: string;
    ignoreProject?: boolean;
  }): Promise<any> {
    return this.httpService.request(
      `${this.K8S_URL}${cluster_id}/persistentvolumeclaims/${
        namespace ? namespace + '/' : ''
      }`,
      {
        method: 'GET',
        params: {
          page,
          page_size,
          name,
        },
        addNamespace: false,
        ignoreProject,
      },
    );
  }

  getPvcDetail(
    cluster_id: string,
    namespace: string,
    name: string,
  ): Promise<any> {
    return this.httpService.request(
      `${
        this.K8S_URL
      }${cluster_id}/persistentvolumeclaims/${namespace}/${name}`,
      {
        method: 'GET',
      },
    );
  }

  async getMirrorPvcs(
    clusterName: string,
    namespace: string,
    name: string,
  ): Promise<any> {
    const MirrorPvcs: MirrorPvc[] = [];
    const mirrorCluster = await this.k8sResourceService.getMirrorCluster(
      clusterName,
    );
    if (mirrorCluster.length) {
      const clusters = mirrorCluster.map((item: any) => {
        return item.name;
      });
      const res: BatchReponse = await this.k8sResourceService.getMirrorResources(
        clusters,
        namespace,
        'persistentvolumeclaims',
        name,
      );
      Object.entries(res.responses).forEach(([id, data]) => {
        if (data.code < 300) {
          const body = JSON.parse(data.body);
          const cluster = mirrorCluster.find(cluster => {
            return cluster.name === id;
          });
          const metadata = body.kubernetes.metadata;
          MirrorPvcs.push({
            uuid: metadata.uid,
            name: metadata.name,
            namespace_name: metadata.namespace,
            cluster_uuid: cluster ? cluster.id : '',
            cluster_name: cluster ? cluster.name : '',
            cluster_display_name: cluster ? cluster.display_name : '',
          });
        }
      });
    }
    return MirrorPvcs;
  }

  updatePvc(
    cluster_id: string,
    name: string,
    namespace: string,
    payload: PvcPayload,
  ) {
    return this.httpService.request(
      `${
        this.K8S_URL
      }/${cluster_id}/persistentvolumeclaims/${namespace}/${name}`,
      {
        method: 'PUT',
        body: payload,
      },
    );
  }

  async deletePvc(cluster_id: string, namespace: string, name: string) {
    return this.httpService.request(
      `${
        this.K8S_URL
      }${cluster_id}/persistentvolumeclaims/${namespace}/${name}`,
      {
        method: 'DELETE',
      },
    );
  }

  async deleteMirrorPvc(name: string, mirrorPvcs?: MirrorPvc[]) {
    if (mirrorPvcs && mirrorPvcs.length) {
      const res: BatchReponse = await this.deletePvcBatch(mirrorPvcs);
      this.k8sResourceService.reourceBatchNotification(
        'cluster_pvc_delete',
        res,
        name,
      );
      return;
    }
  }

  deletePvcBatch(mirrorPvcs: MirrorPvc[]): Promise<BatchReponse> {
    const project_name = getCookie('project');
    const requests = mirrorPvcs.map((item: MirrorPvc) => {
      return {
        cluster: item.cluster_name,
        url: `/v2/kubernetes/clusters/${
          item.cluster_name
        }/persistentvolumeclaims/${item.namespace_name}/${
          item.name
        }?project_name=${project_name}`,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      addNamespace: false,
      body: {
        common: {
          method: 'DELETE',
          header: {
            'Content-Type': 'application/json',
          },
        },
        requests: mapValues(
          keyBy(requests, (item: any) => {
            return item.cluster;
          }),
          (item: any) => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }

  // StorageClass
  createStorageClass(cluster_id: string, payload: any): Promise<StorageClass> {
    return this.httpService
      .request(`${this.K8S_URL}${cluster_id}/storageclasses/`, {
        method: 'POST',
        body: payload,
        addNamespace: false,
        ignoreProject: true,
      })
      .then((ret: { result: StorageClass[] }) => {
        if (ret.result && ret.result.length) {
          return ret.result[0];
        }
      });
  }

  getStorageClasses({
    cluster_id,
    page = 1,
    page_size = 20,
    name = '',
  }: {
    cluster_id: string;
    page: number;
    page_size: number;
    name?: string;
  }): Promise<any> {
    return this.httpService.request(
      `${this.K8S_URL}${cluster_id}/storageclasses/`,
      {
        method: 'GET',
        params: {
          page,
          page_size,
          name,
        },
        ignoreProject: true,
      },
    );
  }

  getStorageClassDetail(cluster_id: string, name: string) {
    return this.httpService.request(
      `${this.K8S_URL}${cluster_id}/storageclasses/${name}`,
      {
        method: 'GET',
        ignoreProject: true,
      },
    );
  }

  updateStorageClass(cluster_id: string, name: string, payload: any) {
    return this.httpService.request(
      `${this.K8S_URL}${cluster_id}/storageclasses/${name}`,
      {
        method: 'PUT',
        body: payload,
        addNamespace: false,
        ignoreProject: true,
      },
    );
  }

  deleteStorageClass(cluster_id: string, name: string) {
    return this.httpService.request(
      `${this.K8S_URL}${cluster_id}/storageclasses/${name}`,
      {
        method: 'DELETE',
        ignoreProject: true,
      },
    );
  }
}
