import { Injectable } from '@angular/core';
import { HttpService } from 'app2/shared/services/http/http.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TerminalPageParams } from 'terminal/app/type';
@Injectable()
export class TerminalService {
  constructor(
    private httpService: HttpService,
    private errorsToastService: ErrorsToastService,
  ) {}

  async openTerminal({
    pod,
    container,
    namespace,
    clusterId,
  }: TerminalPageParams) {
    const sid = await this.getSocketServer({
      pod,
      container,
      namespace,
      clusterId,
    });

    if (sid) {
      const terminalurl = `/terminal?pod=${pod}&container=${container}&namespace=${namespace}&clusterId=${clusterId}&sid=${sid}`;
      // Following is to center the new window.
      const w = 800;
      const h = 600;

      const dualScreenLeft =
        window.screenLeft !== undefined ? window.screenLeft : window.screenX;
      const dualScreenTop =
        window.screenTop !== undefined ? window.screenTop : window.screenY;

      const width = window.innerWidth
        ? window.innerWidth
        : document.documentElement.clientWidth
          ? document.documentElement.clientWidth
          : screen.width;
      const height = window.innerHeight
        ? window.innerHeight
        : document.documentElement.clientHeight
          ? document.documentElement.clientHeight
          : screen.height;

      const left = width / 2 - w / 2 + dualScreenLeft;
      const top = height / 2 - h / 2 + dualScreenTop;

      window.open(
        terminalurl,
        '_blank',
        `width=${w},height=${h},resizable=yes,left=${left},top=${top}`,
      );
    } else {
      // error
    }
  }

  getSocketServer(params: TerminalPageParams) {
    return this.httpService
      .request(
        `/ajax/v2/misc/clusters/${params.clusterId}/exec/${params.namespace}/${
          params.pod
        }/${params.container}`,
        {
          method: 'POST',
          addNamespace: false,
        },
      )
      .then(({ id }) => id)
      .catch(error => {
        this.errorsToastService.error(error);
        return null;
      });
  }
}
