import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams,
  HttpRequest,
} from '@angular/common/http';
import { Inject, Injectable, Optional } from '@angular/core';
import { TimeoutError } from 'rxjs';
import { timeout } from 'rxjs/operators';

import { MessageService, NotificationService } from 'alauda-ui';
import { ACCOUNT, TENCENTSSO, WEBLABS } from 'app2/core/tokens';
import {
  RcAccount,
  RcProfile,
  RcRegisterInfo,
  RcTencentSso,
  Weblabs,
} from 'app2/core/types';
import { LicenseTipModalComponent } from 'app2/layout/header-toolbar/license-tip/license-tip-modal.component';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { CookieService } from 'app2/shared/services/utility/cookie.service';
import { TranslateService } from 'app2/translate/translate.service';

const uuidv1 = require('uuid/v1');

/**
 * Mimic the error response object between rubick UI and Django (MathildeErrorResponse)
 */
export class ErrorResponse extends HttpErrorResponse {
  readonly errors?: [
    {
      code: string;
      source?: number;
      message?: string;
      fields?: any;
    }
  ];

  constructor(init: any, errors: any) {
    super(init);
    this.errors = errors;
  }
}

/**
 * Interface for request options.
 */
export interface RequestOptions<T> {
  method: string;

  /**
   * Parameters in the URL (search string)
   */
  params?: {
    [key: string]: any | any[];
  };

  /**
   * Whether to add namespace or not
   */
  addNamespace?: boolean;

  /**
   * Whether to delete project_name param
   */
  ignoreProject?: boolean;

  /**
   * Body, aka 'data' for legacy service.
   */
  body?: any;

  /**
   * Expected response type. Will be used to do decoding.
   */
  responseType?: HttpRequest<T>['responseType'];

  /**
   * Whether this request should be made in a way that exposes progress events.
   *
   * Progress events are expensive (change detection runs on each event) and so
   * they should only be requested if the consumer intends to monitor them.
   */
  reportProgress?: boolean;

  /**
   * Whether or not to cache the result?
   * Should only be valid for GET/HEAD request.
   *
   * Note, this will enable lifetime cache for the request. There is not way to clean up or update.
   */
  cache?: boolean;

  /**
   * Optional URL. Only be viable when the first request param is not URL.
   */
  url?: string;

  /**
   * Custom timeout in ms
   */
  timeout?: number;
}

const TIMEOUT_IN_MS = 30000; // 30 seconds
const HEADER_REQUEST_ID = 'Alauda-Request-ID';
const HEADER_AJAX_REQUEST = 'RUBICK-AJAX-REQUEST';
const HEADER_NO_JSON_PARSE = 'no-json';

/**
 * Utility function for fetching XHR requests
 */
@Injectable()
export class HttpService {
  private _cache = new Map<string, Promise<any>>();

  constructor(
    private http: HttpClient,
    private cookieService: CookieService,
    private translate: TranslateService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private modalService: ModalService,
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(TENCENTSSO)
    @Optional()
    public tencentSoo: RcTencentSso,
  ) {}

  /**
   * Wraps Angular 2 http service:
   *   - Add new headers
   *   - Map Observable to Promise
   *   - Handle exceptions
   *   - Handle timeout
   * @param url
   * @param options
   * @returns {Promise<any>}
   */
  request<T>(url: string, options?: RequestOptions<T>): Promise<T>;
  request<T>(options: RequestOptions<T>): Promise<T>;
  request(url: string, options?: RequestOptions<any>): Promise<any>;
  request(options: RequestOptions<any>): Promise<any>;

  request<T>(
    urlOrOptions: string | RequestOptions<T>,
    requestOptions: RequestOptions<T> = { method: 'GET' },
  ): Promise<T> {
    const { url, method, options } = this.prepareRequest(
      urlOrOptions,
      requestOptions,
    );

    const cacheKey = this.generateCacheKey(url, options.params);
    if (requestOptions.cache && this._cache.has(cacheKey)) {
      return this._cache.get(cacheKey);
    }

    const requestPromise = this.http
      .request(method, url, options)
      .pipe(
        timeout(
          requestOptions.timeout ? requestOptions.timeout : TIMEOUT_IN_MS,
        ),
      )
      .toPromise()
      .catch(async httpErrorResponse => {
        // In a real world app, we might use a remote logging infrastructure
        // Note: using TS 2.1 new feature 'lookup types' here.
        if (httpErrorResponse instanceof HttpErrorResponse) {
          // Log out the user when API returns 401
          if (httpErrorResponse.status === 401) {
            const redirectUrl =
              httpErrorResponse['redirectUrl'] ||
              this.tencentSoo.ssoLogoutUrl ||
              '/ap/logout';

            setTimeout(() => (window.location.href = redirectUrl), 0);
            const message = this.translate.get('logging_out');
            this.auiNotificationService.error(message);
          } else if (
            httpErrorResponse.status === 403 &&
            httpErrorResponse.error.errors &&
            httpErrorResponse.error.errors[0].code === 'expired'
          ) {
            if (document.getElementById('license-expired-modal-mask')) {
              return;
            }
            const [registerInfo, profile] = await Promise.all([
              this.request<RcRegisterInfo>(
                `/ajax/license-auth/${this.account.namespace}/registry-code`,
                {
                  cache: true,
                  method: 'GET',
                },
              ),
              this.request<RcProfile>('/ajax/auth/profile', {
                cache: true,
                method: 'GET',
              }),
            ]);

            this.modalService.open(LicenseTipModalComponent, {
              title: '',
              width: profile.is_admin ? 560 : 420,
              data: {
                isAdmin: profile.is_admin,
                productName: registerInfo.product_name,
              },
            });

            throw httpErrorResponse;
          }

          throw new ErrorResponse(
            httpErrorResponse,
            httpErrorResponse.error.errors,
          );
        } else if (httpErrorResponse instanceof TimeoutError) {
          //TODO::
          //wait for new tip logic
          this.auiMessageService.warning({
            content: this.translate.get('network_ex'),
          });
          throw httpErrorResponse;
        }
      });

    if (requestOptions.cache) {
      this._cache.set(cacheKey, requestPromise);
    }
    return requestPromise;
  }

  private prepareRequest<T>(
    urlOrOptions: string | RequestOptions<T>,
    requestOptions?: RequestOptions<T>,
  ) {
    let url: string;
    if (typeof urlOrOptions === 'object') {
      if (!urlOrOptions.url) {
        throw new Error('No URL is given!');
      }
      url = urlOrOptions.url;
      requestOptions = urlOrOptions;
    } else {
      url = urlOrOptions;
    }

    let addNamespace = requestOptions.addNamespace;
    if (addNamespace === undefined) {
      addNamespace = true;
    }

    const rawParams = requestOptions.params || {};
    if (!rawParams.namespace && addNamespace) {
      rawParams.namespace = this.account.namespace;
    }

    let params = Object.entries(requestOptions.params || {}).reduce(
      (accum, [key, value]) => {
        if (value != null) {
          accum = accum.append(key, value);
        }
        return accum;
      },
      new HttpParams(),
    );

    // For legacy issues, we also need to set namespace to data
    if (
      addNamespace &&
      requestOptions.body &&
      !(requestOptions.body instanceof FormData)
    ) {
      requestOptions.body.namespace = this.account.namespace;
    }

    if (this.weblabs.PROJECTS_ENABLED && !requestOptions.ignoreProject) {
      const projectName = this.cookieService.getCookie('project');
      if (projectName) {
        params = params.append('project_name', projectName);
      }
    }

    const requestHeaders = requestOptions['headers'];

    let request_id =
      (requestHeaders && requestHeaders[HEADER_REQUEST_ID]) || '';

    if (!request_id) {
      const buffer = new Array(32);
      uuidv1(null, buffer, 0).forEach((item: any) => {
        request_id += item.toString(16);
      });
    }

    let headers = new HttpHeaders()
      .set(HEADER_REQUEST_ID, request_id)
      .set(HEADER_AJAX_REQUEST, 'true');
    if (
      requestOptions.responseType &&
      (requestOptions.responseType === 'blob' ||
        requestOptions.responseType === 'arraybuffer')
    ) {
      headers = headers.set(HEADER_NO_JSON_PARSE, 'true');
    }

    return {
      url,
      method: requestOptions.method || 'GET',
      options: {
        body: requestOptions.body,
        params,
        responseType: requestOptions.responseType,
        headers: headers,
      },
    };
  }

  private generateCacheKey(url: string, params: any) {
    return (
      url +
      '/' +
      Object.entries(params)
        .map(([param, value]) => param + '=' + value)
        .join('&')
    );
  }
}
