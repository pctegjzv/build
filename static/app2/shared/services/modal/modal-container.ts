import {
  ComponentPortal,
  DomPortalHost,
  PortalInjector,
  TemplatePortal,
} from '@angular/cdk/portal';
import {
  ApplicationRef,
  ComponentFactoryResolver,
  Injector,
  TemplateRef,
  Type,
} from '@angular/core';
import ResizeSensor from 'css-element-queries/src/ResizeSensor';
import $ from 'jquery';
import { Observable, Subject } from 'rxjs';
import { first } from 'rxjs/operators';

import { ModalContainerComponent } from 'app2/shared/components/modal-container/modal-container.component';

import { MODAL_DATA, ModalMode, ModalOptions, ModalRef } from './modal.types';

let modalIdCounter = 0;

/**
 * Provide a container for residing a single Modal modalHost$.
 *
 * For now, the implementation is tight to Semantic-UI.
 */
export class ModalContainer {
  private result?: any;

  /**
   * Create an anchor ModalContainer.
   * @param options
   * @returns {ModalContainer}
   */
  static create(options: ModalOptions): ModalContainer {
    const modalContainer = ModalContainer.createRawContainer(options);

    // ModalContainer will be ready later
    Promise.resolve().then(() =>
      ModalContainer.configureContainerResize(modalContainer),
    );

    return modalContainer;
  }

  private static createRawContainer(options: ModalOptions): ModalContainer {
    const onHiddenSubject = new Subject();

    const MODAL_TEMPLATE = (width: number) => `
    <div class="ui modal rc-modal ${
      options.mode === ModalMode.RIGHT_SLIDER ? 'rc-modal--right' : ''
    }"
     style="width: ${width}px;"></div>
    `;

    // Firstly, create a bed for residing the targeting component:
    const modalHost$ = $(MODAL_TEMPLATE(options.width));

    const modalContainer = new ModalContainer(
      modalIdCounter++,
      onHiddenSubject.asObservable(),
      modalHost$,
    );
    modalHost$.find('.modal-close').on('click', () => modalContainer.close());

    // Prepare modal attributes:
    modalHost$.css('margin-left', -options.width / 2);

    const suiModalSettings = {
      transition:
        options.mode !== ModalMode.DEFAULT
          ? `fade ${options.mode}`
          : ModalMode.DEFAULT,
      allowMultiple: true,
      closable: options.closable,
      autofocus: options.autofocus,
      duration: 200,
      onHidden: () => {
        onHiddenSubject.next(modalContainer.result);
        onHiddenSubject.complete();
      },
    };

    // Init semantic-ui modal settings
    modalHost$.modal(suiModalSettings);

    return modalContainer;
  }

  private static configureContainerResize(modalContainer: ModalContainer) {
    const modalInstance = modalContainer.modalHost$;
    // Repositioning modal when window
    const resizeSensor = new ResizeSensor(modalInstance[0], () => {
      modalInstance.modal('refresh');
    });

    modalInstance.modal('refresh');

    // Register cleanup
    modalContainer.onHidden.pipe(first()).subscribe(() => {
      modalInstance.remove();
      resizeSensor.detach(null);
    });
  }

  open(): void {
    $('.ui.dimmer .rc-modal')
      .last()
      .prev()
      .append('<div class="blocking-mask"></div>');
    this.modalHost$.modal('show');
  }

  close(result?: any): void {
    this.result = result;
    this.modalHost$.modal('hide');
    $('.ui.dimmer .rc-modal .blocking-mask')
      .last()
      .remove();
  }

  /**
   * Create and attach the given component to the modal container
   */
  attach<T>(
    componentOrTemplateRef: Type<T> | TemplateRef<T>,
    injector: Injector,
    appRef: ApplicationRef,
    options: ModalOptions,
  ): ModalRef<T> {
    // Modal host element will only be available when modal is opened.
    this.open();
    // Prepare container's PortalHost. This PortalHost is used as an overlay (backdrop) as well.
    const modalContainerPortalHost = new DomPortalHost(
      this.modalHost$[0],
      injector.get(ComponentFactoryResolver),
      appRef,
      injector,
    );

    // Embed a ModalContainer component into portalHost
    const modalContainerRef = modalContainerPortalHost.attachComponentPortal(
      new ComponentPortal(ModalContainerComponent),
    );
    const modalContainer = modalContainerRef.instance;
    modalContainer.title = options.title;
    modalContainer.close.pipe(first()).subscribe(() => this.close()); // Don't emit values here

    const modalRef: ModalRef<T> = {
      id: this.id,
      afterClosed: this.onHidden,
      close: this.close.bind(this),
    };

    if (componentOrTemplateRef instanceof TemplateRef) {
      const portal = new TemplatePortal<any>(componentOrTemplateRef, null, {
        $implicit: options.data,
        modalRef,
      });
      modalContainer.attachTemplatePortal(portal);
    } else {
      // Prepare modal content injections
      const tokens = new WeakMap();
      tokens.set(ModalRef, modalRef);
      tokens.set(MODAL_DATA, options.data);
      const portalInjector = new PortalInjector(injector, tokens);
      const portal = new ComponentPortal(
        componentOrTemplateRef,
        null,
        portalInjector,
      );
      modalRef.componentInstance = modalContainer.attachComponentPortal(
        portal,
      ).instance;
    }

    this.onHidden.subscribe(() => {
      modalContainerPortalHost.dispose();
      modalRef.componentInstance = undefined;
    });

    return modalRef;
  }

  private constructor(
    public id: number,
    public onHidden: Observable<any>,
    private modalHost$: JQuery,
  ) {}
}
