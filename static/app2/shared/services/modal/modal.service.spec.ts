import { PortalModule } from '@angular/cdk/portal';
import { DOCUMENT } from '@angular/common';
import {
  ApplicationRef,
  Component,
  Input,
  NgModule,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { TestBed, inject } from '@angular/core/testing';
import { Subject, forkJoin } from 'rxjs';
import { first, share } from 'rxjs/operators';

import { ModalContainerComponent } from 'app2/shared/components/modal-container/modal-container.component';
import { delay, waterfall } from 'app2/testing/helpers';

import { ModalService } from './modal.service';
import { ModalOptions, ModalRef } from './modal.types';

@Component({
  template: `TEST COMPONENT <span class='testValue'>{{ testValue }}</span>`,
})
class TestModalComponent implements OnInit, OnDestroy {
  @Input()
  testValue: string;
  destroyed = new Subject();
  initialized = false;

  ngOnDestroy(): void {
    this.destroyed.next(true);
    this.destroyed.complete();
  }

  ngOnInit(): void {
    this.initialized = true;
  }
}

@NgModule({
  imports: [PortalModule],
  declarations: [TestModalComponent, ModalContainerComponent],
  entryComponents: [TestModalComponent, ModalContainerComponent],
  providers: [ModalService],
})
class ModalTestModule {}

describe('ModalService', () => {
  const TEST_TITLE = 'test_title';
  const TEST_VALUE = 'test_value';
  const TEST_WIDTH = 666;

  const DEFAULT_OPTIONS: ModalOptions = {
    title: TEST_TITLE,
    width: TEST_WIDTH,
  };

  let modal: ModalService;
  let dom: Document;
  let detectChanges: Function;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ModalTestModule],
    });
  });

  // Inject required services:
  beforeEach(inject(
    [ModalService, DOCUMENT, ApplicationRef],
    (m: ModalService, d: Document, a: ApplicationRef) => {
      modal = m;
      dom = d;
      detectChanges = () => a.tick();
    },
  ));

  // Calling done explicitly to make sure the modal is closed correctly
  afterEach(done => {
    if (modal.modalRefs.size > 0) {
      modal.afterAllClosed.subscribe(done);
      modal.closeAll();
    } else {
      done();
    }
  });

  it('should open a modal with a component', () => {
    const modalRef = modal.open(TestModalComponent, DEFAULT_OPTIONS);
    const component = modalRef.componentInstance;

    detectChanges();

    // Check title
    expect(
      dom.querySelector('.rc-modal > ng-component > .header > span')
        .textContent,
    ).toBe(TEST_TITLE);
    expect(component).not.toBeNull();

    component.testValue = TEST_VALUE;

    // Check the assigned Input value
    expect(component.testValue).toBe(TEST_VALUE);

    // Check if the component is rendered into the modal:
    expect(
      dom.querySelector('.rc-modal > ng-component > .content').innerHTML,
    ).toContain('TEST COMPONENT');
    expect(dom.querySelector('.rc-modal').clientWidth).toBe(TEST_WIDTH);

    // Should have to correctly value rendered
    expect(component.initialized).toBe(true);

    detectChanges();

    expect(
      dom.querySelector('.rc-modal > ng-component > .content .testValue')
        .textContent,
    ).toBe(TEST_VALUE);
  });

  // Utility to check various close methods
  async function checkCloseModal(
    closeFunction: (modalRef: ModalRef<TestModalComponent>) => void,
    done: Function,
  ) {
    const modalRef = modal.open(TestModalComponent, DEFAULT_OPTIONS);
    let afterClosedFlag = false;
    let destroyedFlag = false;
    modalRef.afterClosed.subscribe(() => (afterClosedFlag = true));
    modalRef.componentInstance.destroyed.subscribe(
      () => (destroyedFlag = true),
    );

    expect(afterClosedFlag).not.toBe(true);
    expect(destroyedFlag).not.toBe(true);

    closeFunction(modalRef);

    // Hmm ... dim transition is 200 by default
    await delay(500);
    expect(afterClosedFlag).toBe(true);

    detectChanges();

    // The component should be destroyed
    expect(destroyedFlag).toBe(true);

    // Modal should not be visible
    expect(dom.querySelector('.rc-modal')).toBeNull();
    done();
  }

  it('should be able to close a component: via ModalRef.close', done => {
    checkCloseModal(modalRef => modalRef.close(), done);
  });

  it('should be able to close a component: via clicking the close button', done => {
    checkCloseModal(
      () => $('.rc-modal > ng-component > .header > .modal-close').click(),
      done,
    );
  });

  it('should be able to close a component: via ModalService.closeAll', async done => {
    checkCloseModal(() => modal.closeAll(), done);
  });

  it('should be able to open multiple modals', async done => {
    const MODAL_NAMES = [0, 1, 2].map(id => `TEST_MODAL_NAME_${id}`);

    const modalRefs = MODAL_NAMES.map(modalName =>
      modal.open(TestModalComponent, { title: modalName }),
    );

    detectChanges();

    // Wait for all modal is opened.
    await delay(250);

    // Should have multiple modals up
    const openingModals = dom.querySelectorAll('.rc-modal');
    expect(openingModals.length).toBe(MODAL_NAMES.length);

    // The newest one should at the last one
    const lastIndex = openingModals.length - 1;

    expect(
      openingModals.item(lastIndex).querySelector('.header > span').textContent,
    ).toBe(MODAL_NAMES[lastIndex]);

    const allClosed = forkJoin(
      modalRefs.map(modalRef => modalRef.afterClosed),
    ).pipe(share());

    // Should be able to close window one by one:
    let openingCount = openingModals.length;

    // Close the opening modals in reverse order:
    waterfall(
      modalRefs.reverse().map(modalRef => async () => {
        modalRef.afterClosed.subscribe(() => {
          openingCount--;
        });
        modalRef.close();
        await delay(500);
      }),
    );

    await allClosed.pipe(first()).toPromise();

    // Should have all modal closed:
    expect(openingCount).toBe(0);

    done();
  });
});
