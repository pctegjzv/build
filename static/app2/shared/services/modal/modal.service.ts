import {
  ApplicationRef,
  Injectable,
  Injector,
  TemplateRef,
  Type,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { AlertBoxComponent } from 'app2/shared/components/alert-box/alert-box.component';
import { ConfirmBoxComponent } from 'app2/shared/components/confirm-box/confirm-box.component';

import { ModalContainer } from './modal-container';
import './modal.global.scss';
import {
  AlertBoxOptions,
  ConfirmBoxOptions,
  ModalOptions,
  ModalRef,
} from './modal.types';

/**
 * A service to render a component inside a modal.
 *
 * Inspired by legacy rbModal service and Angular Material 2 dialog.
 */
@Injectable()
export class ModalService {
  /* Default width of modals */
  static DEFAULT_WIDTH = 600;
  private afterAllClosedSubject = new Subject<void>();

  /* Current map of opening modals */
  modalRefs = new Map<number, ModalRef<any>>();

  /** Gets an observable that is notified when all open dialog have finished closing. */
  afterAllClosed: Observable<void> = this.afterAllClosedSubject.asObservable();

  constructor(
    private injector: Injector,
    private applicationRef: ApplicationRef,
  ) {}

  /**
   * Create and open a modal with the given ComponentType/TemplateRef and options.
   *
   * Note: Due to how {ComponentFactoryResolver} works,
   * the component type must be included in NgModule's entryComponents list.
   *
   * Usage:
   * const modalRef = modalService.open(SomeComponent, {
   *   title: 'some title'
   * });
   *
   * modalRef.afterClosed.subscribe(() => ... do something);
   *
   * Hints:
   * - You could interact with the component created dynamically with the Modal via modalRef.componentInstance.
   * - Two tokens will be provided in the dynamic created content: ModalRef and MODAL_DATA
   *   - ModalRef: you could directly access the modal ref API (mostly you want to close the modal)
   *   - MODAL_DATA: the 'data' field you provided in the ModalOptions. You could use it to init your component.
   */
  open<T = any>(
    componentOrTemplateRef: Type<T> | TemplateRef<T>,
    options: ModalOptions,
  ): ModalRef<T> {
    // Merge with default values:
    const combinedOptions = Object.assign(
      {
        width: ModalService.DEFAULT_WIDTH,
        closable: false,
        autofocus: false,
      },
      options,
    );

    const modalContainer = ModalContainer.create(combinedOptions);
    const modalRef = modalContainer.attach(
      componentOrTemplateRef,
      this.injector,
      this.applicationRef,
      options,
    );

    this.modalRefs.set(modalRef.id, modalRef);

    // Reset the lastModalRef when closed
    modalRef.afterClosed.subscribe(() => {
      this.modalRefs.delete(modalRef.id);

      if (this.modalRefs.size === 0) {
        this.afterAllClosedSubject.next();
      }
    });
    return modalRef;
  }

  /**
   * Close all modals
   */
  closeAll(): void {
    Array.from(this.modalRefs.values())
      .sort((modalRef0, modalRef1) => modalRef0.id - modalRef1.id)
      .forEach(modalRef => modalRef.close());
  }

  /**
   * Sugar of 'open' to show a confirm box.
   * @param options
   */
  confirm(options: ConfirmBoxOptions): Promise<void> {
    // We will let ConfirmBoxComponent component to render the title bar.
    const { title, content, confirmText } = options;
    delete options.title;
    delete options.content;
    const modalRef = this.open(ConfirmBoxComponent, {
      ...(options as ModalOptions),
      width: 560,
    });

    modalRef.componentInstance.title = title;
    modalRef.componentInstance.content = content;
    if (confirmText) {
      modalRef.componentInstance.confirmText = confirmText;
    }
    modalRef.componentInstance.confirmed.subscribe((res: boolean) => {
      modalRef.close(res);
    });

    return modalRef.afterClosed.toPromise().then(res => {
      if (!res) {
        throw res;
      }
    });
  }

  /**
   * Sugar of 'open' to show a confirm box.
   * @param options
   */
  alert(options: AlertBoxOptions): Promise<void> {
    // We will let AlertBoxComponent component to render the title bar.
    const { title, content } = options;
    delete options.title;
    delete options.content;
    const modalRef = this.open(AlertBoxComponent, {
      ...(options as ModalOptions),
      width: 560,
    });

    modalRef.componentInstance.title = title;
    modalRef.componentInstance.content = content;
    if (options.confirmText) {
      modalRef.componentInstance.confirmText = options.confirmText;
    }
    modalRef.componentInstance.confirmed.subscribe((res: boolean) => {
      modalRef.close(res);
    });

    return modalRef.afterClosed.toPromise().then(res => {
      if (!res) {
        throw res;
      }
    });
  }
}
