import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';

// Types related to Modal service

export const MODAL_DATA = new InjectionToken<string>('ModalData');

export enum ModalMode {
  DEFAULT = 'scale',
  RIGHT_SLIDER = 'left',
}

/**
 * Options for opening a Modal
 */
export class ModalOptions {
  /**
   * Title of the modal. This is required.
   */
  title: string;

  /**
   * Width of the modal. Default is 600.
   */
  width?: number;

  /**
   * Optional data to be passed into Modal component.
   * The value is accessible via token MODAL_DATA
   */
  data?: any | null;

  /**
   * Should close the modal when clicking the backdrop.
   * Default false.
   */
  closable?: boolean;

  /**
   * Should focus the modal when modal opened
   */
  autofocus?: boolean;

  /**
   * display sider modal or normal modal, default value is scale
   */
  mode?: ModalMode;
}

export class ConfirmBoxOptions extends ModalOptions {
  content?: string;

  /**
   * confirm button text,if null, use default
   */
  confirmText?: string;

  /**
   * cancel button text,if null, use default
   */
  cancelText?: string;
}

export class AlertBoxOptions extends ModalOptions {
  content?: string;

  /**
   * confirm button text,if null, use default
   */
  confirmText?: string;
}

/**
 * Reference to a modal opened via ModalService.
 */
export class ModalRef<T = any> {
  /**
   * The ID of this modal.
   */
  id: number;

  /**
   * The Component Instance.
   */
  componentInstance?: T;

  /**
   * Closed event. Should only emit one event.
   */
  afterClosed: Observable<any>;

  /**
   * Close this Modal.
   * @param res Optional result to be passed to the close event
   */
  close: (res?: any) => void;
}
