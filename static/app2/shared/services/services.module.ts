import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ServiceUtilitiesService } from 'app2/features/service/service-utilities.service';
import { AccountService } from 'app2/shared/services/features/account.service';
import { AlarmService } from 'app2/shared/services/features/alarm.service';
import { AppCatalogService } from 'app2/shared/services/features/app-catalog.service';
import { AppService } from 'app2/shared/services/features/app.service';
import { AppUtilitiesService } from 'app2/shared/services/features/app.utilities.service';
import { BuildService } from 'app2/shared/services/features/build.service';
import { CertificateService } from 'app2/shared/services/features/certificate.service';
import { ConfigmapService } from 'app2/shared/services/features/configmap.service';
import { ConfigmapUtilitiesService } from 'app2/shared/services/features/configmap.utilities.service';
import { DomainService } from 'app2/shared/services/features/domain.service';
import { EnvironmentService } from 'app2/shared/services/features/environment.service';
import { EventService } from 'app2/shared/services/features/event.service';
import { ImageProjectService } from 'app2/shared/services/features/image-project.service';
import { ImageRegistryService } from 'app2/shared/services/features/image-registry.service';
import { ImageRepositoryService } from 'app2/shared/services/features/image-repository.service';
import { IntegrationCenterService } from 'app2/shared/services/features/integration-center.service';
import { IntegrationService } from 'app2/shared/services/features/integration.service';
import { SonarQubeService } from 'app2/shared/services/features/integration.sonar-qube.service';
import { JenkinsService } from 'app2/shared/services/features/jenkins.service';
import { K8sResourceService } from 'app2/shared/services/features/k8s-resource.service';
import { LicenseService } from 'app2/shared/services/features/license.service';
import { LogAlarmService } from 'app2/shared/services/features/log-alarm.service';
import { LogService } from 'app2/shared/services/features/log.service';
import { MonitorService } from 'app2/shared/services/features/monitor.service';
import { MonitorUtilitiesService } from 'app2/shared/services/features/monitor.utilities.service';
import { NamespaceService } from 'app2/shared/services/features/namespace.service';
import { NetworkService } from 'app2/shared/services/features/network.service';
import { NotificationService } from 'app2/shared/services/features/notification.service';
import { OrgService } from 'app2/shared/services/features/org.service';
import { OssClientService } from 'app2/shared/services/features/oss-client.service';
import { PatternHelperService } from 'app2/shared/services/features/pattern-helper.service';
import { PipelineService } from 'app2/shared/services/features/pipeline.service';
import { PluginApiService } from 'app2/shared/services/features/plugin.service';
import { PodService } from 'app2/shared/services/features/pod.service';
import { PrivateBuildCodeClientService } from 'app2/shared/services/features/private-build-code-client.service';
import { ProjectService } from 'app2/shared/services/features/project.service';
import { QuotaSpaceService } from 'app2/shared/services/features/quota-space.service';
import { RBACService } from 'app2/shared/services/features/rbac.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { RegionUtilitiesService } from 'app2/shared/services/features/region.utilities.service';
import { ResourceService } from 'app2/shared/services/features/resource.service';
import { RSRCManagementService } from 'app2/shared/services/features/resrc-manage.service';
import { RoleService } from 'app2/shared/services/features/role.service';
import { ServiceCatalogService } from 'app2/shared/services/features/service-catalog.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { StorageService } from 'app2/shared/services/features/storage.service';
import { TerminalService } from 'app2/shared/services/features/terminal.service';
import { DefaultErrorMapperService } from 'app2/shared/services/form/default-error-mapper.service';
import { CookieService } from 'app2/shared/services/utility/cookie.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { LoggerUtilitiesService } from 'app2/shared/services/utility/logger.service';
import { TitleCaseService } from 'app2/shared/services/utility/title-case.service';
import { YamlCommentParserService } from 'app2/shared/services/yaml/yaml-comment-parser.service';

import { RoleUtilitiesService } from '../services/features/role-utilities.service';

import { BuildUtilitiesService } from './features/build.utilities.service';
import { ImageRepositoryUtilitiesService } from './features/image-repository.utilities.service';
import { NetworkUtilitiesService } from './features/network.utilities.service';
import { HttpService } from './http/http.service';
import { ModalService } from './modal/modal.service';

const FEATURE_PROVIDERS = [
  OrgService,
  AccountService,
  ProjectService,
  RegionService,
  ResourceService,
  PluginApiService,
  RoleService,
  RoleUtilitiesService,
  LogService,
  MonitorService,
  MonitorUtilitiesService,
  LogAlarmService,
  PipelineService,
  BuildService,
  EventService,
  AlarmService,
  StorageService,
  QuotaSpaceService,
  ServiceService,
  ServiceUtilitiesService,
  AppService,
  AppUtilitiesService,
  PodService,
  NamespaceService,
  RegionUtilitiesService,
  ConfigmapService,
  ConfigmapUtilitiesService,
  NetworkService,
  NetworkUtilitiesService,
  CertificateService,
  AppCatalogService,
  ServiceCatalogService,
  IntegrationService,
  RBACService,
  IntegrationService,
  PrivateBuildCodeClientService,
  ImageRegistryService,
  ImageProjectService,
  ImageRepositoryService,
  JenkinsService,
  LicenseService,
  NotificationService,
  ImageRepositoryUtilitiesService,
  BuildUtilitiesService,
  OssClientService,
  IntegrationCenterService,
  EnvironmentService,
  SonarQubeService,
  TerminalService,
  K8sResourceService,
  RSRCManagementService,
  DomainService,
];

@NgModule({
  providers: [
    ModalService,
    HttpService,
    DefaultErrorMapperService,
    TitleCaseService,
    CookieService,
    LoggerUtilitiesService,
    YamlCommentParserService,
    ErrorsToastService,
    PatternHelperService,
    ...FEATURE_PROVIDERS,
  ],
  imports: [HttpClientModule],
})
export class ServicesModule {}
