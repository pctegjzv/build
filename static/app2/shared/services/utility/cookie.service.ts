import { Injectable } from '@angular/core';
import { getCookie, setCookie } from 'app2/utils/cookie';
@Injectable()
export class CookieService {
  constructor() {}

  // TODO: remove these...
  getCookie(name: string): string {
    return getCookie(name);
  }

  setCookie(k: string, v: any) {
    return setCookie(k, v);
  }
}
