/**
 * Function to generate a promise to be resolved after the given timeout.
 * @param delay
 * @returns {Promise<void>}
 */
export function delay(delay?: number): Promise<void> {
  return new Promise<void>(res => setTimeout(() => res(), delay));
}
