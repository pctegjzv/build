import { Injectable } from '@angular/core';
import { escape, get, sortBy } from 'lodash';

import { MessageService, NotificationService } from 'alauda-ui';
import { ErrorResponse } from 'app2/shared/services/http/http.service';
import { TranslateService } from 'app2/translate/translate.service';

export interface ErrorsItem {
  code: string;
  source?: number;
  message?: string;
  fields?: any;
}

@Injectable()
export class ErrorsToastService {
  constructor(
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
  ) {}

  /**
   * 处理用用的错误代码，ErrorResponse中 errors字段表示API返回的错误（参考ErrorResponse定义），通常我们只关心第一个errors[0]
   * 每个error 包含如下结构：
   * {
      code: string;
      source?: number;
      message?: string;
      fields?: any;
    }
   * 这里应该只处理 在多个业务场景下都存在的error 比如 partial_permission_denied 的权限问题
   * 处理过的error 将从源errors中移除，在业务组件中可以继续处理没有匹配到的error
   * 包含在ignoredCodes 中的error code将不会被处理
   * @param {{errors: Array<any>; ignoredCodes?: Array<string>; handleNonGenericCodes?: boolean}} params
   * @returns {Array<any>}
   */
  handleGenericAjaxError({
    errors,
    ignoredCodes,
    handleNonGenericCodes = true,
    fallbackMessage,
  }: {
    errors: Array<ErrorsItem>;
    ignoredCodes?: Array<string>;
    handleNonGenericCodes?: boolean; // 若为true， 将会处理所有的error code，业务组件里就不需要额外处理了。 默认开启。
    fallbackMessage?: string;
  }) {
    const error = this.parseErrors({
      errors,
      ignoredCodes,
      handleNonGenericCodes,
      fallbackMessage,
    });
    if (error.title || error.content) {
      this.auiNotificationService.error({
        title: error.title,
        content: error.content,
      });
      errors.shift();
    }
    return errors;
  }

  parseErrors({
    errors,
    ignoredCodes,
    handleNonGenericCodes = true,
    fallbackMessage,
  }: {
    errors: Array<ErrorsItem>;
    ignoredCodes?: Array<string>;
    handleNonGenericCodes?: boolean; // 若为true， 将会处理所有的error code，业务组件里就不需要额外处理了。 默认开启。
    fallbackMessage?: string;
  }): {
    title?: string;
    content?: string;
  } {
    let errorContent: string, errorTitle: string;
    if (!errors || !errors.length) {
      if (fallbackMessage) {
        this.auiMessageService.error({
          content: fallbackMessage,
        });
      }
      return {};
    }
    // We just handle the first error
    const error = errors[0];
    if (ignoredCodes && ignoredCodes.includes(error.code)) {
      return {};
    }
    if (error.code === 'partial_permission_denied') {
      // code 'partial_permission_denied' used for rbac, parse message instead
      const messageFields = JSON.parse(error.message);
      errorTitle = this.translateService.get(error.code);
      errorContent = messageFields
        .map((field: any) => {
          return `${field.action} (${field.name})`;
        })
        .join('<br>');
    } else if (
      ['project_resources_exist', 'child_resources_exist'].includes(error.code)
    ) {
      // code 'project_resources_exist' used for project delete, parse message instead
      // code 'child_resources_exist' used for resource delete (when sub resources exist)
      let messageFields = JSON.parse(error.message);
      messageFields = sortBy(messageFields, (item: any) => {
        return item.type;
      });
      errorTitle = this.translateService.get(error.code);
      errorContent = messageFields
        .map((field: any) => {
          return `${this.translateService.get(field.type.toLowerCase())}: ${
            field.name
          }`;
        })
        .join('<br>');
    } else if (handleNonGenericCodes) {
      let message;

      if (
        [
          'unknown_issue',
          'server_error',
          'invalid_args',
          'kubernetes_error',
        ].includes(error.code)
      ) {
        message = error.message;
      } else {
        const translatedCode = this.translateService.get(error.code);
        message =
          error.code === translatedCode
            ? escape(error.message)
            : translatedCode;
      }
      errorContent = message;
    }
    return {
      title: errorTitle,
      content: errorContent,
    };
  }

  error(rejection: any) {
    if (!(rejection instanceof ErrorResponse)) {
      if (rejection && process.env.NODE_ENV === 'development') {
        console.error(rejection);
      }
      return;
    }

    if (rejection.errors) {
      return this.handleGenericAjaxError({ errors: rejection.errors });
    }

    this.auiNotificationService.error({
      content: this.translateService.get(
        get(rejection, 'error.message') ||
          this.translateService.get('unknown_issue'),
      ),
    });
  }
}
