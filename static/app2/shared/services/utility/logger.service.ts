import { Injectable } from '@angular/core';

@Injectable()
export class LoggerUtilitiesService {
  private logger: any;

  constructor() {
    this.logger = window.console || {};
  }

  log(...args: any[]) {
    const logFn = this.logger.log || (() => {});
    return logFn.apply(this.logger, args);
  }

  error(...args: any[]) {
    const logFn = this.logger.error || (() => {});
    return logFn.apply(this.logger, args);
  }

  noop(...args: any[]) {
    const logFn = () => {};
    return logFn.apply(null, args);
  }
}
