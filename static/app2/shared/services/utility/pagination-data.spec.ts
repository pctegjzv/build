import { delay } from 'app2/testing/helpers';

import { FetchDataResult, PaginationDataWrapper } from './pagination-data';

const ALL_DATA: number[] = [];
const COUNT = 100;

for (let i = 0; i < COUNT; i++) {
  ALL_DATA.push(i);
}

const ERROR = new Error('error');

let shouldThrowError = false;
async function fetch(
  pageNo: number,
  pageSize: number,
): Promise<FetchDataResult<number>> {
  await delay(100);
  if (shouldThrowError) {
    throw ERROR;
  } else {
    return {
      results: ALL_DATA.slice((pageNo - 1) * pageSize, pageNo * pageSize),
      count: COUNT,
    };
  }
}

describe('PaginationDataWrapper', () => {
  beforeEach(() => {
    shouldThrowError = false;
  });

  it('default page size is 20', async done => {
    const paginationDataWrapper = new PaginationDataWrapper(fetch);
    const pagination = await paginationDataWrapper.setPageNo(1);
    expect(pagination.pageSize).toBe(20);
    done();
  });

  it('will fetch correct data when setPageNo', async done => {
    const RESULTS = [3, 4, 5];
    const PAGE_SIZE = 3;
    const PAGE_NO = 2;

    const EXPECTED_RESULT = {
      pageSize: PAGE_SIZE,
      pageNo: PAGE_NO,
      count: COUNT,
      results: RESULTS,
    };

    const paginationDataWrapper = new PaginationDataWrapper(fetch, PAGE_SIZE);
    expect(paginationDataWrapper.loading).toBe(false);
    const paginationPromise = paginationDataWrapper.setPageNo(PAGE_NO);
    expect(paginationDataWrapper.loading).toBe(true);
    expect(await paginationPromise).toEqual(EXPECTED_RESULT);
    expect(paginationDataWrapper.loading).toBe(false);

    paginationDataWrapper.pagination.subscribe(pagination => {
      expect(pagination).toEqual(EXPECTED_RESULT);
      done();
    });
  });

  it('loadError = true when fetch has error', async done => {
    const RESULTS = [3, 4, 5];
    const PAGE_SIZE = 3;
    const PAGE_NO = 2;

    const paginationDataWrapper = new PaginationDataWrapper(fetch, PAGE_SIZE);

    // Result validation is checked in another test
    await paginationDataWrapper.setPageNo(PAGE_NO);

    shouldThrowError = true;
    let pagination = await paginationDataWrapper.setPageNo(PAGE_NO);

    expect(pagination).toEqual({
      pageSize: PAGE_SIZE,
      pageNo: PAGE_NO,
      count: COUNT,
      // Should still return the last cached results:
      results: RESULTS,
      loadError: ERROR,
    });
    pagination = await paginationDataWrapper.setPageNo(PAGE_NO + 1);
    expect(pagination).toEqual({
      pageSize: PAGE_SIZE,
      pageNo: PAGE_NO + 1,
      count: COUNT,
      results: [],
      loadError: ERROR,
    });

    done();
  });
});
