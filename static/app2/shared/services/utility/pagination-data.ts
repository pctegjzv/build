import { isEqual } from 'lodash';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

/**
 * The Pagination object to be used by the user.
 */
export interface Pagination<T> {
  pageSize: number;
  pageNo: number;
  count: number;
  results: T[];
  loadError?: any;
}

/**
 * Fetch data for paginated API should has the following structure.
 */
export interface FetchDataResult<T> {
  count: number;
  results: T[];
}

export type PaginationFetchFunction<T> = (
  pageNo: number,
  pageSize: number,
  params: any,
) => Promise<FetchDataResult<T>>;

/**
 * Utility class to do data fetching for pagination data.
 */
export class PaginationDataWrapper<T> {
  private _refetchData: PaginationFetchFunction<T>;
  private _params: any = {};
  private _pageSize: number;
  private _pageNo = 1;
  private _count = 0;
  private _results: T[] = [];
  private _loading = false;
  private _paginationSubject: Subject<Pagination<T>>;
  private _pagination: Observable<Pagination<T>>;
  private _data: Observable<T[]>;
  private _loadError: Observable<any>;

  /**
   * Pagination observables.
   */
  get pagination() {
    return this._pagination;
  }

  get data() {
    return this._data;
  }

  get loadError() {
    return this._loadError;
  }

  /**
   * Current page number.
   * @returns {number}
   */
  get pageNo() {
    return this._pageNo;
  }

  /**
   * Loading state
   */
  get loading(): boolean {
    return this._loading;
  }

  /**
   * Current page size.
   * @returns {number}
   */
  get pageSize() {
    return this._pageSize;
  }

  /**
   * Set search query and refetch data.
   *
   * @param params
   * @returns {Promise<void>}
   */
  setParams(params: any): Promise<Pagination<T>> {
    return this.onParamsChanged(params);
  }

  /**
   * Set page number and refetch data.
   * Page number starts from 1.
   * Note, data will be refetched even page number is not changed.
   *
   * @param pageNo
   */
  setPageNo(pageNo: number): Promise<Pagination<T>> {
    return this.onPageNoChanged(pageNo);
  }

  /**
   * Set page size and refetch data.
   * @param pageSize
   */
  setPageSize(pageSize: number): Promise<Pagination<T>> {
    return this.onPageSizeChanged(pageSize);
  }

  /**
   * Reload pagination data based on the current params.
   */
  async refetch(
    pageNo: number = this._pageNo,
    params: any = this._params,
    pageSize: number = this._pageSize,
  ) {
    const getCurrentRequestPayload = () => {
      return {
        pageNo: this._pageNo,
        pageSize: this._pageSize,
        params: this._params,
      };
    };

    const prevPayload = getCurrentRequestPayload();

    this._pageNo = pageNo;
    this._pageSize = pageSize;
    this._params = params;
    this._loading = true;

    const payload = getCurrentRequestPayload();
    const payloadChanged = !isEqual(payload, prevPayload);

    const pagination: Pagination<T> = {
      pageNo,
      pageSize: this._pageSize,
      count: this._count,

      // We should wipe the results whenever payload is changed
      results: payloadChanged ? [] : this._results,
    };

    try {
      const { count, results } = await this._refetchData(
        payload.pageNo,
        payload.pageSize,
        payload.params,
      );
      pagination.results = results;
      pagination.count = count;
    } catch (error) {
      pagination.loadError = error;
    }

    // Cache the results
    // We should trust the last returned count number is correct.
    this._count = pagination.count;

    // Should only emit results if payload is the latest
    if (isEqual(payload, getCurrentRequestPayload())) {
      this._results = pagination.results;
      this._loading = false;
      this._paginationSubject.next(pagination);
    }

    return pagination;
  }

  /**
   * User should provide implementation for refetch function.
   */
  constructor(refetchData: PaginationFetchFunction<T>, pageSize: number = 20) {
    this._refetchData = refetchData;
    this._pageSize = pageSize;

    this._paginationSubject = new Subject();

    this._pagination = this._paginationSubject.asObservable();
    this._data = this._pagination.pipe(map(({ results }) => results));
    this._loadError = this._pagination.pipe(map(({ loadError }) => loadError));
  }

  private async onParamsChanged(params: any): Promise<Pagination<T>> {
    const pagination = await this.refetch(1, {
      ...this._params,
      ...params,
    });
    return pagination;
  }

  /**
   * Update embedded pagination page number
   */
  private onPageNoChanged(pageNo: number): Promise<Pagination<T>> {
    return this.refetch(pageNo, this._params);
  }

  /**
   * Update page size
   */
  private onPageSizeChanged(pageSize: number): Promise<Pagination<T>> {
    return this.refetch(this._pageNo, this._params, pageSize);
  }
}
