import { TestBed, inject } from '@angular/core/testing';
import { TitleCaseService } from 'app2/shared/services/utility/title-case.service';

describe('TitleCaseService', () => {
  let titleCase: TitleCaseService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TitleCaseService],
    });
  });

  beforeEach(inject([TitleCaseService], (t: TitleCaseService) => {
    titleCase = t;
  }));

  it('undefined', () => {
    expect(titleCase.capitalize()).toBe('');
  });

  it('empty string', () => {
    expect(titleCase.capitalize('')).toBe('');
  });

  it('yet another markup language => Yet Another Markup Language', () => {
    expect(titleCase.capitalize('yet another markup language')).toBe(
      'Yet Another Markup Language',
    );
  });

  it('it is an apple => It is an Apple', () => {
    expect(titleCase.capitalize('it is an apple')).toBe('It is an Apple');
  });
});
