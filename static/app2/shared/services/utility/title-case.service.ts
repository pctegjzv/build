import { Injectable } from '@angular/core';

@Injectable()
export class TitleCaseService {
  readonly smallWords = [
    'a',
    'amid',
    'an',
    'and',
    'am',
    'apud',
    'are',
    'as',
    'at',
    'atop',
    'but',
    'by',
    'down',
    'for',
    'from',
    'in',
    'into',
    'is',
    'lest',
    'like',
    'mid',
    'near',
    'nor',
    'of',
    'off',
    'on',
    'onto',
    'or',
    'out',
    'over',
    'pace',
    'past',
    'per',
    'plus',
    'pro',
    'qua',
    'sans',
    'save',
    'so',
    'than',
    'the',
    'thru',
    'till',
    'to',
    'unto',
    'up',
    'upon',
    'via',
    'vice',
    'with',
    'yet',
  ];

  /**
   * Capitalize a single word
   */
  capitalizeWord(s: string): string {
    if (/[A-Z]/.test(s)) {
      return s;
    } else {
      return s.charAt(0).toUpperCase() + s.substr(1).toLowerCase();
    }
  }

  /**
   * Capitalize a sentence
   */
  capitalize(sentence?: string): string {
    if (!sentence) {
      return '';
    }
    let output = sentence.toString();

    // make each word capitalize
    output = output.replace(
      /([^\W_]+[^\s-]*) */g,
      this.capitalizeWord.bind(this),
    );

    // Certain small words should be left lowercase unless they are the first or
    // last words in the string.
    this.smallWords.forEach(word => {
      const regexp = new RegExp('\\s' + this.capitalizeWord(word) + '\\s', 'g');
      output = output.replace(regexp, s => s.toLowerCase());
    });

    return output;
  }
}
