import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TooltipModule } from 'alauda-ui';
import { EventListComponent } from 'app2/features/event/list/event-list.component';
import { ServiceImageDisplayComponent } from 'app2/features/service/image-display/image-display.component';
import { ServiceImageListComponent } from 'app2/features/service/image-display/image-list.component';
import { ImageSelectComponent } from 'app2/features/service/image-select/image-select.component';
import { ReferencedServiceListComponent } from 'app2/features/service/referenced-service/referenced-service-list.component';
import { TranslateModule } from 'app2/translate/translate.module';

import { ComponentsModule } from './components/components.module';
import { DirectivesModule } from './directives/directives.module';
import { PipesModule } from './pipes/pipes.module';

const FEATURE_COMPONENTS = [
  ReferencedServiceListComponent,
  ServiceImageDisplayComponent,
  ServiceImageListComponent,
  EventListComponent,
  ImageSelectComponent,
];

/**
 * Feature component used by more than 2 other feature modules should be declared here
 * And import SharedFeaturesModule from other feature modules
 */
@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    ComponentsModule,
    DirectivesModule,
    PipesModule,
    TooltipModule,
  ],
  declarations: [...FEATURE_COMPONENTS],
  exports: [...FEATURE_COMPONENTS],
  entryComponents: [ImageSelectComponent],
})
export class SharedFeaturesModule {}
