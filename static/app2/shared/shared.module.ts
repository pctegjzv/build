import { PortalModule } from '@angular/cdk/portal';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MATERIAL_SANITY_CHECKS } from '@angular/material';
import {
  ButtonModule,
  CardModule,
  CheckboxModule,
  CodeEditorModule,
  DialogModule,
  DropdownModule,
  FormFieldModule,
  FormModule,
  IconModule,
  InlineAlertModule,
  InputModule,
  NavModule,
  PaginatorModule,
  RadioModule,
  SelectModule,
  SortModule,
  SwitchModule,
  TableModule,
  TabsModule,
  TagModule,
  TooltipModule,
} from 'alauda-ui';

import { ModalService } from 'app2/shared/services/modal/modal.service';
import { SharedFeaturesModule } from 'app2/shared/shared-features.module';
import { TranslateModule } from 'app2/translate/translate.module';

import { ComponentsModule } from './components/components.module';
import { DirectivesModule } from './directives/directives.module';
import { PipesModule } from './pipes/pipes.module';

const NG_MODULES = [CommonModule, FormsModule, ReactiveFormsModule];

const CUSTOM_MODULES = [ComponentsModule, DirectivesModule, PipesModule];

const THIRD_PARTY_MODULES = [FlexLayoutModule, PortalModule];

const AUI_MODULES = [
  IconModule,
  ButtonModule,
  SortModule,
  TableModule,
  CodeEditorModule,
  CardModule,
  TooltipModule,
  DialogModule,
  FormFieldModule,
  InputModule,
  CheckboxModule,
  RadioModule,
  FormModule,
  DropdownModule,
  SelectModule,
  PaginatorModule,
  TagModule,
  TabsModule,
  InlineAlertModule,
  NavModule,
  SwitchModule,
];

const EXPORTABLE_MODULES = [
  SharedFeaturesModule,
  ...NG_MODULES,
  ...CUSTOM_MODULES,
  ...THIRD_PARTY_MODULES,
  ...AUI_MODULES,
];

/**
 * 共享特性模块
 *
 * Do declare components, directives, and pipes in a shared module
 * when those items will be re-used and referenced by the components declared in other feature modules.
 *
 * 坚持把可能被应用其它特性模块使用的公共组件、指令和管道放在SharedModule中，
 * 这些资产倾向于共享自己的新实例（而不是单例）。
 *
 * 坚持在SharedModule中导入所有模块都需要的资产（例如CommonModule和FormsModule）。
 *
 * 坚持在SharedModule中声明所有组件、指令和管道。
 *
 * 坚持从SharedModule中导出其它特性模块所需的全部符号。
 *
 * 避免在SharedModule中指定应用级的单例服务提供商。但如果是故意设计的单例也可以，不过还是要小心。
 */
@NgModule({
  imports: [...EXPORTABLE_MODULES, TranslateModule],
  exports: [...EXPORTABLE_MODULES, TranslateModule],
  providers: [
    {
      provide: MATERIAL_SANITY_CHECKS,
      useValue: false,
    },
    ModalService,
  ],
})
export class SharedModule {}
