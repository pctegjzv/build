// Fix error: Uncaught reflect-metadata shim is required when using class decorators
import { getTestBed } from '@angular/core/testing';
import {
  BrowserDynamicTestingModule,
  platformBrowserDynamicTesting,
} from '@angular/platform-browser-dynamic/testing';
import 'angular';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'expose-loader?$!expose-loader?jQuery!jquery';
import 'font-awesome/css/font-awesome.min.css';
import 'reflect-metadata';
import 'semantic-ui-button/button.min.css';
import 'semantic-ui-checkbox/checkbox.min.css';
import 'semantic-ui-checkbox/checkbox.min.js';
import 'semantic-ui-dimmer/dimmer.min.css';
import 'semantic-ui-dimmer/dimmer.min.js';
import 'semantic-ui-input/input.min.css';
import 'semantic-ui-loader/loader.min.css';
import 'semantic-ui-modal/modal.js';
import 'semantic-ui-modal/modal.min.css';
import 'semantic-ui-popup/popup.min.css';
import 'semantic-ui-popup/popup.min.js';
import 'semantic-ui-transition/transition.min.css';
import 'semantic-ui-transition/transition.min.js';
import 'zone.js';
import 'zone.js/dist/async-test';
import 'zone.js/dist/fake-async-test';
import 'zone.js/dist/jasmine-patch';
import 'zone.js/dist/long-stack-trace-zone';
import 'zone.js/dist/proxy.js';
import 'zone.js/dist/sync-test';

import './core/styles/rubick.global.scss';

// Initialize the Angular testing environment.
getTestBed().initTestEnvironment(
  BrowserDynamicTestingModule,
  platformBrowserDynamicTesting(),
);
