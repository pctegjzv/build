import { Action } from '@ngrx/store';

export const SET_ACCOUNT = `@account/SET_ACCOUNT`;

export class SetAccount implements Action {
  public readonly type = SET_ACCOUNT;
  constructor(
    public payload: {
      namespace: string;
      username: string;
    },
  ) {}
}

export type All = SetAccount;
