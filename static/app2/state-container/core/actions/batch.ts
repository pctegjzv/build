import { Action } from '@ngrx/store';

export const BATCH = `[batch] BATCH`;

export class Batch implements Action {
  public readonly type = BATCH;
  constructor(public payload: Action[]) {}
}

export type All = Batch;
