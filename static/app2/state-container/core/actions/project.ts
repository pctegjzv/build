import { Action } from '@ngrx/store';
import { Project } from 'app2/shared/services/features/project.service';

export const LOAD_PROJECTS = `@project/LOAD_PROJECTS`;
export const LOAD_PROJECTS_SUCCESS = `@project/LOAD_PROJECTS_SUCCESS`;
export const LOAD_PROJECTS_FAIL = `@project/LOAD_PROJECTS_FAIL`;
export const CREATE_PROJECT_SUCCESS = `@project/CREATE_PROJECT_SUCCESS`;
export const DELETE_PROJECT_SUCCESS = `@project/DELETE_PROJECT_SUCCESS`;
export const SET_PROJECT = `@project/SET_PROJECT`;

export class LoadProjects implements Action {
  public readonly type = LOAD_PROJECTS;
}
export class LoadProjectsSuccess implements Action {
  public readonly type = LOAD_PROJECTS_SUCCESS;
  constructor(public payload: Project[]) {}
}
export class LoadProjectsFail implements Action {
  public readonly type = LOAD_PROJECTS_FAIL;
  constructor(public payload: any) {}
}
export class CreateProjectSuccess implements Action {
  public readonly type = CREATE_PROJECT_SUCCESS;
  constructor(public payload: Project) {}
}
export class DeleteProjectSuccess implements Action {
  public readonly type = DELETE_PROJECT_SUCCESS;
  constructor(public payload: string) {}
}
export class SetProject implements Action {
  public readonly type = SET_PROJECT;
  constructor(public payload: string) {}
}

export type ProjectActions =
  | LoadProjects
  | LoadProjectsSuccess
  | LoadProjectsFail
  | CreateProjectSuccess
  | DeleteProjectSuccess
  | SetProject;
