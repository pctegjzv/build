import { Inject, Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';

import * as accountActions from '../actions/account';

@Injectable()
export class AccountEffects {
  @Effect()
  setAccount$: Observable<Action> = of(
    new accountActions.SetAccount(this.account),
  );

  constructor(@Inject(ACCOUNT) private account: RcAccount) {}
}
