import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { from, of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import {
  Project,
  ProjectService,
} from 'app2/shared/services/features/project.service';
import { setCookie } from 'app2/utils/cookie';

import * as projectActions from '../actions/project';

@Injectable()
export class ProjectEffects {
  @Effect()
  loadProjects$ = this.actions$.ofType(projectActions.LOAD_PROJECTS).pipe(
    switchMap(() => {
      return from(this.projectService.getProjects(false)).pipe(
        map(
          (projects: Project[]) =>
            new projectActions.LoadProjectsSuccess(projects),
        ),
        catchError(error => of(new projectActions.LoadProjectsFail(error))),
      );
    }),
  );

  @Effect({ dispatch: false })
  setProjectName$ = this.actions$.ofType(projectActions.SET_PROJECT).pipe(
    tap((action: projectActions.SetProject) => {
      setCookie('project', action.payload);
    }),
  );

  constructor(
    private projectService: ProjectService,
    private actions$: Actions,
  ) {}
}
