import * as accountActions from '../actions/account';

export interface AccountState {
  namespace: string;
  username: string;
}

const initialState: AccountState = { namespace: null, username: null };

export function accountReducer(
  state: AccountState = initialState,
  action: accountActions.All,
): AccountState {
  switch (action.type) {
    case accountActions.SET_ACCOUNT:
      return action.payload;
    default:
      return state;
  }
}
