import {
  ActionReducer,
  ActionReducerMap,
  MetaReducer,
  createFeatureSelector,
} from '@ngrx/store';

import * as batchActions from '../actions/batch';

// import { RouterStateUrl } from '../utils/router';
// import * as fromRouter from '@ngrx/router-store';
import { AccountState, accountReducer } from './account';
import { ProjectState, projectReducer } from './project';

/**
 * As mentioned, we treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface State {
  // router: fromRouter.RouterReducerState<RouterStateUrl>;
  account: AccountState;
  project: ProjectState;
}

/**
 * Our state is composed of a map of action reducer functions.
 * These reducer functions are called with each dispatched action
 * and the current or initial state and return a new immutable state.
 */
export const reducers: ActionReducerMap<State> = {
  // router: fromRouter.routerReducer,
  account: accountReducer,
  project: projectReducer,
};

// console.log all actions
/*export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
  return function(state: State, action: any): State {
    console.log('state', state);
    console.log('action', action);

    return reducer(state, action);
  };
}
*/
// batch
export function batch(reducer: ActionReducer<State>): ActionReducer<State> {
  return function(state: State, action: any): State {
    if ((<batchActions.Batch>action).type === batchActions.BATCH) {
      return action.payload.reduce(reducer, state);
    }

    return reducer(state, action);
  };
}

/**
 * By default, @ngrx/store uses combineReducers with the reducer map to compose
 * the root meta-reducer. To add more meta-reducers, provide an array of meta-reducers
 * that will be composed to form the root meta-reducer.
 */
export const metaReducers: MetaReducer<State>[] = [batch];

export const getEntities = (...entityKeys: string[]) => (state: any) => {
  return entityKeys.reduce(
    (prev, key) => ({ ...prev, [key]: state[key].entities }),
    {},
  );
};

export const getProjectState = createFeatureSelector<State, ProjectState>(
  'project',
);
