import { Project } from 'app2/shared/services/features/project.service';
import { getCookie } from 'app2/utils/cookie';

import * as fromProject from '../actions/project';
export interface ProjectState {
  entities: { [name: string]: Project };
  selectedProjectName: string;
}

const initialState: ProjectState = {
  entities: {},
  selectedProjectName: getCookie('project'),
};

export function projectReducer(
  state: ProjectState = initialState,
  action: fromProject.ProjectActions,
): ProjectState {
  switch (action.type) {
    case fromProject.LOAD_PROJECTS_SUCCESS: {
      const projects = action.payload;
      const entities = projects.reduce(
        (entities: { [name: string]: Project }, project: Project) => {
          return {
            ...entities,
            [project.name]: project,
          };
        },
        {
          ...state.entities,
        },
      );
      return {
        ...state,
        entities,
      };
    }

    case fromProject.CREATE_PROJECT_SUCCESS: {
      const project = action.payload;
      const entities = {
        ...state.entities,
        [project.name]: project,
      };
      return {
        ...state,
        entities,
      };
    }

    case fromProject.DELETE_PROJECT_SUCCESS: {
      const projectName = action.payload;
      const { [projectName]: removed, ...entities } = state.entities;
      return {
        ...state,
        entities,
      };
    }

    case fromProject.SET_PROJECT:
      return {
        ...state,
        selectedProjectName: action.payload,
      };
    default:
      return state;
  }
}

export const getProjectEntities = (state: ProjectState) => state.entities;
export const getSelectedProjectName = (state: ProjectState) =>
  state.selectedProjectName;
