import { createSelector } from '@ngrx/store';

import * as fromFeature from '../reducers';
import * as fromProject from '../reducers/project';

export const getProjectsEntities = createSelector(
  fromFeature.getProjectState,
  fromProject.getProjectEntities,
);

export const getAllProjects = createSelector(getProjectsEntities, entities => {
  return Object.keys(entities).map(name => entities[name]);
});

export const getSelectedProjectName = createSelector(
  fromFeature.getProjectState,
  fromProject.getSelectedProjectName,
);
