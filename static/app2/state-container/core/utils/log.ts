export const log = <TValue>(tag: string) => (value: TValue): TValue => {
  console.log(tag, value);
  return value;
};
