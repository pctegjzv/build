import { LogSourceEffects } from 'app2/state-store/log_source/effects';
import { NodeEffects } from 'app2/state-store/node/effects';
import { RegionEffects } from 'app2/state-store/region/effects';
import { RegionComponentEffects } from 'app2/state-store/region_component/effects';
import { RegionStatsEffects } from 'app2/state-store/region_stats/effects';

export const effects = [
  RegionEffects,
  NodeEffects,
  RegionComponentEffects,
  RegionStatsEffects,
  LogSourceEffects,
];
