import { Action } from '@ngrx/store';

import { LicenseInfoModal } from 'app2/features/license/license.type';
/*
 license actions are defined here
 */
export const UPDATE_BY_KEYS = '[License] update by keys ';

export class UpdateByKeys implements Action {
  readonly type = UPDATE_BY_KEYS;

  constructor(public payload: LicenseInfoModal) {}
}

export type All = UpdateByKeys;
