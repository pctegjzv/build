import { createFeatureSelector } from '@ngrx/store';

import { LicenseInfoModal } from 'app2/features/license/license.type';

import * as licenseActions from '../actions';

export type State = LicenseInfoModal;

const initializeState: State = {};

export function reducer(
  state: State = initializeState,
  action: licenseActions.All,
) {
  switch (action.type) {
    case licenseActions.UPDATE_BY_KEYS:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
}

export const selectFeature = createFeatureSelector('license');
