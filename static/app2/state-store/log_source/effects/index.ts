import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { EMPTY, from } from 'rxjs';
import { catchError, map, mergeMap, pluck } from 'rxjs/operators';

import { RegionService } from 'app2/shared/services/features/region.service';

import * as logSourceActions from '../actions';

@Injectable()
export class LogSourceEffects {
  @Effect()
  getByRegion$ = this.actions
    .ofType(logSourceActions.GET_BY_REGION)
    .pipe(pluck('payload'))
    .pipe(
      mergeMap((regionName: string) =>
        from(this.regionService.getRegionLogSource(regionName)).pipe(
          map(
            logSource =>
              new logSourceActions.GetByRegionSucc({
                regionName,
                logSource,
              }),
          ),
          catchError(() => EMPTY),
        ),
      ),
    );

  constructor(private actions: Actions, private regionService: RegionService) {}
}
