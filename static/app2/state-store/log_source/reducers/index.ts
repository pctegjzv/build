import { createFeatureSelector } from '@ngrx/store';

import { RegionLogSource } from 'app2/shared/services/features/region.service';

import * as logSourceActions from '../actions';

export interface State {
  [regionName: string]: RegionLogSource;
}

const initialState: State = {};

export function reducer(
  state: State = initialState,
  action: logSourceActions.All,
) {
  switch (action.type) {
    case logSourceActions.GET_BY_REGION_SUCC:
      const { regionName, logSource } = action.payload;
      return {
        ...state,
        [regionName]: logSource,
      };
    default:
      return state;
  }
}

export const selectFeature = createFeatureSelector('log_source');
