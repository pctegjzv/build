import { Action } from '@ngrx/store';

import {
  ClusterNode,
  Node,
} from 'app2/shared/services/features/region.service';
import { ErrorResponse } from 'app2/shared/services/http/http.service';

export const GET_BY_REGION = '[Node] get by region';
export const GET_BY_REGION_SUCC = '[Node] get by region success';
export const GET_BY_REGION_FAIL = '[Node] get by region failed';
export const GET_BY_IP = '[Node] get by ip';
export const GET_BY_IP_SUCC = '[Node] get by ip success';
export const DELETE_BY_IP = '[Node] delete by ip';
export const DELETE_BY_IP_SUCC = '[Node] delete by ip success';
export const DELETE_BY_IP_FAIL = '[Node] delete by ip failed';

export class GetByRegion implements Action {
  readonly type = GET_BY_REGION;
  constructor(
    public payload: string | { regionName: string; isK8sV4?: boolean },
  ) {}
}

export class GetByRegionSucc implements Action {
  readonly type = GET_BY_REGION_SUCC;
  constructor(
    public payload: { regionName: string; nodes: Node[] | ClusterNode[] },
  ) {}
}

export class GetByRegionFail implements Action {
  readonly type = GET_BY_REGION_FAIL;
  constructor(public payload: { regionName: string; error: ErrorResponse }) {}
}

export class GetByIp implements Action {
  readonly type = GET_BY_IP;
  constructor(public payload: { regionName: string; privateIp: string }) {}
}

export class GetByIpSucc implements Action {
  readonly type = GET_BY_IP_SUCC;
  constructor(
    public payload: { regionName: string; privateIp: string; node: Node },
  ) {}
}

export class DeleteByIp implements Action {
  readonly type = DELETE_BY_IP;
  constructor(public payload: { regionName: string; privateIp: string }) {}
}

export class DeleteByIpSucc implements Action {
  readonly type = DELETE_BY_IP_SUCC;
  constructor(public payload: { regionName: string; privateIp: string }) {}
}

export class DeleteByIpFail implements Action {
  readonly type = DELETE_BY_IP_FAIL;
  constructor(public payload: { regionName: string; privateIp: string }) {}
}

export type All =
  | GetByRegion
  | GetByRegionSucc
  | GetByIp
  | GetByIpSucc
  | DeleteByIp
  | DeleteByIpSucc
  | DeleteByIpFail;
