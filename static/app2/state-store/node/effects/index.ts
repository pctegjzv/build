import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { EMPTY, from, of } from 'rxjs';
import { catchError, map, mergeMap, pluck } from 'rxjs/operators';

import {
  ClusterNode,
  Node,
  RegionService,
} from 'app2/shared/services/features/region.service';

import * as nodeActions from '../actions';

@Injectable()
export class NodeEffects {
  @Effect()
  getByRegion$ = this.actions
    .ofType(nodeActions.GET_BY_REGION)
    .pipe(pluck('payload'))
    .pipe(
      mergeMap((params: string | { regionName: string; isK8sV4?: boolean }) => {
        let regionName: string;
        let isK8sV4: boolean;

        if (typeof params === 'string') {
          regionName = params;
          isK8sV4 = false;
        } else {
          regionName = params.regionName;
          isK8sV4 = params.isK8sV4;
        }

        return from<Node[] | ClusterNode[]>(
          isK8sV4
            ? this.regionService
                .getClusterNodes(regionName)
                .then(({ items }) => items)
            : this.regionService.getRegionNodes(regionName),
        ).pipe(
          map(nodes => new nodeActions.GetByRegionSucc({ regionName, nodes })),
          catchError(error =>
            of(new nodeActions.GetByRegionFail({ regionName, error })),
          ),
        );
      }),
    );

  @Effect()
  getByIp$ = this.actions
    .ofType(nodeActions.GET_BY_IP)
    .pipe(pluck('payload'))
    .pipe(
      mergeMap((payload: { regionName: string; privateIp: string }) =>
        from(
          this.regionService.getNode(payload.regionName, payload.privateIp),
        ).pipe(
          map(
            node =>
              new nodeActions.GetByIpSucc(Object.assign({}, payload, { node })),
          ),
          catchError(() => EMPTY),
        ),
      ),
    );

  @Effect()
  deleteByIp$ = this.actions
    .ofType(nodeActions.DELETE_BY_IP)
    .pipe(pluck('payload'))
    .pipe(
      mergeMap((payload: { regionName: string; privateIp: string }) =>
        from(
          this.regionService.deleteNode(payload.regionName, payload.privateIp),
        ).pipe(
          map(() => new nodeActions.DeleteByIpSucc(payload)),
          catchError(() => of(new nodeActions.DeleteByIpFail(payload))),
        ),
      ),
    );

  constructor(private actions: Actions, private regionService: RegionService) {}
}
