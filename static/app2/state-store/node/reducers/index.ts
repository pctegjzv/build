import { createFeatureSelector } from '@ngrx/store';

import {
  ClusterNode,
  Node,
  NodeState,
} from 'app2/shared/services/features/region.service';

import * as nodeActions from '../actions';

export interface Entities {
  [ip: string]: Node | ClusterNode;
}

export interface State {
  [regionName: string]: {
    entities: Entities;
    ips: string[];
  };
}

const initialState: State = {};

export function reducer(state: State = initialState, action: nodeActions.All) {
  switch (action.type) {
    case nodeActions.GET_BY_REGION_SUCC:
      const {
        regionName,
        nodes,
      } = (action as nodeActions.GetByRegionSucc).payload;
      return nodes2State(state, regionName, nodes);
    case nodeActions.GET_BY_IP_SUCC:
      return updateNode(state, action.payload);
    case nodeActions.DELETE_BY_IP_SUCC:
      return deleteByIp(state, action.payload);
    default:
      return state;
  }
}

const getPrivateIp = (node: Node | ClusterNode) =>
  'private_ip' in node
    ? node.private_ip
    : node.status.addresses.find(addr => addr.type === 'InternalIP').address;

function nodes2State(
  state: State,
  region: string,
  nodes: Array<Node | ClusterNode>,
): State {
  const ips = nodes.map(node => getPrivateIp(node));
  const entities: Entities = {};
  nodes.forEach(node => {
    entities[getPrivateIp(node)] = node;
  });
  return {
    ...state,
    [region]: {
      ips,
      entities,
    },
  };
}

function updateNode(
  state: State,
  payload: { regionName: string; privateIp: string; node: Node },
): State {
  const regionNodes = state[payload.regionName] || { ips: [], entities: {} };
  const ips = [...new Set([...(regionNodes.ips || []), payload.privateIp])];
  const entities = {
    ...(regionNodes.entities || {}),
    ...{
      [payload.privateIp]: payload.node,
    },
  };
  return {
    ...state,
    ...{
      [payload.regionName]: {
        ips,
        entities,
      },
    },
  };
}

function deleteByIp(
  state: State,
  payload: { regionName: string; privateIp: string },
): State {
  const deleteingNode = state[payload.regionName].entities[payload.privateIp];

  return {
    ...state,
    [payload.regionName]: {
      ips: state[payload.regionName].ips,
      entities: {
        ...state[payload.regionName].entities,
        [payload.privateIp]: {
          ...deleteingNode,
          state: NodeState.ShuttingDown,
        },
      },
    },
  };
}

export const selectFeature = createFeatureSelector<State>('node');
