import {
  State as licenseState,
  reducer as licenseReducer,
} from 'app2/state-store/license/reducers';
import {
  State as logSourceState,
  reducer as logSourceReducer,
} from 'app2/state-store/log_source/reducers';
import {
  State as NodeState,
  reducer as nodeReducer,
} from 'app2/state-store/node/reducers';
import {
  State as RegionState,
  reducer as regionReducer,
} from 'app2/state-store/region/reducers';
import {
  State as RegionComponentState,
  reducer as regionComponentReducer,
} from 'app2/state-store/region_component/reducers';
import {
  State as RegionStatsState,
  reducer as regionStatsReducer,
} from 'app2/state-store/region_stats/reducers';

export interface AppState {
  regions: RegionState;
  node: NodeState;
  region_component: RegionComponentState;
  region_stats: RegionStatsState;
  log_source: logSourceState;
  license: licenseState;
}

export const reducers = {
  regions: regionReducer,
  node: nodeReducer,
  region_component: regionComponentReducer,
  region_stats: regionStatsReducer,
  log_source: logSourceReducer,
  license: licenseReducer,
};
