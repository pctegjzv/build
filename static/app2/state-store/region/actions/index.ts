import { Action } from '@ngrx/store';

import {
  ClusterAccessData,
  Region,
  RegionCreateData,
} from 'app2/shared/services/features/region.service';
import { ErrorResponse } from 'app2/shared/services/http/http.service';

export const GET_ALL = '[Region] get all';
export const GET_ALL_SUCC = '[Region] get all success';
export const GET_ALL_FAIL = '[Region] get all failed';
export const GET_BY_NAME = '[Region] get by name';
export const GET_BY_NAME_SUCC = '[Region] get by name success';
export const GET_BY_NAME_FAIL = '[Region] get by name failed';
export const CREATE = '[Region] create';
export const CREATE_SUCC = '[Region] create success';
export const CREATE_FAIL = '[Region] create failed';
export const DELETE_BY_NAME = '[Region] delete by name';
export const DELETE_BY_NAME_SUCC = '[Region] delete by name success';
export const DELETE_BY_NAME_FAIL = '[Region] delete by name failed';

export class GetAll implements Action {
  readonly type = GET_ALL;
}
export class GetAllSucc implements Action {
  readonly type = GET_ALL_SUCC;
  constructor(public payload: Region[]) {}
}
export class GetAllFail implements Action {
  readonly type = GET_ALL_FAIL;
  constructor() {}
}
export class GetByName implements Action {
  readonly type = GET_BY_NAME;
  constructor(public payload: string) {}
}
export class GetByNameSucc implements Action {
  readonly type = GET_BY_NAME_SUCC;
  constructor(public payload: Region) {}
}

export class GetByNameFail implements Action {
  readonly type = GET_BY_NAME_FAIL;
  constructor(public payload: string) {}
}

export class Create implements Action {
  readonly type = CREATE;
  constructor(public payload: RegionCreateData | ClusterAccessData) {}
}

export class CreateSucc implements Action {
  readonly type = CREATE_SUCC;
  constructor(public payload: Region) {}
}

export class CreateFail implements Action {
  readonly type = CREATE_FAIL;
  payload: string[];
  constructor(public rawError: ErrorResponse) {
    this.payload = rawError.errors
      ? rawError.errors.map(err => err.message)
      : [];
  }
}

export class DeleteByName implements Action {
  readonly type = DELETE_BY_NAME;

  constructor(
    public payload:
      | string
      | {
          name: string;
          isNewCluster: boolean;
        },
  ) {}
}

export class DeleteByNameSucc implements Action {
  readonly type = DELETE_BY_NAME_SUCC;

  constructor(public payload: string) {}
}

export class DeleteByNameFail implements Action {
  readonly type = DELETE_BY_NAME_FAIL;

  constructor(
    public payload: {
      name: string;
      error: any;
    },
  ) {}
}

export type All =
  | GetAll
  | GetAllSucc
  | GetAllFail
  | GetByName
  | GetByNameSucc
  | GetByNameFail
  | Create
  | CreateSucc
  | CreateFail
  | DeleteByName
  | DeleteByNameSucc;
