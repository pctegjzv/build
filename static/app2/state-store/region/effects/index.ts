import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { from, of } from 'rxjs';
import { catchError, map, mergeMap, pluck, switchMap } from 'rxjs/operators';

import {
  Cluster,
  ClusterAccessData,
  Region,
  RegionCreateData,
  RegionService,
} from 'app2/shared/services/features/region.service';

import * as regionActions from '../actions';

@Injectable()
export class RegionEffects {
  @Effect()
  getAll$ = this.actions.ofType(regionActions.GET_ALL).pipe(
    switchMap(() => {
      return from(this.regionService.getRegions()).pipe(
        map(regions => new regionActions.GetAllSucc(regions)),
        catchError(() => of(new regionActions.GetAllFail())),
      );
    }),
  );

  @Effect()
  getByName$ = this.actions
    .ofType(regionActions.GET_BY_NAME)
    .pipe(pluck('payload'))
    .pipe(
      mergeMap((name: string) =>
        from(this.regionService.getRegionByName(name)).pipe(
          map(region => new regionActions.GetByNameSucc(region)),
          catchError(() => of(new regionActions.GetByNameFail(name))),
        ),
      ),
    );

  @Effect()
  create$ = this.actions
    .ofType(regionActions.CREATE)
    .pipe(pluck('payload'))
    .pipe(
      mergeMap((data: RegionCreateData | ClusterAccessData) =>
        from<Region | Cluster>(
          'over_commit' in data
            ? this.regionService.createRegion(data)
            : this.regionService
                .accessCluster(data)
                .then(
                  cluster =>
                    cluster || this.regionService.getCluster(data.name),
                ),
        ).pipe(
          map(region => new regionActions.CreateSucc(region as any)),
          catchError(err => of(new regionActions.CreateFail(err))),
        ),
      ),
    );

  @Effect()
  deleteByName$ = this.actions
    .ofType(regionActions.DELETE_BY_NAME)
    .pipe(pluck('payload'))
    .pipe(
      mergeMap(
        (nameOrOptions: string | { name: string; isNewCluster: boolean }) => {
          if (typeof nameOrOptions === 'string') {
            nameOrOptions = {
              name: nameOrOptions,
              isNewCluster: false,
            };
          }

          const { name, isNewCluster } = nameOrOptions;

          return from(
            isNewCluster
              ? this.regionService.deleteCluster(name)
              : this.regionService.deleteRegion(name),
          ).pipe(
            map(() => new regionActions.DeleteByNameSucc(name)),
            catchError(error =>
              of(
                new regionActions.DeleteByNameFail({
                  name,
                  error,
                }),
              ),
            ),
          );
        },
      ),
    );

  constructor(private regionService: RegionService, private actions: Actions) {}
}
