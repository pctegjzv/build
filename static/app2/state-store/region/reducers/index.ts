import { createFeatureSelector, createSelector } from '@ngrx/store';

import { Region } from 'app2/shared/services/features/region.service';

import * as regionActions from '../actions';

export interface Entities {
  [name: string]: Region;
}

export interface State {
  names: string[];
  entities: Entities;
}

const initialState: State = {
  names: [],
  entities: {},
};

export function reducer(
  state: State = initialState,
  action: regionActions.All,
): State {
  switch (action.type) {
    case regionActions.GET_ALL_SUCC:
      return regions2State(action.payload);
    case regionActions.GET_BY_NAME_SUCC:
    case regionActions.CREATE_SUCC:
      return updateRegionByName(state, action.payload);
    case regionActions.DELETE_BY_NAME_SUCC:
      return deleteRegionByName(state, action.payload);
    default:
      return state;
  }
}

function regions2State(regions: Region[]): State {
  const names = regions.map(region => region.name);
  const entities: Entities = {};
  names.forEach(name => {
    entities[name] = regions.find(region => region.name === name);
  });
  return { names, entities };
}

function updateRegionByName(state: State, region: Region): State {
  const names = [...new Set([...state.names, region.name])];
  const entities = { ...state.entities, ...{ [region.name]: region } };
  return { names, entities };
}

function deleteRegionByName(state: State, region: string): State {
  const names = state.names.filter(item => item !== region);
  return {
    names,
    entities: state.entities,
  };
}

export const selectFeature = createFeatureSelector<State>('regions');
export const selectNames = createSelector(
  selectFeature,
  (state: State) => state.names,
);
export const selectEntities = createSelector(
  selectFeature,
  (state: State) => state.entities,
);
export const selectList = createSelector(
  selectNames,
  selectEntities,
  (names: string[], entities: Entities) => names.map(name => entities[name]),
);
