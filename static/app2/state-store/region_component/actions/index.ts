import { Action } from '@ngrx/store';

import { RegionComponent } from 'app2/shared/services/features/region.service';

export const GET_BY_REGION = '[RegionComponents] get by region';
export const GET_BY_REGION_SUCC = '[RegionComponents] get by region success';

export class GetByRegion implements Action {
  readonly type = GET_BY_REGION;

  constructor(public payload: string) {}
}

export class GetByRegionSucc implements Action {
  readonly type = GET_BY_REGION_SUCC;

  constructor(
    public payload: { regionName: string; components: RegionComponent[] },
  ) {}
}

export type All = GetByRegion | GetByRegionSucc;
