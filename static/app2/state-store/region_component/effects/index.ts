import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { EMPTY, from } from 'rxjs';
import { catchError, map, mergeMap, pluck } from 'rxjs/operators';

import { RegionService } from 'app2/shared/services/features/region.service';

import * as componentActions from '../actions';

@Injectable()
export class RegionComponentEffects {
  @Effect()
  getByRegion$ = this.actions
    .ofType(componentActions.GET_BY_REGION)
    .pipe(pluck('payload'))
    .pipe(
      mergeMap((regionName: string) =>
        from(this.regionService.getRegionComponents(regionName)).pipe(
          map(
            components =>
              new componentActions.GetByRegionSucc({
                regionName,
                components,
              }),
          ),
          catchError(() => EMPTY),
        ),
      ),
    );

  constructor(private actions: Actions, private regionService: RegionService) {}
}
