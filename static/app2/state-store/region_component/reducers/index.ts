import { createFeatureSelector } from '@ngrx/store';

import { RegionComponent } from 'app2/shared/services/features/region.service';

import * as componentActions from '../actions';

export interface State {
  [regionName: string]: RegionComponent[];
}

const initialState: State = {};

export function reducer(
  state: State = initialState,
  action: componentActions.All,
) {
  switch (action.type) {
    case componentActions.GET_BY_REGION_SUCC:
      const { regionName, components } = action.payload;
      return {
        ...state,
        [regionName]: components,
      };
    default:
      return state;
  }
}

export const selectFeature = createFeatureSelector('region_component');
