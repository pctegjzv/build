import { Action } from '@ngrx/store';

import { RegionStats } from 'app2/shared/services/features/region.service';

export const GET_BY_REGION = '[RegionStats] get by region';
export const GET_BY_REGION_SUCC = '[RegionStats] get by region success';
export const GET_BY_REGION_FAIL = '[RegionStats] get by region fail';

export class GetByRegion implements Action {
  readonly type = GET_BY_REGION;

  constructor(public payload: string) {}
}

export class GetByRegionSucc implements Action {
  readonly type = GET_BY_REGION_SUCC;

  constructor(public payload: { regionName: string; stats: RegionStats }) {}
}

export class GetByRegionFail implements Action {
  readonly type = GET_BY_REGION_FAIL;

  constructor(public payload: { regionName: string }) {}
}

export type All = GetByRegion | GetByRegionSucc | GetByRegionFail;
