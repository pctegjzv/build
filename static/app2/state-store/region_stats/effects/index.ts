import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { from, of } from 'rxjs';
import { catchError, debounceTime, map, mergeMap, pluck } from 'rxjs/operators';

import { RegionService } from 'app2/shared/services/features/region.service';

import * as statsActions from '../actions';

@Injectable()
export class RegionStatsEffects {
  @Effect()
  getByRegion$ = this.actions
    .ofType(statsActions.GET_BY_REGION)
    .pipe(debounceTime(0))
    .pipe(pluck('payload'))
    .pipe(
      mergeMap((regionName: string) =>
        from(this.regionService.getRegionStats(regionName)).pipe(
          map(stats => {
            return new statsActions.GetByRegionSucc({
              regionName,
              stats,
            });
          }),
          catchError(() =>
            of(new statsActions.GetByRegionFail({ regionName })),
          ),
        ),
      ),
    );

  constructor(private actions: Actions, private regionService: RegionService) {}
}
