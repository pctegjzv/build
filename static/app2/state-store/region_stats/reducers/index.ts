import { createFeatureSelector } from '@ngrx/store';

import { RegionStats } from 'app2/shared/services/features/region.service';

import * as statsActions from '../actions';

export interface State {
  [regionName: string]: RegionStats;
}

const initialState: State = {};

const DEFAULT_STATS = {
  containers_count: 0,
  cpus_allocated: 0,
  cpus_total: 0,
  cpus_utilization: 0,
  gpus_allocated: 0,
  gpus_total: 0,
  gpus_utilization: 0,
  mem_allocated: 0,
  mem_total: 0,
  mem_utilization: 0,
  nodes_count: 0,
  services_count: 0,
};

export function reducer(state: State = initialState, action: statsActions.All) {
  switch (action.type) {
    case statsActions.GET_BY_REGION_SUCC: {
      const { regionName, stats } = action.payload;
      return {
        ...state,
        [regionName]: stats,
      };
    }
    case statsActions.GET_BY_REGION_FAIL: {
      const { regionName } = action.payload;
      return {
        ...state,
        [regionName]: DEFAULT_STATS,
      };
    }
    default:
      return state;
  }
}

export const selectFeature = createFeatureSelector('region_stats');
