export const translateService = {
  get: (key: string) => 'translated_' + key,
  get otherLang() {
    return 'en';
  },
  changeLanguage() {},
};
