/**
 * Reset all temporary testing stuff.
 */
export function registerCleanup(): void {
  afterAll(() => {
    Array.from(document.querySelectorAll('[ng-version]')).forEach(node =>
      node.remove(),
    );
  });
}

/**
 * Function to generate a promise to be resolved after the given timeout.
 * @param delay
 * @returns {Promise<void>}
 */
export function delay(delay: number): Promise<void> {
  return new Promise<void>(res => setTimeout(() => res(), delay));
}

type promiseFunction = (val: any) => Promise<any>;

/**
 * Run the given promise functions in a waterfall pattern.
 * @param promiseArr
 * @returns {Promise<void>}
 */
export function waterfall(promiseArr: promiseFunction[]) {
  return promiseArr.reduce((accum, p) => accum.then(p), Promise.resolve());
}
