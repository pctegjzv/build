export default {
  categories: [
    {
      name: 'delivery',
      children: [
        {
          name: 'container',
          icon: 'fa fa-cubes',
          children: [
            {
              name: 'app_service',
              stateName: 'app_service.app_service',
            },
            {
              name: 'job_config',
              stateName: 'job.config',
            },
            {
              name: 'job_history',
              stateName: 'job.history',
            },
            {
              name: 'template',
              stateName: 'app_service.template',
            },
            {
              name: 'envfile',
              stateName: 'app_service.envfile',
            },
            {
              name: 'configuration',
              stateName: 'app_service.configuration',
            },
          ],
        },
        {
          name: 'image',
          icon: 'fa fa-building',
          children: [
            {
              name: 'image_repository',
              stateName: 'image.repository',
            },
            {
              name: 'image_sync_center',
              stateName: 'image.sync-center',
              weblab: 'REPO_SYNC_ENABLED',
            },
            {
              name: 'image_sync_history',
              stateName: 'image.sync-history',
              weblab: 'REPO_SYNC_ENABLED',
            },
          ],
        },
        {
          name: 'build',
          icon: 'fa fa-gavel',
          children: [
            {
              name: 'build_config',
              stateName: 'build.config',
            },
            {
              name: 'build_history',
              stateName: 'build.history',
            },
          ],
        },
        {
          name: 'pipeline',
          icon: 'fa fa-wrench',
          children: [
            {
              name: 'pipeline_config',
              stateName: 'pipeline.config',
            },
            {
              name: 'pipeline_history',
              stateName: 'pipeline.history',
            },
          ],
        },
      ],
    },
    {
      name: 'infrastructure',
      children: [
        {
          name: 'region',
          icon: 'fa fa-server',
          stateName: 'region',
        },
        {
          name: 'storage',
          icon: 'fa fa-archive',
          children: [
            {
              name: 'storage_volume',
              stateName: 'storage.volume',
            },
            {
              name: 'storage_snapshot',
              stateName: 'storage.snapshot',
            },
          ],
        },
        {
          name: 'network',
          icon: 'fa fa-sitemap',
          children: [
            {
              name: 'load_balancer',
              stateName: 'load_balancer',
            },
            {
              weblab: 'SUBNET_ENABLED',
              name: 'subnet',
              stateName: 'network.subnet',
            },
            {
              name: 'certificate',
              stateName: 'network.certificate',
              beta: true,
            },
          ],
        },
        {
          name: 'cloud',
          icon: 'fa fa-cloud',
          children: [
            {
              name: 'cloud_instance',
              stateName: 'cloud.instance',
            },
            {
              name: 'cloud_account',
              stateName: 'cloud.account',
            },
          ],
        },
        {
          name: 'resource',
          icon: 'fa fa-bar-chart',
          children: [
            {
              name: 'resource_service_usage',
              stateName: 'resource.service_usage',
            },
            {
              name: 'resource_quota_space',
              stateName: 'quota',
              weblab: 'QUOTA_ENABLED',
            },
          ],
        },
      ],
    },
    {
      name: 'ops',
      children: [
        {
          name: 'monitor_dashboard',
          stateName: 'monitor.dashboard',
          icon: 'fa fa-dashboard',
        },
        {
          name: 'monitor_event',
          stateName: 'monitor.event',
          icon: 'fa fa-calendar',
        },
        {
          name: 'monitor_logs',
          stateName: 'monitor.logs',
          icon: 'fa fa-list-alt',
          weblab: 'LOGS_DASHBOARD',
          beta: true,
        },
        {
          name: 'monitor_notification',
          stateName: 'monitor.notification',
          icon: 'fa fa fa-bullhorn',
        },
        {
          name: 'monitor_alarm',
          stateName: 'monitor.alarm',
          icon: 'fa fa-bell',
        },
      ],
    },
    {
      name: 'extension',
      children: [
        {
          name: 'app_catalog',
          stateName: 'app_catalog',
          icon: 'fa fa-shopping-bag',
          beta: true,
        },
        {
          name: 'integration_center',
          stateName: 'integration_center',
          icon: 'rb-icon-integration',
          beta: true,
        },
      ],
    },
    {
      name: '',
      children: [
        {
          name: 'docs',
          icon: 'fa fa-file',
          href: 'env:user_docs_url',
        },
        {
          name: 'language_picker',
          icon: 'fa fa-globe',
        },
      ],
    },
  ],
  expandMode: 'SINGLE',
  homeStateName: 'app_service.app',
};
