import { Inject, Injectable } from '@angular/core';
import { TranslateService as NgxTranslateService } from '@ngx-translate/core';
import { getGlobal, setGlobal } from 'app2/app-global';
import { ENVIRONMENTS, TRANSLATIONS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { Language } from 'app2/translate/translate.types';
import { initTranslations } from 'app2/utils/bootstrap';
import { get } from 'lodash';
import { BehaviorSubject, Observable } from 'rxjs';
/**
 * Translate service.
 * Still using similar approach like what we did in the Angular 1 app, with some exceptions:
 *   - ng2-translate
 *   - translation files are not pre-processed via webpack. They are directly loaded via `import` this time.
 * Since we still use the same strategy for language detection, the language used in the console
 * page will be the same in the login page.
 */
@Injectable()
export class TranslateService {
  langState = {};
  currentLang$ = new BehaviorSubject<string>(this.translateService.currentLang);

  constructor(
    private translateService: NgxTranslateService,
    @Inject(ENVIRONMENTS) private environments: Environments,
  ) {
    // 对 window 对象的直接访问在 Angular 2 里面是不被推荐的
    // 如果有空的话，最好还是把 window reference 放到 DI 里比较好
    // 不同标准定义locale的字符串形式不同,
    // 为了统一成目前项目中使用 zh_cn 和 en 两种locale, 需要做一次简单的映射。
    let locale =
      localStorage['alauda_locale'] ||
      this.translateService.getBrowserLang() ||
      'en';
    locale = locale.toLowerCase();

    // 注意这里没有用startsWith('zh'), 是考虑繁体中文用户可能会更倾向于用英语
    // 默认语言为en, 当用户浏览器语言为zh-cn或者zh时, 改为使用zh_cn。
    locale = ['zh_cn', 'zh-cn', 'zh'].includes(locale) ? 'zh_cn' : 'en';
    localStorage['alauda_locale'] = locale;
    this.translateService.addLangs(Object.values(Language));
    this.translateService.setTranslation(locale, getGlobal(TRANSLATIONS), true);
    Object.values(Language).forEach(lang => {
      this.langState[lang] = lang === locale;
    });
    this.translateService.use(locale);
    this.currentLang$.next(this.translateService.currentLang);
  }

  /**
   * All supported locales for this app.
   * Should be able to generate from manifest instead.
   */
  get supportedLocales() {
    return ['en', 'zh_cn'];
  }

  /**
   * Get the translated string
   * @param key
   * @param params
   * @returns {string|any}
   */
  get(key: string, params?: any) {
    if (!key) {
      return '';
    }
    return this.translateService.instant(key, params);
  }

  get currentLang(): string {
    return this.translateService.currentLang;
  }

  get otherLang(): string {
    return this.currentLang === 'en' ? 'zh_cn' : 'en';
  }
  getLanguage() {
    return this.currentLang;
  }

  /**
   * Get the translated string as an observable.
   * @param key
   * @param params
   * @returns {string|any}
   */
  stream(key: string, params?: any): Observable<string> {
    return this.translateService.stream(key, params);
  }

  async changeLanguage(language = this.otherLang) {
    // Change language only if it is different
    // from the current language
    if (this.currentLang !== language) {
      // Currently we does not support change language asynchronously
      // Below is a hack for changing language ...
      localStorage['alauda_locale'] = language;
      if (!this.langState[language]) {
        const allTranslations = await initTranslations(
          this.environments.is_private_deploy_enabled,
        );
        this.translateService.setTranslation(language, allTranslations, true);
        this.langState[language] = true;
      }
      this.translateService.use(language);
      this.currentLang$.next(this.translateService.currentLang);
      setGlobal(
        TRANSLATIONS,
        get(this.translateService.translations, language),
      );
      if (getGlobal('$translateProvider')) {
        getGlobal('$translateProvider').translations(
          language,
          get(this.translateService.translations, language),
        );
        if (getGlobal('$translate')) {
          getGlobal('$translate').use(language);
        }
      }
    }
  }
}
