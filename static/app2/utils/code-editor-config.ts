import { CodeEditorActionsConfig } from 'alauda-ui';

const commonActions: CodeEditorActionsConfig = {
  copy: true,
  find: true,
  format: false,
  export: true,
};

export const createActions: CodeEditorActionsConfig = {
  diffMode: false,
  clear: true,
  recover: false,
  import: true,
  ...commonActions,
};

export const viewActions: CodeEditorActionsConfig = {
  diffMode: false,
  clear: false,
  recover: false,
  ...commonActions,
};

export const updateActions: CodeEditorActionsConfig = {
  diffMode: true,
  clear: true,
  recover: true,
  import: true,
  ...commonActions,
};
