import {
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import * as fromStore from 'app_user/store';
import { debounce } from 'lodash';

@Component({
  selector: 'rc-app-root',
  template: '<router-outlet></router-outlet>',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  constructor(
    private store: Store<fromStore.AppState>,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {}
  timer: number;
  mouseMoveHandler: any;

  ngOnInit() {
    this.store.dispatch(new fromStore.LoadProjects());
    this.mouseMoveHandler = debounce(() => {
      this._mouseMoveHandler();
    }, 200);
    document.addEventListener('mousemove', this.mouseMoveHandler);
    this.resetTimer();
  }

  ngOnDestroy() {
    document.removeEventListener('mousemove', this.mouseMoveHandler);
  }

  private _mouseMoveHandler() {
    this.resetTimer();
  }

  private logout() {
    location.href = '/ap/logout';
  }

  private resetTimer() {
    window.clearTimeout(this.timer);
    const latencyMinutes = this.env.auto_logout_latency;
    this.timer = window.setTimeout(this.logout, latencyMinutes * 60 * 1000);
  }
}
