import { ApplicationRef, enableProdMode } from '@angular/core';
import { enableDebugTools } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { setGlobal } from 'app2/app-global';
import {
  ACCOUNT,
  ENVIRONMENTS,
  I18NMANIFEST,
  TRANSLATIONS,
  WEBLABS,
} from 'app2/core/tokens';
import {
  initAccount,
  initEnvironments,
  initI18nManifest,
  initTranslations,
  initWeblabs,
} from 'app2/utils/bootstrap';
import { AppModule } from 'app_user/app.module';
import { DEFAULT_NAV_CONFIG } from 'app_user/core/default-nav-config';
import { NAV_CONFIG } from 'app_user/core/tokens';
import 'font-awesome/css/font-awesome.min.css';
import 'semantic-ui-button/button.min.css';
import 'semantic-ui-checkbox/checkbox.min.css';
import 'semantic-ui-dimmer/dimmer.min.css';
import 'semantic-ui-dropdown/dropdown.min.css';
import 'semantic-ui-icon/icon.min.css';
import 'semantic-ui-input/input.min.css';
import 'semantic-ui-label/label.min.css';
import 'semantic-ui-loader/loader.min.css';
import 'semantic-ui-message/message.min.css';
import 'semantic-ui-modal/modal.min.css';
import 'semantic-ui-popup/popup.min.css';
import 'semantic-ui-search/search.min.css';
import 'semantic-ui-transition/transition.min.css';

function initNavConfig() {
  // TODO: load custom nav_config.json
  return DEFAULT_NAV_CONFIG;
}

async function main() {
  const [environments, i18nManifest] = await Promise.all([
    initEnvironments(),
    initI18nManifest(),
  ]);

  //must before initTranslations
  setGlobal(I18NMANIFEST, i18nManifest);

  const account = initAccount();

  const [weblabs, translations, navConfig] = await Promise.all([
    initWeblabs(account.namespace),
    initTranslations(environments.is_private_deploy_enabled),
    initNavConfig(),
  ]);

  setGlobal(ACCOUNT, account);
  setGlobal(WEBLABS, weblabs);
  setGlobal(TRANSLATIONS, translations);
  setGlobal(ENVIRONMENTS, environments);
  setGlobal(NAV_CONFIG, navConfig);

  if (!environments.debug) {
    enableProdMode();
  } else {
    Error.stackTraceLimit = Infinity;
    require('zone.js/dist/long-stack-trace-zone');
  }

  if (module.hot) {
    // 如果webpack启用了热替换功能
    module.hot.accept();
    //
    module.hot.dispose(() => {
      const styles = document.head.querySelectorAll('style');
      for (let i = 0, len = styles.length; i++; i < len) {
        if (styles[i].innerText.indexOf('_ng') >= 0) {
          styles[i].remove();
          styles[i] = null;
        }
      }
    });

    const origin_log = console.log;
    console.log = (...args: any[]) => {
      if (!(args.length === 1 && /^\[(HMR|WDS)\]/.test(args[0]))) {
        origin_log.apply(console, args);
      }
    };
  }

  const platformRef = await platformBrowserDynamic().bootstrapModule(AppModule);
  if (environments.debug) {
    const appRef = platformRef.injector.get(ApplicationRef);
    const appComponent = appRef.components[0];
    enableDebugTools(appComponent);
  }
}

main();
