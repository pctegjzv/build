import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EffectsModule } from '@ngrx/effects';
import {
  RouterStateSerializer,
  StoreRouterConnectingModule,
} from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {
  CodeEditorModule,
  IconModule,
  IconRegistryService,
  InlineAlertModule,
  MessageModule,
  NOTIFICATION_CONFIG,
  NotificationModule,
  PaginatorIntl,
} from 'alauda-ui';
import { getGlobal } from 'app2/app-global';
import { AppPaginatorIntl } from 'app2/app-paginator-intl';
import { ACCOUNT, ENVIRONMENTS, WEBLABS } from 'app2/core/tokens';
import { GlobalTranslateModule } from 'app2/translate/translate.module';
import { TranslateService } from 'app2/translate/translate.service';
import { AppComponent } from 'app_user/app.component';
import { AppRoutingModule } from 'app_user/app.routing.module';
import { NAV_CONFIG } from 'app_user/core/tokens';
import { UiStateService } from 'app_user/core/ui-state.service';
import * as guards from 'app_user/guards';
import { ServicesModule } from 'app_user/shared/services/services.module';
import { SharedModule } from 'app_user/shared/shared.module';
import { CustomSerializer, effects, reducers } from 'app_user/store';

const basicIconsUrl = require('alauda-ui/assets/basic-icons.svg');
const DEFAULT_MONACO_OPTIONS = {
  fontSize: 12,
  folding: true,
  scrollBeyondLastLine: false,
  minimap: { enabled: false },
  find: { seedSearchStringFromSelection: false, autoFindInSelection: false },
  mouseWheelZoom: true,
  scrollbar: {
    vertical: 'visible',
    horizontal: 'visible',
  },
  fixedOverflowWidgets: true,
};

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ServicesModule,
    GlobalTranslateModule,
    SharedModule,
    IconModule,
    NotificationModule,
    InlineAlertModule,
    MessageModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot(effects),
    StoreRouterConnectingModule.forRoot({
      stateKey: 'router',
    }),
    process.env.NODE_ENV === 'development'
      ? StoreDevtoolsModule.instrument({ maxAge: 25 })
      : [],
    CodeEditorModule.forRoot({
      baseUrl: '/static/assets/monaco_lib',
      defaultOptions: DEFAULT_MONACO_OPTIONS,
    }),
  ],
  declarations: [AppComponent],
  bootstrap: [AppComponent],

  providers: [
    // Since WEBLABS and NAV_CONFIGS are constructed during bootstrap,
    // we inject in the root app instead.
    { useFactory: () => getGlobal(WEBLABS), provide: WEBLABS },
    { useFactory: () => getGlobal(ENVIRONMENTS), provide: ENVIRONMENTS },
    { useFactory: () => getGlobal(ACCOUNT), provide: ACCOUNT },
    { useFactory: () => getGlobal(NAV_CONFIG), provide: NAV_CONFIG },
    { provide: RouterStateSerializer, useClass: CustomSerializer },
    UiStateService,
    ...Object.values(guards),
    {
      provide: PaginatorIntl,
      useFactory: (translate: TranslateService) =>
        translate.currentLang === 'en'
          ? new PaginatorIntl()
          : new AppPaginatorIntl(),
      deps: [TranslateService],
    },
    {
      provide: NOTIFICATION_CONFIG,
      useFactory: () => ({
        duration: getGlobal(ENVIRONMENTS).notification_duration,
        maxStack: getGlobal(ENVIRONMENTS).notification_max_stack,
      }),
    },
  ],
})
export class AppModule {
  constructor(iconRegistryService: IconRegistryService) {
    iconRegistryService.registrySvgSymbolsByUrl(basicIconsUrl);
  }
}
