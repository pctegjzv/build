import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DelayPreloadingStrategy } from 'app2/delay-preloading-strategy';

import { ProjectGuard } from './guards';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'project',
    pathMatch: 'full',
  },
  {
    path: 'project',
    canActivate: [ProjectGuard],
    loadChildren: './features/project/project.module#ProjectModule',
  },
  {
    path: 'workspace',
    canActivate: [ProjectGuard],
    loadChildren: './features/workspace/workspace.module#WorkspaceModule',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: DelayPreloadingStrategy,
    }),
  ],
  exports: [RouterModule],
  providers: [DelayPreloadingStrategy],
})
export class AppRoutingModule {}
