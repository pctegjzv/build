import { NavItem } from 'app_user/core/types';

export const DEFAULT_NAV_CONFIG: NavItem[] = [
  {
    icon: 'basic:application_s',
    label: 'application',
    path: 'app',
  },
  {
    icon: 'basic:gears_s',
    label: 'configmap',
    path: 'configmap',
  },
  {
    icon: 'basic:storage_s',
    label: 'nav_storage',
    path: 'pvc',
  },
  // {
  //   icon: 'basic:puzzle_s',
  //   label: 'nav_middleware',
  //   path: 'platform-middleware',
  // },
  // {
  //   icon: 'basic:micro_service',
  //   label: 'nav_micro_service',
  //   path: 'platform-micro_service',
  // },
  // {
  //   icon: 'basic:big_data',
  //   label: 'nav_big_data',
  //   path: 'platform-big_data',
  // },
  // {
  //   icon: 'basic:app_catalog',
  //   label: 'nav_catalog',
  //   path: 'app_catalog',
  // },
  {
    icon: 'basic:route_s',
    label: 'kube_service',
    path: 'service',
  },
  {
    icon: 'basic:route_outside_s',
    label: 'route',
    path: 'route',
  },
  {
    icon: 'basic:upload',
    label: 'image_upload',
    href: 'env:cmb_image_upload_url',
  },
  {
    icon: 'basic:resource_management_s',
    label: 'resource_management',
    path: 'resource_management',
  },
];
