import { InjectionToken } from '@angular/core';

import { NavItem } from 'app_user/core/types';

export const NAV_CONFIG = new InjectionToken<NavItem[]>('NAV_CONFIG');
