import { HttpErrorResponse } from '@angular/common/http';

export interface NavItem {
  label: string;
  path?: string;
  icon?: string;
  routerLink?: string[];
  items?: NavItem[];
  adminOn?: Boolean;
  href?: string;
}

// base resource list typings

export interface Resource {
  type?: string;
  kind?: string;
  // TODO: 参考 http://confluence.alaudatech.com/display/DEV/All+Resources+YAML 目前application和其他k8s资源返回结构尚未统一，待完善
}

export interface ResourceList {
  results: any[];
  count: number;
  num_pages?: number;
}

export interface ListMeta {
  count: number;
  num_pages: number;
}

export interface PageParams {
  pageIndex: number; // 1 based
  pageSize: number;
}

export function isHttpErrorResponse(res: any): res is HttpErrorResponse {
  return res instanceof HttpErrorResponse;
}

export function isNotHttpErrorResponse<T>(
  res: T | HttpErrorResponse,
): res is T {
  return !isHttpErrorResponse(res);
}

export interface BatchReponse {
  responses: {
    [key: string]: {
      body: string;
      code: number;
    };
  };
}

export interface QuotaRange {
  cpu: {
    max: number;
    used: number;
  };
  memory: {
    max: number;
    used: number;
  };
  storage: {
    max: number;
    used: number;
  };
  pvc_num: {
    max: number;
    used: number;
  };
  pods: {
    max: number;
    used: number;
  };
}

export enum QuotaConfigEnum {
  cpu = 'requests.cpu',
  memory = 'requests.memory',
  storage = 'requests.storage',
  pvc_num = 'persistentvolumeclaims',
  pods = 'pods',
}

export interface BatchApi {
  responses: {
    [s: string]: {
      body: string;
      code: number;
      headers: any;
    };
  };
}
