import { Action } from '@ngrx/store';

import {
  AppCatalogTemplate,
  AppCatalogTemplateRepository,
} from 'app2/shared/services/features/app-catalog.service';

export enum ListPageViewMode {
  // Show import progress
  Import = 'IMPORT',

  // Show templates
  Templates = 'TEMPLATES',
}

export interface AppListParams {
  templateId: string;
  search: string;
}

export enum ListPageActionType {
  ModeChange = '@catalog.template/list-page/mode-change',
  TemplateIdChange = '@catalog.template/list-page/selected-template-id-change',
  AppSearchChange = '@catalog.template/list-page/app-search-change',
}

export enum RepoActionType {
  Load = '@catalog.template/repo/load',
  LoadComplete = '@catalog.template/repo/load-complete',
  LoadError = '@catalog.template/repo/load-error',
  Update = '@catalog.template/repo-update',
  UpdateComplete = '@catalog.template/repo/update-complete',
  UpdateError = '@catalog.template/repo/update-error',
  ImportDialogOpen = '@catalog.template/repo/import-dialog-open',
  ImportDialogOpened = '@catalog.template/repo/import-dialog-opened',
  ImportDialogClosed = '@catalog.template/repo/import-dialog-closed',
  Import = '@catalog.template/repo/import',
  ImportStarted = '@catalog.template/repo/import-started',
  ImportError = '@catalog.template/repo-import-error',
  Refresh = '@catalog.template/repo/refresh',
  RefreshComplete = '@catalog.template/repo/refresh-complete',
  RefreshError = '@catalog.template/repo/refresh-error',
}

export enum TemplateActionType {
  Load = '@catalog.template/template/load',
  LoadError = '@catalog.template/template/load-error',
  LoadComplete = '@catalog.template/template/load-complete',
}

export class ListPageModeChangeAction implements Action {
  readonly type = ListPageActionType.ModeChange;
  constructor(public payload: ListPageViewMode) {}
}

export class ListPageTemplateIdChangeAction implements Action {
  readonly type = ListPageActionType.TemplateIdChange;
  constructor(public payload: string) {}
}

export class ListPageAppSearchChangeAction implements Action {
  readonly type = ListPageActionType.AppSearchChange;
  constructor(public payload: string) {}
}

export class RepoLoadAction implements Action {
  readonly type = RepoActionType.Load;
  constructor(public payload: string) {}
}

export class RepoLoadCompleteAction implements Action {
  readonly type = RepoActionType.LoadComplete;

  constructor(public payload: AppCatalogTemplateRepository) {}
}

export class RepoLoadErrorAction implements Action {
  readonly type = RepoActionType.LoadError;

  constructor(public payload: any) {}
}

export class RepoImportAction implements Action {
  readonly type = RepoActionType.Import;
}

export class RepoImportStartedAction implements Action {
  readonly type = RepoActionType.ImportStarted;

  constructor(public payload: AppCatalogTemplateRepository) {}
}

export class RepoImportErrorAction implements Action {
  readonly type = RepoActionType.ImportError;

  constructor(public payload: any) {}
}

export class RepoRefreshAction implements Action {
  readonly type = RepoActionType.Refresh;

  constructor(public payload: string, public onError?: (error: any) => void) {}
}

export class RepoRefreshCompleteAction implements Action {
  readonly type = RepoActionType.RefreshComplete;

  constructor(public payload: AppCatalogTemplateRepository) {}
}

export class RepoRefreshErrorAction implements Action {
  readonly type = RepoActionType.RefreshError;

  constructor(public payload: any) {}
}

export class ImportDialogOpenAction implements Action {
  readonly type = RepoActionType.ImportDialogOpen;

  constructor(public payload: AppCatalogTemplateRepository) {}
}

export class ImportDialogOpenedAction implements Action {
  readonly type = RepoActionType.ImportDialogOpened;

  constructor(
    public payload: number, // modal ref ID
  ) {}
}

export class ImportDialogClosedAction implements Action {
  readonly type = RepoActionType.ImportDialogClosed;
}

export class TemplateLoadAction implements Action {
  readonly type = TemplateActionType.Load;

  constructor(
    public payload: string, // UUID
  ) {}
}

export class TemplateLoadCompleteAction implements Action {
  readonly type = TemplateActionType.LoadComplete;

  constructor(public payload: AppCatalogTemplate) {}
}

export class TemplateLoadErrorAction implements Action {
  readonly type = TemplateActionType.LoadError;

  constructor(public payload: { error: Error; uuid: string }) {}
}

export type Actions =
  | ListPageModeChangeAction
  | ListPageTemplateIdChangeAction
  | ListPageAppSearchChangeAction
  | RepoLoadAction
  | RepoLoadCompleteAction
  | RepoLoadErrorAction
  | RepoRefreshAction
  | RepoRefreshCompleteAction
  | RepoRefreshErrorAction
  | RepoImportAction
  | RepoImportStartedAction
  | RepoImportErrorAction
  | ImportDialogOpenAction
  | ImportDialogOpenedAction
  | ImportDialogClosedAction
  | TemplateLoadAction
  | TemplateLoadCompleteAction
  | TemplateLoadErrorAction;
