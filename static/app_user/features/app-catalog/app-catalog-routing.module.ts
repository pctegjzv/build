import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppCatalogTemplatesComponent } from './components/app-templates/app-catalog-templates.component';
import { AppCatalogAppCreatePageComponent } from './components/pages/app-catalog-create-app-page.component';

const clusterRoutes: Routes = [
  {
    path: '',
    component: AppCatalogTemplatesComponent,
    pathMatch: 'full',
  },
  {
    path: 'create/:template_id',
    component: AppCatalogAppCreatePageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(clusterRoutes)],
  exports: [RouterModule],
})
export class AppCatalogRoutingModule {}
