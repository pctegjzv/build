import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { FloatingPageService } from 'app2/layout/floating-page.service';
import { AppCatalogService } from 'app2/shared/services/features/app-catalog.service';
import { YamlCommentParserService } from 'app2/shared/services/yaml/yaml-comment-parser.service';
import { SharedModule } from 'app_user/shared/shared.module';

import { AppCatalogRoutingModule } from './app-catalog-routing.module';
import { AppCatalogTemplatesComponent } from './components/app-templates/app-catalog-templates.component';
import { AppCatalogTemplateEmptyViewComponent } from './components/empty-view/app-catalog-template-empty-view.component';
import { AppCatalogAppCreatePageComponent } from './components/pages/app-catalog-create-app-page.component';
import { AppCatalogTemplateCardComponent } from './components/template-card/app-catalog-template-card.component';
import { AppCatalogCatalogTemplateEffects } from './effects/catalog-template';
import { FEATURE_NAME, reducers } from './reducers';

@NgModule({
  imports: [
    SharedModule,
    AppCatalogRoutingModule,
    StoreModule.forFeature(FEATURE_NAME, reducers),
    EffectsModule.forFeature([AppCatalogCatalogTemplateEffects]),
  ],
  declarations: [
    AppCatalogAppCreatePageComponent,
    AppCatalogTemplateEmptyViewComponent,
    AppCatalogTemplateCardComponent,
    AppCatalogTemplatesComponent,
  ],
  exports: [],
  providers: [AppCatalogService, YamlCommentParserService, FloatingPageService],
})
export class AppCatalogModule {}
