import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { ImportDialogOpenAction } from 'app2/features/app-catalog/actions/catalog-template';
import * as fromAppCatalog from 'app2/features/app-catalog/reducers';
import {
  AppCatalogService,
  AppCatalogTemplate,
  AppCatalogTemplateRepository,
} from 'app2/shared/services/features/app-catalog.service';
import { RoleService } from 'app2/shared/services/features/role.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  templateUrl: './app-catalog-templates.component.html',
  styleUrls: ['./app-catalog-templates.component.scss'],
})
export class AppCatalogTemplatesComponent implements OnInit {
  repositoryItems: AppCatalogTemplateRepository[] = [];
  templateItems: AppCatalogTemplate[] = [];
  selected_repo_templates: AppCatalogTemplate[] = [];
  display_templates: AppCatalogTemplate[] = [];
  templateItemsMap = {};
  selectedRepoName: string;
  keyWord = '';
  repo_loading = true;
  template_loading = true;
  empty = false;
  permission = false;
  applicationPermission = false;
  totalTemplate = 0;
  pageSize = 999;
  constructor(
    private router: Router,
    private appCatalog: AppCatalogService,
    private errorsToastService: ErrorsToastService,
    private roleService: RoleService,
    private store: Store<fromAppCatalog.State>,
    private workspaceComponent: WorkspaceComponent,
  ) {}

  ngOnInit() {
    this.roleService
      .getPluralContextPermissions(['helm_template_repo', 'application'])
      .then(res => {
        this.permission =
          res.helm_template_repo.indexOf('helm_template_repo:manage') >= 0;
        this.applicationPermission =
          res.application.indexOf('application:create') >= 0;
      })
      .catch(() => {
        this.permission = false;
      });
    this.refetch();
  }

  get display_empty() {
    return !this.display_templates.length;
  }

  async refetch() {
    this.repo_loading = true;
    this.template_loading = true;
    try {
      const repositories = await this.appCatalog.getTemplateRepositoryList({
        pageNo: 1,
        pageSize: 99,
        params: {},
      });
      this.repositoryItems = repositories.results || [];
      this.repositoryItems.forEach(item => {
        this.totalTemplate += item.template_num;
      });
      if (!repositories.results || repositories.results.length === 0) {
        this.empty = true;
      } else {
        this.getTemplates();
      }
    } catch (errors) {
      this.errorsToastService.error(errors);
      this.empty = true;
    }
    this.repo_loading = false;
  }

  async getTemplates() {
    this.template_loading = true;
    try {
      const templates = await this.appCatalog.getTemplates({
        pageNo: 1,
        pageSize: this.pageSize,
        params: {},
      });
      this.templateItems = templates.results;
      if (this.templateItems) {
        this.selected_repo_templates = this.templateItems;
        this.display_templates = this.templateItems;
        this.handleData(templates.results);
      }
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
    this.template_loading = false;
  }

  handleData(res: any) {
    this.templateItemsMap = {};
    res.forEach((item: any) => {
      if (this.templateItemsMap[item.repo_name]) {
        this.templateItemsMap[item.repo_name].push(item);
      } else {
        this.templateItemsMap[item.repo_name] = [];
        this.templateItemsMap[item.repo_name].push(item);
      }
    });
  }

  tabSelected(selectedNum: number) {
    if (selectedNum !== 0) {
      this.selectedRepoName = this.repositoryItems[selectedNum - 1].name;
      this.selected_repo_templates =
        this.templateItemsMap[this.selectedRepoName] || [];
      this.display_templates = this.selected_repo_templates.filter(item => {
        return item.name.indexOf(this.keyWord) !== -1;
      });
    } else {
      this.selected_repo_templates = this.templateItems;
      this.display_templates = this.selected_repo_templates.filter(item => {
        return item.name.indexOf(this.keyWord) !== -1;
      });
    }
  }

  createAppClicked(template: AppCatalogTemplate) {
    this.router.navigate(['app_catalog', 'create', template.uuid], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  searchChanged(queryString: string) {
    this.keyWord = queryString;
    this.display_templates = this.selected_repo_templates.filter(item => {
      return item.name.indexOf(queryString) !== -1;
    });
  }

  async importDialogOpen(repository: AppCatalogTemplateRepository) {
    this.store.dispatch(new ImportDialogOpenAction(repository));
  }
}
