import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { defer, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

import { MessageService, NotificationService } from 'alauda-ui';
import * as catalogTemplate from 'app2/features/app-catalog/actions/catalog-template';
import { TemplateListState } from 'app2/features/app-catalog/reducers/catalog-template';
import { AppCatalogService } from 'app2/shared/services/features/app-catalog.service';
import { TranslateService } from 'app2/translate/translate.service';

@Injectable()
export class AppCatalogCatalogTemplateEffects {
  @Effect()
  repoLoad$ = this.actions$.ofType(catalogTemplate.RepoActionType.Load).pipe(
    mergeMap((action: catalogTemplate.RepoLoadAction) =>
      defer(() =>
        this.appCatalog.getTemplateRepositories('', action.payload),
      ).pipe(
        map(repos => new catalogTemplate.RepoLoadCompleteAction(repos[0])),
        catchError((error: Error) =>
          of(new catalogTemplate.RepoLoadErrorAction(error)),
        ),
      ),
    ),
  );

  @Effect()
  templateLoad$ = this.actions$
    .ofType(catalogTemplate.TemplateActionType.Load)
    .pipe(
      mergeMap((action: catalogTemplate.TemplateLoadAction) =>
        defer(() => this.appCatalog.getTemplateById(action.payload)).pipe(
          map(
            template =>
              new catalogTemplate.TemplateLoadCompleteAction(template),
          ),
          catchError((error: Error) => {
            const message = this.translate.get(
              'app_catalog_failed_get_template',
            );
            this.auiNotificationService.error(message);
            return of(
              new catalogTemplate.TemplateLoadErrorAction({
                uuid: action.payload,
                error,
              }),
            );
          }),
        ),
      ),
    );

  @Effect()
  repoRefresh$ = this.actions$
    .ofType(catalogTemplate.RepoActionType.Refresh)
    .pipe(
      mergeMap((action: catalogTemplate.RepoRefreshAction) =>
        defer(() =>
          this.appCatalog.refreshTemplateRepository(action.payload),
        ).pipe(
          map(repo => {
            this.store.dispatch(
              new catalogTemplate.RepoLoadAction(action.payload),
            );
            this.auiMessageService.success({
              content: this.translate.get('app_catalog_sync_trigger_success'),
            });
            return new catalogTemplate.RepoRefreshCompleteAction(repo);
          }),
          catchError(error => {
            if (action.onError) {
              action.onError(error);
            }
            return of(new catalogTemplate.RepoRefreshErrorAction(error));
          }),
        ),
      ),
    );

  constructor(
    private store: Store<TemplateListState>,
    private actions$: Actions,
    private appCatalog: AppCatalogService,
    private translate: TranslateService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
  ) {}
}
