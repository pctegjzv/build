import { Component, Input, OnInit } from '@angular/core';

import { AppPlatformTemplateListPageComponent } from 'app2/features/app-platform/components/pages/app-platform-template-list-page.component';
import { AppCatalogTemplateRepository } from 'app2/shared/services/features/app-catalog.service';

@Component({
  selector: 'rc-app-platform-template-empty-view',
  templateUrl: 'app-platform-template-empty-view.component.html',
  styleUrls: ['app-platform-template-empty-view.component.scss'],
})
export class AppPlatformTemplateEmptyViewComponent implements OnInit {
  @Input()
  repository: AppCatalogTemplateRepository;
  @Input()
  type: string;
  empty_view_title = '';
  empty_view_desc = '';
  nav_type = '';

  constructor(public listPage: AppPlatformTemplateListPageComponent) {}

  ngOnInit() {
    switch (this.type) {
      case 'middleware':
        this.empty_view_title = 'app_catalog_middleware_empty_view_title';
        break;
      case 'micro_service':
        this.empty_view_title = 'app_catalog_micro_service_empty_view_title';
        break;
      case 'big_data':
        this.empty_view_title = 'app_catalog_big_data_empty_view_title';
        break;
    }
    this.nav_type = `nav_${this.type}`;
  }
}
