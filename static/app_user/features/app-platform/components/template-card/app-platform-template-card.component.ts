import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { AppCatalogTemplate } from 'app2/shared/services/features/app-catalog.service';

@Component({
  selector: 'rc-app-platform-template-card',
  templateUrl: 'app-platform-template-card.component.html',
  styleUrls: ['app-platform-template-card.component.scss'],
})
export class AppPlatformTemplateCardComponent implements OnInit {
  @Input()
  template: AppCatalogTemplate;
  @Input()
  applicationPermission: boolean;

  @Output()
  createAppClick = new EventEmitter<Event>();

  warm_tips = '';

  constructor() {}

  ngOnInit() {
    if (!this.template.is_active) {
      this.warm_tips = 'app_catalog_template_already_delete';
    } else if (
      this.template.resource_actions.indexOf('public_helm_template:use') < 0
    ) {
      this.warm_tips = 'app_catalog_no_permission_create_app';
    }
  }

  createAppClicked(event: Event) {
    event.stopPropagation();
    if (
      this.template.resource_actions.indexOf('public_helm_template:use') < 0 ||
      !this.applicationPermission
    ) {
      return false;
    }
    this.createAppClick.emit(event);
  }
}
