import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppPlatformAppCreatePageComponent } from './components/pages/app-platform-create-app-page.component';
import { AppPlatformTemplateListPageComponent } from './components/pages/app-platform-template-list-page.component';

const clusterRoutes: Routes = [
  {
    path: '',
    redirectTo: 'micro_service',
    pathMatch: 'full',
  },
  {
    path: 'micro_service',
    component: AppPlatformTemplateListPageComponent,
  },
  {
    path: 'micro_service/create/:template_id',
    component: AppPlatformAppCreatePageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(clusterRoutes)],
  exports: [RouterModule],
})
export class AppPlatformMicroServiceRoutingModule {}
