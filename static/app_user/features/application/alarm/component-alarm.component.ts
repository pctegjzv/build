import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { DialogService, NotificationService } from 'alauda-ui';
import { debounce } from 'lodash';
import {
  BehaviorSubject,
  Observable,
  Subject,
  combineLatest,
  from,
  of,
} from 'rxjs';
import {
  catchError,
  debounceTime,
  filter,
  first,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { HttpService } from 'app2/shared/services/http/http.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { delay } from 'app2/testing/helpers';
import { TranslateService } from 'app2/translate/translate.service';
import { isHttpErrorResponse } from 'app_user/core/types';
import { ApplicationAlarmFormComponent } from 'app_user/features/application/alarm/update/application-alarm-form.component';
import { MetricQueries } from 'app_user/features/application/monitor/component-monitor.component';
import { METRICS_TYPE } from 'app_user/features/application/monitor/component-monitor.constant';

export interface Alarm {
  name: string;
  group_name: string;
  state: string;
  metric: {
    step: number;
    queries?: MetricQueries[];
  };
  compare: string;
  threshold: number;
  wait?: number;
  label?: {
    label_name: string;
  };
  summary?: string;
  description?: string;
  metric_name?: string;
}

const ORDERED_METRICS = [
  'cpu_usage',
  'memory_usage',
  'sent_bytes',
  'received_bytes',
];

@Component({
  selector: 'rc-component-alarm',
  styleUrls: ['component-alarm.component.scss'],
  templateUrl: 'component-alarm.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ComponentAlarmComponent implements OnInit, OnDestroy {
  @Input()
  uuid: string;
  @Input()
  clusterName: string;
  @Input()
  namespaceName: string;
  @Input()
  componentName: string;
  @Input()
  componentType: string;
  groupName: string;

  private onDestroy$ = new Subject<void>();
  search$ = new BehaviorSubject<string>(null);
  updated$ = new BehaviorSubject(null);
  rawResponse$: Observable<any>;
  list$: Observable<Alarm[]>;
  filteredList$: Observable<Alarm[]>;
  columns = ['name', 'metric', 'status', 'threshold', 'action'];

  initialized: boolean;
  fetching = false;
  searching = false;
  showZeroState = true;

  protected poll$ = new BehaviorSubject(null);
  pollInterval = 60000;
  protected debouncedUpdate: () => void;

  constructor(
    @Inject(ACCOUNT) public account: RcAccount,
    public cdr: ChangeDetectorRef,
    private dialogService: DialogService,
    private modalService: ModalService,
    private translate: TranslateService,
    private httpService: HttpService,
    private errorsToastService: ErrorsToastService,
    private auiNotificationService: NotificationService,
  ) {
    this.debouncedUpdate = debounce(() => {
      this.poll$.next(null);
    }, this.pollInterval);
  }

  getUnit(item: Alarm) {
    switch (item.metric_name) {
      case 'cpu_usage':
      case 'memory_usage':
        return '%';
        break;
      case 'sent_bytes':
      case 'received_bytes':
        return this.translate.get('alarm_metric_network_unit');
    }
    return '';
  }

  getThreshold(item: Alarm) {
    switch (item.metric_name) {
      case 'cpu_usage':
      case 'memory_usage':
        return (item.threshold * 1000000) / 10000;
        break;
      case 'sent_bytes':
      case 'received_bytes':
        return item.threshold * 1;
    }
    return item.threshold;
  }

  map(value: any): any[] {
    return value.result;
  }

  onUpdate(item: any) {
    this.updated$.next(item);
  }

  async ngOnInit() {
    this.groupName = `ml:component|cl:${this.clusterName}|kn:${
      this.namespaceName
    }|cn:${this.componentName}|ct:${this.componentType}`;
    this.rawResponse$ = combineLatest(this.poll$, this.updated$).pipe(
      debounceTime(50),
      tap(() => {
        this.fetching = true;
        this.cdr.markForCheck();
      }),
      switchMap(() =>
        this.fetchResources().pipe(catchError(error => of(error))),
      ),
      tap(res => {
        this.fetching = false;
        this.searching = false;
        this.showZeroState =
          isHttpErrorResponse(res) || (res as any).result.length === 0;
        this.cdr.markForCheck();
        this.debouncedUpdate();
      }),
      publishReplay(1),
      refCount(),
    );

    this.list$ = this.rawResponse$.pipe(map((res: any) => this.map(res)));

    this.list$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(list => !!list),
      )
      .subscribe((list: Alarm[]) => {
        if (list.length) {
          this.initialized = true;
        }
      });
    this.filteredList$ = combineLatest(
      this.list$,
      this.search$.pipe(startWith('')),
    )
      .pipe(
        map(([list, search]) => {
          list = list.map((item: Alarm) => {
            let metric_name = '';
            item.metric.queries[0].labels.forEach((label: any) => {
              if (label.name === '__name__') {
                metric_name = METRICS_TYPE.find(el => {
                  return el.name === label.value;
                }).key;
              }
            });
            return {
              ...item,
              metric_name: metric_name,
            };
          });
          if (search) {
            list = list.filter((item: Alarm) => {
              return item.name.includes(search.trim());
            });
          }
          return list.sort(
            (a, b) =>
              ORDERED_METRICS.indexOf(a.metric_name) -
              ORDERED_METRICS.indexOf(b.metric_name),
          );
        }),
      )
      .pipe(catchError(() => []));
    this.poll$.next(null);
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  fetchResources(): Observable<any> {
    return from(
      this.httpService.request(
        `
      /ajax/v1/alerts/${this.account.namespace}/prometheus/${
          this.clusterName
        }/${this.groupName}`,
        {
          method: 'GET',
          addNamespace: false,
        },
      ),
    );
  }

  trackByFn(_index: number, item: Alarm) {
    return item.name;
  }

  async createAlarm() {
    const modalRef = await this.dialogService.open(
      ApplicationAlarmFormComponent,
      {
        data: {
          alarmParam: {
            uuid: this.uuid,
            clusterName: this.clusterName,
            accountNamespace: this.account.namespace,
            namespaceName: this.namespaceName,
            groupName: this.groupName,
            componentType: this.componentType,
            componentName: this.componentName,
          },
        },
      },
    );
    modalRef.componentInstance.close.pipe(first()).subscribe(async () => {
      modalRef.close();
      await delay(500);
      this.onUpdate(null);
    });
  }

  async updateAlarm(alarm: any) {
    const modalRef = await this.dialogService.open(
      ApplicationAlarmFormComponent,
      {
        data: {
          data: alarm,
          alarmParam: {
            uuid: this.uuid,
            clusterName: this.clusterName,
            accountNamespace: this.account.namespace,
            namespaceName: this.namespaceName,
            groupName: this.groupName,
            componentType: this.componentType,
            componentName: this.componentName,
          },
        },
      },
    );
    modalRef.componentInstance.close.pipe(first()).subscribe(async () => {
      modalRef.close();
      await delay(500);
      this.onUpdate(null);
    });
  }

  async deleteAlarm(alarm: Alarm) {
    try {
      await this.modalService.confirm({
        title: this.translate.get('delete_alarm_title'),
        content: this.translate.get('delete_alarm_content', {
          alarm_name: alarm.name,
        }),
      });
      const endpoint = `/ajax/v1/alerts/${this.account.namespace}/prometheus/${
        this.clusterName
      }`;
      await this.httpService.request<any>(
        `
      ${endpoint}/${this.groupName}/${alarm.name}`,
        {
          method: 'DELETE',
          addNamespace: false,
        },
      );
      this.auiNotificationService.success(
        this.translate.get('alarm_delete_success'),
      );
      await delay(500);
      this.onUpdate(null);
    } catch (rejection) {
      this.errorsToastService.error(rejection);
    }
  }
}
