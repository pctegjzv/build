import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { DIALOG_DATA, NotificationService } from 'alauda-ui';
import { cloneDeep } from 'lodash';
import moment from 'moment';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { MonitorUtilitiesService } from 'app2/shared/services/features/monitor.utilities.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { HttpService } from 'app2/shared/services/http/http.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import {
  ChartData,
  MetricQueries,
  MetricQuery,
  MetricsDetails,
  YAxisScale,
} from 'app_user/features/application/monitor/component-monitor.component';
import {
  AGGREGATORS,
  ALARM_TIME_STAMP_OPTIONS,
  METRICS_TYPE,
  MODES,
  MONITOR_COLOR_SET,
} from 'app_user/features/application/monitor/component-monitor.constant';

import { K8S_RESOURCE_NAME_BASE } from '../../../../../app/components/common/config/common-pattern';

@Component({
  selector: 'rc-application-alarm-form',
  templateUrl: 'application-alarm-form.component.html',
  styleUrls: ['application-alarm-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationAlarmFormComponent implements OnInit {
  @Output()
  close = new EventEmitter<any>();

  @ViewChild('form')
  form: NgForm;

  pods: Array<any>;
  data: any;
  alarmParam: {
    uuid: string;
    clusterName: string;
    accountNamespace: string;
    namespaceName: string;
    groupName: string;
    componentType: string;
    componentName: string;
  };
  unitName = '%';
  alertMetric: any;
  formTitle: string;
  formSubmitText: string;
  initialized = false;
  submitting = false;
  resourceNameReg = K8S_RESOURCE_NAME_BASE;
  thresholdRegex = /^(?!0\d)\d*(\.\d{1,4})?$/;

  private onDestroy$ = new Subject<void>();
  private loadMetrics$ = new BehaviorSubject(null);

  metrics = METRICS_TYPE;
  modes = MODES;
  aggregators = AGGREGATORS;
  timeStampOptions = ALARM_TIME_STAMP_OPTIONS.map(option => {
    return {
      ...option,
      name: option.type + this.translate.get('minutes'),
    };
  });
  step: number;
  end_time: number;
  compareOptions = ['>', '>=', '==', '<=', '<', '!='];
  chart: any;
  chartData: ChartData[] = [];
  chartScale: YAxisScale;

  pollNewest = true;

  payload: any;
  viewModel: any;

  constructor(
    private cdr: ChangeDetectorRef,
    private httpService: HttpService,
    private monitorUtilitiesService: MonitorUtilitiesService,
    private serviceService: ServiceService,
    private translate: TranslateService,
    private errorsToastService: ErrorsToastService,
    private auiNotificationService: NotificationService,
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(DIALOG_DATA) private dialogData: any,
  ) {}

  async ngOnInit() {
    this.data = this.dialogData.data;
    this.alarmParam = this.dialogData.alarmParam;
    this.initSelectOptions();
    if (this.data) {
      this.formTitle = 'alarm_update';
      this.formSubmitText = 'update';
    } else {
      this.formTitle = 'alarm_create';
      this.formSubmitText = 'create';
    }
    const response = await this.serviceService.getK8sServiceInstances(
      this.alarmParam.uuid,
    );
    this.pods = response['result'].map(pod => {
      return pod.metadata.name;
    });
    await this.initViewModel();
    this.queryMetrics();
    this.cdr.markForCheck();
    this.initialized = true;
  }

  private initSelectOptions() {
    this.chart = {
      scheme: { domain: MONITOR_COLOR_SET },
      xAxisTickFormatting: (tick: string) => {
        return moment(parseInt(tick, 10) * 1000).format('MM-DD HH:mm');
      },
      yAxisTickFormatting: (val: number) => {
        if (val * 100 > 0.01 || val === 0) {
          return (val * 100).toPrecision(3) + '%';
        }
        return (val * 100).toFixed(4) + '%';
      },
    };
  }

  parseTooltip(value: number) {
    switch (this.viewModel.metric) {
      case this.metrics[0].key:
      case this.metrics[1].key:
        return (value * 100).toFixed(4) + '%';
      case this.metrics[2].key:
      case this.metrics[3].key:
        return (
          (value * 1).toFixed(4) +
          this.translate.get('alarm_metric_network_unit')
        );
    }
  }

  private async initViewModel() {
    this.viewModel = {
      metric: this.metrics[0].key,
      mode: this.modes[0].key,
      aggregator: this.aggregators[0].key,
      timeStampOption: this.timeStampOptions[0].offset,
      compare: this.compareOptions[0],
    };
    if (this.data) {
      this.viewModel.compare = this.data.compare;
      this.viewModel.timeStampOption = this.data.metric.step * 1000;
      if (this.data.metric.queries && this.data.metric.queries[0]) {
        this.data.metric.queries[0].labels.forEach((element: any) => {
          if (element.name === '__name__') {
            this.viewModel.metric = element.value;
          }
        });
        if (
          this.viewModel.metric === this.metrics[0].key ||
          this.viewModel.metric === this.metrics[1].key
        ) {
          this.viewModel.threshold = (this.data.threshold * 1000000) / 10000;
        } else {
          this.viewModel.threshold = this.data.threshold;
          this.unitName = this.translate.get('alarm_metric_network_unit');
          this.chart.yAxisTickFormatting = (val: number) => {
            if (val >= 1000) {
              return val / 1000 + 'k';
            } else {
              return val;
            }
          };
        }
        switch (this.data.metric.queries[0].group_by) {
          case 'pod_name':
            this.viewModel.mode = 'pod';
            break;
          case 'container_name':
            this.viewModel.mode = 'container';
            break;
        }
        this.viewModel.aggregator = this.data.metric.queries[0].aggregator;
      }
    }
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    const endpoint = `/ajax/v1/alerts/${this.alarmParam.clusterName}`;
    const body = {
      name: this.viewModel.name,
      group_name: this.alarmParam.groupName,
      metric: {
        step: this.viewModel.timeStampOption / 1000,
        queries: this.alertMetric.queries,
      },
      compare: this.viewModel.compare,
      threshold: 0,
    };
    if (
      this.viewModel.metric === this.metrics[0].key ||
      this.viewModel.metric === this.metrics[1].key
    ) {
      body.threshold = this.viewModel.threshold / 100;
    } else {
      body.threshold = this.viewModel.threshold / 1;
    }
    if (this.data) {
      delete body.name;
      try {
        this.submitting = true;
        this.httpService
          .request<any>(
            `
        ${endpoint}/prometheus/${this.alarmParam.groupName}/${this.data.name}`,
            {
              method: 'PUT',
              body,
              addNamespace: false,
            },
          )
          .then(() => {
            this.close.next(null);
            this.auiNotificationService.success(
              this.translate.get('alarm_update_success'),
            );
          })
          .catch(error => {
            this.errorsToastService.error(error);
          });
        this.submitting = false;
      } catch (rejection) {
        this.close.next(null);
      }
    } else {
      try {
        this.submitting = true;
        this.httpService
          .request<any>(endpoint, {
            method: 'POST',
            body,
            addNamespace: false,
          })
          .then(() => {
            this.close.next(null);
            this.auiNotificationService.success(
              this.translate.get('alarm_create_success'),
            );
          })
          .catch(error => {
            this.errorsToastService.error(error);
          });
        this.submitting = false;
      } catch (rejection) {
        this.close.next(null);
      }
    }
  }

  cancel() {
    this.close.next(null);
  }

  onMetricSelect(metric: string) {
    switch (metric) {
      case this.metrics[0].key:
      case this.metrics[1].key:
        this.unitName = '%';
        this.chart.yAxisTickFormatting = (val: number) => {
          if (val * 100 > 0.01 || val === 0) {
            return (val * 100).toPrecision(3) + '%';
          }
          return (val * 100).toFixed(4) + '%';
        };
        break;
      case this.metrics[2].key:
      case this.metrics[3].key:
        this.unitName = this.translate.get('alarm_metric_network_unit');
        this.chart.yAxisTickFormatting = (val: number) => {
          if (val >= 1000) {
            return val / 1000 + 'k';
          } else {
            return val;
          }
        };
    }
    this.loadCharts();
  }

  onAggregatorSelect() {
    this.loadCharts();
  }

  onTimeStampOptionSelect() {
    this.loadCharts();
  }

  loadCharts() {
    this.loadMetrics$.next({
      metric: this.viewModel.metric,
      mode: this.viewModel.mode,
      aggregator: this.viewModel.aggregator,
      step: this.viewModel.timeStampOption / 1000,
    });
  }

  private getYAxisValueScale(data: ChartData[]) {
    let {
      yScaleMin,
      yScaleMax,
    } = this.monitorUtilitiesService.getYAxisValueScale(data);
    if (yScaleMin < 0.000001) {
      yScaleMin = 0;
    }
    if (yScaleMax < 0.000001) {
      yScaleMax = 0.000001;
    }
    return { yScaleMin, yScaleMax };
  }

  private parseMetricsRequest(
    query: MetricQueries,
    metricParams: MetricQuery,
    nameLabels: string,
  ) {
    const queryCloned = cloneDeep(query);
    queryCloned.labels.push({
      type: 'EQUAL',
      name: '__name__',
      value: nameLabels,
    });
    if (
      nameLabels === this.metrics[2].key ||
      nameLabels === this.metrics[3].key
    ) {
      queryCloned.downsample = 'rate';
    }
    const params = cloneDeep(metricParams);
    params.queries = [queryCloned];
    this.alertMetric = params;
    const endpoint = `/ajax/v1/metrics/${
      this.alarmParam.clusterName
    }/query_range`;
    return this.httpService.request<MetricsDetails>(endpoint, {
      method: 'POST',
      body: params,
      addNamespace: false,
    });
  }

  private parseMetricsResponse(result: any) {
    if (result === undefined || result.result === undefined) {
      return [];
    }
    return result.result.map((el: any) => {
      // fill in the chart when result is less than 30 points
      if (el.values.length < 30) {
        let dateNewest = this.end_time;
        const obj = {};
        for (let i = 0; i < 30; i++) {
          obj[(dateNewest -= this.step)] = 0;
        }
        if (el.values.length > 0) {
          el.values.forEach((element: any) => {
            obj[element[0]] = element[1];
          });
        }
        el.values = Object.keys(obj).map(function(key) {
          return [Number(key), obj[key]];
        });
      }
      return {
        name:
          el.metric.pod_name ||
          el.metric.container_name ||
          this.alarmParam.componentName,
        series: el.values.map((value: number[]) => {
          return {
            value: value[1],
            name: value[0] + '',
          };
        }),
      };
    });
  }

  private queryMetrics() {
    if (!this.viewModel.mode || !this.viewModel.aggregator) {
      return;
    }
    this.loadMetrics$.pipe(takeUntil(this.onDestroy$)).subscribe(args => {
      if (args === null) {
        return;
      }
      const current_time = parseInt((moment().valueOf() / 1000).toFixed(0), 10);
      this.step = args.step;
      this.end_time = current_time;
      const params: MetricQuery = {
        start: current_time - args.step * 29,
        end: current_time,
        step: args.step,
      };
      const query: MetricQueries = {
        aggregator: args.aggregator,
        downsample: 'avg',
        labels: [
          {
            type: 'EQUAL',
            name: 'namespace',
            value: this.alarmParam.namespaceName,
          },
          {
            type: 'EQUAL',
            name: 'kind',
            value: this.alarmParam.componentType,
          },
          {
            type: 'EQUAL',
            name: 'name',
            value: this.alarmParam.componentName,
          },
          {
            type: 'IN',
            name: 'pod_name',
            value: this.pods.join('|'),
          },
        ],
      };
      switch (args.mode) {
        case 'pod':
          query.group_by = 'pod_name';
          break;
        case 'container':
          query.group_by = 'container_name';
      }
      this.parseMetricsRequest(query, params, args.metric).then(
        result => {
          if (result) {
            this.chartData = this.parseMetricsResponse(result);
            this.chartScale = this.getYAxisValueScale(this.chartData);
            this.cdr.markForCheck();
          }
        },
        error => {
          this.errorsToastService.error(error);
          this.chartData = [];
          this.cdr.markForCheck();
        },
      );
    });

    this.loadCharts();
  }
}
