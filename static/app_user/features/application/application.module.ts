import { NgModule } from '@angular/core';
import { ApplicationRoutingModule } from 'app_user/features/application/application.routing.module';
import { ApplicationCreateComponent } from 'app_user/features/application/create/application-create.component';
import { ApplicationDetailComponent } from 'app_user/features/application/detail/application-detail.component';
import { ApplicationFormComponent } from 'app_user/features/application/form/application-form.component';
import { ApplicationListComponent } from 'app_user/features/application/list/application-list.component';
import { SharedModule } from 'app_user/shared/shared.module';

import { K8sEventListComponent } from 'app2/features/cluster/k8s-event/list/k8s-event-list.component';
import { ServiceConfigAffinityComponent } from 'app2/features/service/config/service-config-affinity.component';
import { ServiceConfigKubeServiceComponent } from 'app2/features/service/config/service-config-kube-service.component';
import { ServiceConfigLabelsComponent } from 'app2/features/service/config/service-config-labels.component';
import { ServiceConfigNodeTagsComponent } from 'app2/features/service/config/service-config-node-tags.component';
import { ServiceDetailConfigComponent } from 'app2/features/service/config/service-detail-config.component';
import { ContainerConfigVolumeComponent } from 'app2/features/service/container-config/service-container-config-volume.component';
import { ContainerConfigComponent } from 'app2/features/service/container-config/service-container-config.component';
import { ContainerConfigConfigmapComponent } from 'app2/features/service/container-config/service-container-configmap.component';
import { ContainerConfigEnvvarsComponent } from 'app2/features/service/container-config/service-container-envvars.component';
import { ContainerConfigHealthcheckComponent } from 'app2/features/service/container-config/service-container-healthcheck.component';
import { ContainerConfigLogFileComponent } from 'app2/features/service/container-config/service-container-log-file.component';
import { ContainerFieldsConfigmapComponent } from 'app2/features/service/create-fields/container-configmap.component';
import { ContainerFieldsEnvvarsComponent } from 'app2/features/service/create-fields/container-envvars.component';
import { HealthcheckDialogComponent } from 'app2/features/service/create-fields/container-healthcheck-dialog.component';
import { HealthcheckComponent } from 'app2/features/service/create-fields/container-healthcheck.component';
import { ContainerFieldsResourceSizeComponent } from 'app2/features/service/create-fields/container-resource-size.component';
import { ContainerVolumeDialogComponent } from 'app2/features/service/create-fields/container-volume-dialog.component';
import { ContainerFieldsVolumeComponent } from 'app2/features/service/create-fields/container-volume.component';
import { ServiceFieldsKubeServiceDialogComponent } from 'app2/features/service/create-fields/kube-service-dialog.component';
import { ServiceFieldsKubeServiceComponent } from 'app2/features/service/create-fields/kube-service.component';
import { ServiceFieldsLabelsComponent } from 'app2/features/service/create-fields/service-labels.component';
import { ServiceFieldsNetworkModeComponent } from 'app2/features/service/create-fields/service-network-mode.component';
import { ServiceFieldsNodeAffinityComponent } from 'app2/features/service/create-fields/service-node-affinity.component';
import { ServiceFieldsNodeTagsComponent } from 'app2/features/service/create-fields/service-node-tags.component';
import { ServiceCreateMethodSelectComponent } from 'app2/features/service/create-method-select/service-create-method-select.component';
import { EndpointsDisplayComponent } from 'app2/features/service/endpoints-display/endpoints-display.component';
import { ServiceImageDisplayComponent } from 'app2/features/service/image-display/image-display.component';
import { ServiceImageListComponent } from 'app2/features/service/image-display/image-list.component';
import { ImageSelectComponent } from 'app2/features/service/image-select/image-select.component';
import { ServiceInstancesComponent } from 'app2/features/service/instances/service-instances.component';
import { ServiceNameValidatorDirective } from 'app2/features/service/validator/service-name-validator.directive';
import { ResourceNameBlackListValidatorDirective } from 'app2/shared/directives/validators/resource-name-blacklist-validator.directive';
import { ComponentAlarmComponent } from 'app_user/features/application/alarm/component-alarm.component';
import { ApplicationAlarmFormComponent } from 'app_user/features/application/alarm/update/application-alarm-form.component';
import { AffinityFieldsetComponent } from 'app_user/features/application/fieldset/affinity-fieldset.component';
import { AppOwnersFieldsetComponent } from 'app_user/features/application/fieldset/app-owners-fieldset.component';
import { ContainerEnvFieldsetComponent } from 'app_user/features/application/fieldset/container-env-fieldset.component';
import { ContainerFieldsetComponent } from 'app_user/features/application/fieldset/container-fieldset.component';
import { ContainerResourcesFieldsetComponent } from 'app_user/features/application/fieldset/container-resources-fieldset.component';
import { ContainerStorageDialogComponent } from 'app_user/features/application/fieldset/container-storage-dialog.component';
import { ContainerStorageFieldsetComponent } from 'app_user/features/application/fieldset/container-storage-fieldset.component';
import { RouteDialogComponent } from 'app_user/features/application/fieldset/route-dialog.component';
import { RouteFieldsetComponent } from 'app_user/features/application/fieldset/route-fieldset.component';
import { ApplicationYamlFormComponent } from 'app_user/features/application/form/application-yaml-form.component';
import { ImageSelectDialogComponent } from 'app_user/features/application/image-select/image-select-dialog.component';
import { ComponentLogComponent } from 'app_user/features/application/log/component-log.component';
import { ComponentMonitorComponent } from 'app_user/features/application/monitor/component-monitor.component';
import { ApplicationUpdateComponent } from 'app_user/features/application/update/application-update.component';
import { OwnerDisplayComponent } from 'app_user/shared/components/owner-display/owner-display.component';

const SHARED_SERVICE_FIELD_COMPONENTS = [
  ServiceFieldsKubeServiceComponent,
  ContainerFieldsEnvvarsComponent,
  ContainerFieldsConfigmapComponent,
  ContainerFieldsResourceSizeComponent,
  ContainerFieldsVolumeComponent,
  ServiceFieldsNodeAffinityComponent,
  ServiceFieldsNodeTagsComponent,
  ServiceFieldsLabelsComponent,
  ServiceFieldsKubeServiceDialogComponent,
  HealthcheckComponent,
  HealthcheckDialogComponent,
  ServiceNameValidatorDirective,
  ResourceNameBlackListValidatorDirective,
  ContainerVolumeDialogComponent,
  ServiceFieldsNetworkModeComponent,
];

const SHARED_SERVICE_DETAIL_COMPONENTS = [
  ServiceInstancesComponent,
  ServiceDetailConfigComponent,
  ServiceConfigKubeServiceComponent,
  ContainerConfigComponent,
  ContainerConfigEnvvarsComponent,
  ContainerConfigConfigmapComponent,
  ContainerConfigHealthcheckComponent,
  ServiceConfigAffinityComponent,
  ServiceConfigLabelsComponent,
  ServiceConfigNodeTagsComponent,
  ContainerConfigVolumeComponent,
  ContainerConfigLogFileComponent,
  OwnerDisplayComponent,
];

const SHARED_APPLICATIONS_COMPONENTS = [
  EndpointsDisplayComponent,
  ServiceImageDisplayComponent,
  ServiceImageListComponent,
];

const SHARED_APPLICATION_ENTRY_COMPONENTS = [
  ImageSelectComponent,
  ServiceFieldsKubeServiceDialogComponent,
  HealthcheckDialogComponent,
  ContainerVolumeDialogComponent,
  ServiceCreateMethodSelectComponent,
];

@NgModule({
  imports: [SharedModule, ApplicationRoutingModule],
  declarations: [
    ...SHARED_SERVICE_DETAIL_COMPONENTS,
    ...SHARED_SERVICE_FIELD_COMPONENTS,
    ...SHARED_APPLICATIONS_COMPONENTS,
    ...SHARED_APPLICATION_ENTRY_COMPONENTS,
    ApplicationAlarmFormComponent,
    ApplicationFormComponent,
    ApplicationYamlFormComponent,
    ApplicationListComponent,
    ApplicationDetailComponent,
    ApplicationCreateComponent,
    ApplicationUpdateComponent,
    AffinityFieldsetComponent,
    AppOwnersFieldsetComponent,
    ContainerFieldsetComponent,
    ContainerResourcesFieldsetComponent,
    ContainerEnvFieldsetComponent,
    RouteFieldsetComponent,
    RouteDialogComponent,
    ContainerStorageFieldsetComponent,
    ContainerStorageDialogComponent,
    ImageSelectDialogComponent,
    ComponentLogComponent,
    ComponentAlarmComponent,
    ComponentMonitorComponent,
    K8sEventListComponent,
  ],
  entryComponents: [
    ...SHARED_APPLICATION_ENTRY_COMPONENTS,
    ApplicationAlarmFormComponent,
    RouteDialogComponent,
    ContainerStorageDialogComponent,
    ImageSelectDialogComponent,
  ],
})
export class ApplicationModule {}
