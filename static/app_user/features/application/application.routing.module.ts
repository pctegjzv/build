import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ApplicationCreateComponent } from 'app_user/features/application/create/application-create.component';
import { ApplicationDetailComponent } from 'app_user/features/application/detail/application-detail.component';
import { ApplicationListComponent } from 'app_user/features/application/list/application-list.component';
import { ApplicationUpdateComponent } from 'app_user/features/application/update/application-update.component';

const routes: Routes = [
  {
    path: '',
    component: ApplicationListComponent,
  },
  {
    path: 'detail/:uuid',
    component: ApplicationDetailComponent,
  },
  {
    path: 'update/:uuid',
    component: ApplicationUpdateComponent,
  },
  {
    path: 'create',
    component: ApplicationCreateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApplicationRoutingModule {}
