import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  templateUrl: 'application-create.component.html',
})
export class ApplicationCreateComponent implements OnInit {
  type: string;
  projectName: string;
  clusterName: string;
  namespaceName: string;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private workspaceComponent: WorkspaceComponent,
  ) {
    const queryParamMap = this.activatedRoute.snapshot.queryParamMap;
    this.type = queryParamMap.get('type');
    if (!['repo', 'yaml'].includes(this.type)) {
      this.router.navigate(['app'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
    const params = this.workspaceComponent.baseParamsSnapshot;
    this.projectName = params['project'];
    this.clusterName = params['cluster'];
    this.namespaceName = params['namespace'];
  }
  ngOnInit() {}
}
