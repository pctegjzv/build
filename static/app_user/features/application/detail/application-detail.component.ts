import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { DialogService, NotificationService } from 'alauda-ui';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';

import { ImageDisplayParams } from 'app2/features/service/image-display/image-display.component';
import { ServiceInstancesComponent } from 'app2/features/service/instances/service-instances.component';
import { ServiceUtilitiesService } from 'app2/features/service/service-utilities.service';
import {
  AppService,
  Application,
  MirrorApp,
} from 'app2/shared/services/features/app.service';
import { AppUtilitiesService } from 'app2/shared/services/features/app.utilities.service';
import {
  K8sResourceService,
  K8sService,
  Route,
} from 'app2/shared/services/features/k8s-resource.service';
import { Cluster } from 'app2/shared/services/features/region.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { MirrorResourceDeleteConfirmComponent } from 'app_user/shared/components/mirror-resource-delete-confirm/mirror-resource-delete-confirm.component';
import { get } from 'lodash';
import { first } from 'rxjs/operators';

@Component({
  templateUrl: 'application-detail.component.html',
  styleUrls: ['application-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationDetailComponent implements OnInit, OnDestroy {
  private appUuid: string;
  private serviceUuid: string;
  private instances: ServiceInstancesComponent;
  private cmb_nodeport_domain: string;
  initialized: boolean;
  pollingTimer: any;
  destroyed: boolean;
  appData: Application;
  appYaml: string;
  appAddresses: string[] = [];
  serviceAddress: string[] = [];

  loading: boolean;
  loadError: any;
  appOwners: any;
  imageDisplayParams: ImageDisplayParams[];
  mirrorApps: MirrorApp[] = [];
  orginMirrorApps: MirrorApp[] = [];
  clusterDisplayName: string;

  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: true,
  };

  @ViewChild('instances')
  set instancesComponent(instances: ServiceInstancesComponent) {
    this.instances = instances;
  }

  constructor(
    @Inject(ENVIRONMENTS) private environments: Environments,
    private activatedRoute: ActivatedRoute,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
    private router: Router,
    private cdr: ChangeDetectorRef,
    private appService: AppService,
    private errorsToastService: ErrorsToastService,
    private regionService: RegionService,
    private workspaceComponent: WorkspaceComponent,
    public appUtilities: AppUtilitiesService,
    public serviceUtilities: ServiceUtilitiesService,
    private dialogService: DialogService,
    private k8sResourceService: K8sResourceService,
  ) {
    this.cmb_nodeport_domain = this.environments.cmb_nodeport_domain;
  }

  async ngOnInit() {
    this.activatedRoute.params.subscribe(async (params: Params) => {
      this.initialized = false;
      this.appUuid = params['uuid'];
      this.serviceUuid = null;
      if (!this.appUuid) {
        this.back();
      }
      await this.refetch();
      this.initialized = true;
      this.getClusterDisplayName();
      this.getMirrorApps();
      this.getAppYaml();
    });
  }

  ngOnDestroy() {
    clearTimeout(this.pollingTimer);
    this.destroyed = true;
  }

  back() {
    this.router.navigate(['app'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  async refetch() {
    if (this.destroyed) {
      return;
    }
    clearTimeout(this.pollingTimer);
    this.loading = true;
    try {
      this.appData = await this.appService.getK8sApp(this.appUuid);
      if (!this.serviceUuid) {
        this.serviceUuid = get(this.appData, 'services[0].resource.uuid');
      }
      this.imageDisplayParams = this.getImageDisplayParams(
        this.appData.kubernetes,
      );
      this.appOwners = this.getAppOwner(this.appData.kubernetes);
      this.refreshInstances();
      this.refreshAddress();
      this.loadError = null;
    } catch ({ status, errors }) {
      this.loadError = errors;
      if (status === 403) {
        this.auiNotificationService.warning(
          this.translateService.get('permission_denied'),
        );
      } else if (status === 404 && !this.initialized) {
        this.auiNotificationService.warning(
          this.translateService.get('application_not_exist'),
        );
      }
      this.back();
    }

    this.loading = false;
    this.resetPollingTimer();
    if (!this.destroyed) {
      this.cdr.detectChanges();
    }
  }

  async startApp() {
    try {
      const response = await this.appUtilities.startApp(this.appData);
      this.refetch();
      return response;
    } catch (rejection) {
      this.errorsToastService.error(rejection);
    }
  }

  async stopApp() {
    try {
      const response = await this.appUtilities.stopApp(this.appData);
      this.refetch();
      return response;
    } catch (rejection) {
      this.errorsToastService.error(rejection);
    }
  }

  updateApp() {
    this.router.navigate(['app', 'update', this.appUuid], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  async deleteApp() {
    if (this.orginMirrorApps.length) {
      const requireSelect = this.orginMirrorApps.filter(item => {
        return item.uuid === this.appData.resource.uuid;
      });
      const modalRef = await this.dialogService.open(
        MirrorResourceDeleteConfirmComponent,
      );
      const mirrorClusterMeta = {
        displayName: 'cluster_display_name',
        mirrorResource: this.orginMirrorApps,
        confirmTitle: this.translateService.get('delete_app'),
        confirmLabel: this.translateService.get('cluster'),
        confirmContent: this.translateService.get(
          'app_service_delete_selected_cluster_app_confirm',
          {
            app_name: this.appData.resource.name,
          },
        ),
      };
      modalRef.componentInstance.setMirrorResourceMeta(
        mirrorClusterMeta,
        requireSelect,
      );
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe(async (res: MirrorApp[]) => {
          modalRef.close();
          if (res) {
            try {
              this.appUtilities.deleteMirrorApps(res);
              this.back();
            } catch (err) {
              this.errorsToastService.error(err);
            }
          }
        });
    } else {
      this.appUtilities.deleteApp(this.appData);
    }
  }

  redirectToMirrorApp(app: MirrorApp) {
    this.router.navigate([
      '/workspace',
      {
        project: this.workspaceComponent.baseParamsSnapshot['project'],
        cluster: app.cluster_name,
        namespace: app.namespace_name,
      },
      'app',
      'detail',
      app.uuid,
    ]);
  }

  private async getAppYaml() {
    try {
      this.appYaml = await this.appService.getK8sAppYaml(this.appUuid);
    } catch (err) {
      //
    }
  }

  private refreshInstances() {
    setTimeout(() => {
      if (this.instances) {
        this.instances.refetch(); //  refresh pod list
      }
    });
  }

  private async refreshAddress() {
    if (this.cmb_nodeport_domain) {
      // 获取内部路由信息
      const serviceNames = await this.getK8sServices();
      this.getK8sRoutes(serviceNames);
    }
  }

  private async getK8sServices() {
    const clusterName = this.appData.cluster.name;
    const namespace = this.appData.namespace.name;
    const res = await Promise.all([
      this.k8sResourceService.getK8sReourceList('services', {
        clusterName,
        namespace,
      }),
      this.k8sResourceService.getK8sTopology({
        cluster: clusterName,
        namespace,
        kind: 'Deployment',
        name: this.appData.resource.name,
      }),
    ]);
    const names = res[1].referenced_by.map((item: any) => {
      return item.node.name;
    });
    this.serviceAddress = [];
    res[0].map((item: K8sService) => {
      if (
        names.includes(item.kubernetes.metadata.name) &&
        item.kubernetes.spec.type === 'NodePort'
      ) {
        item.kubernetes.spec.ports.map(_item => {
          this.serviceAddress.push(
            `http://${this.cmb_nodeport_domain}:${_item.nodePort}`,
          );
        });
      }
    });
    this.cdr.detectChanges();
    return names;
  }

  private async getK8sRoutes(serviceNames: string[]) {
    const clusterName = this.appData.cluster.name;
    const namespace = this.appData.namespace.name;
    let res: Route[];
    try {
      res = await this.k8sResourceService.getK8sReourceList('routes', {
        clusterName,
        namespace,
      });
    } catch (err) {
      return;
    }
    this.appAddresses = [];
    res.map((item: Route) => {
      if (serviceNames.includes(item.kubernetes.spec.to.name)) {
        const path = item.kubernetes.spec.path ? item.kubernetes.spec.path : '';
        this.appAddresses.push(`http://${item.kubernetes.spec.host}${path}`);
      }
    });
    this.cdr.detectChanges();
  }

  private resetPollingTimer() {
    clearTimeout(this.pollingTimer);
    if (this.destroyed) {
      return;
    }
    const waitTime =
      get(this.appData, 'resource.status', '').toLowerCase() === 'deploying'
        ? 3000
        : 10000;
    this.pollingTimer = setTimeout(() => {
      this.refetch();
    }, waitTime);
  }

  private async getMirrorApps() {
    const mirrorApps = await this.appUtilities.getMirrorApps(this.appData);
    this.orginMirrorApps = mirrorApps;
    this.mirrorApps = mirrorApps.filter((app: MirrorApp) => {
      return app.uuid !== this.appData.resource.uuid;
    });
    this.cdr.detectChanges();
  }

  private async getClusterDisplayName() {
    if (this.appData) {
      const cluster: Cluster = await this.regionService.getCluster(
        this.appData.cluster.name,
      );
      this.clusterDisplayName = cluster
        ? cluster.display_name
        : this.appData.cluster.name;
    }
  }

  // TODO: refactor in service utilities
  private getImageDisplayParams(kubernetes: any) {
    const service = kubernetes.find((item: { kind: string }) => {
      return ['Deployment', 'StatefulSet', 'DaemonSet'].includes(item.kind);
    });
    // 参考：http://confluence.alaudatech.com/display/DEV/Kubernetes+Yaml+Explain
    if (service) {
      return service.spec.template.spec.containers.map((container: any) => {
        const size: any = {};
        if (container.resources && container.resources.requests) {
          if (container.resources.requests.cpu) {
            size.cpu = container.resources.requests.cpu;
          }
          if (container.resources.requests.memory) {
            size.memory = container.resources.requests.memory;
          }
        }
        return {
          image: container.image,
          size,
        };
      });
    } else {
      return null;
    }
  }
  private getAppOwner(kubernetes: any) {
    const service = kubernetes.find((item: { kind: string }) => {
      return item.kind === 'Deployment';
    });
    let appOwners: any[] = [];
    if (service) {
      const appOwnerMetada = service.spec.template.metadata.annotations;
      if (appOwnerMetada) {
        if (appOwnerMetada['owner.cmb.io/info']) {
          appOwners = JSON.parse(appOwnerMetada['owner.cmb.io/info']);
        } else {
          appOwners = [
            {
              name: appOwnerMetada['owner.cmb.io/name'],
              phone: appOwnerMetada['owner.cmb.io/phone'],
            },
          ];
        }
      } else {
        // no appOwnerMetada maby create by yaml
        appOwners = [
          {
            name: '',
            phone: '',
          },
        ];
      }
    }
    return appOwners;
  }
}
