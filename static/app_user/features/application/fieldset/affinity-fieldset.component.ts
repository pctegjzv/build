import {
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Self,
  ViewChild,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';
import { MultiSelectionDropdownComponent } from 'app2/shared/components/select/multi-selection-dropdown.component';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { delay } from 'app2/testing/helpers';
import { TranslateService } from 'app2/translate/translate.service';
import { chain, cloneDeep, get, remove } from 'lodash';

/**
 * value: {
 *  affinityServices: [{name: "servic1", uuid: 'xxxx', namespace: "default"}],
 *  antiAffinityServices: [{name: "service2", uuid: 'xxxx', namespace: "default"}]
 * }
 */
@Component({
  selector: 'rc-affinity-fieldset',
  templateUrl: 'affinity-fieldset.component.html',
  styleUrls: ['affinity-fieldset.component.scss'],
})
export class AffinityFieldsetComponent
  implements OnInit, OnDestroy, ControlValueAccessor {
  private _value: any;
  @Input()
  self: any; // self service
  @Input()
  clusterName: string;
  @ViewChild('affinitySelect')
  affinitySelect: MultiSelectionDropdownComponent;
  @ViewChild('antiAffinitySelect')
  antiAffinitySelect: MultiSelectionDropdownComponent;

  private affinityServices: any[] = [];
  private antiAffinityServices: any[] = [];

  affinityServiceOptions: any[] = [];
  antiAffinityServiceOptions: any[] = [];

  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() protected parentForm: NgForm,
    @Optional() protected parentFormGroup: FormGroupDirective,
    @Optional() protected cdr: ChangeDetectorRef,
    private serviceService: ServiceService,
    private translateService: TranslateService,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
    this._value = null;
  }

  async ngOnInit() {
    const { results } = await this.serviceService.getK8sServices({
      pageSize: 100,
      params: {
        cluster: this.clusterName,
      },
    });
    this.initServiceOptions(results);
  }

  ngOnDestroy() {}

  async initServiceOptions(services: any[]) {
    const serviceOptions = services.map((service: any) => {
      return {
        name: service.resource.name,
        uuid: service.resource.uuid,
        namespace: service.namespace.name,
      };
    });

    if (!this.self) {
      // 创建服务
      this.affinityServiceOptions = cloneDeep(serviceOptions);
      this.antiAffinityServiceOptions = [
        {
          uuid: '$$self',
          name: this.translateService.get('affinity_app_self'),
        },
      ].concat(cloneDeep(serviceOptions));
    } else {
      // 更新服务，需要初始化dropdown选项
    }
    if (this._value) {
      await delay(0);
      const affinityServices: string[] = get(
        this._value,
        'affinityServices',
        [],
      );
      const antiAffinityServices: string[] = get(
        this._value,
        'antiAffinityServices',
        [],
      );
      this.affinitySelect.setSelectedValue(
        affinityServices.map((item: any) => {
          return item.name;
        }),
      );
      this.antiAffinitySelect.setSelectedValue(
        antiAffinityServices.map((item: any) => {
          return item.name;
        }),
      );
    }
  }

  addAffinityService(option: any) {
    this.affinityServices = chain(this.affinityServices)
      .concat(option)
      .uniqBy('name')
      .value();
    this.antiAffinityServiceOptions.forEach((item: any) => {
      if (item.uuid === option.uuid) {
        item['$$rcItemDisabled'] = true;
      }
    });
    this.updateModelValue();
  }

  removeAffinityService(option: any) {
    remove(this.affinityServices, (item: any) => {
      return item.name === option.name;
    });
    this.antiAffinityServiceOptions.forEach((item: any) => {
      if (item.name === option.name) {
        item['$$rcItemDisabled'] = false;
      }
    });
    this.updateModelValue();
  }

  addAntiAffinityService(option: any) {
    this.antiAffinityServices = chain(this.antiAffinityServices)
      .concat(option)
      .uniqBy('name')
      .value();
    this.affinityServiceOptions.forEach((item: any) => {
      if (item.name === option.name) {
        item['$$rcItemDisabled'] = true;
      }
    });
    this.updateModelValue();
  }

  removeAntiAffinityService(option: any) {
    remove(this.antiAffinityServices, (item: any) => {
      return item.name === option.name;
    });
    this.affinityServiceOptions.forEach((item: any) => {
      if (item.name === option.name) {
        item['$$rcItemDisabled'] = false;
      }
    });
    this.updateModelValue();
  }

  // set ngModel value when native view changes
  private updateModelValue() {
    this._value = {
      affinityServices: this.affinityServices.map((item: any) => {
        return {
          uuid: item.uuid,
          name: item.name,
          namespace: item.namespace,
        };
      }),
      antiAffinityServices: this.antiAffinityServices.map((item: any) => {
        return {
          uuid: item.uuid,
          name: item.name,
          namespace: item.namespace,
        };
      }),
    };
    this.emitToModel();
  }

  // emit _value to ngModel
  private emitToModel() {
    this.onTouchedCallback();
    this.onChangeCallback(this._value);
  }

  // apply _value to native view
  private applyToNative() {}

  /** Following Methods Implements ControlValueAccessor **/

  writeValue(value: any) {
    this._value = value;
    this.applyToNative();
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}
