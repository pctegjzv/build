import { Component, ViewChild, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, NgForm } from '@angular/forms';
import { BaseItemsFieldsetComponent } from 'app_user/shared/abstract/base-items-fieldset.component';

export interface Owner {
  indexId?: number;
  name: string;
  phone: string;
}

@Component({
  selector: 'rc-app-owners-fieldset',
  templateUrl: './app-owners-fieldset.component.html',
  styleUrls: ['../../../styles/table-list-fieldset.styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AppOwnersFieldsetComponent),
      multi: true,
    },
  ],
})
export class AppOwnersFieldsetComponent extends BaseItemsFieldsetComponent {
  value: Owner[] = [];
  defaultValue: Owner = {
    name: '',
    phone: '',
  };
  numberReg = {
    pattern: /^-?\d+$/,
    tip: 'integer_pattern',
  };

  // overwrite parent class
  @ViewChild('ownerForm')
  form: NgForm;

  constructor() {
    super();
  }
}
