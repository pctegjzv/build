import {
  AfterViewInit,
  Component,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Self,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor, NgControl, NgForm } from '@angular/forms';
import { cloneDeep, isArray, remove } from 'lodash';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';

import { ConfigMapOption } from 'app2/shared/services/features/configmap.service';
import { TranslateService } from 'app2/translate/translate.service';

/*
* value:
* [
  {
    "name": "test_name",
    "value": "test_value1"
  },
  {
    "name": "_testName",
    "valueFrom": {
      "configMapKeyRef": {
        "name": "sy-test",
        "key": "liu"
      }
    }
  }
]
*
* */

@Component({
  selector: 'rc-container-env-fieldset',
  styleUrls: ['container-env-fieldset.component.scss'],
  templateUrl: 'container-env-fieldset.component.html',
})
export class ContainerEnvFieldsetComponent
  implements ControlValueAccessor, OnInit, OnDestroy, AfterViewInit {
  private _value: any;
  private index = 0;
  private envs: Array<any>;
  private onDestroy$ = new Subject<void>();
  @ViewChild('form')
  form: NgForm;
  @Input()
  configMapOptions: ConfigMapOption[] = [];
  configMapKeyOptions: string[] = [];
  envNamePattern = {
    pattern: /^[-._a-zA-Z][-._a-zA-Z0-9]*$/,
    tip: this.translate.get('regexp_tip_k8s_env_vars_name'),
  };

  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    private translate: TranslateService,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
    this.envs = [];
  }
  async ngOnInit() {}

  ngAfterViewInit() {
    this.form.form.valueChanges
      .pipe(
        takeUntil(this.onDestroy$),
        debounceTime(300),
      )
      .subscribe(() => {
        const value = cloneDeep(this.envs).filter(item => {
          return (
            !!item.name && (item.reference ? !!item.configMapKey : !!item.value)
          );
        });
        this._value = this.formatOutputValue(value);
        this.updateModelValue();
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  addEnvVar(useReference = false) {
    this.envs.push({
      index: this.index++,
      name: '',
      value: '',
      reference: useReference,
      configMap: '',
      configMapKey: '',
    });
  }

  deleteEnvVar(index: number) {
    remove(this.envs, (item: any) => {
      return index === item.index;
    });
  }

  trackByIndex(_index: number, item: any) {
    return item.index;
  }

  onConfigMapSelect(configMap: ConfigMapOption, item: any) {
    if (configMap && configMap.data) {
      this.configMapKeyOptions = Object.keys(configMap.data);
      if (!this.configMapKeyOptions.includes(item.configMapKey)) {
        setTimeout(() => {
          item.configMapKey = this.configMapKeyOptions[0] || '';
        });
      }
    }
  }

  private formatOutputValue(list: any) {
    const result: any[] = [];
    list.forEach((item: any) => {
      let cur;
      if (item.reference) {
        cur = {
          name: item.name,
          valueFrom: {
            configMapKeyRef: {
              name: item.configMap,
              key: item.configMapKey,
            },
          },
        };
      } else {
        cur = {
          name: item.name,
          value: item.value,
        };
      }
      result.push(cur);
    });
    return result;
  }

  private formatInputValue(list: any) {
    const result: any[] = [];
    list.forEach((item: any) => {
      const cur = {
        index: this.index++,
        name: item.name,
        value: '',
        reference: false,
        configMap: '',
        configMapKey: '',
      };
      if (item.value) {
        cur.value = item.value;
      } else {
        cur.reference = true;
        cur.configMap = item.valueFrom.configMapKeyRef.name;
        cur.configMapKey = item.valueFrom.configMapKeyRef.key;
      }
      result.push(cur);
    });
    return result;
  }

  // set ngModel value when native view changes
  private updateModelValue() {
    this.emitToModel();
  }

  private emitToModel() {
    this.onChangeCallback(this._value);
    this.onTouchedCallback();
  }

  private applyToNative() {
    this.envs = this.formatInputValue(this._value);
  }

  /** Following Methods Implements ControlValueAccessor **/
  writeValue(value: Array<any>) {
    if (!value || !isArray(value)) {
      return;
    }
    this._value = cloneDeep(value);
    this.applyToNative();
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}
