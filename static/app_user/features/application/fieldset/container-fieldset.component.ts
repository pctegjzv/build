import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Self,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';
import { DialogService, DialogSize } from 'alauda-ui';
import { ACCOUNT, WEBLABS } from 'app2/core/tokens';
import { RcAccount, Weblabs } from 'app2/core/types';
import {
  ContainerViewModel,
  RcImageSelection,
} from 'app2/features/service/service.type';
import { ConfigmapService } from 'app2/shared/services/features/configmap.service';
import { delay } from 'app2/shared/services/utility/delay';
import { ContainerResourcesFieldsetComponent } from 'app_user/features/application/fieldset/container-resources-fieldset.component';
import { ImageSelectDialogComponent } from 'app_user/features/application/image-select/image-select-dialog.component';
import { cloneDeep } from 'lodash';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';

import { K8S_RESOURCE_NAME_BASE } from '../../../../app/components/common/config/common-pattern';

@Component({
  selector: 'rc-container-fieldset',
  templateUrl: 'container-fieldset.component.html',
  styleUrls: [
    '../form/application-form.common.scss',
    './container-fieldset.component.scss',
  ],
})
export class ContainerFieldsetComponent
  implements OnInit, OnDestroy, AfterViewInit, ControlValueAccessor {
  @Input()
  containerIndex = 0;
  @Input()
  serviceUuid: string;
  @Input()
  clusterName: string;
  @Input()
  namespaceName: string;
  @Input()
  projectName: string;
  @ViewChild('containerForm')
  containerForm: NgForm;
  @ViewChild('containerResources')
  containerResources: ContainerResourcesFieldsetComponent;
  private onDestroy$: Subject<void>;

  viewModel: ContainerViewModel;

  resourceNameReg = K8S_RESOURCE_NAME_BASE;
  showAdvanceOptions = false;
  configmapOptions: any[] = [];

  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() protected parentForm: NgForm,
    @Optional() protected parentFormGroup: FormGroupDirective,
    @Optional() protected cdr: ChangeDetectorRef,
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(ACCOUNT) public account: RcAccount,
    private configmapService: ConfigmapService,
    private dialogService: DialogService,
  ) {
    this.onDestroy$ = new Subject<void>();
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }

    this.viewModel = {
      name: '',
      image: '',
      imageTag: '',
      resources: null, // 容器大小，包括最大值和参考值
      volumes: [],
      healthCheck: {},
      entrypoint: '',
      cmd: [],
      envvars: [
        // {
        //   name: 'sdfds',
        //   valueFrom: {
        //     configMapKeyRef: {
        //       name: 'test',
        //       key: 're',
        //     },
        //   },
        // },
      ],
      configmaps: [],
    };
  }

  async ngOnInit() {
    this.ngControl.control.setValidators((control: AbstractControl) => {
      const value = control.value;
      if (!value) {
        return null;
      }
      if (!value.name) {
        return {
          container_filds_missing: {
            name: 'name',
          },
        };
      }
      return null;
    });
    this.getConfigmapOptions();
  }

  async ngAfterViewInit() {
    await delay(0);
    this.containerForm.valueChanges
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(() => {
        this.emitToModel();
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  trackByFn(index: number) {
    return index;
  }

  async openImageSelectDialog() {
    try {
      const modalRef = await this.dialogService.open(
        ImageSelectDialogComponent,
        {
          size: DialogSize.Big,
          data: {
            projectName: this.projectName,
            namespaceName: this.namespaceName,
          },
        },
      );
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe((res: RcImageSelection) => {
          modalRef.close();
          if (res) {
            this.viewModel.image = res.full_image_name;
            this.viewModel.imageTag = res.tag;
            this.getImageTagDetail(res);
          }
        });
    } catch (error) {
      //
    }
  }

  triggerSubmit() {
    this.containerForm.onSubmit(null);
  }

  isValid() {
    this.containerResources.triggerSubmit();
    return this.containerForm.valid && this.containerResources.form.valid;
  }

  private async getConfigmapOptions() {
    this.configmapOptions = await this.configmapService.getConfigMapOptions({
      clusterId: this.clusterName,
      namespace: this.namespaceName,
    });
  }

  private getImageTagDetail(_params: RcImageSelection) {}

  // emit viewModel to ngModel
  private emitToModel() {
    this.onTouchedCallback();
    this.onChangeCallback(this.viewModel);
  }

  /** Following Methods Implements ControlValueAccessor **/

  writeValue(value: any) {
    if (!value) {
      return;
    }
    if (this.viewModel) {
      Object.assign(this.viewModel, value);
    } else {
      this.viewModel = cloneDeep(value);
    }
    if (this.viewModel.image && this.viewModel.imageTag) {
      // this.getImageTagDetail();
    }
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}
