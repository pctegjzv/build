import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  Optional,
  Self,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor, NgControl, NgForm } from '@angular/forms';
import { cloneDeep, get } from 'lodash';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';

import { ContainerResourcesViewModel } from 'app2/features/service/service.type';

import { INT_PATTERN } from '../../../../app/components/common/config/common-pattern';
/*
* value:
* {
*   "requests": {
*     "memory": "3M",
*     "cpu": "3000m"                         // cpu 单位为m时 值为string
*   },
*   "limits": {
*     "memory": "",                          // 无值时 值为 "" 空串
*     "cpu": 3                               // cpu 单位为c时 省略单位c 1c=1000m 值为number
*   }
* }
*
*
* control errors
* {
* 	"container_resource_size_invalid": {
* 		"cpu": {                             // cpu 和 memory 如果没有错误 则不包含此字段
* 			"request": "3000000000m",
* 			"limit": "3"                       // cpu 单位为c时 省略单位c 1c=1000m
* 		},
* 		"memory": {
* 			"request": "300000M",
* 			"limit": "3G"
* 		}
* 	}
* }
*
* */

@Component({
  selector: 'rc-container-resources-fieldset',
  templateUrl: 'container-resources-fieldset.component.html',
  styleUrls: ['container-resources-fieldset.component.scss'],
})
export class ContainerResourcesFieldsetComponent
  implements ControlValueAccessor, OnInit, OnDestroy, AfterViewInit {
  @ViewChild('form')
  form: NgForm;
  private onDestroy$: Subject<void>;
  numberReg = INT_PATTERN;
  memUnits = ['M', 'G'];
  cpuUnits = ['m', 'c'];
  defaultMemUnit = 'G';
  defaultCpuUnit = 'c';
  defaultMemVal: number;
  defaultCpuVal: number;
  viewModel: ContainerResourcesViewModel;

  resource = {
    requests: {
      memory: {
        value: this.defaultMemVal,
        unit: this.defaultMemUnit,
      },
      cpu: {
        value: this.defaultCpuVal,
        unit: this.defaultCpuUnit,
      },
    },
  };

  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
    this.onDestroy$ = new Subject<void>();
  }
  async ngOnInit() {}

  ngAfterViewInit() {
    this.form.valueChanges
      .pipe(
        takeUntil(this.onDestroy$),
        debounceTime(300),
      )
      .subscribe(() => {
        const value = cloneDeep(this.resource);
        this.viewModel = this.stringifyResourceSize(value);
        this.emitToModel();
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  triggerSubmit() {
    this.form.onSubmit(null);
  }

  private parseValueString(str: string, type: string) {
    if (str === '' || str === null || str === undefined) {
      let defaultUnit = '';
      switch (type) {
        case 'memory':
          defaultUnit = this.defaultMemUnit;
          break;
        case 'cpu':
          defaultUnit = this.defaultCpuUnit;
          break;
      }
      return {
        value: null,
        unit: defaultUnit,
      };
    } else {
      let result = str;
      if (type === 'cpu') {
        result = this.cpuUnitPadding(str);
      }
      return {
        value: parseInt(result, 10),
        unit: result.slice(-1),
      };
    }
  }

  private cpuUnitPadding(str: string) {
    const unitIndex = this.cpuUnits.findIndex((item: any) => {
      return str.slice(-1) === item;
    });
    return unitIndex === -1 ? str + 'c' : str;
  }

  private parseResourceSize(cs: any) {
    return {
      requests: {
        memory: this.parseValueString(get(cs, 'requests.memory', ''), 'memory'),
        cpu: this.parseValueString(get(cs, 'requests.cpu', '') + '', 'cpu'),
      },
    };
  }

  private stringifyValue(obj: any) {
    return obj.value
      ? obj.unit === 'c'
        ? parseInt(obj.value, 10)
        : obj.value + obj.unit
      : '';
  }

  private stringifyResourceSize(cs: any) {
    return {
      requests: {
        memory: this.stringifyValue(cs.requests.memory),
        cpu: this.stringifyValue(cs.requests.cpu),
      },
      limits: {
        // limits some to request in cmb env.
        memory: this.stringifyValue(cs.requests.memory),
        cpu: this.stringifyValue(cs.requests.cpu),
      },
    };
  }

  private emitToModel() {
    this.onTouchedCallback();
    this.onChangeCallback(this.viewModel);
  }

  // apply viewModel. to native view
  private applyToNative() {
    if (!this.viewModel) {
      return;
    }
    this.resource = this.parseResourceSize(this.viewModel);
  }

  /** Following Methods Implements ControlValueAccessor **/

  writeValue(value: any) {
    this.viewModel = value;
    this.applyToNative();
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}
