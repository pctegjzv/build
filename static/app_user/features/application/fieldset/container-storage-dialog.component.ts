import {
  Component,
  EventEmitter,
  Inject,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { StorageService } from 'app2/shared/services/features/storage.service';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { get } from 'lodash';

import { SUB_PATH } from '../../../../app/components/common/config/common-pattern';

@Component({
  templateUrl: 'container-storage-dialog.component.html',
  styleUrls: ['container-storage-dialog.component.scss'],
})
export class ContainerStorageDialogComponent implements OnInit, OnDestroy {
  @ViewChild('form')
  form: NgForm;
  @Output()
  close = new EventEmitter<any>();
  initialized: boolean;
  pvcOptions: any[];
  viewModel: {
    type: string;
    containerPath: string;
    subPath?: string;
    pvcName: string;
  };
  subPathRegex = SUB_PATH;

  constructor(
    @Inject(MODAL_DATA)
    private modalData: {
      data: any;
      serviceUuid: string;
      clusterName: string;
      namespaceName: string;
    },
    private storageService: StorageService,
  ) {
    if (this.modalData.data) {
      this.viewModel = this.modalData.data;
    } else {
      this.viewModel = {
        type: 'pvc',
        containerPath: '',
        pvcName: '',
        subPath: '',
      };
    }
  }

  async ngOnInit() {
    await this.getPvcOptions();
    this.initialized = true;
  }

  ngOnDestroy() {}

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.close.emit(this.viewModel);
  }

  cancel() {
    this.close.emit(null);
  }

  private async getPvcOptions() {
    this.pvcOptions = await this.storageService
      .getPvcs({
        cluster_id: this.modalData.clusterName,
        namespace: this.modalData.namespaceName,
        page_size: 100,
      })
      .then((res: any) => {
        return res.results
          .filter((item: any) => {
            // FIXME: fix me when api doc is ready
            // const status: string = item.resource.status.toLowerCase();
            // const accessModes: any = item.resource.accessModes;
            // if (
            //   accessModes.includes('ReadWriteOnce') &&
            //   accessModes.length === 1 &&
            //   status === 'bound'
            // ) {
            //   const referencedByUuid = get(item, 'referenced_by[0].uuid', '');
            //   if (
            //     this.modalData.serviceUuid &&
            //     this.modalData.serviceUuid === referencedByUuid
            //   ) {
            //     return true;
            //   }
            //   return false;
            // }
            const status = get(item, 'kubernetes.status.phase');
            if (status === 'lost') {
              return false;
            }
            return true;
          })
          .map((item: any) => {
            return {
              name: item.kubernetes.metadata.name,
              uuid: item.kubernetes.metadata.uuid,
            };
          });
      })
      .catch(() => []);
  }
}
