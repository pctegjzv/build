import {
  AfterViewInit,
  Component,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Self,
} from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import { ContainerStorageDialogComponent } from 'app_user/features/application/fieldset/container-storage-dialog.component';
import { cloneDeep } from 'lodash';
import { first } from 'rxjs/operators';
/*
* value:
* [
*   {
*     type: 'host-path',     // 'host-path', 'volume', 'pvc'
*
*     hostPath: '',          // type为host-path时有
*
*     volumeName: '',        // type为volume时有以下3字段
*     driverName: '',
*     driverVolumeId: '',    // 此值不存在时 不保留此字段
*
*     pvcName: '',           // type为pvc时有
*     pvcSubPath: '',
*
*     configMapName?: string;
*     configMapUuid?: string;
*     configMapKeyRef?: boolean;
      configMapKeyMap?: {
        index: number;
        name: string;
        path: string;
      }[];
*
*     containerPath: '',     // 3种类型都有
*   }
* ]
*
* */

@Component({
  selector: 'rc-container-storage-fieldset',
  templateUrl: 'container-storage-fieldset.component.html',
  styleUrls: ['container-storage-fieldset.component.scss'],
})
export class ContainerStorageFieldsetComponent
  implements ControlValueAccessor, OnInit, OnDestroy, AfterViewInit {
  @Input()
  serviceUuid: string;
  @Input()
  clusterName: string;
  @Input()
  namespaceName: string;
  storageList: {
    type: string;
    containerPath: string;
    subPath?: string;
    pvcName: string;
  }[] = [];
  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    private translateService: TranslateService,
    private modalService: ModalService,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }
  async ngOnInit() {}

  ngAfterViewInit() {}

  ngOnDestroy() {}

  trackByIndex(_index: number, item: any) {
    return item.index;
  }

  addItem() {
    const modalRef = this.modalService.open(ContainerStorageDialogComponent, {
      width: 600,
      title: this.translateService.get('service_add_volume'),
      data: {
        serviceUuid: this.serviceUuid,
        clusterName: this.clusterName,
        namespaceName: this.namespaceName,
      },
    });
    modalRef.componentInstance.close.pipe(first()).subscribe((data: any) => {
      if (data) {
        this.storageList.push({
          ...data,
        });
      }
      modalRef.close();
      this.emitToModel();
    });
  }

  updateItem(index: number, item: any) {
    const modalRef = this.modalService.open(ContainerStorageDialogComponent, {
      width: 600,
      title: this.translateService.get('update'),
      data: {
        data: cloneDeep(item),
        serviceUuid: this.serviceUuid,
        clusterName: this.clusterName,
        namespaceName: this.namespaceName,
      },
    });
    modalRef.componentInstance.close.pipe(first()).subscribe((res: any) => {
      if (res) {
        this.storageList[index] = res;
      }
      modalRef.close();
      this.emitToModel();
    });
  }

  removeItem(index: number) {
    this.storageList.splice(index, 1);
    this.emitToModel();
  }

  private emitToModel() {
    this.onTouchedCallback();
    this.onChangeCallback(cloneDeep(this.storageList));
  }

  /** Following Methods Implements ControlValueAccessor **/
  writeValue(value: any) {
    if (value && value.length) {
      this.storageList = cloneDeep(value);
    }
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}
