import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ServiceViewModel } from 'app2/features/service/service.type';
import { MODAL_DATA } from 'app2/shared/services/modal/modal.types';
import { cloneDeep } from 'lodash';

import {
  INT_PATTERN,
  K8S_RESOURCE_NAME_BASE,
} from '../../../../app/components/common/config/common-pattern';

@Component({
  templateUrl: 'route-dialog.component.html',
  styleUrls: ['route-dialog.component.scss'],
})
export class RouteDialogComponent implements OnInit {
  @Output()
  close = new EventEmitter<any>();
  @ViewChild('form')
  form: NgForm;
  portNumberReg = INT_PATTERN;
  resourceNameReg = K8S_RESOURCE_NAME_BASE;
  data: ServiceViewModel;
  port: number;
  serviceNameBlackList: string[] = [];

  constructor(
    @Inject(MODAL_DATA)
    private modalData: {
      data: any;
      serviceList: any[];
    },
  ) {}

  async ngOnInit() {
    this.serviceNameBlackList = this.modalData.serviceList.map(item => {
      return item.name;
    });
    if (!this.modalData.data) {
      this.data = {
        name: '',
        type: 'ClusterIP',
        ports: [],
      };
    } else {
      this.serviceNameBlackList = this.serviceNameBlackList.filter(item => {
        return item !== this.modalData.data.name;
      });
      this.data = cloneDeep(this.modalData.data);
      if (this.data.ports && this.data.ports.length) {
        this.port = this.data.ports[0].port;
      }
    }
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    const data = cloneDeep(this.data);
    data.ports = [
      {
        port: this.port,
        containerPort: this.port,
      },
    ];
    this.close.next(data);
  }

  cancel() {
    this.close.next(null);
  }
}
