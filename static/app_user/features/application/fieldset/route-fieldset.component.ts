import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  Optional,
  Self,
} from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';
import { ServiceViewModel } from 'app2/features/service/service.type';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import { RouteDialogComponent } from 'app_user/features/application/fieldset/route-dialog.component';
import { cloneDeep } from 'lodash';
import { first } from 'rxjs/operators';

/**
 * value: [
 * {
 *  name: 'yyy',
 *  type: 'Headless'
 * }]
 */
@Component({
  selector: 'rc-route-fieldset',
  templateUrl: 'route-fieldset.component.html',
  styleUrls: ['route-fieldset.component.scss'],
})
export class RouteFieldsetComponent
  implements OnInit, OnDestroy, AfterViewInit, ControlValueAccessor {
  services: any[] = [];

  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    private modalService: ModalService,
    private translate: TranslateService,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }

  async ngOnInit() {}

  ngAfterViewInit() {}

  ngOnDestroy() {}

  async addService() {
    const modalRef = await this.modalService.open(RouteDialogComponent, {
      width: 700,
      title: this.translate.get('add_route'),
      data: {
        serviceList: this.services,
      },
    });
    modalRef.componentInstance.close.pipe(first()).subscribe(res => {
      if (res) {
        this.services.push(res);
        this.emitToModel();
      }
      modalRef.close();
    });
  }

  removeService(index: number) {
    this.services.splice(index, 1);
    this.emitToModel();
  }

  async updateService(index: number, service: any) {
    const modalRef = await this.modalService.open(RouteDialogComponent, {
      width: 700,
      title: this.translate.get('update'),
      data: {
        data: service,
        serviceList: this.services,
      },
    });
    modalRef.componentInstance.close.pipe(first()).subscribe(res => {
      if (res) {
        this.services[index] = res;
        this.emitToModel();
      }
      modalRef.close();
    });
  }

  getServicePort(service: ServiceViewModel) {
    return service.ports && service.ports.length ? service.ports[0].port : '-';
  }

  private emitToModel() {
    this.onTouchedCallback();
    this.onChangeCallback(cloneDeep(this.services));
  }

  /** Following Methods Implements ControlValueAccessor **/

  writeValue(value: any) {
    if (value && value.length) {
      this.services = cloneDeep(value);
    }
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}
