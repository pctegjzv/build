import {
  Component,
  Input,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from 'alauda-ui';

import {
  ComponentViewModel,
  RcImageSelection,
  ServiceViewModel,
} from 'app2/features/service/service.type';
import {
  Application,
  MirrorApp,
} from 'app2/shared/services/features/app.service';
import { AppUtilitiesService } from 'app2/shared/services/features/app.utilities.service';
import {
  Cluster,
  RegionService,
} from 'app2/shared/services/features/region.service';
import {
  ServiceService,
  // Service,
} from 'app2/shared/services/features/service.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import { AppOwnersFieldsetComponent } from 'app_user/features/application/fieldset/app-owners-fieldset.component';
import { ContainerFieldsetComponent } from 'app_user/features/application/fieldset/container-fieldset.component';
import { ImageSelectDialogComponent } from 'app_user/features/application/image-select/image-select-dialog.component';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import * as jsyaml from 'js-yaml';
import { remove } from 'lodash';
import { every } from 'lodash';
import { first } from 'rxjs/operators';

import { updateActions, viewActions } from 'app2/utils/code-editor-config';

import {
  INT_PATTERN,
  K8S_RESOURCE_NAME_BASE,
} from '../../../../app/components/common/config/common-pattern';

@Component({
  selector: 'rc-application-form',
  templateUrl: 'application-form.component.html',
  styleUrls: [
    'application-form.common.scss',
    'application-form.component.scss',
  ],
})
export class ApplicationFormComponent implements OnInit {
  @Input()
  data: Application;
  @Input()
  projectName: string;
  @Input()
  clusterName: string;
  @Input()
  namespaceName: string;
  numberReg = INT_PATTERN;
  resourceNameReg = K8S_RESOURCE_NAME_BASE;
  serviceUuid = ''; // TODO: service uuid used in volume adding dialog to filter out pvc already in use.

  @ViewChild('form')
  appForm: NgForm;
  @ViewChild('appOwnerField')
  appOwnerField: AppOwnersFieldsetComponent;
  @ViewChildren(ContainerFieldsetComponent)
  private containerFields: QueryList<ContainerFieldsetComponent>;
  private containerIndex = 0; // 支持toc-link定位的container索引

  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: true,
  };
  mirrorClusters: {
    name: string;
    display_name: string;
  }[] = [];
  currentCluster: {
    name: string;
    display_name: string;
  }[];
  mirrorApps: MirrorApp[];

  initialized = false;
  submitting = false;
  showMore: boolean;
  yamlPreview: boolean;
  yamlReadonly = true;

  viewModel: ComponentViewModel;
  yaml: string;
  payload: any;
  clusters: string[];
  editorActionsConfig = viewActions;
  originalYaml = '';

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private modalService: ModalService,
    private dialogService: DialogService,
    private translateService: TranslateService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private serviceService: ServiceService,
    private regionService: RegionService,
    private appUtil: AppUtilitiesService,
    private workspaceComponent: WorkspaceComponent,
  ) {
    if (this.data) {
      this.clusterName = this.data.cluster.name;
      this.namespaceName = this.data.namespace.name;
    }
  }

  async ngOnInit() {
    await this.initViewModel();
    this.initialized = true;
  }

  private async initViewModel() {
    this.payload = {
      resource: {
        name: '',
        create_method: 'UI',
      },
      namespace: {
        name: this.namespaceName,
      },
      kubernetes: [],
    };
    if (!this.data) {
      const queryParamMap = this.activatedRoute.snapshot.queryParamMap;
      this.viewModel = {
        namespace: this.namespaceName,
        name: queryParamMap.get('repository_name'),
        owners: [
          {
            name: '',
            phone: '',
          },
        ],
        kind: 'Deployment', // 部署模式
        replicas: 1, // 实例数
        labels: [],
        minReplicas: null,
        maxReplicas: null,
        nodeTags: [],
        nodeAffinity: [],
        affinity: null, // 亲和性，包括container亲和反亲和，key为 `service.${label_base_domain}/name`
        maxSurge: '',
        maxUnavailable: '',
        kubeServices: [], // kubernetes services
        containers: [],
        networkMode: {
          hostNetwork: false,
          subnet: null,
        },
      };
      this.getMirrorClusters();
      this.addContainer(true);
    } else {
      const kubernetes: any = this.data.kubernetes; // Route 属于app下的资源，所以直接使用appData.kubernetes
      this.viewModel = Object.assign(
        { name: this.data.resource.name, namespace: this.data.namespace.name },
        this.serviceService.getServiceViewModel(kubernetes),
      );
      const service = kubernetes.find((item: { kind: string }) => {
        return item.kind === 'Deployment';
      });
      if (service) {
        const appOwnerMetada = service.spec.template.metadata.annotations;
        if (appOwnerMetada) {
          // multi app owner
          if (appOwnerMetada['owner.cmb.io/info']) {
            this.viewModel.owners = JSON.parse(
              appOwnerMetada['owner.cmb.io/info'],
            );
          } else {
            // old ymal before jul
            this.viewModel.owners = [
              {
                name: appOwnerMetada['owner.cmb.io/name'],
                phone: appOwnerMetada['owner.cmb.io/phone'],
              },
            ];
          }
        } else {
          // no appOwnerMetada maby create by yaml
          this.viewModel.owners = [
            {
              name: '',
              phone: '',
            },
          ];
        }
      }
      this.viewModel.containers.forEach(container => {
        container.index = this.containerIndex++;
      });
      this.viewModel.kubeServices.forEach((item: ServiceViewModel) => {
        if (!item.route) {
          item.route = {
            host: '',
            path: '',
          };
        }
      });
      this.getMirrorApps();
    }
  }

  async addContainer(isFirst = false) {
    if (isFirst) {
      const queryParamMap = this.activatedRoute.snapshot.queryParamMap;
      this.viewModel.containers.push({
        config: {
          name: queryParamMap.get('repository_name'),
          image: queryParamMap.get('full_image_name'),
          imageTag: queryParamMap.get('tag'),
        },
        index: this.containerIndex++,
      });
    } else {
      const modalRef = await this.dialogService.open(
        ImageSelectDialogComponent,
        {
          size: DialogSize.Big,
          data: {
            projectName: this.projectName,
            namespaceName: this.namespaceName,
          },
        },
      );
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe((res: RcImageSelection) => {
          modalRef.close();
          if (res) {
            this.viewModel.containers.push({
              config: {
                name: res.repository_name,
                image: res.full_image_name,
                imageTag: res.tag,
              },
              index: this.containerIndex++,
            });
          }
        });
    }
  }

  removeContainer(index: number) {
    remove(this.viewModel.containers, (container: any) => {
      return index === container.index;
    });
  }

  trackByContainerIndex(_index: number, container: any) {
    return container.index;
  }

  preview() {
    this.triggerSubmit();
    if (!this.isValid()) {
      return;
    }
    try {
      this.generateYaml();
    } catch (error) {
      this.auiNotificationService.error({
        title: this.translateService.get('yaml_error'),
        content: error,
      });
      return;
    }
    this.yamlPreview = true;
  }

  cancelPreview() {
    this.yamlPreview = false;
    this.yaml = '';
  }

  async confirm() {
    this.triggerSubmit();
    if (!this.isValid()) {
      return;
    }
    if (
      this.mirrorClusters.length > 1 &&
      this.clusters &&
      !this.clusters.includes(this.clusterName)
    ) {
      this.auiMessageService.error({
        content: this.translateService.get('missing_current_namespace_cluster'),
      });
      return;
    }
    try {
      this.generateYaml();
    } catch (error) {
      this.auiNotificationService.error({
        title: this.translateService.get('yaml_error'),
        content: error,
      });
      return;
    }
    this.payload.resource = {
      name: this.viewModel.name, // 应用名称和服务名称相同
      create_method: this.yamlReadonly ? 'UI' : 'YAML',
    };
    this.payload.kubernetes = jsyaml.safeLoadAll(this.yaml);
    if (this.data) {
      try {
        await this.modalService.confirm({
          title: this.translateService.get('update'),
          content: this.translateService.get('app_service_update_app_confirm', {
            app_name: this.data.resource.name,
          }),
        });
        this.submitting = true;
        await this.appUtil.updateApp({
          payload: this.payload,
          uuid: this.data.resource.uuid,
          mirrorApps: this.mirrorApps.filter((app: MirrorApp) => {
            return !!this.clusters.find((clusterName: string) => {
              return clusterName === app.cluster_name;
            });
          }),
          routeParams: {
            project: this.projectName,
            cluster: this.clusterName,
            namespace: this.namespaceName,
          },
        });
        this.submitting = false;
      } catch (rejection) {}
    } else {
      try {
        await this.modalService.confirm({
          title: this.translateService.get('create'),
          content: this.translateService.get('app_service_create_app_confirm', {
            app_name: this.viewModel.name,
          }),
        });
        this.submitting = true;
        await this.appUtil.createApp({
          payload: this.payload,
          clusterName: this.clusterName,
          clusters: this.clusters,
          routeParams: {
            project: this.projectName,
            cluster: this.clusterName,
            namespace: this.namespaceName,
          },
        });
        this.submitting = false;
      } catch (rejection) {}
    }
  }

  cancel() {
    return this.router.navigate(['app'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  async toggleYamlReadonly() {
    try {
      await this.modalService.confirm({
        title: this.translateService.get('confirm'),
        content: this.translateService.get(
          this.translateService.get('service_toggle_yaml_preview_confirm'),
        ),
      });
      this.yamlReadonly = false;
      this.codeEditorOptions = {
        ...this.codeEditorOptions,
        readOnly: false,
      };
      this.editorActionsConfig = updateActions;
    } catch (e) {
      return false;
    }
  }

  private generateYaml() {
    if (!this.serviceService.checkServicePayload(this.viewModel)) {
      return;
    }
    this.yaml = this.serviceService.generateServiceYamlPayload(this.viewModel);
    this.originalYaml = this.yaml;
  }

  private triggerSubmit() {
    this.containerFields.forEach((container: ContainerFieldsetComponent) => {
      container.triggerSubmit();
    });
    this.appOwnerField.triggerSubmit();
    this.appForm.onSubmit(null);
  }

  private isValid() {
    return (
      this.appForm.valid &&
      this.appOwnerField.isValid() &&
      every(
        this.containerFields.toArray(),
        (container: ContainerFieldsetComponent) => {
          return container.isValid();
        },
      )
    );
  }

  private async getMirrorClusters() {
    const cluster: Cluster = await this.regionService.getCluster(
      this.clusterName,
    );
    if (cluster.mirror) {
      this.mirrorClusters = cluster.mirror.regions || [];
      this.currentCluster = this.mirrorClusters.filter(item => {
        return item.name === this.clusterName;
      });
      this.clusters = this.mirrorClusters.map((item: { name: string }) => {
        return item.name;
      });
    } else {
      this.clusters = [this.clusterName];
    }
  }

  private async getMirrorApps() {
    this.mirrorApps = await this.appUtil.getMirrorApps(this.data);
    if (!this.mirrorApps.length) {
      return;
    }
    this.mirrorClusters = this.mirrorApps.map((app: MirrorApp) => {
      return {
        name: app.cluster_name,
        display_name: app.cluster_display_name,
      };
    });
    this.currentCluster = this.mirrorClusters.filter(item => {
      return item.name === this.clusterName;
    });
    this.clusters = this.mirrorClusters.map((cluster: { name: string }) => {
      return cluster.name;
    });
  }
}
