import {
  Component,
  Input,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import {
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from 'alauda-ui';
import {
  AppService,
  Application,
  MirrorApp,
} from 'app2/shared/services/features/app.service';
import { AppUtilitiesService } from 'app2/shared/services/features/app.utilities.service';
import { K8sResourceService } from 'app2/shared/services/features/k8s-resource.service';
import {
  Cluster,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import {
  createActions,
  updateActions,
  viewActions,
} from 'app2/utils/code-editor-config';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import * as jsyaml from 'js-yaml';

import { yamlDemo } from './application-yaml-demo';
@Component({
  selector: 'rc-application-yaml-form',
  templateUrl: 'application-yaml-form.component.html',
})
export class ApplicationYamlFormComponent implements OnInit {
  @Input()
  data: Application;
  @Input()
  projectName: string;
  @Input()
  clusterName: string;
  @Input()
  namespaceName: string;

  appPayload: any;
  appYaml = '';
  yamlDemo = yamlDemo;
  submitting = false;
  mirrorClusters: {
    name: string;
    display_name: string;
  }[] = [];
  mirrorApps: MirrorApp[];

  clusters: string[];
  actionsConfigView = viewActions;
  actionsConfigCreate = createActions;
  actionsConfigUpdate = updateActions;
  originalYaml = '';
  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: false,
  };
  currentCluster: {
    name: string;
    display_name: string;
  }[];

  @ViewChild('appForm')
  form: NgForm;

  constructor(
    private modalService: ModalService,
    private translateService: TranslateService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private dialogService: DialogService,
    private regionService: RegionService,
    private router: Router,
    private appUtil: AppUtilitiesService,
    private appService: AppService,
    private workspaceComponent: WorkspaceComponent,
    private k8sResourceService: K8sResourceService,
  ) {
    if (this.data) {
      this.clusterName = this.data.cluster.name;
      this.namespaceName = this.data.namespace.name;
    }
  }

  async ngOnInit() {
    this.appPayload = {
      resource: {
        name: '',
        create_method: 'YAML',
      },
      namespace: {
        name: this.namespaceName,
      },
      kubernetes: [],
    };
    if (this.data) {
      this.getAppYaml();
      this.getMirrorApps();
    } else {
      this.getMirrorClusters();
    }
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    if (
      this.mirrorClusters.length > 1 &&
      this.clusters &&
      !this.clusters.includes(this.clusterName)
    ) {
      this.auiMessageService.error({
        content: this.translateService.get('missing_current_namespace_cluster'),
      });
      return;
    }
    try {
      this.appPayload.kubernetes = jsyaml.safeLoadAll(this.appYaml);
      // FIXME: kubernetes data type
      const deployment = this.appPayload.kubernetes.find((data: any) => {
        return data.kind === 'Deployment';
      });
      this.appPayload.resource.name = deployment
        ? deployment.metadata.name
        : '';
    } catch (error) {
      this.auiNotificationService.error({
        title: this.translateService.get('yaml_error'),
        content: error,
      });
      return;
    }
    if (this.data) {
      try {
        await this.modalService.confirm({
          title: this.translateService.get('update'),
          content: this.translateService.get('app_service_update_app_confirm', {
            app_name: this.appPayload.resource.name,
          }),
        });
        this.submitting = true;
        await this.appUtil.updateApp({
          payload: {
            kubernetes: this.appPayload.kubernetes,
          },
          uuid: this.data.resource.uuid,
          mirrorApps: this.mirrorApps.filter((app: MirrorApp) => {
            return !!this.clusters.find((clusterName: string) => {
              return clusterName === app.cluster_name;
            });
          }),
          routeParams: {
            project: this.projectName,
            cluster: this.clusterName,
            namespace: this.namespaceName,
          },
        });
        this.submitting = false;
      } catch (rejection) {}
    } else {
      try {
        await this.modalService.confirm({
          title: this.translateService.get('create'),
          content: this.translateService.get('app_service_create_app_confirm', {
            app_name: this.appPayload.resource.name,
          }),
        });
        this.submitting = true;
        const hasDeployment = this.appPayload.kubernetes.some((data: any) => {
          return ['Deployment', 'StatefulSet', 'DaemonSet'].includes(data.kind);
        });
        if (hasDeployment) {
          await this.appUtil.createApp({
            payload: this.appPayload,
            clusterName: this.clusterName,
            clusters: this.clusters,
            routeParams: {
              project: this.projectName,
              cluster: this.clusterName,
              namespace: this.namespaceName,
            },
          });
        } else {
          await this.k8sResourceService.createK8sReources(
            this.clusterName,
            this.clusters,
            this.appPayload.kubernetes,
          );
          this.router.navigate(['resource_management', 'list'], {
            relativeTo: this.workspaceComponent.baseActivatedRoute,
          });
        }
        this.submitting = false;
      } catch (rejection) {}
    }
  }

  cancel() {
    return this.router.navigate(['app'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  useDemoTemplate() {
    this.appYaml = yamlDemo;
  }

  showDemoTemplate(template: TemplateRef<any>) {
    this.dialogService.open(template, {
      size: DialogSize.Big,
    });
  }

  private async getAppYaml() {
    this.appYaml = await this.appService.getK8sAppYaml(this.data.resource.uuid);
    this.originalYaml = this.appYaml;
  }

  private async getMirrorClusters() {
    const cluster: Cluster = await this.regionService.getCluster(
      this.clusterName,
    );
    if (cluster.mirror) {
      this.mirrorClusters = cluster.mirror.regions || [];
      this.clusters = this.mirrorClusters.map((item: { name: string }) => {
        return item.name;
      });
      this.currentCluster = this.mirrorClusters.filter(item => {
        return item.name === this.clusterName;
      });
    } else {
      this.clusters = [this.clusterName];
    }
  }

  private async getMirrorApps() {
    this.mirrorApps = await this.appUtil.getMirrorApps(this.data);
    if (!this.mirrorApps.length) {
      return;
    }
    this.mirrorClusters = this.mirrorApps.map((app: MirrorApp) => {
      return {
        name: app.cluster_name,
        display_name: app.cluster_display_name,
      };
    });
    this.currentCluster = this.mirrorClusters.filter(item => {
      return item.name === this.clusterName;
    });
    this.clusters = this.mirrorClusters.map((cluster: { name: string }) => {
      return cluster.name;
    });
  }
}
