import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  ViewChild,
} from '@angular/core';
import { Inject } from '@angular/core';
import { DIALOG_DATA } from 'alauda-ui';
import { sortBy } from 'lodash';
import {
  BehaviorSubject,
  Observable,
  Subject,
  combineLatest,
  from,
} from 'rxjs';
import { debounceTime, map, share, startWith, switchMap } from 'rxjs/operators';

import { RcImageSelection } from 'app2/features/service/service.type';
import { TooltipDirective } from 'app2/shared/directives/tooltip/tooltip.directive';
import {
  ImageRegistry,
  ImageRegistryService,
} from 'app2/shared/services/features/image-registry.service';
import { ImageRepositoryService } from 'app2/shared/services/features/image-repository.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: 'image-select-dialog.component.html',
  styleUrls: ['image-select-dialog.component.scss'],
})
export class ImageSelectDialogComponent implements OnInit, OnDestroy {
  registries: any[] = [];
  private projectName: string;
  private namespaceName: string;
  private onDestroy$ = new Subject<void>();
  @Output()
  close = new EventEmitter<RcImageSelection>();
  mode = 'select'; // select, input
  imageTag = '';
  imageTagOptions: string[] = [];
  imageTagsLoading: boolean;

  initialized: boolean;
  visibleRegistries: any[] = [];
  moreRegistries: any[] = [];
  repositories: any[] = [];

  filterFnSubject$: BehaviorSubject<any>;
  repositoriesWrap$: BehaviorSubject<any>;
  repositories$: Observable<any>;
  filteredRepositories$: Observable<any>;

  selectedRegistry: any;
  selectedRepository: any;
  imageAddressInput: string;
  loading = false;

  additionalRegistry: any;

  filterKey: string;
  exampleRepositoryAddress: string;

  @ViewChild('registryTooltip', { read: TooltipDirective })
  registryListTooltipInstance: TooltipDirective;

  @ViewChild('projectTooltip', { read: TooltipDirective })
  projectListTooltipInstance: TooltipDirective;

  constructor(
    @Optional() protected cdr: ChangeDetectorRef,
    private registryService: ImageRegistryService,
    private repositoryService: ImageRepositoryService,
    private translateService: TranslateService,
    @Inject(DIALOG_DATA)
    private modalData: {
      projectName: string;
      namespaceName: string;
    },
  ) {
    this.filterFnSubject$ = new BehaviorSubject((list: any) => list);
    this.repositoriesWrap$ = new BehaviorSubject([]);
    this.repositories$ = this.repositoriesWrap$.pipe(
      switchMap(repos => repos),
      startWith([]),
    );
    this.filteredRepositories$ = combineLatest(
      this.repositories$,
      this.filterFnSubject$.pipe(debounceTime(200)),
    ).pipe(
      map(([repositories, filterFn]) => repositories.filter(filterFn)),
      share(),
    );
    this.projectName = this.modalData.projectName;
    this.namespaceName = this.modalData.namespaceName;
  }

  async ngOnInit() {
    this.exampleRepositoryAddress = 'index.docker.io/library/ubuntu:latest';
    await this.getRegistries();
    this.initialized = true;
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  confirm() {
    switch (this.mode) {
      case 'input':
        try {
          const parts = this.imageAddressInput.split('/');
          let full_image_name = '';
          const length = parts.length;
          const image = parts[length - 1];
          const [name, tag] = image.split(':');
          if (tag) {
            parts[length - 1] = name;
            full_image_name = parts.join('/');
          } else {
            full_image_name = this.imageAddressInput;
          }
          this.close.next({
            registry_endpoint: parts[0],
            is_public_registry: true,
            registry_name: '',
            project_name: '',
            repository_name: name,
            tag: tag || 'latest',
            full_image_name,
          });
          break;
        } catch (err) {
          //
        }
        break;
      case 'select':
        let full_image_name: string;
        if (this.selectedRegistry.is_third) {
          full_image_name = this.selectedRegistry.is_public
            ? `${this.selectedRegistry.endpoint}/public/${
                this.selectedRepository.name
              }`
            : `${this.selectedRegistry.endpoint}/${this.namespaceName}/${
                this.selectedRepository.name
              }`;
        } else {
          full_image_name = `${this.selectedRegistry.endpoint}/${
            this.selectedRepository.name
          }`;
        }
        this.close.next({
          registry_endpoint: this.selectedRegistry.endpoint,
          is_public_registry: !this.selectedRegistry.is_third,
          registry_name: this.selectedRegistry.name,
          project_name: this.selectedRegistry.is_third
            ? this.namespaceName
            : '',
          repository_name: this.selectedRepository.name,
          tag: this.imageTag,
          full_image_name,
        });
        break;
    }
  }

  cancel() {
    this.close.next(null);
  }

  filterKeyChange() {
    this.filterFnSubject$.next((repo: any) =>
      repo.name.includes(this.filterKey),
    );
  }

  repositorySelected(repo: any) {
    this.selectedRepository = repo;
    this.imageTag = '';
    this.getImageTags();
  }

  async registryChange(registry: any, additional = false) {
    if (additional) {
      if (
        this.selectedRegistry &&
        registry.name === this.selectedRegistry.name
      ) {
        return;
      }
      this.additionalRegistry = registry;
      this.registryListTooltipInstance.hideTooltip();
    } else {
      this.additionalRegistry = null;
    }
    this.selectedRegistry = registry;
    this.selectedRepository = '';
    this.repositoriesWrap$.next(from(this.getRepositories()));
  }

  applyExampleRepository() {
    this.imageAddressInput = this.exampleRepositoryAddress;
  }

  shouldDisableConfirmButton() {
    return (
      !(this.selectedRepository && this.imageTag && this.mode === 'select') &&
      !(this.imageAddressInput && this.mode === 'input')
    );
  }

  private async getImageTags() {
    let tags: any[];
    this.imageTagsLoading = true;
    if (this.selectedRegistry.is_third) {
      // private registry with default namespace as project
      tags = await this.repositoryService.getRepositoryTags({
        registry_name: this.selectedRegistry.name,
        k8s_namespace: this.namespaceName,
        repository_name: this.selectedRepository.name,
        is_third: true,
      });
    } else {
      // public registry with no project
      tags = await this.repositoryService.getRepositoryTags({
        registry_name: this.selectedRegistry.name,
        repository_name: this.selectedRepository.name,
      });
    }
    this.imageTagOptions = tags;
    if (tags.length) {
      this.imageTag = tags[0];
    }
    this.imageTagsLoading = false;
  }

  private async getRepositories() {
    if (this.selectedRegistry.repositories) {
      return this.selectedRegistry.repositories;
    }
    this.loading = true;
    let repositories: any = [];
    try {
      if (this.selectedRegistry.is_third) {
        // private registry with default namespace as project
        repositories = await this.repositoryService.getRepositories(
          this.selectedRegistry.name,
          this.selectedRegistry.is_public ? 'public' : this.namespaceName,
          true, // isThird
        );
      } else {
        // public registry with no project
        repositories = await this.repositoryService.getRepositories(
          this.selectedRegistry.name,
        );
      }
      this.selectedRegistry.repositories = repositories;
    } catch (err) {
      // placeholder
    }
    this.loading = false;
    return repositories;
  }

  private async getRegistries() {
    let registries = await this.registryService.find();
    registries = sortBy(registries, item => item.name);
    registries.forEach((registry: ImageRegistry) => {
      if (registry.is_third && registry.endpoint && this.projectName) {
        if (!registry.is_public) {
          // HACK: 第三方私有镜像仓库（jfrog）目前只支持接入一个，所以这里暂时可以直接固定翻译文案
          registry.display_name = this.translateService.get('private_registry');
          registry.endpoint = `${registry.endpoint}/${this.projectName}`;
        } else {
          registry.display_name = this.translateService.get('public_registry');
        }
      }
    });
    this.registries = registries;
    if (this.registries.length) {
      this.registryChange(this.registries[0]);
      this.selectedRegistry = this.registries[0];
    }
  }
}
