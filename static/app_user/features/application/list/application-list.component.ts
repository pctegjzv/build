import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, Subject, from, of } from 'rxjs';
import { filter, first, map, takeUntil } from 'rxjs/operators';

import { DialogService, DialogSize } from 'alauda-ui';
import { ServiceCreateMethodSelectComponent } from 'app2/features/service/create-method-select/service-create-method-select.component';
import { ServiceUtilitiesService } from 'app2/features/service/service-utilities.service';
import { RcImageSelection } from 'app2/features/service/service.type';
import {
  Application,
  MirrorApp,
} from 'app2/shared/services/features/app.service';
import { AppUtilitiesService } from 'app2/shared/services/features/app.utilities.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { HttpService } from 'app2/shared/services/http/http.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { delay } from 'app2/testing/helpers';
import { TranslateService } from 'app2/translate/translate.service';
import { ResourceList } from 'app_user/core/types';
import { ImageSelectDialogComponent } from 'app_user/features/application/image-select/image-select-dialog.component';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import {
  BaseResourceListComponent,
  FetchParams,
} from 'app_user/shared/abstract/base-resource-list.component';
import { MirrorResourceDeleteConfirmComponent } from 'app_user/shared/components/mirror-resource-delete-confirm/mirror-resource-delete-confirm.component';

@Component({
  styleUrls: ['application-list.component.scss'],
  templateUrl: 'application-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationListComponent
  extends BaseResourceListComponent<Application>
  implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<void>();
  projectName: string;
  clusterName: string;
  namespaceName: string;
  initialized: boolean;
  imageDisplayParamsMap = {};
  pageSize = 20;
  columns = ['name', 'status', 'image', 'action'];
  createEnabled: boolean;

  constructor(
    http: HttpClient,
    router: Router,
    activedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    private dialogService: DialogService,
    private translate: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private httpService: HttpService,
    private serviceUtil: ServiceUtilitiesService,
    private errorsToastService: ErrorsToastService,
    private workspaceComponent: WorkspaceComponent,
    public appUtilities: AppUtilitiesService,
  ) {
    super(http, router, activedRoute, cdr);
  }

  async ngOnInit() {
    super.ngOnInit();
    this.workspaceComponent.baseParams
      .pipe(
        takeUntil(this.onDestroy$),
        map((params: Params) => {
          return [params['project'], params['cluster'], params['namespace']];
        }),
      )
      .subscribe(async ([projectName, clusterName, namespaceName]) => {
        this.projectName = projectName;
        this.clusterName = clusterName;
        this.namespaceName = namespaceName;
        this.createEnabled = await this.roleUtil.resourceTypeSupportPermissions(
          'application',
          {
            project_name: projectName,
            namespace_name: namespaceName,
            cluster_name: clusterName,
          },
          'create',
        );
        this.namespaceChanged(namespaceName);
      });

    this.list$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(list => !!list),
      )
      .subscribe((list: Application[]) => {
        if (list.length) {
          this.initialized = true;
        }
        list.forEach((app: Application) => {
          if (app.services && app.services.length) {
            this.imageDisplayParamsMap[
              app.resource.uuid
            ] = this.serviceUtil.getServiceImageDisplayParams(
              app.services[0],
              'api',
            );
          }
        });
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  pageChanged(page: number) {
    this.onPageEvent({
      pageIndex: page,
      pageSize: this.pageSize,
    });
  }

  fetchResources(params: FetchParams): Observable<ResourceList> {
    if (!this.namespaceName) {
      return of({
        count: 0,
        results: [],
      });
    }
    return from(
      this.httpService.request('/ajax/v2/apps/', {
        method: 'GET',
        params: {
          cluster: this.clusterName,
          namespace: this.namespaceName,
          name: params.search,
          page: params.pageParams.pageIndex,
          page_size: params.pageParams.pageSize,
        },
        addNamespace: false,
      }),
    );
  }

  trackByFn(_index: number, item: Application) {
    return item.resource.uuid;
  }

  async createApp() {
    const modalRef = await this.dialogService.open(
      ServiceCreateMethodSelectComponent,
    );
    modalRef.componentInstance.initResourceType('app');
    modalRef.componentInstance.finish.pipe(first()).subscribe((res: any) => {
      modalRef.close();
      if (res === 'repo') {
        setTimeout(() => {
          this.openImageSelectionDialog();
        }, 200);
      } else if (res === 'yaml') {
        this.router.navigate(['app', 'create'], {
          relativeTo: this.workspaceComponent.baseActivatedRoute,
          queryParams: {
            type: 'yaml',
          },
        });
      }
    });
  }

  private async openImageSelectionDialog() {
    const modalRef = await this.dialogService.open(ImageSelectDialogComponent, {
      size: DialogSize.Big,
      data: {
        projectName: this.projectName,
        namespaceName: this.namespaceName,
      },
    });
    modalRef.componentInstance.close
      .pipe(first())
      .subscribe((res: RcImageSelection) => {
        modalRef.close();
        if (res) {
          const queryParams = Object.assign(
            {
              type: 'repo',
            },
            res,
          );
          this.router.navigate(['app', 'create'], {
            relativeTo: this.workspaceComponent.baseActivatedRoute,
            queryParams,
          });
        }
      });
  }

  updateApp(uuid: string) {
    this.router.navigate(['app', 'update', uuid], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  async deleteApp(app: Application) {
    const mirrorApps = await this.appUtilities.getMirrorApps(app);
    if (mirrorApps.length) {
      const requireSelect = mirrorApps.filter(item => {
        return item.uuid === app.resource.uuid;
      });
      const modalRef = await this.dialogService.open(
        MirrorResourceDeleteConfirmComponent,
      );
      const mirrorClusterMeta = {
        displayName: 'cluster_display_name',
        mirrorResource: mirrorApps,
        confirmTitle: this.translate.get('delete_app'),
        confirmLabel: this.translate.get('cluster'),
        confirmContent: this.translate.get(
          'app_service_delete_selected_cluster_app_confirm',
          {
            app_name: app.resource.name,
          },
        ),
      };
      modalRef.componentInstance.setMirrorResourceMeta(
        mirrorClusterMeta,
        requireSelect,
      );
      modalRef.componentInstance.initialized = true;
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe(async (res: MirrorApp[]) => {
          modalRef.close();
          if (res) {
            try {
              await this.appUtilities.deleteMirrorApps(res);
              await delay(1000);
              this.onUpdate(null);
            } catch (err) {
              this.errorsToastService.error(err);
            }
          }
        });
    } else {
      await this.appUtilities.deleteApp(app);
      await delay(1000);
      this.onUpdate(null);
    }
  }
}
