import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { CodeEditorIntl, MonacoProviderService } from 'alauda-ui';
import { FormatUtcStrPipe } from 'app2/shared/pipes/format-utc-str.pipe';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { HttpService } from 'app2/shared/services/http/http.service';
import { TranslateService } from 'app2/translate/translate.service';
import { get } from 'lodash';
import * as md5 from 'md5';
import { editor } from 'monaco-editor';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, mergeScan, publishReplay, refCount, tap } from 'rxjs/operators';

interface ComponentInstance {
  uuid: string;
  name: string;
  status: string;
  labels: {
    [key: string]: string;
  };
  containers: any[];
}

interface ComponentContainer {
  name: string;
  state: string; // https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.10/#containerstate-v1-core
  // one of 'running', 'terminated', 'waiting'
}

export interface LogDetails {
  info: LogInfo;
  logs: LogLine[];
  selection: LogSelection;
}

export interface LogInfo {
  podName: string;
  containerName: string;
  initContainerName: string;
  fromDate: string;
  toDate: string;
  truncated: boolean;
}

export interface LogLine {
  timestamp: string;
  content: string;
}

export interface LogSelection {
  logFilePosition: string;
  referencePoint: LogLineReference;
  offsetFrom: number;
  offsetTo: number;
}

export interface LogSelectionParams {
  logFilePosition: string;
  referenceTimestamp: string;
  referenceLineNum: number;
  offsetFrom: number;
  offsetTo: number;
}

export interface LogLineReference {
  timestamp: string;
  lineNum: number;
}

type LogSelectionParamScaner = (prev: LogSelection) => LogSelectionParams;

const logsPerView = 100;
const maxLogSize = 2e9;
// Load logs from the beginning of the log file. This matters only if the log file is too large to
// be loaded completely.
const beginningOfLogFile = 'beginning';
// Load logs from the end of the log file. This matters only if the log file is too large to be
// loaded completely.
const endOfLogFile = 'end';
const oldestTimestamp = 'oldest';
const newestTimestamp = 'newest';

@Component({
  selector: 'rc-component-log',
  templateUrl: 'component-log.component.html',
  styleUrls: ['component-log.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ComponentLogComponent implements OnInit, OnDestroy {
  @Input()
  uuid: string; // component uuid
  @Input()
  clusterUuid: string;
  @Input()
  namespaceName: string;
  private intervalTimer: any;
  private editor: editor.IStandaloneCodeEditor;

  private logDetails$: Observable<LogDetails>;
  private logDetails: LogDetails; // Latest fetched log detail
  private logSelectionParamScanner$ = new BehaviorSubject<
    LogSelectionParamScaner
  >(null);
  private selectedInstance: ComponentInstance;
  private selectedContainer: ComponentContainer;
  selectedInstanceName: string;
  selectedContainerName: string;
  logs$: Observable<string>;
  logInfo$: Observable<LogInfo>;
  logsReady$: Observable<boolean>;
  externalLogPageUrl: string; // integrated log page url for CMB

  instances: Array<ComponentInstance>;
  containers: Array<ComponentContainer>;

  monacoOptions: editor.IEditorConstructionOptions = {
    wordWrap: 'on',
    readOnly: true,
    renderLineHighlight: 'none',
  };

  pollNewest = true;
  initialized: boolean;

  constructor(
    private cdr: ChangeDetectorRef,
    private zone: NgZone,
    private serviceService: ServiceService,
    private translate: TranslateService,
    private httpService: HttpService,
    private regionService: RegionService,
    public codeEditorIntl: CodeEditorIntl,
    public monacoProvider: MonacoProviderService,
  ) {}

  ngOnInit() {
    this.instances = [];
    this.containers = [];
    this.getComponentInstances();
    this.intervalTimer = setInterval(() => {
      if (this.pollNewest) {
        this.loadNewest();
      }
    }, 10000);
  }

  ngOnDestroy() {
    clearInterval(this.intervalTimer);
  }

  onInstanceNameSelect(instanceName: string) {
    this.selectedInstance = this.instances.find((item: ComponentInstance) => {
      return item.name === instanceName;
    });
    this.resetContainerNamesOptions();
    this.setupLogDetails$();
  }

  onContainerNameSelect(containerName: string) {
    this.selectedContainer = this.selectedInstance.containers.find(
      (item: ComponentContainer) => {
        return item.name === containerName;
      },
    );
    this.setupLogDetails$();
  }

  loadNewest() {
    this.logSelectionParamScanner$.next(_prev => ({
      logFilePosition: endOfLogFile,
      referenceTimestamp: newestTimestamp,
      referenceLineNum: 0,
      offsetFrom: maxLogSize,
      offsetTo: maxLogSize + logsPerView,
    }));
  }

  loadOldest() {
    this.logSelectionParamScanner$.next(_prev => ({
      logFilePosition: beginningOfLogFile,
      referenceTimestamp: oldestTimestamp,
      referenceLineNum: 0,
      offsetFrom: -maxLogSize - logsPerView,
      offsetTo: -maxLogSize,
    }));
    this.pollNewest = false;
  }

  /**
   * Shifts view by maxLogSize lines to the past.
   * @export
   */
  loadOlder() {
    this.logSelectionParamScanner$.next(prev => ({
      logFilePosition: prev.logFilePosition,
      referenceTimestamp: prev.referencePoint.timestamp,
      referenceLineNum: prev.referencePoint.lineNum,
      offsetFrom: prev.offsetFrom - logsPerView,
      offsetTo: prev.offsetFrom,
    }));
    this.pollNewest = false;
  }

  /**
   * Shifts view by maxLogSize lines to the future.
   * @export
   */
  loadNewer() {
    this.logSelectionParamScanner$.next(prev => ({
      logFilePosition: prev.logFilePosition,
      referenceTimestamp: prev.referencePoint.timestamp,
      referenceLineNum: prev.referencePoint.lineNum,
      offsetFrom: prev.offsetTo,
      offsetTo: prev.offsetTo + logsPerView,
    }));
    this.pollNewest = false;
  }

  onFindClicked() {
    return this.checkActionAndRun('actions.find');
  }

  onMonacoEditorChanged(editor: editor.IStandaloneCodeEditor) {
    this.editor = editor;
    this.scrollToBottom();
  }

  private async getComponentInstances() {
    const { result } = await this.serviceService.getK8sServiceInstances<{
      spec: {
        containers: {
          name: string;
        }[];
      };
      status: {
        phase: string;
      };
      metadata: {
        name: string;
        namespace: string;
        uid: string;
        labels: any;
      };
    }>(this.uuid);

    this.instances = result.map(
      ({
        spec: { containers },
        status: { phase },
        metadata: { name, uid, labels },
      }) => ({
        uuid: uid,
        name,
        labels,
        status: phase,
        containers: containers.map(item => {
          return {
            name: item.name,
          };
        }),
      }),
    );

    if (this.instances.length) {
      this.selectedInstance = this.instances[0];
      this.selectedInstanceName = this.selectedInstance.name;
      this.resetContainerNamesOptions();
      this.setupLogDetails$();
      this.generateExternalLogPageUrl(this.instances);
    }
    this.initialized = true;
    this.cdr.markForCheck();
  }

  private resetContainerNamesOptions() {
    if (!this.selectedInstance) {
      return;
    }
    this.containers = this.selectedInstance.containers.map(item => item);
    if (this.containers.length) {
      this.selectedContainer = this.containers[0];
      this.selectedContainerName = this.selectedContainer.name;
    }
  }

  private checkActionAndRun(actionName: string) {
    const action = this.getEditorAction(actionName);
    return action && action.run();
  }

  private getEditorAction(actionName: string) {
    return this.editor && this.editor.getAction(actionName);
  }

  private formatAllLogs_(logs: LogLine[]) {
    if (logs.length === 0) {
      logs = [
        {
          timestamp: '0',
          content: this.translate.get('zero_state_hint', {
            resourceName: this.translate.get('logs'),
          }),
        },
      ];
    }
    return logs.map(line => this.formatLine(line));
  }

  /**
   * Formats the given log line as raw HTML to display to the user.
   */
  private formatLine(line: LogLine) {
    const formatter = new FormatUtcStrPipe();
    return `${formatter.transform(line.timestamp)} ${line.content}`;
  }

  private scrollToBottom() {
    this.zone.runOutsideAngular(() => {
      setTimeout(() => {
        if (this.editor && this.logDetails) {
          this.editor.revealLine(this.logDetails.logs.length);
        }
      });
    });
  }

  private setupLogDetails$() {
    if (!this.selectedInstance || !this.selectedContainer) {
      return;
    }
    this.logDetails$ = this.logSelectionParamScanner$.pipe(
      mergeScan<LogSelectionParamScaner, LogDetails>(
        (prev, scannerFn) => {
          const params: any = scannerFn(prev && prev.selection);
          function sanitizeParam(param: string) {
            if (params[param] !== undefined && params[param] !== null) {
              params[param] = '' + params[param];
            } else {
              params[param] = '';
            }
          }

          Object.keys(params).forEach(param => sanitizeParam(param));

          const endpoint = `/ajax/v2/kubernetes/clusters/${
            this.clusterUuid
          }/pods/${this.namespaceName}/${this.selectedInstance.name}/${
            this.selectedContainer.name
          }/log`;

          if (this.selectedContainer.state === 'terminated') {
            params.previous = true;
          }
          return this.httpService.request<LogDetails>(endpoint, {
            method: 'GET',
            params,
            addNamespace: false,
          });
        },
        null,
        1,
      ),
      tap(logDetails => {
        this.logDetails = logDetails;
      }),
      publishReplay(1),
      refCount(),
    );

    this.logs$ = this.logDetails$.pipe(
      map(logDetails => this.formatAllLogs_(logDetails.logs).join('\n')),
    );

    this.logInfo$ = this.logDetails$.pipe(map(logDetails => logDetails.info));

    this.logsReady$ = this.logInfo$.pipe(
      map(logInfo => logInfo.fromDate && logInfo.fromDate !== '0'),
    );

    this.loadNewest();
  }

  private async getClucsterLogDomain() {
    const cluster = await this.regionService.getCluster(this.clusterUuid);
    return get(cluster, 'features.customized.log.es', '');
  }

  private async generateExternalLogPageUrl(instances: ComponentInstance[]) {
    const logDomain = await this.getClucsterLogDomain();
    const guid = instances[0].labels['cmb-log-tag'];
    const sign = md5(`guid=${guid}`);
    const podNames = instances.map(instance => instance.name).join(',');
    this.externalLogPageUrl = `http://${logDomain}/sso?guid=${guid}&sign=${sign}&podNames=${podNames}`;
  }
}
