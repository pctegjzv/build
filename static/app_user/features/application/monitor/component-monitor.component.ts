import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { LogService } from 'app2/shared/services/features/log.service';
import { MonitorUtilitiesService } from 'app2/shared/services/features/monitor.utilities.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { HttpService } from 'app2/shared/services/http/http.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import {
  AGGREGATORS,
  MODES,
  MONITOR_COLOR_SET,
  TIME_STAMP_OPTIONS,
} from 'app_user/features/application/monitor/component-monitor.constant';
import * as _ from 'lodash';
import moment from 'moment';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

export interface GroupMode {
  key: string;
  name: string;
}

export interface Aggregator {
  key: string;
  name: string;
}

export interface TimeStampOption {
  type: string;
  offset: number;
  name: string;
}

export interface ChartData {
  name: string;
  value: number;
}

export interface MetricsDetails {
  data: Metrics[];
}

export interface Metrics {
  metric: {
    container_name: string;
  };
  values: number[][];
}

export interface MonitorSelectionParams {
  mode: string;
  aggregator: string;
  start: number;
  end: number;
}

export interface MetricQuery {
  start: number;
  end: number;
  step: number;
  queries?: MetricQueries[];
}

export interface MetricQueries {
  aggregator: string;
  downsample?: string;
  labels?: MetricLabels[];
  group_by?: string;
}

export interface MetricLabels {
  type: string;
  name: string;
  value: string;
}

export interface YAxisScale {
  yScaleMin: number;
  yScaleMax: number;
}

const QUERY_TIME_STAMP_OPTIONS = TIME_STAMP_OPTIONS.concat([
  {
    type: 'custom_time_range',
    offset: 0,
  },
]);

@Component({
  selector: 'rc-component-monitor',
  templateUrl: 'component-monitor.component.html',
  styleUrls: ['component-monitor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ComponentMonitorComponent implements OnInit, OnDestroy {
  @Input()
  uuid: string;
  @Input()
  clusterName: string;
  @Input()
  namespaceName: string;
  @Input()
  componentName: string;
  @Input()
  componentType: string;
  private onDestroy$ = new Subject<void>();
  private intervalTimer: number;

  private loadMetrics$ = new BehaviorSubject(null);

  pods: any;
  modes = MODES;
  aggregators = AGGREGATORS;
  timeStampOptions = QUERY_TIME_STAMP_OPTIONS.map(option => {
    return {
      ...option,
      name: option.type,
    };
  });
  private selectedGroupMode = this.modes[0].key;
  private selectedAggregator = this.aggregators[0].key;
  private selectedTimeStampOption = this.timeStampOptions[0].type;
  timeRangeOutOfDate = false;
  current_time = moment();
  start_time = this.current_time
    .clone()
    .startOf('day')
    .subtract(6, 'days');
  step: number;
  end_time: number;
  queryDates = {
    start_time: this.start_time.valueOf(),
    end_time: this.current_time.valueOf(),
  };
  queryDatesShown = {
    start_time: this.logService.dateNumToStr(this.queryDates.start_time),
    end_time: this.logService.dateNumToStr(this.queryDates.end_time),
  };
  dateTimeOptions = {
    maxDate: this.current_time
      .clone()
      .endOf('day')
      .valueOf(),
    enableTime: true,
  };
  chart: any;
  chartLoading = true;
  cpuData: ChartData[] = [];
  memData: ChartData[] = [];
  sentBytesData: ChartData[] = [];
  receivedBytesData: ChartData[] = [];
  cpuChartScale: YAxisScale;
  memChartScale: YAxisScale;
  sentBytesChartScale: YAxisScale;
  receivedBytesChartScale: YAxisScale;

  constructor(
    private cdr: ChangeDetectorRef,
    private httpService: HttpService,
    private monitorUtilitiesService: MonitorUtilitiesService,
    private serviceService: ServiceService,
    private logService: LogService,
    private errorsToastService: ErrorsToastService,
    @Inject(ACCOUNT) public account: RcAccount,
  ) {}

  async ngOnInit() {
    this.chart = {
      scheme: {
        domain: MONITOR_COLOR_SET,
      },
      xAxisTickFormatting: (tick: string) => {
        return moment(parseInt(tick, 10) * 1000).format('MM-DD HH:mm');
      },
      yAxisTickFormattingPercent: (val: number) => {
        if (val * 100 > 0.01 || val === 0) {
          return (val * 100).toPrecision(3) + '%';
        }
        return (val * 100).toFixed(4) + '%';
      },
      yAxisTickFormatting: (val: number) => {
        if (val >= 1000) {
          return val / 1000 + 'k';
        } else {
          return val;
        }
      },
    };
    const response = await this.serviceService.getK8sServiceInstances(
      this.uuid,
    );
    this.pods = response['result'].map(pod => {
      return pod.metadata.name;
    });
    this.queryMetrics$();
    this.cdr.markForCheck();
    this.intervalTimer = window.setInterval(() => {
      if (!this.chartLoading) {
        if (this.selectedTimeStampOption !== 'custom_time_range') {
          this.resetTimeRange();
        }
        this.loadCharts();
      }
    }, 60000);
  }

  ngOnDestroy() {
    window.clearInterval(this.intervalTimer);
    this.onDestroy$.next();
  }

  onGroupModeSelect() {
    this.chartLoading = true;
    this.loadCharts();
  }

  onAggregatorSelect() {
    this.chartLoading = true;
    this.loadCharts();
  }

  onTimeStampOptionSelect() {
    this.timeRangeOutOfDate = false;
    if (this.selectedTimeStampOption !== 'custom_time_range') {
      this.resetTimeRange();
    }
    this.chartLoading = true;
    this.loadCharts();
  }

  searchClicked() {
    this.chartLoading = true;
    this.loadCharts();
  }

  private resetTimeRange() {
    this.queryDates = this.logService.getTimeRangeByRangeType(
      this.selectedTimeStampOption,
    );
    this.fillCalendar(this.queryDates.start_time, this.queryDates.end_time);
  }

  fillCalendar(start_time: number, end_time: number) {
    this.queryDatesShown.start_time = this.logService.dateNumToStr(start_time);
    this.queryDatesShown.end_time = this.logService.dateNumToStr(end_time);
  }

  onStartTimeSelect(event: any) {
    this.queryDates.start_time = this.logService.dateStrToNum(event);
  }

  onEndTimeSelect(event: any) {
    this.queryDates.end_time = this.logService.dateStrToNum(event);
  }

  loadCharts() {
    this.loadMetrics$.next({
      mode: this.selectedGroupMode,
      aggregator: this.selectedAggregator,
      start: parseInt((this.queryDates.start_time / 1000).toFixed(0), 10),
      end: parseInt((this.queryDates.end_time / 1000).toFixed(0), 10),
    });
  }

  private getYAxisValueScale(data: ChartData[]) {
    let {
      yScaleMin,
      yScaleMax,
    } = this.monitorUtilitiesService.getYAxisValueScale(data);
    if (yScaleMin < 0.000001) {
      yScaleMin = 0;
    }
    if (yScaleMax < 0.000001) {
      yScaleMax = 0.000001;
    }
    return { yScaleMin, yScaleMax };
  }

  private parseMetricsRequest(
    query: MetricQueries,
    metricParams: MetricQuery,
    nameLabels: string,
    downsample?: string,
  ) {
    const queryCloned = _.cloneDeep(query);
    queryCloned.labels.push({
      type: 'EQUAL',
      name: '__name__',
      value: nameLabels,
    });
    if (downsample) {
      queryCloned.downsample = downsample;
    }
    const params = _.cloneDeep(metricParams);
    params.queries = [queryCloned];
    const endpoint = `/ajax/v1/metrics/${this.clusterName}/query_range`;
    return this.httpService.request<MetricsDetails>(endpoint, {
      method: 'POST',
      body: params,
      addNamespace: false,
    });
  }

  private parseMetricsResponse(result: any) {
    if (result === undefined || result.result === undefined) {
      return [];
    }
    return result.result.map((el: any) => {
      // fill in the chart when result is less than 30 points
      if (el.values.length < 30) {
        let dateNewest = this.end_time;
        const obj = {};
        for (let i = 0; i < 30; i++) {
          obj[(dateNewest -= this.step)] = 0;
        }
        if (el.values.length > 0) {
          el.values.forEach((element: any) => {
            obj[element[0]] = element[1];
          });
        }
        el.values = Object.keys(obj).map(function(key) {
          return [Number(key), obj[key]];
        });
      }
      let name = this.componentName;
      if (this.selectedGroupMode === 'pod') {
        name = el.metric.pod_name;
      } else if (this.selectedGroupMode === 'container') {
        name = el.metric.container_name;
      }
      return {
        name: name,
        series: el.values.map((value: number[]) => {
          return {
            value: value[1],
            name: value[0] + '',
          };
        }),
      };
    });
  }

  private queryMetrics$() {
    if (!this.selectedGroupMode || !this.selectedAggregator) {
      return;
    }
    this.loadMetrics$.pipe(takeUntil(this.onDestroy$)).subscribe(args => {
      if (args === null) {
        return;
      }
      this.step = parseInt(((args.end - args.start) / 30).toFixed(0), 10);
      this.end_time = args.end;
      const params: MetricQuery = {
        start: args.start + this.step,
        end: args.end,
        step: this.step,
      };
      const query: MetricQueries = {
        aggregator: args.aggregator,
        downsample: 'avg',
        labels: [
          {
            type: 'EQUAL',
            name: 'namespace',
            value: this.namespaceName,
          },
          {
            type: 'EQUAL',
            name: 'kind',
            value: this.componentType,
          },
          {
            type: 'EQUAL',
            name: 'name',
            value: this.componentName,
          },
          {
            type: 'IN',
            name: 'pod_name',
            value: this.pods.join('|'),
          },
        ],
      };
      switch (args.mode) {
        case 'pod':
          query.group_by = 'pod_name';
          break;
        case 'container':
          query.group_by = 'container_name';
      }
      const requestCpuUtilization = this.parseMetricsRequest(
        query,
        params,
        `${args.mode}.cpu.utilization`,
      );
      const requestMemUtilization = this.parseMetricsRequest(
        query,
        params,
        `${args.mode}.memory.utilization`,
      );
      const requestSentBytes = this.parseMetricsRequest(
        query,
        params,
        `${args.mode}.network.transmit_bytes`,
        'rate',
      );
      const requestReceivedBytes = this.parseMetricsRequest(
        query,
        params,
        `${args.mode}.network.receive_bytes`,
        'rate',
      );
      Promise.all([
        requestCpuUtilization,
        requestMemUtilization,
        requestSentBytes,
        requestReceivedBytes,
      ])
        .then(results => {
          this.chartLoading = false;
          if (results) {
            this.cpuData = this.parseMetricsResponse(results[0]);
            this.cpuChartScale = this.getYAxisValueScale(this.cpuData);
            this.memData = this.parseMetricsResponse(results[1]);
            this.memChartScale = this.getYAxisValueScale(this.memData);
            this.sentBytesData = this.parseMetricsResponse(results[2]);
            this.sentBytesChartScale = this.getYAxisValueScale(
              this.sentBytesData,
            );
            this.receivedBytesData = this.parseMetricsResponse(results[3]);
            this.receivedBytesChartScale = this.getYAxisValueScale(
              this.receivedBytesData,
            );
            this.cdr.markForCheck();
          }
        })
        .catch(error => {
          this.errorsToastService.error(error);
          this.chartLoading = false;
          this.cpuData = [];
          this.memData = [];
          this.sentBytesData = [];
          this.receivedBytesData = [];
          this.cdr.markForCheck();
        });
    });

    this.resetTimeRange();
    this.loadCharts();
  }
}
