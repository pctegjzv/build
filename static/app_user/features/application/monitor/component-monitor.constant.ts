export const METRICS_TYPE = [
  {
    key: 'container.cpu.utilization',
    name: 'cpu_usage',
  },
  {
    key: 'container.mem.utilization',
    name: 'memory_usage',
  },
  {
    key: 'container.network.transmit_bytes',
    name: 'sent_bytes',
  },
  {
    key: 'container.network.receive_bytes',
    name: 'received_bytes',
  },
];

export const MONITOR_COLOR_SET = [
  '#7cb5ec',
  '#61d44a',
  '#434348',
  '#f7a35c',
  '#8085e9',
  '#f15c80',
  '#e4d354',
  '#2b908f',
  '#f45b5b',
  '#08dccb',
];

export const AGGREGATORS = [
  {
    key: 'avg',
    name: 'aggregator_avg',
  },
  {
    key: 'max',
    name: 'aggregator_max',
  },
  {
    key: 'min',
    name: 'aggregator_min',
  },
];

export const MODES = [
  {
    key: 'application',
    name: 'application',
  },
  {
    key: 'pod',
    name: 'Pod',
  },
  {
    key: 'container',
    name: 'container',
  },
];

export const TIME_STAMP_OPTIONS = [
  {
    type: 'last_30_minites',
    offset: 30 * 60 * 1000,
  },
  {
    type: 'last_hour',
    offset: 60 * 60 * 1000,
  },
  {
    type: 'last_6_hour',
    offset: 6 * 3600 * 1000,
  },
  {
    type: 'last_day',
    offset: 24 * 3600 * 1000,
  },
  {
    type: 'last_3_days',
    offset: 3 * 24 * 3600 * 1000,
  },
  {
    type: 'last_7_days',
    offset: 7 * 24 * 3600 * 1000,
  },
];

export const ALARM_TIME_STAMP_OPTIONS = [
  {
    type: '1',
    offset: 60 * 1000,
  },
  {
    type: '2',
    offset: 2 * 60 * 1000,
  },
  {
    type: '3',
    offset: 3 * 60 * 1000,
  },
  {
    type: '5',
    offset: 5 * 60 * 1000,
  },
  {
    type: '10',
    offset: 10 * 60 * 1000,
  },
  {
    type: '30',
    offset: 30 * 60 * 1000,
  },
];
