import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  AppService,
  Application,
} from 'app2/shared/services/features/app.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  templateUrl: 'application-update.component.html',
  styles: [
    `
      rc-loading-mask {
        height: 240px;
        background: transparent;
      }
    `,
  ],
})
export class ApplicationUpdateComponent implements OnInit {
  initialized: boolean;
  type: 'UI' | 'YAML';
  data: Application;
  projectName: string;
  clusterName: string;
  namespaceName: string;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private appService: AppService,
    private errorToastService: ErrorsToastService,
    private workspaceComponent: WorkspaceComponent,
  ) {}
  async ngOnInit() {
    const baseParams = this.workspaceComponent.baseParamsSnapshot;
    this.projectName = baseParams['project'];
    this.clusterName = baseParams['cluster'];
    this.namespaceName = baseParams['namespace'];
    const params = this.activatedRoute.snapshot.params;
    const uuid = params['uuid'];
    await this.getAppDetail(uuid);
    this.initialized = true;
  }

  back() {
    this.router.navigate(['app'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  private async getAppDetail(uuid: string) {
    try {
      this.data = await this.appService.getK8sApp(uuid);
      this.type = this.data.resource.create_method;
    } catch (err) {
      this.errorToastService.error(err);
      this.router.navigate(['app'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
  }
}
