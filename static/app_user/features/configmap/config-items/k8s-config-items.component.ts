import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { NotificationService } from 'alauda-ui';
import { VendorCustomerService } from 'app2/core/customization/vendor-customer.service';
import { CustomModuleName } from 'app2/core/customization/vendor-customer.types';
import { K8sConfigmapYamlDialogComponent } from 'app2/features/configmap/dialog/k8s-configmap-yaml-dialog.component';
import {
  ConfigmapService,
  K8sConfigMap,
} from 'app2/shared/services/features/configmap.service';
import { ConfigmapUtilitiesService } from 'app2/shared/services/features/configmap.utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ModalMode } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';
import { cloneDeep } from 'lodash';
import { first } from 'rxjs/operators';

@Component({
  selector: 'rc-k8s-config-items',
  templateUrl: './k8s-config-items.component.html',
  styleUrls: ['./k8s-config-items.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class K8sConfigItemsComponent
  implements OnInit, OnDestroy, AfterViewInit {
  @Input()
  set _configmapData(_configmapData: K8sConfigMap) {
    this.configmapData = _configmapData;
    this.handleData(_configmapData);
  }
  @Input()
  params: {
    clusterId: string;
    namespace: string;
    name: string;
  };
  @Input()
  clusterId: string;
  @Output()
  actionComplete = new EventEmitter<void>();

  configmapData: K8sConfigMap;
  loading: boolean;
  deleteKey = '';
  referenced_list: any[] = [];
  keyValueList: any[] = [];
  keyValueListOrigin: any[] = [];
  appUserConfigmap = false;
  columns = ['key', 'value', 'action'];

  constructor(
    public configmapUtilities: ConfigmapUtilitiesService,
    private modalService: ModalService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private configmapService: ConfigmapService,
    private vendorCustomerService: VendorCustomerService,
  ) {}

  ngOnInit() {
    this.appUserConfigmap = this.vendorCustomerService.support(
      CustomModuleName.APP_USER_CONFIGMAP,
    );
    if (this.params) {
      this.clusterId = this.clusterId || this.params.clusterId;
      this.refetch();
    }
  }

  ngAfterViewInit() {}

  ngOnDestroy() {}

  trackByFn(_index: number, item: any) {
    return item.key;
  }

  async refetch() {
    this.loading = true;
    try {
      this.configmapData = await this.configmapService.getK8sConfigmap(
        this.params,
      );
      this.handleData(this.configmapData);
    } catch (errors) {
      this.auiNotificationService.error(errors.message);
    }
    this.loading = false;
  }

  handleData(rowData: K8sConfigMap) {
    this.keyValueList = [];
    let data;
    if (rowData) {
      if (!rowData.kubernetes.data) {
        rowData.kubernetes.data = {};
      }
      data = Object.entries(rowData.kubernetes.data);
      for (const [key, value] of data) {
        this.keyValueList.push({
          key,
          value,
        });
      }
      this.keyValueListOrigin = cloneDeep(this.keyValueList);
    }
  }

  showUpdateKeyYamlDialog(data: any, readOnly = false) {
    const modelRef = this.modalService.open(K8sConfigmapYamlDialogComponent, {
      title: this.translateService.get('key') + `: ${data.key}`,
      mode: ModalMode.RIGHT_SLIDER,
      width: 900,
    });
    modelRef.componentInstance.isKey = true;
    modelRef.componentInstance.configmapData = cloneDeep(this.configmapData);
    modelRef.componentInstance.key = data.key;
    modelRef.componentInstance.value = data.value;
    modelRef.componentInstance.readOnly = readOnly;
    modelRef.componentInstance.clusterId = this.clusterId;
    modelRef.componentInstance.configItem = true;
    modelRef.componentInstance.finished.pipe(first()).subscribe(() => {
      if (this.params) {
        this.refetch();
      } else {
        this.actionComplete.next();
      }
      modelRef.close();
    });
  }

  showReferencedList(list: any) {
    this.referenced_list = list || [];
  }

  deleteConfigmapKey(rowData: any) {
    this.deleteKey = rowData.key;
    this.deleteConfigmapKeyAction();
  }

  async deleteConfigmapKeyAction() {
    const configmapDataCopy = cloneDeep(this.configmapData);
    delete configmapDataCopy.kubernetes.data[this.deleteKey];
    try {
      await this.configmapUtilities.deleteConfigmapKey(
        this.clusterId || this.params.clusterId,
        configmapDataCopy,
        this.deleteKey,
      );
      if (this.params) {
        this.refetch();
      } else {
        this.actionComplete.next();
      }
    } catch (rejection) {
      if (rejection && rejection.errors) {
        this.auiNotificationService.error(rejection.errors[0].message);
      }
    }
  }

  onKeywordChange(event: any) {
    this.keyValueList = this.keyValueListOrigin.filter(item => {
      return item.key.indexOf(event.target.value) >= 0;
    });
  }
}
