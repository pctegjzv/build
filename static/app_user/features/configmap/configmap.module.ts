import { NgModule } from '@angular/core';
import { K8sConfigmapYamlDialogComponent } from 'app2/features/configmap/dialog/k8s-configmap-yaml-dialog.component';
import { K8sConfigmapKeyValueListComponent } from 'app2/features/configmap/key-value-list/k8s-configmap-key-value-list.component';
import { FloatingPageService } from 'app2/layout/floating-page.service';
import { CodeEditorComponent } from 'app2/shared/components/code-editor/code-editor.component';
import { K8sConfigItemsComponent } from 'app_user/features/configmap/config-items/k8s-config-items.component';
import { ConfigmapRoutingModule } from 'app_user/features/configmap/configmap.routing.module';
import { ConfigmapCreateComponent } from 'app_user/features/configmap/create/configmap-create.component';
import { ConfigmapDetailComponent } from 'app_user/features/configmap/detail/configmap-detail.component';
import { ConfigItemsFieldsetComponent } from 'app_user/features/configmap/fieldset/config-items-fieldset.component';
import { K8sConfigmapFormComponent } from 'app_user/features/configmap/form/k8s-configmap-form.component';
import { ConfigmapListComponent } from 'app_user/features/configmap/list/configmap-list.component';
import { ConfigmapUpdateComponent } from 'app_user/features/configmap/update/configmap-update.component';
import { SharedModule } from 'app_user/shared/shared.module';

@NgModule({
  imports: [SharedModule, ConfigmapRoutingModule],
  providers: [FloatingPageService],
  declarations: [
    ConfigmapListComponent,
    K8sConfigmapKeyValueListComponent,
    ConfigmapCreateComponent,
    CodeEditorComponent,
    ConfigmapDetailComponent,
    K8sConfigmapFormComponent,
    ConfigItemsFieldsetComponent,
    ConfigmapUpdateComponent,
    K8sConfigmapYamlDialogComponent,
    K8sConfigItemsComponent,
  ],
  entryComponents: [K8sConfigmapYamlDialogComponent],
})
export class ConfigmapModule {}
