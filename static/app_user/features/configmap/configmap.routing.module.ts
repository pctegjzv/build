import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfigmapCreateComponent } from 'app_user/features/configmap/create/configmap-create.component';
import { ConfigmapDetailComponent } from 'app_user/features/configmap/detail/configmap-detail.component';
import { ConfigmapListComponent } from 'app_user/features/configmap/list/configmap-list.component';
import { ConfigmapUpdateComponent } from 'app_user/features/configmap/update/configmap-update.component';

const routes: Routes = [
  {
    path: '',
    component: ConfigmapListComponent,
  },
  {
    path: 'create',
    component: ConfigmapCreateComponent,
  },
  {
    path: 'update/:name',
    component: ConfigmapUpdateComponent,
  },
  {
    path: 'detail/:name',
    component: ConfigmapDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfigmapRoutingModule {}
