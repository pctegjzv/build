import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { DialogService, NotificationService } from 'alauda-ui';
import { K8sConfigmapYamlDialogComponent } from 'app2/features/configmap/dialog/k8s-configmap-yaml-dialog.component';
import {
  ConfigmapService,
  K8sConfigMap,
  MirrorConfigmap,
} from 'app2/shared/services/features/configmap.service';
import { ConfigmapUtilitiesService } from 'app2/shared/services/features/configmap.utilities.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { ModalMode } from 'app2/shared/services/modal/modal.types';
import { TranslateService } from 'app2/translate/translate.service';
import { Subject } from 'rxjs';
import { first, map, takeUntil } from 'rxjs/operators';

import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { MirrorResourceDeleteConfirmComponent } from 'app_user/shared/components/mirror-resource-delete-confirm/mirror-resource-delete-confirm.component';
import { get } from 'lodash';

@Component({
  templateUrl: './configmap-detail.component.html',
  styleUrls: ['./configmap-detail.component.scss'],
})
export class ConfigmapDetailComponent implements OnInit {
  private _loading: boolean;
  private _loadError: any;
  private projectName: string;
  private clusterName: string;
  private namespaceName: string;
  private name: string;
  private onDestroy$ = new Subject<void>();
  configmapData: K8sConfigMap;
  mirrorConfigmaps: MirrorConfigmap[];
  originMirrorConfigmaps: MirrorConfigmap[];
  clusterDisplayName: string;
  initialized = false;

  constructor(
    private translateService: TranslateService,
    private route: ActivatedRoute,
    private configmapService: ConfigmapService,
    private auiNotificationService: NotificationService,
    public configmapUtilities: ConfigmapUtilitiesService,
    private router: Router,
    private dialogService: DialogService,
    private workspaceComponent: WorkspaceComponent,
    private modalService: ModalService,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {}

  async ngOnInit() {
    this.workspaceComponent.baseParams
      .pipe(
        takeUntil(this.onDestroy$),
        map((params: Params) => {
          return [params['project'], params['cluster'], params['namespace']];
        }),
      )
      .subscribe(async ([projectName, clusterName, namespaceName]) => {
        this.projectName = projectName;
        this.clusterName = clusterName;
        this.namespaceName = namespaceName;
        const params = this.route.snapshot.params;
        this.name = params['name'];
        await this.refetch();
        this.initialized = true;
      });
  }

  back() {
    this.router.navigate(['configmap'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  async refetch() {
    this._loading = true;
    try {
      this.configmapData = await this.configmapService.getK8sConfigmap({
        clusterId: this.clusterName,
        namespace: this.namespaceName,
        name: this.name,
      });
      this.clusterDisplayName = this.clusterName; // TODO: FIXME
      await this.getMirrorConfigmaps();
      this._loadError = null;
    } catch ({ status, errors }) {
      this._loadError = errors;
      if (status === 403) {
        // TODO: back to pervious state or parent state
        this.auiNotificationService.warning(
          this.translateService.get('permission_denied'),
        );
        return this.router.navigate(['configmap'], {
          relativeTo: this.workspaceComponent.baseActivatedRoute,
        });
      }
    }
    this._loading = false;
  }

  updateConfigmap() {
    this.router.navigate(['configmap', 'update', this.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  showYamlDialog(title: string, addConfig = false, readOnly = false) {
    const modelRef = this.modalService.open(K8sConfigmapYamlDialogComponent, {
      title: this.translateService.get(title),
      mode: ModalMode.RIGHT_SLIDER,
      width: 900,
    });
    modelRef.componentInstance.addConfiguration = addConfig;
    modelRef.componentInstance.readOnly = readOnly;
    modelRef.componentInstance.params = {
      clusterId: this.clusterName,
      namespace: this.namespaceName,
      name: this.name,
      clusterName: this.clusterName,
    };
    modelRef.componentInstance.finished.pipe(first()).subscribe(() => {
      this.refetch();
      modelRef.close();
    });
  }

  get empty() {
    return !this.configmapData;
  }

  get keyEmpty() {
    if (this.configmapData) {
      return !this.configmapData.hasOwnProperty('referenced_by');
    }
    return false;
  }

  get loading() {
    return this._loading;
  }

  get loadError() {
    return this._loadError;
  }

  get description() {
    return get(
      this.configmapData,
      `kubernetes.metadata.annotations['resource.${
        this.env.label_base_domain
      }/description']`,
      '',
    );
  }

  handleData() {
    let data: any;
    if (this.configmapData) {
      data = Object.entries(this.configmapData.kubernetes.data);
      data = data.sort((kvArr: any, kvArr_next: any) => {
        if (kvArr[0] < kvArr_next[0]) {
          return -1;
        }
        if (kvArr[0] > kvArr_next[0]) {
          return 1;
        }
        return 0;
      });
    }
    return data;
  }

  async deleteConfigmap() {
    if (this.originMirrorConfigmaps.length) {
      const requireSelect = this.originMirrorConfigmaps.filter((item: any) => {
        return item.uuid === this.configmapData.kubernetes.metadata.uid;
      });
      const mirrorClusterMeta = {
        displayName: 'cluster_display_name',
        mirrorResource: this.originMirrorConfigmaps,
        confirmTitle: this.translateService.get('delete_configmap'),
        confirmLabel: this.translateService.get('cluster'),
        confirmContent: this.translateService.get(
          'configmap_service_delete_selected_cluster_configmap_confirm',
          {
            configmap_name: this.configmapData.kubernetes.metadata.name,
          },
        ),
      };
      const modalRef = await this.dialogService.open(
        MirrorResourceDeleteConfirmComponent,
      );
      modalRef.componentInstance.setMirrorResourceMeta(
        mirrorClusterMeta,
        requireSelect,
      );
      modalRef.componentInstance.initialized = true;
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe(async (res: MirrorConfigmap[]) => {
          modalRef.close();
          if (res) {
            try {
              this.configmapUtilities.deleteMirrorConfigmap(
                res,
                this.configmapData,
              );
              return this.router.navigate(['configmap'], {
                relativeTo: this.workspaceComponent.baseActivatedRoute,
              });
            } catch (err) {
              if (err && err.errors) {
                this.auiNotificationService.error(err.errors[0].message);
              }
            }
          }
        });
    } else {
      try {
        await this.configmapUtilities.deleteConfigmap(
          this.clusterName,
          this.configmapData,
        );
        return this.router.navigate(['configmap'], {
          relativeTo: this.workspaceComponent.baseActivatedRoute,
        });
      } catch (err) {
        if (err && err.errors) {
          this.auiNotificationService.error(err.errors[0].message);
        }
      }
    }
  }

  async doRefetch() {
    await this.refetch();
  }

  async getMirrorConfigmaps() {
    const mirrorConfigmaps = await this.configmapService.getMirrorConfigmaps(
      this.clusterName,
      this.namespaceName,
      this.name,
    );
    this.originMirrorConfigmaps = mirrorConfigmaps;
    this.mirrorConfigmaps = mirrorConfigmaps.filter(
      (configmap: MirrorConfigmap) => {
        return configmap.cluster_name !== this.clusterName;
      },
    );
  }

  redirectToMirrorConfigmap(configmap: MirrorConfigmap) {
    this.router.navigate([
      '/workspace',
      {
        project: this.projectName,
        cluster: configmap.cluster_name,
        namespace: this.namespaceName,
      },
      'configmap',
      'detail',
      configmap.name,
    ]);
    this.configmapData = null;
  }
}
