import { Component, ViewChild, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, NgForm } from '@angular/forms';

import { BaseItemsFieldsetComponent } from 'app_user/shared/abstract/base-items-fieldset.component';

import { K8S_RESOURCE_KEY } from '../../../../app/components/common/config/common-pattern';
export interface ConfigItem {
  indexId?: number;
  key: string;
  value: string;
}

@Component({
  selector: 'rc-config-items-fieldset',
  templateUrl: './config-items-fieldset.component.html',
  styleUrls: ['./config-items-fieldset.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ConfigItemsFieldsetComponent),
      multi: true,
    },
  ],
})
export class ConfigItemsFieldsetComponent extends BaseItemsFieldsetComponent {
  value: ConfigItem[] = [];
  defaultValue: ConfigItem = {
    key: '',
    value: '',
  };

  keyReg = K8S_RESOURCE_KEY;

  // overwrite parent class
  @ViewChild('configItemForm')
  form: NgForm;

  constructor() {
    super();
  }
}
