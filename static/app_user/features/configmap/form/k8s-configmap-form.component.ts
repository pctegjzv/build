import { Location } from '@angular/common';
import {
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { DialogService, NotificationService } from 'alauda-ui';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import {
  ConfigmapService,
  K8sConfigMap,
} from 'app2/shared/services/features/configmap.service';
import {
  Cluster,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { updateActions } from 'app2/utils/code-editor-config';
import {
  ConfigItem,
  ConfigItemsFieldsetComponent,
} from 'app_user/features/configmap/fieldset/config-items-fieldset.component';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import * as jsyaml from 'js-yaml';
import { isEmpty, merge } from 'lodash';

import { K8S_RESOURCE_NAME_BASE } from '../../../../app/components/common/config/common-pattern';

@Component({
  selector: 'rc-configmap-form',
  templateUrl: './k8s-configmap-form.component.html',
})
export class K8sConfigmapFormComponent implements OnInit, OnDestroy {
  private namespaceName: string;
  private clusterName: string;
  @Input()
  data: K8sConfigMap;
  formModel = 'UI';
  submitting = false;
  resourceNameReg = K8S_RESOURCE_NAME_BASE;
  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: false,
  };
  initialized = false;

  configMap: K8sConfigMap;

  configItems: ConfigItem[] = [];
  mirrorClusters: {
    name: string;
    display_name: string;
  }[] = [];
  clusters: string[];
  configmapYaml = '';
  configmapYamlOrigin = '';
  editorActionsConfig = updateActions;

  @ViewChild('configmapForm')
  form: NgForm;

  @ViewChild('configItemsForm')
  configItemForm: ConfigItemsFieldsetComponent;

  get descriptionKey() {
    return `resource.${this.env.label_base_domain}/description`;
  }

  constructor(
    private configmapService: ConfigmapService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private router: Router,
    private location: Location,
    private errorsToast: ErrorsToastService,
    private auiNotificationService: NotificationService,
    private regionService: RegionService,
    private workspaceComponent: WorkspaceComponent,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {
    this.configMap = {
      kubernetes: this.genDefaultConfigmap(),
    };
  }

  async ngOnInit() {
    const baseParams = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = baseParams['cluster'];
    this.namespaceName = baseParams['namespace'];
    await this.getMirrorClusters();
    if (this.data) {
      this.configMap.kubernetes = this.data.kubernetes;
      // 联邦集群更新需要删除下面两个属性
      delete this.configMap.kubernetes.metadata.uid;
      delete this.configMap.kubernetes.metadata.resourceVersion;
      this.genConfigmapItems(this.configMap);
    } else {
      this.configItems = [
        {
          key: '',
          value: '',
        },
      ];
    }
    this.initialized = true;
  }

  genDefaultConfigmap() {
    return {
      apiVersion: 'v1',
      kind: 'ConfigMap',
      data: {},
      metadata: {
        name: '',
        annotations: {
          [`resource.${this.env.label_base_domain}/description`]: '',
        },
      },
    } as K8sConfigMap['kubernetes'];
  }

  ngOnDestroy() {}

  formModelChange(event: string) {
    switch (event) {
      case 'YAML':
        try {
          this.formatConfigmap();
          this.configmapYamlOrigin = this.configmapYaml = this.formToYaml(
            this.configMap.kubernetes,
          );
        } catch (e) {
          this.auiNotificationService.error(e.message);
        } finally {
          break;
        }
      case 'UI':
        this.configMap.kubernetes = this.yamlToForm(this.configmapYaml);
        this.genConfigmapItems(this.configMap);
        break;
    }
  }

  private yamlToForm(yaml: string) {
    let formModel: any;
    formModel = jsyaml.safeLoad(yaml);
    if (!formModel || formModel instanceof String) {
      formModel = {};
    }
    return merge(this.genDefaultConfigmap(), formModel);
  }

  private formToYaml(formModel: any) {
    try {
      const json = JSON.parse(JSON.stringify(formModel));
      const yaml = jsyaml.safeDump(json);
      return yaml;
    } catch (err) {
      this.errorsToast.error(err);
    }
  }

  private genConfigmapItems(configmap: K8sConfigMap) {
    this.configItems = [];
    if (configmap.kubernetes.data && !isEmpty(configmap.kubernetes.data)) {
      for (const [key, value] of Object.entries(configmap.kubernetes.data)) {
        this.configItems.push({
          key,
          value,
        });
      }
    } else {
      this.configItems = [
        {
          key: '',
          value: '',
        },
      ];
    }
  }

  private formatConfigmap() {
    const keys = this.configItems.map(({ key }) => key);
    const duplicateKeys: string[] = [];

    keys.forEach((key, index) => {
      if (!duplicateKeys.includes(key) && index !== keys.lastIndexOf(key)) {
        duplicateKeys.push(key);
      }
    });

    if (duplicateKeys.length) {
      const error =
        this.translate.get('configmap_duplicate_keys') +
        duplicateKeys.join(',');
      throw new Error(error);
    }

    if (this.configItems.length) {
      this.configMap.kubernetes.data = {};
      this.configItems.forEach(configItem => {
        this.configMap.kubernetes.data[configItem.key] = configItem.value;
      });
    }
    this.configMap.kubernetes.metadata.namespace = this.namespaceName;
  }

  triggerSubmit() {
    if (this.formModel === 'UI') {
      this.configItemForm.triggerSubmit();
    }
    this.form.onSubmit(null);
  }

  isValid() {
    if (this.formModel === 'UI') {
      return this.form.valid && this.configItemForm.isValid();
    }
    return this.form.valid;
  }

  async confirm() {
    this.triggerSubmit();
    if (!this.isValid()) {
      return;
    }

    try {
      this.formatConfigmap();
    } catch (e) {
      return this.auiNotificationService.error(e.message);
    }

    try {
      let comfirmTitle, comfirmContent;
      if (this.data) {
        comfirmTitle = 'update';
        comfirmContent = 'configmap_service_update_configmap_confirm';
      } else {
        comfirmTitle = 'create';
        comfirmContent = 'configmap_service_create_configmap_confirm';
      }
      await this.dialogService.confirm({
        title: this.translate.get(comfirmTitle),
        content: this.translate.get(comfirmContent, {
          configmap_name: this.configMap.kubernetes.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }

    this.submitting = true;

    try {
      if (this.data) {
        await this.configmapService.updateK8sConfigmap(
          this.clusterName,
          this.configMap,
          this.clusters,
        );
      } else {
        await this.configmapService.createK8sConfigmap(
          this.clusterName,
          this.configMap.kubernetes,
          this.clusters,
        );
      }
      this.router.navigate(
        ['configmap', 'detail', this.configMap.kubernetes.metadata.name],
        {
          relativeTo: this.workspaceComponent.baseActivatedRoute,
        },
      );
    } catch (e) {
      this.errorsToast.error(e);
    }

    this.submitting = false;
  }

  cancel() {
    this.location.back();
  }

  private async getMirrorClusters() {
    const cluster: Cluster = await this.regionService.getCluster(
      this.clusterName,
    );
    if (cluster.mirror) {
      this.mirrorClusters = cluster.mirror.regions || [];
      this.clusters = this.mirrorClusters.map((item: { name: string }) => {
        return item.name;
      });
    }
  }
}
