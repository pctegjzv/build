import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject, from } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';

import { DialogService, NotificationService } from 'alauda-ui';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import {
  ConfigmapService,
  K8sConfigMap,
  MirrorConfigmap,
} from 'app2/shared/services/features/configmap.service';
import { ConfigmapUtilitiesService } from 'app2/shared/services/features/configmap.utilities.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { HttpService } from 'app2/shared/services/http/http.service';
import { TranslateService } from 'app2/translate/translate.service';
import { ResourceList } from 'app_user/core/types';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import {
  BaseResourceListComponent,
  FetchParams,
} from 'app_user/shared/abstract/base-resource-list.component';
import { MirrorResourceDeleteConfirmComponent } from 'app_user/shared/components/mirror-resource-delete-confirm/mirror-resource-delete-confirm.component';

@Component({
  templateUrl: './configmap-list.component.html',
  styleUrls: ['./configmap-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigmapListComponent extends BaseResourceListComponent<any>
  implements OnInit, OnDestroy {
  configmapCreateEnabled: boolean;
  pageSize = 20;
  initialized: boolean;

  columns = ['name', 'namespace', 'description', 'action'];
  private onDestroy$ = new Subject<any>();
  private baseParams: {
    project: string;
    cluster: string;
    namespace: string;
  };

  get descriptionKey() {
    return `resource.${this.env.label_base_domain}/description`;
  }

  constructor(
    http: HttpClient,
    router: Router,
    activedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    private translateService: TranslateService,
    public configmapUtilities: ConfigmapUtilitiesService,
    private auiNotificationService: NotificationService,
    private roleUtil: RoleUtilitiesService,
    private httpService: HttpService,
    private dialogService: DialogService,
    private configmapService: ConfigmapService,
    private workspaceComponent: WorkspaceComponent,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {
    super(http, router, activedRoute, cdr);
  }

  async ngOnInit() {
    super.ngOnInit();
    this.baseParams = this.workspaceComponent.baseParamsSnapshot;
    this.configmapCreateEnabled = await this.roleUtil.resourceTypeSupportPermissions(
      'configmap',
      {
        cluster_name: this.baseParams['cluster'],
        project_name: this.baseParams['project'],
        namespace_name: this.baseParams['namespace'],
      },
      'create',
    );
    this.namespaceChanged(this.baseParams['namespace']);
    this.initialized = true;

    this.list$.pipe(takeUntil(this.onDestroy$)).subscribe(list => {
      if (list.length) {
        this.initialized = true;
      }
    });
  }

  fetchResources(params: FetchParams): Observable<ResourceList> {
    return from(
      this.httpService.request(
        '/ajax/v2/kubernetes/clusters/' +
          this.baseParams['cluster'] +
          '/configmaps/' +
          (this.baseParams['namespace']
            ? this.baseParams['namespace'] + '/'
            : ''),
        {
          method: 'GET',
          params: {
            name: params.search,
            page: params.pageParams.pageIndex,
            page_size: params.pageParams.pageSize,
          },
          addNamespace: false,
        },
      ),
    );
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  trackByFn(_index: number, item: K8sConfigMap) {
    return item.kubernetes.metadata.uid;
  }

  showDetail(name: string) {
    this.router.navigate(['configmap', 'detail', name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  createConfigmap() {
    this.router.navigate(['configmap', 'create'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  updateConfigmap(name: string) {
    this.router.navigate(['configmap', 'update', name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  async deleteConfigmap(configmap: K8sConfigMap) {
    const mirrorConfigmaps = await this.configmapService.getMirrorConfigmaps(
      this.baseParams['cluster'],
      this.baseParams['namespace'],
      configmap.kubernetes.metadata.name,
    );
    if (mirrorConfigmaps.length) {
      const requireSelect = mirrorConfigmaps.filter((item: any) => {
        return item.uuid === configmap.kubernetes.metadata.uid;
      });
      const mirrorClusterMeta = {
        displayName: 'cluster_display_name',
        mirrorResource: mirrorConfigmaps,
        confirmTitle: this.translateService.get('delete_configmap'),
        confirmLabel: this.translateService.get('cluster'),
        confirmContent: this.translateService.get(
          'configmap_service_delete_selected_cluster_configmap_confirm',
          {
            configmap_name: configmap.kubernetes.metadata.name,
          },
        ),
      };
      const modalRef = await this.dialogService.open(
        MirrorResourceDeleteConfirmComponent,
      );
      modalRef.componentInstance.setMirrorResourceMeta(
        mirrorClusterMeta,
        requireSelect,
      );
      modalRef.componentInstance.initialized = true;
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe(async (res: MirrorConfigmap[]) => {
          modalRef.close();
          if (res) {
            try {
              await this.configmapUtilities.deleteMirrorConfigmap(
                res,
                configmap,
              );
              this.onUpdate(null);
            } catch (err) {
              if (err && err.errors) {
                this.auiNotificationService.error(err.errors[0].message);
              }
            }
          }
        });
    } else {
      try {
        await this.configmapUtilities.deleteConfigmap(
          this.baseParams['cluster'],
          configmap,
        );
        this.onUpdate(null);
      } catch (err) {
        if (err && err.errors) {
          this.auiNotificationService.error(err.errors[0].message);
        }
      }
    }
  }

  pageChanged(pageNumber: number) {
    this.onPageEvent({
      pageIndex: pageNumber,
      pageSize: this.pageSize,
    });
  }
}
