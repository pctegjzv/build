import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ConfigmapService,
  K8sConfigMap,
} from 'app2/shared/services/features/configmap.service';

import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';

@Component({
  template: `
    <rc-loading-mask class="rc-form-loading-mask" [loading]="!initialized"></rc-loading-mask>
    <rc-configmap-form *ngIf="initialized" [data]="data"></rc-configmap-form>
  `,
})
export class ConfigmapUpdateComponent implements OnInit {
  private clusterName: string;
  private namespaceName: string;
  initialized = false;
  data: K8sConfigMap;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private errorsToastService: ErrorsToastService,
    private workspaceComponent: WorkspaceComponent,
    private configmapService: ConfigmapService,
  ) {}
  async ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    const name = params['name'];
    try {
      const workspaceParams = this.workspaceComponent.baseParamsSnapshot;
      this.clusterName = workspaceParams['cluster'];
      this.namespaceName = workspaceParams['namespace'];
      this.data = await this.configmapService.getK8sConfigmap({
        clusterId: this.clusterName,
        namespace: this.namespaceName,
        name,
      });
      this.initialized = true;
    } catch (err) {
      this.errorsToastService.error(err);
      return this.router.navigate(['service'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
  }
}
