import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { DIALOG_DATA, DialogRef, DialogService } from 'alauda-ui';
import { MessageService, NotificationService } from 'alauda-ui';
import { NamespaceService } from 'app2/shared/services/features/namespace.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

interface CustomDeleteNamepace {
  cluster?: {
    id: string;
    name: string;
    display_name: string;
    mirror: {
      display_name: string;
      regions: Cluster[];
    };
  };
  kubernetes: {
    kind: 'Namespace';
    metadata: {
      name: string;
      uid: string;
    };
  };
}

interface Cluster {
  name: string;
  display_name: string;
  id: string;
}

@Component({
  selector: 'rc-delete-namespace',
  templateUrl: './delete-namespace.component.html',
  styleUrls: ['./delete-namespace.component.scss'],
})
export class DeleteNamespaceComponent implements OnInit {
  inputName: string;
  clusterNames: string[] = [];
  namespace: CustomDeleteNamepace;
  isError = false;
  loading = false;
  deleteLoading = false;
  @ViewChild('form')
  form: NgForm;

  constructor(
    private dialogService: DialogService,
    private errorToast: ErrorsToastService,
    private namespaceService: NamespaceService,
    private translate: TranslateService,
    private router: Router,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    @Inject(DIALOG_DATA)
    private dialogData: {
      namespace: CustomDeleteNamepace;
      projectName: string;
    },
    private dialogRef: DialogRef<any>,
  ) {}

  async ngOnInit() {
    this.namespace = this.dialogData.namespace;
    if (
      this.namespace.cluster.mirror &&
      this.namespace.cluster.mirror.regions
    ) {
      this.loading = true;
      let count = 0;
      this.namespace.cluster.mirror.regions.forEach(async cluster => {
        if (cluster.id === this.namespace.cluster.id) {
          count += 1;
          if (count === this.namespace.cluster.mirror.regions.length) {
            this.loading = false;
          }
          return this.clusterNames.push(cluster.display_name);
        }
        try {
          await this.namespaceService.getNamespace(
            cluster.id,
            this.namespace.kubernetes.metadata.name,
          );
          count += 1;
          this.clusterNames.push(cluster.display_name);
        } catch (e) {
          count += 1;
          if (e.errors && e.errors[0].code !== 'resource_not_exist') {
            this.errorToast.error(e);
          }
        } finally {
          if (count === this.namespace.cluster.mirror.regions.length) {
            this.loading = false;
          }
        }
      });
    } else {
      this.clusterNames.push(this.namespace.cluster.display_name);
    }
  }

  async delete() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.deleteLoading = true;
    const reqData =
      this.namespace.cluster.mirror && this.namespace.cluster.mirror.regions
        ? this.namespace.cluster.mirror.regions.map(cluster => {
            return {
              clusterName: cluster.name,
              clusterDisplayName: cluster.display_name,
              namespace: this.namespace.kubernetes.metadata.name,
              projectName: this.dialogData.projectName,
            };
          })
        : [
            {
              clusterName: this.namespace.cluster.name,
              clusterDisplayName: this.namespace.cluster.display_name,
              namespace: this.namespace.kubernetes.metadata.name,
              projectName: this.dialogData.projectName,
            },
          ];
    try {
      const batchRes = await this.namespaceService.deleteNamespaceBatch(
        reqData,
      );
      let successNum = 0;
      if (batchRes.responses) {
        const values = Object.entries(batchRes.responses);
        values.forEach(([key, value]) => {
          if ((value.code + '').startsWith('2')) {
            successNum += 1;
          } else {
            this.auiNotificationService.error({
              title: this.translate.get(JSON.parse(value.body).errors[0].code),
              content:
                this.translate.get('cluster_namespace_delete_error', {
                  clusterDisplayName: key,
                }) +
                this.translate.get(JSON.parse(value.body).errors[0].message),
            });
          }
        });
        if (successNum === values.length) {
          this.auiMessageService.success({
            content: this.translate.get('delete_success'),
          });
          this.router.navigate(['/']);
        } else {
          this.dialogService.confirm({
            title: this.translate.get('delete_namespace_fail'),
            content: this.translate.get('create_namespace_tip5'),
            confirmText: this.translate.get('got_it'),
            cancelButton: false,
            beforeConfirm: reslove => {
              this.router.navigate(['/']);
              reslove();
            },
          });
        }
        this.dialogRef.close();
      }
    } catch (e) {
      this.errorToast.error(e);
    } finally {
      this.deleteLoading = false;
    }
  }
}
