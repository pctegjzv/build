import { Component, OnInit } from '@angular/core';
import { Params } from '@angular/router';
import { DialogService } from 'alauda-ui';
import { formatCommonUnit, formatNumUnit } from 'app2/features/cluster/utils';
import {
  Namespace,
  NamespaceService,
} from 'app2/shared/services/features/namespace.service';
import { ProjectService } from 'app2/shared/services/features/project.service';
import {
  Cluster,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { QuotaConfigEnum } from 'app_user/core/types';
import { DeleteNamespaceComponent } from 'app_user/features/namespace/delete-namespace/delete-namespace.component';
import { UpdateQuotaComponent } from 'app_user/features/namespace/update-quota/update-quota.component';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { find, get } from 'lodash';

@Component({
  templateUrl: './namespace-detail.component.html',
  styleUrls: ['./namespace-detail.component.scss'],
})
export class NamespaceDetailComponent implements OnInit {
  private clusterName: string;
  currTab = 'quota';
  namespace: Namespace;
  loading: boolean;
  namespaceName: string;
  quotaItem: any = {};
  cluster: Cluster;
  unitMap = {
    cpu: this.translateService.get('unit_core'),
    memory: this.translateService.get('unit_gb'),
    pvc_num: this.translateService.get('ge'),
    pods: this.translateService.get('ge'),
    storage: this.translateService.get('unit_gb'),
  };
  adminRoles: string[] = [];
  projectName: string;
  namespaceDeletePremission: boolean;
  namespaceUpdatePremission: boolean;
  accountListClusterNames: string;

  constructor(
    private dialogService: DialogService,
    private regionService: RegionService,
    private namespaceService: NamespaceService,
    private errorToast: ErrorsToastService,
    private translateService: TranslateService,
    private projectService: ProjectService,
    private roleUtil: RoleUtilitiesService,
    private workspace: WorkspaceComponent,
  ) {}

  async ngOnInit() {
    this.workspace.baseParams.subscribe(async (params: Params) => {
      this.namespaceName = params['namespace'];
      this.clusterName = params['cluster'];
      this.projectName = params['project'];
      await this.getNamespaceDetail();
      this.accountListClusterNames =
        this.cluster.mirror && this.cluster.mirror.regions
          ? this.cluster.mirror.regions.map(region => region.name).join(',')
          : this.cluster.name;
    });
  }

  keys(obj: object) {
    return Object.keys(obj);
  }

  async updateQuota() {
    const [{ clusters }, quotaResource] = await Promise.all([
      this.projectService.getProjectClustersQuota(this.projectName, [
        this.cluster.id || this.cluster.name,
      ]),
      this.namespaceService.getNamespaceQuota(
        this.cluster.id,
        this.namespaceName,
      ),
    ]);
    this.dialogService.open(UpdateQuotaComponent, {
      data: {
        projectName: this.projectName,
        clustersQuota: clusters,
        namespaceName: this.namespaceName,
        clusterName: this.clusterName,
        selfQuotaConfig: get(quotaResource, 'kubernetes.spec.hard'),
        selfUsedConfig: get(quotaResource, 'kubernetes.status.used'),
        successFun: () => this.refreshQuota(),
      },
    });
  }

  async refreshQuota() {
    const quotaResource = await this.namespaceService.getNamespaceQuota(
      this.cluster.id,
      this.namespaceName,
    );
    const k8sQuotaSpecHard = get(quotaResource, 'kubernetes.spec.hard');
    if (k8sQuotaSpecHard) {
      const quotaConfig = {
        hard: k8sQuotaSpecHard,
        used: get(quotaResource, 'kubernetes.status.used'),
      };
      ['cpu', 'memory', 'pods', 'pvc_num', 'storage'].forEach(k => {
        switch (k) {
          case 'cpu':
            this.quotaItem[k] = [
              {
                name: this.translateService.get('used'),
                value: formatNumUnit(quotaConfig.used[QuotaConfigEnum[k]]),
              },
              {
                name: this.translateService.get('unused'),
                value:
                  formatNumUnit(quotaConfig.hard[QuotaConfigEnum[k]]) -
                  formatNumUnit(quotaConfig.used[QuotaConfigEnum[k]]),
              },
            ];
            break;
          case 'memory':
          case 'storage':
            this.quotaItem[k] = [
              {
                name: this.translateService.get('used'),
                value: +formatCommonUnit(
                  quotaConfig.used[QuotaConfigEnum[k]],
                  true,
                ),
              },
              {
                name: this.translateService.get('unused'),
                value:
                  +formatCommonUnit(
                    quotaConfig.hard[QuotaConfigEnum[k]],
                    true,
                  ) -
                  +formatCommonUnit(quotaConfig.used[QuotaConfigEnum[k]], true),
              },
            ];
            break;
          case 'pods':
          case 'pvc_num':
            this.quotaItem[k] = [
              {
                name: this.translateService.get('used'),
                value: +quotaConfig.used[QuotaConfigEnum[k]],
              },
              {
                name: this.translateService.get('unused'),
                value:
                  formatNumUnit(quotaConfig.hard[QuotaConfigEnum[k]]) -
                  formatNumUnit(quotaConfig.used[QuotaConfigEnum[k]]),
              },
            ];
            break;
        }
      });
    }
  }

  async deleteNamespace() {
    const regions = await this.regionService.getRegions();
    const clusterDetail = find(regions, {
      id: this.namespace.kubernetes.metadata.uid,
    });

    this.dialogService.open(DeleteNamespaceComponent, {
      data: {
        projectName: this.projectName,
        namespace:
          clusterDetail && clusterDetail.mirror
            ? {
                cluster: Object.assign({}, this.cluster, {
                  regions: clusterDetail.mirror.regions,
                }),
                kubernetes: this.namespace.kubernetes,
              }
            : {
                cluster: this.cluster,
                kubernetes: this.namespace.kubernetes,
              },
      },
    });
  }

  private async getNamespaceDetail() {
    this.loading = true;
    try {
      const cluster: Cluster = await this.regionService.getCluster(
        this.clusterName,
      );
      const [namespace, , adminRoles] = await Promise.all([
        this.namespaceService.getNamespace(cluster.id, this.namespaceName),
        ,
        this.projectService.getAdminRoles({
          level: 'namespace',
          cluster_name: cluster.name,
          k8s_namespace_name: this.namespaceName,
          project_name: this.projectName,
        }),
      ]);
      this.namespaceDeletePremission = this.roleUtil.resourceHasPermission(
        namespace,
        'namespace',
        'delete',
      );
      this.namespaceUpdatePremission = this.roleUtil.resourceHasPermission(
        namespace,
        'namespace',
        'update',
      );
      this.cluster = cluster;
      this.namespace = namespace;
      this.adminRoles = adminRoles;
      await this.refreshQuota();
    } catch (e) {
      this.errorToast.error(e);
    } finally {
      this.loading = false;
    }
  }
}
