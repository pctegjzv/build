import { NgModule } from '@angular/core';
import { DeleteNamespaceComponent } from 'app_user/features/namespace/delete-namespace/delete-namespace.component';
import { NamespaceDetailComponent } from 'app_user/features/namespace/namespace-detail.component';
import { NamespaceRoutingModule } from 'app_user/features/namespace/namespace.routing.module';
import { UpdateQuotaComponent } from 'app_user/features/namespace/update-quota/update-quota.component';
import { SharedModule } from 'app_user/shared/shared.module';
@NgModule({
  imports: [SharedModule, NamespaceRoutingModule],
  declarations: [
    NamespaceDetailComponent,
    DeleteNamespaceComponent,
    UpdateQuotaComponent,
  ],
  entryComponents: [DeleteNamespaceComponent, UpdateQuotaComponent],
})
export class NamespaceModule {}
