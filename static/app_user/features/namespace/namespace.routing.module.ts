import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NamespaceDetailComponent } from 'app_user/features/namespace/namespace-detail.component';

const routes: Routes = [
  {
    path: '',
    component: NamespaceDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NamespaceRoutingModule {}
