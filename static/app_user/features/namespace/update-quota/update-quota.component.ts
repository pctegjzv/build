import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DIALOG_DATA, DialogRef, NotificationService } from 'alauda-ui';
import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { formatCommonUnit, formatNumUnit } from 'app2/features/cluster/utils';
import { QuotaStatusModel } from 'app2/shared/services/features/namespace.service';
import { NewCluster } from 'app2/shared/services/features/project.service';
import { HttpService } from 'app2/shared/services/http/http.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { QuotaConfigEnum } from 'app_user/core/types';
import { Quota } from 'app_user/features/project/create-namespace/quota-setting/multiple-quota-settings.component';
import { get, mapKeys } from 'lodash';

@Component({
  selector: 'rc-app-update-quota',
  templateUrl: './update-quota.component.html',
  styleUrls: ['./update-quota.component.scss'],
})
export class UpdateQuotaComponent implements OnInit {
  quotaSetting = [{ quota: {} }];
  clustersQuota: NewCluster[];
  submitting = false;
  minQuotaRange: Quota = {
    cpu: 1,
    memory: 1,
    storage: 0,
    pvc_num: 0,
    pods: 1,
  };
  @ViewChild('form')
  form: NgForm;

  constructor(
    private http: HttpService,
    private errorToastService: ErrorsToastService,
    private notification: NotificationService,
    private translate: TranslateService,
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(DIALOG_DATA)
    private dialogData: {
      clustersQuota: NewCluster[];
      namespaceName: string;
      clusterName: string;
      projectName: string;
      selfQuotaConfig: QuotaStatusModel;
      selfUsedConfig: QuotaStatusModel;
      successFun: Function;
    },
    private dialogRef: DialogRef<any>,
  ) {}

  ngOnInit() {
    this.clustersQuota = this.dialogData.clustersQuota.map(newCluster => {
      if (!get(newCluster, 'quota') || !this.dialogData.selfQuotaConfig) {
        return newCluster;
      }
      ['cpu', 'memory', 'pods', 'pvc_num', 'storage'].forEach(k => {
        switch (k) {
          case 'cpu':
          case 'pods':
          case 'pvc_num':
            newCluster.quota[k].used -= +formatNumUnit(
              this.dialogData.selfQuotaConfig[QuotaConfigEnum[k]],
            );
            this.minQuotaRange[k] = +formatNumUnit(
              this.dialogData.selfUsedConfig[QuotaConfigEnum[k]],
            );
            break;
          case 'memory':
          case 'storage':
            newCluster.quota[k].used -= +formatCommonUnit(
              this.dialogData.selfQuotaConfig[QuotaConfigEnum[k]],
              true,
            );
            this.minQuotaRange[k] = +formatCommonUnit(
              this.dialogData.selfUsedConfig[QuotaConfigEnum[k]],
              true,
            );
            break;
        }
      });
      return Object.assign({}, newCluster, { quota: newCluster.quota });
    });
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    try {
      await this.http.request(
        `/ajax/v2/custom/cmb/clusters/${
          this.dialogData.clusterName
        }/namespaces/${this.dialogData.namespaceName}`,
        {
          method: 'PUT',
          params: {
            project_name: this.dialogData.projectName,
          },
          body: {
            resourcequota: {
              apiVersion: 'v1',
              kind: 'ResourceQuota',
              metadata: {
                name: 'default',
                namespace: this.dialogData.namespaceName,
              },
              spec: {
                hard: mapKeys(
                  this.quotaSetting[0].quota,
                  (_v, k) => QuotaConfigEnum[k],
                ),
              },
            },
          },
        },
      );
      this.notification.success(this.translate.get('update_success'));
      this.dialogRef.close();
      this.dialogData.successFun();
    } catch (e) {
      this.errorToastService.error(e);
    }
  }
}
