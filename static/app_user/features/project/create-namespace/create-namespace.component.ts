import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { DialogService } from 'alauda-ui';
import { find, isEmpty, mapKeys } from 'lodash';

import { Store } from '@ngrx/store';
import { MessageService, NotificationService } from 'alauda-ui';
import { NamespaceService } from 'app2/shared/services/features/namespace.service';
import {
  NamespaceSetting,
  NewCluster,
  Project,
  ProjectService,
} from 'app2/shared/services/features/project.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { QuotaConfigEnum, QuotaRange } from 'app_user/core/types';
import { ClustersQuota } from 'app_user/features/project/create-namespace/quota-setting/multiple-quota-settings.component';
import * as fromStore from 'app_user/store';
import { filter, take } from 'rxjs/operators';

const maxStep = 2;

@Component({
  templateUrl: 'create-namespace.component.html',
  styleUrls: ['create-namespace.component.scss'],
})
export class CreateNamespaceComponent implements OnInit {
  currStep = 1;
  steps: string[];
  createStatus: null | 'ing' | 'wrong';
  deleteLoading = false;
  setting: NamespaceSetting = {
    name: '',
    cluster: null,
    quotas: [],
  };
  selectClusterNames: string[] = [];
  clusters: NewCluster[];
  clustersQuota: {
    name: string;
    display_name: string;
    uuid?: string;
    origin_display_name?: string;
    quota?: QuotaRange;
    regions?: NewCluster[];
  }[];
  loading: boolean;
  quotaLoading: boolean;
  namespaceUuid = '';
  project: Project;
  isNsExistError = false;
  namePrefix = '';

  @ViewChild('form')
  form: NgForm;

  constructor(
    private translate: TranslateService,
    private projectService: ProjectService,
    private regionService: RegionService,
    private errorToast: ErrorsToastService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private router: Router,
    private namespaceService: NamespaceService,
    private dialogService: DialogService,
    private store: Store<fromStore.AppState>,
  ) {}

  async ngOnInit() {
    this.steps = ['add_cluster_quota', 'add_member'];
    this.loading = true;

    const [project, regions] = await Promise.all([
      this.store
        .select(fromStore.getCurrentProjectDetail)
        .pipe(
          filter(project => !!project),
          take(1),
        )
        .toPromise(),
      this.regionService.getRegions(),
    ]);
    this.project = project;
    this.namePrefix = project.name && `${project.name.replace(/\./g, '-')}-`;
    this.clusters = [];
    if (project.clusters) {
      project.clusters.forEach(
        (cluster: { name: string; display_name: string; uuid: string }) => {
          const clusterDetail = find(regions, { name: cluster.name });
          if (
            clusterDetail &&
            !isEmpty(clusterDetail.mirror) &&
            !find(this.clusters, { name: clusterDetail.mirror.name })
          ) {
            this.clusters.push({
              name: clusterDetail.mirror.name,
              uuid: clusterDetail.id,
              origin_display_name: clusterDetail.mirror.display_name,
              display_name: `${clusterDetail.mirror.display_name}`,
              regions: clusterDetail.mirror.regions,
            });
          }
          if (
            clusterDetail &&
            isEmpty(clusterDetail.mirror) &&
            !find(this.clusters, { name: cluster.name })
          ) {
            this.clusters.push(cluster);
          }
        },
      );
    }
    this.loading = false;
  }

  async selectChange(cluster: NewCluster) {
    this.quotaLoading = true;
    const isMirror = !!cluster.regions;
    const clusterIDArr = isMirror
      ? cluster.regions.map(c => c.id)
      : [cluster.uuid || cluster.name];
    this.selectClusterNames = isMirror
      ? cluster.regions.map(c => c.name)
      : [cluster.name];
    const { clusters } = await this.projectService.getProjectClustersQuota(
      this.project.name,
      clusterIDArr,
    );
    this.clustersQuota = isMirror
      ? [
          {
            name: cluster.name,
            origin_display_name: cluster.origin_display_name,
            display_name: cluster.display_name,
            regions: clusters,
          },
        ]
      : clusters;
    this.quotaLoading = false;
  }

  async create() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.createStatus = 'ing';
    const reqData = this.setting.quotas.map((clusterQuota: ClustersQuota) => ({
      clusterName: clusterQuota.name,
      clusterDisplayName: clusterQuota.display_name,
      namespace: this.namePrefix + this.setting.name,
      projectName: this.project.name,
      quotaConfig: mapKeys(clusterQuota.quota, (_v, k) => QuotaConfigEnum[k]),
    }));

    try {
      const batchRes = await this.namespaceService.createNamespaceBatch(
        reqData,
      );
      let successNum = 0;
      if (batchRes.responses) {
        const values = Object.entries(batchRes.responses);
        values.forEach(([key, value]) => {
          if ((value.code + '').startsWith('2')) {
            successNum += 1;
            this.auiNotificationService.success({
              title: this.translate.get('create_namespace_success'),
              content: this.translate.get('cluster_namespace_create_success', {
                clusterDisplayName: key,
              }),
            });
          } else {
            if (
              JSON.parse(value.body).errors[0].code ===
              'namespace_already_exist'
            ) {
              this.isNsExistError = true;
            }
            this.auiNotificationService.error({
              title: this.translate.get(JSON.parse(value.body).errors[0].code),
              content:
                this.translate.get('cluster_namespace_create_error', {
                  clusterDisplayName: key,
                }) +
                this.translate.get(JSON.parse(value.body).errors[0].message),
            });
          }
        });
        if (successNum === values.length) {
          const clusterUuid = this.clustersQuota[0].regions
            ? this.clustersQuota[0].regions[0].uuid
            : this.clustersQuota[0].uuid;
          const namespace = await this.namespaceService.getNamespace(
            clusterUuid,
            this.namePrefix + this.setting.name,
          );
          this.namespaceUuid = namespace.kubernetes.metadata.uid;
          this.currStep = Math.max(this.currStep + 1, maxStep);
        } else if (this.isNsExistError) {
          this.createStatus = null;
          this.setting.cluster = null;
        } else {
          this.createStatus = 'wrong';
        }
      } else {
        this.createStatus = null;
        this.setting.cluster = null;
        this.errorToast.error(batchRes);
      }
    } catch (e) {
      this.setting.cluster = null;
      this.createStatus = null;
      this.errorToast.error(e);
    }
  }

  async delete() {
    this.deleteLoading = true;
    const reqData = this.clustersQuota[0].regions
      ? this.clustersQuota[0].regions.map(cluster => ({
          clusterName: cluster.name,
          clusterDisplayName: cluster.display_name,
          namespace: this.namePrefix + this.setting.name,
          projectName: this.project.name,
        }))
      : [
          {
            projectName: this.project.name,
            clusterName: this.clustersQuota[0].name,
            clusterDisplayName: this.clustersQuota[0].display_name,
            namespace: this.namePrefix + this.setting.name,
          },
        ];
    try {
      const batchRes = await this.namespaceService.deleteNamespaceBatch(
        reqData,
      );
      let successNum = 0;
      if (batchRes.responses) {
        const values = Object.entries(batchRes.responses);
        values.forEach(([key, value]) => {
          if ((value.code + '').startsWith('2')) {
            successNum += 1;
          } else {
            this.auiNotificationService.error({
              title: this.translate.get(JSON.parse(value.body).errors[0].code),
              content:
                this.translate.get('cluster_namespace_delete_error', {
                  clusterDisplayName: key,
                }) +
                this.translate.get(JSON.parse(value.body).errors[0].message),
            });
          }
        });
        if (successNum === values.length) {
          this.auiMessageService.success({
            content: this.translate.get('delete_success'),
          });
          this.router.navigate(['/project', { project: this.project.name }]);
        } else {
          this.dialogService.confirm({
            title: this.translate.get('delete_namespace'),
            content: this.translate.get('create_namespace_tip5'),
            confirmText: this.translate.get('got_it'),
            cancelButton: false,
            beforeConfirm: reslove => {
              this.router.navigate([
                '/project',
                { project: this.project.name },
              ]);
              reslove();
            },
          });
        }
      }
    } catch (e) {
      this.errorToast.error(e);
    } finally {
      this.deleteLoading = false;
    }
  }

  complete() {
    if (this.clustersQuota[0].regions) {
      this.router.navigate(['/project', { project: this.project.name }]);
    } else {
      this.router.navigate([
        '/workspace',
        {
          project: this.project.name,
          cluster: this.clustersQuota[0].name,
          namespace: this.namePrefix + this.setting.name,
        },
        'namespace',
      ]);
    }
  }
}
