import {
  Component,
  ElementRef,
  Input,
  OnInit,
  forwardRef,
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  NgForm,
  ValidationErrors,
  Validator,
} from '@angular/forms';
import {
  EnvironmentService,
  QUOTA_TYPES,
} from 'app2/shared/services/features/environment.service';
import { TranslateService } from 'app2/translate/translate.service';
import { QuotaRange } from 'app_user/core/types';
import { map } from 'lodash';

export interface Quota {
  cpu?: number;
  memory?: number | string;
  storage?: number | string;
  pvc_num?: number;
  pods?: number;
}

export interface ClustersQuota {
  name: string;
  display_name: string;
  origin_display_name?: string;
  quota?: QuotaRange | Quota | null;
  regions?: {
    name: string;
    display_name: string;
    quota?: QuotaRange;
  }[];
}

@Component({
  selector: 'rc-multiple-quota-settings',
  templateUrl: './multiple-quota-settings.component.html',
  styleUrls: ['./multiple-quota-settings.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MultipleQuotaSettingsComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => MultipleQuotaSettingsComponent),
      multi: true,
    },
  ],
})
export class MultipleQuotaSettingsComponent
  implements OnInit, ControlValueAccessor, Validator {
  useSameQuota = false;
  shrink: boolean[] = [];
  propagateChange = (_: any) => {};
  onTouched: () => void;
  quota: Quota = {};
  disabled: boolean;
  _validator: any;
  quotaLoading: boolean;
  _clustersQuota: ClustersQuota[];
  isMirror: boolean;
  originClustersQuota: ClustersQuota[];
  resultClusters: ClustersQuota[];
  @Input()
  minQuotaRange = {
    cpu: 1,
    memory: 1,
    storage: 0,
    pvc_num: 0,
    pods: 1,
  };
  @Input()
  get clustersQuota() {
    return this._clustersQuota;
  }
  set clustersQuota(value: ClustersQuota[]) {
    this.originClustersQuota = value;
    this.isMirror = !!(value && value[0].regions);
    this.useSameQuota = this.isMirror;

    this.refreshClustersQuota();
  }

  constructor(
    private hostElement: ElementRef,
    private environmentService: EnvironmentService,
    private parentForm: NgForm,
    private translate: TranslateService,
  ) {}

  ngOnInit() {
    this.parentForm.ngSubmit.subscribe(() => this.change());
  }

  refreshClustersQuota() {
    if (this.useSameQuota && this.isMirror) {
      this._clustersQuota = [
        Object.assign({}, this.originClustersQuota[0], {
          quota: this.CalculateQuotaRange(this.originClustersQuota[0].regions),
        }),
      ];
    } else if (this.isMirror) {
      this._clustersQuota = this.originClustersQuota[0].regions;
    } else {
      this._clustersQuota = this.originClustersQuota;
    }

    const defaultQuota = this.environmentService.getDefaultQuota('namespace');

    this.resultClusters = map(
      this._clustersQuota,
      (clusterQuota: ClustersQuota) => {
        const newQuota = { quota: {} };
        if (!!defaultQuota) {
          QUOTA_TYPES.forEach(type => {
            newQuota.quota[type] = clusterQuota.quota
              ? Math.min(
                  defaultQuota[type] || this.getMiniNum(type),
                  clusterQuota.quota[type]['max'] -
                    clusterQuota.quota[type]['used'],
                )
              : defaultQuota[type] || this.getMiniNum(type);
          });
        } else {
          QUOTA_TYPES.forEach(type => {
            newQuota.quota[type] = this.getMiniNum(type);
          });
        }
        return Object.assign({}, clusterQuota, newQuota);
      },
    );
    this.change();
  }

  getMiniNum(type: string) {
    return this.minQuotaRange[type] || 0;
  }

  useSameQuotaChange(value: any) {
    this.useSameQuota = value;
    this.refreshClustersQuota();
  }

  CalculateQuotaRange(clusters: ClustersQuota[]): any {
    let quotaRange = {},
      quotaLimited = false;

    clusters.some(cluster => {
      return (quotaLimited = !!cluster.quota);
    });
    if (!quotaLimited) {
      return (quotaRange = null);
    }

    ['cpu', 'memory', 'storage', 'pvc_num', 'pods'].forEach((v: string) => {
      const numArr = clusters.map(cluster => {
        if (cluster.quota && cluster.quota[v]) {
          return cluster.quota[v]['max'] - cluster.quota[v]['used'];
        }
        return Number.MAX_SAFE_INTEGER;
      });
      quotaRange[v] = {
        used: 0,
        max: Math.min(...numArr),
      };
    });
    return quotaRange;
  }

  change() {
    const completeClusters = this.useSameQuota
      ? this.resultClusters[0].regions.map(c => {
          return Object.assign({}, c, {
            quota: this.dealQuotaWithSuffix(this.resultClusters[0]
              .quota as Quota),
          });
        })
      : this.resultClusters.map((c: ClustersQuota) => {
          return Object.assign({}, c, {
            quota: this.dealQuotaWithSuffix(c.quota as Quota),
          });
        });
    this.propagateChange(completeClusters);
  }

  dealQuotaWithSuffix(quota: Quota) {
    return Object.assign({}, quota, {
      memory: quota.memory + 'G',
      storage: quota.storage + 'G',
    });
  }

  writeValue(obj: any): void {
    if (obj) {
      this.resultClusters = obj;
    }
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  getQuotaErrorInfo(quota: QuotaRange, type: string) {
    if (!quota[type]) {
      return '';
    }
    const remaining = quota[type].max - quota[type].used;
    const limitMin = this.getMiniNum(type);
    return this.translate.get(
      remaining < limitMin ? 'quota_range_error_tip' : 'quota_range_tip',
      {
        min: limitMin,
        max: remaining,
      },
    );
  }

  getQuotaFormItemHint(quota: QuotaRange, type: string) {
    if (!quota[type]) {
      return '';
    }
    const remaining = quota[type].max - quota[type].used;
    const limitMin = this.getMiniNum(type);

    return this.translate.get('quota_range_tip', {
      min: limitMin,
      max: remaining,
    });
  }

  quotaValidator() {
    return (control: AbstractControl): ValidationErrors => {
      return this.hostElement.nativeElement.getElementsByClassName('ng-invalid')
        .length
        ? { rangeError: { current: control.value } }
        : null;
    };
  }

  validate(c: AbstractControl): ValidationErrors {
    return this.quotaValidator()(c);
  }

  registerOnValidatorChange?(_fn: () => void): void {}

  between(_v: string, _min: number, _max: number) {}

  getDisplayMirrorName(_name: string) {}
}
