import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Project } from 'app2/shared/services/features/project.service';
import { AppState, getCurrentProject } from 'app_user/store';
import { Observable } from 'rxjs';
import { filter, publishReplay, refCount } from 'rxjs/operators';

@Component({
  templateUrl: 'project-detail.component.html',
  styleUrls: ['project-detail.component.scss'],
})
export class ProjectDetailPageComponent implements OnInit {
  currentProject$: Observable<Project>;
  initialized = false;
  nav: string;
  constructor(private store: Store<AppState>) {
    this.nav = 'detail';
  }

  async ngOnInit() {
    this.currentProject$ = this.store.select(getCurrentProject).pipe(
      filter(project => !!project),
      publishReplay(1),
      refCount(),
    );
  }
}
