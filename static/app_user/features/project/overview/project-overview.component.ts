import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  NamespaceOption,
  NamespaceService,
  UserNamespace,
} from 'app2/shared/services/features/namespace.service';
import {
  NamespaceQuota,
  Project,
  ProjectService,
} from 'app2/shared/services/features/project.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { Observable, Subject, combineLatest, from, merge } from 'rxjs';
import {
  filter,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { Store } from '@ngrx/store';
import * as fromStore from 'app_user/store';

interface UserNamespaceOption {
  name: string;
  uuid?: string;
  cluster_name: string;
  cluster_uuid?: string;
  cluster_display_name?: string;
  quota?: {
    pvc_num: number;
    storage: number;
    memory: string;
    pods: string;
    cpu: string;
  };
}

interface ClusterOption {
  name: string;
  display_name: string;
  uuid: string;
}

@Component({
  templateUrl: 'project-overview.component.html',
  styleUrls: ['project-overview.component.scss'],
})
export class ProjectOverviewComponent implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<void>();

  projects$: Observable<Project[]>;
  projectsLength$: Observable<number>;
  currentProject$: Observable<Project>;

  currentProject: Project;

  clusters$: Observable<ClusterOption[]>;
  clustersLength$: Observable<number>;
  clusterSelect$ = new Subject<ClusterOption>();

  filteredNamespaces$: Observable<UserNamespaceOption[]>;
  namespacesLength$: Observable<number>;

  currentCluster: ClusterOption;
  initialized: boolean;
  namespaceLoading: boolean;
  search$ = new Subject<string>();
  createNamespaceEnabled: boolean;
  constructor(
    private router: Router,
    private projectService: ProjectService,
    private namespaceService: NamespaceService,
    private roleUtil: RoleUtilitiesService,
    private store: Store<fromStore.AppState>,
  ) {}

  async ngOnInit() {
    this.projects$ = this.store.select(fromStore.getAllProjects).pipe(
      publishReplay(1),
      refCount(),
    );
    this.projectsLength$ = this.projects$.pipe(
      map(projects => projects.length),
    );
    this.currentProject$ = this.store.select(fromStore.getCurrentProject).pipe(
      filter(project => !!project),
      tap((project: Project) => {
        this.currentProject = project;
        this.currentCluster = null;
      }),
      tap(async (project: Project) => {
        this.createNamespaceEnabled = await this.roleUtil.resourceTypeSupportPermissions(
          'namespace',
          { project_name: project.name },
          'create',
        );
      }),
      publishReplay(1),
      refCount(),
    );

    this.clusters$ = this.currentProject$.pipe(
      map((project: Project) => {
        return project.clusters || [];
      }),
    );
    this.clustersLength$ = this.clusters$.pipe(
      map(clusters => clusters.length),
    );

    let namespaces$ = merge(
      this.clusters$.pipe(map(() => null)),
      this.clusterSelect$,
    ).pipe(
      switchMap((cluster: ClusterOption) => from(this.getNamespaces(cluster))),
      publishReplay(1),
      refCount(),
    );
    namespaces$ = merge(this.clusterSelect$.pipe(map(() => [])), namespaces$);
    this.filteredNamespaces$ = combineLatest(
      namespaces$,
      this.search$.pipe(startWith('')),
    ).pipe(
      map(
        ([namespaces, search]) =>
          search
            ? namespaces
                .filter((item: UserNamespaceOption) =>
                  item.name.includes(search.trim()),
                )
                .sort(this.nameCompare)
            : namespaces.sort(this.nameCompare),
      ),
    );
    this.namespacesLength$ = namespaces$.pipe(
      map(namespaces => namespaces.length),
    );
    this.projects$.pipe(takeUntil(this.onDestroy$)).subscribe(() => {
      this.initialized = true;
    });
  }

  nameCompare(ns1: UserNamespaceOption, ns2: UserNamespaceOption) {
    return ns1.name.localeCompare(ns2.name);
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  trackByClusterName(_index: number, cluster: any) {
    return cluster.name;
  }

  clusterSelected(cluster: ClusterOption) {
    if (
      (this.currentCluster &&
        cluster &&
        this.currentCluster.uuid === cluster.uuid) ||
      (!this.currentCluster && !cluster)
    ) {
      return;
    }
    this.clusterSelect$.next(cluster);
    this.currentCluster = cluster;
  }

  trackByNamespaceIndex(index: number) {
    return index;
  }

  enterNamespace(namespace: UserNamespaceOption) {
    this.router.navigate([
      'workspace',
      {
        project: this.currentProject.name,
        cluster: namespace.cluster_name,
        namespace: namespace.name,
      },
    ]);
  }

  namespaceDetail(namespace: UserNamespaceOption, event: Event) {
    event.stopPropagation();
    this.router.navigate([
      'workspace',
      {
        project: this.currentProject.name,
        cluster: namespace.cluster_name,
        namespace: namespace.name,
      },
      'namespace',
    ]);
  }

  private async getNamespaces(
    cluster: ClusterOption,
  ): Promise<UserNamespaceOption[]> {
    this.namespaceLoading = true;
    if (!cluster) {
      // get batch namespaces
      const namespaces: UserNamespace[] = await this.namespaceService.getBatchNamespaceOptions(
        this.currentProject.clusters || [],
        this.currentProject.name,
      );
      const quotas: NamespaceQuota[] = await this.projectService.getProjectNamespaceQuotas(
        this.currentProject.name,
      );
      this.namespaceLoading = false;
      return namespaces.map((item: UserNamespace) => {
        const quota: NamespaceQuota = quotas.find((q: NamespaceQuota) => {
          return q.namespace_uuid === item.uuid;
        });
        return quota
          ? Object.assign(item, {
              quota: quota.quota,
            })
          : item;
      });
    } else {
      // get namespaces in cluster
      const [namespaces, quotas] = await Promise.all([
        this.namespaceService.getNamespaceOptions(cluster.uuid, null),
        this.projectService.getProjectNamespaceQuotas(
          this.currentProject.name,
          cluster.uuid,
        ),
      ]);
      this.namespaceLoading = false;
      return namespaces.map((item: NamespaceOption) => {
        const quota: NamespaceQuota = quotas.find((q: NamespaceQuota) => {
          return q.namespace_uuid === item.uuid;
        });
        return {
          quota: quota ? quota.quota : null,
          name: item.name,
          uuid: item.uuid,
          cluster_name: cluster.name,
          cluster_uuid: cluster.uuid,
          cluster_display_name: cluster.display_name,
        };
      });
    }
  }
}
