import { TemplatePortal } from '@angular/cdk/portal';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { Store } from '@ngrx/store';
import { Project } from 'app2/shared/services/features/project.service';
import {
  TemplateHolderType,
  UiStateService,
} from 'app_user/core/ui-state.service';
import * as fromStore from 'app_user/store';
import { sortBy } from 'lodash';
import { filter, map, tap } from 'rxjs/operators';

@Component({
  templateUrl: 'project.component.html',
  styleUrls: ['../../core/styles/layout.common.scss', 'project.component.scss'],
  providers: [ProjectComponent],
})
export class ProjectComponent implements OnInit, OnDestroy {
  pageHeaderContentTemplate$: Observable<TemplatePortal<any>>;

  projects$: Observable<Project[]>;
  currentProject$: Observable<Project>;

  constructor(
    public uiState: UiStateService,
    private router: Router,
    private activateRoute: ActivatedRoute,
    private store: Store<fromStore.AppState>,
  ) {}

  /**
   * {'project': ''}
   */
  get baseParams(): Observable<Params> {
    return this.activateRoute.params;
  }

  get baseParamsSnapshot(): { project: string } {
    const params = this.activateRoute.snapshot.params;
    return {
      project: params['project'],
    };
  }

  get baseActivatedRoute() {
    return this.activateRoute;
  }

  ngOnInit(): void {
    this.pageHeaderContentTemplate$ = this.uiState.getTemplateHolder(
      TemplateHolderType.PageHeaderContent,
    ).templatePortal$;

    this.initializeProjectState();
  }

  ngOnDestroy() {}

  private initializeProjectState() {
    this.projects$ = this.store.select(fromStore.getAllProjects).pipe(
      map((projects: Project[]) => {
        return sortBy(projects, project => project.name);
      }),
      tap((projects: Project[]) => {
        // check if project name in url really exists
        const project = projects.find(
          item => item.name === this.baseParamsSnapshot['project'],
        );
        if (projects.length && !project) {
          const newProjectName = projects[0]['name'];
          this.router.navigate(['/project', { project: newProjectName }]);
        }
      }),
    );
    this.currentProject$ = this.store
      .select(fromStore.getCurrentProject)
      .pipe(filter(project => !!project));
  }
}
