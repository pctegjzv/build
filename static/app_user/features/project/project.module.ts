import { PortalModule } from '@angular/cdk/portal';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
  LayoutModule as AuiLayoutModule,
  NavModule as AuiNavModule,
} from 'alauda-ui';
import { ProjectDetailComponent } from 'app2/features/projects/detail/project-detail.component';
import { CreateNamespaceComponent } from 'app_user/features/project/create-namespace/create-namespace.component';
import { ProjectDetailPageComponent } from 'app_user/features/project/detail/project-detail.component';
import { ProjectOverviewComponent } from 'app_user/features/project/overview/project-overview.component';
import { ProjectComponent } from 'app_user/features/project/project.component';
import { ProjectRoutingModule } from 'app_user/features/project/project.routing.module';
import { SharedModule } from 'app_user/shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    AuiLayoutModule,
    AuiNavModule,
    RouterModule,
    PortalModule,
    ProjectRoutingModule,
  ],
  declarations: [
    ProjectComponent,
    ProjectOverviewComponent,
    ProjectDetailPageComponent,
    ProjectDetailComponent,
    CreateNamespaceComponent,
  ],
})
export class ProjectModule {}
