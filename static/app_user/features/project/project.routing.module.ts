import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateNamespaceComponent } from 'app_user/features/project/create-namespace/create-namespace.component';
import { ProjectDetailPageComponent } from 'app_user/features/project/detail/project-detail.component';
import { ProjectOverviewComponent } from 'app_user/features/project/overview/project-overview.component';
import { ProjectComponent } from 'app_user/features/project/project.component';

const routes: Routes = [
  {
    path: '',
    component: ProjectComponent,
    children: [
      {
        path: '',
        component: ProjectOverviewComponent,
      },
      {
        path: 'detail',
        component: ProjectDetailPageComponent,
      },
      {
        path: 'create_namespace',
        component: CreateNamespaceComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjectRoutingModule {}
