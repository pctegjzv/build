import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogService, NotificationService } from 'alauda-ui';
import { CreatePvcComponent } from 'app2/features/storage/pvc/create/create-pvc.component';
import { PVC_STATUS_MAP } from 'app2/features/storage/storage.constant';
import {
  Cluster,
  RegionService,
} from 'app2/shared/services/features/region.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import {
  MirrorPvc,
  Pvc,
  StorageService,
} from 'app2/shared/services/features/storage.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateService } from 'app2/translate/translate.service';
import { ResourceList } from 'app_user/core/types';
import {
  BaseResourceListComponent,
  FetchParams,
} from 'app_user/shared/abstract/base-resource-list.component';
import { Observable, Subject, from } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';

import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { MirrorResourceDeleteConfirmComponent } from 'app_user/shared/components/mirror-resource-delete-confirm/mirror-resource-delete-confirm.component';

@Component({
  templateUrl: './pvc-list.component.html',
  styleUrls: ['./pvc-list.component.scss'],
})
export class PvcListComponent extends BaseResourceListComponent<any>
  implements OnInit, OnDestroy {
  statusMap = PVC_STATUS_MAP;
  hasCreatePvcPermission = false;
  pageSize = 20;
  initialized: boolean;

  columns = ['name', 'status', 'namespace', 'size', 'action'];
  private onDestroy$ = new Subject<void>();
  private projectName: string;
  private namespaceName: string;
  private clusterName: string;

  mirrorClusters: {
    name: string;
    display_name: string;
  }[] = [];
  clusters: string[];
  constructor(
    http: HttpClient,
    router: Router,
    activedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    private storageService: StorageService,
    private modal: ModalService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private roleUtil: RoleUtilitiesService,
    private regionService: RegionService,
    private dialogService: DialogService,
    private workspaceComponent: WorkspaceComponent,
  ) {
    super(http, router, activedRoute, cdr);
  }

  async ngOnInit() {
    super.ngOnInit();
    const params = this.workspaceComponent.baseParamsSnapshot;
    this.projectName = params['project'];
    this.clusterName = params['cluster'];
    this.namespaceName = params['namespace'];
    this.hasCreatePvcPermission = await this.roleUtil.resourceTypeSupportPermissions(
      'persistentvolumeclaim',
      {
        project_name: this.projectName,
        namespace_name: this.namespaceName,
        cluster_name: this.clusterName,
      },
      'create',
    );
    this.namespaceChanged(this.namespaceName);

    this.list$.pipe(takeUntil(this.onDestroy$)).subscribe(list => {
      if (list.length) {
        this.initialized = true;
      }
    });
    this.getMirrorClusters();
  }

  fetchResources(params: FetchParams): Observable<ResourceList> {
    return from(
      this.storageService.getPvcs({
        cluster_id: this.clusterName,
        name: params.search,
        page: params.pageParams.pageIndex,
        page_size: params.pageParams.pageSize,
        namespace: this.namespaceName,
      }),
    );
  }

  trackByFn(_index: number, item: Pvc) {
    return item.kubernetes.metadata.uid;
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  async createPvc(updating: boolean = false, pvc: Pvc = null) {
    try {
      const modalRef = await this.modal.open(CreatePvcComponent, {
        title: this.translate.get(
          updating ? 'update_volume' : 'storage_create_volume',
        ),
      });
      modalRef.componentInstance.initRegion(
        {
          id: this.clusterName, // cluster_id_name in kubernetes resource api
          name: this.clusterName,
          namespace: this.namespaceName,
        },
        this.mirrorClusters,
      );
      if (updating) {
        modalRef.componentInstance.initValue(pvc);
      }
      modalRef.componentInstance.finish
        .pipe(first())
        .subscribe((result: boolean) => {
          modalRef.close();
          if (result) {
            if (this.mirrorClusters.length === 1) {
              this.auiNotificationService.success(
                this.translate.get(
                  updating
                    ? 'pvc_update_success'
                    : 'storage_volume_create_success',
                ),
              );
            }
            this.onUpdate(null);
          }
        });
    } catch (error) {
      //
    }
  }

  async update(item: any) {
    await this.createPvc(true, item);
  }

  pageChanged(pageNumber: number) {
    this.onPageEvent({
      pageIndex: pageNumber,
      pageSize: this.pageSize,
    });
  }

  hasPermission(item: any, action: string) {
    return this.roleUtil.resourceHasPermission(
      item,
      'persistentvolumeclaim',
      action,
    );
  }

  private async getMirrorClusters() {
    const cluster: Cluster = await this.regionService.getCluster(
      this.clusterName,
    );
    if (cluster.mirror) {
      this.mirrorClusters = cluster.mirror.regions || [];
    }
  }

  async deletePvc(pvc: Pvc) {
    const mirrorPvcs: MirrorPvc[] = await this.storageService.getMirrorPvcs(
      this.clusterName,
      this.namespaceName,
      pvc.kubernetes.metadata.name,
    );
    if (mirrorPvcs.length) {
      const requireSelect = mirrorPvcs.filter((item: any) => {
        return item.uuid === pvc.kubernetes.metadata.uid;
      });
      const modalRef = await this.dialogService.open(
        MirrorResourceDeleteConfirmComponent,
      );
      const mirrorPvcMeta = {
        displayName: 'cluster_display_name',
        mirrorResource: mirrorPvcs,
        confirmTitle: this.translate.get('storage_delete_volume_title'),
        confirmLabel: this.translate.get('cluster'),
        confirmContent: this.translate.get(
          'storage_delete_selected_cluster_volume_content',
          {
            name: pvc.kubernetes.metadata.name,
          },
        ),
      };
      modalRef.componentInstance.setMirrorResourceMeta(
        mirrorPvcMeta,
        requireSelect,
      );
      modalRef.componentInstance.initialized = true;
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe(async (res: MirrorPvc[]) => {
          modalRef.close();
          if (res) {
            try {
              await this.storageService.deleteMirrorPvc(
                pvc.kubernetes.metadata.name,
                res,
              );
              this.onUpdate(null);
            } catch (err) {
              if (err && err.errors) {
                this.auiNotificationService.error(err.errors[0].message);
              }
            }
          }
        });
    } else {
      try {
        await this.modal.confirm({
          title: this.translate.get('storage_delete_volume_title'),
          content: this.translate.get('storage_delete_volume_content', {
            name: pvc.kubernetes.metadata.name,
          }),
        });
      } catch (e) {
        return false;
      }

      try {
        await this.storageService.deletePvc(
          this.clusterName,
          this.namespaceName,
          pvc.kubernetes.metadata.name,
        );
        this.auiNotificationService.success(
          this.translate.get('storage_volume_delete_success'),
        );
        this.onUpdate(null);
      } catch (err) {
        if (err && err.errors) {
          this.auiNotificationService.error(err.errors[0].message);
        }
      }
    }
  }
}
