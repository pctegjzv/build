import { NgModule } from '@angular/core';
import { CreatePvcComponent } from 'app2/features/storage/pvc/create/create-pvc.component';
import { PvcListComponent } from 'app_user/features/pvc/list/pvc-list.component';
import { PvcRoutingModule } from 'app_user/features/pvc/pvc.routing.module';
import { SharedModule } from 'app_user/shared/shared.module';

@NgModule({
  imports: [SharedModule, PvcRoutingModule],
  declarations: [PvcListComponent, CreatePvcComponent],
  entryComponents: [CreatePvcComponent],
})
export class PvcModule {}
