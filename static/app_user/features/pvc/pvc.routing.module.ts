import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PvcListComponent } from 'app_user/features/pvc/list/pvc-list.component';

const routes: Routes = [
  {
    path: '',
    component: PvcListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PvcRoutingModule {}
