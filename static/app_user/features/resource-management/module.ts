import { NgModule } from '@angular/core';
import { NavModule as AuiNavModule } from 'alauda-ui';
import { ResourceManagementCreatePageComponent } from 'app2/features/rsrc-manage/create-page.component';
import {
  ResourceLabelFormComponent,
  ResourceLabelUniqueValidatorDirective,
} from 'app2/features/rsrc-manage/form/label-form.component';
import { ResourceLabelDialogComponent } from 'app2/features/rsrc-manage/label-dialog/label-dialog.component';
import { ResourceManagementListPageComponent } from 'app2/features/rsrc-manage/list-page.component';
import { ResourceManagementRoutingModule } from 'app_user/features/resource-management/routing';
import { SharedModule } from 'app_user/shared/shared.module';

@NgModule({
  imports: [SharedModule, ResourceManagementRoutingModule, AuiNavModule],
  declarations: [
    ResourceManagementListPageComponent,
    ResourceManagementCreatePageComponent,
    ResourceLabelDialogComponent,
    ResourceLabelFormComponent,
    ResourceLabelUniqueValidatorDirective,
  ],
  entryComponents: [ResourceLabelDialogComponent],
})
export class UserViewResourceManagementModule {}
