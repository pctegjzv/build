import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResourceManagementCreatePageComponent } from 'app2/features/rsrc-manage/create-page.component';
import { ResourceManagementListPageComponent } from 'app2/features/rsrc-manage/list-page.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'list',
  },
  {
    path: 'list',
    component: ResourceManagementListPageComponent,
  },
  {
    path: 'create',
    component: ResourceManagementCreatePageComponent,
  },
  {
    path: 'update',
    component: ResourceManagementCreatePageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResourceManagementRoutingModule {}
