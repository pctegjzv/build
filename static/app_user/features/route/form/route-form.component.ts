import { Location } from '@angular/common';
import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { merge } from 'lodash';

import { DialogService, NotificationService } from 'alauda-ui';
import * as jsyaml from 'js-yaml';

import {
  Domain,
  DomainService,
} from 'app2/shared/services/features/domain.service';
import {
  K8sResourceService,
  K8sService,
  Route,
} from 'app2/shared/services/features/k8s-resource.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { ResourceList } from 'app_user/core/types';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

import {
  K8S_RESOURCE_NAME_BASE,
  K8S_SERVICE_NAME,
} from '../../../../app/components/common/config/common-pattern';

@Component({
  selector: 'rc-route-form',
  templateUrl: 'route-form.component.html',
  styleUrls: ['route-form.component.scss'],
})
export class RouteFormComponent implements OnInit, OnDestroy {
  @Input()
  data: Route;
  private clusterName: string;
  private namespaceName: string;
  private RESOURCE_TYPE = 'routes';
  routeYaml = '';
  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: false,
  };

  routeMeta = {
    domain: '',
    host: '',
    path: '',
  };
  domainList: string[] = [];
  extensiveDomainList: string[] = [];
  kubeServiceList: any[] = [];
  portMap = {};

  route: Route['kubernetes'];
  resourceNameReg = K8S_SERVICE_NAME;
  baseRSReg = K8S_RESOURCE_NAME_BASE;
  submitting = false;
  initialized = false;
  formModel = 'UI';
  domainType = 'domain';
  selector: any;

  @ViewChild('routeForm')
  routeForm: NgForm;
  constructor(
    private translateService: TranslateService,
    private router: Router,
    private location: Location,
    private auiNotificationService: NotificationService,
    private dialogService: DialogService,
    private k8sResourceService: K8sResourceService,
    private errorsToastService: ErrorsToastService,
    private workspaceComponent: WorkspaceComponent,
    private domainService: DomainService,
  ) {
    this.route = this.genDefaultRoute();
  }

  async ngOnInit() {
    const params = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = params['cluster'];
    this.namespaceName = params['namespace'];
    await this.getHostList();
    await this.getKubeServiceList();
    if (this.data && this.data.kubernetes) {
      this.route = this.data.kubernetes;
      this.genRouteForm();
    }
    this.route.metadata.namespace = this.namespaceName;
    this.initialized = true;
  }

  async getHostList() {
    let domainList: ResourceList;
    try {
      domainList = await this.domainService.getDomains(false, {
        page: 1,
        page_size: 1000,
        search: '',
      });
    } catch (err) {
      return;
    }
    domainList.results.map((item: Domain) => {
      if (item.domain.startsWith('*.')) {
        this.extensiveDomainList.push(item.domain.substring(2));
      } else {
        this.domainList.push(item.domain);
      }
    });
  }

  genDefaultRoute() {
    return {
      apiVersion: 'route.openshift.io/v1',
      kind: 'Route',
      metadata: {
        name: '',
        namespace: '',
      },
      spec: {
        host: '',
        path: '',
        port: {
          targetPort: '',
        },
        to: {
          kind: 'Service',
          name: '',
          weight: 100,
        },
        wildcardPolicy: 'None',
      },
    };
  }

  ngOnDestroy() {}

  domainTypeChange() {
    this.routeMeta = {
      domain: '',
      host: '',
      path: '',
    };
  }

  setPort(event: string) {
    this.route.spec.port.targetPort = this.portMap[event][0];
  }

  async confirm() {
    this.routeForm.onSubmit(null);
    if (this.routeForm.invalid) {
      return;
    }

    try {
      let comfirmContent, comfirmTitle;
      if (this.data) {
        comfirmContent = 'update_route_confirm';
        comfirmTitle = 'update';
      } else {
        comfirmContent = 'create_route_confirm';
        comfirmTitle = 'create';
      }
      await this.dialogService.confirm({
        title: this.translateService.get(comfirmTitle),
        content: this.translateService.get(comfirmContent, {
          name: this.route.metadata.name,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
    } catch (e) {
      return;
    }

    this.submitting = true;

    try {
      const payload = this.genRoute();
      let notifyContent;
      if (this.data) {
        // update service
        notifyContent = 'update_route_success';
        await this.k8sResourceService.updateK8sReource(
          this.clusterName,
          this.RESOURCE_TYPE,
          payload,
        );
      } else {
        // create service
        notifyContent = 'create_route_success';
        await this.k8sResourceService.createK8sReource(
          this.clusterName,
          this.RESOURCE_TYPE,
          payload,
        );
      }
      this.auiNotificationService.success({
        title: this.translateService.get('success'),
        content: this.translateService.get(notifyContent, {
          name: payload.metadata.name,
        }),
      });
      this.router.navigate(['route'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    } catch (err) {
      this.errorsToastService.error(err);
    } finally {
      this.submitting = false;
    }
  }

  formModelChange(event: string) {
    switch (event) {
      case 'YAML':
        this.formatRoute();
        this.routeYaml = this.formToYaml(this.route);
        break;
      case 'UI':
        this.route = this.yamlToForm(this.routeYaml);
        this.genRouteForm();
        this.genServiceSelectForm();
        break;
    }
  }

  private formToYaml(formModel: any) {
    try {
      const json = JSON.parse(JSON.stringify(formModel));
      const yaml = jsyaml.safeDump(json);
      return yaml;
    } catch (err) {
      this.errorsToastService.error(err);
    }
  }

  private genRouteForm() {
    if (this.route.spec.path) {
      this.routeMeta.path = this.route.spec.path.substring(1);
    }
    if (this.domainType === 'domain') {
      this.routeMeta.domain = '';
      this.routeMeta.host = this.route.spec.host;
    } else {
      const hostList = this.route.spec.host.split('.');
      this.routeMeta.domain = hostList[0];
      this.routeMeta.host = hostList.slice(1).join('.');
    }
  }

  private genServiceSelectForm() {
    if (this.kubeServiceList.length) {
      if (!this.kubeServiceList.includes(this.route.spec.to.name)) {
        this.route.spec.to.name = this.kubeServiceList[0];
      }
      if (
        !this.portMap[this.route.spec.to.name].includes(
          this.route.spec.port.targetPort,
        )
      ) {
        this.route.spec.port.targetPort = this.portMap[
          this.route.spec.to.name
        ][0];
      }
    }
  }

  private genRoute() {
    if (this.formModel === 'YAML') {
      return this.genRouteFromYaml();
    }
    return this.formatRoute();
  }

  private yamlToForm(yaml: string) {
    let formModel: any;
    formModel = jsyaml.safeLoad(yaml);
    if (!formModel || formModel instanceof String) {
      formModel = {};
    }
    return merge(this.genDefaultRoute(), formModel);
  }

  private genRouteFromYaml() {
    const route: any = jsyaml.safeLoad(this.routeYaml);
    if (route.metadata) {
      route.metadata.namespace = this.namespaceName;
    }
    return route;
  }

  private formatRoute() {
    this.route.spec.path = this.routeMeta.path ? `/${this.routeMeta.path}` : '';
    this.route.spec.host = this.routeMeta.domain
      ? `${this.routeMeta.domain}.${this.routeMeta.host}`
      : this.routeMeta.host;
    return this.route;
  }

  cancel() {
    this.location.back();
  }

  async getKubeServiceList() {
    let k8sServiceList: K8sService[];
    try {
      k8sServiceList = await this.k8sResourceService.getK8sReourceList(
        'services',
        {
          clusterName: this.clusterName,
          namespace: this.namespaceName,
        },
      );
    } catch (err) {
      return;
    }
    k8sServiceList.map((item: K8sService) => {
      this.kubeServiceList.push(item.kubernetes.metadata.name);
      this.portMap[
        item.kubernetes.metadata.name
      ] = item.kubernetes.spec.ports.map((item: any) => {
        return item.name || item.port;
      });
    });
    if (this.kubeServiceList.length) {
      this.route.spec.to.name = this.kubeServiceList[0];
      this.route.spec.port.targetPort = this.portMap[
        this.kubeServiceList[0]
      ][0];
    }
  }
}
