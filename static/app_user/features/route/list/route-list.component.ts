import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject, from } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

import { DialogService, DialogSize, NotificationService } from 'alauda-ui';

import { ViewResourceYamlComponent } from 'app_user/shared/components/view-resource-yaml/view-resource-yaml.component';

import {
  K8sResourceService,
  Route,
} from 'app2/shared/services/features/k8s-resource.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { ResourceList } from 'app_user/core/types';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

import * as jsyaml from 'js-yaml';

import {
  BaseResourceListComponent,
  FetchParams,
} from 'app_user/shared/abstract/base-resource-list.component';

@Component({
  templateUrl: 'route-list.component.html',
  styleUrls: ['route-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RouteListComponent extends BaseResourceListComponent<any>
  implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<void>();
  private RESOURCE_TYPE = 'routes';
  private clusterName: string;
  private namespaceName: string;
  createEnabled: boolean;
  pageSize = 20;
  initialized: boolean;

  columns = ['route_name', 'domain', 'kube_service_name', 'port', 'action'];

  constructor(
    http: HttpClient,
    router: Router,
    activedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    private auiNotificationService: NotificationService,
    private errorsToastService: ErrorsToastService,
    private roleUtil: RoleUtilitiesService,
    private translateService: TranslateService,
    private dialogService: DialogService,
    private k8sResourceService: K8sResourceService,
    private workspaceComponent: WorkspaceComponent,
  ) {
    super(http, router, activedRoute, cdr);
  }

  async ngOnInit() {
    super.ngOnInit();
    const params = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = params['cluster'];
    this.namespaceName = params['namespace'];
    this.createEnabled = await this.roleUtil.resourceTypeSupportPermissions(
      'k8s_others',
      {
        cluster_name: this.clusterName,
        namespace_name: this.namespaceName,
        project_name: params['project'],
      },
      'create',
    );
    this.namespaceChanged(this.namespaceName);

    this.list$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(list => !!list),
      )
      .subscribe(list => {
        if (list.length) {
          this.initialized = true;
        }
      });
  }

  fetchResources(params: FetchParams): Observable<ResourceList> {
    return from(
      this.k8sResourceService.getK8sReourceList(this.RESOURCE_TYPE, {
        clusterName: this.clusterName,
        namespace: this.namespaceName,
        name: params.search,
        page: params.pageParams.pageIndex,
        page_size: params.pageParams.pageSize,
      }),
    );
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  trackByFn(_index: number, item: Route) {
    return item.kubernetes.metadata.uid;
  }

  createService() {
    this.router.navigate(['route', 'create'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  pageChanged(page: number) {
    this.onPageEvent({
      pageIndex: page,
      pageSize: this.pageSize,
    });
  }

  canShowUpdate(item: Route) {
    return this.roleUtil.resourceHasPermission(item, 'k8s_others', 'update');
  }

  canShowView(item: Route) {
    return this.roleUtil.resourceHasPermission(item, 'k8s_others', 'view');
  }

  canShowDelete(item: Route) {
    return this.roleUtil.resourceHasPermission(item, 'k8s_others', 'delete');
  }

  update(item: Route) {
    this.router.navigate(['route', 'update', item.kubernetes.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  viewYaml(item: Route) {
    try {
      const yaml = jsyaml.safeDump(item.kubernetes);
      this.dialogService.open(ViewResourceYamlComponent, {
        size: DialogSize.Big,
        data: {
          yaml: yaml,
        },
      });
    } catch (e) {
      this.errorsToastService.error(e);
    }
  }

  async delete(item: Route) {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('delete'),
        content: this.translateService.get('delete_route_confirm', {
          name: item.kubernetes.metadata.name,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
    } catch (e) {
      return;
    }
    try {
      await this.k8sResourceService.deleteK8sReource(
        this.clusterName,
        this.RESOURCE_TYPE,
        item.kubernetes,
      );
      this.auiNotificationService.success({
        title: this.translateService.get('success'),
        content: this.translateService.get('delete_route_success', {
          name: item.kubernetes.metadata.name,
        }),
      });
      this.onUpdate(null);
    } catch (e) {
      this.errorsToastService.error(e);
    }
  }
}
