import { NgModule } from '@angular/core';
import { RouteCreateComponent } from 'app_user/features/route/create/route-create.component';
import { RouteFormComponent } from 'app_user/features/route/form/route-form.component';
import { RouteListComponent } from 'app_user/features/route/list/route-list.component';
import { RouteRoutingModule } from 'app_user/features/route/route.routing.module';
import { RouteUpdateComponent } from 'app_user/features/route/update/route-update.component';
import { SharedModule } from 'app_user/shared/shared.module';

@NgModule({
  imports: [SharedModule, RouteRoutingModule],
  declarations: [
    RouteListComponent,
    RouteFormComponent,
    RouteCreateComponent,
    RouteUpdateComponent,
  ],
  entryComponents: [],
})
export class RouteModule {}
