import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RouteCreateComponent } from 'app_user/features/route/create/route-create.component';
import { RouteListComponent } from 'app_user/features/route/list/route-list.component';
import { RouteUpdateComponent } from 'app_user/features/route/update/route-update.component';

const routes: Routes = [
  {
    path: '',
    component: RouteListComponent,
  },
  {
    path: 'create',
    component: RouteCreateComponent,
  },
  {
    path: 'update/:name',
    component: RouteUpdateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RouteRoutingModule {}
