import { Component, ViewChild, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, NgForm } from '@angular/forms';
import { BaseItemsFieldsetComponent } from 'app_user/shared/abstract/base-items-fieldset.component';

export interface ServicePort {
  indexId?: number;
  name: string;
  protocol: string;
  port: number | '';
  targetPort: number | '';
}

@Component({
  selector: 'rc-service-ports-fieldset',
  templateUrl: './service-ports-fieldset.component.html',
  styleUrls: ['../../../styles/table-list-fieldset.styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ServicePortsFieldsetComponent),
      multi: true,
    },
  ],
})
export class ServicePortsFieldsetComponent extends BaseItemsFieldsetComponent {
  value: ServicePort[] = [];
  defaultValue: ServicePort = {
    name: '',
    protocol: 'TCP',
    port: '',
    targetPort: '',
  };
  numberReg = {
    pattern: /^-?\d+$/,
    tip: 'integer_pattern',
  };

  // overwrite parent class
  @ViewChild('servicePortForm')
  form: NgForm;

  constructor() {
    super();
  }

  getName(item: ServicePort) {
    item.name = `${item.port}-${item.targetPort}`;
    return item.name;
  }
}
