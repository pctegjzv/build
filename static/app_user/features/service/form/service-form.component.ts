import { Location } from '@angular/common';
import {
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { DialogService, NotificationService } from 'alauda-ui';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import {
  AppService,
  Application,
} from 'app2/shared/services/features/app.service';
import {
  K8sResourceService,
  K8sService,
} from 'app2/shared/services/features/k8s-resource.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import {
  ServicePort,
  ServicePortsFieldsetComponent,
} from 'app_user/features/service/fieldset/service-ports-fieldset.component';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import * as jsyaml from 'js-yaml';
import { cloneDeep, merge } from 'lodash';

import { K8S_SERVICE_NAME } from '../../../../app/components/common/config/common-pattern';

@Component({
  selector: 'rc-service-form',
  templateUrl: './service-form.component.html',
  styleUrls: ['./service-form.component.scss'],
})
export class ServiceFormComponent implements OnInit, OnDestroy {
  @Input()
  data: K8sService;
  private namespaceName: string;
  private clusterName: string;
  private RESOURCE_TYPE = 'services';
  private TCP_SERVICE_NAME_PREFIX = 'service';
  resourceNameReg = K8S_SERVICE_NAME;
  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: false,
  };

  submitting = false;
  initialized = false;
  formModel = 'UI';
  typeList = [
    {
      key: 'HTTP',
      value: 'ClusterIP',
    },
    {
      key: 'TCP',
      value: 'NodePort',
    },
  ];
  appSelector: {
    name: string;
    component_name: string;
  };
  k8sService: K8sService['kubernetes'];
  appList: {
    name: string;
    component_name: string;
  }[] = [];
  serviceYaml: string;

  @ViewChild('serviceForm')
  serviceForm: NgForm;
  @ViewChild('servicePorts')
  portField: ServicePortsFieldsetComponent;

  get nameKey() {
    return `service.${this.env.label_base_domain}/name`;
  }

  constructor(
    private translateService: TranslateService,
    private router: Router,
    private location: Location,
    private auiNotificationService: NotificationService,
    private dialogService: DialogService,
    private k8sResourceService: K8sResourceService,
    private appService: AppService,
    private errorsToastService: ErrorsToastService,
    private workspaceComponent: WorkspaceComponent,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {
    this.k8sService = this.genDefaultService();
  }

  async ngOnInit() {
    const params = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = params['cluster'];
    this.namespaceName = params['namespace'];
    await this.getAppList(this.clusterName, this.namespaceName);
    if (this.data && this.data.kubernetes) {
      const serviceDefinition = this.data.kubernetes;
      this.genAppSelector(serviceDefinition);
      this.k8sService.spec.ports = serviceDefinition.spec.ports;
      this.k8sService.spec.type = serviceDefinition.spec.type;
      this.k8sService.metadata.name = serviceDefinition.metadata.name;
      if (serviceDefinition.metadata.labels) {
        this.k8sService.metadata.labels = serviceDefinition.metadata.labels;
      }
      this.k8sService.metadata.resourceVersion =
        serviceDefinition.metadata.resourceVersion;
      this.k8sService.spec.clusterIP = serviceDefinition.spec.clusterIP;
    }
    this.k8sService.metadata.namespace = this.namespaceName;
    this.initialized = true;
  }

  ngOnDestroy() {}

  genDefaultService() {
    const requirePort: ServicePort = {
      protocol: 'TCP',
      name: '',
      port: '',
      targetPort: '',
    };
    return {
      apiVersion: 'v1',
      kind: 'Service',
      metadata: {
        name: '',
        namespace: '',
      },
      spec: {
        type: 'ClusterIP',
        ports: [requirePort],
      },
    };
  }

  async confirm() {
    this.triggerSubmit();
    if (!this.isValid()) {
      return;
    }

    try {
      let comfirmContent, comfirmTitle;
      if (this.data) {
        comfirmContent = 'update_kube_service_confirm';
        comfirmTitle = 'update';
      } else {
        comfirmContent = 'create_kube_service_confirm';
        comfirmTitle = 'create';
      }
      await this.dialogService.confirm({
        title: this.translateService.get(comfirmTitle),
        content: this.translateService.get(comfirmContent, {
          name: this.k8sService.metadata.name,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
    } catch (e) {
      return;
    }

    this.submitting = true;

    try {
      const payload: any = this.genPayload();
      let notifyContent;
      if (this.data) {
        // update service
        notifyContent = 'update_kube_service_success';
        await this.k8sResourceService.updateK8sReource(
          this.clusterName,
          this.RESOURCE_TYPE,
          payload,
        );
      } else {
        // create service
        notifyContent = 'create_kube_service_success';
        await this.k8sResourceService.createK8sReource(
          this.clusterName,
          this.RESOURCE_TYPE,
          payload,
        );
      }
      this.auiNotificationService.success({
        title: this.translateService.get('success'),
        content: this.translateService.get(notifyContent, {
          name: payload.metadata.name || '',
        }),
      });
      this.router.navigate(['service'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    } catch (err) {
      this.errorsToastService.error(err);
    } finally {
      this.submitting = false;
    }
  }

  formModelChange(event: string) {
    switch (event) {
      case 'YAML':
        this.serviceYaml = this.formToYaml(this.formatK8sService());
        break;
      case 'UI':
        const k8sService = this.yamlToForm(this.serviceYaml);
        this.k8sService = k8sService;
        this.genAppSelector(this.k8sService);
        break;
    }
  }

  private formToYaml(formModel: any) {
    try {
      const json = JSON.parse(JSON.stringify(formModel));
      const yaml = jsyaml.safeDump(json);
      return yaml;
    } catch (err) {
      this.errorsToastService.error(err);
    }
  }

  private yamlToForm(yaml: string) {
    let formModel: any;
    formModel = jsyaml.safeLoad(yaml);
    if (!formModel || formModel instanceof String) {
      formModel = {};
    }
    return merge(this.genDefaultService(), formModel);
  }

  private genAppSelector(serviceDefinition: K8sService['kubernetes']) {
    if (
      serviceDefinition.spec.selector &&
      serviceDefinition.spec.selector[this.nameKey]
    ) {
      this.appSelector = this.appList.find((item: any) => {
        return (
          item.component_name === serviceDefinition.spec.selector[this.nameKey]
        );
      });
    }
  }

  genPayload() {
    if (this.formModel === 'YAML') {
      return this.genK8sServiceFromYaml();
    }
    return this.formatK8sService();
  }

  genK8sServiceFromYaml() {
    const k8sService: any = jsyaml.safeLoad(this.serviceYaml);
    if (k8sService.metadata) {
      k8sService.metadata.namespace = this.namespaceName;
      if (k8sService.spec.type === 'NodePort' && !this.data) {
        delete k8sService.metadata.name;
        k8sService.metadata['name'] =
          this.TCP_SERVICE_NAME_PREFIX + new Date().getTime();
      }
    }
    return k8sService;
  }

  formatK8sService() {
    const k8sService = cloneDeep(this.k8sService);
    if (!this.data && k8sService.spec.type === 'NodePort') {
      k8sService.metadata['name'] =
        this.TCP_SERVICE_NAME_PREFIX + new Date().getTime();
    }
    if (this.appSelector) {
      k8sService.spec['selector'] = {
        [this.nameKey]: this.appSelector.component_name,
      };
    } else {
      delete k8sService.spec.selector;
    }
    k8sService.spec.ports.forEach((item: ServicePort) => {
      delete item.indexId;
    });
    return k8sService;
  }

  cancel() {
    this.location.back();
  }

  triggerSubmit() {
    if (this.formModel === 'UI') {
      this.portField.triggerSubmit();
    }
    this.serviceForm.onSubmit(null);
  }

  isValid() {
    if (this.formModel === 'UI') {
      return this.serviceForm.valid && this.portField.isValid();
    }
    return this.serviceForm.valid;
  }

  async getAppList(clusterName: string, namespaceName: string) {
    try {
      const res = await this.appService.getK8sAppsCount(clusterName);
      const appList = await this.appService.getK8sApps({
        pageSize: res.count,
        params: {
          cluster: clusterName,
          namespace: namespaceName,
        },
      });
      this.appList = appList.results.map((item: Application) => {
        if (item.services[0]) {
          const component = item.services[0].resource;
          return {
            name: item.resource.name,
            component_name: component.name,
          };
        }
      });
    } catch (err) {
      return;
    }
  }
}
