import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject, from } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { DialogService, DialogSize, NotificationService } from 'alauda-ui';

import { ViewResourceYamlComponent } from 'app_user/shared/components/view-resource-yaml/view-resource-yaml.component';

import {
  K8sResourceService,
  K8sService,
  K8sServiceSpecTypeMap,
} from 'app2/shared/services/features/k8s-resource.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';
import { ResourceList } from 'app_user/core/types';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

import * as jsyaml from 'js-yaml';

import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import {
  BaseResourceListComponent,
  FetchParams,
} from 'app_user/shared/abstract/base-resource-list.component';

@Component({
  templateUrl: './service-list.component.html',
  styleUrls: ['./service-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceListComponent extends BaseResourceListComponent<any>
  implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<any>();
  private RESOURCE_TYPE = 'services';
  serviceCreateEnabled: boolean;
  pageSize = 20;
  initialized: boolean;
  keyword = ''; //search keyword

  K8sServiceSpecTypeMap = K8sServiceSpecTypeMap;
  columns = ['name', 'type', 'ip_address', 'target_app', 'port', 'action'];
  private clusterName: string;
  private namespaceName: string;

  get nameKey() {
    return `service.${this.env.label_base_domain}/name`;
  }

  constructor(
    http: HttpClient,
    router: Router,
    activedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    private auiNotificationService: NotificationService,
    private errorsToastService: ErrorsToastService,
    private roleUtil: RoleUtilitiesService,
    private translateService: TranslateService,
    private dialogService: DialogService,
    private k8sResourceService: K8sResourceService,
    private workspaceComponent: WorkspaceComponent,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {
    super(http, router, activedRoute, cdr);
  }

  async ngOnInit() {
    super.ngOnInit();
    const params = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = params['cluster'];
    this.namespaceName = params['namespace'];
    this.serviceCreateEnabled = await this.roleUtil.resourceTypeSupportPermissions(
      'service',
      {
        cluster_name: this.clusterName,
        namespace: this.namespaceName,
        project_name: params['project'],
      },
      'create',
    );
    this.namespaceChanged(this.namespaceName);

    this.list$.pipe(takeUntil(this.onDestroy$)).subscribe(list => {
      if (list.length) {
        this.initialized = true;
      }
    });
  }

  fetchResources(params: FetchParams): Observable<ResourceList> {
    return from(
      this.k8sResourceService.getK8sReourceList(this.RESOURCE_TYPE, {
        clusterName: this.clusterName,
        namespace: this.namespaceName,
        name: params.search,
        page: params.pageParams.pageIndex,
        page_size: params.pageParams.pageSize,
      }),
    );
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  trackByFn(_index: number, item: K8sService) {
    return item.kubernetes.metadata.uid;
  }

  createService() {
    this.router.navigate(['service', 'create'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  pageChanged(page: number) {
    this.onPageEvent({
      pageIndex: page,
      pageSize: this.pageSize,
    });
  }

  getTargetAppName(item: K8sService) {
    if (
      item.kubernetes.spec.selector &&
      item.kubernetes.spec.selector[this.nameKey]
    ) {
      return item.kubernetes.spec.selector[this.nameKey];
    }
    return '';
  }

  canShowUpdate(item: K8sService) {
    return this.roleUtil.resourceHasPermission(item, 'service', 'update');
  }

  canShowView(item: K8sService) {
    return this.roleUtil.resourceHasPermission(item, 'service', 'view');
  }

  canShowDelete(item: K8sService) {
    return this.roleUtil.resourceHasPermission(item, 'service', 'delete');
  }

  update(item: K8sService) {
    this.router.navigate(['service', 'update', item.kubernetes.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  viewYaml(item: K8sService) {
    try {
      const yaml = jsyaml.safeDump(item.kubernetes);
      this.dialogService.open(ViewResourceYamlComponent, {
        size: DialogSize.Big,
        data: {
          yaml: yaml,
        },
      });
    } catch (e) {
      this.errorsToastService.error(e);
    }
  }

  async delete(item: K8sService) {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('delete'),
        content: this.translateService.get('delete_kube_service_confirm', {
          name: item.kubernetes.metadata.name,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
    } catch (e) {
      return;
    }
    try {
      await this.k8sResourceService.deleteK8sReource(
        this.clusterName,
        this.RESOURCE_TYPE,
        item.kubernetes,
      );
      this.auiNotificationService.success({
        title: this.translateService.get('success'),
        content: this.translateService.get('delete_kube_service_success', {
          name: item.kubernetes.metadata.name,
        }),
      });
      this.onUpdate(null);
    } catch (e) {
      this.errorsToastService.error(e);
    }
  }
}
