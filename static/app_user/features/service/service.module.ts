import { NgModule } from '@angular/core';
import { ServiceCreateComponent } from 'app_user/features/service/create/service-create.component';
import { ServicePortsFieldsetComponent } from 'app_user/features/service/fieldset/service-ports-fieldset.component';
import { ServiceFormComponent } from 'app_user/features/service/form/service-form.component';
import { ServiceListComponent } from 'app_user/features/service/list/service-list.component';
import { ServiceRoutingModule } from 'app_user/features/service/service.routing.module';
import { ServiceUpdateComponent } from 'app_user/features/service/update/service-update.component';
import { SharedModule } from 'app_user/shared/shared.module';

@NgModule({
  imports: [SharedModule, ServiceRoutingModule],
  declarations: [
    ServiceListComponent,
    ServiceFormComponent,
    ServiceCreateComponent,
    ServiceUpdateComponent,
    ServicePortsFieldsetComponent,
  ],
  entryComponents: [],
})
export class ServiceModule {}
