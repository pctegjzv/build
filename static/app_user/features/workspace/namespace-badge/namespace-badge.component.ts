import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChange,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { TooltipDirective } from 'alauda-ui';
import { Observable, Subject, combineLatest, from as fromPromise } from 'rxjs';
import { map, publishReplay, refCount, startWith, tap } from 'rxjs/operators';

import {
  NamespaceService,
  UserNamespace,
} from 'app2/shared/services/features/namespace.service';
import { Project } from 'app2/shared/services/features/project.service';
import { groupBy } from 'lodash';

@Component({
  selector: 'rc-namespace-badge',
  styleUrls: ['namespace-badge.component.scss'],
  templateUrl: 'namespace-badge.component.html',
})
export class NamespaceBadgeComponent implements OnInit, OnChanges {
  @Input()
  currentProject: Project;
  @Input()
  clusterName: string;
  @Input()
  namespaceName: string;
  @ViewChild('namespaceTooltip', { read: TooltipDirective })
  namespaceTooltip: TooltipDirective;

  active: boolean;
  search$ = new Subject<string>();
  namespaces$: Observable<UserNamespace[]>;
  namespacesLength$: Observable<number>;

  selectedNamespace: UserNamespace;

  constructor(
    private namespaceService: NamespaceService,
    private router: Router,
  ) {}

  async ngOnInit() {}

  ngOnChanges({ currentProject }: { currentProject: SimpleChange }) {
    if (currentProject && currentProject.currentValue) {
      const project = currentProject.currentValue;
      const namespaces$ = fromPromise(
        this.namespaceService.getBatchNamespaceOptions(
          project.clusters || [],
          project.name,
        ),
      ).pipe(
        tap((namespaces: UserNamespace[]) => {
          if (!namespaces.length) {
            this.router.navigate([
              '/project',
              {
                project: project.name,
              },
            ]);
          }
          const clusterName: string = this.clusterName;
          const namespaceName: string = this.namespaceName;
          this.selectedNamespace = namespaces.find(item => {
            return (
              item.name === namespaceName && item.cluster_name === clusterName
            );
          });
          if (!this.selectedNamespace) {
            this.router.navigate([
              '/project',
              {
                project: project.name,
              },
            ]);
          }
        }),
        publishReplay(1),
        refCount(),
      );
      this.namespacesLength$ = namespaces$.pipe(
        map(namespaces => namespaces.length),
      );
      this.namespaces$ = combineLatest(
        namespaces$,
        this.search$.pipe(startWith('')),
      ).pipe(
        map(([namespaces, search]) => {
          return search
            ? this.getGroupedNamespaces(
                namespaces.filter(
                  namespace =>
                    namespace.name.includes(search) ||
                    namespace.cluster_display_name.includes(search),
                ),
              )
            : this.getGroupedNamespaces(namespaces);
        }),
        publishReplay(1),
        refCount(),
      );
    }
  }

  onNamespaceSelected(namespace: UserNamespace) {
    this.selectedNamespace = namespace;
    this.namespaceTooltip.disposeTooltip();
    this.router.navigate([
      'workspace',
      {
        project: this.currentProject.name,
        cluster: namespace.cluster_name,
        namespace: namespace.name,
      },
    ]);
  }

  showNamespaceDetail(namespace: UserNamespace, event: Event) {
    event.stopPropagation();
    this.namespaceTooltip.disposeTooltip();
    this.router.navigate([
      'workspace',
      {
        project: this.currentProject.name,
        cluster: namespace.cluster_name,
        namespace: namespace.name,
      },
      'namespace',
    ]);
  }

  trackByIndex(index: number) {
    return index;
  }

  private getGroupedNamespaces(namespaces: UserNamespace[]): any[] {
    const res = groupBy(namespaces, (item: UserNamespace) => {
      return item.cluster_display_name;
    });
    return Object.keys(res).map((cluster_display_name: string) => {
      return {
        cluster_display_name,
        namespaces: res[cluster_display_name],
      };
    });
  }
}
