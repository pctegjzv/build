import { TemplatePortal } from '@angular/cdk/portal';
import {
  Component,
  Inject,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute, NavigationEnd, Params, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import * as fromStore from 'app_user/store';
import { sortBy } from 'lodash';
import { Observable, Subject } from 'rxjs';
import {
  debounceTime,
  filter,
  map,
  startWith,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';

import { Project } from 'app2/shared/services/features/project.service';
import { NAV_CONFIG } from 'app_user/core/tokens';
import { NavItem } from 'app_user/core/types';
import {
  TemplateHolderType,
  UiStateService,
} from 'app_user/core/ui-state.service';
import { NavItemHelperDirective } from 'app_user/features/workspace/active-nav-item-helper.directive';
@Component({
  styleUrls: [
    '../../core/styles/layout.common.scss',
    'workspace.component.scss',
  ],
  templateUrl: 'workspace.component.html',
  providers: [WorkspaceComponent],
})
export class WorkspaceComponent implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<void>();
  initialized = false;
  pageHeaderContentTemplate$: Observable<TemplatePortal<any>>;

  // Default nav config
  navConfig$: Observable<NavItem[]>;

  // TODO: For now item expansion is only related to user clicks.
  expandedPath = '';
  @ViewChildren(NavItemHelperDirective)
  private navItemHelpers: QueryList<NavItemHelperDirective>;

  projects$: Observable<Project[]>;
  currentProject$: Observable<Project>;
  currentProjectDetail$: Observable<Project>;

  constructor(
    @Inject(NAV_CONFIG) private navConfig: NavItem[],
    @Inject(ENVIRONMENTS) private environments: Environments,
    private uiState: UiStateService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private store: Store<fromStore.AppState>,
  ) {}

  /**
   * {project: '', 'cluster': '', namespace: ''}
   */
  get baseParams(): Observable<Params> {
    return this.activatedRoute.params;
  }

  get baseParamsSnapshot(): {
    project: string;
    cluster: string;
    namespace: string;
  } {
    const params = this.activatedRoute.snapshot.params;
    return {
      project: params['project'],
      cluster: params['cluster'],
      namespace: params['namespace'],
    };
  }

  get baseActivatedRoute(): ActivatedRoute {
    return this.activatedRoute;
  }

  ngOnInit(): void {
    this.navConfig$ = new Observable(o => {
      o.next(this.processNavItemsWithPremission(this.navConfig));
    });

    this.pageHeaderContentTemplate$ = this.uiState.getTemplateHolder(
      TemplateHolderType.PageHeaderContent,
    ).templatePortal$;

    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        startWith({}),
        debounceTime(50),
      )
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(() => {
        setTimeout(() => {
          this.uiState.setActiveItemInfo(this.getActiveNavItemInfo());
          this.initialized = true;
        });
      });

    this.projects$ = this.store.select(fromStore.getAllProjects).pipe(
      map((projects: Project[]) => {
        return sortBy(projects, project => project.name);
      }),
      tap((projects: Project[]) => {
        // check if project name in url really exists
        const project = projects.find(
          item => item.name === this.baseParamsSnapshot['project'],
        );
        if (projects.length && !project) {
          const newProjectName = projects[0]['name'];
          this.router.navigate(['/project', { project: newProjectName }]);
        }
      }),
    );
    this.currentProject$ = this.store
      .select(fromStore.getCurrentProject)
      .pipe(filter(project => !!project));
    this.currentProjectDetail$ = this.store
      .select(fromStore.getCurrentProjectDetail)
      .pipe(filter(project => !!project));
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  getNavItemRouterLink(path: string): any[] {
    const params = this.baseParamsSnapshot;
    return [
      '/workspace',
      {
        project: params['project'],
        cluster: params['cluster'],
        namespace: params['namespace'],
      },
      path,
    ];
  }

  onPrimaryNavItemClick(path: string) {
    this.expandedPath = this.expandedPath === path ? '' : path;
  }

  onSubNavItemClick(_paths: string[]) {
    // TODO: router should handle sub item paths
  }

  isPathExpanded(path: string, routerActive: boolean) {
    return this.expandedPath === path || routerActive;
  }

  getNavItemHref(item: NavItem) {
    let href = item.href;
    if (href && href.startsWith('env:')) {
      const [, env] = href.split('env:');
      href = this.environments[env] || '';
    }
    return href;
  }

  private processNavItemsWithPremission(items: NavItem[]) {
    // TODO: process nav items with permission
    return items;
  }

  private getActiveNavItemInfo() {
    const activeNavItemHelpers = this.navItemHelpers.filter(
      item => item.active,
    );

    return activeNavItemHelpers.map(helper => ({
      label: helper.label,
      routerLink: helper.routerLink && (helper.routerLink as any).commands,
    }));
  }
}
