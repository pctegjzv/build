import { PortalModule } from '@angular/cdk/portal';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
  LayoutModule as AuiLayoutModule,
  NavModule as AuiNavModule,
} from 'alauda-ui';
// import { WorkspaceRoutingModule } from 'app_user/features/workspace/workspace.routing.module';
import { NavItemHelperDirective } from 'app_user/features/workspace/active-nav-item-helper.directive';
import { HomeComponent } from 'app_user/features/workspace/home.component';
import { NamespaceBadgeComponent } from 'app_user/features/workspace/namespace-badge/namespace-badge.component';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { WorkspaceRoutingModule } from 'app_user/features/workspace/workspace.routing.module';
import { SharedModule } from 'app_user/shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    AuiLayoutModule,
    AuiNavModule,
    RouterModule,
    PortalModule,
    WorkspaceRoutingModule,
  ],
  exports: [WorkspaceComponent],
  declarations: [
    WorkspaceComponent,
    HomeComponent,
    NavItemHelperDirective,
    NamespaceBadgeComponent,
  ],
})
export class WorkspaceModule {}
