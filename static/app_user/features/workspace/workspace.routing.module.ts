import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from 'app_user/features/workspace/home.component';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

const routes: Routes = [
  {
    path: '',
    component: WorkspaceComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: HomeComponent,
      },
      {
        path: 'namespace',
        loadChildren: '../namespace/namespace.module#NamespaceModule',
      },
      {
        path: 'app',
        loadChildren: '../application/application.module#ApplicationModule',
      },
      {
        path: 'configmap',
        loadChildren: '../configmap/configmap.module#ConfigmapModule',
      },
      {
        path: 'pvc',
        loadChildren: '../pvc/pvc.module#PvcModule',
      },
      {
        path: 'service',
        loadChildren: '../service/service.module#ServiceModule',
      },
      {
        path: 'route',
        loadChildren: '../route/route.module#RouteModule',
      },
      {
        path: 'resource_management',
        loadChildren:
          '../resource-management/module#UserViewResourceManagementModule',
      },
      // {
      //   path: 'app_catalog',
      //   loadChildren:
      //     'app_user/features/app-catalog/app-catalog.module#AppCatalogModule',
      // },
      // {
      //   path: 'platform-middleware',
      //   loadChildren:
      //     'app_user/features/app-platform/platform-middleware.module#AppPlatformMiddlewareModule',
      // },
      // {
      //   path: 'platform-micro_service',
      //   loadChildren:
      //     'app_user/features/app-platform/platform-micro_service.module#AppPlatformMicroServiceModule',
      // },
      // {
      //   path: 'platform-big_data',
      //   loadChildren:
      //     'app_user/features/app-platform/platform-big_data.module#AppPlatformBigDataModule',
      // },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorkspaceRoutingModule {}
