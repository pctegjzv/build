import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { CookieService } from 'app2/shared/services/utility/cookie.service';

import * as fromStore from '../store';
@Injectable()
export class ProjectGuard implements CanActivate {
  constructor(
    private cookieService: CookieService,
    private router: Router,
    private store: Store<fromStore.AppState>,
  ) {}

  canActivate(route: ActivatedRouteSnapshot) {
    const projectName = route.params['project'];
    if (projectName) {
      this.cookieService.setCookie('project', projectName);
      this.store.dispatch(new fromStore.LoadProjectDetail(projectName));
    } else {
      const projectNameInCookie = this.cookieService.getCookie('project');
      if (!!projectNameInCookie) {
        this.router.navigate(['/project', { project: projectNameInCookie }]);
        return false;
      }
    }
    return true;
  }
}
