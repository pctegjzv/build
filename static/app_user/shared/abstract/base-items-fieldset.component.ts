import { OnInit } from '@angular/core';
import { ControlValueAccessor, NgForm } from '@angular/forms';
import { remove } from 'lodash';

export class BaseItemsFieldsetComponent
  implements OnInit, ControlValueAccessor {
  value: any[] = [];
  indexId = 0;
  form: NgForm;
  defaultValue = {};

  constructor() {}

  ngOnInit() {}

  addRow() {
    this.value.push({
      indexId: this.indexId++,
      ...this.defaultValue,
    });
    this.emitToModel();
  }

  removeRow(indexId: number) {
    remove(this.value, (item: any) => {
      return indexId === item.indexId;
    });
    this.emitToModel();
  }

  trackByIndex(_index: number, item: any) {
    return item.indexId;
  }

  triggerSubmit() {
    this.form.onSubmit(null);
  }

  isValid() {
    return this.form.valid;
  }

  // emit _value to ngModel
  private emitToModel() {
    this.onTouchedCallback();
    this.onChangeCallback(this.value);
  }

  /** Following Methods Implements ControlValueAccessor **/

  writeValue(value: any) {
    if (value) {
      value.forEach((item: any) => {
        item.indexId = this.indexId++;
      });
    }
    this.value = value;
    this.emitToModel();
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}
