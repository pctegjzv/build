import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectorRef, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { debounce } from 'lodash';
import { BehaviorSubject, Observable, combineLatest, of } from 'rxjs';
import {
  catchError,
  debounceTime,
  filter,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';

import {
  ListMeta,
  PageParams,
  Resource,
  ResourceList,
  isHttpErrorResponse,
  isNotHttpErrorResponse,
} from 'app_user/core/types';

export interface FetchParams {
  namespace: string;
  search: string;
  pageParams: PageParams;
}

export interface TableState {
  namespace?: string;
  search?: string;

  // page params
  pageSize?: number;
  pageIndex?: number;
}

/**
 * Base for resource list. Note, unlike simple resource list, this normally requires
 * the user to feed standalone fetch methods.
 *
 * Standard unidirection data flow:
 * [Action](setTableState)
 * ↓↓↓
 * [Table State](construct fetch params)
 * ↓↓↓
 * [Fetch Params](fetchResources)
 * ↓↓↓
 * [Fetch Observable](map)
 * ↓↓↓
 * [Resource List]
 *
 * Typically, every fetch is made through setTableState. A fetch will land if
 * tableState$ emits a new state.
 */
export abstract class BaseResourceListComponent<R extends Resource>
  implements OnInit {
  private defaultPageIndex = 1;
  private defaultPageSize = 20;
  private currentPageIndex = 1;
  protected poll$ = new BehaviorSubject(null);
  protected debouncedUpdate: () => void;

  updated$ = new BehaviorSubject(null);
  fetchParams$: Observable<FetchParams>;

  rawResponse$: Observable<ResourceList | HttpErrorResponse>;
  list$: Observable<R[]>;
  listMeta$: Observable<ListMeta>;

  // List of fetch related params. They should be read only most of the time.
  pageParams$: Observable<PageParams>;
  search$: Observable<string>;
  namespace$: Observable<string>;

  fetching = false;
  searching = false;
  showZeroState = true;

  /**
   * Change this if you want to change poll timing.
   */
  pollInterval = 10000;

  /**
   * The fetch method to be called upon various parameters.
   */
  abstract fetchResources(
    params: FetchParams,
  ): Observable<ResourceList | Error>;

  /**
   * Map API result into Resource list
   */
  map(value: ResourceList): any[] {
    return value.results;
  }

  /**
   * Jump to the previous page if there is no data and not the first page
   */
  emptyListCheck(value: ResourceList) {
    if (!value.results.length && this.currentPageIndex > 1) {
      this.setTableState({ pageIndex: --this.currentPageIndex });
    }
  }
  /**
   * The source of the table's state.
   * One particular usage is the activatedRoute's params.
   */
  getTableState$() {
    return this.activedRoute.queryParams.pipe(
      startWith(this.activedRoute.snapshot.queryParams),
    );
  }

  /**
   * Set new table state. Returns true if state is successfully updated.
   */
  setTableState(newState: TableState) {
    const mergedParams = {
      ...this.activedRoute.snapshot.queryParams,
      pageIndex: this.defaultPageIndex,
      ...newState,
    };
    return this.router.navigate(['.'], {
      queryParams: mergedParams,
      relativeTo: this.activedRoute,
    });
  }

  /**
   * Called when data is updated edgerly.
   */
  onUpdate(item: any) {
    this.updated$.next(item);
  }

  ngOnInit() {
    // The ordering of the following is significant.
    this.initStateParams$();
    this.initFetchParams$();
    this.initRawResponse$();
    this.initList$();
  }

  namespaceChanged(namespace: string) {
    this.setTableState({ namespace });
  }

  onPageEvent(pageEvent: PageParams) {
    this.setTableState(pageEvent);
  }

  async searchByName(name: string) {
    // Only mark as searching state when set state success.
    this.searching = await this.setTableState({
      search: name,
    });
    this.cdr.markForCheck();
  }

  initStateParams$() {
    const tableState$ = this.getTableState$();

    this.namespace$ = tableState$.pipe(map(params => params.namespace || ''));

    this.pageParams$ = tableState$.pipe(
      tap(
        params =>
          (this.currentPageIndex = params.pageIndex || this.defaultPageIndex),
      ),
      map(params => ({
        pageSize: +(params.pageSize || this.defaultPageSize),
        pageIndex: +(params.pageIndex || this.defaultPageIndex),
      })),
    );

    this.search$ = tableState$.pipe(map(params => params.search || ''));
  }

  /**
   * Init fetching params to be used when fetching list result.
   */
  initFetchParams$() {
    this.fetchParams$ = this.getDefaultFetchParams$();
    return this.fetchParams$;
  }

  getDefaultFetchParams$() {
    return combineLatest(
      this.namespace$,
      this.search$,
      this.pageParams$,

      // Polling pulses
      this.poll$,
      this.updated$,
    ).pipe(
      map(([namespace, search, pageParams]) => ({
        namespace,
        search,
        pageParams,
      })),
    );
    return this.fetchParams$;
  }

  /**
   * Initialize raw response of the given resource. The result will be used for
   */
  initRawResponse$(): Observable<ResourceList | HttpErrorResponse> {
    this.rawResponse$ = this.fetchParams$.pipe(
      debounceTime(50),
      tap(() => {
        this.fetching = true;
        // this.showZeroState = false;
        this.cdr.markForCheck();
      }),
      switchMap(params =>
        this.fetchResources(params).pipe(catchError(error => of(error))),
      ),
      tap(res => {
        this.fetching = false;
        this.searching = false;
        this.showZeroState =
          isHttpErrorResponse(res) || (res as ResourceList).count === 0;
        this.cdr.markForCheck();
        this.debouncedUpdate();
      }),
      publishReplay(1),
      refCount(),
    );

    return this.rawResponse$;
  }

  /**
   * Init list$ related observables.
   */
  initList$() {
    this.listMeta$ = this.rawResponse$.pipe(
      filter(res => isNotHttpErrorResponse(res)),
      map((res: ResourceList) => {
        return {
          count: res.count,
          num_pages: res.num_pages,
        };
      }),
    );

    this.list$ = this.rawResponse$.pipe(
      filter(res => isNotHttpErrorResponse(res)),
      tap((res: ResourceList) => this.emptyListCheck(res)),
      map((res: ResourceList) => this.map(res)),
    );
  }

  constructor(
    public http: HttpClient,
    public router: Router,
    public activedRoute: ActivatedRoute,
    public cdr: ChangeDetectorRef,
  ) {
    this.debouncedUpdate = debounce(() => {
      this.poll$.next(null);
    }, this.pollInterval);
  }
}
