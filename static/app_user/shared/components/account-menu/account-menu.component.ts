import { Component, Inject, OnInit } from '@angular/core';
import { DialogService } from 'alauda-ui';

import { ACCOUNT } from 'app2/core/tokens';
import { RcAccount } from 'app2/core/types';
import { OrgService } from 'app2/shared/services/features/org.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { CookieService } from 'app2/shared/services/utility/cookie.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { User } from 'app2/shared/types';
import { TranslateService } from 'app2/translate/translate.service';
import { AccountUpdatePasswordComponent } from 'app_user/shared/components/account-update-password/account-update-password.component';

@Component({
  selector: 'rc-account-menu',
  styleUrls: ['account-menu.component.scss'],
  templateUrl: 'account-menu.component.html',
})
export class AccountMenuComponent implements OnInit {
  username: string;
  isAdmin: boolean;
  language: string;
  showPassUpBtn = false;
  user: User;
  constructor(
    @Inject(ACCOUNT) public account: RcAccount,
    private dialogService: DialogService,
    private translateService: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private cookieService: CookieService,
    private orgService: OrgService,
    private error: ErrorsToastService,
  ) {}

  async ngOnInit() {
    this.username = this.account.username
      ? `${this.account.username}`
      : this.account.namespace;
    try {
      this.isAdmin = await this.roleUtil.resourceTypeSupportPermissions(
        'operation_view',
        {},
        'view',
      );
      if (this.account.username) {
        const user = await this.orgService.getUser();
        this.showPassUpBtn =
          user.sub_type !== 'ldap' && user.type !== 'organizations.LDAPAccount';
      }
      this.language = this.translateService.otherLang;
    } catch (e) {
      this.error.error(e);
    }
  }

  switchToConsole() {
    this.cookieService.setCookie('project', '');
    location.href = '/console';
  }

  changeLanguage() {
    const preLang = this.translateService.currentLang;
    this.translateService.changeLanguage();
    this.language = preLang;
  }

  changePassword() {
    this.dialogService.open(AccountUpdatePasswordComponent, {
      data: {
        username: this.account.username,
      },
    });
  }

  logout() {
    this.dialogService
      .confirm({
        title: this.translateService.get('logout_confirm'),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      })
      .then(() => {
        location.href = '/ap/logout';
      })
      .catch(() => {});
  }
}
