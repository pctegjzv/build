import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DIALOG_DATA, DialogRef, NotificationService } from 'alauda-ui';
import { VendorCustomer } from 'app2/core/customization/vendor-customer.types';
import { ENVIRONMENTS } from 'app2/core/tokens';
import { Environments } from 'app2/core/types';
import { AccountService } from 'app2/shared/services/features/account.service';
import {
  PasswordType,
  PatternHelperService,
} from 'app2/shared/services/features/pattern-helper.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { TranslateService } from 'app2/translate/translate.service';

@Component({
  templateUrl: './account-update-password.component.html',
  styleUrls: ['./account-update-password.component.scss'],
})
export class AccountUpdatePasswordComponent implements OnInit {
  pwdLoading = false;
  submitting = false;
  username: string;
  isSubAccount = false;
  isCmb = false;
  formModel = { password: '', confirmPassword: '' };
  pwdPattern = {};
  @ViewChild('form')
  form: NgForm;

  constructor(
    private accountService: AccountService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private errorHandle: ErrorsToastService,
    private patternHelper: PatternHelperService,
    @Inject(ENVIRONMENTS) public environments: Environments,
    @Inject(DIALOG_DATA) private dialogData: { username: string },
    private dialogRef: DialogRef<any>,
  ) {}

  ngOnInit() {
    this.username = this.dialogData.username || '';
    this.isSubAccount = this.accountService.isSubAccount();
    this.isCmb = this.environments.vendor_customer === VendorCustomer.CMB;
    this.pwdPattern = this.patternHelper.getPasswordPattern(
      this.isCmb ? PasswordType.UAA : '',
    );
  }

  async saveNewPassword() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    const params: any = {
      username: this.username || undefined,
      password: this.form.value.password,
    };
    if ((!this.isSubAccount && !this.username) || this.isSubAccount) {
      params.old_password = this.form.value.old_password;
    }
    this.submitting = true;

    try {
      if (this.username) {
        this.isCmb
          ? await this.accountService.updateUaaAccountPassword(params)
          : await this.accountService.updateSubAccountPassword(params);
      } else {
        await this.accountService.updateProfile(params as any);
      }
      this.auiNotificationService.success({
        content: this.translate.get('update_password_success'),
      });
      this.dialogRef.close();
    } catch (e) {
      this.errorHandle.error(e);
    } finally {
      this.submitting = false;
    }
  }

  get cPwdPattern() {
    return this.formModel.password === this.formModel.confirmPassword
      ? /\.*/
      : /\.{,0}/;
  }
}
