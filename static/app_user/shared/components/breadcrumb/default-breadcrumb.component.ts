import {
  Component,
  ContentChildren,
  OnInit,
  QueryList,
  TemplateRef,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { TranslateService } from 'app2/translate/translate.service';
import { UiStateService } from 'app_user/core/ui-state.service';
import { BreadcrumbItemDirective } from 'app_user/shared/components/breadcrumb/breadcrumb.component';

const INGORED_PARTIALS = ['list', 'detail', 'update'];

@Component({
  selector: 'rc-default-breadcrumb',
  templateUrl: 'default-breadcrumb.component.html',
})
export class DefaultBreadcrumbComponent implements OnInit {
  partials$: Observable<{ routerLink?: string; label: string }[]>;
  @ContentChildren(BreadcrumbItemDirective, { read: TemplateRef })
  contentItemTemplates: QueryList<TemplateRef<any>>;

  constructor(
    private uiState: UiStateService,
    private activatedRoute: ActivatedRoute,
    private translate: TranslateService,
  ) {}

  ngOnInit(): void {
    this.partials$ = this.uiState.activeItemInfo$.pipe(
      map(navItemPartials => [
        ...navItemPartials,
        ...this.getLeafUrls()
          .filter(partial => !INGORED_PARTIALS.includes(partial))
          .map(partial => ({ label: this.translate.get(partial) })),
      ]),
    );
  }

  findLeaftRoute(activatedRoute: ActivatedRoute): ActivatedRoute {
    return activatedRoute.firstChild
      ? this.findLeaftRoute(activatedRoute.firstChild)
      : activatedRoute;
  }

  getLeafUrls() {
    return this.findLeaftRoute(this.activatedRoute).snapshot.url.map(
      fragment => fragment.path,
    );
  }
}
