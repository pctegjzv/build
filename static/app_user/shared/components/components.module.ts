import { PortalModule } from '@angular/cdk/portal';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMenuModule, MatTabsModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import {
  ButtonModule,
  CardModule,
  CheckboxModule,
  CodeEditorModule,
  DialogModule,
  DropdownModule,
  FormFieldModule,
  FormModule,
  IconModule,
  InlineAlertModule,
  InputModule,
  PaginatorModule,
  RadioModule,
  SelectModule,
  SortModule,
  TableModule as AuiTableModule,
  TagModule,
  TooltipModule,
} from 'alauda-ui';
import { ProjectAccountsListComponent } from 'app2/features/projects/accounts-list/accounts-list.component';
import { ProjectPieChartComponent } from 'app2/features/projects/pie/project-pie-chart.component';
import { ProjectRoleListComponent } from 'app2/features/projects/role-list/project-role-list.component';
import { AlertBoxComponent } from 'app2/shared/components/alert-box/alert-box.component';
import { MenuButtonCssStylerDirective } from 'app2/shared/components/button-menu/button-menu.component';
import { ButtonMenuComponent } from 'app2/shared/components/button-menu/button-menu.component';
import { IconTemplateDirective } from 'app2/shared/components/button-menu/button-menu.component';
import {
  ButtonComponent,
  ButtonCssStylerDirective,
  IconButtonCssStylerDirective,
} from 'app2/shared/components/button/button.component';
import { CustomXAxisTicksComponent } from 'app2/shared/components/chart/common/custom-x-axis-ticks.component';
import { CustomXAxisComponent } from 'app2/shared/components/chart/common/custom-x-axis.component';
import { CustomLineChartComponent } from 'app2/shared/components/chart/custom-line-chart.component';
import { CheckboxComponent } from 'app2/shared/components/checkbox/checkbox.component';
import { CollapsePaneComponent } from 'app2/shared/components/collapse/collapse.component';
import { ConfirmBoxComponent } from 'app2/shared/components/confirm-box/confirm-box.component';
import { CustomizeSizeComponent } from 'app2/shared/components/customize-size/customize-size.component';
import { FlexCellComponent } from 'app2/shared/components/flex-list/flex-cell/flex-cell.component';
import { FlexListComponent } from 'app2/shared/components/flex-list/flex-list.component';
import { FormContainerComponent } from 'app2/shared/components/form-container/form-container.component';
import { FooterContentDirective } from 'app2/shared/components/form-container/form-footer-content.directive';
import { FormAddonDirective } from 'app2/shared/components/form-field/form-addon.directive';
import { FormFieldColumnComponent } from 'app2/shared/components/form-field/form-field-column.component';
import { FormFieldComponent } from 'app2/shared/components/form-field/form-field.component';
import { FormPrefixDirective } from 'app2/shared/components/form-field/form-prefix.directive';
import { FormSuffixDirective } from 'app2/shared/components/form-field/form-suffix.directive';
import { LoadingMaskComponent } from 'app2/shared/components/loading-mask/loading-mask.component';
import { ModalContainerComponent } from 'app2/shared/components/modal-container/modal-container.component';
import { Pagination2Component } from 'app2/shared/components/pagination2/pagination.component';
import { PasswordStrengthLabelComponent } from 'app2/shared/components/password-strength-label/password-strength-label.component';
import { PasswordStrengthComponent } from 'app2/shared/components/password-strength/password-strength.component';
import { SearchInputComponent } from 'app2/shared/components/search-input/search-input.component';
import {
  DropdownMaxLengthValidatorDirective,
  DropdownMinLengthValidatorDirective,
  MultiSelectionDropdownComponent,
} from 'app2/shared/components/select/multi-selection-dropdown.component';
import { OptionNotFoundIconComponent } from 'app2/shared/components/select/option-not-found-icon.component';
import { RadioGroupComponent } from 'app2/shared/components/select/radio-group.component';
import { SingleSelectionDropdownComponent } from 'app2/shared/components/select/single-selection-dropdown.component';
import { ServiceStatusBadgeComponent } from 'app2/shared/components/status-badge/service-status-badge.component';
import { StatusBadgeComponent } from 'app2/shared/components/status-badge/status-badge.component';
import { SuggestionInputComponent } from 'app2/shared/components/suggestion-input/suggestion-input.component';
import { TableModule } from 'app2/shared/components/table/table.module';
import {
  TabDirective,
  TabGroupComponent,
} from 'app2/shared/components/tabs/tab-group.component';
import { TagComponent } from 'app2/shared/components/tag/tag.component';
import { TagsInputComponent } from 'app2/shared/components/tags-input/tags-input.component';
import { TagsLabelComponent } from 'app2/shared/components/tags-label/tags-label.ocmponent';
import { InlineNotificationComponent } from 'app2/shared/components/toast-container/inline-notification.component';
import { YamlCommentFormComponent } from 'app2/shared/components/yaml-comment-form/yaml-comment-form.component';
import { PipesModule } from 'app2/shared/pipes/pipes.module';
import { TranslateModule } from 'app2/translate/translate.module';
import { MultipleQuotaSettingsComponent } from 'app_user/features/project/create-namespace/quota-setting/multiple-quota-settings.component';
import { AccountMenuComponent } from 'app_user/shared/components/account-menu/account-menu.component';
import { AccountUpdatePasswordComponent } from 'app_user/shared/components/account-update-password/account-update-password.component';
import {
  BreadcrumbComponent,
  BreadcrumbItemDirective,
  BreadcrumbSeparatorComponent,
} from 'app_user/shared/components/breadcrumb/breadcrumb.component';
import { DefaultBreadcrumbComponent } from 'app_user/shared/components/breadcrumb/default-breadcrumb.component';
import { MirrorClusterSelectComponent } from 'app_user/shared/components/mirror-clutser-select/mirror-cluster-select.component';
import { MirrorResourceDeleteConfirmComponent } from 'app_user/shared/components/mirror-resource-delete-confirm/mirror-resource-delete-confirm.component';
import { ProjectBadgeComponent } from 'app_user/shared/components/project-badge/project-badge.component';
import { ViewResourceYamlComponent } from 'app_user/shared/components/view-resource-yaml/view-resource-yaml.component';
import { ZeroStateComponent } from 'app_user/shared/components/zero-state/zero-state.component';
import { DirectivesModule } from 'app_user/shared/directives/directives.module';

import { StepsProcessComponent } from './steps-process/steps-process.component';

const AUI_MODULES = [
  IconModule,
  ButtonModule,
  SortModule,
  AuiTableModule,
  CodeEditorModule,
  CardModule,
  TooltipModule,
  DialogModule,
  FormFieldModule,
  InputModule,
  InlineAlertModule,
  CheckboxModule,
  RadioModule,
  SelectModule,
  FormModule,
  DropdownModule,
  TagModule,
  PaginatorModule,
];

const THIRDY_PARTY_MODULES = [
  MatTabsModule,
  MatMenuModule,
  NgxChartsModule,
  TableModule,
];

const COMPONENTS = [
  // app2 shared components
  LoadingMaskComponent,
  StatusBadgeComponent,
  ServiceStatusBadgeComponent,
  ConfirmBoxComponent,
  CustomLineChartComponent,
  CustomXAxisComponent,
  CustomXAxisTicksComponent,
  AlertBoxComponent,
  FormContainerComponent,
  FooterContentDirective,
  SingleSelectionDropdownComponent,
  MultiSelectionDropdownComponent,
  DropdownMinLengthValidatorDirective,
  DropdownMaxLengthValidatorDirective,
  RadioGroupComponent,
  TagsInputComponent,
  FormFieldComponent,
  FormFieldColumnComponent,
  FormAddonDirective,
  FormPrefixDirective,
  FormSuffixDirective,
  CheckboxComponent,
  ButtonCssStylerDirective,
  IconButtonCssStylerDirective,
  ButtonComponent,
  SuggestionInputComponent,
  Pagination2Component,
  ButtonMenuComponent,
  MenuButtonCssStylerDirective,
  IconTemplateDirective,
  CustomizeSizeComponent,
  ModalContainerComponent,
  SearchInputComponent,
  YamlCommentFormComponent,
  FlexListComponent,
  FlexCellComponent,
  TabGroupComponent,
  TabDirective,
  CollapsePaneComponent,
  TagComponent,
  InlineNotificationComponent,
  PasswordStrengthComponent,
  PasswordStrengthLabelComponent,
  ProjectPieChartComponent,
  ProjectAccountsListComponent,
  ProjectRoleListComponent,
  // CustomBarVerticalComponent,
  // CustomLineChartComponent,
  // CustomAreaChartComponent,
  // CustomXAxisComponent,
  // CustomXAxisTicksComponent,

  //********** app_user components **********//
  DefaultBreadcrumbComponent,
  BreadcrumbItemDirective,
  BreadcrumbSeparatorComponent,
  BreadcrumbComponent,
  AccountMenuComponent,
  ProjectBadgeComponent,
  StepsProcessComponent,
  ZeroStateComponent,
  AccountUpdatePasswordComponent,
  MirrorResourceDeleteConfirmComponent,
  ViewResourceYamlComponent,
  TagsLabelComponent,
  MirrorClusterSelectComponent,
  MultipleQuotaSettingsComponent,
];

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    RouterModule,
    TranslateModule,
    DirectivesModule,
    PipesModule,
    FormsModule,
    ReactiveFormsModule,
    PortalModule,
    ...AUI_MODULES,
    ...THIRDY_PARTY_MODULES,
  ],
  declarations: [...COMPONENTS, OptionNotFoundIconComponent],
  exports: [...COMPONENTS, ...THIRDY_PARTY_MODULES, ...AUI_MODULES],
  providers: [],
  entryComponents: [
    LoadingMaskComponent,
    ConfirmBoxComponent,
    AlertBoxComponent,
    ModalContainerComponent,
    AccountUpdatePasswordComponent,
    MirrorResourceDeleteConfirmComponent,
    ViewResourceYamlComponent,
  ],
})
export class ComponentsModule {}
