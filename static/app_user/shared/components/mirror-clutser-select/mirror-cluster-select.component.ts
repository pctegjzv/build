import {
  Component,
  Input,
  OnDestroy,
  OnInit,
  TemplateRef,
  forwardRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DialogRef, DialogService, DialogSize } from 'alauda-ui';
import { cloneDeep, isArray } from 'lodash';
import { Subject } from 'rxjs';

interface Cluster {
  name: string;
  display_name: string;
}

@Component({
  selector: 'rc-mirror-cluster-select',
  templateUrl: 'mirror-cluster-select.component.html',
  styleUrls: ['mirror-cluster-select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MirrorClusterSelectComponent),
      multi: true,
    },
  ],
})
export class MirrorClusterSelectComponent
  implements OnInit, OnDestroy, ControlValueAccessor {
  private onDestroy$ = new Subject<void>();
  value: string[];
  @Input()
  clusters: Cluster[];
  @Input()
  currentCluster: Cluster[];
  selectCluster: string[];
  modelRef: DialogRef<TemplateRef<any>>;

  constructor(private dialogService: DialogService) {}

  ngOnInit() {}

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  cancel() {
    this.modelRef.close();
  }

  confirm() {
    this.value = cloneDeep(this.selectCluster);
    this.emitToModel();
    this.modelRef.close();
  }

  isDisabled(cluster: Cluster) {
    return this.currentCluster
      ? isArray(this.currentCluster)
        ? this.currentCluster.indexOf(cluster) >= 0
        : this.currentCluster === cluster
      : false;
  }

  selectTag(template: TemplateRef<any>) {
    this.selectCluster = cloneDeep(this.value);
    this.modelRef = this.dialogService.open(template, {
      size: DialogSize.Medium,
    });
  }

  // emit _value to ngModel
  private emitToModel() {
    this.onTouchedCallback();
    this.onChangeCallback(this.value);
  }

  /** Following Methods Implements ControlValueAccessor **/
  writeValue(value: any) {
    this.value = value;
    this.emitToModel();
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}
