import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { Subject } from 'rxjs';

interface MirrorResourceMeta {
  displayName?: string;
  mirrorResource: any[];
  confirmTitle: string;
  confirmLabel: string;
  confirmContent: string;
}

@Component({
  templateUrl: 'mirror-resource-delete-confirm.component.html',
  styleUrls: ['mirror-resource-delete-confirm.component.scss'],
})
export class MirrorResourceDeleteConfirmComponent implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<void>();
  @Output()
  close = new EventEmitter();
  initialized = true;
  mirrorResourceMeta: MirrorResourceMeta = {
    displayName: 'name',
    mirrorResource: [],
    confirmTitle: '',
    confirmLabel: '',
    confirmContent: '',
  };
  resourceSelected: any[] = [];
  requiredSelected: any[] = [];

  constructor() {}

  ngOnInit() {}

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  cancel() {
    this.close.next(null);
  }

  confirm() {
    this.close.next(this.resourceSelected);
  }

  isDisabled(resource: any) {
    if (this.requiredSelected.length) {
      return this.requiredSelected.indexOf(resource) >= 0;
    }
    return false;
  }

  setMirrorResourceMeta(
    mirrorResourceMeta: MirrorResourceMeta,
    resourceSelected?: any,
  ) {
    Object.assign(this.mirrorResourceMeta, mirrorResourceMeta);
    if (resourceSelected) {
      this.resourceSelected = resourceSelected;
      this.requiredSelected = resourceSelected;
    }
  }
}
