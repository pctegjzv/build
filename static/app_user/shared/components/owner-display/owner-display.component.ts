import { Component, Input } from '@angular/core';

@Component({
  selector: 'rc-owner-display',
  templateUrl: './owner-display.component.html',
  styleUrls: ['./owner-display.component.scss'],
})
export class OwnerDisplayComponent {
  @Input()
  owner: {
    name: string;
    phone: string;
  };

  constructor() {}
}
