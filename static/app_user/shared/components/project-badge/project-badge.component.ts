import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChange,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { TooltipDirective } from 'alauda-ui';

import { Project } from 'app2/shared/services/features/project.service';
import { BehaviorSubject, Observable, combineLatest } from 'rxjs';
import { debounceTime, map, publishReplay, refCount } from 'rxjs/operators';

@Component({
  selector: 'rc-project-badge',
  styleUrls: ['project-badge.component.scss'],
  templateUrl: 'project-badge.component.html',
})
export class ProjectBadgeComponent implements OnInit, OnChanges {
  @Input()
  projects: Project[];
  @Input()
  currentProject: Project;
  @ViewChild('projectTooltip', { read: TooltipDirective })
  projectTooltip: TooltipDirective;

  projects$ = new BehaviorSubject<Project[]>([]);
  filteredProjects$: Observable<Project[]>;
  filterName = '';
  filterName$: BehaviorSubject<string>;

  constructor(private router: Router) {}

  async ngOnInit() {
    this.filterName$ = new BehaviorSubject<string>(this.filterName);
    this.filteredProjects$ = combineLatest(
      this.filterName$,
      this.projects$,
    ).pipe(
      debounceTime(100),
      map(([name, projects]) => {
        name = name.trim();
        return name
          ? projects.filter((project: Project) => {
              const projectName = project.display_name || project.name;
              return projectName.includes(name);
            })
          : projects;
      }),
      publishReplay(1),
      refCount(),
    );
  }

  ngOnChanges({ projects }: { projects: SimpleChange }) {
    if (projects && projects.currentValue) {
      this.projects$.next(projects.currentValue);
    }
  }

  selectProject(project: Project) {
    this.projectTooltip.disposeTooltip();
    this.router.navigate([
      'project',
      {
        project: project.name,
      },
    ]);
  }

  trackByProjectId(_index: number, project: Project) {
    return project.uuid;
  }
}
