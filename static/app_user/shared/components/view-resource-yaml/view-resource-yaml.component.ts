import { Component, Inject, OnInit } from '@angular/core';

import { DIALOG_DATA } from 'alauda-ui';

@Component({
  templateUrl: 'view-resource-yaml.component.html',
  styleUrls: ['view-resource-yaml.component.scss'],
})
export class ViewResourceYamlComponent implements OnInit {
  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: true,
  };
  yaml = '';
  initialized = false;

  constructor(
    @Inject(DIALOG_DATA)
    private modalData: {
      yaml: string;
    },
  ) {}

  ngOnInit() {
    if (this.modalData.yaml) {
      this.yaml = this.modalData.yaml;
    }
    this.initialized = true;
  }
}
