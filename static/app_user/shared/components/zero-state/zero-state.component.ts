import { Component, Input } from '@angular/core';

@Component({
  selector: 'rc-zero-state',
  templateUrl: './zero-state.component.html',
  styleUrls: ['./zero-state.component.scss'],
})
export class ZeroStateComponent {
  @Input()
  resourceName: string;
  @Input()
  zeroState: boolean;
  @Input()
  fetching: boolean;
}
