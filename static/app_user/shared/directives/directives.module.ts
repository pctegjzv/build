import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CalendarDirective } from 'app2/shared/directives/calendar/calendar.directive';
import {
  FormDirective,
  RcFormGroupDirective,
} from 'app2/shared/directives/form/form.directive';
import { InputDirective } from 'app2/shared/directives/input/input.directive';
import { LoadingMaskDirective } from 'app2/shared/directives/loading-mask/loading-mask.directive';
import {
  TocContentDirective,
  TocContentsContainerDirective,
  TocLinkDirective,
} from 'app2/shared/directives/table-of-contents';
import { TooltipModule } from 'app2/shared/directives/tooltip/tooltip.module';
import { TrimDirective } from 'app2/shared/directives/trim/trim.directive';
import { VisibilityDirective } from 'app2/shared/directives/utilities/visibility.directive';
import { CronValidatorDirective } from 'app2/shared/directives/validators/cron.directive';
import {
  MaxValidatorDirective,
  MinValidatorDirective,
} from 'app2/shared/directives/validators/min-max.directive';
import { PageHeaderContentDirective } from 'app_user/shared/directives/page-header-content.directive';

const directives = [
  LoadingMaskDirective,
  InputDirective,
  FormDirective,
  RcFormGroupDirective,
  VisibilityDirective,
  CalendarDirective,
  TocContentsContainerDirective,
  TocContentDirective,
  TocLinkDirective,
  MinValidatorDirective,
  MaxValidatorDirective,
  TrimDirective,
  CronValidatorDirective,

  //********** app_user directives **********//
  PageHeaderContentDirective,
];

@NgModule({
  imports: [CommonModule, TooltipModule],
  declarations: directives,
  exports: [TooltipModule, ...directives],
})
export class DirectivesModule {}
