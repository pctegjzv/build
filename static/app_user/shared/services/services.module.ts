import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { PatternHelperService } from 'app2/shared/services/features/pattern-helper.service';
import { DefaultErrorMapperService } from 'app2/shared/services/form/default-error-mapper.service';
import { HttpService } from 'app2/shared/services/http/http.service';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { CookieService } from 'app2/shared/services/utility/cookie.service';
import { ErrorsToastService } from 'app2/shared/services/utility/errors-toast.service';
import { LoggerUtilitiesService } from 'app2/shared/services/utility/logger.service';
import { TitleCaseService } from 'app2/shared/services/utility/title-case.service';

import { VendorCustomerService } from 'app2/core/customization/vendor-customer.service';
import { ServiceUtilitiesService } from 'app2/features/service/service-utilities.service';
import { AccountService } from 'app2/shared/services/features/account.service';
import { AppService } from 'app2/shared/services/features/app.service';
import { AppUtilitiesService } from 'app2/shared/services/features/app.utilities.service';
import { ConfigmapService } from 'app2/shared/services/features/configmap.service';
import { ConfigmapUtilitiesService } from 'app2/shared/services/features/configmap.utilities.service';
import { DomainService } from 'app2/shared/services/features/domain.service';
import { EnvironmentService } from 'app2/shared/services/features/environment.service';
import { EventService } from 'app2/shared/services/features/event.service';
import { K8sResourceService } from 'app2/shared/services/features/k8s-resource.service';

import { ImageProjectService } from 'app2/shared/services/features/image-project.service';
import { ImageRegistryService } from 'app2/shared/services/features/image-registry.service';
import { ImageRepositoryService } from 'app2/shared/services/features/image-repository.service';
import { ImageRepositoryUtilitiesService } from 'app2/shared/services/features/image-repository.utilities.service';
import { LogService } from 'app2/shared/services/features/log.service';
import { MonitorService } from 'app2/shared/services/features/monitor.service';
import { MonitorUtilitiesService } from 'app2/shared/services/features/monitor.utilities.service';
import { NamespaceService } from 'app2/shared/services/features/namespace.service';
import { NetworkService } from 'app2/shared/services/features/network.service';
import { NetworkUtilitiesService } from 'app2/shared/services/features/network.utilities.service';
import { OrgService } from 'app2/shared/services/features/org.service';
import { PodService } from 'app2/shared/services/features/pod.service';
import { ProjectService } from 'app2/shared/services/features/project.service';
import { RBACService } from 'app2/shared/services/features/rbac.service';
import { RegionService } from 'app2/shared/services/features/region.service';
import { RSRCManagementService } from 'app2/shared/services/features/resrc-manage.service';
import { RoleUtilitiesService } from 'app2/shared/services/features/role-utilities.service';
import { RoleService } from 'app2/shared/services/features/role.service';
import { ServiceService } from 'app2/shared/services/features/service.service';
import { StorageService } from 'app2/shared/services/features/storage.service';
import { TerminalService } from 'app2/shared/services/features/terminal.service';
import { TranslateService } from 'app2/translate/translate.service';

const FEATURE_PROVIDERS = [
  AccountService,
  ProjectService,
  RegionService,
  RoleService,
  EventService,
  StorageService,
  ServiceService,
  ServiceUtilitiesService,
  AppService,
  AppUtilitiesService,
  PodService,
  NamespaceService,
  ConfigmapService,
  ConfigmapUtilitiesService,
  NetworkService,
  NetworkUtilitiesService,
  RBACService,
  RoleUtilitiesService,
  ImageRegistryService,
  ImageProjectService,
  ImageRepositoryService,
  ImageRepositoryUtilitiesService,
  LogService,
  MonitorService,
  MonitorUtilitiesService,
  OrgService,
  EnvironmentService,
  TerminalService,
  RSRCManagementService,
  DomainService,
];

@NgModule({
  providers: [
    ModalService,
    HttpService,
    DefaultErrorMapperService,
    TitleCaseService,
    CookieService,
    LoggerUtilitiesService,
    ErrorsToastService,
    K8sResourceService,
    PatternHelperService,
    VendorCustomerService,
    ...FEATURE_PROVIDERS,
  ],
  imports: [HttpClientModule],
})
export class ServicesModule {
  constructor(
    // Inject the following to make sure they are loaded ahead of other components.
    _translate: TranslateService,
  ) {}
}
