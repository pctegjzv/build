import { PortalModule } from '@angular/cdk/portal';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MATERIAL_SANITY_CHECKS } from '@angular/material';

import { PipesModule } from 'app2/shared/pipes/pipes.module';
import { ModalService } from 'app2/shared/services/modal/modal.service';
import { TranslateModule } from 'app2/translate/translate.module';
import { ComponentsModule } from 'app_user/shared/components/components.module';
import { DirectivesModule } from 'app_user/shared/directives/directives.module';

const NG_MODULES = [CommonModule, FormsModule, ReactiveFormsModule];

const CUSTOM_MODULES = [ComponentsModule, DirectivesModule, PipesModule];

const THIRD_PARTY_MODULES = [FlexLayoutModule, PortalModule];

const EXPORTABLE_MODULES = [
  ...NG_MODULES,
  ...CUSTOM_MODULES,
  ...THIRD_PARTY_MODULES,
];

@NgModule({
  imports: [...EXPORTABLE_MODULES, TranslateModule],
  exports: [...EXPORTABLE_MODULES, TranslateModule],
  providers: [
    {
      provide: MATERIAL_SANITY_CHECKS,
      useValue: false,
    },
    ModalService,
  ],
})
export class SharedModule {}
