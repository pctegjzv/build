import { Action } from '@ngrx/store';
import { Project } from 'app2/shared/services/features/project.service';

export const LOAD_PROJECTS = '[Projects] Load Projects';
export const LOAD_PROJECTS_SUCCESS = '[Projects] Load Projects Success';
export const LOAD_PROJECTS_FAIL = '[Projects] Load Projects Fail';

export const LOAD_PROJECT_DETAIL = '[Projects] Load Project Detail';
export const LOAD_PROJECT_DETAIL_SUCCESS =
  '[Projects] Load Project Detail Success';
export const LOAD_PROJECT_DETAIL_FAIL = '[Projects] Load Project Detail Fail';

export const SYNC_PROJECT_COOKIE = '[Projects] Sync Project Cookie';

export class LoadProjects implements Action {
  readonly type = LOAD_PROJECTS;
}

export class LoadProjectsSuccess implements Action {
  readonly type = LOAD_PROJECTS_SUCCESS;
  constructor(public payload: Project[]) {}
}

export class LoadProjectsFail implements Action {
  readonly type = LOAD_PROJECTS_FAIL;
  constructor(public payload: any) {}
}

export class LoadProjectDetail implements Action {
  readonly type = LOAD_PROJECT_DETAIL;
  constructor(public payload: string) {}
}

export class LoadProjectDetailSuccess implements Action {
  readonly type = LOAD_PROJECT_DETAIL_SUCCESS;
  constructor(public payload: Project) {}
}

export class LoadProjectDetailFail implements Action {
  readonly type = LOAD_PROJECT_DETAIL_FAIL;
  constructor(public payload: any) {}
}

export class SyncProjectCookie implements Action {
  readonly type = SYNC_PROJECT_COOKIE;
  constructor(public payload: string) {}
}

export type ProjectAction =
  | LoadProjects
  | LoadProjectsSuccess
  | LoadProjectsFail
  | LoadProjectDetail
  | LoadProjectDetailSuccess
  | LoadProjectDetailFail
  | SyncProjectCookie;
