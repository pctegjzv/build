import { ProjectEffects } from './projects.effects';

export const effects: any[] = [ProjectEffects];

export * from './projects.effects';
