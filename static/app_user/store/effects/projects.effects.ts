import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Project } from 'app2/shared/services/features/project.service';
import { ProjectService } from 'app2/shared/services/features/project.service';
import { CookieService } from 'app2/shared/services/utility/cookie.service';
import { from, of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import * as projectActions from '../actions/projects.actions';

@Injectable()
export class ProjectEffects {
  constructor(
    private actions$: Actions,
    private projectService: ProjectService,
    private cookieService: CookieService,
  ) {}

  @Effect()
  loadProjects$ = this.actions$.ofType(projectActions.LOAD_PROJECTS).pipe(
    switchMap(() => {
      return from(this.projectService.getProjects(true)).pipe(
        map(
          (projects: Project[]) =>
            new projectActions.LoadProjectsSuccess(projects),
        ),
        catchError(error => of(new projectActions.LoadProjectsFail(error))),
      );
    }),
  );

  @Effect()
  loadCurrentProject$ = this.actions$
    .ofType(projectActions.LOAD_PROJECT_DETAIL)
    .pipe(
      map((action: projectActions.LoadProjectDetail) => action.payload),
      switchMap((payload: string) => {
        return from(this.projectService.getProject(payload)).pipe(
          map(
            (project: Project) =>
              !!project
                ? new projectActions.LoadProjectDetailSuccess(project)
                : new projectActions.LoadProjectDetailFail(null),
          ),
        );
      }),
    );

  @Effect({ dispatch: false })
  syncProjectCookie$ = this.actions$
    .ofType(projectActions.SYNC_PROJECT_COOKIE)
    .pipe(
      tap((payload: string) => {
        this.cookieService.setCookie('project', payload);
      }),
    );
}
