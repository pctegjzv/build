import {
  ActivatedRouteSnapshot,
  Params,
  RouterStateSnapshot,
} from '@angular/router';
import * as fromRouter from '@ngrx/router-store';
import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

import * as fromProject from './projects.reducers';

export interface RouterStateUrl {
  url: string;
  params: Params;
  queryParams: Params;
  // we need project name ...
  projectName: string;
}

export interface AppState {
  router: fromRouter.RouterReducerState<RouterStateUrl>;
  projects: fromProject.ProjectState;
}

export const reducers: ActionReducerMap<AppState> = {
  router: fromRouter.routerReducer,
  projects: fromProject.reducer,
};

export const getRouterState = createFeatureSelector<
  fromRouter.RouterReducerState<RouterStateUrl>
>('router');

export const getProjectsState = createFeatureSelector<
  AppState,
  fromProject.ProjectState
>('projects');

export class CustomSerializer
  implements fromRouter.RouterStateSerializer<RouterStateUrl> {
  serialize(routerState: RouterStateSnapshot): RouterStateUrl {
    const { url } = routerState;
    const { queryParams } = routerState.root;
    let projectName: string;
    let state: ActivatedRouteSnapshot = routerState.root;
    while (state.firstChild) {
      if (!projectName && !!state.params['project']) {
        projectName = state.params['project'];
      }
      state = state.firstChild;
    }
    const { params } = state;

    return { url, queryParams, params, projectName };
  }
}
