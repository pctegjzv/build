import { Project } from 'app2/shared/services/features/project.service';

import * as fromProject from '../actions/projects.actions';

export interface ProjectState {
  entities: { [name: string]: Project };
  entitiesDetail: { [name: string]: Project };
  loaded: boolean;
  loading: boolean;
}

export const initialState: ProjectState = {
  entities: {},
  entitiesDetail: {},
  loaded: false,
  loading: false,
};

export function reducer(
  state = initialState,
  action: fromProject.ProjectAction,
) {
  switch (action.type) {
    case fromProject.LOAD_PROJECTS: {
      return {
        ...state,
        loading: true,
      };
    }
    case fromProject.LOAD_PROJECTS_SUCCESS: {
      const projects = action.payload;
      const entities = projects.reduce(
        (entities: { [name: string]: Project }, project: Project) => {
          return {
            ...entities,
            [project.name]: project,
          };
        },
        {
          ...state.entities,
        },
      );
      return {
        ...state,
        loading: false,
        loaded: true,
        entities,
      };
    }
    case fromProject.LOAD_PROJECTS_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: false,
      };
    }
    case fromProject.LOAD_PROJECT_DETAIL_SUCCESS: {
      const project = action.payload;
      const entitiesDetail = {
        ...state.entitiesDetail,
        [project.name]: project,
      };
      return {
        ...state,
        entitiesDetail,
      };
    }
  }

  return state;
}

export const getProjectEntities = (state: ProjectState) => state.entities;
export const getProjectEntitiesDetail = (state: ProjectState) =>
  state.entitiesDetail;
export const getProjectLoading = (state: ProjectState) => state.loading;
export const getProjectLoaded = (state: ProjectState) => state.loaded;
