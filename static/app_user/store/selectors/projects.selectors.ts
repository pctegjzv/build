import { createSelector } from '@ngrx/store';
import { Project } from 'app2/shared/services/features/project.service';

import * as fromFeature from '../reducers';
import * as fromProject from '../reducers/projects.reducers';

export const getProjectsEntities = createSelector(
  fromFeature.getProjectsState,
  fromProject.getProjectEntities,
);

export const getProjectsEntitiesDetail = createSelector(
  fromFeature.getProjectsState,
  fromProject.getProjectEntitiesDetail,
);

export const getAllProjects = createSelector(getProjectsEntities, entities => {
  return Object.keys(entities).map(name => entities[name]);
});

export const getAllProjectsDetail = createSelector(
  getProjectsEntitiesDetail,
  entitiesDetail => {
    return Object.keys(entitiesDetail).map(name => entitiesDetail[name]);
  },
);

export const getCurrentProject = createSelector(
  getProjectsEntities,
  fromFeature.getRouterState,
  (entities, router): Project => {
    return router.state && entities[router.state.projectName];
  },
);

export const getCurrentProjectDetail = createSelector(
  getProjectsEntitiesDetail,
  fromFeature.getRouterState,
  (entitiesDetail, router): Project => {
    return router.state && entitiesDetail[router.state.projectName];
  },
);

export const getProjectsLoading = createSelector(
  fromFeature.getProjectsState,
  fromProject.getProjectLoading,
);
export const getProjectsLoaded = createSelector(
  fromFeature.getProjectsState,
  fromProject.getProjectLoaded,
);
