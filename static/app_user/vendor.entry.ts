// jquery
import 'codemirror';
import 'codemirror/addon/display/autorefresh';
import 'codemirror/addon/scroll/simplescrollbars';
import 'codemirror/mode/groovy/groovy';
import 'codemirror/mode/shell/shell';
import 'codemirror/mode/yaml/yaml';
import 'expose-loader?$!expose-loader?jQuery!jquery';
// codemirror
import 'expose-loader?CodeMirror!codemirror';
// Only require zh-cn for the moment
import 'moment/locale/zh-cn';
// semantic ui components
import 'semantic-ui-api/api.min.js';
import 'semantic-ui-checkbox/checkbox.min.js';
import 'semantic-ui-checkbox/checkbox.min.js';
import 'semantic-ui-dimmer/dimmer.min.js';
import 'semantic-ui-dropdown/dropdown.min.js';
import 'semantic-ui-modal/modal.min.js';
import 'semantic-ui-popup/popup.min.js';
import 'semantic-ui-search/search.min.js';
import 'semantic-ui-transition/transition.min.js';

// Fix error: Uncaught reflect-metadata shim is required when using class decorators
import 'reflect-metadata';

// Zone js
import 'zone.js';
