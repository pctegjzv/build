import { filter, map } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { NavigationEnd, Router, UrlSegment } from '@angular/router';

@Injectable()
export class AppRouterService {
  constructor(private router: Router) {}

  /**
   * Get the url fragments for the current leaf state.
   */
  get urlSegments() {
    return this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map(() => {
        let segments: UrlSegment[] = [];
        let currentLeafState = this.router.routerState.root;
        while (currentLeafState.firstChild) {
          currentLeafState = currentLeafState.firstChild;
          segments = segments.concat(currentLeafState.snapshot.url);
        }
        return segments.map(segment => segment.path);
      }),
    );
  }
}
