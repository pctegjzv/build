import { Component, Inject, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { AppRouterService } from './app-router.service';
import { TranslateService } from './common/translate.service';
import { ENVIRONMENTS } from './tokens';
import { Environments } from './types';

const LOGO_LARGE_KEY = 'logo-large';
const bottomLinkInvisibleParts = [
  'login/password',
  'login/password/forget-password',
];
const headerInvisibleParts = ['login/password/forget-password'];

@Component({
  selector: 'rld-app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  private footerRouterLinkObject: [string, string];
  bottomLinkInvisible = false;
  headerInvisible = false;

  /**
   * Path to the logo src. Can be configured via environment var
   * @type {string}
   */
  logoSrc = '/static/images/logo/logo-login-large.svg';

  constructor(
    private translate: TranslateService,
    private title: Title,
    private appRouterService: AppRouterService,
    @Inject(ENVIRONMENTS) private ENVIRONMENTS: Environments,
  ) {}

  ngOnInit(): void {
    const routingTitles = [
      ['guide', this.translate.get('login_title')],
      ['login', this.translate.get('login_title')],
      ['register', this.translate.get('register_title')],
      ['forget-password/reset', this.translate.get('reset_password')],
      ['forget-password', this.translate.get('forget_password')],
      ['validation-failure', this.translate.get('title_login_auth_failure')],
      ['oidc-auth-redirect', this.translate.get('oidc_auth_login')],
      ['license-activation', this.translate.get('license-activation')],
    ];

    // first segment -> [ URL, Link Text ]
    const bottomLinks = {
      guide: ['/register', 'register_link_text'],
      login: ['/register', 'register_link_text'],
      'register-complete': ['/login', 'register_complete_link_text'],
      'validation-failure': [null, 'validation_failure_close_window'],
    };

    const defaultLink = ['/login', 'login_link_text'];

    this.appRouterService.urlSegments.subscribe(segments => {
      const newTitle = routingTitles.find(([path]) =>
        segments.join('/').startsWith(path),
      )[1];
      // Angular encapsulates DOM api. We should not try to access DOM directly.
      this.title.setTitle(newTitle);
      this.footerRouterLinkObject = bottomLinks[segments[0]]
        ? bottomLinks[segments[0]]
        : defaultLink;
      this.bottomLinkInvisible = bottomLinkInvisibleParts.includes(
        segments.join('/'),
      );
      this.headerInvisible = headerInvisibleParts.includes(segments.join('/'));
      if (
        this.ENVIRONMENTS.is_private_deploy_enabled &&
        (segments[0] === 'oidc-auth-redirect' ||
          this.footerRouterLinkObject[0] === '/register')
      ) {
        this.footerRouterLinkObject = undefined;
      }
    });

    // Override logo src if there is an environment variable been set.
    const logoSrcEnv =
      this.ENVIRONMENTS.overridden_logo_sources &&
      this.ENVIRONMENTS.overridden_logo_sources
        .split(',')
        .find(_logoSrcEnv => _logoSrcEnv.startsWith(LOGO_LARGE_KEY + ':'));

    if (logoSrcEnv) {
      this.logoSrc = logoSrcEnv.substr(logoSrcEnv.indexOf(':') + 1);
    }
  }

  get footerRouterLink() {
    return this.footerRouterLinkObject && this.footerRouterLinkObject[0];
  }

  get footerLinkText() {
    return this.footerRouterLinkObject && this.footerRouterLinkObject[1];
  }

  get shouldShowLogo() {
    return (
      this.ENVIRONMENTS.third_party_login_type !== '4A' && !this.headerInvisible
    );
  }
}
