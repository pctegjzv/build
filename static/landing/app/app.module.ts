import { NgModule } from '@angular/core';
import { UpgradeModule } from '@angular/upgrade/static';
import {
  ButtonModule,
  DialogModule,
  FormFieldModule,
  FormModule,
  IconModule,
  InlineAlertModule,
  InputModule,
  MessageModule,
  NOTIFICATION_CONFIG,
  NotificationModule,
  TooltipModule,
} from 'alauda-ui';

import { AppRouterService } from './app-router.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonModule } from './common/common.module';
import { LoginTranslateModule } from './translate.module';

import {
  GuideComponent,
  LicenseActivationComponent,
  LoginComponent,
  OidcLoginComponent,
  RegisterCompleteComponent,
  RegisterComponent,
  ResetPasswordCompleteComponent,
  ResetPasswordComponent,
  SendCaptchaCodeComponent,
  ThirdPartyLoginErrorComponent,
} from './feature-components';
import { ForgetPasswordComponent } from './login/pure_password/forget-password/forget-password.component';
import { PurePasswordLoginComponent } from './login/pure_password/pure-password.component';
import { ENVIRONMENTS } from './tokens';

const AluadaUiModules = [
  ButtonModule,
  InputModule,
  FormModule,
  FormFieldModule,
  NotificationModule,
  MessageModule,
  IconModule,
  DialogModule,
  InlineAlertModule,
  TooltipModule,
];

/**
 * The root module. See https://angular.io/docs/ts/latest/guide/style-guide.html for styling guide.
 */
@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    RegisterCompleteComponent,
    LoginComponent,
    SendCaptchaCodeComponent,
    ResetPasswordComponent,
    ResetPasswordCompleteComponent,
    ThirdPartyLoginErrorComponent,
    OidcLoginComponent,
    LicenseActivationComponent,
    GuideComponent,
    PurePasswordLoginComponent,
    ForgetPasswordComponent,
  ],
  imports: [
    CommonModule,
    LoginTranslateModule,
    UpgradeModule,
    AppRoutingModule,
    ...AluadaUiModules,
  ],
  providers: [
    AppRouterService,
    {
      useFactory: () => window['ENVIRONMENTS'],
      provide: ENVIRONMENTS,
    },
    {
      provide: NOTIFICATION_CONFIG,
      useFactory: () => ({
        duration: window['ENVIRONMENTS'].notification_duration,
        maxStack: window['ENVIRONMENTS'].notification_max_stack,
      }),
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(public upgrade: UpgradeModule) {}
}
