import { Injector } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
} from '@angular/forms';
import * as _ from 'lodash';
import { pairwise } from 'rxjs/operators';

import { ErrorMessageParserService } from './error-message-parser.service';
import {
  FormControlFragment,
  FormControlFragmentDeclaration,
} from './form-control-fragment.component';

/**
 * Base abstract class for a FormGroup component
 */
export abstract class BaseFormComponent<T> {
  formModel: T;
  formGroup: FormGroup;
  formControlFragments: FormControlFragment[];
  formControlFragmentMap: { [key: string]: FormControlFragment } = {};

  private _formBuilder: FormBuilder;
  private _errorMessageParser: ErrorMessageParserService;

  constructor() {}

  /**
   * Return FormControlFragmentDeclaration array for this component.
   * {@see buildForm}
   */
  protected abstract get formFragmentDeclarations(): FormControlFragmentDeclaration[];

  /**
   * Instance to the component's injector. This is to get access to the DI.
   * @returns {Injector}
   */
  protected abstract get injectorInstance(): Injector;

  private get formBuilder(): FormBuilder {
    if (!this._formBuilder) {
      this._formBuilder = this.injectorInstance.get(FormBuilder);
    }
    return this._formBuilder;
  }

  private get errorMessageParser(): ErrorMessageParserService {
    if (!this._errorMessageParser) {
      this._errorMessageParser = this.injectorInstance.get(
        ErrorMessageParserService,
      );
    }
    return this._errorMessageParser;
  }

  /**
   * Build the form.
   */
  protected buildForm(
    formFragmentDeclarations: FormControlFragmentDeclaration[] = this
      .formFragmentDeclarations,
  ) {
    this.formControlFragments = formFragmentDeclarations.map(declaration => {
      const fragment = Object.create(declaration) as FormControlFragment;

      // Only validate the validator if the control is not hidden or not disabled
      const validators = declaration.validatorFns.map(
        validatorFn => (abstractControl: AbstractControl) => {
          return !fragment.hidden ? validatorFn(abstractControl) : null;
        },
      );

      fragment.control = new FormControl(
        this.formModel[declaration.name],
        validators,
      );
      return fragment;
    });

    const controlConfigs = {};

    for (const fragment of this.formControlFragments) {
      controlConfigs[fragment.name] = fragment.control;
      this.formControlFragmentMap[fragment.name] = fragment;
    }

    if (!this.formGroup) {
      this.formGroup = this.formBuilder.group(controlConfigs);
      // Use {@link Observable.pairwise} to detect individual field changes
      this.formGroup.valueChanges
        .pipe(pairwise())
        .subscribe(data => this.onValueChanged(data));
    } else {
      const previousNames = Object.keys(this.formGroup.controls);
      const newNames = this.formControlFragments.map(fragment => fragment.name);

      _.difference(previousNames, newNames).forEach(name =>
        this.formGroup.removeControl(name),
      );
      _.difference(newNames, previousNames).forEach(name =>
        this.formGroup.addControl(name, controlConfigs[name]),
      );
      _.intersection(newNames, previousNames).forEach(name =>
        this.formGroup.setControl(name, controlConfigs[name]),
      );
    }
  }

  /**
   * When the form value changes.
   * Typically, update the validation messages.
   */
  protected onValueChanged([prev, current]: [any, any]): void {
    if (!this.formGroup || !current) {
      return;
    }
    this.formModel = current;
    this.formControlFragments
      .filter(fragment => prev[fragment.name] !== current[fragment.name])
      .forEach(fragment => this.updateFragmentErrorMessage(fragment));
  }

  /**
   * Update fragment error message
   */
  updateFragmentErrorMessage(fragment: FormControlFragment | string) {
    if (typeof fragment === 'string') {
      fragment = this.formControlFragmentMap[fragment];
    }
    fragment.error = '';
    const control = fragment.control;
    if (control && control.dirty && !control.valid && control.errors) {
      const errors = this.errorMessageParser.getControlErrorMessage(
        control,
        fragment.name,
      );
      if (errors.length > 0) {
        fragment.error = errors[0];
      }
    }
  }

  /**
   * Set error object of a fragment.
   */
  setFragmentError(fragment: FormControlFragment | string, error?: any) {
    if (typeof fragment === 'string') {
      fragment = this.formControlFragmentMap[fragment];
    }
    if (!error) {
      // Run native validators
      fragment.control.updateValueAndValidity();
    } else {
      fragment.control.setErrors(error);
    }

    this.updateFragmentErrorMessage(fragment);
  }
}
