import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule } from '@ngx-translate/core';
import { TooltipModule } from 'alauda-ui';

import { LanguagePickerComponent } from '../language-picker.component';

import { CaptchaImgComponent } from './captcha-img.component';
import { LoadingMaskComponent } from './components/loading-mask/loading-mask.component';
import { InlineNotificationComponent } from './components/toast-container/inline-notification.component';
import { ErrorMessageParserService } from './error-message-parser.service';
import { FormControlFragmentComponent } from './form-control-fragment.component';
import { HttpService } from './http.service';
import { PasswordStrengthComponent } from './password-strength.component';
import { ScorePasswordService } from './score-password.service';
import { TranslateService } from './translate.service';
import { VerificationCodeButtonComponent } from './verification-code-button.component';

const SHARED_ELEMENTS = [
  CaptchaImgComponent,
  LanguagePickerComponent,
  FormControlFragmentComponent,
  VerificationCodeButtonComponent,
  PasswordStrengthComponent,
  InlineNotificationComponent,
  LoadingMaskComponent,
];

const IMPORT_MODULES = [
  HttpModule,
  BrowserModule,
  FormsModule,
  ReactiveFormsModule,
  TooltipModule,
  TranslateModule,
  BrowserAnimationsModule,
];

/**
 * Defines the services/components/modules which will be shared globally, such as http
 */
@NgModule({
  declarations: SHARED_ELEMENTS,
  imports: IMPORT_MODULES,
  providers: [
    HttpService,
    TranslateService,
    ErrorMessageParserService,
    ScorePasswordService,
  ],
  exports: [...SHARED_ELEMENTS, ...IMPORT_MODULES],
  entryComponents: [LoadingMaskComponent],
})
export class CommonModule {}
