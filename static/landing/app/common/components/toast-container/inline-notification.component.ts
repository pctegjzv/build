import { PortalHostDirective, TemplatePortal } from '@angular/cdk/portal';
import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';

import { toastAnimations } from './toast-animations';
import { ToastType, iconsType } from './toast.type';

@Component({
  selector: 'rc-inline-notification',
  templateUrl: './inline-notification.component.html',
  styleUrls: ['./inline-notification.component.scss'],
  animations: [toastAnimations.inOut],
})
export class InlineNotificationComponent implements OnInit, AfterViewInit {
  @Input()
  content: string;
  @Input()
  title: string;
  @Input()
  closable = false;
  @Input()
  type: ToastType = ToastType.Success;
  @Input()
  template: TemplateRef<any>;
  @Output()
  close: EventEmitter<any> = new EventEmitter();
  @ViewChild(PortalHostDirective)
  private _portalHost: PortalHostDirective;

  get iconType(): string {
    return iconsType[this.type];
  }

  show = true;
  animate: string;

  constructor() {}

  async ngOnInit() {}

  ngAfterViewInit(): void {
    if (this.template) {
      this._portalHost.attachTemplatePortal(
        new TemplatePortal(this.template, null, {}),
      );
    }
  }

  closeSelf() {
    this.show = false;
    this.close.emit();
  }
}
