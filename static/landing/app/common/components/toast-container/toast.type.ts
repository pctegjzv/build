import { TemplateRef, Type } from '@angular/core';

export enum ToastType {
  Error = 'error',
  Success = 'success',
  Warning = 'warning',
  Info = 'info',
  Explanation = 'explanation',
}

/**
 * ToastOptions: containes Basic configuration
 * Just for some containers like Message-Container
 * Detailed document： http://confluence.alaudatech.com/pages/viewpage.action?pageId=23383163
 */
export interface ToastOptions {
  /**
   * toast content
   */
  content?: string;
  /**
   * the toast type
   */
  type?: ToastType;
  /**
   * The id of this toast, The same ID can only have one at the same time
   */
  id?: string;
  /**
   * automatically shut down after a few seconds, if <= 0 ,non automatic closure
   */
  duration?: number;
}

/**
 * Options for Alert-Container
 * Detailed comparison documents: http://confluence.alaudatech.com/pages/viewpage.action?pageId=23383190
 */

export interface AlertOptions extends ToastOptions {
  /**
   * for alertContainer ,can use angular template
   */
  contentRef?: TemplateRef<any> | Type<void>;

  /**
   * Template context.
   */
  context?: any;

  /**
   * for alertContainer, if true,There is a close right angle closure
   */
  closeable?: boolean;
  /**
   * for alertContainer,can use title
   */
  title?: string;
}

export const iconsType = {
  [ToastType.Error]: 'fa-exclamation-triangle',
  [ToastType.Success]: 'fa-check-circle',
  [ToastType.Warning]: 'fa-exclamation-circle',
  [ToastType.Info]: 'fa fa-info-circle',
  [ToastType.Explanation]: 'fa fa-question-circle',
};
