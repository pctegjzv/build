import {
  Directive,
  Input,
  OnChanges,
  SimpleChanges,
  forwardRef,
} from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
  ValidatorFn,
} from '@angular/forms';

/* tslint:disable:use-host-property-decorator */

/**
 * Angular form directives for password
 */
@Directive({
  selector: '[aui-input][rld-custom-error],[rld-custom-error][ngModel]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => CustomErrorDirective),
      multi: true,
    },
  ],
})
export class CustomErrorDirective implements Validator, OnChanges {
  private _validator: ValidatorFn;
  private _onChange: () => void;
  private _customError: string;

  @Input('rld-custom-error')
  set customError(v) {
    this._customError = v;
  }
  get customError() {
    return this._customError;
  }

  ngOnChanges(changes: SimpleChanges): void {
    this._createValidator();
    if ('rld-custom-error' in changes) {
      this._createValidator();
      if (this._onChange) {
        this._onChange();
      }
    }
  }

  validate(c: AbstractControl): ValidationErrors | null {
    return this._validator(c);
  }

  registerOnValidatorChange(fn: () => void): void {
    this._onChange = fn;
  }

  private _createValidator(): void {
    this._validator = (control: AbstractControl): ValidationErrors => {
      return this.customError
        ? { customError: { current: control.value } }
        : null;
    };
  }
}
