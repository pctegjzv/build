import { AbstractControl, ValidatorFn, Validators } from '@angular/forms';

import { ScorePasswordService } from './score-password.service';

function isEmptyInputValue(value: any) {
  return value == null || (typeof value === 'string' && value.length === 0);
}

const USERNAME_PATTERN = /^[A-Za-z][A-Za-z0-9]*$/;
const EMAIL_PATTERN = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;

/**
 * Extending default validator functions here:
 */
export class LandingValidators extends Validators {
  /**
   * Validator that requires controls to have a value of an exact length.
   */
  static fixedLength(requiredLength: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (isEmptyInputValue(control.value)) {
        return null; // don't validate empty values to allow optional controls
      }
      const length: number = control.value ? control.value.length : 0;
      return length !== requiredLength
        ? { length: { requiredLength: requiredLength, actualLength: length } }
        : null;
    };
  }

  static passwordStrength(minStrength: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (isEmptyInputValue(control.value)) {
        return null; // don't validate empty values to allow optional controls
      }
      const strength = ScorePasswordService.score(control.value);
      return strength < minStrength
        ? {
            passwordStrength: {
              minStrength: minStrength,
              actualLength: strength,
            },
          }
        : null;
    };
  }

  static username(control: AbstractControl): { [key: string]: any } {
    return Validators.pattern(USERNAME_PATTERN)(control);
  }

  static email(control: AbstractControl): { [key: string]: any } {
    return Validators.pattern(EMAIL_PATTERN)(control);
  }

  static usernameOrEmail(control: AbstractControl): { [key: string]: any } {
    if (
      isEmptyInputValue(control.value) ||
      USERNAME_PATTERN.test(control.value) ||
      EMAIL_PATTERN.test(control.value)
    ) {
      return null;
    } else {
      return { pattern: true };
    }
  }

  static dynamicValidator(validatorGen: () => ValidatorFn): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      return validatorGen()(control);
    };
  }
}
