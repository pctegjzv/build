import { Component, EventEmitter, Output } from '@angular/core';
import { NotificationService } from 'alauda-ui';
import { Observable, interval } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { HttpService } from './http.service';
import { TranslateService } from './translate.service';

const REQUEST_VERIFICATION_CODE = 60;

/**
 * Send Verification code button. Used as an addon for registration form.
 */
@Component({
  selector: 'rld-verification-code-button', // prefix rld => Rubick landing
  styleUrls: ['../common/style/form.style.scss'],
  template: `
  <button class="ui button primary mini"
          style="border-radius: 5px;"
          (click)="requestVerificationCode()"
          [disabled]="disabled"
          [class.loading]="submitting">
    <span *ngIf="!codeSendCountDown">
      {{ 'send_verification_code' | translate }}
    </span>
    <span *ngIf="codeSendCountDown">
      {{ 'waiting_send_verification_code' | translate }}
      {{ codeSendCountDown | async }}
    </span>
  </button>
  `,
})
export class VerificationCodeButtonComponent {
  @Output()
  requests = new EventEmitter<any>();
  submitting: boolean;
  codeSendCountDown: Observable<number>;
  // To send a verification code, we need the two values:
  mobile: string;
  captcha: string;

  formValid: boolean;

  constructor(
    private http: HttpService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
  ) {}

  get disabled(): boolean {
    return !this.formValid || !!this.codeSendCountDown || this.submitting;
  }

  requestVerificationCode() {
    if (this.disabled) {
      return;
    }
    this.submitting = true;
    // Request verification code here ...
    const payload = {
      number: this.mobile,
      captcha: this.captcha,
    };
    this.http
      .request('/ajax/user/sendsms', {
        method: 'POST',
        body: payload,
      })
      .then(res => {
        this.requests.emit(res);
        if (res.status === 200) {
          this.codeSendCountDown = interval(1000).pipe(
            take(REQUEST_VERIFICATION_CODE),
            map(num => REQUEST_VERIFICATION_CODE - num - 1),
          );
          this.codeSendCountDown.subscribe(
            () => {},
            () => {},
            // On countdown complete
            () => {
              this.codeSendCountDown = null;
            },
          );
        }
      })
      .catch(error => {
        if (error[0].code === 'invalid_captcha') {
          this.auiNotificationService.error({
            content: this.translate.get('invalid_captcha'),
          });
        }
      })
      .then(() => {
        this.submitting = false;
      });
  }
}
