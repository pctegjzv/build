// TODO: group into different modules?
/* tslint:disable */
export default {
  login: 'Login',
  user_login: 'User Login',
  username_login: 'User Login',
  org_login: 'Account Login',
  register: 'Register',
  forget_password: 'Forgot Password',
  reset_password: 'Reset Password',
  login_title: 'Login',
  title_login_auth_failure: 'Login authentication failure',
  third_party_auth_failure:
    'Third-party login authentication failed. Please contact your system administrator',
  validation_failure_close_window: 'Please close this page',
  register_title: 'Register',
  enterprise_register: 'Enterprise Registration',
  predefined_org_hint: 'Your current account: {{orgName}}',
  org_name: 'Account',
  account: 'Account',
  mobile_no: 'Mobile',
  email: 'Email',
  mobile_or_email: 'Mobile or Email',
  username: 'Username',
  password: 'Password',
  password_confirm: 'Confirm Password',
  captcha: 'Captcha Code',
  legacy_login_hint: 'Click here for legacy login',
  required: 'Required',
  minlength_message: 'At least {{requiredLength}} characters',
  maxlength_message: 'At most {{requiredLength}} characters',
  new_password: 'New Password',
  new_password_confirm: 'Confirm New Password',
  length_message: 'Require {{requiredLength}} characters',
  verification_code_placeholder: 'Verification Code',
  confirm_password_incorrect: `Passwords don't match`,
  refresh_tooltip: 'Click to refresh',
  invalid_password: 'Incorrect account/password',
  invalid_captcha: 'Invalid Captcha',
  mobile_vcode_invalid: 'Invalid Mobile Verification Code',
  invalid_pattern: 'Invalid {{type}} format',
  reset_password_success: 'Reset Password Success',
  lang_zh_cn: '中文',
  lang_en: 'English',
  register_link_text: 'Need an account? Register now',
  login_link_text: 'Already have an account? Login now',
  register_complete_link_text: 'Already activated? Login now',
  switch_lang_hint: '点击切换至中文版',
  user_not_exist: 'Account does not exist',
  verify_now: 'Verify Now',
  submit: 'Submit',
  back_to_prev_step: 'Back to Previous Step',
  send_verification_code_hint:
    'Verification code will be sent to your mobile phone or your email',
  password_strength: 'Password Strength',
  medium: 'Medium',
  weak: 'Weak',
  strong: 'Strong',
  password_strength_too_low: 'Password strength too low',
  register_success_info_line_1:
    'We are reviewing the information you submitted. After the audit, a sales representative will contact and help you experience the best container services.',
  register_success_info_line_2: `If you are eager to activate your trail as soon as possible, please call our customer service <span class='phone-number'>(4006-252-832)</span> for consultation.`,
  timeout_error: 'Request failed due to timeout. Please retry later.',
  invalid_register_username: 'Username is not available',
  invalid_register_email: 'Email is not available',
  invalid_register_mobile: 'Mobile is not available',
  invalid_register_vcode: 'Invalid VCode',
  unknown_issue: 'Unknown Issue',
  call: 'Phone call',

  // REGISTERING
  note: 'Note',
  register_info_line_1: `We don't provide VPN services. Please comply with national laws and regulations`,
  register_info_line_2:
    'Fill out your genuine business details so that your registration can be reviewed and approved as soon as possible',
  register_info_line_3: 'All fields are mandatory',
  img_captcha: 'Image Captcha',
  verification_code: 'Mobile Verification Code',
  realname: 'Your Full Name',
  company: 'Company Name',
  city: 'City',
  industry: 'Industry',
  position: 'Your Position',
  apply_mode: 'Applying Mode',
  apply_service: 'Expected Services to Apply',
  informed_way: 'How do you get to know about us?',
  service_mode_list_link: 'Click to browse functions for all the service modes',
  send_verification_code: 'Send',
  waiting_send_verification_code: 'Resend',
  send_verification_code1: 'Send',
  waiting_send_verification_code1: 'Resend',
  mobile_exist: 'Mobile number already in use',
  enterprise: 'Enterprise',
  'data-center': 'Data Center',
  professional: 'Professional',
  education: 'Education',
  'e-commerce': 'E-Commerce',
  'mobile-medical': 'Mobile Medical',
  entertainment: 'Entertainment',
  locale: 'Locale Life',
  'social-network': 'Social Network',
  'estate-service': 'Estate Service',
  'ads-marketing': 'ADs Marketing',
  'mobile-internet': 'Mobile Internet',
  travel: 'Travel',
  game: 'Video Games',
  'tool-software': 'Tool Software',
  'internet-finance': 'Internet Finance',
  'enterprise-service': 'Enterprise Service',
  others: 'Others',
  'cloud-strategy-consultancy': 'Cloud Strategy Consultancy',
  'cloud-solution-architect': 'Cloud Solution architect',
  'application-migrating-deployment': 'Application Migration Deployment',
  'support-operational-optimization': 'Operation Optimization Support',
  'professional-training': 'Professional Training',
  'social-site': 'Social Media',
  'offline-activity': 'Offline Activities',
  'media-reports': 'Media',
  forums: 'Forums',
  'search-engine': 'Search Engine',
  recommended: 'Friend Recommendation',
  register_success: 'Thank you for registering',
  user_not_active: 'Account is not yet activated',
  oidc_auth_login: 'Validating login authorization...',
  license_not_activated: 'license not activated',
  license_activated: 'license activated',
  license_contact:
    'Please contact the administrator to renew your enterprise license.',
  license_contact1:
    'Please contact the administrator to activate your enterprise license.',
  logout: 'logout',
  network_ex: 'Network anomaly, please try again later…',
  license_info_1_1: 'Welcome to ',
  license_info_1_2: 'website in the following ways to contact us:',
  license_info_2_1:
    'Please enter the license key which matches the Platform ID to activate the full functions of ',
  license_info_2_2: ' platform',
  license_info_4: 'The license has expired, please renew the license key',
  license_info_5:
    'To ensure that you can receive the email when the license will expired, your reminder mailbox has been set to:',
  license_info_6_1:
    'At the end of the trial period you must renew the license to continue using ',
  license_info_6_2: ' platform.',
  license_info_6_3:
    'At the end of the professional period you must renew the license to continue using ',
  license_info_7_1: 'The functions of ',
  license_info_7_2:
    ' can not be used, please contact the administrator to renew the license.',
  //
  license_activate: 'Active License',
  license_is_outdata: 'The license has expired.',
  product_license: 'Product License',
  license_has_activated: 'Congratulations, the license is activated!',
  activate_license: 'Activate License',
  license_disabled: 'The license is invalid',
  license_not_match_plateform:
    'The license key does not match the Platform ID.',
  use_useful_license: 'Please use a valid license key',
  please_update_license: 'Please renew the license key',
  please_edit_matched_license: 'Please enter the license key',
  license_manage: 'License Management',
  product_name: 'Product Name',
  customer_name: 'Enterprise Name',
  platform: 'Platform',
  license_type: 'License Type',
  license_code: 'License Key',
  license_outdata: 'Expiration Time',
  license_outdata_email: 'Reminder Mailbox',
  license_outdata_user: 'Reminder Staff',
  release: 'Professional ',
  beta: 'Trial',
  license_expire: 'Professional Licence Validity',
  surplus: 'Remaining Time',
  update_license: 'Renew License',
  to_update_license: 'Renew License',
  only_super_user: 'Administrator',
  all_member: 'Staff',
  beta_day_expire: 'Trial License Validity',

  expired: 'expired',
  expired_msg: 'license code is expired',
  invalid_registration_code: 'invalid registration code',
  invalid_registration_code_msg: 'registry code is invalid',
  invalid_license_code: 'invalid license code',
  invalid_license_code_msg: 'license code is invalid',
  database_error: 'database error',
  database_error_msg: 'Database error',
  invalid_args: 'invalid args',
  invalid_args_msg: 'Invalid parameters were passed',
  permission_denied: 'permission denied',
  permission_denied_msg: 'Current user has no permission to perform the action',

  cmb_login_text: 'UAA Login',
  other_login_methods: 'Or',
  invalid_grant_type: 'Invalid grant type',
  configs_is_invalid: 'Invalid configs',
  refresh_token_not_exist: 'Refresh token not exist',
  access_token_not_exist: 'access_token not exist',
  id_token_not_exist: 'id_token not exist',
  org_name_does_not_exist: 'Org name not exist',

  first_login_modify_password:
    'Login for the first time to change the password',
  old_password: 'Old Passwrod',
  save: 'Save',
  authentication_credentials_not_provided:
    'Authentication credentials not provided',
  warning_max_length: 'Max length is {{length}}',
  warning_min_length: 'Min length is {{length}}',
  password_not_same: 'Two password not consistent',
  update_password_error: 'Update Password Error',
  password_login_reset_password_tip:
    'For your safety, please log in for the first time and change your password!',
  passowrd_pattern_type_a_error:
    'It must include a-z or A-Z,0-9 and other char at the same time',
  password_pattern_full_tip:
    'At least 8,contain alpha,numeric and special symbols',
  retrieve_password: 'Forgot Password',
  to_login: 'Go to login',
  login_pass_platform:
    'You can now go to log in the Paas platform account with your new password',
  please_confirm_password: 'Please confirm your password',
  please_enter_new_password: 'Please enter a new password',
  captcha_can_not_be_none: 'verification code must be filled',
  captcha_has_send_to: 'The verification code has been sent to {{email}}',
  enter_verification_code: 'please enter verification code',
  next_step: 'Next',
  enter_account: 'Please enter your account ID',
  confirm: 'Confirm',

  error_code: 'Verification code error',
  error_email: 'Email is not exits',
  email_send_error: 'Email send failed',
  error_timeout: 'Operation timeout',
  password_same: 'Your new password cannot be the same as the old password.',
  error_reset_self: 'Can not reset your password',
  error_delete_self: 'Can not delete yourself',
  reset_password_error: 'Reset password error',
  error_captch_code: 'Captch error',
  user_is_ldap: 'User is from IDAP , does not support current operation',
  error_old_password: 'Old password error',
};
