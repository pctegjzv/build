export { RegisterComponent } from './register/register.component';
export {
  RegisterCompleteComponent,
} from './register/register-complete.component';
export { LoginComponent } from './login/login.component';
export {
  SendCaptchaCodeComponent,
} from './forget-password/send-captcha-code.component';
export {
  ResetPasswordComponent,
} from './forget-password/reset-password.component';
export {
  ResetPasswordCompleteComponent,
} from './forget-password/reset-password-complete.component';
export {
  ThirdPartyLoginErrorComponent,
} from './login/third_party/third-party-login-error.component';
export { OidcLoginComponent } from './login/third_party/oidc-login.component';
export {
  LicenseActivationComponent,
} from './license-activation/license-activation.component';
export { GuideComponent } from './login/guide/guide.component';
