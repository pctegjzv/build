import {
  Component,
  Injector,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { Router } from '@angular/router';

import { BaseFormComponent } from '../common/base-form-component';
import { CaptchaImgComponent } from '../common/captcha-img.component';
import {
  FormControlFragmentComponent,
  FormControlFragmentDeclaration,
} from '../common/form-control-fragment.component';
import { HttpService } from '../common/http.service';
import { TranslateService } from '../common/translate.service';
import { LandingValidators } from '../common/validators';

interface SendCaptchaCodeFormModel {
  account: string;
  captcha: string;
}

@Component({
  styleUrls: [
    '../common/style/form.style.scss',
    '../common/style/utility.style.scss',
    './send-captcha-code.component.scss',
  ],
  templateUrl: 'send-captcha-code.component.html',
})
export class SendCaptchaCodeComponent
  extends BaseFormComponent<SendCaptchaCodeFormModel>
  implements OnInit {
  @ViewChildren(FormControlFragmentComponent)
  fragments: QueryList<FormControlFragmentComponent>;
  submitting: boolean;
  sendVerificationCodeHint: string;
  errorMessage: string;

  constructor(
    private injector: Injector,
    private http: HttpService,
    private translate: TranslateService,
    private router: Router,
  ) {
    super();
  }

  ngOnInit(): void {
    this.sendVerificationCodeHint = 'send_verification_code_hint';
    this.formModel = { account: '', captcha: '' };
    this.buildForm();
  }

  protected get injectorInstance() {
    return this.injector;
  }

  /**
   * Return 'raw' FormControlFragment.
   * The FormControlFragment in the return list should be decorated with {@link FormControl}.
   * {@see buildForm}
   */
  protected get formFragmentDeclarations(): FormControlFragmentDeclaration[] {
    return [
      {
        name: 'account',
        type: 'text',
        placeholder: 'mobile_or_email',
        validatorFns: [
          LandingValidators.required,
          LandingValidators.minLength(4),
        ],
      },
      {
        name: 'captcha',
        type: 'text',
        placeholder: 'captcha',
        validatorFns: [
          LandingValidators.required,
          LandingValidators.fixedLength(4),
        ],
        addon: 'captcha',
      },
    ];
  }

  get submitDisabled(): boolean {
    return !this.formGroup.valid || this.submitting;
  }

  sendCaptchaCode() {
    if (this.submitDisabled) {
      return;
    }
    const payload = this.formModel;
    this.submitting = true;
    return this.http
      .request('/ajax/user/sendCaptchaCode', {
        method: 'POST',
        body: payload,
      })
      .then(res => {
        if (res.captcha) {
          (this.getCaptchaFragment().addon as CaptchaImgComponent).captchaSrc =
            res.image_url;
          throw [{ code: 'invalid_captcha' }];
        } else {
          // redirect to reset page
          return this.router.navigate([
            'forget-password/reset',
            this.formModel.account,
          ]);
        }
      })
      .catch((errors: any[]) => {
        this.errorMessage = '';
        this.formControlFragmentMap['account'].error = errors.some(
          error => 'user_not_exist' === error.code,
        )
          ? this.translate.get('user_not_exist')
          : null;
        this.formControlFragmentMap['captcha'].error = errors.some(
          error => 'invalid_captcha' === error.code,
        )
          ? this.translate.get('invalid_captcha')
          : null;
        if (
          !(
            this.formControlFragmentMap['account'].error ||
            this.formControlFragmentMap['captcha'].error
          )
        ) {
          this.errorMessage = errors[0].message;
        }
      })
      .then(() => {
        this.submitting = false;
      });
  }

  private getCaptchaFragment() {
    return this.fragments.find(
      item =>
        item.formControlFragment === this.formControlFragmentMap['captcha'],
    );
  }
}
