import { Component, OnInit } from '@angular/core';

import { TranslateService } from './common/translate.service';

@Component({
  selector: 'rld-language-picker',
  template: `
    <div class="rld-language-picker"
         [auiTooltip]="'switch_lang_hint' | translate"
         [auiTooltipPosition]="'bottom end'">
      <i class="fa fa-globe"></i>
      <div (click)="switchLang()">
        {{ nextLang | translate }}
      </div>
    </div>
  `,
  styles: [
    `
      .rld-language-picker {
        display: flex;
        align-items: center;
        font-size: 18px;
        color: #888;
        cursor: pointer;
      }

      .rld-language-picker:hover {
        color: #555;
      }

      i {
        margin-right: 0.5em;
      }
    `,
  ],
})
export class LanguagePickerComponent implements OnInit {
  nextLang: string;

  constructor(private translate: TranslateService) {}

  ngOnInit(): void {
    this.nextLang = `lang_${
      this.translate.currentLang === 'zh_cn' ? 'en' : 'zh_cn'
    }`;
  }

  switchLang() {
    this.translate.toggleLang();
    this.nextLang = `lang_${
      this.translate.currentLang === 'zh_cn' ? 'en' : 'zh_cn'
    }`;
  }
}
