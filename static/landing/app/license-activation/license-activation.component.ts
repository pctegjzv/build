import { Component, OnDestroy, OnInit } from '@angular/core';
import { Headers } from '@angular/http';
import { MessageService, NotificationService } from 'alauda-ui';
import { forEach } from 'lodash';

import { HttpService } from '../common/http.service';
import { TranslateService } from '../common/translate.service';
import { RcProfile } from '../types';

interface PlatformModel {
  registry_code: string;
  product_name: string;
}

@Component({
  templateUrl: 'license-activation.component.html',
  styleUrls: ['license-activation.component.scss'],
})
export class LicenseActivationComponent implements OnInit, OnDestroy {
  licenseCode = '';
  isSubmitting = false;
  licenseActivatedOrder: '1' | '2';
  isFirstActivate: boolean;
  isAdmin: boolean;
  profile: RcProfile;
  platformInfo: PlatformModel;
  loading: boolean;

  constructor(
    private http: HttpService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
  ) {}

  async ngOnInit() {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    forEach(
      document.getElementsByClassName('dis-in-license'),
      (dom: HTMLElement) => {
        dom.style.display = 'none';
      },
    );
    this.loading = true;
    const headers: Headers = new Headers();
    headers.append('X-Requested-With', 'XMLHttpRequest');

    headers.append('RUBICK-AJAX-REQUEST', 'true');
    try {
      this.profile = await this.getAuthProfile();
      this.isAdmin = this.profile['is_admin'];

      const [platformInfo, licenseInfo] = await Promise.all([
        this.http.request(
          `/ajax/license-auth/${this.profile.username}/registry-code`,
          {
            headers,
          },
        ),
        this.http.request(
          `/ajax/license-auth/${this.profile.username}/license-code`,
          {
            headers,
          },
        ),
      ]);

      this.platformInfo = platformInfo;

      this.licenseActivatedOrder = !licenseInfo.license_code ? '1' : '2';
      this.isFirstActivate = !licenseInfo.license_code;
    } catch (errors) {
      if (errors) {
        this.auiNotificationService.error({
          content: this.translateService.get(errors[0].message),
        });
      }
    } finally {
      this.loading = false;
    }
  }

  getAuthProfile(_cache: boolean = true) {
    const headers = new Headers();
    headers.append('RUBICK-AJAX-REQUEST', 'true');

    return this.http.request('/ajax/auth/profile', {
      headers,
    });
  }

  ngOnDestroy() {
    forEach(
      document.getElementsByClassName('dis-in-license'),
      (dom: HTMLElement) => {
        dom.style.display = 'block';
      },
    );
  }

  logout() {
    location.href = '/ap/logout';
  }

  async licenseValidate(): Promise<void> {
    if (this.btnDisabled || this.isSubmitting) {
      return;
    }

    this.isSubmitting = true;

    const headers: Headers = new Headers();
    headers.append('X-Requested-With', 'XMLHttpRequest');
    headers.append('RUBICK-AJAX-REQUEST', 'true');

    try {
      await this.http.request('/ajax/landing/license-activation', {
        method: 'POST',
        headers,
        body: {
          license_code: this.licenseCode,
        },
      });
      //firstActivate is notification
      this.auiMessageService.success({
        content: this.translateService.get('license_has_activated'),
      });

      localStorage.setItem(
        'licenseActivatedOrder',
        `${this.licenseActivatedOrder}`,
      );

      setTimeout(() => {
        location.replace(`/console/license`);
      }, 1000);
    } catch (errors) {
      if (errors) {
        const errMsg =
          this.translateService.get(errors[0].code + '_msg') ===
          errors[0].code + '_msg'
            ? errors[0].message
            : errors[0].code + '_msg';
        this.auiNotificationService.error({
          title: this.translateService.get(errors[0].code),
          content: this.translateService.get(errMsg),
        });
      }
    } finally {
      this.isSubmitting = false;
    }
  }

  get btnDisabled(): boolean {
    return this.isSubmitting || this.licenseCode.trim().length === 0;
  }
}
