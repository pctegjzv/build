import {
  AfterViewInit,
  Component,
  Inject,
  Injector,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { BaseFormComponent } from '../common/base-form-component';
import { CaptchaImgComponent } from '../common/captcha-img.component';
import {
  FormControlFragmentComponent,
  FormControlFragmentDeclaration,
} from '../common/form-control-fragment.component';
import { HttpService } from '../common/http.service';
import { TranslateService } from '../common/translate.service';
import { LandingValidators as Validators } from '../common/validators';
import { ENVIRONMENTS } from '../tokens';
import { Environments, LoginPattern, OidcConfig } from '../types';

const DEFAULT_NAMESPACE_KEY = 'alauda_default_namespace';

interface LoginFormModel {
  organization?: string; // This is actually the 'Account' field in the template when loginMode === 'sub'
  account: string; // 'Username' in 'sub' mode, and 'account' in 'root' mode
  password: string;
  captcha?: string;
}

// Login as a sub account or as a root account or a super mode for third part choose
enum LoginMode {
  sub,
  root,
}

const CAPTCHA_ERRORS = ['invalid_captcha'];

const PASSWORD_ERRORS = [
  'provided_credentials_not_correct',
  'incorrect_authentication_credentials',
];

@Component({
  styleUrls: [
    'login.component.scss',
    '../common/style/form.style.scss',
    '../common/style/utility.style.scss',
  ],
  templateUrl: 'login.component.html',
})
export class LoginComponent extends BaseFormComponent<LoginFormModel>
  implements OnInit, AfterViewInit, OnDestroy {
  @ViewChildren(FormControlFragmentComponent)
  fragments: QueryList<FormControlFragmentComponent>;
  LoginMode = LoginMode; // Enum cannot be declared in a class
  loginMode: LoginMode;
  loginMode$: Observable<LoginMode>;
  submitting: boolean;
  submitted: boolean;
  requiresCaptcha: boolean;
  defaultNamespace: string;
  _next = '';

  private loginModeSubscriber: Subscription;

  constructor(
    private injector: Injector,
    private auiNotificationService: NotificationService,
    private http: HttpService,
    private translate: TranslateService,
    private route: ActivatedRoute,
    private router: Router,
    @Inject(ENVIRONMENTS) public ENVIRONMENTS: Environments,
  ) {
    super();
  }

  ngOnInit(): void {
    this.loginMode$ = this.route.params.pipe(
      map(params => LoginMode[params['mode'] as string]),
    );

    this.defaultNamespace =
      this.ENVIRONMENTS.predefined_login_organization ||
      localStorage.getItem(DEFAULT_NAMESPACE_KEY);
    this.submitting = false;
    this.submitted = false;
    this.formModel = { organization: '', account: '', password: '' };

    this.buildForm();
    this.checkRequiresCaptcha();

    this.loginModeSubscriber = this.loginMode$.subscribe(mode => {
      this.setLoginMode(mode);
    });

    this.route.queryParams.pipe(take(1)).subscribe(params => {
      this._next = params['next'] || '';
    });
  }

  ngAfterViewInit() {
    // There is an Autofill bug that the password field will not trigger change event.
    // We need to manual focus it after page load.
    setTimeout(() => {
      (document.querySelector(
        `input[name='password']`,
      ) as HTMLInputElement).focus();
    });
  }

  ngOnDestroy(): void {
    this.loginModeSubscriber.unsubscribe();
  }

  get loginDisabled() {
    return this.submitted && (!this.formGroup.valid || this.submitting);
  }

  get headerInfo() {
    return this.loginMode === LoginMode.sub ? 'user_login' : 'org_login';
  }

  get loginSwitchLink() {
    return this.loginMode === LoginMode.sub ? '/login/root' : '/login/sub';
  }

  get loginSwitchLinkText() {
    return this.loginMode !== LoginMode.sub ? 'user_login' : 'org_login';
  }

  checkRequiresCaptcha() {
    return this.http
      .request('/ajax/landing/login/requires-captcha/')
      .then(({ result }) => {
        this.requiresCaptcha = result;
      })
      .catch(errors => {
        console.error('Refresh captcha failed: ' + JSON.stringify(errors));
      })
      .then(() => {
        if (
          this.requiresCaptcha &&
          !this.formControlFragmentMap['captcha'].error
        ) {
          return (this.getCaptchaFragment()
            .addon as CaptchaImgComponent).refresh();
        }
      });
  }

  /**
   * Switch between different login modes.
   */
  setLoginMode(mode: LoginMode) {
    if (this.loginMode !== mode) {
      this.loginMode = mode;
      this.resetForm();
    }
  }

  /**
   * Handles user login state
   */
  async login(): Promise<void> {
    this.submitted = true;
    if (this.loginDisabled) {
      return;
    }
    // Add all query parameters to the payload, including next
    const payload = Object.assign(
      {},
      this.formModel,
      this.route.snapshot.queryParams,
    );

    // login to django first, captcha request from django
    this.submitting = true;
    await this.http
      .request('/ajax/landing/login/', {
        method: 'POST',
        body: payload,
      })
      .then(({ url }) => {
        localStorage.setItem(
          DEFAULT_NAMESPACE_KEY,
          this.formModel.organization || this.formModel.account,
        );
        console.log('redirecting to ' + url);
        //only from login can tip  lincese modal
        localStorage.setItem('fromLogin', 'true');
        location.replace(url);
      })
      .catch((errors: any[]) => {
        if (errors.find(error => error.code === 'user_not_active')) {
          this.router.navigate(['/register-complete', { fromLogin: true }]);
        }

        console.error('login failed: ' + JSON.stringify(errors));

        // Show errors which are not in the error code list
        errors.forEach(error => {
          if (![...CAPTCHA_ERRORS, ...PASSWORD_ERRORS].includes(error.code)) {
            this.auiNotificationService.error({
              content: this.translate.get(error.code),
            });
          }
        });

        this.formControlFragmentMap['captcha'].error = errors.some(error =>
          CAPTCHA_ERRORS.includes(error.code),
        )
          ? this.translate.get('invalid_captcha')
          : null;

        this.formControlFragmentMap['password'].error = errors.some(error =>
          PASSWORD_ERRORS.includes(error.code),
        )
          ? this.translate.get('invalid_password')
          : null;

        this.submitting = false;

        // Whenever user inputs an incorrect password, his captcha will be updated.
        // Hence the current captcha input is invalid and we should reset it.
        if (this.formControlFragmentMap['password'].error) {
          this.formControlFragmentMap['captcha'].control.reset();
        }
        return this.checkRequiresCaptcha();
      });
  }

  /**
   * Reset form values, touched states, etc.
   */
  resetForm() {
    if (this.formGroup) {
      this.buildForm();
      this.formGroup.reset();

      this.organizationControl.setValue(this.defaultNamespace);
    }
  }

  protected get injectorInstance() {
    return this.injector;
  }

  protected get formFragmentDeclarations(): FormControlFragmentDeclaration[] {
    const self = this;
    const declarations: FormControlFragmentDeclaration[] = [
      {
        name: 'account',
        type: 'text',
        get placeholder() {
          return self.loginMode === LoginMode.sub ? 'username' : 'org_name';
        },
        validatorFns: [Validators.required],
        hidden:
          !!this.ENVIRONMENTS.predefined_login_organization &&
          self.loginMode === LoginMode.root,
      },
      {
        name: 'password',
        type: 'password',
        placeholder: 'password',
        validatorFns: [Validators.required],
      },
      {
        name: 'captcha',
        type: 'text',
        placeholder: 'captcha',
        validatorFns: [Validators.required],
        addon: 'captcha',
        get hidden() {
          return !self.requiresCaptcha;
        },
      },
    ];

    if (this.loginMode === LoginMode.sub) {
      declarations.unshift({
        name: 'organization',
        type: 'text',
        placeholder: 'org_name',
        validatorFns: [Validators.required],
        hidden: !!this.ENVIRONMENTS.predefined_login_organization,
      });
    }

    return declarations;
  }

  private getCaptchaFragment() {
    return this.fragments.find(
      item =>
        item.formControlFragment === this.formControlFragmentMap['captcha'],
    );
  }

  private get organizationControl(): AbstractControl {
    const controlName =
      this.loginMode === LoginMode.sub ? 'organization' : 'account';
    return this.formControlFragmentMap[controlName].control;
  }

  tpLogin(item: OidcConfig) {
    localStorage.setItem('curr_sso_name', item.source_id);
    location.href = item.auth_url;
  }

  isCodeLoginPattern() {
    return this.ENVIRONMENTS.login_pattern === LoginPattern.CODE;
  }
}
