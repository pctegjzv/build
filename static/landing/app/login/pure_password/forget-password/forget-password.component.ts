import {
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Headers } from '@angular/http';
import { NotificationService } from 'alauda-ui';
import { get } from 'lodash';

import { HEADER_AJAX_REQUEST, HttpService } from '../../../common/http.service';
import { CmbPasswordPattern } from '../../../common/password-strength.component';
import { TranslateService } from '../../../common/translate.service';
import { ENVIRONMENTS } from '../../../tokens';
import { Environments } from '../../../types';

const patterns = [/\.{,0}/, /\.*/];

@Component({
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss'],
})
export class ForgetPasswordComponent implements OnInit {
  formModel: {
    username?: string;
    password?: string;
    confirmPassword?: string;
    input_code?: string;
    id?: string;
    email?: string;
  } = {};
  validatorPattern = patterns[+true];
  loading = false;
  codeLoading = false;
  step = 1;
  remains = 0;
  org_name: string;
  pwdPattern = CmbPasswordPattern;
  @ViewChild('step1')
  step1Form: NgForm;
  @ViewChild('step2')
  step2Form: NgForm;

  constructor(
    private http: HttpService,
    private cdr: ChangeDetectorRef,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
    @Inject(ENVIRONMENTS) public ENVIRONMENTS: Environments,
  ) {}

  ngOnInit() {
    this.org_name = get(
      this.ENVIRONMENTS,
      'sso_configuration.default_org_name',
    );
    if (!this.org_name) {
      console.error('sso config:: get default_org_name failed!');
    }
  }

  get codeInWaiting() {
    return this.remains > 0;
  }

  resetPattern() {
    this.validatorPattern = patterns[+true];
  }

  async confrim() {
    if (this.step2Form.invalid || this.loading) {
      return;
    }
    this.step2Form.onSubmit(null);

    try {
      this.loading = true;
      await this.http.request(
        `/ajax/orgs/${this.org_name}/accounts/${
          this.formModel.username
        }/update_password`,
        {
          method: 'POST',
          headers: new Headers({
            [HEADER_AJAX_REQUEST]: 'true',
          }),
          body: this.formModel,
        },
      );
      this.step = 3;
    } catch (e) {
      if (e[0].code === 'error_code') {
        return (this.validatorPattern = patterns[+false]);
      }
      this.auiNotificationService.error({
        content: this.translateService.get(e[0].code),
      });
    } finally {
      this.loading = false;
    }
  }

  get transferEmail() {
    const originEmail = this.formModel.email || '';
    const baseLength = originEmail.indexOf('@');
    if (baseLength < 4) {
      return originEmail;
    }
    return `${originEmail.slice(0, 2)}*****${originEmail.slice(
      baseLength - 1,
    )}`;
  }

  async sendCode(e: Event) {
    e.preventDefault();
    if (this.remains > 0 || this.codeLoading) {
      return (this.validatorPattern = patterns[+false]);
    }
    try {
      this.codeLoading = true;
      const { email } = await this.http.request(
        `/ajax/orgs/${this.org_name}/accounts/${
          this.formModel.username
        }/send-captch-code`,
        {
          method: 'POST',
          headers: new Headers({
            [HEADER_AJAX_REQUEST]: 'true',
          }),
          body: {
            id: this.formModel.id,
          },
        },
      );
      this.formModel.email = email;
      this.remains = 60;
      const timerId = setInterval(() => {
        this.remains -= 1;
        if (this.remains === 0) {
          clearInterval(timerId);
        }
      }, 1000);
      this.cdr.detectChanges();
    } catch (e) {
      this.auiNotificationService.error({
        content: this.translateService.get(e[0].code),
      });
    } finally {
      this.codeLoading = false;
    }
  }

  async next(_account: string) {
    if (this.step1Form.invalid || this.loading) {
      return;
    }
    this.step1Form.onSubmit(null);

    try {
      this.loading = true;
      const { id } = await this.http.request(
        `/ajax/orgs/${this.org_name}/accounts/${
          this.formModel.username
        }/validation`,
        {
          method: 'GET',
          headers: new Headers({
            [HEADER_AJAX_REQUEST]: 'true',
          }),
        },
      );
      this.formModel.id = id;
      this.step = 2;
    } catch (e) {
      if (e[0].code === 'user_not_exist') {
        return (this.validatorPattern = patterns[+false]);
      }
      this.auiNotificationService.error({
        content: this.translateService.get(e[0].code),
      });
    } finally {
      this.loading = false;
    }
  }

  get cPwdPattern() {
    return this.formModel.password === this.formModel.confirmPassword
      ? patterns[+true]
      : patterns[+false];
  }
}
