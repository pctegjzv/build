import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { DialogService, NotificationService } from 'alauda-ui';

import { HttpService } from '../../common/http.service';
import { CmbPasswordPattern } from '../../common/password-strength.component';
import { TranslateService } from '../../common/translate.service';

@Component({
  templateUrl: './pure-password.component.html',
  styleUrls: ['./pure-password.component.scss'],
})
export class PurePasswordLoginComponent implements OnInit {
  formModel: {
    account?: string;
    password?: string;
    confirmPassword?: string;
  } = {};
  requiresCaptcha: boolean;
  loading = false;
  pwdLoading = false;
  pwdPattern = CmbPasswordPattern;
  @ViewChild('form')
  form: NgForm;
  @ViewChild('password')
  password: NgModel;
  @ViewChild('template')
  template: TemplateRef<any>;

  constructor(
    private http: HttpService,
    private auiNotificationService: NotificationService,
    private dialogService: DialogService,
    private translate: TranslateService,
  ) {}

  ngOnInit() {}

  async login() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    const payload = Object.assign({}, this.form.value, {
      grant_type: 'password',
    });

    try {
      this.loading = true;
      const { url, isFirstLogin } = await this.http.request(
        '/ajax/landing/login/password',
        {
          method: 'POST',
          body: payload,
        },
      );
      if (isFirstLogin) {
        return this.dialogService.open(this.template);
      }
      console.log('redirecting to ' + url);
      //only from login can tip  lincese modal
      localStorage.setItem('fromLogin', 'true');
      location.replace(url);
    } catch (errors) {
      console.error('login failed: ' + JSON.stringify(errors));
      if (errors && errors[0].code && errors[0].code === 'unauthorized') {
        this.auiNotificationService.error({
          title: this.translate.get('invalid_password'),
        });
        return;
      }

      this.auiNotificationService.error({
        title: this.translate.get(errors[0].code),
        content: this.translate.get(errors[0].message),
      });
    } finally {
      this.loading = false;
    }
  }

  async saveNewPassword(old_password: string, password: string) {
    try {
      this.pwdLoading = true;
      const { url } = await this.http.request(
        '/ajax/landing/login/password/change-password',
        {
          method: 'PUT',
          body: {
            old_password,
            password,
          },
        },
      );
      console.log('redirecting to ' + url);
      //only from login can tip  lincese modal
      localStorage.setItem('fromLogin', 'true');
      location.replace(url);
    } catch (errors) {
      console.error('login failed: ' + JSON.stringify(errors));
      if (errors && errors[0].code && errors[0].code === 'unauthorized') {
        this.auiNotificationService.error({
          title: this.translate.get('invalid_password'),
        });
        return;
      }

      this.auiNotificationService.error({
        title: this.translate.get(errors[0].code),
        content: this.translate.get(errors[0].message),
      });
    } finally {
      this.pwdLoading = false;
    }
  }

  get cPwdPattern() {
    return this.formModel.password === this.formModel.confirmPassword
      ? /\.*/
      : /\.{,0}/;
  }
}
