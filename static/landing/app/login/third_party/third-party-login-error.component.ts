import { Component } from '@angular/core';

/**
 * Base routing component container for third-party-login-error
 */
@Component({
  styleUrls: ['third-party-login-error.component.scss'],
  templateUrl: 'third-party-login-error.component.html',
})
export class ThirdPartyLoginErrorComponent {}
