import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { TranslateService } from '../common/translate.service';

@Component({
  styleUrls: [
    '../common/style/form.style.scss',
    '../common/style/utility.style.scss',
  ],
  template: `
<div class="form-container" style="max-width: 600px;">
  <div class="form-header">
    <i *ngIf="!redirectedFromLogin" class="fa fa-check-circle" style="color: #72c63d" aria-hidden="true"></i>
    {{ infoHeader | translate }}
  </div>
  <div style="margin-bottom: 24px;" [innerHTML]="registerSuccessInfo">
  </div>
</div>
`,
})
export class RegisterCompleteComponent implements OnInit {
  registerSuccessInfo: string;
  infoHeader: string;
  redirectedFromLogin: boolean;

  constructor(
    private translate: TranslateService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.redirectedFromLogin = !!this.route.snapshot.params['fromLogin'];
    this.infoHeader = this.redirectedFromLogin
      ? 'user_not_active'
      : 'register_success';

    this.registerSuccessInfo = `
    <p>${this.translate.get('register_success_info_line_1')}</p>
    <p>${this.translate.get('register_success_info_line_2')}</p>
    `;
  }
}
