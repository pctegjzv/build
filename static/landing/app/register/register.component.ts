import {
  AfterViewInit,
  Component,
  Injector,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationService } from 'alauda-ui';
import { Observable, Subject, merge as oMerge } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  merge,
  pairwise,
  switchMap,
  tap,
} from 'rxjs/operators';

import { BaseFormComponent } from '../common/base-form-component';
import {
  FormControlFragment,
  FormControlFragmentComponent,
} from '../common/form-control-fragment.component';
import { HttpService } from '../common/http.service';
import { TranslateService } from '../common/translate.service';
import { LandingValidators as Validators } from '../common/validators';
import { VerificationCodeButtonComponent } from '../common/verification-code-button.component';

// Form model to be sent to the API
interface RegisterFormModel {
  username: string;
  email: string;
  password: string;
  passwordConfirm: string;
  mobile: string;
  realname: string;
  captcha: string;
  code: string;
  company: string;
  city: string;
  industry: string;
  position: string;
  apply_mode: string;
  informed_way: string;
}

const APPLY_MODE_OPTIONS = ['professional', 'enterprise', 'data-center'];

const INDUSTRY_OPTIONS = [
  'education',
  'e-commerce',
  'mobile-medical',
  'entertainment',
  'locale',
  'social-network',
  'estate-service',
  'ads-marketing',
  'mobile-internet',
  'travel',
  'game',
  'tool-software',
  'internet-finance',
  'enterprise-service',
  'others',
];

const APPLY_SERVICE_OPTIONS = [
  'cloud-strategy-consultancy',
  'cloud-solution-architect',
  'application-migrating-deployment',
  'support-operational-optimization',
  'professional-training',
];

const INFORMED_WAY_OPTIONS = [
  'social-site',
  'offline-activity',
  'media-reports',
  'forums',
  'search-engine',
  'recommended',
  'others',
];

@Component({
  styleUrls: [
    'register.component.scss',
    '../common/style/form.style.scss',
    '../common/style/utility.style.scss',
  ],
  templateUrl: 'register.component.html',
})
export class RegisterComponent extends BaseFormComponent<RegisterFormModel>
  implements OnInit, AfterViewInit {
  @ViewChildren(FormControlFragmentComponent)
  fragments: QueryList<FormControlFragmentComponent>;
  submitting = false;

  constructor(
    private injector: Injector,
    private http: HttpService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private router: Router,
  ) {
    super();
  }

  get registerDisabled() {
    return !this.formGroup.valid || this.submitting;
  }

  protected get injectorInstance() {
    return this.injector;
  }

  protected get formFragmentDeclarations(): FormControlFragment[] {
    const confirmPasswordValidator = (
      control: AbstractControl,
    ): { [key: string]: any } => {
      const password =
        this.formControlFragmentMap['password'] &&
        this.formControlFragmentMap['password'].control.value;
      return control.value !== password ? { confirmPassword: true } : null;
    };

    return [
      {
        name: 'username',
        type: 'text',
        placeholder: 'org_name',
        validatorFns: [
          Validators.username,
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(30),
        ],
      },
      {
        name: 'email',
        type: 'email',
        placeholder: 'email',
        validatorFns: [
          Validators.email,
          Validators.required,
          Validators.minLength(4),
        ],
      },
      {
        name: 'password',
        type: 'password',
        placeholder: 'password',
        validatorFns: [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(20),
          Validators.passwordStrength(45), // medium
        ],
        addon: 'password-strength',
      },
      {
        name: 'passwordConfirm',
        type: 'password',
        placeholder: 'password_confirm',
        validatorFns: [
          confirmPasswordValidator,
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(20),
        ],
      },
      {
        name: 'mobile',
        type: 'text',
        placeholder: 'mobile_no',
        validatorFns: [
          Validators.required,
          Validators.minLength(7),
          Validators.maxLength(15),
        ],
      },
      {
        name: 'captcha',
        type: 'text',
        placeholder: 'img_captcha',
        validatorFns: [Validators.required, Validators.fixedLength(4)],
        addon: 'captcha',
      },
      {
        name: 'code',
        type: 'text',
        placeholder: 'verification_code',
        validatorFns: [Validators.required, Validators.fixedLength(6)],
        addon: 'verification-code',
      },
      {
        name: 'realname',
        type: 'text',
        placeholder: 'realname',
        validatorFns: [Validators.required],
      },
      {
        name: 'company',
        type: 'text',
        placeholder: 'company',
        validatorFns: [Validators.required],
      },
      {
        name: 'city',
        type: 'text',
        placeholder: 'city',
        validatorFns: [Validators.required],
      },
      {
        name: 'industry',
        type: 'dropdown',
        options: INDUSTRY_OPTIONS,
        placeholder: 'industry',
        validatorFns: [Validators.required],
      },
      {
        name: 'position',
        type: 'text',
        placeholder: 'position',
        validatorFns: [Validators.required],
      },
      {
        name: 'apply_mode',
        type: 'dropdown',
        options: APPLY_MODE_OPTIONS,
        placeholder: 'apply_mode',
        validatorFns: [Validators.required],
      },
      {
        name: 'apply_service',
        type: 'dropdown',
        multiple: true,
        options: APPLY_SERVICE_OPTIONS,
        placeholder: 'apply_service',
        validatorFns: [Validators.required],
      },
      {
        name: 'informed_way',
        type: 'dropdown',
        options: INFORMED_WAY_OPTIONS,
        placeholder: 'informed_way',
        validatorFns: [Validators.required],
      },
    ];
  }

  ngOnInit(): void {
    this.formModel = <RegisterFormModel>{};
    this.buildForm();
  }

  ngAfterViewInit(): void {
    // Update passwordConfirm error message when password changes
    this.formControlFragmentMap['password'].control.valueChanges
      .pipe(pairwise())
      .subscribe(([prev, current]) => {
        if (current !== prev) {
          setTimeout(() => {
            this.setFragmentError('passwordConfirm');
          });
        }
      });

    const getVerificationCodeButton = () =>
      this.getFragment('code').addon as VerificationCodeButtonComponent;

    const mobileControl = this.formControlFragmentMap['mobile'].control;
    const captchaControl = this.formControlFragmentMap['captcha'].control;

    // Bind the mobile number to the verification code addon
    mobileControl.valueChanges.subscribe(
      value => (getVerificationCodeButton().mobile = value),
    );

    // Bind the captcha to the verification code addon
    captchaControl.valueChanges.subscribe(
      value => (getVerificationCodeButton().captcha = value),
    );

    const mobileBlurStream: Observable<any> = this.getFragment('mobile')
      .inputBlur.asObservable()
      .pipe(map(() => mobileControl.value));

    const verifyingMobileRequests = new Subject();
    // Add a new validator to check the mobile number existence
    mobileControl.valueChanges
      .pipe(
        debounceTime(1000),
        merge(mobileBlurStream),
        distinctUntilChanged(),
        filter(
          () =>
            mobileControl.value &&
            mobileControl.value.length > 6 &&
            mobileControl.value.length < 16,
        ),
        tap(() => {
          this.formControlFragmentMap['mobile'].verifying = true;
          verifyingMobileRequests.next();
        }),
        switchMap(() => this.checkMobileExist(mobileControl.value)),
      )
      .subscribe(({ detail }) => {
        if (detail === 'mobile_exist') {
          mobileControl.setErrors({
            mobile_exist: true,
          });
          this.formControlFragmentMap['mobile'].verified = false;
        } else {
          this.formControlFragmentMap['mobile'].verified = true;
        }
        this.formControlFragmentMap['mobile'].verifying = false;
        this.updateFragmentErrorMessage('mobile');
        verifyingMobileRequests.next();
      });

    oMerge(
      verifyingMobileRequests,
      mobileControl.statusChanges,
      captchaControl.statusChanges,
    ).subscribe(() => {
      getVerificationCodeButton().formValid =
        !this.formControlFragmentMap['mobile'].verifying &&
        this.formControlFragmentMap['mobile'].verified &&
        mobileControl.valid &&
        captchaControl.valid;
    });

    getVerificationCodeButton().requests.subscribe((res: any) => {
      if (res.captcha) {
        (<any>this.getFragment('captcha').addon).captchaSrc = res.image_url;
        this.formControlFragmentMap['captcha'].error = this.translate.get(
          'invalid_captcha',
        );
      } else if (res.statusCode === '160042') {
        this.formControlFragmentMap['mobile'].error = this.translate.get(
          'invalid_register_mobile',
        );
      } else {
        this.formControlFragmentMap['captcha'].error = '';
        this.formControlFragmentMap['mobile'].error = '';
      }
    });
  }

  register() {
    if (this.registerDisabled) {
      return;
    }

    this.submitting = true;

    this.http
      .request('/ap/register', {
        method: 'POST',
        body: this.formModel,
      })
      .then(() => {
        return this.router.navigate(['/register-complete']);
      })
      .catch(errors => {
        if (errors instanceof Array) {
          errors.forEach((error: any) => {
            switch (error.code) {
              case 'username_already_exist':
                this.setFragmentError('username', {
                  register_username: true,
                });
                break;
              case 'mobile_already_exist':
                this.setFragmentError('mobile', {
                  regiser_mobile: true,
                });
                break;
              case 'email_already_exist':
                this.setFragmentError('email', {
                  register_email: true,
                });
                break;
              case 'mobile_vcode_invalid':
                this.setFragmentError('code', {
                  register_mobile_vcode_invalid: true,
                });
                this.auiNotificationService.error({
                  content: this.translate.get('mobile_vcode_invalid'),
                });
                break;
              default:
                this.auiNotificationService.error({
                  content: this.translate.get(error.code),
                });
                break;
            }
          });
        }
      })
      .then(() => {
        this.submitting = false;
      });
  }

  private checkMobileExist(mobile: string) {
    const search = HttpService.buildURLSearchParams({
      mobile,
    });
    return this.http.request('/auth/mobile/exist', { search });
  }

  private getFragment(name: string) {
    return this.fragments.find(
      item => item.formControlFragment === this.formControlFragmentMap[name],
    );
  }
}
