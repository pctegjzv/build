/**
 * Created by liudong on 2017/5/31.
 */
import { InjectionToken } from '@angular/core';

import { Environments, RcAccount } from './types';

export const ENVIRONMENTS = new InjectionToken<Environments>('ENVIRONMENTS');
export const ACCOUNT = new InjectionToken<RcAccount>('ACCOUNT');
