import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';

import EN from './en.i18n';
import ZH_CN from './zh_cn.i18n';

type Locale = 'zh_cn' | 'en';

class LoginTranslateLoader implements TranslateLoader {
  getTranslation(lang: Locale): Observable<any> {
    if (lang === 'zh_cn') {
      return of(ZH_CN);
    } else {
      return of(EN);
    }
  }
}

// This has to be configured at the root level
export const LoginTranslateModule = TranslateModule.forRoot({
  loader: {
    provide: TranslateLoader,
    useClass: LoginTranslateLoader,
  },
});
