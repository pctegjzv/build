/**
 * Created by liudong on 2017/5/31.
 */
export declare interface Environments {
  debug: boolean;
  alauda_image_index: string;
  lycan_url: string;
  is_private_deploy_enabled: boolean;
  private_deploy_password_pattern_type: string;
  third_party_login_type: string;
  user_docs_url: string;
  oss_server_url: string;
  remove_default_image_registry: boolean;
  predefined_login_organization: string;
  overridden_logo_sources: string;
  license_need_validated: boolean;
  sso_source: string;
  sso_guide_page_on: boolean;
  login_pattern: 'code' | 'password' | 'local';
  sso_configuration: {
    configs: OidcConfig[];
    default_org_name: string;
  };
}

export enum SsoTypes {
  TSF = 'tsf',
  CMB = 'cmb',
}

export enum LoginPattern {
  PASSWORD = 'password',
  CODE = 'code',
  LOCAL = 'local',
}

export interface OidcConfig {
  auth_url: string;
  issuer: string;
  client_id: string;
  client_secret: string;
  display_name: string;
  source_id: string;
  redirect_uri: string;
  authorization_endpoint: string;
  token_endpoint: string;
  userinfo_endpoint: string;
  scopes_supported: string[];
  response_types_supported: string[];
  claims_supported: string[];
  logout_endpoint: string;
}

/**
 *  Authenticated user's account information
 */
export declare interface RcAccount {
  namespace: string;
  username: string;
}

/**
 * Model for user profile
 * /v1/auth/profile
 */
export declare interface RcProfile {
  account_type: number;
  company: string;
  email: string;
  logo_file: string;
  mobile: string;
  username: string;
  is_admin: boolean;
}
