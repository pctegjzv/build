import { ApplicationRef, enableProdMode } from '@angular/core';
import { enableDebugTools } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { initEnvironments } from 'app2/utils/bootstrap';

import { AppModule } from './app/app.module';

export const platformRef = platformBrowserDynamic();

export async function main() {
  const ENVIRONMENTS = await initEnvironments();
  window['ENVIRONMENTS'] = ENVIRONMENTS;

  if (!ENVIRONMENTS['debug']) {
    enableProdMode();
  }
  return platformRef
    .bootstrapModule(AppModule)
    .then(module => {
      const applicationRef = module.injector.get(ApplicationRef);
      const appComponent = applicationRef.components[0];
      enableDebugTools(appComponent);
    })
    .catch(err => console.error(err));
}

// support async tag or hmr
switch (document.readyState) {
  case 'interactive':
  case 'complete':
    main();
    break;
  case 'loading':
  default:
    document.addEventListener('DOMContentLoaded', () => main());
}
