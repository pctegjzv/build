// This must be loaded before Zone.js, since it will override the async functions
// import 'babel-polyfill';
import 'reflect-metadata';

// Zone js
import 'zone.js';

// Other libraries
import 'expose-loader?$!expose-loader?jQuery!jquery';

// Import styles
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';

// semantic ui components
import 'semantic-ui-button/button.min.css';
import 'semantic-ui-dropdown/dropdown.min.css';
import 'semantic-ui-dropdown/dropdown.min.js';
import 'semantic-ui-icon/icon.min.css';
import 'semantic-ui-input/input.min.css';
import 'semantic-ui-label/label.min.css';
import 'semantic-ui-popup/popup.min.css';
import 'semantic-ui-popup/popup.min.js';
import 'semantic-ui-transition/transition.min.css';
import 'semantic-ui-transition/transition.min.js';
