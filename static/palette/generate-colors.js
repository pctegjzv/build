const fs = require('fs');
const colors = require('./palette').colors;

let basicColors = '/* basic-colors */\n';
let curColor = '';
for (const color in colors) {
  const cur = color.split('-')[0];
  if (curColor === cur) {
    basicColors += `$${color}: ${colors[color]};\n`;
  } else {
    curColor = cur;
    basicColors += `\n// ${curColor}\n$${color}: ${colors[color]};\n`;
  }
}

fs.writeFileSync('static/app2/shared/styles/_colors.palette.scss', basicColors);
/* eslint no-console: 0 */
console.log('complete at: static/app2/shared/styles/_colors.palette.scss');
