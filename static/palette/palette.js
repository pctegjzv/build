/**
 * Created by xyhe on 11/15/2017.
 */

const tinycolor = require('tinycolor2');
const COLORS = require('./paletteColors');

/**
 * usage:
 *   1. 使用颜色: 这个模块export所有的颜色，在需要的地方import { colors } 或 require
 *   2. 添加颜色: 当需要添加新的颜色时，只需要添加6号色为基础色到paletteColors模块即可
 *   3. 通过调色板生成颜色到[_colors.palette]: npm run colors
 *
 * 颜色生成算法: https://github.com/ant-design/ant-design/tree/734beb84ffc3f0469fbae1566aa8450f966cb261/components/style/color
 *
 */

const colorEasing = (() => {
  const NEWTON_ITERATIONS = 4;
  const NEWTON_MIN_SLOPE = 0.001;
  const SUBDIVISION_PRECISION = 0.0000001;
  const SUBDIVISION_MAX_ITERATIONS = 10;

  const kSplineTableSize = 11;
  const kSampleStepSize = 1.0 / (kSplineTableSize - 1.0);

  const float32ArraySupported = typeof Float32Array === 'function';

  function a(aA1, aA2) {
    return 1.0 - 3.0 * aA2 + 3.0 * aA1;
  }
  function b(aA1, aA2) {
    return 3.0 * aA2 - 6.0 * aA1;
  }
  function c(aA1) {
    return 3.0 * aA1;
  }

  // Returns x(t) given t, x1, and x2, or y(t) given t, y1, and y2.
  function calcBezier(aT, aA1, aA2) {
    return ((a(aA1, aA2) * aT + b(aA1, aA2)) * aT + c(aA1)) * aT;
  }

  // Returns dx/dt given t, x1, and x2, or dy/dt given t, y1, and y2.
  function getSlope(aT, aA1, aA2) {
    return 3.0 * a(aA1, aA2) * aT * aT + 2.0 * b(aA1, aA2) * aT + c(aA1);
  }

  function binarySubdivide(aX, aA, aB, mX1, mX2) {
    let currentX,
      currentT,
      i = 0;
    do {
      currentT = aA + (aB - aA) / 2.0;
      currentX = calcBezier(currentT, mX1, mX2) - aX;
      if (currentX > 0.0) {
        aB = currentT;
      } else {
        aA = currentT;
      }
    } while (
      Math.abs(currentX) > SUBDIVISION_PRECISION &&
      ++i < SUBDIVISION_MAX_ITERATIONS
    );
    return currentT;
  }

  function newtonRaphsonIterate(aX, aGuessT, mX1, mX2) {
    for (let i = 0; i < NEWTON_ITERATIONS; ++i) {
      const currentSlope = getSlope(aGuessT, mX1, mX2);
      if (currentSlope === 0.0) {
        return aGuessT;
      }
      const currentX = calcBezier(aGuessT, mX1, mX2) - aX;
      aGuessT -= currentX / currentSlope;
    }
    return aGuessT;
  }

  const bezierEasing = (mX1, mY1, mX2, mY2) => {
    if (!(0 <= mX1 && mX1 <= 1 && 0 <= mX2 && mX2 <= 1)) {
      throw new Error('bezier x values must be in [0, 1] range');
    }

    // Precompute samples table
    const sampleValues = float32ArraySupported
      ? new Float32Array(kSplineTableSize)
      : new Array(kSplineTableSize);
    if (mX1 !== mY1 || mX2 !== mY2) {
      for (let i = 0; i < kSplineTableSize; ++i) {
        sampleValues[i] = calcBezier(i * kSampleStepSize, mX1, mX2);
      }
    }

    function getTForX(aX) {
      let intervalStart = 0.0;
      let currentSample = 1;
      const lastSample = kSplineTableSize - 1;

      for (
        ;
        currentSample !== lastSample && sampleValues[currentSample] <= aX;
        ++currentSample
      ) {
        intervalStart += kSampleStepSize;
      }
      --currentSample;

      // Interpolate to provide an initial guess for t
      const dist =
        (aX - sampleValues[currentSample]) /
        (sampleValues[currentSample + 1] - sampleValues[currentSample]);
      const guessForT = intervalStart + dist * kSampleStepSize;

      const initialSlope = getSlope(guessForT, mX1, mX2);
      if (initialSlope >= NEWTON_MIN_SLOPE) {
        return newtonRaphsonIterate(aX, guessForT, mX1, mX2);
      } else if (initialSlope === 0.0) {
        return guessForT;
      } else {
        return binarySubdivide(
          aX,
          intervalStart,
          intervalStart + kSampleStepSize,
          mX1,
          mX2,
        );
      }
    }

    return function bezierEasing(x) {
      if (mX1 === mY1 && mX2 === mY2) {
        return x; // linear
      }
      // Because JavaScript number are imprecise, we should guarantee the extremes are right.
      if (x === 0) {
        return 0;
      }
      if (x === 1) {
        return 1;
      }
      return calcBezier(getTForX(x), mY1, mY2);
    };
  };

  return bezierEasing(0.26, 0.09, 0.37, 0.18);
})();

const colorPalette = (() => {
  const warmDark = 0.5; // warm color darken radio
  const warmRotate = -26; // warm color rotate degree
  const coldDark = 0.55; // cold color darken radio
  const coldRotate = 10; // cold color rotate degree
  const getShadeColor = c => {
    const shadeColor = tinycolor(c);
    // warm and cold color will darken in different radio, and rotate in different degree
    // warmer color
    if (shadeColor.toRgb().r > shadeColor.toRgb().b) {
      return shadeColor
        .darken(shadeColor.toHsl().l * warmDark * 100)
        .spin(warmRotate)
        .toHexString();
    }
    // colder color
    return shadeColor
      .darken(shadeColor.toHsl().l * coldDark * 100)
      .spin(coldRotate)
      .toHexString();
  };
  const primaryEasing = colorEasing(0.6);
  return (color, index) => {
    const currentEasing = colorEasing(index * 0.1);
    // return light colors after tint
    if (index <= 6) {
      return tinycolor
        .mix('#ffffff', color, (currentEasing * 100) / primaryEasing)
        .toHexString();
    }
    return tinycolor
      .mix(
        getShadeColor(color),
        color,
        (1 - (currentEasing - primaryEasing) / (1 - primaryEasing)) * 100,
      )
      .toHexString();
  };
})();

const colors = {};

for (const color in COLORS) {
  for (let i = 1; i < 11; i++) {
    colors[`${color}-${i}`] = colorPalette(COLORS[color], i);
  }
}

module.exports = { colors };
