/**
 * 基础颜色，理论上所有的颜色都由基础色衍生。
 * 每个基础颜色会衍生出共计10个颜色，由浅到深; e.g. red-1 red-2 red-3 ... red-10
 * 其中基础色为这十个颜色中的第六个，例如红色就是red-6
 */
module.exports = {
  red: '#f38686',
  green: '#63d283',
  orange: '#ffc959',
  yellow: '#fffee3',
  blue: '#2fa7df',
  black: '#343c46',
  gray: '#e0e2e3',
  silver: '#f1f4f6',
};
