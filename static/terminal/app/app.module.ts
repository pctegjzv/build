import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  IconModule,
  IconRegistryService,
  InputModule,
  NotificationModule,
  NotificationService,
  TooltipModule,
} from 'alauda-ui';
import { ShellComponent } from 'terminal/app/shell/shell.component';
import { TerminalTranslateModule } from 'terminal/app/translate/translate.module';
import { TranslateService } from 'terminal/app/translate/translate.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TerminalComponent } from './terminal/terminal.component';

const AuiModules = [IconModule, InputModule, TooltipModule, NotificationModule];
const Services = [TranslateService, NotificationService];
const basicIconsUrl = require('alauda-ui/assets/basic-icons.svg');

@NgModule({
  declarations: [AppComponent, TerminalComponent, ShellComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    TerminalTranslateModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ...AuiModules,
  ],
  providers: [...Services],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(iconRegistryService: IconRegistryService) {
    iconRegistryService.registrySvgSymbolsByUrl(basicIconsUrl);
  }
}
