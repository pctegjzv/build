export default {
  dark_theme: 'Dark',
  light_theme: 'Light',
  find: 'Find',
  exec_high_latecency_warning:
    'Connection to remote container is slow and will have an impact on EXEC experience',
  exec_disconnected: 'EXEC has been disconnected',
  exec_reconnect: 'reconnect',
  exec_connecting: 'Connecting, please wait...',
  exec_good_connection: 'Connection to the remote container is good',
  exec_failed: 'EXEC failed',
};
