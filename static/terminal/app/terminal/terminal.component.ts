import { animate, style, transition, trigger } from '@angular/animations';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ShellComponent } from 'terminal/app/shell/shell.component';
import { TerminalPageParams } from 'terminal/app/type';
import { findNext, findPrevious } from 'xterm/lib/addons/search/search';

@Component({
  selector: 'rc-terminal',
  templateUrl: './terminal.component.html',
  styleUrls: ['./terminal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('zoomInOut', [
      transition(':enter', [
        style({ transform: 'scale(0.95)', opacity: '0' }),
        animate(200, style({ transform: 'scale(1)', opacity: '1' })),
      ]),
      transition(':leave', [
        style({ transform: 'scale(1)', opacity: '1' }),
        animate(200, style({ transform: 'scale(0.95)', opacity: '0' })),
      ]),
    ]),
  ],
})
export class TerminalComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(ShellComponent)
  shell: ShellComponent;

  params: TerminalPageParams;
  isFindbarActive = false;
  searchQuery = '';

  private keySub: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit(): void {
    this.params = this.activatedRoute.snapshot
      .queryParams as TerminalPageParams;
  }

  ngAfterViewInit(): void {
    // Capturing Ctrl keyboard events in Javascript
    window.addEventListener('keydown', event => {
      this.onKeyEvent(event);
    });

    this.keySub = this.shell.keyEvent$.subscribe(event => {
      this.onKeyEvent(event);
    });
  }

  ngOnDestroy() {
    this.keySub.unsubscribe();
  }

  get isDarkTheme() {
    return this.shell && this.shell.isDarkTheme;
  }

  onKeyEvent(event: KeyboardEvent) {
    if ((event.metaKey || event.ctrlKey) && event.code === 'KeyF') {
      event.preventDefault();
      event.stopPropagation();
      this.toggleFindPanel();
    }
  }

  toggleFindPanel() {
    this.isFindbarActive = !this.isFindbarActive;
    this.searchQuery = '';
    this.cdr.markForCheck();
  }

  findPrevious(query: string) {
    findPrevious(this.shell.term, query);
  }

  findNext(query: string) {
    findNext(this.shell.term, query);
  }

  focusSearchInput() {
    if (this.isFindbarActive) {
      const inputEl = window.document.querySelector(
        '.terminal-search-bar input',
      ) as HTMLInputElement;
      inputEl.focus();
    } else {
      this.shell.term.focus();
    }
  }
}
