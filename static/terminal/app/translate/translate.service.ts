import { Injectable } from '@angular/core';
import { TranslateService as NgxTranslateService } from '@ngx-translate/core';

/**
 * Translate service.
 * Still using similar approach like what we did in the Angular 1 app, with some exceptions:
 *   - ng2-translate
 * Since we still use the same strategy for language detection, the language used in the console
 * page will be the same in the login page.
 */
@Injectable()
export class TranslateService {
  constructor(private translateService: NgxTranslateService) {
    // 对 window 对象的直接访问在 Angular 2 里面是不被推荐的
    // 如果有空的话，最好还是把 window reference 放到 DI 里比较好
    // 不同标准定义locale的字符串形式不同，
    // 为了统一成目前项目中使用 zh_cn 和 en 两种locale, 需要做一次简单的映射。
    let locale =
      localStorage['alauda_locale'] ||
      this.translateService.getBrowserLang() ||
      'en';
    locale = locale.toLowerCase();

    // 注意这里没有用startsWith('zh'), 是考虑繁体中文用户可能会更倾向于用英语
    // 默认语言为en, 当用户浏览器语言为zh-cn或者zh时， 改为使用zh_cn。
    locale = ['zh_cn', 'zh-cn', 'zh'].includes(locale) ? 'zh_cn' : 'en';
    localStorage['alauda_locale'] = locale;
    this.translateService.use(locale);
  }

  /**
   * Get the translated string
   * @param key
   * @param params
   * @returns {string|any}
   */
  get(key: string, params?: any) {
    return this.translateService.instant(key, params);
  }

  get currentLang(): string {
    return this.translateService.currentLang;
  }

  get otherLang(): string {
    return this.currentLang === 'en' ? 'zh_cn' : 'en';
  }

  /**
   * Toggle between English and Chinese
   */
  toggleLang() {
    localStorage['alauda_locale'] = this.otherLang;
    this.translateService.use(this.otherLang);
  }
}
