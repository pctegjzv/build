export interface TerminalResponse {
  id: string;
}

export interface WSSimpleEvent {
  type: string;
  toString(): string;
}

export interface WSCloseEvent extends WSSimpleEvent {
  code: number;
  reason: string;
  wasClean: boolean;
}

export interface WSMessageEvent extends WSSimpleEvent {
  data: string;
}

export interface ShellFrame {
  Op: string;
  Data?: any;
  SessionID?: string;
  Rows?: number;
  Cols?: number;
}

export interface TerminalPageParams {
  pod: string;
  container: string;
  namespace: string;
  clusterId: string;
  sid?: string;
  resourceKind?: string;
  resourceName?: string;
}
