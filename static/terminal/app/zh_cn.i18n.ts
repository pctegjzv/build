export default {
  dark_theme: '夜间',
  light_theme: '日间',
  find: '查找',
  exec_high_latecency_warning:
    '连接至远程容器的网络状况不佳，EXEC的使用体验可能会受到影响。',
  exec_disconnected: 'EXEC链接已断开',
  exec_reconnect: '重新连接',
  exec_connecting: '正在连接, 请稍后',
  exec_good_connection: '远程容器链接状况良好',
  exec_failed: '连接失败',
};
