// This must be loaded before Zone.js, since it will override the async functions
// import 'babel-polyfill';
import 'reflect-metadata';

// Zone js
import 'zone.js';

// Other libraries
import 'expose-loader?$!expose-loader?jQuery!jquery';
